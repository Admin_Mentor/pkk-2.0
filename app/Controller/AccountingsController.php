<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accounts');
ini_set('memory_limit', '256M');
//set_time_limit(0); 
/**
* Accountings Controller
*/
class AccountingsController extends AppController {
/**
* Scaffold
*
* @var mixed
*/
public $scaffold;
public $uses=[
'Type',
'MasterGroup',
'Group',
'SubGroup',
'AccountHead',
'Journal',
'BankDetail',
'Purchase',
'AssetPurchase',
'Sale',
'SaleItem',
'SalesReturn',
'SalesReturnItem',
'Vendor',
'Party',
'CustomerType',
'Customer',
'State',
'Route',
'CustomerGroup',
'Executive',
'ExecutiveRouteMapping',
'Division',
'ExecutiveBonus',
'ExecutiveBonusDetail',
'ProductBonusDetail',
'Product',
'CustomerDiscount',
'Cheque',
'PayStructure',
'ExpenseVatDetail',
'ExecutiveExpense',
'ExecutiveExpenseDetail',
'District',
'AccSubGroup',
'AccMainGroup',
'SystemParameter',
'District',
'Country',
'AccLedger'
];
public function index()
{
	$Types=$this->Type->find('all');
	$AccountHeads_list=[];
	foreach ($Types as $key => $Type) {
		$To_Tables_Single=[];
		$To_Tables_Single['Type']=$Type['Type']['name'];		
		$To_Tables_Single['Type_id']=$Type['Type']['id'];		
		$To_Tables_Single['AccountHead']='';
		$MasterGroup=$this->MasterGroup->find('all',array(
			'conditions'=>array(
				'MasterGroup.type_id'=>$Type['Type']['id'],
				),
			));
		if(!empty($MasterGroup))
		{
			foreach ($MasterGroup as $key => $MasterGroup) {
				$To_Tables_Single['MasterGroup']=$MasterGroup['MasterGroup']['name'];
				$To_Tables_Single['MasterGroup_id']=$MasterGroup['MasterGroup']['id'];
				if(!empty($MasterGroup['Group'])) {
					foreach ($MasterGroup['Group'] as $key => $Group) {
						$To_Tables_Single['Group']=$Group['name'];
						$To_Tables_Single['Group_id']=$Group['id'];
						$SubGroups=$this->SubGroup->find('all',array(
							'conditions'=>array(
								'SubGroup.group_id'=>$Group['id'],
								)
							));
						if(!empty($SubGroups))
						{
							foreach ($SubGroups as $key => $SubGroup) {
								$To_Tables_Single['SubGroup']=$SubGroup['SubGroup']['name'];
								$To_Tables_Single['SubGroup_id']=$SubGroup['SubGroup']['id'];
								$AccountHeads=$this->AccountHead->find('all',array(
									'conditions'=>array(
										'AccountHead.sub_group_id'=>$SubGroup['SubGroup']['id'],
										)
									));
								if(!empty($AccountHeads))
								{
									foreach ($AccountHeads as $key => $AccountHead) {
										$To_Tables_Single['AccountHead']=$AccountHead['AccountHead']['name'];
										$To_Tables_Single['AccountHead_id']=$AccountHead['AccountHead']['id'];
										$To_Tables_Single['opening_balance']=$AccountHead['AccountHead']['opening_balance'];
										$To_Tables_Single['created_at']=$AccountHead['AccountHead']['created_at'];
										array_push($AccountHeads_list, $To_Tables_Single);
									}
								}
								else
								{
									$To_Tables_Single['AccountHead']='';
									$To_Tables_Single['AccountHead_id']='';
									$To_Tables_Single['opening_balance']='';
									$To_Tables_Single['created_at']='';
									array_push($AccountHeads_list, $To_Tables_Single);
								}
							}
						}
						else
						{
							$To_Tables_Single['AccountHead']='';
							$To_Tables_Single['AccountHead_id']='';
							$To_Tables_Single['opening_balance']='';
							$To_Tables_Single['created_at']='';
							$To_Tables_Single['SubGroup']='';
							$To_Tables_Single['SubGroup_id']='';
							array_push($AccountHeads_list, $To_Tables_Single);
						}
					}
				}
				else
				{
					$To_Tables_Single['Group']='';
					$To_Tables_Single['Group_id']='';
					$To_Tables_Single['SubGroup']='';
					$To_Tables_Single['SubGroup_id']='';
					array_push($AccountHeads_list, $To_Tables_Single);
				}
			}	
		}
		else
		{
			$Group=$this->Group->find('all',array(
				'conditions'=>array(
					'Group.type_id'=>$Type['Type']['id'],
					),
				));
			foreach ($Group as $key => $Group) {
				$To_Tables_Single['Group']=$Group['Group']['name'];
				$To_Tables_Single['SubGroup']='';
				$To_Tables_Single['AccountHead']='';
				array_push($AccountHeads_list, $To_Tables_Single);
			}
		}
	}
	$this->set('AccountHeads',$AccountHeads_list);
}
// Capital Start
public function Capital()
{	
	$master_group_id=4;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['Capital']['date']=$date;
		$data['Capital']['date']=$date;
		$data['Capital']['opening_balance']['']='0.00';
		$this->request->data=$data;
		// $AccountHead=$this->AccountHead->find('all',array(
		// 	'joins'=>array(
		// 		array(
		// 			'table'=>'groups',
		// 			'alias'=>'Group',
		// 			'type'=>'INNER',
		// 			'conditions'=>array('Group.id=SubGroup.group_id')
		// 			),
		// 		),
		// 	'fields'=>array(
		// 		'AccountHead.*',
		// 		'SubGroup.*',
		// 		'Group.*',
		// 		),
		// 	'conditions'=>array('master_group_id'=>$master_group_id)
		// 	));
		// $this->set('AccountHead',$AccountHead);
		$group_id_capital=10;
		$group_id_drawing=11;
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id_capital);
		foreach ($SubGroup_list as $key => $value) {
			$value=explode('CAPITAL',$value);
			$SubGroup_list[$key]=$value[0];
		}
		$this->set('SubGroup_list',$SubGroup_list);
	}
	else
	{
		
		$this->redirect(array('controller' => 'Accountings','action' =>'Capital'));
	}
}
public function Capital_Table_ajax()
  {
    $requestData=$this->request->data;
    $columns = [];
    //$columns[]='AccountHead.id';
    $columns[]='AccountHead.created_at';
    $columns[]='SubGroup.name';
    $columns[]='AccountHead.name';
    $columns[]='AccountHead.opening_balance';
    $columns[]='AccountHead.id';
   // $columns[]='AccountHead.id';
    $conditions=[];
    $master_group_id=4;
    $conditions['Group.master_group_id']=$master_group_id;
    if(!empty($requestData['category']))
    {
    	$capital_group_id=$requestData['category'];
    	$capital_sub_group=$this->SubGroup->findById($capital_group_id);
		$capital_sub_group_name=explode('CAPITAL',$capital_sub_group['SubGroup']['name']);
		$this->SubGroup->unbindModel(
            array('hasMany' => array(
              'AccountHead',
              ))
            );
		$SubGroup=$this->SubGroup->find('list',array(
			'conditions'=>array(
				'SubGroup.name LIKE' =>'%'. $capital_sub_group_name[0] . '%',
			),
			'fields'=>array(
				'SubGroup.id','SubGroup.name',
			),
		));
		$All_Location=[];
		foreach ($SubGroup as $key_grp => $value_grp) {
			$Single_Location=$key_grp;
			array_push($All_Location, $Single_Location);
		}
		
        $conditions['AccountHead.sub_group_id']=$All_Location;
    }
    $totalData=$this->AccountHead->find('count',[
    	'joins'=>[
			[
			'table'=>'groups',
			'alias'=>'Group',
			'type'=>'INNER',
			'conditions'=>array('Group.id=SubGroup.group_id')
			],
		],
    	'conditions'=>$conditions]);

    $totalFiltered=$totalData;
    if( !empty($requestData['search']['value']) ) { 
      $q=$requestData['search']['value'];
      $conditions['OR']=array(
        'AccountHead.id LIKE' =>'%'. $q . '%',
        'AccountHead.name LIKE' =>'%'. $q . '%',
        'AccountHead.opening_balance LIKE' =>'%'. $q . '%',
        'SubGroup.name LIKE' =>'%'. $q . '%',
        'AccountHead.created_at LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
      );
      $totalFiltered=$this->AccountHead->find('count',[
      	'joins'=>array(
		array(
			'table'=>'groups',
			'alias'=>'Group',
			'type'=>'INNER',
			'conditions'=>array('Group.id=SubGroup.group_id')
			),
		),
        'conditions'=>$conditions,
      ]);
    }
    $Data=$this->AccountHead->find('all',array(
	'joins'=>array(
		array(
			'table'=>'groups',
			'alias'=>'Group',
			'type'=>'INNER',
			'conditions'=>array('Group.id=SubGroup.group_id')
			),
		),
	'conditions'=>$conditions,
	'offset'=>$requestData['start'],
    'limit'=>$requestData['length'],
    'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
	'fields'=>array(
		'AccountHead.*',
		'SubGroup.*',
		'Group.*',
		),
	));
	$sl_no=1;
    foreach ($Data as $key => $value) {
      $Data[$key]['AccountHead']['id'] =$sl_no;
      $Data[$key]['AccountHead']['created_at'] =date('d-m-Y',strtotime($value['AccountHead']['created_at']));
      $Data[$key]['AccountHead']['action'] ='<span><i table_id="'.$value['AccountHead']['id'].'" class="fa fa-2x fa-edit   edit_head   blue-col"></i></span>&nbsp;&nbsp;';
      $Data[$key]['AccountHead']['action'].="<span><i table_account_name='".$value['AccountHead']['name']."' class='fa fa-2x fa-trash delete_head blue-col'></i></span>";
      $sl_no++;
    }
    $json_data = array(
      "draw"           =>intval($requestData['draw']),
      "recordsTotal"   =>intval($totalData),
      "recordsFiltered"=>intval($totalFiltered),
      "records"        =>$Data
    );
    echo json_encode($json_data); exit;
  }
public function Capital_account_add_ajax()
{
    $data=$this->request->data['Capital'];
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_AccountHead->begin();
		$capital_sub_group=$this->SubGroup->findById($data['category_add_modal']);
		$capital_sub_group_name=explode('CAPITAL',$capital_sub_group['SubGroup']['name']);
		$SubGroup=$this->SubGroup->find('all',array(
			'conditions'=>array(
				'SubGroup.name LIKE' =>'%'. $capital_sub_group_name[0] . '%',
			),
		));
		if(empty($data['opening_balance_drawing']))
			$data['opening_balance_drawing']=0;
		if(empty($data['opening_balance_capital']))
			$data['opening_balance_capital']=0;
		foreach ($SubGroup as $key => $value) {
			if($value['Group']['name']=='Drawing')
			{
				$SubGroup[$key]['type']='DRAWING';
				$SubGroup[$key]['opening_balance']=$data['opening_balance_drawing'];
			}
			else
			{
				$SubGroup[$key]['type']='CAPITAL';
				$SubGroup[$key]['opening_balance']=$data['opening_balance_capital'];
			}
				
		}
		foreach ($SubGroup as $key => $value) {
			$SubGroup_id=$value['SubGroup']['id'];
			$account_name=strtoupper($data['name']);
			$AccountHead_date=[
			'name'=>$account_name.' '.$value['type'],
			'opening_balance'=>$value['opening_balance'],
			'description'=>$data['description'],
			'sub_group_id'=>$SubGroup_id,
			'created_at'=>date('Y-m-d H:i:s',strtotime($data['date'])),
			'updated_at'=>date('Y-m-d H:i:s',strtotime($data['date'])),
			];
			$this->AccountHead->create();
			if(!$this->AccountHead->save($AccountHead_date))
			{
				$errors = $this->AccountHead->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
		}
		$datasource_AccountHead->commit();
		$last_insert_id=$this->AccountHead->getLastInsertId();
	    $AccountHead=$this->AccountHead->findById($last_insert_id);
	    if(empty($AccountHead))
	       throw new Exception("Empty AccountHead", 1);
	    $return['AccountHead'] ='<option value="'.$AccountHead['AccountHead']['id'].'">'.$AccountHead['AccountHead']['name'].'</option>';
		$return['result']='Success';
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$datasource_AccountHead->rollback();
		$this->Session->setFlash(__($return['message']));
	}
	echo json_encode($return);
    exit;
}
public function Capital_account_edit_ajax()
{
    $data=$this->request->data['Capital'];
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_AccountHead->begin();
		$capital_group=$this->SubGroup->findById($data['category_edit_modal']);
		$capital_group_name=explode(' ',$capital_group['SubGroup']['name']);
		$this->SubGroup->unbindModel(array('hasMany'=>array('AccountHead')));
		$SubGroup=$this->SubGroup->find('all',array(
			'conditions'=>array(
				'SubGroup.name LIKE' =>'%'. $capital_group_name[0] . '%',
			),
		));
		foreach ($SubGroup as $key => $value) {
			if($value['SubGroup']['group_id']==11)
			{
				$drawing_sub_group_id=$value['SubGroup']['id'];
			}
			else
			{
				$capital_sub_group_id=$value['SubGroup']['id'];
			}
				
		}
		$account_head=$this->AccountHead->findById($data['edit_modal_id']);
		$account_head_name=explode(' ',$account_head['AccountHead']['name']);
		$AccountHead=$this->AccountHead->find('all',array(
			'conditions'=>array(
				'AccountHead.name LIKE' =>'%'. $account_head_name[0] . '%',
				'SubGroup.group_id' =>array(10,11),
			),
		));
		foreach ($AccountHead as $key => $value) {
			if($value['SubGroup']['group_id']==11)
			{
				$AccountHead[$key]['type']='DRAWING';
				$AccountHead[$key]['opening_balance']=$data['edit_opening_balance_drawing'];
				$AccountHead[$key]['sub_group_id']=$drawing_sub_group_id;
			}
			else
			{
				$AccountHead[$key]['type']='CAPITAL';
				$AccountHead[$key]['opening_balance']=$data['edit_opening_balance_capital'];
				$AccountHead[$key]['sub_group_id']=$capital_sub_group_id;
			}
				
		}
		foreach ($AccountHead as $key => $value) {
			$AccountHead_id=$value['AccountHead']['id'];
			$account_name=strtoupper($data['edit_name_modal']);
			$account_head_name=$account_name.' '.$value['type'];
			// pr($account_head_name);
			// pr($value['AccountHead']['name']);
			// if($account_head_name!=$value['AccountHead']['name'])
			// {
			// 	if(!$this->AccountHead->saveField('name',$account_head_name))
		 //    		throw new Exception("Error Processing in Updation", 1);
			// }
		 //    if(!$this->AccountHead->saveField('opening_balance',$value['opening_balance']))
		 //    	throw new Exception("Error Processing in Updation", 1);
		 //    if(!$this->AccountHead->saveField('sub_group_id',$value['sub_group_id']))
		 //    	throw new Exception("Error Processing brand_id Updation", 1);
		 //    if(!$this->AccountHead->saveField('description',$data['edit_description']))
		 //    	throw new Exception("Error Processing in Updation", 1);
		 //    if(!$this->AccountHead->saveField('updated_at',date('Y-m-d H:i:s',strtotime($data['date']))))
		 //    	throw new Exception("Error Processing in Updation", 1);
		    $AccountHead_data=[
			//'name'=>$account_head_name,
			'opening_balance'=>$value['opening_balance'],
			'sub_group_id'=>$value['sub_group_id'],
			'description'=>$data['edit_description'],
			'updated_at'=>date('Y-m-d H:i:s',strtotime($data['date'])),
			];
			$this->AccountHead->id=$AccountHead_id;
			if(!$this->AccountHead->Save($AccountHead_data)) {
				$errors = $this->AccountHead->validationErrors; foreach ($errors as $key => $value) {throw new Exception($value[0], 1); }
			}
		}
		//exit;
		$datasource_AccountHead->commit();
		$return['result']='Success';
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$datasource_AccountHead->rollback();
		//$this->Session->setFlash(__($return['message']));
	}
	echo json_encode($return);
    exit;
}
public function capital_sub_category_add_ajax()
{
	$datasource_SubGroup = $this->SubGroup->getDataSource();
	try {
		$datasource_SubGroup->begin();
		$capital_group_id=10;
		$drawing_group_id=10;
		$data=$this->request->data;
		$name=trim($data['name']);
		if(!$name)
			throw new Exception("Empty name", 1);
		$group_id=$data['group_id'];			
		if(!$group_id)
			throw new Exception("Empty group_id", 1);
		$Capital_Drawing_id=[
		['id'=>'11','name'=>'DRAWING'],
		['id'=>'10','name'=>'CAPITAL',]
		];
		for ($i=0; $i <2 ; $i++) {
			$SubGroup_date=[
			'name'=>strtoupper($name.' '.$Capital_Drawing_id[$i]['name']),
			'group_id'=>$Capital_Drawing_id[$i]['id'],
			'created_at'=>date('Y-m-d H:i:s',strtotime($data['date'])),
			'updated_at'=>date('Y-m-d H:i:s',strtotime($data['date'])),
			];
			$this->SubGroup->create();
			if(!$this->SubGroup->save($SubGroup_date))
			{
				$errors = $this->SubGroup->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$SubGroup_id=$this->SubGroup->getLastInsertId();
			$SubGroup=$this->SubGroup->findById($SubGroup_id);
		}
		$datasource_SubGroup->commit();
		$return['result']='Success';
		$return['key']=$SubGroup['SubGroup']['id'];
		$group_name=explode('CAPITAL',$SubGroup['SubGroup']['name']);
		$return['value']=$group_name[0];
		
	} catch (Exception $e) {
		$datasource_SubGroup->rollback();
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function capital_sub_category_edit_ajax()
{
	$datasource_SubGroup = $this->SubGroup->getDataSource();
	try {
		$datasource_SubGroup->begin();
		$capital_group_id=10;
		$drawing_group_id=10;
		// $outstanding=20;
		// $prepaid=5;
		// $direct=12;
		// $indirect=13;
		// $restricted_ids=['9','10','11','12'];
		$data=$this->request->data;
		
		$name=trim($data['name']);
		if(!$name)
			throw new Exception("Empty name", 1);
		$id=$data['id'];
		// if(in_array($id, $restricted_ids))
		// 	throw new Exception("You Cant Edit This Head", 1);
		if(!$id)
			throw new Exception("Empty id", 1);
		$capital_sub_group=$this->SubGroup->findById($id);
		$capital_sub_group_name=explode('CAPITAL',$capital_sub_group['SubGroup']['name']);
		$SubGroup=$this->SubGroup->find('all',array(
			'conditions'=>array(
				'SubGroup.name LIKE' =>'%'. $capital_sub_group_name[0] . '%',
			),
		));
		foreach ($SubGroup as $key => $value) {
			if($value['Group']['name']=='Drawing')
			{
				$SubGroup[$key]['type']='DRAWING';
			}
			else
			{
				$SubGroup[$key]['type']='CAPITAL';
			}
				
		}
		foreach ($SubGroup as $key => $value) {
			$function_return=$this->General_SubGroup_edit_function(strtoupper($name).' '.$value['type'],$value['SubGroup']['id']);
			if($function_return['result']!='Success')
				throw new Exception($function_return['result']);
		}
		$datasource_SubGroup->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$datasource_SubGroup->rollback();
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function capital_sub_category_delete_ajax($id)
{
	$datasource_SubGroup = $this->SubGroup->getDataSource();
	try {
		$datasource_SubGroup->begin();
		if(!$id)
			throw new Exception("Empty id", 1);
		$capital_sub_group=$this->SubGroup->findById($id);
		$capital_sub_group_name=explode('CAPITAL',$capital_sub_group['SubGroup']['name']);
		$this->SubGroup->unbindModel(array('hasMany'=>array('AccountHead')));
		$SubGroup=$this->SubGroup->find('all',array(
			'conditions'=>array(
				'SubGroup.name LIKE' =>'%'. $capital_sub_group_name[0] . '%',
			),
		));
		foreach ($SubGroup as $key => $value) {
			$AccountHead=$this->AccountHead->findBySubGroupId($value['SubGroup']['id']);
			if(empty($AccountHead))
			{
				if(!$this->SubGroup->delete($value['SubGroup']['id']))
        			throw new Exception("Error Processing While delete", 1);
			}
			else
			{
				throw new Exception("Contains a Capital Head", 1);
			}
		}
		$datasource_SubGroup->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$datasource_SubGroup->rollback();
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function CapitalAccount_get_ajax($id)
{
	$AccountHead=$this->AccountHead->findById($id);
	$AccountHead_name_array=explode(' ',$AccountHead['AccountHead']['name']);
	$SubGroup_name_array=explode(' ',$AccountHead['SubGroup']['name']);
	$AccountHead_Capital=$this->AccountHead->findByName($AccountHead_name_array[0].' CAPITAL');
	$AccountHead_Drawing=$this->AccountHead->findByName($AccountHead_name_array[0].' DRAWING');
	$this->SubGroup->unbindModel(array('hasMany'=>array('AccountHead')));
	$Capital_group=$this->SubGroup->findByName($SubGroup_name_array[0].' CAPITAL');
	$return['Capital_group']['id']=$Capital_group['SubGroup']['id'];
	$return['AccountHead']['name']=$AccountHead_name_array[0];
	$return['AccountHead_Capital']['id']=$AccountHead_Capital['AccountHead']['id'];
	$return['AccountHead_Capital']['name']=$AccountHead_Capital['AccountHead']['name'];
	$return['AccountHead_Capital']['description']=$AccountHead_Capital['AccountHead']['description'];
	$return['AccountHead_Capital']['opening_balance']=$AccountHead_Capital['AccountHead']['opening_balance'];
	$return['AccountHead_Drawing']['id']=$AccountHead_Drawing['AccountHead']['id'];
	$return['AccountHead_Drawing']['name']=$AccountHead_Drawing['AccountHead']['name'];
	$return['AccountHead_Drawing']['opening_balance']=$AccountHead_Drawing['AccountHead']['opening_balance'];
	echo json_encode($return);
	exit;
}
public function get_mode_ajax($sub_group_id)
{
	$function_return=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
	$return['result']='Error';
	if(!empty($function_return))
	{
		$return['result']='Success';
		$return['options']=$function_return;
	}
	else
	{
		$return['message']='Empty';
	}
	echo json_encode($return);
	exit;
}
public function capital_account_edit()
{
	$data=$this->request->data['Capital'];
	$datasource_SubGroup = $this->SubGroup->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_SubGroup->begin();
		$datasource_AccountHead->begin();
		$Capital_Drawing_id=[
		['id'=>'10','name'=>'CAPITAL',],
		['id'=>'11','name'=>'DRAWING']
		];
		for ($i=0; $i <2 ; $i++) { 
			$AccountHead_id=$data['account_id'][$i];
			$AccountHead=$this->AccountHead->findById($AccountHead_id);
			if(empty($AccountHead))
				throw new Exception("Empty AccountHead", 1);
			$this->AccountHead->id=$AccountHead['AccountHead']['id'];
			$AccountHead_data=[
			'name'=>strtoupper($data['account_name'][$i]),
			'opening_balance'=>$data['opening_balance'][$i],
			'updated_at'=>date('Y-m-d',strtotime($data['date'])),
			];
			if(!$this->AccountHead->save($AccountHead_data))
				throw new Exception("Error Processing Request", 1);
			$this->SubGroup->id=$AccountHead['SubGroup']['id']; 
			$SubGroup_data=[
			'name'=>strtoupper($data['account_name'][$i]),
			'updated_at'=>date('Y-m-d',strtotime($data['date'])),
			];
			if(!$this->SubGroup->save($SubGroup_data))
				throw new Exception("Error Processing Request", 1);
		}
		$datasource_SubGroup->commit();
		$datasource_AccountHead->commit();
		$return['result']='Success';
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$datasource_SubGroup->rollback();
		$datasource_AccountHead->rollback();
		$this->Session->setFlash(__($return['message']));
	}
	$this->redirect(array('controller' => 'Accountings','action' =>'Capital'));
}
public function CapitalTransactions()
{
	$user_id=1;
	$capital_group_id=10;
	$drawing_group_id=11;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['Journal']['date']=$date;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all',array(
			'fields'=>array('AccountHead.id','AccountHead.name','SubGroup.group_id',),
			'conditions'=>array('SubGroup.group_id'=>$capital_group_id)
			));
		$Accounts=[];
		foreach ($AccountHead as $key => $value) {
			$Accounts[$value['AccountHead']['id']] = $value['AccountHead']['name'];
		}
		asort($Accounts);
		$this->set('Accounts',$Accounts);
		$Group=$this->Group->find('list',array('conditions'=>array('master_group_id'=>4)));
		$this->set('Type',$Group);
		$SubGroup=$this->SubGroup->find('list',arraY('conditions'=>array('group_id'=>$capital_group_id)));
		$AccountHeadCapital=$this->AccountHead->find('all',array(
			'fields'=>array('AccountHead.id','AccountHead.name','AccountHead.opening_balance','AccountHead.created_at','SubGroup.group_id'),
			'conditions'=>array('group_id'=>$capital_group_id)
			));
		$All_Account=[];
		function array_serach($array,$name)
		{
			foreach ($array as $key=>$val) {
				if ($val['name'] == $name) { return $key; }
			}
			return null;
		}
		$AccountsCapital=[];
		foreach ($AccountHeadCapital as $key => $value) {
			array_push($AccountsCapital, $value['AccountHead']['id']);
			$full_name=$value['AccountHead']['name'];
			$name=explode(' ',$full_name);
			$Single_Account['name']=$name[0];
			$Single_Account['date']=$value['AccountHead']['created_at'];
			$Single_Account['Capital']=$value['AccountHead']['opening_balance'];
			$Single_Account['Drawing']='0';
			$Account_Key=null;
			if(!empty($All_Account))
			{
				$Account_Key=array_serach($All_Account,$name[0]);
				if($Account_Key!=null)
				{
					$All_Account[$Account_Key]['Capital']+=$Single_Account['Capital'];
					if(strtotime($All_Account[$Account_Key]['date'])<strtotime($Single_Account['date']))
						$All_Account[$Account_Key]['date']=$Single_Account['date'];
				}
				else
				{
					array_push($All_Account, $Single_Account);
				}
			}
			else
			{
				array_push($All_Account, $Single_Account);
			}
		}
		$AccountHeadDrawing=$this->AccountHead->find('all',array(
			'fields'=>array('AccountHead.id','AccountHead.name','AccountHead.opening_balance','AccountHead.created_at','SubGroup.group_id'),
			'conditions'=>array('group_id'=>11)
			));
		$AccountsDrawing=[];
		foreach ($AccountHeadDrawing as $key => $value) {
			array_push($AccountsDrawing, $value['AccountHead']['id']);
			$full_name=$value['AccountHead']['name'];
			$name=explode(' ',$full_name);
			$Single_Account['name']=$name[0];
			$Single_Account['date']=$value['AccountHead']['created_at'];
			$Single_Account['Capital']='';
			$Single_Account['Drawing']=$value['AccountHead']['opening_balance'];
			$Account_Key=null;
			if(!empty($All_Account))
			{
				$Account_Key=array_serach($All_Account,$name[0]);
				if($Account_Key!=null || $Account_Key==0)
				{
					$All_Account[$Account_Key]['Drawing']+=$Single_Account['Drawing'];
					if(strtotime($All_Account[$Account_Key]['date'])<strtotime($Single_Account['date']))
						$All_Account[$Account_Key]['date']=$Single_Account['date'];
				}
				else
				{
					array_push($All_Account, $Single_Account);
				}
			}
			else
			{
				array_push($All_Account, $Single_Account);
			}
		}
		$JournalCapital=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.credit'=>$AccountsCapital,
				'Journal.flag=1',
				),
			'fields'=>array(
				'Journal.*',
				'AccountHeadCredit.*',
				),
			));
		$JournalDrawing=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.debit'=>$AccountsDrawing,
				'Journal.flag=1',
				),
			'fields'=>array(
				'Journal.*',
				'AccountHeadDebit.*',
				),
			));		
		foreach ($JournalCapital as $key => $value) {
			$full_name=$value['AccountHeadCredit']['name'];
			$name=explode(' ',$full_name);
			$Single_Account['name']=$name[0];
			$Single_Account['date']=$value['Journal']['date'];
			$Single_Account['Capital']=$value['Journal']['amount'];
			$Single_Account['Drawing']='0';
			$Account_Key=null;
			if(!empty($All_Account))
			{
				$Account_Key=array_serach($All_Account,$name[0]);
				if($Account_Key!=null || $Account_Key==0)
				{
					$All_Account[$Account_Key]['Capital']+=$Single_Account['Capital'];
					if(strtotime($All_Account[$Account_Key]['date'])<strtotime($Single_Account['date']))
						$All_Account[$Account_Key]['date']=$Single_Account['date'];
				}
				else
				{
					array_push($All_Account, $Single_Account);
				}
			}
			else
			{
				array_push($All_Account, $Single_Account);
			}
		}
		foreach ($JournalDrawing as $key => $value) {
			$full_name=$value['AccountHeadDebit']['name'];
			$name=explode(' ',$full_name);
			$Single_Account['name']=$name[0];
			$Single_Account['date']=$value['Journal']['date'];
			$Single_Account['Drawing']=$value['Journal']['amount'];
			$Account_Key=null;
			if(!empty($All_Account))
			{
				$Account_Key=array_serach($All_Account,$name[0]);
				if($Account_Key!=null || $Account_Key==0)
				{
					$All_Account[$Account_Key]['Drawing']+=$Single_Account['Drawing'];
					if(strtotime($All_Account[$Account_Key]['date'])<strtotime($Single_Account['date']))
						$All_Account[$Account_Key]['date']=$Single_Account['date'];
				}
				else
				{
					array_push($All_Account, $Single_Account);
				}
			}
			else
			{
				array_push($All_Account, $Single_Account);
			}
		}
		$this->set('All_Account',$All_Account);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$debit_credit_array=array(
				$capital_group_id=>array(
					'credit'=>$data['account_head'],
					'debit'=>$data['mode'],
					),
				$drawing_group_id=>array(
					'credit'=>$data['mode'],
					'debit'=>$data['account_head'],
					)
				);
			$credit=$debit_credit_array[$data['Type']]['credit'];
			$debit=$debit_credit_array[$data['Type']]['debit'];
			$amount=$data['amount'];
			$voucher_no='';
			$external_voucher=$data['external_voucher'];
			$remarks=$data['remarks'];
			$date=$data['date'];
			$work_flow='Capital';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$datasource_Journal->commit();
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$datasource_Journal->rollback();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect(array('controller' => 'Accountings','action' =>'CapitalTransactions'));
	}
}
public function Capital_Drawing_select_ajax($id)
{
	$AccountHead=$this->AccountHead->find('all',array(
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'SubGroup.group_id',
			),
		'conditions'=>array('SubGroup.group_id'=>$id)
		));
	$Accounts=[];
	foreach ($AccountHead as $key => $value) {
		$Accounts[$value['AccountHead']['id']] = $value['AccountHead']['name'];
	}
	asort($Accounts);
	echo json_encode($Accounts);
	exit;
}
public function Capital_Drawing_transaction_ajax()
{
	try {
		$data=$this->request->data;
		$name=$data['name'];
		$from_date=date('Y-m-d',strtotime('-1 month'));
		$to_date=date('Y-m-d');
		if(!empty($data['from_date']))
			$from_date=date('Y-m-d',strtotime($data['from_date']));
		if(!empty($data['to_date']))
			$to_date=date('Y-m-d',strtotime($data['to_date']));
		$AccountHeadCapital=$this->AccountHead->find('first',array(
			'conditions'=>array(
				'AccountHead.name'=>trim($name).' CAPITAL',
				)
			));
		if(empty($AccountHeadCapital))
			throw new Exception("Empty AccountHeadCapital", 1);
		$General_Journal_Openging_Debit_N_Credit_function=$this->General_Journal_Openging_Debit_N_Credit_function($AccountHeadCapital['AccountHead']['id'],$from_date);
		$AccountHeadCapital['AccountHead']['opening_balance']+=$General_Journal_Openging_Debit_N_Credit_function['credit']+$General_Journal_Openging_Debit_N_Credit_function['debit'];
		$Journal_Capital=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.credit'=>$AccountHeadCapital['AccountHead']['id'],
				'Journal.flag=1',
				'Journal.date between ? and ?'=>array($from_date,$to_date),
				)
			));
		$Journal_all=[];
		foreach ($Journal_Capital as $key => $value) {
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['mode']=$value['AccountHeadCredit']['name'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['Capital']=$value['Journal']['amount'];
			$Journal_single['Drawing']=0.00;
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		$AccountHeadDrawing=$this->AccountHead->find('first',array(
			'conditions'=>array(
				'AccountHead.name'=>trim($name).' DRAWING',
				)
			));

		if(empty($AccountHeadDrawing))
			throw new Exception("Empty AccountHeadDrawing", 1);
		$General_Journal_Openging_Debit_N_Credit_function=$this->General_Journal_Openging_Debit_N_Credit_function($AccountHeadDrawing['AccountHead']['id'],$from_date);
		$AccountHeadDrawing['AccountHead']['opening_balance']+=$General_Journal_Openging_Debit_N_Credit_function['debit']+$General_Journal_Openging_Debit_N_Credit_function['credit'];
		$Journal_Drawing=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.debit'=>$AccountHeadDrawing['AccountHead']['id'],
				'Journal.flag=1',
				'Journal.date between ? and ?'=>array($from_date,$to_date),
				)
			));
		foreach ($Journal_Drawing as $key => $value) {
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['mode']=$value['AccountHeadCredit']['name'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['Capital']=0.00;
			$Journal_single['Drawing']=$value['Journal']['amount'];
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}

		krsort($Journal_all);
		$return['row']['tbody']='';
		$return['row']['tfoot']='';
		$Balance_Total=0;
		$Capital_Total=$AccountHeadCapital['AccountHead']['opening_balance'];
		$Drawing_Total=$AccountHeadDrawing['AccountHead']['opening_balance'];
		$Capital_Sum_Total=0;
		$Drawing_Sum_Total=0;
		$date=$AccountHeadDrawing['AccountHead']['created_at'];
		if($date<date('Y-m-d',strtotime($from_date)))
			$date=date('Y-m-d',strtotime($from_date));

		$Journal_all[0]=array(
			'id'=>0,
			'date'=>date('d-m-Y',strtotime($date)),
			'mode'=>'Opening Balance',
			'voucher_no'=>'',
			'remarks'=>'',
			'Capital'=>$Capital_Total,
			'Drawing'=>$Drawing_Total,
			);
		//pr($Journal_all);exit;
		foreach ($Journal_all as $key => $value) {
			if(!empty($value['date']))
			{
				$return['row']['tbody'].='<tr class="blue-pddng">';
				$return['row']['tbody'].='<td class="color_label"><span style="display:none" class="journal_id">'.$value['id'].'</span><span>'.date('d-m-Y',strtotime($value['date'])).'</span></td>';
				$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
				$return['row']['tbody'].='<td class="color_label">'.$value['voucher_no'].'</td>';
				$return['row']['tbody'].='<td class="color_label"></td>';
				$return['row']['tbody'].='<td class="color_label"></td>';
				$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
				$return['row']['tbody'].='<td class="color_label text-right">'.floatval($value['Capital']).'</td>';
				$return['row']['tbody'].='<td class="color_label text-right">'.floatval($value['Drawing']).'</td>';
				$Balance=$value['Capital']-$value['Drawing'];
				$Balance_Total+=$Balance;
				$Capital_Sum_Total+=$value['Capital'];
				$Drawing_Sum_Total+=$value['Drawing'];
				$return['row']['tbody'].='<td class="color_label text-right">'.$Balance.'</td>';
				if($value['id']!=0)
					$return['row']['tbody'].='<td><i class="fa fa-trash blue-col delete"></i></td>';
				else
					$return['row']['tbody'].='<td></td>';
				$return['row']['tbody'].='</tr>';
			}
			
		}
		$return['row']['tfoot']='';
		$return['row']['tfoot'].='<tr class="blue-pddng">';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td class="total_amount">Total</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.floatval($Capital_Sum_Total).'</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.floatval($Drawing_Sum_Total).'</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.floatval($Balance_Total).'</td>';
		$return['row']['tfoot'].='<td></td>';

		$return['row']['tfoot'].='</tr>';
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function Capital_delete($name)
{
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$name=explode(' ',trim($name));

		$datasource_AccountHead->begin();
		$AccountHeadCapital=$this->AccountHead->find('first',array(
			'conditions'=>array(
				'AccountHead.name'=>trim($name[0]).' CAPITAL',
				)
			));
		if(empty($AccountHeadCapital))
			throw new Exception("AccountHeadCapital", 1);
		$JournalCredit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.credit'=>$AccountHeadCapital['AccountHead']['id'],'flag=1'),
			));
		if(!empty($JournalCredit))
			throw new Exception("Have Journal Entries so Cant Delete", 1);
		$JournalDebit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.debit'=>$AccountHeadCapital['AccountHead']['id'],'flag=1'),
			));
		if(!empty($JournalDebit))
			throw new Exception("Have Journal Entries so Cant Delete", 1);
		// if(!$this->SubGroup->delete($AccountHeadCapital['SubGroup']['id']))
		// 	throw new Exception("Error Processing SubGroup Deletion", 1);
		if(!$this->AccountHead->delete($AccountHeadCapital['AccountHead']['id']))
			throw new Exception("Error Processing AccountHead Deletion", 1);
		$AccountHeadDrawing=$this->AccountHead->find('first',array(
			'conditions'=>array(
				'AccountHead.name'=>trim($name[0]).' DRAWING',
				)
			));
		if(empty($AccountHeadDrawing))
			throw new Exception("AccountHeadDrawing", 1);
		$JournalCredit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.credit'=>$AccountHeadDrawing['AccountHead']['id'],'flag=1'),
			));
		if(!empty($JournalCredit))
			throw new Exception("Have Journal Entries so Cant Delete", 1);
		$JournalDebit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.debit'=>$AccountHeadDrawing['AccountHead']['id'],'flag=1'),
			));
		if(!empty($JournalDebit))
			throw new Exception("Have Journal Entries so Cant Delete", 1);
		// if(!$this->SubGroup->delete($AccountHeadDrawing['SubGroup']['id']))
		// 	throw new Exception("Error Processing SubGroup Deletion", 1);
		if(!$this->AccountHead->delete($AccountHeadDrawing['AccountHead']['id']))
			throw new Exception("Error Processing AccountHead Deletion", 1);
		$datasource_AccountHead->commit();
		$return['result']='Success';
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$datasource_AccountHead->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$this->Session->setFlash(__($return['message']));
	}
	echo json_encode($return);
	exit;
	//$this->redirect(array('controller' => 'Accountings','action' =>'Capital'));
}
// Capital end
// Asset Start
// Current Asset Start
// Current Asset cash Start
public function AssetCurrentCash()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/AssetCurrentBank';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/AssetCurrentCash'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=1;
	$user_id=1;
	if(!$this->request->data)
	{
		$data['Cash']['date']=date('d-m-Y');
		$data['Cash']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
		try {
			$data=$this->request->data['Cash'];
			$opening_balance=$data['opening_balance'];
			$date=$data['date'];
			$AccountHead_id=$data['account_id'];
			$name=$data['name'];
			$description='';
			$function_return=$this->AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect(array('controller' => 'Accountings','action' =>'AssetCurrentCash'));
	}
}
public function AssetCurrentCashold()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/AssetCurrentCash';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/AssetCurrentCash'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	if(!$this->request->data)
	{
		$data['Cash']['date']=date('d-m-Y');
		$AccountHead=$this->AccountHead->find('first');
		$data['Cash']['opening_balance']=$AccountHead['AccountHead']['opening_balance'];
		$this->request->data=$data;
	}
	else
	{
		try {
			$data=$this->request->data['Cash'];
			$opening_balance=$data['opening_balance'];
			$date=$data['date'];
			$AccountHead_id=1;
			$name='CASH';
			$description='';
			$function_return=$this->AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect(array('controller' => 'Accountings','action' =>'AssetCurrentCash'));
	}
}
public function AssetCurrentCashTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/AssetCurrentCashTransaction';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/AssetCurrentCashTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$user_id=1;
	$sub_group_id=1;
	if(!$this->request->data)
	{
		$data['Journal']['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['Journal']['to_date']=date('d-m-Y');
		$data['to_date']=date('d-m-Y');
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$AccountHead_cash=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$AccountHead_cash_ids = array_keys($AccountHead_cash);
// pr($keys);exit;
		$AccountHead_list=$this->AccountHead->find('list',array(
			'conditions'=>array('NOT'=>array('AccountHead.id'=>$AccountHead_cash_ids))
			));
		$this->set('AccountHeads',$AccountHead_list);
		$conditions_Journal['Journal.date between ? and ?']=array($from_date,$to_date);
		
		$this->request->data=$data;
	}
}
public function Cash_transaction_ajax()
{
		
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='Journal.date';
		$columns[]='AccountHead.name';
		$columns[]='Journal.remarks';
		$columns[]='Journal.debit';
		$columns[]='Journal.credit';
		$columns[]='Journal.id';
	    $conditions=[];
		$conditions['Journal.flag']=1;
		$from_date=date('Y-m-d',strtotime($requestData['from_date']));
		$to_date=date('Y-m-d',strtotime($requestData['to_date']));
		//$conditions['Journal.date <=']=$to_date;
		if(!empty($requestData['account_head'])){
		$conditions['AccountHeadCredit.id']=$requestData['account_head'];
	}
		$conditions['Journal.date between ? and ?']=array($from_date,$to_date);
		$conditions['OR']=array(
				'AccountHeadDebit.acc_sub_group_id' =>5,
				'AccountHeadCredit.acc_sub_group_id' =>5,
				);
		$totalData=$this->Journal->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Journal.date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
				'AccountHeadDebit.name LIKE' =>'%'. $q . '%',
				'AccountHeadCredit.name LIKE' =>'%'. $q . '%',
				'Journal.amount LIKE' =>'%'. $q . '%',
				'Journal.remarks LIKE' =>'%'. $q . '%',
				);
			$totalFiltered=$this->Journal->find('count',[
				'conditions'=>$conditions,
				]);
		}
		$Data=$this->Journal->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Journal.*',
				'AccountHeadCredit.name',
				'AccountHeadDebit.name',
				'AccountHeadCredit.acc_sub_group_id',
				'AccountHeadDebit.acc_sub_group_id',
				)
			));
		//pr($Data);
		$closing_balance=0;
		foreach ($Data as $key => $value) {
	    if($value['AccountHeadCredit']['acc_sub_group_id']==5)
		{
			$Data[$key]['AccountHeadCredit']['name']=$value['AccountHeadDebit']['name'];
			$Data[$key]['Journal']['debit']=0;
			$Data[$key]['Journal']['credit']=floatval($value['Journal']['amount']);
		}
		else
		{
			$Data[$key]['AccountHeadCredit']['name']=$value['AccountHeadCredit']['name'];
			$Data[$key]['Journal']['debit']=floatval($value['Journal']['amount']);
			$Data[$key]['Journal']['credit']=0;
		}
		$closing_balance+=$Data[$key]['Journal']['credit']-$Data[$key]['Journal']['debit'];
		$Data[$key]['Journal']['closing_balance']=$closing_balance;
			$Data[$key]['Journal']['date']=date('d-m-Y',strtotime($value['Journal']['date']));
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	}
	public function Bank_transaction_ajax()
{
		
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='Journal.date';
		$columns[]='AccountHeadCredit.name';
		$columns[]='AccountHeadDebit.name';
		$columns[]='Journal.remarks';
		$columns[]='Journal.debit';
		$columns[]='Journal.credit';
		$columns[]='Journal.id';
	    $conditions=[];
		$conditions['Journal.flag']=1;
		$from_date=date('Y-m-d',strtotime($requestData['from_date']));
		$to_date=date('Y-m-d',strtotime($requestData['to_date']));
		if(!empty($requestData['account_head'])){
			$conditions['OR']=array(
				'AccountHeadDebit.id' =>$requestData['account_head'],
				'AccountHeadCredit.id' =>$requestData['account_head'],
				);
		}
		//$conditions['Journal.date <=']=$to_date;
		$conditions['Journal.date between ? and ?']=array($from_date,$to_date);
		$conditions['OR']=array(
				'AccountHeadDebit.acc_sub_group_id' =>6,
				'AccountHeadCredit.acc_sub_group_id' =>6,
				);
		$totalData=$this->Journal->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Journal.date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
				'AccountHeadDebit.name LIKE' =>'%'. $q . '%',
				'AccountHeadCredit.name LIKE' =>'%'. $q . '%',
				'Journal.amount LIKE' =>'%'. $q . '%',
				'Journal.remarks LIKE' =>'%'. $q . '%',
				);
			$totalFiltered=$this->Journal->find('count',[
				'conditions'=>$conditions,
				]);
		}
		$Data=$this->Journal->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Journal.*',
				'AccountHeadCredit.name',
				'AccountHeadDebit.name',
				'AccountHeadCredit.acc_sub_group_id',
				'AccountHeadDebit.acc_sub_group_id',
				)
			));
		//pr($Data);
		$closing_balance=0;
		foreach ($Data as $key => $value) {
	    if($value['AccountHeadCredit']['acc_sub_group_id']==6)
		{
			$Data[$key]['AccountHeadCredit']['name']=$value['AccountHeadDebit']['name'];
			$Data[$key]['Journal']['debit']=0;
			$Data[$key]['Journal']['credit']=floatval($value['Journal']['amount']);
		}
		else
		{
			$Data[$key]['AccountHeadCredit']['name']=$value['AccountHeadCredit']['name'];
			$Data[$key]['Journal']['debit']=floatval($value['Journal']['amount']);
			$Data[$key]['Journal']['credit']=0;
		}
		$closing_balance+=$Data[$key]['Journal']['credit']-$Data[$key]['Journal']['debit'];
		$Data[$key]['Journal']['closing_balance']=$closing_balance;
			$Data[$key]['Journal']['date']=date('d-m-Y',strtotime($value['Journal']['date']));
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	}
// Current Asset cash End
// Current Asset Bank Start
public function AssetCurrentBank()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/AssetCurrentBank';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/AssetCurrentBank'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=2;
	$user_id=1;
	if(!$this->request->data)
	{
		$data['Bank']['date']=date('d-m-Y');
		$data['Bank']['opening_balance']='0';
		$data['BankDetails']['opening_balance']='0';
		$this->request->data=$data;
		$BankDetail=$this->BankDetail->find('all');
		$this->set('BankDetail',$BankDetail);
	}
	else
	{
		$datasource_BankDetail = $this->BankDetail->getDataSource();
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_BankDetail->begin();
			$datasource_AccountHead->begin();
			$Bank_data=$this->request->data['Bank'];
			$BankDetails_data=$this->request->data['BankDetails'];
			//pr($BankDetails_data);
			//exit;
			$opening_balance=$Bank_data['opening_balance'];
			$name=$Bank_data['name'];
			$date=$Bank_data['date'];
			$account_no=$BankDetails_data['account_no'];
			$branch=$BankDetails_data['branch'];
			$shwn_in_vouchr=$BankDetails_data['shwn_in_vouchr'];
			$description='';
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$AccountHead_id=$this->AccountHead->getLastInsertId();
			$BankDetail_date=[
			'account_head_id'=>$AccountHead_id,
			'account_no'=>$account_no,
			'branch'=>$branch,
			'shwn_in_vouchr'=>$shwn_in_vouchr,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
			'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
			];
			$this->BankDetail->create();
			if(!$this->BankDetail->save($BankDetail_date))
				throw new Exception("Error BankDetail Creation", 1);
			$return['result']='Success';
			$datasource_BankDetail->commit();
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_BankDetail->rollback();
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect(array('controller' => 'Accountings','action' =>'AssetCurrentBank'));
	}
}
public function BankDetails_get_ajax($id)
{
	$BankDetail=$this->BankDetail->find('first',array(
		'conditions'=>array(
			'account_head_id'=>$id,
			),
		'fields'=>array(
			'BankDetail.id',
			'BankDetail.branch',
			'BankDetail.shwn_in_vouchr',
			'BankDetail.account_no',
			//'BankDetail.currency',
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			)
		));
	$return['Bank']['id']=$BankDetail['AccountHead']['id'];
	$return['Bank']['name']=$BankDetail['AccountHead']['name'];
	$return['Bank']['opening_balance']=$BankDetail['AccountHead']['opening_balance'];
	$return['BankDetail']['id']=$BankDetail['BankDetail']['id'];
	$return['BankDetail']['branch']=$BankDetail['BankDetail']['branch'];
	$return['BankDetail']['shwn_in_vouchr']=$BankDetail['BankDetail']['shwn_in_vouchr'];
	
	$return['BankDetail']['account_no']=$BankDetail['BankDetail']['account_no'];
	//$return['BankDetail']['currency']=$BankDetail['BankDetail']['currency'];
	echo json_encode($return);
	exit;
}
public function Bank_account_edit()
{
	$user_id=1;
	$datasource_BankDetail = $this->BankDetail->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_BankDetail->begin();
		$datasource_AccountHead->begin();
		$Bank_data=$this->request->data['Bank'];
		$BankDetails_data=$this->request->data['BankDetails'];
		//pr($BankDetails_data);
		//exit;
		$opening_balance=$Bank_data['opening_balance'];
		$name=$Bank_data['name'];
		$date=$Bank_data['date'];
		$account_no=$BankDetails_data['account_no'];
		$branch=$BankDetails_data['branch'];
		$shwn_in_vouchr=$BankDetails_data['shwn_in_vouchr'];
		$AccountHead_id=$Bank_data['id'];
		$BankDetail_id=$BankDetails_data['id'];
		$description='';
		$function_return=$this->AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description);
		if($function_return['result']!='Success')
			throw new Exception($function_return['message']);
		$BankDetail_date=[
		'account_no'=>$account_no,
		'branch'=>$branch,
		'shwn_in_vouchr'=>$shwn_in_vouchr,
		'modified_by'=>$user_id,
		'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
		];
		$this->BankDetail->id=$BankDetail_id;
		if(!$this->BankDetail->save($BankDetail_date))
			throw new Exception("Error BankDetail Creation", 1);
		$datasource_BankDetail->commit();
		$datasource_AccountHead->commit();
		$return['result']='Success';
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$datasource_BankDetail->rollback();
		$datasource_AccountHead->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$this->Session->setFlash(__($return['message']));
	}
	$this->redirect(array('controller' => 'Accountings','action' =>'AssetCurrentBank'));
}
public function BankAccount_delete($id)
{
	$datasource_BankDetail = $this->BankDetail->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_BankDetail->begin();
		$datasource_AccountHead->begin();
		$BankDetail=$this->BankDetail->findByAccountHeadId($id);
		if(empty($BankDetail))
			throw new Exception("Empty BankDetail", 1);
		$BankDetail_id=$BankDetail['BankDetail']['id'];
		$AccountHead_id=$BankDetail['AccountHead']['id'];
		if(!$this->BankDetail->delete($BankDetail_id))
			throw new Exception("Error Processing BankDetail Deletion", 1);
		$JournalCredit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.credit'=>$AccountHead_id,'flag=1'),
			));
		if(!empty($JournalCredit))
			throw new Exception("This Account have Journal Entry Cant Delete", 1);
		$JournalDebit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.debit'=>$AccountHead_id,'flag=1'),
			));
		if(!empty($JournalDebit))
			throw new Exception("This Account have Journal Entry Cant Delete", 1);
		if(!$this->AccountHead->delete($AccountHead_id))
			throw new Exception("Error Processing AccountHead Deletion", 1);
		$datasource_BankDetail->commit();
		$datasource_AccountHead->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$datasource_BankDetail->rollback();
		$datasource_AccountHead->rollback();
	}
	echo json_encode($return);
	exit;
}
public function AssetCurrentBankTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/AssetCurrentBankTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$user_id=1;
	if(!$this->request->data)
	{
		$data['Journal']['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['Journal']['to_date']=date('d-m-Y');
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$sub_group_id=6;
		$AccountsController = new AccountsController;
		$AccountHead_list=$AccountsController->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$bank=[];
		foreach ($AccountHead_list as $key => $value) {
			array_push($bank, $key);
		}
		$this->set('AccountHeads',$AccountHead_list);
		$this->request->data=$data;
	}
}
// Current Asset Bank End
// Current Asset Account Recievable start
public function get_All_customer_list_ajax()
  {
    $requestData=$this->request->data;
    $columns = [];
    //$columns[]='AccountHead.id';
    $columns[]='Customer.priority';
    $columns[]='Route.name';
    $columns[]='CustomerGroup.name';
    $columns[]='Customer.code';
    $columns[]='CustomerType.name';
    $columns[]='AccountHead.name';
    $columns[]='AccountHead.id';
    $columns[]='Customer.credit_limit';
    $columns[]='AccountHead.id';
    $conditions=[];
	$user_branch_id=$this->Session->read('User.branch_id');
	if($user_branch_id)
	{
		
	}
	if(isset($requestData['customer_type_id']))
	{
		if($requestData['customer_type_id'])
			$conditions['Customer.customer_type_id']=$requestData['customer_type_id'];
	}
	$routearray = [];
	if(isset($requestData['executive_id']))
	{
		if($requestData['executive_id'])
		{
		$executives = $this->ExecutiveRouteMapping->find('all',array(
			          'conditions' => array('ExecutiveRouteMapping.executive_id' =>$requestData['executive_id']),
			          'fields' => array('ExecutiveRouteMapping.route_id'),
	                  ));
		if(count($executives)>0)
		{
			foreach($executives as $key=>$item)
			{
				$routearray[] =$item['ExecutiveRouteMapping']['route_id'];
			}
		}
		if(count($routearray)>0)
		{
		$conditions['Customer.route_id IN']= $routearray;
		}
		else
		{
			$conditions['Customer.route_id']= '';
		}
	   }
	}
	if(isset($requestData['route_id']))
	{
		if($requestData['route_id'])
			$conditions['Customer.route_id']=$requestData['route_id'];
	}
	if(isset($requestData['account_head_id']))
	{
		if($requestData['account_head_id'])
			$conditions['Customer.account_head_id']=$requestData['account_head_id'];
	}
	if(isset($requestData['group']))
	{
		if($requestData['group'])
			$conditions['Customer.customer_group_id']=$requestData['group'];
	}
    $totalData=$this->Customer->find('count',[
    	'joins'=>[],
    	'conditions'=>$conditions]);

    $totalFiltered=$totalData;
    if( !empty($requestData['search']['value']) ) { 
      $q=$requestData['search']['value'];
      $conditions['OR']=array(
        'AccountHead.id LIKE' =>'%'. $q . '%',
        'AccountHead.name LIKE' =>'%'. $q . '%',
        'Customer.credit_limit LIKE' =>'%'. $q . '%',
        'CustomerGroup.name LIKE' =>'%'. $q . '%',
        'CustomerType.name LIKE' =>'%'. $q . '%',
        'Route.name LIKE' =>'%'. $q . '%',
        'Customer.code LIKE' =>'%'. $q . '%',
      );
      $totalFiltered=$this->Customer->find('count',[
      	'joins'=>array(),
        'conditions'=>$conditions,
      ]);
    }
    $Data=$this->Customer->find('all',array(
	'joins'=>array(),
	'conditions'=>$conditions,
	'offset'=>$requestData['start'],
    'limit'=>$requestData['length'],
    'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
	'fields'=>array(
		'AccountHead.*',
		'CustomerGroup.name','Customer.code','Customer.priority','CustomerType.name','Route.name','Customer.credit_limit'
		),
	));
	$sl_no=1;
    foreach ($Data as $key => $value) {
			if($value['Customer']['priority']!=0)
			{
			$Data[$key]['AccountHead']['customer_order']= $value['Customer']['priority'];
			}
			else
			{
             $Data[$key]['AccountHead']['customer_order']="";

			}
			$Data[$key]['AccountHead']['opening_balance']=number_format($value['AccountHead']['opening_balance'],2,'.','');
	$Data[$key]['AccountHead']['priority']='<input class="form-control number priority_edit" style="width:50px" type="text" value="'.$value['Customer']['priority'].'"><i class="fa fa-check fa-lg blue-col edit_priority" ></i>';
	  $Data[$key]['AccountHead']['name'] ='<span style="display:none" class="AccountHead_id">'.$value['AccountHead']['id'].'</span><span>'.$value['AccountHead']['name'].'</span>';
      $Data[$key]['AccountHead']['action'] ='<i class="fa fa-2x fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#Account_modal"></i>&nbsp;&nbsp;';
     // $Data[$key]['AccountHead']['action'].='<a onclick="return confirm("Are you sure?");" href="'.$this->webroot.'Accountings/AccountHead_delete/'.$value['AccountHead']['id'].'"><i class="fa fa-2x fa-trash blue-col blue-col"></i></a>';
      $sl_no++;
    }
    $json_data = array(
      "draw"           =>intval($requestData['draw']),
      "recordsTotal"   =>intval($totalData),
      "recordsFiltered"=>intval($totalFiltered),
      "records"        =>$Data
    );
    echo json_encode($json_data); exit;
  }
public function AssetCurrentAccountRecievable()
{
	$user_branch_id=$this->Session->read('User.branch_id');
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/AssetCurrentAccountRecievable'));
	$Executive_list=$this->Executive->find('list',array('fields'=>['id','name']));
	$this->set(compact('Executive_list'));
	$State_list=$this->State->find('list',array('fields'=>array('id','name')));
  $this->set(compact('State_list'));
	$sub_group_id=3;
	$user_id=1;
	if(!$this->request->data)
	{
		$CustomerType_list=$this->CustomerType->find('list',array('fields'=>array('id','name')));
		$AccountHead_list=$this->AccountHead->find('all',array('joins' => [
			[
			"table" => "customers",
			"alias" => "Customer",
			"type" => "INNER",
			"conditions" => ['AccountHead.id=Customer.account_head_id'],
			],
			],'fields'=>array('AccountHead.id','AccountHead.name','Customer.code'),'conditions'=>array('AccountHead.sub_group_id'=>3)));
		$Customer_list=[];
		foreach ($AccountHead_list as $key => $value) {
			$Customer_list[$value['AccountHead']['id']] = $value['AccountHead']['name'].' '.$value['Customer']['code'];
		}
		$Executive_list=$this->Executive->find('list',array('fields'=>array('id','name')));
		$this->set('Executive_list',$Executive_list);
		$this->Route->virtualFields = array(
			'route_name' => "CONCAT(Route.name, ' ', Route.code)"
			);
		$conditions_branch_route=[];

		if($user_branch_id)
		{
			
		}
		$this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
		$Product_list=$this->Product->find('list',array('fields'=>['id','product_name']));
		$this->set(compact('Product_list'));
		$Route_list=$this->Route->find('list',array('conditions'=>$conditions_branch_route,'fields'=>array('id','route_name')));
		$this->set(compact('Route_list'));
		$CustomerGroup_list=$this->CustomerGroup->find('list',array('fields'=>array('id','name')));
		$this->set(compact('CustomerGroup_list'));
		$Division_list=$this->Division->find('list');
		$this->set(compact('Division_list'));
// $State_list=$this->State->find('list',array('fields'=>array('id','name')));
// $this->set(compact('State_list'));
// $Day_list=array(
//      'Monday'=>'Monday',
//      'Tuesday'=>'Tuesday',
//      'Wednesday'=>'Wednesday',
//      'Thursday'=>'Thursday',
//      'Friday'=>'Friday',
//      'Saturday'=>'Saturday',
//      'Sunday'=>'Sunday',
//      );
//    $this->set(compact('Day_list'));
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
		$data['Customer']['opening_balance']='0';
		$conditions_branch=[];

		if($user_branch_id)
		{
			
		}
		$this->request->data=$data;
		$Customer=$this->Customer->find('all',array(
			'conditions'=>$conditions_branch,
			'order'=>array('AccountHead.name ASC'),
// 'limit'=>110,
			));
		$Customer_All=[];
		foreach ($Customer as $key => $value) {
			$single_row['customer_order']= $value['Customer']['priority'];
			$single_row['route_name']= $value['Route']['name'];
			$single_row['customer_group_name']= $value['CustomerGroup']['name'];
			$single_row['customer_type_name']= $value['CustomerType']['name'];
			$single_row['name']= $value['AccountHead']['name'];
			if($value['Customer']['priority']!=0)
			{
			$single_row['priority']= $value['Customer']['priority'];
			}
			else
			{
             		$single_row['priority']="";

			}
			$single_row['code']= $value['Customer']['code'];
			$single_row['opening_balance']= $value['AccountHead']['opening_balance'];
			$single_row['id']= $value['AccountHead']['id'];
			array_push($Customer_All, $single_row);
		}
		$modalCustomerType_list = $CustomerType_list;
		unset($modalCustomerType_list[1]);
		$this->set('CustomerType_list',$CustomerType_list);
		$this->set('modalCustomerType_list',$modalCustomerType_list);
		$this->set('Customer_list',$Customer_list);
// $this->set('Customer',$Customer);
		$this->set('Customer',$Customer_All);
	}
	else
	{
		$datasource_Customer = $this->Customer->getDataSource();
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_Customer->begin();
			$datasource_AccountHead->begin();
			$AccountHead_data=$this->request->data['AccountHead'];
			$Customer_data=$this->request->data['Customer'];
			$opening_balance=$AccountHead_data['opening_balance'];
			$name=$AccountHead_data['name'];
			$date=$AccountHead_data['date'];
			$place=$Customer_data['place'];
			$code=$Customer_data['code'];
// $day=$Customer_data['day'];
			$route_id=$Customer_data['route_id'];
// $executive_id=$Customer_data['executive_id'];
			$email=$Customer_data['email'];
			$customer_type_id=$Customer_data['customer_type'];
			$mobile=$Customer_data['mobile'];
// $gstin=$Customer_data['gstin'];
           $state_id=$Customer_data['state_id'];
			$description=$Customer_data['description'];
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$AccountHead_id=$this->AccountHead->getLastInsertId();
			$Customer_date=[
			'account_head_id'=>$AccountHead_id,
			'customer_type_id'=>$customer_type_id,
// 'executive_id'=>$executive_id,
			'place'=>$place,
			'code'=>$code,
             'state_id'=>$state_id,
// 'day'=>$day,
			'route_id'=>$route_id,
			'email'=>$email,
			'mobile'=>$mobile,
// 'gstin'=>$gstin,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
			'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
			];
			$this->Customer->create();
			if(!$this->Customer->save($Customer_date))
				throw new Exception("Error Customer Creation", 1);
			$return['result']='Success';
			$datasource_Customer->commit();
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Customer->rollback();
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
//new function
public function AssetCurrentAccountRecievableTransaction()
{
	$sub_group_id=3;
	$user_id=1;
	if(!$this->request->data)
	{
		$CustomerType_list=$this->CustomerType->find('list',array('order'=>['CustomerType.name Asc'],'fields'=>['id','CustomerType.name']));
		$this->set('CustomerType_list',$CustomerType_list);
		$this->Customer->virtualFields = array('customer_name' => "CONCAT(Customer.code,' - ',AccountHead.name)");

		$Customer_list=$this->Customer->find('list',array(
			'joins'=>array(
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'INNER',
					'conditions'=>array('AccountHead.id=Customer.account_head_id')
					),
				),
			'fields'=>['AccountHead.id','customer_name']
			));

		$this->set('Customer_list',$Customer_list);
		$data['Journal']['date']=date('d-m-Y');
		$data['Journal']['customer_type']=1;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y',strtotime('+6 month'));
		$this->request->data=$data;
		$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead_list);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['account_head'];
			$debit=$data['mode'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$external_voucher=$data['external_voucher'];
			$voucher_no='';
			$work_flow='Account Recievable';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect(array('controller' => 'Accountings','action' =>'AssetCurrentAccountRecievableTransaction',$credit,$data['amount']));
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function AssetCurrentAccountRecievableTransaction_table_ajax()
{
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='Customer.code';
	$columns[]='AccountHead.name';
	$columns[]='total';
	$columns[]='recieved';
	$columns[]='balance';
	$conditions=[];
	if(isset($requestData['customer_type_id']))
	{
		if($requestData['customer_type_id'])
			$conditions['Customer.customer_type_id']=$requestData['customer_type_id'];
	}
	if(isset($requestData['account_head_id']))
	{
		if($requestData['account_head_id'])
			$conditions['Customer.account_head_id']=$requestData['account_head_id'];
	}
	$totalData=$this->Customer->find('count',['conditions'=>$conditions]);
$totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
if( !empty($requestData['search']['value']) ) { 
	$q=$requestData['search']['value'];
	$conditions['OR']=array(
		'CustomerType.name LIKE' =>'%'. $q . '%',
		'AccountHead.name LIKE' =>'%'. $q . '%',
		'Customer.code LIKE' =>'%'. $q . '%',
		);
	$totalFiltered=$this->Customer->find('count',[
		'conditions'=>$conditions,
		]);
}

//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column'] <= 1)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
$Data=$this->Customer->find('all',array(
	'conditions'=>$conditions,
	'offset'=>$requestData['start'],
	'limit'=>$requestData['length'],
	//'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
	'order'=> $order,
	'fields'=>array(
		'CustomerType.name',
		'Customer.*',
		'AccountHead.*',
		)
	));

foreach ($Data as $key => $value) {
	$Data[$key]['AccountHead']['date']=date('d-m-Y',strtotime($value['AccountHead']['created_at']));
	$code = $this->Customer->field(
		'Customer.code',
		array('Customer.account_head_id ' => $value['AccountHead']['id']));
	$Data[$key]['Customer']['code']=$code;
	//$Data2[$key]['AccountHead']['name']=$value['AccountHead']['name'];
	$Data[$key]['AccountHead']['total']=$value['AccountHead']['opening_balance'];
	$Data[$key]['AccountHead']['recieved']=0;
	$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
	$Data[$key]['AccountHead']['total']+=$Debit_N_Credit_function['debit'];
	$Data[$key]['AccountHead']['recieved']+=$Debit_N_Credit_function['credit'];
	$Data[$key]['AccountHead']['balance']=$Data[$key]['AccountHead']['total']-$Data[$key]['AccountHead']['recieved'];
// $Data[$key]['AccountHead']['balance']=floatval($Data[$key]['AccountHead']['balance']);
	$Data[$key]['AccountHead']['total']=number_format(($Data[$key]['AccountHead']['total']),3,'.','');
	$Data[$key]['AccountHead']['recieved']=number_format(($Data[$key]['AccountHead']['recieved']),3,'.','');
	$Data[$key]['AccountHead']['balance']=number_format(($Data[$key]['AccountHead']['balance']),3,'.','');
}
//sorting total/balance/recieved
		if($requestData['order'][0]['column'] >= 2) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["AccountHead"][$columnname] == $b["AccountHead"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return (str_replace(',','',$a["AccountHead"][$columnname]) < str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return (str_replace(',','',$a["AccountHead"][$columnname]) > str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
			}
			

			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
//print_r($Data2);exit;

//$Data2 =$this->array_msort($Data2, array('total'=>SORT_DESC));
//$Data2 = array_values($Data2);

$json_data = array(
"draw"               => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
"recordsTotal"       => intval( $totalData ),  // total number of records
"recordsFiltered"    => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
"records"            => $Data  // total data array
);
echo json_encode($json_data);
exit;

}
//new function end
public function AssetCurrentAccountRecievableTransactions()
{
	$sub_group_id=3;
	$user_id=1;
	if(!$this->request->data)
	{
		$CustomerType_list=$this->CustomerType->find('list',array('order'=>['CustomerType.name Asc'],'fields'=>['id','CustomerType.name']));
		$this->set('CustomerType_list',$CustomerType_list);
		$Customer_list=$this->Customer->find('list',array(
			'joins'=>array(
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'INNER',
					'conditions'=>array('AccountHead.id=Customer.account_head_id')
					),
				),
			'conditions'=>['Customer.customer_type_id'=>1],
			'fields'=>['AccountHead.id','AccountHead.name'])
		);
		$this->set('Customer_list',$Customer_list);
		$data['Journal']['date']=date('d-m-Y');
		$data['Journal']['customer_type']=1;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$this->request->data=$data;
		$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead_list);
		$AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$account_all=[];
		foreach ($AccountHeads as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['total']=$valueA['AccountHead']['opening_balance'];
			$account_single['Recieved']='0';
			$Sale=$this->Sale->find('all',array(
				'conditions'=>array(
					'Sale.account_head_id'=>$valueA['AccountHead']['id'],
					'status>1',
					),
				'fields'=>array(
					'Sale.*',
					)
				));
			foreach ($Sale as $key => $value) {
				$account_single['total']+=$value['Sale']['total'];
			}
			$Credit_function_return=$this->General_Journal_Credit_function($valueA['AccountHead']['id']);
			$account_single['Recieved']+=$Credit_function_return['credit'];
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['account_head'];
			$debit=$data['mode'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$work_flow='Crediters/Supliers';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function AssetCurrentAccountRecievableTransactionold()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/AssetCurrentAccountRecievableTransaction';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/AssetCurrentAccountRecievableTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=3;
	$user_id=1;
	if(!$this->request->data)
	{
		$CustomerType_list=$this->CustomerType->find('list',array('order'=>['CustomerType.name Asc'],'fields'=>['id','CustomerType.name']));
		$this->set('CustomerType_list',$CustomerType_list);
		$Customer_list=$this->Customer->find('list',array(
			'joins'=>array(
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'INNER',
					'conditions'=>array('AccountHead.id=Customer.account_head_id')
					),
				),
			'conditions'=>['Customer.customer_type_id'=>1],
			'fields'=>['AccountHead.id','AccountHead.name'])
		);
		$this->set('Customer_list',$Customer_list);
		$data['Journal']['date']=date('d-m-Y');
		$data['Journal']['customer_type']=1;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$this->request->data=$data;
		$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead_list);
		$AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$account_all=[];
		foreach ($AccountHeads as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['total']=$valueA['AccountHead']['opening_balance'];
			$account_single['Recieved']='0';
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id']);
			$account_single['total']+=$Debit_N_Credit_function['debit'];
			$account_single['Recieved']+=$Debit_N_Credit_function['credit'];
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$SalesList=$this->request->data['Sales'];
			$credit=$data['account_head'];
			$debit=$data['mode'];
// $amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
// $voucher_no=$data['voucher_no'];
			$work_flow='Account Recievable';
			for ($i=0; $i <count($SalesList['id']) ; $i++) {
				$id=$SalesList['id'][$i];
				$receipt=$SalesList['amount'][$i];
				$Sale=$this->Sale->findById($id);
				if(!$Sale)
					throw new Exception("Empty Sales", 1);
				if($receipt>0)
				{
					$invoice_no=$Sale['Sale']['invoice_no'];
					$remarks='Sale Invoice No :'.$invoice_no;
// $function_return=$this->JournalCreate($credit,$debit,$receipt,$date,$remarks,$work_flow,$user_id,$voucher_no);
					$function_return=$this->JournalCreate($credit,$debit,$receipt,$date,$remarks,$work_flow,$user_id);
					if($function_return['result']!='Success')
						throw new Exception($function_return['message'], 1);
				}
			}
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function customer_search_ajax()
{
	$data=$this->request->data;
	$conditions=array();
	if(!empty($this->request->data['route_id']))
	{
		$conditions['Customer.route_id']=$this->request->data['route_id'];
	}
	$routearray = [];
	if(!empty($this->request->data['executive_id']))
	{
		$executives = $this->ExecutiveRouteMapping->find('all',array(
			          'conditions' => array('ExecutiveRouteMapping.executive_id' =>$this->request->data['executive_id']),
			          'fields' => array('ExecutiveRouteMapping.route_id'),
	                  ));
		if(count($executives)>0)
		{
			foreach($executives as $key=>$item)
			{
				$routearray[] =$item['ExecutiveRouteMapping']['route_id'];
			}
		}
		if(count($routearray)>0)
		{
		$conditions['Customer.route_id IN']= $routearray;
		}
		else
		{
			$conditions['Customer.route_id']= '';
		}
	}
	if(!empty($this->request->data['group']))
	{
		if($this->request->data['group']!='select')
		{
			$conditions['Customer.customer_group_id']=$this->request->data['group'];
		}
	}
	if(!empty($data['customer_type']))
	{
		$conditions['Customer.customer_type_id']=$data['customer_type'];
	}
	if(!empty($data['account_head_id']))
	{
		$conditions['Customer.account_head_id']=$data['account_head_id'];
	}
	$opening_balance=0;
	$Customer=$this->Customer->find('all',array('conditions'=>$conditions));
	$data['row']='';
	if($Customer)
	{
		foreach ($Customer as $key => $value)
		{
			$data['row'].='<tr>';
			$data['row'].='<td></td>';
			$data['row'].='<td>'.$value["Route"]["name"].'</td>';
			$data['row'].='<td>'.$value["CustomerGroup"]["name"].'</td>';
			$data['row'].='<td>'.$value["CustomerType"]["name"].'</td>';
			$data['row'].='<td>'.$value["Customer"]["code"].'</td>';
			$data['row'].='<td><span style="display:none" class="AccountHead_id">'.$value['AccountHead']['id'].'</span><span>'.$value['AccountHead']['name'].'</span></td>';
            $data['row'].='<td><input class="form-control number priority_edit" style="width:50px" type="text" value="'.$value['Customer']['priority'].'"><i class="fa fa-check fa-lg blue-col edit_priority" ></i></td>';
			$data['row'].='<td>'.$value["AccountHead"]["opening_balance"].'</td>';
			$data['row'].='<td><i class="fa fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#Account_modal"></i></td>';
			$data['row'].='<td><a OnClick="return confirm("blah blah");" href="'.$this->webroot.'Accountings/AccountHead_delete/'.$value['AccountHead']['id'].'"><i class="fa fa-trash blue-col blue-col"></i></a></td>';
			$opening_balance+=$value["AccountHead"]["opening_balance"];
			$data['row'].='</tr>';
		}
		$data['foot']='<tr class="blue-pddng toggle_class">';
		$data['foot'].='<td></td>';
		$data['foot'].='<td></td>';
		$data['foot'].='<td></td>';
		$data['foot'].='<td></td>';
		$data['foot'].='<td></td>';
		$data['foot'].='<td class="total_amount"><b>Total</b></td>';
		$data['foot'].='<td></td>';
		$data['foot'].='<td class="total_amount"><b>'.$opening_balance.'</b></td>';
		$data['foot'].='<td></td>';
		$data['foot'].='<td></td>';
		$data['foot'].='</tr>';
		$data['result']='Success';
	}
	else
	{
		$data['result']='Error';
	}
	echo json_encode($data);
	exit;
}
public function executive_select_ajax()
{
	$executive_id=$this->request->data['executive_id'];
	$Customer=$this->ExecutiveRouteMapping->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			),
		'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive_id),
		'group' => 'Route.id',
		'fields' =>'Route.id,Route.name'
		));
	echo '<option value="">select</option>';
	foreach ($Customer as $key => $value) {
		echo '<option value='.$value['Route']['id'].'>'.$value['Route']['name'].'</option>';
	}
	exit;
}
public function route_select_ajax()
{
	$route_id=$this->request->data['route_id'];
//	$Customer=$this->Customer->find('all',array('conditions'=>array('route_id'=>$route_id),'group' => 'Customer.day'));
//	echo '<option value="">select</option>';
//	foreach ($Customer as $key => $value) {
//		echo '<option value='.$value['Customer']['id'].'>'.$value['Customer']['day'].'</option>';
//	}
	$Customer=$this->CustomerGroup->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
				),
			),
		'conditions'=>array('route_id'=>$route_id),'group' => 'Customer.customer_group_id'));
	echo '<option value="">select</option>';
	foreach ($Customer as $key => $value) {
		echo '<option value='.$value['CustomerGroup']['id'].'>'.$value['CustomerGroup']['name'].'</option>';
	}
	exit;
}
public function group_select_ajax()
{
	$group=$this->request->data['group'];
	$route_id=$this->request->data['route_id'];
	$Customer=$this->CustomerType->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.customer_type_id=CustomerType.id')
				),
			),
		'conditions'=>array('customer_group_id'=>$group,'route_id'=>$route_id),'group' => 'CustomerType.id'));
	echo '<option value="">select</option>';
	foreach ($Customer as $key => $value) {
		echo '<option value='.$value['CustomerType']['id'].'>'.$value['CustomerType']['name'].'</option>';
	}
	exit;
}
public function day_select_ajax()
{
	$day=$this->request->data['day'];
	$route_id=$this->request->data['route_id'];
	$Customer=$this->Customer->find('all',array('conditions'=>array('day'=>$day,'route_id'=>$route_id),'group' => 'CustomerType.id'));
	echo '<option value="">select</option>';
	foreach ($Customer as $key => $value) {
		echo '<option value='.$value['CustomerType']['id'].'>'.$value['CustomerType']['name'].'</option>';
	}
	exit;
}
public function customer_type_select_ajax()
{
	$conditions = array();
	if($this->request->data['customer_type']!=''){
		$customer_type_id=$this->request->data['customer_type'];
		$conditions['customer_type_id'] =$customer_type_id;
	}
	if($this->request->data['route_id']!=''){
		$route_id=$this->request->data['route_id'];
		$conditions['route_id'] = $route_id;
	}
// $day=$this->request->data['day'];
// $executive_id=$this->request->data['executive_id'];
	$Customer=$this->Customer->find('all',
		array('conditions'=>$conditions,
// 	array('customer_type_id'=>$customer_type_id,
// // 'day'=>$day,
// 'route_id'=>$route_id,
// // 'executive_id'=>$executive_id
// ),
			'group' => 'Customer.id'));
	echo '<option value="">select</option>';
	foreach ($Customer as $key => $value) {
		echo '<option value='.$value['AccountHead']['id'].'>'.$value['AccountHead']['name'].'</option>';
	}
	exit;
}
public function route_customer_select_ajax(){
	$conditions = array();
	if($this->request->data['route_id']!=''){
		$route_id=$this->request->data['route_id'];
		$conditions['route_id'] = $route_id;
	}
// $day=$this->request->data['day'];
// $executive_id=$this->request->data['executive_id'];
	$Customer=$this->Customer->find('all',
		array('conditions'=>$conditions,
// 	array('customer_type_id'=>$customer_type_id,
// // 'day'=>$day,
// 'route_id'=>$route_id,
// // 'executive_id'=>$executive_id
// ),
			'group' => 'Customer.id'));
	echo '<option value="">select</option>';
	foreach ($Customer as $key => $value) {
		echo '<option value='.$value['AccountHead']['id'].'>'.$value['AccountHead']['name'].'</option>';
	}
	exit;
}
public function customer_group_select_ajax(){
	$conditions = array();
	if($this->request->data['route_id']!=''){
		$route_id=$this->request->data['route_id'];
		$conditions['route_id'] = $route_id;
	}
	if($this->request->data['customer_group']!=''){
		$customer_group=$this->request->data['customer_group'];
		$conditions['customer_group_id'] = $customer_group;
	}
// $day=$this->request->data['day'];
// $executive_id=$this->request->data['executive_id'];
	$Customer=$this->Customer->find('all',
		array('conditions'=>$conditions,
// 	array('customer_type_id'=>$customer_type_id,
// // 'day'=>$day,
// 'route_id'=>$route_id,
// // 'executive_id'=>$executive_id
// ),
			'group' => 'Customer.id'));
	echo '<option value="">select</option>';
	foreach ($Customer as $key => $value) {
		echo '<option value='.$value['AccountHead']['id'].'>'.$value['AccountHead']['name'].'</option>';
	}
	exit;

}
public function Customer_get_ajax($id)
{
	$return['result']='Error';
	$Customer=$this->Customer->find('first',array(
		'conditions'=>array('Customer.account_head_id'=>$id),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			'AccountHead.description',
			'Customer.id',
			'CustomerType.id',
             'Customer.state_id',
			'Customer.place',
			'Customer.arabic_name',
			'Customer.code',
			'Customer.email',
			'Customer.mobile',
// 'Customer.gstin',
// 'Executive.name',
			'Route.name',
			'Route.id',
			'CustomerGroup.name',
// 'Customer.day',
// 'Customer.executive_id',
			'Customer.route_id',
			'Customer.customer_group_id',
			'Customer.contact_person',
			'Customer.credit_limit',
			'Customer.credit_period',
			'Customer.division_id',
			'Customer.vat_no',
			)
		));

	$Customer_discounts=$this->CustomerDiscount->find('all',array(
		'conditions'=>array('account_head_id'=>$id),
		));
	if(!empty($Customer))
	{
		$return['data']=$Customer;
		$return['discount_data']=$Customer_discounts;
		$return['result']='Success';	
	}
	else
	{
		$return['message']='Empty';
	}
//pr($return);exit;
	echo json_encode($return);
	exit;
}
public function Customer_account_edit()
{
	$user_id=1;
	$datasource_Customer = $this->Customer->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_Customer->begin();
		$datasource_AccountHead->begin();
		$AccountHead_data=$this->request->data['CustomerEdit'];

		$name=$AccountHead_data['name'];
		$arabic_name_edit=$AccountHead_data['arabic_name'];
		$arabic_name_arabic_text=trim(strtoupper($arabic_name_edit));
		$AccountHead_id=$AccountHead_data['AccountHead_id'];
		$opening_balance=$AccountHead_data['opening_balance'];
		$date=date('Y-m-d H:i:s');
		$description=$AccountHead_data['description'];
		$Customer_id=$AccountHead_data['customer_id_hide'];
		$state_id=$AccountHead_data['edit_state_id'];
		$place=$AccountHead_data['place_edit'];
		if(empty($AccountHead_data['route_id']))
		{
			$route='';
		}
		else
		{
			$route=$AccountHead_data['route_id'];
		}
		if(empty($AccountHead_data['customer_group_edit']))
		{
			$group='';
		}
		else
		{
			$group=$AccountHead_data['customer_group_edit'];
		}
		$customer_type_id=$AccountHead_data['customer_type_edit'];
		$email=$AccountHead_data['email'];
		$mobile=$AccountHead_data['mobile'];
		$customer_discount=$AccountHead_data['collection_edit'];
		$contact_person=$AccountHead_data['contact_person'] ? $AccountHead_data['contact_person']:'';
		$credit_limit=$AccountHead_data['credit_limit'];

		$credit_period=$AccountHead_data['credit_period_edit'];
		$division="";
		$vat_no=$AccountHead_data['vat_no_edit'];
// $discount_percentage=$AccountHead_data['discount_percentage'];

		$function_return=$this->AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description);

		if($function_return['result']!='Success')
			throw new Exception($function_return['message']);
		if(!empty($place)){
			$Customer_data['place']= $place;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('place',$place))
				throw new Exception("Cant Updated Address", 1);
		}
		if(!empty($customer_type_id)){
			$Customer_data['customer_type_id']= $customer_type_id;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('customer_type_id',$customer_type_id))
				throw new Exception("Cant Updated Customer Type", 1);
		}
		if(!empty($route)){
			$Customer_data['route_id']= $route;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('route_id',$route))
				throw new Exception("Cant Updated Route", 1);
		}
		if(!empty($state_id)){
			$Customer_data['state_id']= $state_id;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('state_id',$state_id))
				throw new Exception("Cant Updated Route", 1);
		}
		if(!empty($group)){
			$Customer_data['customer_group_id']= $group;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('customer_group_id',$group))
				throw new Exception("Cant Updated Customer Group", 1);
		}
		if(!empty($email)){
			$Customer_data['email']= $email;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('email',$email))
				throw new Exception("Cant Updated Customer Email", 1);
		}
		if(!empty($arabic_name_arabic_text)){
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('arabic_name',$arabic_name_arabic_text))
				throw new Exception("Cant Updated Customer Arabic Name", 1);
		}
		if(!empty($mobile)){
			$Customer_data['mobile']= $mobile;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('mobile',$mobile))
				throw new Exception("Cant Updated Customer mobile", 1);
		}
		if(!empty($contact_person)){
			$Customer_data['contact_person']= $contact_person;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('contact_person',$contact_person))
				throw new Exception("Cant Updated Contact person", 1);
		}
		
		// if(!empty($credit_limit)){

			$Customer_data['credit_limit']= $credit_limit;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('credit_limit',$credit_limit))
				throw new Exception("Cant Updated Credit limit", 1);
		// }
		if(!empty($credit_period)){
			$Customer_data['credit_period']= $credit_period;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('credit_period',$credit_period))
				throw new Exception("Cant Updated Credit period", 1);
		}
		if(!empty($division)){
			$Customer_data['division']= $division;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('division',$division))
				throw new Exception("Cant Updated Division", 1);
		}
// if(!empty($discount_percentage)){
// 	$Customer_data['discount_percentage']= $discount_percentage;
// 	$this->Customer->id=$Customer_id;
// 	if(!$this->Customer->saveField('discount_percentage',$discount_percentage))
// 		throw new Exception("Cant Updated discount percentage", 1);
// }
		if(!empty($customer_discount)){
			$Customer_data['collection_discount']= $customer_discount;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('collection_discount',$customer_discount))
				throw new Exception("Cant Updated customer discount", 1);
		}

		if(!empty($vat_no)){
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('vat_no',$vat_no))
				throw new Exception("Cant Updated Customer vat no", 1);
		}
		$Customer_data['modified_by']= $user_id;
		$this->Customer->id=$Customer_id;
		if(!$this->Customer->saveField('modified_by',$user_id))
			throw new Exception("Cant Updated ", 1);
		$Customer_data['updated_at']= date('Y-m-d H:i:s',strtotime($date));
		$this->Customer->id=$Customer_id;
		if(!$this->Customer->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date))))
			throw new Exception("Cant Updated ", 1);
		if(isset($AccountHead_data['products'])){
			for ($i=0; $i < count($AccountHead_data['products']); $i++) { 
				if(isset($AccountHead_data['discount_id'][$i])){
					$this->CustomerDiscount->id=$AccountHead_data['discount_id'][$i];
					if(!$this->CustomerDiscount->saveField('selling_rate',$AccountHead_data['selling_rates'][$i]))
						throw new Exception("Cant Updated Customer Selling Rate", 1);
				}
				else
				{
					$new_data=array(
						'account_head_id'=>$AccountHead_id,
						'product_id'=>$AccountHead_data['products'][$i],
						'selling_rate'=>$AccountHead_data['selling_rates'][$i],
						'created_at'=>date('Y-m-d'),
						'updated_at'=>date('Y-m-d')
						);
					$this->CustomerDiscount->create();
					if(!$this->CustomerDiscount->save($new_data))
						throw new Exception("Error Processing Request", 1);		
				}
			}
		}
		$datasource_Customer->commit();
		$datasource_AccountHead->commit();
		$return['result']='Success';
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$datasource_Customer->rollback();
		$datasource_AccountHead->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$this->Session->setFlash(__($return['message']));
	}
	echo json_encode($return);
	exit;
}
public function Customer_account_edit_old()
{
	$user_id=1;
	$datasource_Customer = $this->Customer->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_Customer->begin();
		$datasource_AccountHead->begin();
		$AccountHead_data=$this->request->data['CustomerEdit'];
// pr($AccountHead_data);exit;
		$name=$AccountHead_data['name'];
		$arabic_name_edit=$AccountHead_data['arabic_name_edit'];
		$arabic_name_arabic_text=trim(strtoupper($arabic_name_edit));

		$AccountHead_id=$AccountHead_data['AccountHead_id'];
		$opening_balance=$AccountHead_data['opening_balance_edit'];
		$date=date('Y-m-d H:i:s');
		$description=$AccountHead_data['description_edit'];
		$Customer_id=$AccountHead_data['customer_id_hide'];
		$place=$AccountHead_data['place_edit'];
//$executive_id=$AccountHead_data['executive_id_edit'];
		$route=$AccountHead_data['route_id_edit'];
		$group=$AccountHead_data['customer_group_id_edit'];
// $day=$AccountHead_data['day_edit'];
		$customer_type_id=$AccountHead_data['customer_type_edit'];
// $code=$AccountHead_data['code_edit'];
		$email=$AccountHead_data['email_edit'];
		$mobile=$AccountHead_data['mobile_edit'];
// $gstin=$AccountHead_data['gstin_edit'];
// $state_id=$AccountHead_data['state_id_edit'];
		$contact_person=$AccountHead_data['contact_person_edit'] ? $AccountHead_data['contact_person_edit']:'';
		$credit_limit=$AccountHead_data['credit_limit_edit'];
		$credit_period=$AccountHead_data['credit_period_edit'];
		$division=$AccountHead_data['division_edit'];
		$vat_no=$AccountHead_data['vat_no'];
		$function_return=$this->AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description);
		if($function_return['result']!='Success')
			throw new Exception($function_return['message']);
		if(!empty($place)){
			$Customer_data['place']= $place;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('place',$place))
				throw new Exception("Cant Updated Address", 1);
		}
		if(!empty($customer_type_id)){
			$Customer_data['customer_type_id']= $customer_type_id;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('customer_type_id',$customer_type_id))
				throw new Exception("Cant Updated Customer Type", 1);
		}
		if(!empty($route)){
			$Customer_data['route_id']= $route;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('route_id',$route))
				throw new Exception("Cant Updated Route", 1);
		}
		if(!empty($group)){
			$Customer_data['customer_group_id']= $group;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('customer_group_id',$group))
				throw new Exception("Cant Updated Customer Group", 1);
		}
		if(!empty($email)){
			$Customer_data['email']= $email;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('email',$email))
				throw new Exception("Cant Updated Customer Email", 1);
		}
		if(!empty($product_name_arabic_text)){
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('arabic_name',$arabic_name_arabic_text))
				throw new Exception("Cant Updated Customer Arabic Name", 1);
		}
		if(!empty($mobile)){
			$Customer_data['mobile']= $mobile;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('mobile',$mobile))
				throw new Exception("Cant Updated Customer mobile", 1);
		}
		if(!empty($contact_person)){
			$Customer_data['contact_person']= $contact_person;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('contact_person',$contact_person))
				throw new Exception("Cant Updated Contact person", 1);
		}
		if(!empty($credit_period)){
			$Customer_data['credit_period']= $credit_period;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('credit_period',$credit_period))
				throw new Exception("Cant Updated Credit period", 1);
		}
		if(!empty($credit_limit)){
			$Customer_data['credit_limit']= $credit_limit;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('credit_limit',$credit_limit))
				throw new Exception("Cant Updated Credit limit", 1);
		}
		if(!empty($division)){
			$Customer_data['division']= $division;
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('division',$division))
				throw new Exception("Cant Updated Division", 1);
		}
		if(!empty($vat_no)){
			$this->Customer->id=$Customer_id;
			if(!$this->Customer->saveField('vat_no',$vat_no))
				throw new Exception("Cant Updated Customer vat no", 1);
		}
		$Customer_data['modified_by']= $user_id;
		$this->Customer->id=$Customer_id;
		if(!$this->Customer->saveField('modified_by',$user_id))
			throw new Exception("Cant Updated ", 1);
		$Customer_data['updated_at']= date('Y-m-d H:i:s',strtotime($date));
		$this->Customer->id=$Customer_id;
		if(!$this->Customer->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date))))
			throw new Exception("Cant Updated ", 1);
// $this->Customer->id=$Customer_id;
// $fl = $this->Customer->save($Customer_data);
// pr($fl);exit;
// if(!$this->Customer->save($Customer_data))
// 	throw new Exception("Error Customer Updation", 1);
		$datasource_Customer->commit();
		$datasource_AccountHead->commit();
		$return['result']='Success';
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$datasource_Customer->rollback();
		$datasource_AccountHead->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$this->Session->setFlash(__($return['message']));
	}
	exit;
}
public function Customer_delete($id)
{
	$datasource_Customer = $this->Customer->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_Customer->begin();
		$datasource_AccountHead->begin();
		$Customer=$this->Customer->findByAccountHeadId($id);
		if(empty($Customer))
			throw new Exception("Empty Customer", 1);
		$Customer_id=$Customer['Customer']['id'];
		$AccountHead_id=$Customer['AccountHead']['id'];
		$JournalCredit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.credit'=>$AccountHead_id,'flag=1'),
			));
		if(!empty($JournalCredit))
			throw new Exception("This Account have Journal Entry Cant Delete", 1);
		$JournalDebit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.debit'=>$AccountHead_id,'flag=1'),
			));
		if(!empty($JournalDebit))
			throw new Exception("This Account have Journal Entry Cant Delete", 1);
		if(!$this->AccountHead->delete($AccountHead_id))
			throw new Exception("Error Processing AccountHead Deletion", 1);
		if(!$this->Customer->delete($Customer_id))
			throw new Exception("Error Processing Customer Deletion", 1);
		$datasource_Customer->commit();
		$datasource_AccountHead->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$datasource_Customer->rollback();
		$datasource_AccountHead->rollback();
	}
	echo json_encode($return);
	exit;
}
// Current Asset Account Recievable end
//prepaid expese start
public function AssetCurrentPrePaidExpense()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/AssetCurrentPrePaidExpense';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/AssetCurrentPrePaidExpense'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=5;
	$master_group_id=1;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['AccountHead']['date']=$date;
		$data['AccountHead']['group']=$group_id;
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
					'table'=>'groups',
					'alias'=>'Group',
					'type'=>'INNER',
					'conditions'=>array('Group.id=SubGroup.group_id')
					),
				),
			'fields'=>array(
				'AccountHead.*',
				'SubGroup.*',
				'Group.name',
				),
			'conditions'=>array('group_id'=>$group_id)
			));
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		$this->set('SubGroup_list',$SubGroup_list);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
// $data=$this->request->data;
// $datasource_AccountHead = $this->AccountHead->getDataSource();
// try {
// 	$datasource_AccountHead->begin();
// 	$data=$this->request->data['AccountHead'];
// 	$opening_balance=$data['opening_balance'];
// 	$name=trim($data['name']);
// 	$date=$data['date'];
// 	$sub_group_id=$data['category'];
// 	$description=$data['description'];
// 	$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
// 	if($function_return['result']!='Success')
// 		throw new Exception($function_return['message']);
// 	$return['result']='Success';
// 	$datasource_AccountHead->commit();
// 	$this->Session->setFlash(__($return['result']));
// } catch (Exception $e) {
// 	$datasource_AccountHead->rollback();
// 	$return['result']='Error';
// 	$return['message']=$e->getMessage();
// 	$this->Session->setFlash(__($return['message']));
// }
// $this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function AssetCurrentPrePaidExpenseTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/AssetCurrentPrePaidExpenseTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=5;
	$master_group_id=1;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['Journal']['date']=$date;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$this->request->data=$data;
		$All_Accounts_table_list=$this->AccountHead_Table_ListByGroupId($group_id);
		$All_Accounts=[];
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
		$account_all=[];
		foreach ($All_Accounts_table_list as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['SubGroup']=$valueA['SubGroup']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['credit']=$valueA['AccountHead']['opening_balance'];
			$account_single['debit']=$valueA['AccountHead']['opening_balance'];
//paid
			$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
				'credit'=>$modes,
				'debit'=>$valueA['AccountHead']['id'],
				'flag=1'
				)));
			foreach ($Journal_credit as $key => $value) {
				$account_single['credit']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
//total
			$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
// 'credit'=>$modes,
				'debit'=>$valueA['AccountHead']['id'],
				'flag=1'
				)));
			foreach ($Journal_debit as $key => $value) {
				$account_single['debit']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
			$account_all[$account_single['id']]=$account_single;
		}
		if(!empty($account_all)){
			krsort($account_all);
		}
		$this->set('All_Accounts',$account_all);
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		if($SubGroup_list)
		{
			$sub_group_id=array_keys($SubGroup_list)[0];
			$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		}
		else
		{
			$AccountHead_list=[];
		}
		$this->set('AccountHead',$AccountHead_list);
		$this->set('SubGroup_list',$SubGroup_list);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['mode'];
			$debit=$data['account_head'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no='';
			$external_voucher=$data['external_voucher'];
			$work_flow='Pre Paid Expense';
			$AccountHead_expense=$this->AccountHead->findById($data['account_head']);
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
//prepaid expese end
//Current Accrued start
public function AssetCurrentAccrued()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/AssetCurrentAccrued';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/AssetCurrentAccrued'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=6;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['AccountHead']['date']=$date;
		$data['AccountHead']['group']=$group_id;
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
					'table'=>'groups',
					'alias'=>'Group',
					'type'=>'INNER',
					'conditions'=>array('Group.id=SubGroup.group_id')
					),
				),
			'fields'=>array(
				'AccountHead.*',
				'SubGroup.*',
				'Group.name',
				),
			'conditions'=>array('group_id'=>$group_id)
			));
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		$this->set('SubGroup_list',$SubGroup_list);
		$this->set('AccountHead',$AccountHead);
	}
}
public function AssetCurrentAccruedTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/AssetCurrentAccruedTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=6;
	$master_group_id=1;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['Journal']['date']=$date;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$data['Journal']['group']=$group_id;
		$this->request->data=$data;
		$All_Accounts_table_list=$this->AccountHead_Table_ListByGroupId($group_id);
		$All_Accounts=[];
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
		$account_all=[];
		foreach ($All_Accounts_table_list as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['SubGroup']=$valueA['SubGroup']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['debit']='0';
			$account_single['credit']=$valueA['AccountHead']['opening_balance'];
			$Outstanding_AccountHead=$this->AccountHead->findByName($account_single['name'].' OUTSTANDING');
//paid
			$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
				'credit'=>$valueA['AccountHead']['id'],
				'debit'=>$modes,
				'flag=1'
				)));
			foreach ($Journal_debit as $key => $value) {
				$account_single['debit']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
//total
			$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
				'debit'=>$valueA['AccountHead']['id'],
				'flag=1'
				)));
			foreach ($Journal_credit as $key => $value) {
				$account_single['credit']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
			$account_single['balance']=$account_single['credit']-$account_single['debit'];
			$account_all[$account_single['id']]=$account_single;
		}
		if(!empty($account_all)){
			krsort($account_all);
		}
		$this->set('All_Accounts',$account_all);
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		if($SubGroup_list)
		{
			$sub_group_id=array_keys($SubGroup_list)[0];
			$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		}
		else
		{
			$AccountHead_list=[];
		}
		$this->set('AccountHead',$AccountHead_list);
		$this->set('SubGroup_list',$SubGroup_list);	
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['account_head'];
			$debit=$data['mode'];
			$amount=$data['received'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no='';
			$external_voucher=$data['external_voucher'];
			$work_flow='Accrued Income';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$datasource_SubGroup->rollback();
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
//Current Accrued end
// Current Asset End
//fixed start
public function FixedAsset()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/FixedAsset';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/FixedAsset'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=7;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['AccountHead']['date']=$date;
		$data['AccountHead']['group']=$group_id;
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
					'table'=>'groups',
					'alias'=>'Group',
					'type'=>'INNER',
					'conditions'=>array('Group.id=SubGroup.group_id')
					),
				),
			'fields'=>array(
				'AccountHead.*',
				'SubGroup.*',
				'Group.name',
				),
			'conditions'=>array('group_id'=>$group_id)
			));
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		$this->set('SubGroup_list',$SubGroup_list);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
		$data=$this->request->data;
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_AccountHead->begin();
			$data=$this->request->data['AccountHead'];
			$opening_balance=$data['opening_balance'];
			$name=trim($data['name']);
			$date=$data['date'];
			$sub_group_id=$data['category'];
			$description=$data['description'];
			$SubGroup=$this->SubGroup->findById($sub_group_id);
			$SubGroup_name=$SubGroup['SubGroup']['name'];
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function FixedAssetTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/FixedAssetTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=7;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['Journal']['date']=$date;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$data['Journal']['group']=$group_id;
		$this->request->data=$data;
		$All_Accounts_table_list=$this->AccountHead_Table_ListByGroupId($group_id);
		$All_Accounts=[];
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
		$account_all=[];
		foreach ($All_Accounts_table_list as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['SubGroup']=$valueA['SubGroup']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['credit']='0';
			$account_single['amount']=$valueA['AccountHead']['opening_balance'];
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id']);
			$account_single['amount']+=$Debit_N_Credit_function['debit']-$Debit_N_Credit_function['credit'];
			$account_all[$account_single['id']]=$account_single;
		}
		if(!empty($account_all)){
			krsort($account_all);
		}
		$this->set('All_Accounts',$account_all);
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		if($SubGroup_list)
		{
			$sub_group_id=array_keys($SubGroup_list)[0];
			$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		}
		else
		{
			$AccountHead_list=[];
		}	
		$this->set('AccountHead',$AccountHead_list);
		$this->set('SubGroup_list',$SubGroup_list);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['mode'];
			$debit=$data['account_head'];
			$amount=$data['amount'];
			$external_voucher=$data['external_voucher'];
			$voucher_no=$data['voucher_no'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$work_flow='Long Term Asset';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
//fixed end
//InvestmentLongTermAsset start
public function InvestmentLongTermAsset()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/InvestmentLongTermAsset';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/InvestmentLongTermAsset'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=8;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['AccountHead']['date']=$date;
		$data['AccountHead']['group']=$group_id;
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
					'table'=>'groups',
					'alias'=>'Group',
					'type'=>'INNER',
					'conditions'=>array('Group.id=SubGroup.group_id')
					),
				),
			'fields'=>array(
				'AccountHead.*',
				'SubGroup.*',
				'Group.name',
				),
			'conditions'=>array('group_id'=>$group_id)
			));
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		$this->set('SubGroup_list',$SubGroup_list);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
		$data=$this->request->data;
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_AccountHead->begin();
			$data=$this->request->data['AccountHead'];
			$opening_balance=$data['opening_balance'];
			$name=trim($data['name']);
			$date=$data['date'];
			$sub_group_id=$data['category'];
			$description=$data['description'];
			$SubGroup=$this->SubGroup->findById($sub_group_id);
			$SubGroup_name=$SubGroup['SubGroup']['name'];
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function InvestmentLongTermAssetTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/InvestmentLongTermAssetTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=8;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['Journal']['date']=$date;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$data['Journal']['group']=$group_id;
		$this->request->data=$data;
		$All_Accounts_table_list=$this->AccountHead_Table_ListByGroupId($group_id);
		$All_Accounts=[];
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
		$account_all=[];
		foreach ($All_Accounts_table_list as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['SubGroup']=$valueA['SubGroup']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['credit']='0';
			$account_single['debit']=$valueA['AccountHead']['opening_balance'];
			$Debit_function_return=$this->General_Journal_Debit_function($valueA['AccountHead']['id']);
			$account_single['debit']+=$Debit_function_return['debit'];
			$account_all[$account_single['id']]=$account_single;
		}
		if(!empty($account_all)){
			krsort($account_all);
		}
		$this->set('All_Accounts',$account_all);
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		if($SubGroup_list)
		{
			$sub_group_id=array_keys($SubGroup_list)[0];
			$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		}
		else
		{
			$AccountHead_list=[];
		}	
		$this->set('AccountHead',$AccountHead_list);
		$this->set('SubGroup_list',$SubGroup_list);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['mode'];
			$debit=$data['account_head'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$work_flow='Long Term Asset';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
//InvestmentLongTermAsset end
//InvestmentShortTermAsset start
public function InvestmentShortTermAsset()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/InvestmentShortTermAsset';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/InvestmentShortTermAsset'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=9;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['AccountHead']['date']=$date;
		$data['AccountHead']['group']=$group_id;
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
					'table'=>'groups',
					'alias'=>'Group',
					'type'=>'INNER',
					'conditions'=>array('Group.id=SubGroup.group_id')
					),
				),
			'fields'=>array(
				'AccountHead.*',
				'SubGroup.*',
				'Group.name',
				),
			'conditions'=>array('group_id'=>$group_id)
			));
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		$this->set('SubGroup_list',$SubGroup_list);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
		$data=$this->request->data;
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_AccountHead->begin();
			$data=$this->request->data['AccountHead'];
			$opening_balance=$data['opening_balance'];
			$name=trim($data['name']);
			$date=$data['date'];
			$sub_group_id=$data['category'];
			$description=$data['description'];
			$SubGroup=$this->SubGroup->findById($sub_group_id);
			$SubGroup_name=$SubGroup['SubGroup']['name'];
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function InvestmentShortTermAssetTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/InvestmentShortTermAssetTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=9;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['Journal']['date']=$date;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$data['Journal']['group']=$group_id;
		$this->request->data=$data;
		$All_Accounts_table_list=$this->AccountHead_Table_ListByGroupId($group_id);
		$All_Accounts=[];
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
		$account_all=[];
		foreach ($All_Accounts_table_list as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['SubGroup']=$valueA['SubGroup']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['credit']='0';
			$account_single['debit']=$valueA['AccountHead']['opening_balance'];
			$Debit_function_return=$this->General_Journal_Debit_function($valueA['AccountHead']['id']);
			$account_single['debit']+=$Debit_function_return['debit'];
			$account_all[$account_single['id']]=$account_single;
		}
		if(!empty($account_all)){
			krsort($account_all);
		}
		$this->set('All_Accounts',$account_all);
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		if($SubGroup_list)
		{
			$sub_group_id=array_keys($SubGroup_list)[0];
			$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		}
		else
		{
			$AccountHead_list=[];
		}	
		$this->set('AccountHead',$AccountHead_list);
		$this->set('SubGroup_list',$SubGroup_list);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['mode'];
			$debit=$data['account_head'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no='';
			$external_voucher=$data['external_voucher'];
			$work_flow='Long Term Asset';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
//InvestmentShortTermAsset end
// Asset End
// liability Start
// LongTermLiability Start
public function LongTermLiability()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/LongTermLiability';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/LongTermLiability'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=13;
	$user_id=1;
	if(!$this->request->data)
	{
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all');
		$AccountHead=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
		$data=$this->request->data;
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_AccountHead->begin();
			$data=$this->request->data['AccountHead'];
			$opening_balance=$data['opening_balance'];
			$name=$data['name'];
			$date=$data['date'];
			$description=$data['description'];
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect(array('controller' => 'Accountings','action' =>'LongTermLiability'));
	}
}
public function LongTermLiabilityTransactions()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/LongTermLiabilityTransactions'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$user_id=1;
	$sub_group_id=13;
	if(!$this->request->data)
	{
		$data['Journal']['date']=date('d-m-Y');
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$this->request->data=$data;
		$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead_list);
		$AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
		$account_all=[];
		foreach ($AccountHeads as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['Received']=$valueA['AccountHead']['opening_balance'];
			$account_single['Paid']='0';
//recieved
			$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
				'debit'=>$modes,
				'credit'=>$valueA['AccountHead']['id'],
				'flag=1'
				)));
			foreach ($Journal_credit as $key => $value) {
				$account_single['Received']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
//paid
			$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
				'credit'=>$modes,
				'debit'=>$valueA['AccountHead']['id'],
				'flag=1'
				)));
			foreach ($Journal_debit as $key => $value) {
				$account_single['Paid']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$data=$this->request->data['Journal'];
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$remarks=$data['remarks'];
			$type=$data['type'];
			if($type==1)
			{
				$credit=$data['mode'];
				$debit=$data['account_head'];	
			}
			else
			{
				$credit=$data['account_head'];
				$debit=$data['mode'];
			}
			$amount=$data['amount'];
			$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$date=$data['date'];
			$work_flow='LongTermLiability';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$datasource_Journal->commit();
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$datasource_Journal->rollback();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function liability_journal_transaction_ajax()
{
	try {
		$data=$this->request->data;
		$name=$data['name'];
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$AccountHead=$this->AccountHead->findByName(trim($name));
		if(empty($AccountHead))
			throw new Exception("Empty AccountHead", 1);
		$Journal_Recieved=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.credit'=>$AccountHead['AccountHead']['id'],
				'Journal.flag=1',
				'Journal.date between ? and ?'=>array($from_date,$to_date),
				)
			));
		$Journal_all=[];
		foreach ($Journal_Recieved as $key => $value) {
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['mode']=$value['AccountHeadDebit']['name'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['Recieved']=$value['Journal']['amount'];
			$Journal_single['Paid']=0.00;
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		$Journal_Paid=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.debit'=>$AccountHead['AccountHead']['id'],
				'Journal.flag=1',
				'Journal.date between ? and ?'=>array($from_date,$to_date),
				)
			));
		foreach ($Journal_Paid as $key => $value) {
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['mode']=$value['AccountHeadCredit']['name'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['Recieved']=0.00;
			$Journal_single['Paid']=$value['Journal']['amount'];
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		krsort($Journal_all);
		$return['row']['tbody']='';
		$return['row']['tfoot']='';
		$Balance_Total=0;
		$Recieved_Total=0;
		$Paid_Total=0;
		foreach ($Journal_all as $key => $value) {
			$return['row']['tbody'].='<tr class="blue-pddng">';
			$return['row']['tbody'].='<td class="color_label"><span style="display:none" class="journal_id">'.$value['id'].'</span><span>'.date('d-m-Y',strtotime($value['date'])).'</span></td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['Recieved'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['Paid'].'</td>';
			$Balance=$value['Recieved']-$value['Paid'];
			$Balance_Total+=$Balance;
			$Recieved_Total+=$value['Recieved'];
			$Paid_Total+=$value['Paid'];
			$return['row']['tbody'].='<td class="color_label">'.$Balance.'</td>';
			$return['row']['tbody'].='<td><i class="fa fa-pencil-square-o blue-col edit"></i></td>';
			$return['row']['tbody'].='<td><i class="fa fa-trash blue-col delete"></i></td>';
			$return['row']['tbody'].='</tr>';
		}
		$return['row']['tfoot']='';
		$return['row']['tfoot'].='<tr class="blue-pddng">';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td class="total_amount">Total</td>';
		$return['row']['tfoot'].='<td class="total_amount">'.$Recieved_Total.'</td>';
		$return['row']['tfoot'].='<td class="total_amount">'.$Paid_Total.'</td>';
		$return['row']['tfoot'].='<td class="total_amount">'.$Balance_Total.'</td>';
		$return['row']['tfoot'].='</tr>';
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function liability_balance_get_ajax()
{
	try {
		$data=$this->request->data;
		$account_head_id=$data['account_head_id'];
		$amount=$data['amount'];
		$type=$data['type'];
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$Received=$AccountHead['AccountHead']['opening_balance'];
		$Paid=0;
		if(empty($AccountHead))
			throw new Exception("Empty AccountHead", 1);
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
//recieved
		$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
			'debit'=>$modes,
			'credit'=>$AccountHead['AccountHead']['id'],
			'flag=1'
			)));
		foreach ($Journal_credit as $key => $value) {
			$Received+=$value['Journal']['amount'];
		}
//paid
		$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
			'credit'=>$modes,
			'debit'=>$AccountHead['AccountHead']['id'],
			'flag=1'
			)));
		foreach ($Journal_debit as $key => $value) {
			$Paid+=$value['Journal']['amount'];
		}
		$balance=$Received-$Paid;
		if($type==1)
		{
			$balance-=$amount;
		}
		else
		{
			$balance+=$amount;	
		}
		$return['balance']=$balance;
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
// LongTermLiability End
// MediumtermLiability Start
public function MediumTermLiability()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/MediumTermLiability';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/MediumTermLiability'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=14;
	$user_id=1;
	if(!$this->request->data)
	{
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all');
		$AccountHead=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
		$data=$this->request->data;
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_AccountHead->begin();
			$data=$this->request->data['AccountHead'];
			$opening_balance=$data['opening_balance'];
			$name=$data['name'];
			$date=$data['date'];
			$description=$data['description'];
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function MediumTermLiabilityTransactions()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/MediumTermLiabilityTransactions'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$user_id=1;
	$sub_group_id=14;
	if(!$this->request->data)
	{
		$data['Journal']['date']=date('d-m-Y');
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$this->request->data=$data;
		$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead_list);
		$AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
		$account_all=[];
		foreach ($AccountHeads as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['Received']=$valueA['AccountHead']['opening_balance'];
			$account_single['Paid']='0';
//recieved
			$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
				'debit'=>$modes,
				'credit'=>$valueA['AccountHead']['id'],
				'flag=1'
				)));
			foreach ($Journal_credit as $key => $value) {
				$account_single['Received']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
//paid
			$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
				'credit'=>$modes,
				'debit'=>$valueA['AccountHead']['id'],
				'flag=1'
				)));
			foreach ($Journal_debit as $key => $value) {
				$account_single['Paid']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$data=$this->request->data['Journal'];
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$remarks=$data['remarks'];
			$type=$data['type'];
			if($type==1)
			{
				$credit=$data['mode'];
				$debit=$data['account_head'];	
			}
			else
			{
				$credit=$data['account_head'];
				$debit=$data['mode'];
			}
			$amount=$data['amount'];
			$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$date=$data['date'];
			$work_flow='MediumTermLiability';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$datasource_Journal->commit();
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$datasource_Journal->rollback();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
// MediumTermLiability end
// CurrentLiabilityShortTermLoan start
public function CurrentLiabilityShortTermLoan()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/CurrentLiabilityShortTermLoan';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/CurrentLiabilityShortTermLoan'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=18;
	$user_id=1;
	if(!$this->request->data)
	{
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all');
		$AccountHead=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
		$data=$this->request->data;
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_AccountHead->begin();
			$data=$this->request->data['AccountHead'];
			$opening_balance=$data['opening_balance'];
			$name=$data['name'];
			$date=$data['date'];
			$description=$data['description'];
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function CurrentLiabilityShortTermLoanTransactions16_5_2020()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/CurrentLiabilityShortTermLoanTransactions'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$user_id=1;
	$sub_group_id=18;
	if(!$this->request->data)
	{
		$data['Journal']['date']=date('d-m-Y');
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$this->request->data=$data;
		$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead_list);
		$AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
		$account_all=[];
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		foreach ($AccountHeads as $keyA => $valueA) {
			$type=$this->get_type_by_account_dead($valueA['AccountHead']['id']);
		    $Type_name=$type['Type']['name'];
		    $category_name=$category[$type['Type']['name']];
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['Received']=0;
			$account_single['Paid']='0';
//recieved
// 			$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
// 				'debit'=>$modes,
// 				'credit'=>$valueA['AccountHead']['id'],
// 				'flag=1'
// 				)));
// 			foreach ($Journal_credit as $key => $value) {
// 				$account_single['Received']+=$value['Journal']['amount'];
// 				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
// 					$account_single['date']=$value['Journal']['date'];
// 			}
// //paid
// 			$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
// 				'credit'=>$modes,
// 				'debit'=>$valueA['AccountHead']['id'],
// 				'flag=1'
// 				)));
// 			foreach ($Journal_debit as $key => $value) {
// 				$account_single['Paid']+=$value['Journal']['amount'];
// 				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
// 					$account_single['date']=$value['Journal']['date'];
// 			}
		if($category_name=='credit_account')
		{
			$credit=$valueA['AccountHead']['opening_balance'];
			$debit='0';
			if($valueA['AccountHead']['opening_balance']<0)
			{
				$credit=0;
				$debit=$valueA['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$valueA['AccountHead']['opening_balance'];
			$credit='0';
			if($valueA['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$valueA['AccountHead']['opening_balance']*-1;
			}
		}
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id']);
			if($category_name=='credit_account') { 
				$account_single['Received']+=$Debit_N_Credit_function['credit']+$credit;
			$account_single['Paid']+=$Debit_N_Credit_function['debit']+$debit;
			if(strtotime($account_single['date'])<strtotime($Debit_N_Credit_function['date']))
                 $account_single['date']=$Debit_N_Credit_function['date'];
			 }
			   else {
			   	$account_single['Received']+=$Debit_N_Credit_function['debit']+$debit;
			$account_single['Paid']+=$Debit_N_Credit_function['credit']+$credit;
			if(strtotime($account_single['date'])<strtotime($Debit_N_Credit_function['date']));
             $account_single['date']=$Debit_N_Credit_function['date'];
				 }
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$data=$this->request->data['Journal'];
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$remarks=$data['remarks'];
			$type=$data['type'];
			if($type==1)
			{
				$credit=$data['mode'];
				$debit=$data['account_head'];	
			}
			else
			{
				$credit=$data['account_head'];
				$debit=$data['mode'];
			}
			$amount=$data['amount'];
			$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$date=$data['date'];
			$work_flow='ShortTermLoan';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$datasource_Journal->commit();
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$datasource_Journal->rollback();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
// CurrentLiabilityShortTermLoan end
// CurrentLiabilityCrediterSupplier start
public function CurrentLiabilityCrediterSupplier()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/CurrentLiabilityCrediterSupplier';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/CurrentLiabilityCrediterSupplier'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=15;
	$user_id=1;
	$State_list=$this->State->find('list',array('fields'=>array('id','name')));
	$this->set(compact('State_list'));
	if(!$this->request->data)
	{
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
		$data['Party']['opening_balance']='0';
		$this->request->data=$data;
		$Party=$this->Party->find('all',array(
			'oder'=>array('AccountHead.name ASC'),
			));
		$this->set('Party',$Party);
	}
	else
	{
		$datasource_Party = $this->Party->getDataSource();
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_Party->begin();
			$datasource_AccountHead->begin();
			$AccountHead_data=$this->request->data['AccountHead'];
			$Party_data=$this->request->data['Party'];
			$opening_balance=$AccountHead_data['opening_balance'];
			$name=$AccountHead_data['name'];
			$date=$AccountHead_data['date'];
			$place=$Party_data['place'];
			$PartyRow = $this->Party->find('first', array(
				'order' => array('Party.id' => 'DESC') ));
			if(!empty($PartyRow))
			{
				$PrevCodeString = $PartyRow['Party']['code'];
				$PrevCode = str_replace('VR', '', $PrevCodeString);
				$Ncode = $PrevCode + 1;
				$code = 'VR'.$Ncode;
			}
			else{
				$code = 'VR1000';
			}
// $code=$Party_data['code'];
			$email=$Party_data['email'];
			$mobile=$Party_data['mobile'];
			$vat_no=$Party_data['vat_no'];
// $gstin=$Party_data['gstin'];
           $state_id=$Party_data['state_id'];
			$description=$Party_data['description'];
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$AccountHead_id=$this->AccountHead->getLastInsertId();
			$Party_date=[
			'account_head_id'=>$AccountHead_id,
           'state_id'=>$state_id,
			'place'=>$place,
			'code'=>$code,
			'email'=>$email,
			'mobile'=>$mobile,
// 'gstin'=>$gstin,
			'vat_no'=>$vat_no,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
			'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
			];
			$this->Party->create();
			if(!$this->Party->save($Party_date))
				throw new Exception("Error Party Creation", 1);
			$return['result']='Success';
			$datasource_Party->commit();
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Party->rollback();
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function Party_get_ajax($id)
{
	$return['result']='Error';
	$Party=$this->Party->find('first',array(
		'conditions'=>array('account_head_id'=>$id),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			'AccountHead.description',
			'Party.id',
			'Party.place',
			'Party.code',
			'Party.state_id',
			'Party.email',
			'Party.mobile',
			'Party.gstin',
			'Party.vat_no',
			)
		));
	if(!empty($Party))
	{
		$return['data']=$Party;
		$return['result']='Success';	
	}
	else
	{
		$return['message']='Empty';
	}
	echo json_encode($return);
	exit;
}
public function Party_account_edit()
{
	$user_id=1;
	$datasource_Party = $this->Party->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_Party->begin();
		$datasource_AccountHead->begin();
		$AccountHead_data=$this->request->data['AccountHead'];
		$Party_data=$this->request->data['Party'];
		$AccountHead_id=$AccountHead_data['id'];
		$name=$AccountHead_data['name'];
		$opening_balance=$AccountHead_data['opening_balance'];
		$date=$AccountHead_data['date'];
		$description=$Party_data['description'];
		$Party_id=$Party_data['id'];
		$place=$Party_data['place'];
		$code=$Party_data['code'];
		$email=$Party_data['email'];
		$mobile=$Party_data['mobile'];
		$vat_no=$Party_data['vat_no'];
// $gstin=$Party_data['gstin'];
       $state_id=$Party_data['state_id'];
		$function_return=$this->AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description);
		if($function_return['result']!='Success')
			throw new Exception($function_return['message']);
		$Party_data=[
        'state_id'=>$state_id,
		'place'=>$place,
		'code'=>$code,
		'email'=>$email,
		'mobile'=>$mobile,
		'vat_no'=>$vat_no,
		'modified_by'=>$user_id,
		'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
		];
		$this->Party->id=$Party_id;
		if(!$this->Party->save($Party_data))
			throw new Exception("Error Party Updation", 1);
		$datasource_Party->commit();
		$datasource_AccountHead->commit();
		$return['result']='Success';
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$datasource_Party->rollback();
		$datasource_AccountHead->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$this->Session->setFlash(__($return['message']));
	}
	$this->redirect( Router::url( $this->referer(), true ) );
}
public function Party_delete($id)
{
	$datasource_Party = $this->Party->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_Party->begin();
		$datasource_AccountHead->begin();
		$Party=$this->Party->findByAccountHeadId($id);
		if(empty($Party))
			throw new Exception("Empty Party", 1);
		$Party_id=$Party['Party']['id'];
		$AccountHead_id=$Party['AccountHead']['id'];
		$JournalCredit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.credit'=>$AccountHead_id,'flag=1'),
			));
		if(!empty($JournalCredit))
			throw new Exception("This Account have Journal Entry Cant Delete", 1);
		$JournalDebit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.debit'=>$AccountHead_id,'flag=1'),
			));
		if(!empty($JournalDebit))
			throw new Exception("This Account have Journal Entry Cant Delete", 1);
		if(!$this->AccountHead->delete($AccountHead_id))
			throw new Exception("Error Processing AccountHead Deletion", 1);
		if(!$this->Party->delete($Party_id))
			throw new Exception("Error Processing Party Deletion", 1);
		$datasource_Party->commit();
		$datasource_AccountHead->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$datasource_Party->rollback();
		$datasource_AccountHead->rollback();
	}
	echo json_encode($return);
	exit;
}
public function CurrentLiabilityCrediterSupplierTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/CurrentLiabilityCrediterSupplierTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=15;
	$user_id=1;
	if(!$this->request->data)
	{
		$data['Journal']['date']=date('d-m-Y');
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y',strtotime('+6 month'));
		$this->request->data=$data;
		$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead_list);
		$AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$account_all=[];
		foreach ($AccountHeads as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['total']=$valueA['AccountHead']['opening_balance'];
			$account_single['Paid']='0';
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id']);
			$account_single['total']+=$Debit_N_Credit_function['credit'];
			$account_single['Paid']+=$Debit_N_Credit_function['debit'];
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['mode'];
			$debit=$data['account_head'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no="";
			//$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$work_flow='Crediters/Supliers';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
// CurrentLiabilityCrediterSupplier end
// CurrentLiabilityVendor start
public function CurrentLiabilityVendor()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/CurrentLiabilityVendor';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/CurrentLiabilityVendor'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=16;
	$user_id=1;
	$State_list=$this->State->find('list',array('fields'=>array('id','name')));
	$this->set(compact('State_list'));
	if(!$this->request->data)
	{
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
		$data['Vendor']['opening_balance']='0';
		$this->request->data=$data;
		$Vendor=$this->Vendor->find('all',array(
			'oder'=>array('AccountHead.name ASC'),
			));
		$this->set('Vendor',$Vendor);
	}
	else
	{
		$datasource_Vendor = $this->Vendor->getDataSource();
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_Vendor->begin();
			$datasource_AccountHead->begin();
			$AccountHead_data=$this->request->data['AccountHead'];
			$Vendor_data=$this->request->data['Vendor'];
			$opening_balance=$AccountHead_data['opening_balance'];
			$name=$AccountHead_data['name'];
			$date=$AccountHead_data['date'];
			$place=$Vendor_data['place'];
			$code=$Vendor_data['code'];
			$email=$Vendor_data['email'];
			$mobile=$Vendor_data['mobile'];
//			$gstin=$Vendor_data['gstin'];
//			$state_id=$Vendor_data['state_id'];
			$description=$Vendor_data['description'];
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$AccountHead_id=$this->AccountHead->getLastInsertId();
			$Vendor_data=[
			'account_head_id'=>$AccountHead_id,
			'place'=>$place,
			'code'=>$code,
			'email'=>$email,
			'mobile'=>$mobile,
//			'gstin'=>$gstin,
//			'state_id'=>$state_id,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
			'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
			];
			$this->Vendor->create();
			if(!$this->Vendor->save($Vendor_data))
				throw new Exception("Error Vendor Creation", 1);
			$return['result']='Success';
			$datasource_Vendor->commit();
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Vendor->rollback();
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function Vendor_get_ajax($id)
{
	$return['result']='Error';
	$Vendor=$this->Vendor->find('first',array(
		'conditions'=>array('account_head_id'=>$id),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			'AccountHead.description',
			'Vendor.id',
			'Vendor.place',
			'Vendor.state_id',
			'Vendor.code',
			'Vendor.email',
			'Vendor.mobile',
			'Vendor.gstin',
			)
		));
	if(!empty($Vendor))
	{
		$return['data']=$Vendor;
		$return['result']='Success';	
	}
	else
	{
		$return['message']='Empty';
	}
	echo json_encode($return);
	exit;
}
public function Vendor_account_edit()
{
	$user_id=1;
	$datasource_Vendor = $this->Vendor->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_Vendor->begin();
		$datasource_AccountHead->begin();
		$AccountHead_data=$this->request->data['AccountHead'];
		$Vendor_data=$this->request->data['Vendor'];
		$AccountHead_id=$AccountHead_data['id'];
		$name=$AccountHead_data['name'];
		$opening_balance=$AccountHead_data['opening_balance'];
		$date=$AccountHead_data['date'];
		$description=$Vendor_data['description'];
		$Vendor_id=$Vendor_data['id'];
		$place=$Vendor_data['place'];
		$code=$Vendor_data['code'];
		$email=$Vendor_data['email'];
		$mobile=$Vendor_data['mobile'];
		$gstin=$Vendor_data['gstin'];
		$state_id=$Vendor_data['state_id'];
		$function_return=$this->AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description);
		if($function_return['result']!='Success')
			throw new Exception($function_return['message']);
		$Vendor_data=[
		'place'=>$place,
		'code'=>$code,
		'email'=>$email,
		'mobile'=>$mobile,
		'gstin'=>$gstin,
		'state_id'=>$state_id,
		'modified_by'=>$user_id,
		'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
		];
		$this->Vendor->id=$Vendor_id;
		if(!$this->Vendor->save($Vendor_data))
			throw new Exception("Error Vendor Updation", 1);
		$datasource_Vendor->commit();
		$datasource_AccountHead->commit();
		$return['result']='Success';
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$datasource_Vendor->rollback();
		$datasource_AccountHead->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$this->Session->setFlash(__($return['message']));
	}
	$this->redirect( Router::url( $this->referer(), true ) );
}
public function Vendor_delete($id)
{
	$datasource_Vendor = $this->Vendor->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_Vendor->begin();
		$datasource_AccountHead->begin();
		$Vendor=$this->Vendor->findByAccountHeadId($id);
		if(empty($Vendor))
			throw new Exception("Empty Vendor", 1);
		$Vendor_id=$Vendor['Vendor']['id'];
		$AccountHead_id=$Vendor['AccountHead']['id'];
		$JournalCredit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.credit'=>$AccountHead_id,'flag=1'),
			));
		if(!empty($JournalCredit))
			throw new Exception("This Account have Journal Entry Cant Delete", 1);
		$JournalDebit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.debit'=>$AccountHead_id,'flag=1'),
			));
		if(!empty($JournalDebit))
			throw new Exception("This Account have Journal Entry Cant Delete", 1);
		if(!$this->AccountHead->delete($AccountHead_id))
			throw new Exception("Error Processing AccountHead Deletion", 1);
		if(!$this->Vendor->delete($Vendor_id))
			throw new Exception("Error Processing Vendor Deletion", 1);
		$datasource_Vendor->commit();
		$datasource_AccountHead->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$datasource_Vendor->rollback();
		$datasource_AccountHead->rollback();
	}
	echo json_encode($return);
	exit;
}
public function CurrentLiabilityVendorTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/CurrentLiabilityVendorTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=16;
	$user_id=1;
	if(!$this->request->data)
	{
		$data['Journal']['date']=date('d-m-Y');
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$this->request->data=$data;
		$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead_list);
		$AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$account_all=[];
		foreach ($AccountHeads as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['total']=$valueA['AccountHead']['opening_balance'];
			$account_single['Paid']='0';
			$Debit_N_Credit_function_return=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id']);
			$account_single['total']+=$Debit_N_Credit_function_return['credit']-$Debit_N_Credit_function_return['debit'];
			$account_single['Paid']+=$Debit_N_Credit_function_return['debit'];
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['mode'];
			$debit=$data['account_head'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$work_flow='Vendor';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
// CurrentLiabilityVendor end
// CurrentLiabilityProvision start
public function CurrentLiabilityProvision()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/CurrentLiabilityProvision';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/CurrentLiabilityProvision'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=19;
	$user_id=1;
	if(!$this->request->data)
	{
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all');
		$AccountHead=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
		$data=$this->request->data;
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_AccountHead->begin();
			$data=$this->request->data['AccountHead'];
			$opening_balance=$data['opening_balance'];
			$name=$data['name'];
			$date=$data['date'];
			$description=$data['description'];
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
// need to update this function
public function CurrentLiabilityProvisionTransactions()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/CurrentLiabilityProvisionTransactions'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$user_id=1;
	$profit_n_loss=2;
	$sub_group_id=19;
	if(!$this->request->data)
	{
		$data['Journal']['date']=date('d-m-Y');
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$this->request->data=$data;
		$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead_list);
		$AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$account_all=[];
		foreach ($AccountHeads as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['amount']=$valueA['AccountHead']['opening_balance'];
			$Credit_function_return=$this->General_Journal_Credit_function($valueA['AccountHead']['id']);
			$account_single['amount']+=$Credit_function_return['credit'];
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$data=$this->request->data['Journal'];
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$remarks=$data['remarks'];
			$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$credit=$data['account_head'];
			$debit=$profit_n_loss;
			$amount=$data['amount'];
			$date=$data['date'];
			$work_flow='Provision For Taxaction';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$datasource_Journal->commit();
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$datasource_Journal->rollback();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
// CurrentLiabilityProvision end
// CurrentLiabilityOutstandingExpense start
public function CurrentLiabilityOutstandingExpense()
{
	$group_id=20;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['AccountHead']['date']=$date;
		$data['AccountHead']['group']=$group_id;
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
					'table'=>'groups',
					'alias'=>'Group',
					'type'=>'INNER',
					'conditions'=>array('Group.id=SubGroup.group_id')
					),
				),
			'fields'=>array(
				'AccountHead.*',
				'SubGroup.*',
				'Group.name',
				),
			'conditions'=>array('group_id'=>$group_id)
			));
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		$this->set('SubGroup_list',$SubGroup_list);
		$this->set('AccountHead',$AccountHead);
	}
}
public function CurrentLiabilityOutstandingExpenseTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/CurrentLiabilityOutstandingExpenseTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=20;
	$master_group_id=9;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['Journal']['date']=$date;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$data['Journal']['group']=$group_id;
		$this->request->data=$data;
		$All_Accounts_table_list=$this->AccountHead_Table_ListByGroupId($group_id);
		$All_Accounts=[];
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
		$account_all=[];
		foreach ($All_Accounts_table_list as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['SubGroup']=$valueA['SubGroup']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['debit']='0';
			$account_single['credit']=$valueA['AccountHead']['opening_balance'];
			$Outstanding_AccountHead=$this->AccountHead->findByName($account_single['name'].' OUTSTANDING');
//paid
			$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
				'credit'=>$modes,
				'debit'=>$valueA['AccountHead']['id'],
				'flag=1'
				)));
			foreach ($Journal_debit as $key => $value) {
				$account_single['debit']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
//total
			$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
				'credit'=>$valueA['AccountHead']['id'],
				'flag=1'
				)));
			foreach ($Journal_credit as $key => $value) {
				$account_single['credit']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
			$account_single['balance']=$account_single['credit']-$account_single['debit'];
			$account_all[$account_single['id']]=$account_single;
		}
		if(!empty($account_all)){
			krsort($account_all);
		}
		$this->set('All_Accounts',$account_all);
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		if($SubGroup_list)
		{
			$sub_group_id=array_keys($SubGroup_list)[0];
			$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		}
		else
		{
			$AccountHead_list=[];
		}
		$this->set('AccountHead',$AccountHead_list);
		$this->set('SubGroup_list',$SubGroup_list);	
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['mode'];
			$debit=$data['account_head'];
			$amount=$data['paid'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$external_voucher=$data['external_voucher'];
			$voucher_no=$data['voucher_no'];
			$work_flow='Outstanding Expense';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$datasource_SubGroup->rollback();
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function outstanding_expense_table_list_by_group_id_ajax($group_id)
{
	$All_Accounts_table_list=$this->AccountHead_Table_ListByGroupId($group_id);
	$All_Accounts=[];
	$bank=2;
	$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
	$modes=['1'];
	foreach ($bank_list as $key => $value) {
		array_push($modes, $key);
	}
	$credit=0;
	$debit=0;
	$balance=0;
	$account_all=[];
	foreach ($All_Accounts_table_list as $keyA => $valueA) {
		$account_single['id']=$valueA['AccountHead']['id'];
		$account_single['name']=$valueA['AccountHead']['name'];
		$account_single['SubGroup']=$valueA['SubGroup']['name'];
		$account_single['date']=$valueA['AccountHead']['created_at'];
		$account_single['debit']='0';
		$account_single['credit']=$valueA['AccountHead']['opening_balance'];
		$Outstanding_AccountHead=$this->AccountHead->findByName($account_single['name'].' OUTSTANDING');
//paid
		$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
			'credit'=>$modes,
			'debit'=>$valueA['AccountHead']['id'],
			'flag=1'
			)));
		foreach ($Journal_debit as $key => $value) {
			$account_single['debit']+=$value['Journal']['amount'];
			if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
				$account_single['date']=$value['Journal']['date'];
		}
//total
		$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
			'credit'=>$valueA['AccountHead']['id'],
			'flag=1'
			)));
		foreach ($Journal_credit as $key => $value) {
			$account_single['credit']+=$value['Journal']['amount'];
			if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
				$account_single['date']=$value['Journal']['date'];
		}
		$account_single['balance']=$account_single['credit']-$account_single['debit'];
		$credit+=$account_single['credit'];
		$debit+=$account_single['debit'];
		$balance+=$account_single['balance'];
		$account_all[$account_single['id']]=$account_single;
		$account_all[$account_single['id']]=$account_single;
	}
	$account_all_foot='<tr>';
	$account_all_foot.='<td></td>';
	$account_all_foot.='<td></td>';
	$account_all_foot.='<td class="total_amount">Total</td>';
	$account_all_foot.='<td class="total_amount text-right">'.$credit.'</td>';
	$account_all_foot.='<td class="total_amount text-right">'.$debit.'</td>';
	$account_all_foot.='<td class="total_amount text-right">'.$balance.'</td>';
	$account_all_foot.='</tr>';
	if(empty($account_all))
	{
		$return['result']='empty';
	}
	else
	{
		$return['result']='success';
		if(!empty($account_all)){
			krsort($account_all);
		}
		$return['row']['body']='';
		foreach ($account_all as $key => $value) {
			$return['row']['body'].='<tr class="blue-pddng view_transaction">';
			$return['row']['body'].='<td class="padding_left">'.date('d-m-Y',strtotime($value['date'])).'</td>';
			$return['row']['body'].='<td>'.$value['SubGroup'].'</td>';
			$return['row']['body'].='<td class="name">'.$value['name'].'</td>';
			$return['row']['body'].='<td class="text-right">'.$value['credit'].'</td>';
			$return['row']['body'].='<td class="text-right">'.$value['debit'].'</td>';
			$return['row']['body'].='<td class="text-right">'.$value['balance'].'</td>';
			$return['row']['body'].='</tr>';
		}
	}
	$return['row']['foot']=$account_all_foot;
	echo json_encode($return);
	exit;
}
// CurrentLiabilityOutstandingExpense end
// Advance Income Start
public function CurrentLiabilityAdvanceIncome()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/CurrentLiabilityAdvanceIncome';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/CurrentLiabilityAdvanceIncome'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=21;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['AccountHead']['date']=$date;
		$data['AccountHead']['group']=$group_id;
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
					'table'=>'groups',
					'alias'=>'Group',
					'type'=>'INNER',
					'conditions'=>array('Group.id=SubGroup.group_id')
					),
				),
			'fields'=>array(
				'AccountHead.*',
				'SubGroup.*',
				'Group.name',
				),
			'conditions'=>array('group_id'=>$group_id)
			));
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		$this->set('SubGroup_list',$SubGroup_list);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
// $data=$this->request->data;
// $datasource_AccountHead = $this->AccountHead->getDataSource();
// try {
// 	$datasource_AccountHead->begin();
// 	$data=$this->request->data['AccountHead'];
// 	$opening_balance=$data['opening_balance'];
// 	$name=$data['name'];
// 	$date=$data['date'];
// 	$sub_group_id=$data['category'];
// 	$description=$data['description'];
// 	$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
// 	if($function_return['result']!='Success')
// 		throw new Exception($function_return['message']);
// 	$return['result']='Success';
// 	$datasource_AccountHead->commit();
// 	$this->Session->setFlash(__($return['result']));
// } catch (Exception $e) {
// 	$datasource_AccountHead->rollback();
// 	$return['result']='Error';
// 	$return['message']=$e->getMessage();
// 	$this->Session->setFlash(__($return['message']));
// }
// $this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function CurrentLiabilityAdvanceIncomeTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/CurrentLiabilityAdvanceIncomeTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id=21;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['Journal']['date']=$date;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$this->request->data=$data;
		$All_Accounts_table_list=$this->AccountHead_Table_ListByGroupId($group_id);
		$All_Accounts=[];
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
		$account_all=[];
		foreach ($All_Accounts_table_list as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['SubGroup']=$valueA['SubGroup']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['debit']='0';
			$account_single['credit']=$valueA['AccountHead']['opening_balance'];
//paid
			$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
				'credit'=>$valueA['AccountHead']['id'],
				'debit'=>$modes,
				'flag=1'
				)));
			foreach ($Journal_debit as $key => $value) {
				$account_single['debit']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
//total
			$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
				'credit'=>$valueA['AccountHead']['id'],
				'flag=1'
				)));
			foreach ($Journal_credit as $key => $value) {
				$account_single['credit']+=$value['Journal']['amount'];
				if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
					$account_single['date']=$value['Journal']['date'];
			}
			$account_single['balance']=$account_single['credit']-$account_single['debit'];
			$account_all[$account_single['id']]=$account_single;
		}
		if(!empty($account_all)){
			krsort($account_all);
		}
		$this->set('All_Accounts',$account_all);
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		if($SubGroup_list)
		{
			$sub_group_id=array_keys($SubGroup_list)[0];
			$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		}
		else
		{
			$AccountHead_list=[];
		}
		$this->set('AccountHead',$AccountHead_list);
		$this->set('SubGroup_list',$SubGroup_list);	
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['account_head'];
			$debit=$data['mode'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no='';
			$external_voucher=$data['external_voucher'];
			$work_flow='Advance Income';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$datasource_SubGroup->rollback();
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
// Advance Income End
public function get_outstanding_balance($id)
{
	$outstanding=0;
	$debit=0;
	$credit=0;
	$bank=2;
	$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
	$modes=['1'];
	foreach ($bank_list as $key => $value) {
		array_push($modes, $key);
	}
	$AccountHead=$this->AccountHead->findById($id);
	if(!empty($AccountHead))
	{
		$outstanding+=$AccountHead['AccountHead']['opening_balance'];
//total
		$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
			'debit'=>$id,
			'flag=1'
			)));
		foreach ($Journal_debit as $key => $value) {
			$debit+=$value['Journal']['amount'];
		}
//get
		$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
			'debit'=>$id,
			'credit'=>$modes,
			'flag=1'
			)));
		foreach ($Journal_credit as $key => $value) {
			$credit+=$value['Journal']['amount'];
		}
		$outstanding+=$credit-$debit;
	}
	echo json_encode($outstanding);
	exit;
}
public function get_outstanding_balance_from_expense($name)
{
	$outstanding=0;
	$name=explode(' ',$name);
	array_pop($name);
	$name=implode(' ', $name);
	$return=[
		'supplier'=>'empty','trn_number'=>'empty','tax'=>0,
		];
	$AccountHead=$this->AccountHead->findByName($name.' OUTSTANDING');
	if(!empty($AccountHead)) {
		$id=$AccountHead['AccountHead']['id'];
		if(!empty($AccountHead))
		{
			$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)",);
			$outstanding+=$AccountHead['AccountHead']['opening_balance'];
			$Journal_debit=$this->Journal->findByDebitAndFlag($id,'1',['total_amount']);
			$debit=$Journal_debit['Journal']['total_amount'];
			$Journal_credit=$this->Journal->findByCreditAndFlag($id,'1',['total_amount']);
			$credit=$Journal_credit['Journal']['total_amount'];
			$outstanding+=$credit-$debit;
		}
	}
	$prepaid=0;
	$AccountHead=$this->AccountHead->findByName($name.' PREPAID');
	if(!empty($AccountHead)) {
		$id=$AccountHead['AccountHead']['id'];
		if(!empty($AccountHead))
		{
			$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)",);
			$prepaid+=$AccountHead['AccountHead']['opening_balance'];
			$Journal_debit=$this->Journal->findByDebitAndFlag($id,'1',['total_amount']);
			$debit=$Journal_debit['Journal']['total_amount'];
			$Journal_credit=$this->Journal->findByCreditAndFlag($id,'1',['total_amount']);
			$credit=$Journal_credit['Journal']['total_amount'];
			$prepaid+=$debit-$credit;
			$outstanding-=$prepaid;
		}
		$return['supplier']=$AccountHead['AccountHead']['supplier'];
		$return['trn_number']=$AccountHead['AccountHead']['trn_number'];
		$return['tax']=$AccountHead['AccountHead']['tax'];
	}
	$return['outstanding']=$outstanding;
	echo json_encode($return);
	exit;
}
//liability reserves start
public function LiabilityReserves()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/LiabilityReserves'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$sub_group_id=20;
	$user_id=1;
	if(!$this->request->data)
	{
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all');
		$AccountHead=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
		$data=$this->request->data;
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_AccountHead->begin();
			$data=$this->request->data['AccountHead'];
			$opening_balance=$data['opening_balance'];
			$name=$data['name'];
			$date=$data['date'];
			$description=$data['description'];
			$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function LiabilityReservesTransaction()
{
	$user_id=1;
	$profit_n_loss=2;
	$sub_group_id=20;
	if(!$this->request->data)
	{
		$data['Journal']['date']=date('d-m-Y');
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$this->request->data=$data;
		$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead_list);
		$AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$account_all=[];
		foreach ($AccountHeads as $keyA => $valueA) {
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['amount']=$valueA['AccountHead']['opening_balance'];
			$Credit_function_return=$this->General_Journal_Credit_function($valueA['AccountHead']['id']);
			$account_single['amount']+=$Credit_function_return['credit'];
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$data=$this->request->data['Journal'];
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$remarks=$data['remarks'];
			$credit=$data['account_head'];
			$debit=$profit_n_loss;
			$amount=$data['amount'];
			$voucher_no=$data['voucher_no'];
			$date=$data['date'];
			$work_flow='Provision For Taxaction';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$datasource_Journal->commit();
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$datasource_Journal->rollback();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
//liability reserves end
// liability End
//Expense start
public function Expense()
{
	// $PermissionList = $this->Session->read('PermissionList');
	// $menu_id = $this->Menu->field('Menu.id', array('action ' => 'Accountings/Expense'));
	$group_id_direct=12;
	$group_id_indirect=13;
	// $prepaid=5;
	// $outstanding=20;
	// $master_group_id=5;
	// $priliminary_expense_written_off_sub_group_id=26;
	// $priliminary_expense_written_off=18;
	// $exclude_sub_group_id=['5','6','7','8','26'];
	// $user_id=1;
	// $expense_group=3;
	// $expense_main_group=3;
	$expense_sub_group=[11,12,15];
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['AccountHead']['date']=$date;
		$data['AccountHead']['group']=$group_id_indirect;
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
				'table'=>'acc_sub_groups',
				'alias'=>'AccSubGroup',
				'type'=>'INNER',
				'conditions'=>array('AccSubGroup.id=AccountHead.acc_sub_group_id')
				),
				// array(
				// 	'table'=>'groups',
				// 	'alias'=>'Group',
				// 	'type'=>'INNER',
				// 	'conditions'=>array('Group.id=SubGroup.group_id')
				// 	),
				),
			'fields'=>array(
				'AccountHead.*',
				//'SubGroup.*',
				//'Group.name',
				),
			'conditions'=>array(
				'AccountHead.acc_sub_group_id'=>$expense_sub_group,
				)
			));
		$group_check_box_list=[
		$group_id_indirect=>'Indirect',
		$group_id_direct=>'Direct',
		];
		// $SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id_indirect);
		// unset($SubGroup_list[$priliminary_expense_written_off_sub_group_id]);
		$this->set('group_list',$group_check_box_list);
		//$this->set('SubGroup_list',$SubGroup_list);
		$this->set('AccountHead',$AccountHead);
		//$this->set(compact('priliminary_expense_written_off_sub_group_id'));
	}
	else
	{
		$data=$this->request->data;
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_AccountHead->begin();
			$data=$this->request->data['AccountHead'];
			$tax=$data['tax'];
			$show_in_app=$data['show_in_app'];
			$supplier=$data['supplier'];
			$trn_number=$data['trn_number'];
			$paid_opening_balance=$data['paid_opening_balance'];
			$outstanding_opening_balance=$data['outstanding_opening_balance'];
			$prepaid_opening_balance=$data['prepaid_opening_balance'];
			$name=trim($data['name']);
			$date=$data['date'];
			$sub_group_id=$data['category'];
			if(in_array($sub_group_id, $exclude_sub_group_id))
				throw new Exception("You Cant Add Under this Group", 1);
			$description=$data['description'];
			$SubGroup=$this->SubGroup->findById($sub_group_id);
			$SubGroup_name=$SubGroup['SubGroup']['name'];
			$SubGroup_replace=str_replace('PAID', '', $SubGroup_name);
			$function_return=$this->AccountHeadCreate($sub_group_id,$name.' PAID',$paid_opening_balance,$date,$description,$tax,$trn_number,$supplier,$show_in_app);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$SubGroup_prepaid=$this->SubGroup->findByName($SubGroup_replace.'PREPAID');
			$prepaid_name=$name.' PREPAID';
			$prepaid_sub_group_id=$SubGroup_prepaid['SubGroup']['id'];
			$function_return=$this->AccountHeadCreate($prepaid_sub_group_id,$prepaid_name,$prepaid_opening_balance,$date,$description,$tax,$trn_number,$supplier,$show_in_app);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$SubGroup_outstanding=$this->SubGroup->findByName($SubGroup_replace.'OUTSTANDING');
			$outstanding_name=$name.' OUTSTANDING';
			$outstanding_subgroup_id=$SubGroup_outstanding['SubGroup']['id'];
			$function_return=$this->AccountHeadCreate($outstanding_subgroup_id,$outstanding_name,$outstanding_opening_balance,$date,$description,$tax,$trn_number,$supplier,$show_in_app);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function ExpenseTransaction()
{
	$group_id_indirect=13;
	$group_id_direct=12;
	$outstanding_group_id=20;
	$prepaid_group_id=5;
	$master_group_id=5;
	$priliminary_expense_written_off_sub_group_id=26;
	$priliminary_expense_written_off=18;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['Journal']['date']=$date;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$data['Journal']['group']=$group_id_indirect;
		$this->request->data=$data;
		$account_all=$this->expense_table_list_by_group_id($group_id_indirect);
		$this->set('All_Accounts',$account_all['account_all']);
		$group_check_box_list=[
		$group_id_indirect=>'Indirect',
		$group_id_direct=>'Direct',
		];
		$this->set('group_list',$group_check_box_list);
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id_indirect);
		if($SubGroup_list)
		{
			unset($SubGroup_list[$priliminary_expense_written_off_sub_group_id]);
			$sub_group_id=array_keys($SubGroup_list)[0];
			$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		}
		else
		{
			$AccountHead_list=[];
		}
		$routes=$this->Route->find('list',array('fields'=>array('id','name')));
		$this->set(compact('routes'));
		$this->set('AccountHead',$AccountHead_list);
		$this->set('SubGroup_list',$SubGroup_list);
		$this->set(compact('priliminary_expense_written_off_sub_group_id'));
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		$datasource_SubGroup = $this->SubGroup->getDataSource();
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		$datasource_expense = $this->ExpenseVatDetail->getDataSource();
		try {
			$datasource_Journal->begin();
			$datasource_expense->begin();
			$branch_id=$this->Session->read('User.branch_id');
			$data=$this->request->data['Journal'];
			$route_id='';
			if(!empty($data['route_id']))
			{
				$route_id=$data['route_id'];
			}
			$credit=$data['mode'];
			$debit=$data['account_head'];
			$amount=$data['paid'];
			$tax=$data['tax_amount'];
			$external_voucher=$data['external_voucher'];
			$outstanding=$data['outstanding'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no='';
			$work_flow='Expense';
			$AccountHead_expense=$this->AccountHead->findById($data['account_head']);
			$expense_account_name=explode(' ',$AccountHead_expense['AccountHead']['name']);
			array_pop($expense_account_name);
			$expense_account_name=implode(' ',$expense_account_name);
			$expense_subgroup_name=explode(' ',$AccountHead_expense['SubGroup']['name']);
			array_pop($expense_subgroup_name);
			$expense_subgroup_name=implode(' ',$expense_subgroup_name);
			$amount=$data['amount'];
			$paid=$data['paid'];
			$outstanding=0;
			$prepaid=0;
			if($amount>$paid)
			{
				$outstanding=$amount-$paid;	
			}
			else
			{
				$prepaid=$paid-$amount;
				$paid=$amount;
			}
			$outstanding_subgroup_name=$expense_subgroup_name.' OUTSTANDING';
			$SubGroup=$this->SubGroup->findByName($outstanding_subgroup_name);
			if(empty($SubGroup))
			{
				$datasource_SubGroup->begin();
				$SubGroup_return_function=$this->SubGroupCreate($outstanding_group_id,$outstanding_subgroup_name);
				if($SubGroup_return_function['result']!='Success')
					throw new Exception($SubGroup_return_function['message']);
				$SubGroup=$this->SubGroup->findByName($outstanding_subgroup_name);
				$datasource_SubGroup->commit();
			}
			$outstanding_AccountHead_name=$expense_account_name.' OUTSTANDING';
			$AccountHead_OUTSTANDING=$this->AccountHead->findByName($outstanding_AccountHead_name);
			if(empty($AccountHead_OUTSTANDING))
			{
				$datasource_AccountHead->begin();
				$outstanding_sub_group_id=$SubGroup['SubGroup']['id'];
				$opening_balance=0;
				$description='';
				$AccountHead_return_function=$this->AccountHeadCreate($outstanding_sub_group_id,$outstanding_AccountHead_name,$opening_balance,$date,$description);
				if($AccountHead_return_function['result']!='Success') throw new Exception($AccountHead_return_function['message']);
				$AccountHead_OUTSTANDING=$this->AccountHead->findByName($outstanding_AccountHead_name);
				$datasource_AccountHead->commit();
			}
			$prepaid_subgroup_name=$expense_subgroup_name.' PREPAID';
			$SubGroup=$this->SubGroup->findByName($prepaid_subgroup_name);
			if(empty($SubGroup))
			{
				$datasource_SubGroup->begin();
				$SubGroup_return_function=$this->SubGroupCreate($prepaid_group_id,$prepaid_subgroup_name);
				if($SubGroup_return_function['result']!='Success') throw new Exception($SubGroup_return_function['message']);
				$SubGroup=$this->SubGroup->findByName($prepaid_subgroup_name);
				$datasource_SubGroup->commit();
			}
			$prepaid_AccountHead_name=$expense_account_name.' PREPAID';
			$AccountHead_prepaid=$this->AccountHead->findByName($prepaid_AccountHead_name);
			if(empty($AccountHead_prepaid))
			{
				$datasource_AccountHead->begin();
				$prepaid_sub_group_id=$SubGroup['SubGroup']['id'];
				$opening_balance=0;
				$description='';
				$AccountHead_return_function=$this->AccountHeadCreate($prepaid_sub_group_id,$prepaid_AccountHead_name,$opening_balance,$date,$description);
				if($AccountHead_return_function['result']!='Success') throw new Exception($AccountHead_return_function['message']);
				$AccountHead_prepaid=$this->AccountHead->findByName($prepaid_AccountHead_name);
				$datasource_AccountHead->commit();
			}
			$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
			$Outstanding_credit=$this->Journal->find('first',array('conditions'=>array('credit'=>$AccountHead_OUTSTANDING['AccountHead']['id'],'flag'=>1),'fields'=>array('total_amount')));
			$Outstanding_debit=$this->Journal->find('first',array('conditions'=>array('debit'=>$AccountHead_OUTSTANDING['AccountHead']['id'],'flag'=>1,),'fields'=>array('total_amount')));
			$Prepaid_credit=$this->Journal->find('first',array('conditions'=>array('credit'=>$AccountHead_prepaid['AccountHead']['id'],'flag'=>1),'fields'=>array('total_amount')));
			$Prepaid_debit=$this->Journal->find('first',array('conditions'=>array('debit'=>$AccountHead_prepaid['AccountHead']['id'],'flag'=>1,),'fields'=>array('total_amount')));
			$old_outstanding=$AccountHead_OUTSTANDING['AccountHead']['opening_balance'];
			$old_outstanding+=floatval($Outstanding_credit['Journal']['total_amount']);
			$old_outstanding-=floatval($Outstanding_debit['Journal']['total_amount']);
			if($old_outstanding)
			{
				if($prepaid)
				{
					if($old_outstanding>=$prepaid)
					{
						$outstanding_cash_amount=$prepaid;
						$prepaid=0;
					}
					else
					{
						$outstanding_cash_amount=$old_outstanding;
						$prepaid-=$old_outstanding;
					}
					$outstanding_credit=$data['mode'];
					$outstanding_debit=$AccountHead_OUTSTANDING['AccountHead']['id'];
					$function_return=$this->JournalCreate($outstanding_credit,$outstanding_debit,$outstanding_cash_amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,'',$branch_id,'',$route_id);
					if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
				}
			}
			$old_prepaid=$AccountHead_prepaid['AccountHead']['opening_balance'];
			$old_prepaid-=floatval($Prepaid_credit['Journal']['total_amount']);
			$old_prepaid+=floatval($Prepaid_debit['Journal']['total_amount']);
			if($old_prepaid)
			{
				if($outstanding)
				{
					if($old_prepaid>=$outstanding)
					{
						$prepaid_expense_amount=$outstanding;
						$outstanding=0;
					}
					else
					{
						$prepaid_expense_amount=$old_prepaid;
						$outstanding-=$old_prepaid;
					}
					$outstanding_credit=$AccountHead_prepaid['AccountHead']['id'];
					$outstanding_debit=$data['account_head'];
					$function_return=$this->JournalCreate($outstanding_credit,$outstanding_debit,$prepaid_expense_amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,'',$branch_id,'',$route_id);
					if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
				}
			}
			if($paid)
			{
				$function_return=$this->JournalCreate($credit,$debit,$paid,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,'',$branch_id,'',$route_id);
				if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
			}
			if($outstanding>0)
			{
				$outstanding_credit=$AccountHead_OUTSTANDING['AccountHead']['id'];
				$outstanding_debit=$data['account_head'];
				$function_return=$this->JournalCreate($outstanding_credit,$outstanding_debit,$outstanding,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,'',$branch_id,'',$route_id);
				if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
			}
			if($prepaid)
			{
				$prepaid_cash=$prepaid;
				$prepaid_credit=$data['mode'];
				$prepaid_debit=$AccountHead_prepaid['AccountHead']['id'];
				$function_return=$this->JournalCreate($prepaid_credit,$prepaid_debit,$prepaid_cash,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,'',$branch_id,'',$route_id);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message'], 1);
			}
			if(!empty($tax))
			{
					//$tax_amount=($amount*$tax)/100;
				     
				$tax_on_sale_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DUTIES & TAXES'));
				$debit=$tax_on_sale_account_head_id;
			    $credit=$data['account_head'];
				$function_return=$this->JournalCreate($credit,$debit,$tax,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,'',$branch_id,'',$route_id);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message'], 1);
				 $tax_data=[
							'expense_id'=>$data['account_head'],
							'date'=>date('Y-m-d',strtotime($date)),
							'amount'=>$amount-$tax,
							//'vat'=>$tax,
							'vat_amount'=>$tax,
							'total'=>$amount,
							'trn_number'=>$data['t_r_n_number'],
							'supplier_name'=>$data['supplier_name'],
							'external_voucher'=>$external_voucher,

							'journal_id'=>$this->Journal->getLastInsertId(),
							];
								$this->ExpenseVatDetail->create();
								if(!$this->ExpenseVatDetail->save($tax_data)) { $errors = $this->ExpenseVatDetail->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }
			}
			$return['result']='Success';
			$datasource_Journal->commit();
			 $datasource_expense->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$datasource_SubGroup->rollback();
			$datasource_AccountHead->rollback();
			$datasource_expense->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function expense_table_list_by_group_id_ajax($group_id)
{
	$All_Accounts=$this->expense_table_list_by_group_id($group_id);

	$account_all_foot='<tr>';
	$account_all_foot.='<td></td>';
	$account_all_foot.='<td></td>';
	$account_all_foot.='<td class="total_amount">Total</td>';
	$account_all_foot.='<td class="total_amount text-right">'.$All_Accounts['debit'].'</td>';
	$account_all_foot.='<td class="total_amount text-right">'.$All_Accounts['credit'].'</td>';
	$account_all_foot.='<td class="total_amount text-right">'.$All_Accounts['outstanding'].'</td>';
	$account_all_foot.='<td class="total_amount text-right">'.$All_Accounts['prepaid'].'</td>';
	$account_all_foot.='</tr>';
	if(empty($All_Accounts))
	{
		$return['result']='empty';
	}
	else
	{
		$return['result']='success';
		if(!empty($account_all)){
			krsort($account_all);
		}
		$return['row']['body']='';
		foreach ($All_Accounts['account_all'] as $key => $value) {
			$return['row']['body'].='<tr class="blue-pddng view_transaction">';
			$return['row']['body'].='<td class="padding_left">'.date('d-m-Y',strtotime($value['date'])).'</td>';
			$return['row']['body'].='<td>'.$value['SubGroup'].'</td>';
			$return['row']['body'].='<td hidden class="name">'.$value['actual_name'].'</td>';
			$return['row']['body'].='<td>'.$value['name'].'</td>';
			$return['row']['body'].='<td class="text-right">'.$value['debit'].'</td>';
			$return['row']['body'].='<td class="text-right">'.$value['credit'].'</td>';
			$return['row']['body'].='<td class="text-right">'.$value['outstanding'].'</td>';
			$return['row']['body'].='<td class="text-right">'.$value['prepaid'].'</td>';
			$return['row']['body'].='</tr>';
		}
	}
	$return['row']['foot']=$account_all_foot;
	echo json_encode($return);
	exit;
}
public function expense_table_list_by_group_id($group_id)
{
	$return['result']='Empty';
	$user_branch_id=$this->Session->read('User.branch_id');

	$priliminary_expense_written_off=18;
	$All_Accounts_table_list=$this->AccountHead_Table_ListByGroupId($group_id);
	$credit=0;
	$debit=0;
	$outstanding=0;
	$prepaid=0;
	$account_all=[];
	foreach ($All_Accounts_table_list as $keyA => $valueA) {
		if($valueA['AccountHead']['id']!=$priliminary_expense_written_off)
		{
			$explode_name=explode(' ',$valueA['AccountHead']['name']);
			$removed = array_pop($explode_name);
			if($removed!='PAID')
			{
				$explode_name=explode(' ',$valueA['AccountHead']['name']);
			}
			$explode_name=implode($explode_name, ' ');
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['actual_name']=$valueA['AccountHead']['name'];
			$account_single['name']=$explode_name;
			$account_single['SubGroup']=$valueA['SubGroup']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['credit']='0';
			$account_single['debit']=$valueA['AccountHead']['opening_balance'];
			$account_single['outstanding']=0;
			$account_single['prepaid']=0;
			$Journal=$this->General_Journal_Debit_N_Credit_function($account_single['id'],$user_branch_id);
			$account_single['credit']+=$Journal['credit'];
			$account_single['debit']+=$Journal['debit'];
			$Outstanding_AccountHead=$this->AccountHead->findByName($account_single['name'].' OUTSTANDING');
			if(!empty($Outstanding_AccountHead))
			{
				$account_single['outstanding']=$Outstanding_AccountHead['AccountHead']['opening_balance'];
				$account_single['debit']+=$Outstanding_AccountHead['AccountHead']['opening_balance'];
				$Journal_outstanding=$this->General_Journal_Debit_N_Credit_function($Outstanding_AccountHead['AccountHead']['id'],$user_branch_id);
				$account_single['outstanding']+=$Journal_outstanding['credit']-$Journal_outstanding['debit'];
			}
			$PREPAID_AccountHead=$this->AccountHead->findByName($account_single['name'].' PREPAID');
			if(!empty($PREPAID_AccountHead))
			{
				$account_single['prepaid']=$PREPAID_AccountHead['AccountHead']['opening_balance'];
				$account_single['debit']+=$PREPAID_AccountHead['AccountHead']['opening_balance'];
				$Journal_PREPAID=$this->General_Journal_Debit_N_Credit_function($PREPAID_AccountHead['AccountHead']['id'],$user_branch_id);
				$account_single['prepaid']+=$Journal_PREPAID['debit']-$Journal_PREPAID['credit'];
			}
			$credit+=$account_single['credit']+$account_single['debit']-$account_single['outstanding'];
			$debit+=$account_single['debit'];
			$outstanding+=$account_single['outstanding'];
			$prepaid+=$account_single['prepaid'];
			$account_single['credit']+=$account_single['debit']-$account_single['outstanding']+$account_single['prepaid'];;
			$account_all[$account_single['id']]=$account_single;
		}
	}
	$return['account_all']=$account_all;
	if(!empty($return['account_all'])){
		krsort($return['account_all']);
	}
	$return['credit']=$credit;
	$return['debit']=$debit;
	$return['outstanding']=$outstanding;
	$return['prepaid']=$prepaid;
	return $return;
}
public function EditAccountHead_expense()
{
	$data=$this->request->data['AccountHead'];
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	$excluding_ids=['4','5'];
	$discount_paid_account_head_id=$this->GetAccountHeadIdByGroupNameAndAccountHeadName(12,'DISCOUNT_ON_PURCHASE PAID','DISCOUNT_ON_PURCHASE PAID');
	$insurance_account_head_id=$this->GetAccountHeadIdByGroupNameAndAccountHeadName(12,'Insurance','Insurance');
	$tax_on_insurance_account_head_id=$this->GetAccountHeadIdByGroupNameAndAccountHeadName(12,'Tax On Insurance','Tax On Insurance');
	$miscellaneous_expenditure_account_head_id=$this->GetAccountHeadIdByGroupNameAndAccountHeadName(12,'Miscellaneous Expenditure','Miscellaneous Expenditure');
	$tax_on_miscellaneous_expenditure_account_head_id=$this->GetAccountHeadIdByGroupNameAndAccountHeadName(12,'Tax On Miscellaneous Expenditure','Tax On Miscellaneous Expenditure');
	$restricted_ids=[
	$discount_paid_account_head_id,
	$insurance_account_head_id,
	$tax_on_insurance_account_head_id,
	$miscellaneous_expenditure_account_head_id,
	$tax_on_miscellaneous_expenditure_account_head_id,
	'8',
	'13',
	'15',
	'18'
	];
	try {
		$datasource_AccountHead->begin();
		$date=$data['date'];
		$name=trim($data['name']);
		$description=$data['description'];
		$supplier=$data['supplier'];
		$show_in_app=$data['show_in_app'];
		$tax=isset($data['tax'])?$data['tax']:'';
		$trn_number=isset($data['trn_number'])?$data['trn_number']:'';
		$AccountHead_id=$data['account_id'];
		$AccountHead_old=$this->AccountHead->findById($AccountHead_id);
		if(in_array($AccountHead_id, $restricted_ids))
			throw new Exception("You Cant Edit This Head", 1);
		if(!in_array($AccountHead_id, $excluding_ids))
		{
			$old_name_explode=explode(' ',$AccountHead_old['AccountHead']['name']);
			array_pop($old_name_explode);
			$old_name_explode=implode(' ', $old_name_explode);
			$outstanding_name_old=$old_name_explode.' OUTSTANDING';
			$AccountHead_Outstanding=$this->AccountHead->findByName($outstanding_name_old);
			if(!empty($AccountHead_Outstanding))
			{
				$opening_balance=$data['outstanding_opening_balance'];
				if($data['account_id']!=$AccountHead_Outstanding['AccountHead']['id'])
				{
					$id=$AccountHead_Outstanding['AccountHead']['id'];
				} else {
					$id=$data['account_id'];
				}
				$name_new=$name.' OUTSTANDING';
				$function_return=$this->AccountHeadEdit($id,$name_new,$opening_balance,$date,$description,$tax,$trn_number,$supplier,$show_in_app);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message']);
			}
			$parepaid_name_old=$old_name_explode.' PREPAID';
			$AccountHead_Prepaid=$this->AccountHead->findByName($parepaid_name_old);
			if(!empty($AccountHead_Prepaid))
			{
				$opening_balance=$data['prepaid_opening_balance'];
				if($data['account_id']!=$AccountHead_Prepaid['AccountHead']['id'])
				{
					$id=$AccountHead_Prepaid['AccountHead']['id'];
				} else {
					$id=$data['account_id'];
				}
				$name_new=$name.' PREPAID';
				$function_return=$this->AccountHeadEdit($id,$name_new,$opening_balance,$date,$description,$tax,$trn_number,$supplier,$show_in_app);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message']);
			}
			$expense_name_old=$old_name_explode.' PAID';
			$AccountHead_Expense=$this->AccountHead->findByName($expense_name_old);
			if(!empty($AccountHead_Expense))
			{
				$opening_balance=$data['paid_opening_balance'];
				if($data['account_id']!=$AccountHead_Expense['AccountHead']['id'])
				{
					$id=$AccountHead_Expense['AccountHead']['id'];
				} else {
					$id=$data['account_id'];
				}
				$name_new=$name.' PAID';
				$function_return=$this->AccountHeadEdit($id,$name_new,$opening_balance,$date,$description,$tax,$trn_number,$supplier,$show_in_app);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message']);
			}
		}
		else
		{
			$id=$AccountHead_old['AccountHead']['id'];
			$name=$AccountHead_old['AccountHead']['name'];
			$opening_balance=$data['paid_opening_balance'];
			$function_return=$this->AccountHeadEdit($id,$name,$opening_balance,$date,$description,$tax,$trn_number);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
		}
		$return['result']='Success';
		$datasource_AccountHead->commit();
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$datasource_AccountHead->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$this->Session->setFlash(__($return['message']));
	}
	$this->redirect( Router::url( $this->referer(), true ) );
}
//Expense end
// Income Start
public function Income()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/Income';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/Income'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id_indirect=15;
	$group_id_direct=14;
	$master_group_id=6;
	$exclude_sub_group_id=['9','10','11','12'];
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['AccountHead']['date']=$date;
		$data['AccountHead']['group']=$group_id_indirect;
		$data['AccountHead']['opening_balance']='0';
		$this->request->data=$data;
		$AccountHead=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
					'table'=>'groups',
					'alias'=>'Group',
					'type'=>'INNER',
					'conditions'=>array('Group.id=SubGroup.group_id')
					),
				),
			'fields'=>array(
				'AccountHead.*',
				'SubGroup.*',
				'Group.name',
				),
			'conditions'=>array('master_group_id'=>$master_group_id)
			));
		$group_check_box_list=[
		$group_id_indirect=>'Indirect',
		$group_id_direct=>'Direct',
		];
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id_indirect);
		$this->set('group_list',$group_check_box_list);
		$this->set('SubGroup_list',$SubGroup_list);
		$this->set('AccountHead',$AccountHead);
	}
	else
	{
		$data=$this->request->data;
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_AccountHead->begin();
			$data=$this->request->data['AccountHead'];
			$opening_balance=$data['opening_balance'];
			$name=trim($data['name']);
			$date=$data['date'];
			$sub_group_id=$data['category'];
			if(in_array($sub_group_id, $exclude_sub_group_id))
				throw new Exception("You Cant Add Under this Group", 1);
			$description=$data['description'];
			$SubGroup=$this->SubGroup->findById($sub_group_id);
			$SubGroup_name=$SubGroup['SubGroup']['name'];
			$SubGroup_name=explode(' ',$SubGroup_name);
			$function_return=$this->AccountHeadCreate($sub_group_id,$name.' RECEIVED',$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$SubGroup_Accrued=$this->SubGroup->findByName($SubGroup_name[0].' ACCRUED');
			if(!$SubGroup_Accrued)
				throw new Exception("ACCRUED Head Not Found", 1);
			$Accrued_name=$name.' ACCRUED';
			$Accrued_sub_group_id=$SubGroup_Accrued['SubGroup']['id'];
			$function_return=$this->AccountHeadCreate($Accrued_sub_group_id,$Accrued_name,'0',$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$SubGroup_advance=$this->SubGroup->findByName($SubGroup_name[0].' ADVANCE');
			if(!$SubGroup_advance)
				throw new Exception("ADVANCE Head Not Found", 1);
			$advance_name=$name.' ADVANCE';
			$advance_subgroup_id=$SubGroup_advance['SubGroup']['id'];
			$function_return=$this->AccountHeadCreate($advance_subgroup_id,$advance_name,'0',$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function IncomeTransaction()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/IncomeTransaction'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$group_id_indirect=15;
	$group_id_direct=14;
	$advance_group_id=21;
	$accrued_group_id=6;
	$master_group_id=6;
	$user_id=1;
	if(!$this->request->data)
	{
		$date=date('d-m-Y');
		$data['Journal']['date']=$date;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
		$data['Journal']['group']=$group_id_indirect;
		$this->request->data=$data;
		$account_all=$this->income_table_list_by_group_id($group_id_indirect);
		$this->set('All_Accounts',$account_all['account_all']);
		$group_check_box_list=[
		$group_id_indirect=>'Indirect',
		$group_id_direct=>'Direct',
		];
		$this->set('group_list',$group_check_box_list);
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id_indirect);
		if($SubGroup_list)
		{
			$sub_group_id=array_keys($SubGroup_list)[0];
			$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		}
		else
		{
			$AccountHead_list=[];
		}	
		$this->set('AccountHead',$AccountHead_list);
		$this->set('SubGroup_list',$SubGroup_list);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		$datasource_SubGroup = $this->SubGroup->getDataSource();
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['account_head'];
			$debit=$data['mode'];
			$amount=$data['recieved'];
			$accrued=$data['accrued'];
			$accrued_hidden=$data['accrued_hidden'];
			$date=$data['date'];
			$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$remarks=$data['remarks'];
			$work_flow='Income';
			$advance_income=0;
			$AccountHead_income=$this->AccountHead->findById($data['account_head']);
			$explode_income_name=explode(' ',$AccountHead_income['AccountHead']['name']);
			$income_name=$explode_income_name[0];
//accrued_area start
			$SubGroup_name=explode(' ',$AccountHead_income['SubGroup']['name']);
			$accrued_subgroup_name=$SubGroup_name[0].' ACCRUED';
			$SubGroup=$this->SubGroup->findByName($accrued_subgroup_name);
			if(empty($SubGroup))
			{
				$datasource_SubGroup->begin();
				$SubGroup_return_function=$this->SubGroupCreate($accrued_group_id,$accrued_subgroup_name);
				if($SubGroup_return_function['result']!='Success')
					throw new Exception($SubGroup_return_function['message']);
				$SubGroup=$this->SubGroup->findByName($accrued_subgroup_name);
				$datasource_SubGroup->commit();
			}
			$accrued_AccountHead_name=$income_name.' ACCRUED';
			$AccountHead_accrued=$this->AccountHead->findByName($accrued_AccountHead_name);
			if(empty($AccountHead_accrued))
			{
				$datasource_AccountHead->begin();
				$accrued_sub_group_id=$SubGroup['SubGroup']['id'];
				$opening_balance=0;
				$description='';
				$AccountHead_return_function=$this->AccountHeadCreate($accrued_sub_group_id,$accrued_AccountHead_name,$opening_balance,$date,$description);
				if($AccountHead_return_function['result']!='Success')
					throw new Exception($AccountHead_return_function['message']);
				$AccountHead_accrued=$this->AccountHead->findByName($accrued_AccountHead_name);
				$datasource_AccountHead->commit();
			}
//outanding_area end
//advance_area start
			$advance_subgroup_name=$SubGroup_name[0].' ADVANCE';
			$SubGroup=$this->SubGroup->findByName($advance_subgroup_name);
			if(empty($SubGroup))
			{
				$datasource_SubGroup->begin();
				$SubGroup_return_function=$this->SubGroupCreate($advance_group_id,$advance_subgroup_name);
				if($SubGroup_return_function['result']!='Success')
					throw new Exception($SubGroup_return_function['message']);
				$SubGroup=$this->SubGroup->findByName($advance_subgroup_name);
				$datasource_SubGroup->commit();
			}
			$advance_AccountHead_name=$income_name.' ADVANCE';
			$AccountHead_advance=$this->AccountHead->findByName($advance_AccountHead_name);
			if(empty($AccountHead_advance))
			{
				$datasource_AccountHead->begin();
				$advance_sub_group_id=$SubGroup['SubGroup']['id'];
				$opening_balance=0;
				$description='';
				$AccountHead_return_function=$this->AccountHeadCreate($advance_sub_group_id,$advance_AccountHead_name,$opening_balance,$date,$description);
				if($AccountHead_return_function['result']!='Success')
					throw new Exception($AccountHead_return_function['message']);
				$AccountHead_advance=$this->AccountHead->findByName($advance_AccountHead_name);
				$datasource_AccountHead->commit();
			}
//advance_area end
			if($accrued>0)
			{
				$accrued_amount=$accrued-$accrued_hidden;
				if($accrued_amount>0) {
					$accrued_credit=$data['account_head'];
					$accrued_debit=$AccountHead_accrued['AccountHead']['id'];
					$function_return=$this->JournalCreate($accrued_credit,$accrued_debit,$accrued_amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
					if($function_return['result']!='Success')
						throw new Exception($function_return['message'], 1);
				} else {
					$accrued_amount=$accrued_hidden-$data['accrued'];
					$accrued_credit=$AccountHead_accrued['AccountHead']['id'];
					$accrued_debit=$data['mode'];
					$function_return=$this->JournalCreate($accrued_credit,$accrued_debit,$accrued_amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
					if($function_return['result']!='Success')
						throw new Exception($function_return['message'], 1);
				}
			} else if($data['accrued_hidden']) {
				if($data['accrued']<0) {
					$accrued_hidden=$data['accrued_hidden'];
				} else {
					$accrued_hidden=$data['accrued_hidden']-$data['accrued'];
				}
				if($accrued_hidden) {
					$accrued_credit=$AccountHead_accrued['AccountHead']['id'];
					$accrued_debit=$data['mode'];
					$function_return=$this->JournalCreate($accrued_credit,$accrued_debit,$accrued_hidden,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
					if($function_return['result']!='Success')
						throw new Exception($function_return['message'], 1);
				}
				if($accrued<0) {
					$advance_income=$accrued*-1;
					$advance_credit=$AccountHead_advance['AccountHead']['id'];
					$advance_debit=$data['mode'];
					$function_return=$this->JournalCreate($advance_credit,$advance_debit,$advance_income,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
					if($function_return['result']!='Success')
						throw new Exception($function_return['message'], 1);
				}
			} else if($accrued<0) {
				$advance_income=$accrued*-1;
				$advance_credit=$AccountHead_advance['AccountHead']['id'];
				$advance_debit=$data['mode'];
				$function_return=$this->JournalCreate($advance_credit,$advance_debit,$advance_income,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message'], 1);
			}
			if($data['accrued_hidden']) {
				$amount=$data['recieved']-($data['accrued_hidden']-$data['accrued']);	
			}
			else {
				$amount=$data['recieved']-$advance_income;
			}
			if($amount){
				$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message'], 1);
			}
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$datasource_SubGroup->rollback();
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function income_table_list_by_group_id_ajax($group_id)
{
	$All_Accounts=$this->income_table_list_by_group_id($group_id);
	$account_all_foot='<tr>';
	$account_all_foot.='<td></td>';
	$account_all_foot.='<td></td>';
	$account_all_foot.='<td class="total_amount">Total</td>';
	$account_all_foot.='<td class="total_amount text-right">'.$All_Accounts['credit'].'</td>';
	$account_all_foot.='<td class="total_amount text-right">'.$All_Accounts['debit'].'</td>';
	$account_all_foot.='<td class="total_amount text-right">'.$All_Accounts['accrued'].'</td>';
	$account_all_foot.='</tr>';
	if(empty($All_Accounts['account_all']))
	{
		$return['result']='empty';
	}
	else
	{
		$return['result']='success';
		if(!empty($All_Accounts)){
			krsort($All_Accounts);
		}
		$return['row']['body']='';
		foreach ($All_Accounts['account_all'] as $key => $value) {
			$return['row']['body'].='<tr class="blue-pddng view_transaction">';
			$return['row']['body'].='<td class="padding_left">'.date('d-m-Y',strtotime($value['date'])).'</td>';
			$return['row']['body'].='<td>'.$value['SubGroup'].'</td>';
			$return['row']['body'].='<td class="name">'.$value['actual_name'].'</td>';
			$return['row']['body'].='<td class="text-right">'.$value['credit'].'</td>';
			$return['row']['body'].='<td class="text-right">'.$value['debit'].'</td>';
			$return['row']['body'].='<td class="text-right">'.$value['accrued'].'</td>';
			$return['row']['body'].='</tr>';
		}
	}
	$return['row']['foot']=$account_all_foot;
	echo json_encode($return);
	exit;
}
public function income_table_list_by_group_id($group_id)
{
	$All_Accounts_table_list=$this->AccountHead_Table_ListByGroupId($group_id);
	$return=['result'=>'empty'];
	$account_all=[];
	$credit=0;
	$debit=0;
	$accrued=0;
	$advance=0;
	foreach ($All_Accounts_table_list as $keyA => $valueA) {
		$explode_name=explode(' ',$valueA['AccountHead']['name']);
		$account_single['id']=$valueA['AccountHead']['id'];
		$account_single['name']=$explode_name[0];
		$account_single['actual_name']=$valueA['AccountHead']['name'];
		$account_single['SubGroup']=$valueA['SubGroup']['name'];
		$account_single['date']=$valueA['AccountHead']['created_at'];
		$account_single['credit']='0';
		$account_single['debit']=$valueA['AccountHead']['opening_balance'];
		$account_single['accrued']=0;
		$account_single['advance']=0;
		$AccountHead_accrued=$this->AccountHead->findByName($account_single['name'].' ACCRUED');
		$Journal=$this->General_Journal_Debit_N_Credit_function($account_single['id']);
		$account_single['debit']+=$Journal['debit'];
		$account_single['credit']+=$Journal['credit'];
		if(!empty($AccountHead_accrued))
		{
			$account_single['accrued']=$AccountHead_accrued['AccountHead']['opening_balance'];
			$account_single['debit']+=$AccountHead_accrued['AccountHead']['opening_balance'];
			$Journal_ACCRUED=$this->General_Journal_Debit_N_Credit_function($AccountHead_accrued['AccountHead']['id']);
			$account_single['accrued']+=$Journal_ACCRUED['debit']-$Journal_ACCRUED['credit'];
		}
		$AccountHead_advance=$this->AccountHead->findByName($account_single['name'].' ADVANCE');
		if(!empty($AccountHead_advance))
		{
			$account_single['credit']+=$AccountHead_advance['AccountHead']['opening_balance'];
			$account_single['advance']=$AccountHead_advance['AccountHead']['opening_balance'];
			$Journal_ADVANCE=$this->General_Journal_Debit_N_Credit_function($AccountHead_advance['AccountHead']['id']);
			$account_single['advance']+=$Journal_ADVANCE['credit']-$Journal_ADVANCE['debit'];
		}
		$credit+=$account_single['advance'];
		$debit+=$account_single['credit']-$account_single['accrued']+$account_single['advance'];
		$accrued+=$account_single['accrued'];
		$advance+=$account_single['advance'];
		$account_single['debit']+=$account_single['credit']-$account_single['accrued']+$account_single['advance'];
		$account_all[$account_single['id']]=$account_single;
	}
	if(!empty($account_all)){
		krsort($account_all);
		$return=['result'=>'success'];
	}
	$return['account_all']=$account_all;
	$return['credit']=$credit;
	$return['debit']=$debit;
	$return['accrued']=$accrued;
	return $return;
}
public function get_accrued_balance_from_income($name)
{
	$advance=0;
	$debit=0;
	$credit=0;
	$bank=2;
	$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
	$modes=['1'];
	foreach ($bank_list as $key => $value) {
		array_push($modes, $key);
	}
	$explode_name=explode(' ',$name);
	$AccountHead=$this->AccountHead->findByName($explode_name[0].' ACCRUED');
	if(!empty($AccountHead)) {
		$id=$AccountHead['AccountHead']['id'];
		if(!empty($AccountHead))
		{
			$advance+=$AccountHead['AccountHead']['opening_balance'];
			$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
				'debit'=>$id,
				'flag=1'
				)));
			foreach ($Journal_debit as $key => $value) {
				$debit+=$value['Journal']['amount'];
			}
//total
			$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
				'credit'=>$id,
				'debit'=>$modes,
				'flag=1'
				)));
			foreach ($Journal_credit as $key => $value) {
				$credit+=$value['Journal']['amount'];
			}
			$advance+=$debit-$credit;
		}
	}
	echo json_encode($advance);
	exit;
}
public function get_accrued_balance($id)
{
	$advance=0;
	$debit=0;
	$credit=0;
	$bank=2;
	$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
	$modes=['1'];
	foreach ($bank_list as $key => $value) {
		array_push($modes, $key);
	}
	$AccountHead=$this->AccountHead->findById($id);
	if(!empty($AccountHead)) {
		$id=$AccountHead['AccountHead']['id'];
		if(!empty($AccountHead))
		{
			$advance+=$AccountHead['AccountHead']['opening_balance'];
			$Journal_debit=$this->Journal->find('all',array('conditions'=>array(
				'debit'=>$id,
				'flag=1'
				)));
			foreach ($Journal_debit as $key => $value) {
				$debit+=$value['Journal']['amount'];
			}
//total
			$Journal_credit=$this->Journal->find('all',array('conditions'=>array(
				'credit'=>$id,
				'debit'=>$modes,
				'flag=1'
				)));
			foreach ($Journal_credit as $key => $value) {
				$credit+=$value['Journal']['amount'];
			}
			$advance+=$debit-$credit;
		}
	}
	echo json_encode($advance);
	exit;
}
public function EditAccountHead_income()
{
	$data=$this->request->data['AccountHead'];
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	$excluding_ids=['6','7'];
	$restricted_ids=['9','12','14'];
	try {
		$datasource_AccountHead->begin();
		$date=$data['date'];
		$name=trim($data['name']);
		$AccountHead_id=$data['account_id'];
		$AccountHead_old=$this->AccountHead->findById($AccountHead_id);
		if(in_array($AccountHead_id, $restricted_ids))
			throw new Exception("You Cant Edit This Head", 1);
		if(!in_array($AccountHead_id, $excluding_ids))
		{
			$old_name_explode=explode(' ',$AccountHead_old['AccountHead']['name']);
			$accrued_name_old=$old_name_explode[0].' ACCRUED';
			$AccountHead_accrued=$this->AccountHead->findByName($accrued_name_old);
			if(!empty($AccountHead_accrued))
			{
				if($data['account_id']!=$AccountHead_accrued['AccountHead']['id'])
				{
					$description=$AccountHead_accrued['AccountHead']['description'];
					$opening_balance=$AccountHead_accrued['AccountHead']['opening_balance'];
					$id=$AccountHead_accrued['AccountHead']['id'];
				} else {
					$id=$data['account_id'];
					$opening_balance=$data['opening_balance'];
					$description=$data['description'];
				}
				$name_new=$name.' ACCRUED';
				$function_return=$this->AccountHeadEdit($id,$name_new,$opening_balance,$date,$description);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message']);
			}
			$parepaid_name_old=$old_name_explode[0].' ADVANCE';
			$AccountHead_advance=$this->AccountHead->findByName($parepaid_name_old);
			if(!empty($AccountHead_advance))
			{
				if($data['account_id']!=$AccountHead_advance['AccountHead']['id'])
				{
					$description=$AccountHead_advance['AccountHead']['description'];
					$opening_balance=$AccountHead_advance['AccountHead']['opening_balance'];
					$id=$AccountHead_advance['AccountHead']['id'];
				} else {
					$id=$data['account_id'];
					$opening_balance=$data['opening_balance'];
					$description=$data['description'];
				}
				$name_new=$name.' ADVANCE';
				$function_return=$this->AccountHeadEdit($id,$name_new,$opening_balance,$date,$description);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message']);
			}
			$expense_name_old=$old_name_explode[0].' RECEIVED';
			$AccountHead_Expense=$this->AccountHead->findByName($expense_name_old);
			if(!empty($AccountHead_Expense))
			{
				if($data['account_id']!=$AccountHead_Expense['AccountHead']['id'])
				{
					$description=$AccountHead_Expense['AccountHead']['description'];
					$opening_balance=$AccountHead_Expense['AccountHead']['opening_balance'];
					$id=$AccountHead_Expense['AccountHead']['id'];
				} else {
					$id=$data['account_id'];
					$opening_balance=$data['opening_balance'];
					$description=$data['description'];
				}
				$name_new=$name. ' RECEIVED';
				$function_return=$this->AccountHeadEdit($id,$name_new,$opening_balance,$date,$description);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message']);
			}
		}
		else
		{
			$id=$AccountHead_old['AccountHead']['id'];
			$name=$AccountHead_old['AccountHead']['name'];
			$opening_balance=$data['opening_balance'];
			$description=$data['description'];
			$function_return=$this->AccountHeadEdit($id,$name,$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
		}
		$return['result']='Success';
		$datasource_AccountHead->commit();
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$datasource_AccountHead->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$this->Session->setFlash(__($return['message']));
	}
	$this->redirect( Router::url( $this->referer(), true ) );
}
// Income End
// Voucher start
// VoucherReceipt start
public function VoucherReceipt16_5_2020()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/VoucherReceipt'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$cash_group_id=1;
	$bank_group_id=2;
	$crediters_group_id=18;
	$reserves_group_id=24;
	$provision_group_id=23;
	$pre_paid_expense_group_id=5;
	$outstanding_expense_group_id=20;
	$profit_loss_group_id=25;
	$profit_loss_dot_group_id=27;
	$expense_master_group_id=5;
	$user_id=1;
	if(!$this->request->data)
	{
		$AccountHeads=$this->AccountHead->find('all',array(
			'joins'=>[
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			],
			'conditions'=>array(
				'not'=>array(
					'SubGroup.group_id'=>array(
						$cash_group_id,
						$bank_group_id,
						$crediters_group_id,
						$profit_loss_group_id,
						$profit_loss_dot_group_id,
						$pre_paid_expense_group_id,
						$outstanding_expense_group_id,
						$reserves_group_id,
						$provision_group_id,
						),
					'Group.master_group_id'=>[$expense_master_group_id],
					),
				),
			'fields'=>[
			'AccountHead.id',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			'AccountHead.name',
			]
			)
		);
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		}
		$this->set('AccountHead_list',$AccountHead_list);
		$data['Journal']['date']=date('d-m-Y');
		$data['Journal']['customer_type']=1;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y',strtotime('+6 month'));
		$this->request->data=$data;
		$account_all=[];
		foreach ($AccountHeads as $keyA => $valueA) {
			$type=$this->get_type_by_account_dead($valueA['AccountHead']['id']);
		    $Type_name=$type['Type']['name'];
		    $category_name=$category[$type['Type']['name']];
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['total']=0;
			$account_single['paid']=0;
			if($category_name=='credit_account')
		{
			$credit=$valueA['AccountHead']['opening_balance'];
			$debit='0';
			if($valueA['AccountHead']['opening_balance']<0)
			{
				$credit=0;
				$debit=$valueA['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$valueA['AccountHead']['opening_balance'];
			$credit='0';
			if($valueA['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$valueA['AccountHead']['opening_balance']*-1;
			}
		}
			$Credit_function_return=$this->General_Journal_Credit_function($valueA['AccountHead']['id']);
			//$account_single['total']-=$Credit_function_return['credit'];
			$Debit_function_return=$this->General_Journal_Debit_function($valueA['AccountHead']['id']);
		//	$account_single['total']+=$Debit_function_return['debit'];
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id']);
			if($category_name=='credit_account') { 
				$account_single['total']+=$Debit_N_Credit_function['credit']+$credit;
			$account_single['paid']+=$Debit_N_Credit_function['debit']+$debit;
				$account_single['total']=$account_single['total']-$account_single['paid'];
			 }
			   else {
			   	$account_single['total']+=$Debit_N_Credit_function['debit']+$debit;
			$account_single['paid']+=$Debit_N_Credit_function['credit']+$credit;
				$account_single['total']=$account_single['total']-$account_single['paid'];
				 }
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['account_head'];
			$debit=$data['mode'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$work_flow='Voucher Receipt';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
// VoucherReceipt end
// VoucherPayment start
public function VoucherPayment16_5_2020()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/VoucherPayment'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$cash_group_id=1;
	$bank_group_id=2;
	$crediters_group_id=18;
	$profit_loss_group_id=25;
	$customer_group_id=3;
	$income_master_group_id=6;
	$Miscellaneous_Expenditure_And_Losses_group_id=11;
	$accrude_group_id=6;
	$advance_group_id=21;
	$reserves_group_id=24;
	$capital_master_group_id=4;
	$current_asset_master_group_id=1;
	$fixed_asset_master_group_id=2;
	$investment_asset_master_group_id=3;
	$long_liabilities_master_group_id=7;
	$medium_liabilities_master_group_id=8;
	$user_id=1;
	if(!$this->request->data)
	{
		$AccountHeads=$this->AccountHead->find('all',array(
			'joins'=>[
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			],
			'conditions'=>[
			'NOT'=>array(
				'SubGroup.group_id'=>array(
					$cash_group_id,
					$customer_group_id,
					$bank_group_id,
					$profit_loss_group_id,
					$accrude_group_id,
					$advance_group_id,
					$reserves_group_id,
					),
				'Group.master_group_id'=>array(
					$income_master_group_id,
					$capital_master_group_id,
// $fixed_asset_master_group_id,
					$investment_asset_master_group_id,
					$Miscellaneous_Expenditure_And_Losses_group_id,
					),
				),
			],
			'fields'=>[
			'AccountHead.id',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			'AccountHead.name',
			]
			)
		);
// pr($AccountHeads); exit;
		$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		}
		$this->set('AccountHead_list',$AccountHead_list);
		$data['Journal']['date']=date('d-m-Y');
		$data['Journal']['customer_type']=1;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y',strtotime('+6 month'));
		$this->request->data=$data;
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$account_all=[];
		foreach ($AccountHeads as $keyA => $valueA) {
			$type=$this->get_type_by_account_dead($valueA['AccountHead']['id']);
		    $Type_name=$type['Type']['name'];
		    $category_name=$category[$type['Type']['name']];
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['total']=0;
			$account_single['paid']=0;
			if($category_name=='credit_account')
		{
			$credit=$valueA['AccountHead']['opening_balance'];
			$debit='0';
			if($valueA['AccountHead']['opening_balance']<0)
			{
				$credit=0;
				$debit=$valueA['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$valueA['AccountHead']['opening_balance'];
			$credit='0';
			if($valueA['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$valueA['AccountHead']['opening_balance']*-1;
			}
		}
			$Debit_function_return=$this->General_Journal_Debit_function($valueA['AccountHead']['id']);
			//$account_single['total']+=$Debit_function_return['debit'];
			$Credit_function_return=$this->General_Journal_Credit_function($valueA['AccountHead']['id']);
			//$account_single['total']-=$Credit_function_return['credit'];
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id']);
			if($category_name=='credit_account') { 
				$account_single['total']+=$Debit_N_Credit_function['credit']+$credit;
			$account_single['paid']+=$Debit_N_Credit_function['debit']+$debit;
				$account_single['total']=$account_single['total']-$account_single['paid'];
			 }
			   else {
			   	$account_single['total']+=$Debit_N_Credit_function['debit']+$debit;
			$account_single['paid']+=$Debit_N_Credit_function['credit']+$credit;
				$account_single['total']=$account_single['total']-$account_single['paid'];
				 }
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['mode'];
			$debit=$data['account_head'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$external_voucher=$data['external_voucher'];
			$voucher_no=$data['voucher_no'];
			$work_flow='Voucher Payment';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
// VoucherPayment end
// VoucherReceipt end
// VoucherGeneral start
public function VoucherGeneral16_5_2020()
{
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/VoucherGeneral';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/VoucherGeneral'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$profit_loss_group_id=25;
	$profit_loss_dot_group_id=27;
	$reserves_group_id=24;
	$user_id=1;
	if(!$this->request->data)
	{
		$AccountHeads=$this->AccountHead->find('all',array(
			'conditions'=>[
			'NOT'=>array(
				'SubGroup.group_id'=>array(
					$profit_loss_group_id,
					$profit_loss_dot_group_id,
					$reserves_group_id,
					),
				),
			],
			'order'=>array('AccountHead.name ASC'),
			'fields'=>[
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			//'AccountHeadCredit.name',
			//'AccountHeadDebit.name',
			]
			)
		);
		$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		}
		$this->set('AccountHead_list',$AccountHead_list);
		$data['Journal']['date']=date('d-m-Y');
		$data['Journal']['customer_type']=1;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y',strtotime('+6 month'));
		$this->request->data=$data;
		$account_all=[];
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];

		foreach ($AccountHeads as $keyA => $valueA) {
		$type=$this->get_type_by_account_dead($valueA['AccountHead']['id']);
		$Type_name=$type['Type']['name'];
		$category_name=$category[$type['Type']['name']];
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['total']=0;
			$account_single['paid']=0;
			$Debit_function_return=$this->General_Journal_Debit_function($valueA['AccountHead']['id']);
			//$account_single['total']+=$Debit_function_return['debit'];
			$Credit_function_return=$this->General_Journal_Credit_function($valueA['AccountHead']['id']);
			//$account_single['total']-=$Credit_function_return['credit'];
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id']);
				if($category_name=='credit_account')
		{
			$credit=$valueA['AccountHead']['opening_balance'];
			$debit='0';
			if($valueA['AccountHead']['opening_balance']<0)
			{
				$credit=0;
				$debit=$valueA['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$valueA['AccountHead']['opening_balance'];
			$credit='0';
			if($valueA['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$valueA['AccountHead']['opening_balance']*-1;
			}
		}
			if($category_name=='credit_account') { 
				$account_single['total']+=$Debit_N_Credit_function['credit']+$credit;
			$account_single['paid']+=$Debit_N_Credit_function['debit']+$debit;
				$account_single['total']=$account_single['total']-$account_single['paid'];
			 }
			   else {
			   	$account_single['total']+=$Debit_N_Credit_function['debit']+$debit;
			$account_single['paid']+=$Debit_N_Credit_function['credit']+$credit;
				$account_single['total']=$account_single['total']-$account_single['paid'];
				 }
			//pr($account_single['total']);
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['to_account'];
			$debit=$data['by_account'];
			if($credit==$debit)
				throw new Exception("Cant Use Same head", 1);
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no=$data['voucher_no'];
			$work_flow='Voucher General';
			$external_voucher=$data['external_voucher'];
// $function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no);
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,'',$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
// VoucherGeneral end
// VoucherContra start
public function VoucherContra16_5_2020()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/VoucherContra'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$cash_group_id=1;
	$bank_group_id=2;
	$user_id=1;
	$executive= $this->Executive->find('list',array('fields'=>'Executive.id,Executive.account_head_id',));

	array_push($executive,1585);
	if(!$this->request->data)
	{
		$AccountHeads=$this->AccountHead->find('all',array(
			'conditions'=>[
			'SubGroup.group_id'=>array(
				$cash_group_id,
				$bank_group_id,
				),
			'AccountHead.id !='=>$executive,

			],
			'order'=>array('SubGroup.id DESC'),
			'fields'=>[
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			]
			));
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		}
		$this->set('AccountHead_list',$AccountHead_list);
		$data['Journal']['date']=date('d-m-Y');
		$data['Journal']['customer_type']=1;
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y',strtotime('+6 month'));
		$this->request->data=$data;
		$account_all=[];
		foreach ($AccountHeads as $keyA => $valueA) {
			$type=$this->get_type_by_account_dead($valueA['AccountHead']['id']);
		    $Type_name=$type['Type']['name'];
		    $category_name=$category[$type['Type']['name']];
			$account_single['id']=$valueA['AccountHead']['id'];
			$account_single['name']=$valueA['AccountHead']['name'];
			$account_single['date']=$valueA['AccountHead']['created_at'];
			$account_single['total']=0;
			$account_single['paid']=0;
			if($category_name=='credit_account')
		{
			$credit=$valueA['AccountHead']['opening_balance'];
			$debit='0';
			if($valueA['AccountHead']['opening_balance']<0)
			{
				$credit=0;
				$debit=$valueA['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$valueA['AccountHead']['opening_balance'];
			$credit='0';
			if($valueA['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$valueA['AccountHead']['opening_balance']*-1;
			}
		}
			$Debit_function_return=$this->General_Journal_Debit_function($valueA['AccountHead']['id']);
			//$account_single['total']+=$Debit_function_return['debit'];
			$Credit_function_return=$this->General_Journal_Credit_function($valueA['AccountHead']['id']);
			//$account_single['total']-=$Credit_function_return['credit'];
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id']);
			if($category_name=='credit_account') { 
				$account_single['total']+=$Debit_N_Credit_function['credit']+$credit;
			$account_single['paid']+=$Debit_N_Credit_function['debit']+$debit;
				$account_single['total']=$account_single['total']-$account_single['paid'];
			 }
			   else {
			   	$account_single['total']+=$Debit_N_Credit_function['debit']+$debit;
			$account_single['paid']+=$Debit_N_Credit_function['credit']+$credit;
				$account_single['total']=$account_single['total']-$account_single['paid'];
				 }
			$account_all[$account_single['id']]=$account_single;
		}
		$this->set('All_Account',$account_all);
	}
	else
	{
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['to_account'];
			$debit=$data['by_account'];
			if($credit==$debit)
				throw new Exception("Cant Use Same head", 1);
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no='';
			$external_voucher=$data['external_voucher'];
			$work_flow='Voucher Contra';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
// VoucherContra end
// Voucher end
// General start
public function GetVoucherNo()
{
	$Journal=$this->Journal->find('first',array('fields' => array('MAX(Journal.voucher_no) as voucher_no')));
	if($Journal) { $voucher_no=$Journal[0]['voucher_no']+1; } else { $voucher_no=1; }
// $Journal_gap=$this->Journal->query('SELECT a.voucher_no+1 AS start, MIN(b.voucher_no) - 1 AS end
// 	FROM journals AS a, journals AS b
// 	WHERE a.voucher_no < b.voucher_no
// 	GROUP BY a.voucher_no
// 	HAVING start < MIN(b.voucher_no) AND a.voucher_no+1 = MIN(b.voucher_no) - 1
// 	');
// if($Journal_gap)
// 	$voucher_no=$Journal_gap['0']['0']['start'];
	$voucher_no_one=$this->Journal->findByVoucherNo(1);
	if(!$voucher_no_one) $voucher_no=1;
	return $voucher_no;
}

public function JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no=null,$external_voucher=null,$day_register_id=null,$branch_id=null,$executive_id=null,$route_id=null)
{
	
	try {

		if(empty($voucher_no)) { $voucher_no=$this->GetVoucherNo(); }
		if($amount)
		{
			$Journal_data=[
			'remarks'=>$remarks,
			'credit'=>$credit,
			'debit'=>$debit,
			'amount'=>floatval($amount),
			'date'=>date('Y-m-d',strtotime($date)),
			'work_flow'=>$work_flow,
			'voucher_no'=>$voucher_no,
			'external_voucher'=>$external_voucher,
			'day_register_id'=>$day_register_id,
			'flag'=>'1',
			'branch_id'=>$branch_id,
			'executive_id'=>$executive_id,
			'route_id'=>$route_id,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_at'=>date('Y-m-d H:i:s'),
			];
			$this->Journal->create();
			if(!$this->Journal->save($Journal_data))
			{
				$errors = $this->Journal->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	return $return;
}
public function JournalCreate_old_for_receipt_bonus($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no=null,$external_voucher=null,$day_register_id=null,$branch_id=null,$executive_id=null)
{
	try {

		if(empty($voucher_no)) { $voucher_no=$this->GetVoucherNo(); }
		if($amount)
		{
			$Journal_data=[
			'remarks'=>$remarks,
			'credit'=>$credit,
			'debit'=>$debit,
			'amount'=>floatval($amount),
			'date'=>date('Y-m-d',strtotime($date)),
			'work_flow'=>$work_flow,
			'voucher_no'=>$voucher_no,
			'external_voucher'=>$external_voucher,
			'day_register_id'=>$day_register_id,
			'flag'=>'1',
			'branch_id'=>$branch_id,
			'executive_id'=>$executive_id,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_at'=>date('Y-m-d H:i:s'),
			];
			$journal_id=$this->Journal->find('first',array(
				'order'=>'Journal.id DESC',
				));
			$journal_id_bonus=$journal_id['Journal']['id']+1;
			$credit_account=$this->AccountHead->findById($credit);
			$debit_account=$this->AccountHead->findById($debit);
//if($debit_account['AccountHead']['sub_group_id']==9)
			if($credit_account['AccountHead']['sub_group_id']==3)
			{

				if(strpos($remarks,'Sale Invoice No :') !== false)
				{

					if($executive_id!=0)
					{
						$staff_id=$this->Executive->field('Executive.staff_id',array('Executive.id'=>$executive_id));
						$ExecutiveBonus=$this->ExecutiveBonus->findByExecutiveIdAndMonthYear($executive_id,date('m-Y'));
						if(!$ExecutiveBonus)
						{
							$this->ExecutiveBonus->create();
							$ExecutiveBonus_data=[
							'executive_id'=>$executive_id,
							'staff_id'=>$staff_id,
							'month_year'=>date('m-Y'),
							'amount'=>0,
							];
							if(!$this->ExecutiveBonus->save($ExecutiveBonus_data)) { $errors = $this->ExecutiveBonus->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }
							$ExecutiveBonus=$this->ExecutiveBonus->findById($this->ExecutiveBonus->getLastInsertId());
						}
						$Customer=$this->Customer->findByAccountHeadId($credit);
						$credit_period=0;
						if(!empty($Customer['Customer']['credit_period']))
							$credit_period=$Customer['Customer']['credit_period'];
						$remarks_explode=explode(':', $remarks);
						$invoice_no=$remarks_explode[1];
						$sale_id=$this->Sale->field('Sale.id',array('Sale.invoice_no'=>$invoice_no));
						$sale_date=$this->Sale->field('Sale.date_of_delivered',array('Sale.invoice_no'=>$invoice_no));
						$sale_amount=$this->Sale->field('Sale.grand_total',array('Sale.invoice_no'=>$invoice_no));
						$receipt_date=date('Y-m-d',strtotime($date));
						$total_bounus_amount=0;
						$total_bonus=0;
						$diff = abs(strtotime($receipt_date) - strtotime($sale_date));
						$diffdays=$diff / (60 * 60 * 24);
						$bonus_amount=0;
						$bonus_eligibility='No';
//$bonus_amount=$total_bounus_amount;
						$total_days=$credit_period;
						if($diffdays<=$total_days)
						{
							$bonus_eligibility='Yes';
							$this->ProductBonusDetail->virtualFields = array('total_bonus_amount' => "SUM(bonus_amount)");
							$ProductBonusDetail=$this->ProductBonusDetail->find('first',array(
								'fields'=>array('ProductBonusDetail.total_bonus_amount'),
								'conditions'=>array(
									'ProductBonusDetail.sale_id'=>$sale_id,),
								));
							$total_bounus_amount=$ProductBonusDetail['ProductBonusDetail']['total_bonus_amount'];
							$total_bonus=$total_bounus_amount;

						}
						$bonus_amount=$total_bounus_amount;
						$amount=$sale_amount;
						if($bonus_amount>0)
						{
							$ExecutiveBonusDetails_data=[
							'receipt_no'=>$external_voucher,
							'executive_id'=>$executive_id,
							'staff_id'=>$staff_id,
							'invoice_no'=>$invoice_no,
							'amount'=>$amount,
							'sale_date'=>$sale_date,
							'receipt_date'=>$receipt_date,
							'eligibility'=>$bonus_eligibility,
							'bonus_amount'=>$bonus_amount,
							'bonus_return_amount'=>0,
							'journal_id'=>$journal_id_bonus,
							];

							if($ExecutiveBonusDetails_data['amount']>0)
							{
								$this->ExecutiveBonusDetail->create();
								if(!$this->ExecutiveBonusDetail->save($ExecutiveBonusDetails_data)) { $errors = $this->ExecutiveBonusDetail->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }
							}
						}
						$this->ExecutiveBonus->id=$ExecutiveBonus['ExecutiveBonus']['id'];
						$this->ExecutiveBonus->saveField('amount',$this->ExecutiveBonus->field('amount')+$total_bonus);
					}

				}
// else
// {
// 	$debit_account=$this->AccountHead->findById($debit);

// 	if(in_array($debit_account['AccountHead']['sub_group_id'],['1','2']))
// 	{
// 		$Customer=$this->Customer->findByAccountHeadId($credit);
// 		$credit_period=0;
// 		if(!empty($Customer['Customer']['credit_period']))
// 			$credit_period=$Customer['Customer']['credit_period'];
// 		$route_id=$Customer['Customer']['route_id'];
// 		$this->Executive->unbindModel(array('hasMany'=>array('Sale')));
// 		$Executive=$this->Executive->find('first',array(
// 			"joins" => array(
// 		        array(
// 		          "table" => 'executive_route_mappings',
// 		          "alias" => 'ExecutiveRouteMapping',
// 		          "type" => 'inner',
// 		          "conditions" => array('ExecutiveRouteMapping.executive_id=Executive.id'),
// 		          ),
// 		        ),
// 		     'conditions' => array('ExecutiveRouteMapping.route_id'=>$route_id),
// 		     'fields'=>array('ExecutiveRouteMapping.route_id','Executive.id'),
// 		     ));
// 		$executive_id=$Executive['Executive']['id'];
// 		$total_days=$credit_period;
// 		$from_date=date('Y-m-d',strtotime('- '.$total_days.'days'));
// 		$to_date=date('Y-m-d');
// 		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
// 		$this->Sale->virtualFields = array('total_sale' => "SUM(Sale.grand_total)");


// 		$Sale=$this->Sale->find('all',[
// 			'conditions'=>array(
// 				'Sale.account_head_id'=>$credit,
// 			),
// 			'fields'=>[
// 				//'Sale.total_sale',
// 				'Sale.id',
// 				'Sale.invoice_no',
// 				'Sale.date_of_delivered',
// 				'Sale.grand_total',
// 				'Sale.executive_id',
// 				'Sale.balance',
// 			]
// 		]);
// 		$BankCharges=$this->Journal->find('all',[
// 			'conditions'=>array(
// 				'Journal.debit'=>$credit,
// 				'AccountHeadCredit.sub_group_id'=>['1','2'],
// 				'Journal.flag=1',
// 			),
// 			'order'=>['Journal.date ASC'],
// 			'fields'=>[
// 				'Journal.id',
// 				'Journal.amount',
// 			]
// 		]);
// 		$BankCharges_amount=0;
// 		foreach ($BankCharges as $key4 => $value_amount) {
// 			$BankCharges_amount+=$value_amount['Journal']['amount'];
// 		}
// 		$sale_total=0;
// 		$credit_account_details=$this->AccountHead->findById($credit);
// 		$creditor_opening_balance=$credit_account_details['AccountHead']['opening_balance'];

// 		$invoice_array_single=array();
// 		$Journal_voucher=$this->Journal->find('list',array(
// 			'conditions'=>array(
// 				'Journal.credit'=>$credit,
// 				'Journal.work_flow'=>array('Account Recievable','Sales Return','Voucher Receipt','Executive App Customer Receipt','Voucher General','Cheques','From Mobile','Sales','From Executive app'),
// 				'Journal.flag=1',
// 				'Journal.debit !='=>array(13),
// 				'Journal.remarks NOT LIKE' =>'%Sale Invoice No :%',
// 			),
// 			'fields'=>array(
// 				'Journal.id',
// 				'Journal.amount',
// 			),
// 		));
// 		$voucher_amount=0;
// 		$voucher_balance=0;
// 		foreach ($Journal_voucher as $key3 => $value_amount) {
// 			$voucher_amount+=$value_amount;
// 		}
// 		if($creditor_opening_balance)
// 		{
// 			$outstanding_amount=$creditor_opening_balance;
// 			if($creditor_opening_balance>$voucher_amount)
// 			{
// 				$creditor_opening_balance-=$voucher_amount;
// 				$voucher_amount=0;
// 			}
// 			else
// 			{
// 				$voucher_amount-=$creditor_opening_balance;
// 				$creditor_opening_balance=0;
// 			}
// 			if($creditor_opening_balance>$amount)
// 			{
// 				$creditor_opening_balance-=$amount;
// 				$amount=0;
// 			}
// 			else
// 			{
// 				$amount-=$creditor_opening_balance;
// 				$creditor_opening_balance=0;
// 			}

// 		}
// 		$voucher_amount-=$BankCharges_amount;
// 		if($Sale)
// 		{
// 			foreach ($Sale as $keySC2 => $valueSC2) {
// 				$balance_amount=floatval($valueSC2['Sale']['balance']);
// 				if($voucher_amount)
// 				{
// 					if($balance_amount<=$voucher_amount)
// 					{
// 						$voucher_balance=$balance_amount;
// 						$voucher_amount-=$voucher_balance;
// 						$balance_amount=0;
// 					}
// 					else
// 					{
// 						$balance_amount-=$voucher_amount;
// 						$voucher_amount=0;
// 					}
// 				}
// 				if($valueSC2['Sale']['balance']>0)
// 				{
// 					if($balance_amount>0){
// 						$invoice_array_single['sale_id'][]= $valueSC2['Sale']['id'];
// 						$invoice_array_single['delivered_date'][]= $valueSC2['Sale']['date_of_delivered'];
// 						$invoice_array_single['invoice_no'][]= $valueSC2['Sale']['invoice_no'];
// 						$invoice_array_single['grand_total'][]= $valueSC2['Sale']['grand_total'];
// 						$invoice_array_single['balance'][]=$balance_amount ;   
// 					}

// 				}

// 			}
// 		}
// 		if(!empty($invoice_array_single))
// 		{
// 			$total_bonus=0;
// 			$amount_balance=0;
// 			for ($i=0; $i <count($invoice_array_single['invoice_no']) ; $i++) {

// 				$ExecutiveBonus=$this->ExecutiveBonus->findByExecutiveIdAndMonthYear($executive_id,date('m-Y'));

// 				if(!$ExecutiveBonus)
// 				{
// 					$this->ExecutiveBonus->create();
// 					$ExecutiveBonus_data=[
// 						'executive_id'=>$executive_id,
// 						'month_year'=>date('m-Y'),
// 						'amount'=>0,
// 					];
// 					if(!$this->ExecutiveBonus->save($ExecutiveBonus_data)) { $errors = $this->ExecutiveBonus->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }
// 					$ExecutiveBonus=$this->ExecutiveBonus->findById($this->ExecutiveBonus->getLastInsertId());
// 				}

// 				$invoice_amount=$invoice_array_single['balance'][$i];
// 				if($invoice_array_single['balance'][$i]<=$amount)
// 				{
// 					$amount_balance=$invoice_array_single['balance'][$i];
// 					$invoice_array_single['balance'][$i]=0;
// 					$amount-=$amount_balance;
// 				}
// 				else
// 				{
// 					$invoice_array_single['balance'][$i]-=$amount;
// 					$amount=0;
// 				}
// 				$receipt_no=$external_voucher;
// 				$invoice_no=$invoice_array_single['invoice_no'][$i];
// 				$receipt_date=date('Y-m-d',strtotime($date));
// 				$sale_date=$invoice_array_single['delivered_date'][$i];
// 				$diff = abs(strtotime($receipt_date) - strtotime($sale_date));
// 				$diffdays=$diff / (60 * 60 * 24);
// 				$bonus_amount=0;
// 				$bonus_eligibility='No';
// 				$amount_paid=$invoice_amount-$invoice_array_single['balance'][$i];
// 				$invoice_grand_total=$invoice_array_single['grand_total'][$i];
// 				$invoice_collected_amount_percentage=number_format(($amount_paid/$invoice_grand_total*100),2,'.','');

// 				if($diffdays<=$total_days)
// 				{
// 					$bonus_eligibility='Yes';
// 					$total_bounus_amount=0;
// 					$this->ProductBonusDetail->virtualFields = array('total_bonus_amount' => "SUM(bonus_amount)");
// 					$ProductBonusDetail=$this->ProductBonusDetail->find('first',array(
// 					'fields'=>array('ProductBonusDetail.total_bonus_amount'),
// 					'conditions'=>array(
// 						'ProductBonusDetail.sale_id'=>$invoice_array_single['sale_id'][$i],),
// 					));
// 					$total_bounus_amount=$ProductBonusDetail['ProductBonusDetail']['total_bonus_amount'];
// 					$bonus_amount=$total_bounus_amount*$invoice_collected_amount_percentage/100;
// 					$total_bonus+=$total_bounus_amount*$invoice_collected_amount_percentage/100;
// 					// $amount_paid_taxable=$amount_paid*100/118;
// 					// $bonus_amount=$amount_paid_taxable*$incentive/100;
// 					// $total_bonus+=$amount_paid_taxable*$incentive/100;
// 				}
// 				if($bonus_amount>0)
// 				{
// 					$ExecutiveBonusDetails_data=[
// 					'receipt_no'=>$receipt_no,
// 						'executive_id'=>$executive_id,
// 						'invoice_no'=>$invoice_no,
// 						'amount'=>$amount_paid,
// 						'sale_date'=>$sale_date,
// 						'receipt_date'=>$receipt_date,
// 						'eligibility'=>$bonus_eligibility,
// 						'bonus_amount'=>$bonus_amount,
// 						'journal_id'=>$journal_id_bonus,
// 					];
// 					if($ExecutiveBonusDetails_data['amount']>0)
// 					{
// 						$this->ExecutiveBonusDetail->create();
// 						if(!$this->ExecutiveBonusDetail->save($ExecutiveBonusDetails_data)) { $errors = $this->ExecutiveBonusDetail->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }
// 					}
// 				}
// 				$sale_amount_balance=$this->Sale->field('Sale.balance',array('Sale.invoice_no'=>$invoice_no));
// 				$sale_amount_balance-=$amount_paid;
// 				// if($sale_amount_balance==0)
// 				// {
// 				// 	$this->Sale->id=$invoice_array_single['sale_id'][$i];
// 				// 	$this->Sale->saveField('receipt_flag',2);
// 				// 	$this->Sale->saveField('balance',$sale_amount_balance);
// 				// }
// 				// else if($sale_amount_balance>0 && $sale_amount_balance<$invoice_grand_total)
// 				// {
// 				// 	$this->Sale->id=$invoice_array_single['sale_id'][$i];
// 				// 	$this->Sale->saveField('receipt_flag',1);
// 				// 	$this->Sale->saveField('balance',$sale_amount_balance);
// 				// }
// 				$this->ExecutiveBonus->id=$ExecutiveBonus['ExecutiveBonus']['id'];
// 				$this->ExecutiveBonus->saveField('amount',$this->ExecutiveBonus->field('amount')+$total_bonus);
// 			}
// 		}						
// 	}

// }
			}
			$this->Journal->create();
			if(!$this->Journal->save($Journal_data))
			{
				$errors = $this->Journal->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	return $return;
}
public function AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description,$tax=null,$trn_number=null,$supplier=null,$show_in_app=null)
{
	try {
		$AccountHead_date=[
		'name'=>strtoupper($name),
		'opening_balance'=>$opening_balance,
		'description'=>$description,
		'tax'=>$tax,
		'trn_number'=>$trn_number,
		'supplier'=>$supplier,
		'show_in_app'=>$show_in_app,
		'sub_group_id'=>$sub_group_id,
		'acc_sub_group_id'=>$sub_group_id,
		'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
		'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
		];
		$this->AccountHead->create();
		if(!$this->AccountHead->save($AccountHead_date))
		{
			$errors = $this->AccountHead->validationErrors;
			foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	return $return;
}
public function AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description,$tax=null,$trn_number=null,$supplier=null,$show_in_app=null)
{
	try {
		$this->AccountHead->id=$AccountHead_id;
		if(!$this->AccountHead->saveField('opening_balance',$opening_balance)) throw new Exception("Cant Updated the Opening Balance", 1);
		if(!$this->AccountHead->saveField('name',strtoupper($name))) throw new Exception("Cant Updated the name", 1);
		if(!$this->AccountHead->saveField('description',$description)) throw new Exception("Cant Updated the description", 1);
		if(!$this->AccountHead->saveField('tax',$tax)) throw new Exception("Cant Updated the tax", 1);
		if(!$this->AccountHead->saveField('trn_number',$trn_number)) throw new Exception("Cant Updated the trn_number", 1);
		if(!$this->AccountHead->saveField('supplier',$supplier)) throw new Exception("Cant Updated the supplier", 1);
		if(!$this->AccountHead->saveField('show_in_app',$show_in_app)) throw new Exception("Cant Updated the show_in_app", 1);
		if(!$this->AccountHead->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date)))) throw new Exception("Error updation updated_at", 1);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	return $return;
}
public function AccountHeadGet_ajax($id)
{
	$AccountHead=$this->AccountHead->findById($id);
	echo json_encode($AccountHead);
	exit;
}
public function EditAccountHead()
{
	$data=$this->request->data['AccountHead'];
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_AccountHead->begin();
		$opening_balance=$data['opening_balance'];
		$date=$data['date'];
		$name=$data['name'];
		$description=$data['description'];
		$AccountHead_id=$data['account_id'];
		$function_return=$this->AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description);
		if($function_return['result']!='Success')
			throw new Exception($function_return['message']);
		$return['result']='Success';
		$datasource_AccountHead->commit();
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$datasource_AccountHead->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$this->Session->setFlash(__($return['message']));
	}
	$this->redirect( Router::url( $this->referer(), true ) );
}
public function sub_category_add_ajax()
{
	try {
		$data=$this->request->data;
		$name=$data['name'];
		if(!$name)
			throw new Exception("Empty name", 1);
		$group_id=$data['group_id'];
		if(!$group_id)
			throw new Exception("Empty group_id", 1);
		$group_data=[
		'name'=>strtoupper($name),
		'group_id'=>$group_id,
		'created_at'=>date('Y-m-d H:i:s'),
		'updated_at'=>date('Y-m-d H:i:s'),
		];
		$this->SubGroup->create();
		if(!$this->SubGroup->save($group_data))
		{
			$errors = $this->SubGroup->validationErrors;
			foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
			}
		}
		$SubGroup_id=$this->SubGroup->getLastInsertId();
		$SubGroup=$this->SubGroup->findById($SubGroup_id);
		$return['result']='Success';
		$return['key']=$SubGroup['SubGroup']['id'];
		$return['value']=$SubGroup['SubGroup']['name'];
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function SubGroupCreate($group_id,$name)
{
	try {
		$group_data=[
		'name'=>strtoupper($name),
		'group_id'=>$group_id,
		'created_at'=>date('Y-m-d H:i:s'),
		'updated_at'=>date('Y-m-d H:i:s'),
		];
		$this->SubGroup->create();
		if(!$this->SubGroup->save($group_data))
		{
			$errors = $this->SubGroup->validationErrors;
			foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	return $return;
}
public function AccountHead_delete($id)
{
	try {
		$excluding_ids=['1','2','3','4','5','6','7','8','9','10','11','12','13',];
		if(in_array($id, $excluding_ids))
			throw new Exception("You Cant Delete This Account Head", 1);
		$JournalCredit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.credit'=>$id,'flag=1'),
			));
		if($JournalCredit)
			throw new Exception("Used In Journal Debit", 1);
		$JournalDebit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.debit'=>$id,'flag=1'),
			));
		if($JournalDebit)
			throw new Exception("Used In Journal Credit", 1);
		if($this->Purchase->findByAccountHeadId($id))
			throw new Exception("Used In Purchase", 1);
		if($this->AssetPurchase->findByAccountHeadId($id))
			throw new Exception("Used In AssetPurchase", 1);
		$Customer=$this->Customer->findByAccountHeadId($id);
		if(!empty($Customer))
		{
			if(!$this->Customer->delete($Customer['Customer']['id']))
				throw new Exception("Cant Delete This Customer", 1);
		}
		if(!$this->AccountHead->delete($id))
			throw new Exception("Cant Delete This AccountHead", 1);

		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	$this->Session->setFlash(__($return['result']));
	$this->redirect( Router::url( $this->referer(), true ) );
}
public function General_Journal_Debit_function($debit)
{
// $bank=2;
// $bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
// $modes=['1'];
// foreach ($bank_list as $key => $value) {
// 	array_push($modes, $key);
// }
	$Journal=$this->Journal->find('all',array('conditions'=>array(
// 'credit'=>$modes,
		'debit'=>$debit,
		'flag=1'
		)));
	$account_single=[];
	$account_single['debit']=0;
	foreach ($Journal as $key => $value) {
		$account_single['debit']+=$value['Journal']['amount'];
		if(isset($account_single['date'])){
			if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
				$account_single['date']=$value['Journal']['date'];
		}
		else
		{
			$account_single['date']=$value['Journal']['date'];	
		}
	}
	return $account_single;
}
public function General_Journal_Credit_function($credit)
{
// $bank=2;
// $bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
// $modes=['1'];
// foreach ($bank_list as $key => $value) {
// 	array_push($modes, $key);
// }
	$Journal=$this->Journal->find('all',array('conditions'=>array(
// 'debit'=>$modes,
		'credit'=>$credit,
		'flag=1'
		)));
	$account_single=[];
	$account_single['credit']=0;
	foreach ($Journal as $key => $value) {
		$account_single['credit']+=$value['Journal']['amount'];
		if(isset($account_single['date'])){
			if(strtotime($account_single['date'])<strtotime($value['Journal']['date']))
				$account_single['date']=$value['Journal']['date'];
		}
		else
		{
			$account_single['date']=$value['Journal']['date'];	
		}
	}
	return $account_single;
}
public function General_Journal_Debit_N_Credit_function($account_head_id,$user_branch_id=null)
{
	//$account_head_id=3;
	$bank=2;
	$conditions_debit=[];
	$conditions_credit=[];

	if($user_branch_id)
	{
		$conditions_debit['branch_id']=$user_branch_id;
		$conditions_credit['branch_id']=$user_branch_id;
	}

	$conditions_debit['debit']=$account_head_id;
	$conditions_debit['flag']=1;
	$conditions_credit['credit']=$account_head_id;
	$conditions_credit['flag']=1;
	$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
	$modes=['1'];

	foreach ($bank_list as $key => $value) {
		array_push($modes, $key);
	}
	$account_single=[];
	$account_single['date']="";
	$account_single['debit']=0;
	$account_single['credit']=0;
	$Journal_Debit=$this->Journal->find('all',array('conditions'=>$conditions_debit));
	foreach ($Journal_Debit as $key => $value) {
		$account_single['debit']+=$value['Journal']['amount'];
		$account_single['date']=$value['Journal']['date'];
	}
	$Journal_Credit=$this->Journal->find('all',array('conditions'=>$conditions_credit));
	foreach ($Journal_Credit as $key => $value) {
		$account_single['credit']+=$value['Journal']['amount'];
		$account_single['date']=$value['Journal']['date'];
	}
	//pr($account_single);exit;
	return $account_single;
}

public function AccountHead_Option_ListBySubGroupId($sub_group_id)
{
	$AccountHeads=$this->AccountHead->find('all',array(
		'conditions'=>array(
			'acc_sub_group_id'=>$sub_group_id,
			),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'SubGroup.id',
			)
		));
	$AccountHead_list=[];
	foreach ($AccountHeads as $key => $value) {
		$AccountHead_list[$value['AccountHead']['id']] = $value['AccountHead']['name'];
	}
	return $AccountHead_list;
}
public function AccountHead_Option_ListBySubGroupId_ajax($sub_group_id)
{
	try {
		$AccountHeads=$this->AccountHead->find('all',array(
			'conditions'=>array(
				'SubGroup.id'=>$sub_group_id,
				),
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
				'SubGroup.id',
				)
			));
		$AccountHeads_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHeads_list[$value['AccountHead']['id']] = $value['AccountHead']['name'];
		}
		if(empty($AccountHeads))
			throw new Exception("Empty", 1);
		$return['result']='Success';
		$return['options']=$AccountHeads_list;
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
// public function SubGroup_Option_ListByGroupId($group_id1,$group_id2=null)
// {
// 	$SubGroups=$this->SubGroup->find['all',[
// 		'conditions'=>[
// 			'SubGroup.group_id'=>[$group_id1,$group_id2],
// 		],
// 		'fields'=>array(
// 			'SubGroup.id',
// 			'SubGroup.name',
// 		]
// 	]];
// 	$SubGroup_list=[];
// 	foreach ($SubGroups as $key => $value) {
// 		$SubGroup_list[$value['SubGroup']['id']] = $value['SubGroup']['name'];
// 	}
// 	return $SubGroup_list;
// }
public function SubGroup_Option_ListByGroupId($group_id1,$group_id2=null)
{
	$SubGroups=$this->SubGroup->find('all',array(
		'conditions'=>array(
			//'SubGroup.group_id'=>array($group_id1,$group_id2),
			'SubGroup.group_id'=>$group_id1,
			),
		'fields'=>array(
			'SubGroup.id',
			'SubGroup.name',
			)
		));
	$SubGroup_list=[];
	foreach ($SubGroups as $key => $value) {
		$SubGroup_list[$value['SubGroup']['id']] = $value['SubGroup']['name'];
	}
	return $SubGroup_list;
}
public function AccountHead_Option_ListByGroupId($group_id)
{
	$AccountHead=$this->AccountHead->find('all',array(
		'conditions'=>array(
			'SubGroup.group_id'=>$group_id,
			),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			)
		));
	$AccountHead_list=[];
	foreach ($AccountHead as $key => $value) {
		$AccountHead_list[$value['AccountHead']['id']] = $value['AccountHead']['name'];
	}
	return $AccountHead_list;
}
public function AccountHead_Option_ListByMasterGroupId($master_group_id)
{
	$AccountHead=$this->AccountHead->find('all',array(
		'joins'=>array(
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			),
		'conditions'=>array(
			'Group.master_group_id'=>$master_group_id,
			),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Group.master_group_id',
			)
		));
	$AccountHead_list=[];
	foreach ($AccountHead as $key => $value) {
		$AccountHead_list[$value['AccountHead']['id']] = $value['AccountHead']['name'];
	}
	return $AccountHead_list;
}
public function SubGroup_Option_ListByGroupId_ajax($group_id)
{
	try {
		$SubGroups=$this->SubGroup->find('list',array(
			'conditions'=>array(
				'SubGroup.group_id'=>$group_id,
				),
			'fields'=>array('SubGroup.id','SubGroup.name'),
			'order'=>array('SubGroup.name'=>'ASC'),
			));
		if(empty($SubGroups))
			throw new Exception("Empty", 1);
		$return['result']='Success';
		$return['options']=$SubGroups;
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function AccountHead_Table_ListBySubGroupId($sub_group_id)
{
	$AccountHeads=$this->AccountHead->find('all',array(
		'conditions'=>array(
			'SubGroup.id'=>$sub_group_id,
			),
		));
	return $AccountHeads;
}
public function AccountHead_Table_ListByGroupId($group_id)
{
	$AccountHeads=$this->AccountHead->find('all',array(
		'conditions'=>array(
			'SubGroup.group_id'=>$group_id,
			),
		));
	return $AccountHeads;
}
public function Journal_delete($id)
{
	try {
		$PayStructure=$this->PayStructure->findByJournalId($id,[
				'PayStructure.id','PayStructure.journal_id',
			]);
		if($PayStructure)
		{
		$this->PayStructure->id=$PayStructure['PayStructure']['id'];
		if(!$this->PayStructure->saveField('flag','0'))
				throw new Exception("Error Processing Request", 1);
		}
			$Journal=$this->Journal->find('first',array(
					'conditions'=>array(
						'Journal.id'=>$id,
						'Journal.flag=1',
					),
				));
			if($Journal['AccountHeadCredit']['sub_group_id']==15 ||$Journal['AccountHeadDebit']['sub_group_id']==3)
			{
			throw new Exception("You Cant Delete this", 1);
			}
			else
			{
				$this->ExpenseVatDetail->id=$this->ExpenseVatDetail->field('id',array('journal_id'=>$id));
				if(!$this->ExpenseVatDetail->saveField('flag','0'))
			throw new Exception("Error Processing Request", 1);
		$this->Journal->id=$id;
		if(!$this->Journal->saveField('flag','0'))
			throw new Exception("Error Processing Request", 1);
		if(!$this->Journal->saveField('updated_at',date('Y-m-d H:i:s')))
			throw new Exception("Error Processing Request", 1);
			}
	
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function get_type_by_account_dead($account_head_id)
{
				$AccountHead=$this->AccountHead->find('first',[
					'joins'=>array(
						array(
							'table'=>'groups',
							'alias'=>'Group',
							'type'=>'INNER',
							'conditions'=>array('Group.id=SubGroup.group_id')
						),
						array(
							'table'=>'master_groups',
							'alias'=>'MasterGroup',
							'type'=>'INNER',
							'conditions'=>array('MasterGroup.id=Group.master_group_id')
						),
						array(
							'table'=>'types',
							'alias'=>'Type',
							'type'=>'INNER',
							'conditions'=>array('MasterGroup.type_id=Type.id')
						),
					),
					'conditions'=>array('AccountHead.id'=>$account_head_id),
					'fields'=>array(
						'AccountHead.id',
						'AccountHead.name',
						'AccountHead.opening_balance',
						'SubGroup.id',
						'SubGroup.name',
						'Group.id',
						'Group.name',
						'MasterGroup.id',
						'MasterGroup.name',
						'Type.id',
						'Type.name',
					),
				]);
				return $AccountHead;
			}
public function general_journal_transaction_ajax()
{
	try {
		$user_branch_id=$this->Session->read('User.branch_id');
		$branch_id="";
		if($user_branch_id)
		{
			$branch_id=$user_branch_id;
		}
		// $data['name']='SUJITH_IMPREST. PREPAID';
		// $data['name']='RACY SANITARYWARES';
		// $data['from_date']='01-05-2010';
		// $data['to_date']='15-05-2019';
		$data=$this->request->data;
//pr($data);
//exit;
		$name=trim($data['name']);
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$AccountHead=$this->AccountHead->findByName(trim($name));

		if(empty($AccountHead))
			throw new Exception("Empty AccountHead", 1);
		$this->Journal->virtualFields = array(
			'total_amount' => "SUM(Journal.amount)",
			);

		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						'Journal.flag=1',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
//pr($Journal);
//exit;
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$Journal_all=[];
		foreach ($Journal as $key => $value) 
		{
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			if(!empty($value['Executive']['name'])){
				$Journal_single['executive']=$value['Executive']['name'];
			}
			else{
				$Journal_single['executive']="No Executive";
			}
			$Journal_single['external_voucher']=$value['Journal']['external_voucher'];
			if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
			{
				$Journal_single['mode']=$value['AccountHeadCredit']['name'];
				$Journal_single['debit']=$value['Journal']['amount'];
				$Journal_single['credit']=0.00;
			}
			else
			{
				$Journal_single['mode']=$value['AccountHeadDebit']['name'];
				$Journal_single['credit']=$value['Journal']['amount'];
				$Journal_single['debit']=0.00;
			}
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		$return['row']['tbody']='';
		$return['row']['tfoot']='';
		$Balance_Total=0;
		$credit_Total=0;
		$debit_Total=0;
		$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
		$Type_name=$type['Type']['name'];
		$category_name=$category[$type['Type']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['Type']['name']][$type['Group']['name']];
		}
		if($category_name=='credit_account')
		{
			$credit=$AccountHead['AccountHead']['opening_balance'];
			$debit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$credit=0;
				$debit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$AccountHead['AccountHead']['opening_balance'];
			$credit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		if(!in_array($Type_name, ['Expense','Income']))
		{
			$JournalCLosginDebit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array( 'total_amount'),
				));
			$JournalCLosginCredit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array('total_amount'),
				));
			$debit+=$JournalCLosginDebit['Journal']['total_amount'];
			$credit+=$JournalCLosginCredit['Journal']['total_amount'];
			$Journal_all[0]=array(
				'id'=>0,
				'date'=>date('d-m-Y',strtotime($from_date)),
				'Journal.branch_id'=>$branch_id,
				'strtotime'=>0,
				'mode'=>'Opening Balance',
				'executive'=>'',
				'voucher_no'=>'',
				'external_voucher'=>'',
				'remarks'=>'',
				'credit'=>floatval($credit),
				'debit'=>floatval($debit),
				);
		}
		else
		{
			if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
			{

			}
			else
			{
				$debit=0;
				$credit=0;
			}
		}
		if(!in_array($Type_name, ['Expense','Income']))
		{
			$Journal_voucher_no=$this->Journal->find('list',array(
				'conditions'=>array(
					'AND' => array(
						'OR' => array(
							'Journal.debit'=>$AccountHead['AccountHead']['id'],
							'Journal.credit'=>$AccountHead['AccountHead']['id'],
							),
						'AND' => array(
							'Journal.flag=1',
							'Journal.date between ? and ?'=>array($from_date,$to_date),
							'Journal.branch_id'=>$branch_id,
							)
						)
					),
				'fields'=>array(
					'Journal.id',
					'Journal.date',
					'Journal.voucher_no',
					),
				));
			foreach ($Journal_voucher_no as $voucher_no => $lists) {
				if(count($lists)>1)
				{
					$j=0;
					foreach ($lists as $journal_id => $date) {
						if($j)
						{
							if($date==$Journal_all[key($lists)]['date'] && $voucher_no==$Journal_all[key($lists)]['voucher_no'])
							{
								$Journal_all[key($lists)]['credit']+=$Journal_all[$journal_id]['credit'];
								$Journal_all[key($lists)]['debit']+=$Journal_all[$journal_id]['debit'];
								unset($Journal_all[$journal_id]);
								$GetJournal=$this->Journal->findById($Journal_all[key($lists)]['id'],['credit']);
								if($AccountHead['AccountHead']['id']==$GetJournal['Journal']['credit'])
								{
									$Journal_all[key($lists)]['credit']-=$Journal_all[key($lists)]['debit'];
									$Journal_all[key($lists)]['debit']=0;
									$Journal_all[key($lists)]['credit']=($Journal_all[key($lists)]['credit']);
								}
								else
								{
									$Journal_all[key($lists)]['debit']-=$Journal_all[key($lists)]['credit'];
									$Journal_all[key($lists)]['credit']=0;
									$Journal_all[key($lists)]['debit']=($Journal_all[key($lists)]['debit']);
								}
							}								
						}
						$j++;
					}
				}
			}
		}
		$date=[];
		foreach ($Journal_all as $i => $row) {
			$date[$i]  = $row['strtotime'];
		}
		array_multisort($date, SORT_DESC, $Journal_all);
		foreach ($Journal_all as $key => $value) 
		{
//pr($Journal_all);
			$return['row']['tbody'].='<tr class="blue-pddng">';
			$return['row']['tbody'].='<td class="color_label">'.date('d-m-Y',strtotime($value['date'])).'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['executive'].'</td>';
			if($value['voucher_no'])
						{
						$return['row']['tbody'].='<td><a class="fa fa-1x fa-print" target="_blank" href="'.$this->webroot.'Print/voucher_print/'.$value['id'].'"></a>&nbsp;&nbsp&nbsp;&nbsp<span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span></td>';
					   }
					   else
					   {
					   	$return['row']['tbody'].='<td style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no text-right">'.$value['voucher_no'].'</td>';
					   }
			$return['row']['tbody'].='<td class="color_label text-right">'.$value['external_voucher'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
			$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['debit'],3,'.','').'</td>';
			$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['credit'],3,'.','').'</td>';
			if($category_name=='credit_account') { $balance=$value['credit']-$value['debit']; }
			else { $balance=$value['debit']-$value['credit']; }
			$return['row']['tbody'].='<td class="color_label"></td>';
			$credit_Total+=$value['credit'];
			$debit_Total+=$value['debit'];
			$return['row']['tbody'].='<td><span class="journal_id" hidden>'.$value['id'].'</span>';
			$return['row']['tbody'].='<span><i class="fa fa-trash blue-col delete"></i></span>';
			$return['row']['tbody'].='</td>';
			$return['row']['tbody'].='</tr>';
		}
		$return['row']['tfoot']='';
		$return['row']['tfoot'].='<tr class="blue-pddng">';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td class="total_amount">Total</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.$debit_Total.'</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.$credit_Total.'</td>';
		$Type_name=$type['Type']['name'];
		$category_name=$category[$type['Type']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['Type']['name']][$type['Group']['name']];
		}
		if($category_name=='credit_account')
		{
			$Balance_Total=$credit_Total-$debit_Total;
		}
		else
		{
			$Balance_Total=$debit_Total-$credit_Total;	
		}
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,3,'.','').'</td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='</tr>';

		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function general_journal_transaction_by_voucher_no_ajax($voucher_no,$name)
{
	$AccountHead=$this->AccountHead->findByName(trim($name));
	$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					),
				'AND' => array(
					'Journal.flag=1',
					'Journal.voucher_no'=>$voucher_no,
					)
				)
			),
		'fields'=>array(
			'Journal.id',
			'Journal.remarks',
			'Journal.amount',
			'Journal.date',
			'Journal.voucher_no',
			'Journal.work_flow',
			'Journal.created_at',
			'AccountHeadCredit.name',
			'AccountHeadCredit.opening_balance',
			'AccountHeadDebit.name',
			'AccountHeadDebit.opening_balance',
			),
		));
	$category=[
	'Asset'=>'debit_account',
	'Expense'=>'debit_account',
	'Capital'=>array(
		'Capital'=>'credit_account',
		'Drawing'=>'debit_account',
		),
	'Income'=>'credit_account',
	'Liabilities'=>'credit_account',
	];
	$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
	$Type_name=$type['Type']['name'];
	$category_name=$category[$type['Type']['name']];
	$Journal_all=[];
	foreach ($Journal as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
		$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['work_flow']=$value['Journal']['work_flow'];
		$Journal_single['debit']=0;
		$Journal_single['credit']=0;
		if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
		{
			$Journal_single['mode']=$value['AccountHeadCredit']['name'];
			$Journal_single['debit']=$value['Journal']['amount'];
		}
		else
		{
			$Journal_single['mode']=$value['AccountHeadDebit']['name'];
			$Journal_single['credit']=$value['Journal']['amount'];
		}
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	$return['row']['tbody']='';
	$total_debit=0;
	$total_credit=0;
	foreach ($Journal_all as $key => $value) 
	{
		$total_credit+=$value['credit'];
		$total_debit+=$value['debit'];
		$return['row']['tbody'].='<tr class="blue-pddng">';
		$return['row']['tbody'].='<td class="color_label"><span style="display:none" class="journal_id">'.$value['id'].'</span><span>'.date('d-m-Y',strtotime($value['date'])).'</span></td>';
		$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
		$return['row']['tbody'].='<td class="color_label text-right">'.floatval($value['debit']).'</td>';
		$return['row']['tbody'].='<td class="color_label text-right">'.floatval($value['credit']).'</td>';
		$return['row']['tbody'].='<td class="color_label">'.$value['voucher_no'].'</td>';
		$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
		$return['row']['tbody'].='<td class="color_label">'.$value['work_flow'].'</td>';
	}
	$return['row']['tfoot']='';
	$return['row']['tfoot'].='<tr class="blue-pddng">';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td class="total_amount">Total</td>';
	$return['row']['tfoot'].='<td class="total_amount text-right">'.$total_debit.'</td>';
	$return['row']['tfoot'].='<td class="total_amount text-right">'.$total_credit.'</td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<tr class="blue-pddng">';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td class="total_amount">Balance</td>';
	if($Type_name=='Capital')
		$category_name=$category[$type['Type']['name']][$type['Group']['name']];
	if($category_name=='credit_account')
		$Balance_Total=$total_credit-$total_debit;
	else
		$Balance_Total=$total_debit-$total_credit;	
	if($category_name=='debit_account')
	{
		$return['row']['tfoot'].='<td class="total_amount text-right">'.($Balance_Total).'</td>';
		$return['row']['tfoot'].='<td></td>';
	}
	else
	{
		$return['row']['tfoot'].='<td></td>';	
		$return['row']['tfoot'].='<td class="total_amount text-right">'.($Balance_Total).'</td>';
	}
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	echo json_encode($return); exit;
}
public function General_SubGroup_add_function($name,$group_id)
{
	try {
		$SubGroup_data=[
		'name'=>strtoupper($name),
		'group_id'=>$group_id,
		'created_at'=>date('Y-m-d H:i:s'),
		'updated_at'=>date('Y-m-d H:i:s'),
		];
		$this->SubGroup->create();
		if(!$this->SubGroup->save($SubGroup_data))
		{
			$errors = $this->SubGroup->validationErrors;
			foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}
public function General_SubGroup_edit_function($name,$id)
{
	try {
		$this->SubGroup->id=$id;
		if(!$this->SubGroup->saveField('name',strtoupper($name)))
		{
			$errors = $this->SubGroup->validationErrors;
			foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}
public function expense_sub_category_add_ajax()
{
	$datasource_SubGroup = $this->SubGroup->getDataSource();
	try {
		$datasource_SubGroup->begin();
		$outstanding=20;
		$prepaid=5;
		$direct=12;
		$indirect=13;
		$data=$this->request->data;
		$name=trim($data['name']);
		if(!$name)
			throw new Exception("Empty name", 1);
		$group_id=$data['group_id'];
		if(!$group_id)
			throw new Exception("Empty group_id", 1);
		if($direct==$group_id || $indirect==$group_id)
		{
			$function_return=$this->General_SubGroup_add_function($name.' PAID',$group_id);
			if($function_return['result']!='Success')
				throw new Exception($function_return['result']);
			$SubGroup_id=$this->SubGroup->getLastInsertId();
			$SubGroup=$this->SubGroup->findById($SubGroup_id);
			$prepaid_name=$name.' PREPAID';
			$function_return=$this->General_SubGroup_add_function($prepaid_name,$prepaid);
			if($function_return['result']!='Success')
				throw new Exception($function_return['result']);
			$outstanding_name=$data['name'].' OUTSTANDING';
			$function_return=$this->General_SubGroup_add_function($outstanding_name,$outstanding);
			if($function_return['result']!='Success')
				throw new Exception($function_return['result']);
		}
		$datasource_SubGroup->commit();
		$return['result']='Success';
		$return['key']=$SubGroup['SubGroup']['id'];
		$return['value']=$SubGroup['SubGroup']['name'];
	} catch (Exception $e) {
		$datasource_SubGroup->rollback();
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function ExpenseAccountHeadGet_ajax($id)
{
	$AccountHead=$this->AccountHead->findById($id);
	$name=explode(' ',$AccountHead['AccountHead']['name']);
	array_pop($name);
	$expense_account_name=implode(' ', $name);
	$expense_subgroup_name=explode(' ',$AccountHead['SubGroup']['name']);
	$name=explode(' ',$AccountHead['AccountHead']['name']);
	array_pop($name);
	$expense_subgroup_name=implode(' ', $name);
	$outstanding_subgroup_name=$expense_subgroup_name.' OUTSTANDING';
	$outstanding_AccountHead_name=$expense_account_name.' OUTSTANDING';
	//$Outstanding_id=$this->GetAccountHeadIdByGroupNameAndAccountHeadName(20,$outstanding_subgroup_name,$outstanding_AccountHead_name);
	//$AccountHead_OUTSTANDING=$this->AccountHead->findById($Outstanding_id);			
	$prepaid_subgroup_name=$expense_subgroup_name.' PREPAID';
	$prepaid_AccountHead_name=$expense_account_name.' PREPAID';
	//$prepaid_id=$this->GetAccountHeadIdByGroupNameAndAccountHeadName(5,$prepaid_subgroup_name,$prepaid_AccountHead_name);
	//$AccountHead_prepaid=$this->AccountHead->findById($prepaid_id);
	// $AccountHead['AccountHead']['outstanding_opening_balance']=floatval($AccountHead_OUTSTANDING['AccountHead']['opening_balance']);
	// $AccountHead['AccountHead']['prepaid_opening_balance']=floatval($AccountHead_prepaid['AccountHead']['opening_balance']);
	// $AccountHead['AccountHead']['opening_balance']=floatval($AccountHead['AccountHead']['opening_balance']);
	$AccountHead['AccountHead']['outstanding_opening_balance']=0;
	$AccountHead['AccountHead']['prepaid_opening_balance']=0;
	$AccountHead['AccountHead']['opening_balance']=0;
	echo json_encode($AccountHead);
	exit;
}
public function expense_sub_category_edit_ajax()
{
	$datasource_SubGroup = $this->SubGroup->getDataSource();
	try {
		$datasource_SubGroup->begin();
		$outstanding=20;
		$prepaid=5;
		$direct=12;
		$indirect=13;
		$restricted_ids=['5','6','7','8'];
		$data=$this->request->data;
		$name=trim($data['name']);
		if(!$name)
			throw new Exception("Empty name", 1);
		$id=$data['id'];
		if(in_array($id, $restricted_ids))
			throw new Exception("You Cant Edit This Head", 1);
		if(!$id)
			throw new Exception("Empty id", 1);
		$old_Expense_SubGroup=$this->SubGroup->findById($id);
		$old_expense_name=explode(' ',$old_Expense_SubGroup['SubGroup']['name']);
		array_pop($old_expense_name);
		$old_expense_name=implode(' ', $old_expense_name);
		$function_return=$this->General_SubGroup_edit_function($name.' PAID',$id);
		if($function_return['result']!='Success')
			throw new Exception($function_return['result']);
		$old_prepaid_name=$old_expense_name.' PREPAID';
		$old_PrePaid_SubGroup=$this->SubGroup->findByName($old_prepaid_name);
		$prepaid_id=$old_PrePaid_SubGroup['SubGroup']['id'];
		$prepaid_name=$name.' PREPAID';
		$function_return=$this->General_SubGroup_edit_function($prepaid_name,$prepaid_id);
		if($function_return['result']!='Success')
			throw new Exception($function_return['result']);
		$old_outstanding_name=$old_expense_name.' OUTSTANDING';
		$old_Outstanding_SubGroup=$this->SubGroup->findByName($old_outstanding_name);
		$outstanding_id=$old_Outstanding_SubGroup['SubGroup']['id'];
		$outstanding_name=$name.' OUTSTANDING';
		$function_return=$this->General_SubGroup_edit_function($outstanding_name,$outstanding_id);
		if($function_return['result']!='Success')
			throw new Exception($function_return['result']);
		$datasource_SubGroup->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$datasource_SubGroup->rollback();
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function income_sub_category_add_ajax()
{
	$datasource_SubGroup = $this->SubGroup->getDataSource();
	try {
		$datasource_SubGroup->begin();
		$accrued=6;
		$direct=14;
		$indirect=15;
		$advance=21;
		$data=$this->request->data;
		$name=trim($data['name']);
		if(!$name)
			throw new Exception("Empty name", 1);
		$group_id=$data['group_id'];			
		if(!$group_id)
			throw new Exception("Empty group_id", 1);
		if($direct==$group_id || $indirect==$group_id)
		{
			$function_return=$this->General_SubGroup_add_function($name.' RECEIVED',$group_id);
			if($function_return['result']!='Success')
				throw new Exception($function_return['result']);
			$SubGroup_id=$this->SubGroup->getLastInsertId();
			$SubGroup=$this->SubGroup->findById($SubGroup_id);
			$prepaid_name=$name.' ADVANCE';
			$function_return=$this->General_SubGroup_add_function($prepaid_name,$advance);
			if($function_return['result']!='Success')
				throw new Exception($function_return['result']);
			$accrued_name=$data['name'].' ACCRUED';
			$function_return=$this->General_SubGroup_add_function($accrued_name,$accrued);
			if($function_return['result']!='Success')
				throw new Exception($function_return['result']);
		}
		$datasource_SubGroup->commit();
		$return['result']='Success';
		$return['key']=$SubGroup['SubGroup']['id'];
		$return['value']=$SubGroup['SubGroup']['name'];
	} catch (Exception $e) {
		$datasource_SubGroup->rollback();
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function income_sub_category_edit_ajax()
{
	$datasource_SubGroup = $this->SubGroup->getDataSource();
	try {
		$datasource_SubGroup->begin();
		$outstanding=20;
		$prepaid=5;
		$direct=12;
		$indirect=13;
		$restricted_ids=['9','10','11','12'];
		$data=$this->request->data;
		$name=trim($data['name']);
		if(!$name)
			throw new Exception("Empty name", 1);
		$id=$data['id'];
		if(in_array($id, $restricted_ids))
			throw new Exception("You Cant Edit This Head", 1);
		if(!$id)
			throw new Exception("Empty id", 1);
		$old_Income_SubGroup=$this->SubGroup->findById($id);
		$old_Income_name=$old_Income_SubGroup['SubGroup']['name'];
		$old_Income_name=explode(' ', $old_Income_name);
		$function_return=$this->General_SubGroup_edit_function($name.' RECEIVED',$id);
		if($function_return['result']!='Success')
			throw new Exception($function_return['result']);
		$old_ADVANCE_name=$old_Income_name[0].' ADVANCE';
		$old_ADVANCE_SubGroup=$this->SubGroup->findByName($old_ADVANCE_name);
		$ADVANCE_id=$old_ADVANCE_SubGroup['SubGroup']['id'];
		$ADVANCE_name=$name.' ADVANCE';
		$function_return=$this->General_SubGroup_edit_function($ADVANCE_name,$ADVANCE_id);
		if($function_return['result']!='Success')
			throw new Exception($function_return['result']);
		$old_ACCRUED_name=$old_Income_name[0].' ACCRUED';
		$old_ACCRUED_SubGroup=$this->SubGroup->findByName($old_ACCRUED_name);
		$ACCRUED_id=$old_ACCRUED_SubGroup['SubGroup']['id'];
		$ACCRUED_name=$name.' ACCRUED';
		$function_return=$this->General_SubGroup_edit_function($ACCRUED_name,$ACCRUED_id);
		if($function_return['result']!='Success')
			throw new Exception($function_return['result']);
		$datasource_SubGroup->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$datasource_SubGroup->rollback();
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function general_sub_category_edit_ajax()
{
	$datasource_SubGroup = $this->SubGroup->getDataSource();
	try {
		$datasource_SubGroup->begin();
		$data=$this->request->data;
		$name=trim($data['name']);
		if(!$name)
			throw new Exception("Empty name", 1);
		$id=$data['id'];
		if(!$id)
			throw new Exception("Empty id", 1);
		$function_return=$this->General_SubGroup_edit_function($name,$id);
		if($function_return['result']!='Success')
			throw new Exception($function_return['result']);
		$datasource_SubGroup->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$datasource_SubGroup->rollback();
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function General_journal_amount_edit_function()
{
	try {
		$data=$this->request->data;
		$amount=$data['amount'];
		$journal_id=$data['journal_id'];
		if(!$this->Journal->findById($journal_id))
			throw new Exception("Empty Journal", 1);
		$this->Journal->id=$journal_id;
		if(!$this->Journal->saveField('amount',$amount))
			throw new Exception("Error Processing Request", 1);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
//general end
public function Check_Bank($id)
{
	$return['status']=0;
	$BankDetail=$this->BankDetail->find('first',array(
		'conditions'=>array(
			'account_head_id'=>$id,
			),
		'fields'=>array(
			'BankDetail.currency',
			)
		));
	if(!empty($BankDetail)){
		if($BankDetail['BankDetail']['currency']!='INR'){
			$return['status']=1;
			$return['currency']=$BankDetail['BankDetail']['currency'];
		}
	}
	else{
		$return['status']=0;
	}
	echo json_encode($return);
	exit;
}
//
// Beacuse of gulf money customized genral journal ajax function
public function contra_journal_transaction_ajax()
{
	try {
		$data=$this->request->data;
		$name=$data['name'];
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$AccountHead=$this->AccountHead->findByName(trim($name));
		if(empty($AccountHead))
			throw new Exception("Empty AccountHead", 1);
		$Journal=$this->Journal->find('all',array(
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
						),
					'AND' => array(
						'Journal.flag=1',
						)
					)
				),
			));
		$Journal_credit=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.credit'=>$AccountHead['AccountHead']['id'],
				'Journal.flag=1',
				'Journal.date between ? and ?'=>array($from_date,$to_date),
				)
			));
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>[
		'Capital'=>'credit_account',
		'Drawing'=>'debit_account',
		],
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
		$Journal_all=[];
		foreach ($Journal_credit as $key => $value) 
		{
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['mode']=$value['AccountHeadDebit']['name'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['credit']=$value['Journal']['amount'];
			$Journal_single['debit']=0.00;
			$Journal_single['exchange_credit']= $value['Journal']['exchange_rate'];
			$Journal_single['exchange_debit'] = 0.00;
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		$Journal_debit=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.debit'=>$AccountHead['AccountHead']['id'],
				'Journal.flag=1',
				'Journal.date between ? and ?'=>array($from_date,$to_date),
				)
			));
		foreach ($Journal_debit as $key => $value) 
		{
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['mode']=$value['AccountHeadCredit']['name'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['credit']=0.00;
			$Journal_single['exchange_credit']= 0.00;
			$Journal_single['exchange_debit'] = $value['Journal']['exchange_rate'];
			$Journal_single['debit']=$value['Journal']['amount'];
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		krsort($Journal_all);
		$return['row']['tbody']='';
		$return['row']['tfoot']='';
		$Balance_Total=0;
		$credit_Total=0;
		$debit_Total=0;
		$exchange_credit_Total =0;
		$exchange_debit_Total =0;
		$Type_name=$type['Type']['name'];
		$category_name=$category[$type['Type']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['Type']['name']][$type['Group']['name']];
		}
		if($category_name=='credit_account')
		{
			$credit=$AccountHead['AccountHead']['opening_balance'];
			$debit='0';
		}
		else
		{
			$debit=$AccountHead['AccountHead']['opening_balance'];
			$credit='0';
		}
		$date=$AccountHead['AccountHead']['created_at'];
		$Journal_all[0]=array(
			'id'=>0,
			'date'=>date('d-m-Y',strtotime($date)),
			'mode'=>'Opening Balance',
			'voucher_no'=>'',
			'remarks'=>'',
			'credit'=>$credit,
			'debit'=>$debit,
			'exchange_credit'=>'',
			'exchange_debit'=>''
			);
		foreach ($Journal_all as $key => $value) 
		{
			$return['row']['tbody'].='<tr class="blue-pddng">';
			$return['row']['tbody'].='<td class="color_label"><span style="display:none" class="journal_id">'.$value['id'].'</span><span>'.date('d-m-Y',strtotime($value['date'])).'</span></td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['voucher_no'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['credit'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['debit'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['exchange_debit'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['exchange_credit'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['credit']-$value['debit'].'</td>';
			$return['row']['tbody'].='<td></td>';
			$exchange_credit_Total+=$value['exchange_credit'];
			$exchange_debit_Total+=$value['exchange_debit'];
			$credit_Total+=$value['credit'];
			$debit_Total+=$value['debit'];
			$return['row']['tbody'].='<td><i class="fa fa-pencil-square-o blue-col edit"></i></td>';
			$return['row']['tbody'].='<td><i class="fa fa-trash blue-col delete"></i></td>';
			$return['row']['tbody'].='</tr>';
		}
		$return['row']['tfoot']='';
		$return['row']['tfoot'].='<tr class="blue-pddng">';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td class="total_amount">Total</td>';
		$return['row']['tfoot'].='<td class="total_amount">'.$credit_Total.'</td>';
		$return['row']['tfoot'].='<td class="total_amount">'.$debit_Total.'</td>';
		$return['row']['tfoot'].='<td class="total_amount">'.$exchange_debit_Total.'</td>';
		$return['row']['tfoot'].='<td class="total_amount">'.$exchange_credit_Total.'</td>';
		$Type_name=$type['Type']['name'];
		$category_name=$category[$type['Type']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['Type']['name']][$type['Group']['name']];
		}
		if($category_name=='credit_account')
		{
			$Balance_Total=$credit_Total-$debit_Total;
		}
		else
		{
			$Balance_Total=$debit_Total-$credit_Total;	
		}
		$return['row']['tfoot'].='<td class="total_amount">'.$Balance_Total.'</td>';
		$return['row']['tfoot'].='<td></td><td></td></tr>';
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function VoucherPrint($voucher_no=null,$account_head=null){

	$Voucher_data=$this->Journal->find('all',array('fields'=>array('Journal.*','AccountHeadCredit.name','AccountHeadDebit.name'),'conditions'=>array('Journal.voucher_no'=>$voucher_no)));
//pr($Voucher_data);exit;
	require('fpdf/fpdf.php');
	$pdf = new FPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	function convert_number_to_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(0=> 'zero',1=> 'one',2=> 'two',3=> 'three',4=> 'four',5=> 'five',6=> 'six',7=> 'seven',8=> 'eight',9=> 'nine',10=> 'ten',11=> 'eleven',12=> 'twelve',13=> 'thirteen',14=> 'fourteen',15=> 'fifteen',16=> 'sixteen',17=> 'seventeen',18=> 'eighteen',19=> 'nineteen',20=> 'twenty',30=> 'thirty',40=> 'fourty',50=> 'fifty',60=> 'sixty',70=> 'seventy',80=> 'eighty',90=> 'ninety',100=> 'hundred',1000=> 'thousand',1000000=> 'million',1000000000=> 'billion',1000000000000=> 'trillion',1000000000000000=> 'quadrillion',1000000000000000000 => 'quintillion');
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}
	$image_line_width=10;
	$invoice_x_starting=$image_line_width;
	$pdf->SetFont('Arial','B',8);
	$pdf->SetTextColor(0,0,100);
	$pdf->AddPage();
	$pdf->rect(3, 3, 203, 142);
	$pdf->SetXY($invoice_x_starting, 10);
	$invoice_pos=-1;
	$pdf->SetFont('Arial','I',10);
	$pdf->Cell(350,$invoice_pos,'',0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos=10;
	$pdf->SetFont('Arial','B',14);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=14;
	$pdf->SetFont('Arial','B',12);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=16;
	$pdf->SetFont('Arial','',10);
	$pdf->rect(180,22, 23, 6);
	$pdf->Cell(347,$invoice_pos,'Date:'.'    '.date('d-m-Y'),0,0,'C');
	$pdf->rect(6,22, 28, 6);
	$invoice_pos-=14;
	$pdf->text(7,$invoice_pos,'P.V.No :');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=2;
	$x_dist=0;
	$x_dist+=90;
	$pdf->SetFont('Arial','B',14);
	$pdf->text( $x_dist,$invoice_pos,'VOUCHER');
	$gst_details_y_staring=25;
	$invoice_pos+=13;
	$single_array=array();
	$total=0;
	if($Voucher_data){
		foreach ($Voucher_data as $key => $value) {
			if($value['Journal']['credit'] != 20){
				$single_array['AccountHeadDebit']=$value['AccountHeadDebit']['name'];
				$single_array['AccountHeadCredit']=$value['AccountHeadCredit']['name'];
				$single_array['Invoice']=$value['Journal']['remarks'];
				if($value['Journal']['debit'] == 1){
					$sentence="Received From";
					$total+=$value['Journal']['amount'];
					$pdf->SetFont('Arial','',14);
					$x_dist=5;
					$pdf->text($x_dist,$invoice_pos,$sentence);
					$pdf->SetFont('Arial','I',14);
					$pdf->text(43,$invoice_pos,$single_array['AccountHeadDebit']);
					$pdf->Line(40, $gst_details_y_staring+17,200, $gst_details_y_staring+17);
					$pdf->SetFont('Arial','',14);
					$invoice_pos+=9;
					if($single_array['Invoice'] !=""){
						$pdf->text(5,$invoice_pos,'Against Invoice No');
						$single_array['Invoice']=substr($single_array['Invoice'],'17');
						$pdf->text(50,$invoice_pos, $single_array['Invoice']);
						$pdf->Line(47, $gst_details_y_staring+26,80, $gst_details_y_staring+26);
					}
				}else if($value['Journal']['debit'] == 5){
					$sentence="Payment to";
					$total+=$value['Journal']['amount'];
					$pdf->SetFont('Arial','',14);
					$x_dist=5;
					$pdf->text($x_dist,$invoice_pos,$sentence);
					$pdf->SetFont('Arial','I',14);
					$pdf->text(43,$invoice_pos,$single_array['AccountHeadDebit']);
					$pdf->Line(40, $gst_details_y_staring+17,200, $gst_details_y_staring+17);
					$pdf->SetFont('Arial','',14);
					$invoice_pos+=9;
					if($single_array['Invoice'] !=""){
						$pdf->text(5,$invoice_pos,'Against Invoice No');
						$single_array['Invoice']=substr($single_array['Invoice'],'17');
						$pdf->text(50,$invoice_pos, $single_array['Invoice']);
						$pdf->Line(47, $gst_details_y_staring+26,80, $gst_details_y_staring+26);
					}		
				}else if($value['Journal']['work_flow'] == 'Voucher General'){
					$sentence="Received From";
					$total+=$value['Journal']['amount'];
					$pdf->SetFont('Arial','',14);
					$x_dist=5;
					$pdf->text($x_dist,$invoice_pos,$sentence);
					$pdf->SetFont('Arial','I',14);
					$pdf->text(43,$invoice_pos,$single_array['AccountHeadCredit']);
					$pdf->Line(40, $gst_details_y_staring+17,200, $gst_details_y_staring+17);
					$pdf->SetFont('Arial','',14);
					$invoice_pos+=9;
					$pdf->text(5,$invoice_pos,'Payment to');
					$pdf->text(50,$invoice_pos, $single_array['AccountHeadDebit']);
					$pdf->Line(32, $gst_details_y_staring+26,200, $gst_details_y_staring+26);
				}
			}
		}
		$invoice_pos+=9;
		$pdf->text(5,$invoice_pos,'Rs');
		$pdf->text(18,$invoice_pos,strtoupper(convert_number_to_words($total)));
		$pdf->Line(12, $gst_details_y_staring+35,200, $gst_details_y_staring+35);
		$invoice_pos+=5;
		$pdf->rect(3,$invoice_pos , 203, 55 );
		$pdf->SetFont('Arial','',10);
		$invoice_pos+=10;
		$pdf->text(12,$invoice_pos,'ACCOUNT');
$pdf->Line(90, $gst_details_y_staring+39,90, $gst_details_y_staring+39+45+10); // vertical line
$pdf->text(105,$invoice_pos,'PARTICULARS');
$pdf->Line(150, $gst_details_y_staring+39,150, $gst_details_y_staring+39+65+5); // vertical line
$pdf->text(170,$invoice_pos,'AMOUNT');
$pdf->Line(3, $gst_details_y_staring+52,206, $gst_details_y_staring+52);
$invoice_pos+=5;
if($Voucher_data){
	foreach ($Voucher_data as $key => $value) {
		if($value['Journal']['credit'] != 20 ){
			if($value['Journal']['debit'] == 1 || $value['Journal']['debit'] == 5){
				$invoice_pos+=7;
				$pdf->text(12,$invoice_pos,$value['AccountHeadCredit']['name']);
				$pdf->text(105,$invoice_pos,'..........');
				$pdf->text(155,$invoice_pos,$value['Journal']['amount']);
			}else if($value['Journal']['work_flow'] == 'Voucher General'){

				$invoice_pos+=7;
				$pdf->text(12,$invoice_pos,$value['AccountHeadCredit']['name']);
				$pdf->text(105,$invoice_pos,'..........');
				$pdf->text(155,$invoice_pos,$value['Journal']['amount']);
			}
		}
	}
}
$footer_pos=0;
$footer_pos+=125;
$pdf->SetFont('Arial','B',14);
$pdf->text(130,$footer_pos,'TOTAL');
$pdf->text(169,$footer_pos, $total);
$pdf->Line(150, $gst_details_y_staring+109,206, $gst_details_y_staring+109);
$pdf->SetFont('Arial','',10);
$pdf->text(5,$footer_pos,'Manager');
$pdf->text(78,$footer_pos,'Verified');
$footer_pos+=8;
$pdf->text(5,$footer_pos,'Received');
$pdf->Line(25, $gst_details_y_staring+108,65, $gst_details_y_staring+108);
$pdf->text(88,$footer_pos,'Authorized');
$footer_pos+=6;
$pdf->text(88,$footer_pos,'Signature');
}
$pdf->Output();
exit;
}
public function ledger_print_ajax($account_id)
{

	try {
		$sub_group_id=3;
		$user_id=1;
		$salesOffline_array = array();
//$CustomerType=$this->CustomerType->findById($customer_type_id);
//$CustomerTypeName=$CustomerType['CustomerType']['name'];
// $Customers=$this->Customer->find('list',array(
// 	'joins'=>array(
// 		array(
// 			'table'=>'account_heads',
// 			'alias'=>'AccountHead',
// 			'type'=>'INNER',
// 			'conditions'=>array('AccountHead.id=Customer.account_head_id')
// 			),
// 		),
// 	'conditions'=>['Customer.customer_type_id'=>$CustomerType['CustomerType']['id']],
// 	'fields'=>['AccountHead.id','AccountHead.name'])
// );
		$selected_salesOffline=array();
		$Outstanding_customers=array();
//
// foreach ($Customers as $key => $value) 
// {
		$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_id);

		$Debit_N_Credit_function_total=$Debit_N_Credit_function['debit']-$Debit_N_Credit_function['credit'];

		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')), true);
		$Sales=$this->Sale->find('all');
		$Sales = $this->Sale->find('first', array(
			"joins" => array(
				array(
					"table" => 'customers',
					"alias" => 'Customer',
					"type" => 'inner',
					"conditions" => array('Customer.account_head_id=Sale.account_head_id'),
					),
// array(
// 	"table" => 'customer_types',
// 	"alias" => 'CustomerType',
// 	"type" => 'inner',
// 	"conditions" => array('CustomerType.id=Customer.customer_type_id'),
// 	),
				),
			'conditions' => array(
				'Sale.status' => 2,
				'Sale.account_head_id' =>$account_id,
				'Sale.flag=1'
				),
			'fields' => array(
				'Sale.invoice_no',
				'Sale.date_of_delivered',
// 'Sale.invoice_no',
// 'CustomerType.name',
// 'Customer.id',
				'Customer.account_head_id',
// 'Customer.customer_type_id',
				'AccountHead.name',
				)
			));	

// $AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
// $account_all=$this->CustomerTransactionListFucntion($AccountHeads);
// $balance_amount=$account_all[$key]['total']-$account_all[$key]['Recieved'];
		$AccountHeads = $this->AccountHead->find('first', array('conditions' => array('AccountHead.id' =>$account_id)));
		$ct_total=$AccountHeads['AccountHead']['opening_balance']+$Debit_N_Credit_function_total;

		if (!empty($Sales)) 
		{
//$selected_salesOffline['customer_type'] = $Sales['CustomerType']['name'];
			$selected_salesOffline['customer'] = $Sales['AccountHead']['name'];
			$selected_salesOffline['invoice_no'] = array();
//$selected_salesOffline['order_id'] = array();
			$selected_salesOffline['invoice_day'] = array();
			$selected_salesOffline['total'] = 0;
			$outstanding_amount=0;
			$Journal_voucher=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.credit'=>$account_id,
					'NOT' => array(
						'Journal.remarks LIKE' => 'Sale Invoice No :%',
						),
					'Journal.flag=1',
					),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
					),
				));

			$voucher_amount=0;
			$voucher_balance=0;
			foreach ($Journal_voucher as $key3 => $value_amount) {
				$voucher_amount+=$value_amount;
			}
			if($AccountHeads)
			{
				$outstanding_amount=$AccountHeads['AccountHead']['opening_balance'];
				$outstanding_amount_date=date('d-m-Y',strtotime($AccountHeads['AccountHead']['created_at']));

			}
			if($outstanding_amount>$voucher_amount)
			{
				$outstanding_amount-=$voucher_amount;
				$voucher_amount=0;
			}
			else
			{
				$voucher_amount-=$outstanding_amount;
				$outstanding_amount=0;

			}
			if($outstanding_amount>=1)
			{
				array_push($selected_salesOffline['invoice_day'],100);
				array_push($selected_salesOffline['invoice_no'], 'O/S');
			}

			$Journal_invoices=$this->Journal->find('list',array(
				'conditions'=>array(

					'Journal.debit'=>$account_id,
					'Journal.flag=1',
					),
				'group'=>array('Journal.remarks'),
				'fields'=>array(

					'Journal.remarks',
					'Journal.date',

					),
				));
			foreach ($Journal_invoices as $keySC2 => $valueSC2) {
				$split_remarks=explode(':', $keySC2);
				$invoice_from_remarks=$split_remarks[1];

				$Journal_credit=$this->Journal->find('list',array(
					'conditions'=>array(
						'Journal.remarks'=>'Sale Invoice No :'.$invoice_from_remarks,
						'Journal.credit'=>$account_id,
						'Journal.flag=1',
						),
					'fields'=>array(
						'Journal.id',
						'Journal.amount',
						),
					));

				$Journal_debit=$this->Journal->find('list',array(
					'conditions'=>array(
						'Journal.remarks'=>'Sale Invoice No :'.$invoice_from_remarks,
						'Journal.debit'=>$account_id,
						'Journal.flag=1',
						),
					'fields'=>array(
						'Journal.id',
						'Journal.amount',
						),
					));


				$recieved_amount=0;
				$debit_amount=0;

				foreach ($Journal_debit as $key1 => $value1_amount) {
					$debit_amount+=$value1_amount;
				}

				foreach ($Journal_credit as $key2 => $value2_amount) {
					$recieved_amount+=$value2_amount;
				}
				$invoice_check_amount=$debit_amount-$recieved_amount;


				if($voucher_amount)
				{
					if($invoice_check_amount<=$voucher_amount)
					{
						$voucher_balance=$invoice_check_amount;
						$invoice_check_amount=0;
						$voucher_amount-=$voucher_balance;
					}
					else
					{
						$invoice_check_amount-=$voucher_amount;
						$voucher_amount=0;
					}
				}



				if($invoice_check_amount>0)
				{

					array_push($selected_salesOffline['invoice_no'], $invoice_from_remarks);

					$date2 =date('d-m-Y',strtotime($valueSC2));

					$date1 = date("Y-m-d");
					$date2 = strtotime($date2);
					$date1 =  strtotime($date1);
					$dat=$date1-$date2;
					$diff_day=round($dat/86400);
					array_push($selected_salesOffline['invoice_day'],$diff_day);
// array_push($selected_salesOffline['order_id'], $Sales['Sale']['order_no']);

				}


			}




			if($ct_total>=1){
				$selected_salesOffline['total'] = number_format($ct_total, 2, '.', '');

				if($selected_salesOffline['total']>0 && $selected_salesOffline['total']!='-0')
				{
					array_push($salesOffline_array, $selected_salesOffline);

				}
			}
		}
// else
// {
// 	$CustomerType = $this->CustomerType->find('all', array(
// 		"joins" => array(
// 			array(
// 				"table" => 'customers',
// 				"alias" => 'Customer',
// 				"type" => 'left',
// 				"conditions" => array('Customer.customer_type_id=CustomerType.id'),
// 				),
// 			array(
// 				"table" => 'account_heads',
// 				"alias" => 'AccountHead',
// 				"type" => 'inner',
// 				"conditions" => array('AccountHead.id=Customer.account_head_id'),
// 				),
// 			),
// 		'conditions' => array(
// 			'Customer.account_head_id' => $account_id,
// 			),
// 		'fields' => array(
// 			'CustomerType.name',
// 			'AccountHead.name',
// 			'AccountHead.opening_balance',
// 			'AccountHead.created_at',

// 			)
// 		));
// 	$Journal_voucher=$this->Journal->find('list',array(
// 		'conditions'=>array(
// 			'Journal.credit'=>$account_id,
// 			'NOT' => array(
// 				'Journal.remarks LIKE' => 'Sale Invoice No :%',
// 				),
// 			'Journal.flag=1',
// 			),
// 		'fields'=>array(
// 			'Journal.id',
// 			'Journal.amount',
// 			),
// 		));

// 	$voucher_amount=0;
// 	$voucher_balance=0;
// 	foreach ($Journal_voucher as $key3 => $value_amount) {
// 		$voucher_amount+=$value_amount;
// 	}

// 	if(!empty($CustomerType))
// 	{
// 		// $Outstanding_customers['customer_type']=$CustomerType[0]['CustomerType']['name'];
// 		$Outstanding_customers['customer']=$CustomerType[0]['AccountHead']['name'];
// 		$Outstanding_customers['invoice_no']=array();
// 		$Outstanding_customers['invoice_day']=array();
// 		$Outstanding_customers['order_id']=array();
// 		$Outstanding_customers['total']='';
// 		$Outstanding_customers['total'] = $CustomerType[0]['AccountHead']['opening_balance'];
// 		$outstanding_amount_date=date('d-m-Y',strtotime($CustomerType[0]['AccountHead']['created_at']));
// 		if($Outstanding_customers['total']>$voucher_amount)
// 		{
// 			$Outstanding_customers['total']-=$voucher_amount;
// 			$voucher_amount=0;
// 		}
// 		else
// 		{
// 			$voucher_amount-=$Outstanding_customers['total'];
// 			$Outstanding_customers['total']=0;

// 		}
// 		if($Outstanding_customers['total']>=1)
// 		{
// 			array_push($Outstanding_customers['invoice_day'],100);
// 			array_push($Outstanding_customers['invoice_no'], 'O/S');
// 			array_push($salesOffline_array, $Outstanding_customers);
// 		}

// 	}
// }
// }


		require('fpdf/fpdf.php');
		$pdf = new FPDF('p', 'mm', [297, 210]);
		$x=8;
		$y=40;
		$Profile=$this->Global_Var_Profile['Profile'];

		function header_section($pdf,$salesOffline_array,$Profile) {
// pr($Profile);exit;
			$x=8;
			$y=40;
// $pdf->AddPage();
// $pdf->setFont("Arial",'B','14');           
// $pdf->Text(75, 10, $Profile['company_name']);
// $pdf->setFont("Arial",'B','9');    
// $pdf->Text(72, 15, $Profile['address_line_1']);
// $pdf->setFont("Arial",'B','9');    
// $pdf->Text(72, 19, $Profile['address_line_2']);
// $pdf->setFont("Arial",'B','9');  
// $pdf->Text(76, 23, $Profile['mail_address']);
			$pdf->AddPage();
//$pdf->Image('profile/'.$Profile['logo'],-5,5,-150);
//$pdf->Image('profile/'.$Profile['logo'],-5,5,-550);
			$pdf->Image('profile/'.$Profile['logo'],-2,2,-250);
			$image_line_width=40;
			$invoice_x_starting=$image_line_width;
$pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,0,100);
$pdf->rect(1, 1, 208, 295);

$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$pdf->Cell(0,$invoice_pos,'TAX INVOICE ',0,0,'C');
//$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
//$pdf->Cell(0,$invoice_pos,'Page : '.$page,0,0,'R');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=15;
$pdf->SetFont('Arial','B',10);
$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
//$pdf->text(165,30,$field_value);
$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
$pdf->SetFont('Arial','U',10);
$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
$pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring);

// $pdf->SetFont('Arial','B',12);
// $pdf->Text(65,30,"CUSTOMER TYPE:");
// $pdf->Text(103,30,$CustomerTypeName);
//$pdf->rect(5, 36, 200, 280);
$pdf->SetFont('Arial','',10);
$pdf->Text($x, $y+5, "Sl No");
$Date_line_x=30;
$pdf->Line($Date_line_x-10, $y-5,$Date_line_x-10, 296);
$pdf->Text($Date_line_x+5, $y+5, "Customer");
$AccountHead_line_x=$Date_line_x+65;
$pdf->Line($AccountHead_line_x-20+6, $y-5,$AccountHead_line_x-20+6, 296);
$pdf->Text($AccountHead_line_x-10, $y+5, "90 days");
$Remark_line_x=$AccountHead_line_x+50;
$pdf->Line($Remark_line_x-40, $y-5,$Remark_line_x-40, 296);
$pdf->Text($Remark_line_x-35, $y+5, "60 days");
$Credit_line_x=$Remark_line_x+20;
$Debit_line_x=$Credit_line_x-25;
$pdf->Line($Debit_line_x+1, $y-5,$Debit_line_x+1, 296);
$pdf->Text($Debit_line_x+5, $y+5, "30 days");
$Debit_line_x=$Credit_line_x+20;
$pdf->Line($Debit_line_x, $y-5,$Debit_line_x, 296);
$pdf->Text($Debit_line_x+2, $y+5, "Total");
$pdf->Line(5, $x+8+280, 205, $x+8+280); 
//$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring);
$pdf->Line(1, $y+8, 209, $y+8);
$pdf->SetFont('Arial','B',8);
}
$start_x=10;
$start_y=45;
header_section($pdf,$salesOffline_array,$Profile);
$i=0;
$id=0;

foreach ($salesOffline_array as $key => $value) {
	$id++;

	$array_invoice_day=$value['invoice_day'];
	$array_invoice_no=$value['invoice_no'];
// $invoice_count=count($array_invoice_no);
// pr($invoice_count);
// exit;
	$array_invoice=array_combine($array_invoice_no,$array_invoice_day);
//$invoice_count=count($array_invoice);
// pr($invoice_count);
//             exit;

	$pdf->SetFont('','',9);
	$pdf->Text($start_x, $start_y+($i+1)*8,$id);

	$pdf->Text($start_x+178, $start_y+($i+1)*8, $value['total']);
	$description = str_replace('"', "'", $value['customer']);

	$description_length=strlen($description);

	$description_words=explode(' ', $description);

	$word_count=count($description_words);

	$k=0;
	$poped_array=[];
	for ($j=0; $j <$word_count; $j++) {
		$customer_name=implode($description_words,' ');

		$poped_array[]=array_pop($description_words);

		$product_name_length=strlen($customer_name);

		if($product_name_length<=30 && $product_name_length!=0)
		{

			$pdf->Text($start_x+12, $start_y+($i+$k+1)*8, $customer_name);
			$description=trim(substr($description, $product_name_length,$description_length));
			$description_words=explode(' ', $description);
			$word_count=count($description_words);
			if($description_words)
			{
				$word_count+=1;
			}
			$poped_array=[];
			$k++;
			$j=0;  

		}
	}
	if($k){
		$k--;

	}
	$sixty_x_count=0;
	$sixty_loop=0;
	$ninety_loop=0;
	$thirty_x_count=0;
	$ninety_x_count=0;
	asort($array_invoice);
//pr($array_invoice);exit;
	foreach ($array_invoice as $keyin => $valuein) {


		if(90>$valuein && $valuein>30)
		{
//           $descriptioninvoice = str_replace('"', "'", $value['invoice_no']);

//   $descriptioninvoice_length=strlen($descriptioninvoice);

//   $descriptioninvoice_words=explode(' ', $descriptioninvoice);

//   $invoice_count=count($descriptioninvoice_words);
//             $poped_array=[];

//   for ($j=0; $j < $invoice_count; $j++) {
// $invoice_no=implode($descriptioninvoice_words,' ');

// $poped_array[]=array_pop($descriptioninvoice_words);

// $invoice_no_length=strlen($invoice_no);


			$sixty_x_count++;
			$pdf->Text($start_x+82+($sixty_x_count*13), ($start_y+(($sixty_loop)+1)*8),$keyin);
			if($sixty_x_count>2)
			{
				$sixty_x_count=0;
				$i++;
				$sixty_loop++;
				$ninety_loop++;


			}
		}
		else if($valuein<=30)
		{
			$thirty_x_count++;
			$pdf->Text($start_x+120+($thirty_x_count*12), ($start_y+(($i-$sixty_loop)+1)*8),$keyin);
			if($thirty_x_count>2)
			{
				$thirty_x_count=0;
				$i++;
				$ninety_loop++;
			}
		}
		else 
		{

			$ninety_x_count++;
			$pdf->Text($start_x+60+($ninety_x_count*12), ($start_y+8+($i-$ninety_loop)*8),$keyin);
			if($ninety_x_count>1)
			{
				$ninety_x_count=0;
				$i++;

			}
		}
	}
//}
	if($k>0)
	{
		$i++;
	}
	$i++;
// if($customer_type_id==14)
// {
// 	if($i>10)
// 	{
// 		$i=1;
// 		header_section($pdf,$salesOffline_array,$CustomerTypeName);
// 	}
// }
// else
// {
	if($i>28)
	{
		$i=1;
		header_section($pdf,$salesOffline_array,$Profile);
	}
// }



}
$pdf->Output();
exit;
$return['result']='Success';
} catch (Exception $e) {
	$return['result']=$e->getMessage();
}
return $return;
}
public function General_Journal_Openging_Debit_N_Credit_function($account_head_id,$from_date)
{	
	$account_single=[];
	$account_single['debit']=0;
	$account_single['credit']=0;
	$AccountHead=$this->AccountHead->findById($account_head_id);
	$account_single['name']=$AccountHead['AccountHead']['name'];
	$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
	$Journal_Debit=$this->Journal->find('first',array(
		'conditions'=>array(
			'debit'=>$account_head_id,
			'flag=1',
			'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
			),
		'fields'=>array( 'Journal.total_amount'),
		));
	$Journal_Credit=$this->Journal->find('first',array(
		'conditions'=>array(
			'credit'=>$account_head_id,
			'flag=1',
			'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
			),
		'fields'=>array( 'Journal.total_amount'),
		));
	$account_single['credit']=round($Journal_Credit['Journal']['total_amount']);
	$account_single['debit']=round($Journal_Debit['Journal']['total_amount']);
	return $account_single;
}
public function General_Journal_Opening_balance_Debit_N_Credit_With_Date_function($account_head_id,$date)
{
	$bank=2;
	$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
	$modes=['1'];
	foreach ($bank_list as $key => $value) {
		array_push($modes, $key);
	}
	$account_single=[];
	$account_single['debit']=0;
	$account_single['credit']=0;
	$Journal_Debit=$this->Journal->find('all',array('conditions'=>array(
// 'credit'=>$modes,
		'debit'=>$account_head_id,
		'flag=1',
		'date <'=>$date,
		)));
// pr($Journal_Debit); exit;
	foreach ($Journal_Debit as $key => $value) {
		$account_single['debit']+=$value['Journal']['amount'];
	}
	$Journal_Credit=$this->Journal->find('all',array('conditions'=>array(
// 'debit'=>$modes,
		'credit'=>$account_head_id,
		'flag=1',
		'date <'=>$date,
		)));
	foreach ($Journal_Credit as $key => $value) {
		$account_single['credit']+=$value['Journal']['amount'];
	}
	return $account_single;
}
public function GetAccountHeadIdByGroupNameAndAccountHeadName($group_id,$sub_group_name,$account_head_name)
{
	$sub_group_name=strtoupper($sub_group_name);
	$account_head_name=strtoupper($account_head_name);
	$AccountHead=$this->AccountHead->findByName($account_head_name);
	if(!$AccountHead)
	{
		$SubGroup=$this->SubGroup->findByName($sub_group_name);
		if(!$SubGroup)
		{
			$function_return=$this->General_SubGroup_add_function($sub_group_name,$group_id);
			if($function_return['result']!='Success')
				throw new Exception($function_return['result']);
			$sub_group_id=$this->SubGroup->getLastInsertId();
		}
		else
		{
			$sub_group_id=$SubGroup['SubGroup']['id'];
		}
		$opening_balance=0;
		$date=date('Y-m-d');
		$description='';
		$function_return=$this->AccountHeadCreate($sub_group_id,$account_head_name,$opening_balance,$date,$description);
		if($function_return['result']!='Success')
			throw new Exception($function_return['message']);
		$AccountHead_id=$this->AccountHead->getLastInsertId();
	}
	else
	{
		$AccountHead_id=$AccountHead['AccountHead']['id'];
	}
	return $AccountHead_id;
}
public function design(){
}
public function UpdateOpeningBalance()
{
	$return['result']='Error';
	$data=$this->data;
	$created_at=date('Y-m-d',strtotime($data['created_at']));
	$opening_balance=$data['opening_balance'];
	$id=$data['id'];
	try {
		$this->AccountHead->id=$id;
		if(!$this->AccountHead->saveField('opening_balance',$opening_balance))
			throw new Exception("Error Processing Request in opening balance updation", 1);
		if(!$this->AccountHead->saveField('created_at',$created_at))
			throw new Exception("Error Processing Request in created_at updation", 1);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();		
	}
	echo json_encode($return);
	exit;
}
public function route_code()
{
	$Route = $this->Route->find('all', array(
		));
	foreach ($Route as $key => $value) {
# code...

		$cashname = $value['Route']['name'].$value['Route']['code'].'_CASH';
		$sub_group_id =1;
		$name = $cashname;
		$opening_balance = 0;
		$date = date('Y-m-d');
		$description = "";
		$function_return=$this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
//$function_return = $AccountingsController->AccountHeadCreate($sub_group_id, $name, $opening_balance, $date, $description);
		if ($function_return['result'] != 'Success')
			throw new Exception($function_return['message']);
		$CashAccountHead_id = $this->AccountHead->getLastInsertId();
		$this->Route->id=$value['Route']['id'];
		$this->Route->saveField('account_head_id',$CashAccountHead_id);
	}
	exit;
}
public function route_code_update()
{
	$Route = $this->Route->find('all', array(
		));
	foreach ($Route as $key => $value) {

		$name = $value['Route']['name'].$value['Route']['code'].'_CASH';
		$code='R'.$value['Route']['code'];
		$AccountHead=$this->AccountHead->findByName($name);
		if($AccountHead)
		{
		$cashname = $value['Route']['name'].'_CASH';
       $this->AccountHead->id=$AccountHead['AccountHead']['id'];
		$this->AccountHead->saveField('name',$cashname);
				}
		$this->Route->id=$value['Route']['id'];
		$this->Route->saveField('code',$code);
	}
	exit;
}
public function get_map($id)
{
	$conditions=array();
	$conditions['Customer.account_head_id']=$id;
	$Customer=$this->Customer->find('all',array(
		'conditions'=>$conditions,
//'order'=>array('AccountHead.name ASC'),
// 'limit'=>110,
		));
	$return['latitude']=$Customer[0]['Customer']['latitude'];
	$return['longitude']=$Customer[0]['Customer']['longitude'];
	echo json_encode($return);
	exit;
}
public function Customer_print_ajax()
{
	try {
		$data=$this->request->data;
		$account_head_name=$data['account_head_name'];
		$AccountHead=$this->AccountHead->findByName($account_head_name);
		if(!$AccountHead)
			throw new Exception("Empty AccountHead", 1);
		$return['result']='Success';
		$return['id']=$AccountHead['AccountHead']['id'];
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function CustomerStatement($id,$from_date,$to_date)
{

	$user_branch_id=$this->Session->read('User.branch_id');
	$branch_id="";
	if($user_branch_id)
	{
		$branch_id=$user_branch_id;
	}
	$data['name']='SUJITH_IMPREST. PREPAID';
	$data['name']='RACY SANITARYWARES';
	$data['from_date']='01-05-2010';
	$data['to_date']='15-05-2019';
// $data=$this->request->data;

	$from_date=date('Y-m-d',strtotime($from_date));
	$to_date=date('Y-m-d',strtotime($to_date));
	$AccountHead=$this->AccountHead->findById($id);
	$Customer=$this->Customer->findByAccountHeadId($id);
	if(empty($AccountHead))
		throw new Exception("Empty AccountHead", 1);
	$this->Journal->virtualFields = array(
		'total_amount' => "SUM(Journal.amount)",
		);
	$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
					),
				'AND' => array(
					'Journal.flag=1',
					'Journal.date between ? and ?'=>array($from_date,$to_date),
					'Journal.branch_id'=>$branch_id,
					)
				)
			),
		'order'=>array('Journal.date ASC'),
		'fields'=>array(
			'Journal.id',
			'Journal.remarks',
			'Journal.amount',
			'Journal.date',
			'Journal.voucher_no',
			'Journal.external_voucher',
			'Journal.work_flow',
			'Journal.created_at',
			'AccountHeadCredit.name',
			'AccountHeadCredit.opening_balance',
			'AccountHeadDebit.name',
			'AccountHeadDebit.opening_balance',

			),
		));
	$cheque=$this->Cheque->find('all',array(
		'conditions'=>array(
					'Cheque.account_head_id'=>$AccountHead['AccountHead']['id'],
					'Cheque.status'=>array(0,4),
			),
		'order'=>array('Cheque.id ASC'),
		'fields'=>array(
			'Cheque.*',
			'AccountHead.name',
			'AccountHead.id',
			
			),
		));
	$category=[
	'Asset'=>'debit_account',
	'Expense'=>'debit_account',
	'Capital'=>array(
		'Capital'=>'credit_account',
		'Drawing'=>'debit_account',
		),
	'Income'=>'credit_account',
	'Liabilities'=>'credit_account',
	];
	$Journal_all=[];
	foreach ($Journal as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
		$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['external_voucher']=$value['Journal']['external_voucher'];
		if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
		{
			$Journal_single['mode']=$value['AccountHeadCredit']['name'];
			$Journal_single['debit']=$value['Journal']['amount'];
			$Journal_single['credit']=0.00;
		}
		else
		{
			$Journal_single['mode']=$value['AccountHeadDebit']['name'];
			$Journal_single['credit']=$value['Journal']['amount'];
			$Journal_single['debit']=0.00;
		}
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	$return['row']['tbody']='';
	$return['row']['tfoot']='';
	$Balance_Total=0;
	$credit_Total=0;
	$debit_Total=0;
	$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
	$Type_name=$type['Type']['name'];
	$category_name=$category[$type['Type']['name']];
	if($Type_name=='Capital')
	{
		$category_name=$category[$type['Type']['name']][$type['Group']['name']];
	}
	if($category_name=='credit_account')
	{
		$credit=$AccountHead['AccountHead']['opening_balance'];
		$debit='0';
		if($AccountHead['AccountHead']['opening_balance']<0)
		{
			$credit=0;
			$debit=$AccountHead['AccountHead']['opening_balance']*-1;
		}
	}
	else
	{
		$debit=$AccountHead['AccountHead']['opening_balance'];
		$credit='0';
		if($AccountHead['AccountHead']['opening_balance']<0)
		{
			$debit=0;
			$credit=$AccountHead['AccountHead']['opening_balance']*-1;
		}
	}
	if(!in_array($Type_name, ['Expense','Income']))
	{
		$JournalCLosginDebit=$this->Journal->find('first',array(
			'conditions'=>array(
				'Journal.debit'=>$AccountHead['AccountHead']['id'],
				'Journal.flag=1',
				'Journal.date <'=>$from_date,
				'Journal.branch_id'=>$branch_id,
				),
			'fields'=>array( 'total_amount'),
			));
		$JournalCLosginCredit=$this->Journal->find('first',array(
			'conditions'=>array(
				'Journal.credit'=>$AccountHead['AccountHead']['id'],
				'Journal.flag=1',
				'Journal.date <'=>$from_date,
				'Journal.branch_id'=>$branch_id,
				),
			'fields'=>array('total_amount'),
			));
		$debit+=$JournalCLosginDebit['Journal']['total_amount'];
		$credit+=$JournalCLosginCredit['Journal']['total_amount'];
		$Journal_all[0]=array(
			'id'=>0,
			'date'=>date('d-m-Y',strtotime($from_date)),
			'Journal.branch_id'=>$branch_id,
			'strtotime'=>0,
			'mode'=>'Opening Balance',
			'voucher_no'=>'',
			'external_voucher'=>'',
			'remarks'=>'',
			'credit'=>floatval($credit),
			'debit'=>floatval($debit),
			);
	}
	else
	{
		if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
		{

		}
		else
		{
			$debit=0;
			$credit=0;
		}
	}
	if(!in_array($Type_name, ['Expense','Income']))
	{
		$Journal_voucher_no=$this->Journal->find('list',array(
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
						),
					'AND' => array(
						'Journal.flag=1',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'fields'=>array(
				'Journal.id',
				'Journal.date',
				'Journal.voucher_no',
				),
			));
		foreach ($Journal_voucher_no as $voucher_no => $lists) {
			if(count($lists)>1)
			{
				$j=0;
				foreach ($lists as $journal_id => $date) {
					if($j)
					{
						if($date==$Journal_all[key($lists)]['date'] && $voucher_no==$Journal_all[key($lists)]['voucher_no'])
						{
							$Journal_all[key($lists)]['credit']+=$Journal_all[$journal_id]['credit'];
							$Journal_all[key($lists)]['debit']+=$Journal_all[$journal_id]['debit'];
							unset($Journal_all[$journal_id]);
							$GetJournal=$this->Journal->findById($Journal_all[key($lists)]['id'],['credit']);
							if($AccountHead['AccountHead']['id']==$GetJournal['Journal']['credit'])
							{
								$Journal_all[key($lists)]['credit']-=$Journal_all[key($lists)]['debit'];
								$Journal_all[key($lists)]['debit']=0;
								$Journal_all[key($lists)]['credit']=number_format(($Journal_all[key($lists)]['credit']),3,'.','');
							}
							else
							{
								$Journal_all[key($lists)]['debit']-=$Journal_all[key($lists)]['credit'];
								$Journal_all[key($lists)]['credit']=0;
								$Journal_all[key($lists)]['debit']=number_format(($Journal_all[key($lists)]['debit']),3,'.','');
							}
						}								
					}
					$j++;
				}
			}
		}
	}
	
	$date=[];
	foreach ($Journal_all as $i => $row) {
		$date[$i]  = $row['strtotime'];
	}

	array_multisort($date, SORT_ASC, $Journal_all);
	if(!empty($cheque))
	{
	foreach ($cheque as $key => $value_cheque) 
	{
		$Journal_single['id']=$value_cheque['Cheque']['id'];
		$Journal_single['date']=$value_cheque['Cheque']['cheque_date'];
		$Journal_single['strtotime']=strtotime($value_cheque['Cheque']['cheque_date'])+$value_cheque['Cheque']['id'];
		$Journal_single['voucher_no']=$value_cheque['Cheque']['cheque_no'];
		$Journal_single['remarks']="Post Dated Cheque";
		$Journal_single['external_voucher']=$value_cheque['Cheque']['cheque_no'];
		$Journal_single['mode']="CHEQUE";
		$Journal_single['credit']=$value_cheque['Cheque']['cheque_amount'];
		$Journal_single['debit']=0.00;
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
   }
	$conditions_total=[];

	$conditions_total['Customer.account_head_id']=$id;

	$Customer_name=$this->Customer->find('first',array(
		'conditions'=>$conditions_total,
		'order' => array('Customer.id' => 'ASC'),
		'fields'=>array(
			'Customer.account_head_id',
			'AccountHead.opening_balance',
			'AccountHead.name',
			'CustomerType.name',
			'AccountHead.id',

			),
		));
	$from_date=date('Y-m-d',strtotime($from_date));
	$to_date=date('Y-m-d',strtotime($to_date));
	$balance=0;
	$Balance_Total=0;
	$invoice_array=array();
	$account_id=$Customer_name['AccountHead']['id'];
	$Sales = $this->Sale->find('all', array(
		'conditions' => array(
			'Sale.account_head_id' =>$account_id,
//'Sale.date_of_delivered between ? and ?'=>array($from_date,$to_date),

			'Sale.flag'=>1,
			'Sale.status'=>array(2,3),
			),
		'order' => array('Sale.date_of_delivered' => 'ASC'),
		'fields' => array(
			'Sale.id',
			'Sale.date_of_delivered',
			'Sale.invoice_no',
			'Sale.account_head_id',
			'Sale.executive_id',
			'Sale.grand_total',
			'AccountHead.name',
			'Executive.account_head_id',

			)
		)
	);
	$voucher_amount=0;

	$Journal_voucher=$this->Journal->find('list',array(
		'conditions'=>array(
			'Journal.credit'=>$account_id,

//'Journal.debit'=>$value['Executive']['account_head_id'],
//'NOT' => array(
//'Journal.remarks'=>'Sale Invoice No :'.$value3['Sale']['invoice_no'],
//),
//	'Journal.date between ? and ?'=>array($from_date,$to_date),
			'Journal.flag=1',
			),
		'fields'=>array(
			'Journal.id',
			'Journal.amount',
			),
		));

	foreach ($Journal_voucher as $key3 => $value_amount) {
		$voucher_amount+=$value_amount;
	}
	$this->Journal->virtualFields = array('credit_amount' => "SUM(Journal.amount)");
			$Journal_debit=$this->Journal->find('first',array(
				'conditions'=>array(
					//'AccountHeadCredit.sub_group_id'=>3,
					'Journal.debit'=>$account_id,
					'NOT' => array(
						'Journal.remarks LIKE' => 'Sale Invoice No :%',
					),
					'Journal.flag=1',
				),
				'fields'=>array(
					'Journal.credit_amount',
				),
			));
			//pr($Journal_debit);
			$voucher_amount=$voucher_amount-$Journal_debit['Journal']['credit_amount'];
	if(!empty($cheque))
	{
   foreach ($cheque as $key => $value_cheque) 
	{
		$voucher_amount+=$value_cheque['Cheque']['cheque_amount'];
	}
    }
	$voucher_balance=0;
	foreach ($Sales as $keyj =>$valuej)
	{
		$SaleItem=$this->SaleItem->find('list',array(
			'conditions'=>array(
				'SaleItem.sale_id'=>$valuej['Sale']['id'],
				),
			'fields'=>array(
				)
			));
	}
	$invoice_array_single=array();
	if (!empty($Sales)) {


		$invoice_array_single['party_name'] = $Customer_name['AccountHead']['name'];
		$invoice_array_single['invoice_no'] = array();
		$invoice_array_single['balance'] = array();
		$invoice_array_single['delivered_date'] = array();
		$AccountHead=$this->AccountHead->findById($Customer_name['AccountHead']['id']);
		if($AccountHead)
		{
			$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
			$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
		}
		if($outstanding_amount>$voucher_amount)
		{
			$outstanding_amount-=$voucher_amount;
			$voucher_amount=0;
		}
		else
		{
			$voucher_amount-=$outstanding_amount;
			$outstanding_amount=0;
		}
		if($outstanding_amount>=1)
		{
			array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
			array_push($invoice_array_single['invoice_no'], 'Opening Balance');
			array_push($invoice_array_single['balance'],$outstanding_amount);

		}
		foreach ($Sales as $keySC2 => $valueSC2) {
			$Journal=$this->Journal->find('all',array(
				'conditions'=>array(
					'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
					'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
					'Journal.debit=1',
					'Journal.flag=1',
					),
				));
			$balance_amount=$valueSC2['Sale']['grand_total'];
			$paid_amount=0;
			if($voucher_amount)
			{
				if($balance_amount<=$voucher_amount)
				{
					$voucher_balance=$balance_amount;
					$balance_amount=0;
					$voucher_amount-=$voucher_balance;
				}
				else
				{
					$balance_amount-=$voucher_amount;
					$voucher_amount=0;
				}
			}
			if($valueSC2['Sale']['grand_total']>0)
			{
				if($balance_amount){
//if($valueSC2['Sale']['account_head_id']==$data['customer_id'])
//{
					$check_date=$valueSC2['Sale']['date_of_delivered'];


					array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Sale']['date_of_delivered'])));
					array_push($invoice_array_single['invoice_no'], $valueSC2['Sale']['invoice_no']);
					array_push($invoice_array_single['balance'],$balance_amount) ;  

//}
				}
			}
		}
		
		if(count($invoice_array_single['invoice_no']))
		{
			array_push($invoice_array, $invoice_array_single);  
		}      

	}
	else
	{

		$invoice_array_single['party_name'] = $Customer_name['AccountHead']['name'];
//$invoice_array_single['Executive_name'] = $value['Executive']['name'];
		$invoice_array_single['invoice_no'] = array();
		$invoice_array_single['balance'] = array();
		$invoice_array_single['delivered_date'] = array();
		$AccountHead=$this->AccountHead->findById($Customer_name['AccountHead']['id']);
		if($AccountHead)
		{
			$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
			$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
		}
		if($outstanding_amount>$voucher_amount)
		{
			$outstanding_amount-=$voucher_amount;
			$voucher_amount=0;
		}
		else
		{
			$voucher_amount-=$outstanding_amount;
			$outstanding_amount=0;
		}
		if($outstanding_amount>=1)
		{

			array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
			array_push($invoice_array_single['invoice_no'], 'Opening Balance');
			array_push($invoice_array_single['balance'],$outstanding_amount);

		}
		if(count($invoice_array_single['invoice_no']))
		{
			array_push($invoice_array, $invoice_array_single);  
		}      
	}
	$Checkstate=0;
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	$igst_x_position=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 


	function header_section($pdf,$Journal_all,$Profile,$total_page,$page,$Checkstate,$igst_x_position,$Customer,$data_total) 
	{
		$pdf->AddPage();
		if(!empty($Profile['logo']))
			$pdf->Image('profile/'.$Profile['logo'],3,8,40);
		$image_line_width=45;
		$invoice_x_starting=$image_line_width;
//$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(0,0,0);
		$pdf->rect(3, 9, 203,280);
		$item_table_head_y_start=10;
		$pdf->SetDrawColor('150','150','150');
		$pdf->Line($image_line_width, $item_table_head_y_start,$image_line_width, $item_table_head_y_start+40);
		$invoice_pos=0;
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);


		$invoice_pos+=2;
		$pdf->text(45,$invoice_pos+3,$Profile['company_name']);
		$pdf->text(138,$invoice_pos+3,$Profile['company_name_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
		$pdf->text(70,$invoice_pos+3,$Profile['address_line_1']);
		$pdf->text(140,$invoice_pos+3,$Profile['address_line_1_arabic'] );

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(46,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
		$pdf->text(143,$invoice_pos+3,$Profile['address_line_2_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('times', 'U', 10, "", 'false');
		$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(78,$invoice_pos,'VAT Number : '.$Profile['vat_code']);
		$VAT_Number='ظريبه الشراء';
		$pdf->text(140,$invoice_pos,$VAT_Number);
		$gst_details_y_staring=50;
$pdf->Line(3, $gst_details_y_staring,206, $gst_details_y_staring); // horizontal line
$pdf->rect(70, $gst_details_y_staring+5, 55,10);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(75,$invoice_pos+18,"Customer Statement");
$pdf->text(10,$invoice_pos+33,"Customer :");$pdf->text(40,$invoice_pos+33,$Customer['Customer']['code']." ".$Customer['AccountHead']['name']);
$pdf->Line(3, $gst_details_y_staring+40,206, $gst_details_y_staring+40);
$Row_length=95;
$table_length=130;
$table_column=21;
$pdf->SetFont('dejavusans', '', 8);
$pdf->text(3,$Row_length,'Transaction');
$pdf->text(5,$Row_length+5,'Date');
$pdf->Line($table_column, 90,$table_column, $table_length+130); // vertical line
$pdf->text(25,$Row_length,'Document');
$pdf->text(25,$Row_length+5,'Reference');
$pdf->Line($table_column+55, 90,$table_column+55, $table_length+130); // vertical line
$pdf->text(79,$Row_length,'Sub');
$pdf->text(76,$Row_length+5,'Reference');
$pdf->Line($table_column+73, 90,$table_column+73, $table_length+130); // vertical line
$pdf->text(120,$Row_length,'Comments');
$pdf->Line($table_column+140, 90,$table_column+140, $table_length+130); // vertical line
$pdf->text(165,$Row_length,'Debit');
$pdf->Line($table_column+155, 90,$table_column+155, $table_length+130); // vertical line
$pdf->text(180,$Row_length,'Credit');
$pdf->Line($table_column+170, 90,$table_column+170, $table_length+130); // vertical line
$pdf->text(191,$Row_length,'Balance');
$pdf->Line(3, $gst_details_y_staring+55,206, $gst_details_y_staring+55);
$pdf->Line(3, $gst_details_y_staring+$table_length+70,206, $gst_details_y_staring+$table_length+70);
$pdf->Line(3, $gst_details_y_staring+$table_length+80,206, $gst_details_y_staring+$table_length+80);


$pdf->SetFont('times', '', 8, "", 'false');
}
function footer($pdf,$data_total,$invoice_array){
	$footer_start_y=252;
	$pdf->SetFont('times', 'B', 8, "", 'false');
	$pdf->text(95,$footer_start_y,'Closing Balance As of');
	$pdf->text(98,$footer_start_y+3,date('d-m-Y'));

	$pdf->text(161,$footer_start_y,$data_total['total_debit']);
	$pdf->text(176,$footer_start_y,$data_total['total_credit']);
	$pdf->text(192,$footer_start_y,number_format(($data_total['total_debit']-$data_total['total_credit']),2,'.',''));
	$pdf->rect(80, $footer_start_y+13, 126,15);
	$pdf->text(83,$footer_start_y+15,"1-30 Days");
$pdf->Line(105,280,105, $footer_start_y+13); // vertical line
$pdf->text(108,$footer_start_y+15,"31-60 Days");
$pdf->Line(128,280,128, $footer_start_y+13); // vertical line

$pdf->text(130,$footer_start_y+15,"61-90 Days");
$pdf->Line(153,280,153, $footer_start_y+13); // vertical line
$pdf->text(155,$footer_start_y+15,"91-180 Days");
$pdf->Line(183,280,183, $footer_start_y+13); // vertical line
$pdf->text(185,$footer_start_y+15,"Over 180 Days");
$pdf->Line(80, $footer_start_y+20,206, $footer_start_y+20);

$q=0;$w=0;$p=0;$r=0;$t=0;$y=0;
if($invoice_array)
{
	foreach($invoice_array as $key=>$value) {
		$party_name=$value['party_name'];
		$inner_array_count=count($value['invoice_no']);
		$a1=array();
		$b1=array();
		$d1=array();
		$c1=array();
		$f1=array();
		$g=array();
		$m=array();
		foreach ($value['delivered_date'] as $key=>$value1)
		{
			$delivered_date=$value1;
			$now = time();
			$expected_days_diff=$now-strtotime($delivered_date);
			$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
			array_push($m,$diff_day);
		}
		foreach($value['invoice_no'] as $key=>$value2)
		{
			$invoice_no=$value2;

		}
		$new_balance=0;

		foreach ($value['balance'] as $key=>$value3)
		{
			$balance=$value3;
			array_push($g,$balance);
			$new_balance+=$balance;
		}
		for($i=0;$i<count($m);$i++)
		{

			if($m[$i]<=30){ $a=$g[$i];array_push($a1,$a);}
			if($m[$i]<=60 && $m[$i]>30){$b=$g[$i];array_push($b1,$b);}
			if($m[$i]<=90 && $m[$i]>60){$c=$g[$i];array_push($c1,$c);}
			if($m[$i]<=180 && $m[$i]>90){$d=$g[$i];array_push($d1,$d);}
			if($m[$i]>180){$f=$g[$i];array_push($f1,$f);}
		}

		$b=array_sum($a1);if($b!=0){$b11=ROUND($b,3);}else{$b11='';}
		$b5=array_sum($b1);if($b5!=0){$b51=ROUND($b5,3);}else{$b51='';}
		$b19=array_sum($c1);if($b19!=0){$b12=ROUND($b19,3);}else{$b12='';}	
		$b3=array_sum($d1);if($b3!=0){$b13=ROUND($b3,3);}else{$b13='';}			
		$b4=array_sum($f1);if($b4!=0){$b41=ROUND($b4,3);}else{$b41='';}
	}
	$q+=$b11;$w+=$b51;$p+=$b12;$y+=$b41;
	$pdf->text(83,273,$b11);$pdf->text(108,273,$b51);$pdf->text(130,273,$b12);$pdf->text(155,273,$b13);$pdf->text(185,273,$b41);
}
//$pdf->Line(4, $footer_start_y,206, $footer_start_y);
$invoce_word_width=4;
$grand_total_length=30;



}

$balance=0;$total_debit=0;$total_credit=0;$total_balance=0;
$data_total=array(
	'total_debit'=>$total_debit,
	'total_credit'=>$total_credit,
	);

header_section($pdf,$Journal_all,$Profile,$total_page,$page,$Checkstate,$igst_x_position,$Customer,$data_total);
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+85;
foreach ($Journal_all as $key => $value1) {
	$data_total['total_debit']+=floatval($value1['debit']);
	$data_total['total_credit']+=floatval($value1['credit']);
}
foreach($Journal_all as $key=>$value) {
	if($value['mode']!="CHEQUE")
	{
	$sl_no_v_x=4;
	$pdf->SetFont('times', '', 8, "", 'false');
	$pdf->Text($sl_no_v_x, $first_table_x+11+(9*$i), date('d-m-Y',strtotime($value['date'])));
	$mode_name_v_x=$sl_no_v_x+18;
	$pdf->Text($mode_name_v_x, $first_table_x+11+(9*$i), $value['mode']);
	$Sub_refernce=$mode_name_v_x+55;
	$pdf->Text($Sub_refernce, $first_table_x+11+(9*$i), $value['voucher_no']);
	$pdf->Text($Sub_refernce+18, $first_table_x+11+(9*$i), $value['remarks']);
	$debit=$Sub_refernce+85;
	if($value['debit']==0)
	{
		$debit_simple="";
	}
	else
	{
		$debit_simple=floatval($value['debit']);
	}
	if($value['credit']==0)
	{
		$credit_simple="";
	}
	else
	{
		$credit_simple=floatval($value['credit']);
	}
	$pdf->Text($debit, $first_table_x+11+(9*$i),$debit_simple);
	$credit=$debit+9;
	$pdf->Text($credit+5, $first_table_x+11+(9*$i), $credit_simple);
	$balance_x=$credit+20;
	$balance=$balance+$value['debit']-$value['credit'];
	if($balance==0)
	{
		$balance_simple="";
	}
	else
	{
		//$balance_simple=ROUND($balance,2);
		$balance_simple=number_format(($balance),2,'.','');

	}
	$pdf->Text($balance_x, $first_table_x+11+(9*$i),$balance_simple);
	$unit_v_x=$debit+20;
	$quantity_v_x=$credit+20;
	$net_amount_v_x=$balance_x+20;
    }
    else
    {

	$sl_no_v_x=3;
	$pdf->SetFont('times', '', 9, "", 'false');
	$pdf->Text($sl_no_v_x, $first_table_x+11+(9*$i), date('d-m-Y',strtotime($value['date'])));
	$mode_name_v_x=$sl_no_v_x+18;
	$pdf->Text($mode_name_v_x, $first_table_x+11+(9*$i), $value['mode']);
	$Sub_refernce=$mode_name_v_x+55;
	$pdf->Text($Sub_refernce, $first_table_x+11+(9*$i), $value['voucher_no']);
	$pdf->Text($Sub_refernce+18, $first_table_x+11+(9*$i), $value['remarks']);
	$debit=$Sub_refernce+86;
	if($value['debit']==0)
	{
		$debit_simple="";
	}
	else
	{
		$debit_simple=floatval($value['debit']);
	}
	if($value['credit']==0)
	{
		$credit_simple="";
	}
	else
	{
		$credit_simple=floatval($value['credit']);
	}
	$pdf->Text($debit, $first_table_x+11+(9*$i),$debit_simple);
	$credit=$debit+9;
	$pdf->Text($credit+5, $first_table_x+11+(9*$i), $credit_simple);
	$balance_x=$credit+20;
	$balance=$balance+$value['debit']-$value['credit'];
	if($balance==0)
	{
		$balance_simple="";
	}
	else
	{
		//$balance_simple=floatval($balance);
		$balance_simple=number_format(($balance),2,'.','');
	}
	$pdf->Text($balance_x, $first_table_x+11+(9*$i),$balance_simple);
	$unit_v_x=$debit+20;
	$quantity_v_x=$credit+20;
	$net_amount_v_x=$balance_x+20;
    	
    }
	$i++;
	if($i>14)
	{
		$i=0;
		footer($pdf,$data_total,$invoice_array);
		header_section($pdf,$Journal_all,$Profile,$total_page,$page,$Checkstate,$igst_x_position,$Customer,$data_total);
	}
}
footer($pdf,$data_total,$invoice_array);

$pdf->Output();
exit;
}
public function CustomerOutstanding($id,$from_date,$to_date)
{

	$user_branch_id=$this->Session->read('User.branch_id');
	$branch_id="";
	if($user_branch_id)
	{
		$branch_id=$user_branch_id;
	}

	$from_date=date('Y-m-d',strtotime($from_date));
	$to_date=date('Y-m-d',strtotime($to_date));
	$AccountHead=$this->AccountHead->findById($id);
	$Customer=$this->Customer->findByAccountHeadId($id);
	$conditions_total=[];

	$conditions_total['Customer.account_head_id']=$id;

	$Customer_name=$this->Customer->find('first',array(
		'conditions'=>$conditions_total,
		'order' => array('Customer.id' => 'ASC'),
		'fields'=>array(
			'Customer.account_head_id',
			'AccountHead.opening_balance',
			'AccountHead.name',
			'CustomerType.name',
			'AccountHead.id',

			),
		));
	$from_date=date('Y-m-d',strtotime($from_date));
	$to_date=date('Y-m-d',strtotime($to_date));
	$balance=0;
	$Balance_Total=0;
	$invoice_array=array();$credit_array=array();
	$account_id=$Customer_name['AccountHead']['id'];
	$Sales = $this->Sale->find('all', array(
		'conditions' => array(
			'Sale.account_head_id' =>$account_id,
//'Sale.date_of_delivered between ? and ?'=>array($from_date,$to_date),

			'Sale.flag'=>1,
			'Sale.status'=>array(2,3),
			),
		'order' => array('Sale.date_of_delivered' => 'ASC'),
		'fields' => array(
			'Sale.id',
			'Sale.date_of_delivered',
			'Sale.invoice_no',
			'Sale.account_head_id',
			'Sale.executive_id',
			'Sale.grand_total',
			'AccountHead.name',
			'Executive.account_head_id',

			)
		)
	);
	$voucher_amount=0;

	$Journal_voucher=$this->Journal->find('list',array(
		'conditions'=>array(
			'Journal.credit'=>$account_id,
//'Journal.debit'=>$value['Executive']['account_head_id'],
//'NOT' => array(
//'Journal.remarks'=>'Sale Invoice No :'.$value3['Sale']['invoice_no'],
//),
//	'Journal.date between ? and ?'=>array($from_date,$to_date),
			'Journal.flag=1',
			),
		'fields'=>array(
			'Journal.id',
			'Journal.amount',
			),
		));
	foreach ($Journal_voucher as $key3 => $value_amount) {
		$voucher_amount+=$value_amount;
	}

	$voucher_balance=0;
	foreach ($Sales as $keyj =>$valuej)
	{
		$SaleItem=$this->SaleItem->find('list',array(
			'conditions'=>array(
				'SaleItem.sale_id'=>$valuej['Sale']['id'],
				),
			'fields'=>array(
				)
			));
	}
	$invoice_array_single=array();
	if (!empty($Sales)) {


		$invoice_array_single['party_name'] = $Customer_name['AccountHead']['name'];
		$invoice_array_single['invoice_no'] = array();
		$invoice_array_single['invoice_amount'] = array();
		$invoice_array_single['balance'] = array();
		$invoice_array_single['voucher_no'] = array();
		$invoice_array_single['reference'] = array();
		$invoice_array_single['delivered_date'] = array();
		$AccountHead=$this->AccountHead->findById($Customer_name['AccountHead']['id']);
		if($AccountHead)
		{
			$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
			$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
		}
		if($outstanding_amount>$voucher_amount)
		{
			$outstanding_amount-=$voucher_amount;
			$voucher_amount=0;
		}
		else
		{
			$voucher_amount-=$outstanding_amount;
			$outstanding_amount=0;
		}
		if($outstanding_amount>=1)
		{
			array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
			array_push($invoice_array_single['invoice_no'], 'Opening Balance');
			array_push($invoice_array_single['invoice_amount'],ROUND($outstanding_amount,3));
			array_push($invoice_array_single['voucher_no'],"") ; 
			array_push($invoice_array_single['reference'], '');
			array_push($invoice_array_single['balance'],$outstanding_amount);

		}
		foreach ($Sales as $keySC2 => $valueSC2) {
			$Journal=$this->Journal->find('all',array(
				'conditions'=>array(
					'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
					'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
					'Journal.debit=1',
					'Journal.flag=1',
					),
				));
			$Journal_voucher=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
					'Journal.debit'=>$valueSC2['Sale']['account_head_id'],
//'Journal.debit=1',
					'Journal.flag=1',
					),
				));
			$balance_amount=$valueSC2['Sale']['grand_total'];
			$paid_amount=0;
			if($voucher_amount)
			{
				if($balance_amount<=$voucher_amount)
				{
					$voucher_balance=$balance_amount;
					$balance_amount=0;
					$voucher_amount-=$voucher_balance;
				}
				else
				{
					$balance_amount-=$voucher_amount;
					$voucher_amount=0;
				}
			}
			if($valueSC2['Sale']['grand_total']>0)
			{
				if($balance_amount){
//if($valueSC2['Sale']['account_head_id']==$data['customer_id'])
//{
					$check_date=$valueSC2['Sale']['date_of_delivered'];


					array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Sale']['date_of_delivered'])));
					array_push($invoice_array_single['invoice_no'], $valueSC2['Sale']['invoice_no']);
					array_push($invoice_array_single['invoice_amount'],$valueSC2['Sale']['grand_total']) ;  
					array_push($invoice_array_single['balance'],$balance_amount) ; 
					array_push($invoice_array_single['reference'], 'Sales'); 
					array_push($invoice_array_single['voucher_no'],$Journal_voucher['Journal']['voucher_no']) ;  

//}
				}
			}
		}
		if(count($invoice_array_single['invoice_no']))
		{
			array_push($invoice_array, $invoice_array_single);  
		}      

	}
	else
	{

		$invoice_array_single['party_name'] = $Customer_name['AccountHead']['name'];
//$invoice_array_single['Executive_name'] = $value['Executive']['name'];
		$invoice_array_single['invoice_no'] = array();
		$invoice_array_single['balance'] = array();
		$invoice_array_single['delivered_date'] = array();
		$invoice_array_single['reference'] = array();
		$invoice_array_single['voucher_no'] = array();


		$AccountHead=$this->AccountHead->findById($Customer_name['AccountHead']['id']);
		if($AccountHead)
		{
			$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
			$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
		}
		if($outstanding_amount>$voucher_amount)
		{
			$outstanding_amount-=$voucher_amount;
			$voucher_amount=0;
		}
		else
		{
			$voucher_amount-=$outstanding_amount;
			$outstanding_amount=0;
		}
		if($outstanding_amount>=1)
		{

			array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
			array_push($invoice_array_single['invoice_no'], 'Opening Balance');
			array_push($invoice_array_single['reference'], '');
			array_push($invoice_array_single['invoice_amount'],ROUND($outstanding_amount,3));
			array_push($invoice_array_single['voucher_no'],"") ; 
			array_push($invoice_array_single['balance'],$outstanding_amount);

		}
		if(count($invoice_array_single['invoice_no']))
		{
			array_push($invoice_array, $invoice_array_single);  
		}      
	}
	$Checkstate=0;
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	$igst_x_position=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 


	function header_section($pdf,$Profile,$total_page,$page,$Checkstate,$igst_x_position,$Customer,$data_total,$invoice_array) 
	{
		$pdf->AddPage();
		if(!empty($Profile['logo']))
			$pdf->Image('profile/'.$Profile['logo'],3,8,40);
		$image_line_width=45;
		$invoice_x_starting=$image_line_width;
//$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(0,0,0);
		$pdf->rect(3, 9, 203,280);
		$item_table_head_y_start=10;
		$pdf->SetDrawColor('150','150','150');
		$pdf->Line($image_line_width, $item_table_head_y_start,$image_line_width, $item_table_head_y_start+40);
		$invoice_pos=0;
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);


		$invoice_pos+=2;
		$pdf->text(45,$invoice_pos+3,$Profile['company_name']);
		$pdf->text(138,$invoice_pos+3,$Profile['company_name_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
		$pdf->text(70,$invoice_pos+3,$Profile['address_line_1']);
		$pdf->text(140,$invoice_pos+3,$Profile['address_line_1_arabic'] );

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(46,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
		$pdf->text(143,$invoice_pos+3,$Profile['address_line_2_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('times', 'U', 10, "", 'false');
		$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(78,$invoice_pos,'VAT Number : '.$Profile['vat_code']);
		$VAT_Number='ظريبه الشراء';
		$pdf->text(140,$invoice_pos,$VAT_Number);
		$gst_details_y_staring=50;
$pdf->Line(3, $gst_details_y_staring,206, $gst_details_y_staring); // horizontal line
$pdf->rect(70, $gst_details_y_staring+5, 55,10);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(75,$invoice_pos+18,"Customer Outstanding");

$pdf->text(10,$invoice_pos+33,"Customer :");$pdf->text(40,$invoice_pos+33,$Customer['Customer']['code']." ".$Customer['AccountHead']['name']);
$pdf->Line(3, $gst_details_y_staring+40,206, $gst_details_y_staring+40);
$Row_length=95;
$table_length=130;
$table_column=21;
$pdf->SetFont('dejavusans', '', 8);
$pdf->text(3,$Row_length,'Transaction');
$pdf->text(5,$Row_length+5,'Date');
$pdf->Line($table_column, 90,$table_column, $table_length+130); // vertical line
$pdf->text(25,$Row_length,'Document');
$pdf->text(25,$Row_length+5,'Reference');
$pdf->Line($table_column+53, 90,$table_column+53, $table_length+130); // vertical line
$pdf->text(79,$Row_length,'Sub');
$pdf->text(76,$Row_length+5,'Reference');
$pdf->Line($table_column+73, 90,$table_column+73, $table_length+130); // vertical line
$pdf->text(120,$Row_length,'Comments');
$pdf->Line($table_column+156, 90,$table_column+156, $table_length+130); // vertical line
$pdf->text(165,$Row_length,'Debit');
$pdf->Line($table_column+142, 90,$table_column+142, $table_length+130); // vertical line
$pdf->text(180,$Row_length,'Credit');
$pdf->Line($table_column+170, 90,$table_column+170, $table_length+130); // vertical line
$pdf->text(192,$Row_length,'Balance');
$pdf->Line(3, $gst_details_y_staring+55,206, $gst_details_y_staring+55);
$pdf->Line(3, $gst_details_y_staring+$table_length+70,206, $gst_details_y_staring+$table_length+70);
$pdf->Line(3, $gst_details_y_staring+$table_length+80,206, $gst_details_y_staring+$table_length+80);


$pdf->SetFont('times', '', 8, "", 'false');
}
function footer($pdf,$data_total,$invoice_array){
	$footer_start_y=252;
	$pdf->SetFont('times', 'B', 8, "", 'false');
	$pdf->text(95,$footer_start_y,'Closing Balance As of');
	$pdf->text(98,$footer_start_y+3,date('d-m-Y'));
	$pdf->text(163,$footer_start_y,$data_total['total_debit']);
	$pdf->text(177,$footer_start_y,$data_total['total_credit']);
	$pdf->text(192,$footer_start_y,floatval($data_total['total_debit']-$data_total['total_credit']));
	$pdf->rect(80, $footer_start_y+13, 126,15);
	$pdf->text(83,$footer_start_y+15,"1-30 Days");
$pdf->Line(105,280,105, $footer_start_y+13); // vertical line
$pdf->text(108,$footer_start_y+15,"31-60 Days");
$pdf->Line(128,280,128, $footer_start_y+13); // vertical line

$pdf->text(130,$footer_start_y+15,"61-90 Days");
$pdf->Line(153,280,153, $footer_start_y+13); // vertical line
$pdf->text(155,$footer_start_y+15,"91-180 Days");
$pdf->Line(183,280,183, $footer_start_y+13); // vertical line
$pdf->text(185,$footer_start_y+15,"Over 180 Days");
$pdf->Line(80, $footer_start_y+20,206, $footer_start_y+20);

$q=0;$w=0;$p=0;$r=0;$t=0;$y=0;
if($invoice_array)
{
	foreach($invoice_array as $key=>$value) {
		$party_name=$value['party_name'];
		$inner_array_count=count($value['invoice_no']);
		$a1=array();
		$b1=array();
		$d1=array();
		$c1=array();
		$f1=array();
		$g=array();
		$m=array();
		foreach ($value['delivered_date'] as $key=>$value1)
		{
			$delivered_date=$value1;
			$now = time();
			$expected_days_diff=$now-strtotime($delivered_date);
			$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
			array_push($m,$diff_day);
		}
		foreach($value['invoice_no'] as $key=>$value2)
		{
			$invoice_no=$value2;

		}
		$new_balance=0;

		foreach ($value['balance'] as $key=>$value3)
		{
			$balance=$value3;
			array_push($g,$balance);
			$new_balance+=$balance;
		}
		for($i=0;$i<count($m);$i++)
		{

			if($m[$i]<=30){ $a=$g[$i];array_push($a1,$a);}
			if($m[$i]<=60 && $m[$i]>30){$b=$g[$i];array_push($b1,$b);}
			if($m[$i]<=90 && $m[$i]>60){$c=$g[$i];array_push($c1,$c);}
			if($m[$i]<=180 && $m[$i]>90){$d=$g[$i];array_push($d1,$d);}
			if($m[$i]>180){$f=$g[$i];array_push($f1,$f);}
		}

		$b=array_sum($a1);if($b!=0){$b11=ROUND($b,3);}else{$b11='';}
		$b5=array_sum($b1);if($b5!=0){$b51=ROUND($b5,3);}else{$b51='';}
		$b19=array_sum($c1);if($b19!=0){$b12=ROUND($b19,3);}else{$b12='';}	
		$b3=array_sum($d1);if($b3!=0){$b13=ROUND($b3,3);}else{$b13='';}			
		$b4=array_sum($f1);if($b4!=0){$b41=ROUND($b4,3);}else{$b41='';}
	}
	$q+=$b11;$w+=$b51;$p+=$b12;$y+=$b41;
	$pdf->text(83,273,$b11);$pdf->text(108,273,$b51);$pdf->text(130,273,$b12);$pdf->text(155,273,$b13);$pdf->text(185,273,$b41);
}
//$pdf->Line(4, $footer_start_y,206, $footer_start_y);
$invoce_word_width=4;
$grand_total_length=30;



}

$balance=0;$total_debit=0;$total_credit=0;$total_balance=0;
$data_total=array(
	'total_debit'=>$total_debit,
	'total_credit'=>$total_credit,
	);

header_section($pdf,$Profile,$total_page,$page,$Checkstate,$igst_x_position,$Customer,$data_total,$invoice_array);
$Journal_credit=$this->Journal->find('all',array(
	'conditions'=>array(
		'Journal.credit'=>$account_id,
		'Journal.flag=1',
		),
	'order'=>array('Journal.id DESC'),
	'fields'=>array(
		'Journal.id',
		'Journal.remarks',
		'Journal.amount',
		'Journal.date',
		'Journal.voucher_no',
		'Journal.external_voucher',
		),
	));
$credit_single['amount'] = array();$credit_single['date'] = array();
$credit_single['remarks'] = array();$credit_single['voucher_no'] = array();
$credit_single['external_voucher'] = array();
$count=0;$count_credit=0;
foreach ($Journal_credit as $key4 => $value4) {
	$balance_credit=floatval($invoice_array[0]['invoice_amount'][0])-floatval($invoice_array[0]['balance'][0]);
	$count_credit+=$value4['Journal']['amount'];

	if($count_credit<$balance_credit)
	{
//pr($count_credit.'-'.$balance_credit);

		array_push($credit_single['amount'],$value4['Journal']['amount']);
		array_push($credit_single['date'],date("d-m-Y",strtotime($value4['Journal']['date'])));
		array_push($credit_single['remarks'],$value4['Journal']['remarks']);
		array_push($credit_single['voucher_no'],$value4['Journal']['voucher_no']);
		array_push($credit_single['external_voucher'],$value4['Journal']['external_voucher']);

	}
	else
	{
		$count++;

	}
	if($count==1)
	{
		$credit_last=$count_credit-$balance_credit;
		$credit_balance_last=$value4['Journal']['amount']-$credit_last;
		array_push($credit_single['amount'],$credit_balance_last);
		array_push($credit_single['date'],date("d-m-Y", strtotime($value4['Journal']['date'])));
		array_push($credit_single['remarks'],$value4['Journal']['remarks']);
		array_push($credit_single['voucher_no'],$value4['Journal']['voucher_no']);

		array_push($credit_single['external_voucher'],$value4['Journal']['external_voucher']);

	}
}
//pr($balance_credit);
array_push($credit_array, $credit_single);  
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+85;
foreach($invoice_array as $key=>$value1) {
	for($m=0;$m<count($value1['balance']);$m++){
		$data_total['total_debit']+=floatval($value1['invoice_amount'][$m]);
		$data_total['total_credit']+=floatval($value1['invoice_amount'][$m])-floatval($value1['balance'][$m]);
	}
}
foreach($invoice_array as $key=>$value) {
	for($m=0;$m<count($value['balance']);$m++){
		$sl_no_v_x=4;
		$pdf->SetFont('times', '', 9, "", 'false');
		$pdf->Text($sl_no_v_x, $first_table_x+11+(9*$i), $value['delivered_date'][$m]);
		$mode_name_v_x=$sl_no_v_x+20;
		$pdf->Text($mode_name_v_x, $first_table_x+11+(9*$i),$value['reference'][$m]);
		$Sub_refernce=$mode_name_v_x+55;
		$pdf->Text($Sub_refernce, $first_table_x+11+(9*$i), $value['voucher_no'][$m]);
		$pdf->Text($Sub_refernce+15, $first_table_x+11+(9*$i),'Sale Invoice No :'.$value['invoice_no'][$m]);
		$debit=$Sub_refernce+85;
		$pdf->Text($debit, $first_table_x+11+(9*$i), floatval($value['invoice_amount'][$m]));
		$credit=$debit+13;
		$credit_amount= floatval($value['invoice_amount'][$m])-floatval($value['balance'][$m]);
		//$credit_amount=0;
		$pdf->Text($credit, $first_table_x+11+(9*$i), floatval($credit_amount));
		$balance_x=$credit+14;
		$balance=$balance+floatval($value['invoice_amount'][$m])-$credit_amount;
		$pdf->Text($balance_x, $first_table_x+11+(9*$i), $balance);
		$unit_v_x=$debit+20;
		$quantity_v_x=$credit+20;
		$net_amount_v_x=$balance_x+20;
		$i++;
		if($i>15)
		{
			$i=0;
			footer($pdf,$data_total,$invoice_array);
			header_section($pdf,$Profile,$total_page,$page,$Checkstate,$igst_x_position,$Customer,$data_total,$invoice_array);
		}
	}
}
//$new_balance=$balance;
// foreach ($credit_array as $key6 => $value6) {
// 	for($p=0;$p<count($value6['date']);$p++){

// 		$credit_date=4;
// 		$pdf->SetFont('times', '', 9, "", 'false');
// 		$pdf->Text($credit_date, $first_table_x+11+(9*$i), $value6['date'][$p]);
// 		$credit_reference=$credit_date+20;
// 		$pdf->Text($credit_reference, $first_table_x+11+(9*$i),"Receipt");
// 		$credit_voucher=$credit_reference+55;
// 		$pdf->Text($credit_voucher, $first_table_x+11+(9*$i),$value6['voucher_no'][$p]);
// 		$credit_remarks=$credit_voucher+15;
// 		$pdf->Text($credit_remarks, $first_table_x+11+(9*$i),$value6['remarks'][$p]);
// 		$credit_amount=$credit_remarks+65;
// 		$pdf->Text($credit_amount, $first_table_x+11+(9*$i),floatval($value6['amount'][$p]));
// 		$balance_amount_x_pos=$credit_amount+25;
// 		$balance-=$value6['amount'][$p];
// 		$pdf->Text($balance_amount_x_pos, $first_table_x+11+(9*$i),number_format($balance));
// 		$i++;
// 		if($i>15)
// 		{
// 			$i=0;
// 			footer($pdf,$data_total,$invoice_array);
// 			header_section($pdf,$Profile,$total_page,$page,$Checkstate,$igst_x_position,$Customer,$data_total,$invoice_array);
// 		}

// 	}
// }
footer($pdf,$data_total,$invoice_array);

$pdf->Output();
exit;

}
public function Cheque(){
	$bank_id=2;
	$group_id=array(12,13);
	$conditions=[];
	$from=date("d-m-Y", strtotime('-3 month'));
	$to=date("d-m-Y", strtotime('+6 month'));
	$conditions['Cheque.cheque_date between ? and ?']=[date('Y-m-d',strtotime($from)),date('Y-m-d',strtotime($to))];
	$Cheques=$this->Cheque->find('all',array('conditions'=>$conditions,'order'=>array('Cheque.cheque_date Asc')));
	$cheque_entry_method=array(
		'Receipt'=>'Receipt',
		'Payment'=>'Payment');
	$this->set(compact('cheque_entry_method'));
	$BankList=$this->AccountHead_Option_ListBySubGroupId($bank_id);
	$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
	if($SubGroup_list)
	{
		$sub_group_id=array_keys($SubGroup_list);
		$sub_group_id=array_diff($sub_group_id, array('5','6','7','8','180','185'));
		array_push($sub_group_id, "3", "15");
		$AccountHeadList=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
	}
	else
	{
		$AccountHeadList=[];
	}
	$this->set(compact('BankList','Cheques','AccountHeadList'));
	if(!$this->request->data)
	{
		$this->request->data['Cheque']['clearing_date']=date('d-m-Y');
		$this->request->data['Cheque']['Cheque_date']=date('d-m-Y');
		$this->request->data['Cheque']['recieved_date']=date('d-m-Y');
		$this->set('from',date("d-m-Y", strtotime('-3 month')));
		$this->set('to',date("d-m-Y", strtotime('+6 month')));
	}
	else
	{
		$datasource_Cheque = $this->Cheque->getDataSource();
		try {
			$datasource_Cheque->begin();
			$data=$this->request->data['Cheque'];
//pr($data);exit;
			$data['clearing_date']=date('Y-m-d',strtotime($data['clearing_date']));
			$data['cheque_date']=date('Y-m-d',strtotime($data['Cheque_date']));
			$data['recieved_date']=date('Y-m-d',strtotime($data['recieved_date']));
			$data['created_at']=date('Y-m-d H:i:s');
			$data['updated_at']=date('Y-m-d H:i:s');
			$this->Cheque->create();
			if(!$this->Cheque->save($data))
			{
				$errors = $this->Cheque->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
			}
			$datasource_Cheque->commit();
			$return['result']='Success';
		} catch (Exception $e) {
			$datasource_Cheque->rollback();
			$return['result']=$e->getMessage();
		}
		$this->redirect(array('controller' => 'Accountings','action' =>'Cheque'));
	}
}
public function Cheques_update_status_ajax(){
	$user_id=1;
	$bounce_account_id=$this->GetAccountHeadIdByGroupNameAndAccountHeadName(15,'OTHER INCOME RECEIVED','ChequeReturnCharge Received');
	$datasource_Cheque = $this->Cheque->getDataSource();
	$datasource_Journal = $this->Journal->getDataSource();
	$data=$this->request->data;
	//pr($data);exit;
	try {
		$datasource_Cheque->begin();
		$datasource_Journal->begin();
		$date=date('Y-m-d',strtotime($data['UpdateCheque']['clearing_date_update']));
		$Cheque=$this->Cheque->findById($data['UpdateCheque']['Cheques_id_update']);
		$executive_id=$Cheque['Cheque']['executive_id'];
		if(!$Cheque) throw new Exception("Empty Cheque", 1);
		$this->Cheque->id=$Cheque['Cheque']['id'];
		if(!$this->Cheque->saveField('clearing_date',$date)) throw new Exception("Error Processing Request in clearing_date", 1);
		if(!$this->Cheque->saveField('status',$data['UpdateCheque']['Cheques_status_update'])) throw new Exception("Error Processing Request in status", 1);
		$voucher_no='';
		$external_voucher=$Cheque['Cheque']['cheque_no'];

		if($data['UpdateCheque']['Cheques_status_update'] == 1)
		{
			//if($data['UpdateCheque']['sub_group_id_update'] == 3)
			//{
			if($data['UpdateCheque']['cheque_entry_method'] == 'Receipt')
			{
				$credit=$Cheque['Cheque']['account_head_id'];
				$debit=$Cheque['Cheque']['bank_account_head_id'];
				$work_flow='Cheques';
				$amount=$Cheque['Cheque']['cheque_amount'];
				$remarks='Cheque Receipt :'.$Cheque['Cheque']['cheque_no'];
				$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,'','',$executive_id);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message'], 1);
			}
			else
			{
				$debit=$Cheque['Cheque']['account_head_id'];
				$credit=$Cheque['Cheque']['bank_account_head_id'];
				$amount=$Cheque['Cheque']['cheque_amount'];
				$remarks='Cheque Payment :'.$Cheque['Cheque']['cheque_no'];
				$work_flow='Cheques';
				$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,'','','',$executive_id);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message'], 1);
			}
		   //}
		 //   else
		 //   {
		 //   	if($data['UpdateCheque']['cheque_entry_method'] == 'Receipt')
			// {
			// 	$debit=$Cheque['Cheque']['account_head_id'];
			// 	$credit=$Cheque['Cheque']['bank_account_head_id'];
			// 	$work_flow='Cheques';
			// 	$amount=$Cheque['Cheque']['cheque_amount'];
			// 	$remarks='Cheque Receipt :'.$Cheque['Cheque']['cheque_no'];
			// 	$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,'','',$executive_id);
			// 	if($function_return['result']!='Success')
			// 		throw new Exception($function_return['message'], 1);
			// }
			// else
			// {
			// 	$credit=$Cheque['Cheque']['account_head_id'];
			// 	$debit=$Cheque['Cheque']['bank_account_head_id'];
			// 	$amount=$Cheque['Cheque']['cheque_amount'];
			// 	$remarks='Cheque Payment :'.$Cheque['Cheque']['cheque_no'];
			// 	$work_flow='Cheques';
			// 	$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,'','','',$executive_id);
			// 	if($function_return['result']!='Success')
			// 		throw new Exception($function_return['message'], 1);
			// }
		 //   }
		}
		if($data['UpdateCheque']['Cheques_status_update'] == 2)
		{
			//if($data['UpdateCheque']['sub_group_id_update'] == 3)
			if($data['UpdateCheque']['cheque_entry_method'] == 'Receipt')
			{
				$credit=$bounce_account_id;
				$debit=$Cheque['Cheque']['account_head_id'];
			}
			else
			{
				$credit=$Cheque['Cheque']['account_head_id'];
				$debit=$bounce_account_id;
			}
			$sale_amount=$data['UpdateCheque']['service_amount'];
			$date1=date('Y-m-d');
			$remarks="check bounce - cheque No : ".$Cheque['Cheque']['cheque_no'];
			$work_flow="Cheques";
			$this->Cheque->saveField('service_charge',$data['UpdateCheque']['service_amount']);
			$this->JournalCreate($credit,$debit,$sale_amount,$date1,$remarks,$work_flow,1);
		}
		$return['result']='Success';
		$datasource_Cheque->commit();
		$datasource_Journal->commit();
	} catch (Exception $e) {
		$datasource_Cheque->rollback();
		$datasource_Journal->rollback();
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function Cheque_delete($id)
{
	$datasource_Cheque = $this->Cheque->getDataSource();
	try {
		$datasource_Cheque->begin();
		$Cheque=$this->Cheque->find('first',array(
			'conditions'=>array('Cheque.id'=>$id),
			));
		if(!$this->Cheque->delete($id))
			throw new Exception("Cant Delete This Cheque", 1);
		$datasource_Cheque->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$datasource_Cheque->rollback();
	}
	echo json_encode($return); exit;
}
public function account_head_search_ajax($account_id=null,$from=null,$to=null)
{
	$conditions=[];
	if($account_id=='0'){
		$group_id=array(12,13);
		$SubGroup_list=$this->SubGroup_Option_ListByGroupId($group_id);
		$sub_group_id=array_keys($SubGroup_list);
		array_push($sub_group_id, "3", "15");
		$conditions['AccountHead.sub_group_id']=$sub_group_id;
		$conditions['Cheque.clearing_date between ? and ?']=[date('Y-m-d',strtotime($from)),date('Y-m-d',strtotime($to))];
		$conditions['Cheque.status']=array(0,4);
	}
	else if($account_id=='1')
	{
		$conditions['AccountHead.sub_group_id']=15;
		$conditions['Cheque.status']=array(0,4);
		$conditions['Cheque.clearing_date between ? and ?']=[date('Y-m-d',strtotime($from)),date('Y-m-d',strtotime($to))];
	}
	else if($account_id=='2')
	{
		$conditions['AccountHead.sub_group_id']=3;
		$conditions['Cheque.status']=array(0,4);
		$conditions['Cheque.clearing_date between ? and ?']=[date('Y-m-d',strtotime($from)),date('Y-m-d',strtotime($to))];
	}
	else
	{
		$conditions['AccountHead.sub_group_id !=']=array(3,15,5,6,7,8,180,185);
		$conditions['Cheque.status']=array(0,4);
		$conditions['Cheque.clearing_date between ? and ?']=[date('Y-m-d',strtotime($from)),date('Y-m-d',strtotime($to))];
	}
	$Cheques=$this->Cheque->find('all',array('conditions'=>$conditions,'order'=>array('Cheque.cheque_date Asc'),
		'fields'=>array(
			'Cheque.*',
			'BankAccountHead.*',
			'AccountHead.*',
			),
		'order'=>array('Cheque.clearing_date'),

		));
	foreach ($Cheques as $key => $value) {
		$Cheques[$key]['Cheque']['clearing_date']=date('d-m-Y',strtotime($Cheques[$key]['Cheque']['clearing_date']));
		$Cheques[$key]['Cheque']['cheque_date']=date('d-m-Y',strtotime($Cheques[$key]['Cheque']['cheque_date']));
		$Cheques[$key]['Cheque']['recieved_date']=date('d-m-Y',strtotime($Cheques[$key]['Cheque']['recieved_date']));
	}
	$return['Cheques']=$Cheques;
	$return['data']="";
	if($Cheques)
	{
		$return['result']='Success';
		$return['data']=$Cheques;
	}
	else
	{
		$return['message']='Empty';
	}
	echo json_encode($return); exit;
}
public function CurrentLiabilityCrediterSupplierTransaction_table_ajax()
{
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='AccountHead.created_at';
	$columns[]='AccountHead.name';
	$columns[]='total';
	$columns[]='paid';
	$columns[]='balance';
	$conditions=[];
	
	if(isset($requestData['account_head_id']))
	{
		if($requestData['account_head_id'])
			$conditions['AccountHead.id']=$requestData['account_head_id'];
	}
	$conditions['AccountHead.sub_group_id']=15;
	$totalData=$this->AccountHead->find('count',['conditions'=>$conditions]);
$totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
if( !empty($requestData['search']['value']) ) { 
	$q=$requestData['search']['value'];
	$conditions['OR']=array(
		//'CustomerType.name LIKE' =>'%'. $q . '%',
		'AccountHead.name LIKE' =>'%'. $q . '%',
		//'Customer.code LIKE' =>'%'. $q . '%',
		);
	$totalFiltered=$this->AccountHead->find('count',[
		'conditions'=>$conditions,
		]);
}
//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column'] <= 1)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
$Data=$this->AccountHead->find('all',array(
	'conditions'=>$conditions,
	'offset'=>$requestData['start'],
	'limit'=>$requestData['length'],
	//'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
	'order'=>$order,
	'fields'=>array(
		//'CustomerType.name',
		//'Customer.*',
		'AccountHead.*',
		)
	));
//pr($Data);
//exit;
foreach ($Data as $key => $value) {
//	pr($value);
	$Data[$key]['AccountHead']['date']=date('d-m-Y',strtotime($value['AccountHead']['created_at']));
	
	$Data[$key]['AccountHead']['name']=$value['AccountHead']['name'];
	$Data[$key]['AccountHead']['total']=$value['AccountHead']['opening_balance'];
	$Data[$key]['AccountHead']['paid']=0;
	$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
	$Data[$key]['AccountHead']['total']+=$Debit_N_Credit_function['credit'];
	$Data[$key]['AccountHead']['paid']+=$Debit_N_Credit_function['debit'];
	$Data[$key]['AccountHead']['balance']=$Data[$key]['AccountHead']['total']-$Data[$key]['AccountHead']['paid'];
// $Data[$key]['AccountHead']['balance']=floatval($Data[$key]['AccountHead']['balance']);
	$Data[$key]['AccountHead']['total']=number_format(($Data[$key]['AccountHead']['total']),3,'.','');
	$Data[$key]['AccountHead']['paid']=number_format(($Data[$key]['AccountHead']['paid']),3,'.','');
	$Data[$key]['AccountHead']['balance']=number_format(($Data[$key]['AccountHead']['balance']),3,'.','');
}
//sorting total/balance/recieved
		if($requestData['order'][0]['column'] >= 2) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["AccountHead"][$columnname] == $b["AccountHead"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return (str_replace(',','',$a["AccountHead"][$columnname]) < str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return (str_replace(',','',$a["AccountHead"][$columnname]) > str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
			}
			

			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
//exit;
$json_data = array(
"draw"               => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
"recordsTotal"       => intval( $totalData ),  // total number of records
"recordsFiltered"    => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
"records"            => $Data   // total data array
);
echo json_encode($json_data);
exit;

}
public function SupplierStatement($id,$from_date,$to_date)
{

	$user_branch_id=$this->Session->read('User.branch_id');
	$branch_id="";
	if($user_branch_id)
	{
		$branch_id=$user_branch_id;
	}
	$data['name']='SUJITH_IMPREST. PREPAID';
	$data['name']='RACY SANITARYWARES';
	$data['from_date']='01-05-2010';
	$data['to_date']='15-05-2019';
// $data=$this->request->data;

	$from_date=date('Y-m-d',strtotime($from_date));
	$to_date=date('Y-m-d',strtotime($to_date));
	$AccountHead=$this->AccountHead->findById($id);
	$Customer=$this->Party->findByAccountHeadId($id);
	if(empty($AccountHead))
		throw new Exception("Empty AccountHead", 1);
	$this->Journal->virtualFields = array(
		'total_amount' => "SUM(Journal.amount)",
		);
	$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
					),
				'AND' => array(
					'Journal.flag=1',
					'Journal.date between ? and ?'=>array($from_date,$to_date),
					'Journal.branch_id'=>$branch_id,
					)
				)
			),
		'order'=>array('Journal.date ASC'),
		'fields'=>array(
			'Journal.id',
			'Journal.remarks',
			'Journal.amount',
			'Journal.date',
			'Journal.voucher_no',
			'Journal.external_voucher',
			'Journal.work_flow',
			'Journal.created_at',
			'AccountHeadCredit.name',
			'AccountHeadCredit.opening_balance',
			'AccountHeadDebit.name',
			'AccountHeadDebit.opening_balance',

			),
		));
	$cheque=$this->Cheque->find('all',array(
		'conditions'=>array(
					'Cheque.account_head_id'=>$AccountHead['AccountHead']['id'],
					'Cheque.status'=>array(0,4),
			),
		'order'=>array('Cheque.id ASC'),
		'fields'=>array(
			'Cheque.*',
			'AccountHead.name',
			'AccountHead.id',
			
			),
		));
	$category=[
	'Asset'=>'debit_account',
	'Expense'=>'debit_account',
	'Capital'=>array(
		'Capital'=>'credit_account',
		'Drawing'=>'debit_account',
		),
	'Income'=>'credit_account',
	'Liabilities'=>'credit_account',
	];
	$Journal_all=[];
	foreach ($Journal as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
		$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['external_voucher']=$value['Journal']['external_voucher'];
		if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
		{
			$Journal_single['mode']=$value['AccountHeadCredit']['name'];
			$Journal_single['debit']=$value['Journal']['amount'];
			$Journal_single['credit']=0.00;
		}
		else
		{
			$Journal_single['mode']=$value['AccountHeadDebit']['name'];
			$Journal_single['credit']=$value['Journal']['amount'];
			$Journal_single['debit']=0.00;
		}
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	$return['row']['tbody']='';
	$return['row']['tfoot']='';
	$Balance_Total=0;
	$credit_Total=0;
	$debit_Total=0;
	$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
	$Type_name=$type['Type']['name'];
	$category_name=$category[$type['Type']['name']];
	if($Type_name=='Capital')
	{
		$category_name=$category[$type['Type']['name']][$type['Group']['name']];
	}
	if($category_name=='credit_account')
	{
		$credit=$AccountHead['AccountHead']['opening_balance'];
		$debit='0';
		if($AccountHead['AccountHead']['opening_balance']<0)
		{
			$credit=0;
			$debit=$AccountHead['AccountHead']['opening_balance']*-1;
		}
	}
	else
	{
		$debit=$AccountHead['AccountHead']['opening_balance'];
		$credit='0';
		if($AccountHead['AccountHead']['opening_balance']<0)
		{
			$debit=0;
			$credit=$AccountHead['AccountHead']['opening_balance']*-1;
		}
	}
	if(!in_array($Type_name, ['Expense','Income']))
	{
		$JournalCLosginDebit=$this->Journal->find('first',array(
			'conditions'=>array(
				'Journal.debit'=>$AccountHead['AccountHead']['id'],
				'Journal.flag=1',
				'Journal.date <'=>$from_date,
				'Journal.branch_id'=>$branch_id,
				),
			'fields'=>array( 'total_amount'),
			));
		$JournalCLosginCredit=$this->Journal->find('first',array(
			'conditions'=>array(
				'Journal.credit'=>$AccountHead['AccountHead']['id'],
				'Journal.flag=1',
				'Journal.date <'=>$from_date,
				'Journal.branch_id'=>$branch_id,
				),
			'fields'=>array('total_amount'),
			));
		$debit+=$JournalCLosginDebit['Journal']['total_amount'];
		$credit+=$JournalCLosginCredit['Journal']['total_amount'];
		$Journal_all[0]=array(
			'id'=>0,
			'date'=>date('d-m-Y',strtotime($from_date)),
			'Journal.branch_id'=>$branch_id,
			'strtotime'=>0,
			'mode'=>'Opening Balance',
			'voucher_no'=>'',
			'external_voucher'=>'',
			'remarks'=>'',
			'credit'=>floatval($credit),
			'debit'=>floatval($debit),
			);
	}
	else
	{
		if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
		{

		}
		else
		{
			$debit=0;
			$credit=0;
		}
	}
	if(!in_array($Type_name, ['Expense','Income']))
	{
		$Journal_voucher_no=$this->Journal->find('list',array(
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
						),
					'AND' => array(
						'Journal.flag=1',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'fields'=>array(
				'Journal.id',
				'Journal.date',
				'Journal.voucher_no',
				),
			));
		foreach ($Journal_voucher_no as $voucher_no => $lists) {
			if(count($lists)>1)
			{
				$j=0;
				foreach ($lists as $journal_id => $date) {
					if($j)
					{
						if($date==$Journal_all[key($lists)]['date'] && $voucher_no==$Journal_all[key($lists)]['voucher_no'])
						{
							$Journal_all[key($lists)]['credit']+=$Journal_all[$journal_id]['credit'];
							$Journal_all[key($lists)]['debit']+=$Journal_all[$journal_id]['debit'];
							unset($Journal_all[$journal_id]);
							$GetJournal=$this->Journal->findById($Journal_all[key($lists)]['id'],['credit']);
							if($AccountHead['AccountHead']['id']==$GetJournal['Journal']['credit'])
							{
								$Journal_all[key($lists)]['credit']-=$Journal_all[key($lists)]['debit'];
								$Journal_all[key($lists)]['debit']=0;
								$Journal_all[key($lists)]['credit']=number_format(($Journal_all[key($lists)]['credit']),3,'.','');
							}
							else
							{
								$Journal_all[key($lists)]['debit']-=$Journal_all[key($lists)]['credit'];
								$Journal_all[key($lists)]['credit']=0;
								$Journal_all[key($lists)]['debit']=number_format(($Journal_all[key($lists)]['debit']),3,'.','');
							}
						}								
					}
					$j++;
				}
			}
		}
	}
	
	$date=[];
	foreach ($Journal_all as $i => $row) {
		$date[$i]  = $row['strtotime'];
	}

	array_multisort($date, SORT_ASC, $Journal_all);
	if(!empty($cheque))
	{
	foreach ($cheque as $key => $value_cheque) 
	{
		$Journal_single['id']=$value_cheque['Cheque']['id'];
		$Journal_single['date']=$value_cheque['Cheque']['cheque_date'];
		$Journal_single['strtotime']=strtotime($value_cheque['Cheque']['cheque_date'])+$value_cheque['Cheque']['id'];
		$Journal_single['voucher_no']=$value_cheque['Cheque']['cheque_no'];
		$Journal_single['remarks']="Post Dated Cheque";
		$Journal_single['external_voucher']=$value_cheque['Cheque']['cheque_no'];
		$Journal_single['mode']="CHEQUE";
		$Journal_single['debit']=$value_cheque['Cheque']['cheque_amount'];
		$Journal_single['credit']=0.00;
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
   }
	$conditions_total=[];

	$conditions_total['Party.account_head_id']=$id;

	$Customer_name=$this->Party->find('first',array(
		'conditions'=>$conditions_total,
		'order' => array('Party.id' => 'ASC'),
		'fields'=>array(
			'Party.account_head_id',
			'AccountHead.opening_balance',
			'AccountHead.name',
			'AccountHead.id',

			),
		));
	$from_date=date('Y-m-d',strtotime($from_date));
	$to_date=date('Y-m-d',strtotime($to_date));
	$balance=0;
	$Balance_Total=0;
	$invoice_array=array();
	$account_id=$Customer_name['AccountHead']['id'];
	$Purchase = $this->Purchase->find('all', array(
		'conditions' => array(
			'Purchase.account_head_id' =>$account_id,
//'Sale.date_of_delivered between ? and ?'=>array($from_date,$to_date),

			'Purchase.flag'=>1,
			'Purchase.status'=>array(2,3),
			),
		'order' => array('Purchase.date_of_delivered' => 'ASC'),
		'fields' => array(
			'Purchase.id',
			'Purchase.date_of_delivered',
			'Purchase.invoice_no',
			'Purchase.account_head_id',
			'Purchase.grand_total',
			'AccountHead.name',

			)
		)
	);
	$voucher_amount=0;

	$Journal_voucher=$this->Journal->find('list',array(
		'conditions'=>array(
			'Journal.debit'=>$account_id,

//'Journal.debit'=>$value['Executive']['account_head_id'],
//'NOT' => array(
//'Journal.remarks'=>'Sale Invoice No :'.$value3['Sale']['invoice_no'],
//),
//	'Journal.date between ? and ?'=>array($from_date,$to_date),
			'Journal.flag=1',
			),
		'fields'=>array(
			'Journal.id',
			'Journal.amount',
			),
		));
     
	foreach ($Journal_voucher as $key3 => $value_amount) {
		$voucher_amount+=$value_amount;
	}
	$voucher_bounce=0;
			$Journal_bounce=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.credit'=>$account_id,
					'NOT' => array(
						'Journal.remarks LIKE' => 'Purchase Invoice No :%',
					),
					'Journal.flag=1',
				),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
				),
			));
			foreach ($Journal_bounce as $key4 => $value_bounce) {
				$voucher_bounce+=$value_bounce;
			}
			$voucher_amount=$voucher_amount-$voucher_bounce;
	if(!empty($cheque))
	{
   foreach ($cheque as $key => $value_cheque) 
	{
		$voucher_amount+=$value_cheque['Cheque']['cheque_amount'];
	}
    }
	$voucher_balance=0;
	
	$invoice_array_single=array();
	if (!empty($Purchase)) {


		$invoice_array_single['party_name'] = $Customer_name['AccountHead']['name'];
		$invoice_array_single['invoice_no'] = array();
		$invoice_array_single['balance'] = array();
		$invoice_array_single['delivered_date'] = array();
		$AccountHead=$this->AccountHead->findById($Customer_name['AccountHead']['id']);
		if($AccountHead)
		{
			$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
			$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
		}
		if($outstanding_amount>$voucher_amount)
		{
			$outstanding_amount-=$voucher_amount;
			$voucher_amount=0;
		}
		else
		{
			$voucher_amount-=$outstanding_amount;
			$outstanding_amount=0;
		}
		if($outstanding_amount>=1)
		{
			array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
			array_push($invoice_array_single['invoice_no'], 'Opening Balance');
			array_push($invoice_array_single['balance'],$outstanding_amount);

		}
		foreach ($Purchase as $keySC2 => $valueSC2) {
			$balance_amount=$valueSC2['Purchase']['grand_total'];
					if($voucher_amount)
					{
						if($balance_amount<$voucher_amount)
						{
							$voucher_balance=$balance_amount;
							$balance_amount=0;
							$voucher_amount-=$voucher_balance;
						}
						else
						{
							$balance_amount-=$voucher_amount;
							$voucher_amount=0;
						}
					}
			if($valueSC2['Purchase']['grand_total']>0)
			{
				if($balance_amount){
//if($valueSC2['Sale']['account_head_id']==$data['customer_id'])
//{
					$check_date=$valueSC2['Purchase']['date_of_delivered'];


					array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Purchase']['date_of_delivered'])));
					array_push($invoice_array_single['invoice_no'], $valueSC2['Purchase']['invoice_no']);
					array_push($invoice_array_single['balance'],$balance_amount) ;  

//}
				}
			}
		}
		
		if(count($invoice_array_single['invoice_no']))
		{
			array_push($invoice_array, $invoice_array_single);  
		}      

	}
	else
	{

		$invoice_array_single['party_name'] = $Customer_name['AccountHead']['name'];
//$invoice_array_single['Executive_name'] = $value['Executive']['name'];
		$invoice_array_single['invoice_no'] = array();
		$invoice_array_single['balance'] = array();
		$invoice_array_single['delivered_date'] = array();
		$AccountHead=$this->AccountHead->findById($Customer_name['AccountHead']['id']);
		if($AccountHead)
		{
			$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
			$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
		}
		if($outstanding_amount>$voucher_amount)
		{
			$outstanding_amount-=$voucher_amount;
			$voucher_amount=0;
		}
		else
		{
			$voucher_amount-=$outstanding_amount;
			$outstanding_amount=0;
		}
		if($outstanding_amount>=1)
		{

			array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
			array_push($invoice_array_single['invoice_no'], 'Opening Balance');
			array_push($invoice_array_single['balance'],$outstanding_amount);

		}
		if(count($invoice_array_single['invoice_no']))
		{
			array_push($invoice_array, $invoice_array_single);  
		}      
	}
	$Checkstate=0;
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	$igst_x_position=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 


	function header_section($pdf,$Journal_all,$Profile,$total_page,$page,$Checkstate,$igst_x_position,$Customer,$data_total) 
	{
		$pdf->AddPage();
		if(!empty($Profile['logo']))
			$pdf->Image('profile/'.$Profile['logo'],3,8,40);
		$image_line_width=45;
		$invoice_x_starting=$image_line_width;
//$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(0,0,0);
		$pdf->rect(3, 9, 203,280);
		$item_table_head_y_start=10;
		$pdf->SetDrawColor('150','150','150');
		$pdf->Line($image_line_width, $item_table_head_y_start,$image_line_width, $item_table_head_y_start+40);
		$invoice_pos=0;
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);


		$invoice_pos+=2;
		$pdf->text(45,$invoice_pos+3,$Profile['company_name']);
		$pdf->text(138,$invoice_pos+3,$Profile['company_name_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
		$pdf->text(70,$invoice_pos+3,$Profile['address_line_1']);
		$pdf->text(140,$invoice_pos+3,$Profile['address_line_1_arabic'] );

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(46,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
		$pdf->text(143,$invoice_pos+3,$Profile['address_line_2_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('times', 'U', 10, "", 'false');
		$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(78,$invoice_pos,'VAT Number : '.$Profile['vat_code']);
		$VAT_Number='ظريبه الشراء';
		$pdf->text(140,$invoice_pos,$VAT_Number);
		$gst_details_y_staring=50;
$pdf->Line(3, $gst_details_y_staring,206, $gst_details_y_staring); // horizontal line
$pdf->rect(70, $gst_details_y_staring+5, 55,10);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(75,$invoice_pos+18,"Supplier Statement");
$pdf->text(10,$invoice_pos+33,"Supplier :");$pdf->text(40,$invoice_pos+33,$Customer['Party']['code']." ".$Customer['AccountHead']['name']);
$pdf->Line(3, $gst_details_y_staring+40,206, $gst_details_y_staring+40);
$Row_length=95;
$table_length=130;
$table_column=21;
$pdf->SetFont('dejavusans', '', 8);
$pdf->text(3,$Row_length,'Transaction');
$pdf->text(5,$Row_length+5,'Date');
$pdf->Line($table_column, 90,$table_column, $table_length+130); // vertical line
$pdf->text(25,$Row_length,'Document');
$pdf->text(25,$Row_length+5,'Reference');
$pdf->Line($table_column+55, 90,$table_column+55, $table_length+130); // vertical line
$pdf->text(79,$Row_length,'Sub');
$pdf->text(76,$Row_length+5,'Reference');
$pdf->Line($table_column+73, 90,$table_column+73, $table_length+130); // vertical line
$pdf->text(120,$Row_length,'Comments');
$pdf->Line($table_column+140, 90,$table_column+140, $table_length+130); // vertical line
$pdf->text(165,$Row_length,'Debit');
$pdf->Line($table_column+155, 90,$table_column+155, $table_length+130); // vertical line
$pdf->text(180,$Row_length,'Credit');
$pdf->Line($table_column+170, 90,$table_column+170, $table_length+130); // vertical line
$pdf->text(191,$Row_length,'Balance');
$pdf->Line(3, $gst_details_y_staring+55,206, $gst_details_y_staring+55);
$pdf->Line(3, $gst_details_y_staring+$table_length+70,206, $gst_details_y_staring+$table_length+70);
$pdf->Line(3, $gst_details_y_staring+$table_length+80,206, $gst_details_y_staring+$table_length+80);


$pdf->SetFont('times', '', 8, "", 'false');
}
function footer($pdf,$data_total,$invoice_array){
	$footer_start_y=252;
	$pdf->SetFont('times', 'B', 8, "", 'false');
	$pdf->text(95,$footer_start_y,'Closing Balance As of');
	$pdf->text(98,$footer_start_y+3,date('d-m-Y'));

	$pdf->text(161,$footer_start_y,$data_total['total_debit']);
	$pdf->text(176,$footer_start_y,$data_total['total_credit']);
	$pdf->text(192,$footer_start_y,number_format($data_total['total_credit']-$data_total['total_debit']));
	$pdf->rect(80, $footer_start_y+13, 126,15);
	$pdf->text(83,$footer_start_y+15,"1-30 Days");
$pdf->Line(105,280,105, $footer_start_y+13); // vertical line
$pdf->text(108,$footer_start_y+15,"31-60 Days");
$pdf->Line(128,280,128, $footer_start_y+13); // vertical line

$pdf->text(130,$footer_start_y+15,"61-90 Days");
$pdf->Line(153,280,153, $footer_start_y+13); // vertical line
$pdf->text(155,$footer_start_y+15,"91-180 Days");
$pdf->Line(183,280,183, $footer_start_y+13); // vertical line
$pdf->text(185,$footer_start_y+15,"Over 180 Days");
$pdf->Line(80, $footer_start_y+20,206, $footer_start_y+20);

$q=0;$w=0;$p=0;$r=0;$t=0;$y=0;
if($invoice_array)
{
	foreach($invoice_array as $key=>$value) {
		$party_name=$value['party_name'];
		$inner_array_count=count($value['invoice_no']);
		$a1=array();
		$b1=array();
		$d1=array();
		$c1=array();
		$f1=array();
		$g=array();
		$m=array();
		foreach ($value['delivered_date'] as $key=>$value1)
		{
			$delivered_date=$value1;
			$now = time();
			$expected_days_diff=$now-strtotime($delivered_date);
			$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
			array_push($m,$diff_day);
		}
		foreach($value['invoice_no'] as $key=>$value2)
		{
			$invoice_no=$value2;

		}
		$new_balance=0;

		foreach ($value['balance'] as $key=>$value3)
		{
			$balance=$value3;
			array_push($g,$balance);
			$new_balance+=$balance;
		}
		for($i=0;$i<count($m);$i++)
		{

			if($m[$i]<=30){ $a=$g[$i];array_push($a1,$a);}
			if($m[$i]<=60 && $m[$i]>30){$b=$g[$i];array_push($b1,$b);}
			if($m[$i]<=90 && $m[$i]>60){$c=$g[$i];array_push($c1,$c);}
			if($m[$i]<=180 && $m[$i]>90){$d=$g[$i];array_push($d1,$d);}
			if($m[$i]>180){$f=$g[$i];array_push($f1,$f);}
		}

		$b=array_sum($a1);if($b!=0){$b11=ROUND($b,3);}else{$b11='';}
		$b5=array_sum($b1);if($b5!=0){$b51=ROUND($b5,3);}else{$b51='';}
		$b19=array_sum($c1);if($b19!=0){$b12=ROUND($b19,3);}else{$b12='';}	
		$b3=array_sum($d1);if($b3!=0){$b13=ROUND($b3,3);}else{$b13='';}			
		$b4=array_sum($f1);if($b4!=0){$b41=ROUND($b4,3);}else{$b41='';}
	}
	$q+=$b11;$w+=$b51;$p+=$b12;$y+=$b41;
	$pdf->text(83,273,$b11);$pdf->text(108,273,$b51);$pdf->text(130,273,$b12);$pdf->text(155,273,$b13);$pdf->text(185,273,$b41);
}
//$pdf->Line(4, $footer_start_y,206, $footer_start_y);
$invoce_word_width=4;
$grand_total_length=30;



}

$balance=0;$total_debit=0;$total_credit=0;$total_balance=0;
$data_total=array(
	'total_debit'=>$total_debit,
	'total_credit'=>$total_credit,
	);

header_section($pdf,$Journal_all,$Profile,$total_page,$page,$Checkstate,$igst_x_position,$Customer,$data_total);
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+85;
foreach ($Journal_all as $key => $value1) {
	$data_total['total_debit']+=floatval($value1['debit']);
	$data_total['total_credit']+=floatval($value1['credit']);
}
foreach($Journal_all as $key=>$value) {
	if($value['mode']!="CHEQUE")
	{
	$sl_no_v_x=4;
	$pdf->SetFont('times', '', 8, "", 'false');
	$pdf->Text($sl_no_v_x, $first_table_x+11+(9*$i), date('d-m-Y',strtotime($value['date'])));
	$mode_name_v_x=$sl_no_v_x+18;
	$pdf->Text($mode_name_v_x, $first_table_x+11+(9*$i), $value['mode']);
	$Sub_refernce=$mode_name_v_x+55;
	$pdf->Text($Sub_refernce, $first_table_x+11+(9*$i), $value['voucher_no']);
	$pdf->Text($Sub_refernce+18, $first_table_x+11+(9*$i), $value['remarks']);
	$debit=$Sub_refernce+85;
	if($value['debit']==0)
	{
		$debit_simple="";
	}
	else
	{
		$debit_simple=floatval($value['debit']);
	}
	if($value['credit']==0)
	{
		$credit_simple="";
	}
	else
	{
		$credit_simple=floatval($value['credit']);
	}
	$pdf->Text($debit, $first_table_x+11+(9*$i),$debit_simple);
	$credit=$debit+9;
	$pdf->Text($credit+5, $first_table_x+11+(9*$i), $credit_simple);
	$balance_x=$credit+20;
	$balance=round(($balance+$value['credit']-$value['debit']),2);
	if($balance==0)
	{
		$balance_simple="";
	}
	else
	{
		$balance_simple=floatval($balance);
	}
	$pdf->Text($balance_x, $first_table_x+11+(9*$i),$balance_simple);
	$unit_v_x=$debit+20;
	$quantity_v_x=$credit+20;
	$net_amount_v_x=$balance_x+20;
    }
    else
    {

	$sl_no_v_x=3;
	//$pdf->SetFont('times', 'B', 10, "", 'false');
	$pdf->Text($sl_no_v_x, $first_table_x+11+(9*$i), date('d-m-Y',strtotime($value['date'])));
	$mode_name_v_x=$sl_no_v_x+18;
	$pdf->Text($mode_name_v_x, $first_table_x+11+(9*$i), $value['mode']);
	$Sub_refernce=$mode_name_v_x+55;
	$pdf->Text($Sub_refernce, $first_table_x+11+(9*$i), $value['voucher_no']);
	$pdf->Text($Sub_refernce+18, $first_table_x+11+(9*$i), $value['remarks']);
	$debit=$Sub_refernce+86;
	if($value['debit']==0)
	{
		$debit_simple="";
	}
	else
	{
		$debit_simple=floatval($value['debit']);
	}
	if($value['credit']==0)
	{
		$credit_simple="";
	}
	else
	{
		$credit_simple=floatval($value['credit']);
	}
	$pdf->Text($debit, $first_table_x+11+(9*$i),$debit_simple);
	$credit=$debit+9;
	$pdf->Text($credit+5, $first_table_x+11+(9*$i), $credit_simple);
	$balance_x=$credit+20;
		$balance=round(($balance+$value['credit']-$value['debit']),2);

	if($balance==0)
	{
		$balance_simple="";
	}
	else
	{
		$balance_simple=floatval($balance);
	}
	$pdf->Text($balance_x, $first_table_x+11+(9*$i),$balance_simple);
	$unit_v_x=$debit+20;
	$quantity_v_x=$credit+20;
	$net_amount_v_x=$balance_x+20;
    	
    }
	$i++;
	if($i>14)
	{
		$i=0;
		footer($pdf,$data_total,$invoice_array);
		header_section($pdf,$Journal_all,$Profile,$total_page,$page,$Checkstate,$igst_x_position,$Customer,$data_total);
	}
}
footer($pdf,$data_total,$invoice_array);

$pdf->Output();
exit;
}
public function voucher_migration_in_journal()
	{
		// $Sale=$this->Sale->find('all',array(
		// 	'conditions'=>array('Sale.is_erp'=>0),
		// ));
		$Journal=$this->Journal->find('all',array(
					'conditions'=>array('Journal.voucher_no'=>1,'Journal.id !='=>1),
					'fields'=>array(
						'Journal.id',
						'Journal.executive_id',
						'Journal.route_id',
					),
				));
		$user_id=1;$i=56;
		$datasource_Journal = $this->Journal->getDataSource();
		try {
        	$datasource_Journal->begin();
			foreach ($Journal as $key => $value) {				

					$this->Journal->id=$value['Journal']['id'];

					$this->Journal->saveField('voucher_no',$i);
					//$this->Journal->saveField('executive_id',$executive_id);
		        $datasource_Journal->commit();
		        $i++;
			}
		}
		catch (Exception $e)
	    {
	        $datasource_Journal->rollback();
	        $return['result']=$e->getMessage();
	        $this->Session->setFlash(__($return['result']));
	        $this->redirect( Router::url( $this->referer(), true ) );
	    }
		exit;
	}
	public function expense_vat_migration_in_journal()
	{
		 $Journal=$this->Journal->find('all',array(
			  'conditions'=>array(
			    'work_flow'=>'Expense',
			    'credit'=>'23',
			     'flag'=>1,
			    ),
			  'fields'=>array(
			    'Journal.date',
			    'Journal.amount',
			    'Journal.id',
			    'Journal.debit',
			    'Journal.external_voucher'
			    )
			  ));
		$datasource_Journal = $this->Journal->getDataSource();
		try {
        	$datasource_Journal->begin();
			foreach ($Journal as $key => $value) {				

					$this->Journal->id=$value['Journal']['id'];
				$this->Journal->saveField('credit',$value['Journal']['debit']);
				$this->Journal->saveField('debit',23);
		        $datasource_Journal->commit();
			}
		}
		catch (Exception $e)
	    {
	        $datasource_Journal->rollback();
	        $return['result']=$e->getMessage();
	        $this->Session->setFlash(__($return['result']));
	        $this->redirect( Router::url( $this->referer(), true ) );
	    }
		exit;
	}
	public function update_customer_priority($id,$priority)
	{
					$return['status']="Empty";

		  $this->Customer->id=$this->Customer->field('id',array('account_head_id'=>$id));
 			if(!$this->Customer->saveField('priority',$priority))
 			{
 			throw new Exception("Cant Updated priority", 1);
	
			}
			else
			{
			$return['status']="Success";	
			}
       echo json_encode($return);exit;
  	}
  	public function VoucherReceipt()
	{
		// $PermissionList = $this->Session->read('PermissionList');
		// $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/VoucherGeneral'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/VoucherGeneral';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/VoucherReceipt'));
if(!in_array($menu_id, $PermissionList))
{
$this->Session->setFlash("Permission denied");
return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
}
	$cash_group_id=1;
	$bank_group_id=2;
	$crediters_group_id=18;
	$reserves_group_id=24;
	$provision_group_id=23;
	$pre_paid_expense_group_id=5;
	$outstanding_expense_group_id=20;
	$profit_loss_group_id=25;
	$profit_loss_dot_group_id=27;
	$expense_master_group_id=5;
	$user_id=1;
	
		if(!$this->request->data)
		{
			$AccountHeads=$this->AccountHead->find('all',array(
			'joins'=>[
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			],
			'conditions'=>array(
				'not'=>array(
					'SubGroup.group_id'=>array(
						$cash_group_id,
						$bank_group_id,
						$crediters_group_id,
						$profit_loss_group_id,
						$profit_loss_dot_group_id,
						$pre_paid_expense_group_id,
						$outstanding_expense_group_id,
						$reserves_group_id,
						$provision_group_id,
						),
					'Group.master_group_id'=>[$expense_master_group_id],
					),
				),
			'fields'=>[
			'AccountHead.id',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			'AccountHead.name',
			]
			)
		);
			//pr($AccountHeads);
			//exit;
			$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		}
		$this->set('AccountHead_list',$AccountHead_list);
			//$this->set('AccountHead_list',$AccountHeads);
			$data['Journal']['date']=date('d-m-Y');
			$data['Journal']['customer_type']=1;
			$data['from_date']=date('d-m-Y',strtotime('-1 month'));
			$data['to_date']=date('d-m-Y');
			//$data['to_date']=date('d-m-Y',strtotime('+6 month'));
			$this->request->data=$data;
		}
		else
		{
			$datasource_Journal = $this->Journal->getDataSource();
			try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['account_head'];
			$debit=$data['mode'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$work_flow='Voucher Receipt';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function VoucherReceipt_table_ajax()
	{
		
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='AccountHead.created_at';
		$columns[]='AccountHead.name';
		$columns[]='total';
		$cash_group_id=1;
	$bank_group_id=2;
	$crediters_group_id=18;
	$reserves_group_id=24;
	$provision_group_id=23;
	$pre_paid_expense_group_id=5;
	$outstanding_expense_group_id=20;
	$profit_loss_group_id=25;
	$profit_loss_dot_group_id=27;
	$expense_master_group_id=5;
		$conditions=[];
		// $conditions['SubGroup.group_id']=array(
		// 		$cash_group_id=1,
	 //            $bank_group_id=2,
		// 	);
		$conditions['NOT']=array(
			'SubGroup.group_id' =>array(
				$cash_group_id,
						$bank_group_id,
						$crediters_group_id,
						$profit_loss_group_id,
						$profit_loss_dot_group_id,
						$pre_paid_expense_group_id,
						$outstanding_expense_group_id,
						$reserves_group_id,
						$provision_group_id,
			),
		'Group.master_group_id'=>[$expense_master_group_id],
	);
		


		if(isset($requestData['account_head_id']))
	{
		if($requestData['account_head_id'])
			$conditions['AccountHead.id']=$requestData['account_head_id'];
	}
		$totalData=$this->AccountHead->find('count',[
			'joins'=>[
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			],
			'conditions'=>$conditions
		]);
		$totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'AccountHead.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->AccountHead->find('count',[
				'joins'=>[
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			],
				'conditions'=>$conditions,
			]);
		}

		//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column'] <= 1)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
		$Data=$this->AccountHead->find('all',array(
			'joins'=>[
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			],
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			//'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'order'=> $order,
			'fields'=>array(
				'AccountHead.*',
			)
		));
		$category=[
			'Asset'=>'debit_account',
			'Expense'=>'debit_account',
			'Capital'=>array(
				'Capital'=>'credit_account',
				'Drawing'=>'debit_account',
			),
			'Income'=>'credit_account',
			'Liabilities'=>'credit_account',
		];
		foreach ($Data as $key => $value) {
			$function_return=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
			$Data[$key]['AccountHead']['created_at']=date('d-m-Y',strtotime($value['AccountHead']['created_at']));
			$Data[$key]['AccountHead']['name']=$value['AccountHead']['name'];
			$type=$this->get_type_by_account_dead($value['AccountHead']['id']);
			$Type_name=$type['Type']['name'];
			$category_name=$category[$type['Type']['name']];
			if($Type_name=='Capital')
			{
				$category_name=$category[$type['Type']['name']][$type['Group']['name']];
			}
			$debit=$function_return['debit'];
			$credit=$function_return['credit'];
			if($category_name=='credit_account')
			{
				//$credit+=$value['AccountHead']['opening_balance'];
				if($value['AccountHead']['opening_balance']<0)
				{
					$debit+=$value['AccountHead']['opening_balance']*-1;
				}
				else{
					$credit+=$value['AccountHead']['opening_balance'];
				}
			}
			else
			{
				//$debit+=$value['AccountHead']['opening_balance'];
				if($value['AccountHead']['opening_balance']<0)
				{
					$credit+=$value['AccountHead']['opening_balance']*-1;
				}
				else{
					$debit+=$value['AccountHead']['opening_balance'];
				}
			}
			if($category_name=='credit_account')
			{
				$Data[$key]['AccountHead']['total']=$credit;
				$Data[$key]['AccountHead']['total']-=$debit;
			}
			else
			{
				$Data[$key]['AccountHead']['total']=$debit;
				$Data[$key]['AccountHead']['total']-=$credit;
			}
			$Data[$key]['AccountHead']['total']=number_format($Data[$key]['AccountHead']['total'],3,'.','');
		
		}
		//sorting total/balance/recieved
		if($requestData['order'][0]['column'] >= 2) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["AccountHead"][$columnname] == $b["AccountHead"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return (str_replace(',','',$a["AccountHead"][$columnname]) < str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return (str_replace(',','',$a["AccountHead"][$columnname]) > str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
			}
			

			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
		//pr($Data);
		//exit;
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function VoucherPayment()
	{
		// $PermissionList = $this->Session->read('PermissionList');
		// $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/VoucherGeneral'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/VoucherGeneral';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/VoucherPayment'));
if(!in_array($menu_id, $PermissionList))
{
$this->Session->setFlash("Permission denied");
return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
}
		$cash_group_id=1;
	$bank_group_id=2;
	$crediters_group_id=18;
	$profit_loss_group_id=25;
	$customer_group_id=3;
	$income_master_group_id=6;
	$Miscellaneous_Expenditure_And_Losses_group_id=11;
	$accrude_group_id=6;
	$advance_group_id=21;
	$reserves_group_id=24;
	$capital_master_group_id=4;
	$current_asset_master_group_id=1;
	$fixed_asset_master_group_id=2;
	$investment_asset_master_group_id=3;
	$long_liabilities_master_group_id=7;
	$medium_liabilities_master_group_id=8;
	$user_id=1;
		if(!$this->request->data)
		{
			$AccountHeads=$this->AccountHead->find('all',array(
			'joins'=>[
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			],
			'conditions'=>[
			'NOT'=>array(
				'SubGroup.group_id'=>array(
					$cash_group_id,
					$customer_group_id,
					$bank_group_id,
					$profit_loss_group_id,
					$accrude_group_id,
					$advance_group_id,
					$reserves_group_id,
					),
				'Group.master_group_id'=>array(
					$income_master_group_id,
					$capital_master_group_id,
// $fixed_asset_master_group_id,
					$investment_asset_master_group_id,
					$Miscellaneous_Expenditure_And_Losses_group_id,
					),
				),
			],
			'fields'=>[
			'AccountHead.id',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			'AccountHead.name',
			]
			)
		);
			//pr($AccountHeads);
			//exit;
			$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		}
		$this->set('AccountHead_list',$AccountHead_list);
			//$this->set('AccountHead_list',$AccountHeads);
			$data['Journal']['date']=date('d-m-Y');
			//$data['from_date']=date('d-m-Y',strtotime('-1 month'));
			//$data['to_date']=date('d-m-Y');
			$data['Journal']['customer_type']=1;
	        $data['from_date']=date('d-m-Y',strtotime('-1 month'));
		    $data['to_date']=date('d-m-Y',strtotime('+6 month'));
			$this->request->data=$data;
		}
		else
		{
			$datasource_Journal = $this->Journal->getDataSource();
			try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['mode'];
			$debit=$data['account_head'];
			$amount=$data['amount'];
			$date=$data['date'];
			$remarks=$data['remarks'];
			$external_voucher=$data['external_voucher'];
			$voucher_no=$data['voucher_no'];
			$work_flow='Voucher Payment';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function VoucherPayment_table_ajax()
	{
	$cash_group_id=1;
	$bank_group_id=2;
	$crediters_group_id=18;
	$profit_loss_group_id=25;
	$customer_group_id=3;
	$income_master_group_id=6;
	$Miscellaneous_Expenditure_And_Losses_group_id=11;
	$accrude_group_id=6;
	$advance_group_id=21;
	$reserves_group_id=24;
	$capital_master_group_id=4;
	$current_asset_master_group_id=1;
	$fixed_asset_master_group_id=2;
	$investment_asset_master_group_id=3;
	$long_liabilities_master_group_id=7;
	$medium_liabilities_master_group_id=8;
	//$user_id=1;
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='AccountHead.created_at';
	    $columns[]='AccountHead.name';
        $columns[]='total';
		$conditions=[];
		$conditions['NOT']=array(
				'SubGroup.group_id'=>array(
					$cash_group_id,
					$customer_group_id,
					$bank_group_id,
					$profit_loss_group_id,
					$accrude_group_id,
					$advance_group_id,
					$reserves_group_id,
					),
				'Group.master_group_id'=>array(
					$income_master_group_id,
					$capital_master_group_id,
// $fixed_asset_master_group_id,
					$investment_asset_master_group_id,
					$Miscellaneous_Expenditure_And_Losses_group_id,
					)
				);
		if(isset($requestData['account_head_id']))
	{
		if($requestData['account_head_id'])
			$conditions['AccountHead.id']=$requestData['account_head_id'];
	}
		$totalData=$this->AccountHead->find('count',
			['joins'=>[
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			],
			'conditions'=>$conditions]);
		$totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'AccountHead.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->AccountHead->find('count',[
				'joins'=>[
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			],
				'conditions'=>$conditions,
			]);
		}

		//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column'] <= 1)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
		$Data=$this->AccountHead->find('all',array(
			'joins'=>[
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			],
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			//'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'order'=> $order,
			'fields'=>array(
				'AccountHead.*',
			)
		));
		$category=[
			'Asset'=>'debit_account',
			'Expense'=>'debit_account',
			'Capital'=>array(
				'Capital'=>'credit_account',
				'Drawing'=>'debit_account',
			),
			'Income'=>'credit_account',
			'Liabilities'=>'credit_account',
		];
		foreach ($Data as $key => $value) {
			//pr($value);
			$function_return=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
			$Data[$key]['AccountHead']['created_at']=date('d-m-Y',strtotime($value['AccountHead']['created_at']));
			$Data[$key]['AccountHead']['name']=$value['AccountHead']['name'];
			$type=$this->get_type_by_account_dead($value['AccountHead']['id']);
			$Type_name=$type['Type']['name'];
			$category_name=$category[$type['Type']['name']];
			if($Type_name=='Capital')
			{
				$category_name=$category[$type['Type']['name']][$type['Group']['name']];
			}
			$debit=$function_return['debit'];
			$credit=$function_return['credit'];
			if($category_name=='credit_account')
			{
				if($value['AccountHead']['opening_balance']<0) {
					$debit+=$value['AccountHead']['opening_balance']*-1;
				} else {
					$credit+=$value['AccountHead']['opening_balance'];
				}
			}
			else
			{
				//$debit+=$value['AccountHead']['opening_balance'];
				if($value['AccountHead']['opening_balance']<0)
				{
					$credit+=$value['AccountHead']['opening_balance']*-1;
				}
				else{
					$debit+=$value['AccountHead']['opening_balance'];
				}
			}
			if($category_name=='credit_account')
			{
				$Data[$key]['AccountHead']['total']=$credit;
				$Data[$key]['AccountHead']['total']-=$debit;
			}
			else
			{
				$Data[$key]['AccountHead']['total']=$debit;
				$Data[$key]['AccountHead']['total']-=$credit;
			}
			$Data[$key]['AccountHead']['total']=number_format($Data[$key]['AccountHead']['total'],3,'.','');
		
		}
		//sorting total/balance/recieved
		if($requestData['order'][0]['column'] >= 2) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["AccountHead"][$columnname] == $b["AccountHead"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return (str_replace(',','',$a["AccountHead"][$columnname]) < str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return (str_replace(',','',$a["AccountHead"][$columnname]) > str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
			}
			

			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
		//pr($Data);
		//exit;
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function VoucherGeneral()
	{
		// $PermissionList = $this->Session->read('PermissionList');
		// $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/VoucherGeneral'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/VoucherGeneral';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/VoucherGeneral'));
if(!in_array($menu_id, $PermissionList))
{
$this->Session->setFlash("Permission denied");
return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
}
		$profit_loss_group_id=25;
		$profit_loss_dot_group_id=27;
		$reserves_group_id=24;
		$user_id=1;
		if(!$this->request->data)
		{
			$AccountHeads=$this->AccountHead->find('all',array(
			'conditions'=>[
			'NOT'=>array(
				'SubGroup.group_id'=>array(
					$profit_loss_group_id,
					$profit_loss_dot_group_id,
					$reserves_group_id,
					),
				),
			],
			'order'=>array('AccountHead.name ASC'),
			'fields'=>[
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			//'AccountHeadCredit.name',
			//'AccountHeadDebit.name',
			]
			)
		);
			//pr($AccountHeads);
			//exit;
			$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		}
		$this->set('AccountHead_list',$AccountHead_list);
			//$this->set('AccountHead_list',$AccountHeads);
			$data['Journal']['date']=date('d-m-Y');
			$data['from_date']=date('d-m-Y',strtotime('-1 month'));
			$data['to_date']=date('d-m-Y');
			$this->request->data=$data;
		}
		else
		{
			$datasource_Journal = $this->Journal->getDataSource();
			try {
				$datasource_Journal->begin();
				$data=$this->request->data['Journal'];
				$credit=$data['to_account'];
				$debit=$data['by_account'];
				if($credit==$debit)
					throw new Exception("Cant Use Same head", 1);
				$amount=$data['amount'];
				$date=$data['date'];
				$remarks=$data['remarks'];
				$voucher_no=$data['voucher_no'];
				$external_voucher=$data['external_voucher'];
				$work_flow='Voucher General';
				$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,null,null);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message'], 1);
				$return['result']='Success';
				$datasource_Journal->commit();
				$this->Session->setFlash(__($return['result']));
			} catch (Exception $e) {
				$datasource_Journal->rollback();
				$return['result']='Error';
				$return['message']=$e->getMessage();
				$this->Session->setFlash(__($return['message']));
			}
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function VoucherGeneral_table_ajax()
	{
		$profit_loss_group_id=25;
		$profit_loss_dot_group_id=27;
		$reserves_group_id=24;
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='AccountHead.created_at';
		$columns[]='AccountHead.name';
		$columns[]='total';
		$conditions=[];
		$conditions['NOT']=array(
			'SubGroup.group_id' =>array(
				$profit_loss_group_id,
				$profit_loss_dot_group_id,
				$reserves_group_id,
			));
		if(isset($requestData['account_head_id']))
	{
		if($requestData['account_head_id'])
			$conditions['AccountHead.id']=$requestData['account_head_id'];
	}
		$totalData=$this->AccountHead->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'AccountHead.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->AccountHead->find('count',[
				'conditions'=>$conditions,
			]);
		}
		//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column'] <= 1)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
		$Data=$this->AccountHead->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			//'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'order'=> $order,
			'fields'=>array(
				'AccountHead.*',
			)
		));
		$category=[
			'Asset'=>'debit_account',
			'Expense'=>'debit_account',
			'Capital'=>array(
				'Capital'=>'credit_account',
				'Drawing'=>'debit_account',
			),
			'Income'=>'credit_account',
			'Liabilities'=>'credit_account',
		];
		foreach ($Data as $key => $value) {
			//pr($value);
			$function_return=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
			$Data[$key]['AccountHead']['created_at']=date('d-m-Y',strtotime($value['AccountHead']['created_at']));
			$Data[$key]['AccountHead']['name']=$value['AccountHead']['name'];
			$type=$this->get_type_by_account_dead($value['AccountHead']['id']);
			$Type_name=$type['Type']['name'];
			$category_name=$category[$type['Type']['name']];
			if($Type_name=='Capital')
			{
				$category_name=$category[$type['Type']['name']][$type['Group']['name']];
			}
			$debit=$function_return['debit'];
			$credit=$function_return['credit'];
			if($category_name=='credit_account')
			{
				if($value['AccountHead']['opening_balance']<0) {
					$debit+=$value['AccountHead']['opening_balance']*-1;
				} else {
					$credit+=$value['AccountHead']['opening_balance'];
				}
			}
			else
			{
				//$debit+=$value['AccountHead']['opening_balance'];
				if($value['AccountHead']['opening_balance']<0)
				{
					$credit+=$value['AccountHead']['opening_balance']*-1;
				}
				else{
					$debit+=$value['AccountHead']['opening_balance'];
				}
			}
			if($category_name=='credit_account')
			{
				$Data[$key]['AccountHead']['total']=$credit;
				$Data[$key]['AccountHead']['total']-=$debit;
			}
			else
			{
				$Data[$key]['AccountHead']['total']=$debit;
				$Data[$key]['AccountHead']['total']-=$credit;
			}
			$Data[$key]['AccountHead']['total']=number_format($Data[$key]['AccountHead']['total'],3,'.','');
		
		}
		//sorting total/balance/recieved
		if($requestData['order'][0]['column'] >= 2) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["AccountHead"][$columnname] == $b["AccountHead"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return (str_replace(',','',$a["AccountHead"][$columnname]) < str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return (str_replace(',','',$a["AccountHead"][$columnname]) > str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
			}
			

			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
		//pr($Data);
		//exit;
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function VoucherContra($id=null)
	{
		// $PermissionList = $this->Session->read('PermissionList');
		// $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/VoucherGeneral'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/VoucherGeneral';
// 		$menu_id=$this->Menu->field('Menu.id');
// 	$menu_id = $this->Menu->field(
// 		'Menu.id',
// 		array('action ' => 'Accountings/VoucherContra'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	$cash_group_id=5;
	$bank_group_id=6;
	$user_id=1;
	$executive= $this->Executive->find('list',array('fields'=>'Executive.id,Executive.account_head_id',));

	//array_push($executive,1585);
		if(!$this->request->data)
		{
			 if(empty($id)){
			$AccountHeads=$this->AccountHead->find('all',array(
			'conditions'=>[
			'AccountHead.acc_sub_group_id'=>array(
				$cash_group_id,
				$bank_group_id,
				),
			'AccountHead.id !='=>$executive,

			],
			'order'=>array('AccountHead.name ASC'),
			'fields'=>[
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			]
			));
			//pr($AccountHeads);
			//exit;
			$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		}
		$this->set('AccountHead_list',$AccountHead_list);
			//$this->set('AccountHead_list',$AccountHeads);
			$data['Journal']['date']=date('d-m-Y');
			$data['Journal']['customer_type']=1;
			$data['from_date']=date('d-m-Y',strtotime('-1 month'));
			$data['to_date']=date('d-m-Y');
			$this->request->data=$data;
		  }
		  else
		  {
				$Journal=$this->Journal->findById($id);
				$Customer=$this->Customer->findByAccountHeadId($Journal['Journal']['credit']);
				$this->request->data=$Journal;
				$this->request->data['Journal']['date']=date('d-m-Y',strtotime($Journal['Journal']['date']));
					$this->request->data['Journal']['Journal_id']=$Journal['Journal']['id'];
				$this->request->data['Journal']['journal_no']=$Journal['Journal']['voucher_no'];
				  $this->request->data['Journal']['amount']=round($Journal['Journal']['amount'],2);
				    $this->request->data['Journal']['remarks']=$Journal['Journal']['remarks'];
				       $this->request->data['Journal']['external_voucher']=$Journal['Journal']['external_voucher'];
				// $this->request->data['Journal']['narration']=$Journal['Journal']['narration'];
				$this->request->data['Journal']['mode']=$Journal['Journal']['debit'];
				$this->request->data['Journal']['cheque_no']=$Journal['Journal']['cheque_no'];
				$this->request->data['Journal']['by_account']=$Journal['Journal']['credit'];
				 $this->request->data['Journal']['to_account']=$Journal['Journal']['debit'];
				 $AccountHeads=$this->AccountHead->find('all',array(
			'conditions'=>[
			'AccountHead.acc_sub_group_id'=>array(
				$cash_group_id,
				$bank_group_id,
				),
			'AccountHead.id !='=>$executive,

			],
			'order'=>array('AccountHead.name ASC'),
			'fields'=>[
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			]
			));
			//pr($AccountHeads);
			//exit;
			$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		}
		$this->set('AccountHead_list',$AccountHead_list);
				$contravoucherlist=$this->Journal->find('first',array(
					'joins'=>[
						array(
							'table'=>'account_heads',
							'alias'=>'AccountHead',
							'type'=>'LEFT',
							'conditions'=>array('AccountHead.id=Journal.debit')
							),
						],
					'conditions'=>['Journal.id'=>$id,'Journal.flag'=>1],
					'fields'=>array(
						'Journal.*',
						'AccountHead.name'
					)
				));
				 $this->set('contravoucherlist',$contravoucherlist);
		  
		  }
		}
		else
		{
			$datasource_Journal = $this->Journal->getDataSource();
			try {
				$datasource_Journal->begin();
				$data=$this->request->data['Journal'];
				$credit=$data['to_account'];
				$debit=$data['by_account'];
				if($credit==$debit)
					throw new Exception("Cant Use Same head", 1);
				$amount=$data['amount'];
				$date=$data['date'];
				$remarks=$data['remarks'];
				$voucher_no='';
				$external_voucher=$data['external_voucher'];
				$work_flow='Voucher Contra';
				$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
				if($function_return['result']!='Success')
					throw new Exception($function_return['message'], 1);
				$return['result']='Success';
				$datasource_Journal->commit();
				$this->Session->setFlash(__($return['result']));
			} catch (Exception $e) {
				$datasource_Journal->rollback();
				$return['result']='Error';
				$return['message']=$e->getMessage();
				$this->Session->setFlash(__($return['message']));
			}
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function VoucherContraList()
{
	$decimalpoint = 2;
	$this->set('decimalpoint',$decimalpoint);
	// $PermissionList = $this->Session->read('PermissionList');
	// $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Sale/InvoiceList'));

	// if(!in_array($menu_id, $PermissionList))
	// {
	//   $this->Session->setFlash("Permission denied");
	//   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
	// }
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');

	// $executives=$this->Executive->find('list',array(
	// 'fields'=>['Executive.id','Executive.name']
	// ));
	// $executives+=['0' => 'NO EXECUTIVE'];
	// $this->set(compact('executives'));
}
	public function VoucherContra_table_ajax()
	{
	$decimalpoint = 2;
	$user_branch_id=$this->Session->read('User.branch_id');
	$requestData=$this->request->data;
	$columns=[];
	$columns[]='Journal.date';
	$columns[]='Journal.id';
	$columns[]='Journal.id';
	$columns[]='Journal.id';
	$columns[]='Journal.id';
	$columns[]='Journal.narration';
	$columns[]='Journal.amount';
	$conditions=[];

	//order only fields that are field of datatabse table
	$order= '';
	if(!in_array($requestData['order'][0]['column'],[4,5]))
	{
		$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
	}
	if(isset($requestData['from_date']) && isset($requestData['to_date']) )
	{
		$conditions['Journal.date between ? and ?']=[
		date('Y-m-d',strtotime($requestData['from_date'])),
		date('Y-m-d',strtotime($requestData['to_date']))
		];
	}
	if($user_branch_id) $conditions['Journal.branch_id']=$user_branch_id;
	$conditions['Journal.flag']=1;
	$conditions['Journal.work_flow']= 'Voucher Contra';
	$totalData=$this->Journal->find('count',['conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
      $q=$requestData['search']['value'];
      $conditions['OR']=array(
        'Journal.date LIKE' =>'%'. $q . '%',
        'Journal.id LIKE' =>'%'. $q . '%',
        'Journal.narration LIKE' =>'%'. $q . '%',
        'Journal.amount LIKE' =>'%'. $q . '%',
      );
      $totalFiltered=$this->Journal->find('count',[
        'conditions'=>$conditions,
      ]);
    }
 // 'conditions' =>  array('not' => array('Sale.workflow'=>null)),

  	
    $Data=$this->Journal->find('all',array(
      'conditions'=>$conditions,
      'offset'=>$requestData['start'],
      'limit'=>$requestData['length'],
	  'order'=>$order,
	  'group' =>'Journal.voucher_no',
      'fields'=>array(
		'Journal.id',
		'Journal.date',
		'Journal.voucher_no',
		'Journal.remarks',
		'SUM(Journal.amount) as total',
		'AccountHeadCredit.name',
		'AccountHeadDebit.name'
      )
    ));
    //print_r($Data);die;
    foreach ($Data as $key => $value) {
		/*$Data[$key]['Sale']['action']='<span><a target="_blank" href="'.$this->webroot.'Print/fpdf/'.$value['Sale']['id'].'"><i class="fa fa-2x fa-print"></i></a></span>';*/
		//  $Data[$key]['Journal']['action']='<span><a  href="#" onclick="openInvoicePrint('.$value['Journal']['id'].')"><i class="fa fa-2x fa-print"></i></a></span>';
		$Data[$key]['Journal']['date']=date('d-m-Y',strtotime($value['Journal']['date']));
		$Data[$key]['Journal']['id']=$value['Journal']['voucher_no'];
		$Data[$key]['Journal']['remarks']=$value['Journal']['remarks'];
		$Data[$key]['Journal']['from_account']=$value['AccountHeadCredit']['name'];
		$Data[$key]['Journal']['to_account']=$value['AccountHeadDebit']['name'];
		$Data[$key]['Journal']['amount']=number_format($value[0]['total'],2,'.','');
	    $Data[$key]['Journal']['action']='&nbsp;&nbsp;<span><a target="_blank" href="'.$this->webroot.'Accountings/VoucherContra/'.$value['Journal']['id'].'"><i class="fa fa-2x fa-eye"></i></span></a>';
		if($this->Session->read('UserRole.id')==1)
    	$Data[$key]['Journal']['action'].='&nbsp;&nbsp;<span hidden class="table_id">'.$value['Journal']['id'].'</span><a href= "#"><span><i class="fa fa-2x fa-edit edit_icon payment_edit"></i></span></a>';
   }

  //sorting total/balance/recieved
  if(in_array($requestData['order'][0]['column'],[4,5])) {

    //callbackfunction
    function cmp($a, $b,$columnname,$columndir)
    {
      if ($a["Journal"][$columnname] == $b["Journal"][$columnname]) {
        return 0;
      }
      if($columndir == 'asc') {
        return (str_replace(',','',$a["Journal"][$columnname]) < str_replace(',','',$b["Journal"][$columnname])) ? -1 : 1;
      }
      else if($columndir == 'desc')
      {
        return (str_replace(',','',$a["Journal"][$columnname]) > str_replace(',','',$b["Journal"][$columnname])) ? -1 : 1;
      }
    }

    $columnname = $columns[$requestData['order'][0]['column']]; //column name
    $columndir = $requestData['order'][0]['dir']; //column order
    //sorting function
    usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
  }

  $json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData),
    "recordsFiltered"=>intval($totalFiltered),
    "records"        =>$Data
  );
   echo json_encode($json_data); exit;
}
	public function CurrentLiabilityShortTermLoanTransactions()
	{
		// $PermissionList = $this->Session->read('PermissionList');
		// $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/VoucherGeneral'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/VoucherGeneral';
// 		$menu_id=$this->Menu->field('Menu.id');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Accountings/CurrentLiabilityShortTermLoanTransactions'));
if(!in_array($menu_id, $PermissionList))
{
$this->Session->setFlash("Permission denied");
return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
}
	$user_id=1;
	$sub_group_id=18;
		if(!$this->request->data)
		{
		// 	$AccountHeads=$this->AccountHead->find('all',array(
		// 	'conditions'=>[
		// 	'SubGroup.group_id'=>array(
		// 		$cash_group_id,
		// 		$bank_group_id,
		// 		),
		// 	'AccountHead.id !='=>$executive,

		// 	],
		// 	'order'=>array('SubGroup.id DESC'),
		// 	'fields'=>[
		// 	'AccountHead.id',
		// 	'AccountHead.name',
		// 	'AccountHead.created_at',
		// 	'AccountHead.opening_balance',
		// 	]
		// 	));
		// 	//pr($AccountHeads);
		// 	//exit;
		// 	$AccountHead_list=[];
		// foreach ($AccountHeads as $key => $value) {
		// 	$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		// }
		// $this->set('AccountHead_list',$AccountHead_list);
			//$this->set('AccountHead_list',$AccountHeads);
		$data['Journal']['date']=date('d-m-Y');
		$data['from_date']=date('d-m-Y',strtotime('-1 month'));
		$data['to_date']=date('d-m-Y');
			$this->request->data=$data;
			$AccountHead_list=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('AccountHead',$AccountHead_list);
		$AccountHeads=$this->AccountHead_Table_ListBySubGroupId($sub_group_id);
		$bank=2;
		$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
		$modes=['1'];
		foreach ($bank_list as $key => $value) {
			array_push($modes, $key);
		}
		}
		else
		{
			$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$remarks=$data['remarks'];
			$type=$data['type'];
			if($type==1)
			{
				$credit=$data['mode'];
				$debit=$data['account_head'];	
			}
			else
			{
				$credit=$data['account_head'];
				$debit=$data['mode'];
			}
			$amount=$data['amount'];
			$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$date=$data['date'];
			$work_flow='ShortTermLoan';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$datasource_Journal->commit();
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$datasource_Journal->rollback();
			$this->Session->setFlash(__($return['message']));
		}
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function CurrentLiabilityShortTermLoanTransactions_table_ajax()
	{
		$sub_group_id=18;
	    $user_id=1;
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='AccountHead.name';
		$columns[]='AccountHead.created_at';
		$columns[]='AccountHead.opening_balance';
		$columns[]='AccountHead.opening_balance';
		$columns[]='AccountHead.opening_balance';
		$conditions=[];
		$conditions['SubGroup.id']=$sub_group_id;
		
		if(isset($requestData['account_head_id']))
	{
		if($requestData['account_head_id'])
			$conditions['AccountHead.id']=$requestData['account_head_id'];
	}
		$totalData=$this->AccountHead->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'AccountHead.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->AccountHead->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->AccountHead->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'AccountHead.*',
			)
		));
		$category=[
			'Asset'=>'debit_account',
			'Expense'=>'debit_account',
			'Capital'=>array(
				'Capital'=>'credit_account',
				'Drawing'=>'debit_account',
			),
			'Income'=>'credit_account',
			'Liabilities'=>'credit_account',
		];
		foreach ($Data as $key => $value) {
			$function_return=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
			$Data[$key]['AccountHead']['date']=date('d-m-Y',strtotime($value['AccountHead']['created_at']));
			$type=$this->get_type_by_account_dead($value['AccountHead']['id']);
			$Type_name=$type['Type']['name'];
			$category_name=$category[$type['Type']['name']];
			if($Type_name=='Capital')
			{
				$category_name=$category[$type['Type']['name']][$type['Group']['name']];
			}
			$debit=$function_return['debit'];
			$credit=$function_return['credit'];
			if($category_name=='credit_account')
			{
				//$credit+=$value['AccountHead']['opening_balance'];
				if($value['AccountHead']['opening_balance']<0)
				{
					$debit+=$value['AccountHead']['opening_balance']*-1;
				}
				else{
					$credit+=$value['AccountHead']['opening_balance'];
				}
			}
			else
			{
				//$debit+=$value['AccountHead']['opening_balance'];
				if($value['AccountHead']['opening_balance']<0)
				{
					$credit+=$value['AccountHead']['opening_balance']*-1;
				}
				else{
					$debit+=$value['AccountHead']['opening_balance'];
				}
			}
			if($category_name=='credit_account')
			{
				if(strtotime($Data[$key]['AccountHead']['date'])<strtotime($function_return['date']))
                 $Data[$key]['AccountHead']['date']=date('d-m-Y',strtotime($function_return['date']));
			 
				$Data[$key]['AccountHead']['received']=$credit;
				$Data[$key]['AccountHead']['paid']=$debit;
				$Data[$key]['AccountHead']['balance']=$credit;
				$Data[$key]['AccountHead']['balance']-=$debit;
			}
			else
			{
				if(strtotime($Data[$key]['AccountHead']['date'])<strtotime($function_return['date']))
                 $Data[$key]['AccountHead']['date']=date('d-m-Y',strtotime($function_return['date']));
			
				$Data[$key]['AccountHead']['received']=$debit;
				$Data[$key]['AccountHead']['paid']=$credit;
				$Data[$key]['AccountHead']['balance']=$debit;
				$Data[$key]['AccountHead']['balance']-=$credit;
			}
			$Data[$key]['AccountHead']['balance']=number_format($Data[$key]['AccountHead']['balance'],3,'.','');
		
		}
		//pr($Data);
		//exit;
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
public function ExpenseApproval()
			{
				$PermissionList = $this->Session->read('PermissionList');
			//	$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Accountings/expense_approval'));
				$from_date = date('Y-m-d',strtotime('first day of this month'));
				$ExecutiveList=$this->Executive->find('list',array('fields'=>array('id','name')));
				$this->set(compact('ExecutiveList'));
				$to_date = date('Y-m-d');
				$executive_id="";
				// if(!in_array($menu_id, $PermissionList))
				// {
				// 	$this->Session->setFlash("Permission denied");
				// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
				// }
				$Executive_expense=$this->ExecutiveExpense_function($executive_id,$from_date,$to_date);
				
				$this->set(compact('Executive_expense'));
			}
			public function ExecutiveExpense_function($executive_id,$from_date,$to_date)
			{
				$conditions=[];
				$from_date = date('Y-m-d',strtotime($from_date));
				$to_date = date('Y-m-d',strtotime($to_date));
				if(!empty($executive_id)){
					$conditions['ExecutiveExpenseDetail.executive_id']=$executive_id;
				}
				$conditions['ExecutiveExpenseDetail.date between ? and ?']=array($from_date,$to_date);
				$ExecutiveExpense = $this->ExecutiveExpenseDetail->find('all', array(
					'conditions' => $conditions,
					'order' => array('ExecutiveExpenseDetail.id DESC'),
					'fields' => array(
				)
				));
				return $ExecutiveExpense;
			}
			public function expense_search_ajax(){
				$data=$this->request->data;
				$first_date = date('Y-m-d',strtotime('first day of this month'));
				$to_date = date('Y-m-d',strtotime('last day of this month'));
				$executive_id="";
				if(!empty($data['from_date'])){
					$first_date = date('Y-m-d',strtotime($data['from_date']));
				}
				if(!empty($data['to_date'])){
					$to_date = date('Y-m-d',strtotime($data['to_date']));
				}
				if(!empty($data['executive'])){
					$executive_id=$data['executive'];
				}
				$ExecutiveExpense_list=$this->ExecutiveExpense_function($executive_id,$first_date,$to_date);
				echo json_encode($ExecutiveExpense_list);
				exit;
			}
				public function executive_expense_update_ajax($id=null,$approved_amount=null)
				{

					if($approved_amount)
					{
							$this->ExecutiveExpenseDetail->id=$id;
							if(!$this->ExecutiveExpenseDetail->saveField('approved_amount',$approved_amount))
								throw new Exception("Cant Updated approved_amount", 1);
						$return['result']='Success';

					}
					echo json_encode($return);exit;

				}
			public function expense_approval_ajax() {
				$return['result']='Error';
				if($this->request->data){
					$data=$this->request->data;
					$total_approved_amount=0;
					$total_amount=0;
					$datasource_Journal = $this->Journal->getDataSource();
					try {
						$datasource_Journal->begin();
						if($data['value']==2){
							$ExecutiveExpense=$this->ExecutiveExpenseDetail->findById($data['id']);
							$route_account_head_id=$this->Route->findById($ExecutiveExpense['ExecutiveExpenseDetail']['route_id']);
							$credit=$route_account_head_id['Route']['account_head_id'];
							$debit=$ExecutiveExpense['AccountHead']['id'];
							$amount=$ExecutiveExpense['ExecutiveExpenseDetail']['approved_amount'];
							$remarks=$ExecutiveExpense['ExecutiveExpenseDetail']['remarks'];
							$date=$ExecutiveExpense['ExecutiveExpenseDetail']['date'];
							$work_flow='ExpenseApproval';
							$user_id=1;
						   $function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,'','','','',$ExecutiveExpense['ExecutiveExpenseDetail']['executive_id'],$route_account_head_id['Route']['id']);
							if($function_return['result']!='Success')
								throw new Exception($function_return['message'], 1);
							$this->ExecutiveExpenseDetail->id=$data['id'];	
							$ExecutiveExpenseDetail_data['status']=2;
							if(!$this->ExecutiveExpenseDetail->save($ExecutiveExpenseDetail_data))
								throw new Exception("Cant Updated ExecutiveExpenseDetail", 1);
						}
						else
						{
							$this->ExecutiveExpenseDetail->id=$data['id'];	
							$ExecutiveExpenseDetail_data['status']=3;
							if(!$this->ExecutiveExpenseDetail->save($ExecutiveExpenseDetail_data))
								throw new Exception("Cant Updated ExecutiveExpenseDetail", 1);

						}
								
						$datasource_Journal->commit();
						$return['result']='Success';
					} catch (Exception $e) {
						$return['result']='Error';
						$return['message']=$e->getMessage();
					}
				}
				echo json_encode($return);
				exit;
			}
			public function GetSubGroupIdByGroupIdAndSubGroupName($group_id,$sub_group_name)
		{
			$SubGroup=$this->SubGroup->findByName($sub_group_name);
			if(!$SubGroup)
			{
				$function_return=$this->General_SubGroup_add_function($sub_group_name,$group_id);
				if($function_return['result']!='Success')
					throw new Exception($function_return['result']);
				$sub_group_id=$this->SubGroup->getLastInsertId();
			}
			else
			{
				$sub_group_id=$SubGroup['SubGroup']['id'];
			}
			return $sub_group_id;
		}
		public function update_expense_app_show(){
	$user_id=1;
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	$data=$this->request->data;
	try {
		$datasource_AccountHead->begin();
		$this->AccountHead->id=$data['id'];
		if(!$this->AccountHead->saveField('show_in_app',$data['show_in_app'])) throw new Exception("Error Processing Request in show_in_app", 1);
		$return['result']='Success';
		$datasource_AccountHead->commit();
	} catch (Exception $e) {
		$datasource_AccountHead->rollback();
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
}