<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
ini_set('memory_limit', '1024M');
class AccountsController extends AppController {
	public $uses=array(
		'AccMainGroup',
		'AccSubGroup',
		'AccLedger',
		'AccountHead',
		'Sale',
		'Purchase',
		'AssetPurchase',
		'Journal',
		'Customer',
		'Party',
		'CustomerType',
		'CustomerGroup',
		'Route',
		'BranchRouteMapping',
		'Customer',
		'State',
		'District',
		'Country',
		'SystemParameter',
		'CostCenter',
		'CostCenterDetail',
		'Menu',
		'JournalVoucher',
		'JournalVoucherDetail',
		'Executive',
		'Staff',
		'Order',
'ExecutiveRouteMapping',

		// 'Brand',
		// 'Service',
		// 'ProductTypeBrandMapping',
		// 'Product',
		// 'Category',
		// 'SubBrandCategoryMapping',
		// //'ProductTypeCategoryMapping',
		// 'ProductType'
	);
	public function main_group_search()
	{
		$main_group_name=$this->request->data['main_group_name'];
		//$main_group_code=$this->request->data['main_group_code'];
		//$category_name=trim($category_name);
		$AccMainGroup=$this->AccMainGroup->find('first',array('conditions'=>array('AccMainGroup.name'=>$main_group_name)));
		if(!empty($AccMainGroup)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}
	public function MainGroup(){
		$AccMainGroup = $this->AccMainGroup->find('list', array('fields' => array('id','name')));
		$this->set('AccMainGroup',$AccMainGroup);
		$AccMainGroups = $this->AccMainGroup->find('all', array('order'=>array('AccMainGroup.id DESC'),'fields' => array('id','name','code')));
		$this->set('AccMainGroups',$AccMainGroups);
		//$main_code = $this->AccMainGroup->find('first', array('order'=>array('AccMainGroup.id DESC'),'fields' => array('code')));
	 // if(!empty($main_code))
  //   {
  //     $MainGroupCode = $main_code['AccMainGroup']['code'];
  //     $code=$MainGroupCode+1;
  //   }
  //   else
  //   {
  //     $code=001;
  //   }
  //   $data['MainGroup']['code']=$code;
  //   $this->request->data=$data;
	}
	public function add_main_group_ajax() {
      $datasource_AccMainGroup = $this->AccMainGroup->getDataSource();
        try {
        	$datasource_AccMainGroup->begin();
            $data = $this->request->data;
            //pr($data);
           // exit;
            $name = $data['name'];
        $MainGroupCode = $this->AccMainGroup->find('first', array(
                'order' => array('AccMainGroup.id' => 'DESC') ));
            if(!empty($MainGroupCode))
            {
                $main_code = $MainGroupCode['AccMainGroup']['code']+1;
            }
            else{
                $main_code = 001;
            }
            $code=sprintf("%03s",$main_code);
        // pr($customer_code);
//exit;
            // if(!empty($CustomerRow))
            // {
            //     // $customer_code = $CustomerRow[0]['code']+1;
            // }
            // else{
            //     $customer_code = 001;
            // }
           // $code = $data['code'];

            $main_group_data = array(
                'name' => strtoupper($name),
                'code' => $code,
                'created_at'=>date('Y-m-d h:i:s'),
				'updated_at'=>date('Y-m-d h:i:s'),
            );

            $this->AccMainGroup->create();
            if (!$this->AccMainGroup->save($main_group_data))
                throw new Exception("Error In Main Group Create", 1);
            $datasource_AccMainGroup->commit();
            $return['result'] = 'Success';
            //$main_group_id = $this->AccMainGroup->getLastInsertId();
           // $main_group = $this->AccMainGroup->findById($main_group_id);
            
        } catch (Exception $e) {
        	//pr($e)
        	$datasource_AccMainGroup->rollback();
            $return['result'] = $e->getMessage();
        }
        echo json_encode($return);
        exit;
    }
	public function main_group_add_ajax()
	{
		$data=array(
			'name'=>strtoupper($this->request->data['modal_main_group_name']),
			'code'=>strtoupper($this->request->data['modal_main_group_code']),
			'created_at'=>date('Y-m-d h:i:s'),
			'updated_at'=>date('Y-m-d h:i:s'),
			
			);
		$this->AccMainGroup->create();
		if($this->AccMainGroup->save($data))
		{
			$this->Session->setFlash('data is saved');
			$last_iserted_pt=$this->AccMainGroup->findById($this->AccMainGroup->getLastInsertId());
			echo "<option value='".$last_iserted_pt['AccMainGroup']['id']."'>".$last_iserted_pt['AccMainGroup']['name']." </option>";
		}
		exit;
	}
	public function add_main_group_ajax_modal()
	{
		$return = ['result' => 'Empty'];
		try {
 $data = $this->request->data;
            $name = $data['name'];
             //$code = $data['code'];
$main_code = $this->AccMainGroup->find('first', array('order'=>array('AccMainGroup.id DESC'),'fields' => array('code')));
	 if(!empty($main_code))
    {
      $MainGroupCode = $main_code['AccMainGroup']['code'];
      $code=$MainGroupCode+1;
    }
    else
    {
      $code=001;
    }
            $main_group_data=array(
			'name'=>strtoupper($name),
			'code'=>$code,
			'created_at'=>date('Y-m-d h:i:s'),
			'updated_at'=>date('Y-m-d h:i:s'),
			
			);
			$this->AccMainGroup->create();
			if(!$this->AccMainGroup->save($main_group_data))
				throw new Exception("AccMainGroup Creation Error", 1);
			$last_id=$this->AccMainGroup->getLastInsertId();
			$AccMainGroup=$this->AccMainGroup->findById($last_id);
			$return['key']=$AccMainGroup['AccMainGroup']['id'];
			$return['value']=$AccMainGroup['AccMainGroup']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	// public function edit_ajax(){
	// 	try {
	// 		$prdct_id=$this->request->data['prdct_id'];
	// 		if(!$prdct_id)
	// 			throw new Exception("Empty ProductType Id", 1);
	// 		$prdct_typ=strtoupper(trim($this->request->data['prdct_typ']));
	// 		$this->ProductType->id=$prdct_id;
	// 		if(!$this->ProductType->saveField('name',$prdct_typ ))
	// 			throw new Exception("Error Processing ProductType Name Updation", 1);
	// 		if(!$this->ProductType->saveField('updated_at',date('Y-m-d H:i:s')))
	// 			throw new Exception("Error Processing ProductType updated_at Updation", 1);
	// 		$return['result']='Success';
	// 	} catch (Exception $e) {
	// 		$return['result']=$e->getMessage();
	// 	}
	// 	echo json_encode($return);
	// 	exit;
	// }
	// public function warehouse_type_delete_ajax($id)
	// {
	// 	try {
	// 		$Product=$this->Product->findByProductTypeId($id);
	// 		if($Product)
	// 			throw new Exception("This is Used In ".$Product['Product']['name'], 1);
	// 		if(!$this->ProductType->delete($id))
	// 			throw new Exception("Error Processing Request While delete", 1);
	// 		$return['result']='Success';
	// 	} catch (Exception $e) {
	// 		$return['result']=$e->getMessage();
	// 	}
	// 	echo json_encode($return);
	// 	exit;
	// }
	public function main_group_get_ajax($id)
	{
		try {
			$AccMainGroup=$this->AccMainGroup->findById($id);
			if(!$AccMainGroup)
				throw new Exception("Empty Category", 1);
			$return['data']=$AccMainGroup['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	// public function route_get_ajax($id)
	// {
	// 	try {
	// 		$Location=$this->Route->findById($id);
	// 		if(!$Location)
	// 			throw new Exception("Empty Location", 1);
	// 		$return['data']=$Location['Route'];
	// 		$return['result']='Success';
	// 	} catch (Exception $e) {
	// 		$return['result']=$e->getMessage();
	// 	}
	// 	echo json_encode($return);
	// 	exit;
	// }
	public function main_group_edit_ajax()
	{
		try {
			$data=$this->request->data['MainGroupEdit'];
			$Table_data=array(
				'name'=>trim(strtoupper($data['name'])),
				'code'=>trim(strtoupper($data['code'])),
				'updated_at'=>date('Y-m-d h:i:s'),
				);
			$this->AccMainGroup->id=$data['id'];
			if(!$this->AccMainGroup->save($Table_data))
			{
				$errors = $this->AccMainGroup->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$AccMainGroup=$this->Category->read();
			$return['key']=$AccMainGroup['AccMainGroup']['id'];
			$return['value']=$AccMainGroup['AccMainGroup']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function edit_main_group_ajax(){
		try {
			$id=$this->request->data['id'];
			if(empty($id))
				throw new Exception("Empty Category Id");
			$name=strtoupper(trim($this->request->data['name']));
			// if(empty($name))
			// 	throw new Exception("Empty Category Name");
			$code=strtoupper(trim($this->request->data['code']));
			// if(empty($name))
			// 	throw new Exception("Empty Category Name");

			
			$tableData = [
				'name'=>$name,
				'code'=>$code,
				'updated_at'=>date('Y-m-d h:i:s'),
			];
			$this->AccMainGroup->id=$id;
			if(!$this->AccMainGroup->save($tableData))
				throw new Exception("Error In Main Group Request");
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();			
		}
		echo json_encode($return);
		exit;
	}

	public function SubGroup()
	{
		$AccMainGroup = $this->AccMainGroup->find('list', array('fields' => array('id','name')));
		$this->set('AccMainGroup',$AccMainGroup);
		 // $SubGroupCode = $this->AccSubGroup->find('first', array(
   //              'order' => array('AccSubGroup.id' => 'DESC') ));
   //          if(!empty($SubGroupCode))
   //          {
   //              $sub_code = $SubGroupCode['AccSubGroup']['code']+1;
   //          }
   //          else{
   //              $sub_code = 0001;
   //          }
   //          $code=sprintf("%04s",$sub_code);
   //          $data['SubGroup']['code']=$code;
   //          $this->request->data=$data;
	}
	public function get_sub_group_code_ajax($id) {
        $return = ['result' => 'Empty'];
        $AccSubGroup = $this->AccSubGroup->find('first', array(
               'order' => array('AccSubGroup.id' => 'DESC'),
               	'conditions'=>array('AccSubGroup.main_group_id'=>$id) ));
        if ($AccSubGroup) {
        	$sub_code = $AccSubGroup['AccSubGroup']['code']+1;

        }else{
        	$sub_code = 0001;

        }
           $code=sprintf("%04s",$sub_code);
          // pr($code);
          // exit;
           $return['result'] = 'Success';
           $return['code'] = $code;
        echo json_encode($return);
        exit;
    }
	public function SubGroup_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		//$columns[]='id';
		$columns[]='AccMainGroup.name';
		$columns[]='AccSubGroup.name';
		$columns[]='AccSubGroup.code';
		$columns[]='AccSubGroup.id';
		$conditions=[];
		$totalData=$this->AccSubGroup->find('count',[
			'joins'=>array(
				array(
					"table"=>'acc_main_groups',
					"alias"=>'AccMainGroup',
					"type"=>'inner',
					"conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
				),
			),
			'conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'AccSubGroup.name LIKE' =>'%'. $q . '%',
				'AccMainGroup.name LIKE' =>'%'. $q . '%',
				'AccSubGroup.code LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->AccSubGroup->find('count',[
				'joins'=>array(
				array(
					"table"=>'acc_main_groups',
					"alias"=>'AccMainGroup',
					"type"=>'inner',
					"conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
				),
			),
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->AccSubGroup->find('all',array(
			'joins'=>array(
				array(
					"table"=>'acc_main_groups',
					"alias"=>'AccMainGroup',
					"type"=>'inner',
					"conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
				),
			),
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'AccSubGroup.*',
				'AccMainGroup.*',
			)
		));
		//$sl_no=1;
		$start=$requestData['start'];
		$sl_no=$start+1;
		foreach ($Data as $key => $value) {
			$Data[$key]['AccSubGroup']['id']=$sl_no++;
			$Data[$key]['AccMainGroup']['name']=ucwords(strtolower($value['AccMainGroup']['name']));
			$Data[$key]['AccSubGroup']['name']=ucwords(strtolower($value['AccSubGroup']['name']));
			$Data[$key]['AccSubGroup']['action'] ='<span><i table_id="'.$value['AccSubGroup']['id'].'" class="fa fa-2x fa-edit   edit_SubGroup   blue-col"></i></span>&nbsp;&nbsp;';
			//$Data[$key]['AccSubGroup']['action'] .="<span><i table_id='".$value['AccSubGroup']['id']."' class='fa fa-2x fa-trash delete_SubGroup blue-col'></i></span>";
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function SubGroup_Save_ajax()
	{
		try {
			$data=$this->request->data['SubGroup'];
			$data['name']=$data['name'];
			//$data['main_group_id']=$data['MainGroup'];
			if(!$data['id'])
			{
				$data['main_group_id']=$data['MainGroup'];
				$Table_data=array(
				'name'=>$data['name'],
				'code'=>$data['code'],
				'main_group_id'=>$data['main_group_id'],
				'created_at'=>date('Y-m-d h:i:s'),
				'updated_at'=>date('Y-m-d h:i:s'),
				);
				$this->AccSubGroup->create();
				if(!$this->AccSubGroup->save($Table_data))
				{
					$errors = $this->AccSubGroup->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
			   $return['result']='Success';
				// $id=$this->AccSubGroup->getLastInsertId();
				// $SubGroup=$this->AccSubGroup->findById($id);
				// $sub_group_id=$SubCategory['AccSubCategory']['id'];
			// $CategorySubCategoryMapping=$this->CategorySubCategoryMapping->find('first',array(
			// 	'conditions'=>array(
					
			// 		'category_id'=>$data['category_id'],
			// 		'sub_category_id'=>$sub_category_id,
			// 	)
			// ));
			// if(empty($CategorySubCategoryMapping))
			// {
			// 	$value=array(
					
			// 		'category_id'=>''.$data['category_id'].'',
			// 		'sub_category_id'=>''.$sub_category_id.'',
			// 	);
			// 	$this->CategorySubCategoryMapping->create();
			// 	if(!$this->CategorySubCategoryMapping->save($value))
			// 		throw new Exception("Error Processing In BrandMapping Creation", 1);
			// 	$return['result']='Success';
			// }
			}
			else
			{
				//pr($data);
				//exit;
				$this->AccSubGroup->id=$data['id'];
				$this->AccSubGroup->saveField('name',$data['name']);
				$this->AccSubGroup->saveField('code',$data['code']);
				$this->AccSubGroup->saveField('updated_at',date('Y-m-d h:i:s'));
			}
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function SubGroup_type_get_ajax($id)
	{
		try {
			$SubGroup=$this->AccSubGroup->find('first',array(
			'joins'=>array(
				array(
					"table"=>'acc_main_groups',
					"alias"=>'AccMainGroup',
					"type"=>'inner',
					"conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
				),
			),
			'conditions'=>array('AccSubGroup.id'=>$id),
			// 'offset'=>$requestData['start'],
			// 'limit'=>$requestData['length'],
			// 'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'AccSubGroup.*',
				'AccMainGroup.*',
			)
		));
		//pr($SubGroup);
		//exit;
			if(!$SubGroup)
				throw new Exception("Empty SubGroup", 1);
			$return['AccMainGroup']=$SubGroup['AccMainGroup'];
		    $return['AccSubGroup']=$SubGroup['AccSubGroup'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function Delete_SubGroup_ajax($id)
	{
		$userid = $this->Session->read('User.id');

		if($this->AccSubGroup->delete($id))
		{

			$return['result']="Success";
		}
		else
		{
			$return['result']="Can't delete this SubGroup";
		}
		echo json_encode($return);
		exit;
	}
public function Ledger()
{
	$user_branch_id=$this->Session->read('User.branch_id');
	$PermissionList = $this->Session->read('PermissionList');
	$user_id=1;
	if(!$this->request->data)
	{

		$from_date=date('d-m-Y',strtotime('-1 month'));
			$to_date=date('d-m-Y');
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			//$this->request->data=$data;

		$CustomerType_list=$this->CustomerType->find('list',array('fields'=>array('id','name')));
 		$Ledger_list=$this->AccLedger->find('list',array('fields'=>array('id','name')));
 		$this->set('Ledger_list',$Ledger_list);
 		$SubGroup_list=$this->AccSubGroup->find('list',array('fields'=>array('id','name'),'order' => array('name' => 'ASC')));
 		$this->set('SubGroup_list',$SubGroup_list);
 		$MainGroup_list=$this->AccMainGroup->find('list',array('fields'=>array('id','name')));
 		$this->set('MainGroup_list',$MainGroup_list);
 		 $State_list=$this->State->find('list',array('fields'=>array('id','name')));
		 $this->set('State_list',$State_list);
		  $District_list=$this->District->find('list',array('fields'=>array('id','name')));
		 $this->set('District_list',$District_list);
		  $Country_list=$this->Country->find('list',array('fields'=>array('id','name')));
		 $this->set('Country_list',$Country_list);
		 $Status=[
		'1'=>'Active',
		'0'=>'Inactive',
		
		// '3'=>'Expense',
		// '4'=>'Income',
		// '5'=>'Liabilities',
		];
		$this->set('Status',$Status);
		 $cost_center=[
		'1'=>'Yes',
		'0'=>'No',
		
		// '3'=>'Expense',
		// '4'=>'Income',
		// '5'=>'Liabilities',
		];
		$this->set('cost_center',$cost_center);
		// $AccountHead_list=$this->AccountHead->find('all',array('joins' => [
		// 	[
		// 	"table" => "customers",
		// 	"alias" => "Customer",
		// 	"type" => "INNER",
		// 	"conditions" => ['AccountHead.id=Customer.account_head_id'],
		// 	],
		// 	],'fields'=>array('AccountHead.id','AccountHead.name','Customer.code'),'conditions'=>array('sub_group_id'=>3)));
		// $Customer_list=[];
		// foreach ($AccountHead_list as $key => $value) {
		// 	$Customer_list[$value['AccountHead']['id']] = $value['AccountHead']['name'].' '.$value['Customer']['code'];
		// }
		// $Executive_list=$this->Executive->find('list',array('fields'=>array('id','name')));
		// $this->set('Executive_list',$Executive_list);
		$this->Route->virtualFields = array(
			'route_name' => "CONCAT(Route.name, ' ', Route.code)"
			);
		$conditions_branch_route=[];

		if($user_branch_id)
		{
			$route_id=$this->BranchRouteMapping->find('list',array('conditions'=>array('BranchRouteMapping.branch_id'=>$user_branch_id),'fields'=>['BranchRouteMapping.id','BranchRouteMapping.route_id']));
			$conditions_branch_route['Route.id']=$route_id;
		}
		// $this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
		// $Product_list=$this->Product->find('list',array('fields'=>['id','product_name']));
		// $this->set(compact('Product_list'));
		 $Route_list=$this->Route->find('list',array('conditions'=>$conditions_branch_route,'fields'=>array('id','route_name')));
		 $this->set(compact('Route_list'));
		 $CustomerGroup_list=$this->CustomerGroup->find('list',array('fields'=>array('id','name')));
		 $this->set(compact('CustomerGroup_list'));
		// $Division_list=$this->Division->find('list');
		// $this->set(compact('Division_list'));
// $State_list=$this->State->find('list',array('fields'=>array('id','name')));
// $this->set(compact('State_list'));
// $Day_list=array(
//      'Monday'=>'Monday',
//      'Tuesday'=>'Tuesday',
//      'Wednesday'=>'Wednesday',
//      'Thursday'=>'Thursday',
//      'Friday'=>'Friday',
//      'Saturday'=>'Saturday',
//      'Sunday'=>'Sunday',
//      );
//    $this->set(compact('Day_list'));
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
	//	$data['Ledger']['opening_balance']='0';
		//$conditions_branch=[];

		if($user_branch_id)
		{
			$route_id=$this->BranchRouteMapping->find('list',array('conditions'=>array('BranchRouteMapping.branch_id'=>$user_branch_id),'fields'=>['BranchRouteMapping.id','BranchRouteMapping.route_id']));
			$conditions_branch['Customer.route_id']=$route_id;
		}
		$user_cost=$this->Session->read('SystemParameter.cost_center_id');
		$this->set('user_cost',$user_cost);
	    //$data['Ledger']['c_center']=$user_cost;
		$this->request->data=$data;
// 		$AccLedger=$this->AccLedger->find('all',array(
// 			"joins"=>array(
//                 array(
//                     "table"=>'acc_sub_groups',
//                     "alias"=>'AccSubGroup',
//                     "type"=>'left',
//                     "conditions"=>array('AccLedger.sub_group_id=AccSubGroup.id'),
//                 ),
//                 array(
//                     "table"=>'acc_main_groups',
//                     "alias"=>'AccMainGroup',
//                     "type"=>'left',
//                     "conditions"=>array('AccLedger.main_group_id=AccMainGroup.id'),
//                 ),
//                 array(
//                     "table"=>'states',
//                     "alias"=>'State',
//                     "type"=>'left',
//                     "conditions"=>array('AccLedger.state=State.id'),
//                 ),
//                 array(
//                     "table"=>'districts',
//                     "alias"=>'District',
//                     "type"=>'left',
//                     "conditions"=>array('AccLedger.district=District.id'),
//                 ),
//                 array(
//                     "table"=>'countries',
//                     "alias"=>'Country',
//                     "type"=>'left',
//                     "conditions"=>array('AccLedger.country=Country.id'),
//                 ),
//             ),
// 			'fields'=>array('AccLedger.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
// 			//'order'=>array('AccountHead.name ASC'),
// // 'limit'=>110,
// 			));
// 		// pr($AccLedger);
// 		// exit;
// 		$Ledger_All=[];
// 		foreach ($AccLedger as $key => $value) {
// 			$single_row['sub_group']= $value['AccSubGroup']['name'];
// 			$single_row['sub_group_id']= $value['AccLedger']['sub_group_id'];
// 			$single_row['main_group']= $value['AccMainGroup']['name'];
// 			$single_row['address']= $value['AccLedger']['address'];
// 			$single_row['district']= $value['District']['name'];
// 			$single_row['pin_code']= $value['AccLedger']['pin_code'];
// 			$single_row['state']= $value['State']['name'];
// 			$single_row['name']= $value['AccLedger']['name'];
// 			if($value['AccLedger']['status']==1)
// 			{
// 			$single_row['status']='Active';
// 			}
// 			elseif($value['AccLedger']['status']==0)
// 			{
//              $single_row['status']="Inactive";

// 			}
// 			$single_row['code']= $value['AccLedger']['code'];
// 			$single_row['country']= $value['Country']['name'];
// 			$single_row['telephone']= $value['AccLedger']['telephone'];
// 			$single_row['email']= $value['AccLedger']['email'];
// 			$single_row['website']= $value['AccLedger']['website'];
// 			$single_row['contact_person']= $value['AccLedger']['contact_person'];
// 			$single_row['contact_no']= $value['AccLedger']['contact_no'];
// 			$single_row['gstin']= $value['AccLedger']['gstin'];
// 			$single_row['credit_limit']= $value['AccLedger']['credit_limit'];
// 			$single_row['credit_period']= $value['AccLedger']['credit_period'];
// 			$single_row['opening_balance']= $value['AccLedger']['opening_balance'];
// 			$single_row['other_notes']= $value['AccLedger']['other_notes'];
// 			$single_row['id']= $value['AccLedger']['id'];
// 			array_push($Ledger_All, $single_row);
// 		}
// 		$CUstomer=$this->Customer->find('all',array(
// 			"joins"=>array(
//                 array(
//                     "table"=>'acc_sub_groups',
//                     "alias"=>'AccSubGroup',
//                     "type"=>'left',
//                     "conditions"=>array('Customer.sub_group_id=AccSubGroup.id'),
//                 ),
//                 array(
//                     "table"=>'acc_main_groups',
//                     "alias"=>'AccMainGroup',
//                     "type"=>'left',
//                     "conditions"=>array('Customer.main_group_id=AccMainGroup.id'),
//                 ),
//                 array(
//                     "table"=>'states',
//                     "alias"=>'State',
//                     "type"=>'left',
//                     "conditions"=>array('Customer.state_id=State.id'),
//                 ),
//                 array(
//                     "table"=>'districts',
//                     "alias"=>'District',
//                     "type"=>'left',
//                     "conditions"=>array('Customer.district=District.id'),
//                 ),
//                 array(
//                     "table"=>'countries',
//                     "alias"=>'Country',
//                     "type"=>'left',
//                     "conditions"=>array('Customer.country=Country.id'),
//                 ),
//             ),
// 			'fields'=>array('Customer.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
// 			//'order'=>array('AccountHead.name ASC'),
// // 'limit'=>110,
// 			));
// 		// pr($CUstomer);
// 		// exit;
// 		//$Ledger_All=[];
// 		foreach ($CUstomer as $key1 => $value1) {
// 			$single1_row['sub_group']= $value1['AccSubGroup']['name'];
// 			$single1_row['sub_group_id']= $value1['Customer']['sub_group_id'];
// 			$single1_row['main_group']= $value1['AccMainGroup']['name'];
// 			$single1_row['address']= $value1['Customer']['place'];
// 			$single1_row['district']= $value1['District']['name'];
// 			$single1_row['pin_code']= $value1['Customer']['pin_code'];
// 			$single1_row['state']= $value1['State']['name'];
// 			$single1_row['name']= $value1['Customer']['name'];
// 			if($value1['Customer']['status']==1)
// 			{
// 			$single1_row['status']='Active';
// 			}
// 			elseif($value1['Customer']['status']==0)
// 			{
//              $single1_row['status']="Inactive";

// 			}
// 			$single1_row['code']= $value1['Customer']['code'];
// 			$single1_row['country']= $value1['Country']['name'];
// 			$single1_row['telephone']= $value1['Customer']['telephone'];
// 			$single1_row['email']= $value1['Customer']['email'];
// 			$single1_row['website']= $value1['Customer']['website'];
// 			$single1_row['contact_person']= $value1['Customer']['contact_person'];
// 			$single1_row['contact_no']= $value1['Customer']['contact_no'];
// 			$single1_row['gstin']= $value1['Customer']['gstin'];
// 			$single1_row['credit_limit']= $value1['Customer']['credit_limit'];
// 			$single1_row['credit_period']= $value1['Customer']['credit_period'];
// 			$single1_row['opening_balance']= $value1['Customer']['opening_balance'];
// 			$single1_row['other_notes']= $value1['Customer']['other_notes'];
// 			$single1_row['id']= $value1['Customer']['id'];
// 			array_push($Ledger_All, $single1_row);
// 		}
//                $this->Party->unbindModel(array('belongsTo'=>array('State')));
// 				$PArty=$this->Party->find('all',array(
// 			"joins"=>array(
//                 array(
//                     "table"=>'acc_sub_groups',
//                     "alias"=>'AccSubGroup',
//                     "type"=>'left',
//                     "conditions"=>array('Party.sub_group_id=AccSubGroup.id'),
//                 ),
//                 array(
//                     "table"=>'acc_main_groups',
//                     "alias"=>'AccMainGroup',
//                     "type"=>'left',
//                     "conditions"=>array('Party.main_group_id=AccMainGroup.id'),
//                 ),
//                 array(
//                     "table"=>'states',
//                     "alias"=>'State',
//                     "type"=>'left',
//                     "conditions"=>array('Party.state_id=State.id'),
//                 ),
//                 array(
//                     "table"=>'districts',
//                     "alias"=>'District',
//                     "type"=>'left',
//                     "conditions"=>array('Party.district_id=District.id'),
//                 ),
//                 array(
//                     "table"=>'countries',
//                     "alias"=>'Country',
//                     "type"=>'left',
//                     "conditions"=>array('Party.country_id=Country.id'),
//                 ),
//             ),
// 			'fields'=>array('Party.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
// 			//'order'=>array('AccountHead.name ASC'),
// // 'limit'=>110,
// 			));
// 		// pr($CUstomer);
// 		// exit;
// 		//$Ledger_All=[];
// 		foreach ($PArty as $key2 => $value2) {
// 			$single2_row['sub_group']= $value2['AccSubGroup']['name'];
// 			$single2_row['sub_group_id']= $value2['Party']['sub_group_id'];
// 			$single2_row['main_group']= $value2['AccMainGroup']['name'];
// 			$single2_row['address']= $value2['Party']['address'];
// 			$single2_row['district']= $value2['District']['name'];
// 			$single2_row['pin_code']= $value2['Party']['pin_code'];
// 			$single2_row['state']= $value2['State']['name'];
// 			$single2_row['name']= $value2['Party']['name'];
// 			if($value2['Party']['status']==1)
// 			{
// 			$single2_row['status']='Active';
// 			}
// 			elseif($value2['Party']['status']==0)
// 			{
//              $single2_row['status']="Inactive";

// 			}
// 			$single2_row['code']= $value2['Party']['code'];
// 			$single2_row['country']= $value2['Party']['name'];
// 			$single2_row['telephone']= $value2['Party']['land_line'];
// 			$single2_row['email']= $value2['Party']['email'];
// 			$single2_row['website']= $value2['Party']['website'];
// 			$single2_row['contact_person']= $value2['Party']['contact_person'];
// 			$single2_row['contact_no']= $value2['Party']['contact_no'];
// 			$single2_row['gstin']= $value2['Party']['gstin'];
// 			$single2_row['credit_limit']= $value2['Party']['credit_limit'];
// 			$single2_row['credit_period']= $value2['Party']['credit_period'];
// 			$single2_row['opening_balance']= $value2['Party']['opening_balance'];
// 			$single2_row['other_notes']= $value2['Party']['other_notes'];
// 			$single2_row['id']= $value2['Party']['id'];
// 			array_push($Ledger_All, $single2_row);
// 		}
		$modalCustomerType_list = $CustomerType_list;
// 		unset($modalCustomerType_list[1]);
 		$this->set('CustomerType_list',$CustomerType_list);
 		$this->set('modalCustomerType_list',$modalCustomerType_list);
// 		$this->set('Customer_list',$Customer_list);
// // $this->set('Customer',$Customer);
 		//$this->set('Ledger',$Ledger_All);
	}

}
public function add_sub_group_ajax_modal()
	{
		$return = ['result' => 'Empty'];
		try {
 $data = $this->request->data;
            $main_group_id = $data['main_group_id'];
            $name = $data['name'];
             //$code = $data['code'];
              $AccSubGroup = $this->AccSubGroup->find('first', array(
               'order' => array('AccSubGroup.id' => 'DESC'),
               	'conditions'=>array('AccSubGroup.main_group_id'=>$main_group_id) ));
        if ($AccSubGroup) {
        	$sub_code = $AccSubGroup['AccSubGroup']['code']+1;

        }else{
        	$sub_code = 0001;

        }
           $code=sprintf("%04s",$sub_code);

            $sub_group_data=array(
			'name'=>strtoupper($name),
			'code'=>strtoupper($code),
			'main_group_id'=>$main_group_id,
			'created_at'=>date('Y-m-d h:i:s'),
			'updated_at'=>date('Y-m-d h:i:s'),
			
			);
			$this->AccSubGroup->create();
			if(!$this->AccSubGroup->save($sub_group_data))
				throw new Exception("AccSubGroup Creation Error", 1);
			$last_id=$this->AccSubGroup->getLastInsertId();
			$AccSubGroup=$this->AccSubGroup->findById($last_id);
			$return['key']=$AccSubGroup['AccSubGroup']['id'];
			$return['value']=$AccSubGroup['AccSubGroup']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
public function Ledger_delete_ajax($id,$subgroup_id)
{
	$return['result']="Cant Delete";
	$customer_id=$this->SystemParameter->field('value',array('id'=>11));
            $vendor_id=$this->SystemParameter->field('value',array('id'=>12));
             
		if($subgroup_id==$customer_id)
		{
			$account_head=$this->Customer->findById($id);
		$account_head_id=$account_head['Customer']['account_head_id'];
				$JournalCredit=$this->Journal->findByCredit($account_head_id);
				if(empty($JournalCredit))
				{
                    $JournalDebit=$this->Journal->findByDebit($account_head_id);
                    if(empty($JournalDebit))
                    {
                    	 $JournalVoucher=$this->JournalVoucherDetail->findByAccountHeadId($account_head_id);
                    	 if(empty( $JournalVoucher))
                    	 {
                    	 	$Order=$this->Order->findByAccountHeadId($account_head_id);
		                    	 if(empty($Order))
		                    	 {
								if(!$this->AccountHead->delete($account_head_id))
									throw new Exception("Cant Delete This AccountHead", 1);
								if(!$this->Customer->delete($id))
									throw new Exception("Cant Delete This Customer", 1);
								$return['result']="Success";
							       }
                    	 }
                    }
				}
		}
		echo json_encode($return);
	exit;
}
public function Ledger_get_ajax($id,$subgroup_id)
{
	//pr($subgroup_id);
	//exit;
	$return['result']='Error';
 $customer_id=$this->SystemParameter->field('value',array('id'=>11));
            $vendor_id=$this->SystemParameter->field('value',array('id'=>12));

            if($subgroup_id==$customer_id){
            	$Customer=$this->Customer->find('first',array(
	"joins"=>array(
        array(
                 "table"=>'acc_sub_groups',
                 "alias"=>'AccSubGroup',
                  "type"=>'left',
                  "conditions"=>array('Customer.sub_group_id=AccSubGroup.id'),
              ),
         array(
                  "table"=>'acc_main_groups',
                  "alias"=>'AccMainGroup',
                  "type"=>'left',
                  "conditions"=>array('Customer.main_group_id=AccMainGroup.id'),
                ),
            ),
		'conditions'=>array('Customer.id'=>$id),
		'fields'=>array(
			'Customer.*',
			'AccSubGroup.*',
			'AccMainGroup.*',
			)
		));
            	//pr($Customer);
            	//exit;
if(!empty($Customer))
	{
       $return['name']=$Customer['Customer']['name'];
       $return['code']=$Customer['Customer']['code'];
       $return['opening_balance']=$Customer['Customer']['opening_balance'];
       $return['sub_group_id']=$Customer['Customer']['sub_group_id'];
       $return['main_group_id']=$Customer['Customer']['main_group_id'];
       $return['customer_group_id']=$Customer['Customer']['customer_group_id'];
       $return['customer_type_id']=$Customer['Customer']['customer_type_id'];
       $return['route_id']=$Customer['Customer']['route_id'];
       $return['district']=$Customer['Customer']['district'];
       $return['pin_code']=$Customer['Customer']['pin_code'];
       $return['state']=$Customer['Customer']['state_id'];
       $return['country']=$Customer['Customer']['country'];
       $return['email']=$Customer['Customer']['email'];
       $return['website']=$Customer['Customer']['website'];
       $return['telephone']=$Customer['Customer']['telephone'];
        $return['mobile']=$Customer['Customer']['mobile'];
       $return['address']=$Customer['Customer']['place'];
       $return['other_notes']=$Customer['Customer']['other_notes'];
       $return['contact_person']=$Customer['Customer']['contact_person'];
       $return['contact_no']=$Customer['Customer']['contact_no'];
       $return['credit_limit']=$Customer['Customer']['credit_limit'];
       $return['credit_period']=$Customer['Customer']['credit_period'];
       $return['gstin']=$Customer['Customer']['gstin'];
       $return['status']=$Customer['Customer']['status'];
       $return['cost_center']=$Customer['Customer']['cost_center'];
       $return['AccountHead_id']=$Customer['Customer']['id'];
       //$return['name']=$Customer['Customer']['name'];
		//$return['data']=$Customer;
		//$return['discount_data']=$Customer_discounts;
		//$return['result']='Success';	
	}
	// else
	// {
	// 	$return['message']='Empty';
	// }

            }
            elseif($subgroup_id==$vendor_id){
            	$Party=$this->Party->find('first',array(
	"joins"=>array(
        array(
                 "table"=>'acc_sub_groups',
                 "alias"=>'AccSubGroup',
                  "type"=>'left',
                  "conditions"=>array('Party.sub_group_id=AccSubGroup.id'),
              ),
         array(
                  "table"=>'acc_main_groups',
                  "alias"=>'AccMainGroup',
                  "type"=>'left',
                  "conditions"=>array('Party.main_group_id=AccMainGroup.id'),
                ),
            ),
		'conditions'=>array('Party.id'=>$id),
		'fields'=>array(
			'Party.*',
			'AccSubGroup.*',
			'AccMainGroup.*',
			)
		));
if(!empty($Party))
	{
	   $return['name']=$Party['Party']['name'];
       $return['code']=$Party['Party']['code'];
       $return['opening_balance']=$Party['Party']['opening_balance'];
       $return['sub_group_id']=$Party['Party']['sub_group_id'];
       $return['main_group_id']=$Party['Party']['main_group_id'];
       $return['district']=$Party['Party']['district_id'];
       $return['pin_code']=$Party['Party']['pin_code'];
       $return['state']=$Party['Party']['state_id'];
       $return['country']=$Party['Party']['country_id'];
       $return['email']=$Party['Party']['email'];
       $return['website']=$Party['Party']['website'];
       $return['address']=$Party['Party']['place'];
       $return['other_notes']=$Party['Party']['other_notes'];
       $return['contact_person']=$Party['Party']['contact_person'];
       $return['contact_no']=$Party['Party']['contact_no'];
       $return['telephone']=$Party['Party']['telephone'];
       $return['credit_limit']=$Party['Party']['credit_limit'];
       $return['credit_period']=$Party['Party']['credit_period'];
       $return['gstin']=$Party['Party']['gstin'];
       $return['status']=$Party['Party']['status'];
       $return['cost_center']=$Party['Party']['cost_center'];
       $return['AccountHead_id']=$Party['Party']['id'];
		//$return['data']=$Party;
		//$return['discount_data']=$Customer_discounts;
		//$return['result']='Success';	
	}
	// else
	// {
	// 	$return['message']='Empty';
	// }

            }
            else{
	$AccLedger=$this->AccLedger->find('first',array(
	"joins"=>array(
        array(
                 "table"=>'acc_sub_groups',
                 "alias"=>'AccSubGroup',
                  "type"=>'left',
                  "conditions"=>array('AccLedger.sub_group_id=AccSubGroup.id'),
              ),
         array(
                  "table"=>'acc_main_groups',
                  "alias"=>'AccMainGroup',
                  "type"=>'left',
                  "conditions"=>array('AccLedger.main_group_id=AccMainGroup.id'),
                ),
            ),
		'conditions'=>array('AccLedger.id'=>$id),
		'fields'=>array(
			'AccLedger.*',
			'AccSubGroup.*',
			'AccMainGroup.*',

			)
		));
	//pr($AccLedger);
	//exit;
if(!empty($AccLedger))
	{
	   $return['name']=$AccLedger['AccLedger']['name'];
       $return['code']=$AccLedger['AccLedger']['code'];
       $return['opening_balance']=$AccLedger['AccLedger']['opening_balance'];
       $return['sub_group_id']=$AccLedger['AccLedger']['sub_group_id'];
       $return['main_group_id']=$AccLedger['AccLedger']['main_group_id'];
       $return['district']=$AccLedger['AccLedger']['district'];
       $return['pin_code']=$AccLedger['AccLedger']['pin_code'];
       $return['state']=$AccLedger['AccLedger']['state'];
       $return['country']=$AccLedger['AccLedger']['country'];
       $return['email']=$AccLedger['AccLedger']['email'];
       $return['website']=$AccLedger['AccLedger']['website'];
       $return['telephone']=$AccLedger['AccLedger']['telephone'];
       $return['address']=$AccLedger['AccLedger']['address'];
       $return['other_notes']=$AccLedger['AccLedger']['other_notes'];
       $return['contact_person']=$AccLedger['AccLedger']['contact_person'];
       $return['contact_no']=$AccLedger['AccLedger']['contact_no'];
       $return['credit_limit']=$AccLedger['AccLedger']['credit_limit'];
       $return['credit_period']=$AccLedger['AccLedger']['credit_period'];
       $return['gstin']=$AccLedger['AccLedger']['gstin'];
       $return['status']=$AccLedger['AccLedger']['status'];
       $return['cost_center']=$AccLedger['AccLedger']['cost_center'];
       $return['AccountHead_id']=$AccLedger['AccLedger']['id'];

		//$return['data']=$AccLedger;
		//$return['discount_data']=$Customer_discounts;
		//$return['result']='Success';	
	}
	// else
	// {
	// 	$return['message']='Empty';
	// }
	// $Customer_discounts=$this->CustomerDiscount->find('all',array(
	// 	'conditions'=>array('account_head_id'=>$id),
	// 	));
}
	$return['result']='Success';
//pr($return);exit;
	echo json_encode($return);
	exit;
}
public function Ledger_account_edit()
{
	$user_id=1;
	$datasource_AccLedger = $this->AccLedger->getDataSource();
	$datasource_Party = $this->Party->getDataSource();
	$datasource_Customer = $this->Customer->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_AccLedger->begin();
		$datasource_AccountHead->begin();
		$Ledger_data=$this->request->data['LedgerEdit'];
// pr($Ledger_data);
// exit;
		$name=$Ledger_data['name'];
		$code=$Ledger_data['code'];
		//$arabic_name_edit=$AccountHead_data['arabic_name'];
		//$arabic_name_arabic_text=trim(strtoupper($arabic_name_edit));

		$AccountHead_id=$Ledger_data['AccountHead_id'];
		$opening_balance=0;
		if(!empty($Ledger_data['opening_balance']))
		{
				$opening_balance=$Ledger_data['opening_balance'];	
		}
		$date=date('Y-m-d H:i:s');
		$description='';
		// $account_head=$this->AccLedger->findById($AccountHead_id);
		// $account_head_id=$account_head['AccLedger']['account_head_id'];
		$address=$Ledger_data['address'];
		 $website = $Ledger_data['website'];
            $contact_no = $Ledger_data['contact_no'];
           // $telephone = $Ledger_data['telephone'];
          //  $description = '';
            $district='';
			if(!empty($Ledger_data['district']))
			{
			$district = $Ledger_data['district'];
			}
            $pin_code = $Ledger_data['pin_code'];
            $state = $Ledger_data['state'];
            $country = $Ledger_data['country'];
            $status = $Ledger_data['status'];
           // $cost_center = $Ledger_data['cost_center'];
            $cost_center="";
		if(empty($Ledger_data['route_id']))
		{
			$route_id='';
		}
		else
		{
			$route_id=$Ledger_data['route_id'];
		}
		if(empty($Ledger_data['customer_group_id']))
		{
			$customer_group_id='';
		}
		else
		{
			$customer_group_id=$Ledger_data['customer_group_id'];
		}
		if(empty($Ledger_data['customer_type_id']))
		{
			$customer_type_id='';
		}
		else
		{
		$customer_type_id=$Ledger_data['customer_type_id'];
	}
		$email=$Ledger_data['email'];
		$telephone=$Ledger_data['telephone'];
		//$customer_discount=$AccountHead_data['collection_edit'];
		$contact_person=$Ledger_data['contact_person'];
		$credit_limit=$Ledger_data['credit_limit'];

		$credit_period=$Ledger_data['credit_period'];
		$division="";
		$gstin=$Ledger_data['gstin'];
		$other_notes=$Ledger_data['other_notes'];
		$sub_group_id=$Ledger_data['sub_group_id'];
		$main_group_ids=$Ledger_data['main_group_id'];
		$main=$this->AccMainGroup->findByName($main_group_ids);
		$main_group_id=$main['AccMainGroup']['id'];
		 $customer_id=$this->SystemParameter->field('value',array('id'=>11));
         $vendor_id=$this->SystemParameter->field('value',array('id'=>12));
         if($sub_group_id==$customer_id){

         	$account_head=$this->Customer->findById($AccountHead_id);
		$account_head_id=$account_head['Customer']['account_head_id'];
		$function_return=$this->AccountHeadEdit($account_head_id,$name,$opening_balance,$date,$description,$sub_group_id);

		if($function_return['result']!='Success')
			throw new Exception($function_return['message']);
         if(!empty($name)){
			$Ledger_data['name']= $name;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('name',$name))
				throw new Exception("Cant Updated Name", 1);
		}
		if(!empty($code)){
			$Ledger_data['code']= $code;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('code',$code))
				throw new Exception("Cant Updated Code", 1);
		}
		if(!empty($main_group_id)){
			$Ledger_data['main_group_id']= $main_group_id;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('main_group_id',$main_group_id))
				throw new Exception("Cant Updated Main Group", 1);
		}
		if(!empty($sub_group_id)){
			$Ledger_data['sub_group_id']= $sub_group_id;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('sub_group_id',$sub_group_id))
				throw new Exception("Cant Updated Sub Group", 1);
		}
		if(!empty($customer_type_id)){
			$Ledger_data['customer_type_id']= $customer_type_id;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('customer_type_id',$customer_type_id))
				throw new Exception("Cant Updated Customer Type", 1);
		}
		if(!empty($customer_group_id)){
			$Ledger_data['customer_group_id']= $customer_group_id;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('customer_group_id',$customer_group_id))
				throw new Exception("Cant Updated Customer Group", 1);
		}
		if(!empty($route_id)){
			$Ledger_data['route_id']= $route_id;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('route_id',$route_id))
				throw new Exception("Cant Updated Route", 1);
		}
		if(!empty($address)){
			$Ledger_data['address']= $address;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('place',$address))
				throw new Exception("Cant Updated Address", 1);
		}
		if(!empty($district)){
			$Ledger_data['district']= $district;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('district',$district))
				throw new Exception("Cant Updated District", 1);
		}
		if(!empty($pin_code)){
			$Ledger_data['pin_code']= $pin_code;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('pin_code',$pin_code))
				throw new Exception("Cant Updated Pin Code", 1);
		}
		if(!empty($state)){
			$Ledger_data['state']= $state;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('state_id',$state))
				throw new Exception("Cant Updated State", 1);
		}
		if(!empty($country)){
			$Ledger_data['country']= $country;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('country',$country))
				throw new Exception("Cant Updated Country", 1);
		}

		
		if(!empty($email)){
			$Ledger_data['email']= $email;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('email',$email))
				throw new Exception("Cant Updated Email", 1);
		}
		if(!empty($website)){
			$Ledger_data['website']= $website;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('website',$website))
				throw new Exception("Cant Updated Website", 1);
		}
		if(!empty($telephone)){
			$Ledger_data['telephone']= $telephone;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('telephone',$telephone))
				throw new Exception("Cant Updated Telephone", 1);
		}
		if(!empty($contact_person)){
			$Ledger_data['contact_person']= $contact_person;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('contact_person',$contact_person))
				throw new Exception("Cant Updated Contact person", 1);
		}
		if(!empty($contact_no)){
			$Ledger_data['contact_no']= $contact_no;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('contact_no',$contact_no))
				throw new Exception("Cant Updated Contact No", 1);
		}
		
			if(!empty($gstin)){
			$Ledger_data['gstin']= $gstin;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('gstin',$gstin))
				throw new Exception("Cant Updated GSTIN/CR", 1);
		}
		if(!empty($gstin)){
			$Ledger_data['gstin']= $gstin;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('vat_no',$gstin))
				throw new Exception("Cant Updated vat_no/CR", 1);
		}
		 if(!empty($credit_limit)){

			$Ledger_data['credit_limit']= $credit_limit;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('credit_limit',$credit_limit))
				throw new Exception("Cant Updated Credit limit", 1);
		 }
		if(!empty($credit_period)){
			$Ledger_data['credit_period']= $credit_period;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('credit_period',$credit_period))
				throw new Exception("Cant Updated Credit period", 1);
		}
		if(!empty($opening_balance)){
			$Ledger_data['opening_balance']= $opening_balance;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('opening_balance',$opening_balance))
				throw new Exception("Cant Updated Opening Balance", 1);
		}
if(!empty($other_notes)){
			$Ledger_data['other_notes']= $other_notes;
			$this->Customer->id=$AccountHead_id;
			if(!$this->Customer->saveField('other_notes',$other_notes))
				throw new Exception("Cant Updated Other Notes", 1);
		}

		// if(!empty($status)){
		// 	$Ledger_data['status']= $status;
		// 	$this->AccLedger->id=$AccountHead_id;
		// 	if(!$this->AccLedger->saveField('status',$status))
		// 		throw new Exception("Cant Updated Status", 1);
		// }
		if(!$this->Customer->saveField('status',$status))
				throw new Exception("Cant Updated Status", 1);
			
		//  if(!empty($cost_center)){
		//  	$Ledger_data['cost_center']= $cost_center;
		//  	$this->Customer->id=$AccountHead_id;
		// 	if(!$this->Customer->saveField('cost_center',$cost_center))
		// 		throw new Exception("Cant Updated Cost Center", 1);
		// }
		$Ledger_data['updated_at']= date('Y-m-d H:i:s',strtotime($date));
		$this->Customer->id=$AccountHead_id;
		if(!$this->Customer->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date))))
			throw new Exception("Cant Updated ", 1);

         }
         elseif($sub_group_id==$vendor_id){
         	$account_head=$this->Party->findById($AccountHead_id);
		$account_head_id=$account_head['Party']['account_head_id'];
         	$AccountingsController = new AccountingsController;
		$function_return=$AccountingsController->AccountHeadEdit($account_head_id,$name,$opening_balance,$date,$description);

		if($function_return['result']!='Success')
			throw new Exception($function_return['message']);

         	if(!empty($name)){
			$Ledger_data['name']= $name;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('name',$name))
				throw new Exception("Cant Updated Name", 1);
		}
		if(!empty($code)){
			$Ledger_data['code']= $code;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('code',$code))
				throw new Exception("Cant Updated Code", 1);
		}
		if(!empty($main_group_id)){
			$Ledger_data['main_group_id']= $main_group_id;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('main_group_id',$main_group_id))
				throw new Exception("Cant Updated Main Group", 1);
		}
		if(!empty($sub_group_id)){
			$Ledger_data['sub_group_id']= $sub_group_id;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('sub_group_id',$sub_group_id))
				throw new Exception("Cant Updated Sub Group", 1);
		}
		if(!empty($addresss)){
			$Ledger_data['address']= $address;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('address',$address))
				throw new Exception("Cant Updated Address", 1);
		}
		if(!empty($district)){
			$Ledger_data['district']= $district;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('district_id',$district))
				throw new Exception("Cant Updated District", 1);
		}
		if(!empty($pin_code)){
			$Ledger_data['pin_code']= $pin_code;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('pin_code',$pin_code))
				throw new Exception("Cant Updated Pin Code", 1);
		}
		if(!empty($state)){
			$Ledger_data['state']= $state;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('state_id',$state))
				throw new Exception("Cant Updated State", 1);
		}
		if(!empty($country)){
			$Ledger_data['country']= $country;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('country_id',$country))
				throw new Exception("Cant Updated Country", 1);
		}

		
		if(!empty($email)){
			$Ledger_data['email']= $email;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('email',$email))
				throw new Exception("Cant Updated Email", 1);
		}
		if(!empty($website)){
			$Ledger_data['website']= $website;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('website',$website))
				throw new Exception("Cant Updated Website", 1);
		}
		if(!empty($telephone)){
			$Ledger_data['telephone']= $telephone;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('telephone',$telephone))
				throw new Exception("Cant Updated Telephone", 1);
		}
		if(!empty($contact_person)){
			$Ledger_data['contact_person']= $contact_person;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('contact_person',$contact_person))
				throw new Exception("Cant Updated Contact person", 1);
		}
		if(!empty($contact_no)){
			$Ledger_data['contact_no']= $contact_no;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('contact_no',$contact_no))
				throw new Exception("Cant Updated Contact No", 1);
		}
		
			if(!empty($gstin)){
			$Ledger_data['gstin']= $gstin;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('gstin',$gstin))
				throw new Exception("Cant Updated GSTIN/CR", 1);
		}
		if(!empty($gstin)){
			$Ledger_data['gstin']= $gstin;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('vat_no',$gstin))
				throw new Exception("Cant Updated vat_no/CR", 1);
		}
		 if(!empty($credit_limit)){

			$Ledger_data['credit_limit']= $credit_limit;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('credit_limit',$credit_limit))
				throw new Exception("Cant Updated Credit limit", 1);
		 }
		if(!empty($credit_period)){
			$Ledger_data['credit_period']= $credit_period;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('credit_period',$credit_period))
				throw new Exception("Cant Updated Credit period", 1);
		}
		if(!empty($opening_balance)){
			$Ledger_data['opening_balance']= $opening_balance;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('opening_balance',$opening_balance))
				throw new Exception("Cant Updated Opening Balance", 1);
		}
if(!empty($other_notes)){
			$Ledger_data['other_notes']= $other_notes;
			$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('other_notes',$other_notes))
				throw new Exception("Cant Updated Other Notes", 1);
		}

		// if(!empty($status)){
		// 	$Ledger_data['status']= $status;
		// 	$this->AccLedger->id=$AccountHead_id;
		// 	if(!$this->AccLedger->saveField('status',$status))
		// 		throw new Exception("Cant Updated Status", 1);
		// }
		if(!$this->Party->saveField('status',$status))
				throw new Exception("Cant Updated Status", 1);
		// if(!empty($cost_center)){
		// 	$Ledger_data['cost_center']= $cost_center;
		// 	$this->Party->id=$AccountHead_id;
			if(!$this->Party->saveField('cost_center',$cost_center))
				throw new Exception("Cant Updated Cost Center", 1);
		//}
		$Ledger_data['updated_at']= date('Y-m-d H:i:s',strtotime($date));
		$this->Party->id=$AccountHead_id;
		if(!$this->Party->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date))))
			throw new Exception("Cant Updated ", 1);

         }
         else{
         	$account_head=$this->AccLedger->findById($AccountHead_id);
		$account_head_id=$account_head['AccLedger']['account_head_id'];
         	//$AccountingsController = new AccountingsController;
		$function_return=$this->AccountHeadEdit($account_head_id,$name,$opening_balance,$date,$description,$sub_group_id);

		if($function_return['result']!='Success')
			throw new Exception($function_return['message']);
		if(!empty($name)){
			$Ledger_data['name']= $name;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('name',$name))
				throw new Exception("Cant Updated Name", 1);
		}
		if(!empty($code)){
			$Ledger_data['code']= $code;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('code',$code))
				throw new Exception("Cant Updated Code", 1);
		}
		if(!empty($main_group_id)){
			$Ledger_data['main_group_id']= $main_group_id;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('main_group_id',$main_group_id))
				throw new Exception("Cant Updated Main Group", 1);
		}
		if(!empty($sub_group_id)){
			$Ledger_data['sub_group_id']= $sub_group_id;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('sub_group_id',$sub_group_id))
				throw new Exception("Cant Updated Sub Group", 1);
		}
		if(!empty($addresss)){
			$Ledger_data['address']= $address;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('address',$address))
				throw new Exception("Cant Updated Address", 1);
		}
		if(!empty($district)){
			$Ledger_data['district']= $district;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('district',$district))
				throw new Exception("Cant Updated District", 1);
		}
		if(!empty($pin_code)){
			$Ledger_data['pin_code']= $pin_code;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('pin_code',$pin_code))
				throw new Exception("Cant Updated Pin Code", 1);
		}
		if(!empty($state)){
			$Ledger_data['state']= $state;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('state',$state))
				throw new Exception("Cant Updated State", 1);
		}
		if(!empty($country)){
			$Ledger_data['country']= $country;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('country',$country))
				throw new Exception("Cant Updated Country", 1);
		}

		
		if(!empty($email)){
			$Ledger_data['email']= $email;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('email',$email))
				throw new Exception("Cant Updated Email", 1);
		}
		if(!empty($website)){
			$Ledger_data['website']= $website;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('website',$website))
				throw new Exception("Cant Updated Website", 1);
		}
		if(!empty($telephone)){
			$Ledger_data['telephone']= $telephone;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('telephone',$telephone))
				throw new Exception("Cant Updated Telephone", 1);
		}
		if(!empty($contact_person)){
			$Ledger_data['contact_person']= $contact_person;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('contact_person',$contact_person))
				throw new Exception("Cant Updated Contact person", 1);
		}
		if(!empty($contact_no)){
			$Ledger_data['contact_no']= $contact_no;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('contact_no',$contact_no))
				throw new Exception("Cant Updated Contact No", 1);
		}
		
			if(!empty($gstin)){
			$Ledger_data['gstin']= $gstin;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('gstin',$gstin))
				throw new Exception("Cant Updated GSTIN/CR", 1);
		}
		 if(!empty($credit_limit)){

			$Ledger_data['credit_limit']= $credit_limit;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('credit_limit',$credit_limit))
				throw new Exception("Cant Updated Credit limit", 1);
		 }
		if(!empty($credit_period)){
			$Ledger_data['credit_period']= $credit_period;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('credit_period',$credit_period))
				throw new Exception("Cant Updated Credit period", 1);
		}
		if(!empty($opening_balance)){
			$Ledger_data['opening_balance']= $opening_balance;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('opening_balance',$opening_balance))
				throw new Exception("Cant Updated Opening Balance", 1);
		}
if(!empty($other_notes)){
			$Ledger_data['other_notes']= $other_notes;
			$this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('other_notes',$other_notes))
				throw new Exception("Cant Updated Other Notes", 1);
		}

		// if(!empty($status)){
		// 	$Ledger_data['status']= $status;
		// 	$this->AccLedger->id=$AccountHead_id;
		// 	if(!$this->AccLedger->saveField('status',$status))
		// 		throw new Exception("Cant Updated Status", 1);
		// }
		if(!$this->AccLedger->saveField('status',$status))
				throw new Exception("Cant Updated Status", 1);

		// if(!$this->AccLedger->saveField('status',$status))
		// 		throw new Exception("Cant Updated Status", 1);

	  //  if(!empty($cost_center)){
			// $Ledger_data['cost_center']= $cost_center;
			// $this->AccLedger->id=$AccountHead_id;
			if(!$this->AccLedger->saveField('cost_center',$cost_center))
				throw new Exception("Cant Updated Cost Center", 1);
		//}
		$Ledger_data['updated_at']= date('Y-m-d H:i:s',strtotime($date));
		$this->AccLedger->id=$AccountHead_id;
		if(!$this->AccLedger->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date))))
			throw new Exception("Cant Updated ", 1);
	}
		// if(isset($AccountHead_data['products'])){
		// 	for ($i=0; $i < count($AccountHead_data['products']); $i++) { 
		// 		if(isset($AccountHead_data['discount_id'][$i])){
		// 			$this->CustomerDiscount->id=$AccountHead_data['discount_id'][$i];
		// 			if(!$this->CustomerDiscount->saveField('selling_rate',$AccountHead_data['selling_rates'][$i]))
		// 				throw new Exception("Cant Updated Customer Selling Rate", 1);
		// 		}
		// 		else
		// 		{
		// 			$new_data=array(
		// 				'account_head_id'=>$AccountHead_id,
		// 				'product_id'=>$AccountHead_data['products'][$i],
		// 				'selling_rate'=>$AccountHead_data['selling_rates'][$i],
		// 				'created_at'=>date('Y-m-d'),
		// 				'updated_at'=>date('Y-m-d')
		// 				);
		// 			$this->CustomerDiscount->create();
		// 			if(!$this->CustomerDiscount->save($new_data))
		// 				throw new Exception("Error Processing Request", 1);		
		// 		}
		// 	}
		// }
	$datasource_Customer->commit();
	$datasource_Party->commit();
	//$datasource_AccLedger->commit();
		$datasource_AccLedger->commit();
		$datasource_AccountHead->commit();
		$return['result']='Success';
		$this->Session->setFlash(__($return['result']));
	} catch (Exception $e) {
		$datasource_Customer->rollback();
		$datasource_Party->rollback();
		$datasource_AccLedger->rollback();
		$datasource_AccountHead->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		$this->Session->setFlash(__($return['message']));
	}
	echo json_encode($return);
	exit;
}
public function Ledger_delete($id)
{
	try {
		$AccLedger=$this->AccLedger->findById($id);
		$account_head_id=$AccLedger['AccLedger']['account_head_id'];
		$id=$id;
		$excluding_ids=['1','2','3','4','5','6','7','8','9','10','11','12','13',];
		if(in_array($account_head_id, $excluding_ids))
			throw new Exception("You Cant Delete This Account Head", 1);
		$JournalCredit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.credit'=>$account_head_id,'flag=1'),
			));
		if($JournalCredit)
			throw new Exception("Used In Journal Debit", 1);
		$JournalDebit=$this->Journal->find('first',array(
			'conditions'=>array('Journal.debit'=>$account_head_id,'flag=1'),
			));
		if($JournalDebit)
			throw new Exception("Used In Journal Credit", 1);
		if($this->Purchase->findByAccountHeadId($account_head_id))
			throw new Exception("Used In Purchase", 1);
		if($this->AssetPurchase->findByAccountHeadId($account_head_id))
			throw new Exception("Used In AssetPurchase", 1);
		if($this->Sale->findByAccountHeadId($account_head_id))
			throw new Exception("Used In Sale", 1);
		$AccLedger=$this->AccLedger->findById($id);
		if(!empty($AccLedger))
		{
			if(!$this->AccLedger->delete($id))
				throw new Exception("Cant Delete This Ledger", 1);
		}
		if(!$this->AccountHead->delete($account_head_id))
			throw new Exception("Cant Delete This AccountHead", 1);

		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	$this->Session->setFlash(__($return['result']));
	$this->redirect( Router::url( $this->referer(), true ) );
}
public function update_ledger_name()
{
	$Route=$this->Route->find('all',array(
		'conditions'=>array(
			)
		));
	if(!empty($Route))
	{
		foreach ($Route as $key => $value) {
			 $AccountHead_id =$value['Route']['account_head_id'];
				$this->AccountHead->id=$AccountHead_id;
				$name=$value['Route']['name'].'_CASH';
				if(!$this->AccountHead->saveField('name',$name))
					throw new Exception("Error Processing in Updation", 1);
		}
	}
	exit;
}
public function add_ledger_ajax() {
       // $sub_group_id = 3;
        $user_id = 1;
        $datasource_AccLedger = $this->AccLedger->getDataSource();
        $datasource_AccountHead = $this->AccountHead->getDataSource();
        $datasource_Customer = $this->Customer->getDataSource();
        try {
            $datasource_AccLedger->begin();
            $datasource_AccountHead->begin();
            $datasource_Customer->begin();
            $Ledger_data = $this->request->data['Ledger'];
          // pr($Ledger_data);
               // exit;
                         $name = trim($Ledger_data['name']);
            if (empty($name))
                throw new Exception("Empty Ledger Name", 1);
            $sub_group_id = $Ledger_data['sub_group_id'];
               if ($sub_group_id == '' || empty($sub_group_id))
                throw new Exception("Empty Sub Group", 1);
            $main_group_ids = $Ledger_data['main_group_id'];
            $main=$this->AccMainGroup->findByName($main_group_ids);
            $main_group_id=$main['AccMainGroup']['id'];            
            $opening_balance =0;
            if(!empty($Ledger_data['opening_balance']))
            {
            $opening_balance =$Ledger_data['opening_balance'];
            }
          //  $route_id = $Customer_data['route_id'];
            $division_id = $Ledger_data['division_id'];
            // if ($customer_group_id == '')
            //     throw new Exception("Empty Customer Group", 1);
            // if ($route_id == '')
            //     throw new Exception("Empty Route", 1);
            if ($division_id == '')
                $division_id=1;
                //throw new Exception("Empty Division", 1);
            // $TypeCode = $this->CustomerType->field(
            //     'CustomerType.code',
            //     array('CustomerType.id ' => $Ledger_data['customer_type_id']));
            // $code = $TypeCode;
             $LedgerRow = $this->AccLedger->find('first', array(
                'order' => array('AccLedger.id' => 'DESC') ));
            if(!empty($LedgerRow))
            {
                $ledger_code = $LedgerRow['AccLedger']['code']+1;
            }
            else{
                $ledger_code = 1001;
            }
            $CustomerRow = $this->Customer->find('first', array(
                'order' => array('Customer.id' => 'DESC') ));
            if(!empty($CustomerRow))
            {
                $customer_code = $CustomerRow['Customer']['code']+1;
            }
            else{
                $customer_code = 1001;
            }
            $PartyRow = $this->Party->find('first', array(
				'order' => array('Party.id' => 'DESC') ));
			if(!empty($PartyRow))
			{
				$PrevCodeString = $PartyRow['Party']['code'];
				$PrevCode = str_replace('VR', '', $PrevCodeString);
				$Ncode = $PrevCode + 1;
				$party_code = 'VR'.$Ncode;
			}
			else{
				$party_code = 'VR1000';
			}
            //$customer_code=1002;
            $code = $Ledger_data['code'];
            $date = date('Y-m-d');
            $address = $Ledger_data['address'];
            $email = $Ledger_data['email'];
            $website = $Ledger_data['website'];
            $contact_no = $Ledger_data['contact_no'];
            $telephone = $Ledger_data['telephone'];
            $description = '';
            $district = $Ledger_data['district'];
            $pin_code = $Ledger_data['pin_code'];
            $state = $Ledger_data['state'];
            $country = $Ledger_data['country'];
            $status = $Ledger_data['status'];
            $route_id = $Ledger_data['route_id'];
            // $cost_center = $Ledger_data['cost_center'];
                        	$cost_center='';
            $customer_group_id= $Ledger_data['customer_group_id'];
            $customer_type_id=$Ledger_data['customer_type_id'];
            //$route_id = 1;
            //$customer_group_id=1;
            $contact_person = $Ledger_data['contact_person'];
            $credit_period = $Ledger_data['credit_period'];
            $credit_limit = $Ledger_data['credit_limit'];
            $gstin = $Ledger_data['gstin'];
            $other_notes = $Ledger_data['other_notes'];
           // $customer_name_arabic_text=trim(strtoupper($Customer_data['arabic_name']));
            $function_return = $this->AccountHeadCreate($main_group_id,$sub_group_id, $name, $opening_balance, $date, $description);
            if ($function_return['result'] != 'Success')
                throw new Exception($function_return['message']);
            $AccountHead_id = $this->AccountHead->getLastInsertId();
            $customer_id=$this->SystemParameter->field('value',array('id'=>11));
            $vendor_id=$this->SystemParameter->field('value',array('id'=>12));
            
            if($sub_group_id==$customer_id){
        	if(empty($customer_type_id))
        	{
        		  throw new Exception("Empty Customer Type", 1);
        	}
        	if(empty($route_id))
        	{
        		  throw new Exception("Empty Route", 1);
        	}
        	if(empty($customer_group_id))
        	{
        		  throw new Exception("Empty Customer Group", 1);
        	}
            $Customer_date = [
            'account_head_id' => $AccountHead_id,
            'customer_type_id' => $customer_type_id,
          //  'arabic_name'=>$customer_name_arabic_text,
            'state_id' => $state,
            'place' => $address,
            'route_id' => $route_id,
            'division_id' => $division_id,
            'customer_group_id' => $customer_group_id,
            'code' => $customer_code,
            'email' => $email,
            'mobile' => $contact_no,
            'contact_person' => $contact_person,
            'credit_limit' => $credit_limit,
            'credit_period' => $credit_period,
            'gstin' => $gstin,
            'vat_no' => $gstin,
            'created_by' => $user_id,
            'modified_by' => $user_id,
            'created_at' => date('Y-m-d H:i:s', strtotime($date)),
            'updated_at' => date('Y-m-d H:i:s', strtotime($date)),
            'district' => $district,
            'contact_no' => $contact_no,
            'pin_code' => $pin_code,
            'name' => $name,
            'sub_group_id' => $sub_group_id,
            'main_group_id' => $main_group_id,
            'country' => $country,
            'telephone' => $telephone,
            'website' => $website,
            'opening_balance' => $opening_balance,
            'other_notes' => $other_notes,
            'status' => $status,
           // 'cost_center' => $cost_center,
            ];
            $this->Customer->create();
            if (!$this->Customer->save($Customer_date)){
                throw new Exception("Error Customer Creation", 1);
            }
            $last_inserted_account = $this->Customer->findById($this->Customer->getLastInsertId());
            }
            elseif($sub_group_id==$vendor_id){
            	$Party_date=[
			'account_head_id'=>$AccountHead_id,
            'state_id'=>$state,
			'place'=>$address,
			'code'=>$party_code,
			'email'=>$email,
			'mobile'=>$contact_no,
			'telephone'=>$telephone,
             'gstin'=>$gstin,
			'vat_no'=>$gstin,
			'address'=>$address,
			'description'=>$description,
			'district_id'=>$district,
			'pin_code'=>$pin_code,
			'land_line'=>$telephone,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
			'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
			'name'=>$name,
			'sub_group_id'=>$sub_group_id,
			'main_group_id'=>$main_group_id,
			'country_id'=>$country,
			'website'=>$website,
			'contact_person'=>$contact_person,
			'contact_no'=>$contact_no,
			'credit_limit'=>$credit_limit,
			'credit_period'=>$credit_period,
			'opening_balance'=>$opening_balance,
			'other_notes'=>$other_notes,
			'status'=>$status,
			//'cost_center' => $cost_center,
            ];
			$this->Party->create();
			if(!$this->Party->save($Party_date)){
				throw new Exception("Error Party Creation", 1);
			}
            $last_inserted_account = $this->Party->findById($this->Party->getLastInsertId());
            }
            else{
            $Ledger_data = [
            'name'=>$name,
            'code'=>$ledger_code,
            'account_head_id' => $AccountHead_id,
            'sub_group_id' => $sub_group_id,
            'main_group_id'=>$main_group_id,
            'address' => $address,
            'district' => $district,
            'pin_code' => $pin_code,
            'state' => $state,
            'country' => $country,
            'telephone' => $telephone,
            'email' => $email,
            'website' => $website,
           // 'mobile' => $mobile,
            'contact_person' => $contact_person,
            'contact_no' => $contact_no,
            'gstin' => $gstin,
            'credit_limit' => $credit_limit,
            'credit_period' => $credit_period,
            'opening_balance' => $opening_balance,
            'other_notes' => $other_notes,
            'status' => $status,
            'cost_center' => $cost_center,
            'created_at' => date('Y-m-d H:i:s', strtotime($date)),
            'updated_at' => date('Y-m-d H:i:s', strtotime($date)),
            ];
            $this->AccLedger->create();
            if (!$this->AccLedger->save($Ledger_data)){
                throw new Exception("Error Ledger Creation", 1);
            }
          //  $last_inserted_account = $this->AccLedger->findById($this->AccLedger->getLastInsertId());
            $last_inserted_account = $this->AccLedger->find('first',array(
            'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=AccLedger.account_head_id')
			),
		),
		'conditions'=>array('AccLedger.id'=>$this->AccLedger->getLastInsertId()),
		'fields'=>array('AccountHead.id','AccountHead.name')
            ));

        }
            $return['result'] = 'Success';
           $return['key'] = $last_inserted_account['AccountHead']['id'];
           $return['value'] = $last_inserted_account['AccountHead']['name'];
            $datasource_AccLedger->commit();
            $datasource_AccountHead->commit();
            $datasource_Customer->commit();
        } catch (Exception $e) {
            $datasource_AccLedger->rollback();
            $datasource_AccountHead->rollback();
            $datasource_Customer->rollback();
            $return['result'] = $e->getMessage();
        }
        echo json_encode($return);
        exit;
    }
    public function GetSystemParameter($id=null)
  {
  	//pr($id);
  	//exit;
     $return[ 'sub_group_ids' ]=$this->SystemParameter->field('value',array('id'=>11));
      //$return[ 'mode' ]=$this->SystemParameter->field('value',array('id'=>2));
      // $piece=$this->SystemParameter->field('value',array('id'=>1));
     
      // if($piece=='yes'){
      // 	$return['casepiece']='piece';
      // }else{
      // 	$return['casepiece']='case';
      // }
       $conditions=array();
		if(!empty($id))
		{
			$conditions['AccSubGroup.id']=$id;
		}
		//$return['option']='<option value="">SELECT</option>';
		$AccMainGroup = $this->AccMainGroup->find('first', array(
			'joins'=>array(
				array(
					"table"=>'acc_sub_groups',
					"alias"=>'AccSubGroup',
					"type"=>'inner',
					"conditions"=>array('AccMainGroup.id=AccSubGroup.main_group_id'),
				),
			),
			'conditions' =>$conditions ,
			//'order'=>array('AccMainGroup.name'),
			'fields' => array(
				'AccMainGroup.id',
				'AccMainGroup.name',
			)
		)
	);
		//foreach ($AccMainGroup as $key => $value) {
			
			// $return['option']='<option value='.$AccMainGroup['AccMainGroup']['id'].'>'.$AccMainGroup['AccMainGroup']['name'].'</option>';
		$return['option']=$AccMainGroup['AccMainGroup']['name'];
			$return['result']='Success';

		echo json_encode($return);
		exit;
	}
	
	public function ReceiptVoucher($id=null)
    {
				
    				    $cash_group_id=$this->SystemParameter->field('value',array('id'=>9));
						$bank_group_id=$this->SystemParameter->field('value',array('id'=>10));
						$crediters_group_id=$this->SystemParameter->field('value',array('id'=>12));
						$debtors_group_id=$this->SystemParameter->field('value',array('id'=>11));
$AccountHeads=$this->AccountHead->find('all',array(
			'joins'=>[
			array(
				'table'=>'acc_sub_groups',
				'alias'=>'AccSubGroup',
				'type'=>'INNER',
				'conditions'=>array('AccSubGroup.id=AccountHead.sub_group_id')
				),
			],
			'conditions'=>array(
				'or'=>array(
					'AccountHead.sub_group_id'=>array(
						$cash_group_id,
						$bank_group_id,
						$crediters_group_id,
						$debtors_group_id
						// $profit_loss_group_id,
						// $profit_loss_dot_group_id,
						// $pre_paid_expense_group_id,
						// $outstanding_expense_group_id,
						// $reserves_group_id,
						// $provision_group_id,
						),
					//'Group.master_group_id'=>[$expense_master_group_id],
					),
				),
			'fields'=>[
			'AccountHead.id',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			'AccountHead.name',
			]
			)
		);
			//pr($AccountHeads);
			//exit;
			$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		}
		$this->set('AccountHead_list',$AccountHead_list);
		$mode_category=[
					'1'=>'Cash',
					'2'=>'Cheque',
					'3'=>'Bank Transfer',
				];
				$this->set('mode_category',$mode_category);
				$AccountHead_cash=$this->AccountHead->find('list',array('conditions'=>array('sub_group_id'=>$cash_group_id)));
				$this->set('Mode',$AccountHead_cash);
				// $currency=$this->SystemParameter->field('value',array('id'=>6));
				// $data['JournalVoucher']['currency']=$currency;
				// $data['JournalVoucher']['date']=date('d-m-Y');
				// $data['JournalVoucher']['cheque_date']=date('d-m-Y');
    //              $this->request->data=$data;
        // $user_id=$this->Session->read('User.id');
        // $this->set('decimalpoint',2);
        // $accountypes=['18'=>'Vendor','3'=>'Customers','0'=>'G/L Account'];
        // $this->set('accountypes',$accountypes);
        // $Division = $this->Division->find('list', array('fields' => array('id','name')));
        // $this->set('Divisions',$Division);
        // $Location = $this->Location->find('list', array('fields' => array('id','name')));
        // $this->set('Locations',$Location);
        $banks=$this->AccountHead->find('list',array(
            'conditions'=>['AccountHead.sub_group_id'=>$bank_group_id],
            'order'=>array('AccountHead.name ASC'),
            'fields'=>[
                'AccountHead.id',
                'AccountHead.name',
            ]
        ));
        $this->set('banks',$banks);
        // $user_branch_id=$this->Session->read('User.branch_id');
        // $conditions=[];$branchcondition=[];
        // if(!empty($user_branch_id))
        // {
        //     $branch=$this->Branch->findById($user_branch_id);
        //     $branchlist = $this->Branch->find('list',array('conditions'=> ['id'=>$user_branch_id]));
        //     $order_no= '';
        // }
        // else
        // {
        //     $roleName =  $this->Session->read('UserRole.name') ;
        //     $companyId =  $this->Session->read('CompanyId') ;
        //     $branchcondition = [];
        //     $brancharr = [];
        //     if(strtolower($roleName) == 'admin')
        //     {
        //         if($companyId)
        //         {
        //             $branches = $this->Branch->find('list',array('conditions'=>['company_id'=>$companyId],'fields'=>['id']));
        //             $brancharr = array_values($branches);
        //             if(count($brancharr)>0) {
        //                 $branchcondition['Branch.id IN'] = $brancharr;
        //             }
        //         }
        //     }
        //     $branchlist = $this->Branch->find('list',array('conditions'=> $branchcondition));
        //     $order_no= '';
        // }
        // $this->set('BranchList',$branchlist);
        // $userid = $this->Session->read('User.id');
        if (!$this->request->data)
        {
            if(empty($id))
            {
            $Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
    if(!empty($Journals))
    {
      $JournalsNo = $Journals['Journal']['voucher_no'];
      $Journals_no=$JournalsNo+1;
    }
    else
    {
      $Journals_no=1;
    }
                $currency=$this->SystemParameter->field('value',array('id'=>6));
				$data['JournalVoucher']['currency']=$currency;
				$data['JournalVoucher']['date']=date('d-m-Y');
				$data['JournalVoucher']['cheque_date']=date('d-m-Y');
				$data['JournalVoucher']['journal_no']=$Journals_no;
                 $this->request->data=$data;
            }
            else
            {

            }
        }
        else
        {

           $datasource_Journal = $this->Journal->getDataSource();
			try {
				// if($sale_items)
    //   {
       // pr($sale_items);exit;
        
     // }
			$datasource_Journal->begin();
			$data=$this->request->data['JournalVoucher'];
			//pr($data);
			//exit;
			//$credit=$data['account_head'];
			// $debit=$data['mode'];
			// //$amount=$data['total'];
			// $date=$data['date'];
		//$narration=$data['narration'];
			// $voucher_no=$data['reference_no'];
			// // $external_voucher=$data['external_voucher'];
			// $external_voucher='';
			// $work_flow='Voucher Receipt';
		//$voucher_no=$data['reference_no'];
			$user_id=1;
for ($i=0; $i <count($data['acount_head_id']) ; $i++) {
          $remarks=$data['reference'][$i];
          $amount=$data['amount'][$i];
          $credit=$data['acount_head_id'][$i];
            $debit=$data['mode'];
			//$amount=$data['amount'];
			$date=$data['date'];
			$narration=$data['narration'];
			$reference_no=$data['reference_no'];
			$cheque_no=$data['cheque_no'];
			if($data['mode_category_ids']==2){
			//$cheque_no=$data['cheque_no'];
			$cheque_date=$data['cheque_date'];
		    }
		    $voucher_no=$data['journal_no'];
			// $external_voucher=$data['external_voucher'];
			//$external_voucher='';
			$work_flow='Voucher Receipt';
          
           $AccountingsController = new AccountingsController;
		   // $function_return=$AccountingsController->AccountHeadEdit($account_head_id,$name,$opening_balance,$date,$description);
			$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$narration,$reference_no,$cheque_no,$cheque_date);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
        }

			
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
			$this->redirect( Router::url( $this->referer(), true ) );
           
        }
    }

    public function PaymentVoucher($id=null)
    {
				
		$cash_group_id=$this->SystemParameter->field('value',array('id'=>9));
		$bank_group_id=$this->SystemParameter->field('value',array('id'=>10));
		$crediters_group_id=$this->SystemParameter->field('value',array('id'=>12));
		$debtors_group_id=$this->SystemParameter->field('value',array('id'=>11));
		// $AccountHeads=$this->AccountHead->find('all',array(
		// 	'joins'=>[
		// 	array(
		// 		'table'=>'acc_sub_groups',
		// 		'alias'=>'AccSubGroup',
		// 		'type'=>'INNER',
		// 		'conditions'=>array('AccSubGroup.id=AccountHead.sub_group_id')
		// 		),
		// 	],
		// 	'conditions'=>array(
		// 		// 'or'=>array(
		// 		// 	'AccountHead.acc_sub_group_id'=>array(
		// 		// 		$cash_group_id,
		// 		// 		$bank_group_id,
		// 		// 		$crediters_group_id,
		// 		// 		$debtors_group_id
		// 		// 		// $profit_loss_group_id,
		// 		// 		// $profit_loss_dot_group_id,
		// 		// 		// $pre_paid_expense_group_id,
		// 		// 		// $outstanding_expense_group_id,
		// 		// 		// $reserves_group_id,
		// 		// 		// $provision_group_id,
		// 		// 		),
		// 		// 	//'Group.master_group_id'=>[$expense_master_group_id],
		// 		// 	),
		// 		'AccountHead.acc_sub_group_id !='=>0,
		// 		),
		// 	'fields'=>[
		// 	'AccountHead.id',
		// 	'AccountHead.created_at',
		// 	'AccountHead.opening_balance',
		// 	'AccountHead.name',
		// 	]
		// 	)
		// );
		// 	//pr($AccountHeads);
		// 	//exit;
		// $AccountHead_list=[];
		// foreach ($AccountHeads as $key => $value) {
		// 	$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		// }
		// $this->set('AccountHead_list',$AccountHead_list);
		$mode_category=[
					'1'=>'Cash',
					'2'=>'Cheque',
					'3'=>'Bank Transfer',
				];
				$this->set('mode_category',$mode_category);
				$AccountHead_cash=$this->AccountHead->find('list',array('conditions'=>array('acc_sub_group_id'=>$cash_group_id)));
				$this->set('Mode',$AccountHead_cash);
				// $currency=$this->SystemParameter->field('value',array('id'=>6));
				// $data['JournalVoucher']['currency']=$currency;
				// $data['JournalVoucher']['date']=date('d-m-Y');
				// $data['JournalVoucher']['cheque_date']=date('d-m-Y');
    			// $this->request->data=$data;
				// $user_id=$this->Session->read('User.id');
				// $this->set('decimalpoint',2);
				// $accountypes=['18'=>'Vendor','3'=>'Customers','0'=>'G/L Account'];
				// $this->set('accountypes',$accountypes);
				// $Division = $this->Division->find('list', array('fields' => array('id','name')));
				// $this->set('Divisions',$Division);
				// $Location = $this->Location->find('list', array('fields' => array('id','name')));
				// $this->set('Locations',$Location);
        $banks=$this->AccountHead->find('list',array(
            'conditions'=>['AccountHead.acc_sub_group_id'=>$bank_group_id],
            'order'=>array('AccountHead.name ASC'),
            'fields'=>[
                'AccountHead.id',
                'AccountHead.name',
            ]
        ));
        $this->set('banks',$banks);

        $CostCenter=$this->CostCenter->find('list',array(
            // 'conditions'=>['AccountHead.sub_group_id'=>$bank_group_id],
            // 'order'=>array('AccountHead.name ASC'),
            'fields'=>[
                'CostCenter.id',
                'CostCenter.name',
            ]
        ));
		$this->set('CostCenter',$CostCenter);
		
        // $user_branch_id=$this->Session->read('User.branch_id');
        // $conditions=[];$branchcondition=[];
        // if(!empty($user_branch_id))
        // {
        //     $branch=$this->Branch->findById($user_branch_id);
        //     $branchlist = $this->Branch->find('list',array('conditions'=> ['id'=>$user_branch_id]));
        //     $order_no= '';
        // }
        // else
        // {
        //     $roleName =  $this->Session->read('UserRole.name') ;
        //     $companyId =  $this->Session->read('CompanyId') ;
        //     $branchcondition = [];
        //     $brancharr = [];
        //     if(strtolower($roleName) == 'admin')
        //     {
        //         if($companyId)
        //         {
        //             $branches = $this->Branch->find('list',array('conditions'=>['company_id'=>$companyId],'fields'=>['id']));
        //             $brancharr = array_values($branches);
        //             if(count($brancharr)>0) {
        //                 $branchcondition['Branch.id IN'] = $brancharr;
        //             }
        //         }
        //     }
        //     $branchlist = $this->Branch->find('list',array('conditions'=> $branchcondition));
        //     $order_no= '';
        // }
        // $this->set('BranchList',$branchlist);
        // $userid = $this->Session->read('User.id');
        if (!$this->request->data)
        {
            if(empty($id))
            {
            	$Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
				if(!empty($Journals))
				{
				$JournalsNo = $Journals['Journal']['voucher_no'];
				$Journals_no=$JournalsNo+1;
				}
				else
				{
				$Journals_no=1;
				}
                $currency=$this->SystemParameter->field('value',array('id'=>6));
				$data['JournalVoucher']['currency']=$currency;
				$data['JournalVoucher']['date']=date('d-m-Y');
				$data['JournalVoucher']['cheque_date']=date('d-m-Y');
				$data['JournalVoucher']['journal_no']=$Journals_no;
                 $this->request->data=$data;
            }
            else
            {
				$currency=$this->SystemParameter->field('value',array('id'=>6));
				$Journal=$this->Journal->findByVoucherNo($id);
				//pr($Journal);
				//exit;
				$Customer=$this->Customer->findByAccountHeadId($Journal['Journal']['credit']);
				$this->request->data=$Journal;
				$this->request->data['JournalVoucher']['date']=date('d-m-Y',strtotime($Journal['Journal']['date']));
				$this->request->data['JournalVoucher']['journal_no']=$Journal['Journal']['voucher_no'];
				$this->request->data['JournalVoucher']['currency']=$currency;
				if($Journal['Journal']['reference_no'])
					$this->request->data['JournalVoucher']['mode_category_ids']=3;
				elseif($Journal['Journal']['cheque_no'])
					$this->request->data['JournalVoucher']['mode_category_ids']=2;
				else
					$this->request->data['JournalVoucher']['mode_category_ids']=1;
				$this->request->data['JournalVoucher']['narration']=$Journal['Journal']['narration'];
				//$this->request->data['JournalVoucher']['mode']=$Journal['Customer']['id'];
				$this->request->data['JournalVoucher']['cheque_no']=$Journal['Journal']['cheque_no'];
				$this->request->data['JournalVoucher']['cheque_date']=date('d-m-Y',strtotime($Journal['Journal']['cheque_date']));
				$this->request->data['JournalVoucher']['reference_no']=$Journal['Journal']['reference_no'];
				$PaymentVoucherItems=$this->Journal->find('all',array(
					'joins'=>[
						array(
							'table'=>'account_heads',
							'alias'=>'AccountHead',
							'type'=>'LEFT',
							'conditions'=>array('AccountHead.id=Journal.debit')
							),
						],
					'conditions'=>['Journal.voucher_no'=>$id,'Journal.flag'=>1],
					'fields'=>array(
						'Journal.*',
						'AccountHead.name'
					)
				));
				//print_r($PaymentVoucherItems);die;
				$this->set('PaymentVoucherItems',$PaymentVoucherItems);
            }
        }
        else
        {

           $datasource_Journal = $this->Journal->getDataSource();
			try {
				// if($sale_items)
				//   {
				// pr($sale_items);exit;
					
				// }
				$datasource_Journal->begin();
				//$data=$this->request->data;
				$data=$this->request->data['JournalVoucher'];
				//pr($data);
				//exit;
				//$credit=$data['account_head'];
				// $debit=$data['mode'];
				// //$amount=$data['total'];
				// $date=$data['date'];
				//$narration=$data['narration'];
				// $voucher_no=$data['reference_no'];
				// // $external_voucher=$data['external_voucher'];
				// $external_voucher='';
				// $work_flow='Voucher Receipt';
				//$voucher_no=$data['reference_no'];
				if(!$id){
				$user_id=1;
				for ($i=0; $i <count($data['acount_head_id']) ; $i++) {
						$remarks=$data['reference'][$i];
						$amount=$data['amount'][$i];
						$debit=$data['acount_head_id'][$i];

						if($data['mode_category_ids']==1) $credit=$data['mode_cash'];
					if($data['mode_category_ids']==2) $credit=$data['mode_cheque'];
					if($data['mode_category_ids']==3) $credit=$data['mode_bank'];
						//$credit=$data['mode'];
						//$amount=$data['amount'];
						$date=$data['date'];
						$narration=$data['narration'];
						$reference_no=$data['reference_no'];
						$cheque_no=$data['cheque_no'];
					$cheque_date=$data['cheque_date'];
					if($data['mode_category_ids']==2){
					//$cheque_no=$data['cheque_no'];
					$cheque_date=$data['cheque_date'];
					}
						$voucher_no=$data['journal_no'];
						// $external_voucher=$data['external_voucher'];
						//$external_voucher='';
						$work_flow='Voucher Payment';
						$cost_amount=$data['cost_center'][$i];
					
					// $function_return=$AccountingsController->AccountHeadEdit($account_head_id,$name,$opening_balance,$date,$description);
						$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$narration,$reference_no,$cheque_no,$cheque_date);
						if($function_return['result']!='Success')
							throw new Exception($function_return['message'], 1);
						$last_journal_id=$this->Journal->getLastInsertId();
					//print_r($last_journal_id);die;
					
					
					if($cost_amount!=''){
						$amount_explode=explode(",",$cost_amount );
						foreach ($amount_explode as $key1 => $amount_explode_single) {
							$amount_explode_single_array=explode('-', $amount_explode_single);
							$single_cost_center_id=$amount_explode_single_array[0];
							$single_cost_amount=$amount_explode_single_array[1];
							//  $size_id=$single_size;
							// $quantity=$single_quantity;
							//    $SizeWiseProduct = $this->SizeWiseProduct->find('first',array(
							// 'conditions' => array(
							//   'SizeWiseProduct.product_id' => $product_id,
							//   'SizeWiseProduct.size_id' => $size_id,
							//   'SizeWiseProduct.warehouse_id' => $warehouse_id,
							// ),));
							//   $stocksize_id=$SizeWiseProduct['SizeWiseProduct']['id'];
							//   $stocksize_quantity=$SizeWiseProduct['SizeWiseProduct']['quantity'];
							//   $remark='Sale --'.$value['Sale']['invoice_no'].'('.$quantity.')';
							//   $stocksize_quantity-=$quantity;
							//   $stocksize_quantity-=$free_quantity;
							//   $StockController = new StockController;
							//   $warehouse_id = 1;
							$CostCenter_data=[
							'payment_id'=>$last_journal_id,
							'cost_center_id'=>$single_cost_center_id,
							'amount'=>$single_cost_amount,
							'created_at'=>date('Y-m-d H:i:s'),
							'updated_at'=>date('Y-m-d H:i:s'),
							];
							//pr($Sale_data);exit;
							$this->CostCenterDetail->create();
							if(!$this->CostCenterDetail->save($CostCenter_data))
							{
								$errors = $this->CostCenterDetail->validationErrors;
								foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
								}
							}
						}
					}
				}
				}else{
			// $Journal=$this->Journal->findByVoucherNo($id);
			// $branch_id=$Journal['Journal']['branch_id'];
			$user_id=1;
				for ($i=0; $i <count($data['acount_head_id']) ; $i++) {
						$remarks=$data['reference'][$i];
						$amount=$data['amount'][$i];
						$debit=$data['acount_head_id'][$i];

						if($data['mode_category_ids']==1) $credit=$data['mode_cash'];
					if($data['mode_category_ids']==2) $credit=$data['mode_cheque'];
					if($data['mode_category_ids']==3) $credit=$data['mode_bank'];
						//$credit=$data['mode'];
						//$amount=$data['amount'];
						$date=$data['date'];
						$narration=$data['narration'];
						$reference_no=$data['reference_no'];
						$cheque_no=$data['cheque_no'];
					$cheque_date=$data['cheque_date'];
					if($data['mode_category_ids']==2){
					//$cheque_no=$data['cheque_no'];
					$cheque_date=$data['cheque_date'];
					}
						$voucher_no=$data['journal_no'];
						// $external_voucher=$data['external_voucher'];
						//$external_voucher='';
						$work_flow='Voucher Payment';
						$cost_amount=$data['cost_center'][$i];
					
					// $function_return=$AccountingsController->AccountHeadEdit($account_head_id,$name,$opening_balance,$date,$description);
						$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$narration,$reference_no,$cheque_no,$cheque_date);
						if($function_return['result']!='Success')
							throw new Exception($function_return['message'], 1);
						$last_journal_id=$this->Journal->getLastInsertId();
					//print_r($last_journal_id);die;
					
					
					if($cost_amount!=''){
						$amount_explode=explode(",",$cost_amount );
						foreach ($amount_explode as $key1 => $amount_explode_single) {
							$amount_explode_single_array=explode('-', $amount_explode_single);
							$single_cost_center_id=$amount_explode_single_array[0];
							$single_cost_amount=$amount_explode_single_array[1];
							//  $size_id=$single_size;
							// $quantity=$single_quantity;
							//    $SizeWiseProduct = $this->SizeWiseProduct->find('first',array(
							// 'conditions' => array(
							//   'SizeWiseProduct.product_id' => $product_id,
							//   'SizeWiseProduct.size_id' => $size_id,
							//   'SizeWiseProduct.warehouse_id' => $warehouse_id,
							// ),));
							//   $stocksize_id=$SizeWiseProduct['SizeWiseProduct']['id'];
							//   $stocksize_quantity=$SizeWiseProduct['SizeWiseProduct']['quantity'];
							//   $remark='Sale --'.$value['Sale']['invoice_no'].'('.$quantity.')';
							//   $stocksize_quantity-=$quantity;
							//   $stocksize_quantity-=$free_quantity;
							//   $StockController = new StockController;
							//   $warehouse_id = 1;
							$CostCenter_data=[
							'payment_id'=>$last_journal_id,
							'cost_center_id'=>$single_cost_center_id,
							'amount'=>$single_cost_amount,
							'created_at'=>date('Y-m-d H:i:s'),
							'updated_at'=>date('Y-m-d H:i:s'),
							];
							//pr($Sale_data);exit;
							$this->CostCenterDetail->create();
							if(!$this->CostCenterDetail->save($CostCenter_data))
							{
								$errors = $this->CostCenterDetail->validationErrors;
								foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
								}
							}
						}
					}
				}

		}
			
				
				$return['result']='Success';
				$datasource_Journal->commit();
				$this->Session->setFlash(__($return['result']));
			} catch (Exception $e) {
				$datasource_Journal->rollback();
				$return['result']='Error';
				$return['message']=$e->getMessage();
				$this->Session->setFlash(__($return['message']));
			}
			//$this->redirect( Router::url( $this->referer(), true ) );
			$this->redirect(array('controller' => 'Accounts', 'action' => 'PaymentVoucherList'));
           
        }
    }
    public function add_cost_center_ajax() {
        //$country_id = $this->Global_Var_Profile['State']['country_id'];
        //$country_id=1;
        $return = ['result' => 'Empty'];
        try {
            $data = $this->request->data;
            $name = $data['name'];
          //  $code = $data['code'];
            $state_date = array(
               // 'country_id' => $country_id,
                'name' => strtoupper($name),
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>date('Y-m-d H:i:s'),
               // 'code' => strtoupper($code),
                );
            $this->CostCenter->create();
            if (!$this->CostCenter->save($state_date))
                throw new Exception("Error In Cost Center Create", 1);
            $return['result'] = 'Success';
            $CostCenter_id = $this->CostCenter->getLastInsertId();
            $CostCenter = $this->CostCenter->findById($CostCenter_id);
            $return['key'] = $CostCenter['CostCenter']['id'];
            $return['value'] = $CostCenter['CostCenter']['name'];
        } catch (Exception $e) {
            $return['result'] = $e->getMessage();
        }
        echo json_encode($return);
        exit;
    }
    public function JournalVoucher_3_7_2021($id=null)
    {
    		
    				    $cash_group_id=$this->SystemParameter->field('value',array('id'=>9));
						$bank_group_id=$this->SystemParameter->field('value',array('id'=>10));
						$crediters_group_id=$this->SystemParameter->field('value',array('id'=>12));
						$debtors_group_id=$this->SystemParameter->field('value',array('id'=>11));
// $AccountHeads=$this->AccountHead->find('all',array(
// 			'joins'=>[
// 			array(
// 				'table'=>'acc_sub_groups',
// 				'alias'=>'AccSubGroup',
// 				'type'=>'INNER',
// 				'conditions'=>array('AccSubGroup.id=AccountHead.sub_group_id')
// 				),
// 			],
// 			'conditions'=>array(
// 				// 'or'=>array(
// 				// 	'AccountHead.acc_sub_group_id'=>array(
// 				// 		$cash_group_id,
// 				// 		$bank_group_id,
// 				// 		$crediters_group_id,
// 				// 		$debtors_group_id
// 				// 		// $profit_loss_group_id,
// 				// 		// $profit_loss_dot_group_id,
// 				// 		// $pre_paid_expense_group_id,
// 				// 		// $outstanding_expense_group_id,
// 				// 		// $reserves_group_id,
// 				// 		// $provision_group_id,
// 				// 		),
// 				// 	//'Group.master_group_id'=>[$expense_master_group_id],
// 				// 	),
// 				'AccountHead.acc_sub_group_id !='=>0,
// 				),
// 			'fields'=>[
// 			'AccountHead.id',
// 			'AccountHead.created_at',
// 			'AccountHead.opening_balance',
// 			'AccountHead.name',
// 			]
// 			)
// 		);
// 			//pr($AccountHeads);
// 			//exit;
// 			$AccountHead_list=[];
// 		foreach ($AccountHeads as $key => $value) {
// 			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
// 		}
// 		$this->set('AccountHead_list',$AccountHead_list);
		// $mode_category=[
		// 			'1'=>'Cash',
		// 			'2'=>'Cheque',
		// 			'3'=>'Bank Transfer',
		// 		];
		// 		$this->set('mode_category',$mode_category);
		// 		$AccountHead_cash=$this->AccountHead->find('list',array('conditions'=>array('acc_sub_group_id'=>$cash_group_id)));
		// 		$this->set('Mode',$AccountHead_cash);
				$DebitCredit=[
					//''=>'Select'
					'1'=>'Debit',
					'2'=>'Credit',
					//'3'=>'Bank Transfer',
				];
				$this->set('DebitCredit',$DebitCredit);
				// $currency=$this->SystemParameter->field('value',array('id'=>6));
				// $data['JournalVoucher']['currency']=$currency;
				// $data['JournalVoucher']['date']=date('d-m-Y');
				// $data['JournalVoucher']['cheque_date']=date('d-m-Y');
    //              $this->request->data=$data;
        // $user_id=$this->Session->read('User.id');
        // $this->set('decimalpoint',2);
        // $accountypes=['18'=>'Vendor','3'=>'Customers','0'=>'G/L Account'];
        // $this->set('accountypes',$accountypes);
        // $Division = $this->Division->find('list', array('fields' => array('id','name')));
        // $this->set('Divisions',$Division);
        // $Location = $this->Location->find('list', array('fields' => array('id','name')));
        // $this->set('Locations',$Location);
        // $banks=$this->AccountHead->find('list',array(
        //     'conditions'=>['AccountHead.acc_sub_group_id'=>$bank_group_id],
        //     'order'=>array('AccountHead.name ASC'),
        //     'fields'=>[
        //         'AccountHead.id',
        //         'AccountHead.name',
        //     ]
        // ));
        // $this->set('banks',$banks);

        //         $CostCenter=$this->CostCenter->find('list',array(
        //     // 'conditions'=>['AccountHead.sub_group_id'=>$bank_group_id],
        //     // 'order'=>array('AccountHead.name ASC'),
        //     'fields'=>[
        //         'CostCenter.id',
        //         'CostCenter.name',
        //     ]
        // ));
        // $this->set('CostCenter',$CostCenter);
        // $user_branch_id=$this->Session->read('User.branch_id');
        // $conditions=[];$branchcondition=[];
        // if(!empty($user_branch_id))
        // {
        //     $branch=$this->Branch->findById($user_branch_id);
        //     $branchlist = $this->Branch->find('list',array('conditions'=> ['id'=>$user_branch_id]));
        //     $order_no= '';
        // }
        // else
        // {
        //     $roleName =  $this->Session->read('UserRole.name') ;
        //     $companyId =  $this->Session->read('CompanyId') ;
        //     $branchcondition = [];
        //     $brancharr = [];
        //     if(strtolower($roleName) == 'admin')
        //     {
        //         if($companyId)
        //         {
        //             $branches = $this->Branch->find('list',array('conditions'=>['company_id'=>$companyId],'fields'=>['id']));
        //             $brancharr = array_values($branches);
        //             if(count($brancharr)>0) {
        //                 $branchcondition['Branch.id IN'] = $brancharr;
        //             }
        //         }
        //     }
        //     $branchlist = $this->Branch->find('list',array('conditions'=> $branchcondition));
        //     $order_no= '';
        // }
        // $this->set('BranchList',$branchlist);
        // $userid = $this->Session->read('User.id');
        if (!$this->request->data)
        {
            if(empty($id))
            {
            $Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
    if(!empty($Journals))
    {
      $JournalsNo = $Journals['Journal']['voucher_no'];
      $Journals_no=$JournalsNo+1;
    }
    else
    {
      $Journals_no=1;
    }
                $currency=$this->SystemParameter->field('value',array('id'=>6));
				$data['JournalVoucher']['currency']=$currency;
				$data['JournalVoucher']['date']=date('d-m-Y');
				$data['JournalVoucher']['cheque_date']=date('d-m-Y');
				$data['JournalVoucher']['journal_no']=$Journals_no;
                 $this->request->data=$data;
            }
            else
            {
$currency=$this->SystemParameter->field('value',array('id'=>6));
				$Journal=$this->JournalVoucher->findByVoucherNo($id);
				//pr($Journal);
				//exit;
				//$Customer=$this->Customer->findByAccountHeadId($Journal['Journal']['credit']);
				$this->request->data=$Journal;
				$this->request->data['JournalVoucher']['date']=date('d-m-Y',strtotime($Journal['JournalVoucher']['date']));
				$this->request->data['JournalVoucher']['journal_no']=$Journal['JournalVoucher']['voucher_no'];
				$this->request->data['JournalVoucher']['currency']=$currency;
				// if($Journal['Journal']['reference_no'])
				// 	$this->request->data['JournalVoucher']['mode_category_ids']=3;
				// elseif($Journal['Journal']['cheque_no'])
				// 	$this->request->data['JournalVoucher']['mode_category_ids']=2;
				// else
				// 	$this->request->data['JournalVoucher']['mode_category_ids']=1;
				$this->request->data['JournalVoucher']['narration']=$Journal['JournalVoucher']['narration'];
				$this->request->data['JournalVoucher']['total_dr']=$Journal['JournalVoucher']['debit_total'];
				$this->request->data['JournalVoucher']['total_cr']=$Journal['JournalVoucher']['credit_total'];
				// $this->request->data['JournalVoucher']['mode']=$Journal['Customer']['id'];
				// $this->request->data['JournalVoucher']['cheque_no']=$Journal['Journal']['cheque_no'];
				// $this->request->data['JournalVoucher']['cheque_date']=date('d-m-Y',strtotime($Journal['Journal']['cheque_date']));
				// $this->request->data['JournalVoucher']['reference_no']=$Journal['Journal']['reference_no'];
				$JournalVoucherItems=$this->JournalVoucherDetail->find('all',array(
					'joins'=>[
						array(
							'table'=>'journal_vouchers',
							'alias'=>'JournalVoucher',
							'type'=>'LEFT',
							'conditions'=>array('JournalVoucherDetail.journal_voucher_id=JournalVoucher.id')
							),
						array(
							'table'=>'account_heads',
							'alias'=>'AccountHead',
							'type'=>'LEFT',
							'conditions'=>array('AccountHead.id=JournalVoucherDetail.account_head_id')
							),
						],
					'conditions'=>['JournalVoucher.voucher_no'=>$id,'JournalVoucherDetail.flag'=>1],
					'fields'=>array(
						'JournalVoucherDetail.*',
						'AccountHead.name',
						'AccountHead.id'
					)
				));
				//pr($JournalVoucherItems);die;
				$this->set('JournalVoucherItems',$JournalVoucherItems);
            }
        }
        else
        {
		$datasource_JournalVoucher = $this->JournalVoucher->getDataSource();
		$datasource_JournalVoucherDetail = $this->JournalVoucherDetail->getDataSource();
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_JournalVoucher->begin();
			$datasource_JournalVoucherDetail->begin();	
			$datasource_Journal->begin();
			//$data=$this->request->data;
			$data=$this->request->data['JournalVoucher'];
			//pr($data);
			//exit;
			if(!$id){
				$JV_data=[
				'voucher_no'=>$data['journal_no'],
				'date'=>date('Y-m-d',strtotime($data['date'])),
				'debit_total'=>$data['total_dr'],
				'credit_total'=>$data['total_cr'],
				'narration'=>$data['narration'],
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				$this->JournalVoucher->create();
				if(!$this->JournalVoucher->save($JV_data))
				{
					$errors = $this->JournalVoucher->validationErrors;
					foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
					}
				}
				$id=$this->JournalVoucher->getLastInsertId();
			
				$user_id=1;
				//$debit=[];
				for ($i=0; $i <count($data['dr_cr']) ; $i++) {
					if($data['dr_cr'][$i]==2)
					$credit[]=$data['acount_head_id'][$i];
				}
				$credit_id=$credit[0];
				// echo "<pre>";print_r($credit_id);die;
				for ($i=0; $i <count($data['dr_cr']) ; $i++) {


					$JournalVoucherDetail_data=[
					'journal_voucher_id'=>$id,
					'account_head_id'=>$data['acount_head_id'][$i],
					'dr_cr'=>$data['dr_cr'][$i],
					'dr_amount'=>$data['amount_dr'][$i],
					'cr_amount'=>$data['amount_cr'][$i],
					'reference'=>$data['reference'][$i]
					];
					$this->JournalVoucherDetail->create();
					if(!$this->JournalVoucherDetail->save($JournalVoucherDetail_data))
					{
						$errors = $this->JournalVoucherDetail->validationErrors;
						foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
						}
					}
		
					if($data['dr_cr'][$i]==1){
						$debit=$data['acount_head_id'][$i];
						$credit=$credit_id;
						// break;
						// pr($debit);
					// exit;
					}else{
						$credit=$data['acount_head_id'][$i];
					}
		
					if($data['dr_cr'][$i]==1){
						// echo "<pre>";print_r($debit);
						// exit;
						$debit1=$debit;
						$credit=$credit;
				
						$remarks=$data['reference'][$i];
						$amount=$data['amount_dr'][$i];
						$date=$data['date'];
						$narration=$data['narration'];
						$cheque_date='';
						$voucher_no=$data['journal_no'];
						$work_flow='Journal Voucher';
						// echo "<pre>";echo $amount;
						$branch_id=1;
					// $function_return=$AccountingsController->AccountHeadEdit($account_head_id,$name,$opening_balance,$date,$description);
						$function_return=$this->JournalCreate($credit,$debit1,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$narration,$reference_no=null,$cheque_no=null,$cheque_date);
						if($function_return['result']!='Success')
							throw new Exception($function_return['message'], 1);
						$last_journal_id=$this->Journal->getLastInsertId();
					
					
					}
				}  
			}else{
				// print_r($id);die;
				$JV_data=[
				'date'=>date('Y-m-d',strtotime($data['date'])),
				'debit_total'=>$data['total_dr'],
				'credit_total'=>$data['total_cr'],
				'narration'=>$data['narration'],
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				$JournalVoucher=$this->JournalVoucher->findByVoucherNo($id);
				$this->JournalVoucher->id=$id;
				if(!$this->JournalVoucher->save($JV_data))
				{
					$errors = $this->JournalVoucher->validationErrors;
					foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
					}
				}
				$user_id=1;
				//$debit=[];
				for ($i=0; $i <count($data['dr_cr']) ; $i++) {
					if($data['dr_cr'][$i]==2)
					$credit[]=$data['acount_head_id'][$i];
				}
				$credit_id=$credit[0];
				// echo "<pre>";print_r($credit_id);die;
				for ($i=0; $i <count($data['dr_cr']) ; $i++) {


					$JournalVoucherDetail_data=[
					'journal_voucher_id'=>$id,
					'account_head_id'=>$data['acount_head_id'][$i],
					'dr_cr'=>$data['dr_cr'][$i],
					'dr_amount'=>$data['amount_dr'][$i],
					'cr_amount'=>$data['amount_cr'][$i],
					'reference'=>$data['reference'][$i]
					];
					$this->JournalVoucherDetail->create();
					if(!$this->JournalVoucherDetail->save($JournalVoucherDetail_data))
					{
						$errors = $this->JournalVoucherDetail->validationErrors;
						foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
						}
					}
		
					if($data['dr_cr'][$i]==1){
						$debit=$data['acount_head_id'][$i];
						$credit=$credit_id;
						// break;
						// pr($debit);
					// exit;
					}else{
						$credit=$data['acount_head_id'][$i];
					}
		
					if($data['dr_cr'][$i]==1){
						// echo "<pre>";print_r($debit);
						// exit;
						$debit1=$debit;
						$conditions['JournalVoucherDetail.journal_voucher_id']=$id;
						$conditions['JournalVoucherDetail.flag']=1;
						$JournalVoucherDetail=$this->JournalVoucherDetail->find('first',array(
							'conditions'=>$conditions,
							'fields'=>array(
							'JournalVoucherDetail.account_head_id'
							)
						));
						if($JournalVoucherDetail) $credit=$JournalVoucherDetail['JournalVoucherDetail']['account_head_id'];
						else $credit=$credit;
				
						$remarks=$data['reference'][$i];
						$amount=$data['amount_dr'][$i];
						$date=$data['date'];
						$narration=$data['narration'];
						$cheque_date='00-00-0000';
						$voucher_no=$data['journal_no'];
						$work_flow='Journal Voucher';
						// echo "<pre>";echo $amount;
			            $function_return=$this->JournalCreate($credit,$debit1,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$narration,$reference_no=null,$cheque_no=null,$cheque_date);
						if($function_return['result']!='Success')
							throw new Exception($function_return['message'], 1);
						$last_journal_id=$this->Journal->getLastInsertId();
					
					
					}
				}
			}
			$return['result']='Success';
			$datasource_JournalVoucher->commit();
			$datasource_JournalVoucherDetail->commit();
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_JournalVoucher->rollback();
			$datasource_JournalVoucherDetail->rollback();
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		$this->redirect( Router::url( $this->referer(), true ) );
		
	}
    }

    public function CostCenter_table_ajax()
	{
		
		$requestData=$this->request->data;
		$columns = [];
		//$columns[]='AccountHead.created_at';
		$columns[]='CostCenter.name';
		//$columns[]='total';
		$cash_group_id=1;
	$bank_group_id=2;
	$crediters_group_id=18;
	$reserves_group_id=24;
	$provision_group_id=23;
	$pre_paid_expense_group_id=5;
	$outstanding_expense_group_id=20;
	$profit_loss_group_id=25;
	$profit_loss_dot_group_id=27;
	$expense_master_group_id=5;
		$conditions=[];
		// $conditions['SubGroup.group_id']=array(
		// 		$cash_group_id=1,
	 //            $bank_group_id=2,
		// 	);
	// 	$conditions['NOT']=array(
	// 		'SubGroup.group_id' =>array(
	// 			$cash_group_id,
	// 					$bank_group_id,
	// 					$crediters_group_id,
	// 					$profit_loss_group_id,
	// 					$profit_loss_dot_group_id,
	// 					$pre_paid_expense_group_id,
	// 					$outstanding_expense_group_id,
	// 					$reserves_group_id,
	// 					$provision_group_id,
	// 		),
	// 	'Group.master_group_id'=>[$expense_master_group_id],
	// );
		
//$conditions['AccountHead.acc_sub_group_id !=']=0;

		if(isset($requestData['account_head_id']))
	{
		if($requestData['account_head_id'])
			$conditions['CostCenter.id']=$requestData['account_head_id'];
	}
		$totalData=$this->CostCenter->find('count',[
			// 'joins'=>[
			// array(
			// 	'table'=>'groups',
			// 	'alias'=>'Group',
			// 	'type'=>'INNER',
			// 	'conditions'=>array('Group.id=SubGroup.group_id')
			// 	),
			// ],
			'conditions'=>$conditions
		]);
		$totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'CostCenter.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->CostCenter->find('count',[
			// 	'joins'=>[
			// array(
			// 	'table'=>'groups',
			// 	'alias'=>'Group',
			// 	'type'=>'INNER',
			// 	'conditions'=>array('Group.id=SubGroup.group_id')
			// 	),
			// ],
				'conditions'=>$conditions,
			]);
		}

		//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column'] <= 1)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
		$Data=$this->CostCenter->find('all',array(
			// 'joins'=>[
			// array(
			// 	'table'=>'groups',
			// 	'alias'=>'Group',
			// 	'type'=>'INNER',
			// 	'conditions'=>array('Group.id=SubGroup.group_id')
			// 	),
			// ],
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			//'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'order'=> $order,
			'fields'=>array(
				'CostCenter.*',
			)
		));
		// $category=[
		// 	'Asset'=>'debit_account',
		// 	'Expense'=>'debit_account',
		// 	'Capital'=>array(
		// 		'Capital'=>'credit_account',
		// 		'Drawing'=>'debit_account',
		// 	),
		// 	'Income'=>'credit_account',
		// 	'Liabilities'=>'credit_account',
		// ];
		foreach ($Data as $key => $value) {
			//$function_return=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
			//$Data[$key]['AccountHead']['created_at']=date('d-m-Y',strtotime($value['AccountHead']['created_at']));
			$Data[$key]['CostCenter']['name']=$value['CostCenter']['name'];
			
		}
		//sorting total/balance/recieved
		// if($requestData['order'][0]['column'] >= 2) {

		// 	//callbackfunction
		// 	function cmp($a, $b,$columnname,$columndir)
		// 	{
		// 		if ($a["AccountHead"][$columnname] == $b["AccountHead"][$columnname]) {
		// 			return 0;
		// 		}
		// 		if($columndir == 'asc') {
		// 			return (str_replace(',','',$a["AccountHead"][$columnname]) < str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
		// 		}
		// 		else if($columndir == 'desc')
		// 		{
		// 			return (str_replace(',','',$a["AccountHead"][$columnname]) > str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
		// 		}
		// 	}
			

		// 	$columnname = $columns[$requestData['order'][0]['column']]; //column name
		// 	$columndir = $requestData['order'][0]['dir']; //column order
		// 	//sorting function
		// 	usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		// }
		//pr($Data);
		//exit;
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function CostCenter()
	{

		    $data['from_date']=date('d-m-Y',strtotime('-1 month'));
			$data['to_date']=date('d-m-Y');
			//$data['Journal']['cheque_date']=date('d-m-Y');
			//$data['Journal']['journal_no']=$Journals_no;
			//$data['to_date']=date('d-m-Y',strtotime('+6 month'));
			$this->request->data=$data;

	}
public function PaymentVoucherList()
	{
	$decimalpoint =2;
	$this->set('decimalpoint',$decimalpoint);
	// $PermissionList = $this->Session->read('PermissionList');
	// $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Sale/InvoiceList'));

	// if(!in_array($menu_id, $PermissionList))
	// {
	//   $this->Session->setFlash("Permission denied");
	//   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
	// }
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
	$AccountHeads=$this->AccountHead->find('all',array(
		'joins'=>[
		array(
			'table'=>'acc_sub_groups',
			'alias'=>'AccSubGroup',
			'type'=>'INNER',
			'conditions'=>array('AccSubGroup.id=AccountHead.sub_group_id')
			),
		],
		'order'=>array('AccountHead.name ASC'),
		'conditions'=>array(
			
			'AccountHead.acc_sub_group_id !='=>0,
			),
		'fields'=>[
		'AccountHead.id',
		'AccountHead.created_at',
		'AccountHead.opening_balance',
		'AccountHead.name',
		]
		)
	);
		//pr($AccountHeads);
		//exit;
	$AccountHead_list=[];
	foreach ($AccountHeads as $key => $value) {
		$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
	}
	$this->set('AccountHead_list',$AccountHead_list);
}
 
  public function payment_voucher_ajax()
{
	$decimalpoint = 2;
	$user_branch_id=$this->Session->read('User.branch_id');
	$requestData=$this->request->data;
	$columns=[];
	$columns[]='Journal.date';
	$columns[]='Journal.id';
	$columns[]='Journal.narration';
	$columns[]='Journal.amount';
	$conditions=[];

	//order only fields that are field of datatabse table
	$order= '';
	if(!in_array($requestData['order'][0]['column'],[4,5]))
	{
		$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
	}
	if(isset($requestData['from_date']) && isset($requestData['to_date']) )
	{
		$conditions['Journal.date between ? and ?']=[
		date('Y-m-d',strtotime($requestData['from_date'])),
		date('Y-m-d',strtotime($requestData['to_date']))
		];
	}
	if($user_branch_id) $conditions['Journal.branch_id']=$user_branch_id;
	$conditions['Journal.flag']=1;
	$conditions['Journal.work_flow']= 'Voucher Payment';
	$totalData=$this->Journal->find('count',['conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
      $q=$requestData['search']['value'];
      $conditions['OR']=array(
        'Journal.date LIKE' =>'%'. $q . '%',
        'Journal.id LIKE' =>'%'. $q . '%',
        'Journal.narration LIKE' =>'%'. $q . '%',
        'Journal.amount LIKE' =>'%'. $q . '%',
      );
      $totalFiltered=$this->Journal->find('count',[
        'conditions'=>$conditions,
      ]);
    }
 // 'conditions' =>  array('not' => array('Sale.workflow'=>null)),

  	$conditions['Journal.work_flow']= 'Voucher Payment';
    $Data=$this->Journal->find('all',array(
      'conditions'=>$conditions,
      'offset'=>$requestData['start'],
      'limit'=>$requestData['length'],
	  'order'=>$order,
	  'group' =>'Journal.voucher_no',
      'fields'=>array(
		'Journal.id',
		'Journal.date',
		'Journal.voucher_no',
		'Journal.remarks',
		'SUM(Journal.amount) as total',
		'AccountHeadDebit.name'
      )
    ));
    //print_r($Data);die;
    foreach ($Data as $key => $value) {
    	$jounal_voucher_details=$this->Journal->find('all',array(
    		 	'joins'=>array(
				// array(
				// 	'table'=>'journal_voucher_details',
				// 	'alias'=>'JournalVoucherDetail',
				// 	'type'=>'INNER',
				// 	'conditions'=>array('JournalVoucher.id=JournalVoucherDetail.journal_voucher_id')
				// 	),
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'INNER',
					'conditions'=>array('Journal.debit=AccountHead.id')
					),
				),
    		'conditions'=>array('Journal.voucher_no'=>$value['Journal']['voucher_no'],'Journal.flag'=>1),
    		'fields'=>array('AccountHead.name','Journal.remarks','Journal.narration')));
 // pr($jounal_voucher_details);
  $acc_head=[];
     $ref=[];
    	foreach ($jounal_voucher_details as $keyj => $valuej) {
        $acc=$valuej['AccountHead']['name'];
        array_push($acc_head,$acc);
        $refer=$valuej['Journal']['remarks'];
        if(!empty($refer)){
        array_push($ref,$refer);
         }
    		
    	}
		/*$Data[$key]['Sale']['action']='<span><a target="_blank" href="'.$this->webroot.'Print/fpdf/'.$value['Sale']['id'].'"><i class="fa fa-2x fa-print"></i></a></span>';*/
		//  $Data[$key]['Journal']['action']='<span><a  href="#" onclick="openInvoicePrint('.$value['Journal']['id'].')"><i class="fa fa-2x fa-print"></i></a></span>';
		$Data[$key]['Journal']['date']=date('d-m-Y',strtotime($value['Journal']['date']));
		$Data[$key]['Journal']['id']=$value['Journal']['voucher_no'];
		$Data[$key]['Journal']['remarks']=$value['Journal']['remarks'];
		$Data[$key]['Journal']['account_head']=$acc_head;
		$Data[$key]['Journal']['reference']=$ref;
		$Data[$key]['Journal']['particular']=$value['AccountHeadDebit']['name'];
		$Data[$key]['Journal']['amount']=number_format($value[0]['total'],2,'.','');
		$Data[$key]['Journal']['action']='&nbsp;&nbsp;<span><a target="_blank" href="'.$this->webroot.'Accounts/PaymentVoucher/'.$value['Journal']['voucher_no'].'"><i class="fa fa-2x fa-eye"></i></span></a>';
		if($this->Session->read('UserRole.id')==1)
    	$Data[$key]['Journal']['action'].='&nbsp;&nbsp;<span hidden class="table_id">'.$value['Journal']['id'].'</span><a href= "#"><span style="display:none"><i class="fa fa-2x fa-edit edit_icon payment_edit"></i></span></a>';
   }

  //sorting total/balance/recieved
  if(in_array($requestData['order'][0]['column'],[4,5])) {

    //callbackfunction
    function cmp($a, $b,$columnname,$columndir)
    {
      if ($a["Journal"][$columnname] == $b["Journal"][$columnname]) {
        return 0;
      }
      if($columndir == 'asc') {
        return (str_replace(',','',$a["Journal"][$columnname]) < str_replace(',','',$b["Journal"][$columnname])) ? -1 : 1;
      }
      else if($columndir == 'desc')
      {
        return (str_replace(',','',$a["Journal"][$columnname]) > str_replace(',','',$b["Journal"][$columnname])) ? -1 : 1;
      }
    }

    $columnname = $columns[$requestData['order'][0]['column']]; //column name
    $columndir = $requestData['order'][0]['dir']; //column order
    //sorting function
    usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
  }

  $json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData),
    "recordsFiltered"=>intval($totalFiltered),
    "records"        =>$Data
  );
   echo json_encode($json_data); exit;
}
  public function payment_search_ajax()
  {
    $data=$this->request->data;
    $branch_id=$data['branch_id'];
    $first_date = date('Y-m-d',strtotime('first day of this month'));
    $to_date = date('Y-m-d',strtotime('last day of this month'));
    if(!empty($data['from_date'])){
      $first_date = date('Y-m-d',strtotime($data['from_date']));
    }
    if(!empty($data['to_date'])){
      $to_date = date('Y-m-d',strtotime($data['to_date']));
    }

    $type='Invoice';
    $PaymentVoucher=$this->PaymentVoucherList_function($first_date,$to_date,$branch_id);
    echo json_encode($PaymentVoucher);
    exit;
  }
  public function cost_center_ajax()
{
	$data=$this->request->data;
	$CostCenterDetail=$this->CostCenterDetail->find('all',array(
		'conditions'=>array('CostCenterDetail.payment_id'=>$data['id']),
		// 'order'=>array('AccountHead.name ASC'),
		'fields'=>[
			'CostCenterDetail.*',
		]
	));
	//print_r($data);die;
	echo json_encode($CostCenterDetail); exit;
}
public function journal_voucher_ajax()
{
	$decimalpoint =2;
	$requestData=$this->request->data;
	$columns=[];
	$columns[]='JournalVoucher.date';
	$columns[]='JournalVoucher.id';
	$columns[]='AccountHead.name';
	$columns[]='JournalVoucher.narration';
	$columns[]='JournalVoucher.debit_total';
	$columns[]='JournalVoucher.action';
	$conditions=[];

	//order only fields that are field of datatabse table
	if(isset($requestData['from_date']) && isset($requestData['to_date']) )
	{
		$conditions['JournalVoucher.date ? and ?']=[
		date('Y-m-d',strtotime($requestData['from_date'])),
		date('Y-m-d',strtotime($requestData['to_date']))
		];
	}
	$conditions['JournalVoucher.flag']=1;
	$totalData=$this->JournalVoucher->find('count',['conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
      $q=$requestData['search']['value'];
      $conditions['OR']=array(
        'JournalVoucher.date LIKE' =>'%'. $q . '%',
        'JournalVoucher.id LIKE' =>'%'. $q . '%',
        'AccountHead.name LIKE' =>'%'. $q . '%',
        'JournalVoucher.narration LIKE' =>'%'. $q . '%',
        'JournalVoucher.debit_total LIKE' =>'%'. $q . '%',
        'JournalVoucher.credit_total LIKE' =>'%'. $q . '%',
      );
      $totalFiltered=$this->JournalVoucher->find('count',[
        'conditions'=>$conditions,
      ]);
    }
 // 'conditions' =>  array('not' => array('Sale.workflow'=>null)),

  	//$conditions['JournalVoucher.work_flow']= 'Journal Voucher';
    $Data=$this->JournalVoucher->find('all',array(
      'conditions'=>$conditions,
      'offset'=>$requestData['start'],
      'limit'=>$requestData['length'],
	  'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
	  // 'group' =>'JournalVoucher.voucher_no',
      'fields'=>array(
      	'JournalVoucher.id',
		'JournalVoucher.date',
		'JournalVoucher.voucher_no',
		'JournalVoucher.narration',
		'JournalVoucher.debit_total',
		'JournalVoucher.credit_total',
      )
    ));
    // pr($Data);exit;
  
    foreach ($Data as $key => $value) {
    	$jounal_voucher_details=$this->JournalVoucherDetail->find('all',array(
    		 	'joins'=>array(
				// array(
				// 	'table'=>'journal_voucher_details',
				// 	'alias'=>'JournalVoucherDetail',
				// 	'type'=>'INNER',
				// 	'conditions'=>array('JournalVoucher.id=JournalVoucherDetail.journal_voucher_id')
				// 	),
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'INNER',
					'conditions'=>array('JournalVoucherDetail.account_head_id=AccountHead.id')
					),
				),
    		'conditions'=>array('JournalVoucherDetail.journal_voucher_id'=>$value['JournalVoucher']['id'],'JournalVoucherDetail.flag'=>1),
    		'fields'=>array('AccountHead.name','JournalVoucherDetail.reference')));
  $acc_head=[];
     $ref=[];
    	foreach ($jounal_voucher_details as $keyj => $valuej) {
        $acc=$valuej['AccountHead']['name'];
        array_push($acc_head,$acc);
        $refer=$valuej['JournalVoucherDetail']['reference'];
        if(!empty($refer)){
        array_push($ref,$refer);
         }
    		
    	}
    	 
	$Data[$key]['JournalVoucher']['date']=date('d-m-Y',strtotime($value['JournalVoucher']['date']));
	$Data[$key]['JournalVoucher']['id']=$value['JournalVoucher']['voucher_no'];
	$Data[$key]['JournalVoucher']['account_head']=$acc_head;
	$Data[$key]['JournalVoucher']['narration']=$value['JournalVoucher']['narration'];
    $Data[$key]['JournalVoucher']['reference']=$ref;
    $Data[$key]['JournalVoucher']['debit_total']=number_format($value['JournalVoucher']['debit_total'],2,'.','');
	$Data[$key]['JournalVoucher']['credit_total']=number_format($value['JournalVoucher']['credit_total'],2,'.','');
    $Data[$key]['JournalVoucher']['action']='&nbsp;&nbsp;<span><a target="_blank" href="'.$this->webroot.'Accounts/JournalVoucher/'.$value['JournalVoucher']['voucher_no'].'"><i class="fa fa-2x fa-eye"></i></span></a>';
    $Data[$key]['JournalVoucher']['action'].='&nbsp;&nbsp;<a href= "#"><span><i class="fa fa-2x fa-trash edit_icon journal_delete" data-id="'.$value['JournalVoucher']['id'].'" data-voucher="'.$value['JournalVoucher']['voucher_no'].'"></i></span></a>';
   }
  $json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData),
    "recordsFiltered"=>intval($totalFiltered),
    "records"        =>$Data
  );
   echo json_encode($json_data); exit;
}
public function journal_voucher_detail_delete_ajax()
{
	// $datasource_JournalVoucher = $this->JournalVoucher->getDataSource();
	$datasource_JournalVoucherDetail = $this->JournalVoucherDetail->getDataSource();
	$datasource_Journal = $this->Journal->getDataSource();
	try {
		$conditions=[];
		$datasource_JournalVoucherDetail->begin();
		$datasource_Journal->begin();
		$data=$this->request->data;
		$JournalVoucherDetail=$this->JournalVoucherDetail->findById($data['id']);
		$conditions['Journal.remarks']=$JournalVoucherDetail['JournalVoucherDetail']['reference'];
		$Journal=$this->Journal->find('first',array(
			'conditions'=>$conditions,
			'fields'=>array(
			  'Journal.id'
			)
		));
		// print_r($Journal['Journal']['id']);die;
		$this->Journal->id=$Journal['Journal']['id'];
		if(!$this->Journal->saveField('flag','0'))
			throw new Exception("Error Processing Request", 1);
		if(!$this->Journal->saveField('updated_at',date('Y-m-d H:i:s')))
			throw new Exception("Error Processing Request", 1);
		$this->JournalVoucherDetail->id=$data['id'];
		if(!$this->JournalVoucherDetail->saveField('flag','0'))
			throw new Exception("Error Processing Request While delete Journal Voucher Details", 1);
		$datasource_JournalVoucherDetail->commit();
		$datasource_Journal->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$datasource_JournalVoucherDetail->rollback();
		$datasource_Journal->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		print_r($return['message']);die;
	}
	echo json_encode($return); exit;
}
public function JournalVoucherList()
	{
		$decimalpoint = $this->Session->read('decimalpoint');
		$this->set('decimalpoint',$decimalpoint);
		// $PermissionList = $this->Session->read('PermissionList');
		// $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Sale/InvoiceList'));

		// if(!in_array($menu_id, $PermissionList))
		// {
		//   $this->Session->setFlash("Permission denied");
		//   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$this->request->data['from_date']=date('d-m-Y');
		$this->request->data['to_date']=date('d-m-Y');
		// $executives=$this->Executive->find('list',array(
		// 'fields'=>['Executive.id','Executive.name']
		// ));
		// $executives+=['0' => 'NO EXECUTIVE'];
		// $this->set(compact('executives'));
	}

	// public function journal_voucher_detail_insert($journal_items,$id)
 //  {
 //    try {
 //      if($journal_items)
 //      {
 //       // pr($sale_items);exit;
 //        for ($i=0; $i <count($journal_items['dr_cr']) ; $i++) {
 //        	//$debit1=$debit;
	// 	    $account_head_id=$journal_items['acount_head_id'][$i];
	// //}     
	// 	    $dr_cr=$journal_items['dr_cr'][$i];
 //            $reference=$journal_items['reference'][$i];
 //            $amount_cr=$journal_items['amount_cr'][$i];
 //            $amount_dr=$journal_items['amount_dr'][$i];
 //           // $date=$journal_items['date'];
	// 		//$narration=$journal_items['narration'];
	//        // $cheque_date='00-00-0000';
	// 	   // $voucher_no=$data['journal_no'];
	// 		//$work_flow='Journal Voucher';
          
 //          $JournalVoucherDetail_data=[
 //          'journal_voucher_id'=>$id,
 //          'account_head_id'=>$account_head_id,
 //          'dr_cr'=>$dr_cr,
 //          'dr_amount'=>$amount_dr,
 //          'cr_amount'=>$amount_cr,
 //          'reference'=>$reference
 //          ];
 //          $this->JournalVoucherDetail->create();
 //          if(!$this->JournalVoucherDetail->save($JournalVoucherDetail_data))
 //          {
 //            $errors = $this->JournalVoucherDetail->validationErrors;
 //            foreach ($errors as $key => $value) {
 //              throw new Exception($value[0], 1);
 //            }
 //          }
         
 //        }
 //      }
 //      $return['result']='Success';
 //    } catch (Exception $e) {
 //      $return['result']=$e->getMessage();
 //    }
 //    return $return;
 //  }
	public function Ledger_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='code';
		$columns[]='AccountHead.name';
		$columns[]='main_group';
		$columns[]='sub_group';
		$columns[]='address';
		$columns[]='gstin';
		$columns[]='balance';
		$columns[]='id';
		$conditions=[];
	    if(isset($requestData['main_group_ids']))
		{
			if($requestData['main_group_ids'])
				$conditions['AccountHead.acc_main_group_id']=$requestData['main_group_ids'];
		}
		if(isset($requestData['sub_group_ids']))
		{
			if($requestData['sub_group_ids'])
				$conditions['AccountHead.acc_sub_group_id']=$requestData['sub_group_ids'];
		}
	    	 $totalData=$this->AccountHead->find('count',["joins"=>array(
					array(
					"table"=>'acc_sub_groups',
					"alias"=>'AccSubGroup',
					"type"=>'inner',
					"conditions"=>array('AccountHead.acc_sub_group_id=AccSubGroup.id'),
					),
					array(
					"table"=>'acc_main_groups',
					"alias"=>'AccMainGroup',
					"type"=>'inner',
					"conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
					),
                array(
                    "table"=>'acc_ledgers',
                    "alias"=>'AccLedger',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.account_head_id=AccountHead.id'),
                ),
               array(
                    "table"=>'customers',
                    "alias"=>'Customer',
                    "type"=>'left',
                    "conditions"=>array('Customer.account_head_id=AccountHead.id'),
                ),
                array(
                    "table"=>'parties',
                    "alias"=>'Party',
                    "type"=>'left',
                    "conditions"=>array('Party.account_head_id=AccountHead.id'),
                ),
            ),'conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'AccountHead.name LIKE' =>'%'. $q . '%',
    		'AccLedger.name LIKE' =>'%'. $q . '%',
    		);
    	$totalFiltered=$this->AccountHead->find('count',[
    		"joins"=>array(
    			array(
					"table"=>'acc_sub_groups',
					"alias"=>'AccSubGroup',
					"type"=>'inner',
					"conditions"=>array('AccountHead.acc_sub_group_id=AccSubGroup.id'),
					),
					array(
					"table"=>'acc_main_groups',
					"alias"=>'AccMainGroup',
					"type"=>'inner',
					"conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
					),
                array(
                    "table"=>'acc_ledgers',
                    "alias"=>'AccLedger',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.account_head_id=AccountHead.id'),
                ),
               array(
                    "table"=>'customers',
                    "alias"=>'Customer',
                    "type"=>'left',
                    "conditions"=>array('Customer.account_head_id=AccountHead.id'),
                ),
                array(
                    "table"=>'parties',
                    "alias"=>'Party',
                    "type"=>'left',
                    "conditions"=>array('Party.account_head_id=AccountHead.id'),
                ),
            ),
    		'conditions'=>$conditions,
    		]);
    }
    //order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column']==1)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
    $Data=$this->AccountHead->find('all',array(
    	"joins"=>array(
    		array(
					"table"=>'acc_sub_groups',
					"alias"=>'AccSubGroup',
					"type"=>'inner',
					"conditions"=>array('AccountHead.acc_sub_group_id=AccSubGroup.id'),
					),
					array(
					"table"=>'acc_main_groups',
					"alias"=>'AccMainGroup',
					"type"=>'inner',
					"conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
					),
                array(
                    "table"=>'acc_ledgers',
                    "alias"=>'AccLedger',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.account_head_id=AccountHead.id'),
                ),
               array(
                    "table"=>'customers',
                    "alias"=>'Customer',
                    "type"=>'left',
                    "conditions"=>array('Customer.account_head_id=AccountHead.id'),
                ),
                array(
                    "table"=>'parties',
                    "alias"=>'Party',
                    "type"=>'left',
                    "conditions"=>array('Party.account_head_id=AccountHead.id'),
                ),
            ),
    	'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	'order'=>$order,
    	'fields'=>array(
    		 'AccMainGroup.name','AccSubGroup.name','AccountHead.opening_balance', 'AccountHead.name','Customer.place','Party.place','AccLedger.address','Customer.code','Party.code','AccLedger.code','AccountHead.id','Party.id','Party.gstin','Customer.gstin','Customer.id','AccLedger.gstin','AccLedger.id','AccountHead.acc_sub_group_id'
    		)
    	));
			foreach ($Data as $key => $value) {
				$Data[$key]['AccountHead']['main_group']=$value['AccMainGroup']['name'];
				$Data[$key]['AccountHead']['sub_group']=$value['AccSubGroup']['name'];
				  $Data[$key]['AccountHead']['action']='<span style="display:none" class="SubGroup_id">'.$value['AccountHead']['acc_sub_group_id'].'</span>';
		  if($value['AccountHead']['acc_sub_group_id']==2)
		  {
		  		$Data[$key]['AccountHead']['gstin']=$value['Customer']['gstin'];
		  		$Data[$key]['AccountHead']['address']=$value['Customer']['place'];
		       $Data[$key]['AccountHead']['code']=$value['Customer']['code'];
		  	    $Data[$key]['AccountHead']['name']='<span style="display:none" class="AccountHead_id">'.$value['Customer']['id'].'</span><span style="display:none" class="hidden_AccountHead_id">'.$value['AccountHead']['id'].'</span><span class="name">'.ucwords(strtolower($value['AccountHead']['name'])).'</span>';	
		      if($value['Customer']['id']==1)
		      {
		      $Data[$key]['AccountHead']['action'].='<span><i table_id="'.$value['Customer']['id'].'" class="fa fa-2x fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#customer_modal_edit"></i></span>';
		      }
		      else
		      {
		     $Data[$key]['AccountHead']['action'].='<span><i table_id="'.$value['Customer']['id'].'" class="fa fa-2x fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#customer_modal_edit"></i></span><span><i table_id="'.$value['Customer']['id'].'" class="fa fa-2x fa-trash blue-col ledger_delete"></i></span>';
		      }
		  }
		  else if($value['AccountHead']['acc_sub_group_id']==1)
		  {
		  		$Data[$key]['AccountHead']['gstin']=$value['Party']['gstin'];
		  		$Data[$key]['AccountHead']['address']=$value['Party']['place'];
		  		$Data[$key]['AccountHead']['code']=$value['Party']['code'];
		  		$Data[$key]['AccountHead']['name']='<span style="display:none" class="AccountHead_id">'.$value['Party']['id'].'</span><span style="display:none" class="hidden_AccountHead_id">'.$value['AccountHead']['id'].'</span><span class="name">'.ucwords(strtolower($value['AccountHead']['name'])).'</span>';			
		 		$Data[$key]['AccountHead']['action'].='<span><i table_id="'.$value['Party']['id'].'" class="fa fa-2x fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#customer_modal_edit"></i></span>';

		  }
		  else
		  {
		  		$Data[$key]['AccountHead']['gstin']=$value['AccLedger']['gstin'];
		  		$Data[$key]['AccountHead']['address']=$value['AccLedger']['address'];
		  		$Data[$key]['AccountHead']['code']=$value['AccLedger']['code'];
		  		$Data[$key]['AccountHead']['name']='<span style="display:none" class="AccountHead_id">'.$value['AccLedger']['id'].'</span><span style="display:none" class="hidden_AccountHead_id">'.$value['AccountHead']['id'].'</span><span class="name">'.ucwords(strtolower($value['AccountHead']['name'])).'</span>';			
		  		$Data[$key]['AccountHead']['action'].='<span><i table_id="'.$value['AccLedger']['id'].'" class="fa fa-2x fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#customer_modal_edit"></i></span>';
		}
			$account_head_id=$value['AccountHead']['id'];
			$opening_balance=$value['AccountHead']['opening_balance'];
			$total=$opening_balance;
			$paid=0;
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
			$total+=$Debit_N_Credit_function['debit'];
			$paid+=$Debit_N_Credit_function['credit'];
			$balance=$total-$paid;
			if($balance>0){
				$balance=number_format($balance,2,'.','').' Dr';
			}else{
				$balance=abs(number_format($balance,2,'.','')).' Cr';
			}
			$Data[$key]['AccountHead']['balance']=$balance;
		}	
		 //sorting total/balance/recieved
		if($requestData['order'][0]['column'] != 1) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["AccountHead"][$columnname] == $b["AccountHead"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["AccountHead"][$columnname] < $b["AccountHead"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["AccountHead"][$columnname] > $b["AccountHead"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
		$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;
	}
public function Ledger_report_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='code';
		$columns[]='AccountHead.name';
		$columns[]='address';
		$columns[]='gstin';
		$columns[]='balance';
		$conditions=[];
	    if(isset($requestData['main_group_ids']))
		{
			if($requestData['main_group_ids'])
				$conditions['AccountHead.acc_main_group_id']=$requestData['main_group_ids'];
		}
		if(isset($requestData['sub_group_ids']))
		{
			if($requestData['sub_group_ids'])
				$conditions['AccountHead.acc_sub_group_id']=$requestData['sub_group_ids'];
		}
		// if(isset($requestData['district_id']))
		// {
		// 	if($requestData['district_id'])
		// 		$conditions['OR']=array(
  //   		'AccLedger.district' =>$requestData['district_id'],
  //   		 'Customer.district' =>$requestData['district_id'],
  //   		'Party.district_id' =>$requestData['district_id'],

  //   		);
		// }
		$conditions['AccountHead.created_at <=']=date('Y-m-d',strtotime($requestData['to_date']));
			    //$conditions['AccountHead.id !=']=19;
	    	 $totalData=$this->AccountHead->find('count',["joins"=>array(
                array(
                    "table"=>'acc_ledgers',
                    "alias"=>'AccLedger',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.account_head_id=AccountHead.id'),
                ),
               array(
                    "table"=>'customers',
                    "alias"=>'Customer',
                    "type"=>'left',
                    "conditions"=>array('Customer.account_head_id=AccountHead.id'),
                ),
                array(
                    "table"=>'parties',
                    "alias"=>'Party',
                    "type"=>'left',
                    "conditions"=>array('Party.account_head_id=AccountHead.id'),
                ),
            ),'conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'AccountHead.name LIKE' =>'%'. $q . '%',
    		);
    	$totalFiltered=$this->AccountHead->find('count',[
    		"joins"=>array(
                array(
                    "table"=>'acc_ledgers',
                    "alias"=>'AccLedger',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.account_head_id=AccountHead.id'),
                ),
               array(
                    "table"=>'customers',
                    "alias"=>'Customer',
                    "type"=>'left',
                    "conditions"=>array('Customer.account_head_id=AccountHead.id'),
                ),
                array(
                    "table"=>'parties',
                    "alias"=>'Party',
                    "type"=>'left',
                    "conditions"=>array('Party.account_head_id=AccountHead.id'),
                ),
            ),
    		'conditions'=>$conditions,
    		]);
    }
    //order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column']==1)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
    $Data=$this->AccountHead->find('all',array(
    	"joins"=>array(
                array(
                    "table"=>'acc_ledgers',
                    "alias"=>'AccLedger',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.account_head_id=AccountHead.id'),
                ),
               array(
                    "table"=>'customers',
                    "alias"=>'Customer',
                    "type"=>'left',
                    "conditions"=>array('Customer.account_head_id=AccountHead.id'),
                ),
                array(
                    "table"=>'parties',
                    "alias"=>'Party',
                    "type"=>'left',
                    "conditions"=>array('Party.account_head_id=AccountHead.id'),
                ),
            ),
    	'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	'order'=>$order,
    	'fields'=>array(
    		 'AccountHead.opening_balance', 'AccountHead.name','Customer.place','Party.place','AccLedger.address','Customer.code','Party.code','AccLedger.code','AccountHead.id','Party.id','Party.gstin','Customer.gstin','Customer.id','AccLedger.gstin','AccLedger.id','AccountHead.acc_sub_group_id'
    		)
    	));
			foreach ($Data as $key => $value) {
				  $Data[$key]['AccountHead']['action']='<span style="display:none" class="SubGroup_id">'.$value['AccountHead']['acc_sub_group_id'].'</span>';
		  if($value['AccountHead']['acc_sub_group_id']==2)
		  {
		  		$Data[$key]['AccountHead']['gstin']=$value['Customer']['gstin'];
		  		$Data[$key]['AccountHead']['address']=$value['Customer']['place'];
		       $Data[$key]['AccountHead']['code']=$value['Customer']['code'];
              $Data[$key]['AccountHead']['name']='<span style="display:none" class="AccountHead_id">'.$value['Customer']['id'].'</span><span style="display:none" class="hidden_AccountHead_id">'.$value['AccountHead']['id'].'</span><span class="name">'.ucwords(strtolower($value['AccountHead']['name'])).'</span>';			  }
		  else if($value['AccountHead']['acc_sub_group_id']==1)
		  {
		  		$Data[$key]['AccountHead']['gstin']=$value['Party']['gstin'];
		  		$Data[$key]['AccountHead']['address']=$value['Party']['place'];
		  		$Data[$key]['AccountHead']['code']=$value['Party']['code'];
		  		$Data[$key]['AccountHead']['name']='<span style="display:none" class="AccountHead_id">'.$value['Party']['id'].'</span><span style="display:none" class="hidden_AccountHead_id">'.$value['AccountHead']['id'].'</span><span class="name">'.ucwords(strtolower($value['AccountHead']['name'])).'</span>';			

		  }
		  else
		  {
		  		$Data[$key]['AccountHead']['gstin']=$value['AccLedger']['gstin'];
		  		$Data[$key]['AccountHead']['address']=$value['AccLedger']['address'];
		  		$Data[$key]['AccountHead']['code']=$value['AccLedger']['code'];
		  		$Data[$key]['AccountHead']['name']='<span style="display:none" class="AccountHead_id">'.$value['AccLedger']['id'].'</span><span style="display:none" class="hidden_AccountHead_id">'.$value['AccountHead']['id'].'</span><span class="name">'.ucwords(strtolower($value['AccountHead']['name'])).'</span>';			
		  }
			$account_head_id=$value['AccountHead']['id'];
			$opening_balance=$value['AccountHead']['opening_balance'];
			$total=$opening_balance;
			$paid=0;
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
			$total+=$Debit_N_Credit_function['debit'];
			$paid+=$Debit_N_Credit_function['credit'];
			$balance=$total-$paid;
			if($balance>0){
				$balance=number_format($balance,2,'.','').' Dr';
			}else{
				$balance=abs(number_format($balance,2,'.','')).' Cr';
			}
			$Data[$key]['AccountHead']['balance']=$balance;
		}	
		//sorting total/balance/recieved
		if($requestData['order'][0]['column'] != 1) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["AccountHead"][$columnname] == $b["AccountHead"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["AccountHead"][$columnname] < $b["AccountHead"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["AccountHead"][$columnname] > $b["AccountHead"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
		$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;
	}

public function Customer_Ledger_report_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='code';
		$columns[]='AccountHead.name';
		$columns[]='address';
		$columns[]='gstin';
		$columns[]='balance';
		$conditions=[];
		if(isset($requestData['route_id']))
		{
			if($requestData['route_id'])
			{
				$conditions['Customer.route_id']=$requestData['route_id'];
			}
		}
		if(isset($requestData['executive_id']))
		{
			if($requestData['executive_id'])
			{
				$conditions['ExecutiveRouteMapping.executive_id']=$requestData['executive_id'];
			}
		}
		
		$conditions['AccountHead.created_at <']=date('Y-m-d',strtotime($requestData['to_date']));
	    	 $totalData=$this->AccountHead->find('count',["joins"=>array(
               array(
                    "table"=>'customers',
                    "alias"=>'Customer',
                    "type"=>'inner',
                    "conditions"=>array('Customer.account_head_id=AccountHead.id'),
                ),
              array(
					'table'=>'executive_route_mappings',
					'alias'=>'ExecutiveRouteMapping',
					'type'=>'LEFT',
					'conditions'=>array('ExecutiveRouteMapping.route_id=Customer.route_id')
					),
            ),
	    	 'group'=>array('Customer.account_head_id'),
	    	 'conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'AccountHead.name LIKE' =>'%'. $q . '%',
    		);
    	$totalFiltered=$this->AccountHead->find('count',[
    		"joins"=>array(
               array(
                    "table"=>'customers',
                    "alias"=>'Customer',
                    "type"=>'inner',
                    "conditions"=>array('Customer.account_head_id=AccountHead.id'),
                ),
                array(
					'table'=>'executive_route_mappings',
					'alias'=>'ExecutiveRouteMapping',
					'type'=>'LEFT',
					'conditions'=>array('ExecutiveRouteMapping.route_id=Customer.route_id')
					),
            ),
           'group'=>array('Customer.account_head_id'),
    		'conditions'=>$conditions,
    		]);
    }
    //order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column']==1)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
    $Data=$this->AccountHead->find('all',array(
    	"joins"=>array(
               array(
                    "table"=>'customers',
                    "alias"=>'Customer',
                    "type"=>'Inner',
                    "conditions"=>array('Customer.account_head_id=AccountHead.id'),
                ),
                array(
					'table'=>'executive_route_mappings',
					'alias'=>'ExecutiveRouteMapping',
					'type'=>'LEFT',
					'conditions'=>array('ExecutiveRouteMapping.route_id=Customer.route_id')
					),
            ),
    	'group'=>array('Customer.account_head_id'),
    	'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	'order'=>$order,
    	'fields'=>array(
    		 'AccountHead.opening_balance', 'AccountHead.name','Customer.place','Customer.code','AccountHead.id','Customer.gstin','Customer.id','AccountHead.acc_sub_group_id'
    		)
    	));
			foreach ($Data as $key => $value) {
				  $Data[$key]['AccountHead']['action']='<span style="display:none" class="SubGroup_id">'.$value['AccountHead']['acc_sub_group_id'].'</span>';
		  		$Data[$key]['AccountHead']['gstin']=$value['Customer']['gstin'];
		  		$Data[$key]['AccountHead']['address']=$value['Customer']['place'];
		       $Data[$key]['AccountHead']['code']=$value['Customer']['code'];
              $Data[$key]['AccountHead']['name']='<span style="display:none" class="AccountHead_id">'.$value['Customer']['id'].'</span><span style="display:none" class="hidden_AccountHead_id">'.$value['AccountHead']['id'].'</span><span class="name">'.ucwords(strtolower($value['AccountHead']['name'])).'</span>';		
			$account_head_id=$value['AccountHead']['id'];
			$opening_balance=$value['AccountHead']['opening_balance'];
			$total=$opening_balance;
			$paid=0;
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
			$total+=$Debit_N_Credit_function['debit'];
			$paid+=$Debit_N_Credit_function['credit'];
			$balance=$total-$paid;
			if($balance>0){
				$balance=number_format($balance,2,'.','').' Dr';
			}else{
				$balance=abs(number_format($balance,2,'.','')).' Cr';
			}
			$Data[$key]['AccountHead']['balance']=$balance;
		}	
		//sorting total/balance/recieved
		if($requestData['order'][0]['column'] != 1) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["AccountHead"][$columnname] == $b["AccountHead"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["AccountHead"][$columnname] < $b["AccountHead"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["AccountHead"][$columnname] > $b["AccountHead"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
		$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;
	}
	public function get_total_amount_customer_wise()
	{
		$requestData=$this->request->data;
		$to_date=$requestData['to_date'];
		if(!empty($requestData['route_id']))
		{
			if($requestData['route_id'])
			{
				$conditions['Customer.route_id']=$requestData['route_id'];
			}
		}
		if(!empty($requestData['executive_id']))
		{
			if($requestData['executive_id'])
			{
				$conditions['ExecutiveRouteMapping.executive_id']=$requestData['executive_id'];
			}
		}
		$conditions['AccountHead.created_at <']=date('Y-m-d',strtotime($to_date));
    $Data=$this->AccountHead->find('list',array(
    	"joins"=>array(
               array(
                    "table"=>'customers',
                    "alias"=>'Customer',
                    "type"=>'Inner',
                    "conditions"=>array('Customer.account_head_id=AccountHead.id'),
                ),
                array(
					'table'=>'executive_route_mappings',
					'alias'=>'ExecutiveRouteMapping',
					'type'=>'LEFT',
					'conditions'=>array('ExecutiveRouteMapping.route_id=Customer.route_id')
					),
            ),
    	'group'=>array('Customer.account_head_id'),
    	'conditions'=>$conditions,
    	'fields'=>array(
    		 'AccountHead.id', 'AccountHead.id'
    		)
    	));
    $total_balance=0;
			foreach ($Data as $key => $value) {
			$account_head_id=$value;
			$AccountHead=$this->AccountHead->findById($account_head_id);
			$opening_balance=$AccountHead['AccountHead']['opening_balance'];
			$total=$opening_balance;
			$paid=0;
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
			$total+=$Debit_N_Credit_function['debit'];
			$paid+=$Debit_N_Credit_function['credit'];
			$balance=$total-$paid;
			$total_balance+=$balance;
		}	
		if($total_balance>0){
				$total_balance=number_format($total_balance,2,'.','').' Dr';
			}else{
				$total_balance=abs(number_format($total_balance,2,'.','')).' Cr';
			}
	$result=$total_balance;
	echo json_encode($result);
	exit;
	}
	 public function GetSubGroup($id=null)
  {
  	//pr($id);
  	//exit;
    // $return[ 'sub_group_ids' ]=$this->SystemParameter->field('value',array('id'=>9));
      //$return[ 'mode' ]=$this->SystemParameter->field('value',array('id'=>2));
      // $piece=$this->SystemParameter->field('value',array('id'=>1));
     
      // if($piece=='yes'){
      // 	$return['casepiece']='piece';
      // }else{
      // 	$return['casepiece']='case';
      // }
       $conditions=array();
		if(!empty($id))
		{
			$conditions['AccMainGroup.id']=$id;
		}
		//$return['option']='<option value="">SELECT</option>';
		$AccSubGroup = $this->AccSubGroup->find('list', array(
			'joins'=>array(
				array(
					"table"=>'acc_main_groups',
					"alias"=>'AccMainGroup',
					"type"=>'inner',
					"conditions"=>array('AccMainGroup.id=AccSubGroup.main_group_id'),
				),
			),
			'conditions' =>$conditions ,
			//'order'=>array('AccMainGroup.name'),
			'fields' => array(
				'AccSubGroup.id',
				'AccSubGroup.name',
			)
		)
	);
		$return['options']=$AccSubGroup;
	// 	foreach ($AccSubGroup as $key => $value) {
			
	// 	$return['option']='<option value='.$value['AccSubGroup']['id'].'>'.$value['AccSubGroup']['name'].'</option>';
	// }
	//pr($return);
	//exit;
		//$return['option']=$AccSubGroup['AccSubGroup']['name'];
			$return['result']='Success';

		echo json_encode($return);
		exit;
	}
	public function General_Journal_Debit_N_Credit_function($account_head_id,$user_branch_id=null)
{
	//$account_head_id=3;
	//$bank=10;
	$bank=$this->SystemParameter->field('value',array('id'=>10));
	$conditions_debit=[];
	$conditions_credit=[];

	if($user_branch_id)
	{
		$conditions_debit['branch_id']=$user_branch_id;
		$conditions_credit['branch_id']=$user_branch_id;
	}

	$conditions_debit['debit']=$account_head_id;
	$conditions_debit['flag']=1;
	 $conditions_debit['credit !=']=19;
	 $conditions_credit['debit !=']=19;
	$conditions_credit['credit']=$account_head_id;
	//$conditions_credit['credit']=$account_head_id;
	$conditions_credit['flag']=1;
	$bank_list=$this->AccountHead_Option_ListBySubGroupId($bank);
	// $bank=['1'];
	$bank=[];

	foreach ($bank_list as $key => $value) {
		array_push($bank, $key);
	}
	// $conditions_debit['debit']=$bank;
	// $conditions_debit['flag']=1;
	// $conditions_credit['credit']=$bank;
	// $conditions_credit['flag']=1;
	$account_single=[];
	$account_single['date']="";
	$account_single['debit']=0;
	$account_single['credit']=0;
	$Journal_Debit=$this->Journal->find('all',array('conditions'=>$conditions_debit));
	foreach ($Journal_Debit as $key => $value) {
		$account_single['debit']+=$value['Journal']['amount'];
		$account_single['date']=$value['Journal']['date'];
	}
	$Journal_Credit=$this->Journal->find('all',array('conditions'=>array($conditions_credit,'debit !='=>$account_head_id)));
	foreach ($Journal_Credit as $key => $value) {
		$account_single['credit']+=$value['Journal']['amount'];
		$account_single['date']=$value['Journal']['date'];
	}
	//pr($account_single);exit;
	return $account_single;
}

public function AccountHead_Option_ListBySubGroupId($sub_group_id)
{
	$AccountHeads=$this->AccountHead->find('all',array(
					'joins'=>array(
				array(
					'table'=>'acc_sub_groups',
					'alias'=>'AccSubGroup',
					'type'=>'INNER',
					'conditions'=>array('AccountHead.acc_sub_group_id=AccSubGroup.id')
					),
				),
		'conditions'=>array(
			'AccSubGroup.id'=>$sub_group_id,
			),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'AccSubGroup.id',
			)
		));
	$AccountHead_list=[];
	foreach ($AccountHeads as $key => $value) {
		$AccountHead_list[$value['AccountHead']['id']] = $value['AccountHead']['name'];
	}
	return $AccountHead_list;
}
public function TrialBalanceReport()
{
	$user_branch_id=$this->Session->read('User.branch_id');
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/AssetCurrentAccountRecievable';
// 		$menu_id=$this->Menu->field('Menu.id');
// 	$menu_id = $this->Menu->field(
// 		'Menu.id',
// 		array('action ' => 'Accountings/AssetCurrentAccountRecievable'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	//$sub_group_id=3;
	//$cost_center_id=$this->SystemParameter->field('value',array('id'=>11));

	//pr($user_cost);
	//exit;
	$user_id=1;
	if(!$this->request->data)
	{

		$from_date=date('d-m-Y',strtotime('-1 month'));
			$to_date=date('d-m-Y');
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			//$this->request->data=$data;

		$CustomerType_list=$this->CustomerType->find('list',array('fields'=>array('id','name')));
 		$Ledger_list=$this->AccLedger->find('list',array('fields'=>array('id','name')));
 		$this->set('Ledger_list',$Ledger_list);
 		$SubGroup_list=$this->AccSubGroup->find('list',array('fields'=>array('id','name')));
 		$this->set('SubGroup_list',$SubGroup_list);
 		$MainGroup_list=$this->AccMainGroup->find('list',array('fields'=>array('id','name')));
 		$this->set('MainGroup_list',$MainGroup_list);
 		 $State_list=$this->State->find('list',array('fields'=>array('id','name')));
		 $this->set('State_list',$State_list);
		  $District_list=$this->District->find('list',array('fields'=>array('id','name')));
		 $this->set('District_list',$District_list);
		  $Country_list=$this->Country->find('list',array('fields'=>array('id','name')));
		 $this->set('Country_list',$Country_list);
		 $Status=[
		'1'=>'Active',
		'0'=>'Inactive',
		
		// '3'=>'Expense',
		// '4'=>'Income',
		// '5'=>'Liabilities',
		];
		$this->set('Status',$Status);
		 $cost_center=[
		'1'=>'Yes',
		'0'=>'No',
		
		// '3'=>'Expense',
		// '4'=>'Income',
		// '5'=>'Liabilities',
		];
		$this->set('cost_center',$cost_center);
		// $AccountHead_list=$this->AccountHead->find('all',array('joins' => [
		// 	[
		// 	"table" => "customers",
		// 	"alias" => "Customer",
		// 	"type" => "INNER",
		// 	"conditions" => ['AccountHead.id=Customer.account_head_id'],
		// 	],
		// 	],'fields'=>array('AccountHead.id','AccountHead.name','Customer.code'),'conditions'=>array('sub_group_id'=>3)));
		// $Customer_list=[];
		// foreach ($AccountHead_list as $key => $value) {
		// 	$Customer_list[$value['AccountHead']['id']] = $value['AccountHead']['name'].' '.$value['Customer']['code'];
		// }
		// $Executive_list=$this->Executive->find('list',array('fields'=>array('id','name')));
		// $this->set('Executive_list',$Executive_list);
		$this->Route->virtualFields = array(
			'route_name' => "CONCAT(Route.name, ' ', Route.code)"
			);
		$conditions_branch_route=[];

		if($user_branch_id)
		{
			$route_id=$this->BranchRouteMapping->find('list',array('conditions'=>array('BranchRouteMapping.branch_id'=>$user_branch_id),'fields'=>['BranchRouteMapping.id','BranchRouteMapping.route_id']));
			$conditions_branch_route['Route.id']=$route_id;
		}
		// $this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
		// $Product_list=$this->Product->find('list',array('fields'=>['id','product_name']));
		// $this->set(compact('Product_list'));
		 $Route_list=$this->Route->find('list',array('conditions'=>$conditions_branch_route,'fields'=>array('id','route_name')));
		 $this->set(compact('Route_list'));
		 $CustomerGroup_list=$this->CustomerGroup->find('list',array('fields'=>array('id','name')));
		 $this->set(compact('CustomerGroup_list'));
		// $Division_list=$this->Division->find('list');
		// $this->set(compact('Division_list'));
// $State_list=$this->State->find('list',array('fields'=>array('id','name')));
// $this->set(compact('State_list'));
// $Day_list=array(
//      'Monday'=>'Monday',
//      'Tuesday'=>'Tuesday',
//      'Wednesday'=>'Wednesday',
//      'Thursday'=>'Thursday',
//      'Friday'=>'Friday',
//      'Saturday'=>'Saturday',
//      'Sunday'=>'Sunday',
//      );
//    $this->set(compact('Day_list'));
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
	//	$data['Ledger']['opening_balance']='0';
		//$conditions_branch=[];

		if($user_branch_id)
		{
			$route_id=$this->BranchRouteMapping->find('list',array('conditions'=>array('BranchRouteMapping.branch_id'=>$user_branch_id),'fields'=>['BranchRouteMapping.id','BranchRouteMapping.route_id']));
			$conditions_branch['Customer.route_id']=$route_id;
		}
		$user_cost=$this->Session->read('SystemParameter.cost_center_id');
		$this->set('user_cost',$user_cost);
	    //$data['Ledger']['c_center']=$user_cost;
		$this->request->data=$data;

		$modalCustomerType_list = $CustomerType_list;
// 		unset($modalCustomerType_list[1]);
 		$this->set('CustomerType_list',$CustomerType_list);
 		$this->set('modalCustomerType_list',$modalCustomerType_list);
// 		$this->set('Customer_list',$Customer_list);
// // $this->set('Customer',$Customer);
 		//$this->set('Ledger',$Ledger_All);
	}

}

public function TrialBalance_Table_ajax()
{
		$data=$this->request->data;
		//$from_date=$data['from_date'];
		//$to_date=$data['to_date'];
		//$from_date=date('Y-m-d',strtotime(($from_date)));
		//$to_date=date('Y-m-d',strtotime(($to_date)));
		$conditions=[];
	if(!empty($data)){
		if(!empty($data['main_group_ids'])){
			$conditions['AccMainGroup.id']=$data['main_group_ids'];
		}
		if(!empty($data['sub_group_ids'])){
			$conditions['AccSubGroup.id']=$data['sub_group_ids'];
		}
	}
	    // else{
	    // 	$conditions='';
	    // }
		//$list_array=array();
		//$Ledger_All=[];
	$AccLedger=$this->AccLedger->find('all',array(
		"joins"=>array(
			array(
				"table"=>'acc_sub_groups',
				"alias"=>'AccSubGroup',
				"type"=>'left',
				"conditions"=>array('AccLedger.sub_group_id=AccSubGroup.id'),
			),
			array(
				"table"=>'acc_main_groups',
				"alias"=>'AccMainGroup',
				"type"=>'left',
				"conditions"=>array('AccLedger.main_group_id=AccMainGroup.id'),
			),
			array(
				"table"=>'states',
				"alias"=>'State',
				"type"=>'left',
				"conditions"=>array('AccLedger.state=State.id'),
			),
			array(
				"table"=>'districts',
				"alias"=>'District',
				"type"=>'left',
				"conditions"=>array('AccLedger.district=District.id'),
			),
			array(
				"table"=>'countries',
				"alias"=>'Country',
				"type"=>'left',
				"conditions"=>array('AccLedger.country=Country.id'),
			),
		),
		'conditions'=>array($conditions),
		'fields'=>array('AccLedger.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
		
		//'order'=>array('AccountHead.name ASC'),
		// 'limit'=>110,
	));
		// pr($AccLedger);
		// exit;
	$Ledger_All=[];
	foreach ($AccLedger as $key => $value) {
			$account_heads=$this->AccountHead->findByName($value['AccLedger']['name']);
		if($account_heads){
            $account_head_id=$account_heads['AccountHead']['id'];
            $opening_balance=$account_heads['AccountHead']['opening_balance'];
            $total=$opening_balance;
             $paid=0;
			//$Data[$key]['AccountHead']['recieved']=0;
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
			$total+=$Debit_N_Credit_function['debit'];
			$paid+=$Debit_N_Credit_function['credit'];
			$balance=$total-$paid;
        }
		$single_row['sub_group']= $value['AccSubGroup']['name'];
		$single_row['sub_group_id']= $value['AccLedger']['sub_group_id'];
		$single_row['main_group']= $value['AccMainGroup']['name'];
		$single_row['main_group_id']= $value['AccLedger']['main_group_id'];
		$single_row['address']= $value['AccLedger']['address'];
		$single_row['district']= $value['District']['name'];
		$single_row['pin_code']= $value['AccLedger']['pin_code'];
		$single_row['state']= $value['State']['name'];
		$single_row['name']= $value['AccLedger']['name'];
		if($value['AccLedger']['status']==1)
		{
		$single_row['status']='Active';
		}
		elseif($value['AccLedger']['status']==0)
		{
			$single_row['status']="Inactive";

		}
		$single_row['code']= $value['AccLedger']['code'];
		$single_row['country']= $value['Country']['name'];
		$single_row['telephone']= $value['AccLedger']['telephone'];
		$single_row['email']= $value['AccLedger']['email'];
		$single_row['website']= $value['AccLedger']['website'];
		$single_row['contact_person']= $value['AccLedger']['contact_person'];
		$single_row['contact_no']= $value['AccLedger']['contact_no'];
		$single_row['gstin']= $value['AccLedger']['gstin'];
		$single_row['credit_limit']= $value['AccLedger']['credit_limit'];
		$single_row['credit_period']= $value['AccLedger']['credit_period'];
		// $single_row['opening_balance']= $value['AccLedger']['opening_balance'];
		$single_row['opening_balance']= $total-$paid;
		$single_row['other_notes']= $value['AccLedger']['other_notes'];
		$single_row['id']= $value['AccLedger']['id'];
		$single_row['debit']= $total;
		$single_row['credit']= $paid;
		$single_row['balance']= $balance;
		array_push($Ledger_All, $single_row);
	}
	$this->Customer->unbindModel(array('belongsTo'=>array('State')));
	$CUstomer=$this->Customer->find('all',array(
		"joins"=>array(
			array(
				"table"=>'acc_sub_groups',
				"alias"=>'AccSubGroup',
				"type"=>'left',
				"conditions"=>array('Customer.sub_group_id=AccSubGroup.id'),
			),
			array(
				"table"=>'acc_main_groups',
				"alias"=>'AccMainGroup',
				"type"=>'left',
				"conditions"=>array('Customer.main_group_id=AccMainGroup.id'),
			),
			array(
				"table"=>'states',
				"alias"=>'State',
				"type"=>'left',
				"conditions"=>array('Customer.state_id=State.id'),
			),
			array(
				"table"=>'districts',
				"alias"=>'District',
				"type"=>'left',
				"conditions"=>array('Customer.district=District.id'),
			),
			array(
				"table"=>'countries',
				"alias"=>'Country',
				"type"=>'left',
				"conditions"=>array('Customer.country=Country.id'),
			),
		),
		'conditions'=>array($conditions),
		'fields'=>array('Customer.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
		
		//'order'=>array('AccountHead.name ASC'),
		// 'limit'=>110,
	));
		// pr($CUstomer);
		// exit;
		//$Ledger_All=[];
	foreach ($CUstomer as $key1 => $value1) {
		$account_heads=$this->AccountHead->findByName($value1['Customer']['name']);
		if($account_heads){
            $account_head_id=$account_heads['AccountHead']['id'];
            $opening_balance=$account_heads['AccountHead']['opening_balance'];
            $total=$opening_balance;
             $recieved=0;
			//$Data[$key]['AccountHead']['recieved']=0;
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
			$total+=$Debit_N_Credit_function['debit'];
			$recieved+=$Debit_N_Credit_function['credit'];
			$balance=$total-$recieved;
        }
		$single1_row['sub_group']= $value1['AccSubGroup']['name'];
		$single1_row['sub_group_id']= $value1['Customer']['sub_group_id'];
		$single1_row['main_group']= $value1['AccMainGroup']['name'];
		$single1_row['main_group_id']= $value1['Customer']['main_group_id'];
		$single1_row['address']= $value1['Customer']['place'];
		$single1_row['district']= $value1['District']['name'];
		$single1_row['pin_code']= $value1['Customer']['pin_code'];
		$single1_row['state']= $value1['State']['name'];
		$single1_row['name']= $value1['Customer']['name'];
		if($value1['Customer']['status']==1)
		{
		$single1_row['status']='Active';
		}
		elseif($value1['Customer']['status']==0)
		{
			$single1_row['status']="Inactive";

		}
		$single1_row['code']= $value1['Customer']['code'];
		$single1_row['country']= $value1['Country']['name'];
		$single1_row['telephone']= $value1['Customer']['telephone'];
		$single1_row['email']= $value1['Customer']['email'];
		$single1_row['website']= $value1['Customer']['website'];
		$single1_row['contact_person']= $value1['Customer']['contact_person'];
		$single1_row['contact_no']= $value1['Customer']['contact_no'];
		$single1_row['gstin']= $value1['Customer']['gstin'];
		$single1_row['credit_limit']= $value1['Customer']['credit_limit'];
		$single1_row['credit_period']= $value1['Customer']['credit_period'];
		// $single1_row['opening_balance']= $value1['Customer']['opening_balance'];
		$single1_row['opening_balance']= $total-$recieved;
		$single1_row['other_notes']= $value1['Customer']['other_notes'];
		$single1_row['id']= $value1['Customer']['id'];
		$single1_row['debit']= $total;
		$single1_row['credit']= $recieved;
		$single1_row['balance']= $balance;
		array_push($Ledger_All, $single1_row);
	}
	$this->Party->unbindModel(array('belongsTo'=>array('State')));
	$PArty=$this->Party->find('all',array(
		"joins"=>array(
			array(
				"table"=>'acc_sub_groups',
				"alias"=>'AccSubGroup',
				"type"=>'left',
				"conditions"=>array('Party.sub_group_id=AccSubGroup.id'),
			),
			array(
				"table"=>'acc_main_groups',
				"alias"=>'AccMainGroup',
				"type"=>'left',
				"conditions"=>array('Party.main_group_id=AccMainGroup.id'),
			),
			array(
				"table"=>'states',
				"alias"=>'State',
				"type"=>'left',
				"conditions"=>array('Party.state_id=State.id'),
			),
			array(
				"table"=>'districts',
				"alias"=>'District',
				"type"=>'left',
				"conditions"=>array('Party.district_id=District.id'),
			),
			array(
				"table"=>'countries',
				"alias"=>'Country',
				"type"=>'left',
				"conditions"=>array('Party.country_id=Country.id'),
			),
		),
		'conditions'=>array($conditions),
		'fields'=>array('Party.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
		
		//'order'=>array('AccountHead.name ASC'),
		// 'limit'=>110,
	));
		// pr($CUstomer);
		// exit;
		//$Ledger_All=[];
	foreach ($PArty as $key2 => $value2) {

			$account_heads=$this->AccountHead->findByName($value2['Party']['name']);
		if($account_heads){
            $account_head_id=$account_heads['AccountHead']['id'];
            $opening_balance=$account_heads['AccountHead']['opening_balance'];
            $total=$opening_balance;
             $paid=0;
			//$Data[$key]['AccountHead']['recieved']=0;
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
			$total+=$Debit_N_Credit_function['credit'];
			$paid+=$Debit_N_Credit_function['debit'];
			$balance=$total-$paid;
        }
		$single2_row['sub_group']= $value2['AccSubGroup']['name'];
		$single2_row['sub_group_id']= $value2['Party']['sub_group_id'];
		$single2_row['main_group']= $value2['AccMainGroup']['name'];
		$single2_row['main_group_id']= $value2['Party']['main_group_id'];
		$single2_row['address']= $value2['Party']['address'];
		$single2_row['district']= $value2['District']['name'];
		$single2_row['pin_code']= $value2['Party']['pin_code'];
		$single2_row['state']= $value2['State']['name'];
		$single2_row['name']= $value2['Party']['name'];
		if($value2['Party']['status']==1)
		{
		$single2_row['status']='Active';
		}
		elseif($value2['Party']['status']==0)
		{
			$single2_row['status']="Inactive";

		}
		$single2_row['code']= $value2['Party']['code'];
		$single2_row['country']= $value2['Party']['name'];
		$single2_row['telephone']= $value2['Party']['land_line'];
		$single2_row['email']= $value2['Party']['email'];
		$single2_row['website']= $value2['Party']['website'];
		$single2_row['contact_person']= $value2['Party']['contact_person'];
		$single2_row['contact_no']= $value2['Party']['contact_no'];
		$single2_row['gstin']= $value2['Party']['gstin'];
		$single2_row['credit_limit']= $value2['Party']['credit_limit'];
		$single2_row['credit_period']= $value2['Party']['credit_period'];
		// $single2_row['opening_balance']= $value2['Party']['opening_balance'];
		$single2_row['opening_balance']= $paid-$total;
		$single2_row['other_notes']= $value2['Party']['other_notes'];
		$single2_row['id']= $value2['Party']['id'];
		$single2_row['debit']= $paid;
		$single2_row['credit']= $total;
		$single2_row['balance']= $balance;
		array_push($Ledger_All, $single2_row);
	}
		//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		// $Sale=$this->Sale->find('all',array(
		// 	'conditions'=>array('Sale.invoice_no IS NOT NULL',
		// 		'Sale.status=2',
		// 		'Sale.flag=1',
		// 		'Sale.date_of_delivered between ? and ?' => array($from_date,$to_date),
		// 	),
		// 	'fields'=>array(
		// 		'Sale.id',
		// 		'Sale.invoice_no',
		// 		'Sale.date_of_delivered',
		// 		'Sale.grand_total',
		// 		'Sale.account_head_id',
		// 		'Sale.discount_amount',
		// 	),
		// 	'order'=>array('Sale.id DESC'),
		// ));
		
		$return=array();
		$return['row']='';
		$credit=0;
		$debit=0;
		$balance=0;
		$opening_balance=0;
		$total_net_transaction=0;
		//$grand_total=0; $amount=0;$net_amount=0;$roundoff=0;$discount=0;$tax=0;
		asort($Ledger_All);
	foreach ($Ledger_All as $key => $value) {
			// 		$account_heads=$this->AccountHead->findByName($value['name']);
			// 		if($account_heads){
		//            $account_head_id=$account_heads['AccountHead']['id'];
		//            $opening_balance=$account_heads['AccountHead']['opening_balance'];
		//            $total=$opening_balance;
		//             $recieved=0;
		// //$Data[$key]['AccountHead']['recieved']=0;
		// $Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
		// $total+=$Debit_N_Credit_function['debit'];
		// $recieved+=$Debit_N_Credit_function['credit'];
		// $balance=$total-$recieved;
		//            }
			
		//pr($opening_balance);
		//exit;
			$return['row'].='<tr class="blue-pddng">';
			$return['row'].='<td class="view_transaction"><span style="display:none" class="AccountHead_id">'.$value['id'].'</span><span class="name">'.ucwords(strtolower($value['name'])).'</span></td>';
			// $return['row'].='<td class="view_transaction">'.$value['code'].'</td>';
			// $return['row'].='<td>'.ucwords(strtolower($value['main_group'])).'</td>';
			// $return['row'].='<td><span style="display:none" class="SubGroup_id">'.$value['sub_group_id'].'</span><span>'.ucwords(strtolower($value['sub_group'])).'</span></td>';
			// $return['row'].='<td>'.$value['address'].'</td>';
		 //    $return['row'].='<td>'.$value['district'].'</td>';
			// $return['row'].='<td>'.$value['pin_code'].'</td>';
			// $return['row'].='<td>'.$value['state'].'</td>';
			// $return['row'].='<td>'.$value['country'].'</td>';
			// $return['row'].='<td>'.$value['telephone'].'</td>';
			// $return['row'].='<td>'.$value['email'].'</td>';
			// $return['row'].='<td>'.$value['website'].'</td>';
			// $return['row'].='<td>'.$value['contact_person'].'</td>';
			// $return['row'].='<td>'.$value['contact_no'].'</td>';
			// $return['row'].='<td>'.$value['gstin'].'</td>';
			// $return['row'].='<td>'.$value['credit_limit'].'</td>';
			// $return['row'].='<td>'.$value['credit_period'].'</td>';
			$return['row'].='<td class="text-right">'.number_format($value['opening_balance'],2,'.','').'</td>';
			// $return['row'].='<td class="text-right">'.$value['debit'].'</td>';
			$net_transaction=$value['debit']-$value['credit'];
			if($net_transaction>0) $net_transaction=number_format($net_transaction,2).' Dr';
			elseif($net_transaction<0) {
				$net=abs($net_transaction);
				$net_transaction=number_format($net,2).' Cr';
			}
			else $net_transaction=number_format($net_transaction,2);
			$return['row'].='<td class="text-right">'.$net_transaction.'</td>';
			$return['row'].='<td class="text-right">'.number_format($value['balance'],2,'.','').'</td>';
			//$return['row'].='<td>'.$balance.'</td>';
			//$return['row'].='<td><span><i table_id="'.$value['id'].'" class="fa fa-1.5x fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#customer_modal_edit"></i></span></td>';
			$debit+=$value['debit'];
			$credit+=$value['credit'];
			$balance+=$value['balance'];
			$total_net_transaction+=$net_transaction;
			$opening_balance+=$value['opening_balance'];
			// $tax+=$value['tax'];
			// $grand_total+=$value['total'];
			$return['row'].='</tr>';
	}
		//exit;
		// $return['row'].='<tr class="blue-pddng toggle_class">';
		// $return['row'].='<td class="total_amount"><b>Total</b></td>';
		// $return['row'].='<td class="total_amount text-right"><b>'.number_format($opening_balance,2).'</b></td>';
		// $return['row'].='<td class="total_amount text-right"><b>'.number_format($total_net_transaction,2,'.','').'</b></td>';
		// $return['row'].='<td class="total_amount text-right"><b>'.number_format($balance,2,'.','').'</b></td>';
		// //$return['row'].='<td class="total_amount"><b>'.round($balance,3).'</b></td>';
		// $return['row'].='</tr>';
		echo json_encode($return);
		exit;
}
public function general_journal_transaction_trial_ajax()
{
	try {
		$user_branch_id=$this->Session->read('User.branch_id');
		$branch_id="";
		if($user_branch_id)
		{
			$branch_id=$user_branch_id;
		}
		// $data['name']='SUJITH_IMPREST. PREPAID';
		// $data['name']='RACY SANITARYWARES';
		// $data['from_date']='01-05-2010';
		// $data['to_date']='15-05-2019';
		$data=$this->request->data;
		//pr($data);
		//exit;
		$name=trim($data['name']);
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$AccountHead=$this->AccountHead->findByName($name);
		//pr($AccountHead);
		//exit;

		if(empty($AccountHead))
			throw new Exception("Empty AccountHead", 1);
		$this->Journal->virtualFields = array(
			'total_amount' => "SUM(Journal.amount)",
			);

		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
					//'Journal.branch_id'=>$branch_id,
						),
					// 'AND' => array(
					// 	//'Journal.flag=1',
					// 	'Journal.date between ? and ?'=>array($from_date,$to_date),
					// 	//'Journal.branch_id'=>$branch_id,
					// 	)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
			//pr($Journal);
			//exit;
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$Journal_all=[];
		foreach ($Journal as $key => $value) 
		{
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['amount']=$value['Journal']['amount'];
			$Journal_single['work_flow']=$value['Journal']['work_flow'];
			if(!empty($value['Executive']['name'])){
				$Journal_single['executive']=$value['Executive']['name'];
			}
			else{
				$Journal_single['executive']="No Executive";
			}
			$Journal_single['external_voucher']=$value['Journal']['external_voucher'];
			if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
			{
				$Journal_single['mode']=$value['AccountHeadCredit']['name'];
				$Journal_single['debit']=$value['Journal']['amount'];
				$Journal_single['credit']=0.00;
			}
			else
			{
				$Journal_single['mode']=$value['AccountHeadDebit']['name'];
				$Journal_single['credit']=$value['Journal']['amount'];
				$Journal_single['debit']=0.00;
			}
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		$return['row']['tbody']='';
		$return['row']['tfoot']='';
		$Balance_Total=0;
		$credit_Total=0;
		$debit_Total=0;
		$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
		$Type_name=$type['AccMainGroup']['name'];
		$category_name=$category[$type['AccMainGroup']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
		}
		$vendor_id=$this->SystemParameter->field('value',array('id'=>12));
		$sub_group=$this->AccountHead->findById($AccountHead['AccountHead']['id']);
		$sub_group_id=$sub_group['AccountHead']['acc_sub_group_id'];
		//if($sub_group==$vendor_id)
			if($category_name=='credit_account')
		{
			$credit=$AccountHead['AccountHead']['opening_balance'];
			$debit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$credit=0;
				$debit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$AccountHead['AccountHead']['opening_balance'];
			$credit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		if(!in_array($Type_name, ['Expense','Income']))
		{
			$JournalCLosginDebit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array( 'total_amount'),
				));
			$JournalCLosginCredit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array('total_amount'),
				));
			$debit+=$JournalCLosginDebit['Journal']['total_amount'];
			$credit+=$JournalCLosginCredit['Journal']['total_amount'];
			$Journal_all[0]=array(
				'id'=>0,
				'date'=>date('d-m-Y',strtotime($from_date)),
				'Journal.branch_id'=>$branch_id,
				'strtotime'=>0,
				'mode'=>'Opening Balance',
				'executive'=>'',
				'voucher_no'=>'',
				'external_voucher'=>'',
				'remarks'=>'',
				'credit'=>floatval($credit),
				'debit'=>floatval($debit),
				);
		}
		else
		{
			if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
			{

			}
			else
			{
				$debit=0;
				$credit=0;
			}
		}
		if(!in_array($Type_name, ['Expense','Income']))
		{
			$Journal_voucher_no=$this->Journal->find('list',array(
				'conditions'=>array(
					'AND' => array(
						'OR' => array(
							'Journal.debit'=>$AccountHead['AccountHead']['id'],
							'Journal.credit'=>$AccountHead['AccountHead']['id'],
							),
						'AND' => array(
							'Journal.flag=1',
							'Journal.date between ? and ?'=>array($from_date,$to_date),
							'Journal.branch_id'=>$branch_id,
							)
						)
					),
				'fields'=>array(
					'Journal.id',
					'Journal.date',
					'Journal.voucher_no',
					),
				));
			foreach ($Journal_voucher_no as $voucher_no => $lists) {
				if(count($lists)>1)
				{
					$j=0;
					foreach ($lists as $journal_id => $date) {
						if($j)
						{
							if($date==$Journal_all[key($lists)]['date'] && $voucher_no==$Journal_all[key($lists)]['voucher_no'])
							{
								$Journal_all[key($lists)]['credit']+=$Journal_all[$journal_id]['credit'];
								$Journal_all[key($lists)]['debit']+=$Journal_all[$journal_id]['debit'];
								unset($Journal_all[$journal_id]);
								$GetJournal=$this->Journal->findById($Journal_all[key($lists)]['id'],['credit']);
								if($AccountHead['AccountHead']['id']==$GetJournal['Journal']['credit'])
								{
									$Journal_all[key($lists)]['credit']-=$Journal_all[key($lists)]['debit'];
									$Journal_all[key($lists)]['debit']=0;
									$Journal_all[key($lists)]['credit']=($Journal_all[key($lists)]['credit']);
								}
								else
								{
									$Journal_all[key($lists)]['debit']-=$Journal_all[key($lists)]['credit'];
									$Journal_all[key($lists)]['credit']=0;
									$Journal_all[key($lists)]['debit']=($Journal_all[key($lists)]['debit']);
								}
							}								
						}
						$j++;
					}
				}
			}
		}
		// pr($Journal_all);
		// exit;
		$date=[];
		foreach ($Journal_all as $i => $row) {
			$date[$i]  = $row['strtotime'];
		}
		array_multisort($date, SORT_DESC, $Journal_all);
		foreach ($Journal_all as $key => $value) 
		{

		//pr($Journal_all);
		//exit;
			$return['row']['tbody'].='<tr class="blue-pddng">';
			$return['row']['tbody'].='<td class="color_label">'.date('d-m-Y',strtotime($value['date'])).'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['executive'].'</td>';
			if($value['voucher_no'])
						{
							$vendor_id=$this->SystemParameter->field('value',array('id'=>12));
							$sub_group=$this->AccountHead->findById($AccountHead['AccountHead']['id']);
							$sub_group_id=$sub_group['AccountHead']['acc_sub_group_id'];
								//pr($sub_group_id);
							// if(($value['work_flow']=='Account Recievable') && ($sub_group_id>2)){

							// 	$return['row']['tbody'].='<td><a class="fa fa-1x fa-print" target="_blank" href="'.$this->webroot.'Print/voucher_print/'.$value['id'].'"></a>&nbsp;&nbsp&nbsp;&nbsp<span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span>&nbsp;&nbsp&nbsp;&nbsp<a class="fa fa-1x fa-print" style="color:green;" target="_blank" href="'.$this->webroot.'Print/account_receivable_fpdf/'.$AccountHead['AccountHead']['id'].'/'.$value['amount'].'"></a></td>';

							// }
							// else{
								$return['row']['tbody'].='<td><span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span></td>';
							//}
						//$return['row']['tbody'].='<td><a class="fa fa-1x fa-print" target="_blank" href="'.$this->webroot.'Print/voucher_print/'.$value['id'].'"></a>&nbsp;&nbsp&nbsp;&nbsp<span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span></td>';
						
					   }
					   else
					   {
					   	$return['row']['tbody'].='<td style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no text-right">'.$value['voucher_no'].'</td>';
					   }
			$return['row']['tbody'].='<td class="color_label text-right">'.$value['external_voucher'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
			$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['debit'],2,'.','').'</td>';
			$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['credit'],2,'.','').'</td>';
			if($category_name=='credit_account') {
			 $balance=$value['credit']-$value['debit']; }
			else { $balance=$value['debit']-$value['credit']; }
			$return['row']['tbody'].='<td class="color_label"></td>';
			$credit_Total+=$value['credit'];
			$debit_Total+=$value['debit'];
			$return['row']['tbody'].='<td><span class="journal_id" hidden>'.$value['id'].'</span>';
			$return['row']['tbody'].='<span><i class="fa fa-trash blue-col delete"></i></span>';
			$return['row']['tbody'].='</td>';
			$return['row']['tbody'].='</tr>';
		}
		//exit;
		$return['row']['tfoot']='';
		$return['row']['tfoot'].='<tr class="blue-pddng">';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td class="total_amount">Total</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.$debit_Total.'</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.$credit_Total.'</td>';
		$Type_name=$type['AccMainGroup']['name'];
		$category_name=$category[$type['AccMainGroup']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
		}
		if($category_name=='credit_account')
		{

			$Balance_Total=$credit_Total-$debit_Total;
		}
		else
		{
			$Balance_Total=$debit_Total-$credit_Total;	
		}
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,2,'.','').'</td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='</tr>';

		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}

public function get_type_by_account_dead($account_head_id)
{
	$AccountHead=$this->AccountHead->find('first',[
		'joins'=>array(
			array(
				'table'=>'acc_sub_groups',
				'alias'=>'AccSubGroup',
				'type'=>'INNER',
				'conditions'=>array('AccSubGroup.id=AccountHead.acc_sub_group_id')
				),
			array(
				'table'=>'acc_main_groups',
				'alias'=>'AccMainGroup',
				'type'=>'INNER',
				'conditions'=>array('AccMainGroup.id=AccSubGroup.main_group_id')
				),
			// array(
			// 	'table'=>'types',
			// 	'alias'=>'Type',
			// 	'type'=>'INNER',
			// 	'conditions'=>array('MasterGroup.type_id=Type.id')
			// 	),
			),
		'conditions'=>array('AccountHead.id'=>$account_head_id),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			//'SubGroup.id',
			'AccSubGroup.*',
			// 'Group.id',
			// 'Group.name',
			//'AccMainGroup.id',
			'AccMainGroup.*',
			// 'Type.id',
			// 'Type.name',
			),
		]);
	return $AccountHead;
}
public function ProfitLossReport()
{
	$user_branch_id=$this->Session->read('User.branch_id');
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/AssetCurrentAccountRecievable';
// 		$menu_id=$this->Menu->field('Menu.id');
// 	$menu_id = $this->Menu->field(
// 		'Menu.id',
// 		array('action ' => 'Accountings/AssetCurrentAccountRecievable'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	//$sub_group_id=3;
	//$cost_center_id=$this->SystemParameter->field('value',array('id'=>11));

	//pr($user_cost);
	//exit;
	$user_id=1;
	if(!$this->request->data)
	{

		$from_date=date('d-m-Y',strtotime('-1 month'));
			$to_date=date('d-m-Y');
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			//$this->request->data=$data;

		$CustomerType_list=$this->CustomerType->find('list',array('fields'=>array('id','name')));
 		$Ledger_list=$this->AccLedger->find('list',array('fields'=>array('id','name')));
 		$this->set('Ledger_list',$Ledger_list);
 		$SubGroup_list=$this->AccSubGroup->find('list',array('fields'=>array('id','name')));
 		$this->set('SubGroup_list',$SubGroup_list);
 		$MainGroup_list=$this->AccMainGroup->find('list',array('fields'=>array('id','name')));
 		$this->set('MainGroup_list',$MainGroup_list);
 		 $State_list=$this->State->find('list',array('fields'=>array('id','name')));
		 $this->set('State_list',$State_list);
		  $District_list=$this->District->find('list',array('fields'=>array('id','name')));
		 $this->set('District_list',$District_list);
		  $Country_list=$this->Country->find('list',array('fields'=>array('id','name')));
		 $this->set('Country_list',$Country_list);
		 $Status=[
		'1'=>'Active',
		'0'=>'Inactive',
		
		// '3'=>'Expense',
		// '4'=>'Income',
		// '5'=>'Liabilities',
		];
		$this->set('Status',$Status);
		 $cost_center=[
		'1'=>'Yes',
		'0'=>'No',
		
		// '3'=>'Expense',
		// '4'=>'Income',
		// '5'=>'Liabilities',
		];
		$this->set('cost_center',$cost_center);
		// $AccountHead_list=$this->AccountHead->find('all',array('joins' => [
		// 	[
		// 	"table" => "customers",
		// 	"alias" => "Customer",
		// 	"type" => "INNER",
		// 	"conditions" => ['AccountHead.id=Customer.account_head_id'],
		// 	],
		// 	],'fields'=>array('AccountHead.id','AccountHead.name','Customer.code'),'conditions'=>array('sub_group_id'=>3)));
		// $Customer_list=[];
		// foreach ($AccountHead_list as $key => $value) {
		// 	$Customer_list[$value['AccountHead']['id']] = $value['AccountHead']['name'].' '.$value['Customer']['code'];
		// }
		// $Executive_list=$this->Executive->find('list',array('fields'=>array('id','name')));
		// $this->set('Executive_list',$Executive_list);
		$this->Route->virtualFields = array(
			'route_name' => "CONCAT(Route.name, ' ', Route.code)"
			);
		$conditions_branch_route=[];

		if($user_branch_id)
		{
			$route_id=$this->BranchRouteMapping->find('list',array('conditions'=>array('BranchRouteMapping.branch_id'=>$user_branch_id),'fields'=>['BranchRouteMapping.id','BranchRouteMapping.route_id']));
			$conditions_branch_route['Route.id']=$route_id;
		}
		// $this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
		// $Product_list=$this->Product->find('list',array('fields'=>['id','product_name']));
		// $this->set(compact('Product_list'));
		 $Route_list=$this->Route->find('list',array('conditions'=>$conditions_branch_route,'fields'=>array('id','route_name')));
		 $this->set(compact('Route_list'));
		 $CustomerGroup_list=$this->CustomerGroup->find('list',array('fields'=>array('id','name')));
		 $this->set(compact('CustomerGroup_list'));
		// $Division_list=$this->Division->find('list');
		// $this->set(compact('Division_list'));
// $State_list=$this->State->find('list',array('fields'=>array('id','name')));
// $this->set(compact('State_list'));
// $Day_list=array(
//      'Monday'=>'Monday',
//      'Tuesday'=>'Tuesday',
//      'Wednesday'=>'Wednesday',
//      'Thursday'=>'Thursday',
//      'Friday'=>'Friday',
//      'Saturday'=>'Saturday',
//      'Sunday'=>'Sunday',
//      );
//    $this->set(compact('Day_list'));
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
	//	$data['Ledger']['opening_balance']='0';
		//$conditions_branch=[];

		if($user_branch_id)
		{
			$route_id=$this->BranchRouteMapping->find('list',array('conditions'=>array('BranchRouteMapping.branch_id'=>$user_branch_id),'fields'=>['BranchRouteMapping.id','BranchRouteMapping.route_id']));
			$conditions_branch['Customer.route_id']=$route_id;
		}
		$user_cost=$this->Session->read('SystemParameter.cost_center_id');
		$this->set('user_cost',$user_cost);
	    //$data['Ledger']['c_center']=$user_cost;
		$this->request->data=$data;

		$modalCustomerType_list = $CustomerType_list;
// 		unset($modalCustomerType_list[1]);
 		$this->set('CustomerType_list',$CustomerType_list);
 		$this->set('modalCustomerType_list',$modalCustomerType_list);
// 		$this->set('Customer_list',$Customer_list);
// // $this->set('Customer',$Customer);
 		//$this->set('Ledger',$Ledger_All);
	}

}

public function ProfitLoss_Table_ajax()
	{
		$data=$this->request->data;
		//$from_date=$data['from_date'];
		//$to_date=$data['to_date'];
		//$from_date=date('Y-m-d',strtotime(($from_date)));
		//$to_date=date('Y-m-d',strtotime(($to_date)));
		$conditions=[];
		if(!empty($data)){
			if(!empty($data['main_group_ids'])){
		$conditions['AccMainGroup.id']=$data['main_group_ids'];
	      }
	      if(!empty($data['sub_group_ids'])){
		$conditions['AccSubGroup.id']=$data['sub_group_ids'];
	}
	    }
	    $conditions['AccMainGroup.id !=']=array(1,2,4);
	    // else{
	    // 	$conditions='';
	    // }
		//$list_array=array();
		//$Ledger_All=[];
		$AccLedger=$this->AccLedger->find('all',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.main_group_id=AccMainGroup.id'),
                ),
                array(
                    "table"=>'states',
                    "alias"=>'State',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.state=State.id'),
                ),
                array(
                    "table"=>'districts',
                    "alias"=>'District',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.district=District.id'),
                ),
                array(
                    "table"=>'countries',
                    "alias"=>'Country',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.country=Country.id'),
                ),
            ),
            'conditions'=>array($conditions),
			'fields'=>array('AccLedger.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
			
			//'order'=>array('AccountHead.name ASC'),
// 'limit'=>110,
			));
		// pr($AccLedger);
		// exit;
	 $Ledger_All=[];
		foreach ($AccLedger as $key => $value) {
			$account_heads=$this->AccountHead->findByName($value['AccLedger']['name']);
			if($account_heads){
            $account_head_id=$account_heads['AccountHead']['id'];
            $opening_balance=$account_heads['AccountHead']['opening_balance'];
            $total=$opening_balance;
             $paid=0;
	//$Data[$key]['AccountHead']['recieved']=0;
	$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
	$total+=$Debit_N_Credit_function['debit'];
	$paid+=$Debit_N_Credit_function['credit'];
	$balance=$total-$paid;
            }
			$single_row['sub_group']= $value['AccSubGroup']['name'];
			$single_row['sub_group_id']= $value['AccLedger']['sub_group_id'];
			$single_row['main_group']= $value['AccMainGroup']['name'];
			$single_row['main_group_id']= $value['AccLedger']['main_group_id'];
			$single_row['address']= $value['AccLedger']['address'];
			$single_row['district']= $value['District']['name'];
			$single_row['pin_code']= $value['AccLedger']['pin_code'];
			$single_row['state']= $value['State']['name'];
			$single_row['name']= $value['AccLedger']['name'];
			if($value['AccLedger']['status']==1)
			{
			$single_row['status']='Active';
			}
			elseif($value['AccLedger']['status']==0)
			{
             $single_row['status']="Inactive";

			}
			$single_row['code']= $value['AccLedger']['code'];
			$single_row['country']= $value['Country']['name'];
			$single_row['telephone']= $value['AccLedger']['telephone'];
			$single_row['email']= $value['AccLedger']['email'];
			$single_row['website']= $value['AccLedger']['website'];
			$single_row['contact_person']= $value['AccLedger']['contact_person'];
			$single_row['contact_no']= $value['AccLedger']['contact_no'];
			$single_row['gstin']= $value['AccLedger']['gstin'];
			$single_row['credit_limit']= $value['AccLedger']['credit_limit'];
			$single_row['credit_period']= $value['AccLedger']['credit_period'];
			$single_row['opening_balance']= $value['AccLedger']['opening_balance'];
			$single_row['other_notes']= $value['AccLedger']['other_notes'];
			$single_row['id']= $value['AccLedger']['id'];
			$single_row['debit']= $total;
			$single_row['credit']= $paid;
            $single_row['balance']= $balance;
			array_push($Ledger_All, $single_row);
		}
		$CUstomer=$this->Customer->find('all',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'left',
                    "conditions"=>array('Customer.sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'left',
                    "conditions"=>array('Customer.main_group_id=AccMainGroup.id'),
                ),
                array(
                    "table"=>'states',
                    "alias"=>'State',
                    "type"=>'left',
                    "conditions"=>array('Customer.state_id=State.id'),
                ),
                array(
                    "table"=>'districts',
                    "alias"=>'District',
                    "type"=>'left',
                    "conditions"=>array('Customer.district=District.id'),
                ),
                array(
                    "table"=>'countries',
                    "alias"=>'Country',
                    "type"=>'left',
                    "conditions"=>array('Customer.country=Country.id'),
                ),
            ),
            'conditions'=>array($conditions),
			'fields'=>array('Customer.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
			
			//'order'=>array('AccountHead.name ASC'),
// 'limit'=>110,
			));
		// pr($CUstomer);
		// exit;
		//$Ledger_All=[];
		foreach ($CUstomer as $key1 => $value1) {
			$account_heads=$this->AccountHead->findByName($value1['Customer']['name']);
			if($account_heads){
            $account_head_id=$account_heads['AccountHead']['id'];
            $opening_balance=$account_heads['AccountHead']['opening_balance'];
            $total=$opening_balance;
             $recieved=0;
	//$Data[$key]['AccountHead']['recieved']=0;
	$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
	$total+=$Debit_N_Credit_function['debit'];
	$recieved+=$Debit_N_Credit_function['credit'];
	$balance=$total-$recieved;
            }
			$single1_row['sub_group']= $value1['AccSubGroup']['name'];
			$single1_row['sub_group_id']= $value1['Customer']['sub_group_id'];
			$single1_row['main_group']= $value1['AccMainGroup']['name'];
			$single1_row['main_group_id']= $value1['Customer']['main_group_id'];
			$single1_row['address']= $value1['Customer']['place'];
			$single1_row['district']= $value1['District']['name'];
			$single1_row['pin_code']= $value1['Customer']['pin_code'];
			$single1_row['state']= $value1['State']['name'];
			$single1_row['name']= $value1['Customer']['name'];
			if($value1['Customer']['status']==1)
			{
			$single1_row['status']='Active';
			}
			elseif($value1['Customer']['status']==0)
			{
             $single1_row['status']="Inactive";

			}
			$single1_row['code']= $value1['Customer']['code'];
			$single1_row['country']= $value1['Country']['name'];
			$single1_row['telephone']= $value1['Customer']['telephone'];
			$single1_row['email']= $value1['Customer']['email'];
			$single1_row['website']= $value1['Customer']['website'];
			$single1_row['contact_person']= $value1['Customer']['contact_person'];
			$single1_row['contact_no']= $value1['Customer']['contact_no'];
			$single1_row['gstin']= $value1['Customer']['gstin'];
			$single1_row['credit_limit']= $value1['Customer']['credit_limit'];
			$single1_row['credit_period']= $value1['Customer']['credit_period'];
			$single1_row['opening_balance']= $value1['Customer']['opening_balance'];
			$single1_row['other_notes']= $value1['Customer']['other_notes'];
			$single1_row['id']= $value1['Customer']['id'];
			$single1_row['debit']= $total;
			$single1_row['credit']= $recieved;
			$single1_row['balance']= $balance;
			array_push($Ledger_All, $single1_row);
		}
$this->Party->unbindModel(array('belongsTo'=>array('State')));
				$PArty=$this->Party->find('all',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'left',
                    "conditions"=>array('Party.sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'left',
                    "conditions"=>array('Party.main_group_id=AccMainGroup.id'),
                ),
                array(
                    "table"=>'states',
                    "alias"=>'State',
                    "type"=>'left',
                    "conditions"=>array('Party.state_id=State.id'),
                ),
                array(
                    "table"=>'districts',
                    "alias"=>'District',
                    "type"=>'left',
                    "conditions"=>array('Party.district_id=District.id'),
                ),
                array(
                    "table"=>'countries',
                    "alias"=>'Country',
                    "type"=>'left',
                    "conditions"=>array('Party.country_id=Country.id'),
                ),
            ),
            'conditions'=>array($conditions),
			'fields'=>array('Party.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
			
			//'order'=>array('AccountHead.name ASC'),
// 'limit'=>110,
			));
		// pr($CUstomer);
		// exit;
		//$Ledger_All=[];
		foreach ($PArty as $key2 => $value2) {

			$account_heads=$this->AccountHead->findByName($value2['Party']['name']);
			if($account_heads){
            $account_head_id=$account_heads['AccountHead']['id'];
            $opening_balance=$account_heads['AccountHead']['opening_balance'];
            $total=$opening_balance;
             $paid=0;
	//$Data[$key]['AccountHead']['recieved']=0;
	$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
	$total+=$Debit_N_Credit_function['credit'];
	$paid+=$Debit_N_Credit_function['debit'];
	$balance=$total-$paid;
            }
			$single2_row['sub_group']= $value2['AccSubGroup']['name'];
			$single2_row['sub_group_id']= $value2['Party']['sub_group_id'];
			$single2_row['main_group']= $value2['AccMainGroup']['name'];
			$single2_row['main_group_id']= $value2['Party']['main_group_id'];
			$single2_row['address']= $value2['Party']['address'];
			$single2_row['district']= $value2['District']['name'];
			$single2_row['pin_code']= $value2['Party']['pin_code'];
			$single2_row['state']= $value2['State']['name'];
			$single2_row['name']= $value2['Party']['name'];
			if($value2['Party']['status']==1)
			{
			$single2_row['status']='Active';
			}
			elseif($value2['Party']['status']==0)
			{
             $single2_row['status']="Inactive";

			}
			$single2_row['code']= $value2['Party']['code'];
			$single2_row['country']= $value2['Party']['name'];
			$single2_row['telephone']= $value2['Party']['land_line'];
			$single2_row['email']= $value2['Party']['email'];
			$single2_row['website']= $value2['Party']['website'];
			$single2_row['contact_person']= $value2['Party']['contact_person'];
			$single2_row['contact_no']= $value2['Party']['contact_no'];
			$single2_row['gstin']= $value2['Party']['gstin'];
			$single2_row['credit_limit']= $value2['Party']['credit_limit'];
			$single2_row['credit_period']= $value2['Party']['credit_period'];
			$single2_row['opening_balance']= $value2['Party']['opening_balance'];
			$single2_row['other_notes']= $value2['Party']['other_notes'];
			$single2_row['id']= $value2['Party']['id'];
			$single2_row['debit']= $paid;
			$single2_row['credit']= $total;
			$single2_row['balance']= $balance;
			array_push($Ledger_All, $single2_row);
		}
		//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		// $Sale=$this->Sale->find('all',array(
		// 	'conditions'=>array('Sale.invoice_no IS NOT NULL',
		// 		'Sale.status=2',
		// 		'Sale.flag=1',
		// 		'Sale.date_of_delivered between ? and ?' => array($from_date,$to_date),
		// 	),
		// 	'fields'=>array(
		// 		'Sale.id',
		// 		'Sale.invoice_no',
		// 		'Sale.date_of_delivered',
		// 		'Sale.grand_total',
		// 		'Sale.account_head_id',
		// 		'Sale.discount_amount',
		// 	),
		// 	'order'=>array('Sale.id DESC'),
		// ));
		
		$return=array();
		$return['row']='';
		$credit=0;
		$debit=0;
		$balance=0;
		$total_balance=0;
		$total_debit=0;
		$total_credit=0;
		//$grand_total=0; $amount=0;$net_amount=0;$roundoff=0;$discount=0;$tax=0;
		foreach ($Ledger_All as $key => $value) {
	// 		$account_heads=$this->AccountHead->findByName($value['name']);
	// 		if($account_heads){
 //            $account_head_id=$account_heads['AccountHead']['id'];
 //            $opening_balance=$account_heads['AccountHead']['opening_balance'];
 //            $total=$opening_balance;
 //             $recieved=0;
	// //$Data[$key]['AccountHead']['recieved']=0;
	// $Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
	// $total+=$Debit_N_Credit_function['debit'];
	// $recieved+=$Debit_N_Credit_function['credit'];
	// $balance=$total-$recieved;
 //            }
			
		//pr($opening_balance);
		//exit;
			$return['row'].='<tr class="blue-pddng">';
			$return['row'].='<td class="view_transaction">'.$value['code'].'</td>';
			$return['row'].='<td class="view_transaction"><span style="display:none" class="AccountHead_id">'.$value['id'].'</span><span class="name">'.$value['name'].'</span></td>';
			
			$return['row'].='<td>'.$value['main_group'].'</td>';
			$return['row'].='<td><span style="display:none" class="SubGroup_id">'.$value['sub_group_id'].'</span><span>'.$value['sub_group'].'</span></td>';
			// $return['row'].='<td>'.$value['address'].'</td>';
		 //    $return['row'].='<td>'.$value['district'].'</td>';
			// $return['row'].='<td>'.$value['pin_code'].'</td>';
			// $return['row'].='<td>'.$value['state'].'</td>';
			// $return['row'].='<td>'.$value['country'].'</td>';
			// $return['row'].='<td>'.$value['telephone'].'</td>';
			// $return['row'].='<td>'.$value['email'].'</td>';
			// $return['row'].='<td>'.$value['website'].'</td>';
			// $return['row'].='<td>'.$value['contact_person'].'</td>';
			// $return['row'].='<td>'.$value['contact_no'].'</td>';
			// $return['row'].='<td>'.$value['gstin'].'</td>';
			// $return['row'].='<td>'.$value['credit_limit'].'</td>';
			// $return['row'].='<td>'.$value['credit_period'].'</td>';
			// $return['row'].='<td>'.$value['opening_balance'].'</td>';
			$return['row'].='<td>'.$value['debit'].'</td>';
			$return['row'].='<td>'.$value['credit'].'</td>';
			$return['row'].='<td>'.$value['balance'].'</td>';
			//$return['row'].='<td>'.$balance.'</td>';
			//$return['row'].='<td><span><i table_id="'.$value['id'].'" class="fa fa-1.5x fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#customer_modal_edit"></i></span></td>';
			// $net_amount+=$value['net_amount'];
			// $amount+=$value['taxable_amount'];
			 $total_debit+=$value['debit'];
			 $total_credit+=$value['credit'];
			 $total_balance+=$value['balance'];
			$return['row'].='</tr>';
		}
		//exit;
		$return['row'].='<tr class="blue-pddng toggle_class">';
		$return['row'].='<td></td>';
		$return['row'].='<td></td>';
		$return['row'].='<td class="total_amount"><b>Total</b></td>';
		$return['row'].='<td></td>';
		$return['row'].='<td class="total_amount"><b>'.number_format($total_debit,2,'.','').'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.number_format($total_credit,2,'.','').'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.number_format($total_balance,2,'.','').'</b></td>';
		$return['row'].='</tr>';
		echo json_encode($return);
		exit;
	}
	public function BalanceSheetReport()
{
	$user_branch_id=$this->Session->read('User.branch_id');
	$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Accountings/AssetCurrentAccountRecievable';
// 		$menu_id=$this->Menu->field('Menu.id');
// 	$menu_id = $this->Menu->field(
// 		'Menu.id',
// 		array('action ' => 'Accountings/AssetCurrentAccountRecievable'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
	//$sub_group_id=3;
	//$cost_center_id=$this->SystemParameter->field('value',array('id'=>11));

	//pr($user_cost);
	//exit;
	$user_id=1;
	if(!$this->request->data)
	{

		$from_date=date('d-m-Y',strtotime('-1 month'));
			$to_date=date('d-m-Y');
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			//$this->request->data=$data;

		$CustomerType_list=$this->CustomerType->find('list',array('fields'=>array('id','name')));
 		$Ledger_list=$this->AccLedger->find('list',array('fields'=>array('id','name')));
 		$this->set('Ledger_list',$Ledger_list);
 		$SubGroup_list=$this->AccSubGroup->find('list',array('fields'=>array('id','name')));
 		$this->set('SubGroup_list',$SubGroup_list);
 		$MainGroup_list=$this->AccMainGroup->find('list',array('fields'=>array('id','name')));
 		$this->set('MainGroup_list',$MainGroup_list);
 		 $State_list=$this->State->find('list',array('fields'=>array('id','name')));
		 $this->set('State_list',$State_list);
		  $District_list=$this->District->find('list',array('fields'=>array('id','name')));
		 $this->set('District_list',$District_list);
		  $Country_list=$this->Country->find('list',array('fields'=>array('id','name')));
		 $this->set('Country_list',$Country_list);
		 $Status=[
		'1'=>'Active',
		'0'=>'Inactive',
		
		// '3'=>'Expense',
		// '4'=>'Income',
		// '5'=>'Liabilities',
		];
		$this->set('Status',$Status);
		 $cost_center=[
		'1'=>'Yes',
		'0'=>'No',
		
		// '3'=>'Expense',
		// '4'=>'Income',
		// '5'=>'Liabilities',
		];
		$this->set('cost_center',$cost_center);
		// $AccountHead_list=$this->AccountHead->find('all',array('joins' => [
		// 	[
		// 	"table" => "customers",
		// 	"alias" => "Customer",
		// 	"type" => "INNER",
		// 	"conditions" => ['AccountHead.id=Customer.account_head_id'],
		// 	],
		// 	],'fields'=>array('AccountHead.id','AccountHead.name','Customer.code'),'conditions'=>array('sub_group_id'=>3)));
		// $Customer_list=[];
		// foreach ($AccountHead_list as $key => $value) {
		// 	$Customer_list[$value['AccountHead']['id']] = $value['AccountHead']['name'].' '.$value['Customer']['code'];
		// }
		// $Executive_list=$this->Executive->find('list',array('fields'=>array('id','name')));
		// $this->set('Executive_list',$Executive_list);
		$this->Route->virtualFields = array(
			'route_name' => "CONCAT(Route.name, ' ', Route.code)"
			);
		$conditions_branch_route=[];

		if($user_branch_id)
		{
			$route_id=$this->BranchRouteMapping->find('list',array('conditions'=>array('BranchRouteMapping.branch_id'=>$user_branch_id),'fields'=>['BranchRouteMapping.id','BranchRouteMapping.route_id']));
			$conditions_branch_route['Route.id']=$route_id;
		}
		// $this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
		// $Product_list=$this->Product->find('list',array('fields'=>['id','product_name']));
		// $this->set(compact('Product_list'));
		 $Route_list=$this->Route->find('list',array('conditions'=>$conditions_branch_route,'fields'=>array('id','route_name')));
		 $this->set(compact('Route_list'));
		 $CustomerGroup_list=$this->CustomerGroup->find('list',array('fields'=>array('id','name')));
		 $this->set(compact('CustomerGroup_list'));
		// $Division_list=$this->Division->find('list');
		// $this->set(compact('Division_list'));
// $State_list=$this->State->find('list',array('fields'=>array('id','name')));
// $this->set(compact('State_list'));
// $Day_list=array(
//      'Monday'=>'Monday',
//      'Tuesday'=>'Tuesday',
//      'Wednesday'=>'Wednesday',
//      'Thursday'=>'Thursday',
//      'Friday'=>'Friday',
//      'Saturday'=>'Saturday',
//      'Sunday'=>'Sunday',
//      );
//    $this->set(compact('Day_list'));
		$data['AccountHead']['date']=date('d-m-Y');
		$data['AccountHead']['opening_balance']='0';
	//	$data['Ledger']['opening_balance']='0';
		//$conditions_branch=[];

		if($user_branch_id)
		{
			$route_id=$this->BranchRouteMapping->find('list',array('conditions'=>array('BranchRouteMapping.branch_id'=>$user_branch_id),'fields'=>['BranchRouteMapping.id','BranchRouteMapping.route_id']));
			$conditions_branch['Customer.route_id']=$route_id;
		}
		$user_cost=$this->Session->read('SystemParameter.cost_center_id');
		$this->set('user_cost',$user_cost);
	    //$data['Ledger']['c_center']=$user_cost;
		$this->request->data=$data;

		$modalCustomerType_list = $CustomerType_list;
// 		unset($modalCustomerType_list[1]);
 		$this->set('CustomerType_list',$CustomerType_list);
 		$this->set('modalCustomerType_list',$modalCustomerType_list);
// 		$this->set('Customer_list',$Customer_list);
// // $this->set('Customer',$Customer);
 		//$this->set('Ledger',$Ledger_All);
	}

}

public function BalanceSheet_Table_ajax()
	{
		$data=$this->request->data;
		//$from_date=$data['from_date'];
		//$to_date=$data['to_date'];
		//$from_date=date('Y-m-d',strtotime(($from_date)));
		//$to_date=date('Y-m-d',strtotime(($to_date)));
		$conditions=[];
		if(!empty($data)){
			if(!empty($data['main_group_ids'])){
		$conditions['AccMainGroup.id']=$data['main_group_ids'];
	      }
	      if(!empty($data['sub_group_ids'])){
		$conditions['AccSubGroup.id']=$data['sub_group_ids'];
	}
	    }
	    $conditions['AccMainGroup.id !=']=array(3,5);
	    // else{
	    // 	$conditions='';
	    // }
		//$list_array=array();
		//$Ledger_All=[];
		$AccLedger=$this->AccLedger->find('all',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.main_group_id=AccMainGroup.id'),
                ),
                array(
                    "table"=>'states',
                    "alias"=>'State',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.state=State.id'),
                ),
                array(
                    "table"=>'districts',
                    "alias"=>'District',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.district=District.id'),
                ),
                array(
                    "table"=>'countries',
                    "alias"=>'Country',
                    "type"=>'left',
                    "conditions"=>array('AccLedger.country=Country.id'),
                ),
            ),
            'conditions'=>array($conditions),
			'fields'=>array('AccLedger.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
			
			//'order'=>array('AccountHead.name ASC'),
// 'limit'=>110,
			));
		// pr($AccLedger);
		// exit;
	 $Ledger_All=[];
		foreach ($AccLedger as $key => $value) {
			$account_heads=$this->AccountHead->findByName($value['AccLedger']['name']);
			if($account_heads){
            $account_head_id=$account_heads['AccountHead']['id'];
            $opening_balance=$account_heads['AccountHead']['opening_balance'];
            $total=$opening_balance;
             $paid=0;
	//$Data[$key]['AccountHead']['recieved']=0;
	$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
	$total+=$Debit_N_Credit_function['debit'];
	$paid+=$Debit_N_Credit_function['credit'];
	$balance=$total-$paid;
            }
			$single_row['sub_group']= $value['AccSubGroup']['name'];
			$single_row['sub_group_id']= $value['AccLedger']['sub_group_id'];
			$single_row['main_group']= $value['AccMainGroup']['name'];
			$single_row['main_group_id']= $value['AccLedger']['main_group_id'];
			$single_row['address']= $value['AccLedger']['address'];
			$single_row['district']= $value['District']['name'];
			$single_row['pin_code']= $value['AccLedger']['pin_code'];
			$single_row['state']= $value['State']['name'];
			$single_row['name']= $value['AccLedger']['name'];
			if($value['AccLedger']['status']==1)
			{
			$single_row['status']='Active';
			}
			elseif($value['AccLedger']['status']==0)
			{
             $single_row['status']="Inactive";

			}
			$single_row['code']= $value['AccLedger']['code'];
			$single_row['country']= $value['Country']['name'];
			$single_row['telephone']= $value['AccLedger']['telephone'];
			$single_row['email']= $value['AccLedger']['email'];
			$single_row['website']= $value['AccLedger']['website'];
			$single_row['contact_person']= $value['AccLedger']['contact_person'];
			$single_row['contact_no']= $value['AccLedger']['contact_no'];
			$single_row['gstin']= $value['AccLedger']['gstin'];
			$single_row['credit_limit']= $value['AccLedger']['credit_limit'];
			$single_row['credit_period']= $value['AccLedger']['credit_period'];
			$single_row['opening_balance']= $value['AccLedger']['opening_balance'];
			$single_row['other_notes']= $value['AccLedger']['other_notes'];
			$single_row['id']= $value['AccLedger']['id'];
			$single_row['debit']= $total;
			$single_row['credit']= $paid;
            $single_row['balance']= $balance;
			array_push($Ledger_All, $single_row);
		}
		$CUstomer=$this->Customer->find('all',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'left',
                    "conditions"=>array('Customer.sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'left',
                    "conditions"=>array('Customer.main_group_id=AccMainGroup.id'),
                ),
                array(
                    "table"=>'states',
                    "alias"=>'State',
                    "type"=>'left',
                    "conditions"=>array('Customer.state_id=State.id'),
                ),
                array(
                    "table"=>'districts',
                    "alias"=>'District',
                    "type"=>'left',
                    "conditions"=>array('Customer.district=District.id'),
                ),
                array(
                    "table"=>'countries',
                    "alias"=>'Country',
                    "type"=>'left',
                    "conditions"=>array('Customer.country=Country.id'),
                ),
            ),
            'conditions'=>array($conditions),
			'fields'=>array('Customer.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
			
			//'order'=>array('AccountHead.name ASC'),
// 'limit'=>110,
			));
		// pr($CUstomer);
		// exit;
		//$Ledger_All=[];
		foreach ($CUstomer as $key1 => $value1) {
			$account_heads=$this->AccountHead->findByName($value1['Customer']['name']);
			if($account_heads){
            $account_head_id=$account_heads['AccountHead']['id'];
            $opening_balance=$account_heads['AccountHead']['opening_balance'];
            $total=$opening_balance;
             $recieved=0;
	//$Data[$key]['AccountHead']['recieved']=0;
	$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
	$total+=$Debit_N_Credit_function['debit'];
	$recieved+=$Debit_N_Credit_function['credit'];
	$balance=$total-$recieved;
            }
			$single1_row['sub_group']= $value1['AccSubGroup']['name'];
			$single1_row['sub_group_id']= $value1['Customer']['sub_group_id'];
			$single1_row['main_group']= $value1['AccMainGroup']['name'];
			$single1_row['main_group_id']= $value1['Customer']['main_group_id'];
			$single1_row['address']= $value1['Customer']['place'];
			$single1_row['district']= $value1['District']['name'];
			$single1_row['pin_code']= $value1['Customer']['pin_code'];
			$single1_row['state']= $value1['State']['name'];
			$single1_row['name']= $value1['Customer']['name'];
			if($value1['Customer']['status']==1)
			{
			$single1_row['status']='Active';
			}
			elseif($value1['Customer']['status']==0)
			{
             $single1_row['status']="Inactive";

			}
			$single1_row['code']= $value1['Customer']['code'];
			$single1_row['country']= $value1['Country']['name'];
			$single1_row['telephone']= $value1['Customer']['telephone'];
			$single1_row['email']= $value1['Customer']['email'];
			$single1_row['website']= $value1['Customer']['website'];
			$single1_row['contact_person']= $value1['Customer']['contact_person'];
			$single1_row['contact_no']= $value1['Customer']['contact_no'];
			$single1_row['gstin']= $value1['Customer']['gstin'];
			$single1_row['credit_limit']= $value1['Customer']['credit_limit'];
			$single1_row['credit_period']= $value1['Customer']['credit_period'];
			$single1_row['opening_balance']= $value1['Customer']['opening_balance'];
			$single1_row['other_notes']= $value1['Customer']['other_notes'];
			$single1_row['id']= $value1['Customer']['id'];
			$single1_row['debit']= $total;
			$single1_row['credit']= $recieved;
			$single1_row['balance']= $balance;
			array_push($Ledger_All, $single1_row);
		}
$this->Party->unbindModel(array('belongsTo'=>array('State')));
				$PArty=$this->Party->find('all',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'left',
                    "conditions"=>array('Party.sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'left',
                    "conditions"=>array('Party.main_group_id=AccMainGroup.id'),
                ),
                array(
                    "table"=>'states',
                    "alias"=>'State',
                    "type"=>'left',
                    "conditions"=>array('Party.state_id=State.id'),
                ),
                array(
                    "table"=>'districts',
                    "alias"=>'District',
                    "type"=>'left',
                    "conditions"=>array('Party.district_id=District.id'),
                ),
                array(
                    "table"=>'countries',
                    "alias"=>'Country',
                    "type"=>'left',
                    "conditions"=>array('Party.country_id=Country.id'),
                ),
            ),
            'conditions'=>array($conditions),
			'fields'=>array('Party.*','AccSubGroup.name','AccMainGroup.name','District.name','State.name','Country.name'),
			
			//'order'=>array('AccountHead.name ASC'),
// 'limit'=>110,
			));
		// pr($CUstomer);
		// exit;
		//$Ledger_All=[];
		foreach ($PArty as $key2 => $value2) {

			$account_heads=$this->AccountHead->findByName($value2['Party']['name']);
			if($account_heads){
            $account_head_id=$account_heads['AccountHead']['id'];
            $opening_balance=$account_heads['AccountHead']['opening_balance'];
            $total=$opening_balance;
             $paid=0;
	//$Data[$key]['AccountHead']['recieved']=0;
	$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
	$total+=$Debit_N_Credit_function['credit'];
	$paid+=$Debit_N_Credit_function['debit'];
	$balance=$total-$paid;
            }
			$single2_row['sub_group']= $value2['AccSubGroup']['name'];
			$single2_row['sub_group_id']= $value2['Party']['sub_group_id'];
			$single2_row['main_group']= $value2['AccMainGroup']['name'];
			$single2_row['main_group_id']= $value2['Party']['main_group_id'];
			$single2_row['address']= $value2['Party']['address'];
			$single2_row['district']= $value2['District']['name'];
			$single2_row['pin_code']= $value2['Party']['pin_code'];
			$single2_row['state']= $value2['State']['name'];
			$single2_row['name']= $value2['Party']['name'];
			if($value2['Party']['status']==1)
			{
			$single2_row['status']='Active';
			}
			elseif($value2['Party']['status']==0)
			{
             $single2_row['status']="Inactive";

			}
			$single2_row['code']= $value2['Party']['code'];
			$single2_row['country']= $value2['Party']['name'];
			$single2_row['telephone']= $value2['Party']['land_line'];
			$single2_row['email']= $value2['Party']['email'];
			$single2_row['website']= $value2['Party']['website'];
			$single2_row['contact_person']= $value2['Party']['contact_person'];
			$single2_row['contact_no']= $value2['Party']['contact_no'];
			$single2_row['gstin']= $value2['Party']['gstin'];
			$single2_row['credit_limit']= $value2['Party']['credit_limit'];
			$single2_row['credit_period']= $value2['Party']['credit_period'];
			$single2_row['opening_balance']= $value2['Party']['opening_balance'];
			$single2_row['other_notes']= $value2['Party']['other_notes'];
			$single2_row['id']= $value2['Party']['id'];
			$single2_row['debit']= $paid;
			$single2_row['credit']= $total;
			$single2_row['balance']= $balance;
			array_push($Ledger_All, $single2_row);
		}
		//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		// $Sale=$this->Sale->find('all',array(
		// 	'conditions'=>array('Sale.invoice_no IS NOT NULL',
		// 		'Sale.status=2',
		// 		'Sale.flag=1',
		// 		'Sale.date_of_delivered between ? and ?' => array($from_date,$to_date),
		// 	),
		// 	'fields'=>array(
		// 		'Sale.id',
		// 		'Sale.invoice_no',
		// 		'Sale.date_of_delivered',
		// 		'Sale.grand_total',
		// 		'Sale.account_head_id',
		// 		'Sale.discount_amount',
		// 	),
		// 	'order'=>array('Sale.id DESC'),
		// ));
		
		$return=array();
		$return['row']='';
		$credit=0;
		$debit=0;
		$balance=0;
		$total_balance=0;
		$total_debit=0;
		$total_credit=0;
		//$grand_total=0; $amount=0;$net_amount=0;$roundoff=0;$discount=0;$tax=0;
		foreach ($Ledger_All as $key => $value) {
	// 		$account_heads=$this->AccountHead->findByName($value['name']);
	// 		if($account_heads){
 //            $account_head_id=$account_heads['AccountHead']['id'];
 //            $opening_balance=$account_heads['AccountHead']['opening_balance'];
 //            $total=$opening_balance;
 //             $recieved=0;
	// //$Data[$key]['AccountHead']['recieved']=0;
	// $Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
	// $total+=$Debit_N_Credit_function['debit'];
	// $recieved+=$Debit_N_Credit_function['credit'];
	// $balance=$total-$recieved;
 //            }
			
		//pr($opening_balance);
		//exit;
			$return['row'].='<tr class="blue-pddng">';
			$return['row'].='<td class="view_transaction">'.$value['code'].'</td>';
			$return['row'].='<td class="view_transaction"><span style="display:none" class="AccountHead_id">'.$value['id'].'</span><span class="name">'.$value['name'].'</span></td>';
			
			$return['row'].='<td>'.$value['main_group'].'</td>';
			$return['row'].='<td><span style="display:none" class="SubGroup_id">'.$value['sub_group_id'].'</span><span>'.$value['sub_group'].'</span></td>';
			// $return['row'].='<td>'.$value['address'].'</td>';
		 //    $return['row'].='<td>'.$value['district'].'</td>';
			// $return['row'].='<td>'.$value['pin_code'].'</td>';
			// $return['row'].='<td>'.$value['state'].'</td>';
			// $return['row'].='<td>'.$value['country'].'</td>';
			// $return['row'].='<td>'.$value['telephone'].'</td>';
			// $return['row'].='<td>'.$value['email'].'</td>';
			// $return['row'].='<td>'.$value['website'].'</td>';
			// $return['row'].='<td>'.$value['contact_person'].'</td>';
			// $return['row'].='<td>'.$value['contact_no'].'</td>';
			// $return['row'].='<td>'.$value['gstin'].'</td>';
			// $return['row'].='<td>'.$value['credit_limit'].'</td>';
			// $return['row'].='<td>'.$value['credit_period'].'</td>';
			// $return['row'].='<td>'.$value['opening_balance'].'</td>';
			$return['row'].='<td>'.$value['debit'].'</td>';
			$return['row'].='<td>'.$value['credit'].'</td>';
			$return['row'].='<td>'.$value['balance'].'</td>';
			//$return['row'].='<td>'.$balance.'</td>';
			//$return['row'].='<td><span><i table_id="'.$value['id'].'" class="fa fa-1.5x fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#customer_modal_edit"></i></span></td>';
			// $net_amount+=$value['net_amount'];
			// $amount+=$value['taxable_amount'];
			 $total_debit+=$value['debit'];
			 $total_credit+=$value['credit'];
			 $total_balance+=$value['balance'];
			$return['row'].='</tr>';
		}
		//exit;
		$return['row'].='<tr class="blue-pddng toggle_class">';
		$return['row'].='<td></td>';
		$return['row'].='<td></td>';
		$return['row'].='<td class="total_amount"><b>Total</b></td>';
		$return['row'].='<td></td>';
		$return['row'].='<td class="total_amount"><b>'.number_format($total_debit,2,'.','').'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.number_format($total_credit,2,'.','').'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.number_format($total_balance,2,'.','').'</b></td>';
		$return['row'].='</tr>';
		echo json_encode($return);
		exit;
	}

    public function LedgerReport()
   {

		$from_date=date('d-m-Y',strtotime('-1 month'));
			$to_date=date('d-m-Y');
			$this->set('from_date',$from_date);
			$this->set('to_date',$to_date);
 		$SubGroup_list=$this->AccSubGroup->find('list',array('fields'=>array('id','name'),'order' => array('name' => 'ASC')));
 		$this->set('SubGroup_list',$SubGroup_list);
 		$MainGroup_list=$this->AccMainGroup->find('list',array('fields'=>array('id','name')));
 		$this->set('MainGroup_list',$MainGroup_list);
 		 $State_list=$this->State->find('list',array('fields'=>array('id','name')));
		 $this->set('State_list',$State_list);
		  $District_list=$this->District->find('list',array('fields'=>array('id','name')));
		 $this->set('District_list',$District_list);

}
 public function CustomerLedgerReport()
   {

		$from_date=date('d-m-Y',strtotime('-1 month'));
			$to_date=date('d-m-Y');
			$this->set('from_date',$from_date);
			$this->set('to_date',$to_date);
 		$Executive_list=$this->Executive->find('list',array('fields'=>array('id','name'),'order' => array('name' => 'ASC')));
 		$this->set('Executive_list',$Executive_list);
 		$Route_list=$this->Route->find('list',array('fields'=>array('id','name')));
 		$this->set('Route_list',$Route_list);
}
public function general_journal_transaction_ajax_ledger()
{
	try {
		$user_branch_id=$this->Session->read('User.branch_id');
		$branch_id="";
		if($user_branch_id)
		{
			$branch_id=$user_branch_id;
		}
		$data=$this->request->data;
//pr($data);
//exit;
		$customer_id=$this->SystemParameter->field('value',array('id'=>11));
		$name=trim($data['name']);
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$AccountHead_id=$data['AccountHead_id'];
		$AccountHead=$this->AccountHead->findById($AccountHead_id);
		//exit;

		if(empty($AccountHead))
			throw new Exception("Empty AccountHead", 1);
		$this->Journal->virtualFields = array(
			'total_amount' => "SUM(Journal.amount)",
			);
if($AccountHead['AccountHead']['sub_group_id']==$customer_id){
		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						'Journal.debit!=19',
						'Journal.credit!=19',
					   	'Journal.debit!=13',
					    'Journal.credit!=13',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						//'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.narration',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
}else if($AccountHead['AccountHead']['id']=='19'){
		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						//'Journal.debit!=23',
					   // 'Journal.credit!=23',
						//'Journal.debit!=19',
						//'Journal.credit!=19',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						//'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.narration',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
	}else{
		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						//'Journal.debit!=23',
					   // 'Journal.credit!=23',
						'Journal.debit!=19',
						'Journal.credit!=19',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						//'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.narration',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
	}
//pr($Journal);
//exit;
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$Journal_all=[];
		foreach ($Journal as $key => $value) 
		{
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['narration']=$value['Journal']['narration'];
			$Journal_single['amount']=$value['Journal']['amount'];
			$Journal_single['work_flow']=$value['Journal']['work_flow'];
			if(!empty($value['Executive']['name'])){
				$Journal_single['executive']=$value['Executive']['name'];
			}
			else{
				$Journal_single['executive']="No Executive";
			}
			$Journal_single['external_voucher']=$value['Journal']['external_voucher'];
			if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
			{
				$Journal_single['mode']=$value['AccountHeadCredit']['name'];
				$Journal_single['debit']=$value['Journal']['amount'];
				$Journal_single['credit']=0.00;
			}
			else
			{
				$Journal_single['mode']=$value['AccountHeadDebit']['name'];
				$Journal_single['credit']=$value['Journal']['amount'];
				$Journal_single['debit']=0.00;
			}
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		$return['row']['tbody']='';
		$return['row']['tfoot']='';
		$Balance_Total=0;
		$credit_Total=0;
		$debit_Total=0;
		$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
		$Type_name=$type['AccMainGroup']['name'];
		$category_name=$category[$type['AccMainGroup']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
		}
			if($category_name=='credit_account')
		{
			$credit=$AccountHead['AccountHead']['opening_balance'];
			$debit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$credit=0;
				$debit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$AccountHead['AccountHead']['opening_balance'];
			$credit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		if(!in_array($Type_name, ['Income']))
		{
			if($AccountHead['AccountHead']['sub_group_id']==$customer_id){
			$JournalCLosginDebit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit!=13',
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					//'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array( 'total_amount'),
				));
			$JournalCLosginCredit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					'Journal.debit!=13',
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					//'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array('total_amount'),
				));
		}else if($AccountHead['AccountHead']['id']=='19'){
			$JournalCLosginDebit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					//'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array( 'total_amount'),
				));
			$JournalCLosginCredit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					//'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array('total_amount'),
				));
		}
		else{
			$JournalCLosginDebit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit !='=>19,
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array( 'total_amount'),
				));
			$JournalCLosginCredit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					'Journal.debit !='=>19,
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array('total_amount'),
				));
		}
			$debit+=$JournalCLosginDebit['Journal']['total_amount'];
			$credit+=$JournalCLosginCredit['Journal']['total_amount'];
			$Journal_all[0]=array(
				'id'=>0,
				'date'=>date('d-m-Y',strtotime($from_date)),
				'Journal.branch_id'=>$branch_id,
				'strtotime'=>0,
				'mode'=>'Opening Balance',
				'executive'=>'',
				'voucher_no'=>'',
				'external_voucher'=>'',
				'narration'=>'',
				'remarks'=>'',
				'credit'=>floatval($credit),
				'debit'=>floatval($debit),
				);
		}
		else
		{
			if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
			{

			}
			else
			{
				$debit=0;
				$credit=0;
			}
		}
		if(!in_array($Type_name, ['Income']))
		{
			$Journal_voucher_no=$this->Journal->find('list',array(
				'conditions'=>array(
					'AND' => array(
						'OR' => array(
							'Journal.debit'=>$AccountHead['AccountHead']['id'],
							'Journal.credit'=>$AccountHead['AccountHead']['id'],
							),
						'AND' => array(
							'Journal.flag=1',
							'Journal.date between ? and ?'=>array($from_date,$to_date),
							//'Journal.branch_id'=>$branch_id,
							)
						)
					),
				'fields'=>array(
					'Journal.id',
					'Journal.date',
					'Journal.voucher_no',
					),
				));
			foreach ($Journal_voucher_no as $voucher_no => $lists) {
				if(count($lists)>1)
				{
					$j=0;
					foreach ($lists as $journal_id => $date) {
						if($j)
						{
							if($date==$Journal_all[key($lists)]['date'] && $voucher_no==$Journal_all[key($lists)]['voucher_no'])
							{
								$Journal_all[key($lists)]['credit']+=$Journal_all[$journal_id]['credit'];
								$Journal_all[key($lists)]['debit']+=$Journal_all[$journal_id]['debit'];
								unset($Journal_all[$journal_id]);
								$GetJournal=$this->Journal->findById($Journal_all[key($lists)]['id'],['credit']);
								if($AccountHead['AccountHead']['id']==$GetJournal['Journal']['credit'])
								{
									$Journal_all[key($lists)]['credit']-=$Journal_all[key($lists)]['debit'];
									$Journal_all[key($lists)]['debit']=0;
									$Journal_all[key($lists)]['credit']=($Journal_all[key($lists)]['credit']);
								}
								else
								{
									$Journal_all[key($lists)]['debit']-=$Journal_all[key($lists)]['credit'];
									$Journal_all[key($lists)]['credit']=0;
									$Journal_all[key($lists)]['debit']=($Journal_all[key($lists)]['debit']);
								}
							}								
						}
						$j++;
					}
				}
			}
		}
		// pr($Journal_all);
		// exit;
		$date=[];
		foreach ($Journal_all as $i => $row) {
			$date[$i]  = $row['strtotime'];
		}
		array_multisort($date, SORT_ASC, $Journal_all);
		foreach ($Journal_all as $key => $value) 
		{

//pr($Journal_all);
//exit;
			$return['row']['tbody'].='<tr class="blue-pddng">';
			$return['row']['tbody'].='<td class="color_label">'.date('d-m-Y',strtotime($value['date'])).'</td>';
			if($value['voucher_no'])
						{
							$vendor_id=$this->SystemParameter->field('value',array('id'=>10));
							$sub_group=$this->AccountHead->findById($AccountHead['AccountHead']['id']);
							$sub_group_id=$sub_group['AccountHead']['acc_sub_group_id'];
								//pr($sub_group_id);
							// if(($value['work_flow']=='Account Recievable') && ($sub_group_id>2)){

							// 	$return['row']['tbody'].='<td><a class="fa fa-1x fa-print" target="_blank" href="'.$this->webroot.'Print/voucher_print/'.$value['id'].'"></a>&nbsp;&nbsp&nbsp;&nbsp<span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span>&nbsp;&nbsp&nbsp;&nbsp<a class="fa fa-1x fa-print" style="color:green;" target="_blank" href="'.$this->webroot.'Print/account_receivable_fpdf/'.$AccountHead['AccountHead']['id'].'/'.$value['amount'].'"></a></td>';

							// }
							// else{
								// $return['row']['tbody'].='<td><span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span></td>';
							$return['row']['tbody'].='<td style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no">'.$value['voucher_no'].'</td>';
							//}
						//$return['row']['tbody'].='<td><a class="fa fa-1x fa-print" target="_blank" href="'.$this->webroot.'Print/voucher_print/'.$value['id'].'"></a>&nbsp;&nbsp&nbsp;&nbsp<span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span></td>';
						
					   }
					   else
					   {
					   	$return['row']['tbody'].='<td style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no text-right">'.$value['voucher_no'].'</td>';
					   }
			$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
			$return['row']['tbody'].='<td style="display:none" class="color_label">'.$value['executive'].'</td>';
			
			//$return['row']['tbody'].='<td class="color_label text-right">'.$value['external_voucher'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
			$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['debit'],2,'.','').'</td>';
			$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['credit'],2,'.','').'</td>';
			if($category_name=='credit_account') {
			 $balance=$value['credit']-$value['debit']; }
			else { $balance=$value['debit']-$value['credit']; }
			$return['row']['tbody'].='<td class="color_label"></td>';
			$credit_Total+=$value['credit'];
			$debit_Total+=$value['debit'];
			$return['row']['tbody'].='<td class="color_label text-right">'.$value['narration'].'</td>';
			$return['row']['tbody'].='<td style="display:none"><span class="journal_id" style="display:none">'.$value['id'].'</span>';
			$return['row']['tbody'].='<span style="display:none"><i class="fa fa-trash blue-col delete"></i></span>';
			
			$return['row']['tbody'].='</td>';
			$return['row']['tbody'].='</tr>';
		}
		//exit;
		$return['row']['tfoot']='';
		$return['row']['tfoot'].='<tr class="blue-pddng">';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		// $return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td style="display:none"></td>';
		$return['row']['tfoot'].='<td class="total_amount">Total</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($debit_Total,2,'.','').'</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($credit_Total,2,'.','').'</td>';
		$Type_name=$type['AccMainGroup']['name'];
		$category_name=$category[$type['AccMainGroup']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
		}
		if($category_name=='credit_account')
		{

			//$Balance_Total=$credit_Total-$debit_Total;
			$Balance_Total=$debit_Total-$credit_Total;
		}
		else
		{
			$Balance_Total=$debit_Total-$credit_Total;	
		}
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,2,'.','').'</td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='</tr>';

		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function general_journal_transaction_ajax_cash()
{

	try {
		$user_branch_id=$this->Session->read('User.branch_id');
		$branch_id="";
		if($user_branch_id)
		{
			$branch_id=$user_branch_id;
		}
		$data=$this->request->data;
//pr($data);
//exit;
		$customer_id=$this->SystemParameter->field('value',array('id'=>11));
		$name=trim($data['name']);
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$AccountHead_id=$data['AccountHead_id'];
		$AccountHead=$this->AccountHead->findById($AccountHead_id);

		if(empty($AccountHead))
			throw new Exception("Empty AccountHead", 1);
		$this->Journal->virtualFields = array(
			'total_amount' => "SUM(Journal.amount)",
			);
		if($AccountHead['AccountHead']['sub_group_id']==$customer_id){
		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						'Journal.debit!=19',
						'Journal.credit!=19',
					   	'Journal.debit!=13',
					    'Journal.credit!=13',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						//'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.narration',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
}else if($AccountHead['AccountHead']['id']=='19'){
		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						// 'Journal.debit!=19',
						// 'Journal.credit!=19',
					   	// 'Journal.debit!=23',
					    // 'Journal.credit!=23',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						//'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.narration',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
	}else{
		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						'Journal.debit!=19',
						'Journal.credit!=19',
					   	// 'Journal.debit!=23',
					    // 'Journal.credit!=23',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						//'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.narration',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
	}
//pr($Journal);
//exit;
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$Journal_all=[];
		foreach ($Journal as $key => $value) 
		{
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['narration']=$value['Journal']['narration'];
			$Journal_single['amount']=$value['Journal']['amount'];
			$Journal_single['work_flow']=$value['Journal']['work_flow'];
			if(!empty($value['Executive']['name'])){
				$Journal_single['executive']=$value['Executive']['name'];
			}
			else{
				$Journal_single['executive']="No Executive";
			}
			$Journal_single['external_voucher']=$value['Journal']['external_voucher'];
			if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
			{
				$Journal_single['mode']=$value['AccountHeadCredit']['name'];
				$Journal_single['debit']=$value['Journal']['amount'];
				$Journal_single['credit']=0.00;
			}
			else
			{
				$Journal_single['mode']=$value['AccountHeadDebit']['name'];
				$Journal_single['credit']=$value['Journal']['amount'];
				$Journal_single['debit']=0.00;
			}
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		$return['row']['tbody']='';
		$return['row']['tfoot']='';
		$Balance_Total=0;
		$credit_Total=0;
		$debit_Total=0;
		$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
		$Type_name=$type['AccMainGroup']['name'];
		$category_name=$category[$type['AccMainGroup']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
		}
		$vendor_id=$this->SystemParameter->field('value',array('id'=>10));
		$sub_group=$this->AccountHead->findById($AccountHead['AccountHead']['id']);
		$sub_group_id=$sub_group['AccountHead']['acc_sub_group_id'];
		//if($sub_group==$vendor_id)
			if($category_name=='credit_account')
		{
			$credit=$AccountHead['AccountHead']['opening_balance'];
			$debit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$credit=0;
				$debit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$AccountHead['AccountHead']['opening_balance'];
			$credit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		if(!in_array($Type_name, ['Income']))
		{
			$JournalCLosginDebit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit !='=>19,
					'Journal.credit !='=>23,
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array( 'total_amount'),
				));
			$JournalCLosginCredit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					'Journal.debit !='=>19,
					'Journal.debit !='=>23,
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array('total_amount'),
				));
			$debit+=$JournalCLosginDebit['Journal']['total_amount'];
			$credit+=$JournalCLosginCredit['Journal']['total_amount'];
			$Journal_all[0]=array(
				'id'=>0,
				'date'=>date('d-m-Y',strtotime($from_date)),
				'Journal.branch_id'=>$branch_id,
				'strtotime'=>0,
				'mode'=>'Opening Balance',
				'executive'=>'',
				'voucher_no'=>'',
				'external_voucher'=>'',
				'remarks'=>'',
				'credit'=>floatval($credit),
				'debit'=>floatval($debit),
				);
		}
		else
		{
			if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
			{

			}
			else
			{
				$debit=0;
				$credit=0;
			}
		}
		if(!in_array($Type_name, ['Income']))
		{
			$Journal_voucher_no=$this->Journal->find('list',array(
				'conditions'=>array(
					'AND' => array(
						'OR' => array(
							'Journal.debit'=>$AccountHead['AccountHead']['id'],
							'Journal.credit'=>$AccountHead['AccountHead']['id'],
							),
						'AND' => array(
							'Journal.credit !='=>19,
							'Journal.debit !='=>19,
					        'Journal.credit !='=>23,
					        'Journal.debit !='=>19,
							'Journal.flag=1',
							'Journal.date between ? and ?'=>array($from_date,$to_date),
							'Journal.branch_id'=>$branch_id,
							)
						)
					),
				'fields'=>array(
					'Journal.id',
					'Journal.date',
					'Journal.voucher_no',
					),
				));
			foreach ($Journal_voucher_no as $voucher_no => $lists) {
				if(count($lists)>1)
				{
					$j=0;
					foreach ($lists as $journal_id => $date) {
						if($j)
						{
							if($date==$Journal_all[key($lists)]['date'] && $voucher_no==$Journal_all[key($lists)]['voucher_no'])
							{
								$Journal_all[key($lists)]['credit']+=$Journal_all[$journal_id]['credit'];
								$Journal_all[key($lists)]['debit']+=$Journal_all[$journal_id]['debit'];
								unset($Journal_all[$journal_id]);
								$GetJournal=$this->Journal->findById($Journal_all[key($lists)]['id'],['credit']);
								if($AccountHead['AccountHead']['id']==$GetJournal['Journal']['credit'])
								{
									$Journal_all[key($lists)]['credit']-=$Journal_all[key($lists)]['debit'];
									$Journal_all[key($lists)]['debit']=0;
									$Journal_all[key($lists)]['credit']=($Journal_all[key($lists)]['credit']);
								}
								else
								{
									$Journal_all[key($lists)]['debit']-=$Journal_all[key($lists)]['credit'];
									$Journal_all[key($lists)]['credit']=0;
									$Journal_all[key($lists)]['debit']=($Journal_all[key($lists)]['debit']);
								}
							}								
						}
						$j++;
					}
				}
			}
		}
		// pr($Journal_all);
		// exit;
		$date=[];
		foreach ($Journal_all as $i => $row) {
			$date[$i]  = $row['strtotime'];
		}
		array_multisort($date, SORT_ASC, $Journal_all);
		foreach ($Journal_all as $key => $value) 
		{

//pr($Journal_all);
//exit;
			$return['row']['tbody'].='<tr class="blue-pddng">';
			$return['row']['tbody'].='<td class="color_label">'.date('d-m-Y',strtotime($value['date'])).'</td>';
			if($value['voucher_no'])
						{
							$vendor_id=$this->SystemParameter->field('value',array('id'=>10));
							$sub_group=$this->AccountHead->findById($AccountHead['AccountHead']['id']);
							$sub_group_id=$sub_group['AccountHead']['acc_sub_group_id'];
								//pr($sub_group_id);
							// if(($value['work_flow']=='Account Recievable') && ($sub_group_id>2)){

							// 	$return['row']['tbody'].='<td><a class="fa fa-1x fa-print" target="_blank" href="'.$this->webroot.'Print/voucher_print/'.$value['id'].'"></a>&nbsp;&nbsp&nbsp;&nbsp<span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span>&nbsp;&nbsp&nbsp;&nbsp<a class="fa fa-1x fa-print" style="color:green;" target="_blank" href="'.$this->webroot.'Print/account_receivable_fpdf/'.$AccountHead['AccountHead']['id'].'/'.$value['amount'].'"></a></td>';

							// }
							// else{
								// $return['row']['tbody'].='<td><span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span></td>';
							$return['row']['tbody'].='<td style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no">'.$value['voucher_no'].'</td>';
							//}
						//$return['row']['tbody'].='<td><a class="fa fa-1x fa-print" target="_blank" href="'.$this->webroot.'Print/voucher_print/'.$value['id'].'"></a>&nbsp;&nbsp&nbsp;&nbsp<span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span></td>';
						
					   }
					   else
					   {
					   	$return['row']['tbody'].='<td style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no text-right">'.$value['voucher_no'].'</td>';
					   }
			$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
			$return['row']['tbody'].='<td style="display:none" class="color_label">'.$value['executive'].'</td>';
			
			//$return['row']['tbody'].='<td class="color_label text-right">'.$value['external_voucher'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
			$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['debit'],2,'.','').'</td>';
			$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['credit'],2,'.','').'</td>';
			if($category_name=='credit_account') {
			 $balance=$value['credit']-$value['debit']; }
			else { $balance=$value['debit']-$value['credit']; }
			$return['row']['tbody'].='<td class="color_label"></td>';
			$credit_Total+=$value['credit'];
			$debit_Total+=$value['debit'];
			$return['row']['tbody'].='<td class="color_label text-right">'.$value['narration'].'</td>';
			$return['row']['tbody'].='<td style="display:none"><span class="journal_id" style="display:none">'.$value['id'].'</span>';
			$return['row']['tbody'].='<span style="display:none"><i class="fa fa-trash blue-col delete"></i></span>';
			
			$return['row']['tbody'].='</td>';
			$return['row']['tbody'].='</tr>';
		}
		//exit;
		$return['row']['tfoot']='';
		$return['row']['tfoot'].='<tr class="blue-pddng">';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		// $return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td style="display:none"></td>';
		$return['row']['tfoot'].='<td class="total_amount">Total</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($debit_Total,2,'.','').'</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($credit_Total,2,'.','').'</td>';
		$Type_name=$type['AccMainGroup']['name'];
		$category_name=$category[$type['AccMainGroup']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
		}
		if($category_name=='credit_account')
		{

			//$Balance_Total=$credit_Total-$debit_Total;
			$Balance_Total=$debit_Total-$credit_Total;
		}
		else
		{
			$Balance_Total=$debit_Total-$credit_Total;	
		}
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,2,'.','').'</td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='</tr>';

		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	echo json_encode($return);
	exit;

}
public function general_journal_transactions_ajax()
{
	try {
		$user_branch_id=$this->Session->read('User.branch_id');
		$branch_id="";
		if($user_branch_id)
		{
			$branch_id=$user_branch_id;
		}
		// $data['name']='SUJITH_IMPREST. PREPAID';
		// $data['name']='RACY SANITARYWARES';
		// $data['from_date']='01-05-2010';
		// $data['to_date']='15-05-2019';
		$data=$this->request->data;
		//pr($data);
		//exit;
		$name=trim($data['name']);
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$AccountHead=$this->CostCenter->findByName($name);
		// $AccountHead=$this->CostCenter->find('all',
		// 	array(
		// 		'joins'=>array(
		// 		array(
		// 			'table'=>'cost_center_details',
		// 			'alias'=>'CostCenterDetail',
		// 			'type'=>'inner',
		// 			'conditions'=>array('CostCenterDetail.cost_center_id=CostCenter.id')
		// 			),
		// 		),
		// 		'conditions'=>array('CostCenter.name'=>$name),
		// 		'fields'=>array('CostCenter.id','CostCenter.name','CostCenterDetail.*')
		// 	));
		//pr($AccountHead);
		//exit;

		if(empty($AccountHead))
			throw new Exception("Empty CostCenter", 1);
		$this->Journal->virtualFields = array(
			'total_amount' => "SUM(Journal.amount)",
			);

		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'cost_center_details',
					'alias'=>'CostCenterDetail',
					'type'=>'inner',
					'conditions'=>array('Journal.id=CostCenterDetail.payment_id')
					),
				),
			'conditions'=>array(
				//'Journal.id'=>$AccountHead['CostCenterDetail']['payment_id'],
				'CostCenterDetail.cost_center_id'=>$AccountHead['CostCenter']['id'],
				'Journal.date between ? and ?'=>array($from_date,$to_date),
		// 				'AND' => array(
		// 					'OR' => array(
		// 						'Journal.debit'=>$AccountHead['AccountHead']['id'],
		// 						'Journal.credit'=>$AccountHead['AccountHead']['id'],
		// //'Journal.branch_id'=>$branch_id,
		// 						),
		// 					'AND' => array(
		// 						'Journal.flag=1',
		// 						'Journal.date between ? and ?'=>array($from_date,$to_date),
		// 						'Journal.branch_id'=>$branch_id,
		// 						)
		// 					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				//'Executive.id',
				//'Executive.name',
				'CostCenterDetail.*',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
		//pr($Journal);
		//exit;
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$Journal_all=[];
		foreach ($Journal as $key => $value) 
		{
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['amount']=$value['Journal']['amount'];
			$Journal_single['work_flow']=$value['Journal']['work_flow'];
			if(!empty($value['Executive']['name'])){
				$Journal_single['executive']=$value['Executive']['name'];
			}
			else{
				$Journal_single['executive']="No Executive";
			}
			$Journal_single['external_voucher']=$value['Journal']['external_voucher'];
			$Journal_single['amount']=$value['CostCenterDetail']['amount'];
			// if($AccountHead['CostCenter']['name']==$value['AccountHeadDebit']['name'])
			// {
		// 	$Journal_single['mode']=$value['AccountHeadCredit']['name'];
			// 	$Journal_single['debit']=$value['Journal']['amount'];
			// 	$Journal_single['credit']=0.00;
			// }
			// else
			// {
		 	$Journal_single['mode']=$value['AccountHeadDebit']['name'];
			// 	$Journal_single['credit']=$value['Journal']['amount'];
			// 	$Journal_single['debit']=0.00;
			// }
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		$return['row']['tbody']='';
		$return['row']['tfoot']='';
		$Balance_Total=0;
		$credit_Total=0;
		$debit_Total=0;
		
		$date=[];
		foreach ($Journal_all as $i => $row) {
			$date[$i]  = $row['strtotime'];
		}
		array_multisort($date, SORT_DESC, $Journal_all);
		foreach ($Journal_all as $key => $value) 
		{

		//pr($Journal_all);
		//exit;
			$return['row']['tbody'].='<tr class="blue-pddng">';
			$return['row']['tbody'].='<td class="color_label">'.date('d-m-Y',strtotime($value['date'])).'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
			//$return['row']['tbody'].='<td></td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['executive'].'</td>';
			if($value['voucher_no'])
						{
							// $sub_group=$this->AccountHead->findById($AccountHead['AccountHead']['id']);
							// 	$sub_group_id=$sub_group['AccountHead']['sub_group_id'];
								//pr($sub_group_id);
							// if(($value['work_flow']=='Account Recievable') && ($sub_group_id>2)){

							// 	$return['row']['tbody'].='<td><a class="fa fa-1x fa-print" target="_blank" href="'.$this->webroot.'Print/voucher_print/'.$value['id'].'"></a>&nbsp;&nbsp&nbsp;&nbsp<span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span>&nbsp;&nbsp&nbsp;&nbsp<a class="fa fa-1x fa-print" style="color:green;" target="_blank" href="'.$this->webroot.'Print/account_receivable_fpdf/'.$AccountHead['AccountHead']['id'].'/'.$value['amount'].'"></a></td>';

							// }
							// else{
							// 	$return['row']['tbody'].='<td><a class="fa fa-1x fa-print" target="_blank" href="'.$this->webroot.'Print/voucher_print/'.$value['id'].'"></a>&nbsp;&nbsp&nbsp;&nbsp<span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span></td>';
							// }
						//$return['row']['tbody'].='<td><a class="fa fa-1x fa-print" target="_blank" href="'.$this->webroot.'Print/voucher_print/'.$value['id'].'"></a>&nbsp;&nbsp&nbsp;&nbsp<span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span></td>';
						
					   }
					   // else
					   // {
					   //	$return['row']['tbody'].='<td style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no text-right">'.$value['voucher_no'].'</td>';
					  // }
					   	$return['row']['tbody'].='<td style="color:blue;" class="color_label voucher_no text-right">'.$value['voucher_no'].'</td>';
			$return['row']['tbody'].='<td class="color_label text-right">'.$value['external_voucher'].'</td>';
			$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
			$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['amount'],2,'.','').'</td>';
			$debit_Total+=$value['amount'];
			//$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['credit'],3,'.','').'</td>';
			// if($category_name=='credit_account') { $balance=$value['credit']-$value['debit']; }
			// else { $balance=$value['debit']-$value['credit']; }
			// $return['row']['tbody'].='<td class="color_label"></td>';
			// //$credit_Total+=$value['credit'];
			// //$debit_Total+=$value['debit'];
			// $return['row']['tbody'].='<td><span class="journal_id" hidden>'.$value['id'].'</span>';
			// //$return['row']['tbody'].='<span><i class="fa fa-trash blue-col delete"></i></span>';
			// $return['row']['tbody'].='</td>';
			$return['row']['tbody'].='</tr>';
		}
		//exit;
		$return['row']['tfoot']='';
		$return['row']['tfoot'].='<tr class="blue-pddng">';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='<td class="total_amount">Total</td>';
		$return['row']['tfoot'].='<td class="total_amount text-right">'.$debit_Total.'</td>';
		//$return['row']['tfoot'].='<td class="total_amount text-right">'.$credit_Total.'</td>';
		// $Type_name=$type['Type']['name'];
		// $category_name=$category[$type['Type']['name']];
		// if($Type_name=='Capital')
		// {
		// 	$category_name=$category[$type['Type']['name']][$type['Group']['name']];
		// }
		// if($category_name=='credit_account')
		// {
		// 	$Balance_Total=$credit_Total-$debit_Total;
		// }
		// else
		// {
		// 	$Balance_Total=$debit_Total-$credit_Total;	
		// }
		//$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,3,'.','').'</td>';
		//$return['row']['tfoot'].='<td></td>';
		$return['row']['tfoot'].='</tr>';

		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function Searchcustomer() {
  $query=$this->request->query['q'];
  $AccountHead=$this->AccountHead->find('list',array(
		'conditions'=>array(
			'AccountHead.acc_sub_group_id !='=>0,
			'AccountHead.name LIKE' => '%'. $query . '%',
			),
		'fields'=>[
		'AccountHead.id',
		'AccountHead.name',
		]
		)
	);
  $i=0;
  $AccountHeadList=array();
  foreach ($AccountHead as $key => $value) {
    if (false !== stripos($value, $query)) {
     $AccountHeadList[$i]['text'] = $value;
     $AccountHeadList[$i]['id'] = $key;
     $i++;
   }
 }

 $result['items'] = $AccountHeadList;
 echo json_encode($result);
 exit;
}
	public function VoucherReceipt($id=null)
{
	$user_id=1;
	$cash_group_id=$this->SystemParameter->field('value',array('id'=>9));
	$bank_group_id=$this->SystemParameter->field('value',array('id'=>10));
	$crediters_group_id=$this->SystemParameter->field('value',array('id'=>12));
	$debtors_group_id=$this->SystemParameter->field('value',array('id'=>11));
	if($this->Session->read('UserRole.id')==1) $user_branch_id=1;
	else $user_branch_id=$this->Session->read('User.branch_id');
	$mode_category=[
				'1'=>'Cash',
				'2'=>'Cheque',
				'3'=>'Bank Transfer',
			];
	$this->set('mode_category',$mode_category);
	$AccountHead_cash=$this->AccountHead->find('list',array('conditions'=>array('acc_sub_group_id'=>$cash_group_id)));
	$this->set('Mode',$AccountHead_cash);
	$banks=$this->AccountHead->find('list',array(
		'conditions'=>['AccountHead.acc_sub_group_id'=>$bank_group_id],
		'order'=>array('AccountHead.name ASC'),
		'fields'=>[
			'AccountHead.id',
			'AccountHead.name',
		]
	));
	$this->set('banks',$banks);
	//$user_branch_id=$this->Session->read('User.branch_id');
	$CustomerType_list=$this->CustomerType->find('list',array('fields'=>array('id','name')));
	$Ledger_list=$this->AccLedger->find('list',array('fields'=>array('id','name')));
	$this->set('Ledger_list',$Ledger_list);
	$SubGroup_list=$this->AccSubGroup->find('list',array('fields'=>array('id','name'),'order' => array('name' => 'ASC')));
	$this->set('SubGroup_list',$SubGroup_list);
	$MainGroup_list=$this->AccMainGroup->find('list',array('fields'=>array('id','name')));
	$this->set('MainGroup_list',$MainGroup_list);
	$State_list=$this->State->find('list',array('fields'=>array('id','name')));
	$this->set('State_list',$State_list);
	$District_list=$this->District->find('list',array('fields'=>array('id','name')));
	$this->set('District_list',$District_list);
	$Country_list=$this->Country->find('list',array('fields'=>array('id','name')));
	$this->set('Country_list',$Country_list);
	$Status=[
		'1'=>'Active',
		'0'=>'Inactive',
		
	];
	$this->set('Status',$Status);
	$cost_center=[
		'1'=>'Yes',
		'0'=>'No',
	
	];
	$this->set('cost_center',$cost_center);
	
	$this->Route->virtualFields = array(
		'route_name' => "CONCAT(Route.name, ' ', Route.code)"
		);
	$conditions_branch_route=[];
	$Route_list=$this->Route->find('list',array('conditions'=>$conditions_branch_route,'fields'=>array('id','route_name')));
	$this->set(compact('Route_list'));
	$CustomerGroup_list=$this->CustomerGroup->find('list',array('fields'=>array('id','name')));
	$this->set(compact('CustomerGroup_list'));
	$user_cost=$this->Session->read('SystemParameter.cost_center_id');
	$this->set('user_cost',$user_cost);
	$modalCustomerType_list = $CustomerType_list;
	$this->set('CustomerType_list',$CustomerType_list);
	$this->set('modalCustomerType_list',$modalCustomerType_list);

	if(!$this->request->data)
	{
	   if(empty($id)){
	$Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
    if(!empty($Journals))
    {
      $JournalsNo = $Journals['Journal']['voucher_no'];
      $Journals_no=$JournalsNo+1;
    }
    else
    {
      $Journals_no=1;
    }
    $currency=$this->SystemParameter->field('value',array('id'=>6));
				$data['Journal']['currency']=$currency;
			$data['Journal']['date']=date('d-m-Y');
			$data['Journal']['customer_type']=1;
			$data['from_date']=date('d-m-Y',strtotime('-1 month'));
			$data['to_date']=date('d-m-Y');
			$data['Journal']['cheque_date']=date('d-m-Y');
			$data['Journal']['journal_no']=$Journals_no;
			//$data['to_date']=date('d-m-Y',strtotime('+6 month'));
			$this->request->data=$data;
		  }
		  else
		  {
		  	 $currency=$this->SystemParameter->field('value',array('id'=>6));
				$Journal=$this->Journal->findById($id);
				$Customer=$this->Customer->findByAccountHeadId($Journal['Journal']['credit']);
				$this->request->data=$Journal;
				$this->request->data['Journal']['date']=date('d-m-Y',strtotime($Journal['Journal']['date']));
					$this->request->data['Journal']['Journal_id']=$Journal['Journal']['id'];
				$this->request->data['Journal']['journal_no']=$Journal['Journal']['voucher_no'];
				 $this->request->data['Journal']['currency']=$currency;
				  $this->request->data['Journal']['amount']=round($Journal['Journal']['amount'],2);
				    $this->request->data['Journal']['remarks']=$Journal['Journal']['remarks'];
				       $this->request->data['Journal']['external_voucher']=$Journal['Journal']['external_voucher'];
				if($Journal['Journal']['reference_no'])
					$this->request->data['Journal']['mode_category_ids']=3;
				elseif($Journal['Journal']['cheque_no'])
					$this->request->data['Journal']['mode_category_ids']=2;
				else
					$this->request->data['Journal']['mode_category_ids']=1;
				// $this->request->data['Journal']['narration']=$Journal['Journal']['narration'];
				$this->request->data['Journal']['mode']=$Journal['Journal']['debit'];
				$this->request->data['Journal']['cheque_no']=$Journal['Journal']['cheque_no'];
				$this->request->data['Journal']['cheque_date']=date('d-m-Y',strtotime($Journal['Journal']['cheque_date']));
				$this->request->data['Journal']['reference_no']=$Journal['Journal']['reference_no'];
				// $this->request->data['JournalVoucher']['branch_id']=$Journal['Journal']['branch_id'];
				$this->request->data['Journal']['account_head_name']=$Customer['AccountHead']['name'];
				 $this->request->data['Journal']['account_head']=$Journal['Journal']['credit'];
				$ReceiptVoucherList=$this->Journal->find('first',array(
					'joins'=>[
						array(
							'table'=>'account_heads',
							'alias'=>'AccountHead',
							'type'=>'LEFT',
							'conditions'=>array('AccountHead.id=Journal.debit')
							),
						],
					'conditions'=>['Journal.id'=>$id,'Journal.flag'=>1],
					'fields'=>array(
						'Journal.*',
						'AccountHead.name'
					)
				));
				 $this->set('ReceiptVoucherList',$ReceiptVoucherList);
		  }
	}
	else
	{
			$datasource_Journal = $this->Journal->getDataSource();
			try {
			$datasource_Journal->begin();
			$data=$this->request->data['Journal'];
			$credit=$data['account_head'];
			if($data['mode_category_ids']==1) $debit=$data['mode_cash'];
			if($data['mode_category_ids']==2) $debit=$data['mode_cheque'];
			if($data['mode_category_ids']==3) $debit=$data['mode_bank'];
			$amount=$data['amount'];
			$date=$data['date'];
			//$narration=$data['narration'];
			$reference_no=$data['reference_no'];
			$cheque_no=$data['cheque_no'];
			$cheque_date=$data['cheque_date'];
			if($data['mode_category_ids']==2){
			//$cheque_no=$data['cheque_no'];
			$cheque_date=$data['cheque_date'];
		    }
		    $voucher_no=$data['journal_no'];
			$remarks=$data['remarks'];
			$narration='';
			//$voucher_no=$data['voucher_no'];
			$external_voucher=$data['external_voucher'];
			$work_flow='Voucher Receipt';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$narration,$reference_no,$cheque_no,$cheque_date,"","",$external_voucher,"",$user_branch_id);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
		 $id=$this->Journal->getLastInsertId();
			$return['result']='Success';
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		    $this->redirect(array('controller' => 'Accounts','action' =>'VoucherReceipt',$id,$credit,$voucher_no,$data['amount']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
}
public function VoucherReceiptList()
{
	$decimalpoint = 2;
	$this->set('decimalpoint',$decimalpoint);
	// $PermissionList = $this->Session->read('PermissionList');
	// $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Sale/InvoiceList'));

	// if(!in_array($menu_id, $PermissionList))
	// {
	//   $this->Session->setFlash("Permission denied");
	//   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
	// }
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');

	// $executives=$this->Executive->find('list',array(
	// 'fields'=>['Executive.id','Executive.name']
	// ));
	// $executives+=['0' => 'NO EXECUTIVE'];
	// $this->set(compact('executives'));
}
public function journal_get_ajax($id)
{
	try {
		$Journal=$this->Journal->findById($id);
		if(!$Journal)
			throw new Exception("Empty Voucher", 1);
		$return['data']=$Journal['Journal'];
		$return['AccountHeadCredit']=$Journal['AccountHeadCredit']['name'];
		$return['AccountHeadDebit']=$Journal['AccountHeadDebit']['name'];
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function journal_edit_ajax()
{
	try {
		$data=$this->request->data['PaymentVoucher'];
		$this->Journal->id=$data['journal_id'];
		$this->Journal->saveField('credit',$data['account']);
		$this->Journal->saveField('remarks',$data['reference']);
		$this->Journal->saveField('amount',$data['amount']);
		$this->Journal->saveField('updated_at',date('Y-m-d h:i:s'));
		
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	echo json_encode($return); exit;
}
public function journal_delete_ajax()
{
	try {
		$data=$this->request->data['PaymentVoucher'];
		// print_r($data);die;
		$this->Journal->id=$data['journal_id'];
		if(!$this->Journal->saveField('flag','0'))
			throw new Exception("Error Processing Request", 1);
		if(!$this->Journal->saveField('updated_at',date('Y-m-d H:i:s')))
			throw new Exception("Error Processing Request", 1);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	echo json_encode($return); exit;
}
public function receipt_voucher_ajax()
{
	$decimalpoint = 2;
	$user_branch_id=$this->Session->read('User.branch_id');
	$requestData=$this->request->data;
	$columns=[];
	$columns[]='Journal.date';
	$columns[]='Journal.id';
	$columns[]='Journal.narration';
	$columns[]='Journal.amount';
	$conditions=[];

	//order only fields that are field of datatabse table
	$order= '';
	if(!in_array($requestData['order'][0]['column'],[4,5]))
	{
		$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
	}
	if(isset($requestData['from_date']) && isset($requestData['to_date']) )
	{
		$conditions['Journal.date between ? and ?']=[
		date('Y-m-d',strtotime($requestData['from_date'])),
		date('Y-m-d',strtotime($requestData['to_date']))
		];
	}
	if($user_branch_id) $conditions['Journal.branch_id']=$user_branch_id;
	$conditions['Journal.flag']=1;
	$conditions['Journal.work_flow']= 'Voucher Receipt';
	$totalData=$this->Journal->find('count',['conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
      $q=$requestData['search']['value'];
      $conditions['OR']=array(
        'Journal.date LIKE' =>'%'. $q . '%',
        'Journal.id LIKE' =>'%'. $q . '%',
        'Journal.narration LIKE' =>'%'. $q . '%',
        'Journal.amount LIKE' =>'%'. $q . '%',
      );
      $totalFiltered=$this->Journal->find('count',[
        'conditions'=>$conditions,
      ]);
    }
 // 'conditions' =>  array('not' => array('Sale.workflow'=>null)),

  	
    $Data=$this->Journal->find('all',array(
      'conditions'=>$conditions,
      'offset'=>$requestData['start'],
      'limit'=>$requestData['length'],
	  'order'=>$order,
	  'group' =>'Journal.voucher_no',
      'fields'=>array(
		'Journal.id',
		'Journal.date',
		'Journal.voucher_no',
		'Journal.remarks',
		'SUM(Journal.amount) as total',
		'AccountHeadCredit.name'
      )
    ));
    //print_r($Data);die;
    foreach ($Data as $key => $value) {
		/*$Data[$key]['Sale']['action']='<span><a target="_blank" href="'.$this->webroot.'Print/fpdf/'.$value['Sale']['id'].'"><i class="fa fa-2x fa-print"></i></a></span>';*/
		//  $Data[$key]['Journal']['action']='<span><a  href="#" onclick="openInvoicePrint('.$value['Journal']['id'].')"><i class="fa fa-2x fa-print"></i></a></span>';
		$Data[$key]['Journal']['date']=date('d-m-Y',strtotime($value['Journal']['date']));
		$Data[$key]['Journal']['id']=$value['Journal']['voucher_no'];
		$Data[$key]['Journal']['remarks']=$value['Journal']['remarks'];
		$Data[$key]['Journal']['particular']=$value['AccountHeadCredit']['name'];
		$Data[$key]['Journal']['amount']=number_format($value[0]['total'],2,'.','');
	    $Data[$key]['Journal']['action']='&nbsp;&nbsp;<span><a target="_blank" href="'.$this->webroot.'Accounts/VoucherReceipt/'.$value['Journal']['id'].'"><i class="fa fa-2x fa-eye"></i></span></a>';
		if($this->Session->read('UserRole.id')==1)
    	$Data[$key]['Journal']['action'].='&nbsp;&nbsp;<span hidden class="table_id">'.$value['Journal']['id'].'</span><a href= "#"><span><i class="fa fa-2x fa-edit edit_icon payment_edit"></i></span></a>';
   }

  //sorting total/balance/recieved
  if(in_array($requestData['order'][0]['column'],[4,5])) {

    //callbackfunction
    function cmp($a, $b,$columnname,$columndir)
    {
      if ($a["Journal"][$columnname] == $b["Journal"][$columnname]) {
        return 0;
      }
      if($columndir == 'asc') {
        return (str_replace(',','',$a["Journal"][$columnname]) < str_replace(',','',$b["Journal"][$columnname])) ? -1 : 1;
      }
      else if($columndir == 'desc')
      {
        return (str_replace(',','',$a["Journal"][$columnname]) > str_replace(',','',$b["Journal"][$columnname])) ? -1 : 1;
      }
    }

    $columnname = $columns[$requestData['order'][0]['column']]; //column name
    $columndir = $requestData['order'][0]['dir']; //column order
    //sorting function
    usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
  }

  $json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData),
    "recordsFiltered"=>intval($totalFiltered),
    "records"        =>$Data
  );
   echo json_encode($json_data); exit;
}
public function VoucherReceipt_table_ajax()
{
		
		$requestData=$this->request->data;
		$columns = [];
		//$columns[]='AccountHead.created_at';
		$columns[]='AccountHead.name';
		$columns[]='total';
		$columns[]='';
		$cash_group_id=1;
	$bank_group_id=2;
	$crediters_group_id=18;
	$reserves_group_id=24;
	$provision_group_id=23;
	$pre_paid_expense_group_id=5;
	$outstanding_expense_group_id=20;
	$profit_loss_group_id=25;
	$profit_loss_dot_group_id=27;
	$expense_master_group_id=5;
		$conditions=[];
		// $conditions['SubGroup.group_id']=array(
		// 		$cash_group_id=1,
	 //            $bank_group_id=2,
		// 	);
	// 	$conditions['NOT']=array(
	// 		'SubGroup.group_id' =>array(
	// 			$cash_group_id,
	// 					$bank_group_id,
	// 					$crediters_group_id,
	// 					$profit_loss_group_id,
	// 					$profit_loss_dot_group_id,
	// 					$pre_paid_expense_group_id,
	// 					$outstanding_expense_group_id,
	// 					$reserves_group_id,
	// 					$provision_group_id,
	// 		),
	// 	'Group.master_group_id'=>[$expense_master_group_id],
	// );
		
	$conditions['AccountHead.acc_sub_group_id !=']=0;

		if(isset($requestData['account_head_id']))
	{
		if($requestData['account_head_id'])
			$conditions['AccountHead.id']=$requestData['account_head_id'];
	}
		$totalData=$this->AccountHead->find('count',[
			// 'joins'=>[
			// array(
			// 	'table'=>'groups',
			// 	'alias'=>'Group',
			// 	'type'=>'INNER',
			// 	'conditions'=>array('Group.id=SubGroup.group_id')
			// 	),
			// ],
			'conditions'=>$conditions
		]);
		$totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'AccountHead.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->AccountHead->find('count',[
			// 	'joins'=>[
			// array(
			// 	'table'=>'groups',
			// 	'alias'=>'Group',
			// 	'type'=>'INNER',
			// 	'conditions'=>array('Group.id=SubGroup.group_id')
			// 	),
			// ],
				'conditions'=>$conditions,
			]);
		}

		//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column'] <= 1)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
		$Data=$this->AccountHead->find('all',array(
			// 'joins'=>[
			// array(
			// 	'table'=>'groups',
			// 	'alias'=>'Group',
			// 	'type'=>'INNER',
			// 	'conditions'=>array('Group.id=SubGroup.group_id')
			// 	),
			// ],
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			//'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'order'=> $order,
			'fields'=>array(
				'AccountHead.*',
			)
		));
		$category=[
			'Asset'=>'debit_account',
			'Expense'=>'debit_account',
			'Capital'=>array(
				'Capital'=>'credit_account',
				'Drawing'=>'debit_account',
			),
			'Income'=>'credit_account',
			'Liabilities'=>'credit_account',
		];
		foreach ($Data as $key => $value) {
			$function_return=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
			$Data[$key]['AccountHead']['created_at']=date('d-m-Y',strtotime($value['AccountHead']['created_at']));
			$Data[$key]['AccountHead']['name']=ucwords(strtolower($value['AccountHead']['name']));
			$type=$this->get_type_by_account_dead($value['AccountHead']['id']);
			$Type_name=$type['AccMainGroup']['name'];
			$category_name=$category[$type['AccMainGroup']['name']];
			if($Type_name=='Capital')
			{
		   $category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
			}
			$debit=$function_return['debit'];
			$credit=$function_return['credit'];
			if($category_name=='credit_account')
			{
				//$credit+=$value['AccountHead']['opening_balance'];
				if($value['AccountHead']['opening_balance']<0)
				{
					$debit+=$value['AccountHead']['opening_balance']*-1;
				}
				else{
					$credit+=$value['AccountHead']['opening_balance'];
				}
			}
			else
			{
				//$debit+=$value['AccountHead']['opening_balance'];
				if($value['AccountHead']['opening_balance']<0)
				{
					$credit+=$value['AccountHead']['opening_balance']*-1;
				}
				else{
					$debit+=$value['AccountHead']['opening_balance'];
				}
			}
			if($category_name=='credit_account')
			{
				$Data[$key]['AccountHead']['total']=$credit;
				$Data[$key]['AccountHead']['total']-=$debit;
			}
			else
			{
				$Data[$key]['AccountHead']['total']=$debit;
				$Data[$key]['AccountHead']['total']-=$credit;
			}
			$Data[$key]['AccountHead']['total']=number_format($Data[$key]['AccountHead']['total'],2,'.','');
		}
		//sorting total/balance/recieved
		if($requestData['order'][0]['column'] >= 2) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["AccountHead"][$columnname] == $b["AccountHead"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return (str_replace(',','',$a["AccountHead"][$columnname]) < str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return (str_replace(',','',$a["AccountHead"][$columnname]) > str_replace(',','',$b["AccountHead"][$columnname])) ? -1 : 1;
				}
			}
			

			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
		//pr($Data);
		//exit;
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
}
public function AccountHeadCreate($main_group_id,$sub_group_id,$name,$opening_balance,$date,$description,$tax=null,$trn_number=null,$supplier=null,$show_in_app=null)
{
	try {
		$AccountHead_date=[
		'name'=>strtoupper($name),
		'opening_balance'=>$opening_balance,
		'description'=>$description,
		'tax'=>$tax,
		'trn_number'=>$trn_number,
		'supplier'=>$supplier,
		'show_in_app'=>$show_in_app,
		'sub_group_id'=>$sub_group_id,
		'acc_sub_group_id'=>$sub_group_id,
		'acc_main_group_id'=>$main_group_id,
		'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
		'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
		];
		$this->AccountHead->create();
		if(!$this->AccountHead->save($AccountHead_date))
		{
			$errors = $this->AccountHead->validationErrors;
			foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	return $return;
}
public function AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description,$sub_group_id)
{
	try {
		$this->AccountHead->id=$AccountHead_id;
		if(!$this->AccountHead->saveField('opening_balance',$opening_balance)) throw new Exception("Cant Updated the Opening Balance", 1);
		if(!$this->AccountHead->saveField('name',strtoupper($name))) throw new Exception("Cant Updated the name", 1);
		if(!$this->AccountHead->saveField('description',$description)) throw new Exception("Cant Updated the description", 1);
		if(!$this->AccountHead->saveField('sub_group_id',$sub_group_id)) throw new Exception("Cant Updated the sub_group_id", 1);
	    if(!$this->AccountHead->saveField('acc_sub_group_id',$sub_group_id)) throw new Exception("Cant Updated the sub_group_id", 1);
		if(!$this->AccountHead->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date)))) throw new Exception("Error updation updated_at", 1);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	return $return;
}
public function JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$narration=null,$reference_no=null,$cheque_no=null,$cheque_date=null,$mode_category=null,$each_cost_center=null,$external_voucher=null,$day_register_id=null,$branch_id=null,$executive_id=null,$route_id=null)
{	
	try {

		if(empty($voucher_no)) { $voucher_no=$this->GetVoucherNo(); }
		if($amount)
		{
			$Journal_data=[
			'remarks'=>$remarks,
			'credit'=>$credit,
			'debit'=>$debit,
			'amount'=>str_replace(',','',floatval($amount)),
			'date'=>date('Y-m-d',strtotime($date)),
			'work_flow'=>$work_flow,
			'voucher_no'=>$voucher_no,
			'external_voucher'=>$external_voucher,
			'day_register_id'=>$day_register_id,
			'flag'=>'1',
			'branch_id'=>$branch_id,
			'executive_id'=>$executive_id,
			'route_id'=>$route_id,
			'narration'=>$narration,
			'reference_no'=>$reference_no,
			'cheque_no'=>$cheque_no,
			'cheque_date'=>date('Y-m-d',strtotime($cheque_date)),
			'mode_category'=>$mode_category,
			'each_cost_center'=>$each_cost_center,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_at'=>date('Y-m-d H:i:s'),

			];
			
			$this->Journal->create();
			if(!$this->Journal->save($Journal_data))
			{
				$errors = $this->Journal->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	return $return;
}
public function CashBook()
	{
		$this->set('decimalpoint',2);
			$from_date=date('d-m-Y');
			$to_date=date('d-m-Y');
			$this->set('from_date',$from_date);
			$this->set('to_date',$to_date);
			$this->request->data['Reports']['to_date']=$to_date;
			$AccountHeads=$this->AccountHead->find('list',array(
		'conditions'=>array(
			'AccountHead.acc_sub_group_id'=>5,
			),
		'fields'=>array('AccountHead.id','AccountHead.name'),
		));
		    $this->set('AccountHead',$AccountHeads);
	}
	public function AccountHead_Table_ListBySubGroupId($sub_group_id)
{
	$AccountHeads=$this->AccountHead->find('all',array(
		'conditions'=>array(
			'AccountHead.acc_sub_group_id'=>$sub_group_id,
			),
		));
	return $AccountHeads;
}
	public function CashBook_ajax()
	{
		try {
			$user_role_id=$this->Session->read('UserRole.id');
			if($user_role_id==1) $user_branch_id=1;
			else $user_branch_id=$this->Session->read('User.branch_id');
			$branch_id="";
			if($user_branch_id)
			{
				$branch_id=$user_branch_id;
			}
			// $data['name']='SUJITH_IMPREST. PREPAID';
			// $data['name']='RACY SANITARYWARES';
			// $data['from_date']='01-05-2010';
			// $data['to_date']='15-05-2019';
			$data=$this->request->data;
			// print_r($data);
			// exit;
			// $c="'Credit No :'.'%'";
			// $d="'Debit No :'.'%'";
			if($data['from_date']&&$data['to_date']){
				$from_date=date('Y-m-d',strtotime($data['from_date']));
				$to_date=date('Y-m-d',strtotime($data['to_date']));
			}else{
				$from_date=date('Y-m-d');
				$to_date=date('Y-m-d');
			}
			$condition_branch['Branch.id']=$user_branch_id;
			// $cash_id=$this->Branch->find('all',array('conditions'=>$condition_branch,'fields'=>['id','cash_id']));
			$cash_id=$data['cash_id'];
			//$cash_id=$this->SystemParameter->field('value',array('id'=>7));
			//$AccountHead=$this->AccountHead->findByAccSubGroupId($cash_id);
			$AccountHead=$this->AccountHead->findById($cash_id);
			// $AccountHead=$this->AccountHead->findByName($name);
			//print_r($AccountHead);
			//exit;

			if(empty($AccountHead))
				throw new Exception("Empty AccountHead", 1);
			$this->Journal->virtualFields = array(
				'total_amount' => "SUM(Journal.amount)",
				);

			$Journal=$this->Journal->find('all',array(
				'joins'=>array(
					array(
						'table'=>'executives',
						'alias'=>'Executive',
						'type'=>'LEFT',
						'conditions'=>array('Journal.executive_id=Executive.id')
						),
					),
				'conditions'=>array(
					'AND' => array(
						'OR' => array(
							'Journal.debit'=>$AccountHead['AccountHead']['id'],
							'Journal.credit'=>$AccountHead['AccountHead']['id'],
			//'Journal.branch_id'=>$branch_id,
							),
						'AND' => array(
							//'Journal.flag=1',
							'Journal.date between ? and ?'=>array($from_date,$to_date),
							//'Journal.branch_id'=>$branch_id,
							)
						)
					),
				'order'=>array('Journal.date ASC'),
				'fields'=>array(
					'Journal.id',
					'Journal.remarks',
					'Journal.amount',
					'Journal.date',
					'Journal.voucher_no',
					'Journal.external_voucher',
					'Journal.work_flow',
					'Journal.created_at',
					'Journal.executive_id',
					'Executive.id',
					'Executive.name',
					'AccountHeadCredit.name',
					'AccountHeadCredit.opening_balance',
					'AccountHeadDebit.name',
					'AccountHeadDebit.opening_balance',

					),
				));
			// print_r($Journal);
			// exit;
			$category=[
			'Asset'=>'debit_account',
			'Expense'=>'debit_account',
			'Capital'=>array(
				'Capital'=>'credit_account',
				'Drawing'=>'debit_account',
				),
			'Income'=>'credit_account',
			'Liabilities'=>'credit_account',
			];
			$Journal_all=[];$balance=0;
			foreach ($Journal as $key => $value) 
			{
				$Journal_single['id']=$value['Journal']['id'];
				$Journal_single['date']=$value['Journal']['date'];
				$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
				$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
				$Journal_single['remarks']=$value['Journal']['remarks'];
				$Journal_single['amount']=$value['Journal']['amount'];
				$Journal_single['work_flow']=$value['Journal']['work_flow'];
				if(!empty($value['Executive']['name'])){
					$Journal_single['executive']=$value['Executive']['name'];
				}
				else{
					$Journal_single['executive']="No Executive";
				}
				$Journal_single['external_voucher']=$value['Journal']['external_voucher'];
				if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
				{
					$Journal_single['mode']=$value['AccountHeadCredit']['name'];
					$Journal_single['debit']=$value['Journal']['amount'];
					$Journal_single['credit']=0.00;
				}
				else
				{
					$Journal_single['mode']=$value['AccountHeadDebit']['name'];
					$Journal_single['credit']=$value['Journal']['amount'];
					$Journal_single['debit']=0.00;
				}
				$Journal_all[$Journal_single['id']]=$Journal_single;
			}
			//print_r($Journal_all);exit;
			$return['row']['tbody']='';
			$return['row']['tfoot']='';
			$Balance_Total=0;
			$credit_Total=0;
			$debit_Total=0;
			$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
			$Type_name=$type['AccMainGroup']['name'];
			$category_name=$category[$type['AccMainGroup']['name']];
			if($Type_name=='Capital')
			{
				$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
			}
				if($category_name=='credit_account')
			{
				$credit=$AccountHead['AccountHead']['opening_balance'];
				$debit='0';
				if($AccountHead['AccountHead']['opening_balance']<0)
				{
					$credit=0;
					$debit=$AccountHead['AccountHead']['opening_balance']*-1;
				}
			}
			else
			{
				$debit=$AccountHead['AccountHead']['opening_balance'];
				$credit='0';
				if($AccountHead['AccountHead']['opening_balance']<0)
				{
					$debit=0;
					$credit=$AccountHead['AccountHead']['opening_balance']*-1;
				}
			}
			if(!in_array($Type_name, ['Expense','Income']))
			{
				$JournalCLosginDebit=$this->Journal->find('first',array(
					'conditions'=>array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.flag=1',
						'Journal.date <'=>$from_date,
						// 'Journal.branch_id'=>$branch_id,
						),
					'fields'=>array( 'total_amount'),
					));
				$JournalCLosginCredit=$this->Journal->find('first',array(
					'conditions'=>array(
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
						'Journal.flag=1',
						'Journal.date <'=>$from_date,
						// 'Journal.branch_id'=>$branch_id,
						),
					'fields'=>array('total_amount'),
					));
				$debit+=$JournalCLosginDebit['Journal']['total_amount'];
				$credit+=$JournalCLosginCredit['Journal']['total_amount'];
				$balance1=$debit-$credit;
				if($balance<0) {
					$credit=$balance1;
					$debit='0.00';
				}else{
					$debit=$balance1;
					$credit='0.00';
				}
				$Journal_all[0]=array(
					'id'=>0,
					'date'=>date('d-m-Y',strtotime($from_date)),
					//'Journal.branch_id'=>$branch_id,
					'strtotime'=>0,
					'mode'=>'Opening Balance',
					'executive'=>'',
					'voucher_no'=>' ',
					'external_voucher'=>'',
					'remarks'=>'',
					'credit'=>floatval($credit),
					'debit'=>floatval($debit),
					'balance'=>number_format($balance1,2,'.',''),
					);
			}
			else
			{
				if(date('Y-m-d',strtotime($from_date))>=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
				{

				}
				else
				{
					$debit=0;
					$credit=0;
				}
			}
			if(!in_array($Type_name, ['Expense','Income']))
			{
				$Journal_voucher_no=$this->Journal->find('list',array(
					'conditions'=>array(
						'AND' => array(
							'OR' => array(
								'Journal.debit'=>$AccountHead['AccountHead']['id'],
								'Journal.credit'=>$AccountHead['AccountHead']['id'],
								),
							'AND' => array(
								'Journal.flag=1',
								'Journal.date between ? and ?'=>array($from_date,$to_date),
								//'Journal.branch_id'=>$branch_id,
								)
							)
						),
					'fields'=>array(
						'Journal.id',
						'Journal.date',
						'Journal.voucher_no',
						),
					));
			}
			// print_r($Journal_all);
			// exit;
			$date=[];
			foreach ($Journal_all as $i => $row) {
				$date[$i]  = $row['strtotime'];
			}
			array_multisort($date, SORT_ASC, $Journal_all);
			foreach ($Journal_all as $key => $value) 
			{

			//pr($Journal_all);
			//exit;
				$return['row']['tbody'].='<tr class="blue-pddng day-book">';
				$return['row']['tbody'].='<td style="width:10%;"  class="color_label text-center">'.date('d-m-Y',strtotime($value['date'])).'</td>';
				$return['row']['tbody'].='<td style="width:20%;"  class="color_label">'.$value['mode'].'</td>';
				$return['row']['tbody'].='<td style="width:5%;color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no" id="'.$cash_id.'">'.$value['voucher_no'].'</td>';
				$return['row']['tbody'].='<td style="width:30%;" class="color_label">'.$value['remarks'].'</td>';
				$return['row']['tbody'].='<td style="width:10%;" class="color_label text-right">'.number_format($value['debit'],2,'.','').'</td>';
				$return['row']['tbody'].='<td style="width:10%;" class="color_label text-right">'.number_format($value['credit'],2,'.','').'</td>';
				if($category_name=='credit_account') {
				$balance=$balance+$value['credit']-$value['debit']; }
				else { $balance=$balance+$value['debit']-$value['credit']; }
				if($balance>=0){
					$return['row']['tbody'].='<td style="width:10%;" class="color_label text-right">'.number_format($balance,2,'.','').' '.'Dr'.'</td>';
				}
				else{
					$balance1 = str_replace("-", "", $balance);
					$return['row']['tbody'].='<td style="width:10%;" class="color_label text-right">'.number_format($balance1,2,'.','').' '.'Cr'.'</td>';
				}
				// $return['row']['tbody'].='<td style="width:5%;" class="color_label"></td>';
				$credit_Total+=$value['credit'];
				$debit_Total+=$value['debit'];
				// $return['row']['tbody'].='<td><span class="journal_id" hidden>'.$value['id'].'</span>';
				// $return['row']['tbody'].='<span><i class="fa fa-trash blue-col delete"></i></span>';
				// $return['row']['tbody'].='</td>';
				$return['row']['tbody'].='</tr>';
			}
			$Type_name=$type['AccMainGroup']['name'];
			$category_name=$category[$type['AccMainGroup']['name']];
			if($Type_name=='Capital')
			{
				$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
			}
			if($category_name=='credit_account')
			{

				$Balance_Total=$credit_Total-$debit_Total;
			}
			else
			{
				$Balance_Total=$debit_Total-$credit_Total;	
			}
			$return['row']['tbody'].='<tr class="blue-pddng day-book">';
			$return['row']['tbody'].='<td></td>';
			$return['row']['tbody'].='<td></td>';
			$return['row']['tbody'].='<td></td>';
			$return['row']['tbody'].='<td class="total_amount">Total</td>';
			$return['row']['tbody'].='<td class="total_amount text-right">'.number_format($debit_Total,2,'.','').'</td>';
			$return['row']['tbody'].='<td class="total_amount text-right">'.number_format($credit_Total,2,'.','').'</td>';
			if($Balance_Total>=0){
				$return['row']['tbody'].='<td class="total_amount text-right">'.number_format($Balance_Total,2,'.','').' '.'Dr'.'</td>';
			}else{
				$Balance_Total1 = str_replace("-", "", $Balance_Total);
				$return['row']['tbody'].='<td class="total_amount text-right">'.number_format($Balance_Total1,2,'.','').' '.'Cr'.'</td>';
			}
			//exit;
			$return['row']['tfoot']='';
			
			
			// $return['row']['tfoot'].='</tr>';
			$return['row']['tfoot'].='<tr class="blue-pddng">';
			$return['row']['tfoot'].='<td></td>';
			$return['row']['tfoot'].='<td></td>';
			$return['row']['tfoot'].='<td></td>';
			$return['row']['tfoot'].='<td class="total_amount">Closing Balance</td>';
			// if($Balance_Total>0){
			// 	$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,2,'.','').'</td>';
			// 	$return['row']['tfoot'].='<td></td>';
			// }else{
			// 	$return['row']['tfoot'].='<td></td>';
			// 	$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,2,'.','').'</td>';
			// }
			if($Balance_Total>=0){
				$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,2,'.','').' '.'Dr'.'</td>';
				$return['row']['tfoot'].='<td></td>';
				$return['row']['tfoot'].='<td></td>';
			}else{
				$Balance_Total1 = str_replace("-", "", $Balance_Total);
				$return['row']['tfoot'].='<td></td>';
				$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total1,2,'.','').' '.'Cr'.'</td>';
				$return['row']['tfoot'].='<td></td>';
			}
			
			$return['row']['tfoot'].='</tr>';

			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function general_journal_transaction_by_voucher_no_ajax2($voucher_no,$name)
{
	// $AccountHead=$this->AccountHead->findByName(trim($name));
	$AccountHead=$this->AccountHead->findById($name);
	$customer_id=$this->SystemParameter->field('value',array('id'=>11));
	if($AccountHead['AccountHead']['sub_group_id']==$customer_id){
		$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					),
				'AND' => array(
					'Journal.debit!=19',
					'Journal.credit!=19',
					 'Journal.debit!=13',
					 'Journal.credit!=13',
					 'Journal.debit!=23',
					 'Journal.credit!=23',
					'Journal.flag=1',
					'Journal.voucher_no'=>$voucher_no,
					)
				)
			),
		'fields'=>array(
			'Journal.id',
			'Journal.remarks',
			'Journal.amount',
			'Journal.date',
			'Journal.voucher_no',
			'Journal.work_flow',
			'Journal.created_at',
			'AccountHeadCredit.name',
			'AccountHeadCredit.opening_balance',
			'AccountHeadDebit.name',
			'AccountHeadDebit.opening_balance',
			'Journal.narration',
			),
		));
}else if($AccountHead['AccountHead']['id']=='19'){
	$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					),
				'AND' => array(
					//'Journal.debit!=19',
					//'Journal.credit!=19',
					// 'Journal.debit!=23',
					// 'Journal.credit!=23',
					'Journal.flag=1',
					'Journal.voucher_no'=>$voucher_no,
					)
				)
			),
		'fields'=>array(
			'Journal.id',
			'Journal.remarks',
			'Journal.amount',
			'Journal.date',
			'Journal.voucher_no',
			'Journal.work_flow',
			'Journal.created_at',
			'AccountHeadCredit.name',
			'AccountHeadCredit.opening_balance',
			'AccountHeadDebit.name',
			'AccountHeadDebit.opening_balance',
			'Journal.narration',
			),
		));
}else{
	$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					),
				'AND' => array(
					'Journal.debit!=19',
					'Journal.credit!=19',
					// 'Journal.debit!=23',
					// 'Journal.credit!=23',
					'Journal.flag=1',
					'Journal.voucher_no'=>$voucher_no,
					)
				)
			),
		'fields'=>array(
			'Journal.id',
			'Journal.remarks',
			'Journal.amount',
			'Journal.date',
			'Journal.voucher_no',
			'Journal.work_flow',
			'Journal.created_at',
			'AccountHeadCredit.name',
			'AccountHeadCredit.opening_balance',
			'AccountHeadDebit.name',
			'AccountHeadDebit.opening_balance',
			'Journal.narration',
			),
		));
}
	$category=[
	'Asset'=>'debit_account',
	'Expense'=>'debit_account',
	'Capital'=>array(
		'Capital'=>'credit_account',
		'Drawing'=>'debit_account',
		),
	'Income'=>'credit_account',
	'Liabilities'=>'credit_account',
	];
	$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
	$Type_name=$type['AccMainGroup']['name'];
	$category_name=$category[$type['AccMainGroup']['name']];
	$Journal_all=[];
	foreach ($Journal as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
		$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
		$Journal_single['narration']=$value['Journal']['narration'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['work_flow']=$value['Journal']['work_flow'];
		$Journal_single['debit']=0;
		$Journal_single['credit']=0;
		if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
		{
			$Journal_single['mode']=$value['AccountHeadCredit']['name'];
			$Journal_single['debit']=$value['Journal']['amount'];
		}
		else
		{
			$Journal_single['mode']=$value['AccountHeadDebit']['name'];
			$Journal_single['credit']=$value['Journal']['amount'];
		}
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	$return['row']['tbody']='';
	$total_debit=0;
	$total_credit=0;
	foreach ($Journal_all as $key => $value) 
	{
		$total_credit+=$value['credit'];
		$total_debit+=$value['debit'];
		$return['row']['tbody'].='<tr class="blue-pddng">';
		$return['row']['tbody'].='<td class="color_label"><span style="display:none" class="journal_id">'.$value['id'].'</span><span>'.date('d-m-Y',strtotime($value['date'])).'</span></td>';
		$return['row']['tbody'].='<td class="color_label">'.$value['voucher_no'].'</td>';
		$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
		$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
		$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['debit'],2,'.','').'</td>';
		$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['credit'],2,'.','').'</td>';
		
		$return['row']['tbody'].='<td class="color_label">'.$value['narration'].'</td>';
	// 	$return['row']['tbody'].='<td class="color_label">'.$value['work_flow'].'</td>';
	 }
	$return['row']['tfoot']='';
	$return['row']['tfoot'].='<tr class="blue-pddng">';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td class="total_amount">Total</td>';
	$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($total_debit,2,'.','').'</td>';
	$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($total_credit,2,'.','').'</td>';
	
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<tr class="blue-pddng">';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td class="total_amount">Balance</td>';
	if($Type_name=='Capital')
		$category_name='credit_account';
	if($category_name=='credit_account')
		$Balance_Total=$total_credit-$total_debit;
	else
		$Balance_Total=$total_debit-$total_credit;	
	if($category_name=='debit_account')
	{
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,2,'.','').'</td>';
		$return['row']['tfoot'].='<td></td>';
	}
	else
	{
		$return['row']['tfoot'].='<td></td>';	
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,2,'.','').'</td>';
	}
	//$return['row']['tfoot'].='<td></td>';
	//$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	echo json_encode($return); exit;
}
	public function general_journal_transaction_by_voucher_no_ajax($voucher_no,$name,$AccountHead_id)
{
	$AccountHead=$this->AccountHead->findById($AccountHead_id);
$customer_id=$this->SystemParameter->field('value',array('id'=>11));
	if($AccountHead['AccountHead']['sub_group_id']==$customer_id){
		$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					),
				'AND' => array(
					'Journal.debit!=19',
					'Journal.credit!=19',
					 'Journal.debit!=13',
					 'Journal.credit!=13',
					 // 'Journal.debit!=23',
					 // 'Journal.credit!=23',
					'Journal.flag=1',
					'Journal.voucher_no'=>$voucher_no,
					)
				)
			),
		'fields'=>array(
			'Journal.id',
			'Journal.remarks',
			'Journal.amount',
			'Journal.date',
			'Journal.voucher_no',
			'Journal.work_flow',
			'Journal.created_at',
			'AccountHeadCredit.name',
			'AccountHeadCredit.opening_balance',
			'AccountHeadDebit.name',
			'AccountHeadDebit.opening_balance',
			'Journal.narration',
			),
		));
}else if($AccountHead['AccountHead']['id']=='19'){
	$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					),
				'AND' => array(
					//'Journal.debit!=19',
					//'Journal.credit!=19',
					// 'Journal.debit!=23',
					// 'Journal.credit!=23',
					'Journal.flag=1',
					'Journal.voucher_no'=>$voucher_no,
					)
				)
			),
		'fields'=>array(
			'Journal.id',
			'Journal.remarks',
			'Journal.amount',
			'Journal.date',
			'Journal.voucher_no',
			'Journal.work_flow',
			'Journal.created_at',
			'AccountHeadCredit.name',
			'AccountHeadCredit.opening_balance',
			'AccountHeadDebit.name',
			'AccountHeadDebit.opening_balance',
			'Journal.narration',
			),
		));
}else{
	$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					),
				'AND' => array(
					'Journal.debit!=19',
					'Journal.credit!=19',
					// 'Journal.debit!=23',
					// 'Journal.credit!=23',
					'Journal.flag=1',
					'Journal.voucher_no'=>$voucher_no,
					)
				)
			),
		'fields'=>array(
			'Journal.id',
			'Journal.remarks',
			'Journal.amount',
			'Journal.date',
			'Journal.voucher_no',
			'Journal.work_flow',
			'Journal.created_at',
			'AccountHeadCredit.name',
			'AccountHeadCredit.opening_balance',
			'AccountHeadDebit.name',
			'AccountHeadDebit.opening_balance',
			'Journal.narration',
			),
		));
}
	$category=[
	'Asset'=>'debit_account',
	'Expense'=>'debit_account',
	'Capital'=>array(
		'Capital'=>'credit_account',
		'Drawing'=>'debit_account',
		),
	'Income'=>'credit_account',
	'Liabilities'=>'credit_account',
	];
	$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
	$Type_name=$type['AccMainGroup']['name'];
	$category_name=$category[$type['AccMainGroup']['name']];
	$Journal_all=[];
	foreach ($Journal as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
		$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
		$Journal_single['narration']=$value['Journal']['narration'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['work_flow']=$value['Journal']['work_flow'];
		$Journal_single['debit']=0;
		$Journal_single['credit']=0;
		if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
		{
			$Journal_single['mode']=$value['AccountHeadCredit']['name'];
			$Journal_single['debit']=$value['Journal']['amount'];
		}
		else
		{
			$Journal_single['mode']=$value['AccountHeadDebit']['name'];
			$Journal_single['credit']=$value['Journal']['amount'];
		}
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	$return['row']['tbody']='';
	$total_debit=0;
	$total_credit=0;
	foreach ($Journal_all as $key => $value) 
	{
		$total_credit+=$value['credit'];
		$total_debit+=$value['debit'];
		$return['row']['tbody'].='<tr class="blue-pddng">';
		$return['row']['tbody'].='<td class="color_label"><span style="display:none" class="journal_id">'.$value['id'].'</span><span>'.date('d-m-Y',strtotime($value['date'])).'</span></td>';
		$return['row']['tbody'].='<td class="color_label">'.$value['voucher_no'].'</td>';
		$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
		$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
		$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['debit'],2,'.','').'</td>';
		$return['row']['tbody'].='<td class="color_label text-right">'.number_format($value['credit'],2,'.','').'</td>';
		
		$return['row']['tbody'].='<td class="color_label">'.$value['narration'].'</td>';
	// 	$return['row']['tbody'].='<td class="color_label">'.$value['work_flow'].'</td>';
	 }
	$return['row']['tfoot']='';
	$return['row']['tfoot'].='<tr class="blue-pddng">';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td class="total_amount">Total</td>';
	$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($total_debit,2,'.','').'</td>';
	$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($total_credit,2,'.','').'</td>';
	
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<tr class="blue-pddng">';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td class="total_amount">Balance</td>';
	if($Type_name=='Capital')
		$category_name='credit_account';
	if($category_name=='credit_account')
		$Balance_Total=$total_credit-$total_debit;
	else
		$Balance_Total=$total_debit-$total_credit;	
	if($category_name=='debit_account')
	{
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,2,'.','').'</td>';
		$return['row']['tfoot'].='<td></td>';
	}
	else
	{
		$return['row']['tfoot'].='<td></td>';	
		$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($Balance_Total,2,'.','').'</td>';
	}
	//$return['row']['tfoot'].='<td></td>';
	//$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td></td>';
	echo json_encode($return); exit;
}

public function general_journal_transaction_ledger_report()
{
	try {
		$user_branch_id=$this->Session->read('User.branch_id');
		$branch_id="";
		if($user_branch_id)
		{
			$branch_id=$user_branch_id;
		}
		// $data['name']='SUJITH_IMPREST. PREPAID';
		// $data['name']='RACY SANITARYWARES';
		// $data['from_date']='01-05-2010';
		// $data['to_date']='15-05-2019';
		$data=$this->request->data;
//pr($data);
//exit;
		$customer_id=$this->SystemParameter->field('value',array('id'=>11));
		$name=trim($data['name']);
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$AccountHead_id=$data['AccountHead_id'];
		$AccountHead=$this->AccountHead->findById($AccountHead_id);

		if(empty($AccountHead))
			throw new Exception("Empty AccountHead", 1);
		$this->Journal->virtualFields = array(
			'total_amount' => "SUM(Journal.amount)",
			);
		if($AccountHead['AccountHead']['acc_sub_group_id']==$customer_id){
		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						'Journal.debit!=19',
						'Journal.credit!=19',
					   	'Journal.debit!=13',
					    'Journal.credit!=13',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						'Journal.flag=1'
							//'Journal.branch_id'=>$branch_id,
							)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.narration',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));}
else if($AccountHead['AccountHead']['id']=='19'){
		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						//'Journal.debit!=23',
					   // 'Journal.credit!=23',
						//'Journal.debit!=19',
						//'Journal.credit!=19',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						'Journal.flag=1'
						//'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.narration',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
	}else{
		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						//'Journal.debit!=23',
					   // 'Journal.credit!=23',
						'Journal.debit!=19',
						'Journal.credit!=19',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						'Journal.flag=1'
						//'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.narration',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
	}
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$Journal_all=[];$balance=0;
		foreach ($Journal as $key => $value) 
		{
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['narration']=$value['Journal']['narration'];
			$Journal_single['amount']=$value['Journal']['amount'];
			$Journal_single['work_flow']=$value['Journal']['work_flow'];
			if(!empty($value['Executive']['name'])){
				$Journal_single['executive']=$value['Executive']['name'];
			}
			else{
				$Journal_single['executive']="No Executive";
			}
			$Journal_single['external_voucher']=$value['Journal']['external_voucher'];
			if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
			{
				$Journal_single['mode']=$value['AccountHeadCredit']['name'];
				$Journal_single['debit']=$value['Journal']['amount'];
				$Journal_single['credit']=0.00;
			}
			else
			{
				$Journal_single['mode']=$value['AccountHeadDebit']['name'];
				$Journal_single['credit']=$value['Journal']['amount'];
				$Journal_single['debit']=0.00;
			}
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		$return['row']['tbody']='';
		$return['row']['tfoot']='';
		$Balance_Total=0;
		$credit_Total=0;
		$debit_Total=0;
		$type=$this->get_type_by_account_dead($AccountHead['AccountHead']['id']);
		$Type_name=$type['AccMainGroup']['name'];
		$category_name=$category[$type['AccMainGroup']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
		}
			if($category_name=='credit_account')
		{
			$credit=$AccountHead['AccountHead']['opening_balance'];
			$debit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$credit=0;
				$debit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$AccountHead['AccountHead']['opening_balance'];
			$credit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		if(!in_array($Type_name, ['Income']))
		{
			if($AccountHead['AccountHead']['acc_sub_group_id']==$customer_id){
			$JournalCLosginDebit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit!=13',
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array( 'total_amount'),
				));
			$JournalCLosginCredit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					'Journal.debit!=13',
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array('total_amount'),
				));
		}else if($AccountHead['AccountHead']['id']=='19'){
			$JournalCLosginDebit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array( 'total_amount'),
				));
			$JournalCLosginCredit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array('total_amount'),
				));
		}
		else{
			$JournalCLosginDebit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit !='=>19,
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array( 'total_amount'),
				));
			$JournalCLosginCredit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					'Journal.debit !='=>19,
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array('total_amount'),
				));
		}
			$debit+=$JournalCLosginDebit['Journal']['total_amount'];
			$credit+=$JournalCLosginCredit['Journal']['total_amount'];
			$balance1=$debit-$credit;
				if($balance<0) {
					$credit=$balance1;
					$debit='0.00';
				}else{
					$debit=$balance1;
					$credit='0.00';
				}
			$Journal_all[0]=array(
				'id'=>0,
				'date'=>date('d-m-Y',strtotime($from_date)),
				//'Journal.branch_id'=>$branch_id,
				'strtotime'=>0,
				'mode'=>'Opening Balance',
				'executive'=>'',
				'voucher_no'=>'',
				'external_voucher'=>'',
				'narration'=>'',
				'remarks'=>'',
				'credit'=>floatval($credit),
				'debit'=>floatval($debit),
				'balance'=>number_format($balance1,2,'.',''),
				);
		}
		else
		{
			if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
			{

			}
			else
			{
				$debit=0;
				$credit=0;
			}
		}
		if(!in_array($Type_name, ['Income']))
		{
			$Journal_voucher_no=$this->Journal->find('list',array(
				'conditions'=>array(
					'AND' => array(
						'OR' => array(
							'Journal.debit'=>$AccountHead['AccountHead']['id'],
							'Journal.credit'=>$AccountHead['AccountHead']['id'],
							),
						'AND' => array(
							'Journal.flag=1',
							'Journal.date between ? and ?'=>array($from_date,$to_date),
							//'Journal.branch_id'=>$branch_id,
							)
						)
					),
				'fields'=>array(
					'Journal.id',
					'Journal.date',
					'Journal.voucher_no',
					),
				));
			foreach ($Journal_voucher_no as $voucher_no => $lists) {
				if(count($lists)>1)
				{
					$j=0;
					foreach ($lists as $journal_id => $date) {
						if($j)
						{
							if($date==$Journal_all[key($lists)]['date'] && $voucher_no==$Journal_all[key($lists)]['voucher_no'])
							{
								$Journal_all[key($lists)]['credit']+=$Journal_all[$journal_id]['credit'];
								$Journal_all[key($lists)]['debit']+=$Journal_all[$journal_id]['debit'];
								unset($Journal_all[$journal_id]);
								$GetJournal=$this->Journal->findById($Journal_all[key($lists)]['id'],['credit']);
								if($AccountHead['AccountHead']['id']==$GetJournal['Journal']['credit'])
								{
									$Journal_all[key($lists)]['credit']-=$Journal_all[key($lists)]['debit'];
									$Journal_all[key($lists)]['debit']=0;
									$Journal_all[key($lists)]['credit']=($Journal_all[key($lists)]['credit']);
								}
								else
								{
									$Journal_all[key($lists)]['debit']-=$Journal_all[key($lists)]['credit'];
									$Journal_all[key($lists)]['credit']=0;
									$Journal_all[key($lists)]['debit']=($Journal_all[key($lists)]['debit']);
								}
							}								
						}
						$j++;
					}
				}
			}
		}
		$date=[];
			foreach ($Journal_all as $i => $row) {
				$date[$i]  = $row['strtotime'];
			}
			array_multisort($date, SORT_ASC, $Journal_all);
			$excluding_ids=['STOCK'];
			foreach ($Journal_all as $key => $value) 
			{
				$return['row']['tbody'].='<tr class="blue-pddng">';
				$return['row']['tbody'].='<td class="color_label">'.date('d-m-Y',strtotime($value['date'])).'</td>';
				if($value['voucher_no'])
				{
					$vendor_id=$this->SystemParameter->field('value',array('id'=>10));
					$sub_group=$this->AccountHead->findById($AccountHead['AccountHead']['id']);
					$sub_group_id=$sub_group['AccountHead']['acc_sub_group_id'];
					$return['row']['tbody'].='<td><span style="color:blue; text-decoration: underline;cursor: zoom-in;" class="color_label voucher_no ">'.$value['voucher_no'].'</span></td>';
				}
				else
				{
					$return['row']['tbody'].='<td style="color:blue; text-decoration: underline;cursor: zoom-in; width:10%;" class="color_label voucher_no text-right">'.$value['voucher_no'].'</td>';
				}
				$return['row']['tbody'].='<td class="color_label">'.$value['mode'].'</td>';
				$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
				if($value['mode']=='Opening Balance'){
					$return['row']['tbody'].='<td class="color_label text-right" style="width: 10%"></td>';
				}else{
				$return['row']['tbody'].='<td class="color_label text-right" style="width: 10%">'.number_format($value['debit'],2,'.','').'</td>';
				}
				if($value['mode']=='Opening Balance'){
						$return['row']['tbody'].='<td class="color_label text-right" style="width: 10%"></td>';
				}else{
					$return['row']['tbody'].='<td class="color_label text-right" style="width: 10%">'.number_format($value['credit'],2,'.','').'</td>';
				}
				if($category_name=='credit_account') {
				 $balance=$balance+$value['credit']-$value['debit']; }
				else { $balance=$balance+$value['debit']-$value['credit']; }
				// if($value['mode']=='Opening Balance'){
	   
				if($balance>=0){
				$return['row']['tbody'].='<td style="width:15%;" class="color_label text-right">'.number_format($balance,2,'.','').' '.'Dr'.'</td>';
				}
				else{
				$balance1 = str_replace("-", "", $balance);
				$return['row']['tbody'].='<td style="width:15%;" class="color_label text-right">'.number_format($balance1,2,'.','').' '.'Cr'.'</td>';
				}
				$credit_Total+=$value['credit'];
				$debit_Total+=$value['debit'];
				$return['row']['tbody'].='</tr>';
			}
			//exit;
			$return['row']['tfoot'].='<tr class="blue-pddng">';
			$return['row']['tfoot'].='<td></td>';
			$return['row']['tfoot'].='<td></td>';
			$return['row']['tfoot'].='<td></td>';
			$return['row']['tfoot'].='<td class="total_amount">Total</td>';
			$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($debit_Total,2,'.','').'</td>';
			$return['row']['tfoot'].='<td class="total_amount text-right">'.number_format($credit_Total,2,'.','').'</td>';
			$Type_name=$type['AccMainGroup']['name'];
			$category_name=$category[$type['AccMainGroup']['name']];
			if($Type_name=='Capital')
			{
				$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
			}
			if($category_name=='credit_account')
			{

				$Balance_Total=$credit_Total-$debit_Total;
			}
			else
			{
				$Balance_Total=$debit_Total-$credit_Total;	
			}
			if($Balance_Total>0) $Balance_amt=number_format($Balance_Total,2,'.','').' Dr';
			elseif($Balance_Total<0) $Balance_amt=number_format(abs($Balance_Total),2,'.','').' Cr';
			else $Balance_amt=number_format($Balance_Total,2,'.','');
			$return['row']['tfoot'].='<td class="total_amount text-right">'.$Balance_amt.'</td>';
			$return['row']['tfoot'].='</tr>';

			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
public function Expense()
{
	$expense_sub_group=[11,12,15];
		$date=date('d-m-Y');
		$this->set('date',$date);
		$Staff=$this->Staff->find('list',array(
			'fields'=>array(
				'Staff.account_head_id','Staff.account_head_id'
				),
			));
		$AccountHead=$this->AccountHead->find('all',array(
			'joins'=>array(
				array(
				'table'=>'acc_sub_groups',
				'alias'=>'AccSubGroup',
				'type'=>'INNER',
				'conditions'=>array('AccSubGroup.id=AccountHead.acc_sub_group_id')
				),
				// array(
				// 	'table'=>'staffs',
				// 	'alias'=>'Staff',
				// 	'type'=>'INNER',
				// 	'conditions'=>array('AccountHead.id!=Staff.account_head_id')
				// 	),
				),
			'fields'=>array(
				'AccountHead.*',
				//'SubGroup.*',
				//'Group.name',
				),
			'conditions'=>array(
				'AccountHead.id !='=>$Staff,
				'AccountHead.acc_sub_group_id'=>$expense_sub_group,
				)
			));
		$this->set('AccountHead',$AccountHead);
}
public function payment_voucher_detail_delete_ajax()
{
	// $datasource_JournalVoucher = $this->JournalVoucher->getDataSource();
	// $datasource_JournalVoucherDetail = $this->JournalVoucherDetail->getDataSource();
	$datasource_Journal = $this->Journal->getDataSource();
	try {
		// $datasource_JournalVoucherDetail->begin();
		$datasource_Journal->begin();
		$data=$this->request->data;
		// $JournalVoucherDetail=$this->JournalVoucherDetail->findById($data['id']);
		// $conditions['Journal.remarks']=$JournalVoucherDetail['JournalVoucherDetail']['reference'];
		$conditions['Journal.id']=$data['id'];
		$Journal=$this->Journal->find('first',array(
			'conditions'=>$conditions,
			'fields'=>array(
			  'Journal.id'
			)
		));
		// print_r($Journal['Journal']['id']);die;
		$this->Journal->id=$Journal['Journal']['id'];
		if(!$this->Journal->saveField('flag','0'))
			throw new Exception("Error Processing Request", 1);
		if(!$this->Journal->saveField('updated_at',date('Y-m-d H:i:s')))
			throw new Exception("Error Processing Request", 1);
		// $this->JournalVoucherDetail->id=$data['id'];
		// if(!$this->JournalVoucherDetail->saveField('flag','0'))
		// 	throw new Exception("Error Processing Request While delete Journal Voucher Details", 1);
		// $datasource_JournalVoucherDetail->commit();
		$datasource_Journal->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		// $datasource_JournalVoucherDetail->rollback();
		$datasource_Journal->rollback();
		$return['result']='Error';
		$return['message']=$e->getMessage();
		print_r($return['message']);die;
	}
	echo json_encode($return); exit;
}
  public function JournalVoucher($id=null)
    {
//     		$PermissionList = $this->Session->read('PermissionList');
// // $this->Menu->action='Accountings/VoucherGeneral';
// // 		$menu_id=$this->Menu->field('Menu.id');
// 	$menu_id = $this->Menu->field(
// 		'Menu.id',
// 		array('action ' => 'Accounts/JournalVoucher'));
// if(!in_array($menu_id, $PermissionList))
// {
// $this->Session->setFlash("Permission denied");
// return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
				
    		// 		    $cash_group_id=$this->SystemParameter->field('value',array('id'=>7));
						// $bank_group_id=$this->SystemParameter->field('value',array('id'=>8));
						// $crediters_group_id=$this->SystemParameter->field('value',array('id'=>10));
						// $debtors_group_id=$this->SystemParameter->field('value',array('id'=>9));
						 $cash_group_id=$this->SystemParameter->field('value',array('id'=>9));
						$bank_group_id=$this->SystemParameter->field('value',array('id'=>10));
						$crediters_group_id=$this->SystemParameter->field('value',array('id'=>12));
						$debtors_group_id=$this->SystemParameter->field('value',array('id'=>11));
$AccountHeads=$this->AccountHead->find('all',array(
			'joins'=>[
			array(
				'table'=>'acc_sub_groups',
				'alias'=>'AccSubGroup',
				'type'=>'INNER',
				'conditions'=>array('AccSubGroup.id=AccountHead.sub_group_id')
				),
			],
			'conditions'=>array(
				// 'or'=>array(
				// 	'AccountHead.acc_sub_group_id'=>array(
				// 		$cash_group_id,
				// 		$bank_group_id,
				// 		$crediters_group_id,
				// 		$debtors_group_id
				// 		// $profit_loss_group_id,
				// 		// $profit_loss_dot_group_id,
				// 		// $pre_paid_expense_group_id,
				// 		// $outstanding_expense_group_id,
				// 		// $reserves_group_id,
				// 		// $provision_group_id,
				// 		),
				// 	//'Group.master_group_id'=>[$expense_master_group_id],
				// 	),
				'AccountHead.acc_sub_group_id !='=>0,
				),
			'fields'=>[
			'AccountHead.id',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			'AccountHead.name',
			]
			)
		);
			//pr($AccountHeads);
			//exit;
			$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']]=$value['AccountHead']['name'];
		}
		$this->set('AccountHead_list',$AccountHead_list);

				$DebitCredit=[
					//''=>'Select'
					'1'=>'Debit',
					'2'=>'Credit',
					//'3'=>'Bank Transfer',
				];
				$this->set('DebitCredit',$DebitCredit);
			
        if (!$this->request->data)
        {
            if(empty($id))
            {
            $Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
    if(!empty($Journals))
    {
      $JournalsNo = $Journals['Journal']['voucher_no'];
      $Journals_no=$JournalsNo+1;
    }
    else
    {
      $Journals_no=1;
    }
                $currency=$this->SystemParameter->field('value',array('id'=>6));
				$data['JournalVoucher']['currency']=$currency;
				$data['JournalVoucher']['date']=date('d-m-Y');
				$data['JournalVoucher']['cheque_date']=date('d-m-Y');
				$data['JournalVoucher']['journal_no']=$Journals_no;
                 $this->request->data=$data;
            }
            else
            {
$currency=$this->SystemParameter->field('value',array('id'=>6));
				$Journal=$this->JournalVoucher->findByVoucherNo($id);
				//pr($Journal);
				//exit;
				//$Customer=$this->Customer->findByAccountHeadId($Journal['Journal']['credit']);
				$this->request->data=$Journal;
				$this->request->data['JournalVoucher']['date']=date('d-m-Y',strtotime($Journal['JournalVoucher']['date']));
				$this->request->data['JournalVoucher']['journal_no']=$Journal['JournalVoucher']['voucher_no'];
				$this->request->data['JournalVoucher']['currency']=$currency;
				// if($Journal['Journal']['reference_no'])
				// 	$this->request->data['JournalVoucher']['mode_category_ids']=3;
				// elseif($Journal['Journal']['cheque_no'])
				// 	$this->request->data['JournalVoucher']['mode_category_ids']=2;
				// else
				// 	$this->request->data['JournalVoucher']['mode_category_ids']=1;
				$this->request->data['JournalVoucher']['narration']=$Journal['JournalVoucher']['narration'];
				$this->request->data['JournalVoucher']['total_dr']=$Journal['JournalVoucher']['debit_total'];
				$this->request->data['JournalVoucher']['total_cr']=$Journal['JournalVoucher']['credit_total'];
				// $this->request->data['JournalVoucher']['mode']=$Journal['Customer']['id'];
				// $this->request->data['JournalVoucher']['cheque_no']=$Journal['Journal']['cheque_no'];
				// $this->request->data['JournalVoucher']['cheque_date']=date('d-m-Y',strtotime($Journal['Journal']['cheque_date']));
				// $this->request->data['JournalVoucher']['reference_no']=$Journal['Journal']['reference_no'];
				$JournalVoucherItems=$this->JournalVoucherDetail->find('all',array(
					'joins'=>[
						array(
							'table'=>'journal_vouchers',
							'alias'=>'JournalVoucher',
							'type'=>'LEFT',
							'conditions'=>array('JournalVoucherDetail.journal_voucher_id=JournalVoucher.id')
							),
						array(
							'table'=>'account_heads',
							'alias'=>'AccountHead',
							'type'=>'LEFT',
							'conditions'=>array('AccountHead.id=JournalVoucherDetail.account_head_id')
							),
						],
					'conditions'=>['JournalVoucher.voucher_no'=>$id,'JournalVoucherDetail.flag'=>1],
					'fields'=>array(
						'JournalVoucherDetail.*',
						'AccountHead.name',
						'AccountHead.id'
					)
				));
				//pr($JournalVoucherItems);die;
				$this->set('JournalVoucherItems',$JournalVoucherItems);
            }
        }
        else
        {
           $datasource_JournalVoucher = $this->JournalVoucher->getDataSource();
           $datasource_JournalVoucherDetail = $this->JournalVoucherDetail->getDataSource();
           $datasource_Journal = $this->Journal->getDataSource();
			try {
				// if($sale_items)
    //   {
       // pr($sale_items);exit;
        
     // }
			$datasource_JournalVoucher->begin();
			$datasource_JournalVoucherDetail->begin();	
			$datasource_Journal->begin();
			//$data=$this->request->data;
			$data=$this->request->data['JournalVoucher'];
			//pr($data);
			//exit;
           if(!$id){
			   $JV_data=[
          'voucher_no'=>$data['journal_no'],
          'date'=>date('Y-m-d',strtotime($data['date'])),
          'debit_total'=>$data['total_dr'],
          'credit_total'=>$data['total_cr'],
          'narration'=>$data['narration'],
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s'),
          ];
//pr($Sale_data);exit;
          $this->JournalVoucher->create();
          if(!$this->JournalVoucher->save($JV_data))
          {
            $errors = $this->JournalVoucher->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
          $id=$this->JournalVoucher->getLastInsertId();
  
			$user_id=1;
			//$debit=[];
			$cr=0;
		    $dr=0;
		    for ($i=0; $i <count($data['dr_cr']) ; $i++) {
					if($data['dr_cr'][$i]==2){
						$credit[]=$data['acount_head_id'][$i];
						$cr++;
					}else{
						$debit[]=$data['acount_head_id'][$i];
						$dr++;
					}
					
				}
				$credit_id=$credit[0];
				$debit_id=$debit[0];
				// echo "<pre>";print_r($dr);die;
for ($i=0; $i <count($data['dr_cr']) ; $i++) {


	$JournalVoucherDetail_data=[
          'journal_voucher_id'=>$id,
          'account_head_id'=>$data['acount_head_id'][$i],
          'dr_cr'=>$data['dr_cr'][$i],
          'dr_amount'=>$data['amount_dr'][$i],
          'cr_amount'=>$data['amount_cr'][$i],
          'reference'=>$data['reference'][$i]
          ];
          $this->JournalVoucherDetail->create();
          if(!$this->JournalVoucherDetail->save($JournalVoucherDetail_data))
          {
            $errors = $this->JournalVoucherDetail->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
	if($dr>=$cr){
	if($data['dr_cr'][$i]==1){
							$debit=$data['acount_head_id'][$i];
							$credit=$credit_id;
							// break;
							// pr($debit);
						// exit;
						}else{
							$credit=$data['acount_head_id'][$i];
						}
	
		if($data['dr_cr'][$i]==1){
			// pr($debit);
			// exit;
			$debit1=$debit;
			$credit=$credit;
		    // $credit=$data['acount_head_id'][$i];
	//}
            $remarks=$data['reference'][$i];
            $amount=$data['amount_dr'][$i];
            $date=$data['date'];
			$narration=$data['narration'];
	        $cheque_date='00-00-0000';
		    $voucher_no=$data['journal_no'];
			$work_flow='Journal Voucher';
			
          
           $AccountingsController = new AccountingsController;
		   // $function_return=$AccountingsController->AccountHeadEdit($account_head_id,$name,$opening_balance,$date,$description);
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$narration,$reference_no,$cheque_no,$cheque_date);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$last_journal_id=$this->Journal->getLastInsertId();
           
		
          
        }
    }else{
						if($data['dr_cr'][$i]==2){
							$credit=$data['acount_head_id'][$i];
							$debit=$debit_id;
							// break;
							// pr($debit);
						// exit;
						}else{
							$debit=$data['acount_head_id'][$i];
						}
			
						if($data['dr_cr'][$i]==2){
							// exit;
							$debit1=$debit;
							$credit=$credit;
					
							$remarks=$data['reference'][$i];
							$amount=$data['amount_cr'][$i];
							$date=$data['date'];
							$narration=$data['narration'];
							$cheque_date='00-00-0000';
							$voucher_no=$data['journal_no'];
							$work_flow='Journal Voucher';
							// echo "<pre>";echo $credit.' - '.$debit1;
							$branch_id=1;
							$AccountingsController = new AccountingsController;
							// $function_return=$AccountingsController->AccountHeadEdit($account_head_id,$name,$opening_balance,$date,$description);
							$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$narration,$reference_no,$cheque_no,$cheque_date);
							if($function_return['result']!='Success')
								throw new Exception($function_return['message'], 1);
							$last_journal_id=$this->Journal->getLastInsertId();
						
						
						}
					}
        }
    }else{
				// print_r($id);die;
				$JV_data=[
				'date'=>date('Y-m-d',strtotime($data['date'])),
				'debit_total'=>$data['total_dr'],
				'credit_total'=>$data['total_cr'],
				'narration'=>$data['narration'],
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				$JournalVoucher=$this->JournalVoucher->findByVoucherNo($id);
				$this->JournalVoucher->id=$id;
				if(!$this->JournalVoucher->save($JV_data))
				{
					$errors = $this->JournalVoucher->validationErrors;
					foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
					}
				}
				$user_id=1;
				//$debit=[];
				$cr=0;
				$dr=0;
				for ($i=0; $i <count($data['dr_cr']) ; $i++) {
					if($data['dr_cr'][$i]==2){
						$credit[]=$data['acount_head_id'][$i];
						$cr++;
					}else{
						$debit[]=$data['acount_head_id'][$i];
						$dr++;
					}
					
				}
				$credit_id=$credit[0];
				$debit_id=$debit[0];
				// echo "<pre>";print_r($credit_id);die;
				for ($i=0; $i <count($data['dr_cr']) ; $i++) {


					$JournalVoucherDetail_data=[
					'journal_voucher_id'=>$id,
					'account_head_id'=>$data['acount_head_id'][$i],
					'dr_cr'=>$data['dr_cr'][$i],
					'dr_amount'=>$data['amount_dr'][$i],
					'cr_amount'=>$data['amount_cr'][$i],
					'reference'=>$data['reference'][$i]
					];
					$this->JournalVoucherDetail->create();
					if(!$this->JournalVoucherDetail->save($JournalVoucherDetail_data))
					{
						$errors = $this->JournalVoucherDetail->validationErrors;
						foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
						}
					}
					if($dr>=$cr){
						if($data['dr_cr'][$i]==1){
							$debit=$data['acount_head_id'][$i];
							$credit=$credit_id;
							// break;
							// pr($debit);
						// exit;
						}else{
							$credit=$data['acount_head_id'][$i];
						}
			
						if($data['dr_cr'][$i]==1){
							// echo "<pre>";print_r($debit);
							// exit;
							$debit1=$debit;
							$conditions['JournalVoucherDetail.journal_voucher_id']=$id;
							$conditions['JournalVoucherDetail.flag']=1;
							$conditions['JournalVoucherDetail.dr_cr']=2;
							$JournalVoucherDetail=$this->JournalVoucherDetail->find('first',array(
								'conditions'=>$conditions,
								'fields'=>array(
								'JournalVoucherDetail.account_head_id'
								)
							));
							if($JournalVoucherDetail) $credit=$JournalVoucherDetail['JournalVoucherDetail']['account_head_id'];
							else $credit=$credit;
					
							$remarks=$data['reference'][$i];
							$amount=$data['amount_dr'][$i];
							$date=$data['date'];
							$narration=$data['narration'];
							$cheque_date='00-00-0000';
							$voucher_no=$data['journal_no'];
							$work_flow='Journal Voucher';
							// echo "<pre>";echo $amount;
							$branch_id=1;
							$AccountingsController = new AccountingsController;
							// $function_return=$AccountingsController->AccountHeadEdit($account_head_id,$name,$opening_balance,$date,$description);
							$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$narration,$reference_no,$cheque_no,$cheque_date);
							if($function_return['result']!='Success')
								throw new Exception($function_return['message'], 1);
							$last_journal_id=$this->Journal->getLastInsertId();
						
						
						}
					}else{
						if($data['dr_cr'][$i]==2){
							$credit=$data['acount_head_id'][$i];
							$debit=$debit_id;
							// break;
							// pr($debit);
							// exit;
						}else{
							$debit=$data['acount_head_id'][$i];
						}
			
						if($data['dr_cr'][$i]==2){
							// echo "<pre>";print_r($debit);
							// exit;
							$credit=$credit;
							$conditions['JournalVoucherDetail.journal_voucher_id']=$id;
							$conditions['JournalVoucherDetail.flag']=1;
							$conditions['JournalVoucherDetail.dr_cr']=1;
							$JournalVoucherDetail=$this->JournalVoucherDetail->find('first',array(
								'conditions'=>$conditions,
								'fields'=>array(
								'JournalVoucherDetail.account_head_id'
								)
							));
							if($JournalVoucherDetail) $debit=$JournalVoucherDetail['JournalVoucherDetail']['account_head_id'];
							else $debit1=$debit;
					
							$remarks=$data['reference'][$i];
							$amount=$data['amount_cr'][$i];
							$date=$data['date'];
							$narration=$data['narration'];
							$cheque_date='00-00-0000';
							$voucher_no=$data['journal_no'];
							$work_flow='Journal Voucher';
							// echo "<pre>";echo $amount;
							$branch_id=1;
							$AccountingsController = new AccountingsController;
							// $function_return=$AccountingsController->AccountHeadEdit($account_head_id,$name,$opening_balance,$date,$description);
							$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$narration,$reference_no,$cheque_no,$cheque_date);
							if($function_return['result']!='Success')
								throw new Exception($function_return['message'], 1);
							$last_journal_id=$this->Journal->getLastInsertId();
						
						
						}
					}
				}
			}     
			$return['result']='Success';
			$datasource_JournalVoucher->commit();
			$datasource_JournalVoucherDetail->commit();
			$datasource_Journal->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_JournalVoucher->rollback();
			$datasource_JournalVoucherDetail->rollback();
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
			$this->redirect( Router::url( $this->referer(), true ) );
           
        }
    }
}