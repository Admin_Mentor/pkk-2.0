<?php
App::uses('Controller', 'Controller');
App::uses('CakeTime', 'Utility');
date_default_timezone_set('Asia/Kolkata');
class AppController extends Controller {
	public $uses=[
		'Type',
		'MasterGroup',
		'Group',
		'SubGroup',
		'AccountHead',
		'Journal',
		'BankDetail',
		'Profile',
		'Module',
		'Menu',
		'UserRole',
		'UserMenuPermission',
		'StaffDocument',
		'DocumentType',
		'VehicleNo',
		'Staff'
	];
	// public $components = array('Flash','Session');
	public $components = array(
		'Flash','Session',
	);
	public $Global_Var_Profile;
	public $User_Menu_Permissions;
	function beforeFilter()
	{
		$user_id=$this->Session->read('User.id');
		$user_role_id=$this->Session->read('UserRole.id');
	    $view_flag=1;
	    $Profile=$this->Profile->find('first');
		$this->Global_Var_Profile=$Profile;
		$this->set('Profile',$Profile);

		$controller=$this->request->params['controller'];
	    $action=$this->request->params['action'];
	    $menu_action=$controller.'/'.$action;
	    $this->set('menu_action',$menu_action);
		if($controller!='Settings' && $action!='userpass'){
			$userid = $this->Session->read('User.id');
			if(!isset($userid))
			{
				//$this->Session->setFlash(__('Please Login!.'));
				$action=$this->request->params['action'];
				if($controller!='Settings' && $action!='login')
				{
					return $this->redirect(array('controller'=>'User','action' => 'login'));	
				}
			}
			else
			{
				$mode_catagory=[
					'1'=>'Cash',
					'2'=>'Bank',
				];
				$this->set('mode_catagory',$mode_catagory);
				$AccountHead_cash=$this->AccountHead->find('list',array('conditions'=>array('acc_sub_group_id'=>5)));
				$this->set('Mode',$AccountHead_cash);
			}
		}
		$UserMenuPermissions=array(
			'view_permission'=>0,
			'edit_permission'=>0,
			'create_permission'=>0,
			'delete_permission'=>0,
			);
		$menu_id=$this->Menu->field('Menu.id',array('action'=>$menu_action));
		if(!empty($menu_id) && !empty($user_role_id))
	    {
	      $UserMenuPermission=$this->UserMenuPermission->findByUserRoleIdAndMenuId($user_role_id,$menu_id);

	      if(!empty($UserMenuPermission))
	      {
	      	
	        if($UserMenuPermission['UserMenuPermission']['view_permission']==0)
	        {
	          $view_flag=0;
	        }
	      }
	      else
	      {
	        $view_flag=0;
	      }
	    }
	    if(!empty($UserMenuPermission))
	    {
	    	$UserMenuPermissions=$UserMenuPermission['UserMenuPermission'];
	    }
	    $this->User_Menu_Permissions=$menu_action;
	    $this->set('create_permission_flag',$UserMenuPermissions['create_permission']);
		$this->set('edit_permission_flag',$UserMenuPermissions['edit_permission']);
		$this->set('delete_permission_flag',$UserMenuPermissions['delete_permission']);
	    if($view_flag==0)
	    {
	      $this->Session->setFlash("Permission denied");
	      return $this->redirect( Router::url( $this->referer(), true ) );
	    }
	    $conditions_menu=array('UserMenuPermission.full_permission'=>1,'UserMenuPermission.user_role_id'=>$user_role_id);
	    $modules=$this->Module->find('all',array(
					'fields' => array('Module.name','Module.id'),
				));	
	    $module=[];
	    foreach ($modules as $key1 => $value1) {
	    	$conditions_menu['Menu.module_id']=$value1['Module']['id'];
	    	$conditions_menu['Menu.status']=1;
            $menu_new=$this->UserMenuPermission->find('all',array(
					'joins'=>array(
						array(
							'table'=>'menus',
							'alias'=>'Menu',
							'type'=>'INNER',
							'conditions'=>array('UserMenuPermission.menu_id=Menu.id')
						),
					),
					 'order' => array('Menu.id ASC'),
					'conditions'=>$conditions_menu,
					'fields' => array('Menu.action','Menu.name'),
				));	
            if($menu_new)
            {
            foreach ($menu_new as $key2 => $value2) {
            	$module[$value1['Module']['name']][$value2['Menu']['name']]=$value2['Menu']['action'];
            }
            }
	    }
	    $this->set('moduleaction',$module);
	    $notification_condition=[];
	    $notification_condition['StaffDocument.expiry_date <=']=date('Y-m-d',strtotime('+29 day'));
	    $StaffDocument=$this->StaffDocument->find('all',array(
			'conditions'=>$notification_condition,
			'fields'=>array(
				'StaffDocument.*',
			)
		));
		$document=[];
		foreach ($StaffDocument as $key_notfi => $value_notfi) {
			   $now = time();
				$expected_days_diff=strtotime($value_notfi['StaffDocument']['expiry_date'])-$now;
                $diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
                $due=0;
                if($diff_day>0)
                {
                	$due=$diff_day;
                	$expire="will expire soon";
                	$single['time']=$due.' days remaining';
                }
                 else if($diff_day<0)
                {
                $expire="Expired";
                 $single['time']=-$diff_day.' days ago';

                }
                 else if($diff_day==0)
                {
                  $expire="Will Expire Today";
                  $single['time']="Today";
                }
                else
                {
                 $due=0;
                 $expire="";
                 $single['time']="";
                }
                if($value_notfi['StaffDocument']['document_type_id']==1)
				{
				 $name=$this->Staff->field('Staff.name',array('Staff.id'=>$value_notfi['StaffDocument']['staff_id']));

				}
				else if($value_notfi['StaffDocument']['document_type_id']==2)
				{
				$name=$this->VehicleNo->field('number',array('VehicleNo.id'=>$value_notfi['StaffDocument']['staff_id']));
				}
				else
				{
				$name="COMMON";
				}
                $document_type=$this->DocumentType->findById($value_notfi['StaffDocument']['document_type_id']);
			$single['notification']=$document_type['DocumentType']['name'].' of '.$name.' '.$expire;
			$document[]=$single;
			
		}
		 $this->set('document',$document);
		
	}
}