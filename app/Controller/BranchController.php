<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
/**
* Executive Controller
*/
class BranchController extends AppController {
public $uses=[
'Type',
'MasterGroup',
'Group',
'SubGroup',
'AccountHead',
'Executive',
'Location',
'Route',
'Customer',
'Warehouse',
'Branch',
'BranchWarehouseMapping',
];
public $components = array('RequestHandler');
public function AddBranch()
{
	$BranchAll=$this->Branch->find('all');
    $this->set(compact('BranchAll'));
    $Warehouse=$this->Warehouse->find('list',array(
	'conditions'=>array('Warehouse.warehouse_id'=>"",'Warehouse.id !='=>1),
	'fields'=>array('id','name'),
	'order'=>array('name ASC'),
	));
//$Warehouse=$this->Warehouse->find('list');
$this->set(compact('Warehouse'));
$Location=$this->Route->find('list',
array("joins"=>array(
),
));
$this->set(compact('Location'));
if($this->request->data)
{
$data=$this->request->data['Branch'];
$datasource_Branch = $this->Branch->getDataSource();
$datasource_BranchWarehouseMapping = $this->BranchWarehouseMapping->getDataSource();

try {

$name=strtoupper(trim($data['name']));
$Branch=$this->Branch->find('first',array(
'conditions'=>array('Branch.name'=>$name)
));
if(empty($Branch))
{
$branch_data=[
'name'=>trim(strtoupper($data['name'])),
'warehouse_id'=>$data['warehouse_id'],
'mobile'=>$data['mobile'],
'created_at'=>date('Y-m-d H:i:s'),
'updated_at'=>date('Y-m-d H:i:s'),
];
$datasource_Branch->begin();
$this->Branch->create();
if(!$this->Branch->save($branch_data))
{
$errors = $this->Branch->validationErrors;
foreach ($errors as $key => $value) {
throw new Exception($value[0], 1);
}
$branch_id=$this->Branch->getLastInsertId();
}
$datasource_Branch->commit();
$branch_id=$this->Branch->getLastInsertId();
foreach ($data['location_id'] as $key => $value) {
$warehouse_id=$this->BranchWarehouseMapping->find('first',array('conditions'=>array('BranchWarehouseMapping.warehouse_id'=>$value)));
if(empty($warehouse_id))
{
$mapping_data=[
'branch_id'=>$branch_id,
'warehouse_id'=>$value,
];
$datasource_BranchWarehouseMapping->begin();
$this->BranchWarehouseMapping->create();
if(!$this->BranchWarehouseMapping->save($mapping_data))
{
$errors = $this->BranchWarehouseMapping->validationErrors;
foreach ($errors as $key => $value) {
throw new Exception($value[0], 1);
}
}
$datasource_BranchWarehouseMapping->commit();
}
}
$return['result']='success';
$this->Session->setFlash(__($return['result']));
}
else
{
$return['result']='Branch Already Inserted';
$this->Session->setFlash(__($return['result']));
}
} catch (Exception $e) {
$return['result']=$e->getMessage();
$datasource_Branch->rollback();
// $datasource_Staff->rollback();
$datasource_BranchWarehouseMapping->rollback();
}
$this->redirect( Router::url( $this->referer(), true ) );
}
}
public function get_branch_warehouse_ajax($id)
{
	$return=[
			'status'=>'Empty',
			'BranchWarehouse'=>[],
		];
		$id_explode=explode(',', $id);
		if($id_explode)
		{
			foreach ($id_explode as $key => $value) 
			{
			$warehouse_id=$this->BranchWarehouseMapping->find('first',array('conditions'=>array('BranchWarehouseMapping.warehouse_id'=>$value)));
			if($warehouse_id)
			{
				$return['BranchWarehouse'][]=$value;
				$return['status']="This Warehouse Already Used";
			}
			}
		}
		echo json_encode($return);
		exit;
}
public function get_branch_Warehouse_ajax_edit($id=null,$branch_id=null)
{
	$return=[
			'status'=>'Empty',
			'BranchWarehouse'=>[],
		];
		$id_explode=explode(',', $id);
		if($id!="" && $branch_id!="")
		{
		if($id_explode)
		{
			foreach ($id_explode as $key => $value) 
			{
			$warehouse_id=$this->BranchWarehouseMapping->find('first',array('conditions'=>array('BranchWarehouseMapping.branch_id !='=>$branch_id,'BranchWarehouseMapping.warehouse_id'=>$value)));
			if($warehouse_id)
			{
				$return['BranchWarehouse'][]=$value;
				$return['status']="This Warehouse Already Used";
			}
			}
		}
	}
		echo json_encode($return);
		exit;
}
public function get_branch_details_ajax($id)
	{
		$return=[
			'status'=>'Empty',
			'Branch'=>[],
			'BranchRoute'=>[],
		];
		if(isset($id))
		{
			$Branch=$this->Branch->findById($id);
			$return['Branch']=$Branch['Branch'];
			$BranchRoute=$this->BranchWarehouseMapping->find('list',array(
				'conditions'=>array('BranchWarehouseMapping.branch_id'=>$id,),

				'fields'=>'BranchWarehouseMapping.warehouse_id'
			));
			$return['BranchRoute']=$BranchRoute;

		}
		echo json_encode($return);
		exit;
	}

public function EditBranch()
{
	if($this->request->data)
	{
		$data=$this->request->data['Branch'];
		$datasource_Branch = $this->Branch->getDataSource();
		$datasource_BranchWarehouseMapping = $this->BranchWarehouseMapping->getDataSource();
		try {

			$branch_data=[
'name'=>trim(strtoupper($data['name'])),
'warehouse_id'=>$data['warehouse_id'],
'mobile'=>$data['mobile'],
'updated_at'=>date('Y-m-d H:i:s'),
];
          $datasource_Branch->begin();
 			$this->Branch->id=$data['id'];
			if(!$this->Branch->save($branch_data))
			{
				$errors = $this->Branch->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
				$datasource_Branch->commit();
			$branch_id = $data['id']; 
			if(!$this->BranchWarehouseMapping->deleteAll(array('BranchWarehouseMapping.branch_id' => $branch_id)))
			throw new Exception("Cant Delete This Warehouse", 1);
			foreach ($data['location_id'] as $key => $value) {
				$warehouse_id=$this->BranchWarehouseMapping->find('first',array('conditions'=>array('BranchWarehouseMapping.warehouse_id'=>$value)));
         if(empty($warehouse_id))
            {
					$mapping_data=[
										'branch_id'=>$branch_id,
										'warehouse_id'=>$value,
								  ];
		$datasource_BranchWarehouseMapping->begin();
			$this->BranchWarehouseMapping->create();
			if(!$this->BranchWarehouseMapping->save($mapping_data))
			{
				$errors = $this->BranchWarehouseMapping->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$datasource_BranchWarehouseMapping->commit();
		}
	    }
			$return['result']='updated';
					$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
			$datasource_BranchWarehouseMapping->rollback();

			$datasource_Branch->rollback();
		}

		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function get_branch_warehouse($id)
{
	$conditions=array();
    $conditions['BranchWarehouseMapping.branch_id']=$id;
	$branch_id_List = $this->BranchWarehouseMapping->find('all',array('fields'=>'BranchWarehouseMapping.branch_id,Warehouse.name,BranchWarehouseMapping.id',
		'conditions'=>$conditions));
	
	$data['row']='';
	$first_date = date('Y-m-d',strtotime('first day of this month'));
	$i=1;
	foreach ($branch_id_List as $key => $value) {
		$data['row']= $data['row'].'<tr class="blue-pd">';
		$data['row']= $data['row'].'<td>'.$i.'</td>';
		$data['row']= $data['row'].'<td>'.$value["Warehouse"]["name"].'</td>';
		$data['row']= $data['row'].'<td><a data-id="'.$value["BranchWarehouseMapping"]["id"].'" class="del-warehouse"><i class="fa fa-trash blue-col blue-col"></i></a></td>';
	
		$data['row']= $data['row'].'</tr>';
	$data['branch_id'] = $value['BranchWarehouseMapping']['branch_id'];
	$i++;
}
	echo json_encode($data);
	exit;
}
public function DeleteBranchwarehouse($id)
{
	try {
		if(!$this->BranchWarehouseMapping->delete($id))
			throw new Exception("Cant Delete This Warehouse", 1);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	$this->Session->setFlash(__($return['result']));
	echo json_encode($return);
	exit;
	
}
}