<?php
App::uses('AppController', 'Controller');
class BrandController extends AppController {
	public $uses=array(
		'Brand',
		'Service',
		'ProductType',
		'ProductTypeBrandMapping',
		'Product',
		);
	public function add_brand_name_ajax()
	{
		try {
			$data=$this->request->data;
			$userid = 1;;
			if(empty($userid))
				throw new Exception("Please Login first", 1);
			$brand_name=strtoupper(trim($data['brand_name']));
			if(empty($brand_name))
				throw new Exception("Empty brand_name", 2);
			$product_type_id=$data['product_type_id'];
			if(empty($product_type_id))
				throw new Exception("Empty product_type_id", 3);
			$discount_percentage=$data['discount_percentage'];
			$Brand=$this->Brand->find('first',array('conditions'=>array('Brand.name'=>$brand_name)));
			if(empty($Brand))
			{
				$data=array(
					'name'=>$brand_name,
					'discount_percentage'=>$discount_percentage,
					'created_at'=>date('Y-m-d H:i:s'),
					'updated_at'=>date('Y-m-d H:i:s'),
					);
				$this->Brand->create();
				if(!$this->Brand->save($data))
					throw new Exception("Error Processing In Brand Creation", 4);
				$id=$this->Brand->getLastInsertId();
				$Brand=$this->Brand->findById($id);
				$return['option']='<option value="'.$Brand['Brand']['id'].'">'.$Brand['Brand']['name'].'</option>';
				$return['id']=$Brand['Brand']['id'];
				$return['result']='Success';
			}
			$brand_id=$Brand['Brand']['id'];
			$ProductTypeBrandMapping=$this->ProductTypeBrandMapping->find('first',array(
				'conditions'=>array(
					'product_type_id'=>$product_type_id,
					'brand_id'=>$brand_id,
					)
				));
			if(empty($ProductTypeBrandMapping))
			{
				$value=array(
					'product_type_id'=>''.$product_type_id.'',
					'brand_id'=>''.$brand_id.'',
					);
				$this->ProductTypeBrandMapping->create();
				if(!$this->ProductTypeBrandMapping->save($value))
					throw new Exception("Error Processing In ProductTypeBrandMapping Creation", 1);
				$return['result']='Success';
				$return['option']='<option value="'.$Brand['Brand']['id'].'">'.$Brand['Brand']['name'].'</option>';
				$return['id']=$brand_id;
			}
			else
			{
				$return['id']=$brand_id;
				$return['result']='Already Added';
			}
		} catch (Exception $e) {
			$return['result']='error';
			$return['message']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function get_brand_name_ajax($id=null)
	{
		$return['result']='Error';
		 $conditions=array();
  if(!empty($id))
  {
    $conditions['ProductTypeBrandMapping.product_type_id']=$id;
  }
		$return['option']='<option value="">SELECT</option>';
		$return['option']=$return['option'].'<option value="G">GENERAL</option>';
		$ProductTypeBrandMapping = $this->ProductTypeBrandMapping->find('all', array(
			'conditions' =>$conditions ,
			// 'conditions' => array('ProductTypeBrandMapping.product_type_id' => $id),
			'order'=>array('Brand.name ASC'),
			'fields' => array(
				'Brand.id',
				'Brand.name',
				)
			)
		);
		foreach ($ProductTypeBrandMapping as $key => $value) {
			$return['result']='Success';
			$return['option']=$return['option'].'<option value='.$value['Brand']['id'].'>'.$value['Brand']['name'].'</option>';
		}
		echo json_encode($return);
		exit;
	}
	public function index()
	{
		$this->set('ProductType',$this->ProductType->find('list',array('fields'=>array('id','name'),'order'=>array('name ASC'),)));
		$brand = $this->Brand->find('list', array('fields' => array('id','name')));
		$this->set('brand',$brand);
		$this->Brand->unbindModel(array('hasMany' => array('ProductTypeBrandMapping','Product')));
		$brands = $this->Brand->find('all', array(
			// "joins" => array(
	  //       array(
	  //         "table" => 'product_type_brand_mappings',
	  //         "alias" => 'ProductTypeBrandMapping',
	  //         "type" => 'inner',
	  //         "conditions" => array('ProductTypeBrandMapping.brand_id=Brand.id'),
	  //         ),
	  //       array(
	  //         "table" => 'product_types',
	  //         "alias" => 'ProductType',
	  //         "type" => 'inner',
	  //         "conditions" => array('ProductTypeBrandMapping.product_type_id=ProductType.id'),
	  //         ),
   //      	),
			'fields' => array(
				'Brand.id',
				'Brand.name',
				//'ProductType.id',
				//'ProductType.name',
				),
			));
		$this->set('brands',$brands);
	}
	public function check_brand_ajax(){
		$brand=$this->request->data['brand'];
 // pr($brand);exit;
		$result=$this->Brand->find('all',array('conditions'=>array('name'=>$brand)));
		if(empty($result)){
			echo "No";
		}
		else{
			echo "Yes";
		}
		exit;
	}
	public function brand_type_search(){
		$data=$this->request->data;
		$conditions=[];
		if(!empty($data['brand_type']))
		{
			$conditions['Brand.id']=$data['brand_type'];
		}
		$Brands=$this->Brand->find('all',array(
			'conditions'=>$conditions,
			));
		$return['row']='';
		$return['result']='Empty';
		if(!empty($Brands))
		{
			$return['result']='Success';
			foreach( $Brands as $brand_list )
			{
				$return['row'].='<tr class="blue-pd">';
				$return['row'].='<td style="display:none" class="Brand_id">'.$brand_list["Brand"]["id"].'</td>';
				$return['row'].='<td class="brand_name">'.$brand_list["Brand"]["name"].'</td>';
				$return['row'].='<td><a href="#"><i class="fa fa-edit edit_new_btn ad-mar td_leted edit_dt"> </i></a></td>';
				$return['row'].='<td><a href="#"><i class="fa fa-trash ad-mar brand_delete"></i></a></td>';
				$return['row'].='</tr>';
			}
		}
		echo json_encode($return);
		exit;
	}
	public function delete($id)
	{
		$Product=$this->Product->findByBrandNameIdFk($id);
		if(empty($Product))
		{
			if($this->Brand->delete($id))
			{
				$this->Flash->set(__('Brand deleted.',h($id)));
				$this->redirect(array('controller'=>'Brand','action'=>'index'));
			}
		}
		else
		{
			$this->Flash->set(__('It Is Used'));
			$this->redirect(array('controller'=>'Brand','action'=>'index'));
		}
	}
	public function brand_add_ajax()
	{
		try {
			$data=$this->request->data['Brand'];
			$userid = 1;
			if(empty($userid))
				throw new Exception("Please Login first", 1);
			$name=strtoupper(trim($data['name']));
			if(empty($name))
				throw new Exception("Empty name", 2);
			// $product_type_id=$data['product_type_id'];
			// if(empty($product_type_id))
			// 	throw new Exception("Empty product_type_id", 3);
			$Brand=$this->Brand->find('first',array('conditions'=>array('Brand.name'=>$name)));
			if(empty($Brand))
			{
				$Table_data=array(
					'name'=>trim(strtoupper($data['name'])),
					'created_at'=>date('Y-m-d H:i:s'),
					'updated_at'=>date('Y-m-d H:i:s'),
					// 'code'=>trim(strtoupper($data['code'])),
					);
				$this->Brand->create();
				if(!$this->Brand->save($Table_data))
				{
					$errors = $this->Brand->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$id=$this->Brand->getLastInsertId();
				$Brand=$this->Brand->findById($id);
			}
			$brand_id=$Brand['Brand']['id'];
			$return['result']='Success';
			// $ProductTypeBrandMapping=$this->ProductTypeBrandMapping->find('first',array(
			// 	'conditions'=>array(
			// 		'product_type_id'=>$product_type_id,
			// 		'brand_id'=>$brand_id,
			// 		)
			// 	));
			// if(empty($ProductTypeBrandMapping))
			// {
			// 	$value=array(
			// 		'product_type_id'=>''.$product_type_id.'',
			// 		'brand_id'=>''.$brand_id.'',
			// 		);
			// 	$this->ProductTypeBrandMapping->create();
			// 	if(!$this->ProductTypeBrandMapping->save($value))
			// 		throw new Exception("Error Processing In ProductTypeBrandMapping Creation", 1);
			// 	$return['result']='Success';
			// }
			// else
			// {
			// 	$return['key']=$Brand['Brand']['id'];
			// 	$return['result']='Already Added';
			// }
			$return['key']=$Brand['Brand']['id'];
			$return['value']=$Brand['Brand']['name'];
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function brand_delete_ajax($id)
	{
		try {
			$Product=$this->Product->findByBrandId($id);
			if($Product)
				throw new Exception("This is Used In ".$Product['Product']['name'], 1);
			$ProductTypeBrandMapping=$this->ProductTypeBrandMapping->find('all',array(
				'conditions'=>array('brand_id'=>$id),
				));
			foreach ($ProductTypeBrandMapping as $key => $value) {
				$ProductTypeBrandMapping_id=$value['ProductTypeBrandMapping']['id'];
				if(!$this->ProductTypeBrandMapping->delete($ProductTypeBrandMapping_id))
					throw new Exception("Error Processing Request", 1);
			}
			if(!$this->Brand->delete($id))
				throw new Exception("Error Processing Request While delete", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function brand_get_ajax($id)
	{
		try {
			$Brand=$this->Brand->findById($id);
			if(!$Brand)
				throw new Exception("Empty Brand", 1);
			$return['data']=$Brand['Brand'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function brand_edit_ajax()
	{
		try {
			$data=$this->request->data;
			$Table_data=array(
				'name'=>trim(strtoupper($data['brand_name'])),
				'updated_at'=>date('Y-m-d H:i:s'),
				);
			$this->Brand->id=$data['brand_id'];
			if(!$this->Brand->save($Table_data))
			{
				$errors = $this->Brand->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$Brand=$this->Brand->read();
			$return['key']=$Brand['Brand']['id'];
			$return['value']=$Brand['Brand']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
}