<?php

App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');

class CustomerController extends AppController {

    public $uses = array(
        'ProductType',
        'Unit',
        'Product',
        'Stock',
        'Bill',
        'CustomerType',
        'Customer',
        'AccountHead',
        'State',
        'Route',
        'CustomerGroup',
        'Executive',
        'Sale',
        'Journal',
        'SaleItem',
        'Division',
        'SalesReturnItem',
        'CustomerDiscount',
        );

    public function get_customer_by_customer_type_ajax($customer_type_id) {
        try {

            $user_branch_id=$this->Session->read('User.branch_id');
            $conditions =array();
            if($user_branch_id)
            {
            
            }
            if($customer_type_id!=0){
                $conditions['Customer.customer_type_id'] = $customer_type_id;
            }
           
                $AccountHead_list = $this->AccountHead->find('all', [
                'joins' => [
                [
                "table" => "customers",
                "alias" => "Customer",
                "type" => "inner",
                "conditions" => ['AccountHead.id=Customer.account_head_id'],
                ],
                [
                "table" => "routes",
                "alias" => "Route",
                "type" => "LEFT",
                "conditions" => ['Route.id=Customer.route_id'],
                ],

                ],
                'conditions' => $conditions,
                'fields' => [
                'AccountHead.id',
                'AccountHead.name',
                'Customer.code',
                ],
                ]);
           
            $AccountHead =[];

            foreach ($AccountHead_list as $key => $value) {
                //$AccountHead[0]='SELECT';
                $AccountHead[$value['AccountHead']['id']] =$value['Customer']['code'].' - '.$value['AccountHead']['name'];

            }
            if ($AccountHead) {
                
                $return['options'] = $AccountHead;
                $return['result'] = 'Success';
            } else {
                throw new Exception("Empty", 1);
            }
        } catch (Exception $e) {
            $return['result'] = $e->getMessage();
        }
        echo json_encode($return);
        exit;
    }

    public function get_customer_address_ajax($id) {
        try {
            $from_date=date('d-m-Y',strtotime('-1 month'));
            $to_date=date('d-m-Y');
            $Customer = $this->Customer->findByAccountHeadId($id);
            $AccountHead = $this->AccountHead->findById($id);
            if (!$Customer)
                throw new Exception("Empty", 1);
            $AccountingsController = new AccountingsController;
            $function_value_return=$AccountingsController->General_Journal_Debit_N_Credit_function($id);
            $total = $AccountHead['AccountHead']['opening_balance'];
            $Recieved = 0;
            $Debit_N_Credit_function=$AccountingsController->General_Journal_Debit_N_Credit_function($id);
            if(!empty($Debit_N_Credit_function)){
                $total+=$Debit_N_Credit_function['debit'];
                $Recieved+=$Debit_N_Credit_function['credit'];
            }


            $return['balance'] = $total - $Recieved;
            $return['address'] = $Customer['Customer']['place'];
             $return['division'] = $Customer['Division']['name'];
            $return['credit_limit'] = $Customer['Customer']['credit_limit'];
            $return['state_id'] = $Customer['Customer']['state_id'];
            $return['customertype'] = $Customer['Customer']['customer_type_id'];
            $return['result'] = 'Success';
            $return['result'] = 'Success';
        } catch (Exception $e) {
            $return['result'] = $e->getMessage();
        }
        echo json_encode($return);
        exit;
    }

    public function add_state_ajax() {
        $country_id = $this->Global_Var_Profile['State']['country_id'];
        $return = ['result' => 'Empty'];
        try {
            $data = $this->request->data;
            $name = $data['name'];
            $code = $data['code'];
            $state_date = array(
                'country_id' => $country_id,
                'name' => strtoupper($name),
                'code' => strtoupper($code),
                );
            $this->State->create();
            if (!$this->State->save($state_date))
                throw new Exception("Error In State Create", 1);
            $return['result'] = 'Success';
            $State_id = $this->State->getLastInsertId();
            $State = $this->State->findById($State_id);
            $return['key'] = $State['State']['id'];
            $return['value'] = $State['State']['name'];
        } catch (Exception $e) {
            $return['result'] = $e->getMessage();
        }
        echo json_encode($return);
        exit;
    }

    public function add_route_ajax() {

        $return = ['result' => 'Empty'];

        try {
            $data = $this->request->data;
            $name = $data['name'];
            $code = $data['code'];
             $cashname=$name.$code.'_CASH';
                $sub_group_id =1;
            $name1 = $cashname;
            $opening_balance = 0;
            $date = date('Y-m-d');
            $description = "";
              $AccountingsController = new AccountingsController;
            $function_return = $AccountingsController->AccountHeadCreate($sub_group_id, $name1,$opening_balance, $date, $description);

        if ($function_return['result'] != 'Success')
            throw new Exception($function_return['message']);
        $CashAccountHead_id = $this->AccountHead->getLastInsertId();

            $route_data = array(
                'name' => strtoupper($name),
                'code' => strtoupper($code),
                 'account_head_id'=>$CashAccountHead_id,
                );
             
            $this->Route->create();
            if (!$this->Route->save($route_data))
                throw new Exception("Error In Route Create", 1);
            $return['result'] = 'Success';
            $Route_id = $this->Route->getLastInsertId();
            $Route = $this->Route->findById($Route_id);
            $return['key'] = $Route['Route']['id'];
            $return['value'] = $Route['Route']['name'].' '.$Route['Route']['code'];
        } catch (Exception $e) {
            $return['result'] = $e->getMessage();
        }

        echo json_encode($return);
        exit;
    }

    public function add_customer_group_ajax() {

        $return = ['result' => 'Empty'];

        try {
            $data = $this->request->data;
            $name = $data['name'];

            $route_date = array(
                'name' => strtoupper($name),
                );

            $this->CustomerGroup->create();
            if (!$this->CustomerGroup->save($route_date))
                throw new Exception("Error In Route Create", 1);
            $return['result'] = 'Success';
            $CustomerGroup_id = $this->CustomerGroup->getLastInsertId();
            $CustomerGroup = $this->CustomerGroup->findById($CustomerGroup_id);
            $return['key'] = $CustomerGroup['CustomerGroup']['id'];
            $return['value'] = $CustomerGroup['CustomerGroup']['name'];
        } catch (Exception $e) {
            $return['result'] = $e->getMessage();
        }

        echo json_encode($return);
        exit;
    }

    public function get_state_code_ajax($id) {
        $return = ['result' => 'Empty'];
        $State = $this->State->findById($id);
        if ($State) {
            $return['result'] = 'Success';
            $return['code'] = $State['State']['code'];
        }
        echo json_encode($return);
        exit;
    }

    public function add_ajax() {
        $sub_group_id = 3;
        $user_id = 1;
        $datasource_Customer = $this->Customer->getDataSource();
        $datasource_AccountHead = $this->AccountHead->getDataSource();
        $datasource_CustomerDiscount = $this->CustomerDiscount->getDataSource();
        try {
            $datasource_Customer->begin();
            $datasource_AccountHead->begin();
            $datasource_CustomerDiscount->begin();
            $Customer_data = $this->request->data['Customer'];
            $customer_type_id = $Customer_data['customer_type_id'];
            $customer_group_id = $Customer_data['customer_group_id'];
            $opening_balance = $Customer_data['opening_balance'];
            $route_id = $Customer_data['route_id'];
            $division_id = $Customer_data['division_id'];
            if ($customer_type_id == '')
                throw new Exception("Empty Customer Type", 1);
            // if ($customer_group_id == '')
            //     throw new Exception("Empty Customer Group", 1);
            // if ($route_id == '')
            //     throw new Exception("Empty Route", 1);
            if ($division_id == '')
                $division_id=1;
                //throw new Exception("Empty Division", 1);
            if ($opening_balance == '')
                throw new Exception("Empty Opening Balance", 1);

            $name = trim($Customer_data['name']);
            if (!$name)
                throw new Exception("Empty Customer Name", 1);
            $TypeCode = $this->CustomerType->field(
                'CustomerType.code',
                array('CustomerType.id ' => $Customer_data['customer_type_id']));
            $code = $TypeCode;
            $CustomerRow = $this->Customer->find('first', array(
                'order' => array('Customer.id' => 'DESC') ));
            if(!empty($CustomerRow))
            {
                $code = $CustomerRow['Customer']['code']+1;
            }
            else{
                $code = 1001;
            }
            $date = date('Y-m-d');
            $place = $Customer_data['place'];
            $email = $Customer_data['email'];
            
            $mobile = $Customer_data['mobile'];
            $description = $Customer_data['description'];
            $route_id = $Customer_data['route_id'];
            
            $contact_person = $Customer_data['contact_person'];
            $credit_period = $Customer_data['credit_period'];
            $credit_limit = $Customer_data['credit_limit'];
            $vat_no = $Customer_data['vat_no'];
            $customer_name_arabic_text=trim(strtoupper($Customer_data['arabic_name']));
            $AccountingsController = new AccountingsController;
            $function_return = $AccountingsController->AccountHeadCreate($sub_group_id, $name, $opening_balance, $date, $description);
            if ($function_return['result'] != 'Success')
                throw new Exception($function_return['message']);
            $AccountHead_id = $this->AccountHead->getLastInsertId();
            $Customer_date = [
             'state_id' => $Customer_data['state_id'],
            'account_head_id' => $AccountHead_id,
            'customer_type_id' => $customer_type_id,
            'arabic_name'=>$customer_name_arabic_text,
            'place' => $place,
            'route_id' => $route_id,
            'division_id' => $division_id,
            'customer_group_id' => $customer_group_id,
            'code' => $code,
            'email' => $email,
            'mobile' => $mobile,
            'contact_person' => $contact_person,
            'credit_limit' => $credit_limit,
            'credit_period' => $credit_period,
            'vat_no' => $vat_no,
            'created_by' => $user_id,
            'modified_by' => $user_id,
            'created_at' => date('Y-m-d H:i:s', strtotime($date)),
            'updated_at' => date('Y-m-d H:i:s', strtotime($date)),
            ];
            $this->Customer->create();
            if (!$this->Customer->save($Customer_date))
                throw new Exception("Error Customer Creation", 1);
            $return['result'] = 'Success';
            $last_inserted_customer = $this->Customer->findById($this->Customer->getLastInsertId());
            if(isset($Customer_data['products'])){
                for ($i=0; $i <count($Customer_data['products']); $i++) { 
                  $discount_details=array(
                    'account_head_id'=>$last_inserted_customer['AccountHead']['id'],
                    'product_id'=>$Customer_data['products'][$i],
                    'selling_rate'=>$Customer_data['selling_rates'][$i],
                    'created_at'=>date('Y-m-d'),
                    'updated_at'=>date('Y-m-d')
                  );
                   $this->CustomerDiscount->create();
                if(!$this->CustomerDiscount->save($discount_details))
                    throw new Exception("Error Processing Request", 1);
                }
               
                    
            }
            $return['key'] = $last_inserted_customer['AccountHead']['id'];
            $return['value'] = $last_inserted_customer['AccountHead']['name'].' '.$last_inserted_customer['Customer']['code'];
            $datasource_Customer->commit();
            $datasource_AccountHead->commit();
            $datasource_CustomerDiscount->commit();
        } catch (Exception $e) {
            $datasource_Customer->rollback();
            $datasource_AccountHead->rollback();
            $datasource_CustomerDiscount->rollback();
            $return['result'] = $e->getMessage();
        }
        echo json_encode($return);
        exit;
    }

    public function customer_check_ajax() {
        $customertype = $this->request->data['customer_type'];
        $result = $this->Customer->find('all', array('conditions' => array('custom_type_id_fk' => $customertype)));
        echo '<option value="">Select</option>';
        foreach ($result as $key => $value) {
            echo '<option value=' . $value['Customer']['id'] . '>' . $value['Customer']['party_name'] . '</option>';
        }
        exit;
    }

    public function check_customer_ajax() {
        $customer_name = $this->request->data['customer_name'];
        $customer_type = $this->request->data['customer_type'];
        $result = $this->Customer->find('all', array('conditions' => array('party_name' => $customer_name, 'custom_type_id_fk' => $customer_type)));
        if (empty($result)) {
            echo "No";
        } else {
            echo "Yes";
        }
        exit;
    }

    public function customer_search_ajax() {

        $conditions = array();
//$conditions['Stock.flag']=1;
        if (!empty($this->request->data['executive_id'])) {
            $conditions['Customer.executive_id'] = $this->request->data['executive_id'];
        }
        if (!empty($this->request->data['route_id'])) {
            $conditions['Customer.route_id'] = $this->request->data['route_id'];
        }

        if (!empty($this->request->data['day'])) {
            if ($this->request->data['day'] != 'select') {

                $conditions['Customer.day'] = $this->request->data['day'];
            }
        }

        if (!empty($this->request->data['customer_type'])) {
            $conditions['Customer.customer_type_id'] = $this->request->data['customer_type'];
        }
        if (!empty($this->request->data['name'])) {
            $AccountHead_id = $this->AccountHead->find('first', array('conditions' => array('AccountHead.name' => $this->request->data['name'])));
            if (!empty($AccountHead_id)) {
                $conditions['Customer.account_head_id'] = $AccountHead_id['AccountHead']['id'];
            }
        }

//pr($conditions);
        $Customer = $this->Customer->find('all', array('conditions' => $conditions));
        $data['row'] = '';
        if ($Customer) {
            foreach ($Customer as $key => $value) {
                $data['row'] = $data['row'] . '<tr class="blue-pddng">';
                $data['row'] = $data['row'] . '<td>' . $value["Executive"]["name"] . '</td>';
                $data['row'] = $data['row'] . '<td>' . $value["Route"]["name"] . '</td>';
                $data['row'] = $data['row'] . '<td>' . $value["Customer"]["day"] . '</td>';
                $data['row'] = $data['row'] . '<td>' . $value["CustomerType"]["name"] . '</td>';
                $data['row'] = $data['row'] . '<td ><span style="display:none;" class="AccountHead_id">' . $value["AccountHead"]["id"] . '</span><span>' . $value["AccountHead"]["name"] . '</span></td>';
                $data['row'] = $data['row'] . '<td>' . $value["AccountHead"]["opening_balance"] . '</td>';
                $data['row'] = $data['row'] . '<td><a><i class="fa fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#Account_edit__modal"></i></a></td>';
                $data['row'] = $data['row'] . '<td></td>';
                $data['row'] = $data['row'] . '</tr>';
            }
            $data['result'] = 'Success';
        } else {
            $data['result'] = 'Error';
        }
        echo json_encode($data);
        exit;
    }


    public function get_customer_pending_invoice_list_ajax($account_head_id) {

        try {
            $Sales = $this->Sale->find('list', array(
                'conditions' => array(
                    'account_head_id' => $account_head_id,
                    'status' => 2,
                    'flag' => 1
                    ),
                'fields' => array('Sale.id', 'Sale.account_head_id')
                ));
            $AccountHead_opening_balance=$this->AccountHead->find('first',array(
                'conditions'=>array(
                    'AccountHead.id'=>$account_head_id,
                    ),
                'fields'=>array('AccountHead.opening_balance')
                ));
            $Journal_opening=$this->Journal->find('list',array(
                'conditions'=>array(
                    'Journal.remarks'=>'Sale Invoice No :0',
                    'Journal.credit'=>$account_head_id,
                    'Journal.flag=1',
                    ),
                'fields'=>array(
                    'Journal.amount',
                    )
                )); 
            $opening_amount=0;
            if($Journal_opening){
                foreach($Journal_opening as $key_open=>$value_open){
                    $opening_amount+=$value_open;
                }
                $opengin_list_amount=$AccountHead_opening_balance['AccountHead']['opening_balance']-$opening_amount;  
            }else{
                $opengin_list_amount=$AccountHead_opening_balance['AccountHead']['opening_balance'];
            }
            $sale_list = [];
            $invoice_no_list = [];
            if(floatval($opengin_list_amount))
            {
                $sale_list['0']=$opengin_list_amount;          
            }
            foreach ($Sales as $key => $value) {
                $single_list['id'] = $key;
                $Sale = $this->Sale->findById($key);
                $SaleItem = $this->SaleItem->find('list', array(
                    'conditions' => array(
                        'SaleItem.sale_id' => $key,
                        ),
                    'fields' => array(
// 'SaleItem.discount',
                        )
                    ));
                $Journal = $this->Journal->find('list', array(
                    'conditions' => array(
                        'Journal.remarks' => 'Sale Invoice No :' . $Sale['Sale']['invoice_no'],
                        'Journal.credit' => $value,
                        'Journal.flag=1',
                        ),
                    'fields' => array(
                        'Journal.amount',
                        )
                    ));
                $paid_amount = 0;
                foreach ($Journal as $key0 => $amount) {
                    $paid_amount+=$amount;
                }
                $Journal_retun = $this->Journal->find('list', array(
                    'conditions' => array(
                        'Journal.work_flow' => 'Sales Refund :' . $Sale['Sale']['invoice_no'],
                        'Journal.debit' => $value,
                        'Journal.flag=1',
                        ),
                    'fields' => array(
                        'Journal.amount',
                        )
                    ));
                $Refund_amount=0;
                foreach ($Journal_retun as $key0 => $amount) {
                    $Refund_amount+=$amount;
                }
                $SalesReturnItem = $this->SalesReturnItem->find('list', array(
                    'conditions' => array(
                        'SalesReturnItem.invoice_no' => $Sale['Sale']['invoice_no'],
                        ),
                    'fields' => array(
                        'SalesReturnItem.total',
                        )
                    ));
                $Return_amount=0;
                foreach ($SalesReturnItem as $key1 => $amount1) {
                    $Return_amount+=$amount1;
                }

                $discount = 0;
// foreach ($SaleItem as $key1 => $amount1) {
//     $discount+=$amount1;
// }
                $sale_amount = $Sale['Sale']['total'];
// $balance = $sale_amount + $discount - $paid_amount-($Return_amount-$Refund_amount);
                $sale_amounts = $Sale['Sale']['total']-$Return_amount;

                $paid = $paid_amount-$Refund_amount;
                $balance = $sale_amounts + $discount - $paid;
                $single_list['value'] = $balance;
                $single_list['invoice_no'] = $Sale['Sale']['invoice_no'];
                if ($balance) {
                    $sale_list[$key] = floatval($balance);
                }
            }

            if (!$sale_list)
                throw new Exception("Empty", 1);
            $return['sales'] = $sale_list;

            $return['result'] = 'Success';
        } catch (Exception $e) {
            $return['result'] = $e->getMessage();
        }
        echo json_encode($return);
        exit;
    }

    public function get_excecutive_ajax($account_head_id) {

        try {
            $Sales = $this->Customer->find('first', array(
                'conditions' => array(
                    'account_head_id' => $account_head_id
                    ),
                'fields' => array('Executive.id', 'Executive.name')
                ));

            if (!$Sales)
                throw new Exception("Empty", 1);
            $return['sales'] = $Sales;
            $return['result'] = 'Success';
        } catch (Exception $e) {
            $return['result'] = $e->getMessage();
        }
        echo json_encode($return);
        exit;
    }
    public function get_product_unit(){
        $data=$this->request->data;
        $result=array();
        $Product=$this->Product->findById($data['product_name']);
        if(!empty($Product))
        {
            $result['product_unit']=$Product['Unit']['id'];
            $result['unit_name']=$Product['Unit']['name'];
        }
        
        echo json_encode($result);
        exit;
    }
    public function CustomerDiscount_delete($id)
    {
        $datasource_CustomerDiscount = $this->CustomerDiscount->getDataSource();
        
        try {
          $datasource_CustomerDiscount->begin();
          $CustomerDiscount=$this->CustomerDiscount->findById($id);
          if(!$this->CustomerDiscount->delete($id))
            throw new Exception("Error while deleting", 1);

          $datasource_CustomerDiscount->commit();
          $return['result']='Success';
        } catch (Exception $e) {
          $datasource_CustomerDiscount->rollback();
          $return['result']=$e->getMessage();
        }
        echo json_encode($return);
        exit;
      }

}
