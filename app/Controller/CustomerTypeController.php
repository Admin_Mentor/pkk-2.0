<?php
App::uses('AppController', 'Controller');
class CustomerTypeController extends AppController {
	public $uses=array(
		'Customer',
		'CustomerType'
		);
	public function add_customer_type_ajax()
	{
		try {
			$name=strtoupper(trim($this->request->data['name']));

			$latestRow = $this->CustomerType->find('first', array(
                               'order' => array('id' => 'DESC') ));
			$prevCode = $latestRow['CustomerType']['code'];
			if(!empty($prevCode))
			{
				$code = $prevCode + 1;
				
			}
			else{
				$code = '11';
			}
            //             $NameArray = str_split($name);


            // $cnt = count($NameArray);
            // $i = 0;
            // while ($i <= $cnt) {
            //     if($i==0)
            //     {
            //          $code = strtoupper(trim($NameArray[$i]));
            //     }
            //     else{
            //     $code = $code . strtoupper(trim($NameArray[$i]));
            //     }
            //     $RowCount = $this->CustomerType->find('count', array('conditions' => array('CustomerType.code' => $code)));
               
            //     if ($RowCount < 1) {
            //         $i = $cnt;
            //     }
            //     $i++;
            // }
			$data=array(
				'name'=>$name,
                                'code'=>$code,
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s'),
				);
			$this->CustomerType->create();
			if(!$this->CustomerType->save($data))
				throw new Exception("CustomerType Creation Error", 1);
			$last_id=$this->CustomerType->getLastInsertId();
			$CustomerType=$this->CustomerType->findById($last_id);
			$return['key']=$CustomerType['CustomerType']['id'];
			$return['value']=$CustomerType['CustomerType']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	
	public function index()
	{
		$CustomerType = $this->CustomerType->find('list', array('fields' => array('id','name')));
		$this->set('CustomerType',$CustomerType);
		$CustomerTypes = $this->CustomerType->find('all', array('fields' => array('id','name')));
		$this->set('CustomerTypes',$CustomerTypes);
	}
	public function CustomerType_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='id';
		$columns[]='name';
		$columns[]='id';
		$conditions=[];
		$totalData=$this->CustomerType->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'CustomerType.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->CustomerType->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->CustomerType->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'CustomerType.*',
			)
		));
		foreach ($Data as $key => $value) {
			// $Data[$key]['CustomerType']['name'] ='<span><i customer_type_name="'.$value['CustomerType']['name'].'"></i>'.$value['CustomerType']['name'].'</span>&nbsp;&nbsp;';
			$Data[$key]['CustomerType']['action'] ='<span><i customer_type_name="'.$value['CustomerType']['name'].'" table_id="'.$value['CustomerType']['id'].'" class="fa fa-2x fa-edit   edit_CustomerType   blue-col"></i></span>&nbsp;&nbsp;';
			$Data[$key]['CustomerType']['action'] .="<span><i table_id='".$value['CustomerType']['id']."' class='fa fa-2x fa-trash delete_CustomerType blue-col'></i></span>";
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function delete_by_ajax($id)
	{
		$Customer=$this->Customer->findByCustomerTypeId($id);
		if(empty($Customer))
		{
			if($this->CustomerType->delete($id))
			{
				$return['result']='Success';
			}
		}
		else
		{
			$return['result']='It Is Used';
		}
		echo json_encode($return);
		exit;
	}
	public function edit_ajax(){
		try {
			$id=$this->request->data['id'];
			// $discount_percentage=$this->request->data['discount_percentage'];
			if(empty($id))
				throw new Exception("Empty customer_type Id");
			$name=strtoupper(trim($this->request->data['name']));
			if(empty($name))
				throw new Exception("Empty customer_type Name");
			$this->CustomerType->id=$id;
			if(!$this->CustomerType->saveField('name',$name))
				throw new Exception("Error In CustomerType Request");
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();			
		}
		echo json_encode($return);
		exit;
	}	
}
