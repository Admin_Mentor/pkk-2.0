<?php
App::uses('AppController', 'Controller');
class DivisionController extends AppController {
	public $helpers= array('Html','Form');
	public $uses=array('Division');
	public function division_search()
	{
		$division_name=$this->request->data['division_name'];
		$division_name=trim($division_name);
		$Division=$this->Division->find('first',array('conditions'=>array('Division.name'=>$division_name)));
		if(!empty($Division)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}
	public function index(){
		$Division = $this->Division->find('list', array('fields' => array('id','name')));
		$this->set('Division',$Division);
		$Divisions = $this->Division->find('all', array('order'=>array('Division.id DESC'),'fields' => array('id','name')));
		$this->set('Divisions',$Divisions);
	}
	public function add_division_ajax() {

        $return = ['result' => 'Empty'];

        try {
            $data = $this->request->data;
            $name = $data['name'];

            $division_data = array(
                'name' => strtoupper($name),
            );

            $this->Division->create();
            if (!$this->Division->save($division_data))
                throw new Exception("Error In Division Create", 1);
            $return['result'] = 'Success';
            $Division_id = $this->Division->getLastInsertId();
            $Division = $this->Division->findById($Division_id);
            
        } catch (Exception $e) {
            $return['result'] = $e->getMessage();
        }

        exit;
    }
	public function division_add_ajax()
	{
		$data=array(
			'name'=>strtoupper($this->request->data['modal_divion_name']),
			
			);
		$this->Division->create();
		if($this->Division->save($data))
		{
			$this->Session->setFlash('data is saved');
			$last_iserted_pt=$this->Division->findById($this->Division->getLastInsertId());
			echo "<option value='".$last_iserted_pt['Division']['id']."'>".$last_iserted_pt['Division']['name']." </option>";
		}
		exit;
	}
	public function edit_ajax(){
		try {
			$prdct_id=$this->request->data['prdct_id'];
			if(!$prdct_id)
				throw new Exception("Empty ProductType Id", 1);
			$prdct_typ=strtoupper(trim($this->request->data['prdct_typ']));
			$this->ProductType->id=$prdct_id;
			if(!$this->ProductType->saveField('name',$prdct_typ ))
				throw new Exception("Error Processing ProductType Name Updation", 1);
			if(!$this->ProductType->saveField('updated_at',date('Y-m-d H:i:s')))
				throw new Exception("Error Processing ProductType updated_at Updation", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function warehouse_type_delete_ajax($id)
	{
		try {
			$Product=$this->Product->findByProductTypeId($id);
			if($Product)
				throw new Exception("This is Used In ".$Product['Product']['name'], 1);
			if(!$this->ProductType->delete($id))
				throw new Exception("Error Processing Request While delete", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function division_get_ajax($id)
	{
		try {
			$Division=$this->Division->findById($id);
			if(!$Division)
				throw new Exception("Empty Division", 1);
			$return['data']=$Division['Division'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function route_get_ajax($id)
	{
		try {
			$Location=$this->Route->findById($id);
			if(!$Location)
				throw new Exception("Empty Location", 1);
			$return['data']=$Location['Route'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function division_edit_ajax()
	{
		try {
			$data=$this->request->data['DivisionEdit'];
			$Table_data=array(
				'name'=>trim(strtoupper($data['name'])),
				);
			$this->Division->id=$data['id'];
			if(!$this->Division->save($Table_data))
			{
				$errors = $this->Division->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$Division=$this->Division->read();
			$return['key']=$Division['Division']['id'];
			$return['value']=$Division['Division']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function edit_division_ajax(){
		try {
			$id=$this->request->data['id'];
			if(empty($id))
				throw new Exception("Empty Division Id");
			$name=strtoupper(trim($this->request->data['name']));
			if(empty($name))
				throw new Exception("Empty Division Name");
			
			$tableData = [
				'name'=>$name,
			];
			$this->Division->id=$id;
			if(!$this->Division->save($tableData))
				throw new Exception("Error In Division Request");
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();			
		}
		echo json_encode($return);
		exit;
	}
	
	
}