<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
ini_set('memory_limit', '2000M');
ini_set('max_execution_time', '0');
class ExecutiveController extends AppController {
	public $uses=[
	'Type',
	'MasterGroup',
	'Group',
	'SubGroup',
	'AccountHead',
	'Executive',
	'Location',
	'Route',
	'Customer',
	'Warehouse',
	'Sale',
	'Staff',
	'Branch',
	'ExecutiveRouteMapping',
	'NoSale',
	'Product',
	'ExecutiveRate'
	];
	public $components = array('RequestHandler');
	public function Executive_Table_ajax()
	{
		$requestData=$this->request->data;		$columns = [];
		$columns[]='Executive.name';
		$columns[]='Executive.mobile';
		$columns[]='Executive.id';
		$columns[]='Executive.username';
		$columns[]='Executive.password';
		$columns[]='Warehouse.name';
		$columns[]='Executive.id';
		$columns[]='Executive.id';
		$conditions=[];
		if(!empty($requestData['location_id'])) {
			if($requestData['location_id']) $conditions['ExecutiveRouteMapping.route_id']=$requestData['location_id'];
		}
		    $this->Executive->unbindModel(array('hasMany' => array('Sale')));
			$totalData=$this->Executive->find('count', array(
               			"joins"=>array(
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
					),
					// array(
					// 	"table"=>'warehouses',
					// 	"alias"=>'Warehouse',
					// 	"type"=>'left',
					// 	"conditions"=>array('Executive.warehouse_id=Warehouse.id'),
					// ),
				),
                   'group'=>array('ExecutiveRouteMapping.executive_id'),
				'conditions'=>$conditions
				));
			$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Executive.name LIKE' =>'%'. $q . '%',
				'Executive.mobile LIKE'     =>'%'. $q . '%',
				'Executive.username LIKE'            =>'%'. $q . '%',
				'Executive.password LIKE'            =>'%'. $q . '%',
			);
			$totalFiltered=$this->Executive->find('count',[
               			"joins"=>array(
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
					),
					// array(
					// 	"table"=>'warehouses',
					// 	"alias"=>'Warehouse',
					// 	"type"=>'left',
					// 	"conditions"=>array('Executive.warehouse_id=Warehouse.id'),
					// ),
				),
               'group'=>array('ExecutiveRouteMapping.executive_id'),
				'conditions'=>$conditions,
			]);
		}
		 $this->Executive->unbindModel(array('hasMany' => array('Sale')));
		$Data=$this->Executive->find('all', array(
               		"joins"=>array(
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
					),
					// array(
					// 	"table"=>'warehouses',
					// 	"alias"=>'Warehouse',
					// 	"type"=>'left',
					// 	"conditions"=>array('Executive.warehouse_id=Warehouse.id'),
					// ),
				),
             'group'=>array('ExecutiveRouteMapping.executive_id'),
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Executive.*',
				'Warehouse.name',
			)
		));
		foreach ($Data as $key => $value) {
			if($value['Executive']['block']!=0)
                {
                  $block_unblock = 0;
                  $title ='Unblock';
                  $icon = 'fa-2x fa-user';
                }
                else{
                  $block_unblock = 1;
                  $title ='Block';
                  $icon = ' fa-2x fa-user-times';
                }
             $Data[$key]['Executive']['executive_credit_limit']=floatval($value['Executive']['executive_credit_limit']);
			$Data[$key]['Executive']['route']='<a data-id="'.$value['Executive']['id'].'" title="Show Routes" class="route-show" href="#"><i class="fa fa-2x fa-map-marker" aria-hidden="true"></i></a>';
			$Data[$key]['Executive']['edit']='<i data-id="'.$value['Executive']['id'].'" class="fa fa-2x fa-edit edit_executive" aria-hidden="true" style="color: #3c8dbc;"></i>';
	        $Data[$key]['Executive']['block']='<a title="'.$title.'" onclick="return confirm("Are you sure?")" href="'.$this->webroot.'Executive/BlockUnblockExecutive/'.$value['Executive']['id']."/".$block_unblock.'">';
	        $Data[$key]['Executive']['block'].='<i class="fa fa-2x '.$icon.' blue-col"></i></a>';
	
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function AddSalesman()
	{
		$this->Executive->unbindModel(array('hasMany'=>array('Sale')));
		$Executive=$this->Executive->find('all',array(
			'conditions'=>['Executive.block'=>0],
			'fields'=>array(
				'Executive.*',
				'Warehouse.name',
		)));
		$this->set('ExecutiveCount',count($Executive));
		$this->set(compact('Executive'));
		$Warehouse=$this->Warehouse->find('list');
		$this->set(compact('Warehouse'));
		$Location=$this->Route->find('list');

		$this->set(compact('Location'));
		$this->Staff->virtualFields = array(
			'staff_name' => "CONCAT(Staff.name, ' ', Staff.code)"
			);
$Staff=$this->Staff->find('list',array(
			'fields'=>['id','staff_name'],
			'conditions'=>array('Staff.role_id'=>1,'is_executive'=>0,
		)));
		$this->set(compact('Staff'));
		if($this->request->data)
		{
			$AccountingsController = new AccountingsController;
			$data=$this->request->data['Executive'];

			$staff_id = $data['staff_id'];
			$StaffRow = $this->Staff->find('first', array(

				'conditions' => array('Staff.id' => $staff_id) ));

			$data['name'] = $StaffRow['Staff']['name'];
			$cashname = $StaffRow['Staff']['name'].$StaffRow['Staff']['code'].'_CASH';

			$datasource_Executive = $this->Executive->getDataSource();
			$datasource_Staff = $this->Staff->getDataSource();
			$datasource_ExecutiveRouteMapping = $this->ExecutiveRouteMapping->getDataSource();
			try {
				$this->Staff->id = $staff_id;
				 if($StaffRow['Staff']['is_executive']==1){
					throw new Exception("Already Executive", 1);
				}
				$sub_group_id =1;
				$name = $cashname;
				$opening_balance = 0;
				$date = date('Y-m-d');
				$description = "";
				 $executive=$this->Executive->findByWarehouseIdAndBlock($data['warehouse_id'],0);
               if(!empty($executive))
               {
               	throw new Exception("Warehouse Already Selected...Please Block Existing Executive", 1);
               }
				$executive_data=[
				'name'=>$data['name'],
				'account_head_id'=>0,
				'mobile'=>$data['mobile'],
				'password'=>$data['password'],
				'username'=>$data['username'],
				'warehouse_id'=>$data['warehouse_id'],
				'staff_id'=>$staff_id,
				'daily_expense'=>$data['daily_expense'],
				'executive_credit_limit'=>$data['executive_credit_limit'],
				];
				$datasource_Executive->begin();
				$this->Executive->create();
				if(!$this->Executive->save($executive_data))
				{
					$errors = $this->Executive->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_Executive->commit();
				$executive_id=$this->Executive->getLastInsertId();

				foreach ($data['location_id'] as $key => $value) {
					$ExecutiveRouteMapping=$this->ExecutiveRouteMapping->findByRouteId($value);
					// if(!empty($ExecutiveRouteMapping))
					// 	throw new Exception("Route is taken", 1);
					$mapping_data=[
					'executive_id'=>$executive_id,
					'route_id'=>$value,
					];
					$datasource_ExecutiveRouteMapping->begin();
					$this->ExecutiveRouteMapping->create();
					if(!$this->ExecutiveRouteMapping->save($mapping_data))
					{
						$errors = $this->ExecutiveRouteMapping->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					$datasource_ExecutiveRouteMapping->commit();
				}
                $this->Staff->id = $staff_id;
				if ($this->Staff->id) {
					$this->Staff->saveField('is_executive', 1);
				}
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_Executive->rollback();
// $datasource_Staff->rollback();
				$datasource_ExecutiveRouteMapping->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function AddSalesmanOld()
	{
		$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Executive/AddSalesman';
// 		$menu_id=$this->Menu->field('Menu.id');

		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Executive/AddSalesman'));

		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		$Executive=$this->Executive->find('all');
		$this->set(compact('Executive'));
		$Warehouse=$this->Warehouse->find('list');
		$this->set(compact('Warehouse'));
		$Location=$this->Route->find('list');
		$this->set(compact('Location'));
		$Staff=$this->Staff->find('list');
		$this->set(compact('Staff'));
		if($this->request->data)
		{
			$data=$this->request->data['Executive'];


			$StaffRow = $this->Staff->find('first', array(

				'order' => array('Staff.id' => 'DESC') ));

			if(!empty($StaffRow))
			{
				$PrevCode = $StaffRow['Staff']['code'];
				$code = $PrevCode + 1;


			}
			else{

				$code = '1000';

			}
			$data['code'] = $code; 
			$datasource_Executive = $this->Executive->getDataSource();
			$datasource_Staff = $this->Staff->getDataSource();
			$datasource_ExecutiveRouteMapping = $this->ExecutiveRouteMapping->getDataSource();
			try {
				$sub_group_id =35;
				$name = $data['name'];
				$opening_balance = 0;
				$date = date('Y-m-d');
				$description = "";
				$AccountingsController = new AccountingsController;
				$function_return = $AccountingsController->AccountHeadCreate($sub_group_id, $name, $opening_balance, $date, $description);
				if ($function_return['result'] != 'Success')
					throw new Exception($function_return['message']);
				$AccountHead_id = $this->AccountHead->getLastInsertId();

				$staff_data=[
				'name'=>$data['name'],
				'account_head_id'=> $AccountHead_id,
				'code'=>$data['code'],
				'contact_no'=>$data['mobile'],
				'address'=>' ',
				'is_executive'=>1,
				];

				$datasource_Staff->begin();
				$this->Staff->create();
				if(!$this->Staff->save($staff_data))
				{
					$errors = $this->Staff->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_Staff->commit();
				$staff_id=$this->Staff->getLastInsertId();

				$sub_group_id =1;
				$name = $data['name'].' Cash';
				$opening_balance = 0;
				$date = date('Y-m-d');
				$description = "";

				$function_return = $AccountingsController->AccountHeadCreate($sub_group_id, $name, $opening_balance, $date, $description);
				if ($function_return['result'] != 'Success')
					throw new Exception($function_return['message']);
				$CashAccountHead_id = $this->AccountHead->getLastInsertId();

				$executive_data=[
				'name'=>$data['name'],
				'account_head_id'=>$CashAccountHead_id,
				'mobile'=>$data['mobile'],
				'password'=>$data['password'],
				'username'=>$data['username'],
				'warehouse_id'=>$data['warehouse_id'],
// 'location_id'=>$data['location_id'],
				'staff_id'=>$staff_id,
				];
				$datasource_Executive->begin();
				$this->Executive->create();
				if(!$this->Executive->save($executive_data))
				{
					$errors = $this->Executive->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$datasource_Executive->commit();
				$executive_id=$this->Executive->getLastInsertId();
// pr($executive_id);exit;
				foreach ($data['location_id'] as $key => $value) {
					$mapping_data=[
					'executive_id'=>$executive_id,
					'route_id'=>$value,
					];
					$datasource_ExecutiveRouteMapping->begin();
					$this->ExecutiveRouteMapping->create();
					if(!$this->ExecutiveRouteMapping->save($mapping_data))
					{
						$errors = $this->ExecutiveRouteMapping->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					$datasource_ExecutiveRouteMapping->commit();
				}


				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_Executive->rollback();
				$datasource_Staff->rollback();
				$datasource_ExecutiveRouteMapping->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}

	public function api_get_customer_by_location()
	{
		$return=[
		'status'=>'Empty',
		'customers'=>[],
		];
		$data=$this->request->data;
		if(isset($data['location_id']))
		{
			$location=$data['location_id'];
			$return['status']='success';
			$Location=$this->Location->findById($location);
		}
		if(isset($Location))
		{
			$AccountHead=$this->AccountHead->find('all',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('Customer.account_head_id=AccountHead.id'),
						),
					array(
						"table"=>'locations',
						"alias"=>'Location',
						"type"=>'inner',
						"conditions"=>array('Location.id=Customer.location_id'),
						),
					),
				'fields'=>array(
					'AccountHead.id',
					'AccountHead.name',
					'Customer.email',
					'Customer.mobile',
					'Location.id',
					'Location.name',
					),
				'conditions'=>array(
					'Customer.location_id'=>$location,
					)
				));
			if($AccountHead)
			{
				$All_Location=[];
				foreach ($AccountHead as $key => $value) {
					$Single_Location['id']=$value['AccountHead']['id'];
					$Single_Location['name']=$value['AccountHead']['name'];
					$Single_Location['location_name']=$value['Location']['name'];
					$Single_Location['location_id']=$value['Location']['id'];
					$Single_Location['mobile']=$value['Customer']['mobile'];
					$Single_Location['email']=$value['Customer']['email'];
					$Single_Location['credit']='0';
					$Single_Location['debit']='0';
					array_push($All_Location, $Single_Location);
				}
				$return[ 'customers' ]=$All_Location;
			}
		}
		echo json_encode($return);
		exit;
	}
	public function api_get_location_list()
	{
		$return=[
		'status'=>'Empty',
		'Locations'=>[],
		];
		$Location=$this->Location->find('list');
		if($Location)
		{
			$return[ 'status' ]='success';
			$All_Location=[];
			foreach ($Location as $key => $value) {
				$Single_Location['id']=$key;
				$Single_Location['name']=$value;
				array_push($All_Location, $Single_Location);
			}
			$return[ 'Locations' ]=$All_Location;
		}
		echo json_encode($return);
		exit;
	}
	public function get_executive_details_ajax($id)
	{
		$return=[
		'status'=>'Empty',
		'Executive'=>[],
		'ExecutiveRoute'=>[],
		'Cash'=>[],
		];
		if(isset($id))
		{
			$Executive=$this->Executive->findById($id);
			$return['staffName'] = $this->Staff->field(
				'Staff.name',
				array('Staff.id ' => $Executive['Executive']['staff_id']));
			$return['Executive']=$Executive['Executive'];
			$cash_id = $Executive['Executive']['account_head_id'];
//$Cash=$this->AccountHead->findById($cash_id);
//$return['Cash']=$Cash['AccountHead'];	
			$ExecutiveRoute=$this->ExecutiveRouteMapping->find('list',array(
				'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$id,),

				'fields'=>'ExecutiveRouteMapping.route_id'
				));

			$return['ExecutiveRoute']=$ExecutiveRoute;

		}
		echo json_encode($return);
		exit;
	}
	public function EditExecutive()
	{
		if($this->request->data)
		{
			$data=$this->request->data['Executive'];
			$datasource_Executive = $this->Executive->getDataSource();
			$datasource_Staff = $this->Staff->getDataSource();
			$datasource_ExecutiveRouteMapping = $this->ExecutiveRouteMapping->getDataSource();
			try {
				$staff_id = $this->Executive->field(
					'Executive.staff_id',
					array('Executive.id ' => $data['id']));
				$staff_data=[
// 'name'=>$data['name'],
				'contact_no'=>$data['mobile'],
				'address'=>' ',
				'is_executive'=>1,
				];

				$datasource_Staff->begin();
				$this->Staff->id=$staff_id;
				if(!$this->Staff->save($staff_data))
				{
					$errors = $this->Staff->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				  $executive=$this->Executive->findByWarehouseIdAndBlock($data['warehouse_id'],0);

               if(!empty($executive))
               {
               	$executive_edit=$this->Executive->findById($data['id']);
               	if($executive_edit['Executive']['warehouse_id']!=$data['warehouse_id'])
               	{
               	throw new Exception("Warehouse Already Selected...Please Block Existing Executive", 1);
                }
               }
				$executive_data=[
// 'name'=>$data['name'],
				'mobile'=>$data['mobile'],
				'password'=>$data['password'],
				'username'=>$data['username'],
				'warehouse_id'=>$data['warehouse_id'],
				'daily_expense'=>$data['daily_expense'],
                'executive_credit_limit'=>$data['executive_credit_limit'],
				];
				$datasource_Executive->begin();
				$this->Executive->id=$data['id'];
				if(!$this->Executive->save($executive_data))
				{
					$errors = $this->Executive->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$executive_id = $data['id']; 
				$routelist = $this->ExecutiveRouteMapping->find('list', array(
					'conditions' => array('ExecutiveRouteMapping.executive_id' => $executive_id,),
					'fields' => array('ExecutiveRouteMapping.id', 'ExecutiveRouteMapping.route_id')
					));
// pr($routelist);exit;
				foreach ($data['location_id'] as $key => $value) {
					$rowcount = $this->ExecutiveRouteMapping->find('count', array('conditions' => array('ExecutiveRouteMapping.executive_id' => $executive_id,'ExecutiveRouteMapping.route_id' => $value)));
					if($rowcount <1)
					{
						$mapping_data=[
						'executive_id'=>$executive_id,
						'route_id'=>$value,
						];
						$ExecutiveRouteMapping=$this->ExecutiveRouteMapping->findByRouteId($value);
						// if(!empty($ExecutiveRouteMapping))
						// 	throw new Exception("Route is taken", 1);
						$datasource_ExecutiveRouteMapping->begin();
						$this->ExecutiveRouteMapping->create();
						if(!$this->ExecutiveRouteMapping->save($mapping_data))
						{
							$errors = $this->ExecutiveRouteMapping->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}
						$datasource_ExecutiveRouteMapping->commit();
					}
				}
// $removeroutes=array_diff($routelist,$data['location_id']);
// foreach ($removeroutes as $key => $value) {
// 	$this->ExecutiveRouteMapping->delete($key);
// }
				$datasource_Staff->commit();
				$datasource_Executive->commit();
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_Executive->rollback();
				$datasource_Staff->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function BlockUnblockExecutive($id,$flag)
	{
		try {
          if($flag==0)
			{
               		$ExecutiveRouteMapping=$this->ExecutiveRouteMapping->find('all', array(
               			"joins"=>array(
					array(
						"table"=>'executives',
						"alias"=>'Executive',
						"type"=>'inner',
						"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
					),
				),
					'conditions'=>array(
						'Executive.block'=>0,
						'Executive.id !='=>$id,
					)
				));
				if(!empty($ExecutiveRouteMapping))
				{
				 foreach ($ExecutiveRouteMapping as $key => $value)
				   {
					$executive_edit=$this->ExecutiveRouteMapping->findByExecutiveId($id);
               	if($executive_edit['ExecutiveRouteMapping']['route_id']==$value['ExecutiveRouteMapping']['route_id'])
               	{
					throw new Exception("Route Already Selected ...Cant Unblock", 1);
				}
			       }
			 }
			 $warehouse_id=$this->Executive->field('warehouse_id',['Executive.id'=>$id]);
			 $executive=$this->Executive->findByWarehouseIdAndBlock($warehouse_id,0);

               if(!empty($executive))
               {
               	throw new Exception("Warehouse Already Selected...Cant Unblock", 1);
                }
            }
			$this->Executive->id=$id;
			if(!$this->Executive->saveField('block',$flag))
				throw new Exception("Cant Block This Executive", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		$this->redirect( Router::url( $this->referer(), true ) );
	}
	public function DeleteExecutive($id)
	{
		try {
			$staff_id = $this->Executive->field(
				'Executive.staff_id',
				array('Executive.id ' => $id));
			$executive_route_mapping_id = $this->ExecutiveRouteMapping->field(
				'ExecutiveRouteMapping.id',
				array('ExecutiveRouteMapping.executive_id ' => $id));
			$Sale=$this->Sale->findByExecutiveId($id);
			if($Sale)
				throw new Exception("Used In Sale", 1);
			if(!$this->ExecutiveRouteMapping->delete($executive_route_mapping_id))
				throw new Exception("Cant Delete This Executive Route Mapping", 1);
			if(!$this->Executive->delete($id))
				throw new Exception("Cant Delete This Executive", 1);
			$this->Staff->id=$staff_id;
			if(!$this->Staff->saveField('is_executive',0))
				throw new Exception("Cant Delete This Executive", 1);
			// if(!$this->Staff->delete($staff_id))
			// 	throw new Exception("Cant Delete This Executive Staff", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		$this->redirect( Router::url( $this->referer(), true ) );
	}
	public function api_get_invoice_details()
	{
		$return=[
		'status'=>'Empty',
		'Sale'=>[],
		];
		$data=$this->request->data;
		if(isset($data['invoice_no']))
		{
			$invoice_no=$data['invoice_no'];
			$return['status']='success';
			$Sale=$this->Sale->find('all',array(
				"joins"=>array(
					array(
						"table"=>'sale_items',
						"alias"=>'SaleItem',
						"type"=>'inner',
						"conditions"=>array('Sale.id=SaleItem.sale_id'),
						),
					array(
						"table"=>'products',
						"alias"=>'Product',
						"type"=>'inner',
						"conditions"=>array('Product.id=SaleItem.product_id'),
						),
					),
				'conditions'=>array(
					'invoice_no'=>$invoice_no,
					),
				'fields'=>array(
					'SaleItem.unit_price',
					'SaleItem.quantity',
					'SaleItem.total',
					'Product.piecepercart',
					'Product.code',
					'Product.id',
					'Product.name',
					),
				));
		}
		if(isset($Sale))
		{
			$All_Sale=[];
			foreach ($Sale as $key => $value) {
				$Single_Sale['unit_price']=$value['SaleItem']['unit_price'];
				$Single_Sale['quantity']=$value['SaleItem']['quantity'];
				$Single_Sale['total']=$value['SaleItem']['total'];
				$Single_Sale['name']=$value['Product']['name'];
				$Single_Sale['code']=$value['Product']['code'];
				$Single_Sale['piecepercart']=$value['Product']['piecepercart'];
				$Single_Sale['id']=$value['Product']['id'];
				array_push($All_Sale, $Single_Sale);
			}
			$return['Sale']=$All_Sale;	
		}
		echo json_encode($return);
		exit;
	}
	public function api_receipt_payment()
	{
		$user_id=1;
		$return=[
		'status'=>'Empty',
		];
		$data=$this->request->data;
		if($data)
		{
			$datesourse_Sale=$this->Sale->getDataSource();
			$datesourse_Journal=$this->Journal->getDataSource();
			try {
				$datesourse_Sale->begin();
				$datesourse_Journal->begin();
				$PaidList = $data['PaidList'];
				foreach ($PaidList as $key => $value) {
					$invoice_no=$value['invoice_no'];
					$amount=$value['amount'];
					$Sale=$this->Sale->findById($invoice_no);
					$this->Sale->id=$Sale['Sale']['id'];
					if($this->Sale->field('balance')<$amount)
						throw new Exception("balance Amount Must Be Greater than or equal to Payment Amount on invoice no:".$invoice_no."");
					if(!$this->Sale->saveField('balance',$this->Sale->field('balance')-$amount))
						throw new Exception("Error Processing Request in Sale Updation", 1);
					$work_flow='From Mobile';
					$remarks='invoice_no : '.$invoice_no;
					$date=date('Y-m-d');
					$debit=1;
					$credit=$Sale['Sale']['account_head_id'];
					$AccountingsController = new AccountingsController;
					$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);
					if($function_return['result']!='Success')
						throw new Exception($function_return['message'], 1);
				}
				$datesourse_Sale->commit();
				$datesourse_Journal->commit();
				$return['status']='success';
			} catch (Exception $e) {
				$datesourse_Sale->rollback();
				$datesourse_Journal->rollback();
				$return['status']=$e->getMessage();
			}
		}
		echo json_encode($return);
		exit;
	}
	public function get_executive_routes($id)
	{
		$conditions=array();
// $conditions['Stock.flag']=1;


		$conditions['ExecutiveRouteMapping.executive_id']=$id;
		$executive_id_List = $this->ExecutiveRouteMapping->find('all',array('fields'=>'ExecutiveRouteMapping.executive_id,Route.name,ExecutiveRouteMapping.id',
			'conditions'=>$conditions));


		$data['row']='';
		$first_date = date('Y-m-d',strtotime('first day of this month'));
		$i=1;
		foreach ($executive_id_List as $key => $value) {
			$data['row']= $data['row'].'<tr class="blue-pd">';
			$data['row']= $data['row'].'<td>'.$i.'</td>';
			$data['row']= $data['row'].'<td>'.$value["Route"]["name"].'</td>';
			$data['row']= $data['row'].'<td><a data-id="'.$value["ExecutiveRouteMapping"]["id"].'" class="del-route"><i class="fa fa-trash blue-col blue-col"></i></a></td>';

			$data['row']= $data['row'].'</tr>';
			$data['executive_id'] = $value['ExecutiveRouteMapping']['executive_id'];
			$i++;

		}

		echo json_encode($data);
		exit;
	}
	public function DeleteExecutiveRoute($id)
	{
		try {


			if(!$this->ExecutiveRouteMapping->delete($id))
				throw new Exception("Cant Delete This Route", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		echo json_encode($return);
		exit;

	}
	public function get_warehouse_by_executive($id=null)
	{
		$return['option']='';

		$return['option'].='<option value="1">Main</option>';

		$user_branch_id=$this->Session->read('User.branch_id');

		if(empty($user_branch_id)){
			$conditions=[];
			if($id)
			{
				$conditions['Executive.id']=$id;
			}
			$Executive=$this->Executive->find('all',array(
				'conditions'=>$conditions,
				'fields'=>array(
					'Warehouse.id',
					'Warehouse.name',
					'Executive.id',
					)
				));
			if(!empty($Executive) && $id)
			{
				$return['option']='';
				$return['result']='Success';
				$return['option'].='<option value="'.$Executive[0]['Warehouse']['id'].'">'.$Executive[0]['Warehouse']['name'].'</option>';
			}
			if(!empty($Executive) && !$id)
			{
				$return['result']='Success';
			}
			else
			{
				$return['result']='Empty';
			}
		}
		else
		{

			$Branch=$this->Branch->find('all',array(
				'conditions'=>array('Branch.id'=>$user_branch_id),
				'fields'=>array(
					'Warehouse.id',
					'Warehouse.name',
					)
				));
			$return['option']='';
			if($Branch)
			{
				$return['result']='Success';
				$return['option'].='<option value="'.$Branch[0]['Warehouse']['id'].'">'.$Branch[0]['Warehouse']['name'].'</option>';
			}
			else
			{
				$return['result']='Empty';
			}
		}
		echo json_encode($return);
		exit;
	}
	public function route_search()
	{
		$location_name=$this->request->data['location_name'];
		$location_name=trim($location_name);
		$Location=$this->Route->find('first',array('conditions'=>array('Route.name'=>$location_name)));
		if(!empty($Location)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}

	public function add_route_ajax() {

		$return = ['result' => 'Empty'];

		try {
			$data = $this->request->data;
			$name = $data['name'];
			$code = $data['code'];

			$route_date = array(
				'name' => strtoupper($name),
				'code' => strtoupper($code),
				);

			$this->Route->create();
			if (!$this->Route->save($route_date))
				throw new Exception("Error In Route Create", 1);
			$return['result'] = 'Success';
			$Route_id = $this->Route->getLastInsertId();
			$Route = $this->Route->findById($Route_id);
// $return['key'] = $Route['Route']['id'];
// $return['value'] = $Route['Route']['name'];
			echo "<option value='".$Route['Route']['id']."'>".$Route['Route']['name']." </option>";
		} catch (Exception $e) {
			$return['result'] = $e->getMessage();
		}

		exit;
	}
	public function route_get_ajax($id)
	{
		try {
			$Route=$this->Route->findById($id);
			if(!$Route)
				throw new Exception("Empty Route", 1);
			$return['data']=$Route['Route'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function route_edit_ajax()
	{
		try {
			$data=$this->request->data['LocationEdit'];
			$Table_data=array(
				'name'=>trim(strtoupper($data['name'])),
// 'updated_at'=>trim(date('Y-m-d h:i:s')),
				);
			$this->Route->id=$data['id'];
			if(!$this->Route->save($Table_data))
			{
				$errors = $this->Route->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$Route=$this->Route->read();
			$return['key']=$Route['Route']['id'];
			$return['value']=$Route['Route']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function get_staff_details_ajax($id)
	{
		$return=[
		'status'=>'Empty',
		'Staff'=>[],
		];
		if(isset($id))
		{
			$Staff=$this->Staff->findById($id);
			$return['Staff']=$Staff['Staff'];	
		}
		echo json_encode($return);
		exit;
	}
	public function get_executive_route_ajax($id)
{
	$return=[
			'status'=>'Empty',
			'ExecutiveRoute'=>[],
		];
		$id_explode=explode(',', $id);
		if($id_explode)
		{
			foreach ($id_explode as $key => $value) 
			{
				 $ExecutiveRouteMapping=$this->ExecutiveRouteMapping->find('first', array(
               			"joins"=>array(
					array(
						"table"=>'executives',
						"alias"=>'Executive',
						"type"=>'inner',
						"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
					),
				),
					'conditions'=>array(
						'ExecutiveRouteMapping.route_id'=>$value,
						'Executive.block'=>0,
					)
				));
			//$route_id=$this->ExecutiveRouteMapping->find('first',array('conditions'=>array('ExecutiveRouteMapping.route_id'=>$value)));
			if($ExecutiveRouteMapping)
			{
				$return['ExecutiveRoute'][]=$value;
				$return['status']="This Route Already Used";
			}
			}
		}
		echo json_encode($return);
		exit;
}
public function get_executive_route_ajax_edit($id=null,$executive_id=null)
{
	$return=[
			'status'=>'Empty',
			'ExecutiveRoute'=>[],
		];
		$id_explode=explode(',', $id);
		if($id!="" && $executive_id!="")
		{
		if($id_explode)
		{
			foreach ($id_explode as $key => $value) 
			{
			//$route_id=$this->ExecutiveRouteMapping->find('first',array('conditions'=>array('ExecutiveRouteMapping.executive_id !='=>$executive_id,'ExecutiveRouteMapping.route_id'=>$value)));
			$ExecutiveRouteMapping=$this->ExecutiveRouteMapping->find('first', array(
               			"joins"=>array(
					array(
						"table"=>'executives',
						"alias"=>'Executive',
						"type"=>'inner',
						"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
					),
				),
					'conditions'=>array(
						'ExecutiveRouteMapping.route_id'=>$value,
						'ExecutiveRouteMapping.executive_id !='=>$executive_id,
						'Executive.block'=>0,
					)
				));
			if($ExecutiveRouteMapping)
			{
				$return['ExecutiveRoute'][]=$value;
				$return['status']="This Route Already Used";
			}
			}
		}
	}
		echo json_encode($return);
		exit;
}
	public function DayRegisterReport()
	{
		$PermissionList = $this->Session->read('PermissionList');
// $this->Menu->action='Sale/InvoiceList';
//   $menu_id=$this->Menu->field('Menu.id');

		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Sale/InvoiceList'));

		$first_date = date('Y-m-d',strtotime('10-01-2018'));
		$executive =2;
		$last_date = date('Y-m-d');

		if(!in_array($menu_id, $PermissionList))
		{
			$this->Session->setFlash("Permission denied");
			return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		}
		$CustomerList=$this->AccountHead->find('all',array(
			"joins"=>array(
				array(
					"table"=>'customers',
					"alias"=>'Customer',
					"type"=>'inner',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
					),

				),

			'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),

			'fields'=>array(
				'Customer.*',
				'AccountHead.id',
				'AccountHead.name',
				'AccountHead.opening_balance',

				),

			));

// pr($CustomerList);exit;
		if($CustomerList)
		{

			$All_Customer=[];
			foreach ($CustomerList as $key => $value) {
				$Single_Customer['id']=$value['AccountHead']['id'];
				$Single_Customer['name']=$value['AccountHead']['name'];
				$Single_Customer['opening_balance']=$value['AccountHead']['opening_balance'];
				$Single_Customer['email']=$value['Customer']['email'];
				$Single_Customer['shope_code']=$value['Customer']['code'];
				$Single_Customer['credit_limit']=$value['Customer']['credit_limit'];
				$Single_Customer['address']=$value['Customer']['place'];
				$Single_Customer['mobile']=$value['Customer']
				['mobile'];

//
				$total = $value['AccountHead']['opening_balance'];
				$Recieved='0';
				$AccountingsController = new AccountingsController;
				$Debit_N_Credit_function=$AccountingsController->General_Journal_Debit_N_Credit_function($value['AccountHead']['id']);
				$total+=$Debit_N_Credit_function['debit'];
				$Recieved+=$Debit_N_Credit_function['credit'];
//
				$Single_Customer['debit']=$total;
				$Single_Customer['credit']=$Recieved;


//
				$Sale=$this->Sale->find('all',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$value['AccountHead']['id']
						),
					'fields'=>array(
						'Sale.id',
						'Sale.invoice_no',
						'Sale.grand_total',
						'Sale.balance',
						'AccountHead.id',
						'AccountHead.name',
						),
					));
				$All_sale=[];
				foreach ($Sale as $key => $row) {
					$Single_sale['sale_id']=$row['Sale']['id'];
					$Single_sale['invoice_no']=$row['Sale']['invoice_no'];
					$Single_sale['total']=$row['Sale']['grand_total'];
					$Single_sale['balance']=$row['Sale']['balance'];
					array_push($All_sale, $Single_sale);

				}
				$Single_Customer['invoice']=$All_sale;

				$NoSale=$this->NoSale->find('all',array(
					'conditions'=>array(
						'NoSale.customer_account_head_id'=>$value['AccountHead']['id']
						),
					'fields'=>array(
						'NoSale.id',
						'NoSale.description',

						),
					));

				$All_nosale=[];
				foreach ($NoSale as $key => $row) {
					$Single_sale['no_sale_id']=$row['NoSale']['id'];
					$Single_sale['description']=$row['NoSale']['description'];

					array_push($All_nosale, $Single_sale);

				}
				$Single_Customer['nosale']=$All_nosale;

//
				array_push($All_Customer, $Single_Customer);
			}

			$return[ 'Customers' ]=$All_Customer;
		}
		$Sale = $this->Sale->find('all', array(
			"joins" => array(
				array(
					"table" => 'customers',
					"alias" => 'Customer',
					"type" => 'inner',
					"conditions" => array('Customer.account_head_id=Sale.account_head_id'),
					),
				),
			'conditions' => array(
				'Sale.status=2',
				'Sale.executive_id=2',
				'Sale.is_erp=0',
				'(Sale.date_of_order)'=>$first_date,
// '(Sale.date_of_order) between ? and ? '=>array($first_date,$last_date),
				),
			'order' => array('Sale.id DESC'),
			'fields' => array(
				'Sale.*',
				'AccountHead.*',
				'Customer.*',
				)
			));
		$this->set('Sale', $Sale);
		$NoSale = $this->NoSale->find('all', array(
			"joins" => array(
				array(
					"table" => 'customers',
					"alias" => 'Customer',
					"type" => 'inner',
					"conditions" => array('Customer.account_head_id=Sale.account_head_id'),
					),
				),
			'conditions' => array(
				'Sale.status=2',
				'Sale.executive_id=2',
				'Sale.is_erp=0',
				'(Sale.date_of_order)'=>$first_date,
// '(Sale.date_of_order) between ? and ? '=>array($first_date,$last_date),
				),
			'order' => array('Sale.id DESC'),
			'fields' => array(
				'Sale.*',
				'AccountHead.*',
				'Customer.*',
				)
			));
		$this->set('Sale', $Sale);
		pr($Sale);exit;
	}
	public function get_executive_credit_limit($id) {
        $return = ['result' => 'Empty'];
        $credit_limit_balance=0;
        $ExecutiveRouteMapping = $this->ExecutiveRouteMapping->findByRouteId($id);
        if ($ExecutiveRouteMapping) {
        	$executive_id=$ExecutiveRouteMapping['ExecutiveRouteMapping']['executive_id'];
        	$executive_credit_limit=$this->Executive->field('executive_credit_limit',array('Executive.id'=>$executive_id));
        	 $Customer = $this->Customer->find('all', array(
                'conditions' => array(
                    'Customer.route_id' => $id
                    ),
                'fields' => array('Customer.credit_limit')
                ));
        	if($Customer)
        	{
        		$credit_limit=0;
        		foreach ($Customer as $key => $value) {
        			$credit_limit=$credit_limit+$value['Customer']['credit_limit'];
        		}
        	}
            if($executive_credit_limit>0)
            {
            	$credit_limit_balance=$executive_credit_limit-$credit_limit;
            }
            $return['result'] = 'Success';
            $return['credit_limit_balance'] = $credit_limit_balance;
        }
        echo json_encode($return);
        exit;
    }
    public function get_route_by_excutive($id=null)
	{
		$return['result']='Error';
		 $conditions=array();
  if(!empty($id))
  {
    $conditions['ExecutiveRouteMapping.executive_id']=$id;
  }
		$return['option']='<option value="">Select</option>';
		$ExecutiveRouteMapping= $this->ExecutiveRouteMapping->find('all', array(
			'conditions' =>$conditions ,
			'order'=>array('Route.name ASC'),
			'fields' => array(
				'Route.id',
				'Route.name',
				)
			)
		);
		foreach ($ExecutiveRouteMapping as $key => $value) {
			$return['result']='Success';
			$return['option']=$return['option'].'<option value='.$value['Route']['id'].'>'.$value['Route']['name'].'</option>';
		}
		echo json_encode($return);
		exit;
	} 
public function ExecutiveRate($id=null)
{
  $this->Executive->unbindModel(array('hasMany'=>array('Sale')));
 $executives=$this->Executive->find('list');
  $this->set(compact('executives'));
	if($this->request->data)
	 {
	 $return['result']='success';
      $data=$this->request->data['ExecutiveRate'];
      $conditions=[];
    try {
      for ($i=0; $i <count($data['new_rate']) ; $i++) 
      {
	      if($data['new_rate'][$i]>0 )
	      {
			$ExecutiveRateCheck = $this->ExecutiveRate->findByProductIdAndExecutiveId($data['product_id'][$i],$data['executive_id']);
			if(empty($ExecutiveRateCheck))
			{
				$ExecutiveRate_Data['product_id']=$data['product_id'][$i];
				$ExecutiveRate_Data['executive_id']=$data['executive_id'];
				$ExecutiveRate_Data['executive_rate']=floatval($data['new_rate'][$i]);
				$ExecutiveRate_Data['updated_at']=date('Y-m-d H:i:s');
				$ExecutiveRate_Data['created_at']=date('Y-m-d H:i:s');
				$this->ExecutiveRate->create();
				if(!$this->ExecutiveRate->save($ExecutiveRate_Data))
					throw new Exception("Cant Edit Executive Rate", 1);
			}
			else
			{
				$this->ExecutiveRate->id = $ExecutiveRateCheck['ExecutiveRate']['id'];
				$ExecutiveRate_Data['executive_rate']=floatval($data['new_rate'][$i]);
				$ExecutiveRate_Data['updated_at']=date('Y-m-d H:i:s');
				if(!$this->ExecutiveRate->save($ExecutiveRate_Data))
			    throw new Exception("Cant Edit Executive Rate", 1);
			}
         }
     }
           
    }catch (Exception $e) 
    {
      $return['result']=$e->getMessage();
    }
     $this->Session->setFlash(__($return['result']));
  }
}
public function ExecutiveRate_Updation()
{
  $requestData=$this->request->data;
  $conditions=[];
  $conditions['Product.type']=[2,8];
   $this->Product->unbindModel(array('hasMany' => array('SalesReturnItem','SaleItem','PurchasedItem','PurchaseReturnItem','UnwantedList','Stock','StockLog')));
			$totalData=$this->Product->find('count', array(
			'conditions'=>$conditions
			));
			$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Product.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->Product->find('count',[
				'conditions'=>$conditions,
			]);
		}
 $this->Product->unbindModel(array('hasMany' => array('SalesReturnItem','SaleItem','PurchasedItem','PurchaseReturnItem','UnwantedList','Stock','StockLog')));
  $Data=$this->Product->find('all',array(
    "joins"=>array(),
    'conditions'=>$conditions,
   // 'offset'=>$requestData['start'],
   //  'limit'=>$requestData['length'],
 'order' =>  array('Product.id desc'),
    'fields'=>array(
      'Product.id',
      'Product.name',
    )
  ));
  foreach ($Data as $key => $value) {
      $ExecutiveRate = $this->ExecutiveRate->findByProductIdAndExecutiveId($value['Product']['id'],$requestData['executive_id']);
      $executive_rate = 0;
      if($ExecutiveRate)
      {
       $executive_rate= $ExecutiveRate['ExecutiveRate']['executive_rate'];
      }
      
  $Data[$key]['Product']['current_rate']='<input type="hidden" value="'.$value['Product']['id'].'" name=data[ExecutiveRate][product_id][]><span>'.floatval($executive_rate).'</span>';
  $Data[$key]['Product']['new_rate']='<input type="text" class="number new_rate" value="" name=data[ExecutiveRate][new_rate][]>';
  }
  $json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
  echo json_encode($json_data);
  exit;
}
}