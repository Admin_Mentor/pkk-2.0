<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Reports');
/**
* Homes Controller
*/
ini_set('memory_limit', '2000M');
class HomesController extends AppController {
/**
* Scaffold
*
* @var mixed
*/
public $uses=[
'Type',
'MasterGroup',
'Group',
'SubGroup',
'AccountHead',
'Sale',
'Purchase',
'Executive',
'Branch',
'Product',
'Stock',
'SaleItem',
'Journal',
'Customer',
];
public function Dashboard()
{

	$countOFF=$this->Sale->find('count',array('conditions'=>array('status=1','flag=1')));
	$countP=$this->Purchase->find('count',array('conditions'=>array('Purchase.status=1','Purchase.flag=1')));
	$from = date('Y-m-d',strtotime('first day of this month'));
	$to = date('Y-m-d',strtotime('last day of this month'));
	$conditions=[];
	$conditions_warehouse=[];
	$user_branch_id=$this->Session->read('User.branch_id');
	if($user_branch_id)
	{
		$branch_warehouse=$this->Branch->findById($user_branch_id);
		$conditions['Sale.branch_id']=$user_branch_id;
		$conditions_warehouse['Stock.warehouse_id']=$branch_warehouse['Branch']['warehouse_id'];;
	}
	$conditions['Sale.status']=2;
	$conditions['Sale.date_of_delivered between ? and ?']=array($from,$to);
	
	$this->Sale->unbindModel(array('belongsTo'=>array('AccountHead','Executive'),'hasMany' => array('SaleItem')));
	$this->Sale->virtualFields = array(
		'total_grand_amount' => "SUM(Sale.taxable_amount)",
		);
	$Sale=$this->Sale->find('first',array(
		'conditions'=>$conditions,
		'fields'=>array('total_grand_amount'),
		));
	$net_amount=floatval($Sale['Sale']['total_grand_amount']?$Sale['Sale']['total_grand_amount']:0);
	// $Customer=$this->Customer->find('list',array(
	// 	'fields'=>[
	// 	'Customer.account_head_id',
	// 	'Customer.account_head_id'
	// 	]
	// 	));
	$collection=0;
	// foreach ($Customer as $key => $value) {
	// 	$collection+=$this->get_collection_amount($key,$from,$to);
	// }
	$this->Stock->virtualFields = array(
		'line_total' => "SUM(Stock.quantity*Product.cost)",
		);
	$linetotal=0;
	if($this->Session->read('User.id'))
	{
       $StockData=$this->Stock->find('first',array(
		'conditions'=>$conditions_warehouse,
		'fields'=>array('line_total'),
		));	
       $linetotal=$StockData['Stock']['line_total'];
   }
	$totalPending_count=$countOFF;
	$ReportsController = new ReportsController;
	$Sales_month=$ReportsController->SalesBar($user_branch_id,$this->Session->read('User.id'));
	$Purchase_month=$ReportsController->PurchaseBarGraph($this->Session->read('User.id'));
	//$BrandGraph=$ReportsController->BrandGraph();
	//$pieChart=$ReportsController->pieChart($user_branch_id);
	$expensegraph=$ReportsController->expensegraph($user_branch_id);
	$this->set('Sales_month',$Sales_month);
	$this->set('Purchase_month',$Purchase_month);
	//$this->set('BrandGraph',$BrandGraph);
	//$this->set('pieChart',$pieChart);
	//$this->set('Pending_Saleorders',$totalPending_count);
	//$this->set('Pending_Purchaseorders',$countP);
	$this->set('net_amount',ROUND($net_amount));
	$this->set('collection',ROUND($collection));
	$this->set('stockcost',ROUND($linetotal));
	$this->set('expense',ROUND($expensegraph));
}
public function get_collection_amount($account_head_id,$from_date=null,$to_date=null) {
	$conditions=array();
	if(!empty($from_date)){
		$conditions['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
	}
	$user_branch_id=$this->Session->read('User.branch_id');
	if($user_branch_id)
	{
		$conditions['Journal.branch_id']=$user_branch_id;
	}
	$conditions['Journal.credit']=$account_head_id;
	$conditions['AccountHeadDebit.sub_group_id']=['1','2'];
	$conditions['Journal.flag']=1;
	$this->Journal->virtualFields = array( 'total_amount' => "SUM(Journal.amount)" );
	$Journal=$this->Journal->find('first',array('conditions'=>$conditions,
		'fields'=>array('Journal.total_amount')
		));
	$sale_total_amount=0;
	if($Journal['Journal']['total_amount'])
	{
		$sale_total_amount=floatval($Journal['Journal']['total_amount']);
	}
	return $sale_total_amount;
}
public function Master()
{	
}
public function Database(){
		$path = ROOT.'/app/webroot/sql';
		$files = scandir($path);
		// print_r($files);die;
		$this->set('files',$files);
		// foreach ($files as &$value) {
		// 	echo "<a href='http://localhost/".$value."' target='_blank' >".$value."</a><br/><br/>";
		// }
	}
	public function DbBackup(){
		// Database configuration
		// $host = "koopell.chspkhb6sqyz.ap-south-1.rds.amazonaws.com";
		// $username = "koopell";
		// $password = "K00pell8459";
		// $database_name = "koopell";
		$host = "database-1.cinmwcfssiz6.ap-south-1.rds.amazonaws.com";
		$username = "admin";
		$password = 'Mentor$fw6523$ff';
		$database_name = "pkk";
		// Get connection object and set the charset
		$conn = mysqli_connect($host, $username, $password, $database_name);
		$conn->set_charset("utf8");
		// Get All Table Names From the Database
		$tables = array();
		$sql = "SHOW TABLES";
		$result = mysqli_query($conn, $sql);
		while ($row = mysqli_fetch_row($result)) {
			$tables[] = $row[0];
		}
		$sqlScript = "";
		foreach ($tables as $table) {
				// Prepare SQLscript for creating table structure
			$query = "SHOW CREATE TABLE $table";
			$result = mysqli_query($conn, $query);
			$row = mysqli_fetch_row($result);
			$sqlScript .= "\n\n" . $row[1] . ";\n\n";
			$query = "SELECT * FROM $table";
			$result = mysqli_query($conn, $query);
			$columnCount = mysqli_num_fields($result);
			// Prepare SQLscript for dumping data for each table
			for ($i = 0; $i < $columnCount; $i ++) {
				while ($row = mysqli_fetch_row($result)) {
					$sqlScript .= "INSERT INTO $table VALUES(";
					for ($j = 0; $j < $columnCount; $j ++) {
						$row[$j] = $row[$j];
					if (isset($row[$j])) {
							$sqlScript .= '"' . $row[$j] . '"';
						} else {
							$sqlScript .= '""';
						}
						if ($j < ($columnCount - 1)) {
							$sqlScript .= ',';
						}
					}
					$sqlScript .= ");\n";
				}
			}
			
			$sqlScript .= "\n"; 
		}
		if(!empty($sqlScript))
		{
			// Save the SQL script to a backup file
			$backup_file_name = $database_name . '_backup_' . date('d-m-Y_H_i') . '.sql';
			// $fileHandler = fopen($backup_file_name, 'w+');
			// $number_of_lines = fwrite($fileHandler, $sqlScript);
			// fclose($fileHandler); 
			// Download the SQL backup file to the browser
			// header('Content-Description: File Transfer');
			// header('Content-Type: application/octet-stream');
			// header('Content-Disposition: attachment; filename=' . basename($backup_file_name));
			// header('Content-Transfer-Encoding: binary');
			// header('Expires: 0');
			// header('Cache-Control: must-revalidate');
			// header('Pragma: public');
			// header('Content-Length: ' . filesize($backup_file_name));
			ob_clean();
			flush();
			
			$db_backup_path=ROOT.'/app/webroot/sql';
			if (!is_dir ( $db_backup_path )) {
				mkdir ( $db_backup_path, 0777, true );
			}
			readfile($db_backup_path . '/' . $backup_file_name);
			exec('rm ' . $db_backup_path . '/' . $backup_file_name);
			// $fp = fopen($db_backup_path . '/' . $backup_file_name ,'w+');
			// if (($result = fwrite($fp, $contents))) {
			// 	echo "Backup file created '--$backup_file_name' ($result)"; 
			// }
			// fclose($fp); 
			file_put_contents ( ROOT.'/app/webroot/sql/' . $backup_file_name , $sqlScript );
			
		}
		return $this->redirect(array('controller' => 'Homes', 'action' => 'Database'));
	}
	public function DbDelete(){
		$name=$this->request->data['name'];
		$path=ROOT.'/app/webroot/sql/'.$name;
		if( file_exists($path) ){ 
			// Remove file 
			unlink($path); 
	  
			// Set status 
			$return=1; 
		 }else{ 
			// Set status 
			$return=0; 
		 }
		echo json_encode($return);
		exit;
	}

}
