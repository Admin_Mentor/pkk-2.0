<?php 
App::uses('Controller', 'Controller');
App::uses('CakeTime', 'Utility');
App::uses('AppController', 'Controller');
App::uses('AccountingsController', 'Controller');
App::uses('AccountsController', 'Controller');
class HrController extends AppController{
	public $uses=[
		'AccountHead',
		'Staff',
		'BasicPay',
		'PayStructure',
		'Role',
		'StaffBonus',
		'Attendance',
		'AttendanceFreeze',
		'CompanyHoliday',
		'OverTime',
		'DocumentType',
        'StaffDocument',
        'VehicleNo',
        'SystemParameter',
        'AccLedger',
        'Executive'
	];
	public function Staff()
	{
		$roles=$this->Role->find('list',['order'=>['name']]);
		$this->set(compact('roles'));
	}
	public function Staff_Table_ajax()
	{
		$requestData=$this->request->data;		$columns = [];
		$columns[]='Staff.id';
		$columns[]='Staff.name';
		$columns[]='Staff.code';
		$columns[]='Role.name';
		$columns[]='Staff.contact_no';
		$columns[]='Staff.no_of_working_days';
		$columns[]='Staff.payment_mode';
		$columns[]='Staff.date_of_joining';
		$columns[]='Staff.address';
		$columns[]='Staff.bonus_head_id';
		$columns[]='Staff.paid_account_head_id';
		$columns[]='Staff.outstanding_account_head_id';
		$columns[]='Staff.prepaid_account_head_id';
		$columns[]='Staff.id';
		$conditions=[];
		if(isset($requestData['role_id'])) {
			if($requestData['role_id']) $conditions['role_id']=$requestData['role_id'];
		}
		if(isset($requestData['grade'])) {
			if($requestData['grade']) $conditions['grade']=$requestData['grade'];
		}
		if(isset($requestData['payment_mode'])) {
			if($requestData['payment_mode']) $conditions['payment_mode']=$requestData['payment_mode'];
		}
		$totalData=$this->Staff->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Staff.date_of_joining LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
				'Staff.month_leave LIKE'     =>'%'. $q . '%',
				'Staff.name LIKE'            =>'%'. $q . '%',
				'Role.name LIKE'             =>'%'. $q . '%',
				'Staff.code LIKE'            =>'%'. $q . '%',
				'Staff.contact_no LIKE'      =>'%'. $q . '%',
				'Staff.address LIKE'         =>'%'. $q . '%',
				'Staff.id LIKE'              =>'%'. $q . '%',
			);
			$totalFiltered=$this->Staff->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->Staff->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Staff.*',
				'Role.*',
				'Paid.name',
				'Outstanding.name',
				'PrePaid.name',
				'Bonus.name',
			)
		));
		foreach ($Data as $key => $value) {
			$Data[$key]['Staff']['date_of_joining']=date('d-m-Y',strtotime($value['Staff']['date_of_joining']));
			$Data[$key]['Staff']['action']         ='<span><i table_id="'.$value['Staff']['id'].'" class="fa fa-2x fa-edit  edit_staff   blue-col"></i></span>&nbsp;&nbsp;';
			//$Data[$key]['Staff']['action']        .="<span><i table_id='".$value['Staff']['id']."' class='fa fa-2x fa-trash delete_staff blue-col'></i></span>";
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function staff_add_ajax()
	{
		$data=$this->request->data['Staff'];
		$name = trim($data['name']);
		$data['name']=strtoupper(trim($data['name']));
		$data['date_of_joining']=date('Y-m-d',strtotime($data['date_of_joining']));
		$datasource_Staff = $this->Staff->getDataSource();
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		try {
			$datasource_Staff->begin();
			$datasource_AccountHead->begin();
			if(!$data['id'])
			{
				unset($data['id']);
				$sub_group_id =35;
				$StaffRow=$this->Staff->find('first',array('order'=>array('Staff.id'=>'DESC')));
				if($StaffRow)
				{
					$code=$StaffRow['Staff']['code'];
					$code+=1;
				}
				else
				{
					$code='1000';
				}
				$data['code'] = $code; 
				$opening_balance = 0;
				$date = date('Y-m-d');
				$description = "";
				 $salary_id=$this->SystemParameter->field('value',array('id'=>13));
				  $main_group_id=3;
				$name=strtoupper(trim($data['name'])).' '.'STAFF';
			$date=date('d-m-Y');
			  $AccountsController = new AccountsController;
			$function_return = $AccountsController->AccountHeadCreate($main_group_id,$salary_id, $name, 0, $date, '');
			 if ($function_return['result'] != 'Success')
                throw new Exception($function_return['message']);
              $AccountHead_id = $this->AccountHead->getLastInsertId();
               $LedgerRow = $this->AccLedger->find('first', array(
                'order' => array('AccLedger.id' => 'DESC') ));
            if(!empty($LedgerRow))
            {
                $ledger_code = $LedgerRow['AccLedger']['code']+1;
            }
            else{
                $ledger_code = 1001;
            }
			$Ledger_data = [
            'name'=>$name,
            'code'=>$ledger_code,
            'account_head_id' => $AccountHead_id,
            'sub_group_id' => 15,
            'main_group_id'=>3,
            'opening_balance' => 0,
            'other_notes' => '',
            'created_at' => date('Y-m-d H:i:s', strtotime($date)),
            'updated_at' => date('Y-m-d H:i:s', strtotime($date)),
            ];
            $this->AccLedger->create();
            if (!$this->AccLedger->save($Ledger_data)){
                throw new Exception("Error Ledger Creation", 1);
            }
				// $function_return = $this->expense_head($name,$opening_balance,$date,$description);
				// if ($function_return['result'] != 'Success')
				// 	throw new Exception($function_return['message']);
				// $bonus_head_id = $function_return['bonus_head_id'];
				// $AccountHead_paid_id = $function_return['AccountHead_paid_id'];
				// $AccountHead_prepaid_id = $function_return['AccountHead_prepaid_id'];
				// $AccountHead_outstanding_id = $function_return['AccountHead_outstanding_id'];
				$bonus_head_id =0;
				$AccountHead_paid_id =0;
				$AccountHead_prepaid_id = 0;
				$AccountHead_outstanding_id = 0;
				$data['bonus_head_id'] = $bonus_head_id;
				$data['account_head_id'] = $AccountHead_id;
				$data['paid_account_head_id'] = $AccountHead_paid_id;
				$data['prepaid_account_head_id'] = $AccountHead_prepaid_id;
				$data['outstanding_account_head_id'] = $AccountHead_outstanding_id;
				$this->Staff->create();
				if(!$this->Staff->save($data))
				{
					$errors = $this->Staff->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
			}
			else
			{
				$this->Staff->id=$data['id'];
				$this->Staff->saveField('contact_no',$data['contact_no']);
				$this->Staff->saveField('no_of_working_days',$data['no_of_working_days']);
				$this->Staff->saveField('payment_mode',$data['payment_mode']);
				$this->Staff->saveField('grade',$data['grade']);
				$this->Staff->saveField('address',$data['address']);
				$this->Staff->saveField('date_of_joining',$data['date_of_joining']);
				$this->Executive->id=$this->Executive->field('Executive.id',array('Executive.staff_id'=>$data['id']));
               if (!$this->Executive->saveField('mobile',$data['contact_no'])) 
                throw new Exception("Error in Executive Mobile Edit.", 1);
			}
			$datasource_Staff->commit();
			$datasource_AccountHead->commit();
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
			$datasource_Staff->rollback();
			$datasource_AccountHead->rollback();
		}
		echo json_encode($return); exit;
	}
	public function expense_head($name,$opening_balance,$date,$description){
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		$AccountingsController = new AccountingsController;
		try {
			$datasource_AccountHead->begin();
			$name=trim($name);
			$date=date('Y-m-d');
			$group_id=13; $sub_group_id_paid       = $AccountingsController->GetSubGroupIdByGroupIdAndSubGroupName($group_id,'SALARYACCOUNT PAID');
			$group_id=5;  $prepaid_sub_group_id    = $AccountingsController->GetSubGroupIdByGroupIdAndSubGroupName($group_id,'SALARYACCOUNT PREPAID');
			$group_id=20; $outstanding_subgroup_id = $AccountingsController->GetSubGroupIdByGroupIdAndSubGroupName($group_id,'SALARYACCOUNT OUTSTANDING');
			$function_return=$AccountingsController->AccountHeadCreate($sub_group_id_paid,$name.'BONUS PAID',0,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['bonus_head_id'] = $this->AccountHead->getLastInsertId();
			$function_return=$AccountingsController->AccountHeadCreate($sub_group_id_paid,$name.' PAID',$opening_balance,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['AccountHead_paid_id'] = $this->AccountHead->getLastInsertId();
			$prepaid_name=$name.' PREPAID';
			$function_return=$AccountingsController->AccountHeadCreate($prepaid_sub_group_id,$prepaid_name,'0',$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['AccountHead_prepaid_id'] = $this->AccountHead->getLastInsertId();
			$outstanding_name=$name.' OUTSTANDING';
			$function_return=$AccountingsController->AccountHeadCreate($outstanding_subgroup_id,$outstanding_name,'0',$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['AccountHead_outstanding_id'] = $this->AccountHead->getLastInsertId();
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		return $return;
	}
	public function Get_Staff_ajax($id)
	{
		$Staff=$this->Staff->findById($id);
		$Staff['Staff']['date_of_joining']=date('d-m-Y',strtotime($Staff['Staff']['date_of_joining']));
		echo json_encode($Staff); exit;
	}
	public function Delete_Staff_ajax($id)
	{
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		$datasource_Staff       = $this->Staff->getDataSource();
		try {
			$datasource_AccountHead->begin();
			$datasource_Staff->begin();
			$Staff=$this->Staff->findById($id);
			$paid_account_head_id=$Staff['Staff']['paid_account_head_id'];
			$prepaid_account_head_id=$Staff['Staff']['prepaid_account_head_id'];
			$outstanding_account_head_id=$Staff['Staff']['outstanding_account_head_id'];

			$Credit=$this->Journal->findByCredit($paid_account_head_id);
			$Debit=$this->Journal->findByDebit($paid_account_head_id);
			if($Credit || $Debit)
			{
				throw new Exception("Used In Journal So Cant Delete", 1);
			}
			$Credit=$this->Journal->findByCredit($prepaid_account_head_id);
			$Debit=$this->Journal->findByDebit($prepaid_account_head_id);
			if($Credit || $Debit)
			{
				throw new Exception("Used In Journal So Cant Delete", 1);
			}
			$Credit=$this->Journal->findByCredit($outstanding_account_head_id);
			$Debit=$this->Journal->findByDebit($outstanding_account_head_id);
			if($Credit || $Debit)
			{
				throw new Exception("Used In Journal So Cant Delete", 1);
			}
			if(!$this->AccountHead->delete($paid_account_head_id)) throw new Exception("Cant Delete paid_account_head_id", 1);
			if(!$this->AccountHead->delete($prepaid_account_head_id)) throw new Exception("Cant Delete prepaid_account_head_id", 1);
			if(!$this->AccountHead->delete($outstanding_account_head_id)) throw new Exception("Cant Delete outstanding_account_head_id", 1);
			if(!$this->Staff->delete($id)) throw new Exception("Cant Delete This Staff", 1);
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$datasource_Staff->commit();
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$datasource_Staff->rollback();
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function BasicPay($id=null)
	{
		$this->Staff->virtualFields = array('staff_name' => "CONCAT(Staff.name, ' ', Staff.code)");
		$staffs=$this->Staff->find('list', array('fields'=>['id','staff_name']));
		$this->set(compact('staffs'));
		if(!$this->request->data)
		{
			if($id)
			{
				$BasicPay=$this->BasicPay->findById($id);
				$this->request->data['BasicPay']['wef_date']=date('m-Y',strtotime($BasicPay['BasicPay']['wef_date']));
				$this->request->data['BasicPay']['staff_id']=$BasicPay['BasicPay']['staff_id'];
				$this->request->data['BasicPay']['amount']=floatval($BasicPay['BasicPay']['amount']);
				$this->request->data['BasicPay']['remarks']=$BasicPay['BasicPay']['remarks'];
			}
			else
			{
				$this->request->data['BasicPay']['wef_date']=date('m-Y');
			}
		}
		else
		{
			$data=$this->request->data['BasicPay'];
			$data['wef_date']=date('Y-m-d',strtotime('01-'.$data['wef_date']));
			$datasource_BasicPay = $this->BasicPay->getDataSource();
			try {
				$datasource_BasicPay->begin();
				if($id) $this->BasicPay->id=$id;
				else 
				{
					$BasicPay=$this->BasicPay->findByStaffIdAndWefDate($data['staff_id'],$data['wef_date']);
					if($BasicPay) throw new Exception("Duplicate Entry", 1);
					$this->BasicPay->create();
				}
				if(!$this->BasicPay->save($data))
				{
					$errors = $this->BasicPay->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
				$datasource_BasicPay->commit();
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_BasicPay->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			return $this->redirect(array('action' => 'BasicPay'));
		}
	}
	public function BasicPay_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='BasicPay.wef_date';
		$columns[]='Staff.name';
		$columns[]='Staff.code';
		$columns[]='BasicPay.remarks';
		$columns[]='BasicPay.amount';
		$columns[]='BasicPay.id';
		$conditions=[];
		if($requestData['staff_id']) $conditions['staff_id']=$requestData['staff_id'];
		if($requestData['date'])     $conditions['wef_date between ? and ?']=[date('Y-m-1',strtotime($requestData['date'])),date('Y-m-t',strtotime($requestData['date']))];
		$totalData=$this->BasicPay->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'BasicPay.wef_date LIKE' =>'%'. date('Y-m',strtotime($q)) . '%',
				'BasicPay.amount LIKE' =>'%'. $q . '%',
				'BasicPay.remarks LIKE' =>'%'. $q . '%',
				'Staff.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->BasicPay->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->BasicPay->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'BasicPay.*',
				'Staff.name',
				'Staff.code',
			)
		));
		foreach ($Data as $key => $value) {
			$Data[$key]['BasicPay']['amount']=floatval($value['BasicPay']['amount']);
			$Data[$key]['BasicPay']['wef_date']=date('m-Y',strtotime($value['BasicPay']['wef_date']));
			$Data[$key]['BasicPay']['action']='<span data-id="'.$value['BasicPay']['id'].'"><a href="'.$this->webroot.'Hr/BasicPay/'.$value['BasicPay']['id'].'"><i class="fa fa-2x fa-edit"></i></a></span>&nbsp;&nbsp;<a>';
			$Data[$key]['BasicPay']['action'].="<a onclick='return confirm('Are you sure?')' href='".$this->webroot."Hr/DeleteBasicPay/".$value['BasicPay']['id']."'><i class='fa fa-2x fa-trash blue-col'></i></a>";
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data);
		exit;
	}
	public function DeleteBasicPay($id)
	{
		try {
			if(!$this->BasicPay->delete($id))
				throw new Exception("Cant Delete This Pay Structure", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		return $this->redirect(array('action' => 'BasicPay'));
	}
	public function PayStructure()
	{
		if(!$this->request->data){
			$this->Staff->virtualFields = array('staff_name' => "CONCAT(Staff.name, ' ', Staff.code)");
			$staffs=$this->Staff->find('list', array('conditions'=>['payment_mode'=>'Salary'],'fields'=>['id','staff_name']));
			$month = date('m-Y',strtotime("-1 month"));
			$date = date('d-m-Y');
			$this->request->data['month']=$month;
			$this->request->data['date']=$date;
			$this->set(compact('staffs'));
		}
		else
		{
			$data = $this->request->data;
			try {
				if(!$data['staff_id']) throw new Exception("Empty Staff Id", 1);
				if(!$data['month'])    throw new Exception("Empty Month", 1);
				$StaffRow=$this->Staff->findById($data['staff_id']);
				$outstanding_credit=$StaffRow['Staff']['account_head_id'];
				$outstanding_debit =1;
				$outstanding_amount=$data['net_salary'];
				$date       = date('Y-m-d');
				$date_of_pay= date('Y-m-d',strtotime($data['date']));
				$remarks    = $StaffRow['Staff']['code'].':' .$data['month'].'Pay structure Created';
				$work_flow  = "Pay Structure";
				$user_id=$this->Session->read('User.id');
				$AccountingsController = new AccountingsController;
				$voucher_no=$AccountingsController->GetVoucherNo();
				$Journal_data=[
					'remarks'         =>$remarks,
					'credit'          =>$outstanding_credit,
					'debit'           =>$outstanding_debit,
					'amount'          =>$outstanding_amount,
					'date'            =>$date_of_pay,
					'work_flow'       =>$work_flow,
					'voucher_no'      =>$voucher_no,
					'external_voucher'=>"",
					'flag'            =>'1',
					'created_by'      =>$user_id,
					'modified_by'     =>$user_id,
					'created_at'      =>date('Y-m-d H:i:s'),
					'updated_at'      =>date('Y-m-d H:i:s'),
				];
				$this->Journal->create();
				if(!$this->Journal->save($Journal_data))
				{
					$errors = $this->Journal->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
				$pay_data=[
					'staff_id'        =>$data['staff_id'],
					'month_year'      =>$data['month'],
					'date_of_pay'     =>date('Y-m-d',strtotime($data['date'])),
					'basic_salary'    =>$data['basic_salary'],
					'bonus'           =>$data['bonus'],
					'allowance'       =>$data['allowance'],
					'salary_deduction'=>$data['salary_deduction'],
					'deduction'       =>$data['deduction'],
					'net_salary'      =>$data['net_salary'],
					'journal_id'      =>$this->Journal->getLastInsertId(),
				];
				$this->Staff->id=$data['staff_id'];
				if(!$this->Staff->saveField('carry_leave',0))
					throw new Exception("Cant Update the staff carry leave", 1);
				$this->PayStructure->create();
				if(!$this->PayStructure->save($pay_data))
				{
					$errors = $this->PayStructure->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$return['result']='Success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect(Router::url($this->referer(),true));
		}
	}
	public function PayStructure_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='Staff.name';
		$columns[]='PayStructure.date_of_pay';
		$columns[]='PayStructure.month_year';
		$columns[]='PayStructure.basic_salary';
		$columns[]='PayStructure.bonus';
		$columns[]='PayStructure.allowance';
		$columns[]='PayStructure.salary_deduction';
		$columns[]='PayStructure.deduction';
		$columns[]='PayStructure.net_salary';
		$columns[]='PayStructure.action';
		$conditions=[];
		if($requestData['staff_id'])
		{
			$conditions['Staff.id']=$requestData['staff_id'];
		}		
		if($requestData['month'])
		{
			$conditions['PayStructure.month_year']=$requestData['month'];
		}	
		$conditions['PayStructure.flag']=1;	
		$totalData=$this->PayStructure->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'PayStructure.date_of_pay      LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
				'PayStructure.month_year       LIKE' =>'%'. date('m-Y',strtotime($q)) . '%',
				'PayStructure.basic_salary     LIKE' =>'%'. $q . '%',
				'PayStructure.bonus            LIKE' =>'%'. $q . '%',
				'PayStructure.allowance        LIKE' =>'%'. $q . '%',
				'PayStructure.salary_deduction LIKE' =>'%'. $q . '%',
				'PayStructure.deduction        LIKE' =>'%'. $q . '%',
				'PayStructure.net_salary       LIKE' =>'%'. $q . '%',
				'Staff.name                    LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->PayStructure->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->PayStructure->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'PayStructure.*',
				'Staff.name',
			)
		));
		foreach ($Data as $key => $value) {
			if($value['PayStructure']['date_of_pay']!="") $Data[$key]['PayStructure']['date_of_pay']=date('d-m-Y',strtotime($value['PayStructure']['date_of_pay']));
			else $Data[$key]['PayStructure']['date_of_pay']="";			
			$Data[$key]['PayStructure']['basic_salary']    =floatval($value['PayStructure']['basic_salary']);
			$Data[$key]['PayStructure']['bonus']           =floatval($value['PayStructure']['bonus']);
			$Data[$key]['PayStructure']['allowance']       =floatval($value['PayStructure']['allowance']);
			$Data[$key]['PayStructure']['salary_deduction']=floatval($value['PayStructure']['salary_deduction']);
			$Data[$key]['PayStructure']['deduction']       =floatval($value['PayStructure']['deduction']);
			$Data[$key]['PayStructure']['net_salary']      =floatval($value['PayStructure']['net_salary']);
			$Data[$key]['PayStructure']['action']          ="<a onclick='return confirm('Are you sure?')' href='".$this->webroot."Hr/DeletePaystructure/".$value['PayStructure']['id']."'><i class='fa fa-2x fa-trash blue-col'></i></a>";
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data);
		exit;
	}
	public function get_staff_pay_ajax()
	{
		$data['Staff_id']=2;
		$data['month']=date('m-Y');
		$data = $this->request->data;
		$id=$data['Staff_id'];
		$month=$data['month'];
		$return=['status'=>'Empty'];
		try {
			if(!$id) throw new Exception("Empty Staff_id", 1);
			$RowCount=$this->PayStructure->findByStaffIdAndMonthYearAndFlag($id,$month,1);
			if($RowCount) throw new Exception("Already Created", 1);
			$date = strtotime('01-'.$month);
			$first_date = date('Y-m-1',strtotime('01-'.$month));
			$basic_pay_amount = $this->basic_pay_amount($id,$first_date);
			if(!$basic_pay_amount) throw new Exception("Set Basic Pay", 1);
			$return['basic_pay_amount']=floatval($basic_pay_amount);
			$StaffBonus=$this->StaffBonus->findByStaffIdAndMonth($id,date('Y-m-1',strtotime('01-'.$month)));
			$bonus=0;
			if($StaffBonus) { $bonus=$StaffBonus['StaffBonus']['bonus']; }
			$return['bonus']=floatval($bonus);
			$BasicPay=$this->BasicPay->find('first',array(
				'conditions'=>array(
					'(BasicPay.wef_date) <='=>$first_date,
					'staff_id'=>$id
				),
			));
			$return['no_of_working_days']=floatval($BasicPay['Staff']['no_of_working_days']);
			$CompanyHoliday=$this->CompanyHoliday->findByStaffIdAndMonth($data['Staff_id'],$first_date);
			$Attendance=$this->Attendance->findByStaffIdAndMonth($data['Staff_id'],$first_date);
			$return['worked_days']=0;
			if($Attendance) {
				if($Attendance['Attendance']['full_day']) {
					$return['worked_days']+=count(explode(',', $Attendance['Attendance']['full_day']));
				}
				if($Attendance['Attendance']['half_day']) {
					$return['worked_days']+=count(explode(',', $Attendance['Attendance']['half_day']))/2;
				}
			}
			if($CompanyHoliday) {
				$return['worked_days']+=count(explode(',', $CompanyHoliday['CompanyHoliday']['holidays']));
			}
			$return['leave_days']=$return['no_of_working_days']-$return['worked_days'];

			$from=date('Y-m-1',strtotime('01-'.$month));
			$to=date('Y-m-t',strtotime('01-'.$month));
			$this->OverTime->virtualFields = array('total_amount' => "SUM(OverTime.amount)");
			$OverTime=$this->OverTime->find('first',[
				'conditions'=>[
					'staff_id'=>$id,
					'date between ? and ?'=>[$from,$to],
				]
			]);
			$return['over_time']=floatval($OverTime['OverTime']['total_amount']);
			if(!$BasicPay) throw new Exception("Empty BasicPay", 1);
			$return['status']='Success';
		} catch (Exception $e) {
			$return['status']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function basic_pay_amount($id,$first_date){
		$BasicPayList=$this->BasicPay->find('first',array(
			'conditions'=>array(
				'(BasicPay.wef_date) <='=>$first_date,
				'staff_id'=>$id
			),
			'fields'=>'max(BasicPay.wef_date) as date',
		));
		$basic_pay = $this->BasicPay->field('BasicPay.amount',array(
			'BasicPay.wef_date'=>$BasicPayList[0]['date'],
			'staff_id'=>$id
		));
		return $basic_pay;
	}
	public function DeletePaystructure($id)
	{
		$datasource_Paystructure = $this->PayStructure->getDataSource();
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Paystructure->begin();
			$datasource_Journal->begin();
			$this->PayStructure->id=$id;
			$PayStructure=$this->PayStructure->findAllById($id,[
				'PayStructure.id','PayStructure.journal_id',
			]);
			if(!$this->PayStructure->saveField('flag','0'))
				throw new Exception("Error Processing Request", 1);
			if(!empty($PayStructure[0]['PayStructure']['journal_id']))
			{
				$this->Journal->id=$PayStructure[0]['PayStructure']['journal_id'];
				if(!$this->Journal->saveField('flag','0'))
					throw new Exception("Error Processing Request", 1);
			}
			$datasource_Paystructure->commit();
			$datasource_Journal->commit();
			$return['result']='Success';
		} catch (Exception $e) {
			$datasource_Paystructure->rollback();
			$datasource_Journal->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		return $this->redirect(array('action' => 'PayStructure'));
	}
	public function Role()
	{
	}
	public function Role_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='id';
		$columns[]='name';
		$columns[]='id';
		$conditions=[];
		$totalData=$this->Role->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Role.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->Role->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->Role->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Role.*',
			)
		));
		foreach ($Data as $key => $value) {
			$Data[$key]['Role']['action']='';
			if(!$value['Role']['freeze'])
			{
				$Data[$key]['Role']['action'] ='<span><i table_id="'.$value['Role']['id'].'" class="fa fa-2x fa-edit   edit_staff   blue-col"></i></span>&nbsp;&nbsp;';
				$Data[$key]['Role']['action'] .="<span><i table_id='".$value['Role']['id']."' class='fa fa-2x fa-trash delete_staff blue-col'></i></span>";
			}
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function Role_Save_ajax()
	{
		try {
			$data=$this->request->data['Role'];
			$data['name']=strtoupper(trim($data['name']));
			$Role=$this->Role->findByName($data['name']);
			if(empty($Role))
			{
			if(!$data['id'])
			{
				$this->Role->create();
				if(!$this->Role->save($data))
				{
					$errors = $this->Role->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
			}
			else
			{
				$this->Role->id=$data['id'];
				$this->Role->saveField('name',$data['name']);
			}
						$return['result']='success';
		   }
		   else
		   {
			$return['result']='Role Already Exist';

		   }
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function Get_Role_ajax($id)
	{
		$Role=$this->Role->findById($id);
		echo json_encode($Role); exit;
	}
	public function Delete_Role_ajax($id)
	{
		try {
			if(!$this->Role->delete($id)) throw new Exception("Cant Delete", 1);
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function BonusConfiguration()
	{
		$this->Staff->virtualFields = array('staff_name' => "CONCAT(Staff.name, ' ', Staff.code)");
		$staffs=$this->Staff->find('list', array('fields'=>['id','staff_name']));
		$roles=$this->Role->find('list',['order'=>['name']]);
		$this->set(compact('staffs','roles'));
	}
	public function Bonus_Table_ajax()
	{
		$requestData=$this->request->data;
		$conditions=[];
		if($requestData['role_id'])  $conditions['role_id']=$requestData['role_id'];
		if($requestData['staff_id']) $conditions['Staff.id']=$requestData['staff_id'];
		$totalData=$this->Staff->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		$Data=$this->Staff->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'fields'=>array(
				'Staff.*',
				'Role.*',
			)
		));
		foreach ($Data as $key => $value) {
			$Data[$key]['StaffBonus']['action']='<span><i table_id="'.$value['Staff']['id'].'" class="fa fa-2x fa-edit edit_staff blue-col"></i></span>';
			$Data[$key]['Staff']['job_card']=1;
			$Data[$key]['Staff']['service']=1;
			$Data[$key]['StaffBonus']['bonus']=0;
			$StaffBonus=$this->StaffBonus->findByStaffIdAndMonth($value['Staff']['id'],date('Y-m-1',strtotime($requestData['date'])));
			if($StaffBonus)
			{
				$Data[$key]['StaffBonus']['bonus']=floatval($StaffBonus['StaffBonus']['bonus']);
			}
			if($value['Staff']['id']==$requestData['table_id'])
			{
				$Data[$key]['StaffBonus']['action']='<span><i staff_id="'.$value['Staff']['id'].'" month="'.date('Y-m-1',strtotime($requestData['date'])).'" class="fa fa-2x fa-check ok_staff blue-col"></i></span>';
				$Data[$key]['StaffBonus']['bonus'] ="<input type='text' class='form-control number' value='".$Data[$key]['StaffBonus']['bonus']."' id='staff_bonus'>";
			}
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function Bonus_Update_Function()
	{
		try {
			$data=$this->request->data;
			$StaffBonus=$this->StaffBonus->findByStaffIdAndMonth($data['staff_id'],date('Y-m-1',strtotime($data['month'])));
			if($StaffBonus)
			{
				$this->StaffBonus->id=$StaffBonus['StaffBonus']['id'];
				$this->StaffBonus->saveField('bonus',floatval($data['bonus']));
			}
			else
			{
				$this->StaffBonus->create();
				if(!$this->StaffBonus->save($data))
				{
					$errors = $this->StaffBonus->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
			}
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function RoleWiseBonus_Update_Function()
	{
		try {
			$data=$this->request->data;
			$bonus=floatval($data['bonus']);
			$data['month']=date('Y-m-1',strtotime($data['month']));
			$Staff=$this->Staff->findAllByRoleId($data['role_id']);
			foreach ($Staff as $key => $value) {
				$StaffBonus=$this->StaffBonus->findByStaffIdAndMonth($value['Staff']['id'],date('Y-m-1',strtotime($data['month'])));
				if($StaffBonus) {
					$this->StaffBonus->id=$StaffBonus['StaffBonus']['id'];
					$this->StaffBonus->saveField('bonus',$bonus);
				} else {
					$data['staff_id']=$value['Staff']['id'];
					$this->StaffBonus->create();
					if(!$this->StaffBonus->save($data)) {
						$errors = $this->StaffBonus->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
					}
				}
			}
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function SalaryPayment()
	{
		$AccountingsController = new AccountingsController;
		$user_id=1;
		$mode_catagory=[
			'5' =>'Cash',
			'6' =>'Bank',
			'15'=>'SALARY',
		];
		$from_date=date('d-m-Y',strtotime('-1 month'));
			$to_date=date('d-m-Y');
			$this->set('from_date',$from_date);
			$this->set('to_date',$to_date);
		if(!$this->request->data)
		{
			$accountHeads=$this->Staff->find('list',['fields'=>['account_head_id','name']]);
			$this->set('accountHeads',$accountHeads);
			$this->set('mode_catagory',$mode_catagory);
		}
		else
		{
			$datasource_Journal = $AccountingsController->Journal->getDataSource();
			try {
				$datasource_Journal->begin();
				$data=$this->request->data['Journal'];
				$credit   =$data['mode'];
				$debit    =$data['account_head_id'];
				$amount   =$data['amount'];
				$date     =$data['date'];
				$remarks  =$data['remarks'];
				$work_flow='Staff/Salary Payment';
				$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);
				if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
				$return['result']='Success';
				$datasource_Journal->commit();
			} catch (Exception $e) {
				$datasource_Journal->rollback();
				$return['result']=$e->getMessage();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function Salary_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='Staff.name';
		$columns[]='Staff.name';
		$columns[]='Staff.name';
		$columns[]='Staff.name';
		$conditions=[];
		$conditions['payment_mode']="Salary";
		$totalData=$this->Staff->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Staff.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->Staff->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->Staff->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'limit'=>$requestData['length'],
			'fields'=>array(
				'Staff.*',
				'Role.*',
			)
		));
		foreach ($Data as $key => $value) {
			$function_return=$this->get_salary_details($value['Staff']['account_head_id']);
			$Data[$key]['Staff']['name'] ='<span style="display:none" class="hidden_AccountHead_id">'.$value['Staff']['account_head_id'].'</span><span class="name">'.ucwords(strtolower($value['Staff']['name'])).'</span>';
			$Data[$key]['Staff']['credit']      =$function_return['credit'];
			$Data[$key]['Staff']['debit']    =$function_return['debit'];
			$Data[$key]['Staff']['balance']    =round(($function_return['credit']-$function_return['debit']),2);
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function WagesPayment()
	{
		$AccountingsController = new AccountingsController;
		$group_id=13; $sub_group_id = $AccountingsController->GetSubGroupIdByGroupIdAndSubGroupName($group_id,'SALARYACCOUNT PAID');
		$user_id=1;
		if(!$this->request->data) {
			$accountHeads=$this->Staff->find('list',['conditions'=>['payment_mode'=>'Wages'],'fields'=>['paid_account_head_id','name'],'order'=>['name']]);
			$this->set('accountHeads',$accountHeads);
		} else {
			$datasource_Journal = $AccountingsController->Journal->getDataSource();
			try {
				$datasource_Journal->begin();
				$data=$this->request->data['Journal'];
				$credit   =$data['mode'];
				$debit    =$data['account_head_id'];
				$amount   =$data['amount'];
				$date     =$data['date'];
				$remarks  =$data['remarks'];
				$work_flow='Staff/Wages Payment';
				$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);
				if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
				$over_time =$data['over_time'];
				if($over_time) {
					$Staff=$this->Staff->findByPaidAccountHeadId($data['account_head_id']);
					$debit=$Staff['Staff']['bonus_head_id'];
					$function_return=$AccountingsController->JournalCreate($credit,$debit,$over_time,$date,$remarks,$work_flow,$user_id);
					if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
				}
				$return['result']='Success';
				$datasource_Journal->commit();
			} catch (Exception $e) {
				$datasource_Journal->rollback();
				$return['result']=$e->getMessage();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function Wages_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='Staff.paid_account_head_id';
		$columns[]='Staff.name';
		$columns[]='Staff.code';
		$columns[]='Role.name';
		$columns[]='Staff.name';
		$columns[]='Staff.name';
		$columns[]='Staff.name';
		$conditions=[];
		$conditions['payment_mode']="Wages";
		$totalData=$this->Staff->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Staff.name LIKE' =>'%'. $q . '%',
				'Staff.code LIKE' =>'%'. $q . '%',
				'Role.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->BasicPay->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->Staff->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'limit'=>$requestData['length'],
			'fields'=>array(
				'Staff.*',
				'Role.*',
			)
		));
		foreach ($Data as $key => $value) {
			$function_return=$this->get_salary_details($value['Staff']['paid_account_head_id']);
			$salary=$function_return['total'];
			$Data[$key]['Staff']['salary'] ='<span class="table_click" table_type="Salary" table_id="'.$value['Staff']['paid_account_head_id'].'"><b>'.$salary.'</b></span>';
			$function_return=$this->get_salary_details($value['Staff']['bonus_head_id']);
			$bonus=$function_return['total'];
			$Data[$key]['Staff']['bonus']  ='<span class="table_click" table_type="Bonus"  table_id="'.$value['Staff']['bonus_head_id'].'"><b>'.$bonus.'</b></span>';
			$Data[$key]['Staff']['total']  =$bonus+$salary;
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function get_salary_details($account_head_id)
	{
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$name=$AccountHead['AccountHead']['name'];
		$return['outstanding']=0;
		$return['prepaid']=0;
		$return['total']=0;
		$namexplode=explode(' ', $name);
		array_pop($namexplode);
		$name=implode(' ',$namexplode);
			$function_return=$this->General_AccountHead_Credit_Debit_By_Id($account_head_id);
			$return['debit']=$function_return['debit'];	
			$return['credit']=$function_return['credit'];	
		return $return;
	}
	public function AccountHead_Option_ListBySubGroupId($sub_group_id)
	{
		$AccountHeads=$this->AccountHead->find('list',array(
			'conditions'=>array(
				'AccountHead.sub_group_id'=>$sub_group_id,
			),
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
			)
		));
		return $AccountHeads;
	}
	public function AdvancePayment()
	{
		$AccountingsController = new AccountingsController;
		$group_id=13; $sub_group_id = $AccountingsController->GetSubGroupIdByGroupIdAndSubGroupName($group_id,'SALARYACCOUNT PAID');
		$user_id=1;
		if(!$this->request->data)
		{
			$accountHeads=$this->Staff->find('list',['fields'=>['prepaid_account_head_id','name'],'order'=>['name']]);
			$this->set('accountHeads',$accountHeads);
		}
		else
		{
			$datasource_Journal = $AccountingsController->Journal->getDataSource();
			try {
				$datasource_Journal->begin();
				$data=$this->request->data['Journal'];
				$credit   =$data['mode'];
				$debit    =$data['account_head_id'];
				$amount   =$data['amount'];
				$date     =$data['date'];
				$remarks  =$data['remarks'];
				$work_flow='Staff/Advance Payment';
				$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);
				if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
				$return['result']='Success';
				$datasource_Journal->commit();
			} catch (Exception $e) {
				$datasource_Journal->rollback();
				$return['result']=$e->getMessage();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function BonusPayment()
	{
		$AccountingsController = new AccountingsController;
		$group_id=13; $sub_group_id = $AccountingsController->GetSubGroupIdByGroupIdAndSubGroupName($group_id,'SALARYACCOUNT PAID');
		$user_id=1;
		if(!$this->request->data)
		{
			$accountHeads=$this->Staff->find('list',['fields'=>['bonus_head_id','name'],'order'=>['name']]);
			$this->set('accountHeads',$accountHeads);
		}
		else
		{
			$datasource_Journal = $AccountingsController->Journal->getDataSource();
			try {
				$datasource_Journal->begin();
				$data=$this->request->data['Journal'];
				$credit   =$data['mode'];
				$debit    =$data['account_head_id'];
				$amount   =$data['amount'];
				$date     =$data['date'];
				$remarks  =$data['remarks'];
				$work_flow='Staff/Bonus Payment';
				$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);
				if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
				$return['result']='Success';
				$datasource_Journal->commit();
			} catch (Exception $e) {
				$datasource_Journal->rollback();
				$return['result']=$e->getMessage();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
	public function Advance_Salary_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='Staff.prepaid_account_head_id';
		$columns[]='Staff.name';
		$columns[]='Staff.name';
		$columns[]='Staff.name';
		$conditions=[];
		$totalData=$this->Staff->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Staff.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->BasicPay->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->Staff->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'limit'=>$requestData['length'],
			'fields'=>array(
				'Staff.*',
				'Role.*',
			)
		));
		foreach ($Data as $key => $value) {
			$function_return=$this->get_advance_salary_details($value['Staff']['prepaid_account_head_id']);
			$Data[$key]['Staff']['total']  =$function_return['total'];
			$Data[$key]['Staff']['paid']   =$function_return['paid'];
			$Data[$key]['Staff']['balance']=$function_return['total']-$function_return['paid'];
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function BonusSalary_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='Staff.bonus_head_id';
		$columns[]='Staff.name';
		$columns[]='Staff.name';
		$columns[]='Staff.name';
		$conditions=[];
		$totalData=$this->Staff->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Staff.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->BasicPay->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->Staff->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'limit'=>$requestData['length'],
			'fields'=>array(
				'Staff.*',
				'Role.*',
			)
		));
		foreach ($Data as $key => $value) {
			$function_return=$this->get_advance_salary_details($value['Staff']['bonus_head_id']);
			$Data[$key]['Staff']['total']  =$function_return['total'];
			$Data[$key]['Staff']['paid']   =$function_return['paid'];
			$Data[$key]['Staff']['balance']=$function_return['total']-$function_return['paid'];
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function get_advance_salary_details($account_head_id)
	{
		$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
		$Journal=$this->Journal->find('first',['conditions'=>[
			'debit'=>$account_head_id,
			'flag'=>1,
		]]);
		$return['total']=round($Journal['Journal']['total_amount'],2);
		$Journal=$this->Journal->find('first',['conditions'=>[
			'credit'=>$account_head_id,
			'flag'=>1,
		]]);
		$return['paid']=round($Journal['Journal']['total_amount'],2);
		return $return;
	}
	public function General_AccountHead_Credit_Debit_By_Id($id)
	{
		$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
		$Credit=$this->Journal->find('first',['conditions'=>[
			'credit'=>$id,
			'flag'=>1,
		]]);
		$Debit=$this->Journal->find('first',['conditions'=>[
			'debit'=>$id,
			'flag'=>1,
		]]);
		$return['credit']=round($Credit['Journal']['total_amount'],2);
		$return['debit'] =round($Debit['Journal']['total_amount'],2);
		return $return;
	}
	public function get_Wages_PayStructure($staff_id)
	{
		$return=['result'=>'Empty'];
		try {
			$Staff=$this->Staff->findByPaidAccountHeadId($staff_id);
			$staff_id=$Staff['Staff']['id'];
			if(!$staff_id) throw new Exception("Empty Staff_id", 1);
			$BasicPay=$this->BasicPay->find('first',array('conditions'=>['staff_id'=>$staff_id],'order'=>['wef_date DESC']));
			if(!$BasicPay) throw new Exception("Please Set BasicPay", 1);
			$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
			$Journal=$this->Journal->find('first',[
				'conditions'=>[
					'debit'=>$Staff['Staff']['paid_account_head_id'],
					'flag'=>1,
				],
				'fields'=>['Journal.total_amount']
			]);
			$Attendance_count=0;
			$Attendance=$this->Attendance->findAllByStaffId($staff_id,['Attendance.full_day','Attendance.half_day']);
			if($Attendance) {
				foreach ($Attendance as $key => $value) {
					$Attendance_count+=count(explode(',', $value['Attendance']['full_day']));
					if($value['Attendance']['half_day']) {
						$Attendance_count+=count(explode(',', $value['Attendance']['half_day']))/2;	
					}
				}
			}
			$return['amount']=floatval($BasicPay['BasicPay']['amount']);
			$return['total_paid_amount']=$Journal['Journal']['total_amount'];
			$return['Attendance_count']=$Attendance_count;
			$return['total_amount_to_be_paid']=$return['amount']*$Attendance_count;
			$return['balance']=$return['total_amount_to_be_paid']-$return['total_paid_amount'];
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function Attendance()
	{
		$staffs=$this->Staff->find('list',['id','name']);
		$this->set('staffs',$staffs);
	}
	public function AttendanceTable()
	{
		$requestData=$this->request->data;
		$conditions=[];
		if($requestData['staff_id']) $conditions['Staff.id']=$requestData['staff_id'];
		if($requestData['month']) 
		{
			$month=date('Y-m-1',strtotime($requestData['month']));
		}
		else
		{
			$month=date('Y-m-1');
		}
		$totalData=$this->Staff->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;

		$Data=$this->Staff->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'fields'=>array(
				'Staff.*',
			)
		));
		foreach ($Data as $key => $value) {
			$Data[$key]['Staff']['action']='<i table_id="'.$value['Staff']['id'].'" class="fa fa-2x fa-edit  edit_attendance blue-col"></i>';
			$Data[$key]['Staff']['month']=date('m-Y',strtotime($month));
			$end_day=date('t',strtotime($month));
			for ($i=0; $i <=31 ; $i++) {
				$Data[$key]['Staff']['full_day_'.$i]=''; 
				$Data[$key]['Staff']['half_day_'.$i]=''; 
			}
			$CompanyHoliday=$this->CompanyHoliday->findByStaffIdAndMonth($value['Staff']['id'],$month);
			$Attendance=$this->Attendance->findByStaffIdAndMonth($value['Staff']['id'],$month);
			$Attendance_full_day_column_explode=[];
			$Attendance_half_day_column_explode=[];
			if($Attendance) {
				if($Attendance['Attendance']['full_day']) {
					$Attendance_full_day_column_explode=explode(',', $Attendance['Attendance']['full_day']);
				}
				if($Attendance['Attendance']['half_day']) {
					$Attendance_half_day_column_explode=explode(',', $Attendance['Attendance']['half_day']);	
				}
			}
			$Data[$key]['Staff']['worked_days']=count($Attendance_full_day_column_explode);
			if(count($Attendance_half_day_column_explode)>0) {
				$Data[$key]['Staff']['worked_days']+=count($Attendance_half_day_column_explode)/2;
			}
			$CompanyHoliday_column_explode=[];
			if($CompanyHoliday) {
				if($CompanyHoliday['CompanyHoliday']['holidays']) {
					$CompanyHoliday_column_explode=explode(',', $CompanyHoliday['CompanyHoliday']['holidays']);
				}
			}
			$Data[$key]['Staff']['worked_days']+=count($CompanyHoliday_column_explode);
			for ($i=0; $i <=$end_day ; $i++) {
				$full_day_checked='';
				$half_day_checked='';
				$Data[$key]['Staff']['full_day_'.$i]='<b style="color:#dd4b39">Ab</b>';
				$Data[$key]['Staff']['half_day_'.$i]='';

				if(in_array($i,$Attendance_full_day_column_explode)) {
					$full_day_checked='checked';
				}
				if(in_array($i,$Attendance_half_day_column_explode)) {
					$half_day_checked='checked';
				}
				$Data[$key]['Staff']['full_day_'.$i] =($full_day_checked)?'<b style="color:#00a65a">FDP</b>':$Data[$key]['Staff']['full_day_'.$i]; 
				if($full_day_checked) {
					$Data[$key]['Staff']['half_day_'.$i]='';
				} else {
					$Data[$key]['Staff']['half_day_'.$i]=($half_day_checked)?'<b style="color:#00a65a">HDP</b>':$Data[$key]['Staff']['half_day_'.$i]; 
				}
				if(!$full_day_checked && $half_day_checked) {
					$Data[$key]['Staff']['full_day_'.$i]='';
				}
				if($CompanyHoliday_column_explode)
				{
					if(in_array($i,$CompanyHoliday_column_explode)) {
						$Data[$key]['Staff']['full_day_'.$i]='<b style="color:#005082">HD</b>';
					}
				}
				if($requestData['table_id']==$value['Staff']['id']) {
					$Data[$key]['Staff']['action']='<i staff_id="'.$value['Staff']['id'].'" month="'.$month.'" class="fa fa-2x fa-check  ok_attendance blue-col"></i>';
					$Data[$key]['Staff']['full_day_'.$i]='<input  name=full_day[] type="checkbox" value="'.$i.'" '.$full_day_checked.'>';
					$Data[$key]['Staff']['half_day_'.$i]='<input  name=half_day[] type="checkbox" value="'.$i.'" '.$half_day_checked.'>';
				}
			}
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function CompanyHolidayTable()
	{
		$requestData=$this->request->data;
		$conditions=[];
		if($requestData['month']) 
		{
			$month=date('Y-m-1',strtotime($requestData['month']));
		}
		else
		{
			$month=date('Y-m-1');
		}
		$totalData=$this->Staff->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;

		$Data=$this->Staff->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'fields'=>array(
				'Staff.*',
			)
		));
		foreach ($Data as $key => $value) {
			$Data[$key]['Staff']['action']='<i table_id="'.$value['Staff']['id'].'" class="fa fa-2x fa-edit  edit_holiday blue-col"></i>';
			$Data[$key]['Staff']['month']=date('m-Y',strtotime($month));
			$end_day=date('t',strtotime($month));
			for ($i=0; $i <=31 ; $i++) {
				$Data[$key]['Staff']['holiday_'.$i]=''; 
				$day=date('D',strtotime($i.'-'.date('m-Y',strtotime($month))));
				if($day=='Sun')
				{
					$Data[$key]['Staff']['holiday_'.$i] ='<b style="color:#005082">'.$day.'</b>';
				}
			}
			$CompanyHoliday=$this->CompanyHoliday->findByStaffIdAndMonth($value['Staff']['id'],$month);
			$CompanyHoliday_column_explode=[];
			if($CompanyHoliday)
			{
				if($CompanyHoliday['CompanyHoliday']['holidays'])
				{
					$CompanyHoliday_column_explode=explode(',', $CompanyHoliday['CompanyHoliday']['holidays']);
				}
			}
			$Data[$key]['Staff']['worked_days']=count($CompanyHoliday_column_explode);
			for ($i=0; $i <=$end_day ; $i++) {
				$holiday_checked='';
				if(in_array($i,$CompanyHoliday_column_explode))
				{
					$holiday_checked='checked';
				}
				$Data[$key]['Staff']['holiday_'.$i] =($holiday_checked)?'<b style="color:#005082">HD</b>':$Data[$key]['Staff']['holiday_'.$i];
				if($requestData['table_id']==$value['Staff']['id'])
				{
					$Data[$key]['Staff']['action']='<i staff_id="'.$value['Staff']['id'].'" month="'.$month.'" class="fa fa-2x fa-check  ok_holiday blue-col"></i>';
					$Data[$key]['Staff']['holiday_'.$i]='<input  name=holidays[] type="checkbox" value="'.$i.'" '.$holiday_checked.'>';
				}
			}
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function DayAttendanceTable()
	{
		$requestData=$this->request->data;
		$conditions=[];
		if(isset($requestData['day'])) 
		{
			$day=date('Y-m-d',strtotime($requestData['day']));
		}
		else
		{
			$day=date('Y-m-d');
		}
		$totalData=$this->Staff->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;

		$Data=$this->Staff->find('all',array(
			'conditions'=>$conditions,
			'fields'=>array(
				'Staff.id',
				'Staff.name',
			)
		));
		foreach ($Data as $key => $value) {
			$staff_id=$value['Staff']['id'];
			$Data[$key]['Staff']['day']=date('d-m-Y',strtotime($day));
			$Data[$key]['Staff']['day_name']=date('D',strtotime($day));
			$Data[$key]['Staff']['day_id']=date('d',strtotime($day));
			$Data[$key]['Staff']['month']=date('M',strtotime($day));
			$Data[$key]['Staff']['full_day']='';
			$Data[$key]['Staff']['half_day']='';
			$Data[$key]['Staff']['holiday']='';
			$month=date('Y-m-1',strtotime($day));
			$current_day=date('d',strtotime($day));
			$CompanyHoliday=$this->CompanyHoliday->findByStaffIdAndMonth($value['Staff']['id'],$month);
			$holidays_list=[];
			if($CompanyHoliday)
			{
				$holidays=$CompanyHoliday['CompanyHoliday']['holidays'];
				if($holidays)
				{
					$holidays_list=explode(',', $holidays);
				}
			}
			$Attendance=$this->Attendance->findByStaffIdAndMonth($staff_id,$month);
			$full_day_checked='';
			$half_day_checked='';
			$freeze='';
			if($Attendance)
			{
				$full_day_list=$Attendance['Attendance']['full_day'];
				if($full_day_list)
				{
					$full_day_list=explode(',', $full_day_list);
					if(in_array($current_day,$full_day_list)) {$full_day_checked='checked'; }
				}
				$half_day_list=$Attendance['Attendance']['half_day'];
				if($half_day_list)
				{
					$half_day_list=explode(',', $half_day_list);
					if(in_array($current_day,$half_day_list)) {$half_day_checked='checked'; }
				}
			}
			$Data[$key]['Staff']['freeze']='';
			if(!$full_day_checked)
			{
				$Data[$key]['Staff']['half_day']='<input type="checkbox" staff_id="'.$staff_id.'" month="'.$month.'" day="'.$current_day.'" class="js-switch half_day_attendance_change" '.$half_day_checked.' />';	
			}
			if(!$half_day_checked)
			{
				$Data[$key]['Staff']['full_day']='<input type="checkbox" staff_id="'.$staff_id.'" month="'.$month.'" day="'.$current_day.'" class="js-switch full_day_attendance_change" '.$full_day_checked.' />';		
			}
			if(in_array($current_day,$holidays_list)) {
				$Data[$key]['Staff']['half_day']='';
				$Data[$key]['Staff']['full_day']='';
				$Data[$key]['Staff']['holiday']='<b style="color:#005082">Holiday</b>';
			}
			$AttendanceFreeze=$this->AttendanceFreeze->findBydate($day);
			if($AttendanceFreeze)
			{
				$Data[$key]['Staff']['full_day']='<b style="color:#dd4b39" class="pull-right">Absent</b>';
				$Data[$key]['Staff']['half_day']='<b style="color:#dd4b39"></b>';
				$Data[$key]['Staff']['freeze']='freezed';

				if(in_array($current_day,$holidays_list)) {
					$Data[$key]['Staff']['half_day']='';
					$Data[$key]['Staff']['full_day']='';
					$Data[$key]['Staff']['holiday']='<b style="color:#005082">Holiday</b>';
				}

				if($full_day_checked){
					$Data[$key]['Staff']['full_day']='<b style="color:#00a65a">Full Day</b>'; 
					$Data[$key]['Staff']['half_day']=''; 
				}
				if($half_day_checked){
					$Data[$key]['Staff']['half_day']='<b style="color:#00a65a">Half Day</b>'; 
					$Data[$key]['Staff']['full_day']='';
				}
			}
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function Attendance_Update_Function()
	{
		try {
			$data=$this->request->data;
			$CompanyHoliday=$this->CompanyHoliday->findByStaffIdAndMonth($data['staff_id'],$data['month']);
			$CompanyHoliday_column_explode=[];
			if($CompanyHoliday)
			{
				if($CompanyHoliday['CompanyHoliday']['holidays'])
				{
					$CompanyHoliday_column_explode=explode(',', $CompanyHoliday['CompanyHoliday']['holidays']);
				}
			}
			if(!isset($data['full_day'])) { $data['full_day']=[]; }
			if(!isset($data['half_day'])) { $data['half_day']=[]; }
			
			$array_intersect=array_intersect($data['half_day'],$data['full_day']);
			$data['half_day']=array_diff($data['half_day'],$array_intersect);

			$array_intersect=array_intersect($data['half_day'],$CompanyHoliday_column_explode);
			$data['half_day']=array_diff($data['half_day'],$array_intersect);

			$array_intersect=array_intersect($data['full_day'],$CompanyHoliday_column_explode);
			$data['full_day']=array_diff($data['full_day'],$array_intersect);

			if(isset($data['full_day'])) { $data['full_day']=implode(',', $data['full_day']); } else { $data['full_day']=''; }
			if(isset($data['half_day'])) { $data['half_day']=implode(',', $data['half_day']); } else { $data['half_day']=''; }
			$Attendance=$this->Attendance->findByStaffIdAndMonth($data['staff_id'],$data['month']);
			if($Attendance)
			{
				$this->Attendance->id=$Attendance['Attendance']['id'];
				$this->Attendance->saveField('full_day',$data['full_day']);
				$this->Attendance->saveField('half_day',$data['half_day']);
			}
			else
			{
				$this->Attendance->create();
				if(!$this->Attendance->save($data))
				{
					$errors = $this->Attendance->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
			}
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function CompanyHoliday_Update_Function()
	{
		try {
			$data=$this->request->data;
			if(isset($data['holidays'])) { $data['holidays']=implode(',', $data['holidays']); } else { $data['holidays']=''; }
			$CompanyHoliday=$this->CompanyHoliday->findByStaffIdAndMonth($data['staff_id'],$data['month']);
			if($CompanyHoliday)
			{
				$this->CompanyHoliday->id=$CompanyHoliday['CompanyHoliday']['id'];
				$this->CompanyHoliday->saveField('holidays',$data['holidays']);
			}
			else
			{
				$this->CompanyHoliday->create();
				if(!$this->CompanyHoliday->save($data))
				{
					$errors = $this->CompanyHoliday->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
			}
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function SpecialHoliday_Store_Function()
	{
		$datasource_CompanyHoliday = $this->CompanyHoliday->getDataSource();
		try {
			$datasource_CompanyHoliday->begin();
			$data=$this->request->data;
			$date=$data['date'];
			$day=date('d',strtotime($date));
			$month=date('Y-m-1',strtotime($date));
			$Staff=$this->Staff->find('list',[
				'fields'=>['id','name'],'order'=>['name']
			]);
			foreach ($Staff as $staff_id => $staff_name) {
				$CompanyHoliday=$this->CompanyHoliday->findByStaffIdAndMonth($staff_id,$month);
				if($CompanyHoliday) {
					$this->CompanyHoliday->id=$CompanyHoliday['CompanyHoliday']['id'];
					$holidays=$this->CompanyHoliday->field('holidays');
					$holidays=explode(',', $holidays);
					if(!in_array($day,$holidays)) {
						$holidays[]=floatval($day);
					}
					$holidays=implode(',', $holidays);
					$this->CompanyHoliday->saveField('holidays',$holidays);
				} else {
					$data=[
						'month'=>$month,
						'staff_id'=>$staff_id,
						'holidays'=>floatval($day)
					];
					$this->CompanyHoliday->create();
					if(!$this->CompanyHoliday->save($data)) {
						$errors = $this->CompanyHoliday->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
					}
				}
			}
			$datasource_CompanyHoliday->commit();
			$return['result']='success';
		} catch (Exception $e) {
			$datasource_CompanyHoliday->rollback();
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function DayAttendance_Update_Function($data)
	{
		try {
			$Attendance=$this->Attendance->findByStaffIdAndMonth($data['staff_id'],$data['month']);
			if($Attendance)
			{
				$this->Attendance->id=$Attendance['Attendance']['id'];
				$this->Attendance->saveField('full_day',$data['full_day']);
				$this->Attendance->saveField('half_day',$data['half_day']);
			}
			else
			{
				$this->Attendance->create();
				if(!$this->Attendance->save($data))
				{
					$errors = $this->Attendance->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
			}
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function HalfDayAttendance_Update_Function()
	{
		$datasource_Attendance = $this->Attendance->getDataSource();
		try {
			$datasource_Attendance->begin();
			$data=$this->request->data;
			$data['full_day']='';
			if($data['half_day']=='true') {$data['half_day']=$data['day']; }
			else{$data['half_day']=''; }
			$Attendance=$this->Attendance->findByStaffIdAndMonth($data['staff_id'],$data['month']);
			if($Attendance)
			{
				$data['full_day']=$Attendance['Attendance']['full_day'];
				$half_day=$Attendance['Attendance']['half_day'];
				if($data['half_day'])
				{
					$half_day=[]; if($half_day) {$half_day=explode(',', $half_day); }
					if(!in_array($data['half_day'],$half_day))
					{
						$half_day[]=$data['half_day'];
					}
					$half_day=implode(',', $half_day);
				}
				else
				{
					$half_day=explode(',', $half_day);
					if(in_array($data['day'],$half_day))
					{
						$key=array_search($data['day'],$half_day);
						unset($half_day[$key]);
					}
					$half_day=implode(',', $half_day);
				}
				$data['half_day']=$half_day;
			}
			$function_return=$this->DayAttendance_Update_Function($data);
			if($function_return['result']!='success') throw new Exception($function_return['result'], 1);
			$datasource_Attendance->commit();
			$return['result']='success';
		} catch (Exception $e) {
			$datasource_Attendance->rollback();
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function FullDayAttendance_Update_Function()
	{
		$datasource_Attendance = $this->Attendance->getDataSource();
		try {
			$datasource_Attendance->begin();
			$data=$this->request->data;
			$data['half_day']='';
			if($data['full_day']=='true') {$data['full_day']=$data['day']; }
			else{$data['full_day']=''; }
			$Attendance=$this->Attendance->findByStaffIdAndMonth($data['staff_id'],$data['month']);
			if($Attendance)
			{
				$data['half_day']=$Attendance['Attendance']['half_day'];
				$full_day=$Attendance['Attendance']['full_day'];
				if($data['full_day'])
				{
					$full_day=[]; if($full_day) {$full_day=explode(',', $full_day); }
					if(!in_array($data['full_day'],$full_day))
					{
						$full_day[]=$data['full_day'];
					}
					$full_day=implode(',', $full_day);
				}
				else
				{
					$full_day=explode(',', $full_day);
					if(in_array($data['day'],$full_day))
					{
						$key=array_search($data['day'],$full_day);
						unset($full_day[$key]);
					}
					$full_day=implode(',', $full_day);
				}
				$data['full_day']=$full_day;
			}
			$function_return=$this->DayAttendance_Update_Function($data);
			if($function_return['result']!='success') throw new Exception($function_return['result'], 1);
			$datasource_Attendance->commit();
			$return['result']='success';
		} catch (Exception $e) {
			$datasource_Attendance->rollback();
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function Attendance_Freeze_Update_Function()
	{
		try {
			$data=$this->request->data;
			$date=date('Y-m-d',strtotime($data['date']));
			$AttendanceFreeze=$this->AttendanceFreeze->findBydate($date);
			if(!$AttendanceFreeze){
				$data['date']=$date;
				$data['freeze']=1;
				$this->AttendanceFreeze->create();
				if(!$this->AttendanceFreeze->save($data))
				{
					$errors = $this->AttendanceFreeze->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
			}
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function save_multiple_amount_ajax()
	{
		$user_id=1;
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			$data=$this->request->data['JournalMultipleForm'];
			$Staff         =$this->Staff->findByPaidAccountHeadId($data['account_head_id']);
			$credit        =$data['mode'];
			$debit         =$data['account_head_id'];
			$debit         =$data['account_head_id'];
			$remarks       =$data['remarks'];
			$work_flow     ='Staff/Wages Payment';
			$bonus_head_id =$Staff['Staff']['bonus_head_id'];
			$AccountingsController = new AccountingsController;
			for ($i=0; $i <count($data['amount']) ; $i++) {
				$amount=$data['amount'][$i];
				$bonus=$data['bonus'][$i];
				$date=$data['date'][$i];
				$Journal=$this->Journal->find('first',array('conditions'=>[
					'date'  =>date('Y-m-d',strtotime($date)),
					'credit'=>$credit,
					'debit' =>$debit,
					'amount'=>$amount,
				]));
				if(!$Journal)
				{
					$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);
					if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
				}
				if($bonus)
				{
					$Journal=$this->Journal->find('first',array('conditions'=>[
						'date'  =>date('Y-m-d',strtotime($date)),
						'credit'=>$credit,
						'debit' =>$bonus_head_id,
						'amount'=>$amount,
					]));
					if(!$Journal)
					{
						$function_return=$AccountingsController->JournalCreate($credit,$bonus_head_id,$amount,$date,$remarks,$work_flow,$user_id);
						if($function_return['result']!='Success') throw new Exception($function_return['message'], 1);
					}
				}
			}
			$return['result']='success';
			$datasource_Journal->commit();
		} catch (Exception $e) {
			$datasource_Journal->rollback();
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function CompanyHoliday()
	{
		# code...
	}
	public function DayAttendance()
	{
		# code...
	}
	public function BonusHeadCreation()
	{
		$Staff=$this->Staff->find('all',['conditions'=>['bonus_head_id'=>Null]]);
		$AccountingsController = new AccountingsController;
		foreach ($Staff as $key => $value) {
			$name=$value['Staff']['name'];
			$name=trim($name);
			$date=date('Y-m-d');
			$description='';
			$group_id=13; $sub_group_id_paid       = $AccountingsController->GetSubGroupIdByGroupIdAndSubGroupName($group_id,'SALARYACCOUNT PAID');
			$function_return=$AccountingsController->AccountHeadCreate($sub_group_id_paid,$name.'BONUS PAID',0,$date,$description);
			if($function_return['result']!='Success') throw new Exception($function_return['message']);
			$this->Staff->id=$value['Staff']['id'];
			$this->Staff->saveField('bonus_head_id',$this->AccountHead->getLastInsertId());
		}
		exit;
	}
	public function OverTimeManagment()
	{
		$staffs=$this->Staff->find('list',['conditions'=>['payment_mode'=>'Salary'],'fields'=>['id','name'],'order'=>['name']]);
		$this->set('staffs',$staffs);
	}
	public function OverTimeTable()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='OverTime.id';
		$columns[]='OverTime.date';
		$columns[]='Staff.name';
		$columns[]='OverTime.remarks';
		$columns[]='OverTime.amount';
		$columns[]='OverTime.id';
		$conditions=[];
		if($requestData['staff_id']) $conditions['staff_id']=$requestData['staff_id'];
		$totalData=$this->OverTime->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Staff.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->BasicPay->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->OverTime->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'limit'=>$requestData['length'],
			'fields'=>array(
				'OverTime.*',
				'Staff.name',
			)
		));
		foreach ($Data as $key => $value) {
			$Data[$key]['OverTime']['action']="<span><i table_id='".$value['OverTime']['id']."' class='fa fa-2x fa-trash delete_overtime blue-col'></i></span>";
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function OverTime_add_Function()
	{
		try {
			$data=$this->request->data['OverTime'];
			$data['date']=date('Y-m-d',strtotime($data['date']));
			if($data['amount']<=0) throw new Exception("Need Amount", 1);
			$this->OverTime->create();
			if(!$this->OverTime->save($data))
			{
				$errors = $this->OverTime->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
			}
			else
			{
				$credit=1;
				$user_id=1;
				$route_id="";
				$day_register_id="";
				$executive_id="";
				$staff_id=$data['staff_id'];
				$debit =$this->Staff->field('account_head_id',array('Staff.id'=>$staff_id));
				$amount=(int)$data['amount'];
				$date=$data['date'];
				$remarks="Over Time Amount";
				$voucher_no='';
				$external_voucher="";
				$work_flow='Over Time Managment';
			   $AccountingsController = new AccountingsController;
					$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,$day_register_id,'',$executive_id,$route_id);
				if($function_return['result']!='Success')
					throw new Exception($function_return['result'], 1);
			}
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function OverTime_delete_Function($id)
	{
		try {
			if(!$this->OverTime->delete($id)) throw new Exception("Cant Delete This Over Time", 1);
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function DocumentUpload(){
        $staffList=$this->Staff->find('list',array(
            'fields'=>array('id','name'),
            //'conditions'=>array('Staff.role_id'=>4)
        ));
        $this->set('staffList',$staffList);
        $DocumentTypeList=$this->DocumentType->find('list',array(
            'fields'=>array('id','name')));

        $this->set('DocumentTypeList',$DocumentTypeList);

    //     $uploadList_staff=$this->StaffDocument->find('all',array(
    //         'fields'=>array('StaffDocument.document_type_id','StaffDocument.id','StaffDocument.expiry_date','StaffDocument.uploaded_file','StaffDocument.staff_id')));
    //     $all_document=[];
    //        foreach ($uploadList_staff as $key => $value) 
				// {
				// $all_document_single['id']=$value['StaffDocument']['id'];		
				// $all_document_single['expiry_date']=date('d-m-Y',strtotime($value['StaffDocument']['expiry_date']));
				// $all_document_single['uploaded_file']=$value['StaffDocument']['uploaded_file'];
				// if($value['StaffDocument']['document_type_id']==1)
				// {
				//  $name=$this->Staff->field('Staff.name',array('Staff.id'=>$value['StaffDocument']['staff_id']));

				// }
				// else if($value['StaffDocument']['document_type_id']==2)
				// {
				// $name=$this->VehicleNo->field('number',array('VehicleNo.id'=>$value['StaffDocument']['staff_id']));
				// }
				// else
				// {
				// $name="Common";
				// }
				// $all_document_single['name']=$name;
				//  $document_type=$this->DocumentType->findById($value['StaffDocument']['document_type_id']);
				// $all_document_single['document_type_id']= $document_type['DocumentType']['name'];				
				// $all_document[$all_document_single['id']]= $all_document_single;
				// }
    //     $this->set('uploadList',$all_document);
        if(!$this->request->data){
        	if(empty($id))
            { 
           // $this->request->data['Document']['expiry_date']=date('d-m-Y');
            }
        }
        else{
            $data=$this->request->data['Document'];
            if(!empty($id))
            {
            }
            else
            {
                // $file_check=$this->StaffDocument->findByStaffIdAndDocumentTypeId($data['name'],$data['document_type']);
                // if(!$file_check){
                    $upload_file = $data['document_file'];
                    unset($data['document_file']);
                    $staff_document=array(
                        'staff_id'=>$data['name'],
                        'remarks'=>$data['remarks'],
                         'expiry_date'=>date('Y-m-d',strtotime($data['expiry_date'])),
                        'document_type_id'=>$data['document_type'],
                        'created_at'=>date('Y-m-d'),
                        'modified_at'=>date('Y-m-d')
                        );
                    $this->StaffDocument->Create();
                    if(!$this->StaffDocument->save($staff_document))
                        throw new Exception("Error Processing upload", 1);
                    $last_id=$this->StaffDocument->getlastInsertId();
                    $uploadData = '';
                    if(!empty($upload_file['name'])){
                        $fileName = $upload_file['name'];
                        $uploadPath = WWW_ROOT.'profile'.DS;
                        $uploadFile = $uploadPath.$fileName;
                        if(move_uploaded_file($upload_file['tmp_name'],$uploadFile))
                        {
                            $this->StaffDocument->id=$last_id;
                            if (!$this->StaffDocument->saveField('uploaded_file',$fileName)) 
                                throw new Exception("Unable to upload file, please try again.", 1);
                        }
                        else
                        {
                            throw new Exception("Error Processing While Uploading", 1);
                        }
                    }
                    $return['result']="Success";
                // }else{
                //     $return['result']="Already Uploaded";
                // }
                $this->Session->setFlash(__($return['result']));
                return $this->redirect(array('controller' => 'Hr', 'action' => 'DocumentUpload'));
            }

        }

    }
    public function EditStaffDocument ()
	{
		if($this->request->data)
		{
			$data=$this->request->data['Document'];
			$datasource_StaffDocument = $this->StaffDocument->getDataSource();
			try {
//$file_check=$this->StaffDocument->findByStaffIdAndDocumentTypeId($data['name'],$data['document_type']);
               // if(!$file_check){
                    $upload_file = $data['document_file'];
                    unset($data['document_file']);
                    $staff_document=array(
                        'staff_id'=>$data['name'],
                         'expiry_date'=>date('Y-m-d',strtotime($data['expiry_date'])),
                        'document_type_id'=>$data['document_type'],
                        'modified_at'=>date('Y-m-d'),
                        'remarks'=>$data['remarks'],
                        );
               		$this->StaffDocument->id=$data['id'];
                    if(!$this->StaffDocument->save($staff_document))
                        throw new Exception("Error Processing upload", 1);
                    $uploadData = '';
                    if(!empty($upload_file['name'])){
                        $fileName = $upload_file['name'];
                        $uploadPath = WWW_ROOT.'profile'.DS;
                        $uploadFile = $uploadPath.$fileName;
                        if(move_uploaded_file($upload_file['tmp_name'],$uploadFile))
                        {
                            $this->StaffDocument->id=$data['id'];
                            if (!$this->StaffDocument->saveField('uploaded_file',$fileName)) 
                                throw new Exception("Unable to upload file, please try again.", 1);
                        }
                        else
                        {
                            throw new Exception("Error Processing While Uploading", 1);
                        }
                    }
                // }else{
                //     $return['result']="Already Uploaded";
                // }
				$datasource_StaffDocument->commit();
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
				$datasource_StaffDocument->rollback();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
    public function get_document_details_ajax($id)
	{
		$return=[
			'status'=>'Empty',
			'StaffDocument'=>[],
		];
		if(isset($id))
		{
			$StaffDocument=$this->StaffDocument->findById($id);
			$return['status']="Success";
			$StaffDocument['StaffDocument']['expiry_date']=date('d-m-Y',strtotime($StaffDocument['StaffDocument']['expiry_date']));
			$return['StaffDocument']=$StaffDocument['StaffDocument'];
		}
		echo json_encode($return);
		exit;
	}
    public function get_details_by_document_type()
    {
    	$data=$this->request->data;
    	$return['status']="Success";
    	$document_type=$this->DocumentType->findById($data['document_type']);
    	if($document_type['DocumentType']['type']==1)
    	{
    		$result=$this->Staff->find('list',array('fields'=>array('Staff.id','Staff.name')));
    	}
    	else
    	{
         $result=$this->VehicleNo->find('list',array('fields'=>array('VehicleNo.id','VehicleNo.number')));
    	}
    	$return['document_type']=$document_type['DocumentType']['type'];
    	$return['option']=$result;
    	echo json_encode($return);exit;
    }
    public function document_type_add_ajax(){
        $data=$this->request->data;
        $document_exist=$this->DocumentType->findByName($data['type']);
        if(!$document_exist){
            $document_array=array(
                'name'=>strtoupper($data['name']),
                'type'=>strtoupper($data['type']),
                'created_at'=>date('Y-m-d'),
                'modified_at'=>date('Y-m-d'),
                );
            $this->DocumentType->create();
            if(!$this->DocumentType->save($document_array))
                throw new Exception("Error Processing Request", 1);
            $return['result'] = 'Success';
            $last_iserted_id = $this->DocumentType->findById($this->DocumentType->getLastInsertId());
            $return['key'] = $last_iserted_id['DocumentType']['id'];
            $return['value'] = $last_iserted_id['DocumentType']['name'];
        }else{
            $return['result'] = 'Already exist';
        }
        echo json_encode($return);
        exit;

    }
    public function StaffDocument_table_ajax()
{
		$user_role_id=$this->Session->read('UserRole.id');
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='StaffDocument.id';
		$columns[]='StaffDocument.staff_id'; 
		$columns[]='StaffDocument.document_type_id';
		$columns[]='StaffDocument.expiry_date';
		$columns[]='StaffDocument.id'; 
		$columns[]='StaffDocument.uploaded_file'; 
		$columns[]='StaffDocument.id'; 
		$conditions=[];
		$totalData=$this->StaffDocument->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'StaffDocument.expiry_date LIKE' =>'%'. $q . '%',
				'StaffDocument.uploaded_file LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->StaffDocument->find('count',[
				'conditions'=>$conditions,
			]);
		}

		$Data=$this->StaffDocument->find('all',array(
			// 'joins'=>array(
			// 	array(
			// 		'table'=>'account_heads',
			// 		'alias'=>'AccountHead',
			// 		'type'=>'INNER',
			// 		'conditions'=>array('AccountHead.id=StaffDocument.customer_id')
			// 	),
			// ),
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'StaffDocument.*',
			)
		));
		foreach ($Data as $key => $value) {
		$table_id=$value['StaffDocument']['id'];
		 $now = time();
		$Data[$key]['StaffDocument']['date']=date('d-m-Y',strtotime($value['StaffDocument']['expiry_date']));
				if($value['StaffDocument']['document_type_id']==1)
				{
				 $name=$this->Staff->field('Staff.name',array('Staff.id'=>$value['StaffDocument']['staff_id']));

				}
				else if($value['StaffDocument']['document_type_id']==2)
				{
				$name=$this->VehicleNo->field('number',array('VehicleNo.id'=>$value['StaffDocument']['staff_id']));
				}
				else
				{
				$name="COMMON";
				}
		$Data[$key]['StaffDocument']['name']=$name;
		$expected_days_diff=strtotime($value['StaffDocument']['expiry_date'])-$now;
                $diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
                $due=0;
                $Data[$key]['StaffDocument']['colour']="";
                $Data[$key]['StaffDocument']['due_colour']="";
                if($diff_day>0)
                {
                	$due=$diff_day;
                }
                 else if($diff_day<0)
                {
                 $due="Expired";
                }
                 else if($diff_day==0)
                {
                 $due="Will Expire Today";
                }
                else
                {
                 $due=0;	
                }
                if($due=="Expired")
                {
                	 $Data[$key]['StaffDocument']['due_colour']="Green";
                }
                if($diff_day<=30 && $diff_day>=0)
                {
                 $Data[$key]['StaffDocument']['colour']='Red';	
                }
               $Data[$key]['StaffDocument']['due_days']=$due;
		$document_type=$this->DocumentType->findById($value['StaffDocument']['document_type_id']);
		$Data[$key]['StaffDocument']['DocumentType']=$document_type['DocumentType']['name'];		
		$Data[$key]['StaffDocument']['action']='<span hidden class="table_id">'.$table_id.'</span>';
        if(!empty($value['StaffDocument']['uploaded_file']))
        {
		$Data[$key]['StaffDocument']['action'].="<a href='".Router::url('/', true).'profile/'.$value['StaffDocument']['uploaded_file']."' download='".$value['StaffDocument']['uploaded_file']."'><i class='fa fa-2x fa-download' aria-hidden='true'></i><a/>";
        }
		$Data[$key]['StaffDocument']['action'].="&nbsp;&nbsp;<a href='#'><i data-id='".$value['StaffDocument']['id']."' class='fa fa-2x fa-edit document_edit' aria-hidden='true'></i><a/>";
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data);
		exit;
}
public function bonus_head_create()
{
	$Data=$this->Staff->find('all',array(
			'conditions'=>array('Staff.bonus_head_id'=>'')
		));
	foreach ($Data as $key => $value) {
		        $name=$value['Staff']['name'];
	            $opening_balance = 0;
				$date = date('Y-m-d');
				$description = "";
				$function_return = $this->bonus_head_function($name,$opening_balance,$date,$description);
				if ($function_return['result'] != 'Success')
					throw new Exception($function_return['message']);
				$bonus_head_id = $function_return['bonus_head_id'];
				$data['bonus_head_id'] = $bonus_head_id;
				$this->Staff->id=$value['Staff']['id'];
				if(!$this->Staff->save($data))
				{
					$errors = $this->Staff->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
					}

exit;
}
public function bonus_head_function($name,$opening_balance,$date,$description){
		$datasource_AccountHead = $this->AccountHead->getDataSource();
		$AccountingsController = new AccountingsController;
		try {
			$datasource_AccountHead->begin();
			$name=trim($name);
			$date=date('Y-m-d');
			$group_id=13; $sub_group_id_paid       = $AccountingsController->GetSubGroupIdByGroupIdAndSubGroupName($group_id,'SALARYACCOUNT PAID');
			$group_id=5;  $prepaid_sub_group_id    = $AccountingsController->GetSubGroupIdByGroupIdAndSubGroupName($group_id,'SALARYACCOUNT PREPAID');
			$group_id=20; $outstanding_subgroup_id = $AccountingsController->GetSubGroupIdByGroupIdAndSubGroupName($group_id,'SALARYACCOUNT OUTSTANDING');
			$function_return=$AccountingsController->AccountHeadCreate($sub_group_id_paid,$name.'BONUS PAID',0,$date,$description);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message']);
			$return['bonus_head_id'] = $this->AccountHead->getLastInsertId();
			$return['result']='Success';
			$datasource_AccountHead->commit();
			$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$datasource_AccountHead->rollback();
			$return['result']='Error';
			$return['message']=$e->getMessage();
			$this->Session->setFlash(__($return['message']));
		}
		return $return;
	}
}