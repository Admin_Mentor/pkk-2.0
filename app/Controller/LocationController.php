<?php
App::uses('AppController', 'Controller');
class LocationController extends AppController {
	public $helpers= array('Html','Form');
	public $uses=array('Location');
	public function location_search()
	{
		$location_name=$this->request->data['location_name'];
		$location_name=trim($location_name);
		$Location=$this->Location->find('first',array('conditions'=>array('Location.name'=>$location_name)));
		if(!empty($Location)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}
	public function location_add_ajax()
	{
		$data=array(
			'name'=>strtoupper($this->request->data['modal_location_name']),
			
			);
		$this->Location->create();
		if($this->Location->save($data))
		{
			$this->Session->setFlash('data is saved');
			$last_iserted_pt=$this->Location->findById($this->Location->getLastInsertId());
			echo "<option value='".$last_iserted_pt['Location']['id']."'>".$last_iserted_pt['Location']['name']." </option>";
		}
		exit;
	}
	public function edit_ajax(){
		try {
			$prdct_id=$this->request->data['prdct_id'];
			if(!$prdct_id)
				throw new Exception("Empty ProductType Id", 1);
			$prdct_typ=strtoupper(trim($this->request->data['prdct_typ']));
			$this->ProductType->id=$prdct_id;
			if(!$this->ProductType->saveField('name',$prdct_typ ))
				throw new Exception("Error Processing ProductType Name Updation", 1);
			if(!$this->ProductType->saveField('updated_at',date('Y-m-d H:i:s')))
				throw new Exception("Error Processing ProductType updated_at Updation", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function warehouse_type_delete_ajax($id)
	{
		try {
			$Product=$this->Product->findByProductTypeId($id);
			if($Product)
				throw new Exception("This is Used In ".$Product['Product']['name'], 1);
			if(!$this->ProductType->delete($id))
				throw new Exception("Error Processing Request While delete", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function location_get_ajax($id)
	{
		try {
			$Location=$this->Location->findById($id);
			if(!$Location)
				throw new Exception("Empty Location", 1);
			$return['data']=$Location['Location'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function route_get_ajax($id)
	{
		try {
			$Location=$this->Route->findById($id);
			if(!$Location)
				throw new Exception("Empty Location", 1);
			$return['data']=$Location['Route'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function location_edit_ajax()
	{
		try {
			$data=$this->request->data['LocationEdit'];
			$Table_data=array(
				'name'=>trim(strtoupper($data['name'])),
				'updated_at'=>trim(date('Y-m-d h:i:s')),
				);
			$this->Location->id=$data['id'];
			if(!$this->Location->save($Table_data))
			{
				$errors = $this->Location->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$Location=$this->Location->read();
			$return['key']=$Location['Location']['id'];
			$return['value']=$Location['Location']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	
	
	
}