<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
App::import('Controller', 'Stock');
App::import('Controller', 'Sale');
ob_start();
class PrintController extends AppController
{
	public $uses = array(
		'ProductType',
		'Unit',
		'Product',
		'Stock',
		'Customer',
		'CustomerType',
		'StockLog',
		'Sale',
		'SaleItem',
		'SalesReturn',
		'SalesReturnItem',
		'Brand',
		'Journal',
		'State',
		'Profile',
		'Executive',
		'Route',
		'Batch',
		'Warehouse',
		'Purchase',
		'PurchasedItem',
		'StockTransfer',
		'StockTransferItem',
		'ExecutiveRouteMapping',
		'DebitNote',
		'DebitDetail',
		'CreditNote',
		'CreditDetail',
		'Party',
		'SubGroup',
		'AccountHead',
		'ExecutiveBonusCalculation',
		'DayRegister',
		'SystemParameter'
		);


	public function fpdfold($id=null)
	{

		$Sale = $this->Sale->find('first', array(
			"joins" => array(
				array(
					"table" => 'customers',
					"alias" => 'Customer',
					"type" => 'inner',
					"conditions" => array('Customer.account_head_id=Sale.account_head_id'),
					),
				array(
					"table" => 'customer_types',
					"alias" => 'CustomerType',
					"type" => 'inner',
					"conditions" => array('CustomerType.id=Customer.customer_type_id'),
					),
				),
			'conditions' => array('Sale.id'=>$id),
			'fields' => array(
				'Sale.*',
				'AccountHead.id',
				'AccountHead.name',
				'CustomerType.name',
				'Customer.place',
				'Customer.vat_no',
				'Customer.arabic_name',
				'Customer.mobile',
				'Customer.place',
				'Customer.route_id',
				)
			));
//pr($Sale);
//exit;
		$acc_id=$Sale['Customer']['route_id'];
		$route_mobile=$this->Route->findById($acc_id);
		$route_mobile_no=$route_mobile['Route']['mobile'];
//pr($route_mobile);
//exit;
		$name=$Sale['AccountHead']['name'];
		$invoice_no=$Sale['Sale']['invoice_no'];
		$date=$Sale['Sale']['date_of_delivered'];
		$print_field_array=[];
		$print_field_array[0]="Original For Customer";
		$print_field_array[1]="Duplicate For Salesman";
		$print_field_array[2]="Triplicate For Accounts";
		$Checkstate=0;
// pr($Sale);exit;
		if($Sale['CustomerType']['name'] != "GENERALCUSTOMER")
		{
			$Checkstate=0;
		}
		else
		{
			$Checkstate=1;
		}
		$Checkstate=0;
// if($Sale['AccountHead']['name'] == "GENERALCUSTOMER"){
//   $name=$Sale['Sale']['customer_name'];
// }else{
//   $name=$Sale['AccountHead']['name'];
// }
		$name=$Sale['AccountHead']['name'];
		$invoice_no=$Sale['Sale']['invoice_no'];
		$date=$Sale['Sale']['date_of_delivered'];
		$this->SaleItem->virtualFields = array(
			'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
			);
		$SaleItem = $this->SaleItem->find('all', array(
			'conditions' => array('SaleItem.sale_id'=>$id),
			'fields' => array(
				'SaleItem.*',
				'Product.id',
				'Product.name',
				'Product.code',
				'Product.arabic_name',
				'Product.no_of_piece_per_unit',
				)
			));
		require('tcpdf/tcpdf.php');
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Nicola Asuni');
		$pdf->SetTitle('TCPDF Example 018');
		$pdf->SetSubject('TCPDF Tutorial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
		$lg = Array();
		$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
		$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
		$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
		$Profile=$this->Global_Var_Profile['Profile'];
		$page=1;
		$total_page=1;
		$i=0;
		function convert_number_to_words($number) {
			$hyphen      = '-';
			$conjunction = ' and ';
			$separator   = ', ';
			$negative    = 'negative ';
			$decimal     = ' point ';
			$dictionary  = array(
				0                   => 'zero',
				1                   => 'one',
				2                   => 'two',
				3                   => 'three',
				4                   => 'four',
				5                   => 'five',
				6                   => 'six',
				7                   => 'seven',
				8                   => 'eight',
				9                   => 'nine',
				10                  => 'ten',
				11                  => 'eleven',
				12                  => 'twelve',
				13                  => 'thirteen',
				14                  => 'fourteen',
				15                  => 'fifteen',
				16                  => 'sixteen',
				17                  => 'seventeen',
				18                  => 'eighteen',
				19                  => 'nineteen',
				20                  => 'twenty',
				30                  => 'thirty',
				40                  => 'fourty',
				50                  => 'fifty',
				60                  => 'sixty',
				70                  => 'seventy',
				80                  => 'eighty',
				90                  => 'ninety',
				100                 => 'hundred',
				1000                => 'thousand',
				1000000             => 'million',
				1000000000          => 'billion',
				1000000000000       => 'trillion',
				1000000000000000    => 'quadrillion',
				1000000000000000000 => 'quintillion'
				);
			if (!is_numeric($number)) {
				return false;
			}
			if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
				trigger_error(
					'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
					E_USER_WARNING
					);
				return false;
			}
			if ($number < 0) {
				return $negative . convert_number_to_words(abs($number));
			}
			$string = $fraction = null;
			if (strpos($number, '.') !== false) {
				list($number, $fraction) = explode('.', $number);
			}
			switch (true) {
				case $number < 21:
				$string = $dictionary[$number];
				break;
				case $number < 100:
				$tens   = ((int) ($number / 10)) * 10;
				$units  = $number % 10;
				$string = $dictionary[$tens];
				if ($units) {
					$string .= $hyphen . $dictionary[$units];
				}
				break;
				case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
				if ($remainder) {
					$string .= $conjunction . convert_number_to_words($remainder);
				}
				break;
				default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
				if ($remainder) {
					$string .= $remainder < 100 ? $conjunction : $separator;
					$string .= convert_number_to_words($remainder);
				}
				break;
			}
			if (null !== $fraction && is_numeric($fraction)) {
				$string .= $decimal;
				$words = array();
				foreach (str_split((string) $fraction) as $number) {
					$words[] = $dictionary[$number];
				}
				$string .= implode(' ', $words);
			}
			return $string;
		}

		function convert_number_to_arabic_words($number)
		{
			$hyphen      = '-';
			$conjunction = ' و ';
			$separator   = ', ';
			$negative    = 'نفي ';
			$decimal     = ' نقطة ';
			$dictionary  = array(
				0                   => 'صفر',
				1                   => 'واحد',
				2                   => 'اثنان',
				3                   => 'ثلاثة',
				4                   => 'اربعة',
				5                   => 'خمسة',
				6                   => 'ستة',
				7                   => 'سبعة',
				8                   => 'ثمانية',
				9                   => 'تسعة',
				10                  => 'عَشْرة',
				11                  => 'أحد عشر',
				12                  => 'اثني عشر',
				13                  => 'ثلاثة عشر',
				14                  => 'أربعة عشرة',
				15                  => 'خمسة عشر',
				16                  => 'السادس عشر',
				17                  => 'سبعة عشر',
				18                  => 'الثامنة عشر',
				19                  => 'تسعة عشر',
				20                  => 'عشرون',
				30                  => 'ثلاثون',
				40                  => 'اربعون',
				50                  => 'خمسون',
				60                  => 'ستون',
				70                  => 'سبعون',
				80                  => 'ثمانون',
				90                  => 'تسعين',
				100                 => 'مائة',
				1000                => 'ألف',
				1000000             => 'مليون',
				1000000000          => 'مليار',
				1000000000000       => 'تريليون',
				1000000000000000    => 'الكدريليون رقم',
				1000000000000000000 => 'كوينتيليون'
				);
			if (!is_numeric($number)) {
				return false;
			}
			if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
				trigger_error(
					'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
					E_USER_WARNING
					);
				return false;
			}
			if ($number < 0) {
				return $negative . convert_number_to_arabic_words(abs($number));
			}
			$string = $fraction = null;
			if (strpos($number, '.') !== false) {
				list($number, $fraction) = explode('.', $number);
			}
			switch (true) {
				case $number < 21:
				$string = $dictionary[$number];
				break;
				case $number < 100:
				$tens   = ((int) ($number / 10)) * 10;
				$units  = $number % 10;
				$string = $dictionary[$tens];
				if ($units) {
					$string .= $hyphen . $dictionary[$units];
				}
				break;
				case $number < 1000:
				$hundreds  = $number / 100;
				$remainder = $number % 100;
				$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
				if ($remainder) {
					$string .= $conjunction . convert_number_to_arabic_words($remainder);
				}
				break;
				default:
				$baseUnit = pow(1000, floor(log($number, 1000)));
				$numBaseUnits = (int) ($number / $baseUnit);
				$remainder = $number % $baseUnit;
				$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
				if ($remainder) {
					$string .= $remainder < 100 ? $conjunction : $separator;
					$string .= convert_number_to_arabic_words($remainder);
				}
				break;
			}
			if (null !== $fraction && is_numeric($fraction)) {
				$string .= $decimal;
				$words = array();
				foreach (str_split((string) $fraction) as $number) {
					$words[] = $dictionary[$number];
				}
				$string .= implode(' ', $words);
			}
			return $string;
		}
		foreach ($SaleItem as $key => $value) {
			$description = str_replace('"', "'", $value['Product']['name']);
			$description_length=strlen($description);
			$description_words=explode(' ', $description);
			$word_count=count($description_words);
			$k=0;
			$poped_array=[];
			for ($j=0; $j <$word_count; $j++) {
				$poped_array[]=array_pop($description_words);
				$product_name=implode($description_words,' ');
				$product_name_length=strlen($product_name);
				if($product_name_length<=42)
				{
					$poped_array=array_reverse($poped_array);
					$k++;
					$description_words=$poped_array;
					$word_count=count($description_words);
					$poped_array=[];
					$j=0;  
				}
			}
			if($k)
				$k--;
			$i+=$k;
			if($i+$k>=20)
			{
				$total_page++;
				$i=-1;
			}
			$i++;
		}
		$igst_x_position=0;
		if($Checkstate==0)
		{
			$igst_x_position=20;
		} 


		function header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value,$route_mobile_no) 
		{
			$pdf->AddPage();
			$image_file ='profile/'.$Profile['logo'];
//$pdf->Image($image_file, 20, 10, 15, '', 'JPG', '', 'T', false, -550, '', false, false, 0, false, false, false);
			$pdf->Image($image_file,2,1,35);
// $pdf->Image('profile/'.$Profile['logo'],-5,5,-550);
// $pdf->Image('profile/'.$Profile['logo'],2,2,-525);
			$image_line_width=40;
			$invoice_x_starting=$image_line_width;
$pdf->Line($image_line_width, 1,$image_line_width, 40); // vertical line
// //$pdf->SetFont('Arial','B',12);
// $pdf->SetTextColor(0,0,100);
$pdf->rect(1, 1, 208, 295);

// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$invoice_pos+=3;
// $pdf->SetFont('Arial','B',15);
$pdf->SetFont('times', 'B', 8, "", 'false');
$pdf->SetFont('dejavusans', '', 10);

if($Sale['Sale']['sale_type1']=="CashSale"){
	$title="CASH INVOICE";
	$arabic="الفاتورة النقدية";} 
	else{$title="CREDIT INVOICE";
	$arabic="فاتورة ائتمانية";}
	$pdf->text(85,$invoice_pos,$title);
//$pdf->SetFont('dejavusans', '', 8);

	$pdf->text(120,$invoice_pos,$arabic);


// $pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
// $pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=2;
	$pdf->SetFont('dejavusans', 'B', 10);
	$pdf->text(40,$invoice_pos+3,$Profile['company_name']);
	$pdf->SetFont('dejavusans', '', 10);

	$pdf->text(140,$invoice_pos+3,$Profile['company_name_arabic']);

	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
	$pdf->text(78,$invoice_pos+3,$Profile['address_line_1']);
	$pdf->text(140,$invoice_pos+3,$Profile['address_line_1_arabic'] );

	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->text(165,33,$field_value);
	$pdf->SetFont('dejavusans', '', 10);
	$pdf->text(45,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
	$pdf->text(145,$invoice_pos+3,$Profile['address_line_2_arabic']);

	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->SetFont('times', 'U', 12, "", 'false');
	$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
	$invoice_pos+=10;
	$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);

	$pdf->text(78,$invoice_pos,'VAT Number : '.$Profile['vat_code']);
	$VAT_Number='رقم الضريبة';
	$pdf->text(140,$invoice_pos,$VAT_Number);
	$gst_details_y_staring=40;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring); // horizontal line
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos=$gst_details_y_staring+2;
$gst_line_width=120;
$pdf->Line($gst_line_width+50, $gst_details_y_staring,$gst_line_width+50, $gst_details_y_staring+17+21+5); // vertical line
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);
$pdf->text(2,$gst_pos,'Customer Name:');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(2,$gst_pos+7,'Customer No');
$pdf->text(2,$gst_pos+14,'VAT Number');
$pdf->text(2,$gst_pos+21,'Address');
$pdf->SetFont('dejavusans', '', 10);

$pdf->text($gst_line_width-250,$gst_pos,$name);
//$pdf->text($gst_line_width-5,$gst_pos,$Sale['Customer']['arabic_name']);
$pdf->text($gst_line_width-250,$gst_pos+7,$Sale['Customer']['mobile']);
$pdf->text($gst_line_width-250,$gst_pos+14,$Sale['Customer']['vat_no']);
$pdf->text($gst_line_width-250,$gst_pos+21,$Sale['Customer']['place']);
$pdf->text($gst_line_width-285,$gst_pos+28,$Sale['Sale']['invoice_no']);
$pdf->text($gst_line_width-178,$gst_pos+28,$Sale['Sale']['invoice_no']);

if(!$Sale['Sale']['date_of_delivered'])
	$invoice_date=$Sale['Sale']['date_of_order'];
else
	$invoice_date=$Sale['Sale']['date_of_delivered'];

$pdf->text($gst_line_width-290,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));
$pdf->text($gst_line_width-185,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));

$pdf->text($gst_line_width+1+68,$gst_pos,'اسم العميل');
$pdf->text($gst_line_width+1+69,$gst_pos+7,'رقم العميل');
$pdf->text($gst_line_width+1+68,$gst_pos+14,'رقم الضريبة');
$pdf->text($gst_line_width+1+76,$gst_pos+21,'عنوان.');
$pdf->text($gst_line_width+1+68,$gst_pos+28,'رقم الفاتورة');
$pdf->text($gst_line_width+1+76,$gst_pos+35,'التاريخ');

//$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos+=28;
$pdf->text(2,$gst_pos,'Invoice Number');
$gst_pos+=7;
$pdf->text(2,$gst_pos,'Date');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->SetXY(0, 5);
$pdf->Line(40, $gst_details_y_staring,40, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos+=3;

$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+20;
$Consignee_lin_width=100+1;
//$pdf->text(2,$customer_detail_startin,'Customer Name : ');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(75,$customer_detail_startin,'اﺳﻢ اﻟﺰﺑﻮﻥ : ');
$pdf->SetFont('times', '', 10, "", 'false');
//$pdf->text($Consignee_lin_width,$customer_detail_startin,'Address :'.$Sale['Customer']['place']);

$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(195,$customer_detail_startin,'ﻋﻨﻮاﻥ :');
$pdf->SetFont('times', '', 10, "", 'false');



$customer_detail_startin+=7;
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(2,$customer_detail_startin-1,$name);
//$pdf->text(60,$customer_detail_startin+3,$Sale['Customer']['arabic_name']);
$pdf->SetFont('times', '', 10, "", 'false');
$customer_detail_startin+=4;
$customer_detail_startin+=4;
$customer_detail_startin+=1;
//$pdf->text(2,$customer_detail_startin,'VAT Number :'.$Sale['Customer']['vat_no']);
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(82,$customer_detail_startin,'ﻭاﺕ ﺭﻗﻢ:');
$pdf->SetFont('times', '', 10, "", 'false');





$pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+7;
//  table head start
$table_length=135;
$pdf->text(2,$item_table_head_y_start+1,'SL');
$pdf->text(2,$item_table_head_y_start+4,'No');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text(2,$item_table_head_y_start+7, 'رقم');
$pdf->text(1,$item_table_head_y_start+10,'مسلسل');
$table_column=9;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line($table_column+25, $item_table_head_y_start,$table_column+25, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+1,'Item Code');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+5,$item_table_head_y_start+7, 'رقم الصنف ');
$pdf->Line($table_column+5, $item_table_head_y_start,$table_column+5, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+35,$item_table_head_y_start+1,'Description');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+35,$item_table_head_y_start+6,'البيان');
$table_column+=70+$igst_x_position;
$pdf->SetFont('times', '', 10, "", 'false');

$table_column+=16;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column,$item_table_head_y_start+1,'Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column,$item_table_head_y_start+6,'كمية');
$pdf->SetFont('times', '', 10, "", 'false');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->Line($table_column+11, $item_table_head_y_start,$table_column+11, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+1,$item_table_head_y_start+1,'Unit');
$pdf->text($table_column+11,$item_table_head_y_start+1,'Rate');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+1,$item_table_head_y_start+6,'وحدة');
$pdf->Text($table_column+11,$item_table_head_y_start+6,'سعر');
$table_column+=25;
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
// $table_column+=2;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
//$pdf->text($table_column+1,$item_table_head_y_start+6,'Discount');
//$table_column+=14;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column,$item_table_head_y_start+1,'Tx Value');
//$pdf->text($table_column,$item_table_head_y_start+4,'Excl VAT');

$pdf->SetFont('dejavusans', '', 8);
//قيمة الضريبة

$pdf->Text($table_column,$item_table_head_y_start+8,'اﻟﻤﺒﻠﻎ قيمة');
//$pdf->Text($table_column,$item_table_head_y_start+11,'اﺯاﻟﺔ ﻭاﺕ');
$pdf->SetFont('times', '', 10, "", 'false'); 
$table_column+=18.5;
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line

if($Checkstate==1)
{

$pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->text($table_column+6,$item_table_head_y_start+3,'CGST');
$pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
$table_column+=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+6,$item_table_head_y_start+3,'SGST');
$pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
}
else
{

$pdf->Line($table_column, $gst_details_y_staring+17+5+21+6,172+$igst_x_position, $gst_details_y_staring+17+5+21+6); // horizontal line
$pdf->text($table_column+3,$item_table_head_y_start+1,'VAT');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($table_column+11,$item_table_head_y_start+1,'ﺿﺮﻳﺒﺔ');
$pdf->SetFont('times', '', 10, "", 'false'); 

$pdf->text($table_column+1,$item_table_head_y_start+7,'Rate');
$pdf->SetFont('dejavusans', '', 8);

$pdf->Text($table_column+1,$item_table_head_y_start+10,'ﻣﻌﺪﻝ');
$table_column+=14;
$pdf->SetFont('times', '', 10, "", 'false'); 
$pdf->Line($table_column, $item_table_head_y_start+6,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+1,$item_table_head_y_start+7,'Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+1,$item_table_head_y_start+10,'ﻛﻤﻴﺔ');
$pdf->SetFont('times', '', 10, "", 'false');
}
$table_column+=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line

$pdf->text($table_column+3,$item_table_head_y_start+1,'Total');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+2,$item_table_head_y_start+4,'ﻣﺠﻤﻮﻉ');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line(1, $gst_details_y_staring+17+5+21+15,209, $gst_details_y_staring+17+5+21+15); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length,209, $item_table_head_y_start+$table_length); // horizontal line
// $pdf->Line(1, $item_table_head_y_start+$table_length+5,209, $item_table_head_y_start+$table_length+5); // horizontal line
//$pdf->text(12,$item_table_head_y_start+$table_length,'Total');
$pdf->SetFont('dejavusans', '', 10);

$pdf->SetFont('times', '', 10, "", 'false');

}

function footer($pdf,$data,$Sale,$route_mobile_no)
{
	$footer_start_y=218;
$pdf->Line(1, $footer_start_y,209, $footer_start_y); // horizontal line
$invoce_word_width=113;
$grand_total_length=30;
$pdf->Line($invoce_word_width, $footer_start_y,$invoce_word_width, $footer_start_y+$grand_total_length+4); // vertical line
//$pdf->text(10,$footer_start_y+1,'Amount In Words');
$pdf->SetFont('dejavusans', '', 8);
// $pdf->Text(85,$footer_start_y+1,'اﻟﻤﺒﻠﻎ ﺑﺎﻟﻜﻠﻤﺎﺕ');
$pdf->SetFont('times', '', 8, "", 'false');
$actual_total=$Sale['Sale']['total']-$data['total_cgst_tax']-$data['total_sgst_tax']-$data['total_igst_tax'];
$actual_grand_total=$Sale['Sale']['grand_total'];
$convert_number_to_words=strtoupper(convert_number_to_words(floatval($actual_grand_total)));
$convert_number_to_arabic_words=strtoupper(convert_number_to_arabic_words(floatval($actual_grand_total)));
//$pdf->text(5,$footer_start_y+3+10,$convert_number_to_words);
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(40,$footer_start_y+3+10,$convert_number_to_arabic_words);
//$pdf->Text($invoice_lin_width+130,$customer_detail_startin+4+35,strtoupper(convert_number_to_arabic_words($amount)).' '.'الريال فقط' );
$pdf->Line($invoce_word_width+46, $footer_start_y,$invoce_word_width+46, $footer_start_y+$grand_total_length); // vertical line
$Total_pos=1;
$pdf->text($invoce_word_width,$footer_start_y+$Total_pos,'Total Amount');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+22,$footer_start_y+$Total_pos,'اجمالى القيمة');
$pdf->SetFont('times', '', 8, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,number_format($data['total'],2));

$Total_pos+=5;
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+2,'VAT Amount');

$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+28,$footer_start_y+$Total_pos+2,'قيمة الضريبة');
$pdf->SetFont('times', '', 8, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos+2,number_format($data['total_igst_tax'],2));
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+1,'Discount');

$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+28,$footer_start_y+$Total_pos+1,'الخصم');
$pdf->SetFont('times', '', 8, "", 'false');
//   //$other_value=round($data['total'])-$data['total'];
$other_value=$Sale['Sale']['other_value'];
$total_discount=$Sale['Sale']['discount_amount'];
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos+1,$total_discount);

$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+5,'Net Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($invoce_word_width+23,$footer_start_y+$Total_pos+5,'المبلغ الصافى');
$pdf->SetFont('times', '', 12, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos+5,number_format($actual_grand_total,2));
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
$tax_subject_y=$footer_start_y+$grand_total_length;
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->Line(1, $tax_subject_y+4,209, $tax_subject_y+4); // horizontal line
$pdf->text(2,$tax_subject_y+5,'Received By');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(85,$tax_subject_y+5,'المستلم');
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->text(115,$tax_subject_y+5,'Salesman');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(190,$tax_subject_y+5,'البائغ');البائغ:
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->text(115,$tax_subject_y+10,'Route Mobile No');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(190,$tax_subject_y+10,'طريق الجوال');
$pdf->SetFont('times', '', 8, "", 'false');
// $executive_no=$Sale['Executive']['mobile'];

$pdf->text(150,$tax_subject_y+10,$route_mobile_no);
$pdf->Line($invoce_word_width+46, $tax_subject_y,$invoce_word_width+46, $tax_subject_y+4); // vertical line
$signature_area_y=$tax_subject_y+4;
$signature_length=14;
$signature_width=113;

$pdf->SetFont('dejavusans', '', 8);


$pdf->SetFont('times', '', 8, "", 'false');
$pdf->SetXY($signature_width, $signature_area_y);

$pdf->Line($signature_width, $signature_area_y,$signature_width, $signature_area_y+$signature_length); // vertical line
$pdf->Line(1, $signature_area_y+$signature_length,209, $signature_area_y+$signature_length); // horizontal line

}
$page=0;


foreach($print_field_array as $field_key=>$field_value)
{
//$page=1;
	$page++;
	header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value,$route_mobile_no);
	$total=0;
	$total_cgst_tax=0;
	$total_sgst_tax=0;
	$total_igst_tax=0;
	$item_table_head_y_start='85';
	$table_length='128';
	$cgst_grand_amount=0;
	$sgst_grand_amount=0;
	$igst_grand_amount=0;
	$rate_grand_amount=0;
	$discount_grand_amount=0;
	$taxable_value_grand_amount=0;
	$product_grand_total=0;
	$count=count($SaleItem);
	$i=0;
// for ($i=0; $i <$count ; $i++) { 
	$data=array(
		'total'=>$total,
		'total_cgst_tax'=>$total_cgst_tax,
		'total_sgst_tax'=>$total_sgst_tax,
		'total_igst_tax'=>$total_igst_tax,
		);


	foreach ($SaleItem as $keySI => $value) {
		$description = str_replace('"', "'", $value['SaleItem']['product_name']);
// $hsn_code=$value['Product']['hsn_code'];
		$qty=floatval($value['SaleItem']['quantity']);
		$unit_level=$this->Unit->findByName($value['SaleItem']['quantity_mode']);
		if(empty($unit_level)){
			$unit_level['Unit']['level'] =1;
		}
		if($unit_level['Unit']['level'] !=1){
			$value['SaleItem']['quantity']=$value['SaleItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
			$qty = $value['SaleItem']['quantity'];
		}
		$uom=$value['SaleItem']['quantity_mode'];
		$rate=floatval($value['SaleItem']['unit_price']);
		$unit=$value['SaleItem']['quantity_mode'];
		$rate_grand_amount+=$rate;
		$discount=0;


		$discount_grand_amount+=$discount;
		$taxable_value=floatval($value['SaleItem']['net_value']);
		$taxable_value_grand_amount+=$taxable_value;
		$net_value=floatval($value['SaleItem']['net_value']);

		$tax_value=floatval($value['SaleItem']['tax_amount']);
		$total+=$net_value;


		$data['total']+=floatval($net_value);
		$data['total_cgst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$data['total_sgst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$data['total_igst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$product_total=floatval($value['SaleItem']['total']);
		$product_grand_total+=$product_total;
		$cgst_rate=floatval($value['SaleItem']['tax']);
		$cgst_amount=floatval($value['SaleItem']['tax_amount']);
		$cgst_grand_amount+=$cgst_amount;
		$sgst_rate=floatval($value['SaleItem']['tax']);
		$sgst_amount=floatval($value['SaleItem']['tax_amount']);
		$sgst_grand_amount+=$sgst_amount;
		$igst_rate=floatval($value['SaleItem']['tax']);
		$igst_amount=floatval($value['SaleItem']['tax_amount']);
		$igst_grand_amount+=$igst_amount;
		$item_pos=2;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$keySI+1);
		$item_pos+=6;
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$product_name=implode($description_words,' ');
			$poped_array[]=array_pop($description_words);
			$product_name_length=strlen($product_name);
			if($product_name_length<=40)
			{ 
				$pdf->SetFont('dejavusans', '', 8);

				$pdf->text($item_pos+8,$item_table_head_y_start+8+(($i+$k)*8)+5,$value['Product']['code']);
				$arabic=$value['Product']['name'];
				$pdf->text($item_pos+26,$item_table_head_y_start+8+(($i+$k)*8)+5,$arabic);
				$pdf->text($item_pos+26,$item_table_head_y_start+8+(($i+$k)*8)+8,$value['Product']['arabic_name']);
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}

		if($k)
			$k--;
		$item_pos+=1;
		$pdf->SetFont('dejavusans', '', 8);
// $pdf->AddFont('DejaVu','','tahoma.ttf',true);
// $pdf->AddFont('DejaVu', 'B', 'tahoma-Bold.ttf', true);
// $pdf->SetFont('DejaVu','',8);
// $pdf->Text($item_pos,$item_table_head_y_start+11+($i*8)+5,$value['Product']['arabic_name']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$item_pos+=70+$igst_x_position;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$hsn_code);
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,'');
		$item_pos+=16;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$qty);
		$pdf->text($item_pos+8,$item_table_head_y_start+8+($i*8)+5,$unit);
		$item_pos+=20;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($rate,2));
		$item_pos+=10;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),number_format($rate,2));
		if(count($SaleItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$rate_grand_amount);
		}
		$item_pos+=5;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$discount);
		if(count($SaleItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$discount_grand_amount);
		}
// $item_pos+=1;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($taxable_value,2));
		if(count($SaleItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($taxable_value_grand_amount,2));
		}
		$item_pos+=21;
		if($Checkstate==1)
		{
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_rate.'%');
			$item_pos+=7;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_amount);
			if(count($SaleItem)==$keySI+1)
			{
// $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$cgst_grand_amount);
			}
			$item_pos+=15;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_rate.'%');
			$item_pos+=8;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_amount);
			if(count($SaleItem)==$keySI+1)
			{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$sgst_grand_amount);
			}
		}
		else
		{
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$igst_rate.'%');
			$item_pos+=13;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($igst_amount,2));
			if(count($SaleItem)==$keySI+1)
			{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($igst_grand_amount,2));
			}
		}

		$item_pos+=12;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_rate);
		$item_pos+=5;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_amount);
		$pdf->text($item_pos-6,$item_table_head_y_start+8+($i*8)+5,number_format($product_total,2));
		if(count($SaleItem)==$keySI+1)
		{

//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$igst_grand_amount);
//$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($product_grand_total,2));
		}
		$i+=$k;
		$count+=$k;
		if($i+$k>=13)
		{
//footer($pdf,$data,$Sale,$route_mobile_no);
			$page++;
			header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value,$route_mobile_no);
			$i=-1;
			$count-=13;
		}
		$i++;
	}

	footer($pdf,$data,$Sale,$route_mobile_no);
}
$pdf->Output();
exit;

}
public function fpdf_quotationold($id=null)
{

	$Sale = $this->Sale->find('first', array(
		"joins" => array(
			array(
				"table" => 'customers',
				"alias" => 'Customer',
				"type" => 'inner',
				"conditions" => array('Customer.account_head_id=Sale.account_head_id'),
				),
			array(
				"table" => 'customer_types',
				"alias" => 'CustomerType',
				"type" => 'inner',
				"conditions" => array('CustomerType.id=Customer.customer_type_id'),
				),
			),
		'conditions' => array('Sale.id'=>$id),
		'fields' => array(
			'Sale.*',
			'AccountHead.name',
			'CustomerType.name',
			'Customer.place',
			'Customer.vat_no',
			'Customer.arabic_name',
			'Customer.mobile',
			'Customer.place',
			)
		));
	$name=$Sale['AccountHead']['name'];
	$invoice_no=$Sale['Sale']['invoice_no'];
	$date=$Sale['Sale']['date_of_delivered'];
	$print_field_array=[];
	$print_field_array[0]="Original For Customer";
	$print_field_array[1]="Duplicate For Salesman";
	$print_field_array[2]="Triplicate For Accounts";
	$Checkstate=0;
// pr($Sale);exit;
	if($Sale['CustomerType']['name'] != "GENERALCUSTOMER")
	{
		$Checkstate=0;
	}
	else
	{
		$Checkstate=1;
	}
	$Checkstate=0;
// if($Sale['AccountHead']['name'] == "GENERALCUSTOMER"){
//   $name=$Sale['Sale']['customer_name'];
// }else{
//   $name=$Sale['AccountHead']['name'];
// }
	$name=$Sale['AccountHead']['name'];
	$invoice_no=$Sale['Sale']['invoice_no'];
	$date=$Sale['Sale']['date_of_delivered'];
	$this->SaleItem->virtualFields = array(
		'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
		);
	$SaleItem = $this->SaleItem->find('all', array(
		'conditions' => array('SaleItem.sale_id'=>$id),
		'fields' => array(
			'SaleItem.*',
			'Product.id',
			'Product.name',
			'Product.code',
			'Product.arabic_name',
			'Product.no_of_piece_per_unit',
			)
		));
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}
	foreach ($SaleItem as $key => $value) {
		$description = str_replace('"', "'", $value['Product']['name']);
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$poped_array[]=array_pop($description_words);
			$product_name=implode($description_words,' ');
			$product_name_length=strlen($product_name);
			if($product_name_length<=42)
			{
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}
		if($k)
			$k--;
		$i+=$k;
		if($i+$k>=20)
		{
			$total_page++;
			$i=-1;
		}
		$i++;
	}
	$igst_x_position=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 


	function header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value) 
	{
		$pdf->AddPage();
		$image_file ='profile/'.$Profile['logo'];
//$pdf->Image($image_file, 20, 10, 15, '', 'JPG', '', 'T', false, -550, '', false, false, 0, false, false, false);
		$pdf->Image($image_file,2,1,35);
// $pdf->Image('profile/'.$Profile['logo'],-5,5,-550);
// $pdf->Image('profile/'.$Profile['logo'],2,2,-525);
		$image_line_width=40;
		$invoice_x_starting=$image_line_width;
$pdf->Line($image_line_width, 1,$image_line_width, 40); // vertical line
// //$pdf->SetFont('Arial','B',12);
// $pdf->SetTextColor(0,0,100);
$pdf->rect(1, 1, 208, 295);

// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$invoice_pos+=3;
// $pdf->SetFont('Arial','B',15);
$pdf->SetFont('times', 'B', 8, "", 'false');
$pdf->SetFont('dejavusans', '', 10);

// if($Sale['Sale']['sale_type1']=="CashSale"){
$title="QUOTATION";
$arabic="اقتباس"; 
// else{$title="CREDIT INVOICE";
// $arabic="فاتورة ائتمانية";}
$pdf->text(85,$invoice_pos,$title);
//$pdf->SetFont('dejavusans', '', 8);

$pdf->text(120,$invoice_pos,$arabic);


// $pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=2;
$pdf->SetFont('dejavusans', 'B', 10);
$pdf->text(80,$invoice_pos+3,$Profile['company_name']);
$pdf->SetFont('dejavusans', '', 10);

$pdf->text(130,$invoice_pos+3,$Profile['company_name_arabic']);

$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
$pdf->text(78,$invoice_pos+3,$Profile['address_line_1']);
$pdf->text(140,$invoice_pos+3,$Profile['address_line_1_arabic'] );

$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->text(168,33,$field_value);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(60,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
$pdf->text(160,$invoice_pos+3,$Profile['address_line_2_arabic']);

$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->SetFont('times', 'U', 12, "", 'false');
$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
$invoice_pos+=10;
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);

$pdf->text(78,$invoice_pos,'VAT Number : '.$Profile['vat_code']);
$VAT_Number='رقم الضريبة';
$pdf->text(140,$invoice_pos,$VAT_Number);
$gst_details_y_staring=40;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring); // horizontal line
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos=$gst_details_y_staring+2;
$gst_line_width=120;
$pdf->Line($gst_line_width+50, $gst_details_y_staring,$gst_line_width+50, $gst_details_y_staring+17+21+5); // vertical line
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);
$pdf->text(2,$gst_pos,'Customer Name:');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(2,$gst_pos+7,'Customer No');
$pdf->text(2,$gst_pos+14,'VAT Number');
$pdf->text(2,$gst_pos+21,'Address');
$pdf->SetFont('dejavusans', '', 10);

$pdf->text($gst_line_width-250,$gst_pos,$name);
$pdf->text($gst_line_width-5,$gst_pos,$Sale['Customer']['arabic_name']);
$pdf->text($gst_line_width-250,$gst_pos+7,$Sale['Customer']['mobile']);
$pdf->text($gst_line_width-250,$gst_pos+14,$Sale['Customer']['vat_no']);
$pdf->text($gst_line_width-250,$gst_pos+21,$Sale['Customer']['place']);
$pdf->text($gst_line_width-285,$gst_pos+28,$Sale['Sale']['invoice_no']);
$pdf->text($gst_line_width-178,$gst_pos+28,$Sale['Sale']['invoice_no']);

if(!$Sale['Sale']['date_of_delivered'])
	$invoice_date=$Sale['Sale']['date_of_order'];
else
	$invoice_date=$Sale['Sale']['date_of_delivered'];

$pdf->text($gst_line_width-290,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));
$pdf->text($gst_line_width-185,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));

$pdf->text($gst_line_width+1+68,$gst_pos,'اسم العميل');
$pdf->text($gst_line_width+1+69,$gst_pos+7,'رقم العميل');
$pdf->text($gst_line_width+1+68,$gst_pos+14,'رقم الضريبة');
$pdf->text($gst_line_width+1+76,$gst_pos+21,'عنوان.');
$pdf->text($gst_line_width+1+68,$gst_pos+28,'رقم الفاتورة');
$pdf->text($gst_line_width+1+76,$gst_pos+35,'التاريخ');

//$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos+=28;
$pdf->text(2,$gst_pos,'Invoice Number');
$gst_pos+=7;
$pdf->text(2,$gst_pos,'Date');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->SetXY(0, 5);
$pdf->Line(40, $gst_details_y_staring,40, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos+=3;

$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+20;
$Consignee_lin_width=100+1;
//$pdf->text(2,$customer_detail_startin,'Customer Name : ');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(75,$customer_detail_startin,'اﺳﻢ اﻟﺰﺑﻮﻥ : ');
$pdf->SetFont('times', '', 10, "", 'false');
//$pdf->text($Consignee_lin_width,$customer_detail_startin,'Address :'.$Sale['Customer']['place']);

$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(195,$customer_detail_startin,'ﻋﻨﻮاﻥ :');
$pdf->SetFont('times', '', 10, "", 'false');



$customer_detail_startin+=7;
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(2,$customer_detail_startin-1,$name);
//$pdf->text(60,$customer_detail_startin+3,$Sale['Customer']['arabic_name']);
$pdf->SetFont('times', '', 10, "", 'false');
$customer_detail_startin+=4;
$customer_detail_startin+=4;
$customer_detail_startin+=1;
//$pdf->text(2,$customer_detail_startin,'VAT Number :'.$Sale['Customer']['vat_no']);
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(82,$customer_detail_startin,'ﻭاﺕ ﺭﻗﻢ:');
$pdf->SetFont('times', '', 10, "", 'false');





$pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+7;
//  table head start
$table_length=135;
$pdf->text(2,$item_table_head_y_start+1,'SL');
$pdf->text(2,$item_table_head_y_start+4,'No');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text(2,$item_table_head_y_start+7, 'رقم');
$pdf->text(1,$item_table_head_y_start+10,'مسلسل');
$table_column=9;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line($table_column+25, $item_table_head_y_start,$table_column+25, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+1,'Item Code');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+5,$item_table_head_y_start+7, 'رقم الصنف ');
$pdf->Line($table_column+5, $item_table_head_y_start,$table_column+5, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+35,$item_table_head_y_start+1,'Description');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+35,$item_table_head_y_start+6,'البيان');
$table_column+=70+$igst_x_position;
$pdf->SetFont('times', '', 10, "", 'false');

$table_column+=16;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column,$item_table_head_y_start+1,'Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column,$item_table_head_y_start+6,'كمية');
$pdf->SetFont('times', '', 10, "", 'false');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->Line($table_column+11, $item_table_head_y_start,$table_column+11, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+1,$item_table_head_y_start+1,'Unit');
$pdf->text($table_column+11,$item_table_head_y_start+1,'Rate');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+1,$item_table_head_y_start+6,'وحدة');
$pdf->Text($table_column+11,$item_table_head_y_start+6,'سعر');
$table_column+=25;
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
// $table_column+=2;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
//$pdf->text($table_column+1,$item_table_head_y_start+6,'Discount');
//$table_column+=14;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column,$item_table_head_y_start+1,'Tx Value');
//$pdf->text($table_column,$item_table_head_y_start+4,'Excl VAT');

$pdf->SetFont('dejavusans', '', 8);
//قيمة الضريبة

$pdf->Text($table_column,$item_table_head_y_start+8,'اﻟﻤﺒﻠﻎ قيمة');
//$pdf->Text($table_column,$item_table_head_y_start+11,'اﺯاﻟﺔ ﻭاﺕ');
$pdf->SetFont('times', '', 10, "", 'false'); 
$table_column+=18.5;
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line

if($Checkstate==1)
{

$pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->text($table_column+6,$item_table_head_y_start+3,'CGST');
$pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
$table_column+=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+6,$item_table_head_y_start+3,'SGST');
$pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
}
else
{

$pdf->Line($table_column, $gst_details_y_staring+17+5+21+6,172+$igst_x_position, $gst_details_y_staring+17+5+21+6); // horizontal line
$pdf->text($table_column+3,$item_table_head_y_start+1,'VAT');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($table_column+11,$item_table_head_y_start+1,'ﺿﺮﻳﺒﺔ');
$pdf->SetFont('times', '', 10, "", 'false'); 

$pdf->text($table_column+1,$item_table_head_y_start+7,'Rate');
$pdf->SetFont('dejavusans', '', 8);

$pdf->Text($table_column+1,$item_table_head_y_start+10,'ﻣﻌﺪﻝ');
$table_column+=14;
$pdf->SetFont('times', '', 10, "", 'false'); 
$pdf->Line($table_column, $item_table_head_y_start+6,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+1,$item_table_head_y_start+7,'Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+1,$item_table_head_y_start+10,'ﻛﻤﻴﺔ');
$pdf->SetFont('times', '', 10, "", 'false');
}
$table_column+=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line

$pdf->text($table_column+3,$item_table_head_y_start+1,'Total');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+2,$item_table_head_y_start+4,'ﻣﺠﻤﻮﻉ');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line(1, $gst_details_y_staring+17+5+21+15,209, $gst_details_y_staring+17+5+21+15); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length,209, $item_table_head_y_start+$table_length); // horizontal line
// $pdf->Line(1, $item_table_head_y_start+$table_length+5,209, $item_table_head_y_start+$table_length+5); // horizontal line
//$pdf->text(12,$item_table_head_y_start+$table_length,'Total');
$pdf->SetFont('dejavusans', '', 10);

$pdf->SetFont('times', '', 10, "", 'false');

}

function footer($pdf,$data,$Sale)
{
	$footer_start_y=218;
$pdf->Line(1, $footer_start_y,209, $footer_start_y); // horizontal line
$invoce_word_width=113;
$grand_total_length=30;
$pdf->Line($invoce_word_width, $footer_start_y,$invoce_word_width, $footer_start_y+$grand_total_length+4); // vertical line
//$pdf->text(10,$footer_start_y+1,'Amount In Words');
$pdf->SetFont('dejavusans', '', 8);
// $pdf->Text(85,$footer_start_y+1,'اﻟﻤﺒﻠﻎ ﺑﺎﻟﻜﻠﻤﺎﺕ');
$pdf->SetFont('times', '', 8, "", 'false');
$actual_total=$Sale['Sale']['total']-$data['total_cgst_tax']-$data['total_sgst_tax']-$data['total_igst_tax'];
$actual_grand_total=$Sale['Sale']['grand_total'];
$convert_number_to_words=strtoupper(convert_number_to_words(floatval($actual_grand_total)));
$convert_number_to_arabic_words=strtoupper(convert_number_to_arabic_words(floatval($actual_grand_total)));
//$pdf->text(5,$footer_start_y+3+10,$convert_number_to_words);
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(40,$footer_start_y+3+10,$convert_number_to_arabic_words);
//$pdf->Text($invoice_lin_width+130,$customer_detail_startin+4+35,strtoupper(convert_number_to_arabic_words($amount)).' '.'الريال فقط' );
$pdf->Line($invoce_word_width+46, $footer_start_y,$invoce_word_width+46, $footer_start_y+$grand_total_length); // vertical line
$Total_pos=1;
$pdf->text($invoce_word_width,$footer_start_y+$Total_pos,'Total Amount');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+22,$footer_start_y+$Total_pos,'اجمالى القيمة');
$pdf->SetFont('times', '', 8, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,number_format($data['total'],2));

$Total_pos+=5;
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+2,'VAT Amount');

$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+28,$footer_start_y+$Total_pos+2,'قيمة الضريبة');
$pdf->SetFont('times', '', 8, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos+2,number_format($data['total_igst_tax'],2));
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+1,'Discount');

$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+28,$footer_start_y+$Total_pos+1,'الخصم');
$pdf->SetFont('times', '', 8, "", 'false');
//   //$other_value=round($data['total'])-$data['total'];
$other_value=$Sale['Sale']['other_value'];
$total_discount=$Sale['Sale']['discount_amount'];
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos+1,$total_discount);

$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+5,'Net Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($invoce_word_width+23,$footer_start_y+$Total_pos+5,'المبلغ الصافى');
$pdf->SetFont('times', '', 12, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos+5,number_format($actual_grand_total,2));
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
$tax_subject_y=$footer_start_y+$grand_total_length;
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->Line(1, $tax_subject_y+4,209, $tax_subject_y+4); // horizontal line
$pdf->text(2,$tax_subject_y+5,'Received By');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(85,$tax_subject_y+5,'المستلم');
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->text(115,$tax_subject_y+5,'Salesman');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(190,$tax_subject_y+5,'البائغ');البائغ:
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->Line($invoce_word_width+46, $tax_subject_y,$invoce_word_width+46, $tax_subject_y+4); // vertical line
$signature_area_y=$tax_subject_y+4;
$signature_length=14;
$signature_width=113;

$pdf->SetFont('dejavusans', '', 8);


$pdf->SetFont('times', '', 8, "", 'false');
$pdf->SetXY($signature_width, $signature_area_y);

$pdf->Line($signature_width, $signature_area_y,$signature_width, $signature_area_y+$signature_length); // vertical line
$pdf->Line(1, $signature_area_y+$signature_length,209, $signature_area_y+$signature_length); // horizontal line

}
$page=0;


foreach($print_field_array as $field_key=>$field_value)
{
//$page=1;
	$page++;
	header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value);
	$total=0;
	$total_cgst_tax=0;
	$total_sgst_tax=0;
	$total_igst_tax=0;
	$item_table_head_y_start='85';
	$table_length='128';
	$cgst_grand_amount=0;
	$sgst_grand_amount=0;
	$igst_grand_amount=0;
	$rate_grand_amount=0;
	$discount_grand_amount=0;
	$taxable_value_grand_amount=0;
	$product_grand_total=0;
	$count=count($SaleItem);
	$i=0;
// for ($i=0; $i <$count ; $i++) { 
	$data=array(
		'total'=>$total,
		'total_cgst_tax'=>$total_cgst_tax,
		'total_sgst_tax'=>$total_sgst_tax,
		'total_igst_tax'=>$total_igst_tax,
		);


	foreach ($SaleItem as $keySI => $value) {
		$description = str_replace('"', "'", $value['SaleItem']['product_name']);
// $hsn_code=$value['Product']['hsn_code'];
		$qty=floatval($value['SaleItem']['quantity']);
		$unit_level=$this->Unit->findByName($value['SaleItem']['quantity_mode']);
		if(empty($unit_level)){
			$unit_level['Unit']['level'] =1;
		}
		if($unit_level['Unit']['level'] !=1){
			$value['SaleItem']['quantity']=$value['SaleItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
			$qty = $value['SaleItem']['quantity'];
		}
		$uom=$value['SaleItem']['quantity_mode'];
		$rate=floatval($value['SaleItem']['unit_price']);
		$unit=$value['SaleItem']['quantity_mode'];
		$rate_grand_amount+=$rate;
		$discount=0;


		$discount_grand_amount+=$discount;
		$taxable_value=floatval($value['SaleItem']['net_value']);
		$taxable_value_grand_amount+=$taxable_value;
		$net_value=floatval($value['SaleItem']['net_value']);

		$tax_value=floatval($value['SaleItem']['tax_amount']);
		$total+=$net_value;


		$data['total']+=floatval($net_value);
		$data['total_cgst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$data['total_sgst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$data['total_igst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$product_total=floatval($value['SaleItem']['total']);
		$product_grand_total+=$product_total;
		$cgst_rate=floatval($value['SaleItem']['tax']);
		$cgst_amount=floatval($value['SaleItem']['tax_amount']);
		$cgst_grand_amount+=$cgst_amount;
		$sgst_rate=floatval($value['SaleItem']['tax']);
		$sgst_amount=floatval($value['SaleItem']['tax_amount']);
		$sgst_grand_amount+=$sgst_amount;
		$igst_rate=floatval($value['SaleItem']['tax']);
		$igst_amount=floatval($value['SaleItem']['tax_amount']);
		$igst_grand_amount+=$igst_amount;
		$item_pos=2;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$keySI+1);
		$item_pos+=6;
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$product_name=implode($description_words,' ');
			$poped_array[]=array_pop($description_words);
			$product_name_length=strlen($product_name);
			if($product_name_length<=40)
			{ 
				$pdf->SetFont('dejavusans', '', 8);

				$pdf->text($item_pos+8,$item_table_head_y_start+8+(($i+$k)*8)+5,$value['Product']['code']);
				$arabic=$value['Product']['name'];
				$pdf->text($item_pos+26,$item_table_head_y_start+8+(($i+$k)*8)+5,$arabic);
				$pdf->text($item_pos+26,$item_table_head_y_start+8+(($i+$k)*8)+8,$value['Product']['arabic_name']);
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}

		if($k)
			$k--;
		$item_pos+=1;
		$pdf->SetFont('dejavusans', '', 8);
// $pdf->AddFont('DejaVu','','tahoma.ttf',true);
// $pdf->AddFont('DejaVu', 'B', 'tahoma-Bold.ttf', true);
// $pdf->SetFont('DejaVu','',8);
// $pdf->Text($item_pos,$item_table_head_y_start+11+($i*8)+5,$value['Product']['arabic_name']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$item_pos+=70+$igst_x_position;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$hsn_code);
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,'');
		$item_pos+=16;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$qty);
		$pdf->text($item_pos+8,$item_table_head_y_start+8+($i*8)+5,$unit);
		$item_pos+=20;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($rate,2));
		$item_pos+=10;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),number_format($rate,2));
		if(count($SaleItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$rate_grand_amount);
		}
		$item_pos+=5;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$discount);
		if(count($SaleItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$discount_grand_amount);
		}
// $item_pos+=1;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($taxable_value,2));
		if(count($SaleItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($taxable_value_grand_amount,2));
		}
		$item_pos+=21;
		if($Checkstate==1)
		{
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_rate.'%');
			$item_pos+=7;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_amount);
			if(count($SaleItem)==$keySI+1)
			{
// $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$cgst_grand_amount);
			}
			$item_pos+=15;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_rate.'%');
			$item_pos+=8;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_amount);
			if(count($SaleItem)==$keySI+1)
			{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$sgst_grand_amount);
			}
		}
		else
		{
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$igst_rate.'%');
			$item_pos+=13;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($igst_amount,2));
			if(count($SaleItem)==$keySI+1)
			{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($igst_grand_amount,2));
			}
		}

		$item_pos+=12;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_rate);
		$item_pos+=5;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_amount);
		$pdf->text($item_pos-6,$item_table_head_y_start+8+($i*8)+5,number_format($product_total,2));
		if(count($SaleItem)==$keySI+1)
		{

//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$igst_grand_amount);
//$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($product_grand_total,2));
		}
		$i+=$k;
		$count+=$k;
		if($i+$k>=13)
		{
			footer($pdf,$data,$Sale);
			$page++;
			header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value);
			$i=-1;
			$count-=13;
		}
		$i++;
	}

	footer($pdf,$data,$Sale);
}
$pdf->Output();
exit;

}
public function fpdf_purchase($id=null)
{

	$Purchase = $this->Purchase->find('first', array(
		"joins" => array(
			array(
				"table" => 'parties',
				"alias" => 'Party',
				"type" => 'inner',
				"conditions" => array('Party.account_head_id=Purchase.account_head_id'),
				),
			),
		'conditions' => array('Purchase.id'=>$id),
		'fields' => array(
			'AccountHead.*',
			'Party.*',
			'Purchase.*',
			)
		));
	$name=$Purchase['AccountHead']['name'];
	$state_id=$Purchase['Party']['state_id'];
	$invoice_no=$Purchase['Purchase']['invoice_no'];
	$date=$Purchase['Purchase']['date_of_delivered'];
	$print_field_array=[];
	$print_field_array[0]="Original For Party";
//$print_field_array[1]="Duplicate For Accounts";
//$print_field_array[2]="Triplicate For Accounts";
	$this->PurchasedItem->virtualFields = array(
		'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
		);
	$PurchasedItem = $this->PurchasedItem->find('all', array(
		'conditions' => array('PurchasedItem.purchase_id'=>$id),
		'fields' => array(
			'PurchasedItem.*',
			'Product.id',
			'Product.name',
			'Product.code',
			'Product.arabic_name',
			'Product.no_of_piece_per_unit',
			)
		));
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}
	foreach ($PurchasedItem as $key => $value) {
		$description = str_replace('"', "'", $value['Product']['name']);
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$poped_array[]=array_pop($description_words);
			$product_name=implode($description_words,' ');
			$product_name_length=strlen($product_name);
			if($product_name_length<=42)
			{
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}
		if($k)
			$k--;
		$i+=$k;
		if($i+$k>=20)
		{
			$total_page++;
			$i=-1;
		}
		$i++;
	}
	$igst_x_position=0;
	$Checkstate=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 
	function header_section($pdf,$Purchase,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value) 
	{
		$pdf->AddPage();
		$image_file ='profile/'.$Profile['logo'];
//$pdf->Image($image_file, 20, 10, 15, '', 'JPG', '', 'T', false, -550, '', false, false, 0, false, false, false);
		$pdf->Image($image_file,2,1,35);
// $pdf->Image('profile/'.$Profile['logo'],-5,5,-550);
// $pdf->Image('profile/'.$Profile['logo'],2,2,-525);
		$image_line_width=40;
		$invoice_x_starting=$image_line_width;
$pdf->Line($image_line_width, 1,$image_line_width, 40); // vertical line
// //$pdf->SetFont('Arial','B',12);
// $pdf->SetTextColor(0,0,100);
$pdf->rect(1, 1, 208, 295);

// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$invoice_pos+=3;
// $pdf->SetFont('Arial','B',15);
$pdf->SetFont('times', 'B', 8, "", 'false');
$pdf->SetFont('dejavusans', '', 10);
if(empty($Purchase['Purchase']['date_of_delivered']))
{
	$pdf->text(90,$invoice_pos,"Purchase Order");
}
else
{
	$pdf->text(97,$invoice_pos,"Purchase Invoice");
}


$pdf->SetFont('dejavusans', '', 8);

//$pdf->text(115,$invoice_pos,$arabic);
$pdf->SetFont('dejavusans', 'B', 12);


// $pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=2;
$pdf->text(92,$invoice_pos+3,$Profile['company_name']);
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
$pdf->text(100,$invoice_pos+3,$Profile['address_line_1']);
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->text(166,33,$field_value);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(78,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);

$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->SetFont('times', 'U', 12, "", 'false');
$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
$invoice_pos+=10;
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(83,$invoice_pos,'GST Number : '.$Profile['vat_code']);

$gst_details_y_staring=40;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring); // horizontal line
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos=$gst_details_y_staring+2;
$gst_line_width=120;
//$pdf->Line($gst_line_width+50, $gst_details_y_staring,$gst_line_width+50, $gst_details_y_staring+17+21+5); // vertical line
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);
$pdf->text(2,$gst_pos,'Party Name:');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(2,$gst_pos+7,'Party Mobile');
$pdf->text(2,$gst_pos+14,'GST Number');
$pdf->text(2,$gst_pos+21,'Address');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($gst_line_width-285,$gst_pos,$name);
$pdf->text($gst_line_width-285,$gst_pos+7,$Purchase['Party']['telephone']);
$pdf->text($gst_line_width-285,$gst_pos+14,$Purchase['Party']['vat_no']);
$pdf->text($gst_line_width-285,$gst_pos+21,$Purchase['Party']['place']);
if(empty($Purchase['Purchase']['date_of_delivered']))
{
	$pdf->text($gst_line_width-285,$gst_pos+28,$Purchase['Purchase']['order_no']);
	//$pdf->text($gst_line_width-178,$gst_pos+28,$Purchase['Purchase']['order_no']);
}
else
{
	$pdf->text($gst_line_width-285,$gst_pos+28,$Purchase['Purchase']['invoice_no']);
	//$pdf->text($gst_line_width-178,$gst_pos+28,$Purchase['Purchase']['invoice_no']);
}
$pdf->SetFont('dejavusans', '', 10);
if(!$Purchase['Purchase']['date_of_delivered'])
	$invoice_date=$Purchase['Purchase']['date_of_purchase'];
else
	$invoice_date=$Purchase['Purchase']['date_of_delivered'];

$pdf->text($gst_line_width-285,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));
//$pdf->text($gst_line_width-185,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));

// $pdf->text($gst_line_width+1+68,$gst_pos,'اسم الحزب');
// $pdf->text($gst_line_width+1+69,$gst_pos+7,'رقم الطرف');
// $pdf->text($gst_line_width+1+68,$gst_pos+14,'رقم الضريبة');
// $pdf->text($gst_line_width+1+76,$gst_pos+21,'عنوان.');
// if(empty($Purchase['Purchase']['date_of_delivered']))
// {
// 	$pdf->text($gst_line_width+1+68,$gst_pos+28,'رقم الطلب');
// }
// else
// {
// 	$pdf->text($gst_line_width+1+68,$gst_pos+28,'رقم الفاتورة');
// }

//$pdf->text($gst_line_width+1+76,$gst_pos+35,'التاريخ');

//$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos+=28;
if(empty($Purchase['Purchase']['date_of_delivered']))
{
	$pdf->text(2,$gst_pos,'Order Number');
}
else
{
	$pdf->text(2,$gst_pos,'Invoice Number');
}
$gst_pos+=7;
$pdf->text(2,$gst_pos,'Date');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->SetXY(0, 5);
$pdf->Line(40, $gst_details_y_staring,40, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos+=3;

$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+20;
$Consignee_lin_width=100+1;
//$pdf->text(2,$customer_detail_startin,'Customer Name : ');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(75,$customer_detail_startin,'اﺳﻢ اﻟﺰﺑﻮﻥ : ');
$pdf->SetFont('times', '', 10, "", 'false');
//$pdf->text($Consignee_lin_width,$customer_detail_startin,'Address :'.$Sale['Customer']['place']);

$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(195,$customer_detail_startin,'ﻋﻨﻮاﻥ :');
$pdf->SetFont('times', '', 10, "", 'false');



$customer_detail_startin+=7;
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(2,$customer_detail_startin-1,$name);
//$pdf->text(60,$customer_detail_startin+3,$Sale['Customer']['arabic_name']);
$pdf->SetFont('times', '', 10, "", 'false');
$customer_detail_startin+=4;
$customer_detail_startin+=4;
$customer_detail_startin+=1;
//$pdf->text(2,$customer_detail_startin,'VAT Number :'.$Sale['Customer']['vat_no']);
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(82,$customer_detail_startin,'ﻭاﺕ ﺭﻗﻢ:');
$pdf->SetFont('times', '', 10, "", 'false');





$pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+7;
//  table head start
$table_length=135;
$pdf->text(2,$item_table_head_y_start+1,'SL');
$pdf->text(2,$item_table_head_y_start+4,'No');
// $pdf->SetFont('dejavusans', '', 10);
// $pdf->Text(2,$item_table_head_y_start+7, 'رقم');
// $pdf->text(1,$item_table_head_y_start+10,'مسلسل');
$table_column=9;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line($table_column+21, $item_table_head_y_start,$table_column+21, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+1,'Item Code');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text($table_column+5,$item_table_head_y_start+7, 'رقم الصنف ');
$pdf->Line($table_column+5, $item_table_head_y_start,$table_column+5, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+35,$item_table_head_y_start+1,'Description');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text($table_column+35,$item_table_head_y_start+6,'البيان');
$table_column+=70+$igst_x_position+25;
$pdf->SetFont('times', '', 10, "", 'false');

$table_column+=1;
$pdf->Line($table_column-5, $item_table_head_y_start,$table_column-5, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column-3,$item_table_head_y_start+1,'Qty');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text($table_column,$item_table_head_y_start+6,'كمية');
$pdf->SetFont('times', '', 10, "", 'false');
$table_column+=5;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->Line($table_column+22, $item_table_head_y_start,$table_column+22, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+3,$item_table_head_y_start+1,'Unit');
$pdf->text($table_column+22,$item_table_head_y_start+1,'Rate');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text($table_column+1,$item_table_head_y_start+6,'وحدة');
//$pdf->Text($table_column+11+8,$item_table_head_y_start+6,'سعر');
$table_column+=25;
$pdf->SetFont('times', '', 10, "", 'false');

//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$table_column+=10;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
// $pdf->SetFont('dejavusans', '', 10);
$pdf->text($table_column,$item_table_head_y_start+1,'Tax%.');
// $pdf->text($table_column-10,$item_table_head_y_start+6,'خصم');
$table_column+=15;
$pdf->Line($table_column-5, $item_table_head_y_start,$table_column-5, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column-3,$item_table_head_y_start+1,'Tx Amt');
//$pdf->text($table_column,$item_table_head_y_start+4,'Excl VAT');

$pdf->SetFont('dejavusans', '', 8);
//قيمة الضريبة

// $pdf->Text($table_column-15,$item_table_head_y_start+8,'اﻟﻤﺒﻠﻎ قيمة');
//$pdf->Text($table_column,$item_table_head_y_start+11,'اﺯاﻟﺔ ﻭاﺕ');
$pdf->SetFont('times', '', 10, "", 'false'); 
$table_column+=25;
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
// $pdf->Line($table_column-15,$item_table_head_y_start+6,$table_column-15, $item_table_head_y_start+$table_length); // vertical line

//$pdf->Line($table_column-24, $gst_details_y_staring+17+5+21+6,175+$igst_x_position, $gst_details_y_staring+17+5+21+6); // horizontal line
// $pdf->text($table_column-24,$item_table_head_y_start+1,'VAT');
// $pdf->SetFont('dejavusans', '', 8);
// $pdf->Text($table_column-14,$item_table_head_y_start+1,'ﺿﺮﻳﺒﺔ');
// $pdf->SetFont('times', '', 10, "", 'false'); 

// $pdf->text($table_column-24,$item_table_head_y_start+7,'Rate');
// $pdf->SetFont('dejavusans', '', 8);

// $pdf->Text($table_column-24,$item_table_head_y_start+10,'ﻣﻌﺪﻝ');
$table_column+=14;
$pdf->SetFont('times', '', 10, "", 'false'); 
// $pdf->Line($table_column, $item_table_head_y_start+6,$table_column, $item_table_head_y_start+$table_length); // vertical line
// $pdf->text($table_column-30,$item_table_head_y_start+7,'Amount');
$pdf->SetFont('dejavusans', '', 10);
// $pdf->Text($table_column-29,$item_table_head_y_start+10,'ﻛﻤﻴﺔ');
$pdf->SetFont('times', '', 10, "", 'false');
$table_column+=0;
 $pdf->Line($table_column-28, $item_table_head_y_start,$table_column-28, $item_table_head_y_start+$table_length); // vertical line

$pdf->text($table_column-22,$item_table_head_y_start+1,'Total');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text($table_column+2-34,$item_table_head_y_start+4+2,'ﻣﺠﻤﻮﻉ');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line(1, $gst_details_y_staring+17+5+21+10,209, $gst_details_y_staring+17+5+21+10); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length,209, $item_table_head_y_start+$table_length); // horizontal line
// $pdf->Line(1, $item_table_head_y_start+$table_length+5,209, $item_table_head_y_start+$table_length+5); // horizontal line
//$pdf->text(12,$item_table_head_y_start+$table_length,'Total');
$pdf->SetFont('dejavusans', '', 10);

$pdf->SetFont('times', '', 10, "", 'false');

}

function footer($pdf,$data,$Purchase)
{
$state_id=$Purchase['Party']['state_id'];
	$footer_start_y=218;
$pdf->Line(1, $footer_start_y,209, $footer_start_y); // horizontal line
$invoce_word_width=113;
$grand_total_length=30;
$pdf->Line($invoce_word_width, $footer_start_y,$invoce_word_width, $footer_start_y+$grand_total_length+4); // vertical line
//$pdf->text(10,$footer_start_y+1,'Amount In Words');
$pdf->SetFont('dejavusans', '', 8);
// $pdf->Text(85,$footer_start_y+1,'اﻟﻤﺒﻠﻎ ﺑﺎﻟﻜﻠﻤﺎﺕ');
$pdf->SetFont('times', '', 8, "", 'false');
$actual_total=$Purchase['Purchase']['total']-$data['total_cgst_tax']-$data['total_sgst_tax']-$data['total_igst_tax'];
$discount_grand_amount=$data['discount_grand_amount'];
$actual_grand_total=$Purchase['Purchase']['grand_total'];
$convert_number_to_words=strtoupper(convert_number_to_words(floatval($actual_grand_total)));
//$convert_number_to_arabic_words=strtoupper(convert_number_to_arabic_words(floatval($actual_grand_total)));
//$pdf->text(5,$footer_start_y+3+10,$convert_number_to_words);
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(40,$footer_start_y+3+10,$convert_number_to_arabic_words);
//$pdf->Text($invoice_lin_width+130,$customer_detail_startin+4+35,strtoupper(convert_number_to_arabic_words($amount)).' '.'الريال فقط' );
$pdf->Line($invoce_word_width+53, $footer_start_y,$invoce_word_width+53, $footer_start_y+$grand_total_length+10); // vertical line
$Total_pos=1;
$pdf->text($invoce_word_width,$footer_start_y+$Total_pos,'Total Amount');
$pdf->SetFont('dejavusans', '', 8);
//$pdf->Text($invoce_word_width+22,$footer_start_y+$Total_pos,'اجمالى القيمة');
$pdf->SetFont('times', '', 8, "", 'false');

$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos,round($data['total'],2));

$Total_pos+=5;
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+20,'Discount');


$pdf->SetFont('dejavusans', '', 8);
//$pdf->Text($invoce_word_width+28,$footer_start_y+$Total_pos+1+0.8,'الخصم');
$pdf->SetFont('times', '', 8, "", 'false');
$discount_grand_amount=$Purchase['Purchase']['discount_amount'];
$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos+20,$discount_grand_amount);
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
//$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos-3,'Tax %');

$pdf->SetFont('dejavusans', '', 8);
//$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos-3,$Purchase['Purchase']['total_tax']);
//$pdf->Text($invoce_word_width+28,$footer_start_y+$Total_pos+2-1,'نسبة الضريبة');
//$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos-7,'Tax Amount');
if($state_id==19)
{
$tax_amount_split=floatval($Purchase['Purchase']['total_tax_amount'])/2;
$pdf->text($invoce_word_width+65,$footer_start_y+$Total_pos-7,'SGST: '.round($tax_amount_split,3));
$pdf->text($invoce_word_width+65,$footer_start_y+$Total_pos-1,'CGST: '.round($tax_amount_split,3));
$pdf->text($invoce_word_width+65,$footer_start_y+$Total_pos+5,'IGST: 0');
}
else
{
$tax_amount_split=floatval($Purchase['Purchase']['total_tax_amount']);
$pdf->text($invoce_word_width+65,$footer_start_y+$Total_pos-7,'SGST: 0');
$pdf->text($invoce_word_width+65,$footer_start_y+$Total_pos-1,'CGST: 0');
$pdf->text($invoce_word_width+65,$footer_start_y+$Total_pos+5,'IGST: '.round($tax_amount_split,3));
}
$pdf->SetFont('dejavusans', '', 8);

//$pdf->Text($invoce_word_width+28,$footer_start_y+$Total_pos+2-1,'قيمة الضريبة');
$pdf->SetFont('times', '', 8, "", 'false');
//   //$other_value=round($data['total'])-$data['total'];
$other_value=$Purchase['Purchase']['other_value'];
//$total_discount=$Sale['Sale']['discount_amount'];

$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos-5,round($Purchase['Purchase']['total_tax_amount'],2));


$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+10,209, $footer_start_y+$Total_pos+10); // horizontal line
$Total_pos+=5;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+10,'Net Amount');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text($invoce_word_width+23,$footer_start_y+$Total_pos+5,'المبلغ الصافى');
$pdf->SetFont('times', '', 12, "", 'false');
$actual_grand_total=$Purchase['Purchase']['grand_total'];
$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos+10,($actual_grand_total));
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+10,209, $footer_start_y+$Total_pos+10); // horizontal line
$tax_subject_y=$footer_start_y+$grand_total_length;
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->Line(1, $tax_subject_y+10,209, $tax_subject_y+10); // horizontal line
$pdf->text(2,$tax_subject_y+11,'Received By');
$pdf->SetFont('dejavusans', '', 8);
//$pdf->Text(85,$tax_subject_y+5,'المستلم');
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->text(115,$tax_subject_y+11,'Salesman');
$pdf->SetFont('dejavusans', '', 8);
//$pdf->Text(190,$tax_subject_y+5,'البائغ');البائغ:
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->Line($invoce_word_width+64, $tax_subject_y-23,$invoce_word_width+64, $tax_subject_y-4); // vertical line
$pdf->Line($invoce_word_width+64, $footer_start_y+$Total_pos-7,209, $footer_start_y+$Total_pos-7); // horizontal line
$pdf->Line($invoce_word_width+64, $footer_start_y+$Total_pos-1,209, $footer_start_y+$Total_pos-1); // horizontal line

$signature_area_y=$tax_subject_y+4;
$signature_length=14;
$signature_width=113;

$pdf->SetFont('dejavusans', '', 8);


$pdf->SetFont('times', '', 8, "", 'false');
$pdf->SetXY($signature_width, $signature_area_y);

$pdf->Line($signature_width, $signature_area_y,$signature_width, $signature_area_y+$signature_length); // vertical line
$pdf->Line(1, $signature_area_y+$signature_length,209, $signature_area_y+$signature_length); // horizontal line

}
$page=0;


foreach($print_field_array as $field_key=>$field_value)
{
//$page=1;
	$page++;
	header_section($pdf,$Purchase,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value);
	$total=0;
	$total_cgst_tax=0;
	$total_sgst_tax=0;
	$discount=0;
	$total_igst_tax=0;
	$item_table_head_y_start='85';
	$table_length='128';
	$cgst_grand_amount=0;
	$sgst_grand_amount=0;
	$igst_grand_amount=0;
	$rate_grand_amount=0;
	$discount_grand_amount=0;
	$taxable_value_grand_amount=0;
	$product_grand_total=0;
	$count=count($PurchasedItem);
	$i=0;
// for ($i=0; $i <$count ; $i++) { 
	$data=array(
		'total'=>$total,
		'total_cgst_tax'=>$total_cgst_tax,
		'total_sgst_tax'=>$total_sgst_tax,
		'total_igst_tax'=>$total_igst_tax,
		'discount_grand_amount'=>$discount_grand_amount,
		);


	foreach ($PurchasedItem as $keySI => $value) {
		$description = str_replace('"', "'", $value['PurchasedItem']['product_name']);
// $hsn_code=$value['Product']['hsn_code'];
		$qty=floatval($value['PurchasedItem']['quantity']);
		$rate=floatval($value['PurchasedItem']['unit_price']);
		$unit_level=$this->Unit->findByName($value['PurchasedItem']['quantity_mode']);
		if(empty($unit_level)){
			$unit_level['Unit']['level'] =1;
		}
//pr($value['PurchasedItem']['quantity_mode']);exit;
		if($value['PurchasedItem']['quantity_mode']==2){
			$qty=$value['PurchasedItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
			$rate=floatval($value['PurchasedItem']['unit_price']*$value['Product']['no_of_piece_per_unit']);
//$qty = $value['PurchasedItem']['quantity'];
		}

		$uom=$value['PurchasedItem']['quantity_mode'];

		$unit_mode=$value['PurchasedItem']['quantity_mode'];
		$units=$this->Unit->findById($unit_mode);
		$unit=$units['Unit']['name'];
		// if($unit_mode==1)
		// {
		// 	$unit="Pieces";
		// }
		// else
		// {
		// 	$unit="Cases";
		// }
		$rate_grand_amount+=$rate;
		$discount=0;
		$taxable_value=floatval($value['PurchasedItem']['net_value']);
		$discount_item=floatval($value['PurchasedItem']['discount']);
		$taxable_value_grand_amount+=$taxable_value;
		$data['discount_grand_amount']+=$discount_item;
		$net_value=floatval($value['PurchasedItem']['net_value']);

		$tax_value=floatval($value['PurchasedItem']['tax_amount']);
		$total+=$net_value;


		$data['total']+=floatval($net_value);
		$data['total_cgst_tax']+=floatval($value['PurchasedItem']['tax_amount']);
		$data['total_sgst_tax']+=floatval($value['PurchasedItem']['tax_amount']);
		$data['total_igst_tax']+=floatval($value['PurchasedItem']['tax_amount']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$product_total=floatval($value['PurchasedItem']['total']);
		$product_grand_total+=$product_total;
		$cgst_rate=floatval($value['PurchasedItem']['tax']);
		$cgst_amount=floatval($value['PurchasedItem']['tax_amount']);
		$cgst_grand_amount+=$cgst_amount;
		$sgst_rate=floatval($value['PurchasedItem']['tax']);
		$sgst_amount=floatval($value['PurchasedItem']['tax_amount']);
		$sgst_grand_amount+=$sgst_amount;
		$igst_rate=floatval($value['PurchasedItem']['tax']);
		$igst_amount=floatval($value['PurchasedItem']['tax_amount']);
		$igst_grand_amount+=$igst_amount;
		$item_pos=2;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$keySI+1);
		$item_pos+=6;
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$product_name=implode($description_words,' ');
			$poped_array[]=array_pop($description_words);
			$product_name_length=strlen($product_name);
			if($product_name_length<=50)
			{ 
				$pdf->SetFont('dejavusans', '', 8);

				$pdf->text($item_pos+8,$item_table_head_y_start+8+(($i+$k)*8)+5,$value['Product']['code']);
				$pdf->text($item_pos+23,$item_table_head_y_start+8+(($i+$k)*8)+5,$value['Product']['name']);
				//$pdf->text($item_pos+26,$item_table_head_y_start+8+(($i+$k)*8)+8,$value['Product']['arabic_name']);
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}

		if($k)
			$k--;
		$item_pos+=1;
		$pdf->SetFont('dejavusans', '', 8);
// $pdf->AddFont('DejaVu','','tahoma.ttf',true);
// $pdf->AddFont('DejaVu', 'B', 'tahoma-Bold.ttf', true);
// $pdf->SetFont('DejaVu','',8);
// $pdf->Text($item_pos,$item_table_head_y_start+11+($i*8)+5,$value['Product']['arabic_name']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$item_pos+=65+$igst_x_position+25;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$hsn_code);
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,'');
		$item_pos+=1;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$qty);
		$pdf->text($item_pos+11,$item_table_head_y_start+8+($i*8)+5,$unit);
		$item_pos+=32;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,round($rate,3));
		$item_pos+=15;
// //$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),number_format($rate,2));
// if(count($PurchasedItem)==$keySI+1)
// {
// //$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$rate_grand_amount);
// }
// $item_pos+=5;
// // $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$discount);
// if(count($PurchasedItem)==$keySI+1)
// {
// //$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$discount_grand_amount);
// }
// // $item_pos+=1;
// $pdf->text($item_pos+8,$item_table_head_y_start+8+($i*8)+5,round(($taxable_value-$discount_item),2));
// if(count($PurchasedItem)==$keySI+1)
//   $pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,round($discount_item,2));
// if(count($PurchasedItem)==$keySI+1)
// {
// }
// $item_pos+=21;
// if($Checkstate==1)
// {
// $pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_rate.'%');
// $item_pos+=7;
// $pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_amount);
// if(count($PurchasedItem)==$keySI+1)
// {
// // $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$cgst_grand_amount);
// }
// $item_pos+=15;
// $pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_rate.'%');
// $item_pos+=8;
// $pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_amount);
// if(count($PurchasedItem)==$keySI+1)
// {
// }
// }
// else
// {
// $pdf->text($item_pos+4,$item_table_head_y_start+8+($i*8)+5,$igst_rate.'%');
// $item_pos+=13;
// // $pdf->text($item_pos-2,$item_table_head_y_start+8+($i*8)+5,number_format($igst_amount,2));
// if(count($PurchasedItem)==$keySI+1)
// {
// }
// }

		$item_pos+=1;
$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$igst_rate);
		$item_pos+=10;
$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$igst_amount);
		$pdf->text($item_pos+16,$item_table_head_y_start+8+($i*8)+5,round($product_total,2));
		if(count($PurchasedItem)==$keySI+1)
		{
		}
		$i+=$k;
		$count+=$k;
		if($i+$k>=13)
		{
			footer($pdf,$data,$Purchase);
			$page++;
			header_section($pdf,$Purchase,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value);
			$i=-1;
			$count-=13;
		}
		$i++;
	}

	footer($pdf,$data,$Purchase);
}
$pdf->Output();
exit;

}
public function purchase_order($id=null)
{

	$Purchase = $this->Purchase->find('first', array(
		"joins" => array(
			array(
				"table" => 'parties',
				"alias" => 'Party',
				"type" => 'inner',
				"conditions" => array('Party.account_head_id=Purchase.account_head_id'),
				),
			),
		'conditions' => array('Purchase.id'=>$id),
		'fields' => array(
			'AccountHead.*',
			'Party.*',
			'Purchase.*',
			)
		));
	$name=$Purchase['AccountHead']['name'];
	$invoice_no=$Purchase['Purchase']['invoice_no'];
	$date=$Purchase['Purchase']['date_of_delivered'];
	$print_field_array=[];
	$print_field_array[0]="Original For Party";
//$print_field_array[1]="Duplicate For Accounts";
//$print_field_array[2]="Triplicate For Accounts";
	$this->PurchasedItem->virtualFields = array(
		'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
		);
	$PurchasedItem = $this->PurchasedItem->find('all', array(
		'conditions' => array('PurchasedItem.purchase_id'=>$id),
		'fields' => array(
			'PurchasedItem.*',
			'Product.id',
			'Product.name',
			'Product.code',
			'Product.arabic_name',
			'Product.no_of_piece_per_unit',
			)
		));
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}
	foreach ($PurchasedItem as $key => $value) {
		$description = str_replace('"', "'", $value['Product']['name']);
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$poped_array[]=array_pop($description_words);
			$product_name=implode($description_words,' ');
			$product_name_length=strlen($product_name);
			if($product_name_length<=42)
			{
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}
		if($k)
			$k--;
		$i+=$k;
		if($i+$k>=20)
		{
			$total_page++;
			$i=-1;
		}
		$i++;
	}
	$igst_x_position=0;
	$Checkstate=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 


	function header_section($pdf,$Purchase,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value) 
	{
		$pdf->AddPage();
		$image_file ='profile/'.$Profile['logo'];
//$pdf->Image($image_file, 20, 10, 15, '', 'JPG', '', 'T', false, -550, '', false, false, 0, false, false, false);
		$pdf->Image($image_file,2,1,35);
// $pdf->Image('profile/'.$Profile['logo'],-5,5,-550);
// $pdf->Image('profile/'.$Profile['logo'],2,2,-525);
		$image_line_width=40;
		$invoice_x_starting=$image_line_width;
$pdf->Line($image_line_width, 1,$image_line_width, 40); // vertical line
// //$pdf->SetFont('Arial','B',12);
// $pdf->SetTextColor(0,0,100);
$pdf->rect(1, 1, 208, 295);

// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$invoice_pos+=3;
// $pdf->SetFont('Arial','B',15);
$pdf->SetFont('times', 'B', 8, "", 'false');
$pdf->SetFont('dejavusans', '', 10);


$pdf->text(90,$invoice_pos,"Purchase Order");
$pdf->SetFont('dejavusans', '', 8);

//$pdf->text(115,$invoice_pos,$arabic);
$pdf->SetFont('dejavusans', '', 12);


// $pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=2;
$pdf->text(78,$invoice_pos+3,$Profile['company_name'].'جمعة هدريس مؤسسة');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
$pdf->text(70,$invoice_pos+3,$Profile['address_line_1'].'فيصل الملك شارع  -القوافل مركز' );
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->text(166,33,$field_value);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(65,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile'].' '.'ت س   الخمرة  - جدة. ');

$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->SetFont('times', 'U', 12, "", 'false');
$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
$invoice_pos+=10;
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(78,$invoice_pos,'VAT Number 30096554300003  الضريبة رقم');

$gst_details_y_staring=40;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring); // horizontal line
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos=$gst_details_y_staring+2;
$gst_line_width=120;
$pdf->Line($gst_line_width+50, $gst_details_y_staring,$gst_line_width+50, $gst_details_y_staring+17+21+5); // vertical line
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);
$pdf->text(2,$gst_pos,'Party Name:');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(2,$gst_pos+7,'Party No');
$pdf->text(2,$gst_pos+14,'VAT Number');
$pdf->text(2,$gst_pos+21,'Address');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($gst_line_width-250,$gst_pos,$name);
$pdf->text($gst_line_width-250,$gst_pos+7,$Purchase['Party']['mobile']);
$pdf->text($gst_line_width-250,$gst_pos+14,$Purchase['Party']['vat_no']);
$pdf->text($gst_line_width-250,$gst_pos+21,$Purchase['Party']['place']);
$pdf->text($gst_line_width-285,$gst_pos+28,$Purchase['Purchase']['order_no']);
$pdf->text($gst_line_width-178,$gst_pos+28,$Purchase['Purchase']['order_no']);
$pdf->SetFont('dejavusans', '', 10);
if(!$Purchase['Purchase']['date_of_delivered'])
	$invoice_date=$Purchase['Purchase']['date_of_purchase'];
else
	$invoice_date=$Purchase['Purchase']['date_of_delivered'];

$pdf->text($gst_line_width-290,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));
$pdf->text($gst_line_width-185,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));

$pdf->text($gst_line_width+1+68,$gst_pos,'اسم الحزب');
$pdf->text($gst_line_width+1+69,$gst_pos+7,'رقم الطرف');
$pdf->text($gst_line_width+1+68,$gst_pos+14,'رقم الضريبة');
$pdf->text($gst_line_width+1+76,$gst_pos+21,'عنوان.');
$pdf->text($gst_line_width+1+68,$gst_pos+28,'رقم الطلب');
$pdf->text($gst_line_width+1+76,$gst_pos+35,'التاريخ');

//$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos+=28;
$pdf->text(2,$gst_pos,'Order Number');
$gst_pos+=7;
$pdf->text(2,$gst_pos,'Date');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->SetXY(0, 5);
$pdf->Line(40, $gst_details_y_staring,40, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos+=3;

$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+20;
$Consignee_lin_width=100+1;
//$pdf->text(2,$customer_detail_startin,'Customer Name : ');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(75,$customer_detail_startin,'اﺳﻢ اﻟﺰﺑﻮﻥ : ');
$pdf->SetFont('times', '', 10, "", 'false');
//$pdf->text($Consignee_lin_width,$customer_detail_startin,'Address :'.$Sale['Customer']['place']);

$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(195,$customer_detail_startin,'ﻋﻨﻮاﻥ :');
$pdf->SetFont('times', '', 10, "", 'false');



$customer_detail_startin+=7;
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(2,$customer_detail_startin-1,$name);
//$pdf->text(60,$customer_detail_startin+3,$Sale['Customer']['arabic_name']);
$pdf->SetFont('times', '', 10, "", 'false');
$customer_detail_startin+=4;
$customer_detail_startin+=4;
$customer_detail_startin+=1;
//$pdf->text(2,$customer_detail_startin,'VAT Number :'.$Sale['Customer']['vat_no']);
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(82,$customer_detail_startin,'ﻭاﺕ ﺭﻗﻢ:');
$pdf->SetFont('times', '', 10, "", 'false');





$pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+7;
//  table head start
$table_length=135;
$pdf->text(2,$item_table_head_y_start+1,'SL');
$pdf->text(2,$item_table_head_y_start+4,'No');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text(2,$item_table_head_y_start+7, 'رقم');
$pdf->text(1,$item_table_head_y_start+10,'مسلسل');
$table_column=9;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line($table_column+25, $item_table_head_y_start,$table_column+25, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+1,'Item Code');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+5,$item_table_head_y_start+7, 'رقم الصنف ');
$pdf->Line($table_column+5, $item_table_head_y_start,$table_column+5, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+35,$item_table_head_y_start+1,'Description');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+35,$item_table_head_y_start+6,'البيان');
$table_column+=70+$igst_x_position;
$pdf->SetFont('times', '', 10, "", 'false');

$table_column+=16;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column,$item_table_head_y_start+1,'Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column,$item_table_head_y_start+6,'كمية');
$pdf->SetFont('times', '', 10, "", 'false');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->Line($table_column+11, $item_table_head_y_start,$table_column+11, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+1,$item_table_head_y_start+1,'Unit');
$pdf->text($table_column+11,$item_table_head_y_start+1,'Rate');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+1,$item_table_head_y_start+6,'وحدة');
$pdf->Text($table_column+11,$item_table_head_y_start+6,'سعر');
$table_column+=25;
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$table_column+=10;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->SetFont('dejavusans', '', 10);
$pdf->text($table_column-10,$item_table_head_y_start+1,'Disc.');
$pdf->text($table_column-10,$item_table_head_y_start+6,'خصم');
$table_column+=15;
$pdf->Line($table_column+1, $item_table_head_y_start,$table_column+1, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column-15,$item_table_head_y_start+1,'Tx Value');
//$pdf->text($table_column,$item_table_head_y_start+4,'Excl VAT');

$pdf->SetFont('dejavusans', '', 8);
//قيمة الضريبة

$pdf->Text($table_column-15,$item_table_head_y_start+8,'اﻟﻤﺒﻠﻎ قيمة');
//$pdf->Text($table_column,$item_table_head_y_start+11,'اﺯاﻟﺔ ﻭاﺕ');
$pdf->SetFont('times', '', 10, "", 'false'); 
$table_column+=25;
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->Line($table_column-15,$item_table_head_y_start+6,$table_column-15, $item_table_head_y_start+$table_length); // vertical line

$pdf->Line($table_column-24, $gst_details_y_staring+17+5+21+6,175+$igst_x_position, $gst_details_y_staring+17+5+21+6); // horizontal line
$pdf->text($table_column-24,$item_table_head_y_start+1,'VAT');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($table_column-14,$item_table_head_y_start+1,'ﺿﺮﻳﺒﺔ');
$pdf->SetFont('times', '', 10, "", 'false'); 

$pdf->text($table_column-24,$item_table_head_y_start+7,'Rate');
$pdf->SetFont('dejavusans', '', 8);

$pdf->Text($table_column-24,$item_table_head_y_start+10,'ﻣﻌﺪﻝ');
$table_column+=14;
$pdf->SetFont('times', '', 10, "", 'false'); 
$pdf->Line($table_column, $item_table_head_y_start+6,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column-30,$item_table_head_y_start+7,'Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column-29,$item_table_head_y_start+10,'ﻛﻤﻴﺔ');
$pdf->SetFont('times', '', 10, "", 'false');
$table_column+=15;
$pdf->Line($table_column-32, $item_table_head_y_start,$table_column-32, $item_table_head_y_start+$table_length); // vertical line

$pdf->text($table_column-30,$item_table_head_y_start+1,'Total');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+2,$item_table_head_y_start+4,'ﻣﺠﻤﻮﻉ');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line(1, $gst_details_y_staring+17+5+21+15,209, $gst_details_y_staring+17+5+21+15); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length,209, $item_table_head_y_start+$table_length); // horizontal line
// $pdf->Line(1, $item_table_head_y_start+$table_length+5,209, $item_table_head_y_start+$table_length+5); // horizontal line
//$pdf->text(12,$item_table_head_y_start+$table_length,'Total');
$pdf->SetFont('dejavusans', '', 10);

$pdf->SetFont('times', '', 10, "", 'false');

}

function footer($pdf,$data,$Purchase)
{
	$footer_start_y=218;
$pdf->Line(1, $footer_start_y,209, $footer_start_y); // horizontal line
$invoce_word_width=113;
$grand_total_length=30;
$pdf->Line($invoce_word_width, $footer_start_y,$invoce_word_width, $footer_start_y+$grand_total_length+4); // vertical line
//$pdf->text(10,$footer_start_y+1,'Amount In Words');
$pdf->SetFont('dejavusans', '', 8);
// $pdf->Text(85,$footer_start_y+1,'اﻟﻤﺒﻠﻎ ﺑﺎﻟﻜﻠﻤﺎﺕ');
$pdf->SetFont('times', '', 8, "", 'false');
$actual_total=$Purchase['Purchase']['total']-$data['total_cgst_tax']-$data['total_sgst_tax']-$data['total_igst_tax'];
$discount_grand_amount=$data['discount_grand_amount'];
$actual_grand_total=$Purchase['Purchase']['grand_total'];
$convert_number_to_words=strtoupper(convert_number_to_words(floatval($actual_grand_total)));
$convert_number_to_arabic_words=strtoupper(convert_number_to_arabic_words(floatval($actual_grand_total)));
//$pdf->text(5,$footer_start_y+3+10,$convert_number_to_words);
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(40,$footer_start_y+3+10,$convert_number_to_arabic_words);
//$pdf->Text($invoice_lin_width+130,$customer_detail_startin+4+35,strtoupper(convert_number_to_arabic_words($amount)).' '.'الريال فقط' );
$pdf->Line($invoce_word_width+45, $footer_start_y,$invoce_word_width+45, $footer_start_y+$grand_total_length); // vertical line
$Total_pos=1;
$pdf->text($invoce_word_width,$footer_start_y+$Total_pos,'Total Amount');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+22,$footer_start_y+$Total_pos,'اجمالى القيمة');
$pdf->SetFont('times', '', 8, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,number_format($data['total'],2));

$Total_pos+=5;
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+2,'VAT Amount');

$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+28,$footer_start_y+$Total_pos+2,'قيمة الضريبة');
$pdf->SetFont('times', '', 8, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos+2,number_format($data['total_igst_tax'],2));
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+1,'Discount');

$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+28,$footer_start_y+$Total_pos+1,'الخصم');
$pdf->SetFont('times', '', 8, "", 'false');
//   //$other_value=round($data['total'])-$data['total'];
$other_value=$Purchase['Purchase']['other_value'];
//$total_discount=$Sale['Sale']['discount_amount'];
//$discount_grand_amount=0;
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos+1,$discount_grand_amount);

$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+5,'Net Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($invoce_word_width+23,$footer_start_y+$Total_pos+5,'المبلغ الصافى');
$pdf->SetFont('times', '', 12, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos+5,number_format($actual_grand_total,2));
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
$tax_subject_y=$footer_start_y+$grand_total_length;
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->Line(1, $tax_subject_y+4,209, $tax_subject_y+4); // horizontal line
$pdf->text(2,$tax_subject_y+5,'Received By');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(85,$tax_subject_y+5,'المستلم');
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->text(115,$tax_subject_y+5,'Salesman');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(190,$tax_subject_y+5,'البائغ');البائغ:
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->Line($invoce_word_width+45, $tax_subject_y,$invoce_word_width+45, $tax_subject_y+4); // vertical line
$signature_area_y=$tax_subject_y+4;
$signature_length=14;
$signature_width=113;

$pdf->SetFont('dejavusans', '', 8);


$pdf->SetFont('times', '', 8, "", 'false');
$pdf->SetXY($signature_width, $signature_area_y);

$pdf->Line($signature_width, $signature_area_y,$signature_width, $signature_area_y+$signature_length); // vertical line
$pdf->Line(1, $signature_area_y+$signature_length,209, $signature_area_y+$signature_length); // horizontal line

}
$page=0;


foreach($print_field_array as $field_key=>$field_value)
{
//$page=1;
	$page++;
	header_section($pdf,$Purchase,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value);
	$total=0;
	$total_cgst_tax=0;
	$total_sgst_tax=0;
	$discount=0;
	$total_igst_tax=0;
	$item_table_head_y_start='85';
	$table_length='128';
	$cgst_grand_amount=0;
	$sgst_grand_amount=0;
	$igst_grand_amount=0;
	$rate_grand_amount=0;
	$discount_grand_amount=0;
	$taxable_value_grand_amount=0;
	$product_grand_total=0;
	$count=count($PurchasedItem);
	$i=0;
// for ($i=0; $i <$count ; $i++) { 
	$data=array(
		'total'=>$total,
		'total_cgst_tax'=>$total_cgst_tax,
		'total_sgst_tax'=>$total_sgst_tax,
		'total_igst_tax'=>$total_igst_tax,
		'discount_grand_amount'=>$discount_grand_amount,
		);


	foreach ($PurchasedItem as $keySI => $value) {
		$description = str_replace('"', "'", $value['PurchasedItem']['product_name']);
// $hsn_code=$value['Product']['hsn_code'];
		$qty=floatval($value['PurchasedItem']['quantity']);
		$unit_level=$this->Unit->findByName($value['PurchasedItem']['quantity_mode']);
		if(empty($unit_level)){
			$unit_level['Unit']['level'] =1;
		}
		if($value['PurchasedItem']['quantity_mode']==2){
			$qty=$value['PurchasedItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
//$qty = $value['PurchasedItem']['quantity'];
		}

		$uom=$value['PurchasedItem']['quantity_mode'];
		$rate=floatval($value['PurchasedItem']['unit_price']*$value['PurchasedItem']['quantity']);
		$unit_mode=$value['PurchasedItem']['quantity_mode'];
		if($unit_mode==1)
		{
			$unit="Pieces";
		}
		else
		{
			$unit="Cases";
		}
		$rate_grand_amount+=$rate;
		$discount=0;


		$taxable_value=floatval($value['PurchasedItem']['net_value']);
		$discount_item=floatval($value['PurchasedItem']['discount']);
		$taxable_value_grand_amount+=$taxable_value;
		$data['discount_grand_amount']+=$discount_item;
		$net_value=floatval($value['PurchasedItem']['net_value']);

		$tax_value=floatval($value['PurchasedItem']['tax_amount']);
		$total+=$net_value;


		$data['total']+=floatval($net_value);
		$data['total_cgst_tax']+=floatval($value['PurchasedItem']['tax_amount']);
		$data['total_sgst_tax']+=floatval($value['PurchasedItem']['tax_amount']);
		$data['total_igst_tax']+=floatval($value['PurchasedItem']['tax_amount']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$product_total=floatval($value['PurchasedItem']['total']);
		$product_grand_total+=$product_total;
		$cgst_rate=floatval($value['PurchasedItem']['tax']);
		$cgst_amount=floatval($value['PurchasedItem']['tax_amount']);
		$cgst_grand_amount+=$cgst_amount;
		$sgst_rate=floatval($value['PurchasedItem']['tax']);
		$sgst_amount=floatval($value['PurchasedItem']['tax_amount']);
		$sgst_grand_amount+=$sgst_amount;
		$igst_rate=floatval($value['PurchasedItem']['tax']);
		$igst_amount=floatval($value['PurchasedItem']['tax_amount']);
		$igst_grand_amount+=$igst_amount;
		$item_pos=2;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$keySI+1);
		$item_pos+=6;
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$product_name=implode($description_words,' ');
			$poped_array[]=array_pop($description_words);
			$product_name_length=strlen($product_name);
			if($product_name_length<=40)
			{ 
				$pdf->SetFont('dejavusans', '', 8);

				$pdf->text($item_pos+8,$item_table_head_y_start+8+(($i+$k)*8)+5,$value['Product']['code']);
				$pdf->text($item_pos+26,$item_table_head_y_start+8+(($i+$k)*8)+5,$value['Product']['name']);
				$pdf->text($item_pos+26,$item_table_head_y_start+8+(($i+$k)*8)+8,$value['Product']['arabic_name']);
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}

		if($k)
			$k--;
		$item_pos+=1;
		$pdf->SetFont('dejavusans', '', 8);
// $pdf->AddFont('DejaVu','','tahoma.ttf',true);
// $pdf->AddFont('DejaVu', 'B', 'tahoma-Bold.ttf', true);
// $pdf->SetFont('DejaVu','',8);
// $pdf->Text($item_pos,$item_table_head_y_start+11+($i*8)+5,$value['Product']['arabic_name']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$item_pos+=70+$igst_x_position;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$hsn_code);
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,'');
		$item_pos+=16;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$qty);
		$pdf->text($item_pos+8,$item_table_head_y_start+8+($i*8)+5,$unit);
		$item_pos+=20;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,round($rate,3));
		$item_pos+=10;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),number_format($rate,2));
		if(count($PurchasedItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$rate_grand_amount);
		}
		$item_pos+=5;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$discount);
		if(count($PurchasedItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$discount_grand_amount);
		}
// $item_pos+=1;
		$pdf->text($item_pos+8,$item_table_head_y_start+8+($i*8)+5,round(($taxable_value-$discount_item),3));
		if(count($PurchasedItem)==$keySI+1)
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,round($discount_item,3));
		if(count($PurchasedItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($taxable_value_grand_amount,2));
		}
		$item_pos+=21;
		if($Checkstate==1)
		{
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_rate.'%');
			$item_pos+=7;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_amount);
			if(count($PurchasedItem)==$keySI+1)
			{
// $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$cgst_grand_amount);
			}
			$item_pos+=15;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_rate.'%');
			$item_pos+=8;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_amount);
			if(count($PurchasedItem)==$keySI+1)
			{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$sgst_grand_amount);
			}
		}
		else
		{
			$pdf->text($item_pos+4,$item_table_head_y_start+8+($i*8)+5,$igst_rate.'%');
			$item_pos+=13;
			$pdf->text($item_pos-2,$item_table_head_y_start+8+($i*8)+5,number_format($igst_amount,2));
			if(count($PurchasedItem)==$keySI+1)
			{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($igst_grand_amount,2));
			}
		}

		$item_pos+=5;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_rate);
		$item_pos+=5;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_amount);
		$pdf->text($item_pos+0.5,$item_table_head_y_start+8+($i*8)+5,number_format($product_total,2));
		if(count($PurchasedItem)==$keySI+1)
		{

//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$igst_grand_amount);
//$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($product_grand_total,2));
		}
		$i+=$k;
		$count+=$k;
		if($i+$k>=13)
		{
			footer($pdf,$data,$Purchase);
			$page++;
			header_section($pdf,$Purchase,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value);
			$i=-1;
			$count-=13;
		}
		$i++;
	}

	footer($pdf,$data,$Purchase);
}
$pdf->Output();
exit;

}
public function qutofpdf($id=null)
{
	$Sale = $this->Sale->find('first', array(
		"joins" => array(
			array(
				"table" => 'customers',
				"alias" => 'Customer',
				"type" => 'inner',
				"conditions" => array('Customer.account_head_id=Sale.account_head_id'),
				),
			array(
				"table" => 'customer_types',
				"alias" => 'CustomerType',
				"type" => 'inner',
				"conditions" => array('CustomerType.id=Customer.customer_type_id'),
				),
			),
		'conditions' => array('Sale.id'=>$id),
		'fields' => array(
			'Sale.*',
			'AccountHead.name',
			'CustomerType.name',
			)
		));
	$name=$Sale['AccountHead']['name'];
	$invoice_no=$Sale['Sale']['invoice_no'];
// $date=$Sale['Sale']['date_of_delivered'];
	$date=$Sale['Sale']['date_of_order'];
	$SaleItem = $this->SaleItem->find('all', array(
		'conditions' => array('SaleItem.sale_id'=>$id),
		'fields' => array(
			'SaleItem.*',
			'Product.id',
			'Product.name',
			'Product.no_of_piece_per_unit'
			)
		));
	require('tfpdf/tfpdf.php');
	$pdf = new tFPDF('p', 'mm', [297, 210]);
	define('FPDF_FONTPATH','tfpdf/font');
	function header_section($pdf,$name,$date,$invoice_no) {
		$pdf->SetFont('Arial','B',8.4);
		$pdf->AddPage();
		$pdf->Image('img/logo.png',0,5,-400);
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
		$pdf->Text(148, 5, 'للتجارة المواد الغدائية  و احلويات');
		$pdf->Text(160, 9, "تجارة – استيراد – تصدير");
		$pdf->Text(152, 13, "For Food Stuff & Sweets Trading");
		$pdf->Text(152, 17, "Global Trade - Experts & Import");
		$pdf->SetFont('Arial','B',20);
		$pdf->SetTextColor(0,100,0);
// $pdf->Text(43, 13, "IDREES JOMA");
		$pdf->SetFont('Arial','B',12);
// $pdf->Text(43, 18, "Delivering Taste");
		$pdf->SetTextColor(200,00,00);
		$pdf->Text(10, 26, "Quotation No : ".$invoice_no);
		$pdf->SetTextColor(10,10,10);
		$pdf->SetFont('Arial','B',8);
		$head_table_x=28;
		$pdf->rect(150, $head_table_x-10, 55, 8);
		$pdf->Text(152, 21, "Customer:");
		$pdf->Text(152, 25, "No:");
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
		$pdf->Text(195, 21, ":زبون");
		$pdf->Text(199, 25, ":لا");
		$pdf->rect(5, $head_table_x, 140, 8);
		$pdf->Text(10, 33, "M/S : ".$name );
// $pdf->Text(135, 33, ": M/S" );
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
// $pdf->Text(80, 20, "السيولة النقدية/ائتمان فاتورة");
		$pdf->Text(80, 20, "سابتقا");
		$pdf->SetFont('Arial','B',12);
		$pdf->Text(80, 26, "Quotation");
		$pdf->rect(150, $head_table_x, 55, 8);
		$pdf->SetFont('Arial','B',9);
		$pdf->Text(152, 33, "Date : ".date('d-M-Y',strtotime($date)));
// $pdf->AddFont('Arabic','','arabic.php');
// $pdf->SetFont('Arabic','',1);
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',13);
		$pdf->Text(188, 33, ": تاريخ");
// $strp_txt = stripslashes("تاريخ");
// $strp_txt = iconv('UTF-8', 'windows-1252', $strp_txt);
// $strp_txt = utf8_decode($strp_txt);
		$first_table_x=$head_table_x+13;
		$pdf->rect(5, $first_table_x, 200, 200);
		$slno_x=6;
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',8);
// $pdf->Text($slno_x, $first_table_x+5, "SlNo");
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($slno_x, $first_table_x+5+5, "SlNo");
$pdf->Line($slno_x+7, $first_table_x,$slno_x+7, 200); // vertical line
$item_x=$slno_x+50;
$item_line_x=$item_x+100;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($item_x, $first_table_x+5, "وصف");
$pdf->SetFont('Arial','B',7);
$pdf->Text($item_x+50, $first_table_x+5, "Description");
$tax_x=$slno_x+127;
$tax_line_x=$item_x+75;
$pdf->Line($tax_line_x, $first_table_x,$tax_line_x, 200); // vertical line
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($tax_x, $first_table_x+5, "");
$pdf->SetFont('Arial','B',7);
$pdf->Text($tax_x, $first_table_x+5+5, "Tax");


$unit_x=$slno_x+140;
$unit_line_x=$item_x+85;
$pdf->Line($unit_line_x, $first_table_x,$unit_line_x, 200); // vertical line
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($unit_x, $first_table_x+5, "وَحْدة");
$pdf->SetFont('Arial','B',7);
$pdf->Text($unit_x, $first_table_x+5+5, "Unit");




// $pdf->Line($item_line_x, $first_table_x,$item_line_x, 200); // vertical line
// $tax_x=$item_line_x+20;
// $tax_line_x=$tax_x+15;
// $pdf->Text($tax_x, $first_table_x+5, "Tax");
$pdf->Line($item_line_x, $first_table_x,$item_line_x, 200); // vertical line
$qty_x=$item_line_x+4;
$qty_line_x=$qty_x+8;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($qty_x, $first_table_x+5, "كمية");
$pdf->SetFont('Arial','B',7);
$pdf->Text($qty_x, $first_table_x+5+5, "Qty");
$pdf->Line($qty_line_x, $first_table_x,$qty_line_x, 200); // vertical line
$unit_price_x=$qty_line_x+2;
$unit_price_line_x=$unit_price_x+15;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($unit_price_x, $first_table_x+5, "سعر الوحدة");
$pdf->SetFont('Arial','B',7);
$pdf->Text($unit_price_x, $first_table_x+5+5, "Unit Price");
$pdf->Line($unit_price_line_x, $first_table_x,$unit_price_line_x, 200); // vertical line
// $net_amount_x=$qty_line_x+2;
// $net_amount_line_x=$net_amount_x+16;
// $pdf->Text($net_amount_x, $first_table_x+5, "Net Amount");
// $pdf->Line($net_amount_line_x, $first_table_x,$net_amount_line_x, 200); // vertical line
// $tax_amount_x=$unit_price_x+1;
// $tax_amount_line_x=$tax_amount_x+15;
// $pdf->Text($tax_amount_x, $first_table_x+5, "Tax Amount");
// $pdf->Line($tax_amount_line_x, $first_table_x,$tax_amount_line_x, 200); // vertical line
$total_x=$unit_price_line_x+6;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($total_x, $first_table_x+5, "مجموع");
$pdf->SetFont('Arial','B',7);
$pdf->Text($total_x, $first_table_x+5+5, "Total");
$pdf->Line(5, $first_table_x+12, 205, $first_table_x+12); // horizontal line
$pdf->Line(5, 200, 205, 200); // horizontal line
$pdf->Line(5, 205, 205, 205); // horizontal line
$i=0;
}
function footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount)
{
	$pdf->SetFont('Arial','B',8);
$pdf->Line(185, 205,185, 215); // vertical line
$pdf->Text(190, 211, number_format($grand_amount,3));
$pdf->Text(10, 211, 'Grand Total :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text(170, 211, ': مجموع');
$pdf->Line(5, 215, 205, 215); // horizontal line

/*****/
$pdf->SetFont('Arial','B',8);
$pdf->Line(185, 215,185, 224); // vertical line
$pdf->Text(190, 220, number_format($discount,3));
$pdf->Text(10, 220, 'Discount :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text(170, 220, ': مجموع');//add later
$pdf->Line(5, 224, 205, 224); // horizontal line
/****/
/*****/
$pdf->SetFont('Arial','B',8);
$pdf->Line(185, 225,185, 234); // vertical line
$pdf->Text(190, 229, number_format($grand_total,3));
$pdf->Text(10, 229, 'Net Total :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text(170, 229, ': مجموع');
$pdf->Line(5, 234, 205, 234); // horizontal line
/*****/

// $pdf->Text(167, 212, "Discount  :    ".$discount."");
$net_value=$grand_total-$discount;
// $pdf->Text(167, 216, "Net Value :    ".$net_value."");
$footer_line=203;
$pdf->SetFont('Arial','B',8);
// $pdf->Text(10, $footer_line+35+(5*0), "E&OE");
// $pdf->Text(10, $footer_line+35+(5*1), "Terms of Delivery and payment if any");
// $pdf->Text(100, $footer_line+35+(5*2), "DECLARATION");
// $pdf->Text(10, $footer_line+35+(5*3), "Certified that all the particulars shown in the above Tax invoice are true and correct and that my/our Registration under KVAT Act 2003 is");
// $pdf->Text(10, $footer_line+35+(5*4), "valid as on the date of this Bill.");
// $pdf->Text(160, $footer_line+35+(5*5), "For <company name>");
// $pdf->Text(165, $footer_line+35+(5*6), "Autherised Signatory");
// $pdf->Text(10, $footer_line+35+(5*7), " * Original for the buyer for the purpose of claiming Input Tax Credit, Duplicate for the Transport Copy,Triplicate for the filling at the");
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',11);
$pdf->Text(185, 295, "تم الاستلام");
$pdf->Text(60, 295, "بائع");
$pdf->SetFont('Arial','B',8);
$pdf->Text(10, 295, "Salesman");
$pdf->Text(120, 295, "Recieved By");
}
$slno =1;
header_section($pdf,$name,$date,$invoice_no);
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+36;
$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; foreach($SaleItem as $key=>$value) {
	$sl_no_v_x=8;
	$pdf->Text($sl_no_v_x, $first_table_x+11+(5*$i), $slno);
	$product_name_v_x=$sl_no_v_x+10;

	$pdf->Text($product_name_v_x, $first_table_x+11+(5*$i), $value['Product']['name']);
	$tax_v_x=$product_name_v_x+115;
	$pdf->Text($tax_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['tax']);

	$unit_v_x=$product_name_v_x+126;
	$pdf->Text($unit_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['quantity_mode']);
	$quantity_v_x=$product_name_v_x+140;
	$unit_level=$this->Unit->findByName($value['SaleItem']['quantity_mode']);
	if($unit_level['Unit']['level'] !=1){
		$value['SaleItem']['quantity']=$value['SaleItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
	}
	$pdf->Text($quantity_v_x, $first_table_x+11+(5*$i),$value['SaleItem']['quantity'] );
	$unit_price_v_x=$quantity_v_x+12;
	$pdf->Text($unit_price_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['unit_price']);
	$unit_price=$value['SaleItem']['unit_price'];
	$quantity=$value['SaleItem']['quantity'];
// $net_amount_v_x=$quantity_v_x+11;
// $pdf->Text($net_amount_v_x, $first_table_x+11+(5*$i), $unit_price*$quantity);
// $tax_amount_v_x=$net_amount_v_x+17;
// $pdf->Text($tax_amount_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['tax_amount']);
	$total_v_x=$unit_price_v_x+18;
	$pdf->Text($total_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['total']);
	$i++;
	$slno++;
	$grand_amount+=$unit_price*$quantity;
	$grand_tax_amount+=$value['SaleItem']['tax_amount'];
	$grand_total+=$value['SaleItem']['total'];

// $discount=$grand_total-$Sale['Sale']['total'];
	$discount=$Sale['Sale']['discount_amount'];
	$grand_total = $grand_amount - $discount;
// if($i>31)
	if($i>25)
	{
		$i=0;
// footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
		header_section($pdf,$name,$date,$invoice_no);
	}

}
footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
$pdf->Output();
exit;
}
public function stockfpdf($id=null)
{
	$StockTransfer = $this->StockTransfer->findById($id);
	$from_warehouse= $this->Warehouse->field(
		'Warehouse.name',
		array('Warehouse.id ' => $StockTransfer['StockTransfer']['warehouse_from']));
	$to_warehouse= $this->Warehouse->field(
		'Warehouse.name',
		array('Warehouse.id ' => $StockTransfer['StockTransfer']['warehouse_to']));
	$transfer_no =$StockTransfer['StockTransfer']['transfer_no'];
// $date=$Sale['Sale']['date_of_delivered'];
	$date=$StockTransfer['StockTransfer']['date'];
	$this->Product->virtualFields = array(
		'product_name' => "CONCAT(Product.name, '-', Product.code)"
		);

	$StockTransferitems=$this->StockTransferItem->find('all',array(
		"conditions"=>array('stock_transfer_id'=>$id),
		"fields"=>array(
//'StockTransferItem.*',
// 'product_name',
			),
		));
// pr($StockTransferitems);exit;
	require('tfpdf/tfpdf.php');
	$pdf = new tFPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	define('FPDF_FONTPATH','tfpdf/font');
	function header_section($pdf,$Profile,$from_warehouse,$to_warehouse,$date,$transfer_no) {
		$pdf->SetFont('Arial','B',8.4);
		$pdf->AddPage();
// $pdf->Image('img/logo.png',0,5,-400);
		$pdf->Image('profile/'.$Profile['logo'],5,0,23);

		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
// $pdf->Text(148, 5, 'للتجارة المواد الغدائية  و احلويات');
// $pdf->Text(160, 9, "تجارة – استيراد – تصدير");
// $pdf->Text(152, 13, "For Food Stuff & Sweets Trading");
// $pdf->Text(152, 17, "Global Trade - Experts & Import");
		$pdf->SetFont('Arial','B',20);
		$pdf->SetTextColor(0,100,0);
// $pdf->Text(43, 13, "IDREES JOMA");
		$pdf->SetFont('Arial','B',12);
// $pdf->Text(43, 18, "Delivering Taste");
		$pdf->SetTextColor(200,00,00);
		$pdf->Text(10, 26, "Transfer ID : ".$transfer_no);
		$pdf->SetTextColor(10,10,10);
		$pdf->SetFont('Arial','B',8);
		$head_table_x=28;
		$pdf->rect(150, $head_table_x-10, 55, 8);
		$pdf->Text(152, 21, "From : ".$from_warehouse);
// $pdf->Text(152, 25, "No:");
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
// $pdf->Text(195, 21, ":زبون");
// $pdf->Text(199, 25, ":لا");
		$pdf->rect(5, $head_table_x, 140, 8);
		$pdf->SetFont('Arial','',12);

		$pdf->Text(10, 33, "Warehouse : ".$to_warehouse );
// $pdf->Text(135, 33, ": M/S" );
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
// $pdf->Text(80, 20, "السيولة النقدية/ائتمان فاتورة");
// $pdf->Text(80, 20, "سابتقا");
		$pdf->SetFont('Arial','B',12);
		$pdf->Text(80, 26, "Stock Transfer");
		$pdf->rect(150, $head_table_x, 55, 8);
		$pdf->SetFont('Arial','B',9);
		$pdf->Text(152, 33, "Date : ".date('d-M-Y',strtotime($date)));
// $pdf->AddFont('Arabic','','arabic.php');
// $pdf->SetFont('Arabic','',1);
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',13);
// $pdf->Text(188, 33, ": تاريخ");
// $strp_txt = stripslashes("تاريخ");
// $strp_txt = iconv('UTF-8', 'windows-1252', $strp_txt);
// $strp_txt = utf8_decode($strp_txt);
		$first_table_x=$head_table_x+13;
		$pdf->rect(5, $first_table_x, 200, 200);
		$slno_x=6;
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',8);
// $pdf->Text($slno_x, $first_table_x+5, "SlNo");
		$pdf->SetFont('Arial','B',9);
		$pdf->Text($slno_x, $first_table_x+5+5, "SlNo");
$pdf->Line($slno_x+7, $first_table_x,$slno_x+7, 200); // vertical line
$item_x=$slno_x+70;
$item_line_x=$item_x+77;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($item_x, $first_table_x+5, "وصف");
$pdf->SetFont('Arial','B',9);
$pdf->Text($item_x-5, $first_table_x+5+5, "Description");
$tax_x=$slno_x+117;
$tax_line_x=$item_x+60;
$pdf->Line($tax_line_x, $first_table_x,$tax_line_x, 200); // vertical line
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($tax_x, $first_table_x+5, "");
$pdf->SetFont('Arial','B',7);
// $pdf->Text($tax_x, $first_table_x+5+5, "Tax");


$unit_x=$slno_x+135;
$unit_line_x=$item_x+68;
// $pdf->Line($unit_line_x, $first_table_x,$unit_line_x, 200); // vertical line
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($unit_x, $first_table_x+5, "وَحْدة");
$pdf->SetFont('Arial','B',9);
$pdf->Text($unit_x, $first_table_x+5+5, "MRP");




//$pdf->Line($item_line_x, $first_table_x,$item_line_x, 210); // vertical line
$pdf->Line($item_line_x, $first_table_x,$item_line_x, 210); // vertical line

// $tax_x=$item_line_x+20;
// $tax_line_x=$tax_x+15;
// $pdf->Text($tax_x, $first_table_x+5, "Tax");
// $pdf->Line($item_line_x, $first_table_x,$item_line_x, 200); // vertical line
$qty_x=$item_line_x+4;
$qty_line_x=$qty_x+12;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($qty_x, $first_table_x+5, "كمية");
$pdf->SetFont('Arial','B',9);
$pdf->Text($qty_x-2, $first_table_x+5+5, "Unit");
$pdf->Text($qty_x+10, $first_table_x+5+5, "Quantity");

// $pdf->Line($qty_line_x, $first_table_x,$qty_line_x, 200); // vertical line

$unit_price_x=$qty_line_x+3;
$unit_price_line_x=$unit_price_x-6;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($unit_price_x, $first_table_x+5, "سعر الوحدة");
$pdf->SetFont('Arial','B',9);
$pdf->Text($unit_price_x+15, $first_table_x+5+5, "Total ");
$pdf->Line($unit_price_line_x, $first_table_x,$unit_price_line_x, 210); // vertical line
$pdf->Line($unit_price_line_x+15, $first_table_x,$unit_price_line_x+15, 210); // vertical line

// $net_amount_x=$qty_line_x+22;
// $net_amount_line_x=$net_amount_x;
// $pdf->Text($net_amount_x+2, $first_table_x+5+5, "Net Amount");
// $pdf->Line($net_amount_line_x, $first_table_x,$net_amount_line_x, 200); // vertical line
// $tax_amount_x=$unit_price_x+1;
// $tax_amount_line_x=$tax_amount_x+15;
// $pdf->Text($tax_amount_x, $first_table_x+5, "Tax Amount");
// $pdf->Line($tax_amount_line_x, $first_table_x,$tax_amount_line_x, 200); // vertical line
$total_x=$unit_price_line_x+6;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($total_x, $first_table_x+5, "مجموع");
$pdf->SetFont('Arial','B',8);
// $pdf->Text($total_x, $first_table_x+5+5, "Total");
$pdf->Line(5, $first_table_x+12, 205, $first_table_x+12); // horizontal line
$pdf->Line(5, 200, 205, 200); // horizontal line
$pdf->Line(5, 210, 205, 210); // horizontal line
$i=0;
}

$slno =1;
header_section($pdf,$Profile,$from_warehouse,$to_warehouse,$date,$transfer_no);
function footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount)
{
	$pdf->SetFont('Arial','B',8);
	$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
	$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
	$pdf->SetFont('DejaVu','',11);
// $pdf->Text(185, 280, "تم الاستلام");
// $pdf->Text(60, 280, "بائع");
	$pdf->SetFont('Arial','B',8);
	$pdf->Text(10, 280, "Sales Executive");
	$pdf->Text(160, 280, "Ware House Manager");
}
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+36;
//pr($StockTransferitems);exit;
$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; $total_quantity=0; $total_qty=0; $total_pieceqty=0; $total_caseqty=0;
$product_line=0;
foreach($StockTransferitems as $key=>$value) {
	$sl_no_v_x=8;
	$pdf->Text($sl_no_v_x, $first_table_x+12+(5*$i), $slno);
	$product_name_v_x=$sl_no_v_x+10;

	$pdf->Text($product_name_v_x-3, $first_table_x+12+(5*$i), $value['Product']['product_name']);
	$tax_v_x=$product_name_v_x+115;
// $pdf->Text($tax_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['tax']);
	if($value['StockTransferItem']['unit_id'] == 2){
		$quantity=$value['StockTransferItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
		$whole = floor($quantity);      
		$fraction = $value['StockTransferItem']['quantity']%$value['Product']['no_of_piece_per_unit']; 
		$pieces=number_format($fraction);
		$cases=$whole;
		$total_quantity=$cases.'/'.$pieces;
		$mrp=floatval($value['Product']['mrp']*$value['Product']['no_of_piece_per_unit']);
	}else{
		$quantity=$value['StockTransferItem']['quantity'];
		$total_quantity=number_format($quantity);
		$mrp=floatval($value['Product']['mrp']);
	}
	$unit_v_x=$product_name_v_x+125;
	$pdf->Text($unit_v_x-3, $first_table_x+12+(5*$i),$mrp);
	if($value['StockTransferItem']['unit_id']==2)
	{
		$unit="Cases";
	}
	else
	{
		$unit="Pieces";
	}
	$pdf->Text($unit_v_x+12, $first_table_x+12+(5*$i),$unit);

	$quantity_v_x=$unit_v_x+26;
// $unit_level=$this->Unit->findByName($value['SaleItem']['quantity_mode']);
// if($unit_level['Unit']['level'] !=1){
//   $value['SaleItem']['quantity']=$value['SaleItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
// }
	if($value['StockTransferItem']['unit_id'] == 2){
		$quantity=$value['StockTransferItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
		$whole = floor($quantity);      
		$fraction = $value['StockTransferItem']['quantity']%$value['Product']['no_of_piece_per_unit']; 
		$pieces=number_format($fraction);
		$cases=$whole;
		$total_quantity=$cases.'/'.$pieces;
		$total_caseqty+=$total_quantity;
	}else{
		$quantity=$value['StockTransferItem']['quantity'];
		$total_quantity=number_format($quantity);
		$total_pieceqty+=$total_quantity;
	}
	$pdf->Text($quantity_v_x, $first_table_x+12+(5*$i),$total_quantity);
	$unit_price_v_x=$quantity_v_x+35;
// $pdf->Text($unit_price_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['unit_price']);
// $unit_price=$value['SaleItem']['unit_price'];
	$quantity=$value['StockTransferItem']['quantity'];
	$net_amount_v_x=$quantity_v_x+20;
	$pdf->Text($net_amount_v_x-3, $first_table_x+12+(5*$i), $value['Product']['mrp']*$value['StockTransferItem']['quantity']);
// $tax_amount_v_x=$net_amount_v_x+17;
// $pdf->Text($tax_amount_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['tax_amount']);
	$total_v_x=$unit_price_v_x+18;
// $pdf->Text($total_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['total']);
	$i++;
	$slno++;
// $grand_amount+=$unit_price*$quantity;
// $grand_tax_amount+=$value['SaleItem']['tax_amount'];
	$grand_total+=($value['Product']['mrp']*$value['StockTransferItem']['quantity']);
//$total_quantity+=$value['StockTransferItem']['quantity'];
	if($total_caseqty!=0 && $total_pieceqty!=0)
	{
	$total_qty=$total_caseqty.'/'.$total_pieceqty;
    }
    else if($total_caseqty==0)
    {
     $total_qty=$total_pieceqty;

    }
    else if($total_pieceqty==0)
    {
     $total_qty=$total_caseqty;
    }
//$grand_v_x=$total_v_x+25;
// $discount=$grand_total-$Sale['Sale']['total'];
// $discount=$Sale['Sale']['discount_amount'];
// $grand_total = $grand_amount - $discount;
// if($i>31)
	$pdf->Line(5, $first_table_x+7.7+(5*$i+1), 205, $first_table_x+7.7+(5*$i+1)); // horizontal line
	$product_line+=5;
	if($i>28)
	{
		$i=0;
// footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
		header_section($pdf,$Profile,$from_warehouse,$to_warehouse,$date,$transfer_no);
	}

}
$pdf->SetFont('Arial','B',12);
$pdf->Text($unit_v_x-2, 205,'Total');
$pdf->Text($quantity_v_x, 205,$total_qty);
$pdf->Text($net_amount_v_x-5, 205,$grand_total);
footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
$pdf->Output();
exit;
}
public function account_receivable_fpdf1($id=null,$amount=null){

	$journal_data=$this->Journal->find('first',array(
		'conditions'=>array('Journal.credit'=>$id,'Journal.debit !=' =>35),
		'order'=>array('Journal.id DESC')
		));

	require('fpdf/fpdf.php');
	$pdf = new FPDF('p', 'mm', [297, 210]);
	$page=1;
	$total_page=1;
	$Profile=$this->Global_Var_Profile['Profile'];
	function convert_number_to_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}
	function header_section($pdf,$Profile,$total_page,$page,$customer_data){
		$pdf->AddPage();
		$pdf->Image('profile/'.$Profile['logo'],0,5,-225);
		$image_line_width=45;
		$invoice_x_starting=$image_line_width;
		$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(0,0,0);
		$pdf->rect(3, 9, 203,120);
		$item_table_head_y_start=10;
		$pdf->SetDrawColor('150','150','150');
		$pdf->Line($image_line_width, $item_table_head_y_start,$image_line_width, $item_table_head_y_start+40);
		$pdf->Line(1, 108, 1, 108);
		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos=0;
		$invoice_y=0;
		$pdf->SetFont('Arial','B',12);
		$invoice_pos+=30;
		$invoice_y+=98;
		$pdf->Cell($invoice_y,$invoice_pos-12,"RECEIPT VOUCHER",0,0,'C');
		$pdf->SetFont('Arial','B',11);
		$pdf->Cell($invoice_y-180,$invoice_pos,$Profile['company_name'],0,0,'C');
		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=14;
		$invoice_y+=11;
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell($invoice_y,$invoice_pos,$Profile['address_line_1'],0,0,'C');
		$pdf->SetXY($invoice_x_starting, 5);
		$pdf->SetFont('Arial','B',10);
		$invoice_pos+=12;
// $invoice_y+=7;
		$pdf->Cell($invoice_y,$invoice_pos,$Profile['address_line_2'],0,0,'C');
		$pdf->SetXY($invoice_x_starting, 5);        
		$invoice_pos+=12;
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell($invoice_y-35,$invoice_pos,'PH:44273388',0,0,'C');  
		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=12;
// $invoice_y+=15;
		$pdf->SetFont('Arial','U',10);
		$pdf->Cell($invoice_y,$invoice_pos,$Profile['mail_address'],0,0,'C');   
		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos_right=0;
		$invoice_pos_right+=34;
		$pdf->SetFont('Arial','B',12);
		$pdf->Cell(0,$invoice_pos_right,'RECEIPT',0,0,'R');
		$invoice_pos_right+=12;
		$pdf->SetFont('Arial','B',10);
		$invoice_pos_right+=12;
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(0,$invoice_pos_right,'Date : '.date('d-m-Y'),0,0,'R');
		$invoice_pos_right+=12;
		$pdf->SetFont('Arial','B',10);
// $pdf->Cell(0,$invoice_pos_right,'Branch : Main',0,0,'R');
		$invoice_pos_right-=20;
		$pdf->Line(3, $invoice_pos_right,206, $invoice_pos_right); 
	}
	function footer($pdf,$Profile,$total)
	{
		$footer_start_y=115;
		$footer_position_y=0;
		$footer_position_y+=6;
		$pdf->SetFont('Arial','I',10);
		$pdf->rect(5, $footer_start_y, 198,11);
		$pdf->text(6,$footer_start_y+3,'Received By');
		$footer_position_y+=40;
		$pdf->Line($footer_position_y, $footer_start_y,$footer_position_y, $footer_start_y+11);
		$footer_position_y+=2;
		$pdf->text($footer_position_y,$footer_start_y+3,'Copy');
		$pdf->SetFont('Arial','B',10);
// $pdf->text($footer_position_y,$footer_start_y+8,'(Passenger Copy)');
		$footer_position_y+=40;
		$pdf->Line($footer_position_y, $footer_start_y,$footer_position_y, $footer_start_y+11);
		$pdf->SetFont('Arial','I',10);
		$footer_position_y+=2;
		$pdf->text($footer_position_y,$footer_start_y+3,'Prepared By');
		$pdf->SetFont('Arial','B',10);
		$pdf->text($footer_position_y,$footer_start_y+8,'');
		$footer_position_y+=80;
		$pdf->Line($footer_position_y, $footer_start_y,$footer_position_y, $footer_start_y+11);
		$pdf->SetFont('Arial','I',10);
		$footer_position_y+=2;
		$pdf->text($footer_position_y,$footer_start_y+3,'Printed By');
		$pdf->SetFont('Arial','B',10);
		$pdf->text($footer_position_y,$footer_start_y+8,'');
	}       
	header_section($pdf,$Profile,$total_page,$page,$journal_data);
	$amount=$amount;
	$pdf->SetFont('Arial','B',12);
	$customer_detail_startin=42;
	$invoice_lin_width=0;
	$pdf->SetFont('Arial','',10);
//if($Check_details['AccountHead']['sub_group_id'] !=15){
	$cash_receipt="CASH SALE";
	$cash_paid="Received From     : ";
// }else{
//     $cash_receipt="CASH PURCHASE";
//     $cash_paid="Paid To     : ";

// }     
// $pdf->text($invoice_lin_width+5,$customer_detail_startin+4+10, $cash_paid);
// $pdf->SetFont('Arial','B',10);
// $pdf->text($invoice_lin_width+36,$customer_detail_startin+4+10, $cash_receipt);

	$pdf->SetFont('Arial','B',10);
	$pdf->SetFont('Arial','',10);
	$pdf->text($invoice_lin_width+5,$customer_detail_startin+4+15, 'Customer Name  : ');
	$pdf->SetFont('Arial','B',10);
	$pdf->text($invoice_lin_width+36,$customer_detail_startin+4+15, $journal_data['AccountHeadCredit']['name']);
	$pdf->SetFont('Arial','',10);
	$pdf->rect(5, 70, 198,18);
	$pdf->Line(5, 80,203, 80); 
	$payment_detail_start=70;
	$payment_detail_start_y=0;
	$pdf->SetFont('Arial','',11);
	$pdf->text(20,$payment_detail_start+6,'Payment Method');
	$pdf->SetFont('Arial','B',11);
	if($journal_data['Journal']['debit'] == 1){
		$payment_method="Cash";
		$Bank="";
	}else{
		$payment_method="Bank";
		$Bank=$journal_data['AccountHeadDebit']['name'];
	}
	$Invoice_no=substr($journal_data['Journal']['remarks'],17);

	$pdf->text(28,$payment_detail_start+15,$payment_method);
	$payment_detail_start_y+=66;
	$pdf->Line($payment_detail_start_y, $payment_detail_start,$payment_detail_start_y, $payment_detail_start+18);
	$payment_detail_start_y+=24;
	$pdf->SetFont('Arial','',11);

	$pdf->text($payment_detail_start_y,$payment_detail_start+6,'Bank Name');
	$pdf->SetFont('Arial','B',11);  
	$pdf->text(75,$payment_detail_start+15,$Bank);

	$payment_detail_start_y+=60;
//$pdf->Line($payment_detail_start_y, $payment_detail_start,$payment_detail_start_y, $payment_detail_start+18);
	$payment_detail_start_y+=13;
	$pdf->SetFont('Arial','',11);
// $pdf->text($payment_detail_start_y,$payment_detail_start+6,'Invoice No');
	$pdf->SetFont('Arial','B',11); 
	if($Invoice_no != 0) {
// $pdf->text(165,$payment_detail_start+15,$Invoice_no);
	}
	$pdf->SetFont('Arial','B',14);
	$payment_detail_start+=21;
	$pdf->text(125,$payment_detail_start+6,'Total Amount :'.''.$amount);
	$payment_detail_start+=10;
	$pdf->SetFont('Arial','',11);
	$pdf->text(5,$payment_detail_start+6,strtoupper(convert_number_to_words(intval($amount))).' '.'ONLY');
	$total=0;
	footer($pdf,$Profile,$total);        
	$pdf->Output();
	exit;
}
public function account_receivable_fpdf($id=null,$amount=null){

	$journal_data=$this->Journal->find('first',array(
		'joins' => [
		[
		"table" => "customers",
		"alias" => "Customer",
		"type" => "INNER",
		"conditions" => ['AccountHeadCredit.id=Customer.account_head_id'],
		],],
		'fields'=>array('Customer.*','Journal.*','AccountHeadCredit.*','AccountHeadDebit.*'),
		'conditions'=>array('Journal.credit'=>$id,'Journal.debit !=' =>35),
		'order'=>array('Journal.id DESC')
		));
	$route_id=$journal_data['Customer']['route_id'];
	$ExecutiveRouteMapping=$this->ExecutiveRouteMapping->findByRouteId($route_id);
	if($ExecutiveRouteMapping)
	{
		$executive_id=$ExecutiveRouteMapping['ExecutiveRouteMapping']['executive_id'];
		$Executive=$this->Executive->findById($executive_id);
		$executive_name=$Executive['Executive']['name'];
	}
	else
	{
		$executive_name=" ";
	}
	$bank=2;

	$debit=0;
	$credit=0;
	$Journal_Debit=$this->Journal->find('all',array('conditions'=>array(
// 'credit'=>$modes,
		'debit'=>$id,
		'flag=1',
		)));
	foreach ($Journal_Debit as $key => $value) {
		$debit+=$value['Journal']['amount'];
	}
	$Journal_Credit=$this->Journal->find('all',array('conditions'=>array(
// 'debit'=>$modes,
		'credit'=>$id,
		'flag=1',
		)));
	foreach ($Journal_Credit as $key => $value) {
		$credit+=$value['Journal']['amount'];
	}
	$AccountHead=$this->AccountHead->find('all',array(
		'fields'=>array(
			'AccountHead.opening_balance',
			),
		'conditions'=>array('AccountHead.id'=>$id)
		));
	$total=$debit+$AccountHead[0]['AccountHead']['opening_balance'];
	$balance=$total-$credit;
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}
	$igst_x_position=0;

	function header_section($pdf,$Profile,$total_page,$page,$journal_data,$amount,$balance,$debit,$credit,$total,$executive_name
		){
		$pdf->AddPage();
		$pdf->Image('profile/'.$Profile['logo'],3,13,40);
		$image_line_width=45;
		$invoice_x_starting=$image_line_width;
//$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(0,0,0);
		$pdf->rect(3, 9, 203,145);
		$item_table_head_y_start=10;
		$pdf->SetDrawColor('150','150','150');
		$pdf->Line($image_line_width, $item_table_head_y_start,$image_line_width, $item_table_head_y_start+40);
		$invoice_pos=0;
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(90,$invoice_pos,"RECEIPT VOUCHER");
		$recept="سند القبض";
		$pdf->text(125,$invoice_pos,$recept);

		$invoice_pos+=2;
		$pdf->text(78,$invoice_pos+3,$Profile['company_name']);
		$pdf->text(155,$invoice_pos+3,$Profile['company_name_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
		$pdf->text(70,$invoice_pos+3,$Profile['address_line_1']);
		$pdf->text(140,$invoice_pos+3,$Profile['address_line_1_arabic'] );

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(65,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
		$pdf->text(160,$invoice_pos+3,$Profile['address_line_2_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('times', 'U', 10, "", 'false');
		$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(78,$invoice_pos,'VAT Number : '.$Profile['vat_code']);
		$VAT_Number='ظريبه الشراء';
		$pdf->text(140,$invoice_pos,$VAT_Number);

		$gst_details_y_staring=50;
$pdf->Line(3, $gst_details_y_staring,206, $gst_details_y_staring); // horizontal line
$pdf->Line(45, $gst_details_y_staring,45, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos=$gst_details_y_staring+2;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(3,$gst_pos,'Customer Name:');
$pdf->text(3,$gst_pos+7,'Customer No');
$pdf->text(3,$gst_pos+14,'VAT Number');
$pdf->text(3,$gst_pos+21,'Address');
$pdf->text(3,$gst_pos+28,'Receipt No:');
$pdf->text(3,$gst_pos+35,'Date');
$gst_line_width=120;
$pdf->Line($gst_line_width+40, $gst_details_y_staring,$gst_line_width+40, $gst_details_y_staring+17+21+5);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text($gst_line_width-240,$gst_pos,$journal_data['AccountHeadCredit']['name']);
$pdf->text($gst_line_width+2,$gst_pos,$journal_data['Customer']['arabic_name']);
$pdf->text($gst_line_width-240,$gst_pos+7,$journal_data['Customer']['mobile']);
$pdf->text($gst_line_width-240,$gst_pos+14,$journal_data['Customer']['vat_no']);
$pdf->text($gst_line_width-240,$gst_pos+21,$journal_data['Customer']['place']);
$pdf->text($gst_line_width-240,$gst_pos+28,$journal_data['Journal']['receipt_no']);
$pdf->text($gst_line_width-240,$gst_pos+35,date("d-m-Y",strtotime($journal_data['Journal']['date'])));

$pdf->SetFont('dejavusans', '', 10);
$pdf->text($gst_line_width+1+66,$gst_pos,'اسم العميل');
$pdf->text($gst_line_width+1+66,$gst_pos+7,'رقم العميل');
$pdf->text($gst_line_width+1+66,$gst_pos+14,'رقم الضريبة');
$pdf->text($gst_line_width+1+73,$gst_pos+21,'عنوان.');
$pdf->text($gst_line_width+1+67,$gst_pos+28,'عدد إيصال');
$pdf->text($gst_line_width+1+76,$gst_pos+35,'تاريخ');
$pdf->Line(3, $gst_details_y_staring+17+5+21,206, $gst_details_y_staring+17+5+21); // horizontal line
$table_length=25;
$item_table_head_y_start=$gst_details_y_staring+43;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(10,$item_table_head_y_start+1,'Total Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text(10,$item_table_head_y_start+7, 'المبلغ الإجمالي');
$pdf->text(10,$item_table_head_y_start+17,round($balance+$amount).' ₹');
$table_column=50;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line($table_column+50, $item_table_head_y_start,$table_column+50, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+10,$item_table_head_y_start+1,'Payment Method');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+10,$item_table_head_y_start+7, 'طريقة الدفع');
if($journal_data['Journal']['debit'] == 1){
	$payment_method="Cash";
	$Bank="";
}else{
	$payment_method="Bank";
}
$pdf->text($table_column+13,$item_table_head_y_start+17,$payment_method);
$pdf->Line($table_column+5, $item_table_head_y_start,$table_column+5, $item_table_head_y_start+$table_length); // vertical line
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($table_column+60,$item_table_head_y_start+1,'Paid Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+60,$item_table_head_y_start+6,'المبلغ المدفوع');
$pdf->text($table_column+60,$item_table_head_y_start+17,$amount.'  ₹');
$table_column+=100;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($table_column+10,$item_table_head_y_start+1,'Balance Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+10,$item_table_head_y_start+6,'مقدار وسطي');
$pdf->text($table_column+10,$item_table_head_y_start+17,round($balance).'  ₹');

$pdf->Line(3, $gst_details_y_staring+17+5+21+13,206, $gst_details_y_staring+17+5+21+13); // horizontal line
$item_table_head_y_start=$gst_details_y_staring+68;
$table_column=45;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(3,$item_table_head_y_start+3,'Amount In Words');
$convert_number_to_words=strtoupper(convert_number_to_words(floatval($amount)));
$pdf->text(46,$item_table_head_y_start+3,$convert_number_to_words);
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length-7); // vertical line
$pdf->Line($table_column+115, $item_table_head_y_start,$table_column+115, $item_table_head_y_start+$table_length-7); // vertical line
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(181,$item_table_head_y_start+8,'المبلغ بالكلمات');
$convert_number_to_arabic_words=strtoupper(convert_number_to_arabic_words(floatval($amount)));
$pdf->text(100,$item_table_head_y_start+8,$convert_number_to_arabic_words);
$pdf->Line(3, $gst_details_y_staring+17+5+21+25,206, $gst_details_y_staring+17+5+21+25); // horizontal line
$pdf->SetFont('times', '', 10, "", 'false');
$table_column=45;
$item_table_head_y_start=$gst_details_y_staring+86;
$pdf->text(3,$item_table_head_y_start+3,'Received By:');
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(72,$item_table_head_y_start+3,'استلمت من قبل:');
$pdf->Line($table_column+55, $item_table_head_y_start,$table_column+55, $item_table_head_y_start+$table_length-7); // vertical line
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(100,$item_table_head_y_start+3,'Salesman:');
$pdf->text(120,$item_table_head_y_start+3,$executive_name);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(195,$item_table_head_y_start+3,'بائع:');

$pdf->Line(3, $gst_details_y_staring+17+5+21+43,206, $gst_details_y_staring+17+5+21+43); // horizontal line

//$table_column+=70+$igst_x_position;
}
// function footer($pdf,$Profile,$total)
// {
//     $footer_start_y=115;
//     $footer_position_y=0;
//     $footer_position_y+=6;
//     $pdf->SetFont('Arial','I',10);
//     $pdf->rect(5, $footer_start_y, 198,11);
//     $pdf->text(6,$footer_start_y+3,'Received By');
//     $footer_position_y+=40;
//     $pdf->Line($footer_position_y, $footer_start_y,$footer_position_y, $footer_start_y+11);
//     $footer_position_y+=2;
//     $pdf->text($footer_position_y,$footer_start_y+3,'Copy');
//     $pdf->SetFont('Arial','B',10);
//     // $pdf->text($footer_position_y,$footer_start_y+8,'(Passenger Copy)');
//     $footer_position_y+=40;
//     $pdf->Line($footer_position_y, $footer_start_y,$footer_position_y, $footer_start_y+11);
//     $pdf->SetFont('Arial','I',10);
//     $footer_position_y+=2;
//     $pdf->text($footer_position_y,$footer_start_y+3,'Prepared By');
//     $pdf->SetFont('Arial','B',10);
//     $pdf->text($footer_position_y,$footer_start_y+8,'');
//     $footer_position_y+=80;
//     $pdf->Line($footer_position_y, $footer_start_y,$footer_position_y, $footer_start_y+11);
//     $pdf->SetFont('Arial','I',10);
//     $footer_position_y+=2;
//     $pdf->text($footer_position_y,$footer_start_y+3,'Printed By');
//     $pdf->SetFont('Arial','B',10);
//     $pdf->text($footer_position_y,$footer_start_y+8,'');
// }       
header_section($pdf,$Profile,$total_page,$page,$journal_data,$amount,$balance,$debit,$credit,$total,$executive_name);
$pdf->Output();
exit;
}
public function sales_return_printnew($id=null)
{

	$SalesReturn = $this->SalesReturn->find('first', array(
		"joins" => array(
			array(
				"table" => 'customers',
				"alias" => 'Customer',
				"type" => 'inner',
				"conditions" => array('Customer.account_head_id=SalesReturn.account_head_id'),
				),
			array(
				"table" => 'customer_types',
				"alias" => 'CustomerType',
				"type" => 'inner',
				"conditions" => array('CustomerType.id=Customer.customer_type_id'),
				),
			),
		'conditions' => array('SalesReturn.id'=>$id),
		'fields' => array(
			'SalesReturn.*',
			'AccountHead.name',
			'CustomerType.name',
			'Customer.place',
			'Customer.vat_no',
			'Customer.arabic_name',
			'Customer.mobile',
			'Customer.place',
			)
		));
	$name=$SalesReturn['AccountHead']['name'];
	$invoice_no=$SalesReturn['SalesReturn']['invoice_no'];
	$date=$SalesReturn['SalesReturn']['date'];
	$print_field_array=[];
	$print_field_array[0]="Original For Customer";
	$print_field_array[1]="Duplicate";
	$print_field_array[2]="Triplicate For Accounts";
	$Checkstate=0;
// pr($Sale);exit;
	if($SalesReturn['CustomerType']['name'] != "GENERALCUSTOMER")
	{
		$Checkstate=0;
	}
	else
	{
		$Checkstate=1;
	}
	$Checkstate=0;
// if($SalesReturn['AccountHead']['name'] == "GENERALCUSTOMER"){
//   $name=$SalesReturn['Sale']['customer_name'];
// }else{
//   $name=$Sale['AccountHead']['name'];
// }
	$name=$SalesReturn['AccountHead']['name'];
	$invoice_no=$SalesReturn['SalesReturn']['invoice_no'];
	$date=$SalesReturn['SalesReturn']['date'];
	$this->SalesReturnItem->virtualFields = array(
		'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
		);
	$SalesReturnItem = $this->SalesReturnItem->find('all', array(
		'conditions' => array('SalesReturnItem.sales_return_id'=>$id),
		'fields' => array(
			'SalesReturnItem.*',
			'Product.id',
			'Product.name',
			'Product.code',
			'Product.arabic_name',
			'Product.no_of_piece_per_unit',
			)
		));
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}
	foreach ($SalesReturnItem as $key => $value) {
		$description = str_replace('"', "'", $value['Product']['name']);
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$poped_array[]=array_pop($description_words);
			$product_name=implode($description_words,' ');
			$product_name_length=strlen($product_name);
			if($product_name_length<=42)
			{
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}
		if($k)
			$k--;
		$i+=$k;
		if($i+$k>=20)
		{
			$total_page++;
			$i=-1;
		}
		$i++;
	}
	$igst_x_position=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 


	function header_section($pdf,$SalesReturn,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value) 
	{
		$pdf->AddPage();
		$image_file ='profile/'.$Profile['logo'];
//$pdf->Image($image_file, 20, 10, 15, '', 'JPG', '', 'T', false, -550, '', false, false, 0, false, false, false);
		$pdf->Image($image_file,2,1,35);
// $pdf->Image('profile/'.$Profile['logo'],-5,5,-550);
// $pdf->Image('profile/'.$Profile['logo'],2,2,-525);
		$image_line_width=40;
		$invoice_x_starting=$image_line_width;
$pdf->Line($image_line_width, 1,$image_line_width, 40); // vertical line
// //$pdf->SetFont('Arial','B',12);
// $pdf->SetTextColor(0,0,100);
$pdf->rect(1, 1, 208, 295);

// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$invoice_pos+=3;
// $pdf->SetFont('Arial','B',15);
$pdf->SetFont('times', 'B', 8, "", 'false');
$pdf->SetFont('dejavusans', '', 10);

$title="CREDIT NOTE";
$arabic="عائد المبيعات";
$pdf->text(85,$invoice_pos,$title);
//$pdf->SetFont('dejavusans', '', 8);

$pdf->text(120,$invoice_pos,$arabic);


// $pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=2;
$pdf->SetFont('dejavusans', 'B', 10);
$pdf->text(80,$invoice_pos+3,$Profile['company_name']);
$pdf->SetFont('dejavusans', '', 10);

$pdf->text(130,$invoice_pos+3,$Profile['company_name_arabic']);

$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
$pdf->text(78,$invoice_pos+3,$Profile['address_line_1']);
$pdf->text(140,$invoice_pos+3,$Profile['address_line_1_arabic'] );

$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->text(168,33,$field_value);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(60,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
$pdf->text(160,$invoice_pos+3,$Profile['address_line_2_arabic']);

$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=6;
$pdf->SetFont('times', 'U', 12, "", 'false');
$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
$invoice_pos+=10;
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);

$pdf->text(78,$invoice_pos,'VAT Number : '.$Profile['vat_code']);
$VAT_Number='رقم الضريبة';
$pdf->text(140,$invoice_pos,$VAT_Number);
$gst_details_y_staring=40;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring); // horizontal line
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos=$gst_details_y_staring+2;
$gst_line_width=120;
$pdf->Line($gst_line_width+50, $gst_details_y_staring,$gst_line_width+50, $gst_details_y_staring+17+21+5); // vertical line
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);
$pdf->text(2,$gst_pos,'Customer Name:');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(2,$gst_pos+7,'Customer No');
$pdf->text(2,$gst_pos+14,'VAT Number');
$pdf->text(2,$gst_pos+21,'Address');
$pdf->SetFont('dejavusans', '', 10);

$pdf->text($gst_line_width-250,$gst_pos,$name);
$pdf->text($gst_line_width-5,$gst_pos,$SalesReturn['Customer']['arabic_name']);
$pdf->text($gst_line_width-250,$gst_pos+7,$SalesReturn['Customer']['mobile']);
$pdf->text($gst_line_width-250,$gst_pos+14,$SalesReturn['Customer']['vat_no']);
$pdf->text($gst_line_width-250,$gst_pos+21,$SalesReturn['Customer']['place']);
$pdf->text($gst_line_width-285,$gst_pos+28,$SalesReturn['SalesReturn']['invoice_no']);
$pdf->text($gst_line_width-178,$gst_pos+28,$SalesReturn['SalesReturn']['invoice_no']);

$invoice_date=$SalesReturn['SalesReturn']['date'];

$pdf->text($gst_line_width-290,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));
$pdf->text($gst_line_width-185,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));

$pdf->text($gst_line_width+1+68,$gst_pos,'اسم العميل');
$pdf->text($gst_line_width+1+69,$gst_pos+7,'رقم العميل');
$pdf->text($gst_line_width+1+68,$gst_pos+14,'رقم الضريبة');
$pdf->text($gst_line_width+1+76,$gst_pos+21,'عنوان.');
$pdf->text($gst_line_width+1+68,$gst_pos+28,'رقم الفاتورة');
$pdf->text($gst_line_width+1+76,$gst_pos+35,'التاريخ');

//$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos+=28;
$pdf->text(2,$gst_pos,'Invoice Number');
$gst_pos+=7;
$pdf->text(2,$gst_pos,'Date');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->SetXY(0, 5);
$pdf->Line(40, $gst_details_y_staring,40, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos+=3;

$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+20;
$Consignee_lin_width=100+1;
//$pdf->text(2,$customer_detail_startin,'Customer Name : ');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(75,$customer_detail_startin,'اﺳﻢ اﻟﺰﺑﻮﻥ : ');
$pdf->SetFont('times', '', 10, "", 'false');
//$pdf->text($Consignee_lin_width,$customer_detail_startin,'Address :'.$Sale['Customer']['place']);

$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(195,$customer_detail_startin,'ﻋﻨﻮاﻥ :');
$pdf->SetFont('times', '', 10, "", 'false');



$customer_detail_startin+=7;
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(2,$customer_detail_startin-1,$name);
//$pdf->text(60,$customer_detail_startin+3,$Sale['Customer']['arabic_name']);
$pdf->SetFont('times', '', 10, "", 'false');
$customer_detail_startin+=4;
$customer_detail_startin+=4;
$customer_detail_startin+=1;
//$pdf->text(2,$customer_detail_startin,'VAT Number :'.$Sale['Customer']['vat_no']);
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(82,$customer_detail_startin,'ﻭاﺕ ﺭﻗﻢ:');
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+7;
//  table head start
$table_length=135;
$pdf->text(2,$item_table_head_y_start+1,'SL');
$pdf->text(2,$item_table_head_y_start+4,'No');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text(2,$item_table_head_y_start+7, 'رقم');
$pdf->text(1,$item_table_head_y_start+10,'مسلسل');
$table_column=9;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line($table_column+25, $item_table_head_y_start,$table_column+25, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+1,'Item Code');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+5,$item_table_head_y_start+7, 'رقم الصنف ');
$pdf->Line($table_column+5, $item_table_head_y_start,$table_column+5, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+35,$item_table_head_y_start+1,'Description');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+35,$item_table_head_y_start+6,'البيان');
$table_column+=70+$igst_x_position;
$pdf->SetFont('times', '', 10, "", 'false');

$table_column+=16;
if($SalesReturn['SalesReturn']['free_flag']!=0){
$pdf->Line($table_column-17, $item_table_head_y_start,$table_column-17, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column-17,$item_table_head_y_start+1,'Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column-17,$item_table_head_y_start+6,'كمية');
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->Line($table_column-7, $item_table_head_y_start,$table_column-7, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column-7,$item_table_head_y_start+1,'Free Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column-5,$item_table_head_y_start+6,'مجانية');
$pdf->SetFont('times', '', 10, "", 'false');
}
else{
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column,$item_table_head_y_start+1,'Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column,$item_table_head_y_start+6,'كمية');
$pdf->SetFont('times', '', 10, "", 'false');

}
$table_column+=8;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->Line($table_column+8, $item_table_head_y_start,$table_column+8, $item_table_head_y_start+$table_length); // vertical line
//$pdf->text($table_column+1,$item_table_head_y_start+1,'Unit');
$pdf->text($table_column+11,$item_table_head_y_start+1,'Rate');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text($table_column+1,$item_table_head_y_start+6,'وحدة');
$pdf->Text($table_column+11,$item_table_head_y_start+6,'سعر');
$table_column+=25;
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column,$item_table_head_y_start+1,'Tx Value');
$pdf->SetFont('dejavusans', '', 8);
//قيمة الضريبة

$pdf->Text($table_column,$item_table_head_y_start+8,'اﻟﻤﺒﻠﻎ قيمة');
//$pdf->Text($table_column,$item_table_head_y_start+11,'اﺯاﻟﺔ ﻭاﺕ');
$pdf->SetFont('times', '', 10, "", 'false'); 
$table_column+=18.5;
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line



$pdf->Line($table_column, $gst_details_y_staring+17+5+21+6,172+$igst_x_position, $gst_details_y_staring+17+5+21+6); // horizontal line
$pdf->text($table_column+3,$item_table_head_y_start+1,'VAT');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($table_column+11,$item_table_head_y_start+1,'ﺿﺮﻳﺒﺔ');
$pdf->SetFont('times', '', 10, "", 'false'); 

$pdf->text($table_column+1,$item_table_head_y_start+7,'Rate');
$pdf->SetFont('dejavusans', '', 8);

$pdf->Text($table_column+1,$item_table_head_y_start+10,'ﻣﻌﺪﻝ');
$table_column+=14;
$pdf->SetFont('times', '', 10, "", 'false'); 
$pdf->Line($table_column, $item_table_head_y_start+6,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column,$item_table_head_y_start+7,'Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+1,$item_table_head_y_start+10,'ﻛﻤﻴﺔ');
$pdf->SetFont('times', '', 10, "", 'false');

$table_column+=13;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line

$pdf->text($table_column+3,$item_table_head_y_start+1,'Total');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+2,$item_table_head_y_start+4,'ﻣﺠﻤﻮﻉ');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line(1, $gst_details_y_staring+17+5+21+15,209, $gst_details_y_staring+17+5+21+15); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length,209, $item_table_head_y_start+$table_length); // horizontal line
// $pdf->Line(1, $item_table_head_y_start+$table_length+5,209, $item_table_head_y_start+$table_length+5); // horizontal line
//$pdf->text(12,$item_table_head_y_start+$table_length,'Total');
$pdf->SetFont('dejavusans', '', 10);

$pdf->SetFont('times', '', 10, "", 'false');

}

function footer($pdf,$data,$SalesReturn)
{
	$footer_start_y=218;
$pdf->Line(1, $footer_start_y,209, $footer_start_y); // horizontal line
$invoce_word_width=113;
$grand_total_length=30;
$pdf->Line($invoce_word_width, $footer_start_y,$invoce_word_width, $footer_start_y+$grand_total_length+4); // vertical line
//$pdf->text(10,$footer_start_y+1,'Amount In Words');
$pdf->SetFont('dejavusans', '', 8);
// $pdf->Text(85,$footer_start_y+1,'اﻟﻤﺒﻠﻎ ﺑﺎﻟﻜﻠﻤﺎﺕ');
$pdf->SetFont('times', '', 8, "", 'false');
$actual_total=$SalesReturn['SalesReturn']['total']-$data['total_igst_tax'];
$actual_grand_total=$SalesReturn['SalesReturn']['grand_total'];
$convert_number_to_words=strtoupper(convert_number_to_words(floatval($actual_grand_total)));
$convert_number_to_arabic_words=strtoupper(convert_number_to_arabic_words(floatval($actual_grand_total)));
//$pdf->text(5,$footer_start_y+3+10,$convert_number_to_words);
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(40,$footer_start_y+3+10,$convert_number_to_arabic_words);
//$pdf->Text($invoice_lin_width+130,$customer_detail_startin+4+35,strtoupper(convert_number_to_arabic_words($amount)).' '.'الريال فقط' );
$pdf->Line($invoce_word_width+46, $footer_start_y,$invoce_word_width+46, $footer_start_y+$grand_total_length); // vertical line
$Total_pos=1;
$pdf->text($invoce_word_width,$footer_start_y+$Total_pos,'Total Amount');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+22,$footer_start_y+$Total_pos,'اجمالى القيمة');
$pdf->SetFont('times', '', 8, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,number_format($data['total'],2));

$Total_pos+=5;
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+2,'VAT Amount');

$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+28,$footer_start_y+$Total_pos+2,'قيمة الضريبة');
$pdf->SetFont('times', '', 8, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos+2,number_format($data['total_igst_tax'],2));
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;

$pdf->SetFont('times', '', 8, "", 'false');
//   //$other_value=round($data['total'])-$data['total'];
$other_value=0;
$Total_pos+=5;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+5,'Net Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($invoce_word_width+23,$footer_start_y+$Total_pos+5,'المبلغ الصافى');
$pdf->SetFont('times', '', 12, "", 'false');

$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos+5,number_format($actual_grand_total,2));
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
$tax_subject_y=$footer_start_y+$grand_total_length;
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->Line(1, $tax_subject_y+4,209, $tax_subject_y+4); // horizontal line
$pdf->text(2,$tax_subject_y+5,'Received By');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(85,$tax_subject_y+5,'المستلم');
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->text(115,$tax_subject_y+5,'Salesman');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(190,$tax_subject_y+5,'البائغ');البائغ:
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->Line($invoce_word_width+46, $tax_subject_y,$invoce_word_width+46, $tax_subject_y+4); // vertical line
$signature_area_y=$tax_subject_y+4;
$signature_length=14;
$signature_width=113;

$pdf->SetFont('dejavusans', '', 8);


$pdf->SetFont('times', '', 8, "", 'false');
$pdf->SetXY($signature_width, $signature_area_y);

$pdf->Line($signature_width, $signature_area_y,$signature_width, $signature_area_y+$signature_length); // vertical line
$pdf->Line(1, $signature_area_y+$signature_length,209, $signature_area_y+$signature_length); // horizontal line

}
$page=0;


foreach($print_field_array as $field_key=>$field_value)
{
//$page=1;
	$page++;
	header_section($pdf,$SalesReturn,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value);
	$total=0;
	$total_cgst_tax=0;
	$total_sgst_tax=0;
	$total_igst_tax=0;
	$item_table_head_y_start='85';
	$table_length='128';
	$cgst_grand_amount=0;
	$sgst_grand_amount=0;
	$igst_grand_amount=0;
	$rate_grand_amount=0;
	$discount_grand_amount=0;
	$taxable_value_grand_amount=0;
	$product_grand_total=0;
	$count=count($SalesReturnItem);
	$i=0;
// for ($i=0; $i <$count ; $i++) { 
	$data=array(
		'total'=>$total,
		'total_cgst_tax'=>$total_cgst_tax,
		'total_sgst_tax'=>$total_sgst_tax,
		'total_igst_tax'=>$total_igst_tax,
		);

	foreach ($SalesReturnItem as $keySI => $value) {
		$description = str_replace('"', "'", $value['SalesReturnItem']['product_name']);
// $hsn_code=$value['Product']['hsn_code'];
		$qty=floatval($value['SalesReturnItem']['quantity']);
		$free_qty=floatval($value['SalesReturnItem']['free_qty']);
		$unit_level=$this->Unit->findByName($value['SalesReturnItem']['quantity_mode']);
		if(empty($unit_level)){
			$unit_level['Unit']['level'] =1;
		}
		if($unit_level['Unit']['level'] !=1){
			$value['SalesReturnItem']['quantity']=$value['SalesReturnItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
			$qty = $value['SalesReturnItem']['quantity'];
			$value['SalesReturnItem']['free_qty']=$value['SalesReturnItem']['free_qty']/$value['Product']['no_of_piece_per_unit'];

			$free_qty = $value['SalesReturnItem']['free_qty'];
		}
		$uom=$value['SalesReturnItem']['quantity_mode'];

// if($value['SalesReturnItem']['quantity_mode']==1)
// {
// 	$unit="Piece";
// $rate=floatval($value['SalesReturnItem']['unit_price']);
// $qty=$value['SalesReturnItem']['quantity'];
// }
// else
// {
// 	$unit="Case";
// 	$qty=$value['SalesReturnItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
// 	$rate=$value['SalesReturnItem']['unit_price']*$value['Product']['no_of_piece_per_unit'];


// }
		$rate=floatval($value['SalesReturnItem']['unit_price']);
		$qty=$value['SalesReturnItem']['quantity'];
		$rate_grand_amount+=$rate;
		$discount=0;

		$discount_grand_amount+=$discount;
		$taxable_value=floatval($value['SalesReturnItem']['net_value']);
		$taxable_value_grand_amount+=$taxable_value;
		$net_value=floatval($value['SalesReturnItem']['net_value']);

		$tax_value=floatval($value['SalesReturnItem']['tax_amount']);
		$total+=$net_value;

		$data['total']+=floatval($net_value);
		$data['total_igst_tax']+=floatval($value['SalesReturnItem']['tax_amount']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$product_total=floatval($value['SalesReturnItem']['total']);
		$product_grand_total+=$product_total;
		$igst_rate=floatval($value['SalesReturnItem']['tax']);
		$igst_amount=floatval($value['SalesReturnItem']['tax_amount']);
		$igst_grand_amount+=$igst_amount;
		$item_pos=2;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$keySI+1);
		$item_pos+=6;
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$product_name=implode($description_words,' ');
			$poped_array[]=array_pop($description_words);
			$product_name_length=strlen($product_name);
			if($product_name_length<=40)
			{ 
				$pdf->SetFont('dejavusans', '', 8);

				$pdf->text($item_pos+8,$item_table_head_y_start+8+(($i+$k)*8)+5,$value['Product']['code']);
				$arabic=$value['Product']['name'];
				$pdf->text($item_pos+26,$item_table_head_y_start+8+(($i+$k)*8)+5,$arabic);
				$pdf->text($item_pos+26,$item_table_head_y_start+8+(($i+$k)*8)+8,$value['Product']['arabic_name']);
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}

		if($k)
			$k--;
		$item_pos+=1;
		$pdf->SetFont('dejavusans', '', 8);
		$pdf->SetFont('times', '', 8, "", 'false');
		$item_pos+=70+$igst_x_position;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$hsn_code);
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,'');
		$item_pos+=16;
		if($SalesReturn['SalesReturn']['free_flag']!=0){
			$pdf->text($item_pos-17,$item_table_head_y_start+8+($i*8)+5,$qty);
			$pdf->text($item_pos-7,$item_table_head_y_start+8+($i*8)+5,$free_qty);
		}else{
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$qty);
		}
// $pdf->text($item_pos+8,$item_table_head_y_start+8+($i*8)+5,$unit);
		$item_pos+=20;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($rate,2));

		$item_pos+=12.5;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($taxable_value,2));
		$item_pos+=21;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$igst_rate.'%');
		$item_pos+=12;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($igst_amount,2));

		$item_pos+=13;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($product_total,2));

		$i+=$k;
		$count+=$k;
		if($i+$k>=13)
		{
			footer($pdf,$data,$SalesReturn);
			$page++;
			header_section($pdf,$SalesReturn,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value);
			$i=-1;
			$count-=13;
		}
		$i++;
	}

	footer($pdf,$data,$SalesReturn);
}
$pdf->Output();
exit;

}
public function debitnote($id=null)
{
	$DebitNote = $this->DebitNote->find('first', array(
		"joins" => array(
			array(
				"table" => 'parties',
				"alias" => 'Party',
				"type" => 'inner',
				"conditions" => array('Party.account_head_id=DebitNote.account_head_id'),
				),
			),
		'conditions' => array('DebitNote.id'=>$id),
		'fields' => array(
			'DebitNote.*',
			'AccountHead.id',
			'AccountHead.name',
			'Party.place',
			'Party.code',
// 'Customer.arabic_name',
// 'Customer.mobile',
// 'Customer.place',
// 'Customer.route_id',
// 'Customer.code',
			)
		));
	$DebitDetail=$this->DebitDetail->find('all',array(
		"joins" => array(),
		'conditions'=>array(
			'debit_id'=>$id,

			),
		'fields'=>array(
//'DebitDetail.*',

			)
		));
	$Checkstate=0;
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	$igst_x_position=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 
	function header_section($pdf,$Profile,$DebitNote) 
	{
		$pdf->AddPage();
		$pdf->Image('profile/'.$Profile['logo'],8,13,30);
		$image_line_width=45;
		$invoice_x_starting=$image_line_width;
//$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(0,0,0);
		$pdf->rect(8, 9, 190,230);
		$item_table_head_y_start=10;
		$pdf->SetDrawColor('150','150','150');
		$pdf->Line($image_line_width, $item_table_head_y_start,$image_line_width, $item_table_head_y_start+40);
		$invoice_pos=0;
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);


		$invoice_pos+=2;
		$pdf->text(78,$invoice_pos+3,$Profile['company_name']);
		$pdf->text(155,$invoice_pos+3,$Profile['company_name_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
		$pdf->text(70,$invoice_pos+3,$Profile['address_line_1']);
		$pdf->text(140,$invoice_pos+3,$Profile['address_line_1_arabic'] );

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(46,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
		$pdf->text(143,$invoice_pos+3,$Profile['address_line_2_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('times', 'U', 10, "", 'false');
		$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(78,$invoice_pos,'VAT Number : '.$Profile['vat_code']);
		$VAT_Number='ظريبه الشراء';
		$pdf->text(140,$invoice_pos,$VAT_Number);
		$gst_details_y_staring=50;
$pdf->Line(8, $gst_details_y_staring,198, $gst_details_y_staring); // horizontal line
$pdf->rect(75, $gst_details_y_staring+5, 35,10);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(80,$invoice_pos+18,"Debit Note");
$pdf->text(10,$invoice_pos+31,"Party :");$pdf->text(40,$invoice_pos+31,$DebitNote['Party']['code']." ".$DebitNote['AccountHead']['name']);
$pdf->text(10,$invoice_pos+39,"Date :");$pdf->text(40,$invoice_pos+39,date('d-m-Y',strtotime($DebitNote['DebitNote']['date'])));
$pdf->text(160,$invoice_pos+31,"Debit No :");$pdf->text(180,$invoice_pos+31,$DebitNote['DebitNote']['debit_no']);
$pdf->Line(8, $gst_details_y_staring+40,198, $gst_details_y_staring+40);
$Row_length=95;
$table_length=130;
$table_column=30;
$pdf->SetFont('dejavusans', '', 8);

$pdf->text(60,$Row_length,'Description');
$pdf->Line($table_column+85, 90,$table_column+85, $table_length+90); // vertical line
$pdf->text(160,$Row_length,'Amount');
$pdf->Line(8, $gst_details_y_staring+55,198, $gst_details_y_staring+55);
$pdf->Line(8, $gst_details_y_staring+$table_length+30,198, $gst_details_y_staring+$table_length+30);
$pdf->Line(8, $gst_details_y_staring+$table_length+40,198, $gst_details_y_staring+$table_length+40);


$pdf->SetFont('times', '', 8, "", 'false');
}
function footer($pdf,$DebitNote){
	$footer_start_y=213;
	$pdf->SetFont('times', 'B', 8, "", 'false');
	$pdf->text(85,$footer_start_y,'Total');
	$pdf->text(130,$footer_start_y,floatval($DebitNote['DebitNote']['total']));

}
header_section($pdf,$Profile,$DebitNote,$DebitNote);
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+90;
foreach($DebitDetail as $key=>$value) {
	$sl_no_v_x=25;
	$pdf->SetFont('times', '', 8, "", 'false');
	$pdf->Text($sl_no_v_x, $first_table_x+11+(9*$i), $value['DebitDetail']['description']);
	$Sub_refernce=$sl_no_v_x+100;
	$pdf->Text($Sub_refernce, $first_table_x+11+(9*$i), floatval($value['DebitDetail']['amount']));

//$unit_v_x=$debit+20;
//$quantity_v_x=$credit+20;
//$net_amount_v_x=$balance_x+20;
	$i++;
	if($i>10)
	{
		$i=0;
		footer($pdf,$DebitNote);
		header_section($pdf,$Profile,$DebitNote);
	}
}
footer($pdf,$DebitNote);
$pdf->Output();
exit;

}
public function creditnote($id=null)
{
	$CreditNote = $this->CreditNote->find('first', array(
		"joins" => array(
			array(
				"table" => 'customers',
				"alias" => 'Customer',
				"type" => 'inner',
				"conditions" => array('Customer.account_head_id=CreditNote.account_head_id'),
				),
			array(
				"table" => 'customer_types',
				"alias" => 'CustomerType',
				"type" => 'inner',
				"conditions" => array('CustomerType.id=Customer.customer_type_id'),
				),
			),
		'conditions' => array('CreditNote.id'=>$id),
		'fields' => array(
			'CreditNote.*',
			'AccountHead.id',
			'AccountHead.name',
			'CustomerType.name',
			'Customer.place',
			'Customer.vat_no',
			'Customer.arabic_name',
			'Customer.mobile',
			'Customer.place',
			'Customer.route_id',
			'Customer.code',
			)
		));
//pr($CreditNote);exit;
	$CreditDetail=$this->CreditDetail->find('all',array(
		"joins" => array(),
		'conditions'=>array(
			'credit_id'=>$id,

			),
		'fields'=>array(
//'CreditDetail.*',

			)
		));
	$Checkstate=0;
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	$igst_x_position=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 
	function header_section($pdf,$Profile,$CreditNote) 
	{
		$pdf->AddPage();
		$pdf->Image('profile/'.$Profile['logo'],8,13,30);
		$image_line_width=45;
		$invoice_x_starting=$image_line_width;
//$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(0,0,0);
		$pdf->rect(8, 9, 190,230);
		$item_table_head_y_start=10;
		$pdf->SetDrawColor('150','150','150');
		$pdf->Line($image_line_width-4, $item_table_head_y_start,$image_line_width-4, $item_table_head_y_start+40);
		$invoice_pos=0;
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);


		$invoice_pos+=2;
		$pdf->text(40,$invoice_pos+3,$Profile['company_name']);
		$pdf->text(130,$invoice_pos+3,$Profile['company_name_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
		$pdf->text(43,$invoice_pos+3,$Profile['address_line_1']);
		$pdf->text(140,$invoice_pos+3,$Profile['address_line_1_arabic'] );

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(40,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
		$pdf->text(136,$invoice_pos+3,$Profile['address_line_2_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('times', 'U', 10, "", 'false');
		$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(75,$invoice_pos,'VAT Number : '.$Profile['vat_code']);
		$VAT_Number='ظريبه الشراء';
		$pdf->text(140,$invoice_pos,$VAT_Number);
		$gst_details_y_staring=50;
$pdf->Line(8, $gst_details_y_staring,198, $gst_details_y_staring); // horizontal line
$pdf->rect(90, $gst_details_y_staring+5, 35,10);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text(95,$invoice_pos+18,"Credit Note");
$pdf->text(10,$invoice_pos+31,"Customer :");$pdf->text(40,$invoice_pos+31,$CreditNote['Customer']['code']." ".$CreditNote['AccountHead']['name']);
$pdf->text(10,$invoice_pos+39,"Date :");$pdf->text(40,$invoice_pos+39,date('d-m-Y',strtotime($CreditNote['CreditNote']['date'])));
$pdf->text(160,$invoice_pos+31,"Credit No :");$pdf->text(180,$invoice_pos+31,$CreditNote['CreditNote']['credit_no']);
$pdf->Line(8, $gst_details_y_staring+40,198, $gst_details_y_staring+40);
$Row_length=95;
$table_length=130;
$table_column=30;
$pdf->SetFont('dejavusans', '', 8);

$pdf->text(60,$Row_length,'Description');
$pdf->Line($table_column+85, 90,$table_column+85, $table_length+90); // vertical line
$pdf->text(160,$Row_length,'Amount');
$pdf->Line(8, $gst_details_y_staring+55,198, $gst_details_y_staring+55);
$pdf->Line(8, $gst_details_y_staring+$table_length+30,198, $gst_details_y_staring+$table_length+30);
$pdf->Line(8, $gst_details_y_staring+$table_length+40,198, $gst_details_y_staring+$table_length+40);


$pdf->SetFont('times', '', 8, "", 'false');
}
function footer($pdf,$CreditNote){
	$footer_start_y=213;
	$pdf->SetFont('times', 'B', 8, "", 'false');
	$pdf->text(85,$footer_start_y,'Total');
	$pdf->text(130,$footer_start_y,floatval($CreditNote['CreditNote']['total']));

}
header_section($pdf,$Profile,$CreditNote,$CreditNote);
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+90;
foreach($CreditDetail as $key=>$value) {
	$sl_no_v_x=25;
	$pdf->SetFont('times', '', 8, "", 'false');
	$pdf->Text($sl_no_v_x, $first_table_x+11+(9*$i), $value['CreditDetail']['description']);
	$Sub_refernce=$sl_no_v_x+100;
	$pdf->Text($Sub_refernce, $first_table_x+11+(9*$i), floatval($value['CreditDetail']['amount']));

//$unit_v_x=$debit+20;
//$quantity_v_x=$credit+20;
//$net_amount_v_x=$balance_x+20;
	$i++;
	if($i>10)
	{
		$i=0;
		footer($pdf,$CreditNote);
		header_section($pdf,$Profile,$CreditNote);
	}
}
footer($pdf,$CreditNote);
$pdf->Output();
exit;

}
public function fpdf($id=null)
{

	$Sale = $this->Sale->find('first', array(
		"joins" => array(
			array(
				"table" => 'customers',
				"alias" => 'Customer',
				"type" => 'inner',
				"conditions" => array('Customer.account_head_id=Sale.account_head_id'),
				),
			array(
				"table" => 'customer_types',
				"alias" => 'CustomerType',
				"type" => 'inner',
				"conditions" => array('CustomerType.id=Customer.customer_type_id'),
				),
			),
		'conditions' => array('Sale.id'=>$id),
		'fields' => array(
			'Sale.*',
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			'CustomerType.name',
			'Customer.place',
			'Customer.vat_no',
			'Customer.arabic_name',
			'Customer.mobile',
			'Customer.place',
			'Customer.route_id',
			'Customer.state_id',
			)
		));
	$acc_id=$Sale['Customer']['route_id'];
	$route_mobile=$this->Route->findById($acc_id);
	$route_mobile_no='';
	if(!empty($route_mobile))
		$route_mobile_no=$route_mobile['Route']['mobile'];
	$name=$Sale['AccountHead']['name'];
	$invoice_no=$Sale['Sale']['invoice_no'];
	$date=$Sale['Sale']['date_of_delivered'];
	$print_field_array=[];
	$print_field_array[0]="Original For Customer";
	$print_field_array[1]="Duplicate For Accounts";
	// $print_field_array[2]="Triplicate For Accounts";
	$Checkstate=0;
	if($Sale['CustomerType']['name'] != "GENERALCUSTOMER")
	{
		$Checkstate=0;
	}
	else
	{
		$Checkstate=1;
	}
	$Checkstate=0;
	$name=$Sale['AccountHead']['name'];
	$invoice_no=$Sale['Sale']['invoice_no'];
	$AccountingsController= new AccountingsController;
	$account_head_total_debit=$Sale['AccountHead']['opening_balance'];
	$account_head_total_credit=0;
	$Debit_N_Credit_function=$AccountingsController->General_Journal_Debit_N_Credit_function($Sale['AccountHead']['id']);
	$account_head_total_debit+=round($Debit_N_Credit_function['debit'],3);
	$account_head_total_credit+=round($Debit_N_Credit_function['credit'],3);
	$account_head_total_balance=$account_head_total_debit-$account_head_total_credit;
	$previos_balance=$account_head_total_balance;
	$sale_balance=$Sale['Sale']['balance'];
	$previos_balance-=$sale_balance;
	$date=$Sale['Sale']['date_of_delivered'];
	$this->SaleItem->virtualFields = array(
		'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
		);
	$SaleItem = $this->SaleItem->find('all', array(
		'conditions' => array('SaleItem.sale_id'=>$id),
		'fields' => array(
			'SaleItem.*',
			'Product.id',
			'Product.name',
			'Product.code',
			'Product.arabic_name',
			'Product.no_of_piece_per_unit',
			'Unit.*',
			)
		));
// foreach ($SaleItem as $key => $value) {
// 	$free_qty_id=$value['SaleItem']['free_qty'];
// 	# code...
// }
//pr($free_qty_id);
//exit;
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}
	foreach ($SaleItem as $key => $value) {
		$description = str_replace('"', "'", $value['Product']['name']);
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$poped_array[]=array_pop($description_words);
			$product_name=implode($description_words,' ');
			$product_name_length=strlen($product_name);
			if($product_name_length<=42)
			{
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}
		if($k)
			$k--;
		$i+=$k;
		if($i+$k>=20)
		{
			$total_page++;
			$i=-1;
		}
		$i++;
	}
	$igst_x_position=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 


	function header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value,$route_mobile_no) 
	{
		$pdf->AddPage();
		$image_file ='profile/'.$Profile['logo'];
//$pdf->Image($image_file, 20, 10, 15, '', 'JPG', '', 'T', false, -550, '', false, false, 0, false, false, false);
		$pdf->Image($image_file,2,1,35);
// $pdf->Image('profile/'.$Profile['logo'],-5,5,-550);
// $pdf->Image('profile/'.$Profile['logo'],2,2,-525);
		$image_line_width=40;
		$invoice_x_starting=$image_line_width;
$pdf->Line($image_line_width, 1,$image_line_width, 40); // vertical line
// //$pdf->SetFont('Arial','B',12);
// $pdf->SetTextColor(0,0,100);
$pdf->rect(1, 1, 208, 295);

// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$invoice_pos+=3;
// $pdf->SetFont('Arial','B',15);
$pdf->SetFont('times', 'B', 8, "", 'false');
$pdf->SetFont('dejavusans', '', 10);
$title="TAX INVOICE";
$arabic="فاتورة ضريبية";
if($Sale['Sale']['sale_type1']=="CashSale"){
	// $title="CASH INVOICE";
	// $arabic="الفاتورة النقدية";} 
	// else{$title="CREDIT INVOICE";
	// $arabic="فاتورة ائتمانية";
}
	$pdf->text(98,$invoice_pos,$title);
	$invoice_pos+=2;
	$pdf->SetFont('dejavusans', 'B', 10);
	$pdf->text(92,$invoice_pos+3,$Profile['company_name']);
	$pdf->SetFont('dejavusans', '', 10);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->SetFont('dejavusans', '', 10);
	$pdf->text(83,$invoice_pos+3,$Profile['address_line_1']);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->text(165,33,$field_value);
	$pdf->SetFont('dejavusans', '', 10);
	$pdf->text(88,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->SetFont('times', 'U', 12, "", 'false');
	$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
	$invoice_pos+=10;
	$pdf->SetFont('dejavusans', '', 10);
	$pdf->text(83,$invoice_pos,'GST Number : '.$Profile['vat_code']);
	$gst_details_y_staring=40;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring); // horizontal line
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos=$gst_details_y_staring+2;
$gst_line_width=45;
//$pdf->Line($gst_line_width+50, $gst_details_y_staring,$gst_line_width+50, $gst_details_y_staring+17+21+5); // vertical line
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);
$pdf->text(2,$gst_pos,'Customer Name:');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(2,$gst_pos+7,'Customer No');
$pdf->text(2,$gst_pos+14,'GST Number');
$pdf->text(2,$gst_pos+21,'Address');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetXY($invoice_x_starting, 5);
//$pdf->Cell($gst_line_width,$gst_pos+37,$name,0,0,'C');
$pdf->text($gst_line_width,$gst_pos,$name);
//$pdf->text($gst_line_width-5,$gst_pos,$Sale['Customer']['arabic_name']);
$pdf->text($gst_line_width,$gst_pos+7,$Sale['Customer']['mobile']);
$pdf->text($gst_line_width,$gst_pos+14,$Sale['Customer']['vat_no']);
$pdf->text($gst_line_width,$gst_pos+21,$Sale['Customer']['place']);
$pdf->text($gst_line_width,$gst_pos+28,$Sale['Sale']['invoice_no']);

if(!$Sale['Sale']['date_of_delivered'])
	$invoice_date=$Sale['Sale']['date_of_order'];
else
	$invoice_date=$Sale['Sale']['date_of_delivered'];

$pdf->text($gst_line_width,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));

$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos+=28;
$pdf->text(2,$gst_pos,'Invoice Number');
$gst_pos+=7;
$pdf->text(2,$gst_pos,'Date');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->SetXY(0, 5);
$pdf->Line(40, $gst_details_y_staring,40, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos+=3;

$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+36;
$Consignee_lin_width=100+1;

$pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+7;
//  table head start
$table_length=135-10;
$pdf->text(2,$item_table_head_y_start+1,'SL');
$pdf->text(2,$item_table_head_y_start+4,'No');
$pdf->SetFont('dejavusans', '', 10);
$table_column=9;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line($table_column+5, $item_table_head_y_start,$table_column+5, $item_table_head_y_start+$table_length); // vertical line

// $pdf->text($table_column+5,$item_table_head_y_start+1,'Item Code');
// $pdf->SetFont('dejavusans', '', 10);
// $pdf->Text($table_column+5,$item_table_head_y_start+7, 'رقم الصنف ');
// $pdf->Line($table_column+25, $item_table_head_y_start,$table_column+25, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+15,$item_table_head_y_start+1,'Description');
$pdf->SetFont('dejavusans', '', 10);
$table_column+=70-18+$igst_x_position;
$pdf->SetFont('times', '', 10, "", 'false');
$table_column+=16;
if($Sale['Sale']['free_flag']!=0){

$pdf->Line($table_column+20, $item_table_head_y_start,$table_column+20, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+11,$item_table_head_y_start+1,'Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->Line($table_column-7, $item_table_head_y_start,$table_column-7, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column-7,$item_table_head_y_start+1,'Free Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
}
else{

$pdf->Line($table_column+20, $item_table_head_y_start,$table_column+20, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+11,$item_table_head_y_start+1,'Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');

}
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->Line($table_column+28, $item_table_head_y_start,$table_column+28, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+15,$item_table_head_y_start+1,'Unit');
$pdf->text($table_column+30,$item_table_head_y_start+1,'Rate');

$pdf->SetFont('dejavusans', '', 10);
$table_column+=12+3;
$pdf->Line($table_column+23, $item_table_head_y_start,$table_column+23, $item_table_head_y_start+$table_length); // vertical line
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($table_column+24,$item_table_head_y_start+1,'Amount');
$pdf->SetFont('dejavusans', '', 10);

$table_column+=25;
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->Line($table_column+15, $item_table_head_y_start,$table_column+15, $item_table_head_y_start+$table_length); // vertical line
// $table_column+=2;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
//$pdf->text($table_column+1,$item_table_head_y_start+6,'Discount');
//$table_column+=14;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column+26,$item_table_head_y_start+1,'Taxable Amt');
//$pdf->text($table_column,$item_table_head_y_start+4,'Excl VAT');

$pdf->SetFont('dejavusans', '', 8);
//قيمة الضريبة

//$pdf->Text($table_column,$item_table_head_y_start+11,'اﺯاﻟﺔ ﻭاﺕ');
$pdf->SetFont('times', '', 10, "", 'false'); 
$table_column+=18.5;
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
//$pdf->Line($table_column+1.5, $item_table_head_y_start,$table_column+1.5, $item_table_head_y_start+$table_length); // vertical line

if($Checkstate==1)
{

$pdf->Line($table_column+1.5, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->text($table_column+6,$item_table_head_y_start+3,'CGST');
$pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
$table_column+=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+6,$item_table_head_y_start+3,'SGST');
$pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
}
else
{

//$pdf->Line($table_column+1.5, $gst_details_y_staring+17+5+21+6,175.5-3+$igst_x_position, $gst_details_y_staring+17+5+21+6); // horizontal line
$pdf->text($table_column-4,$item_table_head_y_start+1,'Tax%');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetFont('times', '', 10, "", 'false'); 

//$pdf->text($table_column+1,$item_table_head_y_start+7,'Rate');
$pdf->SetFont('dejavusans', '', 8);

$table_column+=14;
$pdf->SetFont('times', '', 10, "", 'false'); 
$pdf->Line($table_column-8, $item_table_head_y_start,$table_column-8, $item_table_head_y_start+$table_length); // vertical line
//$pdf->text($table_column+1,$item_table_head_y_start+7,'Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
}
$table_column+=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line

$pdf->text($table_column,$item_table_head_y_start+1,'Tax Amt');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line(1, $gst_details_y_staring+17+5+21+15,209, $gst_details_y_staring+17+5+21+15); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length,209, $item_table_head_y_start+$table_length); // horizontal line
// $pdf->Line(1, $item_table_head_y_start+$table_length+5,209, $item_table_head_y_start+$table_length+5); // horizontal line
//$pdf->text(12,$item_table_head_y_start+$table_length,'Total');
$pdf->SetFont('dejavusans', '', 10);

$pdf->SetFont('times', '', 10, "", 'false');

}

function footer($pdf,$data,$Sale,$route_mobile_no,$previos_balance)
{
	$footer_start_y=208;
	$pdf->SetFont('dejavusans', '', 8);
	$state_id=$Sale['Customer']['state_id'];
	$pdf->text(25,$footer_start_y-5,'Total');
$pdf->Line(1, $footer_start_y-5.5,209, $footer_start_y-5.5); // horizontal line
$invoce_word_width=115;
$grand_total_length=30;
$pdf->Line($invoce_word_width, $footer_start_y,$invoce_word_width, $footer_start_y+$grand_total_length-6); // vertical line
//$pdf->text(10,$footer_start_y+1,'Amount In Words');
$pdf->SetFont('dejavusans', '', 8);
// $pdf->Text(85,$footer_start_y+1,'اﻟﻤﺒﻠﻎ ﺑﺎﻟﻜﻠﻤﺎﺕ');
$pdf->SetFont('times', '', 8, "", 'false');
//$actual_total=$Sale['Sale']['total']-$data['total_cgst_tax']-$data['total_sgst_tax']-$data['total_igst_tax'];
if($Sale['Sale']['taxable_amount']==0)
	$Sale['Sale']['taxable_amount']=$data['total'];
if($Sale['Sale']['tax_amount']==0)
	$Sale['Sale']['tax_amount']=$data['total_igst_tax'];

$total_tax_amount=$Sale['Sale']['tax_amount'];
$total_taxable_amount=$Sale['Sale']['taxable_amount'];
$actual_total=$Sale['Sale']['total'];
$actual_grand_total=$Sale['Sale']['grand_total'];
$convert_number_to_words=strtoupper(convert_number_to_words(floatval($actual_grand_total)));
$convert_number_to_arabic_words=strtoupper(convert_number_to_arabic_words(floatval($actual_grand_total)));
//$pdf->text(5,$footer_start_y+3+10,$convert_number_to_words);
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(40,$footer_start_y+3+10,$convert_number_to_arabic_words);
//$pdf->Text($invoice_lin_width+130,$customer_detail_startin+4+35,strtoupper(convert_number_to_arabic_words($amount)).' '.'الريال فقط' );

$pdf->Line($invoce_word_width+46+15, $footer_start_y,$invoce_word_width+46+15, $footer_start_y+$grand_total_length-6); // vertical line
$pdf->Line($invoce_word_width, $footer_start_y+5,209, $footer_start_y+5); // horizontal line
$pdf->Line($invoce_word_width, $footer_start_y+10,209, $footer_start_y+10); // horizontal line
$pdf->Line($invoce_word_width, $footer_start_y+15,209, $footer_start_y+15); // horizontal line

$pdf->Line($invoce_word_width, $footer_start_y+22,209, $footer_start_y+22); // horizontal line
$pdf->text($invoce_word_width,$footer_start_y+1,'SGST');
	$pdf->text($invoce_word_width,$footer_start_y+6,'CGST');
		$pdf->text($invoce_word_width,$footer_start_y+11,'IGST');

$pdf->text($invoce_word_width+1,$footer_start_y+16,'Total Amount');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetFont('times', '', 9, "", 'false');
$total_amount=$Sale['Sale']['taxable_amount']+$Sale['Sale']['discount_amount'];
$rtl_x_pos=170;
$pdf->setRTL(true);

if($state_id==19)
{
$tax_amount_split=floatval($total_tax_amount)/2;
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+1,round($tax_amount_split,3));
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+6,round($tax_amount_split,3));
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+11,0);
}
else
{
$tax_amount_split=floatval($total_tax_amount);
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+1,0);
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+6,0);
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+11,round($tax_amount_split,3));
}

$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+16,number_format($actual_total,3,'.',''));
$pdf->setRTL(false);
$footer_start_y+=13;

$Total_pos=1;
$pdf->text($invoce_word_width+1,$footer_start_y+10,'Discount');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetFont('times', '', 9, "", 'false');
$other_value=$Sale['Sale']['other_value'];
$total_discount=$Sale['Sale']['discount_amount'];
$pdf->setRTL(true);
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+10,number_format($total_discount,3,'.',''));
$pdf->setRTL(false);


$Total_pos+=10;
//$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
//$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos,'Taxable Amount');
//$pdf->SetFont('dejavusans', '', 8);
//$pdf->Text($invoce_word_width+49,$footer_start_y+$Total_pos+1,'الخصم');
//$pdf->SetFont('times', '', 9, "", 'false');
//   //$other_value=round($data['total'])-$data['total'];
//$pdf->setRTL(true);
//$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos,number_format($total_taxable_amount,3,'.',''));
//$pdf->setRTL(false);
//$Total_pos+=5;
//$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
//$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+1,'VAT Amount');

//$pdf->SetFont('dejavusans', '', 8);
//$pdf->Text($invoce_word_width+42,$footer_start_y+$Total_pos+1,'قيمة الضريبة');
// $pdf->SetFont('times', '', 9, "", 'false');
// $pdf->setRTL(true);
// $pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+0.8,number_format($total_tax_amount,3,'.',''));
// $pdf->setRTL(false);
$other_value_y=0;
$vertical_line_extend=$footer_start_y+16;
if($Sale['Sale']['other_value']!=0 && $Sale['Sale']['other_value']!=null)
{
	$other_value_y+=6;
	$Total_pos+=5;
	$pdf->Line($invoce_word_width, $vertical_line_extend,$invoce_word_width, $vertical_line_extend+12); // vertical line
	$pdf->Line($invoce_word_width+46+15, $vertical_line_extend,$invoce_word_width+46+15, $vertical_line_extend+12); // vertical line
	$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
	$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+1,'Total');
	$pdf->SetFont('dejavusans', '', 8);
	$pdf->SetFont('times', '', 9, "", 'false');
	$pdf->setRTL(true);
	$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+1,number_format($actual_total,3,'.',''));
	$pdf->setRTL(false);
	$other_value_y+=6;
	$Total_pos+=5;
	
	$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
	$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+1,'Round Off');
	$pdf->SetFont('dejavusans', '', 8);
	$pdf->SetFont('times', '', 9, "", 'false');
	$pdf->setRTL(true);
	$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+1,number_format($Sale['Sale']['other_value'],3,'.',''));
	$pdf->setRTL(false);

}
$Total_pos+=0;
$vertical_line_extend+=$other_value_y;
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+4,209, $footer_start_y+$Total_pos+4); // horizontal line
$pdf->Line($invoce_word_width, $vertical_line_extend-5,$invoce_word_width, $vertical_line_extend+7); // vertical line
$pdf->Line($invoce_word_width+46+15, $vertical_line_extend-5,$invoce_word_width+46+15, $vertical_line_extend+7); // vertical line

$pdf->SetFont('times', '', 9.5, "", 'false');
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+4,'Bill Amount');
$pdf->SetFont('dejavusans', '', 9.5);
$pdf->SetFont('times', '', 12, "", 'false');
$pdf->setRTL(true);
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+4,number_format($actual_grand_total,3,'.',''));
$pdf->setRTL(false);
$tax_subject_y=$footer_start_y+$grand_total_length-7+$other_value_y;

$pdf->SetFont('times', '', 8, "", 'false');

$footer_start_y+=1.5;
if($Sale['Sale']['account_head_id']!=2)
{
	$tax_subject_y+=1;
	$pdf->Line($invoce_word_width, $tax_subject_y-3,209, $tax_subject_y-3); // horizontal line
	$tax_subject_y+=6;
	$footer_start_y+=6;
	$vertical_line_extend+=7;
	$pdf->Line($invoce_word_width, $vertical_line_extend,$invoce_word_width, $vertical_line_extend+12+1); // vertical line
	$pdf->Line($invoce_word_width+46+15, $vertical_line_extend,$invoce_word_width+46+15, $vertical_line_extend+12+1); // vertical line
	$pdf->SetFont('times', '', 9.5, "", 'false');
	$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+3,'Previous Balance');
	$pdf->SetFont('dejavusans', '', 9.5);
	//$previos_balance=0;
	$pdf->setRTL(true);
	$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+3,number_format($previos_balance,3,'.',''));
	$pdf->setRTL(false);
	$pdf->Line($invoce_word_width, $tax_subject_y-2,209, $tax_subject_y-2); // horizontal line
	$tax_subject_y+=6;
	$footer_start_y+=6;
	$pdf->SetFont('times', '', 9.5, "", 'false');
	$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+5,'Total Outstanding');
	$pdf->SetFont('dejavusans', '', 9.5);
	$total_outstanding=$previos_balance;
	if($Sale['Sale']['sale_type1']=='CreditSale')
		$total_outstanding+=$actual_grand_total;
	$pdf->setRTL(true);
	$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+5,number_format($total_outstanding,3,'.',''));
	$pdf->setRTL(false);
	
}
$pdf->Line(1, $tax_subject_y,209, $tax_subject_y); // horizontal line
$tax_subject_y=262;
$pdf->SetFont('times', '', 9, "", 'false');
$pdf->text(2,$tax_subject_y+5,'Received By');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetFont('times', '', 9, "", 'false');
$pdf->text(115,$tax_subject_y+5,'Salesman');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetFont('times', '', 9, "", 'false');
$pdf->text(115,$tax_subject_y+10,'Route Mobile No');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetFont('times', '', 9, "", 'false');
// $executive_no=$Sale['Executive']['mobile'];

$pdf->text(150,$tax_subject_y+10,$route_mobile_no);
//$pdf->Line($invoce_word_width+46, $tax_subject_y,$invoce_word_width+46, $tax_subject_y+4); // vertical line
$tax_subject_y+=2;
$signature_area_y=$tax_subject_y;
$signature_length=14;
$signature_width=115;

$pdf->SetFont('dejavusans', '', 8);


$pdf->SetFont('times', '', 8, "", 'false');
$pdf->SetXY($signature_width, $signature_area_y);

// $pdf->Line($signature_width, $signature_area_y+2,$signature_width, $signature_area_y+$signature_length); // vertical line
// $pdf->Line(1, $signature_area_y+$signature_length,209, $signature_area_y+$signature_length); // horizontal line

}
$page=0;


foreach($print_field_array as $field_key=>$field_value)
{
//$page=1;
	$page++;
	header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value,$route_mobile_no);
	$total=0;
	$total_cgst_tax=0;
	$total_sgst_tax=0;
	$total_igst_tax=0;
	$item_table_head_y_start=85;
	$table_length=128-10;
	$cgst_grand_amount=0;
	$sgst_grand_amount=0;
	$igst_grand_amount=0;
	$rate_grand_amount=0;
	$total_quantity=0;
	$discount_grand_amount=0;
	$total_taxable_discount_amount=0;
	$taxable_value_grand_amount=0;
	$product_grand_total=0;
	$count=count($SaleItem);
	$i=0;
// for ($i=0; $i <$count ; $i++) { 
	$data=array(
		'total'=>$total,
		'total_cgst_tax'=>$total_cgst_tax,
		'total_sgst_tax'=>$total_sgst_tax,
		'total_igst_tax'=>$total_igst_tax,
		);


	foreach ($SaleItem as $keySI => $value) {
		$description = str_replace('"', "'", $value['Product']['name']);
// $hsn_code=$value['Product']['hsn_code'];
		$qty=floatval($value['SaleItem']['quantity']);
		$free_qty=floatval($value['SaleItem']['free_qty']);
// $unit_level=$this->Unit->findByName($value['SaleItem']['quantity_mode']);
// if(empty($unit_level)){
// $unit_level['Unit']['level'] =1;
// }
		$SaleController= new SaleController;
		$UnitLevelConvert=$SaleController->UnitLevelConvert($value['SaleItem']['product_id'],$value['SaleItem']['unit_id']);
		$sale_unit_level=$UnitLevelConvert['sale_unit_level'];
		$no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
		$product_unit_level=$UnitLevelConvert['product_unit_level'];
		$rate=floatval($value['SaleItem']['unit_price']);
		if($sale_unit_level != 1){
			if($sale_unit_level == 2){
				$qty=$value['SaleItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
				$rate=$rate*$value['Product']['no_of_piece_per_unit'];
				$free_qty=$value['SaleItem']['free_qty']/$value['Product']['no_of_piece_per_unit'];
			}
		}
		$total_quantity+=$qty;
		$uom=$value['Unit']['name'];

		$unit=$value['Unit']['name'];
		$rate_grand_amount+=$rate;
		$discount=0;
		$taxable_discount_amount=$rate*$qty;
		$total_taxable_discount_amount+=$taxable_discount_amount;

		$discount_grand_amount+=$discount;
		$taxable_value=floatval($value['SaleItem']['net_value']);
		$taxable_value_grand_amount+=$taxable_value;
		$net_value=floatval($value['SaleItem']['net_value']);

		$tax_value=floatval($value['SaleItem']['tax_amount']);
		$total+=$net_value;


		$data['total']+=floatval($net_value);
		$data['total_cgst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$data['total_sgst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$data['total_igst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$product_total=floatval($value['SaleItem']['total']);
		$product_grand_total+=$product_total;
		$cgst_rate=floatval($value['SaleItem']['tax']);
		$cgst_amount=floatval($value['SaleItem']['tax_amount']);
		$cgst_grand_amount+=$cgst_amount;
		$sgst_rate=floatval($value['SaleItem']['tax']);
		$sgst_amount=floatval($value['SaleItem']['tax_amount']);
		$sgst_grand_amount+=$sgst_amount;
		$igst_rate=floatval($value['SaleItem']['tax']);
		$igst_amount=floatval($value['SaleItem']['tax_amount']);
		$igst_grand_amount+=$igst_amount;
		$item_pos=2;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$keySI+1);
		$item_pos+=6;
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$product_name=implode($description_words,' ');
			$poped_array[]=array_pop($description_words);
			$product_name_length=strlen($product_name);
			if($product_name_length<=40)
			{ 
				$pdf->SetFont('dejavusans', '', 8);

// $pdf->text($item_pos+8,$item_table_head_y_start+8+(($i+$k)*8)+5,$value['Product']['code']);
				$arabic=$value['Product']['name'];
				$pdf->text($item_pos+12,$item_table_head_y_start+8+(($i+$k)*8)+5,$product_name);
				$pdf->text($item_pos+12,$item_table_head_y_start+8+(($i+$k)*8)+8,$value['Product']['arabic_name']);
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}

		if($k)
			$k--;
		$item_pos+=1;
		$pdf->SetFont('dejavusans', '', 8);
// $pdf->AddFont('DejaVu','','tahoma.ttf',true);
// $pdf->AddFont('DejaVu', 'B', 'tahoma-Bold.ttf', true);
// $pdf->SetFont('DejaVu','',8);
// $pdf->Text($item_pos,$item_table_head_y_start+11+($i*8)+5,$value['Product']['arabic_name']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$item_pos+=55+$igst_x_position;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$hsn_code);
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,'');
		$item_pos+=16;
		if($Sale['Sale']['free_flag']!=0){
			$pdf->text($item_pos+7,$item_table_head_y_start+8+($i*8)+5,$qty);
			if(count($SaleItem)==$keySI+1)
			{
				$pdf->text($item_pos+7,$item_table_head_y_start+$table_length,$total_quantity);
			}
			$pdf->text($item_pos-7,$item_table_head_y_start+8+($i*8)+5,$free_qty);
		}
		else{
			$pdf->text($item_pos+7,$item_table_head_y_start+8+($i*8)+5,$qty);
			if(count($SaleItem)==$keySI+1)
			{
				$pdf->text($item_pos+7,$item_table_head_y_start+$table_length,$total_quantity);
			}
		}

		$pdf->text($item_pos+17,$item_table_head_y_start+8+($i*8)+5,$unit);
		$item_pos+=37;
		$pdf->text($item_pos-4,$item_table_head_y_start+8+($i*8)+5,number_format($rate,3,'.',''));
		$item_pos+=7;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($taxable_discount_amount,3,'.',''));
		if(count($SaleItem)==$keySI+1)
		{
			$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($total_taxable_discount_amount,2));
		}
		$item_pos+=10;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),number_format($rate,2));
		if(count($SaleItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$rate_grand_amount);
		}
		$item_pos+=5;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$discount);
		if(count($SaleItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$discount_grand_amount);
		}
// $item_pos+=1;
		$pdf->text($item_pos+11,$item_table_head_y_start+8+($i*8)+5,number_format($taxable_value,3,'.',''));
		if(count($SaleItem)==$keySI+1)
		{
			$pdf->text($item_pos+11,$item_table_head_y_start+$table_length,number_format($taxable_value_grand_amount,2));
		}
		$item_pos+=1;
		if($Checkstate==1)
		{
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_rate.'%');
			$item_pos+=7;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_amount);
			if(count($SaleItem)==$keySI+1)
			{
// $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$cgst_grand_amount);
			}
			$item_pos+=15;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_rate.'%');
			$item_pos+=8;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_amount);
			if(count($SaleItem)==$keySI+1)
			{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$sgst_grand_amount);
			}
		}
		else
		{
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$igst_rate.'%');
			$item_pos+=35;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($igst_amount,3,'.',''));
			if(count($SaleItem)==$keySI+1)
			{
				$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($igst_grand_amount,2));
			}
		}

		$item_pos+=12;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_rate);
		$item_pos+=5+3;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_amount);
		//$pdf->text($item_pos-6,$item_table_head_y_start+8+($i*8)+5,number_format($product_total,3,'.',''));
		if(count($SaleItem)==$keySI+1)
		{

//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$igst_grand_amount);
			//$pdf->text($item_pos-6,$item_table_head_y_start+$table_length,number_format($product_grand_total,2));
		}
		$i+=$k;
		$count+=$k;
		if($i+$k>=13)
		{
//footer($pdf,$data,$Sale,$route_mobile_no,$previos_balance);
			$page++;
			header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value,$route_mobile_no);
			$i=-1;
			$count-=13;
		}
		$i++;
	}

	footer($pdf,$data,$Sale,$route_mobile_no,$previos_balance);
}
$pdf->Output();
exit;
}
public function fpdf_quotation($id=null)
{

	$Sale = $this->Sale->find('first', array(
		"joins" => array(
			array(
				"table" => 'customers',
				"alias" => 'Customer',
				"type" => 'inner',
				"conditions" => array('Customer.account_head_id=Sale.account_head_id'),
				),
			array(
				"table" => 'customer_types',
				"alias" => 'CustomerType',
				"type" => 'inner',
				"conditions" => array('CustomerType.id=Customer.customer_type_id'),
				),
			),
		'conditions' => array('Sale.id'=>$id),
		'fields' => array(
			'Sale.*',
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			'CustomerType.name',
			'Customer.place',
			'Customer.vat_no',
			'Customer.arabic_name',
			'Customer.mobile',
			'Customer.place',
			'Customer.route_id',
			)
		));
	$acc_id=$Sale['Customer']['route_id'];
	$route_mobile=$this->Route->findById($acc_id);
	$route_mobile_no='';
	if(!empty($route_mobile))
		$route_mobile_no=$route_mobile['Route']['mobile'];
	$name=$Sale['AccountHead']['name'];
	$invoice_no=$Sale['Sale']['invoice_no'];
	$date=$Sale['Sale']['date_of_delivered'];
	$print_field_array=[];
	$print_field_array[0]="Original For Customer";
	$print_field_array[1]="Duplicate For Accounts";
	// $print_field_array[2]="Triplicate For Accounts";
	$Checkstate=0;
	if($Sale['CustomerType']['name'] != "GENERALCUSTOMER")
	{
		$Checkstate=0;
	}
	else
	{
		$Checkstate=1;
	}
	$Checkstate=0;
	$name=$Sale['AccountHead']['name'];
	$invoice_no=$Sale['Sale']['invoice_no'];
	$AccountingsController= new AccountingsController;
	$account_head_total_debit=$Sale['AccountHead']['opening_balance'];
	$account_head_total_credit=0;
	$Debit_N_Credit_function=$AccountingsController->General_Journal_Debit_N_Credit_function($Sale['AccountHead']['id']);
	$account_head_total_debit+=round($Debit_N_Credit_function['debit'],3);
	$account_head_total_credit+=round($Debit_N_Credit_function['credit'],3);
	$account_head_total_balance=$account_head_total_debit-$account_head_total_credit;
	$previos_balance=$account_head_total_balance;
	$sale_balance=0;
	//$sale_balance=$Sale['Sale']['balance'];
	$previos_balance-=$sale_balance;
	$date=$Sale['Sale']['date_of_delivered'];
	$this->SaleItem->virtualFields = array(
		'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
		);
	$SaleItem = $this->SaleItem->find('all', array(
		'conditions' => array('SaleItem.sale_id'=>$id),
		'fields' => array(
			'SaleItem.*',
			'Product.id',
			'Product.name',
			'Product.code',
			'Product.arabic_name',
			'Product.no_of_piece_per_unit',
			'Unit.*',
			)
		));
// foreach ($SaleItem as $key => $value) {
// 	$free_qty_id=$value['SaleItem']['free_qty'];
// 	# code...
// }
//pr($free_qty_id);
//exit;
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}
	foreach ($SaleItem as $key => $value) {
		$description = str_replace('"', "'", $value['Product']['name']);
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$poped_array[]=array_pop($description_words);
			$product_name=implode($description_words,' ');
			$product_name_length=strlen($product_name);
			if($product_name_length<=42)
			{
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}
		if($k)
			$k--;
		$i+=$k;
		if($i+$k>=20)
		{
			$total_page++;
			$i=-1;
		}
		$i++;
	}
	$igst_x_position=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 


	function header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value,$route_mobile_no) 
	{
		$pdf->AddPage();
		$image_file ='profile/'.$Profile['logo'];
//$pdf->Image($image_file, 20, 10, 15, '', 'JPG', '', 'T', false, -550, '', false, false, 0, false, false, false);
		$pdf->Image($image_file,2,1,35);
// $pdf->Image('profile/'.$Profile['logo'],-5,5,-550);
// $pdf->Image('profile/'.$Profile['logo'],2,2,-525);
		$image_line_width=40;
		$invoice_x_starting=$image_line_width;
$pdf->Line($image_line_width, 1,$image_line_width, 40); // vertical line
// //$pdf->SetFont('Arial','B',12);
// $pdf->SetTextColor(0,0,100);
$pdf->rect(1, 1, 208, 295);

// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$invoice_pos+=3;
// $pdf->SetFont('Arial','B',15);
$pdf->SetFont('times', 'B', 8, "", 'false');
$pdf->SetFont('dejavusans', '', 10);
$title="QUOTATION";
$arabic="فاتورة ضريبية";
if($Sale['Sale']['sale_type1']=="CashSale"){
	// $title="CASH INVOICE";
	// $arabic="الفاتورة النقدية";} 
	// else{$title="CREDIT INVOICE";
	// $arabic="فاتورة ائتمانية";
}
	$pdf->text(85,$invoice_pos,$title);
//$pdf->SetFont('dejavusans', '', 8);

	$pdf->text(120,$invoice_pos,$arabic);


// $pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
// $pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=2;
	$pdf->SetFont('dejavusans', 'B', 10);
	$pdf->text(40,$invoice_pos+3,$Profile['company_name']);
	$pdf->SetFont('dejavusans', '', 10);

	$pdf->text(140,$invoice_pos+3,$Profile['company_name_arabic']);

	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
	$pdf->text(75,$invoice_pos+3,$Profile['address_line_1']);
	$pdf->text(145,$invoice_pos+3,$Profile['address_line_1_arabic'] );

	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->text(165,33,$field_value);
	$pdf->SetFont('dejavusans', '', 10);
	$pdf->text(45,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
	$pdf->text(145,$invoice_pos+3,$Profile['address_line_2_arabic']);

	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->SetFont('times', 'U', 12, "", 'false');
	$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
	$invoice_pos+=10;
	$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);

	$pdf->text(78,$invoice_pos,'VAT Number : '.$Profile['vat_code']);
	$VAT_Number='رقم الضريبة';
	$pdf->text(140,$invoice_pos,$VAT_Number);
	$gst_details_y_staring=40;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring); // horizontal line
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos=$gst_details_y_staring+2;
$gst_line_width=120;
$pdf->Line($gst_line_width+50, $gst_details_y_staring,$gst_line_width+50, $gst_details_y_staring+17+21+5); // vertical line
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);
$pdf->text(2,$gst_pos,'Customer Name:');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(2,$gst_pos+7,'Customer No');
$pdf->text(2,$gst_pos+14,'VAT Number');
$pdf->text(2,$gst_pos+21,'Address');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetXY($invoice_x_starting, 5);
$pdf->Cell($gst_line_width,$gst_pos+37,$name,0,0,'C');
//$pdf->text($gst_line_width-250,$gst_pos,$name);
//$pdf->text($gst_line_width-5,$gst_pos,$Sale['Customer']['arabic_name']);
$pdf->text($gst_line_width-250,$gst_pos+7,$Sale['Customer']['mobile']);
$pdf->text($gst_line_width-250,$gst_pos+14,$Sale['Customer']['vat_no']);
$pdf->text($gst_line_width-250,$gst_pos+21,$Sale['Customer']['place']);
$pdf->text($gst_line_width-285,$gst_pos+28,$Sale['Sale']['invoice_no']);
$pdf->text($gst_line_width-178,$gst_pos+28,$Sale['Sale']['invoice_no']);

if(!$Sale['Sale']['date_of_delivered'])
	$invoice_date=$Sale['Sale']['date_of_order'];
else
	$invoice_date=$Sale['Sale']['date_of_delivered'];

$pdf->text($gst_line_width-290,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));
$pdf->text($gst_line_width-185,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));

$pdf->text($gst_line_width+1+68,$gst_pos,'اسم العميل');
$pdf->text($gst_line_width+1+69,$gst_pos+7,'رقم العميل');
$pdf->text($gst_line_width+1+68,$gst_pos+14,'رقم الضريبة');
$pdf->text($gst_line_width+1+76,$gst_pos+21,'عنوان.');
$pdf->text($gst_line_width+1+68,$gst_pos+28,'رقم الفاتورة');
$pdf->text($gst_line_width+1+76,$gst_pos+35,'التاريخ');

//$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos+=28;
$pdf->text(2,$gst_pos,'Invoice Number');
$gst_pos+=7;
$pdf->text(2,$gst_pos,'Date');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->SetXY(0, 5);
$pdf->Line(40, $gst_details_y_staring,40, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos+=3;

$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+20;
$Consignee_lin_width=100+1;
//$pdf->text(2,$customer_detail_startin,'Customer Name : ');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(75,$customer_detail_startin,'اﺳﻢ اﻟﺰﺑﻮﻥ : ');
$pdf->SetFont('times', '', 10, "", 'false');
//$pdf->text($Consignee_lin_width,$customer_detail_startin,'Address :'.$Sale['Customer']['place']);

$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(195,$customer_detail_startin,'ﻋﻨﻮاﻥ :');
$pdf->SetFont('times', '', 10, "", 'false');



$customer_detail_startin+=7;
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(2,$customer_detail_startin-1,$name);
//$pdf->text(60,$customer_detail_startin+3,$Sale['Customer']['arabic_name']);
$pdf->SetFont('times', '', 10, "", 'false');
$customer_detail_startin+=4;
$customer_detail_startin+=4;
$customer_detail_startin+=1;
//$pdf->text(2,$customer_detail_startin,'VAT Number :'.$Sale['Customer']['vat_no']);
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(82,$customer_detail_startin,'ﻭاﺕ ﺭﻗﻢ:');
$pdf->SetFont('times', '', 10, "", 'false');





$pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+7;
//  table head start
$table_length=135-10;
$pdf->text(2,$item_table_head_y_start+1,'SL');
$pdf->text(2,$item_table_head_y_start+4,'No');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text(2,$item_table_head_y_start+7, 'رقم');
$pdf->text(1,$item_table_head_y_start+10,'مسلسل');
$table_column=9;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line($table_column+5, $item_table_head_y_start,$table_column+5, $item_table_head_y_start+$table_length); // vertical line

// $pdf->text($table_column+5,$item_table_head_y_start+1,'Item Code');
// $pdf->SetFont('dejavusans', '', 10);
// $pdf->Text($table_column+5,$item_table_head_y_start+7, 'رقم الصنف ');
// $pdf->Line($table_column+25, $item_table_head_y_start,$table_column+25, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+15,$item_table_head_y_start+1,'Description');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+15,$item_table_head_y_start+6,'البيان');
$table_column+=70-18+$igst_x_position;
$pdf->SetFont('times', '', 10, "", 'false');
$table_column+=16;
if($Sale['Sale']['free_flag']!=0){

$pdf->Line($table_column-17, $item_table_head_y_start,$table_column-17, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column-17,$item_table_head_y_start+1,'Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column-17,$item_table_head_y_start+6,'كمية');
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->Line($table_column-7, $item_table_head_y_start,$table_column-7, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column-7,$item_table_head_y_start+1,'Free Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column-5,$item_table_head_y_start+6,'مجانية');
$pdf->SetFont('times', '', 10, "", 'false');
}
else{

$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column,$item_table_head_y_start+1,'Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column,$item_table_head_y_start+6,'كمية');
$pdf->SetFont('times', '', 10, "", 'false');

}
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->Line($table_column+11, $item_table_head_y_start,$table_column+11, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+1,$item_table_head_y_start+1,'Unit');
$pdf->text($table_column+11+1.5,$item_table_head_y_start+1,'Rate');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+1,$item_table_head_y_start+6,'وحدة');
$pdf->Text($table_column+11+1.5,$item_table_head_y_start+6,'سعر');
$table_column+=12+3;
$pdf->Line($table_column+9.5, $item_table_head_y_start,$table_column+9, $item_table_head_y_start+$table_length); // vertical line
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($table_column+10,$item_table_head_y_start+1,'Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+12,$item_table_head_y_start+6,'مبلغ');

$table_column+=25;
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
// $table_column+=2;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
//$pdf->text($table_column+1,$item_table_head_y_start+6,'Discount');
//$table_column+=14;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column,$item_table_head_y_start+1,'Taxable Amt');
//$pdf->text($table_column,$item_table_head_y_start+4,'Excl VAT');

$pdf->SetFont('dejavusans', '', 8);
//قيمة الضريبة

$pdf->Text($table_column,$item_table_head_y_start+8,'اﻟﻤﺒﻠﻎ قيمة');
//$pdf->Text($table_column,$item_table_head_y_start+11,'اﺯاﻟﺔ ﻭاﺕ');
$pdf->SetFont('times', '', 10, "", 'false'); 
$table_column+=18.5;
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->Line($table_column+1.5, $item_table_head_y_start,$table_column+1.5, $item_table_head_y_start+$table_length); // vertical line

if($Checkstate==1)
{

$pdf->Line($table_column+1.5, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->text($table_column+6,$item_table_head_y_start+3,'CGST');
$pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
$table_column+=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+6,$item_table_head_y_start+3,'SGST');
$pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
}
else
{

$pdf->Line($table_column+1.5, $gst_details_y_staring+17+5+21+6,175.5-3+$igst_x_position, $gst_details_y_staring+17+5+21+6); // horizontal line
$pdf->text($table_column+3+2,$item_table_head_y_start+1,'VAT');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($table_column+11+2,$item_table_head_y_start+1,'ﺿﺮﻳﺒﺔ');
$pdf->SetFont('times', '', 10, "", 'false'); 

$pdf->text($table_column+1,$item_table_head_y_start+7,'Rate');
$pdf->SetFont('dejavusans', '', 8);

$pdf->Text($table_column+1,$item_table_head_y_start+10,'ﻣﻌﺪﻝ');
$table_column+=14;
$pdf->SetFont('times', '', 10, "", 'false'); 
$pdf->Line($table_column, $item_table_head_y_start+6,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+1,$item_table_head_y_start+7,'Amount');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+1,$item_table_head_y_start+10,'ﻛﻤﻴﺔ');
$pdf->SetFont('times', '', 10, "", 'false');
}
$table_column+=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line

$pdf->text($table_column+3,$item_table_head_y_start+1,'Total');
$pdf->SetFont('dejavusans', '', 10);
$pdf->Text($table_column+2,$item_table_head_y_start+4,'ﻣﺠﻤﻮﻉ');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line(1, $gst_details_y_staring+17+5+21+15,209, $gst_details_y_staring+17+5+21+15); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length,209, $item_table_head_y_start+$table_length); // horizontal line
// $pdf->Line(1, $item_table_head_y_start+$table_length+5,209, $item_table_head_y_start+$table_length+5); // horizontal line
//$pdf->text(12,$item_table_head_y_start+$table_length,'Total');
$pdf->SetFont('dejavusans', '', 10);

$pdf->SetFont('times', '', 10, "", 'false');

}

function footer($pdf,$data,$Sale,$route_mobile_no,$previos_balance)
{
	//pr($data);exit;
	$footer_start_y=208;
	$pdf->SetFont('dejavusans', '', 8);
	$pdf->text(25,$footer_start_y-5,'Total');
$pdf->Line(1, $footer_start_y-5.5,209, $footer_start_y-5.5); // horizontal line
$invoce_word_width=115;
$grand_total_length=30;
$pdf->Line($invoce_word_width, $footer_start_y,$invoce_word_width, $footer_start_y+$grand_total_length-9); // vertical line
//$pdf->text(10,$footer_start_y+1,'Amount In Words');
$pdf->SetFont('dejavusans', '', 8);
// $pdf->Text(85,$footer_start_y+1,'اﻟﻤﺒﻠﻎ ﺑﺎﻟﻜﻠﻤﺎﺕ');
$pdf->SetFont('times', '', 8, "", 'false');
//$actual_total=$Sale['Sale']['total']-$data['total_cgst_tax']-$data['total_sgst_tax']-$data['total_igst_tax'];
if($Sale['Sale']['taxable_amount']==0)
	$Sale['Sale']['taxable_amount']=$data['total'];
if($Sale['Sale']['tax_amount']==0)
	$Sale['Sale']['tax_amount']=$data['total_igst_tax'];

$total_tax_amount=$Sale['Sale']['tax_amount'];
$total_taxable_amount=$Sale['Sale']['taxable_amount'];
$actual_total=$Sale['Sale']['total'];
$actual_grand_total=$Sale['Sale']['grand_total'];
$convert_number_to_words=strtoupper(convert_number_to_words(floatval($actual_grand_total)));
$convert_number_to_arabic_words=strtoupper(convert_number_to_arabic_words(floatval($actual_grand_total)));
//$pdf->text(5,$footer_start_y+3+10,$convert_number_to_words);
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(40,$footer_start_y+3+10,$convert_number_to_arabic_words);
//$pdf->Text($invoice_lin_width+130,$customer_detail_startin+4+35,strtoupper(convert_number_to_arabic_words($amount)).' '.'الريال فقط' );
$pdf->Line($invoce_word_width+46+15, $footer_start_y,$invoce_word_width+46+15, $footer_start_y+$grand_total_length-9); // vertical line
$pdf->Line($invoce_word_width, $footer_start_y+5,209, $footer_start_y+5); // horizontal line
$pdf->text($invoce_word_width+1,$footer_start_y+1,'Total Amount');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+40,$footer_start_y+1,'المبلغ الإجمالي');
$pdf->SetFont('times', '', 9, "", 'false');
$total_amount=$Sale['Sale']['taxable_amount']+$Sale['Sale']['discount_amount'];
$rtl_x_pos=175;
$pdf->setRTL(true);
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+1,number_format($total_amount,3,'.',''));
$pdf->setRTL(false);
$footer_start_y+=5;

$Total_pos=1;
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos,'Discount');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+41,$footer_start_y+$Total_pos,'اجمالى القيمة');
$pdf->SetFont('times', '', 9, "", 'false');
$other_value=$Sale['Sale']['other_value'];
$total_discount=$Sale['Sale']['discount_amount'];
$pdf->setRTL(true);
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos,number_format($total_discount,3,'.',''));
$pdf->setRTL(false);


$Total_pos+=5;
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos,'Taxable Amount');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+49,$footer_start_y+$Total_pos+1,'الخصم');
$pdf->SetFont('times', '', 9, "", 'false');
//   //$other_value=round($data['total'])-$data['total'];
$pdf->setRTL(true);
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos,number_format($total_taxable_amount,3,'.',''));
$pdf->setRTL(false);
$Total_pos+=5;
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+1,'VAT Amount');

$pdf->SetFont('dejavusans', '', 8);
$pdf->Text($invoce_word_width+42,$footer_start_y+$Total_pos+1,'قيمة الضريبة');
$pdf->SetFont('times', '', 9, "", 'false');
$pdf->setRTL(true);
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+0.8,number_format($total_tax_amount,3,'.',''));
$pdf->setRTL(false);
$other_value_y=0;
$vertical_line_extend=$footer_start_y+16;
if($Sale['Sale']['other_value']!=0 && $Sale['Sale']['other_value']!=null)
{
	$other_value_y+=6;
	$Total_pos+=5;
	$pdf->Line($invoce_word_width, $vertical_line_extend,$invoce_word_width, $vertical_line_extend+12); // vertical line
	$pdf->Line($invoce_word_width+46+15, $vertical_line_extend,$invoce_word_width+46+15, $vertical_line_extend+12); // vertical line
	$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
	$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+1,'Total');
	$pdf->SetFont('dejavusans', '', 8);
	$pdf->Text($invoce_word_width+49,$footer_start_y+$Total_pos+0.5,'مجموع');
	$pdf->SetFont('times', '', 9, "", 'false');
	$pdf->setRTL(true);
	$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+1,number_format($actual_total,3,'.',''));
	$pdf->setRTL(false);
	$other_value_y+=6;
	$Total_pos+=5;
	
	$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
	$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+1,'Round Off');
	$pdf->SetFont('dejavusans', '', 8);
	$pdf->Text($invoce_word_width+44,$footer_start_y+$Total_pos+0.5,'نهاية الجولة');
	$pdf->SetFont('times', '', 9, "", 'false');
	$pdf->setRTL(true);
	$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+1,number_format($Sale['Sale']['other_value'],3,'.',''));
	$pdf->setRTL(false);

}
$Total_pos+=5;
$vertical_line_extend+=$other_value_y;
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos,209, $footer_start_y+$Total_pos); // horizontal line
$pdf->Line($invoce_word_width, $vertical_line_extend,$invoce_word_width, $vertical_line_extend+9); // vertical line
$pdf->Line($invoce_word_width+46+15, $vertical_line_extend,$invoce_word_width+46+15, $vertical_line_extend+9); // vertical line

$pdf->SetFont('times', '', 9.5, "", 'false');
$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+2,'Bill Amount');
$pdf->SetFont('dejavusans', '', 9.5);
$pdf->Text($invoce_word_width+39,$footer_start_y+$Total_pos+2,'فاتورة المبلغ');
$pdf->SetFont('times', '', 12, "", 'false');
$pdf->setRTL(true);
$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+1,number_format($actual_grand_total,3,'.',''));
$pdf->setRTL(false);
$tax_subject_y=$footer_start_y+$grand_total_length-7+$other_value_y;

$pdf->SetFont('times', '', 8, "", 'false');

$footer_start_y+=1.5;
if($Sale['Sale']['account_head_id']!=2)
{
	$tax_subject_y+=1;
	//$pdf->Line($invoce_word_width, $tax_subject_y,209, $tax_subject_y); // horizontal line
	$tax_subject_y+=1;
	$footer_start_y+=6;
	$vertical_line_extend+=7;
	//$pdf->Line($invoce_word_width, $vertical_line_extend,$invoce_word_width, $vertical_line_extend+12+1); // vertical line
//	$pdf->Line($invoce_word_width+46+15, $vertical_line_extend,$invoce_word_width+46+15, $vertical_line_extend+12+1); // vertical line
	$pdf->SetFont('times', '', 9.5, "", 'false');
	//$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+2,'Previous Balance');
	//$pdf->SetFont('dejavusans', '', 9.5);
	//$pdf->Text($invoce_word_width+37,$footer_start_y+$Total_pos+2,'الرصيد السابق');
	//$previos_balance=0;
	//$pdf->setRTL(true);
	//$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+2,number_format($previos_balance,3,'.',''));
	//$pdf->setRTL(false);
	//$pdf->Line($invoce_word_width, $tax_subject_y,209, $tax_subject_y); // horizontal line
	//$tax_subject_y+=6;
	//$footer_start_y+=6;
	//$pdf->SetFont('times', '', 9.5, "", 'false');
//	$pdf->text($invoce_word_width+1,$footer_start_y+$Total_pos+2,'Total Outstanding');
	//$pdf->SetFont('dejavusans', '', 9.5);
	//$pdf->Text($invoce_word_width+35,$footer_start_y+$Total_pos+2,'المجموع المتميز');

	//$total_outstanding=$previos_balance;
	//if($Sale['Sale']['sale_type1']=='CreditSale')
		//$total_outstanding+=$actual_grand_total;
	$pdf->setRTL(true);
	//$pdf->text($invoce_word_width-$rtl_x_pos+63,$footer_start_y+$Total_pos+2,number_format($total_outstanding,3,'.',''));
	$pdf->setRTL(false);
	
}
$pdf->Line(1, $tax_subject_y,209, $tax_subject_y); // horizontal line
$tax_subject_y=245;
$pdf->SetFont('times', '', 9, "", 'false');
$pdf->text(2,$tax_subject_y+5,'Received By');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(85,$tax_subject_y+5,'المستلم');
$pdf->SetFont('times', '', 9, "", 'false');
$pdf->text(115,$tax_subject_y+5,'Salesman');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(190,$tax_subject_y+5,'البائغ');البائغ:
$pdf->SetFont('times', '', 9, "", 'false');
$pdf->text(115,$tax_subject_y+10,'Route Mobile No');
$pdf->SetFont('dejavusans', '', 8);
$pdf->Text(190,$tax_subject_y+10,'طريق الجوال');
$pdf->SetFont('times', '', 9, "", 'false');
// $executive_no=$Sale['Executive']['mobile'];

$pdf->text(150,$tax_subject_y+10,$route_mobile_no);
//$pdf->Line($invoce_word_width+46, $tax_subject_y,$invoce_word_width+46, $tax_subject_y+4); // vertical line
$tax_subject_y+=2;
$signature_area_y=$tax_subject_y;
$signature_length=14;
$signature_width=115;

$pdf->SetFont('dejavusans', '', 8);


$pdf->SetFont('times', '', 8, "", 'false');
$pdf->SetXY($signature_width, $signature_area_y);

// $pdf->Line($signature_width, $signature_area_y+2,$signature_width, $signature_area_y+$signature_length); // vertical line
// $pdf->Line(1, $signature_area_y+$signature_length,209, $signature_area_y+$signature_length); // horizontal line

}
$page=0;


foreach($print_field_array as $field_key=>$field_value)
{
//$page=1;
	$page++;
	header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value,$route_mobile_no);
	$total=0;
	$total_cgst_tax=0;
	$total_sgst_tax=0;
	$total_igst_tax=0;
	$item_table_head_y_start=85;
	$table_length=128-10;
	$cgst_grand_amount=0;
	$sgst_grand_amount=0;
	$igst_grand_amount=0;
	$rate_grand_amount=0;
	$total_quantity=0;
	$discount_grand_amount=0;
	$total_taxable_discount_amount=0;
	$taxable_value_grand_amount=0;
	$product_grand_total=0;
	$count=count($SaleItem);
	$i=0;
// for ($i=0; $i <$count ; $i++) { 
	$data=array(
		'total'=>$total,
		'total_cgst_tax'=>$total_cgst_tax,
		'total_sgst_tax'=>$total_sgst_tax,
		'total_igst_tax'=>$total_igst_tax,
		);


	foreach ($SaleItem as $keySI => $value) {
    $description = str_replace('"', "'", $value['Product']['name']);
// $hsn_code=$value['Product']['hsn_code'];
		$qty=floatval($value['SaleItem']['quantity']);
		$free_qty=floatval($value['SaleItem']['free_qty']);
// $unit_level=$this->Unit->findByName($value['SaleItem']['quantity_mode']);
// if(empty($unit_level)){
// $unit_level['Unit']['level'] =1;
// }
		$SaleController= new SaleController;
		$UnitLevelConvert=$SaleController->UnitLevelConvert($value['SaleItem']['product_id'],$value['SaleItem']['unit_id']);
		$sale_unit_level=$UnitLevelConvert['sale_unit_level'];
		$no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
		$product_unit_level=$UnitLevelConvert['product_unit_level'];
		$rate=floatval($value['SaleItem']['unit_price']);
		if($sale_unit_level != 1){
			if($sale_unit_level == 2){
				$qty=$value['SaleItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
				$rate=$rate*$value['Product']['no_of_piece_per_unit'];
				$free_qty=$value['SaleItem']['free_qty']/$value['Product']['no_of_piece_per_unit'];
			}
		}
		$total_quantity+=$qty;
		$uom=$value['Unit']['name'];

		$unit=$value['Unit']['name'];
		$rate_grand_amount+=$rate;
		$discount=0;
		$taxable_discount_amount=$rate*$qty;
		$total_taxable_discount_amount+=$taxable_discount_amount;

		$discount_grand_amount+=$discount;
		$taxable_value=floatval($value['SaleItem']['net_value']);
		$taxable_value_grand_amount+=$taxable_value;
		$net_value=floatval($value['SaleItem']['net_value']);

		$tax_value=floatval($value['SaleItem']['tax_amount']);
		$total+=$net_value;


		$data['total']+=floatval($net_value);
		$data['total_cgst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$data['total_sgst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$data['total_igst_tax']+=floatval($value['SaleItem']['tax_amount']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$product_total=floatval($value['SaleItem']['total']);
		$product_grand_total+=$product_total;
		$cgst_rate=floatval($value['SaleItem']['tax']);
		$cgst_amount=floatval($value['SaleItem']['tax_amount']);
		$cgst_grand_amount+=$cgst_amount;
		$sgst_rate=floatval($value['SaleItem']['tax']);
		$sgst_amount=floatval($value['SaleItem']['tax_amount']);
		$sgst_grand_amount+=$sgst_amount;
		$igst_rate=floatval($value['SaleItem']['tax']);
		$igst_amount=floatval($value['SaleItem']['tax_amount']);
		$igst_grand_amount+=$igst_amount;
		$item_pos=2;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$keySI+1);
		$item_pos+=6;
// 		$description_length=strlen($description);
// 		$description_words=explode(' ', $description);
// 		$word_count=count($description_words);
// 		$k=0;
// 		$poped_array=[];
// 		for ($j=0; $j <$word_count; $j++) {
// 			$product_name=implode($description_words,' ');
// 			$poped_array[]=array_pop($description_words);
// 			$product_name_length=strlen($product_name);
// 			if($product_name_length<=40)
// 			{ 
// 				$pdf->SetFont('dejavusans', '', 8);

// // $pdf->text($item_pos+8,$item_table_head_y_start+8+(($i+$k)*8)+5,$value['Product']['code']);
// 				$arabic=$value['Product']['name'];
// 				$pdf->text($item_pos+12,$item_table_head_y_start+8+(($i+$k)*8)+5,$product_name);
// 		$pdf->text($item_pos+12,$item_table_head_y_start+8+(($i+$k)*8)+8,$value['Product']['arabic_name']);
// 				$poped_array=array_reverse($poped_array);
// 				$k++;
// 				$description_words=$poped_array;
// 				$word_count=count($description_words);
// 				$poped_array=[];
// 				$j=0;  
// 			}
// 		}

// 		if($k)
// 		$k--;
		$description_length=strlen($description);
    $description_words=explode(' ', $description);
    $word_count=count($description_words);
    $k=0;
    $poped_array=[];
    for ($j=0; $j <$word_count; $j++) {
      $product_name=implode($description_words,' ');
      $poped_array[]=array_pop($description_words);
      $product_name_length=strlen($product_name);
      if($product_name_length<=45)
      {
      	$pdf->SetFont('dejavusans', '', 8);
        $pdf->text($item_pos+8,$item_table_head_y_start+8+(($i+$k)*8)+5,$product_name);
       // $pdf->text($item_pos+8,$item_table_head_y_start+8+(($i+$k)*8)+8,$value['Product']['arabic_name']);
        $poped_array=array_reverse($poped_array);
        $k++;
        $description_words=$poped_array;
        $word_count=count($description_words);
        $poped_array=[];
        $j=0;  
      }
    }
    if($k)
      $k--;
		$item_pos+=1;
		$pdf->SetFont('dejavusans', '', 8);
// $pdf->AddFont('DejaVu','','tahoma.ttf',true);
// $pdf->AddFont('DejaVu', 'B', 'tahoma-Bold.ttf', true);
// $pdf->SetFont('DejaVu','',8);
// $pdf->Text($item_pos,$item_table_head_y_start+11+($i*8)+5,$value['Product']['arabic_name']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$item_pos+=55+$igst_x_position;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$hsn_code);
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,'');
		$item_pos+=16;
		if($Sale['Sale']['free_flag']!=0){
			$pdf->text($item_pos-19,$item_table_head_y_start+8+($i*8)+5,$qty);
			if(count($SaleItem)==$keySI+1)
			{
				$pdf->text($item_pos-19,$item_table_head_y_start+$table_length,$total_quantity);
			}
			$pdf->text($item_pos-5,$item_table_head_y_start+8+($i*8)+5,$free_qty);
		}
		else{
			$pdf->text($item_pos-3,$item_table_head_y_start+8+($i*8)+5,$qty);
			if(count($SaleItem)==$keySI+1)
			{
				$pdf->text($item_pos-3,$item_table_head_y_start+$table_length,$total_quantity);
			}
		}

		$pdf->text($item_pos+6,$item_table_head_y_start+8+($i*8)+5,$unit);
		$item_pos+=20;
		$pdf->text($item_pos-4,$item_table_head_y_start+8+($i*8)+5,number_format($rate,3,'.',''));
		$item_pos+=10;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($taxable_discount_amount,3,'.',''));
		if(count($SaleItem)==$keySI+1)
		{
			$pdf->text($item_pos-1.5,$item_table_head_y_start+$table_length,number_format($total_taxable_discount_amount,3));
		}
		$item_pos+=10;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),number_format($rate,2));
		if(count($SaleItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$rate_grand_amount);
		}
		$item_pos+=5;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$discount);
		if(count($SaleItem)==$keySI+1)
		{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$discount_grand_amount);
		}
// $item_pos+=1;
		$pdf->text($item_pos+3,$item_table_head_y_start+8+($i*8)+5,number_format($taxable_value,3,'.',''));
		if(count($SaleItem)==$keySI+1)
		{
			$pdf->text($item_pos+1,$item_table_head_y_start+$table_length,number_format($taxable_value_grand_amount,3));
		}
		$item_pos+=21;
		if($Checkstate==1)
		{
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_rate.'%');
			$item_pos+=7;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$cgst_amount);
			if(count($SaleItem)==$keySI+1)
			{
// $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$cgst_grand_amount);
			}
			$item_pos+=15;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_rate.'%');
			$item_pos+=8;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$sgst_amount);
			if(count($SaleItem)==$keySI+1)
			{
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$sgst_grand_amount);
			}
		}
		else
		{
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$igst_rate.'%');
			$item_pos+=13;
			$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($igst_amount,3,'.',''));
			if(count($SaleItem)==$keySI+1)
			{
				$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($igst_grand_amount,3));
			}
		}

		$item_pos+=12;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_rate);
		$item_pos+=5+3;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_amount);
		$pdf->text($item_pos-6,$item_table_head_y_start+8+($i*8)+5,number_format($product_total,3,'.',''));
		if(count($SaleItem)==$keySI+1)
		{

//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$igst_grand_amount);
			$pdf->text($item_pos-6,$item_table_head_y_start+$table_length,number_format($product_grand_total,3));
		}
		$i+=$k;
		$count+=$k;
		if($i+$k>=13)
		{
//footer($pdf,$data,$Sale,$route_mobile_no,$previos_balance);
			$page++;
			header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value,$route_mobile_no);
			$i=-1;
			$count-=13;
		}
		$i++;
	}

	footer($pdf,$data,$Sale,$route_mobile_no,$previos_balance);
}
$pdf->Output();
exit;
}
public function sales_return_print($id=null)
{

	$SalesReturn = $this->SalesReturn->find('first', array(
		"joins" => array(
			array(
				"table" => 'customers',
				"alias" => 'Customer',
				"type" => 'inner',
				"conditions" => array('Customer.account_head_id=SalesReturn.account_head_id'),
				),
			array(
				"table" => 'customer_types',
				"alias" => 'CustomerType',
				"type" => 'inner',
				"conditions" => array('CustomerType.id=Customer.customer_type_id'),
				),
			),
		'conditions' => array('SalesReturn.id'=>$id),
		'fields' => array(
			'SalesReturn.*',
			'AccountHead.name',
			'CustomerType.name',
			'Customer.place',
			'Customer.state_id',
			'Customer.vat_no',
			'Customer.arabic_name',
			'Customer.mobile',
			'Customer.place',
			)
		));
	$name=$SalesReturn['AccountHead']['name'];
	$invoice_no=$SalesReturn['SalesReturn']['invoice_no'];
	$date=$SalesReturn['SalesReturn']['date'];
	$print_field_array=[];
	$print_field_array[0]="Original For Customer";
	$print_field_array[1]="Duplicate For Accounts";
	//$print_field_array[2]="Triplicate For Accounts";
	$Checkstate=0;
// pr($Sale);exit;
	if($SalesReturn['CustomerType']['name'] != "GENERALCUSTOMER")
	{
		$Checkstate=0;
	}
	else
	{
		$Checkstate=1;
	}
	$Checkstate=0;
// if($SalesReturn['AccountHead']['name'] == "GENERALCUSTOMER"){
//   $name=$SalesReturn['Sale']['customer_name'];
// }else{
//   $name=$Sale['AccountHead']['name'];
// }
	$name=$SalesReturn['AccountHead']['name'];
	$invoice_no=$SalesReturn['SalesReturn']['invoice_no'];
	$date=$SalesReturn['SalesReturn']['date'];
	$this->SalesReturnItem->virtualFields = array(
		'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
		);
	$SalesReturnItem = $this->SalesReturnItem->find('all', array(
		'conditions' => array('SalesReturnItem.sales_return_id'=>$id),
		'fields' => array(
			'SalesReturnItem.*',
			'Product.id',
			'Product.name',
			'Product.code',
			'Product.arabic_name',
			'Product.no_of_piece_per_unit',
			)
		));
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 018');
	$pdf->SetSubject('TCPDF Tutorial');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}
	foreach ($SalesReturnItem as $key => $value) {
		$description = str_replace('"', "'", $value['Product']['name']);
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$poped_array[]=array_pop($description_words);
			$product_name=implode($description_words,' ');
			$product_name_length=strlen($product_name);
			if($product_name_length<=42)
			{
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}
		if($k)
			$k--;
		$i+=$k;
		if($i+$k>=20)
		{
			$total_page++;
			$i=-1;
		}
		$i++;
	}
	$igst_x_position=0;
	if($Checkstate==0)
	{
		$igst_x_position=20;
	} 


	function header_section($pdf,$SalesReturn,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value) 
	{
		$pdf->AddPage();
		$image_file ='profile/'.$Profile['logo'];
//$pdf->Image($image_file, 20, 10, 15, '', 'JPG', '', 'T', false, -550, '', false, false, 0, false, false, false);
		$pdf->Image($image_file,2,1,35);
// $pdf->Image('profile/'.$Profile['logo'],-5,5,-550);
// $pdf->Image('profile/'.$Profile['logo'],2,2,-525);
		$image_line_width=40;
		$invoice_x_starting=$image_line_width;
$pdf->Line($image_line_width, 1,$image_line_width, 40); // vertical line
// //$pdf->SetFont('Arial','B',12);
// $pdf->SetTextColor(0,0,100);
$pdf->rect(1, 1, 208, 295);

// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$invoice_pos+=3;
// $pdf->SetFont('Arial','B',15);
$pdf->SetFont('times', 'B', 8, "", 'false');
$pdf->SetFont('dejavusans', '', 10);
$title="SALES RETURN";
$arabic="فاتورة ضريبية";
	$pdf->text(98,$invoice_pos,$title);
	$invoice_pos+=2;
	$pdf->SetFont('dejavusans', 'B', 10);
	$pdf->text(92,$invoice_pos+3,$Profile['company_name']);
	$pdf->SetFont('dejavusans', '', 10);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->SetFont('dejavusans', '', 10);
	$pdf->text(83,$invoice_pos+3,$Profile['address_line_1']);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->text(165,33,$field_value);
	$pdf->SetFont('dejavusans', '', 10);
	$pdf->text(88,$invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile']);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->SetFont('times', 'U', 12, "", 'false');
	$pdf->text(90,$invoice_pos+3,$Profile['mail_address']);
	$invoice_pos+=10;
	$pdf->SetFont('dejavusans', '', 10);
	$pdf->text(83,$invoice_pos,'GST Number : '.$Profile['vat_code']);
	$gst_details_y_staring=40;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring); // horizontal line
$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos=$gst_details_y_staring+2;
$gst_line_width=45;
//$pdf->Line($gst_line_width+50, $gst_details_y_staring,$gst_line_width+50, $gst_details_y_staring+17+21+5); // vertical line
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);
$pdf->text(2,$gst_pos,'Customer Name:');
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(2,$gst_pos+7,'Customer No');
$pdf->text(2,$gst_pos+14,'GST Number');
$pdf->text(2,$gst_pos+21,'Address');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetXY($invoice_x_starting, 5);
//$pdf->Cell($gst_line_width,$gst_pos+37,$name,0,0,'C');
$pdf->text($gst_line_width,$gst_pos,$name);
//$pdf->text($gst_line_width-5,$gst_pos,$Sale['Customer']['arabic_name']);
$pdf->text($gst_line_width,$gst_pos+7,$SalesReturn['Customer']['mobile']);
$pdf->text($gst_line_width,$gst_pos+14,$SalesReturn['Customer']['vat_no']);
$pdf->text($gst_line_width,$gst_pos+21,$SalesReturn['Customer']['place']);
$pdf->text($gst_line_width,$gst_pos+28,$SalesReturn['SalesReturn']['invoice_no']);

if(!$SalesReturn['SalesReturn']['date'])
	$invoice_date=$SalesReturn['SalesReturn']['date'];
else
	$invoice_date=$SalesReturn['SalesReturn']['date'];

$pdf->text($gst_line_width,$gst_pos+35,date("d-m-Y",strtotime($invoice_date)));

$pdf->SetFont('times', '', 10, "", 'false');
$gst_pos+=28;
$pdf->text(2,$gst_pos,'Invoice Number');
$gst_pos+=7;
$pdf->text(2,$gst_pos,'Date');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->SetXY(0, 5);
$pdf->Line(40, $gst_details_y_staring,40, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos+=3;

$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+36;
$Consignee_lin_width=100+1;

$pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+7;
//  table head start
$table_length=135-10;
$pdf->text(2,$item_table_head_y_start+1,'SL');
$pdf->text(2,$item_table_head_y_start+4,'No');
$pdf->SetFont('dejavusans', '', 10);
$table_column=9;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line($table_column+5, $item_table_head_y_start,$table_column+5, $item_table_head_y_start+$table_length+10); // vertical line

// $pdf->text($table_column+5,$item_table_head_y_start+1,'Item Code');
// $pdf->SetFont('dejavusans', '', 10);
// $pdf->Text($table_column+5,$item_table_head_y_start+7, 'رقم الصنف ');
// $pdf->Line($table_column+25, $item_table_head_y_start,$table_column+25, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+15,$item_table_head_y_start+1,'Description');
$pdf->SetFont('dejavusans', '', 10);
$table_column+=70-18+$igst_x_position;
$pdf->SetFont('times', '', 10, "", 'false');
$table_column+=16;

$pdf->Line($table_column+20, $item_table_head_y_start,$table_column+20, $item_table_head_y_start+$table_length+10); // vertical line
$pdf->text($table_column+11,$item_table_head_y_start+1,'Qty');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');

$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+10); // vertical line
$pdf->Line($table_column+28, $item_table_head_y_start,$table_column+28, $item_table_head_y_start+$table_length+10); // vertical line
$pdf->text($table_column+15,$item_table_head_y_start+1,'Unit');
$pdf->text($table_column+30,$item_table_head_y_start+1,'Rate');
$pdf->SetFont('dejavusans', '', 10);
$table_column+=12+3;
$pdf->Line($table_column+27, $item_table_head_y_start,$table_column+27, $item_table_head_y_start+$table_length+10); // vertical line
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($table_column+27,$item_table_head_y_start+1,'Amount');
$pdf->SetFont('dejavusans', '', 10);

$table_column+=25;
$pdf->SetFont('times', '', 10, "", 'false');

$pdf->Line($table_column+18, $item_table_head_y_start,$table_column+18, $item_table_head_y_start+$table_length+10); // vertical line
// $table_column+=2;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
//$pdf->text($table_column+1,$item_table_head_y_start+6,'Discount');
//$table_column+=14;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column+26,$item_table_head_y_start+1,'Taxable Amt');
//$pdf->text($table_column,$item_table_head_y_start+4,'Excl VAT');

$pdf->SetFont('dejavusans', '', 8);
//قيمة الضريبة

//$pdf->Text($table_column,$item_table_head_y_start+11,'اﺯاﻟﺔ ﻭاﺕ');
$pdf->SetFont('times', '', 10, "", 'false'); 
$table_column+=18.5;
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->Line($table_column+8, $item_table_head_y_start,$table_column+8, $item_table_head_y_start+$table_length+10); // vertical line

if($Checkstate==1)
{

$pdf->Line($table_column+1.5, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->text($table_column+6,$item_table_head_y_start+3,'CGST');
$pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length+10); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
$table_column+=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+10); // vertical line
$pdf->text($table_column+6,$item_table_head_y_start+3,'SGST');
$pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
$table_column+=8;
$pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length+10); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
}
else
{

//$pdf->Line($table_column+1.5, $gst_details_y_staring+17+5+21+6,175.5-3+$igst_x_position, $gst_details_y_staring+17+5+21+6); // horizontal line
$pdf->text($table_column,$item_table_head_y_start+1,'Tax');
$pdf->SetFont('dejavusans', '', 8);
$pdf->SetFont('times', '', 10, "", 'false'); 

//$pdf->text($table_column+1,$item_table_head_y_start+7,'Rate');
$pdf->SetFont('dejavusans', '', 8);

$table_column+=14;
$pdf->SetFont('times', '', 10, "", 'false'); 
//$pdf->Line($table_column, $item_table_head_y_start+6,$table_column, $item_table_head_y_start+$table_length+10); // vertical line
$pdf->text($table_column+15,$item_table_head_y_start,'Tax Amt');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
}
$table_column+=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+10); // vertical line

//$pdf->text($table_column+3,$item_table_head_y_start+1,'Total');
$pdf->SetFont('dejavusans', '', 10);
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line(1, $gst_details_y_staring+17+5+21+15,209, $gst_details_y_staring+17+5+21+15); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length+10,209, $item_table_head_y_start+$table_length+10); // horizontal line
// $pdf->Line(1, $item_table_head_y_start+$table_length+5,209, $item_table_head_y_start+$table_length+5); // horizontal line
//$pdf->text(12,$item_table_head_y_start+$table_length,'Total');
$pdf->SetFont('dejavusans', '', 10);

$pdf->SetFont('times', '', 10, "", 'false');

}

function footer($pdf,$data,$SalesReturn)
{
	$footer_start_y=218;
//$pdf->Line(1, $footer_start_y,209, $footer_start_y); // horizontal line
$invoce_word_width=113;
$grand_total_length=30;
$pdf->Line($invoce_word_width+4, $footer_start_y,$invoce_word_width+4, $footer_start_y+$grand_total_length+4); // vertical line
//$pdf->text(10,$footer_start_y+1,'Amount In Words');
$pdf->SetFont('dejavusans', '', 8);
// $pdf->Text(85,$footer_start_y+1,'اﻟﻤﺒﻠﻎ ﺑﺎﻟﻜﻠﻤﺎﺕ');
$pdf->SetFont('times', '', 8, "", 'false');
$actual_total=$SalesReturn['SalesReturn']['total']-$data['total_igst_tax'];
$actual_grand_total=$SalesReturn['SalesReturn']['grand_total'];
$convert_number_to_words=strtoupper(convert_number_to_words(floatval($actual_grand_total)));
$convert_number_to_arabic_words=strtoupper(convert_number_to_arabic_words(floatval($actual_grand_total)));
//$pdf->text(5,$footer_start_y+3+10,$convert_number_to_words);
$pdf->SetFont('dejavusans', '', 8);
//$pdf->text(40,$footer_start_y+3+10,$convert_number_to_arabic_words);
//$pdf->Text($invoice_lin_width+130,$customer_detail_startin+4+35,strtoupper(convert_number_to_arabic_words($amount)).' '.'الريال فقط' );
$pdf->Line($invoce_word_width+50, $footer_start_y,$invoce_word_width+50, $footer_start_y+$grand_total_length+4); // vertical line
$Total_pos=1;
$pdf->text($invoce_word_width+5,$footer_start_y+$Total_pos,'Total Amount');
$pdf->SetFont('dejavusans', '', 8);
//$pdf->Text($invoce_word_width+22,$footer_start_y+$Total_pos,'اجمالى القيمة');
$pdf->SetFont('times', '', 8, "", 'false');

$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos,number_format($data['total'],2));
$state_id=$SalesReturn['Customer']['state_id'];
$Total_pos+=5;
$pdf->text($invoce_word_width+5,$footer_start_y+$Total_pos+2,'SGST');
$pdf->text($invoce_word_width+5,$footer_start_y+$Total_pos+7,'CGST');
$pdf->text($invoce_word_width+5,$footer_start_y+$Total_pos+12,'IGST');

$pdf->SetFont('dejavusans', '', 8);
//$pdf->Text($invoce_word_width+28,$footer_start_y+$Total_pos+5,'قيمة الضريبة');
$pdf->SetFont('times', '', 8, "", 'false');
if($state_id==19)
{
$tax_amount_split=floatval($data['total_igst_tax'])/2;
$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos+2,round($tax_amount_split,3));
$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos+7,round($tax_amount_split,3));
$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos+12,0);
}
else
{
$tax_amount_split=floatval($data['total_igst_tax']);
$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos+2,0);
$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos+7,0);
$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos+12,round($tax_amount_split,3));
}
$pdf->Line($invoce_word_width+4, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$pdf->Line($invoce_word_width+4, $footer_start_y+$Total_pos+6,209, $footer_start_y+$Total_pos+6); // horizontal line

$pdf->Line($invoce_word_width+4, $footer_start_y+$Total_pos+11,209, $footer_start_y+$Total_pos+11); // horizontal line

$Total_pos+=5;

$pdf->SetFont('times', '', 8, "", 'false');
//   //$other_value=round($data['total'])-$data['total'];
$other_value=0;
$Total_pos+=8;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($invoce_word_width+5,$footer_start_y+$Total_pos+8,'Net Amount');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text($invoce_word_width+23,$footer_start_y+$Total_pos+5,'المبلغ الصافى');
$pdf->SetFont('times', '', 12, "", 'false');

$pdf->text($invoce_word_width+53,$footer_start_y+$Total_pos+8,number_format($actual_grand_total,2));
$pdf->Line($invoce_word_width+4, $footer_start_y+$Total_pos+3,209, $footer_start_y+$Total_pos+3); // horizontal line
$tax_subject_y=$footer_start_y+$grand_total_length;
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->Line(1, $tax_subject_y+4,209, $tax_subject_y+4); // horizontal line
$pdf->text(2,$tax_subject_y+5,'Received By');
$pdf->SetFont('dejavusans', '', 8);
//$pdf->Text(85,$tax_subject_y+5,'المستلم');
$pdf->SetFont('times', '', 8, "", 'false');
$pdf->text(115,$tax_subject_y+5,'Salesman');
$pdf->SetFont('dejavusans', '', 8);
//$pdf->Text(190,$tax_subject_y+5,'البائغ');البائغ:
$pdf->SetFont('times', '', 8, "", 'false');
//$pdf->Line($invoce_word_width+46, $tax_subject_y,$invoce_word_width+46, $tax_subject_y+4); // vertical line
$signature_area_y=$tax_subject_y+4;
$signature_length=14;
$signature_width=113;

$pdf->SetFont('dejavusans', '', 8);


$pdf->SetFont('times', '', 8, "", 'false');
$pdf->SetXY($signature_width, $signature_area_y);

$pdf->Line($signature_width, $signature_area_y,$signature_width, $signature_area_y+$signature_length); // vertical line
$pdf->Line(1, $signature_area_y+$signature_length,209, $signature_area_y+$signature_length); // horizontal line

}
$page=0;


foreach($print_field_array as $field_key=>$field_value)
{
//$page=1;
	$page++;
	header_section($pdf,$SalesReturn,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value);
	$total=0;
	$total_cgst_tax=0;
	$total_sgst_tax=0;
	$total_igst_tax=0;
	$item_table_head_y_start='87';
	$table_length='128';
	$cgst_grand_amount=0;
	$sgst_grand_amount=0;
	$igst_grand_amount=0;
	$rate_grand_amount=0;
	$discount_grand_amount=0;
	$taxable_value_grand_amount=0;
	$product_grand_total=0;
	$count=count($SalesReturnItem);
	$i=0;
// for ($i=0; $i <$count ; $i++) { 
	$data=array(
		'total'=>$total,
		'total_cgst_tax'=>$total_cgst_tax,
		'total_sgst_tax'=>$total_sgst_tax,
		'total_igst_tax'=>$total_igst_tax,
		);
	foreach ($SalesReturnItem as $keySI => $value) {
		$description = str_replace('"', "'", $value['SalesReturnItem']['product_name']);
// $hsn_code=$value['Product']['hsn_code'];
		$qty=floatval($value['SalesReturnItem']['quantity']);
		$unit_level=$this->Unit->findById($value['SalesReturnItem']['quantity_mode']);
		if(empty($unit_level)){
			$unit_level['Unit']['level'] =1;
		}
		if($unit_level['Unit']['level'] !=1){
			$value['SalesReturnItem']['quantity']=$value['SalesReturnItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
			$qty = $value['SalesReturnItem']['quantity'];
		}
		$uom=$unit_level['Unit']['name'];

// if($value['SalesReturnItem']['quantity_mode']==1)
// {
// 	$unit="Piece";
// $rate=floatval($value['SalesReturnItem']['unit_price']);
// $qty=$value['SalesReturnItem']['quantity'];
// }
// else
// {
// 	$unit="Case";
// 	$qty=$value['SalesReturnItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
// 	$rate=$value['SalesReturnItem']['unit_price']*$value['Product']['no_of_piece_per_unit'];


// }
		$rate=floatval($value['SalesReturnItem']['unit_price']);
		$qty=$value['SalesReturnItem']['quantity'];
		$rate_grand_amount+=$rate;
		$discount=0;

		$discount_grand_amount+=$discount;
		$taxable_value=floatval($value['SalesReturnItem']['net_value']);
		$taxable_value_grand_amount+=$taxable_value;
		$net_value=floatval($value['SalesReturnItem']['net_value']);

		$tax_value=floatval($value['SalesReturnItem']['tax_amount']);
		$total+=$net_value;

		$data['total']+=floatval($net_value);
		$data['total_igst_tax']+=floatval($value['SalesReturnItem']['tax_amount']);
		$pdf->SetFont('times', '', 8, "", 'false');
		$product_total=floatval($value['SalesReturnItem']['total']);
		$product_grand_total+=$product_total;
		$igst_rate=floatval($value['SalesReturnItem']['tax']);
		$igst_amount=floatval($value['SalesReturnItem']['tax_amount']);
		$igst_grand_amount+=$igst_amount;
		$item_pos=2;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$keySI+1);
		$item_pos+=6;
		$description_length=strlen($description);
		$description_words=explode(' ', $description);
		$word_count=count($description_words);
		$k=0;
		$poped_array=[];
		for ($j=0; $j <$word_count; $j++) {
			$product_name=implode($description_words,' ');
			$poped_array[]=array_pop($description_words);
			$product_name_length=strlen($product_name);
			if($product_name_length<=40)
			{ 
				$pdf->SetFont('dejavusans', '', 8);

				$pdf->text($item_pos+8,$item_table_head_y_start+8+(($i+$k)*8)+5,$value['Product']['code']);
				$arabic=$value['Product']['name'];
				$pdf->text($item_pos+26,$item_table_head_y_start+8+(($i+$k)*8)+5,$arabic);
				$pdf->text($item_pos+26,$item_table_head_y_start+8+(($i+$k)*8)+8,$value['Product']['arabic_name']);
				$poped_array=array_reverse($poped_array);
				$k++;
				$description_words=$poped_array;
				$word_count=count($description_words);
				$poped_array=[];
				$j=0;  
			}
		}

		if($k)
			$k--;
		$item_pos+=1;
		$pdf->SetFont('dejavusans', '', 8);
		$pdf->SetFont('times', '', 8, "", 'false');
		$item_pos+=70+$igst_x_position;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$hsn_code);
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,'');
		$item_pos+=1;
		$pdf->text($item_pos+5,$item_table_head_y_start+8+($i*8)+5,$qty);
        $pdf->text($item_pos+8+10,$item_table_head_y_start+8+($i*8)+5,$uom);
		$item_pos+=33;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($rate,2));

		$item_pos+=14;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($product_total,2));
		$item_pos+=17;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,$igst_rate.'%');
		$item_pos+=9;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($taxable_value,2));

		$item_pos+=20;
		$pdf->text($item_pos,$item_table_head_y_start+8+($i*8)+5,number_format($igst_amount,2));

		$i+=$k;
		$count+=$k;
		if($i+$k>=13)
		{
			footer($pdf,$data,$SalesReturn);
			$page++;
			header_section($pdf,$SalesReturn,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value);
			$i=-1;
			$count-=13;
		}
		$i++;
	}

	footer($pdf,$data,$SalesReturn);
}
$pdf->Output();
exit;

}
public function voucher_print($id=null)
{
$Journal=$this->Journal->find('first',array(
            'conditions'=>array(
            'Journal.id'=>$id,
            ),
            'order'=>array('Journal.date ASC'),
            'fields'=>array(
              'Journal.id',
              'Journal.remarks',
              'Journal.amount',
              'Journal.date',
              'Journal.voucher_no',
              'Journal.external_voucher',
              'Journal.work_flow',
              'Journal.created_at',
              'AccountHeadCredit.name',
              'AccountHeadCredit.opening_balance',
              'AccountHeadDebit.name',
              'AccountHeadDebit.opening_balance',
              
            ),
          ));
    $Profile=$this->Global_Var_Profile['Profile'];

    function convert_number_to_words($number) {
      $hyphen      = '-';
      $conjunction = ' and ';
      $separator   = ', ';
      $negative    = 'negative ';
      $decimal     = ' point ';
      $dictionary  = array(0=> 'zero',1=> 'one',2=> 'two',3=> 'three',4=> 'four',5=> 'five',6=> 'six',7=> 'seven',8=> 'eight',9=> 'nine',10=> 'ten',11=> 'eleven',12=> 'twelve',13=> 'thirteen',14=> 'fourteen',15=> 'fifteen',16=> 'sixteen',17=> 'seventeen',18=> 'eighteen',19=> 'nineteen',20=> 'twenty',30=> 'thirty',40=> 'fourty',50=> 'fifty',60=> 'sixty',70=> 'seventy',80=> 'eighty',90=> 'ninety',100=> 'hundred',1000=> 'thousand',1000000=> 'million',1000000000=> 'billion',1000000000000=> 'trillion',1000000000000000=> 'quadrillion',1000000000000000000=> 'quintillion');
      if (!is_numeric($number)) {
        return false;
      }
      if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
        trigger_error(
          'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
          E_USER_WARNING
        );
        return false;
      }
      if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
      }
      $string = $fraction = null;
      if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
      }
      switch (true) {
        case $number < 21:
        $string = $dictionary[$number];
        break;
        case $number < 100:
        $tens   = ((int) ($number / 10)) * 10;
        $units  = $number % 10;
        $string = $dictionary[$tens];
        if ($units) {
          $string .= $hyphen . $dictionary[$units];
        }
        break;
        case $number < 1000:
        $hundreds  = $number / 100;
        $remainder = $number % 100;
        $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
        if ($remainder) {
          $string .= $conjunction . convert_number_to_words($remainder);
        }
        break;
        default:
        $baseUnit = pow(1000, floor(log($number, 1000)));
        $numBaseUnits = (int) ($number / $baseUnit);
        $remainder = $number % $baseUnit;
        $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
        if ($remainder) {
          $string .= $remainder < 100 ? $conjunction : $separator;
          $string .= convert_number_to_words($remainder);
        }
        break;
      }
      if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
          $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
      }
      return $string;
    }
    require('fpdf/fpdf.php');
    $pdf = new FPDF('p', 'mm', [297, 210]);
    
  
    $igst_x_position=0;
   
    function header_section($pdf,$Profile,$igst_x_position,$Journal) 
        {
        $pdf->AddPage();
        $image_line_width=40;
        $invoice_x_starting=$image_line_width;
        $pdf->SetFont('Arial','B',12);
        $pdf->SetTextColor(0,0,100);
        $pdf->rect(7, 53, 197, 140);
        $pdf->SetXY($invoice_x_starting, 5);
        $invoice_pos=8;
        $pdf->SetXY($invoice_x_starting, 5);
        $invoice_pos+=10;
        $pdf->SetXY($invoice_x_starting, 5);
        $invoice_pos+=10;
        $gst_details_y_staring=53;
        $pdf->SetFont('Arial','B',8);
        $gst_pos=$gst_details_y_staring+7;
        $gst_line_width=160;
        $pdf->SetFont('Arial','B',15);
        $pdf->text(90,$gst_pos,'Journal Voucher');
        $pdf->SetFont('Arial','B',8);
        $pdf->text(9,$gst_pos+8,'Number :'.$Journal['Journal']['voucher_no']);
        $pdf->text($gst_line_width+1,$gst_pos+8,'Dated : '.date('d-m-Y',strtotime($Journal['Journal']['date'])));
        $customer_detail_startin=$gst_details_y_staring+6;
        $gst_pos+=4;
        $pdf->Line(7, $gst_details_y_staring+20,204, $gst_details_y_staring+20); // horizontal line
        $customer_detail_startin+=4;
        $customer_detail_startin+=4;
        $customer_detail_startin+=4;
        $item_table_head_y_start=$customer_detail_startin+2;

        // table head start
        $table_length=120;
        $pdf->text(30,$item_table_head_y_start+5,'Particulars');
        $table_column=90;
        $pdf->Line($table_column+7, $item_table_head_y_start,$table_column+7, $item_table_head_y_start+$table_length); // vertical line
        $pdf->text($table_column+25,$item_table_head_y_start+5,'Debit');
        $table_column+=55+$igst_x_position;
        $table_column+=6;
        $pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
        $pdf->text($table_column+25,$item_table_head_y_start+4,'Credit');
        $table_column+=15;
        $pdf->Line(7, $item_table_head_y_start+7,204, $item_table_head_y_start+7); // horizontal line
       $pdf->Line(7, $item_table_head_y_start+100,204, $item_table_head_y_start+100); // horizontal line

      

}
function footer($pdf,$data,$Journal)
{
  $footer_start_y=180;
  $footer_word_width=23;
  $pdf->text($footer_word_width,$footer_start_y,'On Account of:');
  $pdf->text($footer_word_width,$footer_start_y+7,'CD');
  $pdf->text($footer_word_width+90,$footer_start_y,floatval($Journal['Journal']['amount']));
  $pdf->text($footer_word_width+160,$footer_start_y,floatval($Journal['Journal']['amount']));
  $pdf->text($footer_word_width+140,$footer_start_y+30,"Authorised Signatory");

}

  header_section($pdf,$Profile,$igst_x_position,$Journal);
  $total=0; $item_pos=2;
   $data=array();
  $item_pos+=10+$igst_x_position;
   $item_table_head_y_start='75';
      $pdf->text($item_pos,$item_table_head_y_start+7+6,$Journal['AccountHeadDebit']['name']);
       $pdf->text($item_pos+100,$item_table_head_y_start+7+6,floatval($Journal['Journal']['amount']));
      $pdf->text($item_pos,$item_table_head_y_start+28,$Journal['AccountHeadCredit']['name']);
       $pdf->text($item_pos+170,$item_table_head_y_start+28,floatval($Journal['Journal']['amount']));
        $pdf->text($item_pos,$item_table_head_y_start+50,"Remarks : ".$Journal['Journal']['remarks']);
  footer($pdf,$data,$Journal);
$pdf->Output();
exit;

}
public function DailySummaryprint($day_register_id=null)
{

		$DayRegister=$this->DayRegister->find('first',array(
			'conditions'=>array(
				'DayRegister.id'=>$day_register_id,),
			'fields'=>array(
				'DayRegister.*',
				'Route.name',
				'CustomerGroup.name',
				 'Route.name',
				 'Executive.name',
			),
		));
				$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
 				$this->Sale->virtualFields = array( 'sale_amount' => "SUM(Sale.grand_total)",);
				$Sale=$this->Sale->find('first',array(
				'conditions'=>array(
					'Sale.day_register_id'=>$day_register_id,
					'Sale.status'=>2,
				),
				'fields'=>array(
					'sale_amount',
				),
			));

				$this->SalesReturn->virtualFields = array( 'return_amount' => "SUM(SalesReturn.grand_total)",);
				$this->SalesReturn->unbindModel(array('hasMany' => array('SalesReturnItem')));
				$SalesReturn=$this->SalesReturn->find('first',array(
				'conditions'=>array(
					'SalesReturn.day_register_id'=>$day_register_id,
				),
				'fields'=>array(
					'return_amount',
				),
				));
				$this->Sale->virtualFields = array( 'credit_sale_amount' => "SUM(Sale.grand_total)",);
				$CreditSale=$this->Sale->find('first',array(
				'conditions'=>array(
					'Sale.sale_type1'=>'CreditSale',
					'Sale.day_register_id'=>$day_register_id,
					'Sale.status'=>2,
				),
				'fields'=>array(
					'credit_sale_amount',
				),
			));
              $this->Journal->virtualFields = array('receipt_amount' => "SUM(Journal.amount)",);	
              $receipt_total=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.receipt_no !='=>'',
					'Journal.day_register_id'=>$day_register_id,
				),
				'fields'=>array(
					'receipt_amount',
				),
			));
              $ExecutiveBonusCalculation=$this->ExecutiveBonusCalculation->find('all',array(
				'conditions'=>array(
					'ExecutiveBonusCalculation.day_register_id'=>$day_register_id,
					 //'ExecutiveBonusCalculation.flag'=>1,
				),
				'fields'=>array(
					'ExecutiveBonusCalculation.*',
				),
			));
              $total_bonus=0;$total_bonus_return=0;
              foreach ($ExecutiveBonusCalculation as $key_bonus => $value_bonus)
              {
                    $total_bonus=$total_bonus+$value_bonus['ExecutiveBonusCalculation']['bonus_amount'];
                    $total_bonus_return=$total_bonus_return+$value_bonus['ExecutiveBonusCalculation']['bonus_return_amount'];
              }
              $total_bonus_payment=$total_bonus-$total_bonus_return;
          $DailySumary=[];
		$single['total_sale']=$Sale['Sale']['sale_amount'];
		$single['total_return']=$SalesReturn['SalesReturn']['return_amount'];
		$single['collection']=$receipt_total['Journal']['receipt_amount'];
		$single['credit_sale']=$CreditSale['Sale']['credit_sale_amount'];
		$single['bonus_earned']=$total_bonus_payment;
		array_push($DailySumary,$single);
	
$Checkstate=0;
require('fpdf/fpdf.php');
$pdf = new FPDF('p', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$DailySumary,$Profile,$total_page,$page,$Checkstate,$DayRegister) 
{
	$pdf->AddPage();
	$image_line_width=20;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(1, 1, 208, 95);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos=2;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	//$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
	$pdf->SetXY($invoice_x_starting, 5);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$pdf->Cell(0,$invoice_pos,'Daily Summary ',0,0,'C');
	$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring-2,209, $gst_details_y_staring-2); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
// $gst_pos+=8;
// $pdf->text(2,$gst_pos,'Production Number :1');
// $gst_pos+=4;
// $pdf->text(2,$gst_pos,'Order Date : '.date("d-m-Y"));

$gst_pos+=0;
$pdf->text(5,$gst_pos,'Executive : '.$DayRegister['Executive']['name']);
$gst_pos+=4;
$pdf->text(5,$gst_pos,'Route :'.$DayRegister['Route']['name']);
$gst_pos+=4;
$pdf->text(5,$gst_pos,'Date: '.date("d-m-Y",strtotime($DayRegister['DayRegister']['date'])));
$gst_pos+=4;
$pdf->Line(1, $gst_details_y_staring+17,209, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=52;

$table_length=44;
$pdf->text(5,$item_table_head_y_start+6,'Total Sale');
$table_column=35;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'Total Return ');
$table_column+=45;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'Credit Sale');
$table_column+=45;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+10,$item_table_head_y_start+6,'Collection');
$table_column+=45;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+10,$item_table_head_y_start+6,'Bonus Earned');
$table_column+=7;


}
function footer($pdf)
{}
header_section($pdf,$DailySumary,$Profile,$total_page,$page,$Checkstate,$DayRegister) ;
$total=0;
$item_table_head_y_start='60';
$count=count($DailySumary);
$i=0;
foreach ($DailySumary as $key => $value) {
$item_pos=7;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['total_sale']);
$item_pos+=45;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['total_return']);
$item_pos+=40;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['credit_sale']);
$item_pos+=47;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['collection']);
$item_pos+=45;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['bonus_earned']);
$i++;
}
footer($pdf);
$pdf->Output();
exit;


}
public function receipt_voucher_fpdf($id=null,$voucher_no=null,$amount=null){

	$journal_data=$this->Journal->find('first',array(
		'joins' => [
		[
		"table" => "customers",
		"alias" => "Customer",
		"type" => "LEFT",
		"conditions" => ['AccountHeadCredit.id=Customer.account_head_id'],
    ],],
		'fields'=>array('Customer.*','Journal.*','AccountHeadCredit.*','AccountHeadDebit.*'),
		'conditions'=>array('Journal.credit'=>$id,'Journal.voucher_no' =>$voucher_no),
		'order'=>array('Journal.id DESC')
    ));
    //print_r($journal_data);die;
	// $currency=$this->SystemParameter->field('value',array('id'=>6));
	$route_id=$journal_data['Customer']['route_id'];
	// $ExecutiveRouteMapping=$this->ExecutiveRouteMapping->findByRouteId($route_id);
	// if($ExecutiveRouteMapping)
	// {
	// 	$executive_id=$ExecutiveRouteMapping['ExecutiveRouteMapping']['executive_id'];
	// 	$Executive=$this->Executive->findById($executive_id);
	// 	$executive_name=$Executive['Executive']['name'];
  // }
  if($journal_data['Customer']['executive_id'] && $journal_data['Customer']['executive_id']>0){
    $executive_id=$journal_data['Customer']['executive_id'];
		$Executive=$this->Executive->findById($executive_id);
		$executive_name=$Executive['Executive']['name'];
  }
	else
	{
		$executive_name=" ";
	}
	$bank=2;

	$debit=0;
	$credit=0;
	$Journal_Debit=$this->Journal->find('all',array('conditions'=>array(
// 'credit'=>$modes,
		'debit'=>$id,
		'flag=1',
		)));
	foreach ($Journal_Debit as $key => $value) {
		$debit+=$value['Journal']['amount'];
	}
	$Journal_Credit=$this->Journal->find('all',array('conditions'=>array(
// 'debit'=>$modes,
		'credit'=>$id,
		'flag=1',
		)));
	foreach ($Journal_Credit as $key => $value) {
		$credit+=$value['Journal']['amount'];
	}
	$AccountHead=$this->AccountHead->find('all',array(
		'fields'=>array(
			'AccountHead.opening_balance',
			),
		'conditions'=>array('AccountHead.id'=>$id)
		));
	$total=$debit+$AccountHead[0]['AccountHead']['opening_balance'];
  $balance=$total-$credit;
	$currency=$this->SystemParameter->field('value',array('id'=>6));
	// $bank_name=$this->A->field('value',array('id'=>6));
	require('tcpdf/tcpdf.php');
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set document information
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Koopell');
	$pdf->SetTitle('Receipt Voucher');
	$pdf->SetSubject('TCPDF');
	$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
	$pdf->SetPrintHeader(false);
	$pdf->SetPrintFooter(false);

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 018', PDF_HEADER_STRING);
// set header and footer fonts
// $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
// $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
// $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
// $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
// $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language dependent data:
	$lg = Array();
	$lg['a_meta_charset'] = 'UTF-8';
// $lg['a_meta_dir'] = 'rtl';
	$lg['a_meta_language'] = 'fa';
// $lg['w_page'] = 'page';

// set some language-dependent strings (optional)
	$pdf->setLanguageArray($lg);

// ---------------------------------------------------------

// set font
// $pdf->SetFont('dejavusans', '', 12);
	$Profile=$this->Global_Var_Profile['Profile'];
	$page=1;
	$total_page=1;
	$i=0;
	function convert_number_to_words($number) {
		$hyphen      = '-';
		$conjunction = ' and ';
		$separator   = ', ';
		$negative    = 'negative ';
		$decimal     = ' point ';
		$dictionary  = array(
			0                   => 'zero',
			1                   => 'one',
			2                   => 'two',
			3                   => 'three',
			4                   => 'four',
			5                   => 'five',
			6                   => 'six',
			7                   => 'seven',
			8                   => 'eight',
			9                   => 'nine',
			10                  => 'ten',
			11                  => 'eleven',
			12                  => 'twelve',
			13                  => 'thirteen',
			14                  => 'fourteen',
			15                  => 'fifteen',
			16                  => 'sixteen',
			17                  => 'seventeen',
			18                  => 'eighteen',
			19                  => 'nineteen',
			20                  => 'twenty',
			30                  => 'thirty',
			40                  => 'fourty',
			50                  => 'fifty',
			60                  => 'sixty',
			70                  => 'seventy',
			80                  => 'eighty',
			90                  => 'ninety',
			100                 => 'hundred',
			1000                => 'thousand',
			1000000             => 'million',
			1000000000          => 'billion',
			1000000000000       => 'trillion',
			1000000000000000    => 'quadrillion',
			1000000000000000000 => 'quintillion'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
      // overflow
			trigger_error(
				'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}

	function convert_number_to_arabic_words($number)
	{
		$hyphen      = '-';
		$conjunction = ' و ';
		$separator   = ', ';
		$negative    = 'نفي ';
		$decimal     = ' نقطة ';
		$dictionary  = array(
			0                   => 'صفر',
			1                   => 'واحد',
			2                   => 'اثنان',
			3                   => 'ثلاثة',
			4                   => 'اربعة',
			5                   => 'خمسة',
			6                   => 'ستة',
			7                   => 'سبعة',
			8                   => 'ثمانية',
			9                   => 'تسعة',
			10                  => 'عَشْرة',
			11                  => 'أحد عشر',
			12                  => 'اثني عشر',
			13                  => 'ثلاثة عشر',
			14                  => 'أربعة عشرة',
			15                  => 'خمسة عشر',
			16                  => 'السادس عشر',
			17                  => 'سبعة عشر',
			18                  => 'الثامنة عشر',
			19                  => 'تسعة عشر',
			20                  => 'عشرون',
			30                  => 'ثلاثون',
			40                  => 'اربعون',
			50                  => 'خمسون',
			60                  => 'ستون',
			70                  => 'سبعون',
			80                  => 'ثمانون',
			90                  => 'تسعين',
			100                 => 'مائة',
			1000                => 'ألف',
			1000000             => 'مليون',
			1000000000          => 'مليار',
			1000000000000       => 'تريليون',
			1000000000000000    => 'الكدريليون رقم',
			1000000000000000000 => 'كوينتيليون'
			);
		if (!is_numeric($number)) {
			return false;
		}
		if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
			trigger_error(
				'convert_number_to_arabic_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
				E_USER_WARNING
				);
			return false;
		}
		if ($number < 0) {
			return $negative . convert_number_to_arabic_words(abs($number));
		}
		$string = $fraction = null;
		if (strpos($number, '.') !== false) {
			list($number, $fraction) = explode('.', $number);
		}
		switch (true) {
			case $number < 21:
			$string = $dictionary[$number];
			break;
			case $number < 100:
			$tens   = ((int) ($number / 10)) * 10;
			$units  = $number % 10;
			$string = $dictionary[$tens];
			if ($units) {
				$string .= $hyphen . $dictionary[$units];
			}
			break;
			case $number < 1000:
			$hundreds  = $number / 100;
			$remainder = $number % 100;
			$string = $dictionary[$hundreds] . ' ' . $dictionary[100];
			if ($remainder) {
				$string .= $conjunction . convert_number_to_arabic_words($remainder);
			}
			break;
			default:
			$baseUnit = pow(1000, floor(log($number, 1000)));
			$numBaseUnits = (int) ($number / $baseUnit);
			$remainder = $number % $baseUnit;
			$string = convert_number_to_arabic_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
			if ($remainder) {
				$string .= $remainder < 100 ? $conjunction : $separator;
				$string .= convert_number_to_arabic_words($remainder);
			}
			break;
		}
		if (null !== $fraction && is_numeric($fraction)) {
			$string .= $decimal;
			$words = array();
			foreach (str_split((string) $fraction) as $number) {
				$words[] = $dictionary[$number];
			}
			$string .= implode(' ', $words);
		}
		return $string;
	}
	$igst_x_position=0;
    	$bank_name=$this->AccountHead->field('name',array('id'=>$journal_data['Journal']['debit']));
	function header_section($pdf,$Profile,$total_page,$page,$journal_data,$amount,$balance,$debit,$credit,$total,$executive_name,$currency,$bank_name
		){
		$pdf->AddPage();
		//$pdf->Image('profile/'.$Profile['logo'],3,9,40);
		$image_line_width=45;
		$invoice_x_starting=$image_line_width;
//$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(0,0,0);
		$pdf->rect(3, 9, 203,145);
		$item_table_head_y_start=10;
		$pdf->SetDrawColor('150','150','150');
		//$pdf->Line($image_line_width, $item_table_head_y_start,$image_line_width, $item_table_head_y_start+40);
		$invoice_pos=0;
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(92,$invoice_pos,"RECEIPT VOUCHER");
		//$recept="سند القبض";
		//$pdf->text(125,$invoice_pos,$recept);

		$invoice_pos+=2;
		$pdf->SetFont('dejavusans', 'B', 10);
		$pdf->text(94,$invoice_pos+3,strtolower($Profile['company_name']));
		//$pdf->text(155,$invoice_pos+3,$Profile['company_name_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
		$pdf->text(97,$invoice_pos+3,$Profile['address_line_1']);
		//$pdf->text(140,$invoice_pos+3,$Profile['address_line_1_arabic'] );

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('dejavusans', '', 10);
    $pdf->text(83,$invoice_pos+3,$Profile['address_line_2']);
    $invoice_pos+=6;
		$pdf->text(90,$invoice_pos+3,'Mobile : '.$Profile['mobile']);
		//$pdf->text(160,$invoice_pos+3,$Profile['address_line_2_arabic']);

		$pdf->SetXY($invoice_x_starting, 5);
		$invoice_pos+=6;
		$pdf->SetFont('times', '', 12, "", 'false');
		$pdf->text(82,$invoice_pos+3,'Email : '.$Profile['mail_address']);
		$invoice_pos+=10;
		$pdf->SetFont('dejavusans', '', 10);
		$pdf->text(82,$invoice_pos,'GST Number : '.$Profile['vat_code']);
		//$VAT_Number='ظريبه الشراء';
		//$pdf->text(140,$invoice_pos,$VAT_Number);

		$gst_details_y_staring=52;
$pdf->Line(3, $gst_details_y_staring,206, $gst_details_y_staring); // horizontal line
//$pdf->Line(45, $gst_details_y_staring,45, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos=$gst_details_y_staring+2;
// $currency=$this->SystemParameter->field('value',array('id'=>6));
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(3,$gst_pos,'Customer Name:');
$pdf->text(3,$gst_pos+7,'Customer No:');
$pdf->text(3,$gst_pos+14,'GST Number:');
$pdf->text(3,$gst_pos+21,'Address:');
$pdf->text(3,$gst_pos+28,'Receipt No:');
$pdf->text(3,$gst_pos+35,'Date:');
$pdf->text(150,$gst_pos,'Currency:');
$pdf->text(180,$gst_pos,$currency);
$pdf->text(150,$gst_pos+7,'Document No:');
$pdf->text(180,$gst_pos+7,$journal_data['Journal']['voucher_no']);
if(!empty($journal_data['Journal']['cheque_no'])){
$pdf->text(150,$gst_pos+14,'Bank Name:');
$pdf->text(180,$gst_pos+14,$bank_name);
$pdf->text(150,$gst_pos+28,'Cheque No:');
$pdf->text(180,$gst_pos+28,$journal_data['Journal']['cheque_no']);
$pdf->text(140,$gst_pos+35,'Cheque Date:');
$pdf->text(180,$gst_pos+35,date("d-m-Y",strtotime($journal_data['Journal']['cheque_date'])));
}elseif(!empty($journal_data['Journal']['reference_no'])){
$pdf->text(150,$gst_pos+14,'Bank Name:');
$pdf->text(180,$gst_pos+14,$bank_name);
$pdf->text(150,$gst_pos+21,'Reference No:');
$pdf->text(180,$gst_pos+21,$journal_data['Journal']['reference_no']);
//$pdf->text(140,$gst_pos+28,'Cheque No:');
//$pdf->text(140,$gst_pos+35,'Cheque Date:');
}
$gst_line_width=120;
//$pdf->Line($gst_line_width+40, $gst_details_y_staring,$gst_line_width+40, $gst_details_y_staring+17+21+5);
$pdf->SetFont('dejavusans', '', 10);
$pdf->text($gst_line_width-295,$gst_pos,$journal_data['AccountHeadCredit']['name']);
//$pdf->text($gst_line_width+2,$gst_pos,$journal_data['Customer']['arabic_name']);
$pdf->text($gst_line_width-295,$gst_pos+7,$journal_data['Customer']['mobile']);
$pdf->text($gst_line_width-295,$gst_pos+14,$journal_data['Customer']['gstin']);
$pdf->text($gst_line_width-295,$gst_pos+21,$journal_data['Customer']['place']);
$pdf->text($gst_line_width-295,$gst_pos+28,$journal_data['Journal']['receipt_no']);
$pdf->text($gst_line_width-295,$gst_pos+35,date("d-m-Y",strtotime($journal_data['Journal']['date'])));
// if(!empty($journal_data['Journal']['cheque_no'])){
// 	$payment_method="Cheque";
// 	$Bank="";
// }elseif(!empty($journal_data['Journal']['reference_no'])){
// 	$payment_method="Bank";
// }else{
// 	$payment_method="Cash";
// }
$pdf->SetFont('dejavusans', '', 10);
// $pdf->text($gst_line_width+1+66,$gst_pos,'اسم العميل');
// $pdf->text($gst_line_width+1+66,$gst_pos+7,'رقم العميل');
// $pdf->text($gst_line_width+1+66,$gst_pos+14,'رقم الضريبة');
// $pdf->text($gst_line_width+1+73,$gst_pos+21,'عنوان.');
// $pdf->text($gst_line_width+1+67,$gst_pos+28,'عدد إيصال');
// $pdf->text($gst_line_width+1+76,$gst_pos+35,'تاريخ');
$pdf->Line(3, $gst_details_y_staring+17+5+21,206, $gst_details_y_staring+17+5+21); // horizontal line
$table_length=25;
$item_table_head_y_start=$gst_details_y_staring+43;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(10,$item_table_head_y_start+1,'Total Amount');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text(10,$item_table_head_y_start+7, 'المبلغ الإجمالي');
// $pdf->text(10,$item_table_head_y_start+17,round($balance+$amount).' AED');
$pdf->text(10,$item_table_head_y_start+17,number_format($balance+$amount,2));
$table_column=50;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->Line($table_column+50, $item_table_head_y_start,$table_column+50, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+10,$item_table_head_y_start+1,'Payment Method');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text($table_column+10,$item_table_head_y_start+7, 'طريقة الدفع');
// if($journal_data['Journal']['debit'] == 1){
// 	$payment_method="Cash";
// 	$Bank="";
// }else{
// 	$payment_method="Bank";
// }
if(!empty($journal_data['Journal']['cheque_no'])){
	$payment_method="Cheque";
	$Bank="";
}elseif(!empty($journal_data['Journal']['reference_no'])){
	$payment_method="Bank";
}else{
	$payment_method="Cash";
}
$pdf->text($table_column+13,$item_table_head_y_start+17,$payment_method);
$pdf->Line($table_column+5, $item_table_head_y_start,$table_column+5, $item_table_head_y_start+$table_length); // vertical line
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($table_column+60,$item_table_head_y_start+1,'Paid Amount');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text($table_column+60,$item_table_head_y_start+6,'المبلغ المدفوع');
$pdf->text($table_column+60,$item_table_head_y_start+17,$amount);
$table_column+=100;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text($table_column+10,$item_table_head_y_start+1,'Balance Amount');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->Text($table_column+10,$item_table_head_y_start+6,'مقدار وسطي');
$pdf->text($table_column+10,$item_table_head_y_start+17,number_format($balance,2));

$pdf->Line(3, $gst_details_y_staring+17+5+21+13,206, $gst_details_y_staring+17+5+21+13); // horizontal line
$item_table_head_y_start=$gst_details_y_staring+68;
$table_column=45;
$pdf->SetFont('times', '', 10, "", 'false');
$pdf->text(3,$item_table_head_y_start+3,'Amount In Words');
$convert_number_to_words=strtoupper(convert_number_to_words(floatval($amount))).' RUPEES ONLY';
$pdf->text(46,$item_table_head_y_start+3,$convert_number_to_words);
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length-7); // vertical line
//$pdf->Line($table_column+115, $item_table_head_y_start,$table_column+115, $item_table_head_y_start+$table_length-7); // vertical line
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(181,$item_table_head_y_start+8,'المبلغ بالكلمات');
//$convert_number_to_arabic_words=strtoupper(convert_number_to_arabic_words(floatval($amount)));
//$pdf->text(100,$item_table_head_y_start+8,$convert_number_to_arabic_words);
$pdf->Line(3, $gst_details_y_staring+17+5+21+25,206, $gst_details_y_staring+17+5+21+25); // horizontal line
$pdf->SetFont('times', '', 10, "", 'false');
$table_column=45;
$item_table_head_y_start=$gst_details_y_staring+86;
$pdf->text(3,$item_table_head_y_start+3,'Received By:');
$pdf->SetFont('dejavusans', '', 10);
//$pdf->text(72,$item_table_head_y_start+3,'استلمت من قبل:');
//$pdf->Line($table_column+55, $item_table_head_y_start,$table_column+55, $item_table_head_y_start+$table_length-7); // vertical line
// $pdf->SetFont('times', '', 10, "", 'false');
// $pdf->text(100,$item_table_head_y_start+3,'Salesman:');
// $pdf->text(120,$item_table_head_y_start+3,$executive_name);
$pdf->SetFont('dejavusans', '', 6);
//$pdf->text(195,$item_table_head_y_start+3,'بائع:');

$pdf->Line(3, $gst_details_y_staring+17+5+21+43,206, $gst_details_y_staring+17+5+21+43); // horizontal line
$pdf->text(90,$item_table_head_y_start+16,date('d/m/Y - H:i:A'));
//$table_column+=70+$igst_x_position;
}
// function footer($pdf,$Profile,$total)
// {
//     $footer_start_y=115;
//     $footer_position_y=0;
//     $footer_position_y+=6;
//     $pdf->SetFont('Arial','I',10);
//     $pdf->rect(5, $footer_start_y, 198,11);
//     $pdf->text(6,$footer_start_y+3,'Received By');
//     $footer_position_y+=40;
//     $pdf->Line($footer_position_y, $footer_start_y,$footer_position_y, $footer_start_y+11);
//     $footer_position_y+=2;
//     $pdf->text($footer_position_y,$footer_start_y+3,'Copy');
//     $pdf->SetFont('Arial','B',10);
//     // $pdf->text($footer_position_y,$footer_start_y+8,'(Passenger Copy)');
//     $footer_position_y+=40;
//     $pdf->Line($footer_position_y, $footer_start_y,$footer_position_y, $footer_start_y+11);
//     $pdf->SetFont('Arial','I',10);
//     $footer_position_y+=2;
//     $pdf->text($footer_position_y,$footer_start_y+3,'Prepared By');
//     $pdf->SetFont('Arial','B',10);
//     $pdf->text($footer_position_y,$footer_start_y+8,'');
//     $footer_position_y+=80;
//     $pdf->Line($footer_position_y, $footer_start_y,$footer_position_y, $footer_start_y+11);
//     $pdf->SetFont('Arial','I',10);
//     $footer_position_y+=2;
//     $pdf->text($footer_position_y,$footer_start_y+3,'Printed By');
//     $pdf->SetFont('Arial','B',10);
//     $pdf->text($footer_position_y,$footer_start_y+8,'');
// }       
header_section($pdf,$Profile,$total_page,$page,$journal_data,$amount,$balance,$debit,$credit,$total,$executive_name,$currency,$bank_name);
$pdf->Output();
exit;
}
}