<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Stock');
App::import('Controller', 'Purchase');
App::import('Controller','Sale');
App::uses('BarcodeHelper','Vendor');
ini_set('memory_limit', '256M');
class ProductController extends AppController {
  public $helpers= array('Html','Form');
  public $uses=array(
    'Product',
    'Brand',
    'ProductType',
    'Unit',
    'Stock',
    'SaleProductsOffline',
    'PurchasedItem',
    'Sale',
    'SaleItem',
    'StockLog', 
    'CustomerDiscount',
    'TypeOfProduct'
    );
  public function index()
  {
    // $this->ProductType->virtualFields = array(
    //   'product_type_name' => "CONCAT(ProductType.name, ' ', ProductType.code)",
    //   );
    $ProductType=$this->ProductType->find('all',array(
      'fields'=>array(
        'ProductType.id',
        'ProductType.name',
        )
      )
    );
    foreach ($ProductType as $key => $value) {
      foreach ($value['Product'] as $keyP => $valueP) {
        $unit=$this->Unit->field('name',array('id'=>$valueP["unit_id"]));
        $ProductType[$key]['Product'][$keyP]['unit_name']=$unit;
      }
    }
    $this->set('Product',$ProductType);
    $Brand_list=$this->Brand->find('list',array(
      'fields'=>array('id','name'),
      'order'=>array('name ASC'),
      ));
    $Brand_list['G']='GENERAL';
    ksort($Brand_list);
    $this->set('Brand',$Brand_list);
    $ProductType_List = $this->ProductType->find('list', array(
      'fields' => array('id','name'),
      'order'=>array('name ASC'),
      )
    );
    $this->set('ProductType',$ProductType_List);
     $TypeOfProduct = $this->TypeOfProduct->find('list', array(
      'fields' => array('id','name'),
      'order'=>array('name ASC'),
      )
    );
    $this->set('Type',$TypeOfProduct);
    $this->Product->unbindModel(array('belongsTo'=>array('ProductType','Brand','Unit'),'hasMany' => array('PurchaseReturnItem','UnwantedList','Stock','StockLog','SalesReturnItem','SaleItem','PurchasedItem','Unit','Brand')));
    $LastBarcode=$this->Product->find('first',array('conditions'=>array('Product.custom_barcode_selection'=>'on'),'fields'=>array('Product.barcode'),'order'=>'Product.barcode DESC'));
    $this->Product->virtualFields = array(
      'product_name' => "CONCAT(Product.name, ' ', Product.code)"
      );
    if($LastBarcode){
      if(empty($LastBarcode['Product']['barcode']))
      {
        $barcode_last=1111111111111;
      }
      else
      {
        $barcode_last=$LastBarcode['Product']['barcode']+1;
      }

    }
    else{
      $barcode_last=1111111111111;
    }
    $barcode_check=$this->Product->findByBarcode($barcode_last);
    while ($barcode_check) {
      $barcode_last++;
      $barcode_check=$this->Product->findByBarcode($barcode_last);
    }
    $this->set('LastBarcode',$barcode_last);
    $Product_list = $this->Product->find('list', array(
      'fields' => array('id','product_name'),
      'order' => array('name ASC'),
      ));

    $this->set('product_name',$Product_list);
    $unit_name = $this->Unit->find('list', array('fields' => array('id','name')));
    $this->set('unit_name',$unit_name);
   $sub_group_id=1;
    $PurchaseController = new PurchaseController;
    $Accounting_function_return=$PurchaseController->AccountHead_Option_ListBySubGroupId($sub_group_id);
    //$Accounting_function_return['0']='Select';
    $this->set('PartyList',$Accounting_function_return);
  }
  public function Product_Table_ajax()
  {
    $requestData=$this->request->data;
    $columns = [];
    $columns[]='ProductType.name';
    $columns[]='TypeOfProduct.name';
    $columns[]='Product.name';
    $columns[]='Product.code';
    $columns[]='Unit.name';
    $columns[]='Product.no_of_piece_per_unit';
    $columns[]='Product.wholesale_price';
    $columns[]='Product.mrp';
    $columns[]='Product.cost';
    $columns[]='Product.priority';
    $columns[]='Product.id';
    $conditions=[];
    if($requestData['product_type'])
    {
      $conditions['ProductType.id']=$requestData['product_type'];
    }
    if($requestData['type_products'])
    {
      $conditions['TypeOfProduct.id']=$requestData['type_products'];
    }
    if($requestData['product'])
    {
      $conditions['Product.id']=$requestData['product'];
    }
    $this->Product->unbindModel(array('hasMany' => array('SaleItem','PurchasedItem','Stock','StockLog','PurchaseReturnItem','SalesReturnItem')));
    $totalData=$this->Product->find('count',['conditions'=>$conditions]);

    $totalFiltered=$totalData;
    if( !empty($requestData['search']['value']) ) { 
      $q=$requestData['search']['value'];
      $conditions['OR']=array(
        'TypeOfProduct.name LIKE' =>'%'. $q . '%',
        'Product.name LIKE' =>'%'. $q . '%',
        'Product.code LIKE' =>'%'. $q . '%',
        'ProductType.name LIKE' =>'%'. $q . '%',
        'Product.barcode LIKE' =>'%'. $q . '%',
        'Unit.name LIKE' =>'%'. $q . '%',
        'Product.no_of_piece_per_unit LIKE' =>'%'. $q . '%',
        'Product.wholesale_price LIKE' =>'%'. $q . '%',
        'Product.mrp LIKE' =>'%'. $q . '%',
         'Product.priority LIKE' =>'%'. $q . '%',
      );
      $totalFiltered=$this->Product->find('count',[
        'conditions'=>$conditions,
      ]);
    }
    $this->Product->unbindModel(array('hasMany' => array('SaleItem','PurchasedItem','Stock','StockLog','PurchaseReturnItem','SalesReturnItem')));
    $Data=$this->Product->find('all',array(
       'conditions'=>$conditions,
       'offset'=>$requestData['start'],
       'limit'=>$requestData['length'],
       'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
      'fields'=>array(
        'Product.*',
        'ProductType.name',
        'TypeOfProduct.name',
        'Unit.name',
      )
    ));

    foreach ($Data as $key => $value) {
      if($value['Product']['active'])
    {
      $inactive_active = 0;
      $title ='active';
      $icon = 'fa-unlock-alt fa-2x';
    }
    else
    {
      $inactive_active = 1;
      $title ='inactive';
      $icon = 'fa-lock fa-2x';
    }
      $site=$this->webroot.'Product/BarcodePrintSingle/'.$value["Product"]["barcode"];
      $Data[$key]['Product']['wholesale_price_in_cases'] =$value['Product']['wholesale_price']*$value['Product']['no_of_piece_per_unit'];
      if(!empty($value['Product']['barcode']))
        $Data[$key]['Product']['barcode'] ='<span>'.$value['Product']['barcode'].'</span>&nbsp;&nbsp;<span class="Print"><a target="_blank" href="'.$site.'"><i class="fa fa-print" aria-hidden="true"></i></a></span>';
      $Data[$key]['Product']['no_of_piece_per_unit'] =floatval($value['Product']['no_of_piece_per_unit']);
      $Data[$key]['Product']['mrp_in_cases'] =$value['Product']['mrp']*$value['Product']['no_of_piece_per_unit'];
      $Data[$key]['Product']['mrp'] =number_format($value['Product']['mrp'],2,'.','');
        $Data[$key]['Product']['cost'] =number_format($value['Product']['cost'],2,'.','');
          $Data[$key]['Product']['wholesale_price'] =number_format($value['Product']['wholesale_price'],2,'.','');
      $Data[$key]['Product']['action'] ='<span><i table_id="'.$value['Product']['id'].'" class="fa fa-2x fa-edit   edit_Product   blue-col"></i></span>&nbsp;&nbsp;';
      // $Data[$key]['Product']['action'] .="<span><i table_id='".$value['Product']['id']."' class='fa fa-2x fa-trash delete_Product blue-col'></i></span>";
          $Data[$key]['Product']['priority']='<input class="form-control number priority_edit" style="width:50px" type="text" value="'.$value['Product']['priority'].'"><i table_id="'.$value['Product']['id'].'" class="fa fa-check fa-lg blue-col edit_priority" ></i>';

        $Data[$key]['Product']['action'].='<span><a title="'.$title.'"href="'.$this->webroot.'Product/InactiveActiveProduct/'.$value['Product']['id'].'/'.$inactive_active.'"><i class="fa '.$icon.' blue-col blue-col"></i></a></span>';

    }
    $json_data = array(
      "draw"           =>intval($requestData['draw']),
      "recordsTotal"   =>intval($totalData),
      "recordsFiltered"=>intval($totalFiltered),
      "records"        =>$Data
    );
    echo json_encode($json_data); exit;
  }
  public function update_product_priority($id,$priority)
  {
          $return['status']="Empty";

      $this->Product->id=$id;
      if(!$this->Product->saveField('priority',$priority))
      {
      throw new Exception("Cant Updated priority", 1);
  
      }
      else
      {
      $return['status']="Success";  
      }
       echo json_encode($return);exit;
    }
  public function InactiveActiveProduct($id,$flag)
{
  try {

    $this->Product->id=$id;
    if(!$this->Product->saveField('active',$flag))
      throw new Exception("Cant active This Product", 1);
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  $this->Session->setFlash(__($return['result']));
  $this->redirect( Router::url( $this->referer(), true ) );
}
  public function product_search_ajax(){
    $ProductType_conditions=array();
    $Product_conditions=array();
    $request=$this->request->data;
    $ProductType=array();
    $Product=array();
    if(!empty($request['product_type']))
    {
      $ProductType_conditions['ProductType.id']=$request['product_type'];
      $ProductType=$this->ProductType->find('all',array(
        'conditions'=>$ProductType_conditions,
        ));
    }
    if(!empty($request['product_name']))
    {
      $ProductType=array();
      $Product_conditions['Product.id']=$request['product_name'];
      $this->Product->virtualFields = array(
        'product_name' => "CONCAT(Product.name, ' ', Product.code)",
        'product_type_name' => "CONCAT(ProductType.name, ' ', ProductType.code)"
        );
      $Product=$this->Product->find('first',array(
        'conditions'=>$Product_conditions,
        'fields'=>array(
          'Product.id',
          'Product.name',
          'Product.barcode',
          'Product.product_name',
          'Product.no_of_piece_per_unit',
          'Product.wholesale_price',
          'Product.code',
          'Product.cost',
          'Product.mrp',
          'Product.tax',
          'Product.threashold',
          'ProductType.id',
          'ProductType.name',
          'ProductType.code',
          'Unit.*',
          'Brand.id',
          )
        )
      );

    }
    else
    {
      $this->ProductType->virtualFields = array(
        'product_type_name' => "CONCAT(ProductType.name, ' ', ProductType.code)"
        );
      $ProductType=$this->ProductType->find('all',array(
        'conditions'=>$ProductType_conditions,
        ));
    }
    $data['row']='';
    if($ProductType)
    {
      foreach ($ProductType as $keyPT => $valuePT) {
        if(!empty($valuePT['Product'])){
          foreach ($valuePT['Product'] as $keyP => $valueP) {
            $data['row'].='<tr class="blue-pd">';
            $data['row'].='<td class="product_type_name">'.$valuePT["ProductType"]["product_type_name"].'</td>';
            $data['row'].='<td style="display:none" class="Product_type_hidden_feilds">';
            $data['row'].='<span class="product_type_id">'.$valuePT["ProductType"]["id"].'</span>';
            $data['row'].='</td>';
            $data['row'].='<td class="product_type_action">';
            $data['row'].='<span><a href="#"><i class="fa fa-edit edit_new_btn ad-mar product_type_edit"> </i></a></span> &nbsp; &nbsp; &nbsp;';
            $data['row'].='<span><a href="#"><i class="fa fa-trash ad-mar product_type_delete"></i></a></span>';
            $data['row'].='</td>';
            $data['row'].='<td class="Product_name">'.$valueP["name"].' '.$valueP["code"].'</td>';
            $data['row'].='<td class="Product_barcode">'.$valueP["barcode"].'</td>';
            $data['row'].='<td style="display:none" class="Product_hidden_feilds">';
            $data['row'].='<span class="Product_id">'.$valueP["id"].'</span>';
            $data['row'].='</td>';
            $unit=$this->Unit->field('name',array('id'=>$valueP["unit_id"]));

            $data['row'].='<td class="Product_unit">'.$unit.'</td>';
            $data['row'].='<td class="Product_piece">'.$valueP["no_of_piece_per_unit"].'</td>';
            $data['row'].='<td class="Product_wholesale">'.$valueP["wholesale_price"].'</td>';
            $data['row'].='<td class="Product_retail">'.$valueP["mrp"].'</td>';
            $data['row'].='<td class="Product_wholesale">'.$valueP["wholesale_price"]*$valueP["no_of_piece_per_unit"].'</td>';
            $data['row'].='<td class="Product_retail">'.$valueP["mrp"]*$valueP["no_of_piece_per_unit"].'</td>';
            $data['row'].='<td class="product_action">';
            $data['row'].='<span><a href="#"><i class="fa fa-edit edit_new_btn ad-mar  product_edit"> </i></a></span> &nbsp; &nbsp; &nbsp;';
            $data['row'].='<span><a href="#"><i class="fa fa-trash ad-mar product_delete"></i></a></span>';
            $data['row'].='</td>';
            $data['row'].='</tr>';
          }
        }
        else
        {
          $data['row'].='<tr class="blue-pd">';
          $data['row'].='<td class="product_type_name">'.$valuePT["ProductType"]["product_type_name"].'</td>';
          $data['row'].='<td style="display:none" class="Product_type_hidden_feilds">';
          $data['row'].='<span class="product_type_id">'.$valuePT["ProductType"]["id"].'</span>';
          $data['row'].='</td>';
          $data['row'].='<td class="product_type_action">';
          $data['row'].='<span><a href="#"><i class="fa fa-edit edit_new_btn ad-mar product_type_edit"> </i></a></span> &nbsp; &nbsp; &nbsp;';
          $data['row'].='<span><a href="#"><i class="fa fa-trash ad-mar product_type_delete"></i></a></span>';
          $data['row'].='</td>';
          $data['row'].='<td></td>';
          $data['row'].='<td style="display:none" class="Product_hidden_feilds"></td>';
          $data['row'].='<td></td>';
          $data['row'].='<td></td>';
          $data['row'].='<td></td>';
          $data['row'].='<td></td>';
          $data['row'].='<td></td>';
          $data['row'].='<td></td>';
          $data['row'].='<td></td>';
          $data['row'].='<td></td>';
          $data['row'].='</tr>';
        }
      }
      $data['result']='Success';
    }
    else if($Product)
    {
      $data['row'].='<tr class="blue-pd">';
      $data['row'].='<td class="product_type_name">'.$Product["ProductType"]["name"].' '.$Product["ProductType"]["code"].'</td>';
      $data['row'].='<td style="display:none" class="Product_type_hidden_feilds">';
      $data['row'].='<span class="product_type_id">'.$Product["ProductType"]["id"].'</span>';
      $data['row'].='</td>';
      $data['row'].='<td class="product_type_action">';
      $data['row'].='<span><a href="#"><i class="fa fa-edit edit_new_btn ad-mar product_type_edit"> </i></a></span> &nbsp; &nbsp; &nbsp;';
      $data['row'].='<span><a href="#"><i class="fa fa-trash ad-mar product_type_delete"></i></a></span>';
      $data['row'].='</td>';
      $data['row'].='<td class="Product_name">'.$Product["Product"]["product_name"].'</td>';
      $data['row'].='<td class="Product_barcode">'.$Product["Product"]["barcode"].'</td>';
      $data['row'].='<td style="display:none" class="Product_hidden_feilds">';
      $data['row'].='<span class="Product_id">'.$Product["Product"]["id"].'</span>';
      $data['row'].='</td>';
      $data['row'].='<td class="Product_unit">'.$Product["Unit"]["name"].'</td>';
      $data['row'].='<td class="Product_piece">'.$Product["Product"]["no_of_piece_per_unit"].'</td>';
      $data['row'].='<td class="Product_wholesale">'.$Product["Product"]["wholesale_price"].'</td>';
      $data['row'].='<td class="Product_retail">'.$Product["Product"]["mrp"].'</td>';
      $data['row'].='<td class="Product_wholesale">'.$Product["Product"]["wholesale_price"]*$Product["Product"]["no_of_piece_per_unit"].'</td>';
      $data['row'].='<td class="Product_retail">'.$Product["Product"]["mrp"]*$Product["Product"]["no_of_piece_per_unit"].'</td>';
      $data['row'].='<td class="product_action">';
      $data['row'].='<span><a href="#"><i class="fa fa-edit edit_new_btn ad-mar  product_edit"> </i></a></span> &nbsp; &nbsp; &nbsp;';
      $data['row'].='<span><a href="#"><i class="fa fa-trash ad-mar product_delete"></i></a></span>';
      $data['row'].='</td>';
      $data['row'].='</tr>';
    }
    else
    {
      $data['result']='Error';
    }
    echo json_encode($data);
    exit;
  }
  public function get_Product_ajax($id,$party,$unit_id=null,$warehouse_id=null)
  { 
    $PurchasedItem=$this->PurchasedItem->find('all',array(
      "joins" => array(
        ),
      'conditions'=>array(
        'PurchasedItem.product_id'=>$id,
        ),
      'order' => array('Purchase.id DESC'),
      'limit'=>1,
      'fields'=>array(
        'Purchase.invoice_no',
        'Purchase.date_of_purchase',
        'PurchasedItem.unit_cost',
        'PurchasedItem.quantity',
        'Product.no_of_piece_per_unit',
        )
      ));
    $Product=$this->Product->find('first',array(
      'conditions'=>array('Product.id'=>$id)
      ));
    if(!empty($Product))
    {
      $stock_list=$this->Stock->find('first',array(

        'conditions'=>array(
          'Stock.product_id'=>$id,
          ),
        'fields' => array(
          'Stock.quantity',
          'Product.no_of_piece_per_unit',
          )
        ));
      if($unit_id=='null' || $unit_id==0)
      {
        $unit_id=$Product['Product']['unit_id'];
      }
      $return['unit_id']=$unit_id;
      if($unit_id){
        $Unit=$this->Unit->findById($unit_id);
        $unit_level=$Unit['Unit']['level'];
      }
      $SaleController=new SaleController;
      $UnitLevelConvert=$SaleController->UnitLevelConvert($id,$unit_id);

      $sale_unit_level=$UnitLevelConvert['sale_unit_level'];
      $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
      $product_unit_level=$UnitLevelConvert['product_unit_level'];
      if($sale_unit_level != 1){
        if($sale_unit_level == 2){
          $Product['Product']['cost']=$Product['Product']['cost']*$no_of_piece_per_unit;
          $Product['Product']['mrp']=$Product['Product']['mrp']*$no_of_piece_per_unit;
          $Product['Product']['wholesale_price']=$Product['Product']['wholesale_price']*$no_of_piece_per_unit;
        }
      }
      $Product['Product']['level']=$unit_level;
      $Product['Product']['purchase_unit_level']=$unit_level;
      $Product['Product']['product_unit_level']=$Product['Unit']['level'];
      $return['result']='Success';
      $return['Product']=$Product['Product'];
      if($PurchasedItem){
        $return['lastunitcost']=$PurchasedItem[0]['PurchasedItem']['unit_cost']*$PurchasedItem[0]['Product']['no_of_piece_per_unit'];
      }
      else
      {
        $return['lastunitcost']=0;
      }
      $return['available_quantity']=$stock_list['Stock']['quantity']; 
    }
    else
    {
      $return['result']='Empty';
    }
    //pr($return);exit;
    echo json_encode($return);
    exit;
  }
  public function get_product_unit($id)
  {
    $return['unit_name']="";
    $return['unit_id']="";
     $return['result']='Empty';
     $Product=$this->Product->find('first',array(
      'conditions'=>array('Product.id'=>$id)
      ));
     if($Product)
     {
       $return['result']='Success';
     $return['unit_name']=$Product['Unit']['name'];
     $return['unit_id']=$Product['Unit']['id'];
     }
     echo json_encode($return);
    exit;
  }
  public function get_Product_ajax_sale($id,$party=null,$unit_id=null,$warehouse_id=null)
  { 
    $this->Product->unbindModel(array('hasMany' => array('Stock','StockLog','SaleItem','SalesReturnItem','PurchasedItem','PurchaseReturnItem')));
    $Product=$this->Product->find('first',array(
      'conditions'=>array('Product.id'=>$id)
      ));
    $CustomerDiscount=$this->CustomerDiscount->find('first',array(
      'conditions'=>array(
        'account_head_id'=>$party,
        'product_id'=>$id
        )));
    $SaleItem=$this->SaleItem->find('first',array(
      "joins" => array(
        ),
      'conditions'=>array(
        'SaleItem.product_id'=>$id,
        'Sale.account_head_id'=>$party
        ),
      'order' => array('Sale.id DESC'),
      'fields'=>array(
        'SaleItem.unit_price',
        )
      ));
    if(!empty($Product))
    {
      $stock_list=$this->Stock->find('first',array(
        'conditions'=>array(
          'Stock.product_id'=>$id,
          'Stock.warehouse_id'=>1,
          ),
        'fields' => array(
          'Stock.quantity',
          'Product.no_of_piece_per_unit',
          )
        ));
      if($unit_id=='null' || $unit_id==0)
      {
        $unit_id=$Product['Product']['unit_id'];
      }
      $return['unit_id']=$unit_id;
      if($unit_id){
        $Unit=$this->Unit->findById($unit_id);
        $unit_level=$Unit['Unit']['level'];
      }
      if($CustomerDiscount){
        $Product['Product']['selling_rate']=$CustomerDiscount['CustomerDiscount']['selling_rate']/$CustomerDiscount['Product']['no_of_piece_per_unit'];
      }else{
        $Product['Product']['selling_rate']=0;
      } 
      if(empty($unit_level))
      {
      }
       if(!empty($SaleItem)){
        $Product['Product']['lastunitcost']=number_format($SaleItem['SaleItem']['unit_price'],2);
      }
      else
      {
        $Product['Product']['lastunitcost']=number_format($Product['Product']['wholesale_price'],2);
      }
      if ($unit_level==2) {
        $Product['Product']['cost']=$Product['Product']['cost']*$Product['Product']['no_of_piece_per_unit'];
        $Product['Product']['mrp']=$Product['Product']['mrp']*$Product['Product']['no_of_piece_per_unit'];
        $Product['Product']['wholesale_price']=$Product['Product']['wholesale_price']*$Product['Product']['no_of_piece_per_unit']; 
        $Product['Product']['selling_rate']=$Product['Product']['selling_rate']*$Product['Product']['no_of_piece_per_unit']; 
        $Product['Product']['lastunitcost']=$Product['Product']['lastunitcost']*$Product['Product']['no_of_piece_per_unit'];   
      }
      $Product['Product']['level']=$unit_level;
      $Product['Product']['purchase_unit_level']=$unit_level;
      $Product['Product']['product_unit_level']=$Product['Unit']['level'];
      $return['result']='Success';
      $return['Product']=$Product['Product'];

      $return['available_quantity']=$stock_list['Stock']['quantity']; 
    }
    else
    {
      $return['result']='Empty';
    }
    echo json_encode($return);
    exit;
  }
  public function Product_add_ajax()
  {
    $datasource_Product = $this->Product->getDataSource();
    $datasource_Stock = $this->Stock->getDataSource();
    try {
      $datasource_Product->begin();
      $datasource_Stock->begin();
      $userid = 1;
      if(empty($userid))
        throw new Exception("Please Login First Data", 1);
      $data=$this->request->data['ProductAdd'];
      if(empty($data))
        throw new Exception("Empty Data", 1);
      $stock_date=date('Y-m-d');
      if(empty($stock_date))
        throw new Exception("Empty Date", 1);
      $product_name_text=trim(strtoupper($data['name']));
      if(empty($product_name_text))
        throw new Exception("Empty Product Name", 1);
      if($data['Brand_id_modal']=='G')
      {
        $data['Brand_id_modal']=0;
      }
      $mrp=$data['mrp'];
      $wholesale_price=$data['wholesale_price'];
      $Profile=$this->Global_Var_Profile['Profile'];
      if($Profile['product_configuration_type']=='WholeSale')
      {
        $mrp=$wholesale_price;
      }
      else if($Profile['product_configuration_type']=='Retail')
      {
        $wholesale_price=$mrp;
      }
      //if(empty($wholesale_price))
      //  throw new Exception("Empty Executive Rate", 1);
     // if(empty($mrp))
      //  throw new Exception("Empty MRP", 1);
      $cost=$data['cost'];
      //if(empty($cost))
       // throw new Exception("Empty cost", 1);
      if($mrp<$cost)
        throw new Exception("MRP must be greater than Cost", 1);
      //if($wholesale_price<$cost)
       // throw new Exception("WholeSale Price must be greater than Cost", 1);
      //if($mrp<$wholesale_price)
       // throw new Exception("MRP must be greater than WholeSale Price", 1);
      if($data['vat']>=100)
        throw new Exception("Tax Must Less than 100", 1);
      $vat = $data['vat'];
      if(!empty($data['party_id']))
      {
        $party_id = $data['party_id'];
      }
      else{
        $party_id = 0;
      }
      if(isset($data['barcode_on'])){
        $barcode_on = 'on';
        if(!isset($data['custom_barcode_selection'])){
          $LastBarcode=$this->Product->find('first',array('conditions'=>array('Product.active=1','Product.custom_barcode_selection'=>'on'),'fields'=>array('Product.barcode'),'order'=>'Product.barcode DESC'));
          if($LastBarcode){
            if(empty($LastBarcode['Product']['barcode']))
            {
              $barcode_last=1111111111111;
            }
            else
            {
              $barcode_last=$LastBarcode['Product']['barcode']+1;
            }

          }
          else{
            $barcode_last=1111111111111;
          }
          $barcode=$data['Product_barcode'];
          $barcode_selection='on';  
        }
        else{
          $barcode=$data['custom_barcode'];
          $barcode_selection='';    
        }

        if(empty($barcode))
          throw new Exception("Empty Barcode", 1);
        $barcode_check=$this->Product->find('all',array(
          'conditions'=>array('Product.active=1','Product.barcode'=>$barcode),
          ));
        if(!empty($barcode_check))
          throw new Exception("Existing Barcode", 1);
      }
      else{
        $barcode_on = 'off';
        $barcode ="";
        $barcode_selection='';
      }
      $ProductRow = $this->Product->find('first', array(
        // 'conditions'=>array('Product.product_type_id'=>$data['product_type_id_modal']),
        'order' => array('Product.id' => 'DESC') ));
      $ProductTypeRow = $this->ProductType->findById($data['product_type_id_modal']);
      $ProducTypeCode = $ProductTypeRow['ProductType']['code'];
      if(!empty($ProductRow))
      {
        $explode_code=  $ProductRow['Product']['code'];
       // $code_append=$explode_code[1];
        $PrevCode = $explode_code+1;
        $code = $PrevCode;
        $code_check=$this->Product->findByCode($code);
        while ($code_check) {
          $PrevCode++;
          $code = $PrevCode;
          $code_check=$this->Product->findByCode($code);
        }
      }
      else{
        $PrevCode=1001;
        $code = $PrevCode;
        $code_check=$this->Product->findByCode($code);
        while ($code_check) {
          $PrevCode++;
          $code = $PrevCode;
          $code_check=$this->Product->findByCode($code);
        }
      }

      if(empty($data['no_of_piece_per_unit'])){
        $data['no_of_piece_per_unit'] = 1;
      }

      $product_name_arabic_text=trim(strtoupper($data['arabic_name']));
      $Productadddata=array(
        'name'=>''.$product_name_text.'',
        'arabic_name'=>''.$product_name_arabic_text.'',
        'code'=>''.$code,
        'type'=>$data['type_of_product'],
        'product_type_id'=>$data['product_type_id_modal'],
        'tray_occupation'=>$data['tray_occupation'],
         'production_days'=>$data['production_days'],
        'brand_id'=>$data['Brand_id_modal'],
        'unit_id'=>''.$data['unit_id'].'',
        'cost'=>''.$cost.'',
        'party_id'=>$party_id,
        'wholesale_price'=>''.$wholesale_price.'',
        'mrp'=>''.$mrp.'',
        'threashold'=>''.$data['threshold'].'',
        'barcode_on'=>''.$barcode_on.'',
        'barcode'=>''.$barcode.'',
        'custom_barcode_selection'=>''.$barcode_selection.'',
        'no_of_piece_per_unit'=>''.$data['no_of_piece_per_unit'].'',
        'vat'=>''.$data['vat'].'',
        'bonus_percentage'=>''.$data['bonus'].'',
        'tax'=>''.$data['vat'].'',
        'created_at'=>date('Y-m-d H:i:s',strtotime($stock_date)),
        'updated_at'=>date('Y-m-d H:i:s',strtotime($stock_date)),
        'hsn_code'=>''.$data['hsn_code'].'',
        );
      if(!empty($barcode))
      {
        $BarcodeImageGeneration_return=$this->BarcodeImageGeneration((string)$barcode);
        if($BarcodeImageGeneration_return['result']=='Success')
          $Productadddata['barcode_image']=$BarcodeImageGeneration_return['file'];
      }
      if($data['quantity']=='')
        throw new Exception("Empty quantity", 1);
      $this->Product->create();
      if(!$this->Product->save($Productadddata))
      {
        $errors = $this->Product->validationErrors;
        foreach ($errors as $key => $value) {
          throw new Exception($value[0], 1);
        }
      }
      $last_insert_id=$this->Product->getLastInsertId();
      $Product=$this->Product->findById($last_insert_id);
      if(empty($Product))
        throw new Exception("Empty Product", 1);
      $return['Product'] ='<option value="'.$Product['Product']['id'].'">'.$Product['Product']['name'].'</option>';
      $product_id=$last_insert_id;
      $quantity=$data['quantity'];
      $remark='Product Created';
      $flag=1;
      $warehouse_id = 1;
      $StockController = new StockController;
      $Stock_function_return=$StockController->GeneralStock_Add_Function($warehouse_id,$product_id,$quantity,$stock_date,$userid,$remark,$flag);
      if($Stock_function_return['result']!='Success')
        throw new Exception($Stock_function_return['result'], 1);
      $datasource_Product->commit();
      $datasource_Stock->commit();
      $return['result']='Success';
    } catch (Exception $e) {
      $datasource_Product->rollback();
      $datasource_Stock->rollback();
      $return['result']='Error';
      $return['message']=$e->getMessage();
    }
    echo json_encode($return);
    exit;
  }
  public function Type_add_ajax()
  {
    try {
      $data=$this->request->data['Type'];
      $userid = 1;
      if(empty($userid))
        throw new Exception("Please Login first", 1);
      $name=strtoupper(trim($data['name']));
      if(empty($name))
        throw new Exception("Empty name", 2);
      $TypeOfProduct=$this->TypeOfProduct->find('first',array('conditions'=>array('TypeOfProduct.name'=>$name)));
      if(empty($TypeOfProduct))
      {
        $Table_data=array(
          'name'=>trim(strtoupper($data['name'])),
          );
        $this->TypeOfProduct->create();
        if(!$this->TypeOfProduct->save($Table_data))
        {
          $errors = $this->TypeOfProduct->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
        $id=$this->TypeOfProduct->getLastInsertId();
        $TypeOfProduct=$this->TypeOfProduct->findById($id);
         $return['result']="Success";
      }
      else
      {
       $return['result']="Already Added"; 
      }
      $return['key']=$TypeOfProduct['TypeOfProduct']['id'];
      $return['value']=$TypeOfProduct['TypeOfProduct']['name'];
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
    }
    echo json_encode($return);
    exit;
  }
  public function Product_stock_add_ajax()
  {
    $datasource_Product = $this->Product->getDataSource();
    $datasource_Stock = $this->Stock->getDataSource();
    try {
      $datasource_Product->begin();
      $datasource_Stock->begin();
      $userid = 1;
      if(empty($userid))
        throw new Exception("Please Login First Data", 1);
      $data=$this->request->data['ProductAdd'];
      if(empty($data))
        throw new Exception("Empty Data", 1);
      $stock_date=$data['stock_date'];
      if(empty($stock_date))
        throw new Exception("Empty Date", 1);
      $product_name_text=trim(strtoupper($data['name']));
      if(empty($product_name_text))
        throw new Exception("Empty Product Name", 1);
      if($data['Brand_id_modal']=='G')
      {
        $data['Brand_id_modal']=0;
      }
      $mrp=$data['mrp'];
      $wholesale_price=$data['wholesale_price'];
      $Profile=$this->Global_Var_Profile['Profile'];
      if($Profile['product_configuration_type']=='WholeSale')
      {
        $mrp=$wholesale_price;
      }
      else if($Profile['product_configuration_type']=='Retail')
      {
        $wholesale_price=$mrp;
      }
      if(empty($wholesale_price)){
        if($wholesale_price != 0)
          throw new Exception("Empty wholesale_price", 1);
      }
      if(empty($mrp)){
        if($mrp != 0)
          throw new Exception("Empty MRP", 1);
      }
      $cost=$data['cost'];
      if(empty($cost)){
        if($cost != 0)
          throw new Exception("Empty cost", 1);
      }
      if($mrp<$cost)
        throw new Exception("MRP must be greater than Cost", 1);
      if($wholesale_price<$cost)
        throw new Exception("WholeSale Price must be greater than Cost", 1);
      if($mrp<$wholesale_price)
        throw new Exception("MRP must be greater than WholeSale Price", 1);
      if($data['tax']>=100)
        throw new Exception("Tax Must Less than 100", 1);
      $tax = $data['tax'];
      if(!empty($data['party_id']))
      {
        $party_id = $data['party_id'];
      }
      else{
        $party_id = 0;
      }
      $ProductRow = $this->Product->find('first', array(
        'conditions'=>array('Product.product_type_id'=>$data['product_type_id_modal']),
        'order' => array('Product.id' => 'DESC') ));
      $ProductTypeRow = $this->ProductType->findById($data['product_type_id_modal']);
      $ProducTypeCode = $ProductTypeRow['ProductType']['code'];
      if(!empty($ProductRow))
      {
        $PrevCode = $ProductRow['Product']['code'];
        $code = $PrevCode + 1;
      }
      else
      {
        $code = $ProducTypeCode.'11';
      }
      $product_name_arabic_text=trim(strtoupper($data['arabic_name']));
      $Productadddata=array(
        'name'=>''.$product_name_text.'',
        'code'=>''.$code,
// 'arabic_name'=>'المنتج',
        'arabic_name'=>$product_name_arabic_text,
// 'hsn_code'=>''.$hsn_code.'',
        'product_type_id'=>$data['product_type_id_modal'],
        'brand_id'=>$data['Brand_id_modal'],
        'unit_id'=>''.$data['unit_id'].'',
        'cost'=>''.$cost.'',
        'party_id'=>$party_id,
        'wholesale_price'=>''.$wholesale_price.'',
        'mrp'=>''.$mrp.'',
        'threashold'=>''.$data['threshold'].'',
        'no_of_piece_per_unit'=>''.$data['no_of_piece_per_unit'].'',
        'tax'=>$tax,
        'created_at'=>date('Y-m-d H:i:s',strtotime($stock_date)),
        'updated_at'=>date('Y-m-d H:i:s',strtotime($stock_date)),
        );
      if($data['quantity']=='')
        throw new Exception("Empty quantity", 1);
      $this->Product->create();
      if(!$this->Product->save($Productadddata))
      {
        $errors = $this->Product->validationErrors;
        foreach ($errors as $key => $value) {
          throw new Exception($value[0], 1);
        }
      }
      $last_insert_id=$this->Product->getLastInsertId();
      $Product=$this->Product->findById($last_insert_id);
      if(empty($Product))
        throw new Exception("Empty Product", 1);
      $return['Product'] ='<option value="'.$Product['Product']['id'].'">'.$Product['Product']['name'].'</option>';
      $product_id=$last_insert_id;
      $quantity=$data['quantity'];
      $remark='Product Created';
      $flag=1;
      $warehouse_id = 1;
      $StockController = new StockController;
      $Stock_function_return=$StockController->GeneralStock_Add_Function($warehouse_id,$product_id,$quantity,$stock_date,$userid,$remark,$flag);
      if($Stock_function_return['result']!='Success')
        throw new Exception($Stock_function_return['result'], 1);
      $datasource_Product->commit();
      $datasource_Stock->commit();
      $return['result']='Success';
    } catch (Exception $e) {
      $datasource_Product->rollback();
      $datasource_Stock->rollback();
      $return['result']='Error';
      $return['message']=$e->getMessage();
    }
    echo json_encode($return);
    exit;
  }
  public function product_edit_ajax(){

    $data=$this->request->data['ProductEdit'];
    try {
      $id=$data['id'];

      if($data['Party_id_modal']!='empty')
      {
        $party_id = $data['Party_id_modal'];
      }
      else{
        $party_id = 0;
      }
      if($data['Brand_id_modal']=='G')
      {
        $Brand_id='0';  
      }
      else
      {
        $Brand_id=$data['Brand_id_modal'];
      }

      $product_name=strtoupper(trim($data['name']));
      $product_name_arabic_text=trim(strtoupper($data['arabic_name']));
      if(!$product_name)
        throw new Exception("Empty product_name", 1);
      $threashold=$data['threshold'];
    $cost=$data['cost'];
      if($cost=='' || $cost==null)
        throw new Exception("Empty cost", 1);
     $mrp=$data['mrp'];
      $wholesale_price=$data['wholesale_price'];
      if($mrp=='' || $mrp==null)
      throw new Exception("Empty mrp", 1);
      if(!$wholesale_price && $wholesale_price!=0)
        throw new Exception("Empty wholesale_price", 1);
      if($data['vat']>=100)
        throw new Exception("Tax Must Less than 100", 1);
      $vat = $data['vat'];
      $this->Product->unbindModel(array('hasMany'=>array('SaleItem','Stock','StockLog','SalesReturnItem','PurchasedItem','PurchaseReturnItem')));
      $Product=$this->Product->findById($id);
      $this->Product->id=$id;
      if(!empty($data['product_type_id_modal']))
      {
        $ProductRow = $this->Product->find('first', array(
        // 'conditions'=>array('Product.product_type_id'=>$data['product_type_id_modal']),
        'order' => array('Product.id' => 'DESC') ));
      $ProductTypeRow = $this->ProductType->findById($data['product_type_id_modal']);
      $ProducTypeCode = $ProductTypeRow['ProductType']['code'];
      if(!empty($ProductRow))
      {
        $explode_code=  $ProductRow['Product']['code'];
       // $code_append=$explode_code[1];
        $PrevCode = $explode_code+1;
        $code = $PrevCode;
        $code_check=$this->Product->findByCode($code);
        while ($code_check) {
          $PrevCode++;
          $code = $PrevCode;
          $code_check=$this->Product->findByCode($code);
        }
      }
      else{
        $PrevCode=1001;
        $code = $PrevCode;
        $code_check=$this->Product->findByCode($code);
        while ($code_check) {
          $PrevCode++;
          $code = $PrevCode;
          $code_check=$this->Product->findByCode($code);
        }
      }
        if($Product['Product']['product_type_id']!=$data['product_type_id_modal'])
        {
          if(!$this->Product->saveField('product_type_id',$data['product_type_id_modal'] ))
            throw new Exception("Error Processing product type Updation", 1);
          if(!$this->Product->saveField('code',$code ))
            throw new Exception("Error Processing code Updation", 1);
        }
      }
      
     // if($mrp<$cost)
      //  throw new Exception("An Error Occured: Cost is greater than MRP of the added product $product_name", 1);
      if(!$this->Product->saveField('name',$product_name ))
        throw new Exception("Error Processing name Updation", 1);
      if(!$this->Product->saveField('arabic_name',$product_name_arabic_text))
        throw new Exception("Error Processing arabic_name Updation", 1);
      if(!$this->Product->saveField('brand_id',$Brand_id))
        throw new Exception("Error Processing brand_id Updation", 1);
      if(!$this->Product->saveField('party_id',$party_id))
        throw new Exception("Error Processing product_type_id Updation", 1);
      if(!empty($data['unit_id']))
      {
        $unit_id=$data['unit_id'];
        if(!$this->Product->saveField('unit_id',$unit_id ))
          throw new Exception("Error Processing unit_id Updation", 1);
      }
      if(!empty($data['no_of_piece_per_unit']))
      {
        $no_of_piece_per_unit=$data['no_of_piece_per_unit'];
        if(!$this->Product->saveField('no_of_piece_per_unit',$no_of_piece_per_unit ))
          throw new Exception("Error Processing no_of_piece_per_unit Updation", 1);
      }

     
      if(isset($data['barcode_on'])){
        $barcode_on = 'on';
        if(!isset($data['custom_barcode_selection'])){
          $LastBarcode=$this->Product->find('first',array('conditions'=>array('Product.custom_barcode_selection'=>'on'),'fields'=>array('Product.barcode'),'order'=>'Product.barcode DESC'));
          if($LastBarcode){
            if(empty($LastBarcode['Product']['barcode']))
            {
              $barcode_last=1111111111111;
            }
            else
            {
              $barcode_last=$LastBarcode['Product']['barcode']+1;
            }

          }
          else{
            $barcode_last=1111111111111;
          }
          $barcode=$data['Product_barcode'];
          $barcode_selection='on';  
        }
        else{
          $barcode=$data['custom_barcode'];
          $barcode_selection='';    
        }

        if(empty($barcode))
          throw new Exception("Empty Barcode", 1);
        $barcode_check=$this->Product->find('all',array(
          'conditions'=>array('Product.active=1','Product.id !='=>$id,'Product.barcode'=>$barcode),
          ));
        if(!empty($barcode_check))
          throw new Exception("Existing Barcode", 1);
      }
      else{
        $barcode_on = 'off';
        $barcode ="";
        $barcode_selection='';
      }
      $bonus_percentage= $data['bonus'];
      if(!$this->Product->saveField('barcode_on',$barcode_on))
        throw new Exception("Error Processing barcode Updation", 1);
      if(empty($Product['Product']['barcode']))
      {
        if(!$this->Product->saveField('barcode',$barcode))
          throw new Exception("Error Processing barcode Updation", 1);
        if(!empty($barcode))
        {
          $BarcodeImageGeneration_return=$this->BarcodeImageGeneration((string)$barcode);
          if($BarcodeImageGeneration_return['result']=='Success')
            $Productadddata['barcode_image']=$BarcodeImageGeneration_return['file'];
            if(!$this->Product->saveField('barcode_image',$Productadddata['barcode_image']))
              throw new Exception("Error Processing barcode Updation", 1);
        }
      }
      

      

      if(!$this->Product->saveField('custom_barcode_selection',$barcode_selection))
        throw new Exception("Error Processing barcode Updation", 1);
      if(!$this->Product->saveField('threashold',$threashold))
        throw new Exception("Error Processing threashold Updation", 1);
      if(!$this->Product->saveField('cost',$cost ))
        throw new Exception("Error Processing cost Updation", 1);
      if(!$this->Product->saveField('mrp',$mrp ))
        throw new Exception("Error Processing mrp Updation", 1);
      if(!$this->Product->saveField('wholesale_price',$wholesale_price ))
        throw new Exception("Error Processing wholesale_price Updation", 1);
      if(!$this->Product->saveField('vat',$vat ))
        throw new Exception("Error Processing VAT Updation", 1);
      if(!$this->Product->saveField('tax',$vat ))
        throw new Exception("Error Processing VAT Updation", 1);
      if(!$this->Product->saveField('bonus_percentage',$bonus_percentage ))
        throw new Exception("Error Processing Bonus % Updation", 1);
      if(!$this->Product->saveField('type',$data['type_of_product'] ))
        throw new Exception("Error Processing type Updation", 1);
       if(!$this->Product->saveField('tray_occupation',$data['tray_occupation'] ))
        throw new Exception("Error Processing type Updation", 1);
      if(!$this->Product->saveField('production_days',$data['production_days'] ))
        throw new Exception("Error Processing type Updation", 1);
      if(!$this->Product->saveField('hsn_code',$data['hsn_code'] ))
        throw new Exception("Error Processing hsn code Updation", 1);
      $Product=$this->Product->read();
      $return['result']='Success';
      $return['key']=$Product['Product']['id'];
      $return['value']=$Product['Product']['name'];
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
    }

    echo json_encode($return);
    exit;
  }
  public function product_delete_ajax($id)
  {
    try {
      $Product=$this->Product->findById($id);
      if($Product['SaleItem'] || $Product['SalesReturnItem'] || $Product['PurchaseReturnItem'] || $Product['PurchasedItem'])
        throw new Exception("This is Used In ".$Product['Product']['name'], 1);
      if(!$this->Product->delete($id))
        throw new Exception("Error Processing Request While delete", 1);
      $return['result']='Success';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
    }
    echo json_encode($return);
    exit;
  }
  public function product_get_ajax($id)
  {
    try {
      $this->Product->unbindModel(
      array('hasMany' => array(
        'PurchaseReturnItem',
        'PurchasedItem',
        'SaleItem',
        'SalesReturnItem',
        'StockLog',
        'Stock',
        'UnwantedList',
        ))
      );
      $Product=$this->Product->findById($id);
      $PurchasedItem=$this->PurchasedItem->findByProductId($id);
      $SaleItem=$this->SaleItem->findByProductId($id);
      if(!$Product)
        throw new Exception("Empty Product", 1);
      $Product['Product']['unit_check']=0;

      if($PurchasedItem)
      {
        $Product['Product']['unit_check']=1;

      }
      elseif($SaleItem)
      {
        $Product['Product']['unit_check']=1;

      }
      $Product['Product']['cost']=floatval($Product['Product']['cost']);
      $Product['Product']['mrp']=floatval($Product['Product']['mrp']);
      $Product['Product']['wholesale_price']=floatval($Product['Product']['wholesale_price']);
      $Product['Product']['tax']=floatval($Product['Product']['tax']);
      $Product['Product']['threashold']=floatval($Product['Product']['threashold']);
      $Product['Product']['party_id']=floatval($Product['Product']['party_id']);
      $return['data']=$Product;
      $return['result']='Success';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
    }
    echo json_encode($return);
    exit;
  }
  public function delete()
  {
    $this->before_load();
    $id=$this->request->params['pass'][0];
    $stock_pdct_ids= $this->Stock->find('all', array('conditions'=>array('product_id'=>$id)));
    if(count($stock_pdct_ids)>1)
    {
      $this->Flash->set(__('Product in Stock ..Unable to delete Product'));
      $this->redirect(array('controller'=>'Product','action'=>'index'));
    }
    else
    {
      if($stock_pdct_ids[0]['Stock']['flag']==0){
        $st_id= $stock_pdct_ids[0]['Stock']['id'];
        if(($this->Product->delete($id))&&($this->Stock->delete($st_id)))
        {
          $this->Flash->set(__('product deleted.',h($id)));
          $this->redirect(array('controller'=>'Product','action'=>'index'));
        }
        else
        {
          $this->redirect(array('action'=>'index'));
        }
      }
      else
      {
        exit;
      }
    }
  }
  public function delete_Product_ajax($id)
  {
    $userid = 1;
    $Stock=$this->Stock->findByProductId($id);
    $SaleProductsOffline=[];
    $PurchasedItem=[];
    if(empty($PurchasedItem) && empty($SaleProductsOffline))
    {
      if(!empty($Stock))
      {
        if($this->Stock->delete($Stock['Stock']['id']))
        {
          if($this->Product->delete($id))
          {
            $return['result']='Success';
          }
          else
          {
            $return['result']='Error In Product delete'; 
          }
        }
        else
        {
          $return['result']='Error In Stock delete'; 
        }
      }
      else
      {
        if($this->Product->delete($id))
        {
          $return['result']='Success';
        }
        else
        {
          $return['result']='Error In Product delete'; 
        }
      }
    }
    else
    {
      $return['result']='This Product Is Used'; 
    }
    echo json_encode($return);
    exit;
  }
  public function product_type_select_ajax()
  {
    $data=$this->request->data;
    if(!empty($data['product_type']))
    {
      $product_type_id=$data['product_type'];
    }
    if(!empty($data['type_products']))
    {
      $type_products=$data['type_products'];
    }
    $conditions=array();
    if(!empty($product_type_id))
    {
      $conditions['Product.product_type_id']=$product_type_id;
    }
    if(!empty($type_products))
    {
      $conditions['Product.type']=$type_products;
    }
    $conditions['Product.active']=1;
    $this->Product->virtualFields = array(
      'product_name' => "CONCAT(Product.name, ' ', Product.code)"
      );
    $Product=$this->Product->find('all',
      array(
        'conditions'=>$conditions,
        )
      );
    $return['option']='<option value="">SELECT</option>';
    if(!empty($Product))
    {
      $return['result']='Success';
      foreach ($Product as $key => $value) {
        $return['option']=$return['option'].'<option value='.$value['Product']['id'].'>'.$value['Product']['product_name'].'</option>';
      } 
    }
    echo json_encode($return);
    exit;
  }
  public function check_product_ajax(){
    $product_name=$this->request->data['product_name'];
    $result=$this->Product->find('all',array('conditions'=>array('Product.active=1','Product.name'=>$product_name)));
    if(empty($result)){
      echo "No";
    }
    else{
      echo "Yes";
    }
    exit;
  }
  public function check_productid_ajax(){
    $product_name_id=$this->request->data['product_id'];
    $result=$this->Product->find('all',array('conditions'=>array('Product.active=1','product_id'=>$product_name_id)));
    if(empty($result)){
      echo "No";
    }
    else{
      echo "Yes";
    }
    exit;
  }
  public function get_mrp_ajax($id)
  {
    $Product=$this->Product->findById($id);
    echo json_encode($Product['Product']['mrp']);
    exit;
  }
  public function product_list_ajax_rendering()
  {
    $this->Product->virtualFields = array(
      'name' => "CONCAT(Product.product_name, '  (', Stock.quantity , ')' )"
      );
    $Product=$this->Product->find('list',array(
      'joins'=>array(
        array(
          'table'=>'stocks',
          'alias'=>'Stock',
          'type'=>'INNER',
          'conditions'=>array('Stock.product_id=Product.id')
          ),
        ),
      'conditions'=>array('Product.active=1'),
      'fields'=>array('Product.id','name'),
      ));
    echo json_encode($Product);
    exit;
  }
  public function get_product_by_warehouse($id=null)
  {
    $conditions=[];
    $return['option']='<option value="">Select</option>';
    if($id)
    {
      $conditions['Stock.warehouse_id']=$id;
    }
    $this->Stock->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
    $Product=$this->Stock->find('all',array(
      'conditions'=>$conditions,
      'fields'=>array(
        'Product.id',
        'Stock.product_name',
        'Stock.warehouse_id',
        )
      ));
    if($Product)
    {
      $return['result']='Success';
      foreach ($Product as $key => $value) 
      {
        if($value['Product']['id'])
          $return['option'].='<option value="'.$value['Product']['id'].'">'.$value['Stock']['product_name'].'</option>'; 
      }
    }
    else
    {
      $return['result']='Empty';
    }
    echo json_encode($return);
    exit;
  }
  public function GetProductUnitLevel()
  {
    $unit_id=$this->request->data['unit_id'];
    $Unit=$this->Unit->findById($unit_id);
    unset($Unit['Product']);
    echo json_encode($Unit);
    exit;
  }
  public function get_Product_no_of_piece($id)
  { 

    $stock_list=$this->Stock->find('first',array(

      'conditions'=>array(
        'Stock.product_id'=>$id,
        'Stock.warehouse_id'=>1,
        ),
      'fields' => array(
        'Stock.quantity',
        'Product.no_of_piece_per_unit',
        'Product.name',

        )
      ));
    $return['name']=$stock_list['Product']['name'];
    $return['no_of_piece_per_unit']=floatval($stock_list['Product']['no_of_piece_per_unit']);
    $return['available_quantity']=$stock_list['Stock']['quantity']; 
    echo json_encode($return);
    exit;
  }
// public function update_product()
// {
//     $Product=$this->Product->find('all');
//     foreach ($Product as $key => $value) {

//       $wholesale_price=($value['Product']['wholesale_price']*100)/105;
//             $retail=($value['Product']['mrp']*100)/105;
//               $this->Product->id=$value['Product']['id'];

// if(!$this->Product->saveField('wholesale_price',$wholesale_price ))
//     throw new Exception("Error Processing wholesale_price Updation", 1);
//    if(!$this->Product->saveField('mrp',$retail))
//     throw new Exception("Error Processing retail Updation", 1);
//     }
// exit;
// }
  public function update_product_code3()
  {
    $Product=$this->Product->find('all',array('conditions'=>array('Product.product_type_id'=>3),'order' => array('Product.id' => 'ASC')));
    foreach ($Product as $key => $value) {
      $ProductTypeRow = $this->ProductType->findById(3);
      $ProducTypeCode = $ProductTypeRow['ProductType']['code'];
      $code_append=11+$key;
      $code=$ProductTypeRow['ProductType']['code'].$code_append;
      $this->Product->id=$value['Product']['id'];
      if(!$this->Product->saveField('code',$code))
        throw new Exception("Error Processing retail Updation", 1);
    }
    exit;
  }
  public function update_product_code4()
  {
    $Product=$this->Product->find('all',array('conditions'=>array('Product.product_type_id'=>4),'order' => array('Product.id' => 'ASC')));
    foreach ($Product as $key => $value) {
      $ProductTypeRow = $this->ProductType->findById(4);
      $ProducTypeCode = $ProductTypeRow['ProductType']['code'];
      $code_append=11+$key;
      $code=$ProductTypeRow['ProductType']['code'].$code_append;
      $this->Product->id=$value['Product']['id'];
      if(!$this->Product->saveField('code',$code))
        throw new Exception("Error Processing retail Updation", 1);
    }
    exit;
  }
  public function update_product_code5()
  {
    $Product=$this->Product->find('all',array('conditions'=>array('Product.product_type_id'=>5),'order' => array('Product.id' => 'ASC')));
    foreach ($Product as $key => $value) {
      $ProductTypeRow = $this->ProductType->findById(5);
      $ProducTypeCode = $ProductTypeRow['ProductType']['code'];
      $code_append=11+$key;
      $code=$ProductTypeRow['ProductType']['code'].$code_append;
      $this->Product->id=$value['Product']['id'];
      if(!$this->Product->saveField('code',$code))
        throw new Exception("Error Processing retail Updation", 1);
    }
    exit;
  }


  public function get_Product_msl_ajax_sale($id=null,$unit_id=null)
  { 

    $Product=$this->Product->find('first',array(
      'conditions'=>array('Product.id'=>$id)
      ));
    if(!empty($Product))
    {

      if($unit_id){
        $Unit=$this->Unit->findById($unit_id);
        $unit_level=$Unit['Unit']['level'];
      }

      $first_date=date('Y-m-d',strtotime('first day of previous month'));
//pr($first_date);
//exit;
      $last_date= date('Y-m-d',strtotime('last day of previous month'));
//pr($last_date);
      $avg=$this->Sale->find('all',array(
        'joins'=>array(
          array(
            'table'=>'sale_items',
            'alias'=>'SaleItem',
            'type'=>'INNER',
            'conditions'=>array('Sale.id=SaleItem.sale_id')
            ),

          ),
        'conditions'=>array('Sale.date_of_delivered between ? and ?'=>array($first_date,$last_date),
          'SaleItem.product_id'=>$Product['Product']['id'],
          ),
        'fields'=>array('SaleItem.quantity'),
        ));
//pr($avg);
//exit;
      $avg_q=0;
      foreach ($avg as $key5 => $value5) {
//$avg_q+=$value5['SaleItem']['quantity'];
        if ($unit_level==2) {
          $av=$value5['SaleItem']['quantity']/$Product['Product']['no_of_piece_per_unit'];


        }
        else{
          $av=$value5['SaleItem']['quantity'];
        }

        $avg_q+=$av;
      }

      $avg_quantity=ROUND($avg_q/26,3);

      $return['result']='Success';
      $return['avg']=$avg_quantity;


    }
    else
    {
      $return['result']='Empty';
    }
    echo json_encode($return);
    exit;
  }
  public function get_Product_msl_unit_ajax_sale($id=null)
  { 
//pr($id);
//exit;
    $Product=$this->Product->find('first',array(
      'conditions'=>array('Product.id'=>$id)
      ));
//pr($Product);
//exit;
    if(!empty($Product))
    {
//if(empty($unit_id)){
      $unit_id=$Product['Product']['unit_id'];
//pr($unit_id);
//exit;
      if($unit_id==2){
        $unit_name="Cases";
      }
      else{
        $unit_name="Pieces";
      }
      $first_date=date('Y-m-d',strtotime('first day of previous month'));
//pr($first_date);
//exit;
      $last_date= date('Y-m-d',strtotime('last day of previous month'));
//pr($last_date);
      $avg=$this->Sale->find('all',array(
        'joins'=>array(
          array(
            'table'=>'sale_items',
            'alias'=>'SaleItem',
            'type'=>'INNER',
            'conditions'=>array('Sale.id=SaleItem.sale_id')
            ),
// array(
//  'table'=>'products',
//  'alias'=>'Product',
//  'type'=>'INNER',
//  'conditions'=>array('SaleItem.product_id=Product.id')
// ),

          ),
        'conditions'=>array('Sale.date_of_delivered between ? and ?'=>array($first_date,$last_date),
          'SaleItem.product_id'=>$Product['Product']['id'],
          ),
        'fields'=>array('SaleItem.quantity'),
        ));

      $avg_q=0;
      foreach ($avg as $key5 => $value5) {
//$avg_q+=$value5['SaleItem']['quantity'];
        if ($unit_id==2) {
          $av=$value5['SaleItem']['quantity']/$Product['Product']['no_of_piece_per_unit'];


        }
        else{
          $av=$value5['SaleItem']['quantity'];
        }
// pr($value5);
// exit;
        $avg_q+=$av;
      }
      $avg_quantity=ROUND($avg_q/26,3);
      $return['result']='Success';
      $return['avg']=$avg_quantity;
      $return['unit']=$unit_id;

//$return['available_quantity']=$stock_list['Stock']['quantity']; 
    }
    else
    {
      $return['result']='Empty';
    }
    echo json_encode($return);
    exit;
  }
  public function get_product_by_barcode($barcode)
  {
    $this->Product->unbindModel(
      array('hasMany' => array(
        'PurchaseReturnItem',
        'PurchasedItem',
        'SaleItem',
        'SalesReturnItem',
        'StockLog',
        'Stock',
        'UnwantedList',
        ))
      );
    $Product=$this->Product->findByBarcode($barcode);
    if($Product)
    {
      $return['result']='Success';
      $return['Product']['id']=$Product['Product']['id'];
      $return['Product']['name']=$Product['Product']['name'];
    }
    else
    {
      $ProductBarcode=$this->ProductBarcode->findByBarcode($barcode);
      if($ProductBarcode)
      {
        $return['result']='Success';
        $return['Product']['id']=$ProductBarcode['Product']['id'];
        $return['Product']['name']=$ProductBarcode['Product']['name'];
      }
      else
      {
        $return['result']='Empty';      
      }
    }
    echo json_encode($return); exit;
  }
  public function get_auto_generated_barcode()
  {
    $this->Product->unbindModel(array('belongsTo'=>array('ProductType','Brand','Unit'),'hasMany' => array('PurchaseReturnItem','UnwantedList','Stock','StockLog','SalesReturnItem','SaleItem','PurchasedItem','Unit','Brand')));
    $LastBarcode=$this->Product->find('first',array('conditions'=>array('Product.custom_barcode_selection'=>'on'),'fields'=>array('Product.barcode'),'order'=>'Product.barcode DESC'));
    $this->Product->virtualFields = array(
      'product_name' => "CONCAT(Product.name, ' ', Product.code)"
      );
    if($LastBarcode){
      if(empty($LastBarcode['Product']['barcode']))
      {
        $barcode_last=1111111111111;
      }
      else
      {
        $barcode_last=$LastBarcode['Product']['barcode']+1;
      }

    }
    else{
      $barcode_last=1111111111111;
    }
    $barcode_check=$this->Product->findByBarcode($barcode_last);
    while ($barcode_check) {
      $barcode_last++;
      $barcode_check=$this->Product->findByBarcode($barcode_last);
    }
    $return['barcode']=$barcode_last;
    echo json_encode($return);
    exit;
  }
  public function BarcodeImageGeneration($data_to_encode=null)
  {    
    try {
      if(!$data_to_encode)
        throw new Exception("Empty data_to_encode data", 1);
      $barcode=new BarcodeHelper();
        // Generate Barcode data
      $barcode->barcode();
      $barcode->setType('C128');
      $barcode->setCode($data_to_encode);
      $barcode->setSize(80,200);
        // Generate filename
      $random = rand(0,1000000);
      $file='barcode_'.$random.'.png';
      $file_location = 'img/barcode/'.$file;
        // Generates image file on server      
      $barcode->writeBarcodeFile($file_location);
      $return['result']='Success';
      $return['file']=$file;
    } catch (Exception $e) {
      $return['result']='Error';
    }
    return $return;
  }
  public function BarcodePrintSingle($barcode=null)
  {
    if($barcode){
      $barcodelist=[
        $barcode,
      ];
      $Product=$this->Product->findByBarcode($barcode);
      $quantity=0;
      foreach ($Product['Stock'] as $key => $value) {
        $quantity=$value['quantity'];
      }
      
      for ($i=0; $i <$quantity ; $i++) { 
        $barcodelist[]=$barcode;
      }
      $colourlist=array();
      // $colourlist['fill_r']=$Product['Colour']['fill_r'];
      // $colourlist['fill_g']=$Product['Colour']['fill_g'];
      // $colourlist['fill_b']=$Product['Colour']['fill_b'];
      //$function_name=$this->Global_Var_Profile['PrintLink']['barcode_print'];
      //$function_name=$this->BarcodePrintMakMart;
      $this->BarcodePrint($barcodelist,$colourlist);
    }
    else {
      pr("Empty Barcode");exit;
    }
  }
  public function BarcodePrint($barcodelist,$colourlist)
  {
    try {
      $count=1;
      if(!$count)
        throw new Exception("Empty List", 1);
      require('fpdf/fpdf_barcode.php');
      $pdf = new FPDF('p','mm',array(100,40));
      $myarray = array(1,2,3);
      $pdf->AddPage();
      $j=0;
      $pdf->SetFont('Arial','B',8);
      $party_code=key($barcodelist);
      $party_code=explode('-', $party_code);
      if($party_code)
      {
        $party_code=$party_code[0];
      }
      for ($i=1; $i <=$count ; $i++) {
        if($i != 1){
          if(($i-1)%6==0)
          {
            $pdf->AddPage();
            $j=0;
          }
        }
        $barcodelist=array_values($barcodelist);
        $Profile=$this->Global_Var_Profile['Profile'];
        $company_website=$Profile['web_site_adddress'];
        $company_contact_no=$Profile['mobile'];
        $Product=$this->Product->findByBarcode($barcodelist[$i-1]);
        if(!$Product)
          throw new Exception("Empty Product", 1);
        $barcode=$Product['Product']['barcode'];
        if(empty($Product['Product']['barcode_name']) && !empty($barcode))
        {

          $this->Product->id=$Product['Product']['id'];
          $BarcodeImageGeneration_return=$this->BarcodeImageGeneration((string)$barcode);
          if($BarcodeImageGeneration_return['result']=='Success')
            $barcode_image=$BarcodeImageGeneration_return['file'];
            if(!$this->Product->saveField('barcode_image',$barcode_image))
              throw new Exception("Error Processing barcode Updation", 1);
            $Product=$this->Product->findById($Product['Product']['id']);

        }
        if(!$Product['Product']['barcode_image'])
          throw new Exception("Empty image", 1);
        if(!$Product['Product']['name'])
          throw new Exception("Empty name", 1);
        if(!$Product['Product']['mrp'])
          throw new Exception("Empty mrp", 1);
        if(!$Product['Product']['wholesale_price'])
          throw new Exception("Empty wholesale_price", 1);
        $pdf->SetFont('Arial','B',8);
        $mrp=floatval($Product['Product']['mrp']);
        $wholesale_price=floatval($Product['Product']['wholesale_price']);
        $pdf->SetFont('Arial','B',4);

        $angle=0;
        if($j%2==0)
        {
          $sety=-5;
          $setx=0;
          $pdf->SetFont('Arial','B',8);
          $product_name=$Product['Product']['name'].' ('.$Product['Product']['code'].')';
          $pdf->text(5+8+$setx,7+($j*15+7)+1+$sety-5+3,$product_name);
          $pdf->SetFont('Arial','B',9.2);
          $pdf->Image('img/barcode/'.$Product['Product']['barcode_image'],5+5+$setx,1+4+($j*15+7+3.6)+$sety,70,25);
        }
        // else
        // {
        // }
        $j++;
      }
      $pdf->Output();
    } catch (Exception $e) {
      pr($e->getMessage());
    }
    exit;
  }
  public function SaleBarcodePrint($quotation_id)
  {
    try {
      $count=1;
      if(!$count)
        throw new Exception("Empty List", 1);
      require('fpdf/fpdf_barcode.php');
      $pdf = new FPDF('p','mm',array(100,40));
      $myarray = array(1,2,3);
      $QuotationItem=$this->QuotationItem->find('all',
        array(
          'conditions'=>array('QuotationItem.quotation_id'=>$quotation_id),
          'fields'=>array(
            'QuotationItem.product_id',
            'QuotationItem.quantity',
            'Product.barcode',
            'Product.barcode_name',
            'Product.barcode_image',
            'Product.name',
            'Product.mrp',
            'Product.wholesale_price',
            'Product.cost',
            'Product.colour',
          )
        ));
      foreach($QuotationItem as $key=>$value)
      {
        if(!empty($value['Product']['barcode']))
        {
          for ($i=1; $i <=$value['QuotationItem']['quantity'] ; $i++) {
            $pdf->AddPage();
            $pdf->SetFont('Arial','B',8);
            $Profile=$this->Global_Var_Profile['Profile'];
            $company_website=$Profile['web_site_adddress'];
            $company_contact_no=$Profile['mobile'];
            $Product=$this->Product->findByBarcode($value['Product']['barcode']);
            if(!$Product)
              throw new Exception("Empty Product", 1);
            if($Product['Product']['barcode_name'])
            {
              $brand=$Product['Product']['name'];
              $barcode_name=$Product['Product']['barcode_name'];
            }
            else
            {
              $brand='General';
              $barcode_name='';
            }
            if(!$Product['Product']['barcode_image'])
              throw new Exception("Empty image", 1);
            if(!$Product['Product']['name'])
              throw new Exception("Empty name", 1);
            if(!$Product['Product']['mrp'])
              throw new Exception("Empty mrp", 1);
            if(!$Product['Product']['wholesale_price'])
              throw new Exception("Empty wholesale_price", 1);
            $pdf->SetFont('Arial','B',8);
            $mrp=floatval($Product['Product']['mrp']);
            $wholesale_price=floatval($Product['Product']['wholesale_price']);
            $pdf->SetFont('Arial','B',4);
            $angle=0;
            $sety=-3;
            $setx=5;
            $pdf->SetFont('Arial','B',11);
            $pdf->text(5+$setx,7+(7)+1+$sety-3,trim(strtoupper($Product['ProductType']['name'])));
            $pdf->SetFont('Arial','B',8.2);
            $pdf->RotatedImage('img/barcode/'.$Product['Product']['barcode_image'],44+$setx,29+(7+3)+$sety,25,10,90);
            $pdf->text(20+$setx,13+(8.5)+$sety,substr(trim(strtoupper($Product['Product']['colour'])), 0,11));
            $pdf->SetFont('Arial','B',9.3);
            $pdf->text(18+$setx,18.8+(7)+$sety,$mrp);
            $pdf->SetFont('Arial','B',8.5);
            $pdf->text(5+$setx,10+(8)+$sety-0.5,substr(trim(strtoupper($Product['Product']['barcode_name'])), 0,21));
            $pdf->SetFont('Arial','',8.5);
            $pdf->text(5+$setx,13+(8.5)+$sety,'COLOUR : ');
            $pdf->text(5+$setx,18.8+(7)+$sety,'M.R.P : ');
            $pdf->SetFont('Arial','',7);
            $sety+=1.5;
            $pdf->text(5+$setx,22+(7.5)+$sety,'MKTD BY, ');
            $pdf->text(5+$setx,22+(10.5)+$sety,'CALICUT,KERALA');
            $pdf->SetFont('Arial','B',7.2);
            $pdf->text(5+$setx,21.2+(14)+$sety,$company_website);
            $pdf->SetFont('Arial','B',8);
            $pdf->text(17+$setx,22+(7.5)+$sety,'MOZAIK INDIA');
            $pdf->SetFont('Arial','',7.2);
            $pdf->text(5+$setx,22+(16)+$sety,'cust care no: '.$company_contact_no);
            $pdf->SetFont('Arial','',5);
            $pdf->text(18+$setx,19.2+(7)+$sety,'(INCL OF ALL TAXES)');
          }
        }
        
      }
      $pdf->Output();
    } catch (Exception $e) {
      pr($e->getMessage());
    }
    exit;
  }
  public function GetProductJson()
  {
   $this->Product->unbindModel(
    array('hasMany' => array(
      'PurchasedItem',
      'PurchaseReturnItem',
      'SaleItem',
      'SalesReturnItem',
      'StockLog',
      'Stock',
      'UnwantedList',
    )
  ));
   $this->Product->virtualFields = array(
    'Measuring_Unit' => "Unit.name",
    'CGST_Rate' => "Product.tax/2",
    'SGST_Rate' => "Product.tax/2",
    'IGST_Rate' => "Product.tax",
  );
   $Product=$this->Product->find('all',[
    'fields'=>[
      'Product.name as Product_Name',
      'Product.Measuring_Unit',
      'Product.hsn_code as HSN_Code',
      'Product.CGST_Rate',
      'Product.SGST_Rate',
      'Product.IGST_Rate',
    ]
  ]);
   header('Content-disposition: attachment; filename=Products.json');
   header('Content-type: application/json');
   echo json_encode($Product); exit;
  }
  public function get_product_quantity_by_warehouse($id=null)
{
  $conditions=[];
  $return['option']='<option value="">Select</option>';
  if($id)
  {
    $conditions['Stock.warehouse_id']=$id;
    // $conditions['Stock.quantity !=']=0;
  }
      $conditions['Product.type']=[2,8];
       $conditions['Product.active']=1;
  $this->Stock->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code,' (',Stock.quantity ,')')");
  $Product=$this->Stock->find('all',array(
    'conditions'=>$conditions,
    'fields'=>array(
      'Product.id',
      'Stock.product_name',
      'Stock.warehouse_id',
    )
  ));
  if($Product)
  {
   $return['result']='Success';
   foreach ($Product as $key => $value) 
   {
    if($value['Product']['id'])
      $return['option'].='<option value="'.$value['Product']['id'].'">'.$value['Stock']['product_name'].'</option>'; 
  }
}
else
{
  $return['result']='Empty';
}
echo json_encode($return);
exit;
}
}
