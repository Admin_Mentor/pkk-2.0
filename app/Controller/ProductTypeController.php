<?php
App::uses('AppController', 'Controller');
class ProductTypeController extends AppController {
	public $helpers= array('Html','Form');
	public $uses=array('Product','ProductType','Unit');
	public function index()
	{
	}
	public function ProductType_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='id';
		$columns[]='name';
		$columns[]='id';
		$conditions=[];
		$totalData=$this->ProductType->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'ProductType.name LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->ProductType->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->ProductType->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'ProductType.*',
			)
		));
		$sl_no=1;
		foreach ($Data as $key => $value) {
			$Data[$key]['ProductType']['id']=$sl_no++;
			$Data[$key]['ProductType']['action'] ='<span><i table_id="'.$value['ProductType']['id'].'" class="fa fa-2x fa-edit   edit_ProductType   blue-col"></i></span>&nbsp;&nbsp;';
			// $Data[$key]['ProductType']['action'] .="<span><i table_id='".$value['ProductType']['id']."' class='fa fa-2x fa-trash delete_ProductType blue-col'></i></span>";
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function ProductType_Save_ajax()
	{
		try {
			$data=$this->request->data['ProductType'];
			$data['name']=strtoupper(trim($data['name']));
			$latestRow = $this->ProductType->find('first', array(
                               'order' => array('id' => 'DESC') ));
			if(!empty($latestRow))
			{
				$prevCode = $latestRow['ProductType']['code'];
				$code = $prevCode + 1;
			}
			else{
				$code = '11';
			}
			$code_check=$this->ProductType->findByCode($code);
		    while ($code_check) {
		      $code++;
		      $code_check=$this->ProductType->findByCode($code);
		    }
			if(!$data['id'])
			{
				$Table_data=array(
				'name'=>trim(strtoupper($data['name'])),
				'code'=>trim($code),
				'created_at'=>date('Y-m-d h:i:s'),
				'updated_at'=>date('Y-m-d h:i:s'),
				);
				$this->ProductType->create();
				if(!$this->ProductType->save($Table_data))
				{
					$errors = $this->ProductType->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
			}
			else
			{
				$this->ProductType->id=$data['id'];
				$this->ProductType->saveField('name',$data['name']);
				$this->ProductType->saveField('updated_at',date('Y-m-d h:i:s'));
			}
			$return['result']='success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return); exit;
	}
	public function product_type_add_ajax()
	{
		try {
			$data=$this->request->data['ProductTypeAdd'];
			$latestRow = $this->ProductType->find('first', array(
                               'order' => array('id' => 'DESC') ));
			
			if(!empty($latestRow))
			{
				$prevCode = $latestRow['ProductType']['code'];
				$code = $prevCode + 1;
				// if($code<10)
				// {
				// 	$code = '0'.$code;
				// }
			}
			else{
				$code = '11';
			}
			
			$Table_data=array(
				'name'=>trim(strtoupper($data['name'])),
				'code'=>trim($code),
				'created_at'=>date('Y-m-d h:i:s'),
				'updated_at'=>date('Y-m-d h:i:s'),
				// 'code'=>trim(strtoupper($data['code'])),
				);
			$this->ProductType->create();
			if(!$this->ProductType->save($Table_data))
			{
				$errors = $this->ProductType->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$ProductType=$this->ProductType->findById($this->ProductType->getLastInsertId());
			$return['key']=$ProductType['ProductType']['id'];
			$return['value']=$ProductType['ProductType']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function edit_ajax(){
		try {
			$prdct_id=$this->request->data['prdct_id'];
			if(!$prdct_id)
				throw new Exception("Empty ProductType Id", 1);
			$prdct_typ=strtoupper(trim($this->request->data['prdct_typ']));
			$this->ProductType->id=$prdct_id;
			if(!$this->ProductType->saveField('name',$prdct_typ ))
				throw new Exception("Error Processing ProductType Name Updation", 1);
			if(!$this->ProductType->saveField('updated_at',date('Y-m-d H:i:s')))
				throw new Exception("Error Processing ProductType updated_at Updation", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function product_type_delete_ajax($id)
	{
		try {
			$Product=$this->Product->findByProductTypeId($id);
			if($Product)
				throw new Exception("This is Used In ".$Product['Product']['name'], 1);
			if(!$this->ProductType->delete($id))
				throw new Exception("Error Processing Request While delete", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function product_type_get_ajax($id)
	{
		try {
			$ProductType=$this->ProductType->findById($id);
			if(!$ProductType)
				throw new Exception("Empty ProductType", 1);
			$return['data']=$ProductType['ProductType'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function product_type_edit_ajax()
	{
		try {
			$data=$this->request->data['ProductTypeEdit'];
			$Table_data=array(
				'name'=>trim(strtoupper($data['name'])),
				'updated_at'=>trim(date('Y-m-d h:i:s')),
				// 'code'=>trim(strtoupper($data['code'])),
				);
			$this->ProductType->id=$data['id'];
			if(!$this->ProductType->save($Table_data))
			{
				$errors = $this->ProductType->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$ProductType=$this->ProductType->read();
			$return['key']=$ProductType['ProductType']['id'];
			$return['value']=$ProductType['ProductType']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function delete()
	{
		$this->before_load();
		$id=$this->request->params['pass'][0];
		$Product=$this->Product->findByProductTypeIdFk($id);
		if(!empty($Product))
		{
			$this->Flash->set('product Type Is Used.');
			$this->redirect(array('controller'=>'Product','action'=>'index'));
		}
		else
		{
			if($this->ProductType->delete($id))
			{
				$this->Flash->set(__('product type deleted.',h($id)));
				$this->redirect(array('controller'=>'Product','action'=>'index'));
			}
			else
			{
				$this->Flash->set(__('Error Occured'));
			}
		}
	}
	public function check_producttype_ajax(){
		$product_type=$this->request->data['prdct_type'];
		$result=$this->ProductType->find('all',array('conditions'=>array('ProductType.name'=>$product_type)));
		if(empty($result)){
			echo "No";
		}
		else{
			echo "Yes";
		}
		exit;
	}
	public function delete_ProductType_ajax($id)
	{
		$userid = $this->Session->read('User.id');
		if($this->ProductType->delete($id))
		{
			$return['result']="Success";
		}
		else
		{
			$return['result']="Error";
		}
		echo json_encode($return);
		exit;
	}
}