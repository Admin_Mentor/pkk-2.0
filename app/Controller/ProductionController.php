<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
App::import('Controller', 'Sale');
App::import('Controller', 'Stock');
class ProductionController extends AppController {
	public $uses=array(
		'Bom',
		'BomItem',
		'Production',
		'ProductionItem',
		'Product',
		'Unit',
		'Staff',
		'Stock',
		'Journal',
		'ExecutiveBonus',
		'ExecutiveBonusDetail',
		'Role',
		'SaleItem',
		'Sale',
		'Customer'
		);
	public function BomList(){
		$BOMList=array();
		$this->set(compact('BOMList'));
	}
	public function BomList_ajax()
	{
		$requestData=$this->request->data;
		$columns=[];
		$columns[]='Bom.id';
		$columns[]='Product.name';
		$columns[]='Bom.production_cost';
		$columns[]='Bom.created_at';
		$columns[]='Bom.updated_at';
		$columns[]='Bom.id';
		$conditions=[];
		$totalData=$this->Bom->find('count',['conditions'=>$conditions]);

		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Bom.id LIKE' =>'%'. $q . '%',
				'Product.name LIKE' =>'%'. $q . '%',
				// 'Bom.name LIKE' =>'%'. $q . '%',
			    'Bom.production_cost LIKE' =>'%'. $q . '%',
				'Bom.created_at LIKE' =>'%'. $q . '%',
				'Bom.updated_at LIKE' =>'%'. $q . '%',
				);
			$totalFiltered=$this->Bom->find('count',[
				'conditions'=>$conditions,
				]);
		}
		$Data=$this->Bom->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Bom.*',
				'Product.name',
				)
			));

		foreach ($Data as $key => $value) {
			$Data[$key]['Bom']['updated_at']=date('d-m-Y h:i:s',strtotime($value['Bom']['updated_at']));
			$Data[$key]['Bom']['created_at']=date('d-m-Y h:i:s',strtotime($value['Bom']['created_at']));
			$Data[$key]['Bom']['action']='<span><a target="_blank" href="'.$this->webroot.'Production/Bom/'.$value['Bom']['id'].'"><i class="fa fa-2x fa-eye"></i></a></span>';
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	}
	public function Bom($id=null){

		$user_id=1;
		$this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
		$Product_list=$this->Product->find('list',array('fields'=>['id','product_name']));
		$this->set(compact('Product_list'));
		$Unit_list=$this->Unit->find('list',array('fields'=>['id','name']));
		$this->set(compact('Unit_list'));

		if (!$this->request->data){
			if(empty($id)){
//$data['Bom']['date']=date('d-m-Y');
//$this->request->data=$data;
			} else {
				$Bom=$this->Bom->findById($id);
				$BomItem=$this->BomItem->find('all',array('conditions'=>array('bom_id'=>$id)));
				$this->request->data=$Bom;
				$this->set('BomItem',$BomItem);
			}
		} else {
			$datasource_Bom = $this->Bom->getDataSource();
			$datasource_BomItem = $this->BomItem->getDataSource();
			$data=$this->request->data;

			try {
				$datasource_Bom->begin();
				$datasource_BomItem->begin();
				if(empty($id))
				{
					$data=$data['Bom'];
					$Bom_product=$this->Bom->findByProductId($data['product_id']);
					if(!empty($Bom_product))
						throw new Exception("Exist in Production, Select another Product");
					$Bom_data=[
					'product_id'=>$data['product_id'],
					'production_cost'=>$data['production_cost'],
					'created_by'=>$user_id,
					'updated_by'=>$user_id,
					'created_at'=>date('Y-m-d H:i:s'),
					'updated_at'=>date('Y-m-d H:i:s'),
					];
					$this->Bom->create();
					if(!$this->Bom->save($Bom_data))
					{
						$errors = $this->Bom->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					$id=$this->Bom->getLastInsertId();
					$return_function=$this->bom_item_insert($data,$id);
					if($return_function['result']!='Success')
						throw new Exception($return_function['result']);
				}
				else
				{
					$data_bom=$data['Bom'];
					if(isset($data['BomItem']))
					{
						$old_BomItem=$data['BomItem'];
					}
					$Bom_data=[
					'production_cost'=>$data_bom['production_cost'],
					'updated_by'=>$user_id,
					'updated_at'=>date('Y-m-d H:i:s'),
					];
					$this->Bom->id=$id;
					if(!$this->Bom->save($Bom_data))
					{
						$errors = $this->Bom->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					if(isset($old_BomItem))
					{
						$return_function=$this->bom_item_update($old_BomItem);
						if($return_function['result']!='Success')
							throw new Exception($return_function['result']);
					}
					if(isset($data_bom['product_in_id'])){
						$return_function=$this->bom_item_insert($data_bom,$id);
						if($return_function['result']!='Success')
							throw new Exception($return_function['result']);
					}
				}
				$datasource_Bom->commit();
				$datasource_BomItem->commit();
				$return['result']='Success';
				$this->Session->setFlash(__($return['result']));
				$this->redirect(array('controller' => 'Production', 'action' => 'Bom',$id));
			}
			catch (Exception $e)
			{

				$datasource_Bom->rollback();
				$datasource_BomItem->rollback();
				$return['result']=$e->getMessage();
				$this->Session->setFlash(__($return['result']));
				$this->redirect( Router::url( $this->referer(), true ) );
			}
		}

	}
	public function bom_item_insert($bom_items,$id)
	{
		try {
			if($bom_items)
			{
				for ($i=0; $i <count($bom_items['product_in_id']) ; $i++) {
					$BomItem_data=[
					'product_id'=>$bom_items['product_in_id'][$i],
					'bom_id'=>$id,
					'unit_id'=>$bom_items['unit_id'][$i],
					'quantity'=>$bom_items['quantity'][$i],
					];
					$this->BomItem->create();
					if(!$this->BomItem->save($BomItem_data))
					{
						$errors = $this->BomItem->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
				}
			}
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function bom_item_update($bom_items)
	{
		try {
			if($bom_items)
			{
				for ($i=0; $i <count($bom_items['BomItem_id']) ; $i++) {
					$BomItem_data=[
					'quantity'=>$bom_items['quantity'][$i],
					];
					$this->BomItem->id=$bom_items['BomItem_id'][$i];
					if(!$this->BomItem->save($BomItem_data))
					{
						$errors = $this->BomItem->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
				}
			}
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function BomItem_delete($id)
	{
		$datasource_BomItem = $this->BomItem->getDataSource();

		try {
			$datasource_BomItem->begin();
			if(!$this->BomItem->delete($id))
				throw new Exception("Error while deleting", 1);
			$datasource_BomItem->commit();
			$return['result']='Success';
		} catch (Exception $e) {
			$datasource_BomItem->rollback();
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}

	public function ProductionList(){

	}
	public function ProductionList_ajax()
	{
		$requestData=$this->request->data;
		$columns=[];
		$columns[]='Production.id';
		$columns[]='Product.name';
		$columns[]='Production.total_cost';
		$columns[]='Staff.name';
		$columns[]='Production.start_date';
		$columns[]='Production.end_date';
		$columns[]='Production.created_at';
		$columns[]='Production.modified_at';
		$columns[]='Production.id';
		$conditions=[];
		$totalData=$this->Production->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Production.id LIKE' =>'%'. $q . '%',
				'Product.name LIKE' =>'%'. $q . '%',
				'Production.total_cost LIKE' =>'%'. $q . '%',
				'Staff.name LIKE' =>'%'. $q . '%',
				'Production.start_date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
				'Production.end_date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
				'Production.created_at LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
				'Production.modified_at LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
				);
			$totalFiltered=$this->Production->find('count',[
				'conditions'=>$conditions,
				]);
		}
		$Data=$this->Production->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Production.*',
				'Product.name',
				'Staff.name',
				)
			));
		
		foreach ($Data as $key => $value) {
			$Data[$key]['Production']['start_date']=date('d-m-Y',strtotime($value['Production']['start_date']));
			$Data[$key]['Production']['end_date']=date('d-m-Y',strtotime($value['Production']['end_date']));
			$Data[$key]['Production']['modified_at']=date('d-m-Y h:i:s',strtotime($value['Production']['modified_at']));
			$Data[$key]['Production']['created_at']=date('d-m-Y h:i:s',strtotime($value['Production']['created_at']));
			$Data[$key]['Production']['action']='<span><a target="_blank" href="'.$this->webroot.'Production/Production/'.$value['Production']['id'].'"><i class="fa fa-2x fa-eye"></i></a></span>';
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	}
	public function Production($id=null){
		$user_id=1;
		$stock_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'STOCK'));
		$production_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'FINAL PRODUCT'));
		$tailor_role_id=$this->Role->field('Role.id',array('Role.name'=>'TAILOR'));
		$this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
		$Product_list=$this->Product->find('list',array(
			"joins" => array(
				array(
					"table" => 'boms',
					"alias" => 'Bom',
					"type" => 'inner',
					"conditions" => array('Bom.product_id=Product.id'),
					),
				),
			'fields'=>['id','product_name']));
		$this->set(compact('Product_list'));
		$staffs=$this->Staff->find('list',array(
			'conditions'=>array('Staff.role_id'=>$tailor_role_id),
			'fields'=>['id','name']));
		$this->set(compact('staffs'));
		$Unit_list=$this->Unit->find('list',array('fields'=>['id','name']));
		$this->set(compact('Unit_list'));
		$Production=$this->Production->find('all',array('order' => 'Production.id DESC',));
		$Production_count=count($Production);
		$production_no=1;
		if(!empty($Production_count))
		{ 
			$Production=$this->Production->findByProductionNo($Production_count);
			while($Production)
			{
				$Production_count++;
				$Production=$this->Production->findByProductionNo($Production_count);
			}
			$production_no=$Production_count;
		}
		if (!$this->request->data){
			if(empty($id)){
				$data['Production']['start_date']=date('d-m-Y');
				$data['Production']['date']=date('d-m-Y');
				$data['Production']['end_date']=date('d-m-Y');
				$data['Production']['production_no']=$production_no;
				$data['Production']['status']=1;
				$this->request->data=$data;
			} else {
				$Production=$this->Production->findById($id);
				$ProductionItem=$this->ProductionItem->find('all',array('conditions'=>array('production_id'=>$id)));
				$row_total=0;
				foreach ($ProductionItem as $key => $value) {
					 $total=$value['ProductionItem']['total_quantity']*$value['Product']['cost'];
                       $row_total+=$total;
				}
				   $product_cost=($row_total+ $Production['Production']['total_cost'])/$Production['Production']['quantity'];
				$this->request->data=$Production;
				$this->request->data['Production']['product']=$Production['Production']['product_id'];
				$this->request->data['Production']['staff']=$Production['Production']['staff_id'];
				$this->request->data['Production']['production_quantity']=$Production['Production']['quantity'];
				$this->request->data['Production']['product_cost']=round($product_cost,4);
				$this->request->data['Production']['total_production_cost']=$Production['Production']['total_cost'];
				$this->request->data['Production']['date']=date('d-m-Y',strtotime($Production['Production']['start_date']));
				$this->request->data['Production']['end_date']=date('d-m-Y',strtotime($Production['Production']['end_date']));
				foreach ($ProductionItem as $key_item => $value_item) {
					$product_id=$value_item['ProductionItem']['product_id'];
					$unit_id=$value_item['ProductionItem']['unit_id'];
					$item_quantity=$value_item['ProductionItem']['single_production_quantity'];
					$item_total_quantity=$value_item['ProductionItem']['total_quantity'];
					$SaleController= new SaleController;
					$UnitLevelConvert=$SaleController->UnitLevelConvert($product_id,$unit_id);
					$sale_unit_level=$UnitLevelConvert['sale_unit_level'];
					$no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
					$product_unit_level=$UnitLevelConvert['product_unit_level'];
					$quantity=$item_quantity;
					$total_quantity=$item_total_quantity;
					if($sale_unit_level != 1){
						if($sale_unit_level == 2){
							$quantity=$item_quantity/$no_of_piece_per_unit;
							$total_quantity=$item_total_quantity/$no_of_piece_per_unit;
						}
					} 
					$ProductionItem[$key_item]['ProductionItem']['single_production_quantity']=$quantity;
					$ProductionItem[$key_item]['ProductionItem']['total_quantity']=$total_quantity;

				}
				$this->set('ProductionItem',$ProductionItem);
			}
		} else {
			$datasource_Production = $this->Production->getDataSource();
			$datasource_ProductionItem = $this->ProductionItem->getDataSource();
			$datasource_Stock = $this->Stock->getDataSource();
			$datasource_Journal = $this->Journal->getDataSource();
			$datasource_ExecutiveBonus = $this->ExecutiveBonus->getDataSource();
			$datasource_ExecutiveBonusDetail = $this->ExecutiveBonusDetail->getDataSource();
			
			$data=$this->request->data;

			try {
				$datasource_Production->begin();
				$datasource_ProductionItem->begin();
				if(empty($id))
				{

					$data=$data['Production'];
					$process=$data['process'];
					$Production_product=$this->Production->findByProductId($data['product_id']);
					$staff_id=$data['staff'];
					$tailor_cost=$data['total_production_cost'];
					$production_no=$data['production_no'];
					$Production_data=[
					'product_id'=>$data['product_id'],
					'staff_id'=>$data['staff'],
					'production_cost'=>$data['production_cost'],
					'production_no'=>$data['production_no'],
					'quantity'=>$data['production_quantity'],
					'total_cost'=>$data['total_production_cost'],
					'start_date'=>date('Y-m-d',strtotime($data['date'])),
					//'end_date'=>date('Y-m-d',strtotime($data['end_date'])),
					'created_by'=>$user_id,
					'modified_by'=>$user_id,
					'created_at'=>date('Y-m-d H:i:s'),
					'modified_at'=>date('Y-m-d H:i:s'),
					];
					$this->Production->create();
					if(!$this->Production->save($Production_data))
					{
						$errors = $this->Production->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					$id=$this->Production->getLastInsertId();
					$return_function=$this->production_item_insert($data,$id);
					if($return_function['result']!='Success')
						throw new Exception($return_function['result']);
					 $this->Product->id=$data['product_id'];
					$average_cost=$data['product_cost'];
					if(!$this->Product->saveField('cost',$average_cost))
					throw new Exception("Error In Product Updation", 38);
				}
				else
				{
					$data_production=$data['Production'];
					if(isset($data['ProductionItem']))
					{
						$old_ProductionItem=$data['ProductionItem'];
					}
					//$staff_id=$data_production['staff_id'];
					$staff_id=$data_production['staff'];
					$tailor_cost=$data_production['total_production_cost'];
					$production_no=$data_production['production_no'];
					$process=$data_production['process'];
					$Production_data=[
					'staff_id'=>$staff_id,
					'production_cost'=>$data_production['production_cost'],
					'production_no'=>$data_production['production_no'],
					'quantity'=>$data_production['production_quantity'],
					'total_cost'=>$data_production['total_production_cost'],
					'start_date'=>date('Y-m-d',strtotime($data_production['start_date'])),
					'end_date'=>date('Y-m-d',strtotime($data_production['end_date'])),
					'modified_by'=>$user_id,
					'modified_at'=>date('Y-m-d H:i:s'),
					];
					if($process=='update')
					{
						$production_rollback_function=$this->production_rollback($id);
						if($production_rollback_function['result']!='Success')
							throw new Exception($production_rollback_function['result']);
						$process='save';
					}
					$this->Production->id=$id;
					if(!$this->Production->save($Production_data))
					{
						$errors = $this->Production->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					
					if(isset($old_ProductionItem))
					{
						$return_function=$this->production_item_update($old_ProductionItem);
						if($return_function['result']!='Success')
							throw new Exception($return_function['result']);
					}
					$product_id=$this->Production->field('product_id',array('Production.id'=>$id));
					 $this->Product->id=$product_id;
					$average_cost=$data_production['product_cost'];
					if(!$this->Product->saveField('cost',$average_cost))
					throw new Exception("Error In Product Updation", 38);
					
				}
				if($process=='save')
				{
					$ProductionItem = $this->ProductionItem->find('all',array(
						'conditions' => array(
							'ProductionItem.production_id' => $id,
							),
						'fields' => array(
							'ProductionItem.product_id',
							'ProductionItem.total_quantity',
							'Production.modified_at',
							'Production.production_no',
							'Production.product_id',
							'Product.cost',
							'Product.name',
							'Production.quantity',
							'Production.total_cost',
							)
						));
					$tax_value=0;
					$discount=0;
					$net_value=0;
					$stock_value=0;
					$datasource_Stock->begin();
					$date=date('Y-m-d',strtotime($ProductionItem['0']['Production']['modified_at']));
					$production_cost=$ProductionItem['0']['Production']['total_cost'];
					foreach ($ProductionItem as $key => $value) {
						$quantity = $value['ProductionItem']['total_quantity'];
						$product_id=$value['ProductionItem']['product_id'];
						$stock_value+=$value['Product']['cost']*$quantity;
						$Stock = $this->Stock->findByProductId($product_id);
						$stock_id=$Stock['Stock']['id'];
						$stock_quantity=$Stock['Stock']['quantity'];
						$remark='Production Output--'.$value['Production']['production_no'].'('.$quantity.')';
						$stock_quantity-=$quantity;
						if($stock_quantity<=0)
							throw new Exception($value['Product']['name'].' is out of Stock'); 
						$StockController = new StockController;
						$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']); 
					}
					$input_quantity = $ProductionItem[0]['Production']['quantity'];
					$product_id=$ProductionItem[0]['Production']['product_id'];
					$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
					$input_product=$this->Product->findById($ProductionItem[0]['Production']['product_id']);
					$Stock = $this->Stock->findByProductId($product_id);
					$stock_id=$Stock['Stock']['id'];
					$stock_quantity=$Stock['Stock']['quantity'];
					$remark='Production Input--'.$ProductionItem[0]['Production']['production_no'].'('.$input_quantity.')';
					$stock_quantity+=$input_quantity;
					if($stock_quantity<=0)
							throw new Exception($input_product['Product']['name'].' is out of Stock'); 
					$StockController = new StockController;
					$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
					if($Stock_function_return['result']!='Success')
						throw new Exception($Stock_function_return['result']); 
					$datasource_Stock->commit();
					$datasource_Journal->begin();
					$datasource_ExecutiveBonus->begin();
					$datasource_ExecutiveBonusDetail->begin();

					if($staff_id!=0)
					{

						$ExecutiveBonus=$this->ExecutiveBonus->findByStaffIdAndMonthYear($staff_id,date('m-Y'));
						if(!$ExecutiveBonus)
						{
							$this->ExecutiveBonus->create();
							$ExecutiveBonus_data=[
								'staff_id'=>$staff_id,
								'month_year'=>date('m-Y'),
								'amount'=>0,
							];
							if(!$this->ExecutiveBonus->save($ExecutiveBonus_data)) { $errors = $this->ExecutiveBonus->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }
							$ExecutiveBonus=$this->ExecutiveBonus->findById($this->ExecutiveBonus->getLastInsertId());
						}
						$bonus_amount=$tailor_cost;
						$bonus_eligibility='Yes';
						if($bonus_amount>0)
						{
							$ExecutiveBonusDetails_data=[
								'receipt_no'=>null,
								'staff_id'=>$staff_id,
								'invoice_no'=>'Pn No:'.$production_no,
								'production_no'=>$production_no,
								'amount'=>$bonus_amount,
								'sale_date'=>date('Y-m-d'),
								'receipt_date'=>date('Y-m-d'),
								'eligibility'=>$bonus_eligibility,
								'bonus_amount'=>$bonus_amount,
								'bonus_return_amount'=>0,
								'journal_id'=>null,
							];

							if($ExecutiveBonusDetails_data['amount']>0)
							{
								$this->ExecutiveBonusDetail->create();
								if(!$this->ExecutiveBonusDetail->save($ExecutiveBonusDetails_data)) { $errors = $this->ExecutiveBonusDetail->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }
							}
						}
						$this->ExecutiveBonus->id=$ExecutiveBonus['ExecutiveBonus']['id'];
						$this->ExecutiveBonus->saveField('amount',$this->ExecutiveBonus->field('amount')+$bonus_amount);
					}
					// $branch_id=$this->Session->read('User.branch_id');
					// $work_flow='Production';
					// $production_no=$ProductionItem['0']['Production']['production_no'];
					// $remarks='Production No :'.$production_no;
					// $AccountingsController = new AccountingsController;
					// $voucher_no=$AccountingsController->GetVoucherNo();
					// if($stock_value)
					// {
					// 	$debit=$production_account_head_id;
					// 	$credit=$stock_account_head_id;
					// 	$Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$stock_value,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,'');
					// 	if($Accountings_function_return['result']!='Success')
					// 		throw new Exception($Accountings_function_return['message']);
					// }
					// $input_stock_value=number_format((($stock_value+$production_cost)),4,'.','');
					// if($input_stock_value)
					// {
					// 	$credit=$production_account_head_id;
					// 	$debit=$stock_account_head_id;
					// 	$Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$input_stock_value,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,'');
					// 	if($Accountings_function_return['result']!='Success')
					// 		throw new Exception($Accountings_function_return['message']);
					// }	
					$branch_id=$this->Session->read('User.branch_id');
					$work_flow='Production';
					$production_no=$ProductionItem['0']['Production']['production_no'];
					$remarks='Production No :'.$production_no;
					$AccountingsController = new AccountingsController;
					$voucher_no=$AccountingsController->GetVoucherNo();
					$manufacture_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'MANUFACTURING ACCOUNT'));
					if($stock_value)
					{
						$debit=$manufacture_account_head_id;
						$credit=$stock_account_head_id;
						$Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$stock_value,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,'');
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}
					$input_stock_value=number_format((($stock_value+$production_cost)),4,'.','');
					if($input_stock_value)
					{
						$credit=$manufacture_account_head_id;
						$debit=$production_account_head_id;
						$Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$input_stock_value,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,'');
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}	
					if($production_cost)
					{
						$credit=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'PRODUCTION BONUS OUTSTANDING'));
						$debit=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'PRODUCTION BONUS PAID'));
						$Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$production_cost,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,'');
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}	
					if($production_cost)
					{
						$debit=$manufacture_account_head_id;
						$credit=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'PRODUCTION BONUS PAID'));
						$Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$production_cost,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,'');
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}	
					$datasource_ExecutiveBonus->commit();
					$datasource_ExecutiveBonusDetail->commit();
					$datasource_Journal->commit();
				}
				$datasource_Production->commit();
				$datasource_ProductionItem->commit();
				$return['result']='Success';
				$this->Session->setFlash(__($return['result']));
				$this->redirect(array('controller' => 'Production', 'action' => 'Production'));
			}
			catch (Exception $e)
			{

				$datasource_Production->rollback();
				$datasource_ProductionItem->rollback();
				if($process=='save')
				{
					$datasource_Stock->rollback();
					$datasource_ExecutiveBonus->commit();
					$datasource_ExecutiveBonusDetail->commit();
					$datasource_Journal->rollback();
				}
				$return['result']=$e->getMessage();
				$this->Session->setFlash(__($return['result']));
				$this->redirect( Router::url( $this->referer(), true ) );
			}
		}
	}
	public function production_rollback($id)
	{
		$user_id=1;
		$datasource_Production = $this->Production->getDataSource();
		$datasource_Stock = $this->Stock->getDataSource();
		$datasource_ExecutiveBonusDetail = $this->ExecutiveBonusDetail->getDataSource();
		$datasource_ExecutiveBonus = $this->ExecutiveBonus->getDataSource();
		try {
			$datasource_Production->begin();
			$datasource_Stock->begin();
			$datasource_ExecutiveBonusDetail->begin();
			$datasource_ExecutiveBonus->begin();
			$Production=$this->Production->findById($id);
			//pr($Production);exit;
			if(!$Production)
				throw new Exception("Empty Production", 1);
			$this->Production->id=$id;
// if(!$this->Production->saveField('status','1'))
//   throw new Exception('Error While Updating', 1);
			$date=date('Y-m-d');
			$production_no=$Production['Production']['production_no'];
			$input_quantity = $Production['Production']['quantity'];
			$input_product_id=$Production['Production']['product_id'];
			$Stock = $this->Stock->findByProductId($input_product_id);
			$stock_id=$Stock['Stock']['id'];
			$stock_quantity=$Stock['Stock']['quantity'];
			$remark='Change Production Output --'.$production_no.'('.$input_quantity.')';
			$stock_quantity-=$input_quantity;
			$StockController = new StockController;

			$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
			if($Stock_function_return['result']!='Success')
				throw new Exception($Stock_function_return['result']); 
			
			foreach ($Production['ProductionItem'] as $key => $value) {
				$quantity = $value['total_quantity'];
				$product_id=$value['product_id'];
				$warehouse_id=1;
				$Stock = $this->Stock->find('first',array(
					'conditions'=>array(
						'product_id'=>$product_id,
						'warehouse_id'=>$warehouse_id,
						)
					));
				if(!$Stock)
					throw new Exception("Empty Stock", 1);
				$stock_id=$Stock['Stock']['id'];
				$stock_quantity=$Stock['Stock']['quantity'];
				
				$remark='Change Production Input --'.$production_no.'('.$quantity.')';
				$stock_quantity+=$quantity;
				$StockController = new StockController;
				$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
				if($Stock_function_return['result']!='Success')
					throw new Exception($Stock_function_return['result']);
			}
			$staff_id=$Production['Production']['staff_id'];
			$tailor_cost=$Production['Production']['total_cost'];
			if($staff_id!=0)
			{

				$ExecutiveBonus=$this->ExecutiveBonus->findByStaffIdAndMonthYear($staff_id,date('m-Y'));
				if(!$ExecutiveBonus)
				{
					$this->ExecutiveBonus->create();
					$ExecutiveBonus_data=[
						'staff_id'=>$staff_id,
						'month_year'=>date('m-Y'),
						'amount'=>0,
					];
					if(!$this->ExecutiveBonus->save($ExecutiveBonus_data)) { $errors = $this->ExecutiveBonus->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }
					$ExecutiveBonus=$this->ExecutiveBonus->findById($this->ExecutiveBonus->getLastInsertId());
				}
				$ExecutiveBonusDetail=$this->ExecutiveBonusDetail->find('all',array(
					'conditions'=>array(
						'ExecutiveBonusDetail.invoice_no'=>'Pn No:'.$Production['Production']['production_no']
					),
				));
				foreach ($ExecutiveBonusDetail as $key_EBD => $value_EBD) {
			    if(!$this->ExecutiveBonusDetail->delete($value_EBD['ExecutiveBonusDetail']['id']))
			        throw new Exception("Error in Processing", 1);
			    }
				$bonus_amount=$tailor_cost;
				//pr($bonus_amount);exit;
				$this->ExecutiveBonus->id=$ExecutiveBonus['ExecutiveBonus']['id'];
				$this->ExecutiveBonus->saveField('amount',$this->ExecutiveBonus->field('amount')-$bonus_amount);
			}
			$Journal=$this->Journal->find('all',array(
			  'conditions'=>array(
			    'remarks'=>'Production No :'.$production_no,
			    ),
			  'fields'=>array(
			    'Journal.*',
			    )
			  ));
			foreach ($Journal as $key => $value) {
			  if(!$this->Journal->delete($value['Journal']['id']))
			    throw new Exception("Error Processing Journal deletion", 1);
			}		
			$datasource_Production->commit();
			$datasource_Stock->commit();
			$datasource_ExecutiveBonusDetail->commit();
			$datasource_ExecutiveBonus->commit();

			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
			$datasource_Production->rollback();
			$datasource_Stock->rollback();
			$datasource_ExecutiveBonusDetail->rollback();
			$datasource_ExecutiveBonus->rollback();
		}
		return $return;
	}
	public function production_item_insert($production_items,$id)
	{
		try {
			if($production_items)
			{
				for ($i=0; $i <count($production_items['product']) ; $i++) {
					$item_quantity=$production_items['quantity'][$i];
					$item_total_quantity=$production_items['total_quantity'][$i];
					$unit_id=$production_items['unit_id'][$i];
					$SaleController= new SaleController;
					$UnitLevelConvert=$SaleController->UnitLevelConvert($production_items['product'][$i],$unit_id);
					$sale_unit_level=$UnitLevelConvert['sale_unit_level'];
					$no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
					$product_unit_level=$UnitLevelConvert['product_unit_level'];
					$quantity=$item_quantity;
					$total_quantity=$item_total_quantity;
					if($sale_unit_level != 1){
						if($sale_unit_level == 2){
							$quantity=$no_of_piece_per_unit*$item_quantity;
							$total_quantity=$no_of_piece_per_unit*$item_total_quantity;
						}
					}  
					$ProductionItem_data=[
					'product_id'=>$production_items['product'][$i],
					'production_id'=>$id,
					'unit_id'=>$unit_id,
					'single_production_quantity'=>$quantity,
					'no_of_productions'=>$production_items['production_quantity'],
					'total_quantity'=>$total_quantity,
					];
					$this->ProductionItem->create();
					if(!$this->ProductionItem->save($ProductionItem_data))
					{
						$errors = $this->ProductionItem->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
				}
			}
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function production_item_update($production_items)
	{
		try {
			if($production_items)
			{
				for ($i=0; $i <count($production_items['ProductionItem_id']) ; $i++) {
					$item_quantity=$production_items['quantity'][$i];
					$item_total_quantity=$production_items['total_quantity'][$i];
					$unit_id=$production_items['unit_id'][$i];
					$SaleController= new SaleController;
					$UnitLevelConvert=$SaleController->UnitLevelConvert($production_items['product_id'][$i],$unit_id);
					$sale_unit_level=$UnitLevelConvert['sale_unit_level'];
					$no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
					$product_unit_level=$UnitLevelConvert['product_unit_level'];
					$quantity=$item_quantity;
					$total_quantity=$item_total_quantity;
					if($sale_unit_level != 1){
						if($sale_unit_level == 2){
							$quantity=$no_of_piece_per_unit*$item_quantity;
							$total_quantity=$no_of_piece_per_unit*$item_total_quantity;
						}
					}
					$ProductionItem_data=[
//'quantity'=>$production_items['quantity'][$i],
					'total_quantity'=>$total_quantity,
					];
					$this->ProductionItem->id=$production_items['ProductionItem_id'][$i];
					if(!$this->ProductionItem->save($ProductionItem_data))
					{
						$errors = $this->ProductionItem->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
				}
			}
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function get_bom_of_product($product_id)
	{
		$return['result']='Error';
		$BomItem=$this->BomItem->find('all',array(
			'conditions'=>array('Bom.product_id'=>$product_id),
			'fields'=>array(
				'BomItem.product_id',
				'BomItem.unit_id',
				'BomItem.quantity',
				'Product.name',
				'Product.cost',
				'Product.unit_id',
				'Product.no_of_piece_per_unit',
				'Bom.product_id',
				'Bom.production_cost',
				'BomItem.unit_id',
				'Unit.name',
				),
			));
		$bomitem=[];
		if(!empty($BomItem)){
			foreach ($BomItem as $key => $value) {
				$single['BomItem']['product_id']=$value['BomItem']['product_id'];
				$single['BomItem']['name']=$value['Product']['name'];
				if($value['Product']['unit_id']==1)
				{
				$single['BomItem']['product_cost']=$value['Product']['cost'];
				}
				else
				{
			    $single['BomItem']['product_cost']=$value['Product']['cost']*$value['Product']['no_of_piece_per_unit'];		
				}
				$single['BomItem']['unit_id']=$value['BomItem']['unit_id'];
				$single['BomItem']['unit']=$value['Unit']['name'];
				$single['BomItem']['quantity']=$value['BomItem']['quantity'];
				array_push($bomitem,$single);
			}
			$return['BomItem']=$bomitem;
			$return['Bom']['production_cost']=$BomItem[0]['Bom']['production_cost'];
			$return['result']='Success';
		}
		else
		{
			$return['result']='Empty';
		}
		echo json_encode($return);
		exit;
	}
public function production_migration()
{
	$stock_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'STOCK'));
		$production_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'FINAL PRODUCT'));
		$manufacture_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'MANUFACTURING ACCOUNT'));
 $Production=$this->Production->find('all',array(
 ));
 
$user_id=1;
$datasource_Journal = $this->Journal->getDataSource();

try {
       $datasource_Journal->begin();
       $Journal=$this->Journal->find('all',array(
			  'conditions'=>array(
			    'remarks like'=>'Production No :%',
			     'flag'=>1,
			    ),
			  'fields'=>array(
			    'Journal.*',
			    )
			  ));
if($Journal)
{
foreach ($Journal as $key => $value1) {
$this->Journal->id=$value1['Journal']['id'];
$this->Journal->saveField('flag',0);
}
}
foreach ($Production as $key => $value) {
$branch_id=$this->Session->read('User.branch_id');
					$work_flow='Production';
					$production_no=$value['Production']['production_no'];
					$remarks='Production No :'.$production_no;
					$AccountingsController = new AccountingsController;
					$voucher_no=$AccountingsController->GetVoucherNo();
					$Journalvalue=$this->Journal->find('first',array(
			  'conditions'=>array(
			    'remarks'=>'Production No :'.$production_no,
			     'flag'=>0,
			      'credit'=>19,
			      'debit'=>22,
			    ),
			  'fields'=>array(
			    'Journal.amount', 'Journal.date',
			    )
			  ));
					$production_cost=$value['Production']['total_cost'];
					if($Journalvalue['Journal']['amount'])
					{
						$debit=$manufacture_account_head_id;
						$credit=$stock_account_head_id;
						$Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$Journalvalue['Journal']['amount'],$Journalvalue['Journal']['date'],$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,'');
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}
					$input_stock_value=number_format((($Journalvalue['Journal']['amount']+$production_cost)),4,'.','');
					if($input_stock_value)
					{
						$credit=$manufacture_account_head_id;
						$debit=$production_account_head_id;
						$Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$input_stock_value,$Journalvalue['Journal']['date'],$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,'');
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}	
					if($production_cost)
					{
						$credit=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'PRODUCTION BONUS OUTSTANDING'));
						$debit=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'PRODUCTION BONUS PAID'));
						$Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$production_cost,$Journalvalue['Journal']['date'],$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,'');
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}	
					if($production_cost)
					{
						$debit=$manufacture_account_head_id;
						$credit=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'PRODUCTION BONUS PAID'));
						$Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$production_cost,$Journalvalue['Journal']['date'],$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,'');
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}	

  $datasource_Journal->commit();
}
}
catch (Exception $e)
    {
        $datasource_Journal->rollback();
        $return['result']=$e->getMessage();
        $this->Session->setFlash(__($return['result']));
        $this->redirect( Router::url( $this->referer(), true ) );
    }
exit;
}
// public function sale_production_migrate()
// {
//     $Sale=$this->Sale->find('all',array(
//           'conditions'=>array(

//             ),
//           'fields'=>array(
//           	'Sale.invoice_no',
//           	'Sale.id',
//           	'Sale.account_head_id',
//           	'Sale.date_of_delivered',
//           	'Sale.executive_id'

//             )
//           ));
// $user_id=1;
//  	$stock_account_head_id=19;
//  $sale_account_head_id=6;
// $datasource_Journal = $this->Journal->getDataSource();

// try {
//        $datasource_Journal->begin();

//   foreach ($Sale as $key => $value) 
//    {
   	
//    	 $Journal=$this->Journal->find('first',array(
// 			  'conditions'=>array(
// 			     'remarks'=>'Sale Invoice No :'.$value['Sale']['invoice_no'],
// 			     'flag'=>1,
// 			    'Journal.credit'=>$stock_account_head_id,
// 			     'Journal.debit'=>$sale_account_head_id
// 			    ),
// 			  'fields'=>array(
// 			    'Journal.date',
// 			     'Journal.amount',
// 			    'Journal.id'
// 			    )
// 			  ));
        	
// $branch_id=$this->Session->read('User.branch_id');
// 	      $work_flow='Sales';
// 	      $date=$Journal['Journal']['date'];
//           $invoice_no=$value['Sale']['invoice_no'];
//            $account_head_id=$value['Sale']['account_head_id'];
//             $route_id=$this->Customer->field('route_id',array('Customer.account_head_id'=>$account_head_id));
//            $AccountingsController = new AccountingsController;
//           $voucher_no=$AccountingsController->GetVoucherNo();
//           $remarks='Sale Invoice No :'.$invoice_no;
//           $branch_id=$this->Session->read('User.branch_id');
//            $exe_id=$value['Sale']['executive_id'];
//           $executive_id=$exe_id;
//           $SaleItem=$this->SaleItem->find('all',array(
//           'conditions'=>array(
//            'SaleItem.sale_id'=>$value['Sale']['id'],
//             ),
//           'fields'=>array(
//           	'SaleItem.sale_id',
//           	'SaleItem.product_id',
//           	'SaleItem.quantity',

//             )
//           ));
//             $stock_value=0;$final_value=0;$a=0;
// 		foreach ($SaleItem as $key => $value1) 
// 		{
// 			$product_id=$value1['SaleItem']['product_id'];
//           $production = $this->Production->findByProductId($product_id);
//            if(!empty($production))
//             {
//             	$a=1;
//            $Journal1=$this->Journal->find('first',array(
// 			  'conditions'=>array(
// 			    'remarks'=>'Production No :'.$production['Production']['production_no'],
// 			     'flag'=>1,
// 			     'credit'=>25,
// 			      'debit'=>22
// 			    ),
// 			  'fields'=>array(
// 			    'Journal.amount',
// 			    'Journal.id'
// 			    )
// 			  ));
//            $journalvalue=$Journal1['Journal']['amount']/$production['Production']['quantity'];
//            $quantity = $value1['SaleItem']['quantity'];
//               $stock_cost_single=$journalvalue*$quantity;
            
//                $final_value+=$stock_cost_single;
//             }
//         }
//         if($a==1)
//         {
//     $this->Journal->id=$Journal['Journal']['id'];
//      $this->Journal->saveField('flag',0);

//         }
//         $stock_value=$Journal['Journal']['amount']-$final_value;
//          if($stock_value)
//           {
//             $debit=$sale_account_head_id;
//             $credit=$stock_account_head_id;

//             $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$stock_value,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,$executive_id,$route_id);
//             if($Accountings_function_return['result']!='Success')
//               throw new Exception($Accountings_function_return['message']);
//           }
//           $final_accound_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'FINAL PRODUCT'));;
//            if($final_value)
//           {
//             $debit=$sale_account_head_id;
//             $credit=$final_accound_id;

//             $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$final_value,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,$executive_id,$route_id);
//             if($Accountings_function_return['result']!='Success')
//               throw new Exception($Accountings_function_return['message']);
//           }
//      }
//     $datasource_Journal->commit();
// }
// catch (Exception $e)
//     {
//         $datasource_Journal->rollback();
//         $return['result']=$e->getMessage();
//         $this->Session->setFlash(__($return['result']));
//         $this->redirect( Router::url( $this->referer(), true ) );
//     }
// exit;
// }
}