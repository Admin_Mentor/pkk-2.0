<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
App::import('Controller', 'Sale');
App::import('Controller', 'Stock');
ini_set('memory_limit', '1024M');
class ProductionPlanController extends AppController {
public $uses=array(
	'Ingredient',
	'IngredientsItem',
	'Unit',
	'Product',
	'Customer',
	'Order',
	'OrderItem',
	'AccountHead',
	'Product',
	'Customer',
	'ProductionPlan',
	'ProductionPlanItem',
	'Executive',
	'Route',
	'Stock',
	'MaterialRequest',
	'MaterialRequestItem',
	'BomProduction',
	'StockTransfer',
	'StockTransferItem',
	'Warehouse',
	'Branch',
	'DamageRecycle',
	'DamageRecycleItem',
	'Brand',
	'LabourCalculation',
	'SystemParameter'
	);
public function IngredientsList(){
	$IngredientsList=array();
	$this->set(compact('IngredientsList'));
	$this->Session->write('User.production_plan',1);
}
public function Ingredient($id=null)
{
	$user_id=1;
	$Product_list_array=[];
	$this->Product->virtualFields = array('product_name' => "CONCAT(Product.name)");
	$this->Product->unbindModel(array('hasMany' => array('UnwantedList','SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
	$Product_list=$this->Product->find('list',array('order'=>'Product.name ASC',
		'conditions'=>['Product.active=1','type'=>[2,3],],'fields'=>['id','product_name']));
	foreach ($Product_list as $key => $value) {
			$Ingredient=$this->Ingredient->findByProductId($key);
			if(empty($Ingredient))
			{
				$Product_list_array[$key]=$value;
			}
	}
$Brand_list=$this->Brand->find('list',array(
    'fields'=>array('id','name'),
    'order'=>array('name ASC'),
  ));
  $Brand_list['0']='GENERAL';
  ksort($Brand_list);
  $this->set('Brand',$Brand_list);
	$ingredients_list=$this->Product->find('list',array('conditions'=>['Product.active=1','type'=>array(1,4,3,8)],'fields'=>['id','product_name']));
	$this->set(compact('Product_list_array'));
	$this->set(compact('ingredients_list'));
	$Unit_list=$this->Unit->find('list',array('fields'=>['id','name']));
	$this->set(compact('Unit_list'));
	if (!$this->request->data){
		if(empty($id)){
//$data['Ingredient']['date']=date('d-m-Y');
//$this->request->data=$data;
		} else {
			$Ingredient=$this->Ingredient->findById($id);
			$IngredientItem=$this->IngredientsItem->find('all',array('conditions'=>array('incredient_id'=>$id)));
			$this->request->data=$Ingredient;
			$this->set('IngredientsItem',$IngredientItem);
		}
	} else {
		$datasource_Ingredients = $this->Ingredient->getDataSource();
		$datasource_IngredientsItem = $this->IngredientsItem->getDataSource();
		$data=$this->request->data;
		try {
			$datasource_Ingredients->begin();
			$datasource_IngredientsItem->begin();
			if(empty($id))
			{
				$data=$data['Ingredient'];
				$Ingredient_product=$this->Ingredient->findByProductId($data['product_id']);
				if(!empty($Ingredient_product))
					throw new Exception("Exist in Production, Select another Product");
				$Ingredient_data=[
				'product_id'=>$data['product_id'],
				'created_by'=>$user_id,
				'updated_by'=>$user_id,
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				$this->Ingredient->create();
				if(!$this->Ingredient->save($Ingredient_data))
				{
					$errors = $this->Ingredient->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$id=$this->Ingredient->getLastInsertId();
				$return_function=$this->Ingredient_item_insert($data,$id);
				if($return_function['result']!='Success')
					throw new Exception($return_function['result']);
			}
			else
			{
				$data_Ingredient=$data['Ingredient'];
				if(isset($data['IngredientsItem']))
				{
					$old_IngredientItem=$data['IngredientsItem'];
				}
				$Ingredient_data=[
				'updated_by'=>$user_id,
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				$this->Ingredient->id=$id;
				if(!$this->Ingredient->save($Ingredient_data))
				{
					$errors = $this->Ingredient->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				if(isset($old_IngredientItem))
				{
					$return_function=$this->Ingredient_item_update($old_IngredientItem);
					if($return_function['result']!='Success')
						throw new Exception($return_function['result']);
				}
				if(isset($data_Ingredient['product_in_id'])){
					$return_function=$this->Ingredient_item_insert($data_Ingredient,$id);
					if($return_function['result']!='Success')
						throw new Exception($return_function['result']);
				}
			}
			$datasource_Ingredients->commit();
			$datasource_IngredientsItem->commit();
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
			$this->redirect(array('controller' => 'ProductionPlan', 'action' => 'Ingredient',$id));
		}
		catch (Exception $e)
		{

			$datasource_Ingredients->rollback();
			$datasource_IngredientsItem->rollback();
			$return['result']=$e->getMessage();
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
	}
}
public function Ingredient_item_insert($Ingredient_item,$id)
{
	try {
		if($Ingredient_item)
		{
			for ($i=0; $i <count($Ingredient_item['product_in_id']) ; $i++) {
				$IngredientItem_data=[
				'product_id'=>$Ingredient_item['product_in_id'][$i],
				'incredient_id'=>$id,
				'unit_id'=>$Ingredient_item['unit_id'][$i],
				'quantity'=>$Ingredient_item['quantity'][$i],
				'brand_id'=>$Ingredient_item['brand_id'][$i],
				];
				$this->IngredientsItem->create();
				if(!$this->IngredientsItem->save($IngredientItem_data))
				{
					$errors = $this->IngredientsItem->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}
public function Ingredient_item_update($Ingredient_item)
{
	try {
		if($Ingredient_item)
		{
			for ($i=0; $i <count($Ingredient_item['IngredientsItem_id']) ; $i++) {
				$IngredientItem_data=[
				'quantity'=>$Ingredient_item['quantity'][$i],
				];
				$this->IngredientsItem->id=$Ingredient_item['IngredientsItem_id'][$i];
				if(!$this->IngredientsItem->save($IngredientItem_data))
				{
					$errors = $this->IngredientsItem->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
			}
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}
public function IngredientsItem_delete($id)
{
	$datasource_IngredientsItem = $this->IngredientsItem->getDataSource();

	try {
		$datasource_IngredientsItem->begin();
		if(!$this->IngredientsItem->delete($id))
			throw new Exception("Error while deleting", 1);
		$datasource_IngredientsItem->commit();
		$return['result']='Success';
	} catch (Exception $e) {
		$datasource_IngredientsItem->rollback();
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);
	exit;
}
public function IngredientList_ajax()
{
	$requestData=$this->request->data;
	$columns=[];
	$columns[]='Ingredient.id';
	$columns[]='Product.name';
	$columns[]='Ingredient.created_at';
	$columns[]='Ingredient.updated_at';
	$columns[]='Ingredient.id';
	$conditions=[];
	$totalData=$this->Ingredient->find('count',['conditions'=>$conditions]);

	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Ingredient.id LIKE' =>'%'. $q . '%',
			'Product.name LIKE' =>'%'. $q . '%',
		   // 'Ingredient.name LIKE' =>'%'. $q . '%',
			'Ingredient.created_at LIKE' =>'%'. $q . '%',
			'Ingredient.updated_at LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->Ingredient->find('count',[
			'conditions'=>$conditions,
			]);
	}
	$Data=$this->Ingredient->find('all',array(
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'Ingredient.*',
			'Product.name',
			)
		));

	foreach ($Data as $key => $value) {
		$Data[$key]['Ingredient']['updated_at']=date('d-m-Y h:i:s',strtotime($value['Ingredient']['updated_at']));
		$Data[$key]['Ingredient']['created_at']=date('d-m-Y h:i:s',strtotime($value['Ingredient']['created_at']));
		$Data[$key]['Ingredient']['action']='<span><a target="_blank" href="'.$this->webroot.'ProductionPlan/Ingredient/'.$value['Ingredient']['id'].'"><i class="fa fa-2x fa-eye"></i></a></span>';
	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public function SemiProductionOrderIndex()
{
	$first_date = date('Y-m-d',strtotime('first day of this month'));
	$firstdate = date('d-m-Y');
	$this->set('firstdate', $firstdate);
	$last_date = date('Y-m-d');
	$todate = date('d-m-Y');
	$this->set('todate', $todate);

}
public function SemiProductionOrder($id=null)
{
	$user_id=1;
	$this->Product->virtualFields = array(
		'product_name' => "CONCAT(Product.name, ' ', Product.code)"
		);
	$Product_list=$this->Product->find('list',array('conditions'=>['Product.active=1','type'=>3],'fields'=>['id','product_name']));
	$this->set(compact('Product_list'));
	$Order=$this->Order->find('first',array('fields' => array('MAX(Order.order_no) as order_no')));
	if(!empty($Order))
	{
		$order_no=$Order[0]['order_no']+1;
	}
	else
	{
		$order_no=1;
	}
	$order_no_check=$this->Order->findByOrderNo($order_no);
	while ($order_no_check) {
		$order_no+=1;
		$order_no_check=$this->Order->findByOrderNo($order_no);        
	}
	if (!$this->request->data) 
	{
		if(empty($id))
		{
			$data['SemiProductionOrder']['order_no']=$order_no;
			$data['SemiProductionOrder']['date']=date('d-m-Y');
			$data['SemiProductionOrder']['id']='';
			$data['SemiProductionOrder']['status']=1;
			$data['SemiProductionOrder']['status_flag']=0;
			$this->request->data=$data;
		}
		else
		{

			$Order=$this->Order->findById($id);
			$this->request->data=$Order;
			$this->request->data['SemiProductionOrder']['status_flag']=1;
			$this->request->data['SemiProductionOrder']['id']=$Order['Order']['id'];
			$this->request->data['SemiProductionOrder']['date']=date('d-m-Y',strtotime($Order['Order']['date']));
			$this->request->data['SemiProductionOrder']['status']=$Order['Order']['status'];
			$this->request->data['SemiProductionOrder']['flag']=$Order['Order']['flag'];
			$this->request->data['SemiProductionOrder']['order_no']=$Order['Order']['order_no'];
			$OrderItem=$this->OrderItem->find('all',array(
				"joins" => array(
					),
				'conditions'=>array(
					'order_id'=>$id,
					),
				'fields'=>array(
					'OrderItem.*',
					'Product.code',
					'Product.name'
					)
				));
			$this->set('OrderItems',$OrderItem);

		}
	}
	else
	{
		$datasource_Order = $this->Order->getDataSource();
		$datasource_OrderItem = $this->OrderItem->getDataSource();
		try {
			$datasource_Order->begin();
			$datasource_OrderItem->begin();
			$data=$this->request->data;
			$id=$data['SemiProductionOrder']['id'];
			if($id){
				$data_Order=$data['SemiProductionOrder'];
				$data=array(
     // 'account_head_id'=>$data_Order['account_head_id'],
      //'order_no'=>$data_Order['order_no'],
					'date_of_production'=>date('Y-m-d',strtotime(($data_Order['date']))),
					'date'=>date('Y-m-d',strtotime(($data_Order['date']))),
					'date_of_delivered'=>date('Y-m-d',strtotime(($data_Order['date']))),
					'modified_by'=>$user_id,
					'modified_at'=>date('Y-m-d H:i:s'),
					'type_order'=>2,
					);
				$process=$data_Order['process'];
				if($process=='update')
				{
					if(!$this->OrderItem->DeleteAll(array('order_id'=>$id)))
						throw new Exception("Error Processing OrderItem deletion", 1);
					for ($i=0; $i <count($data_Order['product_id']) ; $i++) {
						$items = [
						'product_id' => $data_Order['product_id'][$i],
						'quantity' => $data_Order['quantity'][$i],
						'mrp' => 0,
						'order_id' => $id,
						];
						$this->OrderItem->create();
						if (!$this->OrderItem->save($items)) {
							$errors = $this->OrderItem->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}

					}
				}
				if($process=='cancel') { goto order_cancel; }
				if($process=='delete')
				{
					if(!$this->OrderItem->DeleteAll(array('order_id'=>$id)))
						throw new Exception("Error Processing OrderItem deletion", 1);
					if(!$this->Order->delete($id))
						throw new Exception("Error Processing Order deletion", 1);
					$datasource_Order->commit();
					$datasource_OrderItem->commit();
					$return['result']='Success';
					$this->Session->setFlash(__($return['result']));
					return $this->redirect(array('controller' => 'ProductionPlan', 'action' => 'SemiProductionOrder'));
				}
				if($process=='after_altration')
				{
					if(!$this->Order->delete($value['Order']['id']))
						throw new Exception("Error Processing  Order deletion", 1);
					$datasource_Order->commit();
					$process='delivery';
				}
				if($process=='delivery')
				{
					$GRN_data['status']=2;
				}
				$this->Order->id=$id;
				if(!$this->Order->save($data))
				{
					$errors = $this->Order->validationErrors;
					foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
				order_cancel :
				if($process=='cancel')
				{
					$this->Order->id=$id;
					if(!$this->Order->saveField('flag','0'))
						throw new Exception('Error While Cancelling', 1);
					if(!$this->Order->saveField('status','0'))
						throw new Exception('Error While Cancelling', 1);
					if(!$this->Order->saveField('second_status','0'))
						throw new Exception('Error While Cancelling', 1);
					if(!$this->Order->saveField('date',date('Y-m-d',strtotime($data_Order['date']))))
						throw new Exception('Error While Cancelling', 1);
				}
			}
			else
			{
				$data_Order=$data['SemiProductionOrder'];
				//$order_no=$data_Order['order_no'];
					$Order=$this->Order->find('first',array('fields' => array('MAX(Order.order_no) as order_no')));
					if(!empty($Order))
					{
					$order_no=$Order[0]['order_no']+1;
					}
					else
					{
					$order_no=1;
					}
					$order_no_check=$this->Order->findByOrderNo($order_no);
					while ($order_no_check) {
					$order_no+=1;
					$order_no_check=$this->Order->findByOrderNo($order_no);        
					}
				$Order=$this->Order->findByOrderNo($order_no);
				if(count($Order)<=0)
				{
					$data=array(
						'order_no'=>$order_no,
						'date'=>date('Y-m-d',strtotime($data_Order['date'])),
						'date_of_delivered'=>date('Y-m-d',strtotime(($data_Order['date']))),
						'date_of_production'=>date('Y-m-d',strtotime(($data_Order['date']))),
						'created_by'=>$user_id,
						'type_order'=>2,
						'modified_by'=>$user_id,
						'created_at'=>date('Y-m-d H:i:s',strtotime($data_Order['date'])),
						'updated_at'=>date('Y-m-d H:i:s',strtotime($data_Order['date'])),
						);
					$this->Order->create();
					if(!$this->Order->save($data))
					{
						$errors = $this->Order->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					$id=$this->Order->getLastInsertId();
					$Orders=$this->Order->findByOrderNo($order_no);
					$order_id=$this->Order->getLastInsertId();
					for ($i=0; $i <count($data_Order['product_id']) ; $i++) {
						$items = [
						'product_id' => $data_Order['product_id'][$i],
						'quantity' => $data_Order['quantity'][$i],
						'mrp' => 0,
						'order_id' => $order_id,
						];
						$this->OrderItem->create();
						if (!$this->OrderItem->save($items)) {
							$errors = $this->OrderItem->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}

					}
				}
			}
			$datasource_Order->commit();
			$datasource_OrderItem->commit();
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
			return $this->redirect(array('controller' => 'ProductionPlan', 'action' => 'SemiProductionOrder',$id)); 
		} catch (Exception $e) {
			$datasource_OrderItem->rollback();
			$datasource_Order->rollback();
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		return $this->redirect(array('controller' => 'ProductionPlan', 'action' => 'SemiProductionOrder',$id));


	}
}
public function Order($id=null)
{
	$sub_group_id=15;
	$user_id=1;
	//$Product_list=array();
	$this->Product->virtualFields = array(
		'product_name' => "CONCAT(Product.name, ' ', Product.code)"
		);
	$Product_list=$this->Product->find('list',array('conditions'=>['Product.active=1','Product.type'=>[2,8]],'fields'=>['id','product_name']));
// $this->AccountHead->virtualFields = array('customer_name' => "CONCAT(AccountHead.name,' ', Customer.code)");
	$AccountHead = $this->AccountHead->find('list', [
		'joins' => [
		[
		"table" => "customers",
		"alias" => "Customer",
		"type" => "INNER",
		"conditions" => ['AccountHead.id=Customer.account_head_id'],
		],
		],
		'fields' => [
		'AccountHead.id',
		'AccountHead.name',
		],
		]);
	$Executive_list = $this->Executive->find('list', [
		'fields' => [
		'Executive.id',
		'Executive.name',
		],
		]);
	$Route_list = $this->Route->find('list', [
		'fields' => [
		'Route.id',
		'Route.name',
		],
		]);
	$delivery_days=$this->Product->find('list',array(
			'order'=>array('name ASC'),
			'fields'=>array('Product.production_days','Product.production_days'),
			));
	$this->set('delivery_days_list',$delivery_days);
	$this->set('Route_list',$Route_list);
	$this->set('Customer_list',$AccountHead);
	$this->set('Executive_list',$Executive_list);
	$this->set(compact('Product_list'));
	$Order=$this->Order->find('first',array('fields' => array('MAX(Order.order_no) as order_no')));
	if(!empty($Order))
	{
		$order_no=$Order[0]['order_no']+1;
	}
	else
	{
		$order_no=1;
	}
	$order_no_check=$this->Order->findByOrderNo($order_no);
	while ($order_no_check) {
		$order_no+=1;
		$order_no_check=$this->Order->findByOrderNo($order_no);        
	}
	if (!$this->request->data) 
	{
		if(empty($id))
		{
			$data['Order']['order_no']=$order_no;
			$data['Order']['date']=date('d-m-Y');
			$data['Order']['date_of_production']=date('d-m-Y',strtotime('+1 day'));
			$data['Order']['date_of_delivered']=date('d-m-Y',strtotime('+3 day'));
			$data['Order']['id']='';
			$data['Order']['delivery_days']=3;
			$data['Order']['status']=1;
			$data['Order']['status_flag']=0;
			$this->request->data=$data;
		}
		else
		{
			$Order=$this->Order->findById($id);
			$Customer=$this->Customer->findByAccountHeadId($Order['Order']['account_head_id']);
			$this->request->data=$Order;
			$this->request->data['Order']['status_flag']=1;
			$this->request->data['Order']['delivery_days']=$Order['Order']['delivery_days'];
			$this->request->data['Order']['date']=date('d-m-Y',strtotime($Order['Order']['date']));
			$this->request->data['Order']['date_of_delivered']=date('d-m-Y',strtotime($Order['Order']['date_of_delivered']));
			$this->request->data['Order']['date_of_production']=date('d-m-Y',strtotime($Order['Order']['date_of_production']));
			$this->request->data['Order']['status']=$Order['Order']['second_status'];
           $this->request->data['Order']['account_head_id']=$Order['Order']['account_head_id'];
            $this->request->data['Order']['executive_id']=$Order['Order']['executive_id'];
            $this->request->data['Order']['route_id']=$Order['Order']['route_id'];
			$this->request->data['Order']['account_head_name']=$Customer['AccountHead']['name'];
            $this->request->data['Order']['executive_name']=$this->Executive->field('Executive.name',array('Executive.id'=>$Order['Order']['executive_id']));
            $this->request->data['Order']['route_name']=$this->Route->field('Route.name',array('Route.id'=>$Order['Order']['route_id']));
			
			$this->request->data['Order']['address']='';
			if(!empty($Customer)){
				$this->request->data['Order']['address']=$Customer['Customer']['place'];
			}
			$OrderItem=$this->OrderItem->find('all',array(
				"joins" => array(
					),
				'conditions'=>array(
					'order_id'=>$id,
					),
				'fields'=>array(
					'OrderItem.*',
					'Product.code',
					'Product.name'
					)
				));
			$this->set('OrderItems',$OrderItem);
		}
	}
	else
	{

		$datasource_Order = $this->Order->getDataSource();
		$datasource_OrderItem = $this->OrderItem->getDataSource();
		try {
			$datasource_Order->begin();
			$datasource_OrderItem->begin();
			$data=$this->request->data;
			if($id){
				$data_Order=$data['Order'];
				$data=array(
     // 'account_head_id'=>$data_Order['account_head_id'],
      //'order_no'=>$data_Order['order_no'],
					'date'=>date('Y-m-d',strtotime(($data_Order['date']))),
					'date_of_delivered'=>date('Y-m-d',strtotime(($data_Order['date_of_delivered']))),
					'date_of_production'=>date('Y-m-d',strtotime(($data_Order['date_of_production']))),
					'modified_by'=>$user_id,
					'modified_at'=>date('Y-m-d H:i:s'),
					);
				$process=$data_Order['process'];
				if($process=='update')
				{
					if(!$this->OrderItem->DeleteAll(array('order_id'=>$id)))
						throw new Exception("Error Processing OrderItem deletion", 1);
					for ($i=0; $i <count($data_Order['product_id']) ; $i++) {
						$items = [
						'product_id' => $data_Order['product_id'][$i],
						'quantity' => $data_Order['quantity'][$i],
						'mrp' => $data_Order['mrp'][$i],
						'order_id' => $id,
						];
						$this->OrderItem->create();
						if (!$this->OrderItem->save($items)) {
							$errors = $this->OrderItem->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}

					}
				}
				if($process=='cancel') { goto purchase_cancel; }
				if($process=='delete')
				{
					if(!$this->OrderItem->DeleteAll(array('order_id'=>$id)))
						throw new Exception("Error Processing OrderItem deletion", 1);
					if(!$this->Order->delete($id))
						throw new Exception("Error Processing Order deletion", 1);
					$datasource_Order->commit();
					$datasource_OrderItem->commit();
					$return['result']='Success';
					$this->Session->setFlash(__($return['result']));
					return $this->redirect(array('controller' => 'ProductionPlan', 'action' => 'Order'));
				}
				if($process=='after_altration')
				{
					if(!$this->Order->delete($value['Order']['id']))
						throw new Exception("Error Processing  Order deletion", 1);
					$datasource_Order->commit();
					$process='delivery';
				}
				if($process=='delivery')
				{
					$GRN_data['status']=2;
				}
				$this->Order->id=$id;
				if(!$this->Order->save($data))
				{
					$errors = $this->Order->validationErrors;
					foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
				purchase_cancel :
				if($process=='cancel')
				{
					$this->Order->id=$id;
					if(!$this->Order->saveField('flag','0'))
						throw new Exception('Error While Cancelling', 1);
					if(!$this->Order->saveField('status','0'))
						throw new Exception('Error While Cancelling', 1);
					if(!$this->Order->saveField('second_status','0'))
						throw new Exception('Error While Cancelling', 1);
					if(!$this->Order->saveField('date',date('Y-m-d',strtotime($data_Order['date']))))
						throw new Exception('Error While Cancelling', 1);
				}
			}
			else
			{
				$data_Order=$data['Order'];
				//$order_no=$data_Order['order_no'];
                $Order=$this->Order->find('first',array('fields' => array('MAX(Order.order_no) as order_no')));
						if(!empty($Order))
						{
							$order_no=$Order[0]['order_no']+1;
						}
						else
						{
							$order_no=1;
						}
						$order_no_check=$this->Order->findByOrderNo($order_no);
						while ($order_no_check) {
							$order_no+=1;
							$order_no_check=$this->Order->findByOrderNo($order_no);        
						}
				$Order=$this->Order->findByOrderNo($order_no);
				if(count($Order)<=0)
				{
					$data=array(
						'executive_id'=>$data_Order['executive_id'],
						'delivery_days'=>$data_Order['delivery_days'],
						'route_id'=>$data_Order['route_id'],
						'account_head_id'=>$data_Order['account_head_id'],
						'order_no'=>$order_no,
						'date'=>date('Y-m-d',strtotime($data_Order['date'])),
						'date_of_delivered'=>date('Y-m-d',strtotime(($data_Order['date_of_delivered']))),
						'date_of_production'=>date('Y-m-d',strtotime(($data_Order['date_of_production']))),
						'created_by'=>$user_id,
						'modified_by'=>$user_id,
						'created_at'=>date('Y-m-d H:i:s',strtotime($data_Order['date'])),
						'updated_at'=>date('Y-m-d H:i:s',strtotime($data_Order['date'])),
						);
					$this->Order->create();
					if(!$this->Order->save($data))
					{
						$errors = $this->Order->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					$id=$this->Order->getLastInsertId();
					$Orders=$this->Order->findByOrderNo($order_no);
					$order_id=$this->Order->getLastInsertId();
					for ($i=0; $i <count($data_Order['product_id']) ; $i++) {
						$items = [
						'product_id' => $data_Order['product_id'][$i],
						'quantity' => $data_Order['quantity'][$i],
						'mrp' => $data_Order['mrp'][$i],
						'order_id' => $order_id,
						];
						$this->OrderItem->create();
						if (!$this->OrderItem->save($items)) {
							$errors = $this->OrderItem->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}

					}
				}
			}
			$datasource_Order->commit();
			$datasource_OrderItem->commit();
			$return['result']='Success';
			$this->Session->setFlash(__($return['result']));
			return $this->redirect(array('controller' => 'ProductionPlan', 'action' => 'Order',$id)); 
		} catch (Exception $e) {
			$datasource_OrderItem->rollback();
			$datasource_Order->rollback();
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		return $this->redirect(array('controller' => 'ProductionPlan', 'action' => 'Order',$id));

	}

}
public function OrderIndex()
{
	$first_date = date('Y-m-d',strtotime('first day of this month'));
	$firstdate = date('d-m-Y');
	$this->set('firstdate', $firstdate);
	$last_date = date('Y-m-d');
	$todate = date('d-m-Y');
	$this->set('todate', $todate);
	// $AccountHead = $this->AccountHead->find('list', [
	// 	'joins' => [
	// 	[
	// 	"table" => "customers",
	// 	"alias" => "Customer",
	// 	"type" => "INNER",
	// 	"conditions" => ['AccountHead.id=Customer.account_head_id'],
	// 	],
	// 	],
	// 	'fields' => [
	// 	'AccountHead.id',
	// 	'AccountHead.name',
	// 	],
	// 	]);
	$Executive_list = $this->Executive->find('list', [
		'fields' => [
		'Executive.id',
		'Executive.name',
		],
		]);
	$Route_list = $this->Route->find('list', [
		'fields' => [
		'Route.id',
		'Route.name',
		],
		]);
	$this->set('Route_list',$Route_list);
	//$this->set('Customer_list',$AccountHead);
	$this->set('Executive_list',$Executive_list);

}
public function Searchcustomer() {
  $query=$this->request->query['q'];
  $AccountHead = $this->AccountHead->find('list', [
		'joins' => [
		[
		"table" => "customers",
		"alias" => "Customer",
		"type" => "INNER",
		"conditions" => ['AccountHead.id=Customer.account_head_id'],
		],
		],
		 'conditions'=>array(
      'AccountHead.name LIKE' => '%'. $query . '%',
    ),
		'fields' => [
		'AccountHead.id',
		'AccountHead.name',
		],
		]);
  $i=0;
  $AccountHeadList=array();
  foreach ($AccountHead as $key => $value) {
    if (false !== stripos($value, $query)) {
     $AccountHeadList[$i]['text'] = $value;
     $AccountHeadList[$i]['id'] = $key;
     $i++;
   }
 }

 $result['items'] = $AccountHeadList;
 echo json_encode($result);
 exit;
}
public function order_search_ajax()
{
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='Order.order_no';
	$columns[]='Order.date';
	$columns[]='Order.date_of_delivered';
	$columns[]='Route.name';
	$columns[]='Executive.name';
	$columns[]='AccountHead.name';
	$columns[]='Order.status';
	$columns[]='Order.id';
	$columns[]='Order.id';
	$conditions=[];
	if(isset($requestData['type']))
	{
		$conditions['type_order']=$requestData['type'];
	}
	if(isset($requestData['executive_id']))
	{
		if($requestData['executive_id'])
			$conditions['Order.executive_id']=$requestData['executive_id'];
	}
	if(isset($requestData['route_id']))
	{
		if($requestData['route_id'])
			$conditions['Order.route_id']=$requestData['route_id'];
	}
	if(isset($requestData['customer_id']))
	{
		if($requestData['customer_id'])
			$conditions['Order.account_head_id']=$requestData['customer_id'];
	}
	if(isset($requestData['from_date']) && isset($requestData['to_date']) )
	{
		// $conditions['Order.date between ? and ?']=[
		// date('Y-m-d',strtotime($requestData['from_date'])),
		// date('Y-m-d',strtotime($requestData['to_date']))
		// ];
		// $conditions['Order.date_of_delivered between ? and ?']=[
		// date('Y-m-d',strtotime($requestData['from_date'])),
		// date('Y-m-d',strtotime($requestData['to_date']))
		// ];
		$conditions['OR']=array(
			'Order.date' =>date('Y-m-d',strtotime($requestData['from_date'])),
			'Order.date_of_delivered' =>date('Y-m-d',strtotime($requestData['to_date'])),
			);
	}
	$totalData=$this->Order->find('count',['conditions'=>$conditions]);
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Order.date LIKE' =>'%'. $q . '%',
			'Order.date_of_delivered LIKE' =>'%'. $q . '%',
			'Route.name LIKE' =>'%'. $q . '%',
			'Executive.name LIKE' =>'%'. $q . '%',
			'AccountHead.name LIKE' =>'%'. $q . '%',
			'Order.order_no LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->Order->find('count',[
			'conditions'=>$conditions,
			]);
	}
	$Data=$this->Order->find('all',array(
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'Order.*',
			'Route.id',
			'Route.name',
			'Executive.name',
			'AccountHead.name',
			)
		));
	function get_status($status,$second_status,$production_no,$type)
	{
		if($status==0)
			$name='<span style="color:Orange;">CANCELLED<span>';
		if($status==1)
			$name='<span style="color:red;">ORDER PLACED<span>';
		// if($status==2 && $second_status==2 )
		// 	$name='<span style="color:yellow;">PRODUCTION STARTED<span>';
		// if($status==2 && $second_status==3)
		// 	$name='<span style="color:blue;">PRODUCTION COMPLETED<span>';
			if($status==2)
			{
				if($type==1)
				{
				$name='<span style="color:green;">ORDER DELIVERED<span>';	
				}
				else
				{
				$name='<span style="color:green;">PRODUCTION COMPLETED<span>';
				}
		   }
		return $name;
	}
	foreach ($Data as $key => $value) {
		$Data[$key]['Order']['no_of_items']=$this->OrderItem->find('count',['conditions'=>['order_id'=>$value['Order']['id']]]);
		$Data[$key]['Order']['date']=date('d-m-Y',strtotime($value['Order']['date']));
		$Data[$key]['Order']['date_of_delivered']=date('d-m-Y',strtotime($value['Order']['date_of_delivered']));
		$Data[$key]['Order']['status']=get_status($value['Order']['status'],$value['Order']['second_status'],$value['Order']['production_no'],$value['Order']['type_order']);
		if($value['Order']['type_order']==1)
		{
			$Data[$key]['Order']['action']='<span><a target="_blank" href="'.$this->webroot.'ProductionPlan/Order/'.$value['Order']['id'].'"><i class="fa fa-2x fa-eye"></i></span></a>';
		}
		else
		{
			$Data[$key]['Order']['action']='<span><a target="_blank" href="'.$this->webroot.'ProductionPlan/SemiProductionOrder/'.$value['Order']['id'].'"><i class="fa fa-2x fa-eye"></i></span></a>';
		}

	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public function SemiProduction($id=null)
{

	$user_id=1;
	$Production=$this->ProductionPlan->find('all',array('order' => 'ProductionPlan.id DESC',));
	$Production_count=count($Production);
	$production_no=1;
	if(!empty($Production_count))
	{ 
		$Production=$this->ProductionPlan->findByProductionNo($Production_count);
		while($Production)
		{
			$Production_count++;
			$Production=$this->ProductionPlan->findByProductionNo($Production_count);
		}
		$production_no=$Production_count;
	}
	if (!$this->request->data){
		if(empty($id)){
			$data['ProductionPlan']['date_of_production']=date('d-m-Y');
			$data['ProductionPlan']['production_no']=$production_no;
			$date_of_production=date('Y-m-d');
			$date_order=date('Y-m-d',strtotime('-1 day'));
			$order_status=$this->OrderItem->find('first',array('order'=>'Order.id DESC','conditions'=>array('Order.flag'=>1,'Order.type_order'=>2,'Order.date_of_production'=>$date_of_production,'OrderItem.status'=>1,'Order.status'=>1,)));
			$data['ProductionPlan']['status']="";
			if($order_status)
			{
				$data['ProductionPlan']['status']=$order_status['Order']['status'];	
			}
			$data['ProductionPlan']['status_flag']=0;
			$date_order=date('Y-m-d',strtotime('-1 day'));
			$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
			$Product_list=$this->Product->find('all',array('conditions'=>['Product.active=1','type'=>3],
				'fields'=>['Product.*']));
			$orderitem=[];
			$Product_array=[];
			if(!empty($Product_list)){
				foreach ($Product_list as $key => $value) {
					$quantity=0;
					$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
					$OrderItem=$this->OrderItem->find('first',array(
						'conditions'=>array(
							'Order.flag'=>1,'Order.status'=>1,'OrderItem.second_status'=>1,'Order.type_order'=>2,'Order.date_of_production'=>$date_of_production,'OrderItem.product_id'=>$value['Product']['id']),
						'fields'=>array('OrderItem.total_quantity'),
						));
					if(!empty($OrderItem['OrderItem']['total_quantity']))
					{
						$single['Product']['name']=$value['Product']['name'];
						$single['Product']['id']=$value['Product']['id'];
						$single['Product']['quantity']=$OrderItem['OrderItem']['total_quantity'];
						$Product_array[$value['Product']['id']]=$value['Product']['name'];
						array_push($orderitem,$single);
					}
				}
			}
			$this->set('orderitem',$orderitem);
			$this->set(compact('Product_array'));
			$this->request->data=$data;

		} 
		else 
		{

			$Production=$this->ProductionPlan->findById($id);
			$ProductionItem=$this->ProductionPlanItem->find('all',array('conditions'=>array('production_id'=>$id)));
			$this->request->data=$Production;
			$this->request->data['ProductionPlan']['date_of_production']=date('d-m-Y',strtotime($Production['ProductionPlan']['date']));
			$this->request->data['ProductionPlan']['date_of_order']=date('d-m-Y',strtotime($Production['ProductionPlan']['date_of_order']));
			$this->request->data['ProductionPlan']['date_of_delivered']=date('d-m-Y',strtotime($Production['ProductionPlan']['date_of_delivered']));
			$this->set('ProductionItem',$ProductionItem);
			$this->request->data['ProductionPlan']['status_flag']=1;
			 $Product=$this->Product->findById($Production['ProductionPlan']['product_id']);
		$this->request->data['ProductionPlan']['product_name']=$Product['Product']['name'];
		$this->request->data['ProductionPlan']['product_id']=$Product['Product']['id'];

		}
	}
	else
	{

		$datasource_ProductionPlan = $this->ProductionPlan->getDataSource();
	$datasource_ProductionPlanItem = $this->ProductionPlanItem->getDataSource();
	$datasource_Stock = $this->Stock->getDataSource();
	$datasource_Order = $this->Order->getDataSource();
	$datasource_OrderItem = $this->OrderItem->getDataSource();
	$datasource_BomProduction = $this->BomProduction->getDataSource();
	$datasource_MaterialRequest = $this->MaterialRequest->getDataSource();
	$data_all=$this->request->data;
		try {
			$datasource_ProductionPlan->begin();
		$datasource_ProductionPlanItem->begin();
		$datasource_Stock->begin();
		$datasource_Order->begin();
		$datasource_OrderItem->begin();
	   $datasource_BomProduction->begin();
	   	$datasource_MaterialRequest->begin();
			if(empty($id))
			{

				$data=$data_all['ProductionPlan'];
				$process=$data['process'];
				$production_no=$data['production_no'];
				$Production_data=[
				'production_no'=>$data['production_no'],
				'product_id'=>$data['product_id'],
				'type_production'=>2,
				'date'=>date('Y-m-d',strtotime($data['date_of_production'])),
				'date_of_order'=>date('Y-m-d',strtotime($data['date_of_production'])),
				'date_of_delivered'=>date('Y-m-d',strtotime($data['date_of_production'])),
				'created_by'=>$user_id,
				'modified_by'=>$user_id,
				'end_date'=>date('Y-m-d'),
			     'status'=>2,
				'created_at'=>date('Y-m-d H:i:s'),
				'modified_at'=>date('Y-m-d H:i:s'),
				];
				$this->ProductionPlan->create();
				if(!$this->ProductionPlan->save($Production_data))
				{
					$errors = $this->ProductionPlan->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$data_BOM=$data_all['BOM'];
				if(!empty($data_BOM))
				{
				for ($i=0; $i <count($data_BOM['product_id']) ; $i++) 
				{
				$BomProduction=[
				'production_no'=>$data['production_no'],
				'product_id'=>$data_BOM['product_id'][$i],
				'unit_id'=>$data_BOM['unit_id'][$i],
				'quantity'=>$data_BOM['quantity'][$i],
				'final_quantity'=>$data_BOM['quantity'][$i],
				];
				$this->BomProduction->create();
				if(!$this->BomProduction->save($BomProduction))
				{
				$errors = $this->BomProduction->validationErrors;
				foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
				}
				}
				$fromStock=$this->Stock->find('first',array(
				'conditions'=>array(
					'Stock.product_id'=>$data_BOM['product_id'][$i],
					'Stock.warehouse_id'=>1,
					),
				'fields'=>array('Stock.id','Stock.quantity')
				));
				$user_id=1;
			     if(!$fromStock)
				      throw new Exception("Empty Stock", 1);
				$fromQty=$fromStock['Stock']['quantity']-$data_BOM['quantity'][$i];
				if($fromQty<0)
				      throw new Exception("Empty From Stock", 1);
				  $remark='Production Ingredient --'.'('.$data_BOM['product_id'][$i].')'.'('.$data_BOM['quantity'][$i].')';
					$StockController = new StockController;
					$Stock_function_return=$StockController->GeneralStock_Edit_Function($fromStock['Stock']['id'],$fromQty,date('Y-m-d'),$user_id,$remark);
					if($Stock_function_return['result']!='Success')
						throw new Exception($Stock_function_return['result']);
				}
				}
				$id=$this->ProductionPlan->getLastInsertId();
				$Orders=$this->OrderItem->find('all',array('order'=>'Order.id DESC','conditions'=>array('OrderItem.product_id'=>$data['product_id'],'Order.type_order'=>2,'Order.flag'=>1,'OrderItem.second_status'=>1)));
				foreach ($Orders as $key1 => $value1) {
					$this->Order->id=$value1['Order']['id'];
					$get_order=$this->OrderItem->find('all',array('conditions'=>['OrderItem.second_status'=>1,'OrderItem.order_id'=>$value1['Order']['id']]));
					if(count($get_order)==1)
					{
                    if(!$this->Order->saveField('status','2'))
						throw new Exception('Error While status updating', 1);
					}
					// if(!$this->Order->saveField('second_status','3'))
					// 	throw new Exception('Error While status updating', 1);
					// if(!$this->Order->saveField('production_no',$data['production_no']))
				// 	throw new Exception('Error While status updating', 1);
				$this->OrderItem->id=$value1['OrderItem']['id'];
				if(!$this->OrderItem->saveField('status','2'))
					throw new Exception('Error While status updating', 1);
				if(!$this->OrderItem->saveField('second_status','3'))
					throw new Exception('Error While status updating', 1);
				if(!$this->OrderItem->saveField('production_no',$data['production_no']))
					throw new Exception('Error While status updating', 1);
				}
				$MaterialRequest=$this->MaterialRequest->find('first',array(
		'conditions'=>array('MaterialRequest.order_product_id'=>$data['product_id']),
		'fields'=>array('MaterialRequest.id'),
		));
			if($MaterialRequest){
			$this->MaterialRequest->id=$MaterialRequest['MaterialRequest']['id'];
				if(!$this->MaterialRequest->saveField('production_no',$data['production_no']))
					throw new Exception('Error While MaterialRequest updating', 1);
            }
				$return_function=$this->semi_production_plan_item_insert($data,$id);
				if($return_function['result']!='Success')
					throw new Exception($return_function['result']);

			}
			else
			{}
		$datasource_ProductionPlan->commit();
		$datasource_ProductionPlanItem->commit();
		$datasource_Stock->commit();
		$datasource_Order->commit();
		$datasource_OrderItem->commit();
		$datasource_BomProduction->commit();
		$datasource_MaterialRequest->commit();
		$return['result']='Success';
		//$this->Session->setFlash(_($return['result']));
		$this->redirect(array('controller' => 'ProductionPlan', 'action' => 'SemiProduction',$id));
	}
	catch (Exception $e)
	{
		$datasource_Stock->rollback();
		$datasource_ProductionPlan->rollback();
		$datasource_ProductionPlanItem->rollback();
		$datasource_OrderItem->rollback();
		$datasource_Order->rollback();
		$datasource_BomProduction->rollback();
		$datasource_MaterialRequest->rollback();
		$return['result']=$e->getMessage();
		$this->Session->setFlash(__($return['result']));
		$this->redirect( Router::url( $this->referer(), true ) );
	}
	
}

}
public function SemiProductionList()
{

}
public function ProductionPlan($id=null)
{
$user_id=1;
$Production=$this->ProductionPlan->find('all',array('order' => 'ProductionPlan.id DESC',));
$Production_count=count($Production);
$this->set('from',date("d-m-Y"));
	$this->set('to',date("d-m-Y"));
$production_no=1;
if(!empty($Production_count))
{ 
	$Production=$this->ProductionPlan->findByProductionNo($Production_count);
	while($Production)
	{
		$Production_count++;
		$Production=$this->ProductionPlan->findByProductionNo($Production_count);
	}
	$production_no=$Production_count;
}
	// $Product_list=$this->Product->find('list',array(
	// 	'conditions'=>['type'=>2],
	// 	'fields'=>['Product.id','Product.name']));
	// $this->set(compact('Product_list'));
$project=$this->SystemParameter->field('value',array('id'=>19));
		$this->set('type_project',$project);
if (!$this->request->data){
	if(empty($id)){
		$data['ProductionPlan']['date_of_order']=date('d-m-Y',strtotime('-1 day'));
		$data['ProductionPlan']['date_of_delivered']=date('d-m-Y',strtotime('+2 day'));
		$data['ProductionPlan']['date_of_production']=date('d-m-Y');
		$data['ProductionPlan']['production_no']=$production_no;
		$date_order=date('Y-m-d',strtotime('-1 day'));
		$date_of_production=date('Y-m-d');
		$date_of_delivered=date('Y-m-d',strtotime('+2 day'));
		$order_status=$this->OrderItem->find('first',array('order'=>'Order.id DESC','conditions'=>array('Order.flag'=>1,'Order.type_order'=>1,'Order.date_of_production'=>$date_of_production,'OrderItem.status'=>1,'Order.status'=>1,)));
		$data['ProductionPlan']['status']="";
		if($order_status)
		{
			$data['ProductionPlan']['status']=$order_status['OrderItem']['status'];	
		}
		$data['ProductionPlan']['status_flag']=0;
		$date_order=date('Y-m-d',strtotime('-1 day'));
		$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
		$Product_list=$this->Product->find('all',array('order'=>'Product.name ASC','conditions'=>['Product.active=1','type'=>2],
			'fields'=>['Product.*']));
		$orderitem=[];
		$Product_array=[];
		if(!empty($Product_list)){
			foreach ($Product_list as $key => $value) {
				$quantity=0;
				$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
				$OrderItem=$this->OrderItem->find('first',array(
					'conditions'=>array(
						'OrderItem.quantity !='=>0,'Order.flag'=>1,'Order.date_of_production'=>$date_of_production,'OrderItem.second_status'=>1,'Order.status'=>1,'Order.type_order'=>1,'OrderItem.product_id'=>$value['Product']['id']),
					'fields'=>array('OrderItem.total_quantity'),
					));
				if(!empty($OrderItem['OrderItem']['total_quantity']))
				{
					$single['Product']['name']=$value['Product']['name'];
					$single['Product']['id']=$value['Product']['id'];
					$single['Product']['quantity']=$OrderItem['OrderItem']['total_quantity'];
					$Product_array[$value['Product']['id']]=$value['Product']['name'];
					array_push($orderitem,$single);
				}
			}
		}
		$ingredients_list=[]; $Brand_list=[];
	$this->set(compact('ingredients_list'));
	 $this->set('Brand',$Brand_list);
		$this->set(compact('Product_array'));
		$this->set('orderitem',$orderitem);
		$this->request->data=$data;

	} 
	else 
	{

		$Production=$this->ProductionPlan->findById($id);
      $Product=$this->Product->findById($Production['ProductionPlan']['product_id']);
		$ProductionItem=$this->ProductionPlanItem->find('all',array('conditions'=>array('production_id'=>$id)));
		$this->request->data=$Production;
		$this->request->data['ProductionPlan']['date_of_production']=date('d-m-Y',strtotime($Production['ProductionPlan']['date']));
		$this->request->data['ProductionPlan']['date_of_order']=date('d-m-Y',strtotime($Production['ProductionPlan']['date_of_order']));
		$this->request->data['ProductionPlan']['date_of_delivered']=date('d-m-Y',strtotime($Production['ProductionPlan']['date_of_delivered']));
		$this->request->data['ProductionPlan']['production_labour']=floor($Production['ProductionPlan']['production_labour']);
	    $this->request->data['ProductionPlan']['packing_labour']=floor($Production['ProductionPlan']['packing_labour']);
		$this->set('ProductionItem',$ProductionItem);
		$BomProduction=$this->BomProduction->find('all',array(
	"joins"=>array(
		array(
			"table"=>'products',
			"alias"=>'Product',
			"type"=>'inner',
			"conditions"=>array('Product.id=BomProduction.product_id'),
			),
		array(
			"table"=>'units',
			"alias"=>'Unit',
			"type"=>'inner',
			"conditions"=>array('Unit.id=BomProduction.unit_id'),
			),
		array(
			"table"=>'brands',
			"alias"=>'Brand',
			"type"=>'left',
			"conditions"=>array('Brand.id=BomProduction.brand_id'),
			),
		),
	'conditions'=>array('BomProduction.additional_quantity !='=>0,'BomProduction.production_no'=>$Production['ProductionPlan']['production_no']),
	'fields'=>array('Product.name','Unit.name','Brand.name','BomProduction.additional_quantity'),
	));
		$BOM=[];
		if(!empty($BomProduction))
		{
		foreach ($BomProduction as $key => $value) 
		{
		$single['brand']=$value['Brand']['name'];
		if(!$value['Brand']['name'])
		{
		$single['brand']="GENERAL";
		}
		$single['product_name']=$value['Product']['name'];
		$single['unit']=$value['Unit']['name'];

		$single['quantity']=floatval($value['BomProduction']['additional_quantity']);
		array_push($BOM,$single);
		}
		}
$this->set('additional_BOM',$BOM);
		$this->request->data['ProductionPlan']['status_flag']=1;
		$this->request->data['ProductionPlan']['product_name']=$Product['Product']['name'];
		$this->request->data['ProductionPlan']['product_id']=$Product['Product']['id'];
		$ingredients_list=$this->Product->find('list',array('conditions'=>['Product.active=1','type'=>array(1,3)],'fields'=>['id','name']));
	$this->set(compact('ingredients_list'));
	$Brand_list=$this->Brand->find('list',array(
    'fields'=>array('id','name'),
    'order'=>array('name ASC'),
  ));
  $Brand_list['0']='GENERAL';
  ksort($Brand_list);
  $this->set('Brand_list',$Brand_list);

	}
}
else
{

	$datasource_ProductionPlan = $this->ProductionPlan->getDataSource();
	$datasource_ProductionPlanItem = $this->ProductionPlanItem->getDataSource();
	$datasource_Stock = $this->Stock->getDataSource();
	$datasource_Order = $this->Order->getDataSource();
	$datasource_OrderItem = $this->OrderItem->getDataSource();
	$datasource_BomProduction = $this->BomProduction->getDataSource();
	$datasource_MaterialRequest = $this->MaterialRequest->getDataSource();
	$data_all=$this->request->data;
	try {
		$datasource_ProductionPlan->begin();
		$datasource_ProductionPlanItem->begin();
		$datasource_Stock->begin();
		$datasource_Order->begin();
		$datasource_OrderItem->begin();
	   $datasource_BomProduction->begin();
	   	$datasource_MaterialRequest->begin();
		if(empty($id))
		{
			$data=$data_all['ProductionPlan'];
			$process=$data['process'];
			$production_no=$data['production_no'];
			$Production_data=[
			'production_no'=>$data['production_no'],
			'production_labour'=>$data['production_labour'],
			'packing_labour'=>$data['packing_labour'],
			'product_id'=>$data['product_id'],
			'type_production'=>1,
			'date'=>date('Y-m-d',strtotime($data['date_of_production'])),
			'date_of_order'=>date('Y-m-d',strtotime($data['date_of_production'])),
			'end_date'=>date('Y-m-d',strtotime($data['date_of_production'])),
			'date_of_delivered'=>date('Y-m-d',strtotime($data['date_of_delivered'])),
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s'),
			'modified_at'=>date('Y-m-d H:i:s'),
			];
			$this->ProductionPlan->create();
			if(!$this->ProductionPlan->save($Production_data))
			{
				$errors = $this->ProductionPlan->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$data_BOM=$data_all['BOM'];
			if(!empty($data_BOM))
			{
			for ($i=0; $i <count($data_BOM['product_id']) ; $i++) 
			{  $brand_id=$data_BOM['brand_id'][$i];
				if($brand_id=='null')
				{
					$brand_id="";
				}
				$BomProduction=[
				'production_no'=>$data['production_no'],
				'product_id'=>$data_BOM['product_id'][$i],
				'brand_id'=>$brand_id,
				'unit_id'=>$data_BOM['unit_id'][$i],
				'quantity'=>$data_BOM['quantity'][$i],
				'final_quantity'=>$data_BOM['quantity'][$i],
				];
				$this->BomProduction->create();
				if(!$this->BomProduction->save($BomProduction))
				{
					$errors = $this->BomProduction->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
            }
          }
			$id=$this->ProductionPlan->getLastInsertId();
			$date=date('Y-m-d',strtotime($data['date_of_production']));
			$Orders=$this->OrderItem->find('all',array('order'=>'Order.id DESC','conditions'=>array('OrderItem.product_id'=>$data['product_id'],'Order.type_order'=>1,'Order.flag'=>1,'Order.date_of_production'=>$date,'OrderItem.second_status'=>1)));
			foreach ($Orders as $key1 => $value1) {
				$this->Order->id=$value1['Order']['id'];
				// if(!$this->Order->saveField('status','2'))
				// 	throw new Exception('Error While status updating', 1);
				// if(!$this->Order->saveField('second_status','2'))
				// 	throw new Exception('Error While status updating', 1);
				// if(!$this->Order->saveField('production_no',$data['production_no']))
				// 	throw new Exception('Error While status updating', 1);
				$this->OrderItem->id=$value1['OrderItem']['id'];
				if(!$this->OrderItem->saveField('second_status','2'))
					throw new Exception('Error While status updating', 1);
				if(!$this->OrderItem->saveField('production_no',$data['production_no']))
					throw new Exception('Error While status updating', 1);

			}
			$MaterialRequest=$this->MaterialRequest->find('first',array(
		'conditions'=>array('MaterialRequest.order_product_id'=>$data['product_id'],'MaterialRequest.date_of_delivered'=>$date),
		'fields'=>array('MaterialRequest.id'),
		));
			if($MaterialRequest){
			$this->MaterialRequest->id=$MaterialRequest['MaterialRequest']['id'];
				if(!$this->MaterialRequest->saveField('production_no',$data['production_no']))
					throw new Exception('Error While MaterialRequest updating', 1);
            }
			$return_function=$this->production_plan_item_insert($data,$id);
			if($return_function['result']!='Success')
				throw new Exception($return_function['result']);
		}
		else
		{
			$user_id=1;
			$data_production=$data_all['ProductionPlan'];
			if(isset($data_all['ProductionPlanItem']))
			{
				$old_ProductionItem=$data_all['ProductionPlanItem'];
			}
			$process=$data_production['process'];
			$Production_data=[
			'end_date'=>date('Y-m-d'),
			'status'=>2,
			'modified_at'=>date('Y-m-d H:i:s'),
			];
			$this->ProductionPlan->id=$id;
			if(!$this->ProductionPlan->save($Production_data))
			{
				$errors = $this->ProductionPlan->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}

			$date=date('Y-m-d',strtotime($data_production['date_of_production']));
			$Orders=$this->OrderItem->find('all',array('order'=>'Order.id DESC','conditions'=>array('OrderItem.product_id'=>$data_production['product_id'],'Order.type_order'=>1,'Order.flag'=>1,'Order.date_of_production'=>$date,'OrderItem.second_status'=>2)));
			foreach ($Orders as $key1 => $value1) {
				$this->Order->id=$value1['Order']['id'];
				// if(!$this->Order->saveField('status','2'))
				// 	throw new Exception('Error While status updating', 1);
				// if(!$this->Order->saveField('second_status','3'))
				// 	throw new Exception('Error While status updating', 1);
				// if(!$this->Order->saveField('production_no',$data_production['production_no']))
				// 	throw new Exception('Error While status updating', 1);
				$this->OrderItem->id=$value1['OrderItem']['id'];
				if(!$this->OrderItem->saveField('second_status','3'))
					throw new Exception('Error While status updating', 1);
				if(!$this->OrderItem->saveField('production_no',$data_production['production_no']))
					throw new Exception('Error While status updating', 1);
			}
			if(isset($old_ProductionItem))
			{
				$return_function=$this->productionplan_item_update($old_ProductionItem,$data_production);
				if($return_function['result']!='Success')
					throw new Exception($return_function['result']);
			}

		}
		$datasource_ProductionPlan->commit();
		$datasource_ProductionPlanItem->commit();
		$datasource_Stock->commit();
		$datasource_Order->commit();
		$datasource_OrderItem->commit();
		$datasource_BomProduction->commit();
		$datasource_MaterialRequest->commit();
		$return['result']='Success';
		$this->Session->setFlash(__($return['result']));
		$this->redirect(array('controller' => 'ProductionPlan', 'action' => 'ProductionPlan',$id));
	}
	catch (Exception $e)
	{
		$datasource_Stock->rollback();
		$datasource_ProductionPlan->rollback();
		$datasource_ProductionPlanItem->rollback();
		$datasource_OrderItem->rollback();
		$datasource_Order->rollback();
		$datasource_BomProduction->rollback();
		$datasource_MaterialRequest->rollback();
		$return['result']=$e->getMessage();
		$this->Session->setFlash(__($return['result']));
		$this->redirect( Router::url( $this->referer(), true ) );
	}
	
}
}

public function productionplan_item_update($production_items,$data)
{
try {
	if($production_items)
	{
		$total_good_quantity=0;
		for ($i=0; $i <count($production_items['id']) ; $i++) {
			$damage_quantity=$production_items['damage_quantity'][$i];
			$good_quantity=$production_items['good_quantity'][$i];
			$item_total_quantity=$production_items['total_quantity'][$i];
			$total_good_quantity+=$good_quantity;
			$ProductionItem_data=[
			'good_quantity'=>$good_quantity,
			'damage_quantity'=>$damage_quantity,
			];
				//$good_quantity=$item_total_quantity-$damage_quantity;
			$this->ProductionPlanItem->id=$production_items['id'][$i];
			if(!$this->ProductionPlanItem->save($ProductionItem_data))
			{
				$errors = $this->ProductionPlanItem->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			else
			{   
				$user_id=1;
				$IngredientsItem=$this->IngredientsItem->find('all',array(
					'conditions'=>array('Ingredient.product_id'=>$production_items['product'][$i],'Product.type'=>4),
					'fields'=>array('Ingredient.product_id','IngredientsItem.*','Product.type','Product.name'),
					));
				if(!empty($IngredientsItem)){
					foreach ($IngredientsItem as $key1 => $value1) {
						$packing_quantity=$value1['IngredientsItem']['quantity']*$good_quantity;
						$fromStock=$this->Stock->find('first',array(
							'conditions'=>array(
								'Stock.product_id'=>$value1['IngredientsItem']['product_id'],
								'Stock.warehouse_id'=>1
								),
							'fields'=>array('Stock.id','Stock.quantity')));
						if($fromStock['Stock']['quantity'] < $packing_quantity)
							throw new Exception("Need ".$packing_quantity. " Stock for Packing Material : " .$value1['Product']['name']."", 1);	
						$frmQty=$fromStock['Stock']['quantity']-$packing_quantity;
						$remark='Production --'.$data['production_no'].'('.$value1['IngredientsItem']['product_id'].')'.'('.$packing_quantity.')';
						$StockController = new StockController;
						$Stock_function_return=$StockController->GeneralStock_Edit_Function($fromStock['Stock']['id'],$frmQty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
					}
				}

				$Stock = $this->Stock->find('first',array(
					'conditions'=>array(
						'Stock.product_id'=>$production_items['product'][$i],
						'Stock.warehouse_id'=>1,
						)
					));
				if(!$Stock)
					throw new Exception("Empty Stock", 1);
				$stock_id=$Stock['Stock']['id'];
				$user_id=1;
				$stock_quantity=$Stock['Stock']['quantity'];
				$remark='Production --'.$data['production_no'].'('.$production_items['product'][$i].')'.'('.$good_quantity.')';
				$stock_quantity+=$good_quantity;
				$StockController = new StockController;
				$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,date('Y-m-d'),$user_id,$remark);
				if($Stock_function_return['result']!='Success')
					throw new Exception($Stock_function_return['result']);
				if($damage_quantity>0)
				{
					$warehouse_id= $this->Warehouse->field('Warehouse.id',array('Warehouse.warehouse_id ' =>1));
					if(empty($warehouse_id))
						throw new Exception("Damage Warehouse Required", 1); 
					$toStock=$this->Stock->find('first',array(
						'conditions'=>array(
							'Stock.product_id'=>$production_items['product'][$i],
							'Stock.warehouse_id'=>$warehouse_id
							),
						'fields'=>array('Stock.id','Stock.quantity')));
					if(!$toStock)
					{
						$remark='Production --'.$data['production_no'].'('.$production_items['product'][$i].')'.'('.$damage_quantity.')';
						$flag=1;
						$StockController = new StockController;
						$Stock_function_return=$StockController->GeneralStock_Add_Function($warehouse_id,$production_items['product'][$i],$damage_quantity,date('Y-m-d'),$user_id,$remark,$flag);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result'], 1);
					}
					else
					{
						$toQty=$toStock['Stock']['quantity']+$damage_quantity;
						$remark='Production --'.$data['production_no'].'('.$production_items['product'][$i].')'.'('.$damage_quantity.')';
						$StockController = new StockController;
						$Stock_function_return=$StockController->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
					}
				}

			}
		}
		$BomProduction=$this->BomProduction->find('all',array(
			"joins"=>array(
				array(
					"table"=>'products',
					"alias"=>'Product',
					"type"=>'inner',
					"conditions"=>array('Product.id=BomProduction.product_id'),
					),
				array(
					"table"=>'units',
					"alias"=>'Unit',
					"type"=>'inner',
					"conditions"=>array('Unit.id=BomProduction.unit_id'),
					),
				),
			'conditions'=>array('BomProduction.production_no'=>$data['production_no']),
			'fields'=>array('BomProduction.*','Product.type'),
			));	
		if(!empty($BomProduction))
		{
			foreach ($BomProduction as $key2 => $value2) 
			{
				$bom_quantity=$value2['BomProduction']['quantity'];
				$additional_quantity=$value2['BomProduction']['additional_quantity'];
				$ProductionPlanItem=$this->ProductionPlanItem->find('first',array(
							'conditions'=>array('ProductionPlan.production_no'=>$data['production_no']),
							'fields'=>array('ProductionPlanItem.total_quantity','ProductionPlanItem.good_quantity','ProductionPlanItem.damage_quantity'),
							));
				$total_production_quantity=$ProductionPlanItem['ProductionPlanItem']['good_quantity']+$ProductionPlanItem['ProductionPlanItem']['damage_quantity'];
				$production_quantity=$ProductionPlanItem['ProductionPlanItem']['total_quantity'];
				$bom_final_quantity=$total_production_quantity*($bom_quantity/$ProductionPlanItem['ProductionPlanItem']['total_quantity']);
				if($value2['Product']['type']!=3)
				{
				if($total_production_quantity > $production_quantity)
				{
				$bom_final_quantity=$production_quantity*($bom_quantity/$ProductionPlanItem['ProductionPlanItem']['total_quantity']);
				}
			    }
				$IngredientsItem_bom=$this->IngredientsItem->find('all',array(
					'conditions'=>array('IngredientsItem.product_id'=>$value2['BomProduction']['product_id']),
					'fields'=>array('Ingredient.product_id','IngredientsItem.*'),
					));
						$updation_bom_quantity=round($bom_final_quantity,6);
						$this->BomProduction->id=$value2['BomProduction']['id'];
						if(!$this->BomProduction->saveField('quantity',$updation_bom_quantity))
							throw new Exception('Error While Quantity updating', 1);
						if(!$this->BomProduction->saveField('final_quantity',$updation_bom_quantity))
							throw new Exception('Error While Quantity updating', 1);
						$Stock = $this->Stock->find('first',array(
							'conditions'=>array(
								'Stock.product_id'=>$value2['BomProduction']['product_id'],
								'Stock.warehouse_id'=>1,
								)
							));
						if(!$Stock)
							throw new Exception("Empty Stock", 1);
						$stock_id=$Stock['Stock']['id'];
						$stock_quantity=$Stock['Stock']['quantity'];
						$total_stock_updation_quantity=$updation_bom_quantity+$additional_quantity;
						$remark='Production Ingredient --'.'('.$value2['BomProduction']['product_id'].')'.'('.$total_stock_updation_quantity.')';
						$from_qty=$stock_quantity-$total_stock_updation_quantity;
						if($from_qty<0)
						{
							$from_qty=$stock_quantity;
						}
						$StockController = new StockController;
						$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$from_qty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']); 
					}
		}
	}
	$return['result']='Success';
} catch (Exception $e) {
	$return['result']=$e->getMessage();
}
return $return;
}
public function semi_production_plan_item_insert($production_items,$id)
{
try {
	if($production_items)
	{
		for ($i=0; $i <count($production_items['product']) ; $i++) {
			$user_id=1;
			$item_quantity=$production_items['quantity'][$i];
			$quantity=$item_quantity;
			$ProductionItem_data=[
			'product_id'=>$production_items['product'][$i],
			'production_id'=>$id,
			'total_quantity'=>$item_quantity,
			];
			$this->ProductionPlanItem->create();
			if(!$this->ProductionPlanItem->save($ProductionItem_data))
			{
				$errors = $this->ProductionPlanItem->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			else
			{   
				$Stock = $this->Stock->find('first',array(
					'conditions'=>array(
						'Stock.product_id'=>$production_items['product'][$i],
						'Stock.warehouse_id'=>1,
						)
					));
				if(!$Stock)
					throw new Exception("Empty Stock", 1);
				$stock_id=$Stock['Stock']['id'];
				$stock_quantity=$Stock['Stock']['quantity'];

				$remark='Change Production Input --'.$production_items['production_no'].'('.$production_items['product'][$i].')'.'('.$item_quantity.')';
				$stock_quantity+=$item_quantity;
				$StockController = new StockController;
				$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$production_items['date_of_production'],$user_id,$remark);
				if($Stock_function_return['result']!='Success')
					throw new Exception($Stock_function_return['result']); 
			}
		}
	}
	$return['result']='Success';
} catch (Exception $e) {
	$return['result']=$e->getMessage();
}
return $return;
}
public function production_plan_item_insert($production_items,$id)
{
try {
	if($production_items)
	{
		for ($i=0; $i <count($production_items['product']) ; $i++) {
			$user_id=1;
			$item_quantity=$production_items['quantity'][$i];
			$quantity=$item_quantity;
			$ProductionItem_data=[
			'product_id'=>$production_items['product'][$i],
			'production_id'=>$id,
			'total_quantity'=>$item_quantity,
			];
			$this->ProductionPlanItem->create();
			if(!$this->ProductionPlanItem->save($ProductionItem_data))
			{
				$errors = $this->ProductionPlanItem->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			// else
			// {   
			// 	$Stock = $this->Stock->find('first',array(
			// 		'conditions'=>array(
			// 			'Stock.product_id'=>$production_items['product'][$i],
			// 			'Stock.warehouse_id'=>1,
			// 			)
			// 		));
			// 	if(!$Stock)
			// 		throw new Exception("Empty Stock", 1);
			// 	$stock_id=$Stock['Stock']['id'];
			// 	$stock_quantity=$Stock['Stock']['quantity'];

			// 	$remark='Change Production Input --'.$production_items['production_no'].'('.$production_items['product'][$i].')'.'('.$item_quantity.')';
			// 	$stock_quantity+=$item_quantity;
			// 	$StockController = new StockController;
			// 	$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$production_items['date_of_production'],$user_id,$remark);
			// 	if($Stock_function_return['result']!='Success')
			// 		throw new Exception($Stock_function_return['result']); 
			// }
		}
	}
	$return['result']='Success';
} catch (Exception $e) {
	$return['result']=$e->getMessage();
}
return $return;
}
public function get_order($date=null)
{
$return['result']='Error';
$date_order=date('Y-m-d',strtotime($date));
$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
$Product_list=$this->Product->find('all',array(
	'fields'=>['Product.*']));
$orderitem=[];
if(!empty($Product_list)){
	foreach ($Product_list as $key => $value) {
		$quantity=0;
		$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
		$OrderItem=$this->OrderItem->find('first',array(
			'conditions'=>array('Order.status !='=>2,'Order.date_of_delivered'=>$date_of_delivered,'OrderItem.product_id'=>$value['Product']['id']),
			'fields'=>array('OrderItem.total_quantity'),
			));
		if(!empty($OrderItem['OrderItem']['total_quantity']))
		{
			$single['order']['product_name']=$value['Product']['name'];
			$single['order']['product_id']=$value['Product']['id'];
			$single['order']['quantity']=$OrderItem['OrderItem']['total_quantity'];
			array_push($orderitem,$single);
		}


	}
	$return['orderitem']=$orderitem;
	$return['result']='Success';
}
else
{
	$return['result']='Empty';
}
echo json_encode($return);
exit;
}
public function individual_get_ajax($product_id=null,$date=null,$type_order=null,$id=null)
{
$return['result']='Error';
$date_of_production=date('Y-m-d',strtotime($date));
if(empty($id))
{
	if($type_order==1)
	{
		$Order_details=$this->OrderItem->find('all',array(
			'conditions'=>array('Order.flag'=>1,'Order.type_order'=>1,'Order.date_of_production'=>$date_of_production,'Order.status'=>1,'OrderItem.second_status'=>1,'OrderItem.product_id'=>$product_id),
			'fields'=>array('OrderItem.quantity','Order.order_no','Order.route_id','Order.executive_id'),
			));
	}
	else
	{
		$Order_details=$this->OrderItem->find('all',array(
			'conditions'=>array('Order.flag'=>1,'Order.type_order'=>2,'Order.date_of_production'=>$date_of_production,'Order.status'=>1,'OrderItem.second_status'=>1,'OrderItem.product_id'=>$product_id),
			'fields'=>array('OrderItem.quantity','Order.order_no'),
			));
	}

}
else
{
	$production_no=$this->ProductionPlan->field('production_no',array('ProductionPlan.id'=>$id));
	if($type_order==1)
	{
		$Order_details=$this->OrderItem->find('all',array(
			'conditions'=>array('Order.flag'=>1,'Order.type_order'=>1,'Order.date_of_production'=>$date_of_production,'OrderItem.status'=>2,'OrderItem.product_id'=>$product_id,'OrderItem.production_no'=>$production_no),
			'fields'=>array('OrderItem.quantity','Order.order_no','Order.route_id','Order.executive_id'),
			));
	}
	else
	{
		$Order_details=$this->OrderItem->find('all',array(
			'conditions'=>array('Order.flag'=>1,'Order.type_order'=>2,'OrderItem.status'=>2,'OrderItem.product_id'=>$product_id,'OrderItem.production_no'=>$production_no),
			'fields'=>array('OrderItem.quantity','Order.order_no'),
			));
	}

}
$orderitem=[];
if(!empty($Order_details)){
	foreach ($Order_details as $key => $value) {
		$single['order']['Executive']="";
		$single['order']['Route']="";
		if($type_order==1)
		{
			$single['order']['Executive']=$this->Executive->field('Executive.name',array('Executive.id'=>$value['Order']['executive_id']));
			$single['order']['Route']=$this->Route->field('Route.name',array('Route.id'=>$value['Order']['route_id']));
			$from = date('Y-m-d',strtotime('first day of this month'));
			$to = date('Y-m-d',strtotime('last day of this month'));
			$this->OrderItem->virtualFields=['average_consumption'=>"AVG(OrderItem.quantity)"];
			$average_consumption=$this->OrderItem->find('first',array(
				'conditions'=>array('(Order.date_of_production) between ? and ? '=>array($from,$to),
					'OrderItem.product_id'=>$product_id,'Order.route_id'=>$value['Order']['route_id']),
				'fields'=>array('OrderItem.average_consumption'),
				));
			$single['order']['average_consumption']=floatval($average_consumption['OrderItem']['average_consumption']);
		}
		$single['order']['quantity']=floatval($value['OrderItem']['quantity']);
		$from = date('Y-m-d',strtotime('first day of this month'));
		$to = date('Y-m-d',strtotime('last day of this month'));
		$single['order']['order_no']=$value['Order']['order_no'];
		array_push($orderitem,$single);
	}
	$return['orderitem']=$orderitem;
	$return['result']='Success';
}
else
{
	$return['result']='Empty';
}
echo json_encode($return);
exit;
}
public function get_production_order_list($date=null,$type_order=null)
{
$return['result']='Error';
$date_of_production=date('Y-m-d',strtotime($date));
$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
		$Product_list=$this->Product->find('all',array('order'=>'Product.name ASC','conditions'=>['Product.active=1','type'=>[2,8]],
			'fields'=>['Product.id','Product.name']));
		$orderitem=[];
		if(!empty($Product_list)){
			foreach ($Product_list as $key => $value) {
				$quantity=0;
				$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
				$OrderItem=$this->OrderItem->find('first',array(
					'conditions'=>array(
						'OrderItem.quantity !='=>0,'Order.flag'=>1,'Order.date_of_production'=>$date_of_production,'Order.status'=>1,'OrderItem.second_status'=>1,'Order.type_order'=>1,'OrderItem.product_id'=>$value['Product']['id']),
					'fields'=>array('OrderItem.total_quantity'),
					));
				if(!empty($OrderItem['OrderItem']['total_quantity']))
				{
					$single['Product']['name']=$value['Product']['name'];
					$single['Product']['id']=$value['Product']['id'];
					$single['order']['quantity']=floatval($OrderItem['OrderItem']['total_quantity']);
					array_push($orderitem,$single);
				}
			}
		}
if(!empty($orderitem))
{
		$return['orderitem']=$orderitem;
	$return['result']='Success';
}
else
{
	$return['orderitem']=[];
	$return['result']='Empty';
}
echo json_encode($return);
exit;
}
public function get_production_order_list_fetch()
{
$return['result']='Error';
$data=$this->request->data;
	$conditions=[];
	$from_date=date('Y-m-d',strtotime($data['from_date']));
	$to_date=date('Y-m-d',strtotime($data['to_date']));
$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
		$Product_list=$this->Product->find('all',array('order'=>'Product.name ASC','conditions'=>['Product.active=1','type'=>[8,2]],
			'fields'=>['Product.id','Product.name']));
		$orderitem=[];
		if(!empty($Product_list)){
			foreach ($Product_list as $key => $value) {
				$quantity=0;
				$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
				$OrderItem=$this->OrderItem->find('first',array(
					'conditions'=>array(
						'OrderItem.quantity !='=>0,'Order.flag'=>1,'Order.date_of_production'=>$to_date,'Order.status'=>1,'OrderItem.second_status'=>1,'Order.type_order'=>1,'OrderItem.product_id'=>$value['Product']['id']),
					'fields'=>array('OrderItem.total_quantity'),
					));
				if(!empty($OrderItem['OrderItem']['total_quantity']))
				{
					$single['Product']['name']=$value['Product']['name'];
					$single['Product']['id']=$value['Product']['id'];
					$single['order']['quantity']=floatval($OrderItem['OrderItem']['total_quantity']);
					array_push($orderitem,$single);
				}
			}
		}
if(!empty($orderitem))
{
		$return['orderitem']=$orderitem;
	$return['result']='Success';
}
else
{
	$return['orderitem']=[];
	$return['result']='Empty';
}
echo json_encode($return);
exit;
}
public function after_production_bill_of_material_get_ajax($production_no=null)
{

$return['result']='Empty';
$return['BOM']=[];
$BomProduction=$this->BomProduction->find('all',array(
	"joins"=>array(
		array(
			"table"=>'products',
			"alias"=>'Product',
			"type"=>'inner',
			"conditions"=>array('Product.id=BomProduction.product_id'),
			),
		array(
			"table"=>'units',
			"alias"=>'Unit',
			"type"=>'inner',
			"conditions"=>array('Unit.id=BomProduction.unit_id'),
			),
		array(
			"table"=>'brands',
			"alias"=>'Brand',
			"type"=>'left',
			"conditions"=>array('Brand.id=BomProduction.brand_id'),
			),
		),
	'conditions'=>array('BomProduction.quantity !='=>0,'BomProduction.production_no'=>$production_no),
	'fields'=>array('Product.name','Unit.name','Brand.name','BomProduction.quantity'),
	));
$BOM=[];
if(!empty($BomProduction))
{
	foreach ($BomProduction as $key => $value) 
	{
		$single['brand']=$value['Brand']['name'];
		if(!$value['Brand']['name'])
		{
      $single['brand']="GENERAL";
		}
		$single['product_name']=$value['Product']['name'];
		$single['unit']=$value['Unit']['name'];
		
		$single['quantity']=floatval($value['BomProduction']['quantity']);
		array_push($BOM,$single);
	}
	$return['BOM']=$BOM;    
	$return['result']='Success';
}
echo json_encode($return);
exit;

}
public function bill_of_material_get_ajax()
{
$return['result']='Empty';
$data=$this->request->data;
$date=$data['date'];
$product_id=$data['product_id'];
$type_order=$data['type_order'];
$date_of_production=date('Y-m-d',strtotime($date));
$brand = $this->Brand->find('list',array( 
    'fields'=>array('id','name'),
    'order'=>array('id ASC'),
    'order'=>array('id ASC'),));
  $return['Brand_list']=$brand;
$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
if($type_order==1)
{
	$Product_list=$this->Product->find('all',array('conditions'=>['Product.active=1','Product.type'=>[1,3,8]],
		'fields'=>['Product.*','Unit.*']));
	$Ingredientitem=[]; 
	$order_status=$this->OrderItem->find('first',array('order'=>'Order.id DESC','conditions'=>array('Order.flag'=>1,'Order.type_order'=>1,'Order.date_of_production'=>$date_of_production,'Order.status'=>1,'OrderItem.status'=>1,'OrderItem.Product_id'=>$product_id)));
	$return['order_status']=0;
	if($order_status)
	{
		$return['order_status']=$order_status['OrderItem']['status'];	
	}
}
else
{
	$Product_list=$this->Product->find('all',array('conditions'=>['Product.active=1','Product.type'=>1],
		'fields'=>['Product.*','Unit.*']));
	$Ingredientitem=[]; 
	$order_status=$this->OrderItem->find('first',array('order'=>'Order.id DESC','conditions'=>array('Order.flag'=>1,'Order.type_order'=>2,'Order.date_of_production'=>$date_of_production,'Order.status'=>1,'OrderItem.status'=>1,'OrderItem.Product_id'=>$product_id)));
	$return['order_status']=0;
	if($order_status)
	{
		$return['order_status']=$order_status['OrderItem']['status'];	
	}
}
if(!empty($Product_list)){
	foreach ($Product_list as $key => $value) {
		$conditions=[];
			$conditions['Ingredient.product_id']=$product_id;
		$conditions['IngredientsItem.product_id']=$value['Product']['id'];
		$IngredientsItem=$this->IngredientsItem->find('all',array(
			'conditions'=>$conditions,
			'fields'=>array('Ingredient.product_id','IngredientsItem.*'),
			));
		$Stock = $this->Stock->findByProductIdAndWarehouseId($value['Product']['id'],1);
		$this->BomProduction->virtualFields=['total_bom_quantity'=>"SUM(BomProduction.quantity)",'total_bom_additional_quantity'=>"SUM(BomProduction.additional_quantity)"];
		$BomProduction=$this->BomProduction->find('first',array(
			"joins"=>array(
				array(
					"table"=>'production_plans',
					"alias"=>'ProductionPlan',
					"type"=>'inner',
					"conditions"=>array('ProductionPlan.production_no=BomProduction.production_no'),
					),
				),
			'conditions'=>array('ProductionPlan.status'=>1,'BomProduction.product_id'=>$value['Product']['id']),
			'fields'=>array('BomProduction.total_bom_quantity','BomProduction.total_bom_additional_quantity'),
			));	
		$stock_quantity=$Stock['Stock']['quantity'];
		$total_bom_quantity=0;
		$total_bom_additional_quantity=0;
		if($BomProduction['BomProduction']['total_bom_quantity'])
		{
			$total_bom_quantity=$BomProduction['BomProduction']['total_bom_quantity'];
		}
		if($BomProduction['BomProduction']['total_bom_additional_quantity'])
		{
		$total_bom_additional_quantity=$BomProduction['BomProduction']['total_bom_additional_quantity'];
		}
		$stock_quantity=$stock_quantity-($total_bom_quantity+$total_bom_additional_quantity);

		if($stock_quantity<0)
		{
			$stock_quantity=0;
		}
		$quantity=0;$count=0;
		if(!empty($IngredientsItem)){
			foreach ($IngredientsItem as $key1 => $value1) {
				$single['IngredientsItem']['brand_id']=$value1['IngredientsItem']['brand_id'];
				$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];     
					if($type_order==1)
					{         
						$ProductionPlan=$this->OrderItem->find('first',array(
							'conditions'=>array('Order.flag'=>1,'Order.type_order'=>1,'Order.date_of_production'=>$date_of_production,'Order.status'=>1,'OrderItem.second_status'=>1,'OrderItem.product_id'=>$value1['Ingredient']['product_id']),
							'fields'=>array('OrderItem.total_quantity'),
							));
					}
					else
					{
						$ProductionPlan=$this->OrderItem->find('first',array(
							'conditions'=>array('Order.flag'=>1,'Order.type_order'=>2,'Order.date_of_production'=>$date_of_production,'Order.status'=>1,'OrderItem.second_status'=>1,'OrderItem.product_id'=>$value1['Ingredient']['product_id']),
							'fields'=>array('OrderItem.total_quantity'),
							));
					}
				if(!empty($ProductionPlan))
				{
					$quantity=$quantity+($ProductionPlan['OrderItem']['total_quantity']*$value1['IngredientsItem']['quantity']);
					$count=1;
				}
			}
			if($quantity!=0)
			{   
				$single['IngredientsItem']['product_id']=$value['Product']['id'];
				$single['IngredientsItem']['product_type_id']=$value['Product']['type'];
				$single['IngredientsItem']['product_name']=$value['Product']['name'];
				$single['IngredientsItem']['unit']=$value['Unit']['name'];
				$single['IngredientsItem']['quantity']=round($quantity,6);
				$single['IngredientsItem']['unit_id']=$value['Unit']['id'];
				$single['IngredientsItem']['shortage']=0;
				if($quantity>$Stock['Stock']['quantity'])
				{
					$single['IngredientsItem']['shortage']=round(($quantity-$stock_quantity),6);
				}
				$single['IngredientsItem']['stock_quantity']=round($stock_quantity,6);
				array_push($Ingredientitem,$single);
			}
			$return['result']='Success';
			if(empty($Ingredientitem))
			{
				$return['result']='Empty';
			}
			$return['Ingredient']=$Ingredientitem;    
			if($type_order==1)
			{  
				$OrderItem_check=$this->OrderItem->find('all',array(
					'conditions'=>array('OrderItem.product_id'=>$product_id,'Order.flag'=>1,'Order.type_order'=>1,'Order.date_of_production'=>$date_of_production,'OrderItem.second_status'=>1,'Order.status'=>1),
					'fields'=>array('OrderItem.product_id'),
					));
			}
			else
			{
				$OrderItem_check=$this->OrderItem->find('all',array(
					'conditions'=>array('OrderItem.product_id'=>$product_id,'Order.flag'=>1,'Order.type_order'=>2,'Order.date_of_production'=>$date_of_production,'OrderItem.second_status'=>1,'Order.status'=>1),
					'fields'=>array('OrderItem.product_id'),
					));
			}
			foreach ($OrderItem_check as $key => $value3) {
				$Ingredients=$this->Ingredient->find('first',array(
					'conditions'=>array('Ingredient.product_id'=>$value3['OrderItem']['product_id']),
					));
				if(empty($Ingredients))
				{
					$return['result']='Ingredients Empty';	
				}
			}

		}

	}
}
else
{
	$return['result']='Empty';
}
echo json_encode($return);
exit;
}
public function ProductionPlanList(){
	$first_date = date('Y-m-d',strtotime('first day of this month'));
	$firstdate = date('d-m-Y');
	$this->set('firstdate', $firstdate);
	$last_date = date('Y-m-d');
	$todate = date('d-m-Y',strtotime('+1 days'));
	$this->set('todate', $todate);
	$products=$this->ProductionPlanItem->find('list',[
			"joins"=>array(
				array(
					"table"=>'products',
					"alias"=>'Product',
					"type"=>'inner',
					"conditions"=>array('Product.id=ProductionPlanItem.product_id'),
				),
			),
			'fields'=>['Product.id','Product.name']
		]);
	$this->set('products',$products);

}
public function ProductionPlanList_ajax()
{
$requestData=$this->request->data;
$columns=[];
$columns[]='ProductionPlan.production_no';
$columns[]='ProductionPlan.date';
$columns[]='ProductionPlan.date_of_delivered';
$columns[]='ProductionPlan.id';
$columns[]='ProductionPlan.id';
$columns[]='ProductionPlan.id';
$columns[]='ProductionPlan.id';
$columns[]='ProductionPlan.id';
$conditions=[];
if(isset($requestData['type']))
{
	$conditions['ProductionPlan.type_production']=$requestData['type'];
}
if(isset($requestData['status_id']))
	{
		if($requestData['status_id'])
			$conditions['ProductionPlan.status']=$requestData['status_id'];
	}
	if(isset($requestData['product_id']))
	{
		if($requestData['product_id'])
			$conditions['ProductionPlanItem.product_id']=$requestData['product_id'];
	}
	if(isset($requestData['date_of_production']) && isset($requestData['date_of_delivered']) )
	{
		$conditions['OR']=array(
			'ProductionPlan.date' =>date('Y-m-d',strtotime($requestData['date_of_production'])),
			'ProductionPlan.date_of_delivered' =>date('Y-m-d',strtotime($requestData['date_of_delivered'])),
			);
	}
$totalData=$this->ProductionPlanItem->find('count',['conditions'=>$conditions]);
$totalFiltered=$totalData;
if( !empty($requestData['search']['value']) ) { 
	$q=$requestData['search']['value'];
	$conditions['OR']=array(
		'ProductionPlan.id LIKE' =>'%'. $q . '%',
		'ProductionPlan.date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
		'ProductionPlan.date_of_delivered LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
		);
	$totalFiltered=$this->ProductionPlanItem->find('count',[
		'conditions'=>$conditions,
		]);
}
$Data=$this->ProductionPlanItem->find('all',array(
	'conditions'=>$conditions,
	'offset'=>$requestData['start'],
	'limit'=>$requestData['length'],
	'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
	'fields'=>array(
		'ProductionPlan.*',
		"ProductionPlanItem.*",
		'Product.name'
		)
	));

foreach ($Data as $key => $value) {
		$Data[$key]['ProductionPlanItem']['total_quantity']=floatval($value['ProductionPlanItem']['total_quantity']);
	$Data[$key]['ProductionPlanItem']['damage_quantity']=floatval($value['ProductionPlanItem']['damage_quantity']);
	$Data[$key]['ProductionPlanItem']['good_quantity']=floatval($value['ProductionPlanItem']['good_quantity']);
	$Data[$key]['ProductionPlan']['date']=date('d-m-Y',strtotime($value['ProductionPlan']['date']));
	$Data[$key]['ProductionPlan']['date_of_delivered']=date('d-m-Y',strtotime($value['ProductionPlan']['date_of_delivered']));
	if($value['ProductionPlan']['status']==1)
	{
		$Data[$key]['ProductionPlan']['status'] = '<span style="color:green;">PRODUCTION STARTED<span>';
	}
	else
	{
		$Data[$key]['ProductionPlan']['status'] = '<span style="color:blue;">PRODUCTION COMPLETED<span>';
	}
	if($value['ProductionPlan']['type_production']==1)
	{
		$Data[$key]['ProductionPlan']['action']='<span><a target="_blank" href="'.$this->webroot.'ProductionPlan/ProductionPlan/'.$value['ProductionPlan']['id'].'"><i class="fa fa-2x fa-eye"></i></a></span>';	
	}
	else
	{
		$Data[$key]['ProductionPlan']['action']='<span><a target="_blank" href="'.$this->webroot.'ProductionPlan/SemiProduction/'.$value['ProductionPlan']['id'].'"><i class="fa fa-2x fa-eye"></i></a></span>';
	}
}
$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;
}
public function after_production_BOM_print($production_no=null)
{
$this->ProductionPlan->unbindModel(array('hasMany' => array('ProductionPlanItem')));
$production_plan_ids=$this->ProductionPlanItem->find('first',array(
	'conditions'=>array('ProductionPlan.production_no'=>$production_no),
	'fields'=>array('ProductionPlan.id','ProductionPlan.date','ProductionPlan.date_of_order','ProductionPlan.date_of_delivered','ProductionPlan.production_no','Product.name')));

$BomProduction=$this->BomProduction->find('all',array(
	"joins"=>array(
		array(
			"table"=>'products',
			"alias"=>'Product',
			"type"=>'inner',
			"conditions"=>array('Product.id=BomProduction.product_id'),
			),
		array(
			"table"=>'units',
			"alias"=>'Unit',
			"type"=>'inner',
			"conditions"=>array('Unit.id=BomProduction.unit_id'),
			),
		),
	'conditions'=>array('BomProduction.production_no'=>$production_no),
	'fields'=>array('Product.name','Unit.name','BomProduction.quantity'),
	));
$BOM=[];
if(!empty($BomProduction))
{
	foreach ($BomProduction as $key => $value) 
	{
		$single['product_name']=$value['Product']['name'];
		$single['unit']=$value['Unit']['name'];
		$single['quantity']=$value['BomProduction']['quantity'];
		array_push($BOM,$single);
	}
}
$Checkstate=0;
require('fpdf/fpdf.php');
$pdf = new FPDF('p', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$BOM,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) 
{
	$pdf->AddPage();
	$image_line_width=20;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(1, 1, 208, 216);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos=2;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
	$pdf->SetXY($invoice_x_starting, 5);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$pdf->Cell(0,$invoice_pos,'BOM ',0,0,'C');
	$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring-2,209, $gst_details_y_staring-2); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
// $gst_pos+=8;
// $pdf->text(2,$gst_pos,'Production Number :1');
// $gst_pos+=4;
// $pdf->text(2,$gst_pos,'Order Date : '.date("d-m-Y"));

$gst_pos+=0;
$pdf->text(2,$gst_pos,'Product : '.$production_plan_ids['Product']['name']);
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Production Number :'.$production_plan_ids['ProductionPlan']['production_no']);
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Date Of Production: '.date("d-m-Y",strtotime($production_plan_ids['ProductionPlan']['date'])));
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Date Of Delivered  : '.date("d-m-Y",strtotime($production_plan_ids['ProductionPlan']['date_of_delivered'])));
$pdf->Line(1, $gst_details_y_staring+17,209, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=52;

$table_length=165;
$pdf->text(2,$item_table_head_y_start+6,'SL No');
$table_column=12;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+20,$item_table_head_y_start+6,'Product ');
$table_column+=120;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'Quantity');
$table_column+=30;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+10,$item_table_head_y_start+6,'Unit');
$table_column+=7;


}
function footer($pdf)
{}
header_section($pdf,$BOM,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) ;
$total=0;
$item_table_head_y_start='60';
$count=count($BOM);
$i=0;
foreach ($BOM as $key => $value) {
$item_pos=2;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$key+1);
$item_pos+=15;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['product_name']);
$item_pos+=120;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),floatval($value['quantity']));
$item_pos+=30;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['unit']);
$i++;
}
footer($pdf);
$pdf->Output();
exit;

}
public function BOM_print($date_of_production=null,$product_name=null,$date=null,$product_id=null,$type_order=null)
{
$date_of_delivered=date('Y-m-d',strtotime($date));
$date_of_production=date('Y-m-d',strtotime($date_of_production));
$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
$Ingredientitem=[]; 
if($type_order==1)
{
	$Product_list=$this->Product->find('all',array('conditions'=>['Product.active=1','Product.type'=>[1,3],],
		'fields'=>['Product.*','Unit.*']));
}
else
{
	$Product_list=$this->Product->find('all',array('conditions'=>['Product.active=1','Product.type'=>1],
		'fields'=>['Product.*','Unit.*']));
	$Ingredientitem=[]; 
}
if(!empty($Product_list)){
	foreach ($Product_list as $key => $value) {
		$conditions=[];
			$conditions['Ingredient.product_id']=$product_id;
					$conditions['IngredientsItem.product_id']=$value['Product']['id'];
		$IngredientsItem=$this->IngredientsItem->find('all',array(
			'conditions'=>$conditions,
			'fields'=>array('Ingredient.product_id','IngredientsItem.*'),
			));
		$Stock = $this->Stock->findByProductIdAndWarehouseId($value['Product']['id'],1);
		$quantity=0;
		if(!empty($IngredientsItem)){
			foreach ($IngredientsItem as $key1 => $value1) {
				$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];    
					if($type_order==1)
					{         
						$ProductionPlan=$this->OrderItem->find('first',array(
							'conditions'=>array('Order.flag'=>1,'Order.type_order'=>1,'Order.date_of_production'=>$date_of_production,'Order.status'=>1,'OrderItem.second_status'=>1,'OrderItem.product_id'=>$value1['Ingredient']['product_id']),
							'fields'=>array('OrderItem.total_quantity'),
							));
					}
					else
					{
						$ProductionPlan=$this->OrderItem->find('first',array(
							'conditions'=>array('Order.flag'=>1,'Order.type_order'=>2,'Order.date_of_production'=>$date_of_production,'Order.status'=>1,'OrderItem.second_status'=>1,'OrderItem.product_id'=>$value1['Ingredient']['product_id']),
							'fields'=>array('OrderItem.total_quantity'),
							));
					}
				if(!empty($ProductionPlan))
				{
					$quantity=$quantity+($ProductionPlan['OrderItem']['total_quantity']*$value1['IngredientsItem']['quantity']);
				}
			}
			if($quantity!=0)
			{   
				$single['IngredientsItem']['product_id']=$value['Product']['id'];
				$single['IngredientsItem']['product_name']=$value['Product']['name'];
				$single['IngredientsItem']['unit']=$value['Unit']['name'];
				$single['IngredientsItem']['quantity']=$quantity;
				$single['IngredientsItem']['unit_id']=$value['Unit']['id'];
				$single['IngredientsItem']['stock_quantity']=round($Stock['Stock']['quantity']);
				array_push($Ingredientitem,$single);
			}					
		}

	}
}
$Checkstate=0;
require('fpdf/fpdf.php');
$pdf = new FPDF('p', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$Ingredientitem,$Profile,$total_page,$page,$Checkstate,$date_of_production,$product_name,$date_of_delivered) 
{
	$pdf->AddPage();
	$image_line_width=20;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(1, 1, 208, 216);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos=2;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
	$pdf->SetXY($invoice_x_starting, 5);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$pdf->Cell(0,$invoice_pos,'BOM ',0,0,'C');
	$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Product : '.$product_name);
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Date of Delivered : '.date('d-m-Y',strtotime($date_of_delivered)));
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Date of Production : '.date('d-m-Y',strtotime($date_of_production)));
$pdf->Line(1, $gst_details_y_staring+17,209, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=52;

$table_length=165;
$pdf->text(2,$item_table_head_y_start+6,'SL No');
$table_column=12;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+20,$item_table_head_y_start+6,'Product ');
$table_column+=110;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
//$pdf->text($table_column+5,$item_table_head_y_start+6,'Stock Quantity');
$table_column+=30;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'Quantity');
$table_column+=30;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+10,$item_table_head_y_start+6,'Unit');
$table_column+=7;


}
function footer($pdf)
{}
header_section($pdf,$Ingredientitem,$Profile,$total_page,$page,$Checkstate,$date_of_production,$product_name,$date_of_delivered) ;
$total=0;
$item_table_head_y_start='60';
$count=count($Ingredientitem);
$i=0;
foreach ($Ingredientitem as $key => $value) {
$item_pos=2;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$key+1);
$item_pos+=15;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['IngredientsItem']['product_name']);
$item_pos+=110;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['IngredientsItem']['stock_quantity']);
$item_pos+=30;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['IngredientsItem']['quantity']);
$item_pos+=30;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['IngredientsItem']['unit']);
$i++;
}
footer($pdf);
$pdf->Output();
exit;

}
public function bill_of_material_ok_ajax()
{
$user_id=1;
$datasource_Order = $this->Order->getDataSource();
$datasource_BomProduction = $this->BomProduction->getDataSource();
$datasource_OrderItem = $this->OrderItem->getDataSource();
try {
	$datasource_Order->begin();
	$datasource_BomProduction->begin();
	$datasource_OrderItem->begin();
	$ProductId=$this->request->data['ProductId'];
	$data = $this->request->data;
	if(!empty($data))
	{
		$date=date('Y-m-d',strtotime($data['date']));
		$type_order=$data['type_order'];
		$conditions=[];
		$conditions['OrderItem.second_status']=1;	
		$conditions['OrderItem.status']=1;	
		$conditions['Order.type_order']=$type_order;	
		$conditions['Order.flag']=1;	
		if(!empty($ProductId))
		{
			$conditions['OrderItem.product_id']=$ProductId;
		}
		if($type_order==1)
		{
			$conditions['Order.date_of_production']=date('Y-m-d',strtotime($date));	
			$Orders=$this->OrderItem->find('all',array('conditions'=>$conditions));
		}
		else
		{
			$Orders=$this->OrderItem->find('all',array('conditions'=>$conditions));
		}
		foreach ($Orders as $key1 => $value1) 
		{
			$this->Order->id=$value1['Order']['id'];
			// if(!$this->Order->saveField('status','2'))
			// 	throw new Exception('Error While status updating', 1);
			if(!$this->Order->saveField('second_status','2'))
				throw new Exception('Error While status updating', 1);
			$this->OrderItem->id=$value1['OrderItem']['id'];
			if(!$this->OrderItem->saveField('status','2'))
				throw new Exception('Error While status updating', 1);
			$this->OrderItem->id=$value1['OrderItem']['id'];
			if(!$this->OrderItem->saveField('second_status','1'))
				throw new Exception('Error While status updating', 1);
		}
	}
	$return['result']="Success";
	$datasource_Order->commit();
	$datasource_BomProduction->commit();
	$datasource_OrderItem->commit();
	$this->Session->setFlash(__($return['result']));
} catch (Exception $e) {
	$datasource_BomProduction->rollback();
	$datasource_Order->rollback();
	$datasource_OrderItem->commit();
	$return['result']=$e->getMessage();
}
echo json_encode($return);exit;
}
public function material_request_add_ajax()
{
$user_id=1;
$data = $this->request->data['BOM'];
$date=date('Y-m-d',strtotime($data['date']));
$return['result']='Error';
$datasource_MaterialRequest = $this->MaterialRequest->getDataSource();
$datasource_MaterialRequestItem = $this->MaterialRequestItem->getDataSource();
try {
	$datasource_MaterialRequest->begin();
	$datasource_MaterialRequestItem->begin();
	$production_no=$this->MaterialRequest->findByOrderProductIdAndDateOfDelivered($data['order_product_id'],$date);
	if(!empty($production_no))
		$return['result']='Material Request created';
	else
	{
		if(!empty($data))
		{
			$insert_data=[
			//'production_no'=>$data['production_no'],
			'date_of_delivered'=>$date,
			'order_product_id'=>$data['order_product_id'],
			'number_of_items'=>count($data['shortage']),
			'created_by'=>$user_id,
			'updated_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_at'=>date('Y-m-d H:i:s'),
			];
			$this->MaterialRequest->create();
			if(!$this->MaterialRequest->save($insert_data))
			{
				$errors = $this->MaterialRequest->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$id=$this->MaterialRequest->getLastInsertId();
			$return_function=$this->MaterialRequest_item_insert($data,$id);
			if($return_function['result']!='Success')
				throw new Exception($return_function['result']);
		}
		$return['result']='Success';

	}
	$datasource_MaterialRequest->commit();
	$datasource_MaterialRequestItem->commit();

}
catch (Exception $e)
{

	$datasource_MaterialRequest->rollback();
	$datasource_MaterialRequestItem->rollback();
	$return['result']=$e->getMessage();
}
echo json_encode($return);exit;
}
public function additional_material_add()
{
$user_id=1;
$data = $this->request->data;
$return['result']='Error';
$datasource_BomProduction = $this->BomProduction->getDataSource();
try {
	$datasource_BomProduction->begin();
		if(!empty($data))
		{
			$BomProduction_exit=$this->BomProduction->findByProductIdAndBrandIdAndProductionNo($data['product_id'],$data['brand_id'],$data['production_no']);
		if($BomProduction_exit)
		{
			$update_quantity=$BomProduction_exit['BomProduction']['additional_quantity']+$data['quantity'];
			$update_data=[
				'additional_quantity'=>$update_quantity,
				];
			$this->BomProduction->id=$BomProduction_exit['BomProduction']['id'];
			if(!$this->BomProduction->save($update_data))
				{
				$errors = $this->BomProduction->validationErrors;
				foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
				}
				}
				// $Stock = $this->Stock->find('first',array(
				// 			'conditions'=>array(
				// 				'Stock.product_id'=>$data['product_id'],
				// 				'Stock.warehouse_id'=>1,
				// 				)
				// 			));
				// 		if(!$Stock)
				// 			throw new Exception("Empty Stock", 1);
				// 		$stock_id=$Stock['Stock']['id'];
				// 		$stock_quantity=$Stock['Stock']['quantity'];
				// 		$remark='Change Stock quantity --'.'('.$data['product_id'].')'.'('.$data['quantity'].')';
				// 		$from_qty=$stock_quantity-$data['quantity'];
				// 		$user_id=1;
				// 		if($from_qty<0)
				// 		{
				// 			$from_qty=$stock_quantity;
				// 		}
				// 		$StockController = new StockController;
				// 		$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$from_qty,date('Y-m-d'),$user_id,$remark);
				// 		if($Stock_function_return['result']!='Success')
				// 			throw new Exception($Stock_function_return['result']);
		}
		else{

			$BomProduction=[
				'production_no'=>$data['production_no'],
				'product_id'=>$data['product_id'],
				'unit_id'=>$data['unit_id'],
				'quantity'=>0,
				'brand_id'=>$data['brand_id'],
				'final_quantity'=>0,
				'additional_quantity'=>$data['quantity']
				];
				$this->BomProduction->create();
				if(!$this->BomProduction->save($BomProduction))
				{
				$errors = $this->BomProduction->validationErrors;
				foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
				}
				}
				// $Stock = $this->Stock->find('first',array(
				// 			'conditions'=>array(
				// 				'Stock.product_id'=>$data['product_id'],
				// 				'Stock.warehouse_id'=>1,
				// 				)
				// 			));
				// 		if(!$Stock)
				// 			throw new Exception("Empty Stock", 1);
				// 		$stock_id=$Stock['Stock']['id'];
				// 		$stock_quantity=$Stock['Stock']['quantity'];
				// 		$remark='Change Stock quantity --'.'('.$data['product_id'].')'.'('.$data['quantity'].')';
				// 		$from_qty=$stock_quantity-$data['quantity'];
				// 		$user_id=1;
				// 		if($from_qty<0)
				// 		{
				// 			$from_qty=$stock_quantity;
				// 		}
				// 		$StockController = new StockController;
				// 		$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$from_qty,date('Y-m-d'),$user_id,$remark);
				// 		if($Stock_function_return['result']!='Success')
				// 			throw new Exception($Stock_function_return['result']);
	 	}
	 		 	
		$return['result']='Success';
	}
	$datasource_BomProduction->commit();
}
catch (Exception $e)
{

	$datasource_BomProduction->rollback();
	$return['result']=$e->getMessage();
}
echo json_encode($return);exit;
}
public function MaterialRequest_item_insert($data,$id)
{
try {
	if($data)
	{
		$items=0;
		for ($i=0; $i <count($data['product_id']) ; $i++) {
			if($data['shortage'][$i]>0)
			{
				$items++;
				$IngredientItem_data=[
				'product_id'=>$data['product_id'][$i],
				'm_r_id'=>$id,
				'unit_id'=>$data['unit_id'][$i],
				'quantity'=>$data['quantity'][$i],
				'shortage'=>$data['shortage'][$i],
				];
				$this->MaterialRequestItem->create();
				if(!$this->MaterialRequestItem->save($IngredientItem_data))
				{
					$errors = $this->MaterialRequestItem->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
			}
			$this->MaterialRequest->id=$id;
			if(!$this->MaterialRequest->saveField('number_of_items',$items))
				throw new Exception('Error While updating', 1);
		}
	}
	$return['result']='Success';
} catch (Exception $e) {
	$return['result']=$e->getMessage();
}
return $return;
}
public function MaterialRequestList()
{
$first_date = date('Y-m-d',strtotime('first day of this month'));
$firstdate = date('d-m-Y',strtotime('first day of this month'));
$this->set('firstdate', $firstdate);
$last_date = date('Y-m-d');
$todate = date('d-m-Y');
$this->set('todate', $todate);

}
public function MaterialRequest($id=null)
{
if(!empty($id))
{
	$Data=$this->MaterialRequestItem->find('all',array(
		'conditions'=>array('MaterialRequestItem.m_r_id'=>$id),
		'fields'=>array('Unit.name','Product.name','MaterialRequestItem.*','MaterialRequest.production_no'),
		));
	$this->set('MaterialRequestItem', $Data);

	$data['MaterialRequest']['production_no']=$id;
	$this->request->data=$data;
}

}
public function material_request_search_ajax()
{
$requestData=$this->request->data;
$columns = [];
$columns[]='MaterialRequest.updated_at';
$columns[]='MaterialRequest.production_no';
$columns[]='MaterialRequest.number_of_items';
$columns[]='MaterialRequest.id';
$conditions=[];
	// if(isset($requestData['from_date']) && isset($requestData['to_date']) )
	// {
	// 	$conditions['Order.date between ? and ?']=[
	// 	date('Y-m-d',strtotime($requestData['from_date'])),
	// 	date('Y-m-d',strtotime($requestData['to_date']))
	// 	];
	// }
$totalData=$this->MaterialRequest->find('count',['conditions'=>$conditions]);
$totalFiltered=$totalData;
if( !empty($requestData['search']['value']) ) { 
	$q=$requestData['search']['value'];
	$conditions['OR']=array(
		'MaterialRequest.updated_at LIKE' =>'%'. $q . '%',
		'MaterialRequest.production_no LIKE' =>'%'. $q . '%',
		'MaterialRequest.number_of_items LIKE' =>'%'. $q . '%',
		);
	$totalFiltered=$this->MaterialRequest->find('count',[
		'conditions'=>$conditions,
		]);
}
$Data=$this->MaterialRequest->find('all',array(
	'conditions'=>$conditions,
	'offset'=>$requestData['start'],
	'limit'=>$requestData['length'],
	'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
	'fields'=>array(
		'MaterialRequest.*',
		)
	));

foreach ($Data as $key => $value) {
	$Data[$key]['MaterialRequest']['date']=date('d-m-Y',strtotime($value['MaterialRequest']['updated_at']));
	$Data[$key]['MaterialRequest']['action']='<span><a target="_blank" href="'.$this->webroot.'ProductionPlan/MaterialRequest/'.$value['MaterialRequest']['id'].'"><i class="fa fa-2x fa-eye"></i></span></a>';

}
$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;
}
public function single_production_ajax()
{
$return['result']='Empty';
$return['orderitem']=[];
$return['order_status']=1;
$data=$this->request->data;
$date=$data['date'];
$product_id=$data['product_id'];
$type_order=$data['type_order'];
$conditions=[];
$conditions['Order.status']=1;
$conditions['OrderItem.second_status']=1;	
$conditions['Order.type_order']=$data['type_order'];	
$conditions['Order.flag']=1;	
$conditions_product=[];
$conditions_product['Product.id']=$product_id;
$conditions['Order.date_of_production']=date('Y-m-d',strtotime($date));	
$date_order=date('Y-m-d',strtotime('-1 day'));
$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));

if($type_order==1)
{
	$conditions_product['Product.type']=2;	
	$conditions_product['Product.active']=1;	
	$Product_list=$this->Product->find('first',array('conditions'=>$conditions_product,
	'fields'=>['Product.*']));
}
else
{
	$conditions_product['Product.type']=3;
	$conditions_product['Product.active']=1;	
	$Product_list=$this->Product->find('first',array('conditions'=>$conditions_product,
	'fields'=>['Product.*']));
}
$orderitem=[];
if(!empty($Product_list)){
		$conditions['OrderItem.product_id']=$Product_list['Product']['id'];
		$conditions['OrderItem.quantity !=']=0;
		$quantity=0;
		$order_status=$this->OrderItem->find('first',array('order'=>'Order.id DESC','conditions'=>array('Order.flag'=>1,'Order.type_order'=>$type_order,'Order.date_of_production'=>date('Y-m-d',strtotime($date)),'Order.status'=>1,'OrderItem.second_status'=>1,'OrderItem.Product_id'=>$product_id)));
	if($order_status)
	{
		$return['order_status']=$order_status['OrderItem']['status'];	
	}
		$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
		$OrderItem=$this->OrderItem->find('first',array(
			'conditions'=>$conditions,
			'fields'=>array('OrderItem.total_quantity','OrderItem.status','Order.delivery_days'),
			));
		if(!empty($OrderItem['OrderItem']['total_quantity']))
		{
			$single['Product_name']=$Product_list['Product']['name'];
			$single['Product_id']=$Product_list['Product']['id'];
			$single['Product_quantity']=floatval($OrderItem['OrderItem']['total_quantity']);
			$single['date_of_delivered']=date('d-m-Y',strtotime('+'.$OrderItem['Order']['delivery_days'].'day'));
			$return['orderitem']=$single;    
			//array_push($orderitem,$single);
		}
	}
$LabourCalculation=$this->LabourCalculation->findByProductId($product_id);
$return['labour_packing']=0;    
$return['labour_production']=0;
if($LabourCalculation)
{
$one_quantity_production=$LabourCalculation['LabourCalculation']['labour_production']/$LabourCalculation['LabourCalculation']['quantity'];
$one_quantity_packing=$LabourCalculation['LabourCalculation']['labour_packing']/$LabourCalculation['LabourCalculation']['quantity'];
$return['labour_packing']=floor($OrderItem['OrderItem']['total_quantity']*$one_quantity_packing);    
$return['labour_production']=floor($OrderItem['OrderItem']['total_quantity']*$one_quantity_production);;    
}
$return['result']='Success';
echo json_encode($return);
exit;

}
public function daily_transfer_ajax($warehouse_id=null,$date,$warehouse_from)
{

$return['result']='Empty';
$return['orderitem']=[];
$conditions=[];
$conditions['OrderItem.second_status']=[1,3];	
$conditions['Order.date_of_delivered']=date('Y-m-d',strtotime($date));	
$conditions['Order.type_order']=1;	
$conditions['Order.flag']=1;	
$conditions['OrderItem.quantity !=']=0;	
	$conditions['Executive.warehouse_id']=$warehouse_id;
$this->Product->unbindModel(array('belongsTo'=>array('ProductType','Brand','Unit'),'hasMany' => array('PurchaseReturnItem','UnwantedList','Stock','StockLog','SalesReturnItem','SaleItem','PurchasedItem','Unit','Brand')));
$Product_list=$this->Product->find('all',array(
	"joins"=>array(
		array(
			"table"=>'order_items',
			"alias"=>'OrderItem',
			"type"=>'inner',
			"conditions"=>array('Product.id=OrderItem.product_id'),
			),
		array(
			"table"=>'orders',
			"alias"=>'Order',
			"type"=>'inner',
			"conditions"=>array('Order.id=OrderItem.order_id'),
			),
		array(
			"table"=>'executives',
			"alias"=>'Executive',
			"type"=>'inner',
			"conditions"=>array('Executive.id=Order.executive_id'),
			),
		array(
			"table"=>'warehouses',
			"alias"=>'Warehouse',
			"type"=>'inner',
			"conditions"=>array('Warehouse.id=Executive.warehouse_id'),
			),
		),
	  'group' => 'OrderItem.product_id',
	'conditions'=>$conditions,
	 'order' => array('Product.priority' => 'ASC'),
	'fields'=>['Product.name','Product.id','Product.tray_occupation']
	));
$orderitem=[];
if(!empty($Product_list)){
	$tary_list=$this->Product->find('first',array('conditions'=>['type'=>5],
		'fields'=>['Product.id','Product.name','Product.tray_occupation']));
	$single['name']=$tary_list['Product']['name'];
	$single['id']=$tary_list['Product']['id'];
	$single['order_id']=0;
	$single['warehouse_name']="";
	$single['warehouse_id']="";
	$single['hidden_tray_occupation']=$tary_list['Product']['tray_occupation'];
	$fromStock_tray=$this->Stock->find('first',array(
		'conditions'=>array(
			'Stock.product_id'=>$tary_list['Product']['id'],
			'Stock.warehouse_id'=>$warehouse_from,
			),
		'fields'=>array('Stock.id','Stock.quantity')
		));
	$single['stock_quantity']=0;
	if($fromStock_tray)
	{
		$single['stock_quantity']=floatval($fromStock_tray['Stock']['quantity']);	
	}
	$single['quantity']=0;
	$single['tray_occupation']=0;
	array_push($orderitem,$single);
		foreach ($Product_list as $key => $value) {
    $conditions['OrderItem.product_id']=$value['Product']['id'];
    $order_list=$this->OrderItem->find('all',array(
    		"joins"=>array(
		array(
			"table"=>'executives',
			"alias"=>'Executive',
			"type"=>'inner',
			"conditions"=>array('Executive.id=Order.executive_id'),
			),
		array(
			"table"=>'warehouses',
			"alias"=>'Warehouse',
			"type"=>'inner',
			"conditions"=>array('Warehouse.id=Executive.warehouse_id'),
			),
		),
	'conditions'=>$conditions,
	'fields'=>['OrderItem.order_id']
	));
		$order_id='';
foreach ($order_list as $key2 => $value_order) {
	if($order_id=='')
	{
	$order_id=$value_order['OrderItem']['order_id'];
	}
	else
	{
	$order_id=$order_id.','.$value_order['OrderItem']['order_id'];
	}
}
    $this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
	$OrderItem_all=$this->OrderItem->find('first',array(
	"joins"=>array(
		array(
			"table"=>'executives',
			"alias"=>'Executive',
			"type"=>'inner',
			"conditions"=>array('Executive.id=Order.executive_id'),
			),
		array(
			"table"=>'warehouses',
			"alias"=>'Warehouse',
			"type"=>'inner',
			"conditions"=>array('Warehouse.id=Executive.warehouse_id'),
			),
		),
	'conditions'=>$conditions,
	'fields'=>['OrderItem.total_quantity','Order.id','Warehouse.id','Warehouse.name']
	));
		$single['name']=$value['Product']['name'];
		$single['id']=$value['Product']['id'];
		$single['order_id']=$order_id;
		$single['warehouse_name']=$OrderItem_all['Warehouse']['name'];
		$single['warehouse_id']=$OrderItem_all['Warehouse']['id'];
		$single['hidden_tray_occupation']=$value['Product']['tray_occupation'];
		$fromStock=$this->Stock->find('first',array(
			'conditions'=>array(
				'Stock.product_id'=>$value['Product']['id'],
				'Stock.warehouse_id'=>1,
				),
			'fields'=>array('Stock.id','Stock.quantity')
			));
		$single['stock_quantity']=floatval($fromStock['Stock']['quantity']);
		$single['quantity']=floatval($OrderItem_all['OrderItem']['total_quantity']);
		if($value['Product']['tray_occupation']!=0)
		{
			$quantity=$OrderItem_all['OrderItem']['total_quantity']/$value['Product']['tray_occupation'];
			$whole = round($quantity,2);      
			$fraction =$OrderItem_all['OrderItem']['total_quantity']%$value['Product']['tray_occupation']; 
			$pieces=number_format($fraction);
			$tray=$whole;
			$tray_piece=0;
			if($pieces>=1)
			{
				$tray_piece=1;
			}
			$single['tray_occupation']=$whole;
		}
		else
		{
			$single['tray_occupation']=0;
		}
					array_push($orderitem,$single);
	}
}
$return['orderitem']=$orderitem;    
$return['result']='Success';
echo json_encode($return);
exit;

}
public function DailyTransferList()
{
	$conditions = [];
	$this->set('from',date("d-m-Y", strtotime('-1 day')));
	$this->set('to',date("d-m-Y"));
	$conditions['StockTransfer.status'] = 2;
	$conditions['StockTransfer.transfer_type']	 = 3;
	$conditions['StockTransfer.flag'] = 1;
	$StockTransfer=$this->StockTransfer->find('all',array(
		"joins"=>array(
			array(
				"table"=>'warehouses',
				"alias"=>'WarehouseFrom',
				"type"=>'inner',
				"conditions"=>array('WarehouseFrom.id=StockTransfer.warehouse_from'),
				),
			array(
				"table"=>'warehouses',
				"alias"=>'WarehouseTo',
				"type"=>'inner',
				"conditions"=>array('WarehouseTo.id=StockTransfer.warehouse_to'),
				),
			),
		'order'=>array('StockTransfer.id DESC'),
		'conditions'=>$conditions,
		"fields"=>array(
			'StockTransfer.id',
			'StockTransfer.transfer_no',
			'StockTransfer.date',
			'StockTransfer.remarks',
			'WarehouseFrom.name',
			'WarehouseTo.name',
			),
		));
	$this->set(compact('StockTransfer'));
}
	public function get_daily_transfer_ajax()
{

	$requestData=$this->request->data;
	$columns = [];
	$columns[]='StockTransfer.transfer_no';
	$columns[]='StockTransfer.date';
	$columns[]='StockTransfer.created_at';
	$columns[]='WarehouseFrom.name'; 
	$columns[]='WarehouseTo.name'; 
	$columns[]='StockTransfer.remarks'; 
	$columns[]='StockTransfer.status';
	$columns[]='StockTransfer.id';
	$conditions=[];
	//	pr($requestData);
	$user_branch_id = $this->Session->read('User.branch_id');
			if(!empty($user_branch_id))
			{
			//$conditions['StockTransfer.branch_id'] = $user_branch_id;
			$branch=$this->Branch->findById($user_branch_id);
		$conditions['StockTransfer.warehouse_from']=$branch['Branch']['warehouse_id'];
			}
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions['StockTransfer.date between ? and ?']=[$from_date,$to_date];
	if(isset($requestData['status']))
	{
		if($requestData['status']==3)
		{
			$conditions['StockTransfer.status']=2;
			$conditions['StockTransfer.flag'] = 1;
		    $status="Approved";
		}
		else if($requestData['status']==2)
		{
			$conditions['StockTransfer.status']=2;
			$conditions['StockTransfer.flag'] = 0;
		    $status="Rejected";
		}
		else
		{
			$conditions['StockTransfer.flag']=1;
			$conditions['StockTransfer.status']=1;	
		    $status="Pending";
		}
	}
	//$conditions['StockTransfer.status'] = 2;
	$conditions['StockTransfer.transfer_type']	 = 2;
	//$conditions['StockTransfer.flag'] = 1;
	$totalData=$this->StockTransfer->find('count',['conditions'=>$conditions]);
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'StockTransfer.date LIKE' =>'%'. $q . '%',
			'WarehouseFrom.name LIKE' =>'%'. $q . '%',
			'WarehouseTo.name LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->StockTransfer->find('count',[
			"joins"=>array(
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseFrom',
					"type"=>'inner',
					"conditions"=>array('WarehouseFrom.id=StockTransfer.warehouse_from'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseTo',
					"type"=>'inner',
					"conditions"=>array('WarehouseTo.id=StockTransfer.warehouse_to'),
					),
				),
			'conditions'=>$conditions,
			]);
	}
		//pr($conditions);
	$Data=$this->StockTransfer->find('all',array(
		"joins"=>array(
			array(
				"table"=>'warehouses',
				"alias"=>'WarehouseFrom',
				"type"=>'inner',
				"conditions"=>array('WarehouseFrom.id=StockTransfer.warehouse_from'),
				),
			array(
				"table"=>'warehouses',
				"alias"=>'WarehouseTo',
				"type"=>'inner',
				"conditions"=>array('WarehouseTo.id=StockTransfer.warehouse_to'),
				),
			),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'StockTransfer.id',
			'StockTransfer.transfer_no',
			'StockTransfer.created_at',
			'StockTransfer.date',
			'StockTransfer.status',
			'StockTransfer.remarks',
			'WarehouseFrom.name',
			'WarehouseTo.name',
			)
		));
	foreach ($Data as $key => $value) {
		$Data[$key]['StockTransfer']['status']=$status;
		$Data[$key]['StockTransfer']['date']=date('d-m-Y',strtotime($value['StockTransfer']['date']));
		$Data[$key]['StockTransfer']['created_at']=date("h:i a", strtotime($value['StockTransfer']['created_at']));
		$table_id=$value['StockTransfer']['id'];
		$Data[$key]['StockTransfer']['action']='<a href="'.$this->webroot.'Stock/ViewStockTransfer/'.$table_id.'" title=""><span><i class="fa fa-2x fa-eye" aria-hidden="true"></i></span></a>
		<span></span><a href="'.$this->webroot.'ProductionPlan/ExportDailyStockTransfer/'.$table_id.'" title="" target="_blank"><span><i class="fa fa-2x fa-print" aria-hidden="true"></i></span></a>';
	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data);
	exit;
	
}
public function DailyStockTransfer()
{
$branch_id = $this->Session->read('User.branch_id');
$todate = date('d-m-Y');
$this->set('date', $todate);
$date_order=date('Y-m-d',strtotime('-1 day'));
if($branch_id)
{
		$Warehouse_from=$this->Warehouse->find('list', array(
		"joins"=>array(array(
			"table"=>'branches',
			"alias"=>'Branch',
			"type"=>'inner',
			"conditions"=>array('Branch.warehouse_id=Warehouse.id'),
			),
		),
			'conditions'=>array('Branch.id' =>$branch_id),
		'order'=>array('Warehouse.name ASC'),
		'fields' => array(
			'Warehouse.id',
			'Warehouse.name',
				)));
		$conditions_branch=[];
$conditions_branch['BranchWarehouseMapping.branch_id']=$branch_id;
$this->Warehouse->unbindModel(
			array('hasMany' => array(
				'Executive',
				'StockLog',
				'Stock',
				))
			);
	$Warehouse=$this->Warehouse->find('list', array(
		"joins"=>array(array(
			"table"=>'branch_warehouse_mappings',
			"alias"=>'BranchWarehouseMapping',
			"type"=>'inner',
			"conditions"=>array('BranchWarehouseMapping.warehouse_id=Warehouse.id'),
			),
		),
		'conditions' =>$conditions_branch ,
		'order'=>array('Warehouse.name ASC'),
		'fields' => array(
			'Warehouse.id',
			'Warehouse.name',
				)));
}
else
{
$Warehouse=$this->Warehouse->find('list',array(
	'conditions'=>array('Warehouse.warehouse_id'=>"",'Warehouse.id !='=>1),
	'fields'=>array('id','name'),
	'order'=>array('name ASC'),
	));
$Warehouse_from=$this->Warehouse->find('list',array(
'conditions'=>array('Warehouse.id'=>1),
	'fields'=>array('id','name'),
	'order'=>array('id ASC'),
	));
}
// $stock_warehouse=$this->StockTransfer->find('list',array(
// 	'conditions'=>array('StockTransfer.date'=>date('Y-m-d')),
// 	'fields'=>array('StockTransfer.warehouse_to','StockTransfer.warehouse_to'),
// 	));
// foreach ($stock_warehouse as $key => $value) {
// 	unset($Warehouse[$key]);

// }
$this->set('Warehouse_from',$Warehouse_from);
$this->set('Warehouse',$Warehouse);
if (!$this->request->data)
{}
else
{

$datasource_StockTransfer = $this->StockTransfer->getDataSource();
$datasource_StockTransferItem = $this->StockTransferItem->getDataSource();
$datasource_Stock = $this->Stock->getDataSource();
$datasource_Order = $this->Order->getDataSource();
try {
	$datasource_StockTransfer->begin();
	$datasource_StockTransferItem->begin();
	$datasource_Stock->begin();
	$datasource_Order->begin();
	$user_id=1;
	$data=$this->request->data['DailyStockTransfer'];
	$Transfer=$this->StockTransfer->find('first',array('order' => 'StockTransfer.id DESC',));
	if(!empty($Transfer))
	{
		$TransferNo = $Transfer['StockTransfer']['transfer_no'];
		$transfer_no=$TransferNo+1;
	}
	else
	{
		$transfer_no=1;
	}
	$StockTransfer_data=[
	'transfer_no'=>$transfer_no,
	'date'=>date('Y-m-d'),
	'warehouse_from'=>$data['warehouse_from'],
	'warehouse_to'=>$data['warehouse_id'],
	'remarks'=>$data['remarks'],
	'status'=>1,
	'transfer_type'=>2,
	'created_by'=>1,
	'modified_by'=>1,
	'created_at'=>date('Y-m-d H:i:s'),
	'updated_at'=>date('Y-m-d H:i:s'),
	'branch_id'=>$branch_id,
	];
	$this->StockTransfer->create();
	if(!$this->StockTransfer->save($StockTransfer_data))
	{
		$errors = $this->StockTransfer->validationErrors;
		foreach ($errors as $key => $value) {
			throw new Exception($value[0], 1);
		}
	}
	$stock_transfer_id=$this->StockTransfer->getLastInsertId();
	$tray_quantity=0;
	for ($k=0; $k <count($data['tray_occupation']) ; $k++) 
	{
		if($data['product_id'][$k]!=1)
		{
			$tray_quantity+=$data['tray_occupation'][$k];
		}
	}
	for ($i=0; $i <count($data['order_id']) ; $i++) 
	{
		if($data['product_id'][$i]==1)
		{
        $data['quantity'][$i]=$data['quantity'][$i]+$tray_quantity;
		}
		$whole=0;
		if(!empty($data['hidden_tray_occupation'][$i]) || $data['hidden_tray_occupation'][$i]!=0)
		{
          $quantity=$data['order_quantity'][$i]/$data['hidden_tray_occupation'][$i];
			$whole = round($quantity,2);      
		}
		$StockTransferItem_data=[
		'product_id'=>$data['product_id'][$i],
		'unit_id'=>1,
		'stock_transfer_id'=>$stock_transfer_id,
		'quantity'=>$data['quantity'][$i],
		'ordered_qty'=>$data['order_quantity'][$i],
	     'trace'=>$whole,
		];
		$this->StockTransferItem->create();
		if(!$this->StockTransferItem->save($StockTransferItem_data))
		{
			$errors = $this->StockTransferItem->validationErrors;
			foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
			}
		}
		else
		{
			if($data['order_id'][$i]!=0){
				$order_id=explode(',',$data['order_id'][$i]);
				for ($j=0; $j < count($order_id); $j++) { 
				$this->Order->id=$order_id[$j];
				if(!$this->Order->saveField('status','2'))
					throw new Exception('Error While status updating', 1);
				if(!$this->Order->saveField('second_status','2'))
				throw new Exception('Error While status updating', 1);
				$this->OrderItem->id=$this->OrderItem->field('OrderItem.id',array('OrderItem.product_id'=>$data['product_id'][$i],'OrderItem.order_id'=>$order_id[$j]));
				if(!$this->OrderItem->saveField('second_status','4'))
					throw new Exception('Error While status updating', 1);
				if(!$this->OrderItem->saveField('status','2'))
					throw new Exception('Error While status updating', 1);
				}
			}
			$fromStock=$this->Stock->find('first',array(
				'conditions'=>array(
					'Stock.product_id'=>$data['product_id'][$i],
					'Stock.warehouse_id'=>$data['warehouse_from'],
					),
				'fields'=>array('Stock.id','Stock.quantity')
				));
			if(!$fromStock)
				      throw new Exception("Empty Stock", 1);
				$fromQty=$fromStock['Stock']['quantity']-$data['quantity'][$i];
				if($fromQty<0)
				      throw new Exception("Empty From Stock", 1);
				  	$remark='Stock Transfer --'.$transfer_no.'('.$data['quantity'][$i].')';	
					$StockController = new StockController;
					$Stock_function_return=$StockController->GeneralStock_Edit_Function($fromStock['Stock']['id'],$fromQty,date('Y-m-d'),$user_id,$remark);
					if($Stock_function_return['result']!='Success')
						throw new Exception($Stock_function_return['result']);
			// $toStock=$this->Stock->find('first',array(
			// 	'conditions'=>array(
			// 		'Stock.product_id'=>$data['product_id'][$i],
			// 		'Stock.warehouse_id'=>$data['warehouse_id']
			// 		),
			// 	'fields'=>array('Stock.id','Stock.quantity')));
			// if(!$toStock)
			// {
			// 	$remark='Stock Transfer ['.$stock_transfer_id.','.$data['quantity'][$i].']';
			// 	$flag=1;
			// 	$StockController = new StockController;
			// 	$Stock_function_return=$StockController->GeneralStock_Add_Function($data['warehouse_id'],$data['product_id'][$i],$data['quantity'][$i],date('Y-m-d'),$user_id,$remark,$flag);
			// 	if($Stock_function_return['result']!='Success')
			// 		throw new Exception($Stock_function_return['result'], 1);
			// }
			// else
			// {
			// 	$toQty=$toStock['Stock']['quantity']+$data['quantity'][$i];
			// 	$remark='Stock Transfer ['.$stock_transfer_id.','.$data['quantity'][$i].']';
			// 	$StockController = new StockController;
			// 	$Stock_function_return=$StockController->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
			// 	if($Stock_function_return['result']!='Success')
			// 		throw new Exception($Stock_function_return['result']);
			// }
		}
	}
	$datasource_Stock->commit();
	$datasource_StockTransfer->commit();
	$datasource_StockTransferItem->commit();
	$datasource_Order->commit();
	$return['result']="success";
} catch (Exception $e){
	$datasource_StockTransfer->rollback();
	$datasource_StockTransferItem->rollback();
	$datasource_Stock->rollback();
	$datasource_Order->rollback();
	$return['result']=$e->getMessage();
}
$this->Session->setFlash(__($return['result']));
$this->redirect(array('controller' => 'ProductionPlan','action' =>'DailyTransferList'));


}
}
public function production_print($production_no=null)
{
$this->ProductionPlan->unbindModel(array('hasMany' => array('ProductionPlanItem')));
$production_plan_ids=$this->ProductionPlan->find('first',array(
	'conditions'=>array('ProductionPlan.production_no'=>$production_no),
	'fields'=>array('ProductionPlan.id','ProductionPlan.date','ProductionPlan.date_of_order','ProductionPlan.date_of_delivered','ProductionPlan.production_no')));
$production_plan_id=$production_plan_ids['ProductionPlan']['id'];
//pr($production_plan_id);
//exit;
$this->ProductionPlanItem->unbindModel(array('belongsTo' => array('ProductionPlan','Product')));
$ProductionPlan=$this->ProductionPlanItem->find('all',array(
	"joins"=>array(
		array(
			"table"=>'products',
			"alias"=>'Product',
			"type"=>'inner',
			"conditions"=>array('Product.id=ProductionPlanItem.product_id'),
			),
		// array(
		// 	"table"=>'units',
		// 	"alias"=>'Unit',
		// 	"type"=>'inner',
		// 	"conditions"=>array('Unit.id=BomProduction.unit_id'),
		// 	),
		),
	'conditions'=>array('ProductionPlanItem.production_id'=>$production_plan_id),
	'fields'=>array('Product.name','ProductionPlanItem.total_quantity','ProductionPlanItem.damage_quantity','ProductionPlanItem.good_quantity'),
	));
$Production=[];
if(!empty($ProductionPlan))
{
	foreach ($ProductionPlan as $key => $value) 
	{
		$single['product_name']=$value['Product']['name'];
				//$single['unit']=$value['Unit']['name'];
		$single['quantity']=round($value['ProductionPlanItem']['total_quantity']);
		$single['damage_quantity']=round($value['ProductionPlanItem']['damage_quantity']);
		$single['good_quantity']=round($value['ProductionPlanItem']['good_quantity']);
		array_push($Production,$single);
	}
}
$Checkstate=0;
require('fpdf/fpdf.php');
$pdf = new FPDF('p', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) 
{
	$pdf->AddPage();
	$image_line_width=20;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(1, 1, 208, 216);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos=2;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
	$pdf->SetXY($invoice_x_starting, 5);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$pdf->Cell(0,$invoice_pos,'PRODUCTION ',0,0,'C');
	$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring-2,209, $gst_details_y_staring-2); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Production Number :'.$production_plan_ids['ProductionPlan']['production_no']);
//$gst_pos+=4;
//$pdf->text(2,$gst_pos,'Date Of Order     : '.date("d-m-Y",strtotime($production_plan_ids['ProductionPlan']['date_of_order'])));
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Date Of Production: '.date("d-m-Y",strtotime($production_plan_ids['ProductionPlan']['date'])));
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Date Of Delivered  : '.date("d-m-Y",strtotime($production_plan_ids['ProductionPlan']['date_of_delivered'])));
$pdf->Line(1, $gst_details_y_staring+17,209, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=52;

$table_length=165;
$pdf->text(2,$item_table_head_y_start+6,'SL No');
$table_column=12;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+20,$item_table_head_y_start+6,'Product ');
$table_column+=80;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'Order Quantity');
$table_column+=35;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+10,$item_table_head_y_start+6,'Good Quantity');
$table_column+=35;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+10,$item_table_head_y_start+6,'Damage Quantity');
$table_column+=7;


}
function footer($pdf)
{}
header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) ;
$total=0;
$item_table_head_y_start='60';
$count=count($Production);
$i=0;
foreach ($Production as $key => $value) {
$item_pos=2;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$key+1);
$item_pos+=15;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['product_name']);
$item_pos+=80;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['quantity']);
$item_pos+=35;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['good_quantity']);
$item_pos+=35;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['damage_quantity']);
$i++;
}
footer($pdf);
$pdf->Output();
exit;

}
public function production_order_print($order_no=null)
{
$this->Order->unbindModel(array('hasMany' => array('OrderItem')));
$production_plan_ids=$this->Order->find('first',array(
	"joins"=>array(
		array(
			"table"=>'customers',
			"alias"=>'Customer',
			"type"=>'inner',
			"conditions"=>array('Customer.account_head_id=Order.account_head_id'),
			),
		// array(
		// 	"table"=>'units',
		// 	"alias"=>'Unit',
		// 	"type"=>'inner',
		// 	"conditions"=>array('Unit.id=BomProduction.unit_id'),
		// 	),
		),
	'conditions'=>array('Order.order_no'=>$order_no),
	'fields'=>array('Order.id','Order.date','Order.date_of_delivered','Order.order_no','AccountHead.id','AccountHead.name','Executive.id','Executive.name','Route.id','Route.name','Customer.place')));
$production_plan_id=$production_plan_ids['Order']['id'];
//pr($production_plan_id);
//exit;
$this->OrderItem->unbindModel(array('belongsTo' => array('Order','Product')));
$ProductionPlan=$this->OrderItem->find('all',array(
	"joins"=>array(
		array(
			"table"=>'products',
			"alias"=>'Product',
			"type"=>'inner',
			"conditions"=>array('Product.id=OrderItem.product_id'),
			),
		// array(
		// 	"table"=>'units',
		// 	"alias"=>'Unit',
		// 	"type"=>'inner',
		// 	"conditions"=>array('Unit.id=BomProduction.unit_id'),
		// 	),
		),
	'conditions'=>array('OrderItem.order_id'=>$production_plan_id),
	'fields'=>array('Product.name','OrderItem.quantity'),
	));
$Production=[];
if(!empty($ProductionPlan))
{
	foreach ($ProductionPlan as $key => $value) 
	{
		$single['product_name']=$value['Product']['name'];
				//$single['unit']=$value['Unit']['name'];
		$single['quantity']=round($value['OrderItem']['quantity']);
		array_push($Production,$single);
	}
}
$Checkstate=0;
require('fpdf/fpdf.php');
$pdf = new FPDF('p', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) 
{
	$pdf->AddPage();
	$image_line_width=20;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(1, 1, 208, 216);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos=2;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
	$pdf->SetXY($invoice_x_starting, 5);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$pdf->Cell(0,$invoice_pos,'Order ',0,0,'C');
	$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring-2,209, $gst_details_y_staring-2); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
$gst_pos+=0;
$pdf->text(2,$gst_pos,'Route            :'.$production_plan_ids['Route']['name']);
$pdf->text(150,$gst_pos,'Order Number       :'.$production_plan_ids['Order']['order_no']);
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Executive     : '.$production_plan_ids['Executive']['name']);
$pdf->text(150,$gst_pos,'Date Of Order        : '.date("d-m-Y",strtotime($production_plan_ids['Order']['date'])));
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Customer     : '.$production_plan_ids['AccountHead']['name']);
$pdf->text(150,$gst_pos,'Date Of Delivered  : '.date("d-m-Y",strtotime($production_plan_ids['Order']['date_of_delivered'])));

$gst_pos+=4;
$pdf->text(2,$gst_pos,'Address       : '.$production_plan_ids['Customer']['place']);

$pdf->Line(1, $gst_details_y_staring+17,209, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=52;

$table_length=165;
$pdf->text(2,$item_table_head_y_start+6,'SL No');
$table_column=12;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+20,$item_table_head_y_start+6,'Product ');
$table_column+=120;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'Quantity');
$table_column+=30;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
//$pdf->text($table_column+10,$item_table_head_y_start+6,'Unit');
$table_column+=7;


}
function footer($pdf)
{}
header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) ;
$total=0;
$item_table_head_y_start='60';
$count=count($Production);
$i=0;
foreach ($Production as $key => $value) {
$item_pos=2;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$key+1);
$item_pos+=15;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['product_name']);
$item_pos+=120;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['quantity']);
$item_pos+=30;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['unit']);
$i++;
}
footer($pdf);
$pdf->Output();
exit;

}
public function semi_production_print($production_no=null)
{
$this->ProductionPlan->unbindModel(array('hasMany' => array('ProductionPlanItem')));
$production_plan_ids=$this->ProductionPlan->find('first',array(
	'conditions'=>array('ProductionPlan.production_no'=>$production_no),
	'fields'=>array('ProductionPlan.id','ProductionPlan.date','ProductionPlan.production_no')));
$production_plan_id=$production_plan_ids['ProductionPlan']['id'];
//pr($production_plan_id);
//exit;
$this->ProductionPlanItem->unbindModel(array('belongsTo' => array('ProductionPlan','Product')));
$ProductionPlan=$this->ProductionPlanItem->find('all',array(
	"joins"=>array(
		array(
			"table"=>'products',
			"alias"=>'Product',
			"type"=>'inner',
			"conditions"=>array('Product.id=ProductionPlanItem.product_id'),
			),
		// array(
		// 	"table"=>'units',
		// 	"alias"=>'Unit',
		// 	"type"=>'inner',
		// 	"conditions"=>array('Unit.id=BomProduction.unit_id'),
		// 	),
		),
	'conditions'=>array('ProductionPlanItem.production_id'=>$production_plan_id),
	'fields'=>array('Product.name','ProductionPlanItem.total_quantity'),
	));
$Production=[];
if(!empty($ProductionPlan))
{
	foreach ($ProductionPlan as $key => $value) 
	{
		$single['product_name']=$value['Product']['name'];
				//$single['unit']=$value['Unit']['name'];
		$single['quantity']=round($value['ProductionPlanItem']['total_quantity']);
		array_push($Production,$single);
	}
}
$Checkstate=0;
require('fpdf/fpdf.php');
$pdf = new FPDF('p', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) 
{
	$pdf->AddPage();
	$image_line_width=20;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(1, 1, 208, 216);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos=2;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
	$pdf->SetXY($invoice_x_starting, 5);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$pdf->Cell(0,$invoice_pos,'SEMI PRODUCTION ',0,0,'C');
	$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring-2,209, $gst_details_y_staring-2); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
$gst_pos+=0;
$pdf->text(2,$gst_pos,'Production Number :'.$production_plan_ids['ProductionPlan']['production_no']);
//$gst_pos+=4;
//$pdf->text(2,$gst_pos,'Date Of Order     : '.date("d-m-Y",strtotime($production_plan_ids['ProductionPlan']['date_of_order'])));
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Date Of Production: '.date("d-m-Y",strtotime($production_plan_ids['ProductionPlan']['date'])));
//$gst_pos+=4;
//$pdf->text(2,$gst_pos,'Date Of Delivered  : '.date("d-m-Y",strtotime($production_plan_ids['ProductionPlan']['date_of_delivered'])));
$pdf->Line(1, $gst_details_y_staring+17,209, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=52;

$table_length=165;
$pdf->text(2,$item_table_head_y_start+6,'SL No');
$table_column=12;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+20,$item_table_head_y_start+6,'Product ');
$table_column+=120;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'Quantity');
$table_column+=30;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
//$pdf->text($table_column+10,$item_table_head_y_start+6,'Unit');
$table_column+=7;


}
function footer($pdf)
{}
header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) ;
$total=0;
$item_table_head_y_start='60';
$count=count($Production);
$i=0;
foreach ($Production as $key => $value) {
$item_pos=2;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$key+1);
$item_pos+=15;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['product_name']);
$item_pos+=120;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['quantity']);
$item_pos+=30;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['unit']);
$i++;
}
footer($pdf);
$pdf->Output();
exit;

}
public function semi_production_order_print($order_no=null)
{
$this->Order->unbindModel(array('hasMany' => array('OrderItem')));
$production_plan_ids=$this->Order->find('first',array(
	// "joins"=>array(
	// 	array(
	// 		"table"=>'customers',
	// 		"alias"=>'Customer',
	// 		"type"=>'inner',
	// 		"conditions"=>array('Customer.account_head_id=Order.account_head_id'),
	// 		),
	// 	// array(
	// 	// 	"table"=>'units',
	// 	// 	"alias"=>'Unit',
	// 	// 	"type"=>'inner',
	// 	// 	"conditions"=>array('Unit.id=BomProduction.unit_id'),
	// 	// 	),
	// 	),
	'conditions'=>array('Order.order_no'=>$order_no),
	'fields'=>array('Order.id','Order.date','Order.order_no',)));
$production_plan_id=$production_plan_ids['Order']['id'];
//pr($production_plan_id);
//exit;
$this->OrderItem->unbindModel(array('belongsTo' => array('Order','Product')));
$ProductionPlan=$this->OrderItem->find('all',array(
	"joins"=>array(
		array(
			"table"=>'products',
			"alias"=>'Product',
			"type"=>'inner',
			"conditions"=>array('Product.id=OrderItem.product_id'),
			),
		// array(
		// 	"table"=>'units',
		// 	"alias"=>'Unit',
		// 	"type"=>'inner',
		// 	"conditions"=>array('Unit.id=BomProduction.unit_id'),
		// 	),
		),
	'conditions'=>array('OrderItem.order_id'=>$production_plan_id),
	'fields'=>array('Product.name','OrderItem.quantity'),
	));
$Production=[];
if(!empty($ProductionPlan))
{
	foreach ($ProductionPlan as $key => $value) 
	{
		$single['product_name']=$value['Product']['name'];
				//$single['unit']=$value['Unit']['name'];
		$single['quantity']=round($value['OrderItem']['quantity']);
		array_push($Production,$single);
	}
}
$Checkstate=0;
require('fpdf/fpdf.php');
$pdf = new FPDF('p', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) 
{
	$pdf->AddPage();
	$image_line_width=20;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(1, 1, 208, 216);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos=2;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
	$pdf->SetXY($invoice_x_starting, 5);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$pdf->Cell(0,$invoice_pos,'Semi Production Order ',0,0,'C');
	$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring-2,209, $gst_details_y_staring-2); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
$gst_pos+=0;
//$pdf->text(2,$gst_pos,'Route            :'.$production_plan_ids['Route']['name']);
$pdf->text(2,$gst_pos,'Order Number       :'.$production_plan_ids['Order']['order_no']);
$gst_pos+=4;
//$pdf->text(2,$gst_pos,'Executive     : '.$production_plan_ids['Executive']['name']);
$pdf->text(2,$gst_pos,'Date Of Order        : '.date("d-m-Y",strtotime($production_plan_ids['Order']['date'])));
$gst_pos+=4;
//$pdf->text(2,$gst_pos,'Customer     : '.$production_plan_ids['AccountHead']['name']);
//$pdf->text(150,$gst_pos,'Date Of Delivered  : '.date("d-m-Y",strtotime($production_plan_ids['Order']['date_of_delivered'])));

$gst_pos+=4;
//$pdf->text(2,$gst_pos,'Address       : '.$production_plan_ids['Customer']['place']);

$pdf->Line(1, $gst_details_y_staring+17,209, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=52;

$table_length=165;
$pdf->text(2,$item_table_head_y_start+6,'SL No');
$table_column=12;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+20,$item_table_head_y_start+6,'Product ');
$table_column+=120;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'Quantity');
$table_column+=30;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
//$pdf->text($table_column+10,$item_table_head_y_start+6,'Unit');
$table_column+=7;


}
function footer($pdf)
{}
header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) ;
$total=0;
$item_table_head_y_start='60';
$count=count($Production);
$i=0;
foreach ($Production as $key => $value) {
$item_pos=2;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$key+1);
$item_pos+=15;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['product_name']);
$item_pos+=120;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['quantity']);
$item_pos+=30;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['unit']);
$i++;
}
footer($pdf);
$pdf->Output();
exit;

}
public function material_request_print($production_no=null)
{
$this->MaterialRequest->unbindModel(array('hasMany' => array('MaterialRequestItem')));
$production_plan_ids=$this->MaterialRequest->find('first',array(
	'conditions'=>array('MaterialRequest.id'=>$production_no),
	'fields'=>array('MaterialRequest.id','MaterialRequest.updated_at','MaterialRequest.production_no','MaterialRequest.number_of_items')));
$production_plan_id=$production_plan_ids['MaterialRequest']['id'];
//pr($production_plan_id);
//exit;
$this->MaterialRequestItem->unbindModel(array('belongsTo' => array('MaterialRequest','Product','Unit')));
$ProductionPlan=$this->MaterialRequestItem->find('all',array(
	"joins"=>array(
		array(
			"table"=>'products',
			"alias"=>'Product',
			"type"=>'inner',
			"conditions"=>array('Product.id=MaterialRequestItem.product_id'),
			),
		array(
			"table"=>'units',
			"alias"=>'Unit',
			"type"=>'inner',
			"conditions"=>array('Unit.id=MaterialRequestItem.unit_id'),
			),
		),
	'conditions'=>array('MaterialRequestItem.m_r_id'=>$production_plan_id),
	'fields'=>array('Product.name','MaterialRequestItem.quantity','MaterialRequestItem.shortage','Unit.name'),
	));
$Production=[];
if(!empty($ProductionPlan))
{
	foreach ($ProductionPlan as $key => $value) 
	{
		$single['product_name']=$value['Product']['name'];
		$single['unit']=$value['Unit']['name'];
		$single['quantity']=round($value['MaterialRequestItem']['quantity'],6);
		$single['shortage']=round($value['MaterialRequestItem']['shortage'],6);
		array_push($Production,$single);
	}
}
$Checkstate=0;
require('fpdf/fpdf.php');
$pdf = new FPDF('p', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) 
{
	$pdf->AddPage();
	$image_line_width=20;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(1, 1, 208, 216);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos=2;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
	$pdf->SetXY($invoice_x_starting, 5);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$pdf->Cell(0,$invoice_pos,'MATERIAL REQUEST',0,0,'C');
	$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring-2,209, $gst_details_y_staring-2); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
$gst_pos+=0;
$pdf->text(2,$gst_pos,'Production Number :'.$production_plan_ids['MaterialRequest']['production_no']);
//$gst_pos+=4;
//$pdf->text(2,$gst_pos,'Date Of Order     : '.date("d-m-Y",strtotime($production_plan_ids['ProductionPlan']['date_of_order'])));
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Date                           : '.date("d-m-Y",strtotime($production_plan_ids['MaterialRequest']['updated_at'])));
//$gst_pos+=4;
//$pdf->text(2,$gst_pos,'Date Of Delivered  : '.date("d-m-Y",strtotime($production_plan_ids['ProductionPlan']['date_of_delivered'])));
$pdf->Line(1, $gst_details_y_staring+17,209, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=52;

$table_length=165;
$pdf->text(2,$item_table_head_y_start+6,'SL No');
$table_column=12;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+20,$item_table_head_y_start+6,'Product ');
$table_column+=80;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'Quantity');
$table_column+=50;
$pdf->Line($table_column-10, $item_table_head_y_start,$table_column-10, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column-5,$item_table_head_y_start+6,'Quantity to be Purchase');
$table_column+=30;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+10,$item_table_head_y_start+6,'Unit');
$table_column+=7;


}
function footer($pdf)
{}
header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) ;
$total=0;
$item_table_head_y_start='60';
$count=count($Production);
$i=0;
foreach ($Production as $key => $value) {
$item_pos=2;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$key+1);
$item_pos+=15;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['product_name']);
$item_pos+=80;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['quantity']);
$item_pos+=50;
$pdf->text($item_pos-5,$item_table_head_y_start+7+($i*6),$value['shortage']);
$item_pos+=30;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['unit']);
$i++;
}
footer($pdf);
$pdf->Output();
exit;

}
public function ingredients_print($product_id=null)
{
//$this->MaterialRequest->unbindModel(array('hasMany' => array('MaterialRequestItem')));
$production_plan_ids=$this->Ingredient->find('first',array(
	'conditions'=>array('Ingredient.product_id'=>$product_id),
	'fields'=>array('Ingredient.id','Ingredient.updated_at','Ingredient.product_id','Ingredient.created_at','Product.id','Product.name')));
$production_plan_id=$production_plan_ids['Ingredient']['id'];
//pr($production_plan_id);
//exit;
$this->IngredientsItem->unbindModel(array('belongsTo' => array('Ingredient','Product','Unit')));
$ProductionPlan=$this->IngredientsItem->find('all',array(
	"joins"=>array(
		array(
			"table"=>'products',
			"alias"=>'Product',
			"type"=>'inner',
			"conditions"=>array('Product.id=IngredientsItem.product_id'),
			),
		array(
			"table"=>'units',
			"alias"=>'Unit',
			"type"=>'inner',
			"conditions"=>array('Unit.id=IngredientsItem.unit_id'),
			),
		),
	'conditions'=>array('IngredientsItem.incredient_id'=>$production_plan_id),
	'fields'=>array('Product.name','IngredientsItem.quantity','Unit.name'),
	));
$Production=[];
if(!empty($ProductionPlan))
{
	foreach ($ProductionPlan as $key => $value) 
	{
		$single['product_name']=$value['Product']['name'];
		$single['unit']=$value['Unit']['name'];
		$single['quantity']=round($value['IngredientsItem']['quantity'],6);
				//$single['shortage']=round($value['IngredientsItem']['shortage']);
		array_push($Production,$single);
	}
}
$Checkstate=0;
require('fpdf/fpdf.php');
$pdf = new FPDF('p', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) 
{
	$pdf->AddPage();
	$image_line_width=20;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(1, 1, 208, 216);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos=2;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
	$pdf->SetXY($invoice_x_starting, 5);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$pdf->Cell(0,$invoice_pos,'INGREDIENTS',0,0,'C');
	$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring-2,209, $gst_details_y_staring-2); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
$gst_pos+=0;
$pdf->text(2,$gst_pos,'Product          :'.$production_plan_ids['Product']['name']);
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Created At     : '.date("d-m-Y",strtotime($production_plan_ids['Ingredient']['created_at'])));
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Updated At    : '.date("d-m-Y",strtotime($production_plan_ids['Ingredient']['updated_at'])));
//$gst_pos+=4;
//$pdf->text(2,$gst_pos,'Date Of Delivered  : '.date("d-m-Y",strtotime($production_plan_ids['ProductionPlan']['date_of_delivered'])));
$pdf->Line(1, $gst_details_y_staring+17,209, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=52;

$table_length=165;
$pdf->text(2,$item_table_head_y_start+6,'SL No');
$table_column=12;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+20,$item_table_head_y_start+6,'Product ');
$table_column+=120;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'Quantity');
//$table_column+=50;
// $pdf->Line($table_column-10, $item_table_head_y_start,$table_column-10, $item_table_head_y_start+$table_length); // vertical line
// $pdf->text($table_column-5,$item_table_head_y_start+6,'Quantity to be Purchase');
$table_column+=30;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+10,$item_table_head_y_start+6,'Unit');
$table_column+=7;


}
function footer($pdf)
{}
header_section($pdf,$Production,$Profile,$total_page,$page,$Checkstate,$production_plan_ids) ;
$total=0;
$item_table_head_y_start='60';
$count=count($Production);
$i=0;
foreach ($Production as $key => $value) {
$item_pos=2;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$key+1);
$item_pos+=15;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['product_name']);
$item_pos+=120;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['quantity']);
//$item_pos+=50;
//$pdf->text($item_pos-5,$item_table_head_y_start+7+($i*6),$value['shortage']);
$item_pos+=30;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value['unit']);
$i++;
}
footer($pdf);
$pdf->Output();
exit;

}
public function get_damage_production_quantity()
{
$data=$this->request->data;
  $this->Stock->virtualFields=['total_quantity'=>"SUM(Stock.quantity)"];
$Stock=$this->Stock->find('first',array(
	
	'conditions'=>array('Stock.product_id'=>$data['product_id'],'Warehouse.warehouse_id'=>1),
	'fields'=>array('Stock.total_quantity'),
	));
  $return['result']='Success';
  $return['damage_quantity']=0;
if(!empty($Stock)){
$return['damage_quantity']=floatval($Stock['Stock']['total_quantity']);
$return['result']='Success';
}
echo json_encode($return);
exit;
}
public function get_recycle_list()
{
  $requestData=$this->request->data;
  $columns = [];
  $columns[]='DamageRecycle.issue_no';
  $columns[]='DamageRecycle.date';
  $columns[]='DamageRecycle.remarks';
  $columns[]='DamageRecycle.id'; 
  $conditions=[];
$from_date = date('Y-m-d',strtotime($requestData['from_date']));
  $to_date   = date('Y-m-d',strtotime($requestData['to_date']));
  if(isset($from_date) && isset($to_date))
  {
    $conditions['DamageRecycle.date BETWEEN ? and ?'] =  array($from_date, $to_date);
  }
  $totalData=$this->DamageRecycle->find('count',['conditions'=>$conditions]);
  $totalFiltered=$totalData;
  if( !empty($requestData['search']['value']) ) { 
    $q=$requestData['search']['value'];
    $conditions['OR']=array(
      'DamageRecycle.remark LIKE' =>'%'. $q . '%',
    );
    $totalFiltered=$this->DamageRecycle->find('count',[
      'conditions'=>$conditions,
    ]);
  }
    $Data=$this->DamageRecycle->find('all',array(
    'conditions'=>$conditions,
    'offset'=>$requestData['start'],
    'limit'=>$requestData['length'],
    'fields'=>array(
      'DamageRecycle.*',
    )
  ));
  foreach ($Data as $key => $value) {
    $table_id=$value['DamageRecycle']['id'];
        $Data[$key]['DamageRecycle']['date']=date('d-m-Y',strtotime($value['DamageRecycle']['date']));
    $Data[$key]['DamageRecycle']['action']='<span hidden class="table_id">'.$table_id.'</span>';
     $Data[$key]['DamageRecycle']['action']='<a href="'.$this->webroot.'ProductionPlan/DamageRecycle/'.$table_id.'" title=""><span<i class="fa fa-2x fa-eye" aria-hidden="true"></i></span></a>';
  }
  $json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData),
    "recordsFiltered"=>intval($totalFiltered),
    "records"        =>$Data
  );
  echo json_encode($json_data);
  exit; 
}
public function DamageRecycle($id=null)
{
    $user_id=1;
  //   $Products=$this->Product->find('list',array(
  //           'fields'=>array('id','name'),
  //           'order'=>array('name ASC'),
  //          "joins"=>array(
		// array(
		// 	"table"=>'production_plan_items',
		// 	"alias"=>'ProductionPlanItem',
		// 	"type"=>'inner',
		// 	"conditions"=>array('Product.id=ProductionPlanItem.product_id'),
		// 	),
		// array(
		// 	"table"=>'production_plans',
		// 	"alias"=>'ProductionPlan',
		// 	"type"=>'inner',
		// 	"conditions"=>array('ProductionPlan.id=ProductionPlanItem.production_id'),
		// 	),
		// ),
  //     'conditions'=>array('Product.type'=>2,'ProductionPlanItem.damage_quantity !='=>0,'ProductionPlan.status'=>2)
  //          ));
      $Products=$this->Product->find('list',array(
            'fields'=>array('id','name'),
            'order'=>array('name ASC'),
           "joins"=>array(
		array(
			"table"=>'stocks',
			"alias"=>'Stock',
			"type"=>'inner',
			"conditions"=>array('Product.id=Stock.product_id'),
			),
		array(
			"table"=>'warehouses',
			"alias"=>'Warehouse',
			"type"=>'inner',
			"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
			),
		),
      'conditions'=>array('Product.active=1','Product.type'=>2,'Stock.quantity !='=>0,'Warehouse.id'=>2)
           ));
    $this->set('Product',$Products);
     $Productsall=$this->Product->find('list',array(
            'fields'=>array('id','name'),
            'order'=>array('name ASC'),
      'conditions'=>array('Product.type'=>8)
           ));
    $this->set('Productsall',$Productsall);
  if (!$this->request->data)
  {
      if(empty($id)){
      $data['DamageRecycle']['date']=date('d-m-Y');
      $this->request->data=$data;
      } 
      else
      {
        $DamageRecycle=$this->DamageRecycle->findById($id);
      $DamageRecycleItem=$this->DamageRecycleItem->find('all',array('conditions'=>array('issue_id'=>$id)));
      $this->request->data=$DamageRecycle;
      $this->set('DamageRecycleItem',$DamageRecycleItem);
      $this->request->data['DamageRecycle']['date']=date('d-m-Y',strtotime($DamageRecycle['DamageRecycle']['date']));
      }
  }
  else
  {
  	$datasource_DamageRecycle=$this->DamageRecycle->getDataSource();
    $datasource_DamageRecycleItem=$this->DamageRecycleItem->getDataSource();
    $datasource_Stock=$this->Stock->getDataSource();
    try{
    $datasource_DamageRecycle->begin();
    $datasource_DamageRecycleItem->begin();
    $datasource_Stock->begin();
        $data=$this->request->data['DamageRecycle'];
        if(empty($id))
        {
          $DamageRecycle=$this->DamageRecycle->find('first',array('order' => 'DamageRecycle.id DESC',));
          if(!empty($DamageRecycle))
          {
          $IssueNo = $DamageRecycle['DamageRecycle']['issue_no'];
          $issue_no=$IssueNo+1;
          }
          else
          {
          $issue_no=1;
          }
          $Save_data=[
          'date'=>date('Y-m-d',strtotime($data['date'])),
          'remarks'=>$data['remarks'],
          'issue_no'=>$issue_no,
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s'),
          ];
          $this->DamageRecycle->create();
          if(!$this->DamageRecycle->save($Save_data))
          {
            $errors = $this->DamageRecycle->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
          $issue_id=$this->DamageRecycle->getLastInsertId();
          for ($i=0; $i <count($data['product_id']) ; $i++) 
          {
            $Item_data=[
            'product_id'=>$data['product_id'][$i],
            'issue_id'=>$issue_id,
            'recycle_quantity'=>$data['recycle_quantity'][$i],
            'recycled_quantity'=>$data['recycled_quantity'][$i],
            'recycled_as'=>$data['recycle_as'][$i],
            ];
                $this->DamageRecycleItem->create();
                if(!$this->DamageRecycleItem->save($Item_data))
                {
                  $errors = $this->DamageRecycleItem->validationErrors;
                  foreach ($errors as $key => $value) {
                    throw new Exception($value[0], 1);
                  }
                }
                else
                {

                  $fromStock=$this->Stock->find('first',array(
                    'conditions'=>array(
                      'Stock.product_id'=>$data['product_id'][$i],
                      'Stock.warehouse_id'=>2,
                      ),
                    'fields'=>array('Stock.id','Stock.quantity')
                    ));
                  $fromQty=$fromStock['Stock']['quantity']-$data['damage_quantity'][$i];
						if($fromQty<0)
				      throw new Exception("Empty From Stock", 1);
                      $remark='Recycle Quantity ['.$issue_id.','.$data['recycle_quantity'][$i].']';
                      $StockController = new StockController;
					$Stock_function_return=$StockController->GeneralStock_Edit_Function($fromStock['Stock']['id'],$fromQty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
						$toStock=$this->Stock->find('first',array(
						'conditions'=>array(
							'Stock.product_id'=>$data['recycle_as'][$i],
							'Stock.warehouse_id'=>1,
							),
						'fields'=>array('Stock.id','Stock.quantity')));
						$warehouse_id=1;
					if(!$toStock)
					{
                      $remark='Recycled Quantity ['.$issue_id.','.$data['recycled_quantity'][$i].']';
						$flag=1;
						$StockController = new StockController;
						$Stock_function_return=$StockController->GeneralStock_Add_Function($warehouse_id,$data['recycle_as'][$i],$data['recycled_quantity'][$i],date('Y-m-d'),$user_id,$remark,$flag);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result'], 1);
					}
					else
					{
						$toQty=$toStock['Stock']['quantity']+$data['recycled_quantity'][$i];
                      $remark='Recycled Quantity ['.$issue_id.','.$data['recycled_quantity'][$i].']';
						$StockController = new StockController;
						$Stock_function_return=$StockController->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
					}

                }
              }
          }
          $return['result']="Success";
        $datasource_DamageRecycle->commit();
      $datasource_DamageRecycleItem->commit();
      $datasource_Stock->commit();
    }
    catch (Exception $e)
    {
      $return['result']=$e->getMessage();
      $datasource_DamageRecycle->rollback();
     $datasource_DamageRecycleItem->rollback();
            $datasource_Stock->commit();
    }
    $this->Session->setFlash(__($return['result']));
  $this->redirect(array('controller' => 'ProductionPlan','action' =>'DamageRecycleList'));
}
}
public function DamageRecycleList()
{
$this->set('from',date("d-m-Y", strtotime('-30 day')));
	$this->set('to',date("d-m-Y"));
}
public function LabourCalculation($id=null)
	{
		if(!$this->request->data)
		{
			$this->request->data['LabourCalculation']['month']=date('m-Y');
			$this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
	        $Product_list=$this->Product->find('list',array('conditions'=>['Product.active=1','type'=>2],'fields'=>['id','product_name']));
	        $this->set(compact('Product_list'));
			if($id)
			{
				$LabourCalculation=$this->LabourCalculation->findById($id);
				$LabourCalculation['LabourCalculation']['month']=date('m-Y',strtotime($LabourCalculation['LabourCalculation']['month']));
				$LabourCalculation['LabourCalculation']['labour_packing']=floatval($LabourCalculation['LabourCalculation']['labour_packing']);
			    $LabourCalculation['LabourCalculation']['quantity']=floatval($LabourCalculation['LabourCalculation']['quantity']);
			    $LabourCalculation['LabourCalculation']['labour_production']=floatval($LabourCalculation['LabourCalculation']['labour_production']);
				$this->request->data['LabourCalculation']=$LabourCalculation['LabourCalculation'];
			}
		}
		else
		{
			try {
				$data=$this->request->data['LabourCalculation'];
				if(!$id)
				{
					$data['month']=date('Y-m-d',strtotime('01-'.$data['month']));
					$Prev=$this->LabourCalculation->findByProductId($data['product_id']);
					if($Prev)
						throw new Exception("Already Created", 1);
					$this->LabourCalculation->create();
					if(!$this->LabourCalculation->save($data))
					{
						$errors = $this->LabourCalculation->validationErrors;
						foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
					}
				}
				else
				{
					$this->LabourCalculation->id=$id;
					$data_update['labour_production']=$data['labour_production'];
					$data_update['labour_packing']=$data['labour_packing'];
					$data_update['quantity']=$data['quantity'];
					if(!$this->LabourCalculation->save($data_update))
					{
						$errors = $this->LabourCalculation->validationErrors;
						foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
					}
				}
				$return['result']='success';
			} catch (Exception $e) {
				$return['result']=$e->getMessage();
			}
			$this->Session->setFlash(__($return['result']));
			return $this->redirect(array('action' => 'LabourCalculation'));
		}
	}
	public function LabourCalculationTable()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='LabourCalculation.month';
		$columns[]='LabourCalculation.Product';
		$columns[]='LabourCalculation.quantity';
	    $columns[]='LabourCalculation.labour_production';
		$columns[]='LabourCalculation.labour_packing';
		$columns[]='LabourCalculation.id';
		$conditions=[];
		$totalData=$this->LabourCalculation->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'LabourCalculation.month LIKE' =>'%'. date('Y-m-d',strtotime('01-'.$q)) . '%',
				'LabourCalculation.labour_packing LIKE' =>'%'. $q . '%',
				'LabourCalculation.labour_production LIKE' =>'%'. $q . '%',
				'LabourCalculation.quantity LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->LabourCalculation->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->LabourCalculation->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'LabourCalculation.*',
				"Product.name"
			)
		));
		foreach ($Data as $key => $value) {
			$Data[$key]['LabourCalculation']['labour_packing']=floatval($value['LabourCalculation']['labour_packing']);
			$Data[$key]['LabourCalculation']['quantity']=floatval($value['LabourCalculation']['quantity']);
			$Data[$key]['LabourCalculation']['labour_production']=floatval($value['LabourCalculation']['labour_production']);
			$Data[$key]['LabourCalculation']['month']=date('m-Y',strtotime($value['LabourCalculation']['month']));
				$Data[$key]['LabourCalculation']['action']='<a href="'.$this->webroot.'ProductionPlan/LabourCalculation/'.$value['LabourCalculation']['id'].'"><i class="fa fa-2x fa-edit"></i></a><a>';
				$Data[$key]['LabourCalculation']['action'].="<a onclick='return confirm('Are you sure?')' href='".$this->webroot."ProductionPlan/LabourCalculationDelete/".$value['LabourCalculation']['id']."'><i class='fa fa-2x fa-trash blue-col'></i></a>";
			
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data);
		exit;
	}
	public function LabourCalculationDelete($id)
	{
		$this->LabourCalculation->id = $id;
		if (!$this->LabourCalculation->exists()) {
			throw new NotFoundException(__('Invalid company working'));
		}
		if ($this->LabourCalculation->delete($id)) {
			$this->Flash->success(__('Labaour Calculation has been deleted.'));
		} else {
			$this->Flash->error(__('Labaour Calculation could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'LabourCalculation'));
	}
	public function get_product_list()
{
	$data=$this->request->data;
  $conditions=array();
		if(!empty($data['delivery_days']))
		{
			$conditions['Product.production_days']=$data['delivery_days'];
		}
		$conditions['Product.active']=1;
		$conditions['Product.type']=[2,8];
		$Product=$this->Product->find('list',array(
			'conditions'=>$conditions,
			'fields'=>array(
				'Product.id',
				'Product.name',
				)
			));
  echo json_encode($Product);
		exit;
}
public function order_delete()
{
  $Order=$this->Order->find('list',array(
    'conditions'=>array('Order.date <'=>"2021-07-04" ),
    'fields'=>array('Order.id','Order.id')
    ));
      foreach ($Order as $key => $value) 
    {
       if(!$this->OrderItem->DeleteAll(array('order_id'=>$key)))
		throw new Exception("Error Processing OrderItem deletion", 1);
        if(!$this->Order->delete($key))
			throw new Exception("Error while deleting", 1);
   }
  exit;
}
public function OrderForm() 
	{
		$date_of_production=date('Y-m-d');
		$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
		$Product_list=$this->Product->find('all',array('order'=>'Product.priority ASC','conditions'=>['Product.active=1','type'=>2],
			'fields'=>['Product.*']));
		$Product_array=[];
		$Executive=$this->Executive->find('list',array(
					'fields'=>array('Executive.id','Executive.name'),
					));
			foreach ($Product_list as $key => $value) {
				$quantity=0;
				$single['Executive']=[];;
				if($Executive){
				$single['Product_name']=$value['Product']['name'];
				foreach ($Executive as $key1 => $value1) {
				$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
				$OrderItem=$this->OrderItem->find('first',array(
					'conditions'=>array(
						'Order.executive_id'=>$key1,'OrderItem.quantity !='=>0,'Order.flag'=>1,'Order.date_of_production'=>$date_of_production,'OrderItem.second_status'=>1,'Order.status'=>1,'Order.type_order'=>1,'OrderItem.product_id'=>$value['Product']['id']),
					'fields'=>array('OrderItem.total_quantity'),
					));
				$executive_array['quantity']=0;
				if(!empty($OrderItem['OrderItem']['total_quantity']))
				{
					$executive_array['quantity']=$OrderItem['OrderItem']['total_quantity'];
				}
				$single['Executive'][]=$executive_array;
			 }
			 array_push($Product_array,$single);
			}
		}
$Checkstate=0;
require('fpdf/fpdf.php');
$pdf = new FPDF('l', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$Product_array,$Profile,$total_page,$page,$Checkstate,$Executive) 
{
	$pdf->AddPage();
	$image_line_width=20;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(5, 5, 287, 200);
	$pdf->SetXY($invoice_x_starting, 10);
	$invoice_pos=2;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(0,$invoice_pos,'Page : '.$page,0,0,'R');
	$pdf->SetXY($invoice_x_starting, 10);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 10);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 10);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 10);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 10);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$gst_details_y_staring=35;
$pdf->Line(5, $gst_details_y_staring-2,292, $gst_details_y_staring-2); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
// $gst_pos+=8;
// $pdf->text(2,$gst_pos,'Production Number :1');
// $gst_pos+=4;
// $pdf->text(2,$gst_pos,'Order Date : '.date("d-m-Y"));

$gst_pos+=1;
$pdf->text(135,$gst_pos,'Date: '.date("d-m-Y"));
$pdf->Line(5, $gst_details_y_staring+8,292, $gst_details_y_staring+8); // horizontal line
$pdf->SetXY(0, 5);
$pdf->SetFont('Arial','B',10);
$pdf->text(135,$gst_pos+10,'ORDER FORM');
$pdf->Line(5, $gst_details_y_staring+17,292, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(5, $gst_details_y_staring+25,292, $gst_details_y_staring+25); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=52;

$table_length=153;
$pdf->text(10,$item_table_head_y_start+6,'ITEM NAME');
$table_column=60;
foreach ($Executive as $key2 => $value2) 
{
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+3,$item_table_head_y_start+6,$value2);
$table_column+=35;
}
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+3,$item_table_head_y_start+6,"Total");
}
function footer($pdf)
{}
header_section($pdf,$Product_array,$Profile,$total_page,$page,$Checkstate,$Executive) ;
$total=0;
$item_table_head_y_start='60';
$count=count($Product_array);
$i=0;
$pdf->SetFont('Arial','',9);
foreach ($Product_array as $key_list => $value_list) {
$pdf->text(7,$item_table_head_y_start+8+($i*6),$value_list['Product_name']);
$item_pos=65;
$total_qty=0;
foreach ($value_list['Executive'] as $key3 => $value3) {
$total_qty+=$value3['quantity'];
$pdf->text($item_pos,$item_table_head_y_start+8+($i*6),floatval($value3['quantity']));
$item_pos+=35;
}
$pdf->text($item_pos,$item_table_head_y_start+8+($i*6),floatval($total_qty));
if($i>=30)
		{
			$page++;
        header_section($pdf,$Product_array,$Profile,$total_page,$page,$Checkstate,$Executive) ;
			$i=-1;
			$count-=30;
		}
$i++;
}
footer($pdf);
$pdf->Output();
exit;

	}
public function OrderForm_second($date,$warehouse_id) 
	{
		$date_of_production=date('Y-m-d',strtotime($date));
		$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
		$Product_list=$this->Product->find('all',array('order'=>'Product.priority ASC','conditions'=>['Product.active=1','type'=>2],
			'fields'=>['Product.*']));
		$Product_array=[];
		$Executive_name=$this->Executive->field('Executive.name',array('Executive.warehouse_id'=>$warehouse_id));

			foreach ($Product_list as $key => $value) {
				$quantity=0;
				$single['Product_name']=$value['Product']['name'];
				$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
				$OrderItem=$this->OrderItem->find('first',array(
					"joins"=>array(
					array(
					"table"=>'executives',
					"alias"=>'Executive',
					"type"=>'inner',
					"conditions"=>array('Executive.id=Order.executive_id'),
					),
					array(
					"table"=>'warehouses',
					"alias"=>'Warehouse',
					"type"=>'inner',
					"conditions"=>array('Warehouse.id=Executive.warehouse_id'),
					),
					),
					'conditions'=>array(
						'Executive.warehouse_id'=>$warehouse_id,'OrderItem.quantity !='=>0,'Order.flag'=>1,'Order.date_of_delivered'=>$date_of_production,'OrderItem.second_status'=>[1,3],'Order.status'=>1,'Order.type_order'=>1,'OrderItem.product_id'=>$value['Product']['id']),
					'fields'=>array('OrderItem.total_quantity'),
					));
				$single['quantity']=0;
				if(!empty($OrderItem['OrderItem']['total_quantity']))
				{
					$single['quantity']=$OrderItem['OrderItem']['total_quantity'];
				}
			 array_push($Product_array,$single);
			    }
$Checkstate=0;
require('fpdf/fpdf.php');
$pdf = new FPDF('p', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$Product_array,$Profile,$total_page,$page,$Checkstate,$date_of_production,$Executive_name) 
{
	$pdf->AddPage();
	$image_line_width=5;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(5, 5, 200.5, 285);
	$pdf->SetXY($invoice_x_starting, 5);
	$pdf->SetFont('Arial','B',9);
	$pdf->Cell(0,6,"ORDER FORM",0,0,'L');
	//$pdf->Cell(0,2,'Page : '.$page,0,0,'R');
	$pdf->SetXY($invoice_x_starting, 10);
	$pdf->Cell(0,2,'Mob : '.$Profile['mobile'],0,0,'R');
	$pdf->SetXY($invoice_x_starting, 13);
	$invoice_pos=3;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	
	$pdf->SetXY($invoice_x_starting, 13);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 13);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 13);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 13);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 13);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$gst_details_y_staring=30;
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
$gst_pos+=8;
$pdf->text(8,$gst_pos,'NAME: '.$Executive_name);
$gst_pos+=6;
$pdf->text(8,$gst_pos,'DATE: '.date('d-m-Y',strtotime($date_of_production)));
$gst_pos+=6;
$pdf->text(8,$gst_pos,'INVOICE NUMBER: ');
$gst_pos+=6;
$pdf->text(8,$gst_pos,'VEHICLE NUMBER: ');
$pdf->Line(5, $gst_details_y_staring+6,205, $gst_details_y_staring+6); // horizontal line
$pdf->Line(5, $gst_details_y_staring+33,205, $gst_details_y_staring+33); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(5, $gst_details_y_staring+43,205, $gst_details_y_staring+43); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=63;

$table_length=227;
$pdf->text(6,$item_table_head_y_start+6,'SL No');
$table_column=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+25,$item_table_head_y_start+6,'PARTICULARS ');
$table_column+=95;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'ORD QTY');
$table_column+=24;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+8,$item_table_head_y_start+6,'DEL QTY');
$table_column+=24;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+6,$item_table_head_y_start+6,'EXP QTY');
$table_column+=24;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+6,$item_table_head_y_start+3,'TRACE');
$pdf->text($table_column+5,$item_table_head_y_start+8,'+');
$pdf->Line($table_column+23.5, $item_table_head_y_start+5,$table_column, $item_table_head_y_start+5); // horizontal line
$pdf->Line($table_column+11, $item_table_head_y_start+5,$table_column+11, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+15,$item_table_head_y_start+8,'-');
$table_column+=7;
}
function footer($pdf)
{}
header_section($pdf,$Product_array,$Profile,$total_page,$page,$Checkstate,$date_of_production,$Executive_name) ;
$total=0;
$item_table_head_y_start='71';
$count=count($Product_array);
$i=0;
$pdf->SetFont('Arial','',8);
foreach ($Product_array as $key_list => $value_list) {
$pdf->text(6,$item_table_head_y_start+8+($i*6),$key_list+1);
$item_pos=17;
$pdf->text($item_pos,$item_table_head_y_start+8+($i*6),$value_list['Product_name']);
$item_pos+=98;
$pdf->text($item_pos,$item_table_head_y_start+8+($i*6),floatval($value_list['quantity']));
$pdf->Line(5, $item_table_head_y_start+10+($i*6),205, $item_table_head_y_start+10+($i*6)); // horizontal line

		if($i>=32)
		{
		$page++;
		header_section($pdf,$Product_array,$Profile,$total_page,$page,$Checkstate,$date_of_production,$Executive_name) ;
		$i=-1;
		$count-=32;
		}
$i++;
}
footer($pdf);
$pdf->Output();
exit;
	}
public function order_form_excel($date,$warehouse_id)
{
	   $this->response->download("daily_stock_trasfer_order_form.csv");
	$date_of_production=date('Y-m-d',strtotime($date));
		$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchaseReturnItem','PurchasedItem','SalesReturnItem','StockLog','Stock')));
		$Product_list=$this->Product->find('all',array('order'=>'Product.priority ASC','conditions'=>['Product.active=1','type'=>[2,8]],
			'fields'=>['Product.*']));
		$Product_array=[];
		$Executive_name=$this->Executive->field('Executive.name',array('Executive.warehouse_id'=>$warehouse_id));

			foreach ($Product_list as $key => $value) {
				$quantity=0;
				$single['sl_no']=$key+1;
				$single['Product_name']=$value['Product']['name'];
				$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
				$OrderItem=$this->OrderItem->find('first',array(
					"joins"=>array(
					array(
					"table"=>'executives',
					"alias"=>'Executive',
					"type"=>'inner',
					"conditions"=>array('Executive.id=Order.executive_id'),
					),
					array(
					"table"=>'warehouses',
					"alias"=>'Warehouse',
					"type"=>'inner',
					"conditions"=>array('Warehouse.id=Executive.warehouse_id'),
					),
					),
					'conditions'=>array(
						'Executive.warehouse_id'=>$warehouse_id,'OrderItem.quantity !='=>0,'Order.flag'=>1,'Order.date_of_delivered'=>$date_of_production,'OrderItem.second_status'=>[1,3],'Order.status'=>1,'Order.type_order'=>1,'OrderItem.product_id'=>$value['Product']['id']),
					'fields'=>array('OrderItem.total_quantity'),
					));
				$single['quantity']=0;
				if(!empty($OrderItem['OrderItem']['total_quantity']))
				{
					$single['quantity']=$OrderItem['OrderItem']['total_quantity'];
				}
			 array_push($Product_array,$single);
			    }
   $Profile=$this->Global_Var_Profile['Profile'];
   $this->set('Product_array', $Product_array);
   $this->set('Executive_name', $Executive_name);
   $this->set('date', date('d-m-Y',strtotime($date_of_production)));
		$this->layout = 'ajax';
		return false;
		exit;
}
public function ExportDailyStockTransfer($id) 
	{
		$StockTransfer=$this->StockTransfer->findById($id);
		$StockTransferItem=$this->StockTransferItem->find('all',array('conditions'=>array('stock_transfer_id'=>$id)));
$Checkstate=0;
$Executive_name=$this->Executive->field('Executive.name',array('Executive.warehouse_id'=>$StockTransfer['StockTransfer']['warehouse_to']));
require('fpdf/fpdf.php');
$pdf = new FPDF('p', 'mm', [297, 210]);
$Profile=$this->Global_Var_Profile['Profile'];
$page=1;
$total_page=1;
$i=0;
function header_section($pdf,$StockTransfer,$Profile,$total_page,$page,$Checkstate,$StockTransferItem,$Executive_name) 
{
	$pdf->AddPage();
	$image_line_width=5;
	$invoice_x_starting=$image_line_width;
// $pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,0,100);
	$pdf->rect(5, 5, 200.5, 285);
	//$pdf->Cell(0,2,'Page : '.$page,0,0,'R');
	//$pdf->SetXY($invoice_x_starting, 10);
	//$pdf->Cell(0,2,'Mob : '.$Profile['mobile'],0,0,'R');
	$pdf->SetXY($invoice_x_starting, 13);
	$invoice_pos=2;
	$pdf->SetFont('Arial','B',18);
	$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
	$pdf->SetFont('Arial','B',8);
	
	$pdf->SetXY($invoice_x_starting, 13);
// $invoice_pos+=8;
	$pdf->SetXY($invoice_x_starting, 13);
	$invoice_pos+=15;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 13);
	$invoice_pos+=10;
	$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 13);
	$invoice_pos+=10;
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
	$pdf->SetXY($invoice_x_starting, 13);
	$invoice_pos+=10;
// $pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
	$gst_details_y_staring=30;
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
//$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
$gst_pos+=8;
$pdf->text(7,$gst_pos,'DATE: '.date('d-m-Y',strtotime($StockTransfer['StockTransfer']['date'])));
$pdf->text(125,$gst_pos,'INVOICE NO: '.$StockTransfer['StockTransfer']['transfer_no']);
$gst_pos+=6;
$pdf->text(7,$gst_pos,'SALES MAN: '.$Executive_name);
$gst_pos+=8;
$pdf->SetFont('Arial','B',11);
$pdf->text(90,$gst_pos,'INVOICE');
$pdf->SetFont('Arial','B',8);
$pdf->Line(5, $gst_details_y_staring+6,205, $gst_details_y_staring+6); // horizontal line
$pdf->Line(5, $gst_details_y_staring+33,205, $gst_details_y_staring+33); // horizontal line
$pdf->SetXY(0, 5);

$pdf->Line(5, $gst_details_y_staring+43,205, $gst_details_y_staring+43); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$item_table_head_y_start=63;

$table_length=227;
$pdf->text(6,$item_table_head_y_start+6,'SL No');
$table_column=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+25,$item_table_head_y_start+6,'ITEMS ');
$table_column+=95;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+6,'ORD QTY');
$table_column+=20;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'TRACE');
$table_column+=20;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'DELI QTY');
$table_column+=20;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'PRICE');
$table_column+=20;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+6,'TOTAL');
$table_column+=7;
}
function footer($pdf)
{}
header_section($pdf,$StockTransfer,$Profile,$total_page,$page,$Checkstate,$StockTransferItem,$Executive_name) ;
$item_table_head_y_start='71';
$count=count($StockTransferItem);
$i=0;
$pdf->SetFont('Arial','',8);
$trace=0;
$total=0;
$table_length=210;
foreach ($StockTransferItem as $key_list => $value_list) {
$trace+=$value_list['StockTransferItem']['trace'];
$total+=$value_list['Product']['wholesale_price']*$value_list['StockTransferItem']['quantity'];
$pdf->text(7,$item_table_head_y_start+7+($i*6),$key_list+1);
$item_pos=17;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$value_list['Product']['name']);
$item_pos+=97;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),floatval($value_list['StockTransferItem']['ordered_qty']));
$item_pos+=20;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),floatval($value_list['StockTransferItem']['trace']));
$item_pos+=20;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),floatval($value_list['StockTransferItem']['quantity']));
$item_pos+=20;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),floatval($value_list['Product']['wholesale_price']));
$item_pos+=18;
$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),floatval($value_list['Product']['wholesale_price']*$value_list['StockTransferItem']['quantity']));
if(count($StockTransferItem)==$key_list+1)
		{
$pdf->Line(5, $table_length+$item_table_head_y_start,205, $table_length+$item_table_head_y_start); // horizontal line
$pdf->SetFont('Arial','B',10);
$pdf->text($item_pos-120,$item_table_head_y_start+$table_length+6,"GRAND TOTAL");
$pdf->text($item_pos-55,$item_table_head_y_start+$table_length+6,$trace);
$pdf->text($item_pos,$item_table_head_y_start+$table_length+6,number_format($total,2));
		}		
		if($i>=35)
		{
		$page++;
		header_section($pdf,$StockTransfer,$Profile,$total_page,$page,$Checkstate,$StockTransferItem,$Executive_name) ;
		$i=-1;
		$count-=35;
		}
		$pdf->SetFont('Arial','',8);
$i++;
}
footer($pdf);
$pdf->Output();
exit;


	}
}