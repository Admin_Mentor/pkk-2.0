<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
App::import('Controller', 'Accounts');
ini_set('memory_limit', '1024M');
class ProfitLossController extends AppController {
	public $scaffold;
	public $uses=[
		'Type',
		'SalesHistoryPeriod',
		'MasterGroup',
		'Group',
		'SubGroup',
		'AccountHead',
		'ProductType',
		'SubType',
		'VendorGroup',
		'Brand',
		'Journal',
		'BankDetail',
		'Purchase',
		'PurchasedItem',
		'AssetPurchase',
		'Sale',
		'SaleItem',
		'MainBranchTransferItem',
		'MainBranchTransfer',
		'StockTransferItem',
		'Vendor',
		'Party',
		'CustomerType',
		'Customer',
		'SalesReturn',
		'SalesReturnItem', 
		'PurchaseReturn',
		'PurchaseReturnItem',
		'StockLog',
		'Stock',
		'ProductType',
		'Product',
		'Brand',
		'ExecutiveRouteMapping',
		'NoSale',
		'Category',
		'Executive',
		'StockTransfer',
		'State',
		'Unit',
		'Route',
		'Branch',
		'CustomerDiscount',
		'DayRegister',
		'ClosingDay',
		'CustomerCircle',
		'Warehouse',
		'GrnOrder',
		'Quotation',
		'ExecutiveJournal',
		'GrnOrderItem',
		'Remark',
		'QuotationItem',
		'MainBranchTransferItem',
		'ExecutiveBonus',
		'ExecutiveBonusDetails',
		'Complaint',
		'ServiceItem',
		'Kafeel',
		'Specification',
		'Model',
		'Task',
		'AreaManager',
		'SystemParameter',
		'AccSubGroup',
		'AccMainGroup',
		'AccLedger'
	];    
    public function ProfitLossReport()
	{
		// $PermissionList = $this->Session->read('PermissionList');
		// $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Reports/ProfitLossReport'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		if(!$this->request->data)
		{
			$date=date('d-m-Y');
			$from_date=date('1-1-2021');
			$to_date=date('d-m-Y');
			$data['Reports']['from_date']=$from_date;
			$data['Reports']['to_date']=$to_date;
			if(isset($user_branch_id))
			{
			$BranchList = $this->Branch->findById($user_branch_id);
			}
			else
			{
			$BranchList=$this->Branch->find('list',array('conditions' =>  array(),'fields'=>array('id','name')));
			}
			$this->set('BranchList',$BranchList);
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			$this->request->data=$data;
			$ProfitLoss=$this->profit_loss_calculator($from_date,$to_date);
			$this->set('ProfitLoss',$ProfitLoss);
			$this->set('decimalpoint',$this->Session->read('decimalpoint'));
		}
	}
	public function ProfitLossReport_ajax()
	{
		$from_date=$this->request->data['from_date'];
		$to_date=$this->request->data['to_date'];
		$branch_id=$this->request->data['branch_id'];
		$from_date=date('Y-m-d',strtotime($from_date));
		$to_date=date('Y-m-d',strtotime($to_date));
		$ProfitLoss=$this->profit_loss_calculator($from_date,$to_date,$branch_id);
		echo json_encode($ProfitLoss);
		exit;
	}
    public function StockCalculator1($from_date,$to_date,$branch_id=null)
	{
		$StockAdjest=$this->AccountHead->findByname('StockAdjest');
		$stock_account_head_id=19;
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$account_single_opening=[];
		$account_single_opening['debit']=0;
		$account_single_opening['credit']=0;
		$AccountHead=$this->AccountHead->findById($stock_account_head_id);
		if(isset($StockAdjest['AccountHead']['opening_balance']))
		{
		$AccountHead['AccountHead']['opening_balance']+=$StockAdjest['AccountHead']['opening_balance'];
		}
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$conditions['Journal.flag']=1;
		//if($branch_id) $conditions['Journal.branch_id']=$branch_id;
		$Journal_Debit_opening=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>array(
				'debit'=>$stock_account_head_id,
				$conditions,
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
			)));
		$account_single_opening['debit']+=$Journal_Debit_opening['Journal']['total_amount'];
		$Journal_Credit_opening=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>array(
				'credit'=>$stock_account_head_id,
				$conditions,
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
			)));
		$account_single_opening['credit']+=$Journal_Credit_opening['Journal']['total_amount'];
		$Journal_Debit=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>array(
				'debit'=>$stock_account_head_id,
				$conditions,
				'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
			)));
		$account_single['debit']+=$Journal_Debit['Journal']['total_amount'];
		$Journal_Credit=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>array(
				'credit'=>$stock_account_head_id,
				$conditions,
				'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
			)));
		$account_single['credit']+=$Journal_Credit['Journal']['total_amount'];
		$return['open']=number_format($AccountHead['AccountHead']['opening_balance']+$account_single_opening['debit']-$account_single_opening['credit'],2,'.','');
		$return['close']=number_format($return['open']+$account_single['debit']-$account_single['credit'],2,'.','');
		return $return;
	}
	public function StockCalculator($from_date,$to_date,$branch_id=null)
	{
		$stock=$this->Stock->find('first',array(
			
			'fields'=>array('SUM(Stock.quantity*Product.cost) as cost')
		));
		$return['open']=0;
		$return['close']=$stock[0]['cost'];
		return $return;
	}

    public function AccountHead_Table_ListByGroupId1($group_id)
	{
		$AccountHeads=$this->AccountHead->find('all',array(
			'joins'=>[
				array(
					'table'=>'acc_sub_groups',
					'alias'=>'AccSubGroup',
					'type'=>'INNER',
					'conditions'=>array('AccSubGroup.id=AccountHead.acc_sub_group_id')
					),
				],
			'conditions'=>array(
				'AccSubGroup.main_group_id'=>$group_id,
			),
			'fields'=>array('AccountHead.*','AccSubGroup.*')
		));
		return $AccountHeads;
	}
	public function AccountHead_Table_ListBySubGroupId1($group_id)
	{
		$AccountHeads=$this->AccountHead->find('all',array(
			'joins'=>[
				array(
					'table'=>'acc_sub_groups',
					'alias'=>'AccSubGroup',
					'type'=>'INNER',
					'conditions'=>array('AccSubGroup.id=AccountHead.acc_sub_group_id')
					),
				],
			'conditions'=>array(
				'AccSubGroup.id'=>$group_id,
			),
			'fields'=>array('AccountHead.*','AccSubGroup.*')
		));
		return $AccountHeads;
	}
	public function AccountHead_list_By_SubGroup_id($sub_group_id)
	{
		$data=$this->AccountHead->find('list',[
			'conditions'=>array('AccountHead.acc_sub_group_id'=>$sub_group_id),
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
			),
		]);
		return $data;
	}
	public function profit_loss_calculator($from_date,$to_date,$branch_id=null)
	{
		$from_date=date('Y-m-d',strtotime($from_date));
		$to_date=date('Y-m-d',strtotime($to_date));
		$ProfitLoss['result']='Success';
		$ProfitLoss['Stock']=$this->StockCalculator1($from_date,$to_date,$branch_id);
		$ProfitLoss['Income']=$this->IncomeCalculator1($from_date,$to_date,$branch_id);
		$ProfitLoss['Expense']=$this->ExpenseCalculator1($from_date,$to_date,$branch_id);
		$ProfitLoss['Purchase']=$this->PurchaseCalculator1($from_date,$to_date,$branch_id);
		$ProfitLoss['Sale']=$this->SaleCalculator1($from_date,$to_date,$branch_id);
		//$ProfitLoss['Service']=$this->ServiceCalculator($from_date,$to_date,$branch_id);
		$Product=$this->Product_id_list_function($from_date,$to_date,$branch_id);
		$GSTReport=['total'=>[]];
		$gst_in=0;
		$gst_out=0;
		$total_in=0;
		$total_out=0;
		$GSTReport['total']['out']=0;
		$GSTReport['total']['in']=0;
		// $single_GSTReport=$this->Get_All_GSTReportForBalanceSheet($Product,$from_date,$to_date,$gst='All',$type='All',$route_id);
		// $GSTReport['total']['in']=$single_GSTReport['tax']['in'];
		// $GSTReport['total']['out']=$single_GSTReport['tax']['out'];
		// $gst=number_format($GSTReport['total']['out']-$GSTReport['total']['in'],2,'.','');
		// if($gst>0)
		// {
		// 	$tax_amount['left']=$gst;
		// 	$tax_amount['right']=0;
		// }
		// else
		// {
		// 	$tax_amount['left']=0;
		// 	$tax_amount['right']=$gst*-1;
		// }
		$tax_amount['left']=0;
		$tax_amount['right']=0;
		$ProfitLoss['Expense']['IndirectExpense']['amount']+=$tax_amount['left'];
		$ProfitLoss['Income']['IndirectIncome']['amount']+=$tax_amount['right'];
		$IndirectExpense_gst_array['name']='tax';
		$IndirectExpense_gst_array['PrePaid']=0;
		$IndirectExpense_gst_array['paid']=0;
		$IndirectExpense_gst_array['Outstanding']=$tax_amount['left'];
		$IndirectExpense_gst_array['total']=$tax_amount['left'];
		$ProfitLoss['Expense']['IndirectExpense']['single']['TAX']=$IndirectExpense_gst_array;
		$IndirectIncome_gst_array['name']='tax';
		$IndirectIncome_gst_array['Received']=0;
		$IndirectIncome_gst_array['Advance']=0;
		$IndirectIncome_gst_array['Accrued']=$tax_amount['right'];
		$IndirectIncome_gst_array['Total']=$tax_amount['right'];
		$ProfitLoss['Income']['IndirectIncome']['single']['TAX']=$IndirectIncome_gst_array;
		$ProfitLoss['First']['Left']=0;
		$ProfitLoss['First']['Right']=0;
		$ProfitLoss['Second']['Left']=0;
		$ProfitLoss['Second']['Right']=0;
		$ProfitLoss['FirstTotal']['Left']=0;
		$ProfitLoss['FirstTotal']['Right']=0;
		$ProfitLoss['Gross']['Left']=0;
		$ProfitLoss['Gross']['Right']=0;
		$ProfitLoss['Net']['Right']=0;
		$ProfitLoss['Net']['Left']=0;
		$ProfitLoss['SecondTotal']['Left']=0;
		$ProfitLoss['SecondTotal']['Right']=0;
		$FirstLeft=$ProfitLoss['Purchase']['Purchase']['amount']+$ProfitLoss['Expense']['DirectExpense']['amount']+$ProfitLoss['Stock']['open']+$tax_amount['right'];
		// $FirstLeft=$ProfitLoss['Purchase']['PurchaseValue']-$ProfitLoss['Purchase']['PurchaseReturn']+$ProfitLoss['Expense']['DirectExpense']['amount']+$ProfitLoss['Stock']['open']+$tax_amount['right'];
		$FirstLeft=number_format($FirstLeft,2,'.','');
		$FirstRight=$ProfitLoss['Sale']['SaleValue']-$ProfitLoss['Sale']['SalesReturn']+$ProfitLoss['Income']['DirectIncome']['amount']+$ProfitLoss['Stock']['close']+$tax_amount['left'];
		$FirstRight=number_format($FirstRight,2,'.','');
		if($FirstLeft>$FirstRight)
		{
			$ProfitLoss['First']['Right']=number_format(($FirstLeft-$FirstRight),2,'.','');
			$ProfitLoss['Gross']['Left']=number_format(($FirstLeft-$FirstRight),2,'.','');
		}
		else
		{
			$ProfitLoss['First']['Left']=number_format(($FirstRight-$FirstLeft),2,'.','');
			$ProfitLoss['Gross']['Right']=number_format(($FirstRight-$FirstLeft),2,'.','');
		}
		$ProfitLoss['FirstTotal']['Left']+=$FirstLeft+$ProfitLoss['First']['Left'];
		$ProfitLoss['FirstTotal']['Right']+=$FirstRight+$ProfitLoss['First']['Right'];
		$SecondLeft=$ProfitLoss['Expense']['IndirectExpense']['amount']+$ProfitLoss['Gross']['Left'];
		$SecondLeft=number_format($SecondLeft,2,'.','');
		$SecondRight=abs($ProfitLoss['Income']['IndirectIncome']['amount'])+$ProfitLoss['Gross']['Right'];
		$SecondRight=number_format($SecondRight,2,'.','');
		if($SecondLeft>$SecondRight)
		{
			$ProfitLoss['Second']['Right']=number_format(($SecondLeft-$SecondRight),2,'.','');
			$ProfitLoss['Net']['Right']=number_format(($SecondLeft-$SecondRight),2,'.','');
		}
		else
		{
			$ProfitLoss['Second']['Left']=number_format(($SecondRight-$SecondLeft),2,'.','');
			$ProfitLoss['Net']['Left']=number_format(($SecondRight-$SecondLeft),2,'.','');
		}
		// if($ProfitLoss['Net']['Left']>0){
		// 	$ProfitLoss['SecondTotal']['Left']+=$ProfitLoss['Net']['Left'];
		// 	$ProfitLoss['SecondTotal']['Right']+=$ProfitLoss['Net']['Left'];
		// }
		// else{
		// 	$ProfitLoss['SecondTotal']['Left']+=$ProfitLoss['Net']['Right'];
		// 	$ProfitLoss['SecondTotal']['Right']+=$ProfitLoss['Net']['Right'];
		// }
		$ProfitLoss['SecondTotal']['Left']+=$ProfitLoss['Net']['Left']+$SecondLeft;
		$ProfitLoss['SecondTotal']['Right']+=$ProfitLoss['Net']['Right']+$SecondRight;
		return $ProfitLoss;
	}
	public function IncomeCalculator1($from_date,$to_date,$branch_id=null)
	{
		$group_id_indirect=8;
		$group_id_direct=13;
		$excluding_ids=['7'];
		$AccountingsController = new AccountingsController;
		$group=[
			$group_id_direct=>'DirectIncome',
			$group_id_indirect=>'IndirectIncome'
		];
		foreach ($group as $group_key => $group_value) {
		// $group_key=5;
		// $group_value='IndirectIncome';
			$All_Accounts_table_list=$this->AccountHead_Table_ListBySubGroupId1($group_key);
			$account_all=[];
			$Total['DirectIncome']=0;
			$Total['IndirectIncome']=0;
			$Total['Advance']=0;
			$Total['Accrued']=0;
			foreach ($All_Accounts_table_list as $keyA => $valueA) {
				$single=[];
				if(!in_array($valueA['AccountHead']['acc_sub_group_id'], $excluding_ids))
				{
					$single['SubGroup']=$valueA['AccSubGroup']['name'];
					if(!isset($account_all[$single['SubGroup']]))
					{
					    $account_all[$single['SubGroup']]['DirectIncome']=0;
						$account_all[$single['SubGroup']]['IndirectIncome']=0;
						$account_all[$single['SubGroup']]['Advance']=0;
						$account_all[$single['SubGroup']]['Accrued']=0;
						$account_all[$single['SubGroup']]['Total']=0;
						$account_all[$single['SubGroup']]['Received']=0;
					}
					$name=explode(' ',$valueA['AccountHead']['name']);
					array_pop($name);
					$name=implode(' ', $name);
					$main_function=$this->General_Journal_Debit_N_Credit_function1($valueA['AccountHead']['id'],$from_date,$to_date,$branch_id);
					if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($valueA['AccountHead']['created_at'])))
					{
						$main_function['credit']+=$valueA['AccountHead']['opening_balance'];
					}
					$account_all[$single['SubGroup']][$group_value]+=$main_function['debit']-$main_function['credit'];
					$account_all[$single['SubGroup']]['Received']+= $main_function['debit']- $main_function['credit'];
					$account_all[$single['SubGroup']]['Total']+=    $main_function['debit']- $main_function['credit'];
					$Total[$group_value]+=                          $main_function['debit']- $main_function['credit'];
					$Advance=$this->AccountHead->findByName($name.' ADVANCE');
					if(!empty($Advance))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function1($Advance['AccountHead']['id'],$from_date,$to_date,$branch_id);
						$Advance_amount=$Advance['AccountHead']['opening_balance'];
						$Advance_amount+=$return_function['debit']-$return_function['credit'];
						$account_all[$single['SubGroup']]['Advance']+=$Advance_amount;
						$account_all[$single['SubGroup']]['Total']-=  $Advance_amount;
						$Total[$group_value]-=                        $Advance_amount;
					}
					$Accrued=$this->AccountHead->findByName($name.' ACCRUED');
					if(!empty($Accrued))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function1($Accrued['AccountHead']['id'],$from_date,$to_date,$branch_id);
						$Accrued_amount=$Accrued['AccountHead']['opening_balance'];
						$Accrued_amount+=$return_function['debit']-$return_function['credit'];
						$account_all[$single['SubGroup']]['Accrued']+=  $Accrued_amount;
						if($main_function['credit']<$Accrued_amount)
						{
							$account_all[$single['SubGroup']]['Total']+=$Accrued_amount;
							$Total[$group_value]+=                      $Accrued_amount;	
						}
						elseif($main_function['credit']==0 && $Accrued_amount!=0)
						{
							$account_all[$single['SubGroup']]['Total']+=$Accrued_amount;
							$Total[$group_value]+=                      $Accrued_amount;	
						}
					}
				}
			}
			$return[$group_value]['amount']=number_format($Total[$group_value],2,'.','');
			$return[$group_value]['single']=$account_all;
		}
		return $return;	
	}
	public function ExpenseCalculator1($from_date,$to_date,$branch_id=null)
	{
		$group_id_indirect=12;
		$group_id_direct=11;
		$group_id=3;
		//$indirect_sub_id=$this->SystemParameter->field('value',array('id'=>17));
		// $excluding_ids=[
		// 	'4','5','8','112','117','363','454'
		// ];
		$excluding_ids=[
			'19'
		];
		$excluding_ids_in=[
			'10','11'
		];
		$AccountingsController = new AccountingsController;
		$group=[
			$group_id_direct=>'DirectExpense',
			$group_id_indirect=>'IndirectExpense'
		];
		
		$Direct_Accounts_table_list=$this->AccountHead_Table_ListBySubGroupId1($group_id_direct);
		$Indirect_Accounts_table_list=$this->AccountHead_Table_ListByGroupId1($group_id);
		 // pr($All_Accounts_table_list);
		 // exit;
		$account_all=[];
		$account_all1=[];
		//$Total['Expense']=0;
		$Total['DirectExpense']=0;
		$Total['IndirectExpense']=0;
		$Total['PrePaid']=0;
		$Total['Outstanding']=0;
		foreach ($Direct_Accounts_table_list as $keyA => $valueA) {
		
			$single=[];
			// if(!in_array($valueA['AccountHead']['id'], $excluding_ids))
			// {
				$single['SubGroup']=$valueA['AccSubGroup']['name'];
				if(!isset($account_all[$single['SubGroup']]))
				{
					//$account_all[$single['SubGroup']]['Expense']=0;
					$account_all[$single['SubGroup']]['DirectExpense']=0;
					$account_all[$single['SubGroup']]['PrePaid']=0;
					$account_all[$single['SubGroup']]['Outstanding']=0;
					$account_all[$single['SubGroup']]['total']=0;
					$account_all[$single['SubGroup']]['paid']=0;
				}
				$name=explode(' ',$valueA['AccountHead']['name']);
				array_pop($name);
				$name=implode(' ', $name);
				$expense_function=$this->General_Journal_Debit_N_Credit_function1($valueA['AccountHead']['id'],$from_date,$to_date,$branch_id);
				if(date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($valueA['AccountHead']['created_at'])))
					$expense_function['debit']+=$valueA['AccountHead']['opening_balance'];
				$total=$expense_function['debit']-$expense_function['credit'];
				$account_all[$single['SubGroup']]['DirectExpense']+=round($total,2);
				$account_all[$single['SubGroup']]['paid']      +=round($total,2);
				$account_all[$single['SubGroup']]['total']     +=round($total,2);
				$Total['DirectExpense']                           +=round($total,2);
				$PrePaid=$this->AccountHead->findByName($name.' PREPAID');
				if(!empty($PrePaid))
				{
					$return_function=$this->General_Journal_Debit_N_Credit_function1($PrePaid['AccountHead']['id'],'0000-00-00',$to_date,$branch_id);
					$PrePaid_amount=0;
					$PrePaid_amount+=$return_function['debit']-$return_function['credit'];
					$account_all[$single['SubGroup']]['PrePaid']+=$PrePaid_amount;
				}
				$Outstanding=$this->AccountHead->findByName($name.' OUTSTANDING');
				if(!empty($Outstanding))
				{
					$Outstanding_amount=$Outstanding['AccountHead']['opening_balance'];
					$return_function=$this->General_Journal_Openging_Debit_N_Credit_function1($Outstanding['AccountHead']['id'],$from_date,$branch_id);						
					$Outstanding_amount+=$return_function['credit']-$return_function['debit'];
					$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
					$conditions['Journal.flag']=1;
					if($branch_id) $conditions['Journal.branch_id']=$branch_id;
					$Journal_Debit=$this->Journal->find('first',array(
						'conditions'=>array(
							'debit'=>$Outstanding['AccountHead']['id'],
							// 'credit'=>$valueA['AccountHead']['id'],
							$conditions,
							'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
						),
						'fields'=>array( 'Journal.total_amount'),
					));
					$Journal_Credit=$this->Journal->find('first',array(
						'conditions'=>array(
							'credit'=>$Outstanding['AccountHead']['id'],
							// 'debit'=>$valueA['AccountHead']['id'],
							$conditions,
							'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
						),
						'fields'=>array( 'Journal.total_amount'),
					));
					$Outstanding_amount+=$Journal_Credit['Journal']['total_amount'];
					$Outstanding_amount-=$Journal_Debit['Journal']['total_amount'];
					// $account_all[$single['SubGroup']]['total']      +=round($Outstanding_amount,2);
					$account_all[$single['SubGroup']]['Outstanding']+=number_format($Outstanding_amount,2,'.','');
					// $account_all[$single['SubGroup']][$group_value] +=round($Outstanding_amount,2);	
					// $Total[$group_value]                            +=round($Outstanding_amount,2);	
				}
			// }			
			
		}
		$return['DirectExpense']['amount']=number_format($Total['DirectExpense'],2,'.','');
		$return['DirectExpense']['single']=$account_all;

		foreach ($Indirect_Accounts_table_list as $keyA => $valueA) {
		
			$single=[];
			if(!in_array($valueA['AccountHead']['acc_sub_group_id'], $excluding_ids_in))
			{
				$single['SubGroup']=$valueA['AccSubGroup']['name'];
				if(!isset($account_all1[$single['SubGroup']]))
				{
					//$account_all[$single['SubGroup']]['Expense']=0;
					$account_all1[$single['SubGroup']]['IndirectExpense']=0;
					$account_all1[$single['SubGroup']]['PrePaid']=0;
					$account_all1[$single['SubGroup']]['Outstanding']=0;
					$account_all1[$single['SubGroup']]['total']=0;
					$account_all1[$single['SubGroup']]['paid']=0;
				}
				$name=explode(' ',$valueA['AccountHead']['name']);
				array_pop($name);
				$name=implode(' ', $name);
				$expense_function=$this->General_Journal_Debit_N_Credit_function1($valueA['AccountHead']['id'],$from_date,$to_date,$branch_id);
				if(date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($valueA['AccountHead']['created_at'])))
					$expense_function['debit']+=$valueA['AccountHead']['opening_balance'];
				$total_in=$expense_function['debit']-$expense_function['credit'];
				$account_all1[$single['SubGroup']]['IndirectExpense']+=round($total_in,2);
				$account_all1[$single['SubGroup']]['paid']      +=round($total_in,2);
				$account_all1[$single['SubGroup']]['total']     +=round($total_in,2);
				$Total['IndirectExpense']                           +=round($total_in,2);
				$PrePaid=$this->AccountHead->findByName($name.' PREPAID');
				if(!empty($PrePaid))
				{
					$return_function=$this->General_Journal_Debit_N_Credit_function1($PrePaid['AccountHead']['id'],'0000-00-00',$to_date,$branch_id);
					$PrePaid_amount=0;
					$PrePaid_amount+=$return_function['debit']-$return_function['credit'];
					$account_all1[$single['SubGroup']]['PrePaid']+=$PrePaid_amount;
				}
				$Outstanding=$this->AccountHead->findByName($name.' OUTSTANDING');
				if(!empty($Outstanding))
				{
					$Outstanding_amount=$Outstanding['AccountHead']['opening_balance'];
					$return_function=$this->General_Journal_Openging_Debit_N_Credit_function1($Outstanding['AccountHead']['id'],$from_date,$branch_id);						
					$Outstanding_amount+=$return_function['credit']-$return_function['debit'];
					$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
					$conditions1['Journal.flag']=1;
					if($branch_id) $conditions1['Journal.branch_id']=$branch_id;
					$Journal_Debit=$this->Journal->find('first',array(
						'conditions'=>array(
							'debit'=>$Outstanding['AccountHead']['id'],
							// 'credit'=>$valueA['AccountHead']['id'],
							$conditions1,
							'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
						),
						'fields'=>array( 'Journal.total_amount'),
					));
					$Journal_Credit=$this->Journal->find('first',array(
						'conditions'=>array(
							'credit'=>$Outstanding['AccountHead']['id'],
							// 'debit'=>$valueA['AccountHead']['id'],
							$conditions1,
							'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
						),
						'fields'=>array( 'Journal.total_amount'),
					));
					$Outstanding_amount+=$Journal_Credit['Journal']['total_amount'];
					$Outstanding_amount-=$Journal_Debit['Journal']['total_amount'];
					$account_all1[$single['SubGroup']]['Outstanding']+=number_format($Outstanding_amount,2,'.','');	
				}
			}			
			
		}
		$return['IndirectExpense']['amount']=number_format($Total['IndirectExpense'],2,'.','');
		$return['IndirectExpense']['single']=$account_all1;
		
		return $return;	
	}
	public function get_accounthead_by_sub_group_ajax()
	{
		$requestData=$this->request->data;
		$sub_group_name=$requestData['sub_group_name'];
		$AccSubGroup=$this->AccSubGroup->findByName(trim($sub_group_name));
		$Data=[];
		$totalData=1;
		$totalFiltered=$totalData;
		if($AccSubGroup)
		{
			$from_date=date('Y-m-d',strtotime($requestData['from_date']));
			$to_date=date('Y-m-d',strtotime($requestData['to_date']));
			
			$gst_on_sale_account_head_id=9;
			$gst_on_purchase_account_head_id=8;		
			$type_name=$AccSubGroup['AccSubGroup']['main_group_id'];
			$sub_group_id=$AccSubGroup['AccSubGroup']['id'];
			$sub_group_name=$AccSubGroup['AccSubGroup']['name'];
			$AccoutHead_list=$this->AccountHead_list_By_SubGroup_id($sub_group_id);
			$return=[];
			foreach ($AccoutHead_list as $key => $name) {
				if($key!=$gst_on_purchase_account_head_id && $key!=$gst_on_sale_account_head_id)
				{
					$AccountHead=$this->AccountHead->findById($key,['AccountHead.id','AccountHead.opening_balance','AccountHead.name','AccountHead.created_at']);
					$single['name']=$name;
					$single['amount']=0;
					if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
					{
						$single['amount']=$AccountHead['AccountHead']['opening_balance'];
					}
					$single['first']=0;
					$single['second']=0;
					$function_value_return=$this->General_Journal_Debit_N_Credit_function1($key,$from_date,$to_date);
					if($type_name==4)
					{
						$Accrued_amount=0;
						$Advance_amount=0;
						$name=explode(' ',$name);
						array_pop($name);
						$name=implode(' ', $name);
						$single['amount']+=$function_value_return['credit'];
						$Advance=$this->AccountHead->findByName($name.' ADVANCE');
						if(!empty($Advance))
						{
							$return_function=$this->General_Journal_Debit_N_Credit_function1($Advance['AccountHead']['id'],$from_date,$to_date);
							$Advance_amount=$Advance['AccountHead']['opening_balance'];
							$Advance_amount+=$return_function['credit']-$return_function['debit'];
						}
						$Accrued=$this->AccountHead->findByName($name.' ACCRUED');
						if(!empty($Accrued))
						{
							$return_function=$this->General_Journal_Debit_N_Credit_function1($Accrued['AccountHead']['id'],$from_date,$to_date);
							$Accrued_amount=$Accrued['AccountHead']['opening_balance'];
							$single['amount']+=$Accrued_amount;
							$Accrued_amount+=$return_function['debit']-$return_function['credit'];
						}
						$single['first']=$Accrued_amount;
						$single['second']=$Advance_amount;
						$single['total']=$single['amount'];
						if($single['total']<$single['first'])
						{
							$single['total']+=$single['first'];
						}
						elseif($single['total']==0 && $single['first']!=0)
						{
							$single['second']+=$single['first']*-1;
							$single['first']=0;
						}
					}
					if($type_name==3)
					{
						$outstanding_amount=0;
						$prePaid_amount=0;
						$name=explode(' ',$name);
						array_pop($name);
						$name=implode(' ', $name);
						$single['amount']+=$function_value_return['debit']-$function_value_return['credit'];
						$PrePaid=$this->AccountHead->findByName($name.' PREPAID');
						if(!empty($PrePaid))
						{
							$return_function=$this->General_Journal_Debit_N_Credit_function1($PrePaid['AccountHead']['id'],'0000-00-00',$to_date);
							$prePaid_amount=$PrePaid['AccountHead']['opening_balance'];
							$prePaid_amount+=$return_function['debit']-$return_function['credit'];
						}
						$Outstanding=$this->AccountHead->findByName($name.' OUTSTANDING');
						if(!empty($Outstanding))
						{
							$return_function=$this->General_Journal_Openging_Debit_N_Credit_function1($Outstanding['AccountHead']['id'],$from_date);						
							$outstanding_amount=$Outstanding['AccountHead']['opening_balance'];
							$outstanding_amount+=$return_function['credit']-$return_function['debit'];
							$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
							$Journal_Debit=$this->Journal->find('first',array(
								'conditions'=>array(
									'debit'=>$Outstanding['AccountHead']['id'],
									'flag=1',
									'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
								),
								'fields'=>array( 'Journal.total_amount'),
							));
							$Journal_Credit=$this->Journal->find('first',array(
								'conditions'=>array(
									'credit'=>$Outstanding['AccountHead']['id'],
									'flag=1',
									'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
								),
								'fields'=>array( 'Journal.total_amount'),
							));
							$outstanding_amount+=$Journal_Credit['Journal']['total_amount'];
							$outstanding_amount-=$Journal_Debit['Journal']['total_amount'];
							$outstanding_amount=round($outstanding_amount,2);
						}
						$single['second']+=$prePaid_amount;	
						$single['total']=$single['amount'];
						if($outstanding_amount!=0)
						{
							$single['first']+=$outstanding_amount;
							$single['total']+=$outstanding_amount;
						}
					}
					$single['amount']=round($single['amount']);
					$single['first']=round($single['first']);
					$single['second']=round($single['second']);
					$single['total']=round($single['total']);
					if($single['first'] || $single['second'] || $single['total'] )
					{
						$return[]=$single;
					}
				}
			}
			$Data=$return;
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData), 
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data);
		exit;
	}
	public function PurchaseCalculator1($from_date,$to_date,$branch_id=null)
	{
		$purchase_account_head_id=5;
		$purchase_return_account_head_id=7;
		$gst_on_purchase_account_head_id=8;
		$debit_note_account_head_id=29;
		$return['PurchaseValue']=0;
		$return['PurchaseReturn']=0;
		//$return['DebitNote']=0;
		$sub_group_id=10;
		$Total['Purchase']=0;$account_all=[];
		// $Purchase_AccountHead=$this->AccountHead->findById($purchase_account_head_id);
		$Purchase_AccountHead=$this->AccountHead->find('all',array(
			'joins'=>[
				array(
					'table'=>'acc_sub_groups',
					'alias'=>'AccSubGroup',
					'type'=>'INNER',
					'conditions'=>array('AccSubGroup.id=AccountHead.acc_sub_group_id')
					),
				],
			'conditions'=>array(
				'AccSubGroup.id'=>$sub_group_id,
			),
			'fields'=>array('AccountHead.name','AccountHead.id','AccountHead.opening_balance','AccountHead.created_at'),
		));
		//print_r($Purchase_AccountHead);die;
		$Purchase_Return_AccountHead=$this->AccountHead->findById($purchase_return_account_head_id);
		//$Debit_Note_AccountHead=$this->AccountHead->findById($debit_note_account_head_id);
		$single=[];
		foreach ($Purchase_AccountHead as $key => $value) {
			$single['AccountHead']=$value['AccountHead']['name'];
			$account_all[$single['AccountHead']]['total']=0;
			$PurchaseValue=$this->General_Journal_Debit_N_Credit_function1($value['AccountHead']['id'],$from_date,$to_date,$branch_id);
			if(date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($value['AccountHead']['created_at'])))
				$PurchaseValue['debit']+=$value['AccountHead']['opening_balance'];
			$return['PurchaseValue']+=$PurchaseValue['debit'];
			$return['PurchaseValue']-=$PurchaseValue['credit'];
			$account_all[$single['AccountHead']]['total'] +=$PurchaseValue['debit']-$PurchaseValue['credit'];
			$Total['Purchase'] +=$PurchaseValue['debit']-$PurchaseValue['credit'];
		}
		$return['Purchase']['amount']=number_format($Total['Purchase'],2,'.','');
		$return['Purchase']['single']=$account_all;
		$GstOnPurchase=$this->General_Journal_Debit_N_Credit_function1($gst_on_purchase_account_head_id,$from_date,$to_date,$branch_id);
		$return['GstOnPurchase']=$GstOnPurchase;
		$PurchaseReturn=$this->General_Journal_Debit_N_Credit_function1($purchase_return_account_head_id,$from_date,$to_date,$branch_id);
		if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($Purchase_Return_AccountHead['AccountHead']['created_at'])))
			$PurchaseReturn['credit']+=$Purchase_Return_AccountHead['AccountHead']['opening_balance'];
		$return['PurchaseReturn']=$PurchaseReturn['credit']-$PurchaseReturn['debit'];
		// $DebitNote=$this->General_Journal_Debit_N_Credit_function1($debit_note_account_head_id,$from_date,$to_date,$branch_id);
		// if(date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($Debit_Note_AccountHead['AccountHead']['created_at'])))
		// 	$DebitNote['credit']+=$Debit_Note_AccountHead['AccountHead']['opening_balance'];
		// $return['DebitNote']=$DebitNote['credit']-$DebitNote['debit'];
		return $return;
	}
	public function SaleCalculator1($from_date,$to_date,$branch_id=null)
	{
        $sale_account_head_id=6;
		$discount_account_head_id=13;
		$sale_return_account_head_id=4;
		$gst_on_sale_account_head_id=9;
		$return['SaleValue']=0;
		$return['SalesReturn']=0;
		$Sale_AccountHead=$this->AccountHead->findById($sale_account_head_id);
		$Sale_Return_AccountHead=$this->AccountHead->findById($sale_return_account_head_id);
		$SaleValue=$this->General_Journal_Debit_N_Credit_function1($sale_account_head_id,$from_date,$to_date,$branch_id);
		$work_flow='Sales Discount';
		$DiscountValue=$this->General_Journal_Discount_function($discount_account_head_id,$from_date,$to_date,$work_flow,$branch_id);
		if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($Sale_AccountHead['AccountHead']['created_at'])))
			$SaleValue['credit']+=$Sale_AccountHead['AccountHead']['opening_balance'];
		$GstOnSale=$this->General_Journal_Debit_N_Credit_function1($gst_on_sale_account_head_id,$from_date,$to_date,$branch_id);
		$return['SaleValue']=($SaleValue['credit']-$SaleValue['debit'])-($DiscountValue['debit']-$DiscountValue['credit']);
		$return['GstOnSale']=$GstOnSale;
		$SalesReturn=$this->General_Journal_Debit_N_Credit_function1($sale_return_account_head_id,$from_date,$to_date,$branch_id);
		if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($Sale_Return_AccountHead['AccountHead']['created_at'])))
			$SalesReturn['debit']+=$Sale_Return_AccountHead['AccountHead']['opening_balance'];
		$return['SalesReturn']=$SalesReturn['debit']-$SalesReturn['credit'];
		return $return;
	}
	public function ServiceCalculator($from_date,$to_date,$branch_id=null)
	{
		$service_account_head_id=160;
		$discount_account_head_id=13;
		$gst_on_service_account_head_id=9;
		$return['ServiceValue']=0;
		$Service_AccountHead=$this->AccountHead->findById($service_account_head_id);
		$ServiceValue=$this->General_Journal_Debit_N_Credit_function1($service_account_head_id,$from_date,$to_date,$branch_id);
		$work_flow='Service Discount';
		$DiscountValue=$this->General_Journal_Discount_function($discount_account_head_id,$from_date,$to_date,$work_flow,$branch_id);
		if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($Service_AccountHead['AccountHead']['created_at'])))
			$ServiceValue['credit']+=$Service_AccountHead['AccountHead']['opening_balance'];
		$GstOnService=$this->General_Journal_Debit_N_Credit_function1($gst_on_service_account_head_id,$from_date,$to_date,$branch_id);
		$return['ServiceValue']=($ServiceValue['credit']-$ServiceValue['debit'])-($DiscountValue['debit']-$DiscountValue['credit']);
		$return['GstOnService']=$GstOnService;
		
		return $return;
	}
	public function General_Journal_Debit_N_Credit_function1($account_head_id,$from_date,$to_date,$branch_id=null)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$Journal_Debit_conditions= [];
		$Journal_Debit_conditions['debit'] = $account_head_id;
		if($branch_id) $Journal_Debit_conditions['Journal.branch_id']=$branch_id;
		$Journal_Debit_conditions['credit !='] =19;
		$Journal_Debit_conditions['flag'] = '1';
		$Journal_Debit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		$Journal_Credit_conditions= [];
		$Journal_Credit_conditions['credit'] = $account_head_id;
		if($branch_id) $Journal_Credit_conditions['Journal.branch_id']=$branch_id;
		$Journal_Credit_conditions['debit !='] = 19;
		$Journal_Credit_conditions['flag'] = '1';
		$Journal_Credit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
			$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
						'conditions'=>array($Journal_Debit_conditions),
						'fields'=>array( 'Journal.total_amount'),
					));
		$Journal_Credit=$this->Journal->find('first',array(
						'conditions'=>array($Journal_Credit_conditions),
						'fields'=>array( 'Journal.total_amount'),
					));
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
	public function General_Journal_Openging_Debit_N_Credit_function1($account_head_id,$from_date,$branch_id=null)
	{	
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$conditions['Journal.flag']=1;
		if($branch_id) $conditions['Journal.branch_id']=$branch_id;
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$account_head_id,
				'credit !='=>19,
				$conditions,
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>array(
				'credit'=>$account_head_id,
				'debit !='=>19,
				$conditions,
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
	public function Product_id_list_function()
	{
		$Product_list=[];
		$SaleItem_DISTINCT_Product=$this->SaleItem->find('all',array('conditions'=>array('Sale.status>=2'),'fields'=>array('DISTINCT SaleItem.product_id')));
		$SalesReturnItem_DISTINCT_Product=$this->SalesReturnItem->find('all',array('conditions'=>array('SalesReturn.status>=2'),'fields'=>array('DISTINCT SalesReturnItem.product_id')));
		$PurchaseReturnItem_DISTINCT_Product=$this->PurchaseReturnItem->find('all',array('conditions'=>array('PurchaseReturn.status>=2'),'fields'=>array('DISTINCT PurchaseReturnItem.product_id')));
		$PurchasedItem_DISTINCT_Product=$this->PurchasedItem->find('all',array('conditions'=>array('Purchase.status>=2'),'fields'=>array('DISTINCT PurchasedItem.product_id')));
		foreach ($PurchasedItem_DISTINCT_Product as $key => $value) {$Product_list[$value['PurchasedItem']['product_id']]=$value['PurchasedItem']['product_id'];}
		foreach ($PurchaseReturnItem_DISTINCT_Product as $key => $value) {$Product_list[$value['PurchaseReturnItem']['product_id']]=$value['PurchaseReturnItem']['product_id'];}
		foreach ($SaleItem_DISTINCT_Product as $key => $value) {$Product_list[$value['SaleItem']['product_id']]=$value['SaleItem']['product_id'];}
		foreach ($SalesReturnItem_DISTINCT_Product as $key => $value) {$Product_list[$value['SalesReturnItem']['product_id']]=$value['SalesReturnItem']['product_id'];}
		return $Product_list;
	}
	public function General_Journal_Discount_function($account_head_id,$from_date,$to_date,$work_flow,$branch_id=null)
	{
		$account_single=[];
		$account_single['discount']=0;
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$Journal_Debit_conditions= [];
		$Journal_Debit_conditions['debit'] = $account_head_id;
		$Journal_Debit_conditions['flag'] = '1';
		$Journal_Debit_conditions['work_flow'] = $work_flow;
		if($branch_id) $Journal_Debit_conditions['Journal.branch_id'] = $branch_id;
		$Journal_Debit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		$Journal_Credit_conditions= [];
		$Journal_Credit_conditions['debit'] = $account_head_id;
		$Journal_Credit_conditions['flag'] = '1';
		$Journal_Credit_conditions['work_flow'] = $work_flow;
		if($branch_id) $Journal_Credit_conditions['Journal.branch_id'] = $branch_id;
		$Journal_Credit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>$Journal_Debit_conditions,
			'fields'=>array( 'Journal.total_amount'),
		));
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>$Journal_Credit_conditions,
			'fields'=>array( 'Journal.total_amount'),
		));
		$account_single['debit']=round($Journal_Debit['Journal']['total_amount']);
		$account_single['credit']=round($Journal_Credit['Journal']['total_amount']);
		return $account_single;
	}
	public function print_profit_loss_report($from_date,$to_date,$branch_id=null) 
	{
		$this->response->download("reports_profit_loss.csv");
		$conditions=array();
		$from=date("Y-m-d", strtotime($from_date));
		$to  =date("Y-m-d", strtotime($to_date));
		$list_array=array();
		$list_array=$this->profit_loss_calculator($from,$to,$branch_id);
		$accounts_array=array();
		$data=[];
		$single=[
			'Opening Stock',
			'',
			'',
			$list_array['Stock']['open'],
			'Net Sales',
			'',
			'',
			//$list_array['Sale']['SaleValue']-$list_array['Sale']['SalesReturn']
			abs(number_format(($list_array['Sale']['SaleValue']-$list_array['Sale']['SalesReturn']),2,'.',''))
		];
		$data[]=$single;
		$single=[
			'',
			'',
			'',
			'',
			'Sale',
			'',
			//$list_array['Sale']['SaleValue'],
			abs(number_format($list_array['Sale']['SaleValue'],2,'.','')),
			'',
		];
		$data[]=$single;
		$single=[
			'',
			'',
			'',
			'',
			'(-)Sales Return',
			'',
			number_format($list_array['Sale']['SalesReturn'],2,'.',''),
			'',
		];
		$data[]=$single;
		// $single=[
		// 	'',
		// 	'',
		// 	'',
		// 	'',
		// 	'(-)Credit Note',
		// 	'',
		// 	number_format($list_array['Sale']['CreditNote'],2,'.',''),
		// 	'',
		// ];
		$data[]=$single;
		// $single=[
		// 	'',
		// 	'',
		// 	'',
		// 	'',
		// 	'Net Service',
		// 	'',
		// 	'',
		// 	number_format($list_array['Service']['ServiceValue'],2,'.',''),
			
		// ];
		$data[]=$single;
		// $single=[
		// 	'',
		// 	'',
		// 	'',
		// 	'',
		// 	'Service',
		// 	'',
		// 	number_format($list_array['Service']['ServiceValue'],2,'.',''),
		// 	'',
		// ];
		$data[]=$single;
		$single=[
			'Net Purchase',
			'',
			'',
			number_format($list_array['Purchase']['PurchaseValue']-$list_array['Purchase']['PurchaseReturn'],2,'.',''),
			'Closing Stock',
			'',
			'',
			$list_array['Stock']['close']
		];
		$data[]=$single;
		$single=[
			'Purchase',
			'',
			number_format($list_array['Purchase']['PurchaseValue'],2,'.',''),
			'',
			'',
			'',
			'',
			'',
		];
		$data[]=$single;
		$single=[
			'(-)Purchase Return',
			'',
			number_format($list_array['Purchase']['PurchaseReturn'],2,'.',''),
			'',
			'',
			'',
			'',
			'',
		];
		// $data[]=$single;
		// $single=[
		// 	'(-)Debit Note',
		// 	'',
		// 	number_format($list_array['Purchase']['DebitNote'],2,'.',''),
		// 	'',
		// 	'',
		// 	'',
		// 	'',
		// 	'',
		// ];
		$data[]=$single;
		$single=[
			'Direct Expense',
			'',
			'',
			$list_array['Expense']['DirectExpense']['amount'],
			'Direct Income',
			'',
			'',
			$list_array['Income']['DirectIncome']['amount']
		];
		$data[]=$single;
		if($list_array['Income']['DirectIncome']['single']) {
			foreach ($list_array['Income']['DirectIncome']['single'] as $key => $value) {
				if($value['Total']){
					$single=[
						'',
						'',
						'',
						'',
						$key,
						'',
						$value['Total'],
						'',
						''
					];
					$data[]=$single;
					$SubGroup=$this->AccSubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllByAccSubGroupId($SubGroup['AccSubGroup']['id']);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						$ClosingBalance=$function_value_return['credit']-$function_value_return['debit'];
						if($ClosingBalance){
							$single=[
								'',
								'',
								'',
								'',
								'   '.$AccountHeadValue['AccountHead']['name'],
								$ClosingBalance,
								'',
								''
							];
							$data[]=$single;
						}
					}
				}
			}
		}
		if($list_array['Expense']['DirectExpense']['single']) {
			foreach ($list_array['Expense']['DirectExpense']['single'] as $key => $value) {
				if($value['total']){
					$single=[
						$key,
						'',
						$value['total'],
						'',
						'',
						'',
						'',
						''
					];
					$data[]=$single;
					$SubGroup=$this->AccSubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllByAccSubGroupId($SubGroup['AccSubGroup']['id']);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						$ClosingBalance=$function_value_return['debit']-$function_value_return['credit'];
						if($ClosingBalance){
							$single=[
								'   '.$AccountHeadValue['AccountHead']['name'],
								$ClosingBalance,
								'',
								'',
								'',
								'',
								'',
								''
							];
							$data[]=$single;
						}
					}
				}
			}
		}
		$single=[
			'Gross Profit c/d',
			'',
			'',
			$list_array['First']['Left'],
			'Gross Loss c/d',
			'',
			'',
			$list_array['First']['Right']
		];
		$data[]=$single;
		$single=[
			'Total',
			'',
			'',
			$list_array['FirstTotal']['Left'],
			'Total',
			'',
			'',
			$list_array['FirstTotal']['Right']
		];
		$data[]=$single;
		$single=[
			'Gross Loss b/d',
			'',
			'',
			$list_array['Gross']['Left'],
			'Gross Profit b/d',
			'',
			'',
			$list_array['Gross']['Right']
		];
		$data[]=$single;
		$single=[
			'Indirect Expense',
			'',
			'',
			$list_array['Expense']['IndirectExpense']['amount'],
			'Indirect Income',
			'',
			'',
			abs(number_format($list_array['Income']['IndirectIncome']['amount'],2,'.',''))
		];
		$data[]=$single;
		if($list_array['Income']['IndirectIncome']['single']) {
			foreach ($list_array['Income']['IndirectIncome']['single'] as $key => $value) {
				if($value['Total']){
					$single=[
						'',
						'',
						'',
						'',
						$key,
						'',
						abs(number_format($value['Total'],2,'.','')),
						''
					];
					$data[]=$single;
					$SubGroup=$this->AccSubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllByAccSubGroupId($SubGroup['AccSubGroup']['id']);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						$ClosingBalance=$function_value_return['credit']-$function_value_return['debit'];
						if($ClosingBalance){
							$single=[
								'',
								'',
								'',
								'',
								'   '.$AccountHeadValue['AccountHead']['name'],
								$ClosingBalance,
								'',
								''
							];
							$data[]=$single;
						}
					}
				}
			}
		}
		if($list_array['Expense']['IndirectExpense']['single']) {
			foreach ($list_array['Expense']['IndirectExpense']['single'] as $key => $value) {
				if($value['total']){
					$single=[
						$key,
						'',
						$value['total'],
						'',
						'',
						'',
						'',
						''
					];
					$data[]=$single;
					$SubGroup=$this->AccSubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllByAccSubGroupId($SubGroup['AccSubGroup']['id']);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						$ClosingBalance=$function_value_return['debit']-$function_value_return['credit'];
						if($ClosingBalance){
							$single=[
								'   '.$AccountHeadValue['AccountHead']['name'],
								$ClosingBalance,
								'',
								'',
								'',
								'',
								'',
								''
							];
							$data[]=$single;
						}
					}
				}
			}
		}
		$single=[
			'Net Profit c/d',
			'',
			'',
			$list_array['Net']['Left'],
			'Net Loss c/d',
			'',
			'',
			$list_array['Net']['Right']
		];
		$data[]=$single;
		$single=[
			'Total',
			'',
			'',
			$list_array['SecondTotal']['Left'],
			'Total',
			'',
			'',
			$list_array['SecondTotal']['Right']
		];
		$data[]=$single;
		foreach($data as $key1 => $value1) { 
			array_push($accounts_array,$value1);
		}
		$this->set(compact('accounts_array','from','to','branch_id'));
		$this->layout = 'ajax';
		return false;
		exit;
	}
	public function profit_loss_report_print($from_date,$to_date) 
	{
		// $this->response->download("reports_profit_loss.csv");
		$conditions=array();
		$from=date("Y-m-d", strtotime($from_date));
		$to  =date("Y-m-d", strtotime($to_date));
		$from_date=date("d-M-Y", strtotime($from_date));
		$to_date  =date("d-M-Y", strtotime($to_date));
		$list_array=array();
		$list_array=$this->profit_loss_calculator($from,$to);
		$accounts_array=array();
		$data=[];
		$single=[
			'Opening Stock',
			'',
			'',
			$list_array['Stock']['open'],
			'Net Sales',
			'',
			'',
			//$list_array['Sale']['SaleValue']-$list_array['Sale']['SalesReturn']
			number_format(($list_array['Sale']['SaleValue']-($list_array['Sale']['SalesReturn'])),2,'.','')
		];
		$data[]=$single;
		$single=[
			'',
			'',
			'',
			'',
			'Sale',
			'',
			//$list_array['Sale']['SaleValue'],
			abs(number_format($list_array['Sale']['SaleValue'],2,'.','')),
			'',
		];
		$data[]=$single;
		$single=[
			'',
			'',
			'',
			'',
			'Sales Return',
			'',
			number_format($list_array['Sale']['SalesReturn'],2,'.',''),
			'',
		];
		$data[]=$single;
		// $single=[
		// 	'',
		// 	'',
		// 	'',
		// 	'',
		// 	'Credit Note',
		// 	'',
		// 	number_format($list_array['Sale']['CreditNote'],2,'.',''),
		// 	'',
		// ];
		// $data[]=$single;
		$single=[
			'Net Purchase',
			'',
			'',
			number_format($list_array['Purchase']['PurchaseValue']-$list_array['Purchase']['PurchaseReturn'],2,'.',''),
			'Closing Stock',
			'',
			'',
			$list_array['Stock']['close']
		];
		$data[]=$single;
		$single=[
			'Purchase',
			'',
			number_format($list_array['Purchase']['PurchaseValue'],2,'.',''),
			'',
			'',
			'',
			'',
			'',
		];
		$data[]=$single;
		$single=[
			'Purchase Return',
			'',
			number_format($list_array['Purchase']['PurchaseReturn'],2,'.',''),
			'',
			'',
			'',
			'',
			'',
		];
		$data[]=$single;
		// $single=[
		// 	'Debit Note',
		// 	'',
		// 	number_format($list_array['Purchase']['DebitNote'],2,'.',''),
		// 	'',
		// 	'',
		// 	'',
		// 	'',
		// 	'',
		// ];
		// $data[]=$single;
		$single=[
			'Direct Expense',
			'',
			'',
			number_format($list_array['Expense']['DirectExpense']['amount'],2,'.',''),
			'Direct Income',
			'',
			'',
			number_format($list_array['Income']['DirectIncome']['amount'],2,'.','')
		];
		$data[]=$single;
		if($list_array['Income']['DirectIncome']['single']) {
			foreach ($list_array['Income']['DirectIncome']['single'] as $key => $value) {
				if($value['Total']){
					$single=[
						'',
						'',
						'',
						'',
						$key,
						'',
						number_format($value['Total'],2,'.',''),
						'',
						''
					];
					$data[]=$single;
					$SubGroup=$this->AccSubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllBySubGroupId($SubGroup['AccSubGroup']['id']);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						$ClosingBalance=$function_value_return['credit']-$function_value_return['debit'];
						if($ClosingBalance){
							$single=[
								'',
								'',
								'',
								$AccountHeadValue['AccountHead']['name'],
								'',
								number_format($ClosingBalance,2,'.',''),
								'',
								''
							];
							$data[]=$single;
						}
					}
				}
			}
		}
		if($list_array['Expense']['DirectExpense']['single']) {
			foreach ($list_array['Expense']['DirectExpense']['single'] as $key => $value) {
				if($value['total']){
					$single=[
						$key,
						'',
						number_format($value['total'],2,'.',''),
						'',
						'',
						'',
						'',
						''
					];
					$data[]=$single;
					$SubGroup=$this->AccSubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllBySubGroupId($SubGroup['AccSubGroup']['id']);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						$ClosingBalance=$function_value_return['debit']-$function_value_return['credit'];
						if($ClosingBalance){
							$single=[
								$AccountHeadValue['AccountHead']['name'],
								number_format($ClosingBalance,2,'.',''),
								'',
								'',
								'',
								'',
								'',
								''
							];
							$data[]=$single;
						}
					}
				}
			}
		}
		$single=[
			'Gross Profit c/d',
			'',
			'',
			number_format($list_array['First']['Left'],2,'.',''),
			'Gross Loss c/d',
			'',
			'',
			number_format($list_array['First']['Right'],2,'.','')
		];
		$data[]=$single;
		$single=[
			'Total',
			'',
			'',
			number_format($list_array['FirstTotal']['Left'],2,'.',''),
			'Total',
			'',
			'',
			number_format($list_array['FirstTotal']['Right'],2,'.','')
		];
		$data[]=$single;
		$single=[
			'Gross Loss b/d',
			'',
			'',
			number_format($list_array['Gross']['Left'],2,'.',''),
			'Gross Profit b/d',
			'',
			'',
			number_format($list_array['Gross']['Right'],2,'.','')
		];
		$data[]=$single;
		$single=[
			'Indirect Expense',
			'',
			'',
			abs(number_format($list_array['Expense']['IndirectExpense']['amount'],2,'.','')),
			'Indirect Income',
			'',
			'',
			number_format($list_array['Income']['IndirectIncome']['amount'],2,'.','')
		];
		$data[]=$single;
		if($list_array['Income']['IndirectIncome']['single']) {
			foreach ($list_array['Income']['IndirectIncome']['single'] as $key => $value) {
				if($value['Total']){
					// $single=[
					// 	'',
					// 	'',
					// 	'',
					// 	'',
					// 	$key,
					// 	'',
					// 	'   '.abs(number_format($value['Total'],2,'.','')),
					// 	''
					// ];
					// $data[]=$single;
					$SubGroup=$this->AccSubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllBySubGroupId($SubGroup['AccSubGroup']['id']);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						$ClosingBalance=$function_value_return['credit']-$function_value_return['debit'];
						if($ClosingBalance){
							$single=[
								'',
								'',
								'',
								'',
								$AccountHeadValue['AccountHead']['name'],
								'',
								'   '.number_format($ClosingBalance,2,'.',''),
								''
							];
							//$single=[
							// 	'',
							// 	'',
							// 	'',
							// 	'',
							// 	'   '.$AccountHeadValue['AccountHead']['name'],
							// 	$ClosingBalance,
							// 	'',
							// 	''
							// ];
							$data[]=$single;
						}
					}
				}
			}
		}
		if($list_array['Expense']['IndirectExpense']['single']) {
			foreach ($list_array['Expense']['IndirectExpense']['single'] as $key => $value) {
				if($value['total']){
					// $single=[
					// 	$key,
					// 	'',
					// 	$value['total'],
					// 	'',
					// 	'',
					// 	'',
					// 	'',
					// 	''
					// ];
					// $data[]=$single;
					$SubGroup=$this->AccSubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllBySubGroupId($SubGroup['AccSubGroup']['id']);
					// pr($AccountHeads);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						// pr($function_value_return);
						// $Accrued_amount=$AccountHeadValue['AccountHead']['opening_balance'];
						// $ClosingBalance+=$Accrued_amount;
						$ClosingBalance=$AccountHeadValue['AccountHead']['opening_balance']+$function_value_return['debit']-$function_value_return['credit'];
						// pr($ClosingBalance);
						if($ClosingBalance){
							$single=[
								$AccountHeadValue['AccountHead']['name'],
								number_format(abs($ClosingBalance),2,'.',''),
								'',
								'',
								'',
								'',
								'',
								''
							];
							$data[]=$single;
						}
					}
				}
			}
		}
		$single=[
			'Net Profit c/d',
			'',
			'',
			number_format($list_array['Net']['Left'],2,'.',''),
			'Net Loss c/d',
			'',
			'',
			number_format($list_array['Net']['Right'],2,'.','')
		];
		$data[]=$single;
		$single=[
			'Totals',
			'',
			'',
			number_format($list_array['SecondTotal']['Left'],2,'.',''),
			'Totals',
			'',
			'',
			number_format($list_array['SecondTotal']['Right'],2,'.','')
		];
		$data[]=$single;
		 // pr($data);
	  // exit;
		foreach($data as $key1 => $value1) { 
			array_push($accounts_array,$value1);
		}

	$print_field_array=[];
	$print_field_array[0]="";
		require('fpdf/fpdf.php');
		$pdf = new FPDF('p', 'mm', [297, 210]);

		$Profile=$this->Global_Var_Profile['Profile'];
		$page=1;
		$total_page=1;
		$i=0;
		$igst_x_position=0;
		function header_section($pdf,$accounts_array,$Profile,$total_page,$page,$igst_x_position,$field_value,$from_date,$to_date) 
		{
			$pdf->AddPage();
			
			$image_line_width=40;
			$invoice_x_starting=$image_line_width;

$pdf->rect(1, 1, 208, 295);

// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$invoice_pos+=3;
// $pdf->SetFont('Arial','B',15);
$pdf->SetFont('Arial', 'B', 10, "", 'false');

	$invoice_pos+=2;
	// $pdf->SetFont('dejavusans', 'B', 10);
	$pdf->SetFont('Arial', 'B', 10, "", 'false');
	$pdf->text(88,$invoice_pos+5,$Profile['company_name']);
	// $pdf->SetFont('dejavusans', '', 10);

	// $pdf->text(140,$invoice_pos+3,$Profile['company_name_arabic']);

	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->SetFont('Arial', '', 10, "", 'false');
	$pdf->text(92,$invoice_pos+5,$Profile['address_line_1']);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->text(165,33,$field_value);
	$pdf->text(75,$invoice_pos+5,$Profile['address_line_2'].' ,'.$Profile['mobile']);

	$pdf->text(82,$invoice_pos+11,$Profile['mail_address']);
	$invoice_pos+=17;
	$pdf->text(75,$invoice_pos,'GST Number : '.$Profile['vat_code']);
	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=1;
	$pdf->SetFont('Arial', 'B', 10, "", 'false');
	$pdf->text(88,$invoice_pos+10,'Profit & Loss A/C');
	$invoice_pos+=10;
	// $pdf->SetFont('dejavusans', '', 10);
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);
$pdf->SetFont('Arial', '', 10, "", 'false');
	// $pdf->text(78,$invoice_pos+15,'VAT Number : '.$Profile['vat_code']);
		$pdf->text(80,$invoice_pos+5,$from_date."  to  ".$to_date );

	$gst_details_y_staring=40;
 $pdf->Line(1, $gst_details_y_staring+15,209, $gst_details_y_staring+15); // horizontal line
 $pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
 $pdf->Line(65, $gst_details_y_staring-3,150, $gst_details_y_staring-3); // horizontal line
$pdf->SetFont('Arial', '', 10, "", 'false');
$gst_pos=$gst_details_y_staring+2;
$gst_line_width=120;

 $gst_pos+=28;

$gst_pos+=7;


$pdf->SetXY(0, 5);
// $pdf->Line(40,$gst_details_y_staring,40, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos+=3;

$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+20;
$Consignee_lin_width=100+1;

$pdf->SetFont('Arial', '', 10, "", 'false');



$customer_detail_startin+=7;

$pdf->SetFont('Arial', '', 10, "", 'false');
$customer_detail_startin+=4;
$customer_detail_startin+=4;
$customer_detail_startin+=1;

$pdf->SetFont('Arial', '', 10, "", 'false');

// $pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+7;
//  table head start
$table_length=135;

$table_column=9;
$pdf->SetFont('Arial', 'B', 10, "", 'false');
// $pdf->Line($table_column+95, $item_table_head_y_start-28,$table_column+95, $item_table_head_y_start+60+$table_length); // vertical line
$pdf->text($table_column,$item_table_head_y_start-22,'Particulars');
$pdf->SetFont('Arial', '', 9, "", 'false');
$pdf->text($table_column+35,$item_table_head_y_start-22,$from_date."  to  ".$to_date);

$table_column+=70+$igst_x_position;
$pdf->SetFont('Arial', 'B', 10, "", 'false');

$table_column+=16;
// $pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+20,$item_table_head_y_start-22,'Particulars');
$pdf->SetFont('Arial', '', 9, "", 'false');
$pdf->text($table_column+55,$item_table_head_y_start-22,$from_date."  to  ".$to_date);

$pdf->SetFont('Arial', '', 10, "", 'false');


}

function footer($pdf,$data,$accounts_array)
{
	$footer_start_y=218;
 // $pdf->Line(1, $footer_start_y+60,209, $footer_start_y+60); // horizontal line


}
$page=0;


foreach($print_field_array as $field_key=>$field_value)
{
//$page=1;
	$page++;
	header_section($pdf,$accounts_array,$Profile,$total_page,$page,$igst_x_position,$field_value,$from_date,$to_date);
	$total=0;
	$total_cgst_tax=0;
	$total_sgst_tax=0;
	$total_igst_tax=0;
	$item_table_head_y_start='85';
	$table_length='128';
	$cgst_grand_amount=0;
	$sgst_grand_amount=0;
	$igst_grand_amount=0;
	$rate_grand_amount=0;
	$discount_grand_amount=0;
	$taxable_value_grand_amount=0;
	$product_grand_total=0;
	$count=count($accounts_array);
	$i=0;
// for ($i=0; $i <$count ; $i++) { 
	$data=array(
		'total'=>$total,
		'total_cgst_tax'=>$total_cgst_tax,
		'total_sgst_tax'=>$total_sgst_tax,
		'total_igst_tax'=>$total_igst_tax,
		);

 //pr($accounts_array);
//exit;
	foreach ($accounts_array as $keySI => $value) {

		$k=0;

		if($k)
			$k--;
		$item_pos=0;
		$item_pos+=1;
		
		$pdf->SetFont('Arial', '', 10, "", 'false');
	
		$item_pos+=5;
		if(in_array($value[0], ['Opening Stock','Net Purchase','Indirect Expense','Direct Expense','Gross Profit c/d','Gross Loss b/d','Net Profit c/d'])){
		$pdf->SetFont('Arial', 'B', 10, "", 'false');
		$pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),($value[0]));
	}else if(in_array($value[0], ['Totals'])){
		$totals='Total';
		$pdf->SetFont('Arial', 'B', 10, "", 'false');
		$pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),($totals));
	}
	else if(in_array($value[0], ['Debit Note'])){
		// $totals='Total';
		$pdf->SetFont('Arial', 'I', 10, "", 'false');
		$pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),($value[0]));
	   $pdf->Line(50, $item_table_head_y_start+8+($i*8-19),78, $item_table_head_y_start+8+($i*8-19)); // horizontal line
	}
	else if(in_array($value[0], ['Total'])){
		// $totals='Total';
		$pdf->SetFont('Arial', 'B', 10, "", 'false');
		$pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),($value[0]));
	   $pdf->Line(80, $item_table_head_y_start+8+($i*8-17),105, $item_table_head_y_start+8+($i*8-17)); // horizontal line
	}
	else if(in_array($value[0], ['Purchase','Purchase Return'])){
		$pdf->SetFont('Arial', 'I', 10, "", 'false');
		$pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),($value[0]));
	}
	else{
		$pdf->SetFont('Arial', 'I', 9, "", 'false');
		$pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),($value[0]));
	}
	    // $pdf->text($item_pos+58,$item_table_head_y_start+8+($i*8-20),($value[1]));
	$pdf->SetX(0);
	$pdf->SetY($item_table_head_y_start+8+($i*8-20));
	     $pdf->MultiCell(70,$item_table_head_y_start-87,$value[1],0,'R');
	     $pdf->Ln(3);
	     // $pdf->MultiCell( 0, 10, $value[1], 0, 0, 'R' );
		$pdf->SetFont('Arial', 'I', 10, "", 'false');
		// $pdf->text($item_pos+50,$item_table_head_y_start+8+($i*8-20),($value[2]));
		$pdf->SetX(0);
	    $pdf->SetY($item_table_head_y_start+8+($i*8-20));
	     $pdf->MultiCell(70,$item_table_head_y_start-87,$value[2],0,'R');
	     $pdf->Ln(3);
		$pdf->SetFont('Arial', 'B', 10, "", 'false');
		// $pdf->text($item_pos+80,$item_table_head_y_start+8+($i*8-20),($value[3]));
		$pdf->SetX(0);
	    $pdf->SetY($item_table_head_y_start+8+($i*8-20));
	     $pdf->MultiCell(95,$item_table_head_y_start-87,$value[3],0,'R');
	     $pdf->Ln(3);
		if(in_array($value[4], ['Net Sales','Closing Stock','Direct Income','Gross Loss c/d','Gross Profit b/d','Indirect Income','Net Loss c/d'])){
		$pdf->SetFont('Arial', 'B', 10, "", 'false');
		$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($value[4]));
	}
	else if(in_array($value[4], ['Totals'])){
		// $table_column=12;
		$totals='Total';
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($totals));
        $pdf->Line(1, $item_table_head_y_start+8+($i*8-20-5),209, $item_table_head_y_start+8+($i*8-20-5)); // horizontal line
		$pdf->Line(1, $item_table_head_y_start+8+($i*8-20+4),209, $item_table_head_y_start+8+($i*8-20+4)); // horizontal line
		 // $pdf->Line($table_column+95, $item_table_head_y_start-30,$table_column+95, $item_table_head_y_start+8+($i*8-20+4)); // vertical line
	}
	else if(in_array($value[4], ['Credit Note'])){
		// $totals='Total';
		$pdf->SetFont('Arial', 'I', 10);
		$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($value[4]));
	   $pdf->Line(150, $item_table_head_y_start+8+($i*8-19),175, $item_table_head_y_start+8+($i*8-19)); // horizontal line
	}
	else if(in_array($value[4], ['Total'])){
		// $totals='Total';
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($value[4]));
	   $pdf->Line(180, $item_table_head_y_start+8+($i*8-17),205, $item_table_head_y_start+8+($i*8-17)); // horizontal line
	}
	else if(in_array($value[4], ['Sale','Sales Return'])){
		$pdf->SetFont('Arial', 'I', 10, "", 'false');
		$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($value[4]));
	}
	else{
		$pdf->SetFont('Arial', 'I', 9, "", 'false');
		$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($value[4]));
	}
	    $table_column=12;
		$pdf->SetFont('Arial', 'B', 10, "", 'false');
		$pdf->text($item_pos+170,$item_table_head_y_start+8+($i*8-20),($value[5]));
		$pdf->SetFont('Arial', 'I', 10, "", 'false');
		// $pdf->text($item_pos+150,$item_table_head_y_start+8+($i*8-20),($value[6]));
		$pdf->SetX(0);
	    $pdf->SetY($item_table_head_y_start+8+($i*8-20));
	     $pdf->MultiCell(165,$item_table_head_y_start-87,$value[6],0,'R');
	     $pdf->Ln(3);
		$pdf->SetFont('Arial', 'B', 10, "", 'false');
		// $pdf->text($item_pos+180,$item_table_head_y_start+8+($i*8-20),($value[7]));
		$pdf->SetX(0);
	    $pdf->SetY($item_table_head_y_start+8+($i*8-20));
	     $pdf->MultiCell(195,$item_table_head_y_start-87,$value[7],0,'R');
	     $pdf->Ln(3);
		$pdf->Line($table_column+95, $item_table_head_y_start-30,$table_column+95, $item_table_head_y_start+8+($i*8-20+4)); // vertical line
		// if(count($SaleItem)==$keySI+1)
// 		{

// //$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$igst_grand_amount);
// //$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($product_grand_total,2));
// 		}
		$i+=$k;
		$count+=$k;
		if($i+$k>=24)
		{
//footer($pdf,$data,$Sale,$route_mobile_no);
			 $page++;
			header_section($pdf,$accounts_array,$Profile,$total_page,$page,$igst_x_position,$field_value,$from_date,$to_date);
			// $i-=25;
			// $footer_start_y=218;
           // $pdf->Line(1, ($i+$k),209, ($i+$k)); // horizontal line
		   // $pdf->Line($table_column+95, $item_table_head_y_start-30,$table_column+95, $item_table_head_y_start+8+($i*8-20+4));
		  // $count-2;
		    $i=-1;
			// $count-=23;
		}
		$i++;
	}

	footer($pdf,$data,$accounts_array);
}
		
		$pdf->Output();
		exit;
	}
}