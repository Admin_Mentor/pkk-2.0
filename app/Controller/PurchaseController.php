<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
App::import('Controller', 'Stock');
App::import('Controller', 'Sale');
class PurchaseController extends AppController
{
  public $uses = array(
    'Product',
    'Stock',
    'FixedStock',
    'Party',
    'Vendor',
    'Purchase',
    'PurchasedItem',
    'AssetPurchase',
    'AssetPurchasedItem',
    'UnwantedList',
    'AccountHead',
    'PurchaseReturn',
    'PurchaseReturnItem',
    'State',
    'Expense',
    'PurchaseExpense',
    'Unit',
    'ProductType',
    'Brand',
    'IngredientsItem',
    'OrderItem',
    'Ingredient',
    'StockLog'
  );
  public function PurchaseIndex()
  {
    $PermissionList = $this->Session->read('PermissionList');
    $menu_id = $this->Menu->field(
      'Menu.id',
      array('action ' => 'Purchase/PurchaseIndex'));
    // if(!in_array($menu_id, $PermissionList))
    // {
    //   $this->Session->setFlash("Permission denied");
    //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
    // }
    $first_date = date('Y-m-d',strtotime('first day of this month'));
    $firstdate = date('d-m-Y',strtotime('first day of this month'));
    $this->set('firstdate', $firstdate);
    $last_date = date('Y-m-d');
    $todate = date('d-m-Y');
    $this->set('todate', $todate);
    $Purchase = $this->Purchase->find('all', array(
      "joins" => array(
        array(
          "table" => 'parties',
          "alias" => 'Party',
          "type" => 'inner',
          "conditions" => array('Party.account_head_id=Purchase.account_head_id'),
        ),
      ),
      'conditions' => array(
        '(Purchase.date_of_purchase) between ? and ? '=>array($first_date,$last_date),
        'Purchase.status'=>2,
      ),
      'order' => array('Purchase.id DESC'),
      'fields' => array(
        'AccountHead.*',
        'Party.*',
        'Purchase.*',
      )
    ));
    $this->set('Purchase', $Purchase);
  }
  public function PurchaseOrderIndex()
  {
    $PermissionList = $this->Session->read('PermissionList');
    $menu_id = $this->Menu->field(
      'Menu.id',
      array('action ' => 'Purchase/PurchaseIndex'));
    // if(!in_array($menu_id, $PermissionList))
    // {
    //   $this->Session->setFlash("Permission denied");
    //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
    // }
    $first_date = date('Y-m-d',strtotime('first day of this month'));
    $firstdate = date('d-m-Y',strtotime('first day of this month'));
    $this->set('firstdate', $firstdate);
    $last_date = date('Y-m-d');
    $todate = date('d-m-Y');
    $this->set('todate', $todate);
    $Purchase = $this->Purchase->find('all', array(
      "joins" => array(
        array(
          "table" => 'parties',
          "alias" => 'Party',
          "type" => 'inner',
          "conditions" => array('Party.account_head_id=Purchase.account_head_id'),
        ),
      ),
      'conditions' => array(
        '(Purchase.date_of_purchase) between ? and ? '=>array($first_date,$last_date),
        'Purchase.status'=>1,
      ),
      'order' => array('Purchase.id DESC'),
      'fields' => array(
        'AccountHead.*',
        'Party.*',
        'Purchase.*',
      )
    ));
    $this->set('Purchase', $Purchase);
  }
  public function purchase_search_ajax()
  {
    $data['row']='';
    $first_date = date('Y-m-d',strtotime('first day of this month'));
    $last_date = date('Y-m-d',strtotime('last day of this month'));
    if(!empty($this->request->data['from_date'])){
     $first_date = date('Y-m-d',strtotime($this->request->data['from_date']));
   }
   if(!empty($this->request->data['to_date'])){
     $last_date = date('Y-m-d',strtotime($this->request->data['to_date']));
   }
   $Purchase = $this->Purchase->find('all', array(
    "joins" => array(
      array(
        "table" => 'parties',
        "alias" => 'Party',
        "type" => 'inner',
        "conditions" => array('Party.account_head_id=Purchase.account_head_id'),
      ),
    ),
    'conditions' => array(
      '(Purchase.date_of_purchase) between ? and ? '=>array($first_date,$last_date),
    ),
    'order' => array('date_of_purchase DESC'),
    'fields' => array(
      'AccountHead.*',
      'Party.*',
      'Purchase.*',
    )
  ));
   foreach ($Purchase as $key => $value) {
    if($value['Purchase']['status']==0)
    {
      $name='<i title="Cancelled" style="color:red;" class="fa fa-close fa-3x" aria-hidden="true"></i>';
    }
    if($value['Purchase']['status']==1)
    {
      $name='<i title="Order Placed" style="color:red;" class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>';
      $print='<span style="width: 20px"><a target="_blank" href="'.$this->webroot.'Print/purchase_order/'.$value['Purchase']['id'].'"><i class="fa fa-print fa-2x" aria-hidden="true"></i></a></span>';
    }
    if($value['Purchase']['status']==2)
    {
      $name='<i title="Order Delivered" style="color:green;" class="fa fa-truck fa-2x" aria-hidden="true"></i>';
      $print='<span style="width: 20px"><a target="_blank" href="'.$this->webroot.'Print/fpdf_purchase/'.$value['Purchase']['id'].'"><i class="fa fa-print fa-2x" aria-hidden="true"></i></i></a></span>';
    }
    if($value['Purchase']['status']==3)
    {
      $name='<i title="Altration" style="color:red;" class="fa fa-cart-plus fa-2x" aria-hidden="true"></i>';
    }

    $data['row']= $data['row'].'<tr class="blue-pddng">';
    $data['row']= $data['row'].'<td>';
    $data['row'].='<span>'.date('d-m-Y', strtotime($value['Purchase']['date_of_purchase'])).'</span>';
    $data['row'].='<span class="purchase_id" style="display:none;">'.$value['Purchase']['id'].'</span>';
    $data['row']= $data['row'].'<td>'.$value['AccountHead']['name'].'</td>';
    $data['row']= $data['row'].'<td>'.$value['Purchase']['order_no'].'</td>';
    $data['row']= $data['row'].'<td>'.$value['Purchase']['invoice_no'].'</td>';
    $data['row']= $data['row'].'<td class="text-right">'. number_format($value['Purchase']['grand_total'],2,'.','').'</td>';
    $data['row']= $data['row'].'<td>'.$name.' '.$print. '</td>';
    $data['row']= $data['row'].'</tr>';
  }
  echo json_encode($data);
  exit;
}
public function purchase_order_search_ajax()
{
  $data['row']='';
  $first_date = date('Y-m-d',strtotime('first day of this month'));
  $last_date = date('Y-m-d',strtotime('last day of this month'));
  if(!empty($this->request->data['from_date'])){
   $first_date = date('Y-m-d',strtotime($this->request->data['from_date']));
 }
 if(!empty($this->request->data['to_date'])){
   $last_date = date('Y-m-d',strtotime($this->request->data['to_date']));
 }
 $Purchase = $this->Purchase->find('all', array(
  "joins" => array(
    array(
      "table" => 'parties',
      "alias" => 'Party',
      "type" => 'inner',
      "conditions" => array('Party.account_head_id=Purchase.account_head_id'),
    ),
  ),
  'conditions' => array(
    '(Purchase.date_of_purchase) between ? and ? '=>array($first_date,$last_date),
    'Purchase.status'=>1,
  ),
  'order' => array('date_of_purchase DESC'),
  'fields' => array(
    'AccountHead.*',
    'Party.*',
    'Purchase.*',
  )
));
 foreach ($Purchase as $key => $value) {
  if($value['Purchase']['status']==0)
  {
    $name='<i title="Cancelled" style="color:red;" class="fa fa-close fa-3x" aria-hidden="true"></i>';
  }
  if($value['Purchase']['status']==1)
  {
    $name='<i title="Order Placed" style="color:red;" class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>';
    $print='<span style="width: 20px"><a target="_blank" href="'.$this->webroot.'Print/purchase_order/'.$value['Purchase']['id'].'"><i class="fa fa-print fa-2x" aria-hidden="true"></i></a></span>';
  }
  if($value['Purchase']['status']==2)
  {
    $name='<i title="Order Delivered" style="color:green;" class="fa fa-truck fa-2x" aria-hidden="true"></i>';
    $print='<span style="width: 20px"><a target="_blank" href="'.$this->webroot.'Print/fpdf_purchase/'.$value['Purchase']['id'].'"><i class="fa fa-print fa-2x" aria-hidden="true"></i></i></a></span>';
  }
  if($value['Purchase']['status']==3)
  {
    $name='<i title="Altration" style="color:red;" class="fa fa-cart-plus fa-2x" aria-hidden="true"></i>';
  }

  $data['row']= $data['row'].'<tr class="blue-pddng">';
  $data['row']= $data['row'].'<td>';
  $data['row'].='<span>'.date('d-m-Y', strtotime($value['Purchase']['date_of_purchase'])).'</span>';
  $data['row'].='<span class="purchase_id" style="display:none;">'.$value['Purchase']['id'].'</span>';
  $data['row']= $data['row'].'<td>'.$value['AccountHead']['name'].'</td>';
  $data['row']= $data['row'].'<td>'.$value['Purchase']['order_no'].'</td>';
  $data['row']= $data['row'].'<td>'.$value['Purchase']['invoice_no'].'</td>';
  $data['row']= $data['row'].'<td class="text-right">'. number_format($value['Purchase']['grand_total'],2,'.','').'</td>';
  $data['row']= $data['row'].'<td>'.$name.' '.$print. '</td>';
  $data['row']= $data['row'].'</tr>';
}
echo json_encode($data);
exit;
}
public function AssetPurchaseIndex()
{
  $PermissionList = $this->Session->read('PermissionList');
  $menu_id = $this->Menu->field(
    'Menu.id',
    array('action ' => 'Purchase/AssetPurchaseIndex'));
  // if(!in_array($menu_id, $PermissionList))
  // {
  //   echo 'in';exit;
  //   $this->Session->setFlash("Permission denied");
  //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
  // }
  $first_date = date('Y-m-d',strtotime('first day of this month'));
  $last_date = date('Y-m-d');
  $firstdate = date('d-m-Y',strtotime('first day of this month'));
  $this->set('firstdate', $firstdate);
  $todate = date('d-m-Y');
  $this->set('todate', $todate);
  $AssetPurchase = $this->AssetPurchase->find('all', array(
    "joins" => array(
      array(
        "table" => 'vendors',
        "alias" => 'Vendor',
        "type" => 'inner',
        "conditions" => array('Vendor.account_head_id=AssetPurchase.account_head_id'),
      ),
    ),
    'conditions' => array(
      '(AssetPurchase.date_of_purchase) between ? and ? '=>array($first_date,$last_date),
    ),
    'order' => array('date_of_purchase DESC'),
    'fields' => array(
      'AccountHead.*',
      'Vendor.*',
      'AssetPurchase.*',
    )
  ));
  $this->set('AssetPurchase', $AssetPurchase);
}
public function asset_purchase_search_ajax()
{
  $data['row']='';
  $first_date = date('Y-m-d',strtotime('first day of this month'));
  $last_date = date('Y-m-d',strtotime('last day of this month'));
  if(!empty($this->request->data['from_date'])){
   $first_date = date('Y-m-d',strtotime($this->request->data['from_date']));
 }
 if(!empty($this->request->data['to_date'])){
   $last_date = date('Y-m-d',strtotime($this->request->data['to_date']));
 }
 $AssetPurchase = $this->AssetPurchase->find('all', array(
  "joins" => array(
    array(
      "table" => 'vendors',
      "alias" => 'Vendor',
      "type" => 'inner',
      "conditions" => array('Vendor.account_head_id=AssetPurchase.account_head_id'),
    ),
  ),
  'conditions' => array(
    '(AssetPurchase.date_of_purchase) between ? and ? '=>array($first_date,$last_date),
  ),
  'order' => array('date_of_purchase DESC'),
  'fields' => array(
    'AccountHead.*',
    'Vendor.*',
    'AssetPurchase.*',
  )
));
 foreach ($AssetPurchase as $key => $value) {
  if($value['AssetPurchase']['status']==0)
  {
    $name='<i title="Cancelled" style="color:red;" class="fa fa-close fa-3x" aria-hidden="true"></i>';
  }
  if($value['AssetPurchase']['status']==1)
  {
    $name='<i title="Order Placed" style="color:red;" class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>';
  }
  if($value['AssetPurchase']['status']==2)
  {
    $name='<i title="Order Delivered" style="color:green;" class="fa fa-truck fa-2x" aria-hidden="true"></i>';
  }
  if($value['AssetPurchase']['status']==3)
  {
    $name='<i title="Altration" style="color:red;" class="fa fa-cart-plus fa-2x" aria-hidden="true"></i>';
  }
  $data['row']= $data['row'].'<tr class="blue-pddng">';
  $data['row']= $data['row'].'<td>
  <span>'.date('d-m-Y', strtotime($value['AssetPurchase']['date_of_purchase'])).'</span>
  <span class="purchase_id" style="display:none;">'.$value['AssetPurchase']['id'].'</span>
  </td>';
  $data['row']= $data['row'].'<td>'.$value['AccountHead']['name'].'</td>';
  $data['row']= $data['row'].'<td>'.$value['AssetPurchase']['order_no'].'</td>';
  $data['row']= $data['row'].'<td>'.$value['AssetPurchase']['invoice_no'].'</td>';
  $data['row']= $data['row'].'<td class="text-right">'. round($value['AssetPurchase']['grand_total'],2).'</td>';
  $data['row']= $data['row'].'<td>'.$name.'</td>';
  $data['row']= $data['row'].'</tr>';
}
echo json_encode($data);
exit;
}
public function Purchase($id=null)
{
  $PermissionList = $this->Session->read('PermissionList');
  $menu_id = $this->Menu->field(
    'Menu.id',
    array('action ' => 'Purchase/Purchase'));
  // if(!in_array($menu_id, $PermissionList))
  // {
  //   $this->Session->setFlash("Permission denied");
  //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
  // }
  $purchase_account_head_id=5;
  $discount_received_account_head_id=12;
  $discount_paid_account_head_id=13;
  $roundoff_received_account_head_id=14;
  $roundoff_paid_account_head_id=15;
  // $tax_on_purchase_account_head_id=8;
  $tax_on_purchase_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DUTIES & TAXES'));
  $stock_account_head_id=19;
  $sub_group_id=1;
  $user_id=1;
  // $AccountingsController = new AccountingsController;
  $Accounting_function_return=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
  $this->set('Party_list',$Accounting_function_return);
  $Expense_list=$this->Expense->find('list',array('fields'=>array('id','name')));
  $this->set(compact('Expense_list'));
  $this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
  $Product_list=$this->Product->find('list',array('conditions'=>array('Product.active=1','type !='=>[2,3]),'order'=>array('Product.name ASC'),'fields'=>['id','product_name']));
  $this->set('Product_list',$Product_list);
  $State_list=$this->State->find('list',array('fields'=>array('id','name')));
  $this->set(compact('State_list'));
  // $this->Stock->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
  $quantity_mode=$this->Unit->find('list',array('fields'=>array('Unit.id','Unit.name')));
  $this->set(compact('quantity_mode'));
  $Stock_List = $this->Stock->find('all', array(
    "joins" => array(
      array(
        "table" => 'brands',
        "alias" => 'Brand',
        "type" => 'left',
        "conditions" => array('Brand.id=Product.brand_id'),
      ),
      array(
        "table" => 'product_types',
        "alias" => 'ProductType',
        "type" => 'inner',
        "conditions" => array('ProductType.id=Product.product_type_id'),
      ),
      array(
        "table" => 'units',
        "alias" => 'Unit',
        "type" => 'inner',
        "conditions" => array('Unit.id=Product.unit_id'),
      ),
    ),
    'conditions'=>array('Stock.warehouse_id'=>1),
    'fields' => array(
      'Stock.*',
      'Unit.*',
      'Product.*',
      'ProductType.name',
      'Brand.name',
    )
  ));
  $this->ProductType->virtualFields = array(
    'product_type_name' => "CONCAT(ProductType.name, ' ', ProductType.code)",
  );
  $ProductType=$this->ProductType->find('all',array(
    'fields'=>array(
      'ProductType.id',
      'ProductType.product_type_name',
    )
  )
);
  foreach ($ProductType as $key => $value) {
    foreach ($value['Product'] as $keyP => $valueP) {
      $unit=$this->Unit->field('name',array('id'=>$valueP["unit_id"]));
      $ProductType[$key]['Product'][$keyP]['unit_name']=$unit;
    }
  }
  $this->set('Product',$ProductType);
  $Brand_list=$this->Brand->find('list',array(
    'fields'=>array('id','name'),
    'order'=>array('name ASC'),
  ));
  $Brand_list['G']='GENERAL';
  ksort($Brand_list);
  $this->set('Brand',$Brand_list);
  $ProductType_List = $this->ProductType->find('list', array(
    'fields' => array('id','product_type_name'),
    'order'=>array('name ASC'),
  )
);
  $this->set('ProductType',$ProductType_List);
  $this->Product->unbindModel(array('belongsTo'=>array('ProductType','Brand','Unit'),'hasMany' => array('PurchaseReturnItem','UnwantedList','Stock','StockLog','SalesReturnItem','SaleItem','PurchasedItem','Unit','Brand')));
  $LastBarcode=$this->Product->find('first',array('conditions'=>array('Product.custom_barcode_selection'=>'on'),'fields'=>array('Product.barcode'),'order'=>'Product.barcode DESC'));
  $this->Product->virtualFields = array(
    'product_name' => "CONCAT(Product.name, ' ', Product.code)"
  );
  if($LastBarcode){
    if(empty($LastBarcode['Product']['barcode']))
    {
      $barcode_last=1111111111111;
    }
    else
    {
      $barcode_last=$LastBarcode['Product']['barcode']+1;
    }

  }
  else{
    $barcode_last=1111111111111;
  }
  $barcode_check=$this->Product->findByBarcode($barcode_last);
  while ($barcode_check) {
    $barcode_last++;
    $barcode_check=$this->Product->findByBarcode($barcode_last);
  }
  $this->set('LastBarcode',$barcode_last);
  $unit_name = $this->Unit->find('list', array('fields' => array('id','name')));
  $this->set('unit_name',$unit_name);
  $sub_group_id=1;
  $Accounting_function_return=$this->AccountHead_Option_ListBySubGroupId($sub_group_id);
  $this->set('PartyList',$Accounting_function_return);
  $UnwantedList=$this->UnwantedList->find('list',array('fields'=>array('id','product_id')));
  $this->set('UnwantedList',$UnwantedList);
  $Purchase=$this->Purchase->find('first',array('fields' => array('MAX(Purchase.order_no) as order_no')));
  if(!empty($Purchase))
  {
    $invoice_no=$Purchase[0]['order_no']+1;
    $order_no=$Purchase[0]['order_no']+1;
  }
  else
  {
    $invoice_no=1;
    $order_no=1;
  }
  $invoice_no_check=$this->Purchase->findByInvoiceNo($invoice_no);
  while ($invoice_no_check) {
    $invoice_no+=1;
    $invoice_no_check=$this->Purchase->findByInvoiceNo($invoice_no);        
  }
  $order_no_check=$this->Purchase->findByOrderNo($order_no);
  while ($order_no_check) {
    $order_no+=1;
    $order_no_check=$this->Purchase->findByOrderNo($order_no);        
  }
  //$Stock_List=array();
  $this->set('Stock_List', $Stock_List);
  $data['Party']['opening_balance']='0';
  if (!$this->request->data) 
  {
    if(empty($id))
    {
     // $data['Purchase']['invoice_no']=$invoice_no;

      $data['Purchase']['order_no']=$order_no;
      $data['Purchase']['other_value']='0';
      $data['Purchase']['expense_total']='0';
      $data['Purchase']['date']=date('d-m-Y');
      $data['Purchase']['status']=1;
      $data['Purchase']['total_tax']=5;
      $data['Purchase']['total_tax_import']=0;
       $data['Purchase']['purchase_type']='CreditPurchase';
      $this->request->data=$data;
    }
    else
    {
      $Purchase=$this->Purchase->findById($id);
      $Party=$this->Party->findByAccountHeadId($Purchase['Purchase']['account_head_id']);
      $this->request->data=$Purchase;
      $this->request->data['Purchase']['date']=date('d-m-Y',strtotime($Purchase['Purchase']['date_of_purchase']));
      $this->request->data['Purchase']['address']='';
      $this->request->data['Purchase']['discount']=round($Purchase['Purchase']['discount'],2);
      $this->request->data['Purchase']['discount_amount']=round($Purchase['Purchase']['discount_amount'],2);
       $this->request->data['Purchase']['expense_total']=round($Purchase['Purchase']['expense_total'],2);
        $this->request->data['Purchase']['purchase_type']=$Purchase['Purchase']['purchase_type'];
      if(!empty($Party)){
        $this->request->data['Purchase']['address']=$Party['Party']['place'];
      }
      $PurchasedItem=$this->PurchasedItem->find('all',array(
       "joins" => array(
        array(
          "table" => 'brands',
          "alias" => 'Brand',
          "type" => 'left',
          "conditions" => array('Brand.id=Product.brand_id'),
        ),
        array(
          "table" => 'units',
          "alias" => 'Unit',
          "type" => 'left',
          "conditions" => array('Unit.id=PurchasedItem.quantity_mode'),
        ),
        array(
          "table" => 'product_types',
          "alias" => 'ProductType',
          "type" => 'inner',
          "conditions" => array('ProductType.id=Product.product_type_id'),
        ),
      ),
       'conditions'=>array(
        'purchase_id'=>$id,
      ),
       'order'=>array('PurchasedItem.id'),
       'fields'=>array(
        'PurchasedItem.*',
        'Product.name',
        'Product.code',
        'Brand.name',
        'ProductType.name',
        'Product.no_of_piece_per_unit',
        'Unit.*'
      )
     ));
      foreach ($PurchasedItem as $key => $value) {
        $unit_name = $this->Unit->field(
          'Unit.name',
          array('id ' => $PurchasedItem[$key]['PurchasedItem']['quantity_mode']));
        if($value['PurchasedItem']['quantity_mode'] == 2){
          $unit=$this->Unit->find('first',array('conditions'=>array('Unit.id'=>$value['PurchasedItem']['quantity_mode'])));   
          $PurchasedItem[$key]['PurchasedItem']['quantity']= $PurchasedItem[$key]['PurchasedItem']['quantity']/$PurchasedItem[$key]['Product']['no_of_piece_per_unit'];
          $PurchasedItem[$key]['PurchasedItem']['mrp']= $PurchasedItem[$key]['PurchasedItem']['mrp']*$PurchasedItem[$key]['Product']['no_of_piece_per_unit'];
          $PurchasedItem[$key]['PurchasedItem']['unit_price']= $PurchasedItem[$key]['PurchasedItem']['unit_price']*$PurchasedItem[$key]['Product']['no_of_piece_per_unit'];
          $PurchasedItem[$key]['PurchasedItem']['unit_cost']= $PurchasedItem[$key]['PurchasedItem']['unit_cost']*$PurchasedItem[$key]['Product']['no_of_piece_per_unit'];  
          $PurchasedItem[$key]['PurchasedItem']['wholesale_price']= $PurchasedItem[$key]['PurchasedItem']['wholesale_price']*$PurchasedItem[$key]['Product']['no_of_piece_per_unit'];    
          $PurchasedItem[$key]['PurchasedItem']['unit_name']= $unit['Unit']['name'];    
        }
        else{
         $PurchasedItem[$key]['PurchasedItem']['unit_name']= $PurchasedItem[$key]['Unit']['name']; 
       }
     }
     $this->set('PurchasedItem',$PurchasedItem);
     $PurchaseExpenseAll=$this->PurchaseExpense->find('all',array(
       'conditions'=>array(
        'purchase_id'=>$id,
      ),
       'fields'=>array(
        'PurchaseExpense.*',
        'Expense.name',
        
      )
     ));
     $PurchaseExpense =[];
     foreach ($PurchaseExpenseAll as $key => $value) {
      $SingleExpense['id'] = $value['PurchaseExpense']['id'];
      $SingleExpense['expense_id'] = $value['PurchaseExpense']['expense_id'];
      $SingleExpense['name'] = $value['Expense']['name'];
      $SingleExpense['amount'] = $value['PurchaseExpense']['amount'];
      array_push($PurchaseExpense, $SingleExpense);
    }
    $this->set('PurchaseExpense',$PurchaseExpense);
  }
}
else
{
  $datasource_Purchase = $this->Purchase->getDataSource();
  $datasource_PurchasedItem = $this->PurchasedItem->getDataSource();
  $datasource_Product = $this->Product->getDataSource();
  $datasource_Stock = $this->Stock->getDataSource();
  $datasource_Journal = $this->Journal->getDataSource();
  try {
    $datasource_Purchase->begin();
    $datasource_PurchasedItem->begin();
    $data=$this->request->data;
    if(empty($id))
    {
      $data=$data['Purchase'];
      if(empty($data['discount']))
      {
        $data['discount']=0;
      }
      if(empty($data['discount_amount']))
      {
        $data['discount_amount']=0;
      }
      if($data['tax_type']==0)
      {
        $tax_value=$data['total_tax'];
      }
      else
      {
        $tax_value=$data['total_tax_import'];
      }
      $Purchase_data=[
        'account_head_id'=>$data['account_head_id'],
        'order_no'=>$data['order_no'],
        'invoice_no'=>$data['invoice_no'],
        'date_of_purchase'=>date('Y-m-d',strtotime($data['date'])),
        'total'=>$data['net_total'],
        'other_name'=>$data['other_name'],
        'other_value'=>$data['other_value'],
        'discount'=>$data['discount'],
        'discount_amount'=>$data['discount_amount'],
        'purchase_type'=>$data['purchase_type'],
        'total_tax'=>0,
        'total_tax_amount'=>$data['row_total_tax'],
        'grand_total'=>$data['grand_total'],
        'expense_total'=>$data['expense_total'],
        'balance'=>$data['grand_total'],
        'created_by'=>$user_id,
        'modified_by'=>$user_id,
        'created_at'=>date('Y-m-d H:i:s'),
        'updated_at'=>date('Y-m-d H:i:s'),
      ];
      //pr( $Purchase_data);exit;
      $process=$data['process'];
      if($process=='delivery')
      {
        $Purchase_data['date_of_delivered']=date('Y-m-d',strtotime($data['date']));
        $Purchase_data['status']=2;
      }
      $this->Purchase->create();
      if(!$this->Purchase->save($Purchase_data))
      {
        $errors = $this->Purchase->validationErrors;
        foreach ($errors as $key => $value) {

          throw new Exception($value[0], 1);
        }
      }
      $id=$this->Purchase->getLastInsertId();
      $return_function=$this->purchased_item_insert($data,$id);
      if($return_function['result']!='Success')
      {
        throw new Exception($return_function['result']);
      }
      if(isset($data['expense_id'])) {
        $return_function=$this->purchase_expense_insert($data,$id);
        if($return_function['result']!='Success')
          throw new Exception($return_function['result']);
      }
    }
    else
    {

      $data_Purchase=$data['Purchase'];
      if(isset($data['PurchasedItem']))
      {
        $data_PurchasedItem=$data['PurchasedItem'];  
      }
      if(empty($data_Purchase['discount']))
      {
        $data_Purchase['discount']=0;
      }
      if(empty($data_Purchase['discount_amount']))
      {
        $data_Purchase['discount_amount']=0;
      }
      $Purchase_data=[
        'account_head_id'=>$data_Purchase['account_head_id'],
        'date_of_purchase'=>date('Y-m-d',strtotime($data_Purchase['date'])),
        'order_no'=>$data_Purchase['order_no'],
        'invoice_no'=>$data_Purchase['invoice_no'],
        'total'=>$data_Purchase['net_total'],
        'discount'=>$data_Purchase['discount'],
        'discount_amount'=>$data_Purchase['discount_amount'],
        'purchase_type'=>$data_Purchase['purchase_type'],
        'total_tax'=>0,
        'total_tax_amount'=>$data_Purchase['row_total_tax'],
        'other_name'=>$data_Purchase['other_name'],
        'other_value'=>$data_Purchase['other_value'],
        'grand_total'=>$data_Purchase['grand_total'],
        'expense_total'=>$data_Purchase['expense_total'],
        'balance'=>$data_Purchase['grand_total'],
        'modified_by'=>$user_id,
        'updated_at'=>date('Y-m-d H:i:s'),
      ];
      $process=$data_Purchase['process'];
      if($process=='cancel')
      {
        goto purchase_cancel;
      }
      // if($process=='altration')
      // {
      //   $this->Session->setFlash(__('Altration Activated'));
      //   $this->Purchase->id=$id;
      //   if(!$this->Purchase->saveField('status','3'))
      //     throw new Exception('Error While Updating', 1);
      //   $datasource_Purchase->commit();
      //   $datasource_PurchasedItem->commit();
      //   return $this->redirect(array('controller' => 'Purchase', 'action' => 'Purchase',$id));
      // }
      if($process=='after_delete' || $process=='delete')
      {
        if($process=='after_delete')
        {
          $purchase_rollback_function=$this->purchase_rollback($id);
          if($purchase_rollback_function['result']!='Success')
            throw new Exception($purchase_rollback_function['result']);
        }
        $PurchasedItem=$this->PurchasedItem->find('all',array(
          'conditions'=>array(
            'purchase_id'=>$id
          ),
        ));
        foreach ($PurchasedItem as $key => $value) {
          if(!$this->PurchasedItem->delete($value['PurchasedItem']['id']))
            throw new Exception("Error Processing PurchasedItem deletion", 1);
        }
        if(!$this->Purchase->delete($id))
          throw new Exception("Error Processing Purchase deletion", 1);
        $datasource_Purchase->commit();
        $datasource_PurchasedItem->commit();
        $return['result']='Success';
        $this->Session->setFlash(__($return['result']));
        return $this->redirect(array('controller' => 'Purchase', 'action' => 'Purchase'));
      }
      if($process=='altration')
      {
        $purchase_rollback_function=$this->purchase_edit_rollback($id);
        if($purchase_rollback_function['result']!='Success')
          throw new Exception($purchase_rollback_function['result']);
        if(isset($data_Purchase['product_id']) || isset($data_PurchasedItem['PurchasedItem_id'])) 
        {
        $process='delivery';
        }
        else
        {
          $Journal=$this->Journal->find('all',array(
          'conditions'=>array(
          'remarks'=>'Purchase Invoice No :'.$data_Purchase['invoice_no'],
          ),
          'fields'=>array(
          'Journal.*',
          )
          ));
          foreach ($Journal as $key => $value) {
          if(!$this->Journal->delete($value['Journal']['id']))
          throw new Exception("Error Processing Journal deletion", 1);
          }
        }
      }
      if($process=='delivery')
      {
        $Purchase_data['date_of_delivered']=date('Y-m-d',strtotime($data_Purchase['date']));
        $Purchase_data['status']=2;
      }
      $this->Purchase->id=$id;
      if(!$this->Purchase->save($Purchase_data))
      {
        $errors = $this->Purchase->validationErrors;
        foreach ($errors as $key => $value) {
          throw new Exception($value[0], 1);
        }
      }
      if(isset($data_Purchase['product_id'])) {
        $return_function=$this->purchased_item_insert($data_Purchase,$id);
        if($return_function['result']!='Success')
          throw new Exception($return_function['result']);
      }
      if(isset($data_PurchasedItem['PurchasedItem_id'])) {
        $return_function=$this->purchased_item_update($data_PurchasedItem);
        if($return_function['result']!='Success')
          throw new Exception($return_function['result']);
      }
      //create expense details
      if(isset($data_Purchase['expense_id'])) {
        $return_function=$this->purchase_expense_insert($data_Purchase,$id);
        if($return_function['result']!='Success')
          throw new Exception($return_function['result']);
      }
      purchase_cancel :
      if($process=='cancel')
      {
        $this->Purchase->id=$id;
        if(!$this->Purchase->saveField('flag','0'))
          throw new Exception('Error While Cancelling', 1);
        if(!$this->Purchase->saveField('status','0'))
          throw new Exception('Error While Cancelling', 1);
        if(!$this->Purchase->saveField('date_of_delivered',date('Y-m-d',strtotime($data_Purchase['date']))))
          throw new Exception('Error While Cancelling', 1);
      }
    }
    $AccountingsController = new AccountingsController;
    if($process=='delivery')
    {
      $PurchasedItem = $this->PurchasedItem->find('all',array(
        'conditions' => array(
          'PurchasedItem.purchase_id' => $id,
        ),
        'fields' => array(
          'PurchasedItem.product_id',
          'PurchasedItem.quantity',
          'PurchasedItem.mrp',
          'PurchasedItem.wholesale_price',
          'PurchasedItem.unit_price',
          'PurchasedItem.unit_cost',
          'PurchasedItem.taxable_value',
          'PurchasedItem.net_value',
          'PurchasedItem.tax_amount',
          'PurchasedItem.discount',
          'Purchase.account_head_id',
          'Purchase.invoice_no',
          'Purchase.date_of_delivered',
          'Purchase.total_tax_amount',
          'Purchase.discount_amount',
          'Purchase.grand_total',
          'Purchase.other_value',
          'Purchase.purchase_type',
        )
      ));
      $date=$PurchasedItem[0]['Purchase']['date_of_delivered'];
      $tax_value=0;
      $discount=0;
      $net_value=0;
      $taxable_value=0;
      $datasource_Product->begin();
      $datasource_Stock->begin();
      foreach ($PurchasedItem as $key => $value) {
        $tax_value+=$value['PurchasedItem']['tax_amount'];
        $discount+=$value['PurchasedItem']['discount'];
        $net_value+=$value['PurchasedItem']['net_value'];
        $taxable_value+=$value['PurchasedItem']['taxable_value'];
        $quantity = $value['PurchasedItem']['quantity'];
        $unit_price = $value['PurchasedItem']['unit_price'];
        $unit_cost = $value['PurchasedItem']['unit_cost'];
        if($quantity!=0)
        {
         $unit_discount=$value['PurchasedItem']['discount']/$quantity;
       }
       else
       {
        $unit_discount=0;
      }
      $unit_cost=$unit_cost-$unit_discount;
      $mrp = $value['PurchasedItem']['mrp'];
      $wholesale_price = $value['PurchasedItem']['wholesale_price'];
       //$unit_cost1=$value['PurchasedItem']['discount']
      $product_id=$value['PurchasedItem']['product_id'];
      $Stock = $this->Stock->find('first',array(
        'conditions' => array(
          'Stock.product_id' => $product_id,
          'Stock.warehouse_id' => 1,
        ),));
      $getStock = $this->Stock->find('all', array(
        'conditions'=>['Stock.product_id'=>$product_id],
      ));
      $cost_in_stock=$Stock['Product']['cost'];
      $quantity1 = $value['PurchasedItem']['quantity']/$Stock['Product']['no_of_piece_per_unit'];
      
        // $Stock = $this->Stock->findByProductId($product_id);
     $stock_id=$Stock['Stock']['id'];
     $stock_quantity=$Stock['Stock']['quantity'];
     $StockController = new StockController;
     $remark='Purchase --'.$value['Purchase']['invoice_no'].'('.$quantity.')';
     $stock_quantity+=$quantity;
     $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
     if($Stock_function_return['result']!='Success')
      throw new Exception($Stock_function_return['result']);
    $this->Product->id=$product_id;
       // if(!$this->Product->saveField('mrp',$mrp))
        //  throw new Exception("Error In Product Updation", 37);
        //if(!$this->Product->saveField('wholesale_price',$wholesale_price))
         // throw new Exception("Error In Product Updation", 37);
  $Purchased_Item_average=$this->PurchasedItem->find('all',array(
    'conditions'=>array(
      'PurchasedItem.product_id'=>$product_id,
      ),
    'fields'=>array(
      'PurchasedItem.unit_cost',
      'PurchasedItem.quantity',
      )
    ));
  $all_cost=0;$all_quantity=0;
  foreach ($Purchased_Item_average as $keyA => $valueAverage) {
    $all_cost+=($valueAverage['PurchasedItem']['unit_cost']*$valueAverage['PurchasedItem']['quantity']);
    $all_quantity+=$valueAverage['PurchasedItem']['quantity'];
  }
  $average_cost=$all_cost/$all_quantity;
    if(!$this->Product->saveField('cost',round($average_cost,3)))
      throw new Exception("Error In Product Updation", 38);
  }
  $datasource_Journal->begin();
  $account_head_id=$PurchasedItem[0]['Purchase']['account_head_id'];
  $debit=$purchase_account_head_id;
  $credit=$account_head_id;
  $amount=$PurchasedItem['0']['Purchase']['grand_total'];
   $receipt=0;
        if($PurchasedItem['0']['Purchase']['purchase_type']=='CashPurchase')
        {
        $receipt=$amount;
        }
  $roundoff=$PurchasedItem['0']['Purchase']['other_value'];
  $Party=$this->Party->findByAccountHeadId($account_head_id);
  $state_id=$Party['Party']['state_id'];
  $purchase_amount=$net_value;
  $work_flow='Purchase';
  $invoice_no=$PurchasedItem['0']['Purchase']['invoice_no'];
  $date=$PurchasedItem['0']['Purchase']['date_of_delivered'];
  $remarks='Purchase Invoice No :'.$invoice_no;
  $AccountingsController = new AccountingsController;
  $voucher_no=$AccountingsController->GetVoucherNo();
  $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$purchase_amount,$date,$remarks,$work_flow,$user_id,$voucher_no);
  if($Accountings_function_return['result']!='Success')
    throw new Exception($Stock_function_return['message']);
  if($purchase_amount)
  {
    $debit=$stock_account_head_id;
    $credit=$purchase_account_head_id;
    $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$purchase_amount,$date,$remarks,$work_flow,$user_id,$voucher_no);
    if($Accountings_function_return['result']!='Success')
      throw new Exception($Accountings_function_return['message']);
  }
  $tax_value=$PurchasedItem[0]['Purchase']['total_tax_amount'];
  if($tax_value)
  {
    $credit=$account_head_id;
    $debit=$tax_on_purchase_account_head_id;
    $AccountingsController = new AccountingsController;
    $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$tax_value,$date,$remarks,$work_flow,$user_id,$voucher_no);
    if($Accountings_function_return['result']!='Success')
      throw new Exception($Stock_function_return['message']);   
  }
  $discount=$PurchasedItem[0]['Purchase']['discount_amount'];
  if($discount)
  {
    if($discount>0)
    {
      $debit=$account_head_id;
      $credit=$discount_received_account_head_id;
    }
    else
    {
      $discount=$discount*-1;
      $credit=$account_head_id;
      $debit=$discount_paid_account_head_id;
    }
    $AccountingsController = new AccountingsController;
    $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$discount,$date,$remarks,$work_flow,$user_id,$voucher_no);
    if($Accountings_function_return['result']!='Success')
      throw new Exception($Stock_function_return['message']);
  }
  if($roundoff)
  {
    if($roundoff>0)
    {
      $debit=$roundoff_paid_account_head_id;
      $credit=$account_head_id;
    }
    else
    {
      $roundoff=$roundoff*-1;
      $debit=$account_head_id;
      $credit=$roundoff_received_account_head_id;
    }
    $AccountingsController = new AccountingsController;
    $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$roundoff,$date,$remarks,$work_flow,$user_id,$voucher_no);
    if($Accountings_function_return['result']!='Success')
      throw new Exception($Accountings_function_return['message']);
  }
  if(floatval($receipt))
          {
            $credit=1;
            $debit=$account_head_id;
            $voucher_no='';
            $AccountingsController = new AccountingsController;
    $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$receipt,$date,$remarks,$work_flow,$user_id,$voucher_no);
            if($Accountings_function_return['result']!='Success')
              throw new Exception($Stock_function_return['message']);
          }
  $datasource_Product->commit();
  $datasource_Stock->commit();
  $datasource_Journal->commit();
}
$datasource_Purchase->commit();
$datasource_PurchasedItem->commit();
if(empty($id))
{
  $return['result']='Success';
  $this->Session->setFlash(__($return['result']));
  return $this->redirect(array('controller' => 'Purchase', 'action' => 'Purchase',$purchase_id));
}
else
{
  $return['result']='Updated';
  $this->Session->setFlash(__($return['result']));
  return $this->redirect(array('controller' => 'Purchase', 'action' => 'Purchase',$id)); 
}
} catch (Exception $e) {
  if(isset($process))
  {
    if($process=='delivery')
    {
      $datasource_Product->rollback();
      $datasource_Stock->rollback();
      $datasource_Journal->rollback();
    }
  }
  $datasource_PurchasedItem->rollback();
  $datasource_Purchase->rollback();
  $return['result']=$e->getMessage();
  // pr($e); exit;
}
$this->Session->setFlash(__($return['result']));
return $this->redirect(array('controller' => 'Purchase', 'action' => 'Purchase'));
}
}
public function purchase_expense_insert($purchase_expenses,$purchase_id)
{
  try {
    if($purchase_expenses)
    {
      for ($i=0; $i <count($purchase_expenses['expense_id']) ; $i++) {
        $Expense_data=[
          'expense_id'=>$purchase_expenses['expense_id'][$i],
          'purchase_id'=>$purchase_id,
          'amount'=>$purchase_expenses['expense_amount'][$i],
        ];
        $this->PurchaseExpense->create();
        if(!$this->PurchaseExpense->save($Expense_data))
        {
          $errors = $this->PurchaseExpense->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
      }
    }
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  return $return;
}
public function purchase_expense_update($purchase_expenses)
{
  try {
    if($purchase_expenses)
    {
      for ($i=0; $i <count($purchase_expenses['PurchaseExpense_id']) ; $i++) {
        $PurchaseExpense_old_data=[
         'expense_id'=>$purchase_expenses['product_id'][$i],
         'amount'=>$purchase_expenses['expense_amount'][$i],
       ];
       $this->PurchaseExpense->id=$purchased_items['PurchaseExpense_id'][$i];
       if(!$this->PurchaseExpense->save($PurchaseExpense_old_data))
       {
        $errors = $this->PurchaseExpense->validationErrors;
        foreach ($errors as $key => $value) {
          throw new Exception($value[0], 1);
        }
      }
    }
  }
  $return['result']='Success';
} catch (Exception $e) {
  $return['result']=$e->getMessage();
}
return $return;
}
public function purchased_item_update($purchased_items)
{
  try {
    if($purchased_items)
    {
      for ($i=0; $i <count($purchased_items['PurchasedItem_id']) ; $i++) {
        $unit_price=$purchased_items['unit_price'][$i];
        $unit_cost=$purchased_items['unit_cost'][$i];
        $wholesale_price=$purchased_items['wholesale_price'][$i];
        $mrp=$purchased_items['mrp'][$i];
        $quantity=$purchased_items['quantity'][$i];
        $SaleController = new SaleController;
        $UnitLevelConvert=$SaleController->UnitLevelConvert($purchased_items['product_id'][$i],$purchased_items['unit_id'][$i]);
        $sale_unit_level=$UnitLevelConvert['sale_unit_level'];
      //$no_of_box_per_carton=$UnitLevelConvert['no_of_box_per_carton'];
        $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
        $product_unit_level=$UnitLevelConvert['product_unit_level'];
        if($sale_unit_level == 2){
          $quantity=$no_of_piece_per_unit*$quantity;
          $mrp=$mrp/$no_of_piece_per_unit;
          $unit_price=$unit_price/$no_of_piece_per_unit;
          $unit_cost=$unit_cost/$no_of_piece_per_unit;
          $wholesale_price=$wholesale_price/$no_of_piece_per_unit;
        }
        $net_value=$unit_price*$quantity;
        //$discount=$purchased_items['discount'][$i];
        $discount=0;
        $taxable_value=$net_value-$discount;
        $tax=$purchased_items['tax'][$i];
       // $tax=0;
        $tax_amount=$taxable_value*$tax/100;
        $PurchasedItem_old_data=[
          'unit_price'=>$unit_price,
          'unit_cost'=>$unit_cost,
          'wholesale_price'=>$wholesale_price,
          'mrp'=>$mrp,
          'quantity'=>$quantity,
          'net_value'=>$net_value,
          'discount'=>0,
          'tax'=>$tax,
          'tax_amount'=>$tax_amount,
          'total'=>$purchased_items['row_total'][$i],
        ];
        $this->PurchasedItem->id=$purchased_items['PurchasedItem_id'][$i];
        if(!$this->PurchasedItem->save($PurchasedItem_old_data))
        {
          $errors = $this->PurchasedItem->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
      }
    }
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  return $return;
}
public function purchased_item_insert($purchased_items,$purchase_id)
{
  try {
    if($purchased_items)
    {
      for ($i=0; $i <count($purchased_items['product_id']) ; $i++) {
        //pr($purchased_items);pr($purchase_id);
        $unit_price=$purchased_items['unit_price'][$i];
        $unit_cost=$purchased_items['unit_cost'][$i];
        $Product_details=$this->Product->findById($purchased_items['product_id'][$i]);
        $wholesale_price=$Product_details['Product']['wholesale_price'];
        $mrp=$Product_details['Product']['mrp'];
        // $wholesale_price=$purchased_items['wholesale_price'][$i];
        // $mrp=$purchased_items['mrp'][$i];
        $quantity=$purchased_items['quantity'][$i];
        $SaleController = new SaleController;
        $UnitLevelConvert=$SaleController->UnitLevelConvert($purchased_items['product_id'][$i],$purchased_items['unit_id'][$i]);
        $sale_unit_level=$UnitLevelConvert['sale_unit_level'];
        $no_of_piece_per_unit=1;
        if(!empty($UnitLevelConvert['no_of_piece_per_unit']))
        {
          $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
        }
        
        $product_unit_level=$UnitLevelConvert['product_unit_level'];
        if($sale_unit_level == 2){
          $quantity=$no_of_piece_per_unit*$quantity;
          $mrp=$mrp/$no_of_piece_per_unit;
          $unit_price=$unit_price/$no_of_piece_per_unit;
          $unit_cost=$unit_cost/$no_of_piece_per_unit;
          $wholesale_price=$wholesale_price/$no_of_piece_per_unit;
        }
        $net_value=$unit_price*$quantity;
        //$discount=$purchased_items['discount'][$i];
        $discount=0;
        $taxable_value=$net_value-$discount;
        $tax=$purchased_items['tax'][$i];
       // $tax=0;
        $tax_amount=$taxable_value*$tax/100;
        $row_total=$taxable_value+$tax_amount;
        if($purchased_items['Brand_id'][$i]=='G')
        {
          $purchased_items['Brand_id'][$i]=0;
        }
        $PurchasedItem_data=[
          'brand_id'=>$purchased_items['Brand_id'][$i],
          'product_id'=>$purchased_items['product_id'][$i],
          'purchase_id'=>$purchase_id,
          'unit_price'=>$unit_price,
          'unit_id'=>$purchased_items['unit_id'][$i],
          'unit_cost'=>$unit_cost,
          'wholesale_price'=>$wholesale_price,
          'mrp'=>$mrp,
          'quantity_mode'=>$purchased_items['unit_id'][$i],
          'quantity'=>$quantity,
          'net_value'=>$net_value,
          'discount'=>$discount,
          'tax'=>$tax,
          'tax_amount'=>$purchased_items['tax_amount'][$i],
          'total'=>$purchased_items['row_total'][$i],
        ];
       // pr($PurchasedItem_data);exit;
        $this->PurchasedItem->create();
        if(!$this->PurchasedItem->save($PurchasedItem_data))
        {
          $errors = $this->PurchasedItem->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
      }
    }
    $return['result']='Success';
  } catch (Exception $e) {
    //pr($e);exit;
    $return['result']=$e->getMessage();
    //pr($return['result']); exit;
  }
  return $return;
}
public function purchase_rollback($id)
{
  $user_id=1;
  $datasource_Purchase = $this->Purchase->getDataSource();
  $datasource_Stock = $this->Stock->getDataSource();
  $datasource_Journal = $this->Journal->getDataSource();
  try {
    $datasource_Purchase->begin();
    $datasource_Stock->begin();
    $datasource_Journal->begin();
    $Purchase=$this->Purchase->findById($id);
    $invoice_no=$Purchase['Purchase']['invoice_no'];
    if(!$Purchase)
      throw new Exception("Empty Purchases", 1);
    $this->Purchase->id=$id;
    if(!$this->Purchase->saveField('status','1'))
      throw new Exception('Error While Updating', 1);
    foreach ($Purchase['PurchasedItem'] as $key => $value) {
      $quantity = $value['quantity'];
      $product_id=$value['product_id'];
      $unit_cost = $value['unit_cost'];
      if($quantity!=0)
      {
        $unit_discount=$value['discount']/$quantity;
      }
      else
      {
        $unit_discount=0;
      }
      $unit_cost=$unit_cost-$unit_discount;
      $Stock = $this->Stock->findByProductId($product_id);
      if(!$Stock)
        throw new Exception("Empty Stock", 1);
      $getStock = $this->Stock->find('all', array(
        'conditions'=>['Stock.product_id'=>$product_id],
      ));
      $whole_stock=0;
      foreach ($getStock as $key_stock => $value_stock) {
        $whole_stock+=$value_stock['Stock']['quantity'];
      }
      $stock_id=$Stock['Stock']['id'];
      $stock_quantity=$Stock['Stock']['quantity'];
      $cost_in_stock=$Stock['Product']['cost'];
      $date=date('Y-m-d');
      $remark='Change Purchase Order --'.$id.'('.$quantity.')';
      $stock_quantity-=$quantity;
      $StockController = new StockController;
      $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
      if($Stock_function_return['result']!='Success')
        throw new Exception($Stock_function_return['result']);
     // $this->Product->id=$product_id;
    //   if($quantity!=0)
    //   {
    //    $average_cost=number_format(((($cost_in_stock*$whole_stock)-($unit_cost*$quantity))/($whole_stock-$quantity)),3,'.','');
    //  }
    //  else
    //  {
    //   $average_cost=0;
    // }
    // if(!$this->Product->saveField('cost',$average_cost))
    //   throw new Exception("Error In Product Updation", 38);
  }
  $Journal=$this->Journal->find('all',array(
    'conditions'=>array(
      'remarks'=>'Purchase Invoice No :'.$invoice_no,
    ),
    'fields'=>array(
      'Journal.*',
    )
  ));
  foreach ($Journal as $key => $value) {
    if(!$this->Journal->delete($value['Journal']['id']))
      throw new Exception("Error Processing Journal deletion", 1);
  }
  $datasource_Purchase->commit();
  $datasource_Stock->commit();
  $datasource_Journal->commit();
  $return['result']='Success';
} catch (Exception $e) {
  $return['result']=$e->getMessage();
  $datasource_Purchase->rollback();
  $datasource_Stock->rollback();
  $datasource_Journal->rollback();
}
return $return;
}
public function purchase_edit_rollback($id)
{
  $user_id=1;
  $datasource_Purchase = $this->Purchase->getDataSource();
  $datasource_Stock = $this->Stock->getDataSource();
  $datasource_Journal = $this->Journal->getDataSource();
  try {
    $datasource_Purchase->begin();
    $datasource_Stock->begin();
    $datasource_Journal->begin();
    $Purchase=$this->Purchase->findById($id);
    $invoice_no=$Purchase['Purchase']['invoice_no'];
    if(!$Purchase)
      throw new Exception("Empty Purchases", 1);
    $this->Purchase->id=$id;
    if(!$this->Purchase->saveField('status','1'))
      throw new Exception('Error While Updating', 1);
    foreach ($Purchase['PurchasedItem'] as $key => $value) {
      $quantity = $value['quantity'];
      $product_id=$value['product_id'];
      $unit_cost = $value['unit_cost'];
      if($quantity!=0)
      {
        $unit_discount=$value['discount']/$quantity;
      }
      else
      {
        $unit_discount=0;
      }
      $unit_cost=$unit_cost-$unit_discount;
      $Stock = $this->Stock->findByProductId($product_id);
      if(!$Stock)
        throw new Exception("Empty Stock", 1);
      $getStock = $this->Stock->find('all', array(
        'conditions'=>['Stock.product_id'=>$product_id],
      ));
      $whole_stock=0;
      foreach ($getStock as $key_stock => $value_stock) {
        $whole_stock+=$value_stock['Stock']['quantity'];
      }
      $stock_id=$Stock['Stock']['id'];
      $stock_quantity=$Stock['Stock']['quantity'];
      $cost_in_stock=$Stock['Product']['cost'];
      $date=date('Y-m-d');
      $remark='Purchase --'.$invoice_no.'('.$quantity.')';
      $stocklog=$this->StockLog->findByProductIdAndRemark($product_id,$remark);
      if($stocklog)
      {
         if(!$this->StockLog->delete($stocklog['StockLog']['id']))
            throw new Exception("Error Processing stockLog deletion", 1);
      }
      $stock_quantity-=$quantity;
      $StockController = new StockController;
      $Stock_function_return=$StockController->GeneralStock_PurchaseEdit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
      if($Stock_function_return['result']!='Success')
        throw new Exception($Stock_function_return['result']);
  }
  $Journal=$this->Journal->find('all',array(
    'conditions'=>array(
      'remarks'=>'Purchase Invoice No :'.$invoice_no,
    ),
    'fields'=>array(
      'Journal.*',
    )
  ));
  foreach ($Journal as $key => $value) {
    if(!$this->Journal->delete($value['Journal']['id']))
      throw new Exception("Error Processing Journal deletion", 1);
  }
  $datasource_Purchase->commit();
  $datasource_Stock->commit();
  $datasource_Journal->commit();
  $return['result']='Success';
} catch (Exception $e) {
  $return['result']=$e->getMessage();
  $datasource_Purchase->rollback();
  $datasource_Stock->rollback();
  $datasource_Journal->rollback();
}
return $return;
}
public function purchase_return_rollback($id)
{
  $user_id=1;
  $datasource_PurchaseReturn = $this->PurchaseReturn->getDataSource();
  $datasource_Stock = $this->Stock->getDataSource();
  $datasource_Journal = $this->Journal->getDataSource();
  try {
    $datasource_PurchaseReturn->begin();
    $datasource_Stock->begin();
    $datasource_Journal->begin();
    $PurchaseReturn=$this->PurchaseReturn->findById($id);
    $invoice_no=$PurchaseReturn['PurchaseReturn']['invoice_no'];
    if(!$PurchaseReturn)
      throw new Exception("Empty PurchaseReturns", 1);
    $this->PurchaseReturn->id=$id;
    if(!$this->PurchaseReturn->saveField('status','1'))
      throw new Exception('Error While Updating', 1);
    foreach ($PurchaseReturn['PurchaseReturnItem'] as $key => $value) {
      //$quantity = $value['quantity'];
      $product_id=$this->Product->findById($value['product_id']);
      if($value['quantity_mode']==2)
      {
        $quantity = $value['quantity']*$product_id['Product']['no_of_piece_per_unit']; 
      }
      else
      {
        $quantity = $value['quantity'];

      }
      $unit_price = $value['unit_price'];
      $product_id=$value['product_id'];
      $Stock = $this->Stock->findByProductId($product_id);
      if(!$Stock)
        throw new Exception("Empty Stock", 1);
      $getStock = $this->Stock->find('all', array(
        'conditions'=>['Stock.product_id'=>$product_id],
      ));
      $cost_in_stock=$Stock['Product']['cost'];
          //$quantity1 = $value['PurchaseReturnItem']['quantity']/$Stock['Product']['no_of_piece_per_unit'];
      $whole_stock=0;
      foreach ($getStock as $key_stock => $value_stock) {
       $whole_stock+=$value_stock['Stock']['quantity'];
     }
     $stock_id=$Stock['Stock']['id'];
     $stock_quantity=$Stock['Stock']['quantity'];
     $date=date('Y-m-d');
     $remark='Change PurchaseReturn Order --'.$id.'('.$quantity.')';
     $stock_quantity+=$quantity;
     $StockController = new StockController;
     $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
     if($Stock_function_return['result']!='Success')
      throw new Exception($Stock_function_return['result']);

    $this->Product->id=$product_id;
       // if(!$this->Product->saveField('mrp',$mrp))
        //  throw new Exception("Error In Product Updation", 37);
        //if(!$this->Product->saveField('wholesale_price',$wholesale_price))
         // throw new Exception("Error In Product Updation", 37);
    if($quantity!=0)
    {
      $average_cost=number_format(((($cost_in_stock*$whole_stock)+($unit_price*$quantity))/($whole_stock+$quantity)),3,'.','');
    }
    else
    {
      $average_cost=0;
    }
    if(!$this->Product->saveField('cost',$average_cost))
      throw new Exception("Error In Product Updation", 38);
  }
  $Journal=$this->Journal->find('all',array(
    'conditions'=>array(
      'remarks'=>'PurchaseReturn Invoice No :'.$invoice_no,
    ),
    'fields'=>array(
      'Journal.*',
    )
  ));
  foreach ($Journal as $key => $value) {
    if(!$this->Journal->delete($value['Journal']['id']))
      throw new Exception("Error Processing Journal deletion", 1);
  }
  $datasource_PurchaseReturn->commit();
  $datasource_Stock->commit();
  $datasource_Journal->commit();
  $return['result']='Success';
} catch (Exception $e) {
  $return['result']=$e->getMessage();
  $datasource_PurchaseReturn->rollback();
  $datasource_Stock->rollback();
  $datasource_Journal->rollback();
}
return $return;
}
public function AssetPurchase($id=null)
{
  $PermissionList = $this->Session->read('PermissionList');
  // $this->Menu->action='Purchase/AssetPurchase';
  //   $menu_id=$this->Menu->field('Menu.id');
  $menu_id = $this->Menu->field(
    'Menu.id',
    array('action ' => 'Purchase/AssetPurchase'));
  // if(!in_array($menu_id, $PermissionList))
  // {
  //   $this->Session->setFlash("Permission denied");
  //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
  // }
  $sub_group_id=16;//vendor
  $discount_received_account_head_id=12;
  $discount_paid_account_head_id=13;
  $roundoff_received_account_head_id=14;
  $roundoff_paid_account_head_id=15;
  $group_id=7;//fixed assets
  $user_id=1;
  $AccountingsControllerVendorList = new AccountingsController;
  $Vendor_list=$AccountingsControllerVendorList->AccountHead_Option_ListBySubGroupId($sub_group_id);
  $this->set('Vendor_list',$Vendor_list);
  $AccountingsControllerFixedAssetList = new AccountingsController;
  $Fixed_Asset_list=$AccountingsControllerFixedAssetList->AccountHead_Option_ListByGroupId($group_id);
  $this->set('Fixed_Asset_list',$Fixed_Asset_list);
  $State_list=$this->State->find('list',array('fields'=>array('id','name')));
  $this->set(compact('State_list'));
  $AssetPurchase=$this->AssetPurchase->find('count');
  if(!empty($AssetPurchase))
  {
    $invoice_no=$AssetPurchase+1;
    $order_no=$AssetPurchase+1;
  }
  else
  {
    $invoice_no=1;
    $order_no=1;
  }
  $data['Vendor']['opening_balance']='0';
  if (!$this->request->data) 
  {
    if(empty($id))
    {
      $data['AssetPurchase']['invoice_no']=$invoice_no;
      $data['AssetPurchase']['order_no']=$order_no;
      $data['AssetPurchase']['other_value']='0';
      $data['AssetPurchase']['date']=date('d-m-Y');
      $data['AssetPurchase']['status']=1;
      $this->request->data=$data;
    }
    else
    {
      $AssetPurchase=$this->AssetPurchase->findById($id);
      $Vendor=$this->Vendor->findByAccountHeadId($AssetPurchase['AssetPurchase']['account_head_id']);     
      $this->request->data=$AssetPurchase;
      $this->request->data['AssetPurchase']['date']=date('d-m-Y',strtotime($AssetPurchase['AssetPurchase']['date_of_purchase']));
      $this->request->data['AssetPurchase']['address']=$Vendor['Vendor']['place'];
      $AssetPurchasedItem=$this->AssetPurchasedItem->find('all',array(
       'conditions'=>array(
        'asset_purchase_id'=>$id,
      ),
       'fields'=>array(
        'AssetPurchasedItem.*',
        'AccountHead.name',
      )
     ));
      $this->set('AssetPurchasedItem',$AssetPurchasedItem);
    }
  }
  else
  {
    $datasource_AssetPurchase = $this->AssetPurchase->getDataSource();
    $datasource_AssetPurchasedItem = $this->AssetPurchasedItem->getDataSource();
    $datasource_FixedStock = $this->FixedStock->getDataSource();
    $datasource_Journal = $this->Journal->getDataSource();
    try {
      $datasource_AssetPurchase->begin();
      $datasource_AssetPurchasedItem->begin();
      $data=$this->request->data;
      if(empty($id))
      {
        $data=$data['AssetPurchase'];
        $AssetPurchase_data=[
          'account_head_id'=>$data['account_head_id'],
          'order_no'=>$data['order_no'],
          'invoice_no'=>$data['invoice_no'],
          'date_of_purchase'=>date('Y-m-d',strtotime($data['date'])),
          'total'=>$data['total'],
          'other_name'=>$data['other_name'],
          'other_value'=>$data['other_value'],
          'grand_total'=>$data['grand_total'],
          'balance'=>$data['grand_total'],
          'created_by'=>$user_id,
          'modified_by'=>$user_id,
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s'),
        ];
        $this->AssetPurchase->create();
        if(!$this->AssetPurchase->save($AssetPurchase_data))
        {
          $errors = $this->AssetPurchase->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
        $AssetPurchase_id=$this->AssetPurchase->getLastInsertId();
        $return_function=$this->asset_purchased_item_insert($data,$AssetPurchase_id);
        if($return_function['result']!='Success')
          throw new Exception($return_function['result']);
      }
      else
      {
        $data_AssetPurchase=$data['AssetPurchase'];
        $data_AssetPurchasedItem=$data['AssetPurchasedItem'];
        $AssetPurchase_data=[
          'account_head_id'=>$data_AssetPurchase['account_head_id'],
          'order_no'=>$data_AssetPurchase['order_no'],
          'invoice_no'=>$data_AssetPurchase['invoice_no'],
          'total'=>$data_AssetPurchase['total'],
          'other_name'=>$data_AssetPurchase['other_name'],
          'other_value'=>$data_AssetPurchase['other_value'],
          'grand_total'=>$data_AssetPurchase['grand_total'],
          'balance'=>$data_AssetPurchase['grand_total'],
          'modified_by'=>$user_id,
          'updated_at'=>date('Y-m-d H:i:s'),
        ];
        $process=$data_AssetPurchase['process'];
        if($process!='cancel')
        {
          if($process=='delivery')
          {
            $AssetPurchase_data['date_of_delivered']=date('Y-m-d',strtotime($data_AssetPurchase['date']));
            $AssetPurchase_data['status']=2;
          }
          $this->AssetPurchase->id=$id;
          if(!$this->AssetPurchase->save($AssetPurchase_data))
          {
            $errors = $this->AssetPurchase->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
          //create AssetPurchased items
          if(isset($data_AssetPurchase['Asset_id'])) {
            $return_function=$this->asset_purchased_item_insert($data_AssetPurchase,$id);
            if($return_function['result']!='Success')
              throw new Exception($return_function['result']);
          }
          // update AssetPurchased items
          $return_function=$this->asset_purchased_item_update($data_AssetPurchasedItem);
          if($return_function['result']!='Success')
            throw new Exception($return_function['result']);
          if($process=='delivery')
          {
            $AssetPurchasedItem = $this->AssetPurchasedItem->find('all',array(
              'conditions' => array(
                'AssetPurchasedItem.asset_purchase_id' => $id,
              ),
              'fields' => array(
                'AssetPurchasedItem.account_head_id',
                'AssetPurchasedItem.total',
                'AssetPurchasedItem.quantity',
                'AssetPurchasedItem.net_value',
                'AssetPurchasedItem.discount',
                'AssetPurchasedItem.cgst_amount',
                'AssetPurchasedItem.sgst_amount',
                'AssetPurchasedItem.igst_amount',
                'AssetPurchase.account_head_id',
                'AssetPurchase.date_of_delivered',
                'AssetPurchase.invoice_no',
                'AssetPurchase.other_value',
              )
            ));
            $net_value=0;
            $discount=0;
            $cgst=0;
            $sgst=0;
            $igst=0;
            $datasource_FixedStock->begin();
            $datasource_Journal->begin();
            $credit=$AssetPurchasedItem['0']['AssetPurchase']['account_head_id'];
            $invoice_no=$AssetPurchasedItem['0']['AssetPurchase']['invoice_no'];
            $roundoff=$AssetPurchasedItem['0']['AssetPurchase']['other_value'];
            $work_flow='Asset Purchase';
            $remarks='AssetPurchase Invoice No :'.$invoice_no;
            $date=$AssetPurchasedItem['0']['AssetPurchase']['date_of_delivered'];
            foreach ($AssetPurchasedItem as $key => $value) {
              $net_value+=$value['AssetPurchasedItem']['net_value'];
              $discount+=$value['AssetPurchasedItem']['discount'];
              $cgst+=$value['AssetPurchasedItem']['cgst_amount'];
              $sgst+=$value['AssetPurchasedItem']['sgst_amount'];
              $igst+=$value['AssetPurchasedItem']['igst_amount'];
              $amount=$value['AssetPurchasedItem']['total'];
              $quantity = $value['AssetPurchasedItem']['quantity'];
              $account_head_id=$value['AssetPurchasedItem']['account_head_id'];
              $FixedStock = $this->FixedStock->findByAccountHeadId($account_head_id);
              $StockController = new StockController;
              if($FixedStock)
              {
                $FixedStock_id=$FixedStock['FixedStock']['id'];
                $FixedStock_quantity=$FixedStock['FixedStock']['quantity'];
                $remark='AssetPurchase --'.$invoice_no.'('.$quantity.')';
                $FixedStock_quantity+=$quantity;
                $FixedStock_function_return=$StockController->General_FixedStock_Edit_Function($FixedStock_id,$FixedStock_quantity,$date,$user_id,$remark);  
              }
              else
              {
                $remark='AssetPurchase --'.$invoice_no.'('.$quantity.')';
                $FixedStock_function_return=$StockController->General_FixedStock_Add_Function($account_head_id,$quantity,$date,$user_id,$remark,$flag=1);  
              }
              if($FixedStock_function_return['result']!='Success')
                throw new Exception($FixedStock_function_return['result']);
              $debit=$account_head_id;
              $AccountingsController = new AccountingsController;
              $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$net_value,$date,$remarks,$work_flow,$user_id);
              if($Accountings_function_return['result']!='Success')
                throw new Exception($Stock_function_return['message']);
              {
                if($discount>0)
                {
                  $debit=$account_head_id;
                  $credit=$discount_received_account_head_id;                    
                }
                else
                {
                  $discount=$discount*-1;
                  $debit=$discount_paid_account_head_id;
                  $credit=$account_head_id;
                }
                $AccountingsController = new AccountingsController;
                $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$discount,$date,$remarks,$work_flow,$user_id);
                if($Accountings_function_return['result']!='Success')
                  throw new Exception($Stock_function_return['message']);
              }
            }
            {
              if($roundoff>0)
              {
                $debit=$roundoff_paid_account_head_id;
                $credit=$AssetPurchasedItem['0']['AssetPurchase']['account_head_id'];
              }
              else
              {
                $roundoff=$roundoff*-1;
                $debit=$AssetPurchasedItem['0']['AssetPurchase']['account_head_id'];
                $credit=$roundoff_received_account_head_id;
              }
              $AccountingsController = new AccountingsController;
              $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$roundoff,$date,$remarks,$work_flow,$user_id);
              if($Accountings_function_return['result']!='Success')
                throw new Exception($Stock_function_return['message']);
            }
            $datasource_Journal->commit();
            $datasource_FixedStock->commit();
          }
        }
        else
        {
          $this->AssetPurchase->id=$id;
          if(!$this->AssetPurchase->saveField('flag','0'))
            throw new Exception('Error While Cancelling', 1);
          if(!$this->AssetPurchase->saveField('status','0'))
            throw new Exception('Error While Cancelling', 1);
          if(!$this->AssetPurchase->saveField('date_of_delivered',date('Y-m-d',strtotime($data_Purchase['date']))))
            throw new Exception('Error While Cancelling', 1);
        }
      }
      $datasource_AssetPurchase->commit();
      $datasource_AssetPurchasedItem->commit();
      if(empty($id))
      {
        $return['result']='Success';
        $this->Session->setFlash(__($return['result']));
        return $this->redirect(array('controller' => 'Purchase', 'action' => 'AssetPurchase',$AssetPurchase_id));
      }
      else
      {
        $return['result']='Updated';
        $this->Session->setFlash(__($return['result']));
        return $this->redirect(array('controller' => 'Purchase', 'action' => 'AssetPurchase',$id)); 
      }
    } catch (Exception $e) {
      if(isset($process))
      {
        if($process=='delivery')
        {
          $datasource_FixedStock->rollback();
        }
      }
      $datasource_AssetPurchasedItem->rollback();
      $datasource_AssetPurchase->rollback();
      $return['result']=$e->getMessage();
    }
    $this->Session->setFlash(__($return['result']));
    $this->redirect( Router::url( $this->referer(), true ) );
  }
}
public function asset_purchased_item_insert($purchased_items,$asset_purchase_id)
{
  try {
    if($purchased_items)
    {
      for ($i=0; $i <count($purchased_items['Asset_id']) ; $i++) {
        $AssetPurchasedItem_data=[
          'account_head_id'=>$purchased_items['Asset_id'][$i],
          'asset_purchase_id'=>$asset_purchase_id,
          'price'=>$purchased_items['price'][$i],
          'quantity'=>$purchased_items['quantity'][$i],
          'net_value'=>$purchased_items['net_value'][$i],
          'discount'=>$purchased_items['discount'][$i],
          'taxable_value'=>$purchased_items['taxable_value'][$i],
          'cgst'=>$purchased_items['cgst'][$i],
          'cgst_amount'=>$purchased_items['cgst_amount'][$i],
          'sgst'=>$purchased_items['sgst'][$i],
          'sgst_amount'=>$purchased_items['sgst_amount'][$i],
          'igst'=>$purchased_items['igst'][$i],
          'igst_amount'=>$purchased_items['igst_amount'][$i],
          'total'=>$purchased_items['row_total'][$i],
        ];
        $this->AssetPurchasedItem->create();
        if(!$this->AssetPurchasedItem->save($AssetPurchasedItem_data))
        {
          $errors = $this->AssetPurchasedItem->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
      }
    }
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  return $return;
}
public function asset_purchased_item_update($purchased_items)
{
  try {
    if($purchased_items)
    {
      for ($i=0; $i <count($purchased_items['AssetPurchasedItem_id']) ; $i++) {
        $AssetPurchasedItem_old_data=[
          'price'=>$purchased_items['price'][$i],
          'quantity'=>$purchased_items['quantity'][$i],
          'net_value'=>$purchased_items['net_value'][$i],
          'discount'=>$purchased_items['discount'][$i],
          'taxable_value'=>$purchased_items['taxable_value'][$i],
          'cgst'=>$purchased_items['cgst'][$i],
          'cgst_amount'=>$purchased_items['cgst_amount'][$i],
          'sgst'=>$purchased_items['sgst'][$i],
          'sgst_amount'=>$purchased_items['sgst_amount'][$i],
          'igst'=>$purchased_items['igst'][$i],
          'igst_amount'=>$purchased_items['igst_amount'][$i],
          'total'=>$purchased_items['row_total'][$i],
        ];
        $this->AssetPurchasedItem->id=$purchased_items['AssetPurchasedItem_id'][$i];
        if(!$this->AssetPurchasedItem->save($AssetPurchasedItem_old_data))
        {
          $errors = $this->AssetPurchasedItem->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
      }
    }
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  return $return;
}
public function get_party_address($id)
{
  $return['result']='Error';
  $return['address']='';
  $Party=$this->Party->findByAccountHeadId($id);
  if($Party)
  {
    $return['result']='Success';
    $return['address']=$Party['Party']['place']; 
    $return['state_id']=$Party['Party']['state_id']; 
  }
  echo json_encode($return);
  exit;
}
public function get_fixed_stock_quantity($id)
{
  $return['result']='Error';
  $return['quantity']='0';
  $FixedStock=$this->FixedStock->findByAccountHeadId($id);
  if($FixedStock)
  {
    $return['result']='Success';
    $return['quantity']=$FixedStock['FixedStock']['quantity'];  
  }
  echo json_encode($return);
  exit;
}
public function get_vendor_address($id)
{
  $return['result']='Error';
  $return['address']='';
  $Vendor=$this->Vendor->findByAccountHeadId($id);
  if($Vendor)
  {
    $return['result']='Success';
    $return['address']=$Vendor['Vendor']['place'];  
  }
  echo json_encode($return);
  exit;
}
public function delete_wish_list($id)
{
  $data=  array(
    'product_id'=>$id,
  );
  $this->UnwantedList->create();
  if ($this->UnwantedList->save($data))
  {
    $return['result']="Success";
  }
  else
  {
    $return['result']="Error";
  }
  echo json_encode($return);
  exit;
}
public function Party_Account_head_ajax()
{
  $user_id=1;
  $sub_group_id=1;
  $datasource_Party = $this->Party->getDataSource();
  $datasource_AccountHead = $this->AccountHead->getDataSource();
  try {
    $datasource_Party->begin();
    $datasource_AccountHead->begin();
    $Party_data=$this->request->data['Party'];
    $opening_balance=$Party_data['opening_balance'];
    $name=$Party_data['name'];
    $date=date('Y-m-d');
    $place=$Party_data['place'];
    // $code=$Party_data['code'];
    $email=$Party_data['email'];
    $mobile=$Party_data['mobile'];
    $vat_no=$Party_data['vat_no'];
    // $gstin=$Party_data['gstin'];
    // $state_id=$Party_data['state_id'];
    $description=$Party_data['description'];
    $StaffRow = $this->Party->find('first', array(
      'order' => array('Party.id' => 'DESC') ));
    if(!empty($StaffRow))
    {
      $PrevCode = $StaffRow['Party']['code'];
      $numeric = str_replace("VR","",$PrevCode);
      $numeric_code = $numeric + 1;
      $code = 'VR'.$numeric_code;
    }
    else{
      $code = 'VR1111';
    }
    $AccountingsController = new AccountingsController;
    $function_return=$AccountingsController->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
    if($function_return['result']!='Success')
      throw new Exception($function_return['message']);
    $AccountHead_id=$this->AccountHead->getLastInsertId();
    $Party_date=[
      'account_head_id'=>$AccountHead_id,
    // 'state_id'=>$state_id,
      'place'=>$place,
      'code'=>$code,
      'email'=>$email,
      'mobile'=>$mobile,
      'vat_no'=>$vat_no,
      'created_by'=>$user_id,
      'modified_by'=>$user_id,
      'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
      'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
    ];
    $this->Party->create();
    if(!$this->Party->save($Party_date))
      throw new Exception("Error Party Creation", 1);
    $return['result']='Success';
    $Party_id=$this->Party->getLastInsertId();
    $Party=$this->Party->findById($Party_id);
    $return['key']=$Party['AccountHead']['id'];
    $return['value']=$Party['AccountHead']['name'];
    $datasource_Party->commit();
    $datasource_AccountHead->commit();
  } catch (Exception $e) {
    $datasource_Party->rollback();
    $datasource_AccountHead->rollback();
    $return['result']=$e->getMessage();
  }
  echo json_encode($return);
  exit;
}
public function Vendor_Account_head_ajax()
{
  $user_id=1;
  $sub_group_id=16;
  $datasource_Vendor = $this->Vendor->getDataSource();
  $datasource_AccountHead = $this->AccountHead->getDataSource();
  try {
    $datasource_Vendor->begin();
    $datasource_AccountHead->begin();
    $Vendor_data=$this->request->data['Vendor'];
    $opening_balance=$Vendor_data['opening_balance'];
    $name=$Vendor_data['name'];
    $date=date('Y-m-d');
    $place=$Vendor_data['place'];
    $code=$Vendor_data['code'];
    $email=$Vendor_data['email'];
    $mobile=$Vendor_data['mobile'];
    $gstin=$Vendor_data['gstin'];
    $state_id=$Vendor_data['state_id'];
    $description=$Vendor_data['description'];
    $AccountingsController = new AccountingsController;
    $function_return=$AccountingsController->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description);
    if($function_return['result']!='Success')
      throw new Exception($function_return['message']);
    $AccountHead_id=$this->AccountHead->getLastInsertId();
    $Vendor_date=[
      'account_head_id'=>$AccountHead_id,
      'place'=>$place,
      'code'=>$code,
      'email'=>$email,
      'mobile'=>$mobile,
      'gstin'=>$gstin,
      'state_id'=>$state_id,
      'created_by'=>$user_id,
      'modified_by'=>$user_id,
      'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
      'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
    ];
    $this->Vendor->create();
    if(!$this->Vendor->save($Vendor_date))
      throw new Exception("Error Vendor Creation", 1);
    $return['result']='Success';
    $Vendor_id=$this->Vendor->getLastInsertId();
    $Vendor=$this->Vendor->findById($Vendor_id);
    $return['key']=$Vendor['AccountHead']['id'];
    $return['value']=$Vendor['AccountHead']['name'];
    $datasource_Vendor->commit();
    $datasource_AccountHead->commit();
  } catch (Exception $e) {
    $datasource_Vendor->rollback();
    $datasource_AccountHead->rollback();
    $return['result']=$e->getMessage();
  }
  echo json_encode($return);
  exit;
}
public function PurchaseExpense_delete($id)
{
  $datasource_PurchaseExpense = $this->PurchaseExpense->getDataSource();
  try {
    $datasource_PurchaseExpense->begin();
    $user_id=1;
    
    if(!$this->PurchaseExpense->delete($id))
      throw new Exception("Error while deleting", 1);
    $datasource_PurchaseExpense->commit();
    $return['result']='Success';
  } catch (Exception $e) {
    $datasource_PurchaseExpense->rollback();
    $return['result']=$e->getMessage();
  }
  echo json_encode($return);
  exit;
}
public function PurchasedItem_delete($id)
{
  $datasource_PurchasedItem = $this->PurchasedItem->getDataSource();
  $datasource_Stock = $this->Stock->getDataSource();
  try {
    $datasource_PurchasedItem->begin();
    $datasource_Stock->begin();
    $user_id=1;
    $PurchasedItem=$this->PurchasedItem->findById($id);
    if($PurchasedItem['Purchase']['status']==2)
    {
      $product_id=$PurchasedItem['PurchasedItem']['product_id'];
      $quantity=$PurchasedItem['PurchasedItem']['quantity'];
      $remark='Delete Purchase :'.$PurchasedItem['Purchase']['invoice_no'];
      $Stock = $this->Stock->findByProductId($product_id);
      $stock_id=$Stock['Stock']['id'];
      $stock_quantity=$Stock['Stock']['quantity'];
      $date=date('Y-m-d');
      $stock_quantity-=$quantity;
      $StockController = new StockController;
      $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
      if($Stock_function_return['result']!='Success')
        throw new Exception($Stock_function_return['result']); 
    }
    if(!$this->PurchasedItem->delete($id))
      throw new Exception("Error while deleting", 1);
    $datasource_PurchasedItem->commit();
    $datasource_Stock->commit();
    $return['result']='Success';
  } catch (Exception $e) {
    $datasource_PurchasedItem->rollback();
    $datasource_Stock->rollback();
    $return['result']=$e->getMessage();
  }
  echo json_encode($return);
  exit;
}
public function PurchaseReturnIndex()
{
  $PermissionList = $this->Session->read('PermissionList');
  $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Purchase/PurchaseReturnIndex'));
  // if(!in_array($menu_id, $PermissionList))
  // {
  //   $this->Session->setFlash("Permission denied");
  //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
  // }
  $first_date = date('Y-m-d',strtotime('first day of this month'));
  $last_date = date('Y-m-d');
  $firstdate = date('d-m-Y',strtotime('first day of this month'));
  $this->set('firstdate', $firstdate);
  $todate = date('d-m-Y');
  $this->set('todate', $todate);
  $PurchaseReturn = $this->PurchaseReturn->find('all', array(
    "joins" => array(
      array(
        "table" => 'parties',
        "alias" => 'Party',
        "type" => 'inner',
        "conditions" => array('Party.account_head_id=PurchaseReturn.account_head_id'),
      ),
    ),
    'conditions' => array(
      '(PurchaseReturn.date) between ? and ? '=>array($first_date,$last_date),
    ),
    'order' => array('date DESC'),
    'fields' => array(
      'AccountHead.*',
      'Party.*',
      'PurchaseReturn.*',
    )
  ));
  $this->set('PurchaseReturn', $PurchaseReturn);
}
public function purchase_return_search_ajax()
{
  $data['row']='';
  $first_date = date('Y-m-d',strtotime('first day of this month'));
  $last_date = date('Y-m-d',strtotime('last day of this month'));
  if(!empty($this->request->data['from_date'])){
   $first_date = date('Y-m-d',strtotime($this->request->data['from_date']));
 }
 if(!empty($this->request->data['to_date'])){
   $last_date = date('Y-m-d',strtotime($this->request->data['to_date']));
 }
 $PurchaseReturn = $this->PurchaseReturn->find('all', array(
  "joins" => array(
    array(
      "table" => 'parties',
      "alias" => 'Party',
      "type" => 'inner',
      "conditions" => array('Party.account_head_id=PurchaseReturn.account_head_id'),
    ),
  ),
  'conditions' => array(
    '(PurchaseReturn.date) between ? and ? '=>array($first_date,$last_date),
  ),
  'order' => array('date DESC'),
  'fields' => array(
    'AccountHead.*',
    'Party.*',
    'PurchaseReturn.*',
  )
));
 foreach ($PurchaseReturn as $key => $value) {
  if($value['PurchaseReturn']['status']==0)
  {
    $name='<i title="Cancelled" style="color:red;" class="fa fa-close fa-3x" aria-hidden="true"></i>';
  }
  if($value['PurchaseReturn']['status']==1)
  {
    $name='<i title="Order Placed" style="color:red;" class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>';
  }
  if($value['PurchaseReturn']['status']==2)
  {
    $name='<i title="Order Delivered" style="color:green;" class="fa fa-truck fa-2x" aria-hidden="true"></i>';
  }
  $sno = $key+1;
  $data['row']= $data['row'].'<tr class="blue-pddng">';
  $data['row']= $data['row'].'<td>';
  $data['row'].='<span>'.date('d-m-Y',strtotime($value['PurchaseReturn']['date'])).'</span>';
  $data['row'].='<span class="PurchaseReturn_id" style="display:none;"">'. $value['PurchaseReturn']['id'].'</span>';
  $data['row']= $data['row'].'<td>'.$value['AccountHead']['name'].'</td>';
  $data['row']= $data['row'].'<td>'.$value['PurchaseReturn']['invoice_no'].'</td>';
  $data['row']= $data['row'].'<td class="text-right">'. number_format($value['PurchaseReturn']['grand_total']).'</td>';
  $data['row']= $data['row'].'<td>'.$name.'</td>';
  $data['row']= $data['row'].'</tr>';
}
echo json_encode($data);
exit;
}
public function PurchaseReturn($id=null)
{
  $PermissionList = $this->Session->read('PermissionList');
  $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Purchase/PurchaseReturn'));
  // if(!in_array($menu_id, $PermissionList))
  // {
  //   $this->Session->setFlash("Permission denied");
  //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
  // }
  $sub_group_id=3;
  $user_id=1;
  $purchase_return_account_head_id=7;
  $discount_received_account_head_id=12;
  $discount_paid_account_head_id=13;
  $roundoff_paid_account_head_id=15;
  //$tax_on_purchase_account_head_id=8;
  $tax_on_purchase_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DUTIES & TAXES'));
  $stock_account_head_id=19;
  $AccountingsController = new AccountingsController;
  $Accounting_function_return=$AccountingsController->AccountHead_Option_ListBySubGroupId($sub_group_id);
  $this->set('Party_list',$Accounting_function_return);
  $Product_list=$this->Product->find('list',array('conditions'=>array('Product.active=1','type !='=>[2,3]),'order'=>array('Product.name ASC'),'fields'=>['id','name']));
  $this->set('Product_list',$Product_list);
  $PurchaseReturn=$this->PurchaseReturn->find('first',array('fields' => array('MAX(PurchaseReturn.invoice_no) as order_no')));
  $Unit=$this->Unit->find('list',array('fields'=>['id','name']));
  $this->set('Unit',$Unit);

  if(!empty($PurchaseReturn)) 
  {
    $invoice_no=$PurchaseReturn[0]['order_no']+1;
  } 
  else 
  {
    $invoice_no=1;
  }
  $invoice_no_check=$this->PurchaseReturn->findByInvoiceNo($invoice_no);
  while ($invoice_no_check) {
    $invoice_no+=1;
    $invoice_no_check=$this->PurchaseReturn->findByInvoiceNo($invoice_no);        
  }
  if (!$this->request->data) 
  {
    if(empty($id)) 
    {
     $data['PurchaseReturn']['invoice_no']=$invoice_no;
     $data['PurchaseReturn']['date']=date('d-m-Y');
     $data['PurchaseReturn']['status']=1;
     $Party_list=$this->Party->find('list',array(
      'joins'=>array(
        array(
          'table'=>'account_heads',
          'alias'=>'AccountHead',
          'type'=>'INNER',
          'conditions'=>array('AccountHead.id=Party.account_head_id')
        ),
      ),
      'fields'=>['AccountHead.id','AccountHead.name'])
   );
     if(empty($Party_list))
     {
      return $this->redirect(array('controller' => 'Accountings', 'action' => 'CurrentLiabilityCrediterSupplier'));
    }
    $this->set('Party_list',$Party_list);
    $invoice_no_list=$this->purchase_invoice_no_list_by_party_id(array_keys($Party_list)[0]);
    $this->set('invoice_no_list',$invoice_no_list);
    $this->request->data=$data;
  }
  else

  {
    $PurchaseReturn=$this->PurchaseReturn->findById($id);
    $Party=$this->Party->findByAccountHeadId($PurchaseReturn['PurchaseReturn']['account_head_id']);
    $this->request->data=$PurchaseReturn;
    $this->request->data['PurchaseReturn']['discount']=round($PurchaseReturn['PurchaseReturn']['discount'],2);
    $this->request->data['PurchaseReturn']['date']=date('d-m-Y',strtotime($PurchaseReturn['PurchaseReturn']['date']));
    $this->request->data['PurchaseReturn']['address']=$Party['Party']['place'];
    $Party_list=$this->Party->find('list',array(
      'joins'=>array(
        array(
          'table'=>'account_heads',
          'alias'=>'AccountHead',
          'type'=>'INNER',
          'conditions'=>array('AccountHead.id=Party.account_head_id')
        ),
      ),
      'fields'=>['AccountHead.id','AccountHead.name'])
  );
    $this->set('Party_list',$Party_list);
    $PurchaseReturnItem=$this->PurchaseReturnItem->find('all',array(
     "joins" => array(
      array(
        "table" => 'brands',
        "alias" => 'Brand',
        "type" => 'left',
        "conditions" => array('Brand.id=Product.brand_id'),
      ),
      array(
        "table" => 'product_types',
        "alias" => 'ProductType',
        "type" => 'inner',
        "conditions" => array('ProductType.id=Product.product_type_id'),
      ),
    ),
     'conditions'=>array(
      'purchase_return_id'=>$id,
    ),
     'fields'=>array(
      'PurchaseReturnItem.*',
      'Product.name',
      'Brand.name',
      'ProductType.name',
      'Product.no_of_piece_per_unit',
      'Unit.name',
    )
   ));
    foreach ($PurchaseReturnItem as $key => $value) {
      $product_id=$value['PurchaseReturnItem']['product_id'];
      if(empty($value['PurchaseReturnItem']['unit_id']))
        $value['PurchaseReturnItem']['unit_id']=$value['PurchaseReturnItem']['quantity_mode'];
      $unit_id=$value['PurchaseReturnItem']['unit_id'];
      $SaleController= new SaleController;
      $UnitLevelConvert=$SaleController->UnitLevelConvert($product_id,$unit_id);
      $sale_unit_level=$UnitLevelConvert['sale_unit_level'];
      $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
      $product_unit_level=$UnitLevelConvert['product_unit_level'];
      if($sale_unit_level != 1){
        if($sale_unit_level == 2){
          $PurchaseReturnItem[$key]['PurchaseReturnItem']['quantity']=$value['PurchaseReturnItem']['quantity']/$no_of_piece_per_unit;
          $PurchaseReturnItem[$key]['PurchaseReturnItem']['unit_price']=$value['PurchaseReturnItem']['unit_price']*$no_of_piece_per_unit;
          $PurchaseReturnItem[$key]['PurchaseReturnItem']['invoice_price']=$value['PurchaseReturnItem']['invoice_price']*$no_of_piece_per_unit;

        }
      }

    }
    $invoice_no_list=$this->purchase_invoice_no_list_by_party_id($PurchaseReturn['PurchaseReturn']['account_head_id']);
    // pr($invoice_no_list);
     // exit;
    $this->set('invoice_no_list',$invoice_no_list);
    $this->set('PurchaseReturnItem',$PurchaseReturnItem);
  }
} 
else 
{
  $datasource_PurchaseReturn = $this->PurchaseReturn->getDataSource();
  $datasource_PurchaseReturnItem = $this->PurchaseReturnItem->getDataSource();
  $datasource_Product = $this->Product->getDataSource();
  $datasource_Stock = $this->Stock->getDataSource();
  $datasource_Journal = $this->Journal->getDataSource();
  try {
    $datasource_PurchaseReturn->begin();
    $datasource_PurchaseReturnItem->begin();
    $data=$this->request->data;
     //pr($data);
     //exit;
    if(empty($id)) 
    {
      $data=$data['PurchaseReturn'];
      $PurchaseReturn_data=[
        'account_head_id'=>$data['account_head_id'],
        'invoice_no'=>$data['invoice_no'],
        'date'=>date('Y-m-d',strtotime($data['date'])),
        'total'=>$data['grand_total'],
        'discount'=>$data['discount'],
        'total_tax'=>0,
        'total_tax_amount'=>0,
        'grand_total'=>$data['net_amount'],
        'created_by'=>$user_id,
        'modified_by'=>$user_id,
        'created_at'=>date('Y-m-d H:i:s'),
        'updated_at'=>date('Y-m-d H:i:s'),
      ];
      $process=$data['process'];
      if($process=='delivery')
      {
        $PurchaseReturn_data['status']=2;
      }
      $this->PurchaseReturn->create();
      if(!$this->PurchaseReturn->save($PurchaseReturn_data)) 
      {
        $errors = $this->PurchaseReturn->validationErrors;
        foreach ($errors as $key => $value) {
          throw new Exception($value[0], 1);
        }
      }
      $id=$this->PurchaseReturn->getLastInsertId();
      for ($i=0; $i <count($data['product_id']) ; $i++) {
        $this->Purchase->unbindModel(array('hasMany' => array('PurchasedItem')));
        $inv_no=0;
        if($data['invoice_no_list'][$i])
        {
         $invo_no=$this->Purchase->findById($data['invoice_no_list'][$i]);
         $inv_no=$invo_no['Purchase']['invoice_no'];
        }
        //pr($invo_no);
        $product_id=$data['product_id'][$i];
        $unit_id=$data['quantity_mode'][$i];
        $SaleController= new SaleController;
        $UnitLevelConvert=$SaleController->UnitLevelConvert($product_id,$unit_id);
        $sale_unit_level=$UnitLevelConvert['sale_unit_level'];
        $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
        $product_unit_level=$UnitLevelConvert['product_unit_level'];
        if($sale_unit_level != 1){
          if($sale_unit_level == 2){
            $data['quantity'][$i]=$data['quantity'][$i]*$no_of_piece_per_unit;
            $data['unit_price'][$i]=$data['unit_price'][$i]/$no_of_piece_per_unit;
            $data['invoice_price'][$i]=$data['invoice_price'][$i]/$no_of_piece_per_unit;
            
          }
        }
        $PurchaseReturnItem_data=[
          'product_id'=>$data['product_id'][$i],
          'purchase_return_id'=>$id,
          'invoice_no'=>$inv_no,
          'invoice_price'=>$data['invoice_price'][$i],
          'quantity_mode'=>$data['quantity_mode'][$i],
          'unit_id'=>$data['quantity_mode'][$i],
          'unit_price'=>$data['unit_price'][$i],
          'quantity'=>$data['quantity'][$i],
          'net_value'=>$data['net_value'][$i],
         'tax'=>$data['tax'][$i],
          'tax_amount'=>$data['tax_amount'][$i],
          'total'=>$data['row_total'][$i],
        ];
        $this->PurchaseReturnItem->create();
        if(!$this->PurchaseReturnItem->save($PurchaseReturnItem_data)) 
        {
          $errors = $this->PurchaseReturnItem->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
      }
     // exit;
    } 
    else
    {
      $data_PurchaseReturn=$data['PurchaseReturn'];
      if(isset($data['PurchaseReturnItem']))
      {
        $old_PurchaseReturnItem=$data['PurchaseReturnItem'];  
      }
      $PurchaseReturn_data=[
        'account_head_id'=>$data_PurchaseReturn['account_head_id'],
        'invoice_no'=>''.$data_PurchaseReturn['invoice_no'].'',
        'date'=>date('Y-m-d',strtotime($data_PurchaseReturn['date'])),
        'total'=>$data_PurchaseReturn['grand_total'],
        'discount'=>$data_PurchaseReturn['discount'],
        'tax'=>0,
          'tax_amount'=>0,
        'grand_total'=>$data_PurchaseReturn['net_amount'],
        'updated_at'=>date('Y-m-d H:i:s'),
      ];
      $process=$data_PurchaseReturn['process'];
      if($process=='cancel')
      {
        goto cancel_process;
      }
      if($process=='delete')
      {
        goto delete_process;
      }
      if($process=='delivery')
      {
        $PurchaseReturn_data['status']=2;
      }
      $this->PurchaseReturn->id=$id;
      if(!$this->PurchaseReturn->save($PurchaseReturn_data))
      {
        $errors = $this->PurchaseReturn->validationErrors;
        foreach ($errors as $key => $value) {
          throw new Exception($value[0], 1);
        }
      }
      if(isset($data_PurchaseReturn['product_id'])){
        for ($i=0; $i <count($data_PurchaseReturn['product_id']) ; $i++) {
         $this->Purchase->unbindModel(array('hasMany' => array('PurchasedItem')));
         $invo_nos=$this->Purchase->findById($data_PurchaseReturn['invoice_no_list'][$i]);
         $inv_nos=$invo_nos['Purchase']['invoice_no'];
         if(empty($data_PurchaseReturn['tax'][$i]))
          $data_PurchaseReturn['tax'][$i]=0.00;
        $data_PurchaseReturn['tax_amount'][$i]=0.00;
        $PurchaseReturnItem_data=[
          'product_id'=>$data_PurchaseReturn['product_id'][$i],
          'purchase_return_id'=>$id,
          'invoice_no'=>$inv_nos,
          'invoice_price'=>$data_PurchaseReturn['invoice_price'][$i],
          'unit_id'=>$data_PurchaseReturn['quantity_mode'][$i],
          'quantity_mode'=>$data_PurchaseReturn['quantity_mode'][$i],
          'unit_price'=>$data_PurchaseReturn['unit_price'][$i],
          'quantity'=>$data_PurchaseReturn['quantity'][$i],
          'net_value'=>$data_PurchaseReturn['net_value'][$i],
          'tax'=>$data_PurchaseReturn['tax'][$i],
          'tax_amount'=>$data_PurchaseReturn['tax_amount'][$i],
          'total'=>$data_PurchaseReturn['row_total'][$i],
        ];
        $this->PurchaseReturnItem->create();
        if(!$this->PurchaseReturnItem->save($PurchaseReturnItem_data)) 
        {
          $errors = $this->PurchaseReturnItem->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
      }
    }
    cancel_process :
    if($process=='cancel')
    {
      $this->PurchaseReturn->id=$id;
      if(!$this->PurchaseReturn->saveField('flag','0'))
        throw new Exception('Error While Cancelling', 1);
      if(!$this->PurchaseReturn->saveField('status','0'))
        throw new Exception('Error While Cancelling', 1);
      if(!$this->PurchaseReturn->saveField('date',date('Y-m-d',strtotime($data['PurchaseReturn']['date']))))
        throw new Exception('Error While Cancelling', 1);
    }
    delete_process :
    if($process=='delete')
    {
      $PurchaseReturn=$this->PurchaseReturn->findById($id);
      $status=$PurchaseReturn['PurchaseReturn']['status'];
      if($status==2)
      {
        $purchase_return_rollback_function=$this->purchase_return_rollback($id);
        if($purchase_return_rollback_function['result']!='Success')
          throw new Exception($purchase_return_rollback_function['result']);
      }
      $PurchaseReturnItem=$this->PurchaseReturnItem->find('all',array(
        'conditions'=>array(
          'purchase_return_id'=>$id
        ),
      ));
      foreach ($PurchaseReturnItem as $key => $value) {
        if(!$this->PurchaseReturnItem->delete($value['PurchaseReturnItem']['id']))
          throw new Exception("Error Processing PurchaseReturnItem deletion", 1);
      }
      if(!$this->PurchaseReturn->delete($id))
        throw new Exception("Error Processing PurchaseReturn deletion", 1);
      $datasource_PurchaseReturn->commit();
      $datasource_PurchaseReturnItem->commit();
      $return['result']='Success';
      $this->Session->setFlash(__($return['result']));
      return $this->redirect(array('controller' => 'Purchase', 'action' => 'PurchaseReturn'));
    }
  }
  if($process=='delivery')
  {
    $PurchaseReturnItem = $this->PurchaseReturnItem->find('all',array(
      'conditions' => array(
        'PurchaseReturnItem.purchase_return_id' => $id,
      ),
      'fields' => array(
        'PurchaseReturnItem.product_id',
        'PurchaseReturnItem.quantity',
        'PurchaseReturnItem.unit_price',
        'Product.no_of_piece_per_unit',
        'PurchaseReturnItem.net_value',
        'PurchaseReturnItem.quantity_mode',
        'PurchaseReturnItem.tax_amount',
        'PurchaseReturn.account_head_id',
        'PurchaseReturn.invoice_no',
        'PurchaseReturn.total_tax_amount',
        'PurchaseReturn.date',
        'PurchaseReturn.grand_total',
        'PurchaseReturn.discount',
        'PurchaseReturn.date',
      )
    ));
    $net_value=0;
    $tax_amount=0;
    $datasource_Stock->begin();
    foreach ($PurchaseReturnItem as $key => $value) {
      $tax_amount+= $value['PurchaseReturnItem']['tax_amount'];
      $net_value+= $value['PurchaseReturnItem']['net_value'];
        // if($value['PurchaseReturnItem']['quantity_mode']==2)
        // {
        //   $quantity = $value['PurchaseReturnItem']['quantity']*$value['Product']['no_of_piece_per_unit']; 
        // }
        // else
        // {
        //     $quantity = $value['PurchaseReturnItem']['quantity'];

        // }
      $quantity = $value['PurchaseReturnItem']['quantity'];
      $unit_price = $value['PurchaseReturnItem']['unit_price'];
      $product_id=$value['PurchaseReturnItem']['product_id'];
      $Stock = $this->Stock->findByProductId($product_id);
      $getStock = $this->Stock->find('all', array(
        'conditions'=>['Stock.product_id'=>$product_id],
      ));
      $cost_in_stock=$Stock['Product']['cost'];
      $quantity1 = $value['PurchaseReturnItem']['quantity']/$Stock['Product']['no_of_piece_per_unit'];
      $whole_stock=0;
      foreach ($getStock as $key_stock => $value_stock) {
       $whole_stock+=$value_stock['Stock']['quantity'];
     }
     $stock_id=$Stock['Stock']['id'];
     $stock_quantity=$Stock['Stock']['quantity'];
     $date=$PurchaseReturnItem['0']['PurchaseReturn']['date'];
     $StockController = new StockController;
     $remark='PurchaseReturn --'.$value['PurchaseReturn']['invoice_no'].'('.$quantity.')';
     $stock_quantity-=$quantity;
     $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
     if($Stock_function_return['result']!='Success')
      throw new Exception($Stock_function_return['result']);
    $this->Product->id=$product_id;
    if($quantity!=0)
    {
      $division=$whole_stock-$quantity;
      $average_cost=0;
      if($division!=0)
      {
      $average_cost=number_format(((($cost_in_stock*$whole_stock)-($unit_price*$quantity))/($division)),3,'.','');
      }
    }
    else
    {
      $average_cost=0;
    }
    if(!$this->Product->saveField('cost',$average_cost))
      throw new Exception("Error In Product Updation", 38);
  }
  $datasource_Journal->begin();
  $account_head_id=$PurchaseReturnItem['0']['PurchaseReturn']['account_head_id'];
  $debit=$account_head_id;
  $credit=$purchase_return_account_head_id;
  $discount=$PurchaseReturnItem['0']['PurchaseReturn']['discount'];
  $total_tax_amount=$PurchaseReturnItem['0']['PurchaseReturn']['total_tax_amount'];
  $work_flow='Purchase Return';
  $invoice_no=$PurchaseReturnItem['0']['PurchaseReturn']['invoice_no'];
  $date=$PurchaseReturnItem['0']['PurchaseReturn']['date'];
  $remarks='PurchaseReturn Invoice No :'.$invoice_no;
  $AccountingsController = new AccountingsController;
  $voucher_no=$AccountingsController->GetVoucherNo();
  $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$net_value,$date,$remarks,$work_flow,$user_id,$voucher_no);
  if($Accountings_function_return['result']!='Success')
    throw new Exception($Stock_function_return['message']);
  if($net_value)
  {
    $debit=$purchase_return_account_head_id;
    $credit=$stock_account_head_id;
    $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$net_value,$date,$remarks,$work_flow,$user_id,$voucher_no);
    if($Accountings_function_return['result']!='Success')
      throw new Exception($Accountings_function_return['message']);
  }
  if($tax_amount)
  {
    $credit=$tax_on_purchase_account_head_id;
    $debit=$account_head_id;
    $AccountingsController = new AccountingsController;
    $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$tax_amount,$date,$remarks,$work_flow,$user_id,$voucher_no);
    if($Accountings_function_return['result']!='Success')
      throw new Exception($Stock_function_return['message']);   
  }
  {
    if($discount>0)
    {
      $credit=$PurchaseReturnItem['0']['PurchaseReturn']['account_head_id'];
      $debit=$discount_paid_account_head_id;
    }
    else
    {

      $discount=$discount*-1;
      $credit=$discount_paid_account_head_id;
      $debit=$PurchaseReturnItem['0']['PurchaseReturn']['account_head_id'];
    }
    $AccountingsController = new AccountingsController;
    $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$discount,$date,$remarks,$work_flow,$user_id,$voucher_no);
    if($Accountings_function_return['result']!='Success')
      throw new Exception($Stock_function_return['message']);
  }
  $datasource_Journal->commit();
  $datasource_Stock->commit();
}
$datasource_PurchaseReturn->commit();
$datasource_PurchaseReturnItem->commit();
$return['result']='Updated';
$this->Session->setFlash(__($return['result']));
return $this->redirect(array('controller' => 'Purchase', 'action' => 'PurchaseReturn',$id)); 
} 
catch (Exception $e) 
{
  $datasource_PurchaseReturn->rollback();
  $datasource_PurchaseReturnItem->rollback();
  $datasource_Stock->rollback();
  $return['result']=$e->getMessage();
  $this->Session->setFlash(__($return['result']));
  $this->redirect( Router::url( $this->referer(), true ) );
}
}
}

public function get_product_list_ajax($purchase_id=null)
{
  $conditions=[];
  $joins=[];
  if($purchase_id)
  {
    $conditions['PurchasedItem.purchase_id']=$purchase_id;
    $joins=[
      array(
        'table'=>'purchased_items',
        'alias'=>'PurchasedItem',
        'type'=>'INNER',
        'conditions'=>array('PurchasedItem.product_id=Product.id')
      )
    ];
  }
  $return['result']='Empty';
     $conditions['Product.type !=']=[2,3];
  $Product=$this->Product->find('list',array(
    'joins'=>$joins,
    'order'=>array('Product.name ASC'),
    'fields'=>array('Product.id','Product.name'),
    'conditions'=>$conditions,
  ));
  if($Product)
  {
    $return['result']='Success';
    $return['products']=$Product;  
  }
  $return['products']['']='Select';
  // pr($return);
  // exit;
// arsort($return['options']);
  echo json_encode($return);
  exit;
}
public function get_invoice_list_ajax($party_id)
{
  $invoice_no_list=$this->purchase_invoice_no_list_by_party_id($party_id);
  $return['options']=$invoice_no_list;
// arsort($return['options']);
  echo json_encode($return);
  exit;
}
public function purchase_invoice_no_list_by_party_id($account_head_id)
{
  $invoice_no_list_array=$this->Purchase->find('list',array(
    'conditions'=>[
      'Purchase.account_head_id'=>$account_head_id,
      'Purchase.status >'=>1,
    ],
    'fields'=>array(
      'Purchase.id',
      'Purchase.invoice_no',
    )
  )
);
  $invoice_no_list_array['']='General Invoice';
  ksort($invoice_no_list_array);
  return $invoice_no_list_array;
}
public function get_product_purchased_item_detials_ajax($purchase_id=null,$product_id=null)
{

  $data=$this->request->data;
  $return['result']='Error';
  $product_id=$data['product_id'];
  $purchase_id=$data['purchase_id'];  

  $unit_id=$data['unit_id'];  
  $conditions=[];
  $joins=[];
  $fields=[
    'Product.mrp',
    'Product.tax',
    'Product.unit_id',
    'Product.no_of_piece_per_unit'
  ];
  if($purchase_id!='General Invoice')
  {
    $Purchase_details=$this->Purchase->findByInvoiceNo($purchase_id);

    $conditions['PurchasedItem.purchase_id']=$Purchase_details['Purchase']['id'];
    $joins=[
      array(
        'table'=>'purchased_items',
        'alias'=>'PurchasedItem',
        'type'=>'INNER',
        'conditions'=>array('PurchasedItem.product_id=Product.id')
      )
    ];
    $fields=[
      'PurchasedItem.unit_price',
      'PurchasedItem.tax',
      'PurchasedItem.quantity_mode',
      'PurchasedItem.quantity',
      'PurchasedItem.discount',
      'Product.no_of_piece_per_unit',
    ];
  }
  if($product_id)
  {
    $conditions['Product.id']=$product_id;
    $conditions['Product.type !=']=[2,3];
    $Product=$this->Product->find('first',array(
      'joins'=>$joins,
      'fields'=>$fields,
      'order'=>array('Product.name ASC'),
      'conditions'=>$conditions,
    ));

    if($purchase_id!='General Invoice')
    {
     $Purchase=$this->Purchase->find('first',array(
      'fields'=>array('Purchase.total_tax'),
      'conditions'=>array('Purchase.id'=>$Purchase_details['Purchase']['id']),
    ));
     $return['Product']['quantity_mode']=$Product['PurchasedItem']['quantity_mode'];
     $return['Product']['no_of_piece_per_unit']=$Product['Product']['no_of_piece_per_unit'];


     $return['Product']['quantity']=$Product['PurchasedItem']['quantity'];
     $return['Product']['mrp']=$Product['PurchasedItem']['unit_price'];
        $return['Product']['tax']=$Product['PurchasedItem']['tax'];     
     $SaleController = new SaleController;
     $UnitLevelConvert=$SaleController->UnitLevelConvert($product_id,$unit_id);
     $sale_unit_level=$UnitLevelConvert['sale_unit_level'];
     $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];

     $PurchaseReturnItem=$this->PurchaseReturnItem->find('all',array(
      'conditions'=>array(
        'PurchaseReturnItem.invoice_no'=>$purchase_id,
        'PurchaseReturnItem.product_id'=>$product_id,
      ),
      'fields'=>array(
        'PurchaseReturnItem.quantity',
      )
    ));
     if($PurchaseReturnItem)
     {
      foreach ($PurchaseReturnItem as $key => $value) {
        $return['Product']['quantity']-=$value['PurchaseReturnItem']['quantity'];
      }
    }
    if($sale_unit_level != 1){
      if($sale_unit_level == 2){
        $return['Product']['quantity']=$return['Product']['quantity']/$no_of_piece_per_unit;
        $return['Product']['mrp']=$return['Product']['mrp']*$no_of_piece_per_unit;
      }
    }
  }
  else
  {
    $return['Product']['mrp']=$Product['Product']['mrp'];
    $return['Product']['quantity']=0;
    $return['Product']['tax']=$Product['Product']['tax'];
    $return['Product']['quantity_mode']=$Product['Product']['unit_id'];
    $return['Product']['no_of_piece_per_unit']=$Product['Product']['no_of_piece_per_unit'];
  }
  $return['result']='Success';
}
else
{

  $return['Product']['mrp']=0;
  $return['Product']['quantity']=0;
  $return['Product']['tax']=0;
  $return['Product']['quantity_mode']=0;
  $return['Product']['no_of_piece_per_unit']=0;

  $return['result']='Empty';
}
echo json_encode($return);
exit;

}
public function expense_add_ajax()
{
  $data=array(
    'name'=>strtoupper($this->request->data['name']),
  );
  $this->Expense->create();
  if($this->Expense->save($data))
  {
    $last_iserted_pt=$this->Expense->findById($this->Expense->getLastInsertId());
    $return['key']=$last_iserted_pt['Expense']['id'];
    $return['value']=$last_iserted_pt['Expense']['name'];
    $return['result']='Success';
  }else{
    $return['result']='Eror in creation';
  }
  echo json_encode($return);
  exit;
}
public function PurchaseReturnItem_delete($id)
{
  try {
    if(!$this->PurchaseReturnItem->delete($id))
      throw new Exception("Error while deleting", 1);
    $return['result']='Success';
  } catch (Exception $e) {
   $return['result']=$e->getMessage();
 }
 echo json_encode($return);
 exit;
}
public function get_party_purchased_list($id)
{
//    $this->PurchasedItem->virtualFields = array(
//     'product_name' => "CONCAT(Product.name, ' ', Product.code)"
// );
  $PurchasedItems=$this->Purchase->find('all',array(
    "joins" => array(
      array(
        "table" => 'purchased_items',
        "alias" => 'PurchasedItem',
        "type" => 'inner',
        "conditions" => array('PurchasedItem.purchase_id=Purchase.id'),
        'order'=>array('PurchasedItem.id'=>'DESC')
      ),
      array(
        "table" => 'products',
        "alias" => 'Product',
        "type" => 'inner',
        "conditions" => array('Product.id=PurchasedItem.product_id'),
      ),
      array(
        "table" => 'product_types',
        "alias" => 'ProductType',
        "type" => 'inner',
        "conditions" => array('ProductType.id=Product.product_type_id'),
      ),
      array(
        "table" => 'brands',
        "alias" => 'Brand',
        "type" => 'inner',
        "conditions" => array('Brand.id=Product.brand_id'),
      ),
    ),
    'conditions'=>array(
      'Purchase.account_head_id'=>$id,
      'Purchase.status>1',
    ),
    'order'=>array('Purchase.id'=>'
      '),
    'fields'=>array(
      'Product.code',
      'PurchasedItem.unit_price',
      'PurchasedItem.quantity',
      'PurchasedItem.id',
      'Purchase.updated_at',
      'Product.id',
      'Product.name',
      'Product.mrp',
      'Product.wholesale_price',
      'PurchasedItem.tax',
      'Product.tax',
      'Product.cost',
      'ProductType.name',
      'Brand.name',
    )
  ));
  $PurchasedItems_array=[];
  foreach ($PurchasedItems as $key => $value) {
    $single_purchased_items['name']=$value['Product']['name'];
    $single_purchased_items['product_name']=$value['Product']['name'].' '.$value['Product']['code'];
    $single_purchased_items['id']=$value['Product']['id'];
    $single_purchased_items['table_id']=$value['PurchasedItem']['id'];
    $single_purchased_items['mrp']=$value['Product']['mrp'];
    $single_purchased_items['wholesale_price']=$value['Product']['wholesale_price'];
    $single_purchased_items['tax']=$value['Product']['tax'];
    $single_purchased_items['cost']=$value['Product']['cost'];
    $single_purchased_items['ProductType']=$value['ProductType']['name'];
    $single_purchased_items['unit_price']=$value['PurchasedItem']['unit_price'];
    $single_purchased_items['quantity']=$value['PurchasedItem']['quantity'];
    if($value['Brand']['name'])
    {
      $brand_name=$value['Brand']['name'];
    }
    else
    {
      $brand_name='GENERAL';
    }
    $single_purchased_items['Brand']=$brand_name;
    $PurchasedItems_array[$single_purchased_items['id']]=$single_purchased_items;    
  }
  if($PurchasedItems_array)
  {
    $return['PurchasedItems']=$PurchasedItems_array;
    $return['result']='Success';  
  }
  else
  {
    $return['result']='Empty';   
  }
  echo json_encode($return);
  exit;
}
public function AccountHead_Option_ListBySubGroupId($sub_group_id)
{
  $AccountHeads=$this->AccountHead->find('all',array(
    'joins'=>array(
      array(
        'table'=>'parties',
        'alias'=>'Party',
        'type'=>'INNER',
        'conditions'=>array('AccountHead.id=Party.account_head_id')
      ),
    ),
    'conditions'=>array(
      'SubGroup.id'=>$sub_group_id,
    ),
    'fields'=>array(
      'AccountHead.id',
      'AccountHead.name',
      'Party.code',
    // 'AccountHead.name',
      'SubGroup.id',
    )
  ));
  $AccountHead_list=[];
  foreach ($AccountHeads as $key => $value) {
    $AccountHead_list[$value['AccountHead']['id']] = $value['AccountHead']['name'].' '.$value['Party']['code'];
  }
  return $AccountHead_list;
}
//print
public function fpdfold($id=null)
{
  require('fpdf/fpdf.php');
  $pdf = new FPDF('p', 'mm', [297, 210]);
  $purchase_item=$this->Purchase->find('first', array(
    "joins" => array(
      array(
        "table" => 'parties',
        "alias" => 'Party',
        "type" => 'inner',
        "conditions" => array('Party.account_head_id=Purchase.account_head_id'),
      ),
      array(
        "table" => 'states',
        "alias" => 'State',
        "type" => 'inner',
        "conditions" => array('State.id=Party.state_id'),
      ),
    ),
    'conditions' => array('Purchase.id'=>$id),
    'fields' => array(
      'Purchase.*',
      'Party.*',
      'AccountHead.name',
      'State.name',
      'State.code',
    )
  ));
  if($purchase_item['State']['code'] !="KL")
  {
    $Checkstate=0;
  }
  else
  {
    $Checkstate=1;
  }
  $name=$purchase_item['AccountHead']['name'];
  $invoice_no=$purchase_item['Purchase']['invoice_no'];
  $date=$purchase_item['Purchase']['date_of_delivered'];
  $Purchase_item_list = $this->PurchasedItem->find('all', array(
    'joins'=>array(
      array(
        "table" => 'units',
        "alias" => 'Unit',
        "type" => 'inner',
        "conditions" => array('Unit.id=Product.unit_id'),
      ),
    ),
    'conditions' => array('PurchasedItem.purchase_id'=>$id),
    'fields' => array(
      'PurchasedItem.*',
      'Product.id',
      'Product.name',
      'Product.hsn_code',
      'Unit.name',
    )
  ));
  function convert_number_to_words($number)
  {
    $hyphen      = '-';
    $conjunction = ' and ';
    $separator   = ', ';
    $negative    = 'negative ';
    $decimal     = ' point ';
    $dictionary  = array(
      0                   => 'zero',
      1                   => 'one',
      2                   => 'two',
      3                   => 'three',
      4                   => 'four',
      5                   => 'five',
      6                   => 'six',
      7                   => 'seven',
      8                   => 'eight',
      9                   => 'nine',
      10                  => 'ten',
      11                  => 'eleven',
      12                  => 'twelve',
      13                  => 'thirteen',
      14                  => 'fourteen',
      15                  => 'fifteen',
      16                  => 'sixteen',
      17                  => 'seventeen',
      18                  => 'eighteen',
      19                  => 'nineteen',
      20                  => 'twenty',
      30                  => 'thirty',
      40                  => 'fourty',
      50                  => 'fifty',
      60                  => 'sixty',
      70                  => 'seventy',
      80                  => 'eighty',
      90                  => 'ninety',
      100                 => 'hundred',
      1000                => 'thousand',
      1000000             => 'million',
      1000000000          => 'billion',
      1000000000000       => 'trillion',
      1000000000000000    => 'quadrillion',
      1000000000000000000 => 'quintillion'
    );
    if (!is_numeric($number)) {
      return false;
    }
    if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
      trigger_error(
        'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
        E_USER_WARNING
      );
      return false;
    }
    if ($number < 0) {
      return $negative . convert_number_to_words(abs($number));
    }
    $string = $fraction = null;
    if (strpos($number, '.') !== false) {
      list($number, $fraction) = explode('.', $number);
    }
    switch (true) {
      case $number < 21:
      $string = $dictionary[$number];
      break;
      case $number < 100:
      $tens   = ((int) ($number / 10)) * 10;
      $units  = $number % 10;
      $string = $dictionary[$tens];
      if ($units) {
        $string .= $hyphen . $dictionary[$units];
      }
      break;
      case $number < 1000:
      $hundreds  = $number / 100;
      $remainder = $number % 100;
      $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
      if ($remainder) {
        $string .= $conjunction . convert_number_to_words($remainder);
      }
      break;
      default:
      $baseUnit = pow(1000, floor(log($number, 1000)));
      $numBaseUnits = (int) ($number / $baseUnit);
      $remainder = $number % $baseUnit;
      $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
      if ($remainder) {
        $string .= $remainder < 100 ? $conjunction : $separator;
        $string .= convert_number_to_words($remainder);
      }
      break;
    }
    if (null !== $fraction && is_numeric($fraction)) {
      $string .= $decimal;
      $words = array();
      foreach (str_split((string) $fraction) as $number) {
        $words[] = $dictionary[$number];
      }
      $string .= implode(' ', $words);
    }
    return $string;
  }
  $Profile=$this->Global_Var_Profile['Profile'];
  $page=1;
  $total_page=1;
  $i=0;
  foreach ($Purchase_item_list as $key => $value) {
    $description = str_replace('"', "'", $value['Product']['name']);
    $description_length=strlen($description);
    $description_words=explode(' ', $description);
    $word_count=count($description_words);
    $k=0;
    $poped_array=[];
    for ($j=0; $j <$word_count; $j++) {
      $poped_array[]=array_pop($description_words);
      $product_name=implode($description_words,' ');
      $product_name_length=strlen($product_name);
      if($product_name_length<=42)
      {
        $poped_array=array_reverse($poped_array);
        $k++;
        $description_words=$poped_array;
        $word_count=count($description_words);
        $poped_array=[];
        $j=0;  
      }
    }
    if($k)
      $k--;
    $i+=$k;
    if($i+$k>=20)
    {
      $total_page++;
      $i=-1;
    }
    $i++;
  }
  $igst_x_position=0;
  if($Checkstate==0)
  {
    $igst_x_position=20;
  } 
  function header_section($pdf,$purchase_item,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position) 
  {
    $pdf->AddPage();
    $pdf->Image('profile/'.$Profile['logo'],5,5,-600);
    $image_line_width=40;
    $invoice_x_starting=$image_line_width;
$pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,0,100);
$pdf->rect(1, 1, 208, 295);
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$pdf->Cell(0,$invoice_pos,'COMMON INVOICE ',0,0,'C');
$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=15;
$pdf->SetFont('Arial','B',10);
$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
$pdf->SetFont('Arial','U',10);
$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
$pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
// $pdf->text(2,$gst_pos,'Your Gstin Number : '.$Profile['gst_tin_code']);
$pdf->text($gst_line_width+1,$gst_pos,'Transportation Mode:(Apply for Supply of Goods only)');
$gst_pos+=4;
// $pdf->text(2,$gst_pos,'Tax is Payable on Reverse Charge : (Yes/No)');
$pdf->text($gst_line_width+1,$gst_pos,'Veh.No :');
$gst_pos+=4;
$pdf->text(2,$gst_pos,'Your Invoice Serial Number : '.$purchase_item['Purchase']['invoice_no']);
$pdf->text($gst_line_width+1,$gst_pos,'Date & Time of Supply:');
$gst_pos+=4;
if(!$purchase_item['Purchase']['date_of_delivered'])
  $invoice_date=$purchase_item['Purchase']['date_of_purchase'];
else
  $invoice_date=$purchase_item['Purchase']['date_of_delivered'];
$pdf->text(2,$gst_pos,'Your Invoice Date : '.date("d-m-Y",strtotime($invoice_date)));
$pdf->text($gst_line_width+1,$gst_pos,'Place Of Supply:');
$pdf->Line(1, $gst_details_y_staring+17,209, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);
$pdf->Line(100, $gst_details_y_staring+17,100, $gst_details_y_staring+17+21+5); // vertical line
$pdf->text(35,$gst_pos+5,'Detail Of Receiver (Billed to)');
$pdf->SetXY(100, 27);
$pdf->Cell(0,$gst_pos+5,'Detail Of Consignee (Shipped to)',0,0,'C');
$pdf->Line(1, $gst_details_y_staring+17+5,209, $gst_details_y_staring+17+5); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$Consignee_lin_width=100+1;
$pdf->text(2,$customer_detail_startin,'Name : '.$name);
$pdf->text($Consignee_lin_width,$customer_detail_startin,'Name :');
$customer_detail_startin+=4;
$pdf->text(2,$customer_detail_startin,'Address : '.$purchase_item['Party']['place']);
$pdf->text($Consignee_lin_width,$customer_detail_startin,'Address :');
$customer_detail_startin+=4;
// $pdf->text(2,$customer_detail_startin,'State :'.$purchase_item['State']['name']);
// $pdf->text($Consignee_lin_width,$customer_detail_startin,'State :');
$customer_detail_startin+=4;
// $pdf->text(2,$customer_detail_startin,'State Code :'.$purchase_item['State']['code']);
// $pdf->text($Consignee_lin_width,$customer_detail_startin,'State Code :');
$customer_detail_startin+=4;
// $pdf->text(2,$customer_detail_startin,'GSTIN Number :'.$purchase_item['Party']['gstin']);
// $pdf->text($Consignee_lin_width,$customer_detail_startin,'GSTIN Number : ');
$pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+2;
// table head start
$table_length=135;
$pdf->text(2,$item_table_head_y_start+3,'SL');
$pdf->text(2,$item_table_head_y_start+7,'No');
$table_column=7;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+20,$item_table_head_y_start+5,'Description of Goods');
$table_column+=70+$igst_x_position;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
// $pdf->text($table_column+2+2,$item_table_head_y_start+3,'HSN');
$pdf->text($table_column+2+1.5,$item_table_head_y_start+6,'CODE');
// $pdf->text($table_column+2+2,$item_table_head_y_start+9,'(GST)');
$table_column+=16;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column+1,$item_table_head_y_start+6,'Qty');
$table_column+=7;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column+1,$item_table_head_y_start+6,'UOM');
$table_column+=9;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column+2,$item_table_head_y_start+6,'Rate');
$table_column+=12;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column+1,$item_table_head_y_start+6,'Discount');
$table_column+=14;
// $pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
// $pdf->text($table_column+2,$item_table_head_y_start+4,'Taxable');
// $pdf->text($table_column+2+1,$item_table_head_y_start+8,'Value');
// $table_column+=14;
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
// if($Checkstate==1)
// {
//   $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
//   $pdf->text($table_column+6,$item_table_head_y_start+3,'CGST');
//   $pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
//   $table_column+=8;
//   $pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
//   $pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
//   $table_column+=15;
//   $pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
//   $pdf->text($table_column+6,$item_table_head_y_start+3,'SGST');
//   $pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
//   $table_column+=8;
//   $pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
//   $pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
// }
// else
// {
//    $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,172+$igst_x_position, $gst_details_y_staring+17+5+21+4); // horizontal line
//    $pdf->text($table_column+6,$item_table_head_y_start+3,'IGST');
//    $pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
//    $table_column+=8;
//    $pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
//    $pdf->text($table_column+1,$item_table_head_y_start+8,'Amount');
//  }
$table_column+=15;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column+4,$item_table_head_y_start+6,'Total');
$pdf->Line(1, $gst_details_y_staring+17+5+21+10,209, $gst_details_y_staring+17+5+21+10); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length,209, $item_table_head_y_start+$table_length); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length+5,209, $item_table_head_y_start+$table_length+5); // horizontal line
$pdf->text(12,$item_table_head_y_start+$table_length+3,'Total');
}
function footer($pdf,$data,$purchase_item)
{
  $footer_start_y=218;
$pdf->Line(1, $footer_start_y,209, $footer_start_y); // horizontal line
$invoce_word_width=141;
$grand_total_length=30;
$pdf->Line($invoce_word_width, $footer_start_y,$invoce_word_width, $footer_start_y+$grand_total_length); // vertical line
$pdf->text(50,$footer_start_y+3,'Invoice Value (In Words)');
$actual_grand_total=$purchase_item['Purchase']['grand_total'];
$convert_number_to_words=strtoupper(convert_number_to_words(floatval($actual_grand_total)));
$pdf->text(5,$footer_start_y+3+10,$convert_number_to_words);
$pdf->Line($invoce_word_width+46, $footer_start_y,$invoce_word_width+46, $footer_start_y+$grand_total_length); // vertical line
$Total_pos=3;
$actual_total=$purchase_item['Purchase']['total'];
$pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Total');
//$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,$data['total']);
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,$actual_total);
$pdf->Line(1, $footer_start_y+4,209, $footer_start_y+4); // horizontal line
$Total_pos+=5;
// $pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Freight Charges');
// $pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,'0');
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
// $pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Looking and packing Charges');
// $pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,'0');
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
// $pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Insurance Charges');
// $pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,'0');
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Round Off');
//$other_value=round($data['total'])-$data['total'];
$other_value=$purchase_item['Purchase']['other_value'];
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,$other_value);
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
// pr($actual_grand_total); exit;
$pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Invoice Total');
//$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,$data['total']+$other_value);
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,$actual_grand_total);
$pdf->Line(1, $footer_start_y+$grand_total_length,209, $footer_start_y+$grand_total_length); // horizontal line
$tax_subject_y=$footer_start_y+$grand_total_length;
$tax_pos=5;
// $pdf->text($tax_pos,$tax_subject_y+3,'HSN/SAC');
// $tax_pos+=100;
// $pdf->Line($tax_pos-1, $tax_subject_y,$tax_pos-1, $tax_subject_y+19); // vertical line
// $pdf->text($tax_pos,$tax_subject_y+3,'Taxable Value');
$tax_pos+=35;
$pdf->Line($tax_pos-1, $tax_subject_y,$tax_pos-1, $tax_subject_y+24); // vertical line
// $pdf->text($tax_pos,$tax_subject_y+3,'Cgst Rate');
// $tax_pos+=15;
$pdf->Line($tax_pos-1, $tax_subject_y,$tax_pos-1, $tax_subject_y+24); // vertical line
// $pdf->text($tax_pos,$tax_subject_y+3,'Cgst Amount');
// $tax_pos+=20;
// $pdf->Line($tax_pos-1, $tax_subject_y,$tax_pos-1, $tax_subject_y+24); // vertical line
// $pdf->text($tax_pos,$tax_subject_y+3,'Sgst Rate');
$tax_pos+=15;
$pdf->Line($tax_pos-1, $tax_subject_y,$tax_pos-1, $tax_subject_y+24); // vertical line
// $pdf->text($tax_pos,$tax_subject_y+3,'Sgst Amount');
// $tax_pos+=20;
$pdf->Line(1, $tax_subject_y+5,209, $tax_subject_y+5); // horizontal line
$signature_area_y=$tax_subject_y;
$signature_length=18;
$signature_width=100;
$e=0;
$gst_amount=0;
// foreach ($data['gst']['cgst'] as $key => $value) {
//   $e++;
//   $tax_pos=5;
//   $pdf->text($tax_pos,$tax_subject_y+3+($e*5),$value['hsn_code']);
//   $tax_pos+=100;
//   // $pdf->text($tax_pos,$tax_subject_y+3+($e*5),$value['amount']);
//   $tax_pos+=35;
//   $pdf->text($tax_pos,$tax_subject_y+3+($e*5),$key);
//   $tax_pos+=15;
//   $pdf->text($tax_pos,$tax_subject_y+3+($e*5),$value['amount']);
//   $gst_amount+=$value['amount'];
//   $tax_pos+=20;
//   $pdf->text($tax_pos,$tax_subject_y+3+($e*5),$key);
//   $tax_pos+=15;
//   $pdf->text($tax_pos,$tax_subject_y+3+($e*5),$value['amount']);
//   $tax_pos+=20;
//   if($e==3)
//   {
//     break;
//   }
// }
$tax_pos=5+100;
$pdf->text($tax_pos,$tax_subject_y+3+20,'Total');
$tax_pos+=35+15;
// $pdf->text($tax_pos,$tax_subject_y+3+20,$gst_amount);
// $tax_pos+=20+15;
// $pdf->text($tax_pos,$tax_subject_y+3+20,$gst_amount);
// $pdf->Line(1, $signature_area_y+6,209, $signature_area_y+6); // horizontal line
// $pdf->SetXY(100, $signature_area_y);
// $pdf->Cell(0,5,'HSN/SAC',0,0,'');
// $pdf->Line($signature_width, $signature_area_y,$signature_width, $signature_area_y+$signature_length); // vertical line
$pdf->Line(1, $signature_area_y+$signature_length+1+5,209, $signature_area_y+$signature_length+1+5); // horizontal line
$condition_area_y=$signature_area_y+$signature_length+1+5;
$condition_length=30-6;
$condition_width=100;
$pdf->Line(1, $condition_area_y+6-1,209, $condition_area_y+6-1); // horizontal line
$pdf->text(25,$condition_area_y+3,'YOUR TERMS & CONDITIONS OF SALE');
$pdf->SetXY($condition_width, $condition_area_y);
$pdf->text(30+$condition_width,$condition_area_y+3,'YOUR COMPANY NAME');
$pdf->SetXY($condition_width, $condition_area_y);
$pdf->text($condition_width+2,$condition_area_y+13-4,'Signature');
$pdf->Line($condition_width, $condition_area_y-2+14,209, $condition_area_y-2+14); // horizontal line
$pdf->text($condition_width+35,$condition_area_y+18-2,'Autherized Signature');
$pdf->Line($condition_width, $condition_area_y+20-2,209, $condition_area_y+20-2); // horizontal line
$pdf->text($condition_width+2,$condition_area_y+24-2,'Name:');
$pdf->text($condition_width+2,$condition_area_y+24+4,'Designation :');
$pdf->Line($condition_width, $condition_area_y,$condition_width, $condition_area_y+$condition_length); // vertical line
}
header_section($pdf,$purchase_item,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position);
$total=0;
$item_table_head_y_start='85';
$table_length='128';
$cgst_grand_amount=0;
$sgst_grand_amount=0;
$igst_grand_amount=0;
$rate_grand_amount=0;
$discount_grand_amount=0;
$taxable_value_grand_amount=0;
$product_grand_total=0;
$count=count($Purchase_item_list);
$i=0;
foreach ($Purchase_item_list as $key => $value) {
// $gst['cgst'][floatval($value['PurchasedItem']['cgst'])]['amount']=0;
// $gst['sgst'][floatval($value['PurchasedItem']['sgst'])]['amount']=0;
// $gst['igst'][floatval($value['PurchasedItem']['igst'])]['amount']=0;
}
foreach($Purchase_item_list as $key => $value){
  $description = str_replace('"', "'", $value['Product']['name']);
// $hsn_code=$value['Product']['hsn_code'];
  $qty=floatval($value['PurchasedItem']['quantity']);
  $uom=$value['Unit']['name'];
  $rate=floatval($value['PurchasedItem']['unit_price']);
  $rate_grand_amount+=$rate;
  $discount=$value['PurchasedItem']['discount'];
  $rate_grand_amount+=$rate;
  $discount_grand_amount+=$discount;
// $taxable_value=floatval($value['PurchasedItem']['taxable_value']);
// $taxable_value_grand_amount+=$taxable_value;
  $net_value=floatval($value['PurchasedItem']['net_value']);
// $tax_value=floatval($value['PurchasedItem']['cgst_amount'])+floatval($value['PurchasedItem']['sgst_amount'])+floatval($value['PurchasedItem']['igst_amount']);
// $gst['cgst'][floatval($value['PurchasedItem']['cgst'])]['hsn_code']=$value['Product']['hsn_code'];
// $gst['sgst'][floatval($value['PurchasedItem']['sgst'])]['hsn_code']=$value['Product']['hsn_code'];
// $gst['igst'][floatval($value['PurchasedItem']['igst'])]['hsn_code']=$value['Product']['hsn_code'];
// $gst['cgst'][floatval($value['PurchasedItem']['cgst'])]['amount']+=floatval($value['PurchasedItem']['cgst_amount']);
// $gst['sgst'][floatval($value['PurchasedItem']['sgst'])]['amount']+=floatval($value['PurchasedItem']['sgst_amount']);
// $gst['igst'][floatval($value['PurchasedItem']['igst'])]['amount']+=floatval($value['PurchasedItem']['igst_amount']);
// $total+=$net_value+(floatval($value['PurchasedItem']['cgst_amount'])*2);
  $data=array(
    'total'=>$total,
  );
// $data['gst']=$gst;
  $product_total=floatval($value['PurchasedItem']['total']);
  $product_grand_total+=$product_total;
// $cgst_rate=floatval($value['PurchasedItem']['cgst']);
// $cgst_amount=floatval($value['PurchasedItem']['cgst_amount']);
// $cgst_grand_amount+=$cgst_amount;
// $sgst_rate=floatval($value['PurchasedItem']['sgst']);
// $sgst_amount=floatval($value['PurchasedItem']['sgst_amount']);
// $sgst_grand_amount+=$sgst_amount;
// $igst_rate=floatval($value['PurchasedItem']['igst']);
// $igst_amount=floatval($value['PurchasedItem']['igst_amount']);
// $igst_grand_amount+=$igst_amount;
  $item_pos=2;
  $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$key+1);
  $item_pos+=6;
  $description_length=strlen($description);
  $description_words=explode(' ', $description);
  $word_count=count($description_words);
  $k=0;
  $poped_array=[];
  for ($j=0; $j <$word_count; $j++) {
    $product_name=implode($description_words,' ');
    $poped_array[]=array_pop($description_words);
    $product_name_length=strlen($product_name);
    if($product_name_length<=40)
    {
      $pdf->text($item_pos,$item_table_head_y_start+7+(($i+$k)*6),$product_name);
      $poped_array=array_reverse($poped_array);
      $k++;
      $description_words=$poped_array;
      $word_count=count($description_words);
      $poped_array=[];
      $j=0;  
    }
  }
  if($k)
    $k--;
  $item_pos+=70+$igst_x_position;
  $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$hsn_code);
  $item_pos+=16;
  $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$qty);
  $item_pos+=7;
  $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$uom);
  $item_pos+=10;
  $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$rate);
  if($count==$i+1)
  {
    $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$rate_grand_amount);
  }
  $item_pos+=11;
  $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$discount);
  if($count==$i+1)
  {
    $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$discount_grand_amount);
  }
  $item_pos+=14;
  $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$taxable_value);
  if($count==$i+1)
  {
    $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$taxable_value_grand_amount);
  }
  $item_pos+=15;
  if($Checkstate==1)
  {
    $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$cgst_rate);
    $item_pos+=7;
    $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$cgst_amount);
    if($count==$i+1)
    {
      $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$cgst_grand_amount);
    }
    $item_pos+=15;
    $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$sgst_rate);
    $item_pos+=8;
    $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$sgst_amount);
    if($count==$i+1)
    {
      $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$sgst_grand_amount);
    }
  }
  else
  {
    $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_rate);
    $item_pos+=7;
    $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_amount);
    if($count==$i+1)
    {
      $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$igst_grand_amount);
    }
  }
  $item_pos+=15;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_rate);
  $item_pos+=2;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_amount);
  $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$product_total);
  if($count==$i+1)
  {
  //$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$igst_grand_amount);
    $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$product_grand_total);
  }
  $i+=$k;
  $count+=$k;
  if($i+$k>=20)
  {
    footer($pdf,$data,$purchase_item);
    $page++;
    header_section($pdf,$purchase_item,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position);
    $i=-1;
    $count-=20;
  }
  $i++;
// }
}
footer($pdf,$data,$purchase_item);
$pdf->Output();
exit;
}
//print
//print edit
public function fpdf($id=null)
{
  $Sale = $this->Sale->find('first', array(
    "joins" => array(
      array(
        "table" => 'customers',
        "alias" => 'Customer',
        "type" => 'inner',
        "conditions" => array('Customer.account_head_id=Sale.account_head_id'),
      ),
      array(
        "table" => 'customer_types',
        "alias" => 'CustomerType',
        "type" => 'inner',
        "conditions" => array('CustomerType.id=Customer.customer_type_id'),
      ),
    ),
    'conditions' => array('Sale.id'=>$id),
    'fields' => array(
      'Sale.*',
      'AccountHead.name',
      'CustomerType.name',
    )
  ));
  $name=$Sale['AccountHead']['name'];
  $invoice_no=$Sale['Sale']['invoice_no'];
  $date=$Sale['Sale']['date_of_delivered'];
  $SaleItem = $this->SaleItem->find('all', array(
    'conditions' => array('SaleItem.sale_id'=>$id),
    'fields' => array(
      'SaleItem.*',
      'Product.id',
      'Product.name',
    )
  ));
  require('tfpdf/tfpdf.php');
  $pdf = new tFPDF('p', 'mm', [297, 210]);
  define('FPDF_FONTPATH','tfpdf/font');
  function header_section($pdf,$name,$date,$invoice_no) {
    $pdf->SetFont('Arial','B',8.4);
    $pdf->AddPage();
    $pdf->Image('img/logo.png',0,5,-400);
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',10);
    $pdf->Text(148, 5, 'للتجارة المواد الغدائية  و احلويات');
    $pdf->Text(160, 9, "تجارة – استيراد – تصدير");
    $pdf->Text(152, 13, "For Food Stuff & Sweets Trading");
    $pdf->Text(152, 17, "Global Trade - Experts & Import");
    $pdf->SetFont('Arial','B',20);
    $pdf->SetTextColor(0,100,0);
// $pdf->Text(43, 13, "IDREES JOMA");
    $pdf->SetFont('Arial','B',12);
// $pdf->Text(43, 18, "Delivering Taste");
    $pdf->SetTextColor(200,00,00);
    $pdf->Text(10, 26, "Invoice No : ".$invoice_no);
    $pdf->SetTextColor(10,10,10);
    $pdf->SetFont('Arial','B',8);
    $head_table_x=28;
    $pdf->rect(150, $head_table_x-10, 55, 8);
    $pdf->Text(152, 21, "Customer:");
    $pdf->Text(152, 25, "No:");
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',10);
    $pdf->Text(195, 21, ":زبون");
    $pdf->Text(199, 25, ":لا");
    $pdf->rect(5, $head_table_x, 140, 8);
    $pdf->Text(10, 33, "M/S : ".$name );
    $pdf->Text(135, 33, ": M/S" );
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',10);
    $pdf->Text(80, 20, "السيولة النقدية/ائتمان فاتورة");
    $pdf->SetFont('Arial','B',12);
    $pdf->Text(80, 26, "CASH/CREDIT INVOICE");
    $pdf->rect(150, $head_table_x, 55, 8);
    $pdf->SetFont('Arial','B',9);
    $pdf->Text(152, 33, "Date : ".date('d-M-Y',strtotime($date)));
  // $pdf->AddFont('Arabic','','arabic.php');
  // $pdf->SetFont('Arabic','',1);
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',13);
    $pdf->Text(188, 33, ": تاريخ");
  // $strp_txt = stripslashes("تاريخ");
// $strp_txt = iconv('UTF-8', 'windows-1252', $strp_txt);
// $strp_txt = utf8_decode($strp_txt);
    $first_table_x=$head_table_x+13;
    $pdf->rect(5, $first_table_x, 200, 200);
    $slno_x=6;
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',8);
    $pdf->Text($slno_x, $first_table_x+5, "SlNo");
    $pdf->SetFont('Arial','B',7);
    $pdf->Text($slno_x, $first_table_x+5+5, "SlNo");
$pdf->Line($slno_x+7, $first_table_x,$slno_x+7, 200); // vertical line
$item_x=$slno_x+50;
$item_line_x=$item_x+100;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($item_x, $first_table_x+5, "وصف");
$pdf->SetFont('Arial','B',7);
$pdf->Text($item_x+50, $first_table_x+5, "Description");
// $pdf->Line($item_line_x, $first_table_x,$item_line_x, 200); // vertical line
// $tax_x=$item_line_x+20;
// $tax_line_x=$tax_x+15;
// $pdf->Text($tax_x, $first_table_x+5, "Tax");
$pdf->Line($item_line_x, $first_table_x,$item_line_x, 200); // vertical line
$qty_x=$item_line_x+4;
$qty_line_x=$qty_x+8;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($qty_x, $first_table_x+5, "كمية");
$pdf->SetFont('Arial','B',7);
$pdf->Text($qty_x, $first_table_x+5+5, "Qty");
$pdf->Line($qty_line_x, $first_table_x,$qty_line_x, 200); // vertical line
$unit_price_x=$qty_line_x+2;
$unit_price_line_x=$unit_price_x+15;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($unit_price_x, $first_table_x+5, "سعر الوحدة");
$pdf->SetFont('Arial','B',7);
$pdf->Text($unit_price_x, $first_table_x+5+5, "Unit Price");
$pdf->Line($unit_price_line_x, $first_table_x,$unit_price_line_x, 200); // vertical line
// $net_amount_x=$qty_line_x+2;
// $net_amount_line_x=$net_amount_x+16;
// $pdf->Text($net_amount_x, $first_table_x+5, "Net Amount");
// $pdf->Line($net_amount_line_x, $first_table_x,$net_amount_line_x, 200); // vertical line
// $tax_amount_x=$unit_price_x+1;
// $tax_amount_line_x=$tax_amount_x+15;
// $pdf->Text($tax_amount_x, $first_table_x+5, "Tax Amount");
// $pdf->Line($tax_amount_line_x, $first_table_x,$tax_amount_line_x, 200); // vertical line
$total_x=$unit_price_line_x+6;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($total_x, $first_table_x+5, "مجموع");
$pdf->SetFont('Arial','B',7);
$pdf->Text($total_x, $first_table_x+5+5, "Total");
$pdf->Line(5, $first_table_x+12, 205, $first_table_x+12); // horizontal line
$pdf->Line(5, 200, 205, 200); // horizontal line
$pdf->Line(5, 205, 205, 205); // horizontal line
$i=0;
}
function footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount)
{
  $pdf->SetFont('Arial','B',11);
$pdf->Line(185, 205,185, 215); // vertical line
$pdf->Text(190, 211, $grand_total);
$pdf->Text(10, 211, 'Total :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',11);
$pdf->Text(170, 211, ': مجموع');
$pdf->Line(5, 215, 205, 215); // horizontal line
// $pdf->Text(167, 212, "Discount  :    ".$discount."");
$net_value=$grand_total-$discount;
// $pdf->Text(167, 216, "Net Value :    ".$net_value."");
$footer_line=203;
$pdf->SetFont('Arial','B',8);
// $pdf->Text(10, $footer_line+35+(5*0), "E&OE");
// $pdf->Text(10, $footer_line+35+(5*1), "Terms of Delivery and payment if any");
// $pdf->Text(100, $footer_line+35+(5*2), "DECLARATION");
// $pdf->Text(10, $footer_line+35+(5*3), "Certified that all the particulars shown in the above Tax invoice are true and correct and that my/our Registration under KVAT Act 2003 is");
// $pdf->Text(10, $footer_line+35+(5*4), "valid as on the date of this Bill.");
// $pdf->Text(160, $footer_line+35+(5*5), "For <company name>");
// $pdf->Text(165, $footer_line+35+(5*6), "Autherised Signatory");
// $pdf->Text(10, $footer_line+35+(5*7), " * Original for the buyer for the purpose of claiming Input Tax Credit, Duplicate for the Transport Copy,Triplicate for the filling at the");
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',11);
$pdf->Text(185, 295, "تم الاستلام");
$pdf->Text(60, 295, "بائع");
$pdf->SetFont('Arial','B',8);
$pdf->Text(10, 295, "Salesman");
$pdf->Text(120, 295, "Recieved By");
}
header_section($pdf,$name,$date,$invoice_no);
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+36;
$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; foreach($SaleItem as $key=>$value) {
  $sl_no_v_x=8;
  $pdf->Text($sl_no_v_x, $first_table_x+11+(5*$i), $i+1);
  $product_name_v_x=$sl_no_v_x+10;
  $pdf->Text($product_name_v_x, $first_table_x+11+(5*$i), $value['Product']['name']);
// $tax_v_x=$product_name_v_x+100;
// $pdf->Text($tax_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['tax']);
  $quantity_v_x=$product_name_v_x+140;
  $pdf->Text($quantity_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['quantity']);
  $unit_price_v_x=$quantity_v_x+12;
  $pdf->Text($unit_price_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['unit_price']);
  $unit_price=$value['SaleItem']['unit_price'];
  $quantity=$value['SaleItem']['quantity'];
// $net_amount_v_x=$quantity_v_x+11;
// $pdf->Text($net_amount_v_x, $first_table_x+11+(5*$i), $unit_price*$quantity);
// $tax_amount_v_x=$net_amount_v_x+17;
// $pdf->Text($tax_amount_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['tax_amount']);
  $total_v_x=$unit_price_v_x+18;
  $pdf->Text($total_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['total']);
  $i++;
  if($i>31)
  {
    $i=0;
    footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
    header_section($pdf,$name,$date,$invoice_no);
  }
  $grand_amount+=$unit_price*$quantity;
  $grand_tax_amount+=$value['SaleItem']['tax_amount'];
  $grand_total+=$value['SaleItem']['total'];
  $discount=$grand_total-$Sale['Sale']['total'];
}
footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
$pdf->Output();
exit;
}
public function GetUnitLevel($unit_id)
{
  $this->Unit->unbindModel(array('hasMany' => array('Product')));
  $Unit=$this->Unit->find('first',array('fields'=>array('Unit.level'),'conditions'=>array('Unit.id'=>$unit_id)));
  echo json_encode($Unit);
  exit;
}
//print edit
public function get_last_purchase_ajax()
{
  $data=$this->request->data;
  $PurchasedItem=$this->PurchasedItem->find('all',array(
   "joins" => array(
   ),
   'conditions'=>array(
         //'Purchase.account_head_id'=>$data['party_id'],
     'PurchasedItem.product_id'=>$data['product'],
   ),
   'order' => array('Purchase.id DESC'),
   'limit'=>5,
   'fields'=>array(
    'Purchase.invoice_no',
    'Purchase.date_of_purchase',
    'PurchasedItem.unit_cost',
    'PurchasedItem.quantity',
    'PurchasedItem.quantity_mode',
    'Product.no_of_piece_per_unit',
    'Purchase.account_head_id',
  )
 ));

  $data['row']='';
  foreach ($PurchasedItem as $key => $value) 
  {
    $AccountHead=$this->AccountHead->findById($value['Purchase']['account_head_id']);
    if($value['PurchasedItem']['quantity_mode']==2){
     $quantity=$value['PurchasedItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
     $cost=$value['PurchasedItem']['unit_cost']*$value['Product']['no_of_piece_per_unit'];
     $unit="Cases";

   }
   else
   {
    $quantity=$value['PurchasedItem']['quantity'];
    $cost=$value['PurchasedItem']['unit_cost'];
    $unit="Pieces";

  }
  $data['row']= $data['row'].'<tr class="blue-pd">';
  $data['row']= $data['row'].'<td>'.$AccountHead['AccountHead']['name'].'</td>';
  $data['row']= $data['row'].'<td>'.$value['Purchase']['invoice_no'].'</td>';
  $data['row']= $data['row'].'<td>'.date('d-m-Y',strtotime($value['Purchase']['date_of_purchase'])).'</td>';
  $data['row']= $data['row'].'<td>'.floatval($cost).'</td>';
  $data['row']= $data['row'].'<td>'.$unit.'</td>';
  $data['row']= $data['row'].'<td>'.floatval($quantity).'</td>';
  $data['row']= $data['row'].'</tr>';
}
echo json_encode($data);
exit;
}
}