
<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
App::import('Controller', 'ProfitLoss');
App::import('Controller', 'Accounts');
ini_set('memory_limit', '1024M');
set_time_limit(0); 
class ReportsController extends AppController {
	public $scaffold;
	public $uses=[
	'Type',
	'MasterGroup',
	'Group',
	'SubGroup',
	'AccountHead',
	'Journal',
	'BankDetail',
	'Purchase',
	'PurchasedItem',
	'AssetPurchase',
	'Sale',
	'SaleItem',
	'Vendor',
	'Party',
	'CustomerType',
	'Customer',
	'SalesReturn',
	'SalesReturnItem',
	'PurchaseReturn',
	'PurchaseReturnItem',
	'StockLog',
	'Stock',
	'ProductType',
	'Product',
	'ExecutiveRouteMapping',
	'NoSale',
	'Executive',
	'StockTransfer',
	'State',
	'Unit',
	'DayRegister',
	'ClosingDay',
	'Warehouse',
	'Route',
	'CustomerGroup',
	'Brand',
	'ProductTypeBrandMapping',
	'Branch',
	'SaleTarget',
	'ExecutiveBonusDetails',
	'Staff',
	'Role',
	'Production',
	'Cheque',
	'ExpenseVatDetail',
	'ExpenseVat',
	'ExecutiveDenomination',
	'ExecutiveBonusCalculation',
	'ProductBonusDetail',
	'ExecutiveExpenseDetail',
	'AccMainGroup',
	'AccSubGroup',
	'SystemParameter'
	];
	public function ProfitLossReport()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Reports/ProfitLossReport'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		if(!$this->request->data)
		{
			$date=date('d-m-Y');
			$from_date=date('1-m-Y');
			$to_date=date('d-m-Y');
			$data['Reports']['from_date']=$from_date;
			$data['Reports']['to_date']=$to_date;
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			$this->request->data=$data;
			$ProfitLoss=$this->profit_loss_calculator($from_date,$to_date);
			$this->set('ProfitLoss',$ProfitLoss);
		}
	}
	public function RouteWiseProfitLossReport()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Reports/ProfitLossReport'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		if(!$this->request->data)
		{
			$date=date('d-m-Y');
			$from_date=date('1-m-Y');
			$to_date=date('d-m-Y');
			$routes=$this->Route->find('list',array('fields'=>array('id','name')));
			$this->set(compact('routes'));
			$data['Reports']['from_date']=$from_date;
			$data['Reports']['to_date']=$to_date;
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			$this->request->data=$data;
			$ProfitLoss=$this->profit_loss_calculator_route($from_date,$to_date,1);
			$this->set('ProfitLoss',$ProfitLoss);
		}
	}
	public function ProfitLossReport_ajax()
	{
		$from_date=$this->request->data['from_date'];
		$to_date=$this->request->data['to_date'];
		$from_date=date('Y-m-d',strtotime($from_date));
		$to_date=date('Y-m-d',strtotime($to_date));
		$route_id=null;
		if(!empty($this->request->data['route_id']))
			$route_id=$this->request->data['route_id'];
		if(empty($route_id))
		{
		$ProfitLoss=$this->profit_loss_calculator($from_date,$to_date,$route_id);
	    }
	    else
	    {
	    $ProfitLoss=$this->profit_loss_calculator_route($from_date,$to_date,$route_id);
	    }
		echo json_encode($ProfitLoss);
		exit;
	}
	public function StockCalculator($from_date,$to_date,$route_id=null)
	{
		$stock_account_head_id=19;
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$account_single_opening=[];
		$account_single_opening['debit']=0;
		$account_single_opening['credit']=0;
		$AccountHead=$this->AccountHead->findById($stock_account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$conditions=[];
		$conditions_Debit_opening=[];
		$conditions_Debit_opening['Journal.flag']=1;
		$conditions_Debit_opening['Journal.debit']=$stock_account_head_id;
		$conditions_Debit_opening['Journal.date <']=date('Y-m-d',strtotime($from_date));
		if(!empty($route_id))
		{
		$conditions_Debit_opening['Journal.route_id']=$route_id;	
		}
		$conditions_Credit_opening=[];
		$conditions_Credit_opening['Journal.flag']=1;
		$conditions_Credit_opening['Journal.credit']=$stock_account_head_id;
		$conditions_Credit_opening['Journal.date <']=date('Y-m-d',strtotime($from_date));
		if(!empty($route_id))
		{
		$conditions_Credit_opening['Journal.route_id']=$route_id;	
		}
		$conditions_Journal_Debit=[];
		$conditions_Journal_Debit['Journal.flag']=1;
		$conditions_Journal_Debit['Journal.debit']=$stock_account_head_id;
		$conditions_Journal_Debit['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
		if(!empty($route_id))
		{
		$conditions_Journal_Debit['Journal.route_id']=$route_id;	
		}
		$conditions_Journal_Credit=[];
		$conditions_Journal_Credit['Journal.flag']=1;
		$conditions_Journal_Credit['Journal.credit']=$stock_account_head_id;
		$conditions_Journal_Credit['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
		if(!empty($route_id))
		{
		$conditions_Journal_Credit['Journal.route_id']=$route_id;	
		}
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit_opening=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Debit_opening));
		$account_single_opening['debit']+=$Journal_Debit_opening['Journal']['total_amount'];
		$Journal_Credit_opening=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Credit_opening));
		$account_single_opening['credit']+=$Journal_Credit_opening['Journal']['total_amount'];
		$Journal_Debit=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Journal_Debit));
		$account_single['debit']+=$Journal_Debit['Journal']['total_amount'];
		$Journal_Credit=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Journal_Credit));
		$account_single['credit']+=$Journal_Credit['Journal']['total_amount'];
		if(!empty($route_id))
		{
			$Stock=$this->Stock->find('all',
				array(
				"joins"=>array(
					array(
						"table"=>'executives',
						"alias"=>'Executive',
						"type"=>'inner',
						"conditions"=>array('Executive.warehouse_id=Stock.warehouse_id'),
						),
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
						),
					array(
						"table"=>'routes',
						"alias"=>'Route',
						"type"=>'inner',
						"conditions"=>array('Route.id=ExecutiveRouteMapping.route_id'),
						),
					),
				'conditions'=>array(
					'Route.id'=>$route_id,
					),
				'fields'=>array(
					'Stock.quantity',
					'Product.cost',
					),
				));
			$total_cost=0;
			foreach ($Stock as $key_stock => $value_stock) {
				$total_cost+=$value_stock['Product']['cost']*$value_stock['Stock']['quantity'];
			}
			$return['open']=$total_cost+$account_single_opening['debit']-$account_single_opening['credit'];
			$return['close']=$total_cost;
		}
		else
		{
			$return['open']=floatval($AccountHead['AccountHead']['opening_balance']+$account_single_opening['debit']-$account_single_opening['credit']);
			$return['close']=floatval($AccountHead['AccountHead']['opening_balance']+$account_single_opening['debit']-$account_single_opening['credit']+$account_single['debit']-$account_single['credit']);
		}
		return $return;
	}
	public function CostCalculator($from_date,$to_date,$route_id=null)
	{
		$sale_account_head_id=6;
		$return['costValue']=0;
		$Sale_AccountHead=$this->AccountHead->findById($sale_account_head_id);
		$SaleValue=$this->General_Journal_Debit_N_Credit_function_route_cost_sale($sale_account_head_id,$from_date,$to_date,$route_id);
		$return['costValue']=$SaleValue['credit'];		
		return $return;
	}
	public function StockCalculatorProfit($from_date,$to_date,$route_id=null)
	{
		$stock_account_head_id=19;
		$final_product=22;
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$account_single_opening=[];
		$account_single_opening['debit']=0;
		$account_single_opening['credit']=0;
		$AccountHead=$this->AccountHead->findById($stock_account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$conditions=[];
		$conditions_Debit_opening=[];
		$conditions_Debit_opening['Journal.flag']=1;
		$conditions_Debit_opening['Journal.debit']=$stock_account_head_id;
		$conditions_Debit_opening['Journal.date <']=date('Y-m-d',strtotime($from_date));

		$conditions_Credit_opening=[];
		$conditions_Credit_opening['Journal.flag']=1;
		$conditions_Credit_opening['Journal.credit']=$stock_account_head_id;
		$conditions_Credit_opening['Journal.date <']=date('Y-m-d',strtotime($from_date));

		$conditions_Journal_Debit=[];
		$conditions_Journal_Debit['Journal.flag']=1;
		$conditions_Journal_Debit['Journal.debit']=$stock_account_head_id;
		$conditions_Journal_Debit['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];

		$conditions_Journal_Credit=[];
		$conditions_Journal_Credit['Journal.flag']=1;
		$conditions_Journal_Credit['Journal.credit']=$stock_account_head_id;
		$conditions_Journal_Credit['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];

		
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit_opening=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Debit_opening));
		$account_single_opening['debit']+=$Journal_Debit_opening['Journal']['total_amount'];
		$Journal_Credit_opening=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Credit_opening));
		$account_single_opening['credit']+=$Journal_Credit_opening['Journal']['total_amount'];
		$Journal_Debit=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Journal_Debit));
		$account_single['debit']+=$Journal_Debit['Journal']['total_amount'];
		$Journal_Credit=$this->Journal->find('first',array(
			'fields'=>array('Journal.total_amount'),
			'conditions'=>$conditions_Journal_Credit));
		$account_single['credit']+=$Journal_Credit['Journal']['total_amount'];
			$return['open']=floatval($AccountHead['AccountHead']['opening_balance']+$account_single_opening['debit']-$account_single_opening['credit']);
			$return['close']=floatval($AccountHead['AccountHead']['opening_balance']+$account_single_opening['debit']-$account_single_opening['credit']+$account_single['debit']-$account_single['credit']);
		return $return;
	}
	public function profit_loss_calculator($from_date,$to_date,$route_id=null)
	{
		$from_date=date('Y-m-d',strtotime($from_date));
		$to_date=date('Y-m-d',strtotime($to_date));
		$ProfitLoss['result']='Success';
		$ProfitLoss['Stock']=$this->StockCalculatorProfit($from_date,$to_date,$route_id);
		$ProfitLoss['Income']=$this->IncomeCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Expense']=$this->ExpenseCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Purchase']=$this->PurchaseCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Sale']=$this->SaleCalculator($from_date,$to_date,$route_id);
		$Product=$this->Product_id_list_function($from_date,$to_date);
	$GSTReport=['total'=>[]];
		$gst_in=0;
		$gst_out=0;
		$total_in=0;
		$total_out=0;
		$GSTReport['total']['out']=0;
		$GSTReport['total']['in']=0;
		$tax_amount['left']=0;
		$tax_amount['right']=0;
		$ProfitLoss['Expense']['IndirectExpense']['amount']+=$tax_amount['left'];
		$ProfitLoss['Income']['IndirectIncome']['amount']+=$tax_amount['right'];
		$IndirectExpense_gst_array['name']='tax';
		$IndirectExpense_gst_array['PrePaid']=0;
		$IndirectExpense_gst_array['paid']=0;
		$IndirectExpense_gst_array['Outstanding']=$tax_amount['left'];
		$IndirectExpense_gst_array['total']=$tax_amount['left'];
		$ProfitLoss['Expense']['IndirectExpense']['single']['TAX']=$IndirectExpense_gst_array;
		$IndirectIncome_gst_array['name']='tax';
		$IndirectIncome_gst_array['Received']=0;
		$IndirectIncome_gst_array['Advance']=0;
		$IndirectIncome_gst_array['Accrued']=$tax_amount['right'];
		$IndirectIncome_gst_array['Total']=$tax_amount['right'];
		$ProfitLoss['Income']['IndirectIncome']['single']['TAX']=$IndirectIncome_gst_array;
		$ProfitLoss['First']['Left']=0;
		$ProfitLoss['First']['Right']=0;
		$ProfitLoss['Second']['Left']=0;
		$ProfitLoss['Second']['Right']=0;
		$ProfitLoss['FirstTotal']['Left']=0;
		$ProfitLoss['FirstTotal']['Right']=0;
		$ProfitLoss['Gross']['Left']=0;
		$ProfitLoss['Gross']['Right']=0;
		$ProfitLoss['Net']['Right']=0;
		$ProfitLoss['Net']['Left']=0;
		$ProfitLoss['SecondTotal']['Left']=0;
		$ProfitLoss['SecondTotal']['Right']=0;
		$FirstLeft=$ProfitLoss['Purchase']['PurchaseValue']-$ProfitLoss['Purchase']['PurchaseReturn']+$ProfitLoss['Expense']['DirectExpense']['amount']+$ProfitLoss['Stock']['open']+$tax_amount['right'];
		$FirstLeft=number_format($FirstLeft,2,'.','');
		$FirstRight=abs($ProfitLoss['Sale']['SaleValue']+$ProfitLoss['Sale']['SalesReturn']+$ProfitLoss['Sale']['CreditNote'])+abs($ProfitLoss['Income']['DirectIncome']['amount'])+$ProfitLoss['Stock']['close']+$tax_amount['left'];
		$FirstRight=number_format($FirstRight,2,'.','');
		if($FirstLeft>$FirstRight)
		{
			$ProfitLoss['First']['Right']=number_format(($FirstLeft-$FirstRight),2,'.','');
			$ProfitLoss['Gross']['Left']=number_format(($FirstLeft-$FirstRight),2,'.','');
		}
		else
		{
			$ProfitLoss['First']['Left']=number_format(($FirstRight-$FirstLeft),2,'.','');
			$ProfitLoss['Gross']['Right']=number_format(($FirstRight-$FirstLeft),2,'.','');
		}
		$ProfitLoss['FirstTotal']['Left']+=$FirstLeft+$ProfitLoss['First']['Left'];
		$ProfitLoss['FirstTotal']['Right']+=$FirstRight+$ProfitLoss['First']['Right'];
		$SecondLeft=$ProfitLoss['Expense']['IndirectExpense']['amount']+$ProfitLoss['Gross']['Left'];
		$SecondLeft=number_format($SecondLeft,2,'.','');
		$SecondRight=abs($ProfitLoss['Income']['IndirectIncome']['amount'])+$ProfitLoss['Gross']['Right'];
		$SecondRight=number_format($SecondRight,2,'.','');
		if($SecondLeft>$SecondRight)
		{
			$ProfitLoss['Second']['Right']=number_format(($SecondLeft-$SecondRight),2,'.','');
			$ProfitLoss['Net']['Right']=number_format(($SecondLeft-$SecondRight),2,'.','');
		}
		else
		{
			$ProfitLoss['Second']['Left']=number_format(($SecondRight-$SecondLeft),2,'.','');
			$ProfitLoss['Net']['Left']=number_format(($SecondRight-$SecondLeft),2,'.','');
		}
		$ProfitLoss['SecondTotal']['Left']+=$ProfitLoss['Net']['Left']+$SecondLeft;
		$ProfitLoss['SecondTotal']['Right']+=$ProfitLoss['Net']['Right']+$SecondRight;
		return $ProfitLoss;
	}
	public function profit_loss_calculator_route($from_date,$to_date,$route_id=null)
	{
		$from_date=date('Y-m-d',strtotime($from_date));
		$to_date=date('Y-m-d',strtotime($to_date));
		$ProfitLoss['result']='Success';
		$ProfitLoss['CostofSale']=$this->CostCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Income']=$this->IncomeCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Expense']=$this->ExpenseCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Purchase']=$this->PurchaseCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Cost']=$this->PurchaseCalculator($from_date,$to_date,$route_id);
		$ProfitLoss['Sale']=$this->SaleCalculator($from_date,$to_date,$route_id);
		$Product=$this->Product_id_list_function($from_date,$to_date);
		$GSTReport=['total'=>[]];
		$gst_in=0;
		$gst_out=0;
		$total_in=0;
		$total_out=0;
		$GSTReport['total']['out']=0;
		$GSTReport['total']['in']=0;
		$tax_amount['left']=0;
		$tax_amount['right']=0;
		$ProfitLoss['Expense']['IndirectExpense']['amount']+=$tax_amount['left'];
		$ProfitLoss['Income']['IndirectIncome']['amount']+=$tax_amount['right'];
		$IndirectExpense_gst_array['name']='tax';
		$IndirectExpense_gst_array['PrePaid']=0;
		$IndirectExpense_gst_array['paid']=0;
		$IndirectExpense_gst_array['Outstanding']=$tax_amount['left'];
		$IndirectExpense_gst_array['total']=$tax_amount['left'];
		$ProfitLoss['Expense']['IndirectExpense']['single']['TAX']=$IndirectExpense_gst_array;
		$IndirectIncome_gst_array['name']='tax';
		$IndirectIncome_gst_array['Received']=0;
		$IndirectIncome_gst_array['Advance']=0;
		$IndirectIncome_gst_array['Accrued']=$tax_amount['right'];
		$IndirectIncome_gst_array['Total']=$tax_amount['right'];
		$ProfitLoss['Income']['IndirectIncome']['single']['TAX']=$IndirectIncome_gst_array;
		$ProfitLoss['First']['Left']=0;
		$ProfitLoss['First']['Right']=0;
		$ProfitLoss['Second']['Left']=0;
		$ProfitLoss['Second']['Right']=0;
		$ProfitLoss['FirstTotal']['Left']=0;
		$ProfitLoss['FirstTotal']['Right']=0;
		$ProfitLoss['Gross']['Left']=0;
		$ProfitLoss['Gross']['Right']=0;
		$ProfitLoss['Net']['Right']=0;
		$ProfitLoss['Net']['Left']=0;
		$ProfitLoss['SecondTotal']['Left']=0;
		$ProfitLoss['SecondTotal']['Right']=0;
		$FirstLeft=$ProfitLoss['CostofSale']['costValue']+$ProfitLoss['Expense']['DirectExpense']['amount']+$tax_amount['right'];
		$FirstLeft=number_format($FirstLeft,4,'.','');
		$FirstRight=$ProfitLoss['Sale']['SaleValue']-$ProfitLoss['Sale']['SalesReturn']+$ProfitLoss['Income']['DirectIncome']['amount']+$tax_amount['left'];
		$FirstRight=number_format($FirstRight,4,'.','');
		if($FirstLeft>$FirstRight)
		{
			$ProfitLoss['First']['Right']=number_format(($FirstLeft-$FirstRight),3,'.','');
			$ProfitLoss['Gross']['Left']=number_format(($FirstLeft-$FirstRight),3,'.','');
		}
		else
		{
			$ProfitLoss['First']['Left']=number_format(($FirstRight-$FirstLeft),3,'.','');
			$ProfitLoss['Gross']['Right']=number_format(($FirstRight-$FirstLeft),3,'.','');
		}
		$ProfitLoss['FirstTotal']['Left']+=$FirstLeft+$ProfitLoss['First']['Left'];
		$ProfitLoss['FirstTotal']['Right']+=$FirstRight+$ProfitLoss['First']['Right'];
		$SecondLeft=$ProfitLoss['Expense']['IndirectExpense']['amount']+$ProfitLoss['Gross']['Left'];
		$SecondRight=$ProfitLoss['Income']['IndirectIncome']['amount']+$ProfitLoss['Gross']['Right'];
		if($SecondLeft>$SecondRight)
		{
			$ProfitLoss['Second']['Right']=$SecondLeft-$SecondRight;
			$ProfitLoss['Net']['Right']=$SecondLeft-$SecondRight;
		}
		else
		{
			$ProfitLoss['Second']['Left']=$SecondRight-$SecondLeft;
			$ProfitLoss['Net']['Left']=$SecondRight-$SecondLeft;
		}
		$ProfitLoss['SecondTotal']['Left']+=$ProfitLoss['Net']['Left']+$SecondLeft;
		$ProfitLoss['SecondTotal']['Right']+=$ProfitLoss['Net']['Right']+$SecondRight;
		return $ProfitLoss;
	}
	public function ExpenseCalculator($from_date,$to_date,$route_id=null)
	{
		$group_id_indirect=12;
		$group_id_direct=11;
		$outstanding_group_id=20;
		$prepaid_group_id=5;
		$AccountingsController = new AccountingsController;
		$excluding_ids=['10'];
		$AccountingsController = new AccountingsController;
		$group=[$group_id_direct=>'DirectExpense',$group_id_indirect=>'IndirectExpense'];
		foreach ($group as $group_key => $group_value) {
		// $group='Expense';
		// $group_key=3;
		$All_Accounts_table_list=$this->AccountHead_Table_ListBySubGroupId($group_key);
		 // pr($All_Accounts_table_list);
		 // exit;
		$account_all=[];
		//$Total['Expense']=0;
		$Total['DirectExpense']=0;
		$Total['IndirectExpense']=0;
		$Total['PrePaid']=0;
		$Total['Outstanding']=0;
		foreach ($All_Accounts_table_list as $keyA => $valueA) {
			$single=[];
			if(!in_array($valueA['AccountHead']['sub_group_id'], $excluding_ids))
			{
				$single['SubGroup']=$valueA['AccSubGroup']['name'];
				if(!isset($account_all[$single['SubGroup']]))
				{
					//$account_all[$single['SubGroup']]['Expense']=0;
					$account_all[$single['SubGroup']]['DirectExpense']=0;
					$account_all[$single['SubGroup']]['IndirectExpense']=0;
					$account_all[$single['SubGroup']]['PrePaid']=0;
					$account_all[$single['SubGroup']]['Outstanding']=0;
					$account_all[$single['SubGroup']]['total']=0;
					$account_all[$single['SubGroup']]['paid']=0;
				}
				$name=explode(' ',$valueA['AccountHead']['name']);
				array_pop($name);
				$name=implode(' ', $name);
				$expense_function=$this->General_Journal_Debit_N_Credit_function1($valueA['AccountHead']['id'],$from_date,$to_date);
				if(date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($valueA['AccountHead']['created_at'])))
					$expense_function['debit']+=$valueA['AccountHead']['opening_balance'];
				$account_all[$single['SubGroup']][$group_value]+=round($expense_function['debit'],2);
				$account_all[$single['SubGroup']]['paid']      +=round($expense_function['debit'],2);
				$account_all[$single['SubGroup']]['total']     +=round($expense_function['debit'],2);
				$Total[$group_value]                           +=round($expense_function['debit'],2);
				$PrePaid=$this->AccountHead->findByName($name.' PREPAID');
				if(!empty($PrePaid))
				{
					$return_function=$this->General_Journal_Debit_N_Credit_function1($PrePaid['AccountHead']['id'],'0000-00-00',$to_date);
					$PrePaid_amount=0;
					$PrePaid_amount+=$return_function['debit']-$return_function['credit'];
					$account_all[$single['SubGroup']]['PrePaid']+=$PrePaid_amount;
				}
				$Outstanding=$this->AccountHead->findByName($name.' OUTSTANDING');
				if(!empty($Outstanding))
				{
					$Outstanding_amount=$Outstanding['AccountHead']['opening_balance'];
					$return_function=$this->General_Journal_Openging_Debit_N_Credit_function1($Outstanding['AccountHead']['id'],$from_date);						
					$Outstanding_amount+=$return_function['credit']-$return_function['debit'];
					$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
					$Journal_Debit=$this->Journal->find('first',array(
						'conditions'=>array(
							'debit'=>$Outstanding['AccountHead']['id'],
							// 'credit'=>$valueA['AccountHead']['id'],
							'flag=1',
							'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
						),
						'fields'=>array( 'Journal.total_amount'),
					));
					$Journal_Credit=$this->Journal->find('first',array(
						'conditions'=>array(
							'credit'=>$Outstanding['AccountHead']['id'],
							// 'debit'=>$valueA['AccountHead']['id'],
							'flag=1',
							'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
						),
						'fields'=>array( 'Journal.total_amount'),
					));
					$Outstanding_amount+=$Journal_Credit['Journal']['total_amount'];
					$Outstanding_amount-=$Journal_Debit['Journal']['total_amount'];
					// $account_all[$single['SubGroup']]['total']      +=round($Outstanding_amount,2);
					$account_all[$single['SubGroup']]['Outstanding']+=number_format($Outstanding_amount,2,'.','');
					// $account_all[$single['SubGroup']][$group_value] +=round($Outstanding_amount,2);	
					// $Total[$group_value]                            +=round($Outstanding_amount,2);	
				}
			}			
			
		}
		$return[$group_value]['amount']=number_format($Total[$group_value],2,'.','');
		$return[$group_value]['single']=$account_all;
		}
		return $return;	
	}
	public function ExpenseCalculatorForBalansheet($from_date,$to_date)
	{
		$group_id_indirect=13;
		$group_id_direct=12;
		$outstanding_group_id=20;
		$prepaid_group_id=5;
		$AccountingsController = new AccountingsController;
		$excluding_ids=['4','5','8',];
		$AccountingsController = new AccountingsController;
		$group=[$group_id_direct=>'DirectExpense',$group_id_indirect=>'IndirectExpense'];
		foreach ($group as $group_key => $group_value) {
			$All_Accounts_table_list=$AccountingsController->AccountHead_Table_ListByGroupId($group_key);
			$account_all=[];
			$Total['DirectExpense']=0;
			$Total['IndirectExpense']=0;
			$Total['PrePaid']=0;
			$Total['Outstanding']=0;
			foreach ($All_Accounts_table_list as $keyA => $valueA) {
				$single=[];
				if(!in_array($valueA['AccountHead']['id'], $excluding_ids))
				{
					$single['SubGroup']=$valueA['SubGroup']['name'];
					if(!isset($account_all[$single['SubGroup']]))
					{
						$account_all[$single['SubGroup']]['DirectExpense']=0;
						$account_all[$single['SubGroup']]['IndirectExpense']=0;
						$account_all[$single['SubGroup']]['PrePaid']=0;
						$account_all[$single['SubGroup']]['Outstanding']=0;
						$account_all[$single['SubGroup']]['total']=0;
						$account_all[$single['SubGroup']]['paid']=0;
					}
					$name=explode(' ',$valueA['AccountHead']['name']);
					array_pop($name);
					$name=implode(' ', $name);
					$expense_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id'],$from_date,$to_date);
					if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($valueA['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($valueA['AccountHead']['created_at'])))
					{
						$expense_function['debit']+=$valueA['AccountHead']['opening_balance'];
					}
					$account_all[$single['SubGroup']][$group_value]+=$expense_function['debit'];
					$account_all[$single['SubGroup']]['paid']+=      $expense_function['debit'];
					$account_all[$single['SubGroup']]['total']+=     $expense_function['debit'];
					$Total[$group_value]+=                           $expense_function['debit'];
					$PrePaid=$this->AccountHead->findByName($name.' PREPAID');
					if(!empty($PrePaid))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($PrePaid['AccountHead']['id'],$from_date,$to_date);
						$PrePaid_amount=$PrePaid['AccountHead']['opening_balance'];
						$PrePaid_amount+=$return_function['debit']-$return_function['credit'];
						$account_all[$single['SubGroup']]['PrePaid']+=$PrePaid_amount;
						$account_all[$single['SubGroup']]['total']-=  $PrePaid_amount;
						$Total['PrePaid']+=                           $PrePaid_amount;
					}
					$Outstanding=$this->AccountHead->findByName($name.' OUTSTANDING');
					if(!empty($Outstanding))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Outstanding['AccountHead']['id'],$from_date,$to_date);						
						$Outstanding_amount=$Outstanding['AccountHead']['opening_balance'];
						$Outstanding_amount+=$return_function['credit']-$return_function['debit'];
						$account_all[$single['SubGroup']]['Outstanding']+=$Outstanding_amount;
						$account_all[$single['SubGroup']]['paid']-=       $Outstanding_amount;
// $account_all[$single['SubGroup']]['total']+=      $Outstanding_amount;
						$Total['Outstanding']+=                           $Outstanding_amount;
					}				
				}
			}
			$return[$group_value]['amount']=$Total[$group_value];
// $return[$group_value]['amount']+=$Total['Outstanding'];
			//$return[$group_value]['amount']-=$Total['PrePaid'];
		}
		return $return;	
	}
	public function IncomeCalculator($from_date,$to_date,$route_id=null)
	{
		$group_id_indirect=8;
		$group_id_direct=13;
		$advance_group_id=21;
		$paccrued_group_id=6;
		$master_group_id=6;
		$excluding_ids=['7'];
		$AccountingsController = new AccountingsController;
		$group=[$group_id_indirect=>'IndirectIncome',$group_id_direct=>'DirectIncome'];
		foreach ($group as $group_key => $group_value) {
		// $group_key=5;
		// $group_value='IndirectIncome';
			$All_Accounts_table_list=$this->AccountHead_Table_ListBySubGroupId($group_key);
			$account_all=[];
			$Total['DirectIncome']=0;
			$Total['IndirectIncome']=0;
			$Total['Advance']=0;
			$Total['Accrued']=0;
			foreach ($All_Accounts_table_list as $keyA => $valueA) {
				$single=[];
				if(!in_array($valueA['AccountHead']['sub_group_id'], $excluding_ids))
				{
					$single['SubGroup']=$valueA['AccSubGroup']['name'];
					if(!isset($account_all[$single['SubGroup']]))
					{
					    $account_all[$single['SubGroup']]['DirectIncome']=0;
						$account_all[$single['SubGroup']]['IndirectIncome']=0;
						$account_all[$single['SubGroup']]['Advance']=0;
						$account_all[$single['SubGroup']]['Accrued']=0;
						$account_all[$single['SubGroup']]['Total']=0;
						$account_all[$single['SubGroup']]['Received']=0;
					}
					$name=explode(' ',$valueA['AccountHead']['name']);
					array_pop($name);
					$name=implode(' ', $name);
					$main_function=$this->General_Journal_Debit_N_Credit_function1($valueA['AccountHead']['id'],$from_date,$to_date);
					if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($valueA['AccountHead']['created_at'])))
					{
						$main_function['credit']+=$valueA['AccountHead']['opening_balance'];
					}
					$account_all[$single['SubGroup']][$group_value]+=$main_function['debit']-$main_function['credit'];
					$account_all[$single['SubGroup']]['Received']+= $main_function['debit']- $main_function['credit'];
					$account_all[$single['SubGroup']]['Total']+=    $main_function['debit']- $main_function['credit'];
					$Total[$group_value]+=                          $main_function['debit']- $main_function['credit'];
					$Advance=$this->AccountHead->findByName($name.' ADVANCE');
					if(!empty($Advance))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function1($Advance['AccountHead']['id'],$from_date,$to_date);
						$Advance_amount=$Advance['AccountHead']['opening_balance'];
						$Advance_amount+=$return_function['debit']-$return_function['credit'];
						$account_all[$single['SubGroup']]['Advance']+=$Advance_amount;
						$account_all[$single['SubGroup']]['Total']-=  $Advance_amount;
						$Total[$group_value]-=                        $Advance_amount;
					}
					$Accrued=$this->AccountHead->findByName($name.' ACCRUED');
					if(!empty($Accrued))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function1($Accrued['AccountHead']['id'],$from_date,$to_date);
						$Accrued_amount=$Accrued['AccountHead']['opening_balance'];
						$Accrued_amount+=$return_function['debit']-$return_function['credit'];
						$account_all[$single['SubGroup']]['Accrued']+=  $Accrued_amount;
						if($main_function['credit']<$Accrued_amount)
						{
							$account_all[$single['SubGroup']]['Total']+=$Accrued_amount;
							$Total[$group_value]+=                      $Accrued_amount;	
						}
						elseif($main_function['credit']==0 && $Accrued_amount!=0)
						{
							$account_all[$single['SubGroup']]['Total']+=$Accrued_amount;
							$Total[$group_value]+=                      $Accrued_amount;	
						}
					}
				}
			}
			$return[$group_value]['amount']=number_format($Total[$group_value],2,'.','');
			$return[$group_value]['single']=$account_all;
		}
		return $return;	
	}
	public function AccountHead_Table_ListBySubGroupId($group_id)
	{
		$AccountHeads=$this->AccountHead->find('all',array(
			'joins'=>[
				array(
					'table'=>'acc_sub_groups',
					'alias'=>'AccSubGroup',
					'type'=>'INNER',
					'conditions'=>array('AccSubGroup.id=AccountHead.acc_sub_group_id')
					),
				],
			'conditions'=>array(
				'AccSubGroup.id'=>$group_id,
			),
			'fields'=>array('AccountHead.*','AccSubGroup.*')
		));
		return $AccountHeads;
	}
	public function IncomeCalculatorForBalansheet($from_date,$to_date)
	{
		$group_id_indirect=15;
		$group_id_direct=14;
		$advance_group_id=21;
		$paccrued_group_id=6;
		$master_group_id=6;
		$excluding_ids=['6','7','9'];
		$AccountingsController = new AccountingsController;
		$group=[$group_id_indirect=>'IndirectIncome',$group_id_direct=>'DirectIncome'];
		foreach ($group as $group_key => $group_value) {
			$All_Accounts_table_list=$AccountingsController->AccountHead_Table_ListByGroupId($group_key);
			$account_all=[];
			$Total['DirectIncome']=0;
			$Total['IndirectIncome']=0;
			$Total['Advance']=0;
			$Total['Accrued']=0;
			foreach ($All_Accounts_table_list as $keyA => $valueA) {
				$single=[];
				if(!in_array($valueA['AccountHead']['id'], $excluding_ids))
				{
					$single['SubGroup']=$valueA['SubGroup']['name'];
					if(!isset($account_all[$single['SubGroup']]))
					{
						$account_all[$single['SubGroup']]['DirectIncome']=0;
						$account_all[$single['SubGroup']]['IndirectIncome']=0;
						$account_all[$single['SubGroup']]['Advance']=0;
						$account_all[$single['SubGroup']]['Accrued']=0;
						$account_all[$single['SubGroup']]['Total']=0;
						$account_all[$single['SubGroup']]['Received']=0;
					}
					$name=explode(' ',$valueA['AccountHead']['name']);
					array_pop($name);
					$name=implode(' ', $name);
					$main_function=$this->General_Journal_Debit_N_Credit_function($valueA['AccountHead']['id'],$from_date,$to_date);
					$main_function['credit']+=$valueA['AccountHead']['opening_balance'];
					$account_all[$single['SubGroup']][$group_value]+=$main_function['credit'];
					$account_all[$single['SubGroup']]['Received']+=  $main_function['credit'];
					$account_all[$single['SubGroup']]['Total']+=     $main_function['credit'];
					$Total[$group_value]+=                           $main_function['credit'];
					$Advance=$this->AccountHead->findByName($name.' ADVANCE');
					if(!empty($Advance))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Advance['AccountHead']['id'],$from_date,$to_date);
						$Advance_amount=$Advance['AccountHead']['opening_balance'];
						$Advance_amount+=$return_function['credit']-$return_function['debit'];
						$account_all[$single['SubGroup']]['Advance']+=$Advance_amount;
						$account_all[$single['SubGroup']]['Total']-=  $Advance_amount;
						$Total['Advance']+=                           $Advance_amount;
					}
					$Accrued=$this->AccountHead->findByName($name.' ACCRUED');
					if(!empty($Accrued))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($Accrued['AccountHead']['id'],$from_date,$to_date);
						$Accrued_amount=$Accrued['AccountHead']['opening_balance'];
						$Accrued_amount+=$return_function['debit']-$return_function['credit'];
						$account_all[$single['SubGroup']]['Accrued']+= $Accrued_amount;
						$account_all[$single['SubGroup']]['Received']-=$Accrued_amount;
// $account_all[$single['SubGroup']]['Total']+=   $Accrued_amount;
						$Total['Accrued']+=                            $Accrued_amount;
					}
				}
			}
			$return[$group_value]['amount']=$Total[$group_value];
// $return[$group_value]['amount']+=$Total['Accrued'];
			$return[$group_value]['amount']-=$Total['Advance'];
		}
		return $return;	
	}
	public function PurchaseCalculator($from_date,$to_date,$route_id=null)
	{
		$purchase_account_head_id=5;
		$purchase_return_account_head_id=7;
		$gst_on_purchase_account_head_id=8;
		$return['PurchaseValue']=0;
		$return['PurchaseReturn']=0;
		$return['DebitNote']=0;
		$sub_group_id=10;
		// $Purchase_AccountHead=$this->AccountHead->findById($purchase_account_head_id);
		$Purchase_AccountHead=$this->AccountHead->find('all',array(
			'joins'=>[
				array(
					'table'=>'acc_sub_groups',
					'alias'=>'AccSubGroup',
					'type'=>'INNER',
					'conditions'=>array('AccSubGroup.id=AccountHead.acc_sub_group_id')
					),
				],
			'conditions'=>array(
				'AccSubGroup.id'=>$sub_group_id,
			),
			'fields'=>array('AccountHead.name','AccountHead.id','AccountHead.opening_balance','AccountHead.created_at'),
		));
		// print_r($Purchase_AccountHead);die;
		$Purchase_Return_AccountHead=$this->AccountHead->findById($purchase_return_account_head_id);
		foreach ($Purchase_AccountHead as $key => $value) {
			$PurchaseValue=$this->General_Journal_Debit_N_Credit_function1($value['AccountHead']['id'],$from_date,$to_date);
			if(date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($value['AccountHead']['created_at'])))
				$PurchaseValue['debit']+=$value['AccountHead']['opening_balance'];
			$return['PurchaseValue']+=$PurchaseValue['debit'];
			//$return['PurchaseValue']-=$PurchaseValue['credit'];
			
		}
		
		$GstOnPurchase=$this->General_Journal_Debit_N_Credit_function1($gst_on_purchase_account_head_id,$from_date,$to_date);
		$return['GstOnPurchase']=$GstOnPurchase;
		$PurchaseReturn=$this->General_Journal_Debit_N_Credit_function1($purchase_return_account_head_id,$from_date,$to_date);
		if(date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($Purchase_Return_AccountHead['AccountHead']['created_at'])))
			$PurchaseReturn['credit']+=$Purchase_Return_AccountHead['AccountHead']['opening_balance'];
		$return['PurchaseReturn']=$PurchaseReturn['credit']-$PurchaseReturn['debit'];
		return $return;
	}
	public function SaleCalculator($from_date,$to_date,$route_id=null)
	{
		$sale_account_head_id=6;
		$sale_return_account_head_id=4;
		$gst_on_sale_account_head_id=9;
		//$credit_note_account_head_id=28;
		$return['SaleValue']=0;
		$return['SalesReturn']=0;
		$return['CreditNote']=0;
		$sub_group_id=7;
		$Sale_AccountHead=$this->AccountHead->findById($sale_account_head_id);
		$Sale_Return_AccountHead=$this->AccountHead->findById($sale_return_account_head_id);
		//$Credit_Note_AccountHead=$this->AccountHead->findById($credit_note_account_head_id);
		$Sale_AccountHead=$this->AccountHead->find('all',array(
			'joins'=>[
				array(
					'table'=>'acc_sub_groups',
					'alias'=>'AccSubGroup',
					'type'=>'INNER',
					'conditions'=>array('AccSubGroup.id=AccountHead.acc_sub_group_id')
					),
				],
			'conditions'=>array(
				'AccSubGroup.id'=>$sub_group_id,
			),
			'fields'=>array('AccountHead.name','AccountHead.id','AccountHead.opening_balance','AccountHead.created_at'),
		));
		//pr($Sale_AccountHead);
		//exit;
		foreach ($Sale_AccountHead as $key => $value) {
			$SaleValue=$this->General_Journal_Debit_N_Credit_function1($value['AccountHead']['id'],$from_date,$to_date);
			if(date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($value['AccountHead']['created_at'])))
				$SaleValue['credit']-=$value['AccountHead']['opening_balance'];
			$return['SaleValue']-=$SaleValue['credit'];
			
		}
		// pr($return['SaleValue']);
		// exit;
		// $SaleValue=$this->General_Journal_Debit_N_Credit_function1($sale_account_head_id,$from_date,$to_date);
		// if(date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($Sale_AccountHead['AccountHead']['created_at'])))
		// 	$SaleValue['credit']+=$Sale_AccountHead['AccountHead']['opening_balance'];
		$GstOnSale=$this->General_Journal_Debit_N_Credit_function1($gst_on_sale_account_head_id,$from_date,$to_date);
		//$return['SaleValue']=$SaleValue['credit'];
		$return['GstOnSale']=$GstOnSale;
		$SalesReturn=$this->General_Journal_Debit_N_Credit_function1($sale_return_account_head_id,$from_date,$to_date);
		if(date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($Sale_Return_AccountHead['AccountHead']['created_at'])))
			$SalesReturn['debit']+=$Sale_Return_AccountHead['AccountHead']['opening_balance'];
		$return['SalesReturn']=$SalesReturn['debit']-$SalesReturn['credit'];
		// $CreditNote=$this->General_Journal_Debit_N_Credit_function1($credit_note_account_head_id,$from_date,$to_date);
		// if(date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($Credit_Note_AccountHead['AccountHead']['created_at'])))
		// 	$CreditNote['debit']+=$Credit_note_AccountHead['AccountHead']['opening_balance'];
		// $return['CreditNote']=$CreditNote['debit']-$CreditNote['credit'];
		return $return;
	}
	public function General_Journal_Debit_N_Credit_function($account_head_id,$from_date,$to_date,$route_id=null)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$debit_conditions=[];
		$credit_conditions=[];
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$debit_conditions['Journal.debit']=$account_head_id;
		$debit_conditions['Journal.flag']=1;
		$debit_conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		if(!empty($route_id))
			$debit_conditions['Journal.route_id']=$route_id;
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>$debit_conditions,
			'fields'=>array( 'Journal.total_amount'),
			));
		$credit_conditions['Journal.credit']=$account_head_id;
		$credit_conditions['Journal.flag']=1;
		$credit_conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		if(!empty($route_id))
			$credit_conditions['Journal.route_id']=$route_id;
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>$credit_conditions,
			'fields'=>array( 'Journal.total_amount'),
			));
		$account_single['credit']=round($Journal_Credit['Journal']['total_amount'],3);
		$account_single['debit']=round($Journal_Debit['Journal']['total_amount'],3);
		return $account_single;
	}
	public function General_Journal_Debit_N_Credit_function_route($account_head_id,$from_date,$to_date,$route_id=null)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$debit_conditions=[];
		$credit_conditions=[];$sale=0;$sale_return=0;
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$debit_conditions['Journal.debit']=$account_head_id;
		$debit_conditions['Journal.flag']=1;
		$debit_conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		if(!empty($route_id))
		    $Customer=$this->Customer->find('all',array('conditions'=>array('Customer.route_id'=>$route_id),'fields'=>'Customer.account_head_id'));
		    foreach ($Customer as $key => $value) {
		    		$debit_conditions['Journal.credit']=$value['Customer']['account_head_id'];
		    	$Journal_Debit1=$this->Journal->find('first',array(
			'conditions'=>$debit_conditions,
			'fields'=>array( 'Journal.total_amount'),
			));
		    	$sale_return+=$Journal_Debit1['Journal']['total_amount'];
		    }
		$credit_conditions['Journal.credit']=$account_head_id;
		$credit_conditions['Journal.flag']=1;
		$credit_conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		if(!empty($route_id))
		    foreach ($Customer as $key => $value1) {
		    	$credit_conditions['Journal.debit']=$value1['Customer']['account_head_id'];
		    	$Journal_Credit1=$this->Journal->find('first',array(
			'conditions'=>$credit_conditions,
			'fields'=>array( 'Journal.total_amount'),
			));
		    	$sale+=$Journal_Credit1['Journal']['total_amount'];
		    }
		$account_single['credit']=round($sale,3);
		$account_single['debit']=round($sale_return,3);
		return $account_single;
	}
	public function General_Journal_Debit_N_Credit_function_route_cost_sale($account_head_id,$from_date,$to_date,$route_id=null)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$debit_conditions=[];
		$credit_conditions=[];$sale=0;$sale_return=0;
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		//$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		// $customer_condition=[];
		// if(!empty($route_id))
		// {
  //    $customer_condition['Customer.route_id']=$route_id;
		// }
  //        $Customer=$this->Customer->find('all',array('conditions'=>$customer_condition,'fields'=>'Customer.account_head_id'));
		   $Customer=$this->Customer->find('all',array('conditions'=>array('Customer.route_id'=>$route_id),'fields'=>'Customer.account_head_id'));
		$credit_conditions['Journal.credit']=$account_head_id;
		$credit_conditions['Journal.flag']=1;
		$credit_conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		if(!empty($route_id))
		    foreach ($Customer as $key => $value1) {
		    	$credit_conditions['Journal.debit']=$value1['Customer']['account_head_id'];
		    	$Journal_Credit=$this->Journal->find('all',array(
			'conditions'=>$credit_conditions,
			'fields'=>array( 'Journal.remarks'),
			));
		    	if(!empty($Journal_Credit))
		    	{
		    	foreach ($Journal_Credit as $key => $value2) {
		    		  $remarks=$value2['Journal']['remarks'];
                      // pr($remarks);
		    	$conditions=[];
		       $conditions['Journal.date between ? and ?']=array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		    	$conditions['Journal.flag']=1;
		    	$conditions['Journal.remarks']=$remarks;
		    	$conditions['Journal.credit']=19;
		    
		    	$Journal=$this->Journal->find('all',array(
			'conditions'=>$conditions,
			'fields'=>array( 'Journal.amount'),
			));
		    foreach ($Journal as $key => $value3) {
		    	$sale+=$value3['Journal']['amount'];
		    }

		    	}
		    	
		    		}
		    	}
//pr($Journal);
		 
		$account_single['credit']=round($sale,3);
		return $account_single;
	}
	public function General_Journal_Openging_Debit_N_Credit_function($account_head_id,$from_date)
	{	
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$account_head_id,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>array(
				'credit'=>$account_head_id,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$account_single['credit']=round($Journal_Credit['Journal']['total_amount'],3);
		$account_single['debit']=round($Journal_Debit['Journal']['total_amount'],3);
		return $account_single;
	}
		public function TrialBalanceNew()
	{
		$user_branch_id=$this->Session->read('User.branch_id');
		if(!$this->request->data)
		{
			$stock_account_head_id=19;
			$date=date('d-m-Y');
			//$from_date=date('d-m-Y',strtotime('-1 month'));
		$from_date=date('01-01-2021');
			$to_date=date('d-m-Y');
			$data['Reports']['from_date']=$from_date;
			$data['Reports']['to_date']=$to_date;
			$data['from_date']=$from_date;
			 $data['to_date']=$to_date;
			$this->request->data=$data;
		}
		
	}
	public function trial_balance_ajax()
	{
          $requestData=$this->request->data;
		$columns = [];
		$columns[]='AccMainGroup.name';
		$columns[]='AccMainGroup.id';
		$columns[]='AccMainGroup.id';
		$columns[]='AccMainGroup.id';
		$columns[]='AccMainGroup.id';
		$columns[]='AccMainGroup.id';
		$conditions=[];
		//$conditions['AccMainGroup.id']=1;
		$from_date=$requestData['from_date'];
		$to_date=$requestData['to_date'];

		$totalData=$this->AccMainGroup->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		$Data=$this->AccMainGroup->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'AccMainGroup.*',
				)
			));
		//pr($Data);
		foreach ($Data as $key => $value) {
			$type_id=$value['AccMainGroup']['id'];
			$Data[$key]['AccMainGroup']['opening_balance']=0;
			$Data[$key]['AccMainGroup']['amount']=0;
			$Data[$key]['AccMainGroup']['current']=0;
			$Data[$key]['AccMainGroup']['debit']=0;
			$Data[$key]['AccMainGroup']['credit']=0;
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_trial_balance($value['AccMainGroup']['id'],$from_date,$to_date);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function_trial_balance($value['AccMainGroup']['id'],$from_date,$to_date);
			if(in_array($type_id, ['1','3']))
	     	{
					$Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['opening_balance'];
					if($type_id=='1')
					{
						$Data[$key]['AccMainGroup']['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$Data[$key]['AccMainGroup']['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$Data[$key]['AccMainGroup']['current']+=number_format($function_value_return['debit'],2,'.','');
						$Data[$key]['AccMainGroup']['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					if($type_id=='3')
					{
						$Data[$key]['AccMainGroup']['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$Data[$key]['AccMainGroup']['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$Data[$key]['AccMainGroup']['current']+=number_format($function_value_return['debit'],2,'.','');
						$Data[$key]['AccMainGroup']['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['opening_balance'];
					$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['current'];
		    }
		    if($type_id=='5')
			{					
					$Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['opening_balance'];
					$Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['debit'];
					$Data[$key]['AccMainGroup']['opening_balance']-=$opening_function_value_return['credit'];
					$Data[$key]['AccMainGroup']['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['opening_balance'];
					$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['current'];
			}
			if($type_id=='2')
		     {
				    $Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['opening_balance'];
					$Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['debit'];
					$Data[$key]['AccMainGroup']['opening_balance']-=$opening_function_value_return['credit'];
					$Data[$key]['AccMainGroup']['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['opening_balance'];
				    $Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['current'];
		   }
		   if($type_id=='4')
		   {
					$Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['opening_balance'];
					$Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['debit'];
					$Data[$key]['AccMainGroup']['opening_balance']-=$opening_function_value_return['credit'];
					$Data[$key]['AccMainGroup']['current']+=$function_value_return['debit']-$function_value_return['credit'];			
					$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['opening_balance'];
					$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['current'];
		   }
			$opening_balance=number_format($Data[$key]['AccMainGroup']['opening_balance'],2,'.','');
			$Data[$key]['AccMainGroup']['opening_balances']=number_format($Data[$key]['AccMainGroup']['opening_balance'],2,'.','');
			if($opening_balance>0) $Data[$key]['AccMainGroup']['opening_balance']=$opening_balance.' Dr';
			elseif($opening_balance<0) $Data[$key]['AccMainGroup']['opening_balance']=abs($opening_balance).' Cr';
			else $Data[$key]['AccMainGroup']['opening_balance']=$opening_balance;
			$Data[$key]['AccMainGroup']['current']=number_format($Data[$key]['AccMainGroup']['current'],2,'.','');
			$amount=number_format($Data[$key]['AccMainGroup']['amount'],2,'.','');
			if($amount>0) $Data[$key]['AccMainGroup']['amount']=$amount.' Dr';
			elseif($amount<0) $Data[$key]['AccMainGroup']['amount']=abs($amount).' Cr';
			else $Data[$key]['AccMainGroup']['amount']=$amount;
			$Data[$key]['AccMainGroup']['credit']+=$function_value_return['credit'];
			$Data[$key]['AccMainGroup']['debit']+=$function_value_return['debit'];
			$Data[$key]['AccMainGroup']['credit']=number_format($Data[$key]['AccMainGroup']['credit'],2,'.','');
			$Data[$key]['AccMainGroup']['debit']=number_format($Data[$key]['AccMainGroup']['debit'],2,'.','');
			$Data[$key]['AccMainGroup']['name']='<span hidden class="type_name">'.$value['AccMainGroup']['name'].'</span><span hidden class="type_id">'.$value['AccMainGroup']['id'].'</span>'.$value['AccMainGroup']['name'];
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	
	}
	public function get_total_amount_trial_balance($sub_group_id,$from_date,$to_date)
	{
		$result=array();
			$conditions=[];
		$return=[];
		$conditions['AccSubGroup.id']=$sub_group_id;
		$return['status']="success";
		// $from_date="00-00-0000";
		// $to_date=date('Y-m-d',strtotime($to_date));
		// print_r($to_date);die;
		$Data=$this->AccSubGroup->find('all',array(
				'conditions'=>$conditions,
				'fields'=>array(
					'AccSubGroup.*',
					)
				));
		$total_amount=0;$total_debit=0;$total_credit=0;$total_opening=0;
			foreach ($Data as $key => $value) 
			{
			$type_id=$value['AccSubGroup']['main_group_id'];
				$single['opening_balance']=0;
				$single['amount']=0;
				$single['current']=0;
				$single['debit']=0;
				$single['credit']=0;
				$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_trial_balance_subgroup($value['AccSubGroup']['id'],$from_date,$to_date);
				$function_value_return=$this->General_Journal_Debit_N_Credit_function_trial_balance_subgroup($value['AccSubGroup']['id'],$from_date,$to_date);
						if(in_array($type_id, ['1','3']))
				{
						$single['opening_balance']+=$opening_function_value_return['opening_balance'];
						if($type_id=='1')
						{
							$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
							$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
							$single['current']+=number_format($function_value_return['debit'],2,'.','');
							$single['current']-=number_format($function_value_return['credit'],2,'.','');
						}
						if($type_id=='3')
						{
							$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
							$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
							$single['current']+=number_format($function_value_return['debit'],2,'.','');
							$single['current']-=number_format($function_value_return['credit'],2,'.','');
						}
						$single['amount']+=$single['opening_balance'];
						$single['amount']+=$single['current'];
				}
				if($type_id=='5')
				{					
						$single['opening_balance']+=$opening_function_value_return['opening_balance'];
						$single['opening_balance']+=$opening_function_value_return['debit'];
						$single['opening_balance']-=$opening_function_value_return['credit'];
						$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
						$single['amount']+=$single['opening_balance'];
						$single['amount']+=$single['current'];
				}
				if($type_id=='2')
				{
						$single['opening_balance']+=$opening_function_value_return['opening_balance'];
						$single['opening_balance']+=$opening_function_value_return['debit'];
						$single['opening_balance']-=$opening_function_value_return['credit'];
						$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
						$single['amount']+=$single['opening_balance'];
						$single['amount']+=$single['current'];
			}
			if($type_id=='4')
			{
						$single['opening_balance']+=$opening_function_value_return['opening_balance'];
						$single['opening_balance']+=$opening_function_value_return['debit'];
						$single['opening_balance']-=$opening_function_value_return['credit'];
						$single['current']+=$function_value_return['debit']-$function_value_return['credit'];			
						$single['amount']+=$single['opening_balance'];
						$single['amount']+=$single['current'];
			}
				// $debit+=$function_value_return['debit'];
				// $credit+=$function_value_return['credit'];
				$single['debit']=number_format($function_value_return['debit'],2,'.','');
				$single['credit']=number_format($function_value_return['credit'],2,'.','');
				$single['amount']=number_format($single['amount'],2,'.','');
				$single['opening_balance']=number_format($single['opening_balance'],2,'.','');
				$total_debit+=$single['debit'];
				$total_credit+=$single['credit'];
				$total_amount+=$single['amount'];
				$total_opening+=$single['opening_balance'];
			}
			if($total_amount>0){
					$total_amount=number_format($total_amount,2,'.','').' Dr';
				}else{
					$total_amount=abs(number_format($total_amount,2,'.','')).' Cr';
				}
		$result['opening']=number_format($total_opening,2,'.','');
		$result['debit']=number_format($total_debit,2,'.','');
		$result['credit']=number_format($total_credit,2,'.','');
		$result['total']=$total_amount;
		echo json_encode($result);
		exit;
	}
	public function get_total_amount($sub_group_id,$to_date)
{
	$result=array();
		$conditions=[];
	$return=[];
	$conditions['AccSubGroup.id']=$sub_group_id;
	$return['status']="success";
	$from_date="01-01-2021";
	$Data=$this->AccSubGroup->find('all',array(
			'conditions'=>$conditions,
			'fields'=>array(
				'AccSubGroup.*',
				)
			));
	$total_amount=0;
		foreach ($Data as $key => $value) 
		{
		   $type_id=$value['AccSubGroup']['main_group_id'];
			$single['opening_balance']=0;
			$single['amount']=0;
			$single['current']=0;
			$single['debit']=0;
			$single['credit']=0;
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_trial_balance_subgroup($value['AccSubGroup']['id'],$from_date,$to_date);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function_trial_balance_subgroup($value['AccSubGroup']['id'],$from_date,$to_date);
		    		if(in_array($type_id, ['1','3']))
	     	{
					$single['opening_balance']+=$opening_function_value_return['opening_balance'];
					if($type_id=='1')
					{
						$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single['current']+=number_format($function_value_return['debit'],2,'.','');
						$single['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					if($type_id=='3')
					{
						$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single['current']+=number_format($function_value_return['debit'],2,'.','');
						$single['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					$single['amount']+=$single['opening_balance'];
					$single['amount']+=$single['current'];
		    }
		    if($type_id=='5')
			{					
					$single['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single['opening_balance']+=$opening_function_value_return['debit'];
					$single['opening_balance']-=$opening_function_value_return['credit'];
					$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$single['amount']+=$single['opening_balance'];
					$single['amount']+=$single['current'];
			}
			if($type_id=='2')
		     {
				    $single['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single['opening_balance']+=$opening_function_value_return['credit'];
					$single['opening_balance']-=$opening_function_value_return['debit'];
					$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$single['amount']+=$single['opening_balance'];
				    $single['amount']+=$single['current'];
		   }
		   if($type_id=='4')
		   {
					$single['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single['opening_balance']+=$opening_function_value_return['credit'];
					$single['opening_balance']-=$opening_function_value_return['debit'];
					$single['current']+=$function_value_return['debit']-$function_value_return['credit'];			
					$single['amount']+=$single['opening_balance'];
					$single['amount']+=$single['current'];
		   }
				$single['amount']=number_format($single['amount'],2,'.','');
				$total_amount+=$single['amount'];
		}
		if($total_amount>0){
				$total_amount=number_format($total_amount,2,'.','').' Dr';
			}else{
				$total_amount=abs(number_format($total_amount,2,'.','')).' Cr';
			}
	$result=$total_amount;
	echo json_encode($result);
	exit;
}
	public function Trial_balance_subgroupwise()
	{
	$requestData=$this->request->data;
	$conditions=[];
	$return=[];
	$conditions['AccSubGroup.main_group_id']=$requestData['type_id'];
		//$conditions['AccSubGroup.id !=']=14;
	$return['status']="success";
	$type_id=$requestData['type_id'];
	$from_date=$requestData['from_date'];
    $to_date=$requestData['to_date'];
	$Data=$this->AccSubGroup->find('all',array(
		 'order' =>  array('AccSubGroup.name desc'),
			'conditions'=>$conditions,
			'fields'=>array(
				'AccSubGroup.*',
				)
			));
	$Subgroup=[];
		foreach ($Data as $key => $value) 
		{
			$single['sub_group_id']=$value['AccSubGroup']['id'];
			$single['name']=$value['AccSubGroup']['name'];
			$single['opening_balance']=0;
			$single['amount']=0;
			$single['current']=0;
			$single['debit']=0;
			$single['credit']=0;
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_trial_balance_subgroup($value['AccSubGroup']['id'],$from_date,$to_date);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function_trial_balance_subgroup($value['AccSubGroup']['id'],$from_date,$to_date);
		    if(in_array($type_id, ['1','3']))
	     	{
					$single['opening_balance']+=$opening_function_value_return['opening_balance'];
					if($type_id=='1')
					{
						$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single['current']+=number_format($function_value_return['debit'],2,'.','');
						$single['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					if($type_id=='3')
					{
						$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single['current']+=number_format($function_value_return['debit'],2,'.','');
						$single['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					$single['amount']+=$single['opening_balance'];
					$single['amount']+=$single['current'];
		    }
		    if($type_id=='5')
			{					
					$single['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single['opening_balance']+=$opening_function_value_return['debit'];
					$single['opening_balance']-=$opening_function_value_return['credit'];
					$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$single['amount']+=$single['opening_balance'];
					$single['amount']+=$single['current'];
			}
			if($type_id=='2')
		    {
				    $single['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single['opening_balance']+=$opening_function_value_return['credit'];
					$single['opening_balance']-=$opening_function_value_return['debit'];
					$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$single['amount']+=$single['opening_balance'];
				    $single['amount']+=$single['current'];
		   	}
			if($type_id=='4')
			{
				$single['opening_balance']+=$opening_function_value_return['opening_balance'];
				$single['opening_balance']+=$opening_function_value_return['credit'];
				$single['opening_balance']-=$opening_function_value_return['debit'];
				$single['current']+=$function_value_return['debit']-$function_value_return['credit'];			
				$single['amount']+=$single['opening_balance'];
				$single['amount']+=$single['current'];
			}
			$single['opening_balance']=number_format($single['opening_balance'],2,'.','');
			$single['current']=number_format($single['current'],2,'.','');
			$single['amount']=number_format($single['amount'],2,'.','');
			$single['credit']+=$function_value_return['credit'];
			$single['debit']+=$function_value_return['debit'];
			$single['credit']=number_format($single['credit'],2,'.','');
			$single['debit']=number_format($single['debit'],2,'.','');
			$single['name']=$value['AccSubGroup']['name'];
			$single['sub_group_id']=$value['AccSubGroup']['id'];
			$Subgroup[]=$single;
		}
		$return['Subgroup']=$Subgroup;
		
		echo json_encode($return);exit;
	
	}
	public function Trial_balance_accounthead()
	{
          $requestData=$this->request->data;
		$columns = [];
		$columns[]='AccountHead.name';
		$columns[]='AccountHead.id';
		$columns[]='AccountHead.id';
		$columns[]='AccountHead.id';
		$columns[]='AccountHead.id';
		$conditions=[];
		//$conditions['AccMainGroup.id']=1;
		$from_date=$requestData['from_date'];
		$to_date=$requestData['to_date'];
        $conditions['AccountHead.acc_sub_group_id']=$requestData['sub_group_id'];
	    $conditions['AccountHead.id !=']=19;
		$type_id=$requestData['type_id'];
		$totalData=$this->AccountHead->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		$Data=$this->AccountHead->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'AccountHead.*',
				)
			));
		//pr($Data);
		foreach ($Data as $key => $value) {
			$Data[$key]['AccountHead']['opening_balance']=0;
			$Data[$key]['AccountHead']['amount']=0;
			$Data[$key]['AccountHead']['current']=0;
			$Data[$key]['AccountHead']['debit']=0;
			$Data[$key]['AccountHead']['credit']=0;
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function1($value['AccountHead']['id'],$from_date,$to_date);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function1($value['AccountHead']['id'],$from_date,$to_date);
			if(in_array($type_id, ['1','3']))
					{     
						 $created_at = date('d-m-Y',strtotime($value['AccountHead']['created_at'])); 
						// if($to_date >= $created_at)
						// {
						$Data[$key]['AccountHead']['opening_balance']+=$value['AccountHead']['opening_balance'];
						// }
					if($type_id=='1')
					{
						$Data[$key]['AccountHead']['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$Data[$key]['AccountHead']['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$Data[$key]['AccountHead']['current']+=number_format($function_value_return['debit'],2,'.','');
						$Data[$key]['AccountHead']['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					if($type_id=='3')
					{
						$Data[$key]['AccountHead']['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$Data[$key]['AccountHead']['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$Data[$key]['AccountHead']['current']+=number_format($function_value_return['debit'],2,'.','');
						$Data[$key]['AccountHead']['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['opening_balance'];
					$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['current'];
		    }
		    if($type_id=='5')
			{					
				        $created_at = date('d-m-Y',strtotime($value['AccountHead']['created_at'])); 
						// if($to_date >= $created_at)
						// {
						$Data[$key]['AccountHead']['opening_balance']+=$value['AccountHead']['opening_balance'];
						// }
					$Data[$key]['AccountHead']['opening_balance']+=$opening_function_value_return['debit'];
					$Data[$key]['AccountHead']['opening_balance']-=$opening_function_value_return['credit'];
					$Data[$key]['AccountHead']['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['opening_balance'];
					$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['current'];
			}
			if($type_id=='2')
		     {
				     $created_at = date('d-m-Y',strtotime($value['AccountHead']['created_at'])); 
						// if($to_date >= $created_at)
						// {
						$Data[$key]['AccountHead']['opening_balance']+=$value['AccountHead']['opening_balance'];
						// }
					$Data[$key]['AccountHead']['opening_balance']+=$opening_function_value_return['credit'];
					$Data[$key]['AccountHead']['opening_balance']-=$opening_function_value_return['debit'];
					$Data[$key]['AccountHead']['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['opening_balance'];
				    $Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['current'];
		   }
		   if($type_id=='4')
		   {
					  $created_at = date('d-m-Y',strtotime($value['AccountHead']['created_at'])); 
						// if($to_date >= $created_at)
						// {
						$Data[$key]['AccountHead']['opening_balance']+=$value['AccountHead']['opening_balance'];
						// }
					$Data[$key]['AccountHead']['opening_balance']+=$opening_function_value_return['credit'];
					$Data[$key]['AccountHead']['opening_balance']-=$opening_function_value_return['debit'];
					$Data[$key]['AccountHead']['current']+=$function_value_return['debit']-$function_value_return['credit'];			
					$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['opening_balance'];
					$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['current'];
		   }
				$Data[$key]['AccountHead']['opening_balance']=number_format($Data[$key]['AccountHead']['opening_balance'],2,'.','');
				$Data[$key]['AccountHead']['current']=number_format($Data[$key]['AccountHead']['current'],2,'.','');
				$Data[$key]['AccountHead']['amount']=number_format($Data[$key]['AccountHead']['amount'],2,'.','');
				$Data[$key]['AccountHead']['credit']+=$function_value_return['credit'];
				$Data[$key]['AccountHead']['debit']+=$function_value_return['debit'];
				$Data[$key]['AccountHead']['credit']=number_format($Data[$key]['AccountHead']['credit'],2,'.','');
				$Data[$key]['AccountHead']['debit']=number_format($Data[$key]['AccountHead']['debit'],2,'.','');
				$Data[$key]['AccountHead']['name']='<span hidden class="hidden_AccountHead_id">'.$value['AccountHead']['id'].'</span>'.$value['AccountHead']['name'];
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	
	}
	public function General_Journal_Debit_N_Credit_function_trial_balance_subgroup($type_id,$from_date,$to_date)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$Journal_Debit_conditions= [];
		$Journal_Debit_conditions['AccountHeadDebit.acc_sub_group_id'] = $type_id;
		$Journal_Debit_conditions['AccountHeadDebit.id !='] =19;
		$Journal_Debit_conditions['credit !='] =19;
		$Journal_Debit_conditions['flag'] = '1';
		$Journal_Debit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		$Journal_Credit_conditions= [];
		$Journal_Credit_conditions['AccountHeadCredit.acc_sub_group_id'] = $type_id;
		$Journal_Credit_conditions['AccountHeadCredit.id !='] = 19;
		$Journal_Credit_conditions['debit !='] = 19;
		$Journal_Credit_conditions['flag'] = '1';
		$Journal_Credit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
			$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
						'conditions'=>$Journal_Debit_conditions,
						'fields'=>array( 'Journal.total_amount'),
					));
		$Journal_Credit=$this->Journal->find('first',array(
						'conditions'=>$Journal_Credit_conditions,
						'fields'=>array( 'Journal.total_amount'),
					));
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
	public function General_Journal_Openging_Debit_N_Credit_function_trial_balance_subgroup($type_id,$from_date,$to_date)
	{	
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>array(
				'AccountHeadDebit.acc_sub_group_id'=>$type_id,
				'credit !='=>19,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>array(
				'AccountHeadCredit.acc_sub_group_id'=>$type_id,
				'debit !='=>19,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$this->AccountHead->virtualFields = array('total_opening_balance' => "SUM(AccountHead.opening_balance)");
		$opening_balance=$this->AccountHead->find('first',array(
			'conditions'=>array(
				'AccountHead.acc_sub_group_id'=>$type_id,
				'AccountHead.id !='=>19,
				'AccountHead.created_at <='=>date('Y-m-d',strtotime($to_date)),
				),
			'fields'=>array( 'AccountHead.total_opening_balance'),
			));
		$account_single['opening_balance']=number_format($opening_balance['AccountHead']['total_opening_balance'],2,'.','');
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
	public function General_Journal_Debit_N_Credit_function_trial_balance($type_id,$from_date,$to_date)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$Journal_Debit_conditions= [];
		$Journal_Debit_conditions['AccMainGroup.id'] = $type_id;
		//$Journal_Debit_conditions['AccountHeadDebit.acc_sub_group_id !='] =14;
		$Journal_Debit_conditions['AccountHeadDebit.id !='] =19;
		$Journal_Debit_conditions['credit !='] =19;
		$Journal_Debit_conditions['flag'] = '1';
		$Journal_Debit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		$Journal_Credit_conditions= [];
		$Journal_Credit_conditions['AccMainGroup.id'] = $type_id;
		//$Journal_Debit_conditions['AccountHeadCredit.acc_sub_group_id !='] =14;
		$Journal_Credit_conditions['debit !='] = 19;
		$Journal_Credit_conditions['flag'] = '1';
		$Journal_Credit_conditions['AccountHeadCredit.id !='] =19;
		$Journal_Credit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
			$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'left',
                    "conditions"=>array('AccountHeadDebit.acc_sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'left',
                    "conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
                ),),
						'conditions'=>$Journal_Debit_conditions,
						'fields'=>array( 'Journal.total_amount'),
					));
		$Journal_Credit=$this->Journal->find('first',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'left',
                    "conditions"=>array('AccountHeadCredit.acc_sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'left',
                    "conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
                ),),
						'conditions'=>$Journal_Credit_conditions,
						'fields'=>array( 'Journal.total_amount'),
					));
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
	public function General_Journal_Openging_Debit_N_Credit_function_trial_balance($type_id,$from_date,$to_date)
	{	
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'left',
                    "conditions"=>array('AccountHeadDebit.acc_sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'left',
                    "conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
                ),),
			'conditions'=>array(
				'AccMainGroup.id'=>$type_id,
				//'AccountHeadDebit.acc_sub_group_id !='=>14,
				'AccountHeadDebit.id !='=>19,
				'credit !='=>19,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$Journal_Credit=$this->Journal->find('first',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'left',
                    "conditions"=>array('AccountHeadCredit.acc_sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'left',
                    "conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
                ),),
			'conditions'=>array(
			'AccMainGroup.id'=>$type_id,
			 'AccountHeadDebit.id !='=>19,
				//'AccountHeadCredit.acc_sub_group_id !='=>14,
				'debit !='=>19,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$this->AccountHead->virtualFields = array('total_opening_balance' => "SUM(AccountHead.opening_balance)");
		$opening_balance=$this->AccountHead->find('first',array(
				"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccountHead.acc_sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
                ),),
                'conditions'=>array(
				'AccMainGroup.id'=>$type_id,
				//'AccountHead.acc_sub_group_id !='=>14,
				'AccountHead.id !='=>19,
				'AccountHead.created_at <='=>date('Y-m-d',strtotime($to_date)),
				),
			'fields'=>array( 'AccountHead.total_opening_balance'),
			));
		$account_single['opening_balance']=number_format($opening_balance['AccountHead']['total_opening_balance'],2,'.','');
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
	public function ReportTrialBalance()
	{
		//$this->set('decimalpoint',$this->Session->read('decimalpoint'));
		$user_branch_id=$this->Session->read('User.branch_id');
		$conditions=[];
		//$this->set('decimalpoint',$this->Session->read('decimalpoint'));
		if(!$this->request->data)
		{
			$stock_account_head_id=19;
			$date=date('d-m-Y');
			//$from_date=date('d-m-Y',strtotime('-1 month'));
			$from_date=date('01-01-2021');
			$to_date=date('d-m-Y');
			$data['Reports']['from_date']=$from_date;
			$data['Reports']['to_date']=$to_date;
			$data['from_date']=$from_date;
			 $data['to_date']=$to_date;
			$this->request->data=$data;
		}
		$MainGroup=$this->AccMainGroup->find('all',array('order' =>  array('name asc'),'fields'=>array('id','name')));
		$this->set(compact('MainGroup'));
	}
	public function ReportTrialBalance_ajax()
	{
		$from_date=date('d-m-Y',strtotime($this->request->data['from_date']));
		$to_date=date('d-m-Y',strtotime($this->request->data['to_date']));
		$TrialBalance=$this->ReportTrialBalancecalculator($from_date,$to_date);
		$TrialBalance['Total']['Left']=$TrialBalance['Asset']+$TrialBalance['Expense'];
		$TrialBalance['Total']['Right']=$TrialBalance['Liabilities']+$TrialBalance['Capital']+$TrialBalance['Income'];
		echo json_encode($TrialBalance);
		exit;
	}
	public function ReportTrialBalancecalculator($from_date,$to_date)
	{
		$return['result']='empty';
		$Type_ids=[
		'1'=>'Asset',
		'2'=>'Capital',
		'3'=>'Expense',
		'4'=>'Income',
		'5'=>'Liabilities',
		];
		$return['Asset']['amount']=0;
		$return['Capital']['amount']=0;
		$return['Expense']['amount']=0;
		$return['Income']['amount']=0;
		$return['Liabilities']['amount']=0;
		foreach ($Type_ids as $id => $name) {
			$AccoutHead_list=$this->AccountHead_list_By_Type_Id($id);
			foreach ($AccoutHead_list as $key => $value) {
				$AccountHead=$this->AccountHead->find('first',[
					'joins'=>array(
						array(
							'table'=>'groups',
							'alias'=>'Group',
							'type'=>'INNER',
							'conditions'=>array('Group.id=SubGroup.group_id')
							),
						array(
							'table'=>'master_groups',
							'alias'=>'MasterGroup',
							'type'=>'INNER',
							'conditions'=>array('MasterGroup.id=Group.master_group_id')
							),
						array(
							'table'=>'types',
							'alias'=>'Type',
							'type'=>'INNER',
							'conditions'=>array('MasterGroup.type_id=Type.id')
							),
						),
					'conditions'=>array('AccountHead.id'=>$key),
					'fields'=>array(
						'AccountHead.id',
						'AccountHead.name',
						'AccountHead.opening_balance',
						'SubGroup.id',
						'SubGroup.name',
						'Group.id',
						'Group.name',
						'MasterGroup.id',
						'MasterGroup.name',
						'Type.id',
						'Type.name',
						),
					]);
				$return[$name]['amount']+=$AccountHead['AccountHead']['opening_balance'];
				$function_value_return=$this->General_Journal_Debit_N_Credit_function($key,$from_date,$to_date);
				if($name=='Asset')
				{
					$return[$name]['amount']+=$function_value_return['debit']-$function_value_return['credit'];
					$single_data['name']=$function_value_return['name'];
					$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
					$single_data['single_amount']+=$function_value_return['debit']-$function_value_return['credit'];
					$return[$name]['single'][]=$single_data;
				}
				if($name=='Liabilities')
				{
					$return[$name]['amount']+=$function_value_return['credit']-$function_value_return['debit'];
					$single_data['name']=$function_value_return['name'];
					$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
					$single_data['single_amount']+=$function_value_return['credit']-$function_value_return['debit'];
					$return[$name]['single'][]=$single_data;
				}
				if($name=='Income')
				{
					$return[$name]['amount']+=$function_value_return['credit'];
					$single_data['name']=$function_value_return['name'];
					$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
					$single_data['single_amount']+=$function_value_return['credit'];
					$return[$name]['single'][]=$single_data;
				}
				if($name=='Expense')
				{
					$return[$name]['amount']+=$function_value_return['debit'];
					$single_data['name']=$function_value_return['name'];
					$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
					$single_data['single_amount']+=$function_value_return['debit'];
					$return[$name]['single'][]=$single_data;
				}
				if($name=='Capital')
				{
					$AccountHead_Name=$AccountHead['AccountHead']['name'];
					$AccountHead_Name_split=explode(' ',$AccountHead_Name);
					if($AccountHead_Name_split[1]=='CAPITAL')
					{
						$return[$name]['amount']+=$function_value_return['credit']-$function_value_return['debit'];
						$single_data['name']=$function_value_return['name'];
						$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
						$single_data['single_amount']+=$function_value_return['credit']-$function_value_return['debit'];
						$return[$name]['single'][]=$single_data;
					}
					else
					{
						$return[$name]['amount']-=$function_value_return['debit']-$function_value_return['credit'];
						$single_data['name']=$function_value_return['name'];
						$single_data['single_amount']=$AccountHead['AccountHead']['opening_balance'];
						$single_data['single_amount']+=$function_value_return['debit']-$function_value_return['credit'];
						$return[$name]['single'][]=$single_data;
					}
				}
			}
			$return['result']='Success';
		}
		return $return;
	}
	public function AccountHead_list_By_Type_Id($type_id)
	{
		$AccountHead=$this->AccountHead->find('all',[
			'joins'=>array(
				array(
					'table'=>'groups',
					'alias'=>'Group',
					'type'=>'INNER',
					'conditions'=>array('Group.id=SubGroup.group_id')
					),
				array(
					'table'=>'master_groups',
					'alias'=>'MasterGroup',
					'type'=>'INNER',
					'conditions'=>array('MasterGroup.id=Group.master_group_id')
					),
				array(
					'table'=>'types',
					'alias'=>'Type',
					'type'=>'INNER',
					'conditions'=>array('MasterGroup.type_id=Type.id')
					),
				),
			'conditions'=>array('Type.id'=>$type_id),
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
// 'SubGroup.id',
// 'SubGroup.name',
// 'Group.id',
// 'Group.name',
// 'MasterGroup.id',
// 'MasterGroup.name',
				'Type.id',
				'Type.name',
				),
			]);
		$AccountHead_list=[];
		foreach ($AccountHead as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']] = $value['AccountHead']['name'];
		}
		return $AccountHead_list;
	}
	public function AccountHead_list_By_SubGroup_id($sub_group_id)
	{
		$data=$this->AccountHead->find('list',[
			'conditions'=>array('AccountHead.sub_group_id'=>$sub_group_id),
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
				),
			]);
		return $data;
	}
	public function BalanceSheet()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Reports/BalanceSheet'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		if(!$this->request->data)
		{
			$date=date('d-m-Y');
			$from_date=date('1-m-Y');
			$to_date=date('d-m-Y');
			//$from_date=date('d-m-Y',strtotime(date('Y').'-04-01'));
			//$to_date=date('d-m-Y',strtotime('+1 day'));
			$data['Reports']['from_date']=$from_date;
			$data['Reports']['to_date']=$to_date;
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			$this->request->data=$data;
		}
	}
	public function BalanceSheet_ajax()
	{
		$from_date=date('d-m-Y',strtotime($this->request->data['from_date']));
		$to_date=date('d-m-Y',strtotime($this->request->data['to_date']));
		$BalanceSheet=$this->ReportTrialBalancecalculator($from_date,$to_date);
		$ProfitLoss=$this->profit_loss_calculator($from_date,$to_date);
		$BalanceSheet['Profit']=$ProfitLoss['Net']['Left'];
		$BalanceSheet['Loss']=$ProfitLoss['Net']['Right'];
		$BalanceSheet['Total']['Left']=$BalanceSheet['Capital']+$BalanceSheet['Liabilities']+$ProfitLoss['Net']['Left']-$ProfitLoss['Net']['Right'];
		$BalanceSheet['Total']['Right']=$BalanceSheet['Asset'];
		echo json_encode($BalanceSheet);
		exit;
	}
	public function DayBook()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Reports/DayBook'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		if(!$this->request->data)
		{
			$from_date=date('d-m-Y');
			$to_date=date('d-m-Y');
			$this->request->data['Reports']['from_date']=$from_date;
			$this->request->data['Reports']['to_date']=$to_date;
		}
	}
	public function DayBook_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='Journal.date';
		$columns[]='Journal.voucher_no';
		$columns[]='AccountHeadCredit.name';
		$columns[]='Journal.work_flow';
		$columns[]='Journal.credit';
		$columns[]='Journal.debit';
		$conditions=[];
		$conditions['Journal.flag']=1;
		$from_date=date('Y-m-d',strtotime($requestData['from_date']));
		$to_date=date('Y-m-d',strtotime($requestData['to_date']));
		//$conditions['Journal.date <=']=$to_date;
		$conditions['Journal.date between ? and ?']=array($from_date,$to_date);
		$conditions['AND']=array(
				'Journal.debit !=' =>19,
				'Journal.credit !=' =>19,
				);
		$totalData=$this->Journal->find('count',['conditions'=>$conditions,'group' => ['Journal.voucher_no']]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Journal.date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
				'Journal.work_flow LIKE' =>'%'. $q . '%',
				'AccountHeadCredit.name LIKE' =>'%'. $q . '%',
				'AccountHeadDebit.name LIKE' =>'%'. $q . '%',
				'Journal.amount LIKE' =>'%'. $q . '%',
				'Journal.remarks LIKE' =>'%'. $q . '%',
				'Journal.voucher_no LIKE' =>'%'. $q . '%',
				);
			$totalFiltered=$this->Journal->find('count',[
				'conditions'=>$conditions,
				'group' => ['Journal.voucher_no'],
				]);
		}
		$Data=$this->Journal->find('all',array(
			'conditions'=>$conditions,
			'group' => ['Journal.voucher_no'],
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Journal.*',
				'AccountHeadCredit.name',
				'AccountHeadDebit.name',
				'AccountHeadCredit.acc_sub_group_id',
				'AccountHeadDebit.acc_sub_group_id',
				)
			));
		//pr($Data);
		foreach ($Data as $key => $value) {
			$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)",);
	$Journal_voucher_no=$this->Journal->findByVoucherNoAndFlag($value['Journal']['voucher_no'],'1',['Journal.total_amount']);
	$Journal_voucher_no=$this->Journal->find('first',array(
							'conditions'=>array(
							'AND' => array(
									// 'OR' => array(
									// 	'Journal.credit !=' =>19,
									// 	'Journal.debit !=' =>19,
									// ),
									'AND' => array(
										'Journal.debit!=19',
						                'Journal.credit!=19',
						                'Journal.debit!=13',
						                'Journal.credit!=13',
										'Journal.flag=1',
										'Journal.voucher_no'=>$value['Journal']['voucher_no'],
									)
								)
							),
							'fields'=>array(
								'Journal.total_amount',
							),
						));
	$Data[$key]['Journal']['amount']=0;
			if($Journal_voucher_no['Journal']['total_amount'])
			{
				$Data[$key]['Journal']['amount']=number_format($Journal_voucher_no['Journal']['total_amount'],2,'.','');	
			}
		$Data[$key]['AccountHeadCredit']['name']=$value['AccountHeadDebit']['name'];
		$Data[$key]['Journal']['debit']=$Data[$key]['Journal']['amount'];
		$Data[$key]['Journal']['credit']=0;
	    if(in_array($value['AccountHeadCredit']['acc_sub_group_id'], ['5','6']))
		{
			$Data[$key]['AccountHeadCredit']['name']=$value['AccountHeadDebit']['name'];
			$Data[$key]['Journal']['credit']=0;
			$Data[$key]['Journal']['debit']=number_format($Data[$key]['Journal']['amount'],2,'.','');
		}
		else if(in_array($value['AccountHeadDebit']['acc_sub_group_id'], ['5','6']))
		{
			$Data[$key]['AccountHeadCredit']['name']=$value['AccountHeadCredit']['name'];
			$Data[$key]['Journal']['credit']=number_format($Data[$key]['Journal']['amount'],2,'.','');
			$Data[$key]['Journal']['debit']=0;
		}
		else if($value['AccountHeadDebit']['acc_sub_group_id']==2)
		{
			$Data[$key]['AccountHeadCredit']['name']=$value['AccountHeadDebit']['name'];
			$Data[$key]['Journal']['credit']=0;
			$Data[$key]['Journal']['debit']=number_format($Data[$key]['Journal']['amount'],2,'.','');

		}
		else if($value['AccountHeadCredit']['acc_sub_group_id']==1)
		{
			$Data[$key]['AccountHeadCredit']['name']=$value['AccountHeadCredit']['name'];
			$Data[$key]['Journal']['debit']=0;
			$Data[$key]['Journal']['credit']=number_format($Data[$key]['Journal']['amount'],2,'.','');

		}
			$Data[$key]['Journal']['date']=date('d-m-Y',strtotime($value['Journal']['date']));
			$Data[$key]['Journal']['amount']=number_format($value['Journal']['amount'],2,'.','');
			$Data[$key]['Journal']['action']='<span hidden class="journal_id">'.$value['Journal']['id'].'</span><span><a><i class="fa fa-trash fa daybook_delete"></i></a></span>';
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	}
	public function Ledger()
	{
		$user_id=1;
		if(!$this->request->data)
		{
			$from_date=date('d-m-Y',strtotime('-1 month'));
			$to_date=date('d-m-Y');
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			$this->request->data=$data;
			$AccountHead=$this->AccountHead->find('list',array('order'=>['AccountHead.id DESC']));
			$this->set(compact('AccountHead'));
		}
	}
	public function StockPurchaseReport()
	{
		$ProductType=$this->ProductType->find('list');
		$this->set(compact('ProductType'));
		$Product=$this->Product->find('list');
		$this->set(compact('Product'));
	}
	public function Get_All_GSTReportForBalanceSheet($products,$from_date,$to_date,$route_id=null)
	{
		$single_GSTReport=[];
		$all_GSTReport=[];
		$single_GSTReport['tax']['out']=0;
		$single_GSTReport['tax']['in']=0;
		$this->PurchasedItem->virtualFields=['total_tax_amount'=>"SUM(PurchasedItem.tax_amount)"];
		$PurchasedItem=$this->PurchasedItem->find('first',array(
			'conditions'=>array(
				'Purchase.status=2',
				'Product.id'=>$products,
				'Purchase.date_of_delivered between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
				),
			'fields'=>array('PurchasedItem.total_tax_amount'),
			));
		$single_GSTReport['tax']['in']+=$PurchasedItem['PurchasedItem']['total_tax_amount'];;
		$this->PurchaseReturnItem->virtualFields=['total_tax_amount'=>"SUM(PurchaseReturnItem.tax_amount)"];
		$PurchaseReturnItem=$this->PurchaseReturnItem->find('first',array(
			'conditions'=>array(
				'PurchaseReturn.status=2',
				'Product.id'=>$products,
				'PurchaseReturn.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
				),
			'fields'=>array('PurchaseReturnItem.total_tax_amount'),
			));
		$single_GSTReport['tax']['out']+=$PurchaseReturnItem['PurchaseReturnItem']['total_tax_amount'];
		$this->SaleItem->virtualFields=['total_tax_amount'=>"SUM(SaleItem.tax_amount)"];
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.status=2',
				'Product.id'=>$products,
				'Sale.date_of_delivered between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
				),
			'fields'=>array('SaleItem.total_tax_amount'),
			));
		$single_GSTReport['tax']['out']+=$SaleItem['SaleItem']['total_tax_amount'];
		$this->SalesReturnItem->virtualFields=['total_tax_amount'=>"SUM(SalesReturnItem.tax_amount)"];
		$SalesReturnItem=$this->SalesReturnItem->find('first',array(
			'conditions'=>array(
				'SalesReturn.status=2',
				'Product.id'=>$products,
				'SalesReturn.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
				),
			'fields'=>array('SalesReturnItem.total_tax_amount'),
			));
		$single_GSTReport['tax']['in']+=$SalesReturnItem['SalesReturnItem']['total_tax_amount'];
		return $single_GSTReport;
	}
	public function GSTReport()
	{
		$data['GSTReport']['from_date']=$from_date=date('d-m-Y',strtotime('-1 month'));
		$data['GSTReport']['to_date']=$to_date=date('d-m-Y');
		$GST_RATE=array('0'=>'0 %','5'=>'5 %','12'=>'12 %','18'=>'18 %','Others'=>'Others %');
		$this->set(compact('GST_RATE'));
		$this->request->data=$data;
		$ProductType=$this->ProductType->find('list');
		$this->set(compact('ProductType'));
		$Product=$this->Product->find('list');
		$this->set(compact('Product'));
		$GSTReport=['list'=>[],'total'=>[]];
		$cgst_in=0;
		$cgst_out=0;
		$sgst_in=0;
		$sgst_out=0;
		$igst_in=0;
		$igst_out=0;
		$total_in=0;
		$total_out=0;
		foreach ($Product as $key => $value) {
			$single_GSTReport=$this->get_GSTReport($key,$from_date,$to_date);
			if($single_GSTReport)
			{
				foreach ($single_GSTReport as $keyG => $valueG) {
					if($valueG['total']['in'] || $valueG['total']['out'])
					{
						$cgst_out+=$valueG['cgst']['out'];
						$cgst_in+=$valueG['cgst']['in'];
						$sgst_out+=$valueG['sgst']['out'];
						$sgst_in+=$valueG['sgst']['in'];
						$igst_out+=$valueG['igst']['out'];
						$igst_in+=$valueG['igst']['in'];
						$GSTReport['list'][]=$valueG;
					}
				}
			}
			$GSTReport['total']['cgst']['in']=$cgst_in;
			$GSTReport['total']['cgst']['out']=$cgst_out;
			$GSTReport['total']['sgst']['in']=$sgst_in;
			$GSTReport['total']['sgst']['out']=$sgst_out;
			$GSTReport['total']['igst']['in']=$igst_in;
			$GSTReport['total']['igst']['out']=$igst_out;
			$GSTReport['total']['total']['in']=$cgst_in+$sgst_in+$igst_in;
			$GSTReport['total']['total']['out']=$cgst_out+$sgst_out+$igst_out;
		}
		$this->set(compact('GSTReport'));
	}
	public function GSTReport_ajax()
	{
		$data=$this->request->data;
		$conditions=[];
		if($data['product_type_id'])
		{
			$product_type_id=$data['product_type_id'];	
			$conditions['Product.product_type_id']=$product_type_id;
		}
		if($data['product_id'])
		{
			$product_id=$data['product_id'];	
			$conditions['Product.id']=$product_id;
		}
		$from_date=$data['from_date'];
		$to_date=$data['to_date'];
		$GSTReport=['list'=>[],'total'=>[]];
		$cgst_in=0;
		$cgst_out=0;
		$sgst_in=0;
		$sgst_out=0;
		$igst_in=0;
		$igst_out=0;
		$total_in=0;
		$total_out=0;
		$Product=$this->Product->find('all',array(
			'conditions'=>$conditions,
			));
		foreach ($Product as $key => $value) {
			$single_GSTReport=$this->get_GSTReport($key,$from_date,$to_date);
			if($single_GSTReport)
			{
				if($single_GSTReport['total']['in'] || $single_GSTReport['total']['out'])
				{
					$cgst_out+=$single_GSTReport['cgst']['out'];
					$cgst_in+=$single_GSTReport['cgst']['in'];
					$sgst_out+=$single_GSTReport['sgst']['out'];
					$sgst_in+=$single_GSTReport['sgst']['in'];
					$igst_out+=$single_GSTReport['igst']['out'];
					$igst_in+=$single_GSTReport['igst']['in'];
					$GSTReport['list'][]=$single_GSTReport;
				}
			}
			$GSTReport['total']['cgst']['in']=$cgst_in;
			$GSTReport['total']['cgst']['out']=$cgst_out;
			$GSTReport['total']['sgst']['in']=$sgst_in;
			$GSTReport['total']['sgst']['out']=$sgst_out;
			$GSTReport['total']['igst']['in']=$igst_in;
			$GSTReport['total']['igst']['out']=$igst_out;
			$GSTReport['total']['total']['in']=$cgst_in+$sgst_in+$igst_in;
			$GSTReport['total']['total']['out']=$cgst_out+$sgst_out+$igst_out;
		}
		echo json_encode($GSTReport);
		exit;
	}
	public function get_GSTReport($product_id,$from_date,$to_date,$gst=null,$type=null,$product_type_id=null)
	{
		$profile_state_id=$this->Global_Var_Profile['State']['id'];
		$Product=$this->Product->findById($product_id);
		$single_GSTReport=[];
		$all_GSTReport=[];
		$single_GSTReport['cgst']['out']=0;
		$single_GSTReport['cgst']['in']=0;
		$single_GSTReport['sgst']['out']=0;
		$single_GSTReport['sgst']['in']=0;
		$single_GSTReport['igst']['out']=0;
		$single_GSTReport['igst']['in']=0;
		$single_GSTReport['total']['out']=0;
		$single_GSTReport['total']['in']=0;
		if($Product)
		{
			$single_GSTReport['product']=$Product['Product']['name'];
			if($type=='Purchase' || !$type)
			{
				$conditions=[];
				$conditions['Purchase.status']=2;
				if($product_type_id)
				{
					$conditions['ProductType.id']=$product_type_id;
				}
				if($product_id)
				{
					$conditions['Product.id']=$product_id;
				}
				$conditions['Purchase.date_of_delivered between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
				if($gst)
				{
//$conditions['OR']['PurchasedItem.cgst']=$gst/2;
					$conditions['OR']['PurchasedItem.gst']=$gst;
				}
				if($gst=='Zero')
				{
					unset($conditions['OR']);
					$conditions['PurchasedItem.gst']=0;
//$conditions['PurchasedItem.gst']=0;
				}
//pr($conditions);exit;
				$PurchasedItem=$this->PurchasedItem->find('all',array(
					'joins'=>array(
						array(
							'table'=>'product_types',
							'alias'=>'ProductType',
							'type'=>'INNER',
							'conditions'=>array('ProductType.id=Product.product_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>array(
// 'PurchasedItem.cgst_amount',
// 'PurchasedItem.sgst_amount',
						'PurchasedItem.gst_amount',
						'PurchasedItem.gst',
// 'PurchasedItem.sgst',
// 'PurchasedItem.igst',
						'Purchase.date_of_delivered',
						'PurchasedItem.taxable_value',
						'Purchase.invoice_no',
						'Purchase.account_head_id',
						'Purchase.date_of_delivered',
						'ProductType.id',
						'Product.id',
						'ProductType.name',
						'Product.hsn_code',
						),
					));
				if($PurchasedItem)
				{
					foreach ($PurchasedItem as $keyP => $valueP) {
						$single_GSTReport['date']=date('d-m-Y',strtotime($valueP['Purchase']['date_of_delivered']));
						$single_GSTReport['product_type']=$valueP['ProductType']['name'];
						$single_GSTReport['hsn']=$valueP['Product']['hsn_code'];
						$single_GSTReport['type']='Purchase';
						$single_GSTReport['invoice_no']=$valueP['Purchase']['invoice_no'];
						$single_GSTReport['taxable_value']=$valueP['PurchasedItem']['taxable_value'];
						$account_head_id=$valueP['Purchase']['account_head_id'];
						$Party=$this->Party->findByAccountHeadId($account_head_id);
						$state_id=$Party['Party']['state_id'];
						if($profile_state_id==$state_id)
						{
							$single_GSTReport['cgst']['rate_percentage']=$valueP['PurchasedItem']['gst']/2;
							$single_GSTReport['cgst']['out']=$valueP['PurchasedItem']['gst_amount']/2;
							$single_GSTReport['sgst']['rate_percentage']=$valueP['PurchasedItem']['gst']/2;
							$single_GSTReport['sgst']['out']=$valueP['PurchasedItem']['gst_amount']/2;	
							$single_GSTReport['igst']['rate_percentage']=0;
							$single_GSTReport['igst']['out']=0;
						}
						else
						{
							$single_GSTReport['cgst']['rate_percentage']=0;
							$single_GSTReport['cgst']['out']=0;
							$single_GSTReport['sgst']['rate_percentage']=0;
							$single_GSTReport['sgst']['out']=0;
							$single_GSTReport['igst']['rate_percentage']=$valueP['PurchasedItem']['gst'];
							$single_GSTReport['igst']['out']=$valueP['PurchasedItem']['gst_amount'];
						}
						$single_GSTReport['total']['out']=$single_GSTReport['cgst']['out']+$single_GSTReport['sgst']['out']+$single_GSTReport['igst']['out'];
						$all_GSTReport[]=$single_GSTReport;
					}
				}
			}
			if($type=='Sale' || !$type)
			{
				$conditions=[];
				$conditions['Sale.status']=2;
				if($product_type_id)
				{
					$conditions['ProductType.id']=$product_type_id;
				}
				if($product_id)
				{
					$conditions['Product.id']=$product_id;
				}
				$conditions['Sale.date_of_delivered between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
				if($gst)
				{
//$conditions['OR']['SaleItem.cgst']=$gst/2;
					$conditions['OR']['SaleItem.gst']=$gst;
				}
				if($gst=='Zero')
				{
					unset($conditions['OR']);
//$conditions['SaleItem.cgst']=0;
					$conditions['SaleItem.gst']=0;
				}
				$SaleItem=$this->SaleItem->find('all',array(
					'joins'=>array(
						array(
							'table'=>'product_types',
							'alias'=>'ProductType',
							'type'=>'INNER',
							'conditions'=>array('ProductType.id=Product.product_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>array(
// 'SaleItem.cgst_amount',
// 'SaleItem.sgst_amount',
						'SaleItem.gst_amount',
// 'SaleItem.cgst',
// 'SaleItem.sgst',
						'SaleItem.gst',
						'SaleItem.taxable_value',
						'Sale.invoice_no',
						'Sale.account_head_id',
						'Sale.date_of_delivered',
						'ProductType.id',
						'ProductType.name',
						'Product.hsn_code',
						),
					));
// exit;
				if($SaleItem)
				{
					$single_GSTReport['cgst']['out']=0;
					$single_GSTReport['cgst']['in']=0;
					$single_GSTReport['sgst']['out']=0;
					$single_GSTReport['sgst']['in']=0;
					$single_GSTReport['igst']['out']=0;
					$single_GSTReport['igst']['in']=0;
					$single_GSTReport['total']['out']=0;
					$single_GSTReport['total']['in']=0;
					foreach ($SaleItem as $keyS => $valueS) {
						$single_GSTReport['date']=date('d-m-Y',strtotime($valueS['Sale']['date_of_delivered']));
						$single_GSTReport['product_type']=$valueS['ProductType']['name'];
						$single_GSTReport['hsn']=$valueS['Product']['hsn_code'];
						$single_GSTReport['type']='Sale';
						$single_GSTReport['invoice_no']=$valueS['Sale']['invoice_no'];
						$single_GSTReport['taxable_value']=$valueS['SaleItem']['taxable_value'];
						$account_head_id=$valueS['Sale']['account_head_id'];
						$Customer=$this->Customer->findByAccountHeadId($account_head_id);
						$state_id=$Customer['Customer']['state_id'];
						if($profile_state_id==$state_id)
						{
							$single_GSTReport['cgst']['rate_percentage']=$valueS['SaleItem']['gst']/2;
							$single_GSTReport['cgst']['in']=$valueS['SaleItem']['gst_amount']/2;
							$single_GSTReport['sgst']['rate_percentage']=$valueS['SaleItem']['gst']/2;
							$single_GSTReport['sgst']['in']=$valueS['SaleItem']['gst_amount']/2;	
							$single_GSTReport['igst']['rate_percentage']=0;
							$single_GSTReport['igst']['in']=0;
						}
						else
						{
							$single_GSTReport['cgst']['rate_percentage']=0;
							$single_GSTReport['cgst']['in']=0;
							$single_GSTReport['sgst']['rate_percentage']=0;
							$single_GSTReport['sgst']['in']=0;
							$single_GSTReport['igst']['rate_percentage']=$valueS['SaleItem']['gst'];
							$single_GSTReport['igst']['in']=$valueS['SaleItem']['gst_amount'];
						}
						$single_GSTReport['total']['in']=$single_GSTReport['cgst']['in']+$single_GSTReport['sgst']['in']+$single_GSTReport['igst']['in'];
						$all_GSTReport[]=$single_GSTReport;
					}
// 	if($SaleItem){
// pr($SaleItem);
// }
				}
			}
		}
		return $all_GSTReport;
	}
	public function get_VATReport($product_id,$from_date,$to_date,$gst=null,$type=null,$product_type_id=null)
	{
		$profile_state_id=$this->Global_Var_Profile['State']['id'];
		$Product=$this->Product->findById($product_id);
		$single_GSTReport=[];
		$all_GSTReport=[];
		$single_GSTReport['cgst']['out']=0;
		$single_GSTReport['cgst']['in']=0;
		$single_GSTReport['sgst']['out']=0;
		$single_GSTReport['sgst']['in']=0;
		$single_GSTReport['igst']['out']=0;
		$single_GSTReport['igst']['in']=0;
		$single_GSTReport['total']['out']=0;
		$single_GSTReport['total']['in']=0;
		if($Product)
		{
			$single_GSTReport['product']=$Product['Product']['name'];
			if($type=='Purchase' || !$type)
			{
				$conditions=[];
				$conditions['Purchase.status']=2;
				if($product_type_id)
				{
					$conditions['ProductType.id']=$product_type_id;
				}
				if($product_id)
				{
					$conditions['Product.id']=$product_id;
				}
				$conditions['Purchase.date_of_delivered between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
				if($gst)
				{
//$conditions['OR']['PurchasedItem.cgst']=$gst/2;
					$conditions['OR']['PurchasedItem.vat']=$gst;
				}
				if($gst=='Zero')
				{
					unset($conditions['OR']);
					$conditions['PurchasedItem.vat']=0;
//$conditions['PurchasedItem.gst']=0;
				}
//pr($conditions);exit;
				$PurchasedItem=$this->PurchasedItem->find('all',array(
					'joins'=>array(
						array(
							'table'=>'product_types',
							'alias'=>'ProductType',
							'type'=>'INNER',
							'conditions'=>array('ProductType.id=Product.product_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>array(
// 'PurchasedItem.cgst_amount',
// 'PurchasedItem.sgst_amount',
						'PurchasedItem.vat_amount',
						'PurchasedItem.vat',
// 'PurchasedItem.sgst',
// 'PurchasedItem.igst',
						'Purchase.date_of_delivered',
						'PurchasedItem.net_value',
						'Purchase.invoice_no',
						'Purchase.account_head_id',
						'Purchase.date_of_delivered',
						'ProductType.id',
						'Product.id',
						'ProductType.name',
						'Product.hsn_code',
						),
					));
				if($PurchasedItem)
				{
					foreach ($PurchasedItem as $keyP => $valueP) {
						$single_GSTReport['date']=date('d-m-Y',strtotime($valueP['Purchase']['date_of_delivered']));
						$single_GSTReport['product_type']=$valueP['ProductType']['name'];
						$single_GSTReport['hsn']=$valueP['Product']['hsn_code'];
						$single_GSTReport['type']='Purchase';
						$single_GSTReport['invoice_no']=$valueP['Purchase']['invoice_no'];
						$single_GSTReport['taxable_value']=$valueP['PurchasedItem']['net_value'];
						$account_head_id=$valueP['Purchase']['account_head_id'];
						$Party=$this->Party->findByAccountHeadId($account_head_id);
						$state_id=$Party['Party']['state_id'];
						$single_GSTReport['cgst']['rate_percentage']=0;
						$single_GSTReport['cgst']['out']=0;
						$single_GSTReport['sgst']['rate_percentage']=0;
						$single_GSTReport['sgst']['out']=0;
						$single_GSTReport['igst']['rate_percentage']=$valueP['PurchasedItem']['vat'];
						$single_GSTReport['igst']['out']=$valueP['PurchasedItem']['vat_amount'];
						$single_GSTReport['total']['out']=$single_GSTReport['cgst']['out']+$single_GSTReport['sgst']['out']+$single_GSTReport['igst']['out'];
						$all_GSTReport[]=$single_GSTReport;
					}
				}
			}
			if($type=='Sale' || !$type)
			{
				$conditions=[];
				$conditions['Sale.status']=2;
				if($product_type_id)
				{
					$conditions['ProductType.id']=$product_type_id;
				}
				if($product_id)
				{
					$conditions['Product.id']=$product_id;
				}
				$conditions['Sale.date_of_delivered between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
				if($gst)
				{
//$conditions['OR']['SaleItem.cgst']=$gst/2;
					$conditions['OR']['SaleItem.tax']=$gst;
				}
				if($gst=='Zero')
				{
					unset($conditions['OR']);
//$conditions['SaleItem.cgst']=0;
					$conditions['SaleItem.tax']=0;
				}
				$SaleItem=$this->SaleItem->find('all',array(
					'joins'=>array(
						array(
							'table'=>'product_types',
							'alias'=>'ProductType',
							'type'=>'INNER',
							'conditions'=>array('ProductType.id=Product.product_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>array(
// 'SaleItem.cgst_amount',
// 'SaleItem.sgst_amount',
						'SaleItem.tax_amount',
// 'SaleItem.cgst',
// 'SaleItem.sgst',
						'SaleItem.tax',
						'SaleItem.net_value',
						'Sale.invoice_no',
						'Sale.account_head_id',
						'Sale.date_of_delivered',
						'ProductType.id',
						'ProductType.name',
						'Product.hsn_code',
						),
					));
// exit;
				if($SaleItem)
				{
					$single_GSTReport['cgst']['out']=0;
					$single_GSTReport['cgst']['in']=0;
					$single_GSTReport['sgst']['out']=0;
					$single_GSTReport['sgst']['in']=0;
					$single_GSTReport['igst']['out']=0;
					$single_GSTReport['igst']['in']=0;
					$single_GSTReport['total']['out']=0;
					$single_GSTReport['total']['in']=0;
					foreach ($SaleItem as $keyS => $valueS) {
						$single_GSTReport['date']=date('d-m-Y',strtotime($valueS['Sale']['date_of_delivered']));
						$single_GSTReport['product_type']=$valueS['ProductType']['name'];
						$single_GSTReport['hsn']=$valueS['Product']['hsn_code'];
						$single_GSTReport['type']='Sale';
						$single_GSTReport['invoice_no']=$valueS['Sale']['invoice_no'];
						$single_GSTReport['taxable_value']=$valueS['SaleItem']['net_value'];
						$account_head_id=$valueS['Sale']['account_head_id'];
						$Customer=$this->Customer->findByAccountHeadId($account_head_id);
						$state_id=$Customer['Customer']['state_id'];
						$single_GSTReport['cgst']['rate_percentage']=0;
						$single_GSTReport['cgst']['in']=0;
						$single_GSTReport['sgst']['rate_percentage']=0;
						$single_GSTReport['sgst']['in']=0;
						$single_GSTReport['igst']['rate_percentage']=$valueS['SaleItem']['tax'];
						$single_GSTReport['igst']['in']=$valueS['SaleItem']['tax_amount'];
						$single_GSTReport['total']['in']=$single_GSTReport['cgst']['in']+$single_GSTReport['sgst']['in']+$single_GSTReport['igst']['in'];
						$all_GSTReport[]=$single_GSTReport;
					}
// 	if($SaleItem){
// pr($SaleItem);
// }
				}
			}
		}
		return $all_GSTReport;
	}
	public function stock_report_function()
	{
	}
	public function SaleItemWise()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Reports/SaleItemWise'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$this->set('from',date("d-m-Y", strtotime('-1 day')));
		$this->set('to',date("d-m-Y"));
		$this->set('Product_type',$this->ProductType->find('list',array('fields'=>array('id','name'))));
		$this->set('Product',$this->Product->find('list',array('fields'=>array('id','name'))));

	}
	public function SaleItemWise_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='Product.name';
		$columns[]='Sale.invoice_no';
		$columns[]='Sale.date_of_delivered';
		$columns[]='SaleItem.unit_price';
		$columns[]='SaleItem.quantity';
		$columns[]='SaleItem.tax_amount';
		$columns[]='SaleItem.total';
		$conditions=[];
		$from_date=date('Y-m-d',strtotime($requestData['from_date']));
		$to_date=date('Y-m-d',strtotime($requestData['to_date']));
		if(!empty($requestData['product_id']))
		{
			if($requestData['product_id'])
				$conditions['Product.id']=$requestData['product_id'];	
		}
		if(!empty($requestData['product_type_id']))
		{
			if($requestData['product_type_id'])
				$conditions['ProductType.id']=$requestData['product_type_id'];	
		}
		$conditions['Sale.flag']=1;
		$conditions['Sale.status']=2;
		$conditions['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
		$totalData=$this->Product->find('count',array(
			"joins"=>array(
				array(
					"table"=>'sale_items',
					"alias"=>'SaleItem',
					"type"=>'INNER',
					"conditions"=>array('SaleItem.product_id=Product.id'),
					),
				array(
					"table"=>'sales',
					"alias"=>'Sale',
					"type"=>'INNER',
					"conditions"=>array('Sale.id=SaleItem.sale_id'),
					),
				),
			'conditions'=>$conditions,
			));

		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Product.name LIKE' =>'%'. $q . '%',
				'Sale.invoice_no LIKE' =>'%'. $q . '%',
				'Sale.date_of_delivered LIKE' =>'%'. $q . '%',
				'SaleItem.unit_price LIKE' =>'%'. $q . '%',
				'SaleItem.tax_amount LIKE' =>'%'. $q . '%',
				'SaleItem.total LIKE' =>'%'. $q . '%',
				);
			$totalFiltered=$this->Product->find('count',array(
				"joins"=>array(
					array(
						"table"=>'sale_items',
						"alias"=>'SaleItem',
						"type"=>'INNER',
						"conditions"=>array('SaleItem.product_id=Product.id'),
						),
					array(
						"table"=>'sales',
						"alias"=>'Sale',
						"type"=>'INNER',
						"conditions"=>array('Sale.id=SaleItem.sale_id'),
						),
					),
				'conditions'=>$conditions,
				));
		}

		$Data=$this->Product->find('all',array(
			"joins"=>array(
				array(
					"table"=>'sale_items',
					"alias"=>'SaleItem',
					"type"=>'INNER',
					"conditions"=>array('SaleItem.product_id=Product.id'),
					),
				array(
					"table"=>'sales',
					"alias"=>'Sale',
					"type"=>'INNER',
					"conditions"=>array('Sale.id=SaleItem.sale_id'),
					),
				),
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Product.name',
				'Sale.id',
				'Sale.invoice_no',
				'Sale.date_of_delivered',
				'SaleItem.unit_price',
				'SaleItem.quantity',
				'SaleItem.total',
				'SaleItem.tax_amount',
				)
			));
//pr($Data);
		foreach ($Data as $key => $value) {
			$Data[$key]['Sale']['date_of_delivered']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
		}

		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        => $Data
			);
		echo json_encode($json_data);
		exit;

	}
	public function Summary()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Reports/Summary'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
// $from=date('Y-m-d', strtotime("-1 Year"));
		$from=date('Y-m-d', strtotime("now"));
		$to=date('Y-m-d', strtotime("now"));
		$SaleCompletedTotal=$this->SaleCompletedTotal($from,$to);
		$this->set('SaleTotal',$SaleCompletedTotal);
		$SaleSparePartTotal=$this->SaleSparePartTotal($from,$to);
		$this->set('SaleSparePartTotal',$SaleSparePartTotal);
		$SaleReturnSparePartTotal=$this->SaleReturnSparePartTotal($from,$to);
		$this->set('SaleReturnSparePartTotal',$SaleReturnSparePartTotal);
		$SaleReturnCompletedTotal=$this->SaleReturnCompletedTotal($from,$to);
		$this->set('SaleReturnTotal',$SaleReturnCompletedTotal);
// $LabourChargeTotal=$this->LabourChargeTotal($from,$to);
// $this->set('LabourChargeTotal',$LabourChargeTotal);
// $ServiceDiscountTotal=$this->ServiceDiscountTotal($from,$to);
// $this->set('ServiceAmountTotal',$ServiceDiscountTotal);
// $ServiceCompletedTotal=$this->ServiceCompletedTotal($from,$to);
// $this->set('ServiceTotal',$ServiceCompletedTotal);
// $ServiceCashTotal=$this->ServiceCashTotal($from,$to);
// $this->set('ServiceCashTotal',$ServiceCashTotal);
// $PainterCharge=$this->PainterCharge($from,$to);
// $this->set('PainterCharge',$PainterCharge);
// $ServiceSparePartTotal=$this->ServiceSparePartTotal($from,$to);
// $this->set('ServiceSparePartTotal',$ServiceSparePartTotal);
// $ExpanceAmountTotal=$this->ExpanceAmountTotal($from,$to);
//             $this->set('ExpanceAmountTotal',$ExpanceAmountTotal);
// $ExpanceTotal=$this->ExpanceTotal($from,$to); 
// $this->set('ExpanceTotal',$ExpanceTotal); 
// $IncomeTotal=$this->IncomeTotal($from,$to);
// $this->set('IncomeTotal',$IncomeTotal);
// $ServiceTotalProfit=$ServiceSparePartTotal['SparePartsListAmount']-$ServiceSparePartTotal['ProductCost']+$LabourChargeTotal-$ServiceDiscountTotal['ServiceDiscountTotal'];
// $this->set('ServiceTotalProfit',$ServiceTotalProfit);
		$PurchaseTotal=$this->PurchaseTotal($from,$to);
		$this->set('PurchaseTotal',$PurchaseTotal);
		$PurchaseReturnTotal=$this->PurchaseReturnTotal($from,$to);
		$this->set('PurchaseReturnTotal',$PurchaseReturnTotal);
// $AccountPayable=$this->AccountPayable($from,$to);
// $this->set('AccountPayable',$AccountPayable);
		$opening_balance=$this->opening_balance($from);
		$this->set('opening_balance',$opening_balance);
		$closing_balance=$this->closing_balance($from,$to);
		$this->set('closing_balance',$closing_balance);
		$ReceiptCash=$this->ReceiptCash($from,$to);
		$this->set('ReceiptCash',$ReceiptCash);
// $ServiceReceiptCash=$this->ServiceReceiptCash($from,$to);
// $this->set('ServiceReceiptCash',$ServiceReceiptCash);
		$PaymentCash=$this->PaymentCash($from,$to);
		$this->set('PaymentCash',$PaymentCash);
	}
	public function SummaryAjax()
	{
		$data=$this->request->data;
		$from=$data['from'];
		$to=$data['to'];
		$SaleCompletedTotal=$this->SaleCompletedTotal($from,$to);
		$return['SaleCompletedTotal']=$SaleCompletedTotal;
		$SaleSparePartTotal=$this->SaleSparePartTotal($from,$to);
		$return['SaleSparePartTotal']=$SaleSparePartTotal;
		$SaleReturnSparePartTotal=$this->SaleReturnSparePartTotal($from,$to);
		$return['SaleReturnSparePartTotal']=$SaleReturnSparePartTotal;
		$SaleReturnCompletedTotal=$this->SaleReturnCompletedTotal($from,$to);
		$return['SaleReturnTotal']=$SaleReturnCompletedTotal;
// $LabourChargeTotal=$this->LabourChargeTotal($from,$to);
// $return['LabourChargeTotal']=$LabourChargeTotal;
// $ServiceDiscountTotal=$this->ServiceDiscountTotal($from,$to);
// $return['ServiceAmountTotal']=$ServiceDiscountTotal;
// $ServiceCashTotal=$this->ServiceCashTotal($from,$to);
// $return['ServiceCashTotal']=$ServiceCashTotal;
// $PainterCharge=$this->PainterCharge($from,$to);
// $return['PainterCharge']=$PainterCharge;
// $ServiceCompletedTotal=$this->ServiceCompletedTotal($from,$to);
// $return['ServiceTotal']=$ServiceCompletedTotal;
// $ServiceSparePartTotal=$this->ServiceSparePartTotal($from,$to);
// $return['ServiceSparePartTotal']=$ServiceSparePartTotal;
// $ExpanceAmountTotal=$this->ExpanceAmountTotal($from,$to);
// $return['ExpanceAmountTotal']=$ExpanceAmountTotal;
// $ExpanceTotal=$this->ExpanceTotal($from,$to);
// $return['ExpanceTotal']=$ExpanceTotal;
// $IncomeTotal=$this->IncomeTotal($from,$to);
// $return['IncomeTotal']=$IncomeTotal;
// $ServiceTotalProfit=$ServiceSparePartTotal['SparePartsListAmount']-$ServiceSparePartTotal['ProductCost']+$LabourChargeTotal-$ServiceDiscountTotal['ServiceDiscountTotal'];
// $return['ServiceTotalProfit']=$ServiceTotalProfit;
		$PurchaseTotal=$this->PurchaseTotal($from,$to);
		$return['PurchaseTotal']=$PurchaseTotal;
		$PurchaseReturnTotal=$this->PurchaseReturnTotal($from,$to);
		$return['PurchaseReturnTotal']=$PurchaseReturnTotal;
// $AccountPayable=$this->AccountPayable($from,$to);
// $return['AccountPayable']=$AccountPayable;
		$opening_balance=$this->opening_balance($from);
		$return['opening_balance']=$opening_balance;
		$ReceiptCash=$this->ReceiptCash($from,$to);
		$return['ReceiptCash']=$ReceiptCash;
// $ServiceReceiptCash=$this->ServiceReceiptCash($from,$to);
// $return['ServiceReceiptCash']=$ServiceReceiptCash;
		$PaymentCash=$this->PaymentCash($from,$to);
		$return['PaymentCash']=$PaymentCash;
		echo json_encode($return);
		exit;
	}
	public function SaleCompletedTotal($from,$to)
	{
		$Sale=$this->Sale->find('all',array(
			'conditions'=>array(
				'Sale.status=2','Sale.flag=1',
				'Sale.date_of_delivered between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			));
		$grand_total=0;
		$paid_total=0;
		$topay_total=0;
		foreach ($Sale as $key => $value) {
			$grand_total+=$value['Sale']['total'];
		}
		$return['SaleCompletedTotal']=$grand_total;
		return $return;
	}
	public function SaleSparePartTotal($from,$to)
	{
		$Sale=$this->Sale->find('all',array(
			"joins"=>array(
				array(
					"table"=>'sale_items',
					"alias"=>'SaleItem',
					"type"=>'inner',
					"conditions"=>array('Sale.id=SaleItem.sale_id'),
					),
				array(
					"table"=>'products',
					"alias"=>'Product',
					"type"=>'inner',
					"conditions"=>array('Product.id=SaleItem.product_id'),
					),
				),
			'conditions'=>array(
				'Sale.status=2','Sale.flag=1',
				'Sale.date_of_delivered between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			'fields'=>array(
				'Sale.*',
				'SaleItem.quantity',
				'SaleItem.total',
				'Product.name',
				'Product.cost',
				)
			)
		);
// pr($SalesOffline);
		$SaleSparePartsAmount=0;
		$SaleProductCost=0;
		foreach ($Sale as $key => $value) {
			$SaleSparePartsAmount+=$value['SaleItem']['total'];
			$SparePartsListCostAmount=$value['Product']['cost']*$value['SaleItem']['quantity'];
			$SaleProductCost+=$SparePartsListCostAmount;
		}
		$return['SaleSparePartsAmount']=$SaleSparePartsAmount;
		$return['SaleProductCost']=$SaleProductCost;
		return $return;
	}
	public function SaleReturnCompletedTotal($from,$to)
	{
		$SalesReturn=$this->SalesReturn->find('all',array(
			'conditions'=>array(
				'SalesReturn.date between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			));
		$grand_total=0;
		foreach ($SalesReturn as $key => $value) {
			$grand_total+=$value['SalesReturn']['grand_total'];
		}
		return $grand_total;
	}
	public function SaleReturnSparePartTotal($from,$to)
	{
		$SalesReturn=$this->SalesReturn->find('all',array(
			"joins"=>array(
				array(
					"table"=>'sales_return_items',
					"alias"=>'SalesReturnItem',
					"type"=>'inner',
					"conditions"=>array('SalesReturn.id=SalesReturnItem.sales_return_id'),
					),
				array(
					"table"=>'products',
					"alias"=>'Product',
					"type"=>'inner',
					"conditions"=>array('Product.id=SalesReturnItem.product_id'),
					),
				),
			'conditions'=>array(
				'SalesReturn.date between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			'fields'=>array(
				'SalesReturn.*',
				'SalesReturnItem.quantity',
				'SalesReturnItem.total',
				'Product.name',
				'Product.cost',
				)
			)
		);
		$SaleReturnSparePartsAmount=0;
		$SaleReturnProductCost=0;
		foreach ($SalesReturn as $key => $value) {
			$SaleReturnSparePartsAmount+=$value['SalesReturnItem']['total'];
			$SparePartsListCostAmount=$value['Product']['cost']*$value['SalesReturnItem']['quantity'];
			$SaleReturnProductCost+=$SparePartsListCostAmount;
		}
		$return['SaleReturnSparePartsAmount']=$SaleReturnSparePartsAmount;
		$return['SaleReturnProductCost']=$SaleReturnProductCost;
		return $return;
	}
// public function LabourChargeTotal($from,$to)
// {
// 	$LabourCharge=$this->Service->find('all',array(
// 		"joins"=>array(
// 			array(
// 				"table"=>'labour_charge_lists',
// 				"alias"=>'LabourChargeList',
// 				"type"=>'inner',
// 				"conditions"=>array('Service.id=LabourChargeList.service_id_fk'),
// 				),
// 			array(
// 				'table'=>'work_codes',
// 				'alias'=>'WorkCode',
// 				'type'=>'INNER',
// 				'conditions'=>array('WorkCode.id=LabourChargeList.labour_work_code_id_fk')
// 				),
// 			),
// 		'conditions'=>array(
// 			'WorkCode.name!="ZA99L0"',
// 			'Service.status>=2',
// 			'Service.created between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
// 			),
// 		'fields'=>array(
// 			'LabourChargeList.*',
// 			'WorkCode.*',
// 			)
// 		)
// 	);
// 	$grand_total=0;
// 	foreach ($LabourCharge as $key => $value) {
// 		$grand_total+=$value['LabourChargeList']['price'];
// 	}
// 	return $grand_total;
// }
// public function ServiceDiscountTotal($from,$to)
// {
// 	$Service=$this->Service->find('all',array(
// 		'joins'=>array(
// 			array(
// 				"table"=>'painter_lists',
// 				"alias"=>'PainterList',
// 				"type"=>'left',
// 				"conditions"=>array('Service.id=PainterList.service_id_fk'),
// 				),
// 			),
// 		'conditions'=>array(
// 			'Service.status>=2',
// 			'Service.created between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
// 			),
// 		'fields'=>array(
// 			'Service.grand_total',
// 			'Service.paid',
// 			'Service.balance',
// 			'Service.discount',
// 			'PainterList.id',
// 			'PainterList.painting_charge',
// 			),
// 		));
// 	$service_total=0;
// 	$amount_recieved=0;
// 	$balance_total=0;
// 	$dicount_total=0;
// 	foreach ($Service as $key => $value) {
// 		$service_total+=$value['Service']['grand_total'];
// 		$amount_recieved+=$value['Service']['paid'];
// 		$balance_total+=$value['Service']['balance'];
// 		$dicount_total+=$value['Service']['discount'];
// 		if(!empty($value['PainterList']['id']))
// 		{
// 			$service_total-=$value['PainterList']['painting_charge'];
// 			// $amount_recieved-=$value['PainterList']['painting_charge'];
// 			// $balance_total-=$value['PainterList']['painting_charge'];
// 			// $dicount_total-=$value['PainterList']['painting_charge'];
// 		}
// 	}
// 	$return['ServiceGrandTotal']=$service_total;
// 	$return['ServiceRecievedTotal']=$amount_recieved;
// 	$return['ServiceBalanceTotal']=$balance_total;
// 	$return['ServiceDiscountTotal']=$dicount_total;
// 	return $return;
// }
	public function PurchaseTotal($from,$to)
	{
		$Purchase=$this->Purchase->find('all',array(
			'conditions'=>array(
				'status '=>2,
				'date_of_delivered between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			)
		);
		$grand_total=0;
		foreach ($Purchase as $key => $value) {
			$grand_total+=$value['Purchase']['grand_total'];
		}
		return $grand_total;	
	}
	public function PurchaseReturnTotal($from,$to)
	{
		$PurchaseReturn=$this->PurchaseReturn->find('all',array(
			'conditions'=>array(
				'date between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			)
		);
		$grand_total=0;
		foreach ($PurchaseReturn as $key => $value) {
			$grand_total+=$value['PurchaseReturn']['grand_total'];
		}
		return $grand_total;	
	}
	public function opening_balance($from)
	{
		$conditions_opening['date <']=date('Y-m-d',strtotime($from));
		$Journalopening=$this->Journal->find('all',array(
			'conditions'=>$conditions_opening
			)
		);
		$opening_balance=0;
		foreach ($Journalopening as $key => $value) {
			$opening_balance+=$value['Journal']['credit']-$value['Journal']['debit'];
		}
		return $opening_balance;
	}
	public function closing_balance($from,$to)
	{
		$Journalclosing=$this->Journal->find('all',array(
			'conditions'=>array(
				'date between ? and ?' => array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to)))
				)
			)
		);
		$closing_balance=0;
		foreach ($Journalclosing as $key => $value) {
			$closing_balance+=$value['Journal']['credit']-$value['Journal']['debit'];
		}
		return $closing_balance;
	}
	public function ReceiptCash($from,$to)
	{
		$ReceiptCash=$this->Journal->find('all',array(
			'conditions'=>array(
				'date between ? and ?' => array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				'work_flow="Receipt"',
				),
			)
		);
		$ReceiptCashtotal=0;
		foreach ($ReceiptCash as $keyRC => $valueRC) {
			$ReceiptCashtotal+=$valueRC['Journal']['credit']-$valueRC['Journal']['debit'];
		}
		return $ReceiptCashtotal;
	}
	public function PaymentCash($from,$to)
	{
		$PaymentCash=$this->Journal->find('all',array(
			'conditions'=>array(
				'date between ? and ?' => array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				'work_flow="Payment"',
				),
			)
		);
		$total=0;
		foreach ($PaymentCash as $key => $value) {
			$total+=$value['Journal']['debit']-$value['Journal']['credit'];
		}
		return $total;
	}
	public function ServiceSparePartTotal($from,$to)
	{
		$Service=$this->Service->find('all',array(
			"joins"=>array(
				array(
					"table"=>'spare_parts_lists',
					"alias"=>'SparePartsList',
					"type"=>'inner',
					"conditions"=>array('Service.id=SparePartsList.service_id_fk'),
					),
				array(
					"table"=>'products',
					"alias"=>'Product',
					"type"=>'inner',
					"conditions"=>array('Product.id=SparePartsList.product_id_fk'),
					),
				),
			'conditions'=>array(
				'status>=2',
				'Service.created between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			'fields'=>array(
				'Service.*',
				'SparePartsList.quantity',
				'SparePartsList.amount',
				'Product.product_name',
				'Product.cost',
				)
			)
		);
		$SparePartsListAmount=0;
		$ProductCost=0;
		foreach ($Service as $key => $value) {
			$SparePartsListAmount+=$value['SparePartsList']['amount'];
			$SparePartsListCostAmount=$value['Product']['cost']*$value['SparePartsList']['quantity'];
			$ProductCost+=$SparePartsListCostAmount;
		}
		$return['SparePartsListAmount']=$SparePartsListAmount;
		$return['ProductCost']=$ProductCost;
		return $return;
	}
	public function ExpanceTotal($from,$to)
	{
		$Expense_amount=0;
		$Expense=$this->Expense->find('all',array(
			'conditions'=>array(
				'exp_date between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			));  
		foreach ($Expense as $key => $value) {
			$Expense_amount+=$value['Expense']['exp_paid'];
		}
		$DeliveryExpense=$this->DeliveryExpense->find('all',array(
			'conditions'=>array(
				'exp_date between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			));
		$DeliveryExpense_paid=0;
		foreach ($DeliveryExpense as $key => $value) {
			$DeliveryExpense_paid+=$value['DeliveryExpense']['exp_paid'];
		}
		$Expense_amount+=$DeliveryExpense_paid;
		$ExpencesInPurchase=$this->ExpensesSumup->find('all',array(
			'conditions'=>array(
				'created between ? and ?'=>array(date('Y-m-d', strtotime($from)),date('Y-m-d', strtotime($to))),
				),
			));  
		$ExpencesInPurchase_Amount=0;
		foreach ($ExpencesInPurchase as $key => $value) {
			$ExpencesInPurchase_Amount+=$value['ExpensesSumup']['exp_paid'];
		}
		$Expense_amount+=$ExpencesInPurchase_Amount;
		$Expense['Total']=$Expense_amount;
		$Expense['PurchaseTotal']=$ExpencesInPurchase_Amount; 
		return $Expense;
	}

	public function product_type_select_ajax($product_type_id=null)
	{
//$data=$this->request->data;
//$product_type_id =$data['product_type_id'];
		$product=$this->Product->find('list',array('conditions'=>array('product_type_id'=>$product_type_id)));
//pr($product);
// $return['row']='<option value="">Select</option>';
// foreach ($product as $key => $value) {
// 	$return['row']=$return['row'].'<option value='.$key.'>'.$value.'</option>';;
// }
		echo json_encode($product); exit;

	}
	public function product_type_select_ajax_purchaseitem()
	{
		$product_type_id=$this->request->data['product_type_id'];
		$product=$this->Product->find('all',array('conditions'=>array('product_type_id'=>$product_type_id)));
		echo '<option value="">Select</option>';
		foreach ($product as $key => $value) {
			echo '<option value='.$value['Product']['id'].'>'.$value['Product']['name'].'</option>';
		}
		exit;
	}
	public function SalesBar($user_branch_id=null,$user_id=null){
//pr($Sale_date);
//exit;
		$data_month_wise_final=array();
		$data_month_wise=array();
		$data_month_wise['jan']=0;
		$data_month_wise['feb']=0;
		$data_month_wise['mar']=0;
		$data_month_wise['apr']=0;
		$data_month_wise['may']=0;
		$data_month_wise['jun']=0;
		$data_month_wise['jul']=0;
		$data_month_wise['aug']=0;
		$data_month_wise['sep']=0;
		$data_month_wise['oct']=0;
		$data_month_wise['nov']=0;
		$data_month_wise['dec']=0;
		$total_jan_off=0;
		$total_feb_off=0;
		$total_mar_off=0;
		$total_apr_off=0;
		$total_may_off=0;
		$total_jun_off=0;
		$total_jul_off=0;
		$total_aug_off=0;
		$total_sep_off=0;
		$total_oct_off=0;
		$total_nov_off=0;
		$total_dec_off=0;
//$data_month_wise_off=array();
		$data_month_wise_selected_off=array();
		$data_month_wise_selected_off['jan']=0;
		$data_month_wise_selected_off['feb']=0;
		$data_month_wise_selected_off['mar']=0;
		$data_month_wise_selected_off['apr']=0;
		$data_month_wise_selected_off['may']=0;
		$data_month_wise_selected_off['jun']=0;
		$data_month_wise_selected_off['jul']=0;
		$data_month_wise_selected_off['aug']=0;
		$data_month_wise_selected_off['sep']=0;
		$data_month_wise_selected_off['oct']=0;
		$data_month_wise_selected_off['nov']=0;
		$data_month_wise_selected_off['dec']=0;
		$conditions=[];
		if(!empty($user_branch_id))
		{
			$conditions['Sale.branch_id']=$user_branch_id;
		}
		$conditions['Sale.status']=2;
		$conditions['Sale.flag']=1;
		$Sale_date = date('Y-m-d');
		$date_build = date_parse_from_format("Y-m-d", $Sale_date);
		$year = $date_build["year"];

		$conditions['year(Sale.date_of_delivered)']=$year;
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
        if(!$user_id)
        {
            $conditions=[];
        }

        $this->Sale->unbindModel(array('belongsTo'=>array('AccountHead','Executive'),'hasMany' => array('SaleItem')));
	$this->Sale->virtualFields = array(
		'total_grand_amount' => "SUM(Sale.taxable_amount)",
		);
			$mnth_off=['01','02','03','04','05','06','07','08','09','10','11','12'];
				for ($i=0; $i < count($mnth_off); $i++) { 
							$conditions['month(Sale.date_of_delivered)']= $mnth_off[$i];
							$Sale=$this->Sale->find('first',array(
							'conditions'=>$conditions,
							'fields'=>array('total_grand_amount'),
							));
				$net_amount=floatval($Sale['Sale']['total_grand_amount']?$Sale['Sale']['total_grand_amount']:0);
			if($mnth_off[$i]=='01')
			{
				$data_month_wise_selected_off['jan']=$net_amount;
			}
			if($mnth_off[$i]=='02')
			{
				$data_month_wise_selected_off['feb']=$net_amount;
			}
			if($mnth_off[$i]=='03')
			{
				$data_month_wise_selected_off['mar']=$net_amount;
			}
			if($mnth_off[$i]=='04')
			{
				$data_month_wise_selected_off['apr']=$net_amount;
			}
			if($mnth_off[$i]=='05')
			{
				$data_month_wise_selected_off['may']=$net_amount;
			}
			if($mnth_off[$i]=='06')
			{
				$data_month_wise_selected_off['jun']=$net_amount;
			}
			if($mnth_off[$i]=='07')
			{
				$data_month_wise_selected_off['jul']=$net_amount;
			}
			if($mnth_off[$i]=='08')
			{
				$data_month_wise_selected_off['aug']=$net_amount;
			}
			if($mnth_off[$i]=='09')
			{
				$data_month_wise_selected_off['sep']=$net_amount;
			}
			if($mnth_off[$i]=='10')
			{
				$data_month_wise_selected_off['oct']=$net_amount;
			}
			if($mnth_off[$i]=='11')
			{
				$data_month_wise_selected_off['nov']=$net_amount;
			}
			if($mnth_off[$i]=='12')
			{
				$data_month_wise_selected_off['dec']=$net_amount;
			}
		}
		$data_month_wise['jan']=$data_month_wise_selected_off['jan'];
		$data_month_wise['feb']=$data_month_wise_selected_off['feb'];
		$data_month_wise['mar']=$data_month_wise_selected_off['mar'];
		$data_month_wise['apr']=$data_month_wise_selected_off['apr'];
		$data_month_wise['may']=$data_month_wise_selected_off['may'];
		$data_month_wise['jun']=$data_month_wise_selected_off['jun'];
		$data_month_wise['jul']=$data_month_wise_selected_off['jul'];
		$data_month_wise['aug']=$data_month_wise_selected_off['aug'];
		$data_month_wise['sep']=$data_month_wise_selected_off['sep'];
		$data_month_wise['oct']=$data_month_wise_selected_off['oct'];
		$data_month_wise['nov']=$data_month_wise_selected_off['nov'];
		$data_month_wise['dec']=$data_month_wise_selected_off['dec'];
		array_push($data_month_wise_final, $data_month_wise);
		return $data_month_wise_final;
		$this->set('data_month_wise_final',$data_month_wise_final);
	}
// ---------------------------end of Sales year wise report  -------------------
// ------------------------------ Purchase Rrepot--------------------
	public function PurchaseBarGraph($user_id=null)
	{
		$data_month_wise_final=array();
		$total_jan=0;
		$total_feb=0;
		$total_mar=0;
		$total_apr=0;
		$total_may=0;
		$total_jun=0;
		$total_jul=0;
		$total_aug=0;
		$total_sep=0;
		$total_oct=0;
		$total_nov=0;
		$total_dec=0;
		$data_month_wise_selected=array();
		$data_month_wise_selected['jan']=0;
		$data_month_wise_selected['feb']=0;
		$data_month_wise_selected['mar']=0;
		$data_month_wise_selected['apr']=0;
		$data_month_wise_selected['may']=0;
		$data_month_wise_selected['jun']=0;
		$data_month_wise_selected['jul']=0;
		$data_month_wise_selected['aug']=0;
		$data_month_wise_selected['sep']=0;
		$data_month_wise_selected['oct']=0;
		$data_month_wise_selected['nov']=0;
		$data_month_wise_selected['dec']=0;

		$Purchase_date = date('Y-m-d');
		$date_build = date_parse_from_format("Y-m-d", $Purchase_date);
		$year = $date_build["year"];

// $conditions['year(Sale.date_of_delivered)']=$year;
		$this->Purchase->unbindModel(array('hasMany' => array('PurchasedItem')));
		$purchases_total=$this->Purchase->find('all',array(
			'fields'=>array('id','total','grand_total','date_of_delivered'),
			'conditions'=>array(
				'status=2',
				'flag=1',
				'year(Purchase.date_of_delivered)'=>$year,
				)
			));
		$purchases_total=[];
		if($user_id)
        {
        	$purchases_total=$this->Purchase->find('all',array(
			'fields'=>array('id','total','grand_total','date_of_delivered'),
			'conditions'=>array(
				'status=2',
				'flag=1',
				'year(Purchase.date_of_delivered)'=>$year,
				)
			));
        }
//pr($purchases_total);exit;
		foreach ($purchases_total as $key => $value) {
			$date1=$value['Purchase']['date_of_delivered'];
			$date_explode1=explode("-",$date1);
			$mnth= $date_explode1[1];
			if($mnth=='01')
			{
				$total_jan+=$value['Purchase']['total'];
				$data_month_wise_selected['jan']=$total_jan;
			}
			if($mnth=='02')
			{
				$total_feb+=$value['Purchase']['total'];
				$data_month_wise_selected['feb']=$total_feb;
			}
			if($mnth=='03')
			{
				$total_mar+=$value['Purchase']['total'];
				$data_month_wise_selected['mar']=$total_mar;
			}
			if($mnth=='04')
			{
				$total_apr+=$value['Purchase']['total'];
				$data_month_wise_selected['apr']=$total_apr;
			}
			if($mnth=='05')
			{
				$total_may+=$value['Purchase']['total'];
				$data_month_wise_selected['may']=$total_may;
			}
			if($mnth=='06')
			{
				$total_jun+=$value['Purchase']['total'];
				$data_month_wise_selected['jun']=$total_jun;
			}
			if($mnth=='07')
			{
				$total_jul+=$value['Purchase']['total'];
				$data_month_wise_selected['jul']=$total_jul;
			}
			if($mnth=='08')
			{
				$total_aug+=$value['Purchase']['total'];
				$data_month_wise_selected['aug']=$total_aug;
			}
			if($mnth=='09')
			{
				$total_sep+=$value['Purchase']['total'];
				$data_month_wise_selected['sep']=$total_sep;
			}
			if($mnth=='10')
			{
				$total_oct+=$value['Purchase']['total'];
				$data_month_wise_selected['oct']=$total_oct;
			}
			if($mnth=='11')
			{
				$total_nov+=$value['Purchase']['total'];
				$data_month_wise_selected['nov']=$total_nov;
			}
			if($mnth=='12')
			{
				$total_dec+=$value['Purchase']['total'];
				$data_month_wise_selected['dec']=$total_dec;
			}
		}
		array_push($data_month_wise_final, $data_month_wise_selected);
		return $data_month_wise_final;
		$this->set('data_month_wise_final',$data_month_wise_final);
	}
	public function BrandGraph()
	{
		$Sale_total=$this->SaleItem->find('all',array(
			'joins'=>array(
				array(
					'table'=>'brands',
					'alias'=>'Brand',
					'type'=>'INNER',
					'conditions'=>array('Brand.id=Product.brand_id')
					),
				),
			'fields'=>array(
				'SaleItem.quantity',
				'Product.id',
				'Product.name',
				'Brand.id',
				'Brand.name',
				'Product.brand_id',
				),
			'conditions'=>array('Sale.status=2','Sale.flag=1')
			));
		$Product=$this->SaleItem->find('list',array(
			'joins'=>array(
				array(
					'table'=>'products',
					'alias'=>'Product',
					'type'=>'INNER',
					'conditions'=>array('Product.id=SaleItem.product_id')
					),
				),
			'fields'=>array('Product.id','Product.name'),
			));

		$product_array=[];
		foreach ($Sale_total as $key => $value) {
			$product_id=$value['Product']['id'];
			$brand_id=$value['Brand']['id'];
			$quantity=$value['SaleItem']['quantity'];
			$singl_product_array['brand_id']=$value['Brand']['id'];
			$singl_product_array['brand_name']=$value['Brand']['name'];
			$singl_product_array['brand_name']=$value['Brand']['name'];
			$singl_product_array['quantity']=$quantity;
			if($product_array)
			{
				if(isset($product_array[$brand_id]))
				{
					$product_array[$brand_id]['quantity']+=$quantity;
				}	
				else
				{
					$product_array[$brand_id]=$singl_product_array;
				}
			}
			else
			{
				$product_array[$brand_id]=$singl_product_array;
			}
		}
		$sum_sale_quantity = 0;
		foreach ($product_array as $key => $value){
			$sum_sale_quantity += $value['quantity'];
		}
		$limit=5;
		$brand_array=[];
		$i=0;
		$colors=['prgrs_color_1','prgrs_color_2','prgrs_color_3','prgrs_color_4','prgrs_color_5','prgrs_color_6','prgrs_color_7','info','danger','info'];
		$price = array();
		foreach ($product_array as $key => $value){
			$price[$key] =  $value['quantity'];

		}
		array_multisort($price, SORT_DESC, $product_array);
		foreach ($product_array as $keyPR => $value)
		{
			$i++;
			if($i<=$limit) {
				$single_sale_quantity = $value['quantity']/$sum_sale_quantity*100;
				$single['name']=$value['brand_name'];
				$single['quantity']=round($single_sale_quantity,3);
				$single['color']=$colors[$i];
				$brand_array[]=$single;
			}
			else
			{
				break;
			}
		}
		return $brand_array;

	}
//...............Pie Chart................//
	public function pieChart($user_branch_id=null)
	{
		$piechart_all=array();
		$from = date('Y-m-d',strtotime('first day of this month'));
		$to = date('Y-m-d',strtotime('last day of this month'));
		$Route=$this->Route->find('list',array(
			'order'=>array('id ASC'),
			'fields'=>[
			'Route.id',
			'Route.name'
			]
			));
		$conditions=[];
		if(!empty($user_branch_id))
		{
			$conditions['Sale.branch_id']=$user_branch_id;
		}
		$conditions['Sale.status']=2;
		$conditions['Sale.date_of_delivered between ? and ?']=array($from,$to);
	$this->SaleItem->unbindModel(array('belongsTo'=>array('Product','Warehouse')));
		foreach ($Route as $key => $value) {
			$this->SaleItem->virtualFields = array(
				//'total_net_value' => "SUM(SaleItem.net_value)",
				'total_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
				'total_sale' => "SUM(SaleItem.total)",
				);
			$conditions['Customer.route_id']=$key;
			$SaleItem=$this->SaleItem->find('first',array(
				"joins" => array(
					array(
						"table" => 'customers',
						"alias" => 'Customer',
						"type" => 'inner',
						"conditions" => array('Customer.account_head_id=Sale.account_head_id'),
						),
					array(
						"table" => 'routes',
						"alias" => 'Route',
						"type" => 'inner',
						"conditions" => array('Customer.route_id=Route.id'),
						),
					),
				'conditions'=>$conditions,
				'fields'=>array('total_net_value','total_sale'),
				));
			$net_value=floatval($SaleItem['SaleItem']['total_net_value']?$SaleItem['SaleItem']['total_net_value']:0);
			$single['net_value']=round($net_value);
			$single['route']=$value;
			$piechart_all[]=$single;

		}

		return $piechart_all;

	}
	public function SaleSummary()
	{
	// $PermissionList = $this->Session->read('PermissionList');
	// $menu_id = $this->Menu->field(
	// 	'Menu.id',
	// 	array('action ' => 'Reports/SaleSummary'));
	// if(!in_array($menu_id, $PermissionList))
	// {
	// 	$this->Session->setFlash("Permission denied");
	// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
	// }
		$from=date("d-m-Y");
		$this->set('from',$from);
		$to=date("d-m-Y");
		$this->set('to',$to);
	}
	public function SaleSummary_ajax()
	{
		$data=$this->request->data;
		$from_date=$data['from_date'];
		$to_date=$data['to_date'];
		$from_date=date('Y-m-d',strtotime(($from_date)));
		$to_date=date('Y-m-d',strtotime(($to_date)));
		$list_array=array();
		//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$Sale=$this->Sale->find('all',array(
			'conditions'=>array('Sale.invoice_no IS NOT NULL',
				'Sale.status=2',
				'Sale.flag=1',
				'Sale.date_of_delivered between ? and ?' => array($from_date,$to_date),
			),
			'fields'=>array(
				'Sale.id',
				'Sale.invoice_no',
				'Sale.date_of_delivered',
				'Sale.grand_total',
				'Sale.account_head_id',
				'Sale.discount_amount',
			),
			'order'=>array('Sale.id DESC'),
		));
		if(!empty($Sale))
		{
			foreach ($Sale as $key => $value) {
				$list_single_offline['invoice_no']=$value['Sale']['invoice_no'];
				$list_single_offline['delivered_date']=$value['Sale']['date_of_delivered'];
				
				$AccountHead=$this->AccountHead->find('first',array(
					'conditions'=>array(
						'AccountHead.id' =>$value['Sale']['account_head_id']
					)));
				if(!empty($AccountHead))
				{
				$list_single_offline['customer_name']=$AccountHead['AccountHead']['name'];	
				}
				else
				{
$list_single_offline['customer_name']="";
			     }
				$list_single_offline['total']=$value['Sale']['grand_total'];
				$list_single_offline['discount_amount']=$value['Sale']['discount_amount'];
				$this->SaleItem->virtualFields = array( 
					'SaleItem_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
					//'SaleItem_net_value' => "SUM(SaleItem.net_value)",
					'SaleItem_tax' => "SUM(SaleItem.tax_amount)",
					//'SaleItem_total' => "SUM(SaleItem.total)",
					'SaleItem_total' => "SUM((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount)",
				);
				$SaleItem=$this->SaleItem->find('first',array(
					'conditions'=>array('sale_id'=>$value['Sale']['id']),
					'fields'=>array(
						'SaleItem_net_value',
						'SaleItem_tax',
						'SaleItem_total',
					)
				));
				$list_single_offline['net_amount']=$SaleItem['SaleItem']['SaleItem_net_value']?$SaleItem['SaleItem']['SaleItem_net_value']:0;
				$list_single_offline['taxable_amount']=$SaleItem['SaleItem']['SaleItem_net_value']-$value['Sale']['discount_amount'];
				$list_single_offline['amount']=$SaleItem['SaleItem']['SaleItem_total']?$SaleItem['SaleItem']['SaleItem_total']:0;
				$list_single_offline['tax']=$SaleItem['SaleItem']['SaleItem_tax']?$SaleItem['SaleItem']['SaleItem_tax']:0;
				array_push($list_array, $list_single_offline);
			}
		}
		$return=array();
		$return['row']='';
		$grand_total=0; $amount=0;$net_amount=0;$roundoff=0;$discount=0;$tax=0;
		foreach ($list_array as $key => $value) {
			$return['row'].='<tr class="blue-pddng toggle_class">';
			$return['row'].='<td>'.$value['invoice_no'].'</td>';
			$return['row'].='<td>'.date('d-m-Y',strtotime($value['delivered_date'])).'</td>';
			$return['row'].='<td>'.$value['customer_name'].'</td>';
			$return['row'].='<td>'.round($value['net_amount'],3).'</td>';
			$return['row'].='<td>'.round($value['discount_amount'],3).'</td>';
		    $return['row'].='<td>'.round($value['taxable_amount'],3).'</td>';
			$return['row'].='<td>'.round($value['tax'],3).'</td>';
			$return['row'].='<td>'.round($value['total'],3).'</td>';
			$net_amount+=$value['net_amount'];
			$amount+=$value['taxable_amount'];
			$discount+=$value['discount_amount'];
			$tax+=$value['tax'];
			$grand_total+=$value['total'];
			$return['row'].='</tr>';
		}
		$return['row'].='<tr class="blue-pddng toggle_class">';
		$return['row'].='<td></td>';
		$return['row'].='<td></td>';
		$return['row'].='<td class="total_amount"><b>Total</b></td>';
		$return['row'].='<td class="total_amount"><b>'.round($net_amount,3).'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.round($discount,3).'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.round($amount,3).'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.round($tax,3).'</b></td>';
		$return['row'].='<td class="total_amount"><b>'.round($grand_total,3).'</b></td>';
		$return['row'].='</tr>';
		echo json_encode($return);
		exit;
	}
	public function PurchaseItemWise()
	{
	// $PermissionList = $this->Session->read('PermissionList');
	// $menu_id = $this->Menu->field(
	// 	'Menu.id',
	// 	array('action ' => 'Reports/PurchaseItemWise'));
	// if(!in_array($menu_id, $PermissionList))
	// {
	// 	$this->Session->setFlash("Permission denied");
	// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
	// }
		$this->set('Product_type',$this->ProductType->find('list',array('fields'=>array('id','name'))));
		$this->set('Product',$this->Product->find('list',array('conditions'=>array('type !='=>[2,3]),'fields'=>array('id','name'))));
		$first_product=$this->Product->find('first');
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0) );
		$this->set('date1',$date1);
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 month", $time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('final',$final);
		$date2=date("Y-m-d", strtotime($date1) );
		$final2=date("Y-m-d", strtotime($final) );
		$Product_list=$this->Product->find('all',array(
			"joins"=>array(
				array(
					"table"=>'purchased_items',
					"alias"=>'PurchasedItem',
					"type"=>'INNER',
					"conditions"=>array('PurchasedItem.product_id=Product.id'),
				),
				array(
					"table"=>'purchases',
					"alias"=>'Purchase',
					"type"=>'INNER',
					"conditions"=>array('Purchase.id=PurchasedItem.purchase_id'),
				),
			),
			'conditions'=>array('Product.id'=>$first_product['Product']['id']),
			'order'=>array('Purchase.date_of_delivered ASC'),
			'fields'=>array(
				'Product.name',
				'Purchase.invoice_no',
				'Purchase.date_of_delivered',
				'PurchasedItem.unit_price',
				'PurchasedItem.quantity',
				'PurchasedItem.total',
			)
		)
	);
		$Product_list_array=array();
		foreach ($Product_list as $key => $value) {
			if(!empty($value['Purchase']))
			{
				$product_list_single['invoice_no']=$value['Purchase']['invoice_no'];
				$product_list_single['delivered_date']=$value['Purchase']['date_of_delivered'];
				$product_list_single['rate']=$value['PurchasedItem']['unit_price'];
				$product_list_single['product_qty']=$value['PurchasedItem']['quantity'];
				$product_list_single['total']=$value['PurchasedItem']['total'];
			}
			array_push($Product_list_array, $product_list_single);
		}
		$this->set('Product_list',$Product_list_array);
	}
	public function purchase_item_wise_ajax()
	{

		$requestData=$this->request->data;
		$conditions=[];
		$conditions['Purchase.flag']=1;
		$conditions['Purchase.status']=2;
		if($requestData['product_type_id']) $conditions['Product.product_type_id']=$requestData['product_type_id'];
		if($requestData['product_id']) $conditions['PurchasedItem.product_id']=$requestData['product_id'];
		$from_date=date('Y-m-d',strtotime($requestData['from_date']));
		$to_date=date('Y-m-d',strtotime($requestData['to_date']));
		$conditions['Purchase.date_of_delivered between ? and ?']=[$from_date,$to_date];
		$totalData=$this->PurchasedItem->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Purchase.invoice_no LIKE'    =>'%'. $q . '%',
				'Product.name LIKE'           =>'%'. $q . '%',
				'PurchasedItem.quantity LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->PurchasedItem->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->PurchasedItem->find('all',array(
			"joins"=>array(
				array(
					"table"=>'product_types',
					"alias"=>'ProductType',
					"type"=>'INNER',
					"conditions"=>array('ProductType.id=Product.product_type_id'),
				),
			),
			'conditions'=>$conditions,
			'order'=>array('Purchase.date_of_delivered'),
			'fields'=>array(
				'Purchase.invoice_no',
				'Purchase.date_of_delivered',
				'PurchasedItem.unit_price',
				'Product.name',
				'ProductType.name',
				'PurchasedItem.quantity',
				'PurchasedItem.product_id',
				'PurchasedItem.total',
			)
		));
		foreach ($Data as $key => $value) {
			$Data[$key]['Purchase']['date_of_delivered']=date('d-m-Y',strtotime($value['Purchase']['date_of_delivered']));
			$Data[$key]['PurchasedItem']['quantity']=floatval($value['PurchasedItem']['quantity']);
			$Data[$key]['PurchasedItem']['unit_price']=number_format($value['PurchasedItem']['unit_price'], 2, '.', '');
			$Data[$key]['PurchasedItem']['total']=number_format($value['PurchasedItem']['total'], 2, '.', '');
			$Data[$key]['PurchasedItem']['return_qty']=0;
			$credit_note_no_list=$this->PurchaseReturnItem->find('all',array(
				'conditions'=>array(
					'PurchaseReturnItem.invoice_no'=>$value['Purchase']['invoice_no'],
					'PurchaseReturnItem.product_id'=>$value['PurchasedItem']['product_id'],
				)
			));
			foreach ($credit_note_no_list as $val) {
				$Data[$key]['PurchasedItem']['return_qty']+=$val['PurchaseReturnItem']['quantity'];
				$Data[$key]['PurchasedItem']['total']     -=$val['PurchaseReturnItem']['total'];
			}
			$Data[$key]['PurchasedItem']['return_qty']=number_format($Data[$key]['PurchasedItem']['return_qty'], 2, '.', '');
			$Data[$key]['PurchasedItem']['total']=number_format($Data[$key]['PurchasedItem']['total'], 2, '.', '');
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData), 
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	
	}
	public function GraphicalReport()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Reports/GraphicalReport'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$countOFF=$this->Sale->find('count',array('conditions'=>array('status=1','flag=1')));
		$countP=$this->Purchase->find('count',array('conditions'=>array('status=1','flag=1')));
		$totalPending_count=$countOFF;
		$Sales_month=$this->SalesBar();
		$Purchase_month=$this->PurchaseBarGraph();
		$this->set('Sales_month',$Sales_month);
		$this->set('Purchase_month',$Purchase_month);
		$this->set('Pending_Saleorders',$totalPending_count);
		$this->set('Pending_Purchaseorders',$countP);
	}
	public function PurchaseSummary()
	{
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Reports/PurchaseSummary'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0) );
		$this->set('date1',$date1);
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 month", $time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('final',$final);
		$date2=date("Y-m-d", strtotime($date1) );
		$final2=date("Y-m-d", strtotime($final) );
		$purchases_array=array();
		$purchases=$this->Purchase->find('all',
			array(
				'conditions'=>array(
					'Purchase.status=2',
					'Purchase.flag=1',
					'Purchase.date_of_delivered between ? and ?' => array($final2,$date2)
					),
				'fields'=>array('id','date_of_delivered','invoice_no','status','grand_total','other_value','account_head_id'),
				'order'=>array('Purchase.date_of_delivered ASC'),
				));
		if(!empty($purchases))
		{
			foreach ($purchases as $key => $value) {
				$purchases_array_selected=array();
				$purchases_array_selected['party_invoice_no']=$value['Purchase']['invoice_no'];
				$purchases_array_selected['date_of_delivered']=$value['Purchase']['date_of_delivered'];
				$AccountHead=$this->AccountHead->find('first',array(
					'conditions'=>array(
						'AccountHead.id' =>$value['Purchase']['account_head_id']
						)));
				$purchases_array_selected['party_name']=$AccountHead['AccountHead']['name'];
				$purchases_array_selected['other_value']=$value['Purchase']['other_value'];
				$purchases_array_selected['grand_total']=$value['Purchase']['grand_total'];
				$tax_amount_total=0;
				$total=0;
				$PurchasedItem_grouped=$this->PurchasedItem->find('all',array('conditions'=>array('purchase_id'=>$value['Purchase']['id'])));
				if(!empty($PurchasedItem_grouped))
				{
					foreach ($PurchasedItem_grouped as $key => $value1) {
						$tax_amount_total+=$value1['PurchasedItem']['cgst_amount']+$value1['PurchasedItem']['sgst_amount']+$value1['PurchasedItem']['igst_amount'];
						$total+=$value1['PurchasedItem']['net_value'];
					}
					$purchases_array_selected['tax_amount']=$tax_amount_total;
					$purchases_array_selected['net_value']=$total;
				}
				array_push($purchases_array, $purchases_array_selected);
			}
		}
		$this->set('purchases_array',$purchases_array);
	}
	public function purchase_summary_ajax()
	{
// 	$data=$this->request->data;
// 	$from_date=$data['from_date'];
// 	$to_date=$data['to_date'];
// 	$explode_from_date=explode('-', $from_date);
// 	$from_date=$explode_from_date[2].'-'.$explode_from_date[1].'-'.$explode_from_date[0];
// 	$explode_to_date=explode('-', $to_date);
// 	$to_date=$explode_to_date[2].'-'.$explode_to_date[1].'-'.$explode_to_date[0];
// 	$purchases_array=array();
// 	$purchases=$this->Purchase->find('all',
// 		array(
// 			'conditions'=>array(
// 				'Purchase.status=2',
// 				'Purchase.flag=1',
// 				'Purchase.date_of_delivered between ? and ?' => array($from_date,$to_date)
// 			),
// 			'fields'=>array('id','date_of_delivered','invoice_no','status','grand_total','other_value','account_head_id'),
// 			'order'=>array('Purchase.date_of_delivered ASC'),
// 		));
// 	if(!empty($purchases))
// 	{
// 		foreach ($purchases as $key => $value) {
// 			$purchases_array_selected=array();
// 			$purchases_array_selected['party_invoice_no']=$value['Purchase']['invoice_no'];
// 			$purchases_array_selected['date_of_delivered']=$value['Purchase']['date_of_delivered'];
// 			$AccountHead=$this->AccountHead->find('first',array(
// 				'conditions'=>array(
// 					'AccountHead.id' =>$value['Purchase']['account_head_id']
// 				)));
// 			$purchases_array_selected['party_name']=$AccountHead['AccountHead']['name'];
// 			$purchases_array_selected['other_value']=$value['Purchase']['other_value'];
// 			$purchases_array_selected['grand_total']=$value['Purchase']['grand_total'];
// 			$tax_amount_total=0;
// 			$total=0;
// 			$PurchasedItem_grouped=$this->PurchasedItem->find('all',array('conditions'=>array('purchase_id'=>$value['Purchase']['id'])));
// 			if(!empty($PurchasedItem_grouped))
// 			{
// 				foreach ($PurchasedItem_grouped as $key => $value1) {
// 					$tax_amount_total+=$value1['PurchasedItem']['cgst_amount']+$value1['PurchasedItem']['sgst_amount']+$value1['PurchasedItem']['igst_amount'];
// 					$total+=$value1['PurchasedItem']['net_value'];
// 				}
// 				$purchases_array_selected['tax_amount']=$tax_amount_total;
// 				$purchases_array_selected['net_value']=$total;
// 			}
// 			array_push($purchases_array, $purchases_array_selected);
// 		}
// 	}
// 	$return=array();
// 	$return['row']='';
// 	$grand_total=0;
// 	$tax=0; $amount=0;$roundoff=0;
// 	if(!empty($purchases_array)){
// 		foreach ($purchases_array as $key => $value) {
// 			$return['row']=$return['row'].'<tr class="blue-pddng toggle_class">';
// 			$return['row']=$return['row'].'<td>'.$value['party_invoice_no'].'</td>';
// 			$return['row']=$return['row'].'<td>'.date('d-m-Y',strtotime($value['date_of_delivered'])).'</td>';
// 			$return['row']=$return['row'].'<td>'.$value['party_name'].'</td>';
// 			$return['row']=$return['row'].'<td>'.$value['net_value'].'</td>';
// 			$return['row']=$return['row'].'<td>'.$value['tax_amount'].'</td>';
// 			$return['row']=$return['row'].'<td>'.$value['other_value'].'</td>';
// 			$return['row']=$return['row'].'<td>'.$value['grand_total'].'</td>';
// 			$amount+=$value['net_value'];
// 			$tax+=$value['tax_amount'];
// 			$roundoff+=$value['other_value'];
// 			$grand_total+=$value['grand_total'];
// 			$return['row']=$return['row'].'</tr>';
// 		}
// 		$return['row']=$return['row'].'<tr class="blue-pddng toggle_class">';
// 		$return['row']=$return['row'].'<td></td>';
// 		$return['row']=$return['row'].'<td></td>';
// 		$return['row']=$return['row'].'<td class="total_amount"><b>Total</b></td>';
// 		$return['row']=$return['row'].'<td class="total_amount"><b>'.$amount.'</b></td>';
// 		$return['row']=$return['row'].'<td class="total_amount"><b>'.$tax.'</b></td>';
// 		$return['row']=$return['row'].'<td class="total_amount"><b>'.$roundoff.'</b></td>';
// 		$return['row']=$return['row'].'<td class="total_amount"><b>'.$grand_total.'</b></td>';
// 		$return['row']=$return['row'].'</tr>';
// 	}
// 	echo json_encode($return);
// 	exit;
//
		$data=$this->request->data;
		$from_date=$data['from_date'];
		$to_date=$data['to_date'];
		$explode_from_date=explode('-', $from_date);
		$from_date=$explode_from_date[2].'-'.$explode_from_date[1].'-'.$explode_from_date[0];
		$explode_to_date=explode('-', $to_date);
		$to_date=$explode_to_date[2].'-'.$explode_to_date[1].'-'.$explode_to_date[0];
		$purchases_array=array();
		$purchases=$this->Purchase->find('all',
			array(
				'conditions'=>array(
					'Purchase.status>=2',
					'Purchase.flag=1',
					'Purchase.date_of_delivered between ? and ?' => array($from_date,$to_date)
					),
				'fields'=>array('id','date_of_delivered','invoice_no','status','grand_total','other_value','account_head_id','total_tax_amount','discount_amount'),
				'order'=>array('Purchase.date_of_delivered DESC'),
				));
		if(!empty($purchases))
		{
			foreach ($purchases as $key => $value) {
				$purchases_array_selected=array();
				$purchases_array_selected['party_invoice_no']=$value['Purchase']['invoice_no'];
				$purchases_array_selected['date_of_delivered']=$value['Purchase']['date_of_delivered'];
				$AccountHead=$this->AccountHead->find('first',array(
					'conditions'=>array(
						'AccountHead.id' =>$value['Purchase']['account_head_id']
						)));
				$purchases_array_selected['party_name']=$AccountHead['AccountHead']['name'];
				$purchases_array_selected['other_value']=$value['Purchase']['other_value'];
				$purchases_array_selected['grand_total']=$value['Purchase']['grand_total'];
				$tax_amount_total=0;
				$total=0;
				$discount=0;
				$PurchasedItem_grouped=$this->PurchasedItem->find('all',array('conditions'=>array('purchase_id'=>$value['Purchase']['id'])));
				if(!empty($PurchasedItem_grouped))
				{
					foreach ($PurchasedItem_grouped as $key => $value1) {
						$total+=$value1['PurchasedItem']['quantity']*$value1['PurchasedItem']['unit_price'];
						$discount+=$value1['PurchasedItem']['discount'];
					}
					$purchases_array_selected['net_value']=$total;
					$purchases_array_selected['discount']=$value['Purchase']['discount_amount'];
					$purchases_array_selected['taxable_value']=$total-$value['Purchase']['discount_amount'];
					$purchases_array_selected['tax_amount']=$value['Purchase']['total_tax_amount'];
				}
				array_push($purchases_array, $purchases_array_selected);
			}
		}
		$return=array();
		$return['row']='';
		$grand_total=0;
		$tax=0;
		$net_value=0;
		$discount=0;
		$taxable_value=0;
		$roundoff=0;
		$other_Expense=0;
		if(!empty($purchases_array)){
			foreach ($purchases_array as $key => $value) {
				$return['row']=$return['row'].'<tr class="blue-pddng toggle_class">';
				$return['row']=$return['row'].'<td>'.$value['party_invoice_no'].'</td>';
				$return['row']=$return['row'].'<td>'.date('d-m-Y',strtotime($value['date_of_delivered'])).'</td>';
				$return['row']=$return['row'].'<td>'.$value['party_name'].'</td>';
				$return['row']=$return['row'].'<td style="text-align:right">'.number_format($value['net_value'], 2, '.', '').'</td>';
				$return['row']=$return['row'].'<td style="text-align:right">'.number_format($value['discount'], 2, '.', '').'</td>';
				$return['row']=$return['row'].'<td style="text-align:right">'.number_format($value['taxable_value'], 2, '.', '').'</td>';
				$return['row']=$return['row'].'<td style="text-align:right">'.number_format($value['tax_amount'], 2, '.', '').'</td>';
				// $return['row']=$return['row'].'<td>'.$value['other_value'].'</td>';
				$return['row']=$return['row'].'<td style="text-align:right">'.number_format($value['grand_total'], 2, '.', '').'</td>';
				$net_value+=$value['net_value'];
				$discount+=$value['discount'];
				$taxable_value+=$value['net_value'];
				$taxable_value-=$value['discount'];
				$tax+=$value['tax_amount'];
				$roundoff+=$value['other_value'];
				$grand_total+=$value['grand_total'];
				$return['row']=$return['row'].'</tr>';
			}
			$return['row']=$return['row'].'<tr class="blue-pddng toggle_class">';
			$return['row']=$return['row'].'<td></td>';
			$return['row']=$return['row'].'<td></td>';
			$return['row']=$return['row'].'<td style="text-align:right" class="total_amount"><b>Total</b></td>';
			$return['row']=$return['row'].'<td style="text-align:right" class="total_amount"><b>'.number_format($net_value, 2, '.', '').'</b></td>';
			$return['row']=$return['row'].'<td  style="text-align:right"class="total_amount"><b>'.number_format($discount, 2, '.', '').'</b></td>';
			$return['row']=$return['row'].'<td  style="text-align:right"class="total_amount"><b>'.number_format($taxable_value, 2, '.', '').'</b></td>';
			$return['row']=$return['row'].'<td  style="text-align:right" class="total_amount"><b>'.number_format($tax, 2, '.', '').'</b></td>';
			// $return['row']=$return['row'].'<td class="total_amount"><b>'.$number_formatoff.'</b></td>';
			$return['row']=$return['row'].'<td style="text-align:right" class="total_amount"><b>'.number_format($grand_total, 2, '.', '').'</b></td>';
			$return['row']=$return['row'].'</tr>';
		}
		echo json_encode($return);
		exit;

	}

	public function DayRegisterSummary()
	{
		$Executive_list=$this->Executive->find('list');
		$this->set(compact('Executive_list'));
	}
//new day register report
	public function DayRegisterReport()
	{
		if(!empty($this->request->data['date'])){
			$date = date('Y-m-d',strtotime($this->request->data['date']));
		}
		$executive =$this->request->data['executive'];
		$return['row']['tbody'] ="";
		$return['date']=$this->request->data['date'];
		$return['transfer_print'] ="";
		$warehouse_id = $this->Executive->field(
			'Executive.warehouse_id',
			array('Executive.id ' => $executive));
		$TransferList=$this->StockTransfer->find('first',array(
			'conditions'=>array('(StockTransfer.date) <='=>$date,'warehouse_to'=>$warehouse_id),
			'fields'=>'max(StockTransfer.date) as date,StockTransfer.id as id',
			));
		if(!empty($TransferList)){
			if(!empty($TransferList['StockTransfer']['id'])){
				$return['transfer_print'] = '<span style=""><a target="_blank" href="'.$this->webroot.'Print/stockfpdf/'.$TransferList['StockTransfer']['id'].'" title="VanStock"><i class="fa fa-cart-plus fa-2x" aria-hidden="true"></i></a><a></a></span><a>';
			}
		}
		$return['executive']=$this->request->data['executive'];
		$CustomerList=$this->AccountHead->find('all',array(
			"joins"=>array(
				array(
					"table"=>'customers',
					"alias"=>'Customer',
					"type"=>'inner',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
					),
				),
			'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
			'fields'=>array(
				'Customer.*',
				'AccountHead.id',
				'AccountHead.name',
				'AccountHead.opening_balance',
				),
			));
		if($CustomerList)
		{
			$All_Customer=[];
			foreach ($CustomerList as $key => $value) {
				$Single_Customer['id']=$value['AccountHead']['id'];
				$Single_Customer['name']=$value['AccountHead']['name'];
				$Single_Customer['shope_code']=$value['Customer']['code'];
				$Single_Customer['action'] = '';
				$Single_Customer['remarks'] = '';
				$Single_Customer['sort'] = 0;
				$Single_Customer['print'] = "";
//
				$Sale=$this->Sale->find('first',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$value['AccountHead']['id'],
						'Sale.executive_id'=>$executive,
						'Sale.date_of_delivered'=>$date,
						'Sale.status'=>2
						),
					'fields'=>array(
						'Sale.id',
						'Sale.invoice_no',
						),
					));
				$All_sale=[];
				if(!empty($Sale)){
					$All_sale['sale_id']=$Sale['Sale']['id'];
					$All_sale['invoice_no']=$Sale['Sale']['invoice_no'];
					$Single_Customer['action'] = 'Sale';
					$Single_Customer['remarks'] = 'Invoice No:'.$Sale['Sale']['invoice_no'];
					$Single_Customer['sort'] = 3;
					$Single_Customer['print'] = '<span style=""><a target="_blank" href="'.$this->webroot.'Print/fpdf/'.$Sale['Sale']['id'].'"><i class="fa fa-print" aria-hidden="true"></i></a><a></a></span><a>';
				}
				$Single_Customer['sale']=$All_sale;
				$Saleqtn=$this->Sale->find('first',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$value['AccountHead']['id'],
						'Sale.executive_id'=>$executive,
						'Sale.date_of_order'=>$date,
						'Sale.status'=>1,
						),
					'fields'=>array(
						'Sale.id',
						'Sale.invoice_no',
						),
					));
				$All_qtn =[];
				if(!empty($Saleqtn)){
					$All_qtn['sale_id']=$Saleqtn['Sale']['id'];
					$All_qtn['invoice_no']=$Saleqtn['Sale']['invoice_no'];
					$Single_Customer['action'] = 'Quotation';
					$Single_Customer['remarks'] = 'Quotation No:'.$Saleqtn['Sale']['invoice_no'];
					$Single_Customer['sort'] = 2;
					$Single_Customer['print'] = '<span style=""><a target="_blank" href="'.$this->webroot.'Print/qutofpdf/'.$Saleqtn['Sale']['id'].'"><i class="fa fa-print" aria-hidden="true"></i></a><a></a></span><a>';
				}
				$Single_Customer['quotation']=$All_qtn;
				$NoSale=$this->NoSale->find('first',array(
					'conditions'=>array(
						'NoSale.customer_account_head_id'=>$value['AccountHead']['id'],
						'NoSale.executive_id'=>$executive,
						'NoSale.date'=>$date
						),
					'fields'=>array(
						'NoSale.id',
						'NoSale.description',
						),
					));
				$All_nosale =[];
				if(!empty($NoSale)){
					$All_nosale['no_sale_id']=$NoSale['NoSale']['id'];
					$All_nosale['description']=$NoSale['NoSale']['description'];
					$Single_Customer['action'] = 'No Sale';
					$Single_Customer['remarks'] = $NoSale['NoSale']['description'];
					$Single_Customer['sort'] = 1;
					$Single_Customer['print'] = "";
				}
				$Single_Customer['nosale']=$All_nosale;
				if($Single_Customer['action']!=''){
					array_push($All_Customer, $Single_Customer);
				}
//
			}
			array_multisort( array_column($All_Customer, "sort"), SORT_DESC, $All_Customer );
			$return['row']['tbody'] ="";
			foreach ($All_Customer as $key => $value) {
				$return['row']['tbody'].='<tr class="blue-pddng">';
				$return['row']['tbody'].='<td class="color_label ">'.$value['name'].'('.$value['shope_code'].')'.'</td>';
				$return['row']['tbody'].='<td class="color_label">'.$value['action'].'</td>';
				$return['row']['tbody'].='<td class="color_label">'.$value['remarks'].'</td>';
				$return['row']['tbody'].='<td class="color_label">'.$value['print'].'</td>';
				$return['row']['tbody'].='</tr>';
			}
		}
		echo json_encode($return);
		exit;
	}
	public function DayRegisterDetailFunction($executive,$date)
	{
		try {
			$date=date('Y-m-d',strtotime($date));
// $Sale=$this->Sale->find('all',array(
// 	'conditions'=>array(
// 		'Sale.status>1',
// 		'Sale.account_head_id'=>$account_head_id,
// 		'Sale.date_of_delivered between ? and ?' => array($from_date,$to_date)
// 		),
// 	'fields'=>array(
// 	// 'Sale.id',
// 	// 'Sale.total',
// 	// 'Sale.grand_total',
// 	// 'Sale.other_value',
// 	// 'Sale.other_value',
// 		),
// 	));
// $AccountHead=$this->AccountHead->findById($account_head_id);
			require('fpdf/fpdf.php');
			$pdf = new FPDF('p', 'mm', [297, 210]);
			$x=8;
			$y=8;
			function header_section($pdf) {
				$x=8;
				$y=8;
				$pdf->AddPage();
				$pdf->SetFont('Arial','B',12);
				$pdf->rect(5, 5, 200, 290);
				$pdf->Text($x, $y+3, "#");
				$Date_line_x=25;
				$pdf->Line($Date_line_x, $y-3,$Date_line_x, 295);
				$pdf->Text($Date_line_x+10, $y+5, "Customer");
				$AccountHead_line_x=$Date_line_x+65;
				$pdf->Line($AccountHead_line_x, $y-3,$AccountHead_line_x, 295);
				$pdf->Text($AccountHead_line_x+10, $y+5, "Action");
				$Remark_line_x=$AccountHead_line_x+50;
				$pdf->Line($Remark_line_x, $y-3,$Remark_line_x, 295);
				$pdf->Text($Remark_line_x+5, $y+5, "Remark");
				$Credit_line_x=$Remark_line_x+20;
// $pdf->Line($Credit_line_x, $y-3,$Credit_line_x, 295);
// $pdf->Text($Credit_line_x+5, $y+5, "Debit");
				$Debit_line_x=$Credit_line_x+20;
// $pdf->Line($Debit_line_x, $y-3,$Debit_line_x, 295);
// $pdf->Text($Debit_line_x+5, $y+5, "Balance");
				$pdf->Line(5, $x+8, 205, $x+8); 
				$pdf->SetFont('Arial','B',8);
			}
			$Date_line_x=25;
			$AccountHead_line_x=$Date_line_x+65;
			$Remark_line_x=$AccountHead_line_x+50;
			$Credit_line_x=$Remark_line_x+20;
			$Debit_line_x=$Credit_line_x+20;
// header_section($pdf,$Sale);
			header_section($pdf);
// $Journal_all=array(
// 	array(
// 		'date'=>$AccountHead['AccountHead']['created_at'],
// 		'name'=>$AccountHead['AccountHead']['name'],
// 		'remarks'=>'openning Balance',
// 		'debit'=>$AccountHead['AccountHead']['opening_balance'],
// 		'credit'=>0
// 		)
// 	);
// $Journal_credit=$this->Journal->find('all',array(
// 	'conditions'=>array(
// 		'Journal.credit'=>$account_head_id,
// 		'AccountHeadDebit.sub_group_id'=>array('1','2'),
// 		'Journal.flag=1',
// 		'Journal.date between ? and ?'=>array($from_date,$to_date),
// 		)
// 	));
//new lines
			$CustomerList=$this->AccountHead->find('all',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('Customer.account_head_id=AccountHead.id'),
						),
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
						),
					),
				'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
				'fields'=>array(
					'Customer.*',
					'AccountHead.id',
					'AccountHead.name',
					'AccountHead.opening_balance',
					),
				));
			if($CustomerList)
			{
				$All_Customer=[];
				foreach ($CustomerList as $key => $value) {
					$Single_Customer['id']=$value['AccountHead']['id'];
					$Single_Customer['name']=$value['AccountHead']['name'];
					$Single_Customer['shope_code']=$value['Customer']['code'];
					$Single_Customer['action'] = '';
					$Single_Customer['remarks'] = '';
					$Single_Customer['sort'] = 0;
//
					$Sale=$this->Sale->find('first',array(
						'conditions'=>array(
							'Sale.account_head_id'=>$value['AccountHead']['id'],
							'Sale.executive_id'=>$executive,
							'Sale.date_of_delivered'=>$date,
							'Sale.status'=>2,
							),
						'fields'=>array(
							'Sale.id',
							'Sale.invoice_no',
							),
						));
					$All_sale=[];
					if(!empty($Sale)){
						$All_sale['sale_id']=$Sale['Sale']['id'];
						$All_sale['invoice_no']=$Sale['Sale']['invoice_no'];
						$Single_Customer['action'] = 'Sale';
						$Single_Customer['remarks'] = 'Invoice No:'.$Sale['Sale']['invoice_no'];
						$Single_Customer['sort'] = 3;
					}
					$Single_Customer['sale']=$All_sale;
					$Saleqtn=$this->Sale->find('first',array(
						'conditions'=>array(
							'Sale.account_head_id'=>$value['AccountHead']['id'],
							'Sale.executive_id'=>$executive,
							'Sale.date_of_order'=>$date,
							'Sale.status'=>1,
							),
						'fields'=>array(
							'Sale.id',
							'Sale.invoice_no',
							),
						));
					$All_qtn =[];
					if(!empty($Saleqtn)){
						$All_qtn['sale_id']=$Saleqtn['Sale']['id'];
						$All_qtn['invoice_no']=$Saleqtn['Sale']['invoice_no'];
						$Single_Customer['action'] = 'Quotation';
						$Single_Customer['remarks'] = 'Quotation No:'.$Saleqtn['Sale']['invoice_no'];
						$Single_Customer['sort'] = 2;
					}
					$Single_Customer['quotation']=$All_qtn;
					$NoSale=$this->NoSale->find('first',array(
						'conditions'=>array(
							'NoSale.customer_account_head_id'=>$value['AccountHead']['id'],
							'NoSale.executive_id'=>$executive,
							'NoSale.date'=>$date,
							),
						'fields'=>array(
							'NoSale.id',
							'NoSale.description',
							),
						));
					$All_nosale =[];
					if(!empty($NoSale)){
						$All_nosale['no_sale_id']=$NoSale['NoSale']['id'];
						$All_nosale['description']=$NoSale['NoSale']['description'];
						$Single_Customer['action'] = 'No Sale';
						$Single_Customer['remarks'] = $NoSale['NoSale']['description'];
						$Single_Customer['sort'] = 1;
					}
					$Single_Customer['nosale']=$All_nosale;
//
					if($Single_Customer['action']!=''){
						array_push($All_Customer, $Single_Customer);
					}
				}
				array_multisort( array_column($All_Customer, "sort"), SORT_DESC, $All_Customer );
			}
//new lines end
// foreach ($Journal_credit as $key => $value) 
// {
// 	$Journal_single['date']=$value['Journal']['date'];
// 	$Journal_single['name']=$value['AccountHeadDebit']['name'];
// 	$Journal_single['remarks']=$value['Journal']['remarks'];
// 	$Journal_single['credit']=$value['Journal']['amount'];
// 	$Journal_single['debit']=0;
// 	$Journal_all[]=$Journal_single;
// }
// foreach ($Sale as $key => $value) {
// 	$Journal_single['date']=$value['Sale']['date_of_delivered'];
// 	$Journal_single['name']=$value['AccountHead']['name'];
// 	$Journal_single['remarks']='Invoice No : '.$value['Sale']['invoice_no'];
// 	$Journal_single['debit']=$value['Sale']['grand_total'];
// 	$Journal_single['credit']=0;
// 	$Journal_all[]=$Journal_single;
// }
			$balance=0;
//$Journal_all = Set::sort($Journal_all, '{n}.date', 'asc');
			$i=0;
			$Slno =0;
			$display_date = date('d-m-Y',strtotime($date));
			foreach ($All_Customer as $key => $value) {
				$Slno ++;
				$pdf->Text($x, $y+5+(($i+1)*8), $Slno);
				$pdf->Text($Date_line_x+10, $y+5+(($i+1)*8), $value['name']);
				$pdf->Text($AccountHead_line_x+10, $y+5+(($i+1)*8), $value['action']);
				$pdf->Text($Remark_line_x+5, $y+5+(($i+1)*8),$value['remarks']);
// $pdf->Text($Credit_line_x+5, $y+5+(($i+1)*8),'' );
// $balance+=$value['debit']-$value['credit'];
// $pdf->Text($Debit_line_x+5, $y+5+(($i+1)*8), 0);
				if($i>33)
				{
					$i=-1;
					header_section($pdf);
				}
				$i++;
			}
			$pdf->Output();
// $pdf->Output($account_head_id.'.pdf', 'D');
			exit;
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function CustomerPerformance()
	{
		$this->request->data['from_date']=date("d-m-Y", strtotime('-1 day'));
		$this->request->data['to_date']=date("d-m-Y");
		$this->set('CustomerType',$this->CustomerType->find('list',array('fields'=>array('id','name'))));
	}
	public function customer_performance_ajax_new()
	{
		$requestData=$this->request->data;
		$conditions=[];
		$from=date('Y-m-d',strtotime($requestData['from_date']));
		$to=date('Y-m-d',strtotime($requestData['to_date']));
		$customer_type_id = $requestData['customer_type_id'];
		if($customer_type_id) $conditions['CustomerType.id']=$customer_type_id;
		$CustomerType=$this->CustomerType->find('list',array('conditions'=>$conditions,'fields'=>['id','name']));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$Data=[];
		$totalData=0;
		$totalFiltered=0;
		foreach ($CustomerType as $customer_type_id => $customer_type_name) {
			$conditions=[];
			$conditions['Sale.date_of_delivered between ? and ?' ]=array($from,$to);
			$conditions['Customer.customer_type_id']=$customer_type_id;
			$Customer=$this->Sale->find('list',[
				'joins'=>array(
					array(
						'table'=>'customers',
						'alias'=>'Customer',
						'type'=>'INNER',
						'conditions'=>array('Customer.account_head_id=Sale.account_head_id')
						),
					),
				'conditions'=>$conditions,
				'fields'=>['account_head_id','account_head_id'],
				]);
			$conditions=[];
			$conditions['SalesReturn.date between ? and ?' ]=array($from,$to);
			$conditions['Customer.customer_type_id']=$customer_type_id;
			$Customer+=$this->SalesReturn->find('list',[
				'joins'=>array(
					array(
						'table'=>'customers',
						'alias'=>'Customer',
						'type'=>'INNER',
						'conditions'=>array('Customer.account_head_id=SalesReturn.account_head_id')
						),
					),
				'conditions'=>$conditions,
				'fields'=>['account_head_id','account_head_id'],
				]);
			$this->SaleItem->virtualFields = array( 
				'sale_amount' => "SUM(Sale.grand_total)",
				'sale_cost' => "SUM(SaleItem.net_value)",
				'product_cost' => "SUM(SaleItem.quantity*Product.cost)",
				);
			$this->SalesReturnItem->virtualFields = array( 
				'sale_return_amount' => "SUM(SalesReturn.grand_total)",
				'sale_return_cost' => "SUM(SalesReturnItem.net_value)",
				'product_cost' => "SUM(SalesReturnItem.quantity*Product.cost)",
				);
			$totalData+=$totalFiltered+=count($Customer);
			foreach ($Customer as $key => $account_head_id) {
				$conditions=[];
				$conditions['Sale.date_of_delivered between ? and ?' ]=array($from,$to);
				$conditions['Sale.account_head_id']=$account_head_id;
				if($customer_type_id) $conditions['CustomerType.id']=$customer_type_id;
				$SaleItem=$this->SaleItem->find('first',array(
					'joins'=>array(
						array(
							'table'=>'account_heads',
							'alias'=>'AccountHead',
							'type'=>'INNER',
							'conditions'=>array('AccountHead.id=Sale.account_head_id')
							),
						array(
							'table'=>'customers',
							'alias'=>'Customer',
							'type'=>'INNER',
							'conditions'=>array('Customer.account_head_id=Sale.account_head_id')
							),
						array(
							'table'=>'customer_types',
							'alias'=>'CustomerType',
							'type'=>'INNER',
							'conditions'=>array('CustomerType.id=Customer.customer_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>[
					'AccountHead.name',
					'CustomerType.name',
					'SaleItem.sale_amount',
					'SaleItem.sale_cost',
					'SaleItem.product_cost',
					]
					));
				$conditions=[];
				$conditions['SalesReturn.date between ? and ?' ]=array($from,$to);
				$conditions['SalesReturn.account_head_id']=$account_head_id;
				if($customer_type_id) $conditions['CustomerType.id']=$customer_type_id;
				$SalesReturnItem=$this->SalesReturnItem->find('first',array(
					'joins'=>array(
						array(
							'table'=>'account_heads',
							'alias'=>'AccountHead',
							'type'=>'INNER',
							'conditions'=>array('AccountHead.id=SalesReturn.account_head_id')
							),
						array(
							'table'=>'customers',
							'alias'=>'Customer',
							'type'=>'INNER',
							'conditions'=>array('Customer.account_head_id=SalesReturn.account_head_id')
							),
						array(
							'table'=>'customer_types',
							'alias'=>'CustomerType',
							'type'=>'LEFT',
							'conditions'=>array('CustomerType.id=Customer.customer_type_id')
							),
						),
					'conditions'=>$conditions,
					'fields'=>[
					'AccountHead.name',
					'CustomerType.name',
					'SalesReturnItem.sale_return_amount',
					'SalesReturnItem.sale_return_cost',
					'SalesReturnItem.product_cost',
					]
					));
				$single['AccountHead']=$SaleItem['AccountHead']['name'];
				$single['CustomerType']=$SaleItem['CustomerType']['name']?$SaleItem['CustomerType']['name']:'GENERAL';
				$single['sale_amount']=floatval($SaleItem['SaleItem']['sale_amount']);
				$single['sale_cost']=floatval($SaleItem['SaleItem']['sale_cost']);
				$single['sale_product_cost']=floatval($SaleItem['SaleItem']['product_cost']);
				$single['sale_return_amount']=floatval($SalesReturnItem['SalesReturnItem']['sale_return_amount']);
				$single['sale_return_cost']=floatval($SalesReturnItem['SalesReturnItem']['sale_return_cost']);
				$single['return_product_cost']=floatval($SalesReturnItem['SalesReturnItem']['product_cost']);
				$single['profit']=$single['sale_cost']-($single['sale_product_cost']+$single['sale_return_cost'])+$single['return_product_cost'];
				if($single['sale_amount'] || $single['sale_cost'] || $single['sale_product_cost'] || $single['sale_return_amount'] || $single['sale_return_cost'] || $single['return_product_cost'] || $single['return_product_cost'])
					$Data[]=$single;
			}
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData), 
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	}
	public function customer_performance_ajax()
	{
		$data=$this->request->data;
		$customer_type_id = $data['customer_type_id'];
		$from = date('Y-m-d',strtotime($data['from_date']));
		$to = date('Y-m-d',strtotime($data['to_date']));
		$conditions=[];
		if($customer_type_id) $conditions['CustomerType.id']=$customer_type_id;
		$CustomerType=$this->CustomerType->find('list',array('conditions'=>$conditions,'fields'=>array('CustomerType.name')));
		$list_array=[];
		$list_array_single=[];
		$cutomertype=[];
		foreach ($CustomerType as $id => $name) {
			$Customer = $this->AccountHead->find('all',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('AccountHead.id=Customer.account_head_id'),
						),
					),
				'conditions'=>array('Customer.customer_type_id'=>$id),
				'fields'=>array(
					'AccountHead.id',
					'AccountHead.name',
					)
				));
			$cutomertype['name']=$name;
			$cutomertype['Customer']='';
			$cutomertype['id']=$id;
			$cutomertype['amount']=0;
			$cutomertype['return']=0;
			$cutomertype['cost']=0;
			$cutomertype['cost_Return']=0;
			$cutomertype['single_customer']=[];
			foreach ($Customer as $key => $value) {
				$customer_id=$value['AccountHead']['id'];
				$siglecutomertype['Customer']=$value['AccountHead']['name'];
				$Sale = $this->Sale->find('all',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$customer_id,
						'Sale.flag=1',
						'Sale.status=2',
						'Sale.date_of_delivered between ? and ?' => array($from,$to)
						),
					'fields'=>array(
						'Sale.*',
						)
					));
				$siglecutomertype['name']=$name;
				$siglecutomertype['id']=$id;
				$amount=0;
				$return_amount=0;
				$return_cost=0;
				$cost=0;
				$net_value=0;
				foreach ($Sale as $keySOff => $value) {
					$offline_id=$value['Sale']['id'];
					$amount+=$value['Sale']['grand_total'];
					foreach ($value['SaleItem'] as $keyO => $value1) {
						$product_id=$value1['product_id'];
						$net_value+=$value1['net_value'];
						$product_qty=$value1['quantity'];
						$Product=$this->Product->find('first',array('conditions'=>array('Product.id'=>$product_id)));
						if(!empty($Product)) {
							$cost+=$Product['Product']['cost']*$product_qty;
						}
					}
				}
				$SalesReturn = $this->SalesReturn->find('all',array(
					'conditions'=>array(
						'SalesReturn.account_head_id'=>$customer_id,
						'SalesReturn.flag=1',
						'SalesReturn.status=2',
						'SalesReturn.date between ? and ?' => array($from,$to)
						),
					'fields'=>array(
						'SalesReturn.*',
						)
					));
				foreach ($SalesReturn as $keySOff => $value) {
					$offline_id=$value['SalesReturn']['id'];
					$return_amount+=$value['SalesReturn']['grand_total'];
					foreach ($value['SalesReturnItem'] as $keyO => $value1) {
						$product_id=$value1['product_id'];
						$product_qty=$value1['quantity'];
						$Product=$this->Product->findById($product_id);
						if(!empty($Product)) {
							$return_cost+=$Product['Product']['cost']*$product_qty;
						}
					}
				}
				$siglecutomertype['cost']=$cost;
				$cutomertype['cost']+=$cost;
				$siglecutomertype['return']=$return_amount;
				$cutomertype['return']+=$return_amount;
				$siglecutomertype['amount']=$net_value;
				$cutomertype['amount']+=$net_value;
				$siglecutomertype['cost_Return']=$return_cost;
				$cutomertype['cost_Return']+=$return_cost;
				$cutomertype['single_customer'][]=$siglecutomertype;
			}
			$list_array[]=$cutomertype;
		}
		if($list_array)
		{
			$per=0;
			foreach ($list_array as $key => $value) {
				if($value['amount']){
					echo "<tr class='blue-pd' id='parent-".$value['id']."'>";
					echo '<td>'.$value['name'].'</td>';
					echo '<td>'.$value['Customer'].'</td>';
					echo '<td>'.$value['amount'].'</td>';
					echo '<td>'.$value['cost'].'</td>';
					echo '<td>'.$value['return'].'</td>';
					echo '<td>'.$value['cost_Return'].'</td>';
					$per=$value['amount']-($value['cost']+$value['return'])+$value['cost_Return'];
					echo '<td>'.$per.'</td>';
					echo '</tr>';
					$perS=0;
					foreach ($value['single_customer'] as $keyS => $valueS) :
						echo "<tr class='toggle_rows blue-pd child-parent-".$valueS['id']."'>";
					echo '<td></td>';
					if(isset($valueS['Customer'])){
						echo '<td>'.$valueS['Customer'].'</td>';
					}
					else
					{
						echo "<td><td>";
					}
					echo '<td>'.$valueS['amount'].'</td>';
					echo '<td>'.$valueS['cost'].'</td>';
					echo '<td>'.$valueS['return'].'</td>';
					echo '<td>'.$valueS['cost_Return'].'</td>';
					$perS=$valueS['amount']-($valueS['cost']+$valueS['return'])+$valueS['cost_Return'];
					echo '<td>'.$perS.'</td>';
					echo '</tr>';
					endforeach;
				}
			}
		}
		else
		{
			echo '<tr><td colspan="9" align="center"><h3>No Data is Available</h3></td></tr>';
		}
		exit;
	}
	public function SaleOutstanding1($id = null,$to_date = null)
	{
		if(!$to_date)
			$to_date=date('d-m-Y');
		$this->request->data['to_date']=$to_date;
		$this->Customer->virtualFields = array('customer_name' => "CONCAT(AccountHead.name, ' ', Customer.code)");

		$customer_list=$this->Customer->find('list',array(
			"joins"=>array(
				array(
					"table"=>'account_heads',
					"alias"=>'AccountHead',
					"type"=>'INNER',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				),
			'fields'=>array(
				'AccountHead.id','customer_name'
				)
			)
		);
		$this->set('Customers',$customer_list);
		$this->set('customer_id',$id);
		if($id){
			$Outstanding=$this->getOutstandingData($id,$to_date);
// $OutstandingCheques=$this->getOutstandingDataCheques($id,$to_date);
// $this->set('OutstandingCheques',$OutstandingCheques);
			$this->set('Outstanding',$Outstanding);
		}
	}
// public function getOutstandingDataCheques($id,$to_date)
// {
// 	$conditions=array('AccountHead.id'=>$id,'Journal.work_flow'=>'Account Recievable');
// 	$Cheques=$this->Check->find('all',array('conditions'=>array('status != 1','account_head_id'=>$id)));
// 	$OutstandingCheque=array();
// 	$sl_no=1;
// 	foreach ($Cheques as $key => $value) {
// 		$status='Uncleared';
// 		if($value['Check']['status']==1){
// 			$status='Cleared';
// 		}else if($value['Check']['status']==2){
// 			$status='Bounce';
// 		}
// 		$OutstandingCheque[]=array(
// 			'sl_no'=>$sl_no,
// 			'check_no'=>$value['Check']['check_no'],
// 			'check_amount'=>$value['Check']['check_amount'],
// 			'recieved_date'=>date('d-M-Y',strtotime($value['Check']['recieved_date'])),
// 			'check_date'=>date('d-M-Y',strtotime($value['Check']['check_date'])),
// 			'status'=>$status,
// 			);
// 		$sl_no++;
// 	}
// 	return $OutstandingCheque;
// }
	public function getOutstandingData($id,$to_date)
	{
		$conditions=array('AccountHead.id'=>$id,'Journal.work_flow'=>'Account Recievable');
		$OpeningBalance=$this->AccountHead->findById($id);
		$Sale=$this->Sale->find('all',array(
			'conditions'=>array(
				'Sale.status > 1',
				'Sale.account_head_id'=>$id,
				'Sale.date_of_delivered <= '=>date('Y-m-d',strtotime($to_date)),
				),
			'fields'=>array(
				),
			));
		$SalesReturn=$this->SalesReturn->find('all',array(
			'conditions'=>array(
				'SalesReturn.status >1',
				'SalesReturn.account_head_id'=>$id,
				),
			'fields'=>array(
				),
			));
		$Journal_credit=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.credit'=>$id,
				'Journal.flag=1',
				'OR' => array(
					array('AccountHeadDebit.sub_group_id'=>array('1','2')),
					array('AccountHeadDebit.id'=>'135')
					),
				)
			));
		foreach ($Sale as $key => $value) {
			$Sale[$key]['Sale']['paid_amount']=0;
			foreach ($Journal_credit as $keyJC => $valueJC) {
				if ($valueJC['Journal']['remarks'] ==  "Sale Invoice No :".$value['Sale']['invoice_no']) {
					$Sale[$key]['Sale']['paid_amount']+=$valueJC['Journal']['amount'];
					unset($Journal_credit[$keyJC]);
				}
			}
		}
		$total_opening_paid=0;
		foreach ($Journal_credit as $keyJC => $valueJC) {
			$total_opening_paid+=$valueJC['Journal']['amount'];
		}
		foreach ($SalesReturn as $keySR => $valueSR) {
			$total_opening_paid+=$valueSR['SalesReturn']['grand_total'];
		}
		$Outstanding[0]=array(
			'sl_no'=>1,
			'date'=>'',
			'invoice_no'=>'Opening Balance',
			'total'=>round($OpeningBalance['AccountHead']['opening_balance']),
			'paid'=>round($total_opening_paid),
			'balance'=>round($OpeningBalance['AccountHead']['opening_balance']-$total_opening_paid),
			'days'=>''
			);
		$sl_no=2;
		foreach ($Sale as $key => $value) {
			$datedif = time() - strtotime($value['Sale']['date_of_delivered']);
			$days=floor($datedif / (60 * 60 * 24));
			$Outstanding[]=array(
				'sl_no'=>$sl_no,
				'date'=>date('d-M-Y',strtotime($value['Sale']['date_of_delivered'])),
				'invoice_no'=>$value['Sale']['invoice_no'],
				'total'=>round($value['Sale']['grand_total']),
				'paid'=>round($value['Sale']['paid_amount']),
				'balance'=>round($value['Sale']['grand_total']-$value['Sale']['paid_amount']),
				'days'=>$days
				);
			$sl_no++;
		}
		$total_amount=0;
		$total_paid=0;
		$total_balance=0;
		foreach ($Outstanding as $key => $value) {
			$total_amount+=$value['total'];
			$total_paid+=$value['paid'];
			$total_balance+=$value['balance'];
			if($value['balance'] == 0){
				unset($Outstanding[$key]);
			}
		}
		$Outstanding[]=array(
			'sl_no'=>'',
			'date'=>'',
			'invoice_no'=>'Total',
			'total'=>$total_amount,
			'paid'=>$total_paid,
			'balance'=>$total_balance,
			'days'=>''
			);
		return $Outstanding;
	}
	public function OutstandingPrint($id=null,$to_date = null,$flag = null)
	{
		if($id){
			if(!$to_date)
				$to_date=date('d-m-Y');
			$AccountHead=$this->AccountHead->findById($id);
			$Outstanding=$this->getOutstandingData($id,$to_date);
// $OutstandingCheque=$this->getOutstandingDataCheques($id,$to_date);
			$header=array('Sl No','Invoice Date','Invoice Number','Invoice Amount','Paid','Balance','Days');
// $header_cheque=array('Sl No','Cheque Number','Cheque Amount','Received Date','Cheque Date','Status');
// $width_cheque=array(12,33,40,40,25,25);
			$width=array(12,30,40,40,25,25);
			if($flag != 1){
				foreach ($Outstanding as $key => $value) {
					unset($Outstanding[$key]['days']);
				}
				unset($header[6]);
			}
// pr($Outstanding);
// pr($header);
			if($flag == 1)
				$width[6]=15;
			require('fpdf/fpdf.php');
			$pdf = new FPDF('p');
// $pdf->SetFont('Arial','B',14);
			$Profile=$this->Global_Var_Profile;
// pr($Profile); exit;
			$pdf->AddPage();
			$pdf->setFont("Arial",'B','12');           
// $pdf->Image('profile/'.$Profile['Profile']['logo'],85,5,-300);
			$pdf->Text(62, 15, $Profile['Profile']['company_name']);
			$pdf->setFont("Arial",'B','9');
			$pdf->Text(60, 20, $Profile['Profile']['address_line_1']);
			$pdf->Text(53, 24, $Profile['Profile']['mail_address'].",".$Profile['Profile']['web_site_adddress']);
			$pdf->Line(1,26,209,26);
			$pdf->SetFont('Arial','B','11');
			$pdf->Text(10, 33, 'Outstanding Report Of '.$AccountHead['AccountHead']['name'].' To '.date('d-M-Y',strtotime($to_date)));
			$pdf -> SetY(37);
// pr($header);exit; 
			for($i=0;$i<count($header);$i++)
				$pdf->Cell($width[$i],7,$header[$i],1,0,'C');
			$pdf->Ln();
			$pdf->SetFillColor(224,235,255);
			$fill = false;
			$pdf->SetFont('Arial','','10');
			foreach($Outstanding as $row)
			{
				$i=0;
				foreach($row as $col){
					$pdf->Cell($width[$i],6,$col,'LR',0,'L',$fill);
// $pdf->Cell($width[$i],6,$col,1,0,'L',$fill);
					$i++;
				}
				$pdf->Ln();
				$fill = !$fill;
			}
			$pdf->Cell(array_sum($width),0,'','T');
// $pdf->AddPage();
// $pdf->SetFont('Arial','B','11');
// $pdf->Text(10, $i*8+35, 'Post Dated Cheques Of '.$AccountHead['AccountHead']['name']);
// $pdf->setFont("Arial",'B','9');           
// $pdf->Image("img/liyalams_logo.jpeg",85,5,-300);
// $pdf->Text(48, 20, '15/85 A.VALIYORA P O,VENGARA. MALAPPURAM-676304,TEL:0494 2450242');
// $pdf->Text(76, 24, 'www.liyalams.com,liyalams@gmail.com');
// $pdf->Line(1,26,209,26);
			$count=count($Outstanding);
			$pdf->SetFont('Arial','B','11');
// $pdf->Text(10, 48+($count*8), 'Post Dated Cheques Of '.$AccountHead['AccountHead']['name'].' To '.date('d-M-Y',strtotime($to_date)));
			$pdf -> SetY(49+($count*8));
// echo $count;
// exit;
// for($i=0;$i<count($header_cheque);$i++)
// 	$pdf->Cell($width_cheque[$i],7,$header_cheque[$i],1,0,'C');
			$pdf->Ln();
			$pdf->SetFillColor(224,235,255);
			$fill = false;
// $pdf->SetFont('Arial','','10');
// foreach($OutstandingCheque as $row)
// {
// 	$i=0;
// 	foreach($row as $col){
// 		$pdf->Cell($width_cheque[$i],6,$col,'LR',0,'L',$fill);
// 	// $pdf->Cell($width[$i],6,$col,1,0,'L',$fill);
// 		$i++;
// 	}
// 	$pdf->Ln();
// 	$fill = !$fill;
// }
// $pdf->Cell(array_sum($width_cheque),0,'','T');
			$pdf->Output();
		}
		exit;
	}
	public function InvoiceMargin()
	{
		$data=$this->request->data;
		$list_array=array();
		if(!$data)
		{
			$date0= date('m/d/Y');
			$date1=date("d-m-Y", strtotime($date0));
			$time = strtotime($date1);
			$final1 = date('m/d/Y', strtotime("-1 day",$time));
			$final=date("d-m-Y", strtotime($final1) );
			$this->set('date1',$date1);
			$this->set('final',$final);
		}
		else
		{
			$from_date=$data['InvoiceMargin']['from_date'];
			$to_date=$data['InvoiceMargin']['to_date'];
			$from=date("Y-m-d", strtotime($from_date));
			$to=date("Y-m-d", strtotime($to_date));
			$this->set('date1',$to_date);
			$this->set('final',$from_date);
			$list_array=$this->invoice_margin_report($from,$to);
		}
		$this->set('list_array',$list_array);
	}
	public function invoice_margin_report($from,$to)
	{
		$list_array=array();
		$list_array_single=array();
		$cutomertype=array();
		$SalesOffline = $this->Sale->find('all',array(
			'conditions'=>array(
// 'Sale.account_head_id'=>$customer_id,
				'Sale.flag=1',
				'Sale.status=2',
				'Sale.date_of_delivered between ? and ?' => array($from,$to)
				),
			'fields'=>array(
				'Sale.*',
				)
			));
		$cutomertype['single_invoice']=[];
		foreach ($SalesOffline as $keySOff => $valueSoff) {
			$offline_id=$valueSoff['Sale']['id'];
			$sale_invoice_no=$valueSoff['Sale']['invoice_no'];
			$siglecutomertype['invoice_no']=$sale_invoice_no;
			$siglecutomertype['invoice_date']=date('d-m-Y',strtotime($valueSoff['Sale']['date_of_delivered']));
			$amountOffline=0;
			$cost=0;
			foreach ($valueSoff['SaleItem'] as $keySO => $valueSoff11) {
				$product_id_fk=$valueSoff11['product_id'];
				$Product=$this->Product->find('first',array('conditions'=>array('Product.id'=>$product_id_fk)));
				$product_qty=$valueSoff11['quantity'];
				$amountOffline+=$valueSoff11['quantity']*$valueSoff11['unit_price'];
				if(!empty($Product)) {
					$cost+=$Product['Product']['cost']*$product_qty;
				}
			}
			$siglecutomertype['amount']=$amountOffline;
			$siglecutomertype['cost']=$cost;
			$siglecutomertype['margin']=round(($amountOffline-$cost),3);
			$cutomertype['single_invoice'][]=$siglecutomertype;
		}
		$list_array[]=$cutomertype;
		return $list_array;
	}
	public function TaxReport()
	{
		$data['TaxReport']['from_date']=$from_date=date('d-m-Y',strtotime('-1 month'));
		$data['TaxReport']['to_date']=$to_date=date('d-m-Y');
		$this->request->data=$data;
		$SaleItem_DISTINCT_cgst=$this->SaleItem->find('all',array(
			'fields'=>array('DISTINCT SaleItem.tax'),
			));
		$PurchaseItem_DISTINCT_cgst=$this->PurchasedItem->find('all',array(
			'fields'=>array('DISTINCT PurchasedItem.tax'),
			));
		$taxes=[];
		foreach ($SaleItem_DISTINCT_cgst as $key => $value) {
			$taxes[floatval($value['SaleItem']['tax'])]=floatval($value['SaleItem']['tax'])."%";
		}
		foreach ($PurchaseItem_DISTINCT_cgst as $key => $value) {
			$taxes[floatval($value['PurchasedItem']['tax'])]=floatval($value['PurchasedItem']['tax'])."%";
		}
		$taxes['Zero']='0%';
		$this->set(compact('taxes'));
	}
	public function TaxReportAjax()
	{
		$system_state_id=$this->Global_Var_Profile['Profile']['state_id'];
		$data=$this->request->data;
		$data['account_head_id']='';
		$data['tax']='';
		$date=$data['date'];
		$account_head_id=$data['account_head_id'];
		$tax=$data['tax'];
		$explode_date=explode('-',$date);
		$from=date('Y-m-d',strtotime(trim($explode_date[0])));
		$to=date('Y-m-d',strtotime(trim($explode_date[1])));
		$list=[];
		$type=$data['type'];
		$foot['net_value']=0;
		$foot['discount']=0;
		$foot['taxable_amount']=0;
		$foot['tax_amount']=0;
		$foot['cgst']=0;
		$foot['sgst']=0;
		$foot['igst']=0;
		$foot['total']=0;
		$foot['vat']=0;
		$foot['insurance']=0;
		$foot['miscellanious']=0;
		if($type=='Purchase')
		{
			$party_conditions=[];
			if($data['account_head_id'])
				$party_conditions['Party.account_head_id']=$data['account_head_id'];

		
				
				$conditions=[];
				$conditions['Purchase.date_of_delivered between ? and ?']=array($from,$to);
				//$conditions['Purchase.account_head_id']=$party['Party']['account_head_id'];
				$conditions['Purchase.status >=']=2;
				$conditions['Purchase.total_tax_amount !=']=0;
				$this->Purchase->unbindModel(array('hasMany' => array('PurchasedItem')));
				$Purchase=$this->Purchase->find('all',[
					'conditions'=>$conditions,
					'fields'=>array(
						'Purchase.id','Purchase.account_head_id',
						'Purchase.invoice_no',
						'Purchase.date_of_delivered',
						'Purchase.date_of_purchase',
						'Purchase.total_tax_amount',
						'Purchase.grand_total',
						'Purchase.discount_amount', 
//'Purchase.current_date',
						'Purchase.total', 
//'Purchase.insurance_total', 
//'Purchase.miscellanious_expense_total', 
						),
					'order'=>array('Purchase.date_of_delivered DESC'),
					]);
				foreach ($Purchase as $key => $value) {
					$party=$this->Party->find('first',array('conditions'=>array('Party.account_head_id'=>$value['Purchase']['account_head_id'])));
					$single['name']=$party['AccountHead']['name'];
					$single['gstin']=$party['Party']['vat_no']?$party['Party']['vat_no']:'';
				$single['place']=$party['Party']['place']?$party['Party']['place']:'';
					$single['invoice_no']=$value['Purchase']['invoice_no'];
					$single['date']=date('d-m-Y',strtotime($value['Purchase']['date_of_delivered']));
					$single['invoice_date']=date('d-m-Y',strtotime($value['Purchase']['date_of_purchase']));
					$single['purchase_total']=floatval($value['Purchase']['total']);
//$single['insurance']=floatval($value['Purchase']['insurance_total']);
//$single['miscellanious']=floatval($value['Purchase']['miscellanious_expense_total']);
					$conditionsItem=[];
					$conditionsItem['Purchase.id']=$value['Purchase']['id'];
					if($tax)
					{
						$conditionsItem['PurchasedItem.tax']=$tax;
						if($tax=='Zero')
							$conditionsItem['PurchasedItem.tax']=0;	
					}
					$this->PurchasedItem->virtualFields = array( 
						'total_tax_amount' => "SUM(PurchasedItem.tax_amount)",
						'total_net_value' => "SUM(PurchasedItem.unit_price*PurchasedItem.quantity)",
						'total_discount' => "SUM(PurchasedItem.discount)",
						);
					$PurchasedItem=$this->PurchasedItem->find('first',[
						'conditions'=>$conditionsItem,
						'fields'=>array(
							'PurchasedItem.total_tax_amount',
							'PurchasedItem.total_net_value',
							'PurchasedItem.total_discount',
							),
						]);
					//$single['tax_amount']=floatval($PurchasedItem['PurchasedItem']['total_tax_amount']);
					$state_id=$party['Party']['state_id'];
					if($state_id==19)
					{
					$single['tax_amount']=$value['Purchase']['total_tax_amount'];
					$single['cgst']=round(($value['Purchase']['total_tax_amount']/2),3);
					$single['igst']=0;
					}
					else
					{
					$single['tax_amount']=$value['Purchase']['total_tax_amount'];
					$single['cgst']=0;	
					$single['igst']=round(($value['Purchase']['total_tax_amount']),3);	
					}
					$single['net_value']=$PurchasedItem['PurchasedItem']['total_net_value'];
					$single['discount']=$value['Purchase']['discount_amount'];
					$single['taxable_amount']=$PurchasedItem['PurchasedItem']['total_net_value'];
					$single['total']=$value['Purchase']['grand_total'];
					//$single['total']+=$PurchasedItem['PurchasedItem']['total_tax_amount'];
					 $single['net_value']=round($single['net_value'],2);
					  $single['tax_amount']=round($single['tax_amount'],2);
					$single['discount']=round($value['Purchase']['discount_amount'],2);
					$single['taxable_amount']=round($single['taxable_amount'],2);
					$single['total']=round($single['total'],2);
					$foot['net_value']+=$single['net_value'];
					$foot['discount']+=$single['discount'];
					$foot['taxable_amount']+=$single['taxable_amount'];
					$foot['cgst']+=$single['cgst'];
					$foot['igst']+=$single['igst'];
					$foot['total']+=$single['total'];
					$list['body'][]=$single;
				//}
			}
			$list['foot']=$foot;
		}
		else if($type=='purchasereturn')
		{

			$party_conditions=[];
			if($data['account_head_id'])
				$party_conditions['Party.account_head_id']=$data['account_head_id'];
				$conditions=[];
				$conditions['PurchaseReturn.date between ? and ?']=array($from,$to);
				//$conditions['PurchaseReturn.account_head_id']=$party['Party']['account_head_id'];
				$conditions['PurchaseReturn.status >=']=2;
				$this->PurchaseReturn->unbindModel(array('hasMany' => array('PurchaseReturnItem')));
				$PurchaseReturn=$this->PurchaseReturn->find('all',[
					'conditions'=>$conditions,
					'fields'=>array(
						'PurchaseReturn.id','PurchaseReturn.account_head_id',
						'PurchaseReturn.invoice_no',
						'PurchaseReturn.date',
						'PurchaseReturn.total_tax_amount',
						'PurchaseReturn.grand_total',
						'PurchaseReturn.discount', 
						'PurchaseReturn.total', 
						),
					'order'=>array('PurchaseReturn.date DESC'),
					]);
				foreach ($PurchaseReturn as $key => $value) {
					$party=$this->Party->find('first',array('conditions'=>array('Party.account_head_id'=>$value['PurchaseReturn']['account_head_id'])));
					$single['name']=$party['AccountHead']['name'];
					$single['gstin']=$party['Party']['vat_no']?$party['Party']['vat_no']:'';
				$single['place']=$party['Party']['place']?$party['Party']['place']:'';
					$single['invoice_no']=$value['PurchaseReturn']['invoice_no'];
					$single['date']=date('d-m-Y',strtotime($value['PurchaseReturn']['date']));
					$single['invoice_date']=date('d-m-Y',strtotime($value['PurchaseReturn']['date']));
					$single['purchase_total']=floatval($value['PurchaseReturn']['total']);
//$single['insurance']=floatval($value['Purchase']['insurance_total']);
//$single['miscellanious']=floatval($value['Purchase']['miscellanious_expense_total']);
					$conditionsItem=[];
					$conditionsItem['PurchaseReturn.id']=$value['PurchaseReturn']['id'];
					if($tax)
					{
						$conditionsItem['PurchaseReturnItem.tax']=$tax;
						if($tax=='Zero')
							$conditionsItem['PurchaseReturnItem.tax']=0;	
					}
					$this->PurchaseReturnItem->virtualFields = array( 
						'total_tax_amount' => "SUM(PurchaseReturnItem.tax_amount)",
						'total_net_value' => "SUM(PurchaseReturnItem.unit_price*PurchaseReturnItem.quantity)",
						);
					$PurchaseReturnItem=$this->PurchaseReturnItem->find('first',[
						'conditions'=>$conditionsItem,
						'fields'=>array(
							'PurchaseReturnItem.total_tax_amount',
							'PurchaseReturnItem.total_net_value',
							),
						]);
					//$single['tax_amount']=floatval($PurchaseReturnItem['PurchaseReturnItem']['total_tax_amount']);
					$state_id=$party['Party']['state_id'];
					if($state_id==19)
					{
					$single['tax_amount']=$PurchaseReturnItem['PurchaseReturnItem']['total_tax_amount'];
					$single['cgst']=round(($PurchaseReturnItem['PurchaseReturnItem']['total_tax_amount']/2),3);
					$single['igst']=0;
					}
					else
					{
					$single['tax_amount']=$PurchaseReturnItem['PurchaseReturnItem']['total_tax_amount'];
					$single['cgst']=0;	
					$single['igst']=round(($PurchaseReturnItem['PurchaseReturnItem']['total_tax_amount']),3);	
					}
					$single['net_value']=$PurchaseReturnItem['PurchaseReturnItem']['total_net_value'];
					$single['discount']=$value['PurchaseReturn']['discount'];
					$single['taxable_amount']=$PurchaseReturnItem['PurchaseReturnItem']['total_net_value'];
					$single['total']=$value['PurchaseReturn']['grand_total'];
					//$single['total']+=$PurchaseReturnItem['PurchaseReturnItem']['total_tax_amount'];
					 $single['net_value']=round($single['net_value'],2);
					  $single['tax_amount']=round($single['tax_amount'],2);
					$single['discount']=round($value['PurchaseReturn']['discount'],2);
					$single['taxable_amount']=round($single['taxable_amount'],2);
					$single['total']=round($single['total'],2);
					$foot['net_value']+=$single['net_value'];
					$foot['discount']+=$single['discount'];
					$foot['taxable_amount']+=$single['taxable_amount'];
					$foot['cgst']+=$single['cgst'];
					$foot['igst']+=$single['igst'];
					$foot['total']+=$single['total'];
					$list['body'][]=$single;
				//}
			}
			$list['foot']=$foot;
		
		}
		else if($type=='Salereturn')
		{

			$customer_conditions=[];
			if($data['account_head_id'])
				$customer_conditions['Customer.account_head_id']=$data['account_head_id'];
			$customers=$this->Customer->find('all',array(
				'conditions'=>$customer_conditions,
				'fields'=>['Customer.account_head_id','Customer.vat_no','AccountHead.name','Customer.place','Customer.state_id','CustomerType.name','CustomerType.name']
				));
			$SalesReturn=$this->SalesReturn->find('all',array(
			'conditions'=>array('SalesReturn.invoice_no IS NOT NULL',
				'SalesReturn.status=2',
				'SalesReturn.flag=1',
				'SalesReturn.date between ? and ?' => array($from,$to),
			),
			'fields'=>array(
				'SalesReturn.id',
				'SalesReturn.invoice_no',
				'SalesReturn.date',
				'SalesReturn.grand_total',
				'SalesReturn.account_head_id',
				'SalesReturn.discount',
			),
				'order'=>array('SalesReturn.date DESC'),
		));
			foreach ($SalesReturn  as $key => $value) {
				$customer=$this->Customer->find('first',array('conditions'=>array('Customer.account_head_id'=>$value['SalesReturn']['account_head_id'])));
				$single['gstin']="";$single['name']="";$single['place']="";$single['type']="";
				if(!empty($customer))
				{
                 $single['gstin']=$customer['Customer']['vat_no']?$customer['Customer']['vat_no']:'';
                $state_id=$customer['Customer']['state_id'];
				$single['name']=$customer['AccountHead']['name'];
				$single['place']=$customer['Customer']['place']?$customer['Customer']['place']:'';
				$single['type']=$customer['CustomerType']['name'];
				}
				$single['invoice_no']=$value['SalesReturn']['invoice_no'];
				$single['date']=date('d-m-Y',strtotime($value['SalesReturn']['date']));
				//$single['SalesReturn_total']=floatval($value['SalesReturn']['total']);
					$conditionsItem=[];
					$conditionsItem['SalesReturn.id']=$value['SalesReturn']['id'];
					$this->SalesReturnItem->virtualFields = array( 
				     'total_net_value' => "SUM(SalesReturnItem.unit_price*SalesReturnItem.quantity)",
						'total_tax_amount' => "SUM(SalesReturnItem.tax_amount)",
						'total_taxable_amount' => "SUM(SalesReturnItem.net_value)",
						'SalesReturnItem_total' => "SUM((SalesReturnItem.unit_price*SalesReturnItem.quantity)+SalesReturnItem.tax_amount)",
						);
					$SalesReturnItem=$this->SalesReturnItem->find('first',[
						'conditions'=>$conditionsItem,
						'fields'=>array(
							'SalesReturnItem.total_tax_amount',
							'SalesReturnItem.total_net_value',
							'SalesReturnItem.SalesReturnItem_total',
							'SalesReturnItem.total_taxable_amount'
							),
						]);
                     if($state_id==19)
					{
					$single['tax_amount']=$SalesReturnItem['SalesReturnItem']['total_tax_amount'];
					$single['cgst']=round(($SalesReturnItem['SalesReturnItem']['total_tax_amount']/2),3);
					$single['igst']=0;
					}
					else
					{
					$single['tax_amount']=$SalesReturnItem['SalesReturnItem']['total_tax_amount'];
					$single['cgst']=0;	
					$single['igst']=round(($SalesReturnItem['SalesReturnItem']['total_tax_amount']),3);	
					}
				    $single['vat']=$SalesReturnItem['SalesReturnItem']['total_tax_amount'];
					$single['net_value']=$SalesReturnItem['SalesReturnItem']['total_net_value'];
					$single['discount']=$value['SalesReturn']['discount'];
					$single['taxable_amount']=$SalesReturnItem['SalesReturnItem']['total_taxable_amount'];
					$single['total']=$value['SalesReturn']['grand_total'];
					$single['net_value']=round($single['net_value'],2);
					$single['vat']=round($single['vat'],2);
					$single['discount']=round($single['discount'],2);
					$single['taxable_amount']=round($single['taxable_amount'],2);
					$single['total']=round($single['total'],2);
					$foot['net_value']+=$single['net_value'];
					$foot['vat']+=$single['vat'];
					$foot['discount']+=$single['discount'];
					$foot['total']+=$single['total'];
					$foot['taxable_amount']+=$single['taxable_amount'];
					$foot['cgst']+=$single['cgst'];
					$foot['igst']+=$single['igst'];
					$list['body'][]=$single;
			}
			$list['foot']=$foot;
		
		}
		else
		{
			$customer_conditions=[];
			if($data['account_head_id'])
				$customer_conditions['Customer.account_head_id']=$data['account_head_id'];
			$customers=$this->Customer->find('all',array(
				'conditions'=>$customer_conditions,
				'fields'=>['Customer.account_head_id','Customer.vat_no','AccountHead.name','Customer.place','Customer.state_id','CustomerType.name','CustomerType.name']
				));
			$Sale=$this->Sale->find('all',array(
			'conditions'=>array('Sale.invoice_no IS NOT NULL',
				'Sale.status=2',
				'Sale.flag=1',
				'Sale.date_of_delivered between ? and ?' => array($from,$to),
			),
			'fields'=>array(
				'Sale.id',
				'Sale.invoice_no',
				'Sale.date_of_delivered',
				'Sale.grand_total',
				'Sale.account_head_id',
				'Sale.discount_amount',
			),
				'order'=>array('Sale.date_of_delivered DESC'),
		));
			foreach ($Sale  as $key => $value) {
				$customer=$this->Customer->find('first',array('conditions'=>array('Customer.account_head_id'=>$value['Sale']['account_head_id'])));
				$single['gstin']="";$single['name']="";$single['place']="";$single['type']="";
				if(!empty($customer))
				{
                 $single['gstin']=$customer['Customer']['vat_no']?$customer['Customer']['vat_no']:'';
                $state_id=$customer['Customer']['state_id'];
				$single['name']=$customer['AccountHead']['name'];
				$single['place']=$customer['Customer']['place']?$customer['Customer']['place']:'';
				$single['type']=$customer['CustomerType']['name'];
				}
				$single['invoice_no']=$value['Sale']['invoice_no'];
				$single['date']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
				//$single['Sale_total']=floatval($value['Sale']['total']);
					$conditionsItem=[];
					$conditionsItem['Sale.id']=$value['Sale']['id'];
					$this->SaleItem->virtualFields = array( 
				     'total_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
						'total_tax_amount' => "SUM(SaleItem.tax_amount)",
						'total_taxable_amount' => "SUM(SaleItem.net_value)",
						'SaleItem_total' => "SUM((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount)",
						);
					$SaleItem=$this->SaleItem->find('first',[
						'conditions'=>$conditionsItem,
						'fields'=>array(
							'SaleItem.total_tax_amount',
							'SaleItem.total_net_value',
							'SaleItem.SaleItem_total',
							'SaleItem.total_taxable_amount'
							),
						]);
                     if($state_id==19)
					{
					$single['tax_amount']=$SaleItem['SaleItem']['total_tax_amount'];
					$single['cgst']=round(($SaleItem['SaleItem']['total_tax_amount']/2),3);
					$single['igst']=0;
					}
					else
					{
					$single['tax_amount']=$SaleItem['SaleItem']['total_tax_amount'];
					$single['cgst']=0;	
					$single['igst']=round(($SaleItem['SaleItem']['total_tax_amount']),3);	
					}
				    $single['vat']=$SaleItem['SaleItem']['total_tax_amount'];
					$single['net_value']=$SaleItem['SaleItem']['total_net_value'];
					$single['discount']=$value['Sale']['discount_amount'];
					$single['taxable_amount']=$SaleItem['SaleItem']['total_taxable_amount'];
					$single['total']=$value['Sale']['grand_total'];
					$single['net_value']=round($single['net_value'],2);
					$single['vat']=round($single['vat'],2);
					$single['discount']=round($single['discount'],2);
					$single['taxable_amount']=round($single['taxable_amount'],2);
					$single['total']=round($single['total'],2);
					$foot['net_value']+=$single['net_value'];
					$foot['vat']+=$single['vat'];
					$foot['discount']+=$single['discount'];
					$foot['total']+=$single['total'];
					$foot['taxable_amount']+=$single['taxable_amount'];
					$foot['cgst']+=$single['cgst'];
					$foot['igst']+=$single['igst'];
					$list['body'][]=$single;
			}
			$list['foot']=$foot;
		}
		echo json_encode($list);exit;
	}
	public function print_invoice_margin_report($from_date,$to_date) 
	{
		$this->response->download("invoice_margin.csv");
		$conditions=array();
		$from=date("Y-m-d", strtotime($from_date));
		$to=date("Y-m-d", strtotime($to_date));
		$list_array=array();
		$list_array=$this->invoice_margin_report($from,$to);
		$stock_array=array();
		foreach($list_array[0] as $key1 => $value1) { 
			foreach($value1 as $keysingle => $valuesingle) { 
				array_push($stock_array,$valuesingle);
			}
		}
		$this->set('Stock_array', $stock_array);
		$this->layout = 'ajax';
		return false;
		exit;
	}
	public function DueList()
	{
		$sub_group_id=3;
		$CustomerType_list=$this->CustomerType->find('list',array('fields'=>array('id','name')));
		$route_list=$this->Route->find('list',array('fields'=>array('id','name')));
		$customer_group=$this->CustomerGroup->find('list',array('fields'=>array('id','name')));
		$this->Customer->virtualFields = array('customer_name' => "CONCAT(AccountHead.name, ' ', Customer.code)");

		$Customer_list=$this->Customer->find('list',array(
			"joins"=>array(
				array(
					"table"=>'account_heads',
					"alias"=>'AccountHead',
					"type"=>'INNER',
					"conditions"=>array('Customer.account_head_id=AccountHead.id'),
					),
				),
			'conditions'=>array('acc_sub_group_id'=>2),
			'fields'=>array(
				'AccountHead.id','customer_name'
				)
			)
		);
		$State_list=$this->State->find('list',array('fields'=>array('id','name')));
		$this->set(compact('State_list'));
		$Customer=$this->Customer->find('all',array(
			'conditions'=>array('Customer.route_id'=>1),

			'order'=>array('AccountHead.name ASC'),

			));

		$this->set('CustomerType_list',$CustomerType_list);
		$this->set('Customer_list',$Customer_list);
		$this->set('Customer',$Customer);
//$this->set('route_list',$route_list);
		$route_list[0]='All';
		$this->set(compact('route_list'));
		$this->set('customer_group',$customer_group);
		$new_customer_array=array();
		$this->set('new_customer_array',$new_customer_array);
	}
	public function ExecutiveDueList(){
		$sub_group_id=3;
		$Executive_list=$this->Executive->find('list',array('fields'=>array('id','name')));
		$this->set('Executive_list',$Executive_list);
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0));
		$this->set('date1',$date1);
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 month",$time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('final',$final);
		$date2=date("Y-m-d", strtotime($date1));
		$final2=date("Y-m-d", strtotime($final));
	}
	public function executive_due_report_ajax()
	{
		$data=$this->request->data;
		$from_date=date('Y-m-d',strtotime($data['from_date']));
//$to_date=date('Y-m-d',strtotime($data['to_date']));
		$conditions=[];
		if($data['executive_id']){
			$conditions['ExecutiveRouteMapping.executive_id']=$data['executive_id'];
		}
//$conditions['Sale.account_head_id']=892;
		$data['row']='';
		$data['tfoot']='';
		$data['result']='Error';

		$Customer=$this->Customer->find('all',array(
			'joins'=>array(
// array(
// 	'table'=>'executives',
// 	'alias'=>'Executive',
// 	'type'=>'LEFT',
// 	'conditions'=>array('Executive.id=Sale.executive_id')
// ),
				array(
					"table"=>'executive_route_mappings',
					"alias"=>'ExecutiveRouteMapping',
					"type"=>'inner',
					"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
					),
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Executive.id=ExecutiveRouteMapping.executive_id')
					),
				),
			'conditions'=>$conditions,
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
				'Executive.id',
				'Executive.name',
				'Customer.route_id',
				'Executive.account_head_id',
				'ExecutiveRouteMapping.route_id',
				)
			));
//pr($Customer);exit;
		if($Customer)
		{
			$invoice_array=array();
			foreach ($Customer as $key => $value) {
				$account_id=$value['AccountHead']['id'];
				$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
				$Sales = $this->Sale->find('all', array(
					'conditions' => array(
						'Sale.account_head_id' =>$account_id,
//'Sale.executive_id'=>$value['Executive']['id'],
						'Sale.flag'=>1,
						'Sale.status'=>array(2,3),
						),
					'order' => array('Sale.date_of_delivered' => 'ASC'),
					'fields' => array(
						'Sale.id',
						'Sale.date_of_delivered',
						'Sale.invoice_no',
						'Sale.account_head_id',
						'Sale.executive_id',
						'Sale.grand_total',
						'AccountHead.name',
						'Executive.account_head_id',

						)
					)
				);
				$voucher_amount=0;

				$Journal_voucher=$this->Journal->find('list',array(
					'conditions'=>array(
						'Journal.credit'=>$account_id,
						'Journal.debit !='=>13,
//'NOT' => array(
//'Journal.remarks'=>'Sale Invoice No :'.$value3['Sale']['invoice_no'],
//),
						'Journal.flag=1',
						),
					'fields'=>array(
						'Journal.id',
						'Journal.amount',
						),
					));
				foreach ($Journal_voucher as $key3 => $value_amount) {
					$voucher_amount+=$value_amount;
				}

				$Journal_voucher1=$this->Journal->find('list',array(
					'conditions'=>array(
						'Journal.debit'=>$account_id,
						'Journal.credit'=>1,
						'Journal.flag=1',
						),
					'fields'=>array(
						'Journal.id',
						'Journal.amount',
						),
					));
				$voucher_balance=0;
				foreach ($Journal_voucher1 as $key3 => $value_amount1) {
					$voucher_amount-=$value_amount1;
				}

				foreach ($Sales as $keyj =>$valuej)
				{
					$SaleItem=$this->SaleItem->find('list',array(
						'conditions'=>array(
							'SaleItem.sale_id'=>$valuej['Sale']['id'],
							),
						'fields'=>array(
//'SaleItem.discount',
							)
						));
				}
				$invoice_array_single=array();
				if (!empty($Sales)) {


					$invoice_array_single['party_name'] = $value['AccountHead']['name'];
					$invoice_array_single['Executive_name'] = $value['Executive']['name'];
					$invoice_array_single['invoice_no'] = array();
					$invoice_array_single['balance'] = array();
					$invoice_array_single['delivered_date'] = array();
					$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
					if($AccountHead)
					{
						$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
						$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
					}
					if($outstanding_amount>$voucher_amount)
					{
						$outstanding_amount-=$voucher_amount;
						$voucher_amount=0;
					}
					else
					{
						$voucher_amount-=$outstanding_amount;
						$outstanding_amount=0;
					}
//pr($voucher_amount);
					if($outstanding_amount>=1)
						{$from12=date("Y-m-d", strtotime($data['from_date']));
					$outstanding_amount_date1=date("Y-m-d", strtotime($outstanding_amount_date));
					if($outstanding_amount_date1>=$from12){
						array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['invoice_no'], 'Opening Balance');
						array_push($invoice_array_single['balance'],$outstanding_amount);
					}
				}
				foreach ($Sales as $keySC2 => $valueSC2) {
					$Journal=$this->Journal->find('all',array(
						'conditions'=>array(
							'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
							'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
							'Journal.debit=1',
							'Journal.flag=1',
							),
						));

					$balance_amount=$valueSC2['Sale']['grand_total'];
					$paid_amount=0;
// foreach ($Journal as $key0 => $amount) {
// 	$paid_amount+=$amount['Journal']['amount'];
// 	$balance_amount-=$amount['Journal']['amount'];
// }
					if($voucher_amount)
					{
						if($balance_amount<=$voucher_amount)
						{
							$voucher_balance=$balance_amount;
							$balance_amount=0;
							$voucher_amount-=$voucher_balance;
						}
						else
						{
							$balance_amount-=$voucher_amount;
							$voucher_amount=0;
						}
					}
					if($valueSC2['Sale']['grand_total']>0)
					{
						if($balance_amount){

// if($valueSC2['Sale']['executive_id']==$data['executive_id'])
// {
							$check_date=$valueSC2['Sale']['date_of_delivered'];

							$from11=date("Y-m-d", strtotime($data['from_date']));
							$to11=date('Y-m-d');
							if($check_date>=$from11){
								array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Sale']['date_of_delivered'])));
								array_push($invoice_array_single['invoice_no'], $valueSC2['Sale']['invoice_no']);
								array_push($invoice_array_single['balance'],$balance_amount) ;  
							}
//}
						}
					}
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      

			}
			else
			{


				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
				$invoice_array_single['Executive_name'] = $value['Executive']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{
					$from12=date("Y-m-d", strtotime($data['from_date']));
					$outstanding_amount_date1=date("Y-m-d", strtotime($outstanding_amount_date));
					if($outstanding_amount_date1>=$from12){
						array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['invoice_no'], 'Opening Balance');
						array_push($invoice_array_single['balance'],$outstanding_amount);
					}
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      
			}

		}
		$data['result']='Success';
		$data['data']=$invoice_array;
	}
//pr($invoice_array);
	echo json_encode($data);
	exit;

}
public function get_oustanding_amount($account_head_id,$opening_balance){
//pr($account_head_id."--".$opening_balance);
	$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$account_head_id,
					'Journal.credit'=>$account_head_id,
					),
				'AND' => array(
					'Journal.flag=1',
					)
				)
			),
		));
	$Journal_credit=$this->Journal->find('all',array(
		'conditions'=>array(
			'Journal.credit'=>$account_head_id,
			'Journal.flag=1',
			)
		));

	$category=[
	'Asset'=>'debit_account',
	'Expense'=>'debit_account',
	'Capital'=>[
	'Capital'=>'credit_account',
	'Drawing'=>'debit_account',
	],
	'Income'=>'credit_account',
	'Liabilities'=>'credit_account',
	];
	$AccountingsController = new AccountingsController;
	$type=$AccountingsController->get_type_by_account_dead($account_head_id);
	$Journal_all=[];
	foreach ($Journal_credit as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['mode']=$value['AccountHeadDebit']['name'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['credit']=$value['Journal']['amount'];
		$Journal_single['debit']=0.00;
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	$Journal_debit=$this->Journal->find('all',array(
		'conditions'=>array(
			'Journal.debit'=>$account_head_id,
			'Journal.flag=1',
			)
		));
	foreach ($Journal_debit as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['mode']=$value['AccountHeadCredit']['name'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['credit']=0.00;
		$Journal_single['debit']=$value['Journal']['amount'];
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	$return['row']['tbody']='';
	$return['row']['tfoot']='';
	$category_name='';
	$Balance_Total=0;
	$credit_Total=0;
	$debit_Total=0;
	if(!empty($type)){
		$Type_name=$type['Type']['name'];
		$category_name=$category[$type['Type']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['Type']['name']][$type['Group']['name']];
		}
	}
	if($category_name=='credit_account')
	{
		$credit=$opening_balance;
		$debit='0';
	}
	else
	{
		$debit=$opening_balance;
		$credit='0';
	}
	$Journal_all[0]=array(
		'id'=>0,
		'mode'=>'Opening Balance',
		'remarks'=>'',
		'credit'=>$credit,
		'debit'=>$debit,
		);
	foreach ($Journal_all as $key => $value) 
	{
		$credit_Total+=$value['credit'];
		$debit_Total+=$value['debit'];
	}

	$Balance_Total=$debit_Total-$credit_Total;	
	return $Balance_Total;
}
public function due_report_ajax()
{
	$conditions=array();
	if(!empty($this->request->data['customer_type']))
	{
		$conditions['Customer.customer_type_id']=$this->request->data['customer_type'];
	}
	if(!empty($this->request->data['route_id']))
	{
		$conditions['Customer.route_id']=$this->request->data['route_id'];
	}
	if(!empty($this->request->data['group_id']))
	{
		$conditions['Customer.customer_group_id']=$this->request->data['group_id'];
	}
	if(!empty($this->request->data['name']))
	{
		$AccountHead_id=$this->AccountHead->find('first',array('conditions'=>array('AccountHead.name'=>$this->request->data['name'])));
		$Balance_Total=0;
		if(!empty($AccountHead_id)){
			$conditions['Customer.account_head_id']=$AccountHead_id['AccountHead']['id'];   
		}
	}
	$Customer=$this->Customer->find('all',array('conditions'=>$conditions));
	$data['row']='';
	$data['tfoot']='';
	$grand_total=0;
	$q=0;$w=0;$p=0;$y=0;$cheque_total=0;
	if($Customer)
	{
		foreach ($Customer as $key => $value) {
        

			$invoice_array=array();

			$account_id=$value['AccountHead']['id'];
			$cheque=$this->Cheque->find('all',array(
		'conditions'=>array(
					'Cheque.account_head_id'=>$account_id,
					'Cheque.status'=>array(0,4),
			),
		'order'=>array('Cheque.id ASC'),
		'fields'=>array(
			'Cheque.cheque_amount',
			
			),
		));
			$cheque_amount=0;
			foreach ($cheque as $key => $cheque) {
                $cheque_amount+=$cheque['Cheque']['cheque_amount'];
			}
			$Sales = $this->Sale->find('all', array(
				'conditions' => array(
					'Sale.account_head_id' =>$account_id,
//'Sale.executive_id'=>$value['Executive']['id'],
					'Sale.flag'=>1,
					'Sale.status'=>array(2,3),
					),
				'order' => array('Sale.date_of_delivered' => 'ASC'),
				'fields' => array(
					'Sale.id',
					'Sale.date_of_delivered',
					'Sale.invoice_no',
					'Sale.account_head_id',
					'Sale.executive_id',
					'Sale.grand_total',
					'AccountHead.name',
					'Executive.account_head_id',

					)
				)
			);
			$voucher_amount=0;

			$Journal_voucher=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.credit'=>$account_id,
					'Journal.debit !='=>13,
//'NOT' => array(
//'Journal.remarks'=>'Sale Invoice No :'.$value3['Sale']['invoice_no'],
//),
					'Journal.flag=1',
					),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
					),
				));

			foreach ($Journal_voucher as $key3 => $value_amount) {
				$voucher_amount+=$value_amount;
			}

			$Journal_voucher1=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.debit'=>$account_id,
					'Journal.credit'=>1,
					'Journal.flag=1',
					),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
					),
				));
			$voucher_balance=0;
			foreach ($Journal_voucher1 as $key3 => $value_amount1) {
				$voucher_amount-=$value_amount1;
			}

			foreach ($Sales as $keyj =>$valuej)
			{
				$SaleItem=$this->SaleItem->find('list',array(
					'conditions'=>array(
						'SaleItem.sale_id'=>$valuej['Sale']['id'],
						),
					'fields'=>array(
//'SaleItem.discount',
						)
					));
			}
			$invoice_array_single=array();

			if (!empty($Sales)) {


				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
//$invoice_array_single['Executive_name'] = $value['Executive']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
//pr($voucher_amount);
				if($outstanding_amount>=1)
				{
					array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
					array_push($invoice_array_single['invoice_no'], 'Opening Balance');
					array_push($invoice_array_single['balance'],$outstanding_amount);

				}
				foreach ($Sales as $keySC2 => $valueSC2) {
					$Journal=$this->Journal->find('all',array(
						'conditions'=>array(
							'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
							'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
							'Journal.debit=1',
							'Journal.flag=1',
							),
						));
					$balance_amount=$valueSC2['Sale']['grand_total'];
					$paid_amount=0;
// foreach ($Journal as $key0 => $amount) {
// 	$paid_amount+=$amount['Journal']['amount'];
// 	$balance_amount-=$amount['Journal']['amount'];
// }
					if($voucher_amount)
					{
						if($balance_amount<=$voucher_amount)
						{
							$voucher_balance=$balance_amount;
							$balance_amount=0;
							$voucher_amount-=$voucher_balance;
						}
						else
						{
							$balance_amount-=$voucher_amount;
							$voucher_amount=0;
						}
					}
					if($valueSC2['Sale']['grand_total']>0)
					{
						if($balance_amount){
//if($valueSC2['Sale']['account_head_id']==$data['customer_id'])
//{
							$check_date=$valueSC2['Sale']['date_of_delivered'];


							array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Sale']['date_of_delivered'])));
							array_push($invoice_array_single['invoice_no'], $valueSC2['Sale']['invoice_no']);
							array_push($invoice_array_single['balance'],$balance_amount) ;  

//}
						}
					}
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      

			}
			else
			{


				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
//$invoice_array_single['Executive_name'] = $value['Executive']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{

					array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
					array_push($invoice_array_single['invoice_no'], 'Opening Balance');
					array_push($invoice_array_single['balance'],$outstanding_amount);

				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      
			}
//pr($invoice_array);
//if($invoice_array){

			$b11='';$b51='';$b12='';$b41='';

			foreach ($invoice_array as $key => $value4)
			{

				$inner_array_count=count($value4['invoice_no']);
				$a1=array();
				$b1=array();
				$c1=array();
				$f1=array();
				$g=array();
				$m=array();
				foreach ($value4['delivered_date'] as $key=>$value1)
				{
					$delivered_date=$value1;
					$now = time();
					$expected_days_diff=$now-strtotime($delivered_date);
					$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
					array_push($m,$diff_day);
				}
				foreach($value4['invoice_no'] as $key=>$value2)
				{
					$invoice_no=$value2;

				}
				$new_balance=0;

				foreach ($value4['balance'] as $key=>$value3)
				{
					$balance=$value3;
					array_push($g,$balance);
					$new_balance+=$balance;
				}
				for($i=0;$i<count($m);$i++)
				{

					if($m[$i]<=30){ $a=$g[$i];array_push($a1,$a);}
					if($m[$i]<=60 && $m[$i]>30){$b=$g[$i];array_push($b1,$b);}
					if($m[$i]<=90 && $m[$i]>60){$c=$g[$i];array_push($c1,$c);}
					if($m[$i]>90){$f=$g[$i];array_push($f1,$f);}

				}

//$grand_total+=$new_balance;

				$b=array_sum($a1);if($b!=0){$b11=ROUND($b,3);}else{$b11='';}
				$b5=array_sum($b1);if($b5!=0){$b51=ROUND($b5,3);}else{$b51='';}
				$b19=array_sum($c1);if($b19!=0){$b12=ROUND($b19,3);}else{$b12='';}
				$b4=array_sum($f1);if($b4!=0){$b41=ROUND($b4,3);}else{$b41='';}

				$q+=$b11;$w+=$b51;$p+=$b12;$y+=$b41;
				$grand_total=$q+$w+$p+$y;

			}

			$Balance_Total=$this->get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance']);

			$Balance_Total = number_format($Balance_Total,3);
//$grand_total+=$Balance_Total;
			if($this->request->data['check_id'] !=0)
			{
				if($Balance_Total !=0)
				{
//pr($value['AccountHead']['name']);
                         $cheque_total+=$cheque_amount;
					$data['row']= $data['row'].'<tr>';
					$data['row']= $data['row'].'<td>'.$value["Customer"]["code"].'</td>';
					$data['row']= $data['row'].'<td><span style="display:none" class="AccountHead_id">'.$value['AccountHead']['id'].'</span><span>'.$value['AccountHead']['name'].'</span></td>';
					$data['row']= $data['row'].'<td>'.$value["Route"]["name"].'</td>';
					$data['row']= $data['row'].'<td>'.$value["CustomerGroup"]["name"].'</td>';
					$data['row']= $data['row'].'<td>'.$b11.'</td>';
					$data['row']= $data['row'].'<td>'.$b51.'</td>';
					$data['row']= $data['row'].'<td>'.$b12.'</td>';
					$data['row']= $data['row'].'<td>'.$b41.'</td>';
				    $data['row']= $data['row'].'<td>'.$cheque_amount.'</td>';
					$data['row']= $data['row'].'<td>'.$Balance_Total.'</td>';
					$data['row']= $data['row'].'</tr>';
				}
			}
			else{
                $cheque_total+=$cheque_amount;
				$data['row']= $data['row'].'<tr>';
				$data['row']= $data['row'].'<td>'.$value["Customer"]["code"].'</td>';
				$data['row']= $data['row'].'<td><span style="display:none" class="AccountHead_id">'.$value['AccountHead']['id'].'</span><span>'.$value['AccountHead']['name'].'</span></td>';
				$data['row']= $data['row'].'<td>'.$value["Route"]["name"].'</td>';
				$data['row']= $data['row'].'<td>'.$value["CustomerGroup"]["name"].'</td>';
				$data['row']= $data['row'].'<td>'.$b11.'</td>';
				$data['row']= $data['row'].'<td>'.$b51.'</td>';
				$data['row']= $data['row'].'<td>'.$b12.'</td>';
				$data['row']= $data['row'].'<td>'.$b41.'</td>';
		        $data['row']= $data['row'].'<td>'.$cheque_amount.'</td>';
				$data['row']= $data['row'].'<td>'.$Balance_Total.'</td>';
				$data['row']= $data['row'].'</tr>';

			}


           
//$data['result']='Success';
		}
		$data['tfoot']= $data['tfoot'].'<tr>';
		$data['tfoot']= $data['tfoot'].'<td colspan="3"></td>';

		$data['tfoot']= $data['tfoot'].'<td><h3>Total</h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$q.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$w.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$p.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$y.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$cheque_total.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$grand_total.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'</tr>';
		$data['result']='Success';
	}
	else
	{
		$data['result']='Error';
	}
	echo json_encode($data);
	exit;
}
public function DueListPrint()
{
	$data=$this->request->data;
	$conditions=array();
	if(!empty($data['Report']['customer_type'])){
		$conditions['Customer.customer_type_id']=$data['Report']['customer_type'];
	}
	if(!empty($data['Report']['route_id'])){
		$conditions['Route.id']=$data['Report']['route_id'];
	}
	if(!empty($data['Report']['name']))
	{
		$conditions['AccountHead.id']=$data['Report']['name'];
	}
	$Customer=$this->Customer->find('all',array('conditions'=>$conditions,
		'order'=>array('AccountHead.name ASC'),
		'fields'=>array(
			'Customer.account_head_id',
			'CustomerType.name',
			'Customer.code',
			'Route.name',
			'AccountHead.name',
			'AccountHead.opening_balance',
			)));
	$final_array=array();
	$header=array(
		'Code',
		'Name',
		'Outstanding'
		);
	$width=array(
		'50',
		'115',
		'100'
		);
	foreach ($Customer as $key => $value) {
		$Balance_Total=$this->get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance']);
		$Balance_Total = number_format($Balance_Total,2);
		if($Balance_Total){
			$final_array[$key][]=$value['Customer']['code'];
			$final_array[$key][]=$value['AccountHead']['name'];
			$final_array[$key][]=$Balance_Total;
		}
	}
	require('fpdf/fpdf.php');
	$pdf = new FPDF('l');
	$pdf->addPage();
	$pdf->SetFont('Arial','',16);
	$pdf->text(130,20,'CUSTOMER DUE  LIST');
	$pdf->SetFont('Arial','',14);
	$pdf -> SetY(35); 
	for($i=0;$i<count($header);$i++)
		$pdf->Cell($width[$i],7,$header[$i],1,0,'C');
	$pdf->Ln();
	$pdf->SetFillColor(224,235,255);
	$fill = false;
	$pdf->SetFont('Arial','','10');
	foreach($final_array as $row)
	{
		$i=0;
		foreach($row as $col){
			$pdf->Cell($width[$i],6,$col,'LR',0,'L',$fill);
			$i++;
		}
		$pdf->Ln();
		$fill = !$fill;
	}
	$pdf->Cell(array_sum($width),0,'','T');
	$pdf->Output();
	exit;
}
public function account_recievable_print($customer_type,$customer)
{
	$conditions=array();
	if($customer_type!=0){
		$CustomerType=$this->CustomerType->findById($customer_type);
		$CustomerType_name=$CustomerType['CustomerType']['name'];
		$conditions['Customer.customer_type_id']=$customer_type;
	}
	else{
		$CustomerType_name='All';
	}
	if($customer!=0){
		$conditions['Customer.account_head_id']=$customer;
	}
	$Customers=$this->Customer->find('all',array(
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			),
		'order' => array('Customer.id' => 'ASC'),
		'conditions'=>$conditions,
		)
	);
	$invoice_array=array();
	foreach ($Customers as $key => $value)
	{
		$account_id=$value['AccountHead']['id'];
		$Sales = $this->Sale->find('all', array(
			'conditions' => array(
				'Sale.account_head_id' =>$account_id,
				'Sale.flag'=>1,
				'Sale.status'=>array(2,3),
				),
			'order' => array('Sale.date_of_delivered' => 'ASC'),
			'fields' => array(
				'Sale.id',
				'Sale.date_of_delivered',
				'Sale.invoice_no',
				'Sale.account_head_id',
				'Sale.executive_id',
				'Sale.grand_total',
				'AccountHead.name',
				'Executive.account_head_id',

				)
			)
		);

		$voucher_amount=0;
		$discount_paid_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DISCOUNT PAID'));
		$Journal_voucher=$this->Journal->find('list',array(
			'conditions'=>array(
				'Journal.credit'=>$account_id,
				//'Journal.debit'=>$value['Executive']['account_head_id'],
				'NOT' => array(
					'Journal.debit'=>array($discount_paid_account_head_id),
					'Journal.remarks LIKE'=>'%Sale Invoice No :%',
				),
				'Journal.flag=1',
				),
			'fields'=>array(
				'Journal.id',
				'Journal.amount',
				),
			));
		foreach ($Journal_voucher as $key3 => $value_amount) {
			$voucher_amount+=$value_amount;
		}
		$Journal_extra_debited=$this->Journal->find('list',array(
		'conditions'=>array(
			'Journal.debit'=>$account_id,
			'Journal.work_flow'=>array('Cheques'),
			'Journal.flag=1',
			),
		'fields'=>array(
			'Journal.id',
			'Journal.amount',
			),
		));
		$other_debited_amount=0;
		foreach ($Journal_extra_debited as $key3 => $value_amount) {
			$other_debited_amount+=$value_amount;
		}
		$total_debited_amount=0;
		$total_credited_amount=0;
		$total_credited_amount+=$voucher_amount;
		//$total_debited_amount+=$other_debited_amount;
		$voucher_balance=0;
		foreach ($Sales as $keyj =>$valuej)
		{
			$SaleItem=$this->SaleItem->find('list',array(
				'conditions'=>array(
					'SaleItem.sale_id'=>$valuej['Sale']['id'],
					),
				'fields'=>array(
					)
				));
		}
		$invoice_array_single=array();
		if (!empty($Sales)) {


			$invoice_array_single['party_name'] = $value['AccountHead']['name'];
			$invoice_array_single['invoice_no'] = array();
			$invoice_array_single['balance'] = array();
			$invoice_array_single['delivered_date'] = array();
			$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
			$Journal_opening_received=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.remarks'=>'Sale Invoice No :0',
				'Journal.credit'=>$value['AccountHead']['id'],
				// 'NOT' => array(
				// 	'Journal.work_flow'=>'From Mobile',
				// ),
				// 'Journal.debit=1',
				'Journal.flag=1',
				),
			));

			if($AccountHead)
			{
				$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
				$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
			}
			$opening_paid_amount=0;
			foreach ($Journal_opening_received as $key_each_sale => $value_each_sale) {
				$opening_paid_amount+=$value_each_sale['Journal']['amount'];
			}
			$outstanding_amount+=$other_debited_amount;
			$total_debited_amount+=$outstanding_amount;
			$total_credited_amount+=$opening_paid_amount;
			$outstanding_amount-=$opening_paid_amount;
			if($outstanding_amount>$voucher_amount)
			{
				$outstanding_amount-=$voucher_amount;
				$voucher_amount=0;
			}
			else
			{
				$voucher_amount-=$outstanding_amount;
				$outstanding_amount=0;
			}
			if($outstanding_amount>0)
			{
				array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
				array_push($invoice_array_single['invoice_no'], 'Opening Balance');
				array_push($invoice_array_single['balance'],$outstanding_amount);

			}

			foreach ($Sales as $keySC2 => $valueSC2) {
				$Journal=$this->Journal->find('all',array(
					'conditions'=>array(
						'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
						'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
						// 'NOT' => array(
						// 	'Journal.work_flow'=>'From Mobile',
						// ),
						'AccountHeadDebit.sub_group_id=1',
						'Journal.flag=1',
						),
					));
				$balance_amount=$valueSC2['Sale']['grand_total'];
				$paid_amount=0;
				$app_paid_amount=0;
				foreach ($Journal as $key_each_sale => $value_each_sale) {
					$paid_amount+=$value_each_sale['Journal']['amount'];
				}
				$total_paid_in_amount=$paid_amount+$app_paid_amount;
				$balance_amount-=$paid_amount+$app_paid_amount;
				$total_debited_amount+=$valueSC2['Sale']['grand_total'];
				$total_credited_amount+=$paid_amount+$app_paid_amount;
				
				if($voucher_amount)
				{
					if($balance_amount<=$voucher_amount)
					{
						$voucher_balance=$balance_amount;
						$balance_amount=0;
						$voucher_amount-=$voucher_balance;
					}
					else
					{
						$balance_amount-=$voucher_amount;
						$voucher_amount=0;
					}
				}
				if($valueSC2['Sale']['grand_total']>0)
				{
					if($balance_amount>0){
						if($valueSC2['Sale']['account_head_id']==$customer)
						{
							$check_date=$valueSC2['Sale']['date_of_delivered'];


							array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Sale']['date_of_delivered'])));
							array_push($invoice_array_single['invoice_no'], $valueSC2['Sale']['invoice_no']);
							array_push($invoice_array_single['balance'],$balance_amount) ;  

						}
					}
				}
			}
			// pr($total_debited_amount);
			// pr($total_credited_amount);
			// pr($account_id);
			// exit;
			if(count($invoice_array_single['invoice_no']))
			{
				array_push($invoice_array, $invoice_array_single);  
			}      

		}
		else
		{


			$invoice_array_single['party_name'] = $value['AccountHead']['name'];
			$invoice_array_single['invoice_no'] = array();
			$invoice_array_single['balance'] = array();
			$invoice_array_single['delivered_date'] = array();
			$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
			if($AccountHead)
			{
				$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
				$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
			}
			$Journal_opening_received=$this->Journal->find('all',array(
			'conditions'=>array(
				'Journal.remarks'=>'Sale Invoice No :0',
				'Journal.credit'=>$value['AccountHead']['id'],
				'Journal.flag=1',
				),
			));
			$opening_paid_amount=0;
			foreach ($Journal_opening_received as $key_each_sale => $value_each_sale) {
				$opening_paid_amount+=$value_each_sale['Journal']['amount'];
			}
			$outstanding_amount+=$other_debited_amount;
			$outstanding_amount-=$opening_paid_amount;
			if($outstanding_amount>$voucher_amount)
			{
				$outstanding_amount-=$voucher_amount;
				$voucher_amount=0;
			}
			else
			{
				$voucher_amount-=$outstanding_amount;
				$outstanding_amount=0;
			}
			if($outstanding_amount>=1)
			{

				array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
				array_push($invoice_array_single['invoice_no'], 'Opening Balance');
				array_push($invoice_array_single['balance'],$outstanding_amount);

			}
			if(count($invoice_array_single['invoice_no']))
			{
				array_push($invoice_array, $invoice_array_single);  
			}      
		}

	}
	require('fpdf/fpdf.php');   
	$pdf = new FPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	function header_section($pdf,$CustomerType_name,$Profile){
		$pdf->SetFont('Arial','B',11);
		$pdf->AddPage();
		$pdf->Text(10, 7,$Profile['company_name']);
		$pdf->SetFont('Arial','',9);
		$pdf->Text(10, 12,$Profile['address_line_1']);
		$pdf->Text(10, 17,$Profile['address_line_2']);
		$pdf->Text(10, 22,$Profile['mobile']);
		$pdf->SetFont('Arial','B',11);
		$pdf->Text(85,25,"INVOICE WISE DUE LIST");
		$pdf->rect(5, 30, 200, 260);
		$first_table_x=25;
		$pdf->SetFont('Arial','B',10);
		$pdf->SetFont('Arial','B',11);
		$i=0;
	}
	function footer($pdf,$grand_total,$Line_y)
	{
		$pdf->SetFont('Arial','B',10);
		$pdf->Text(45, $Line_y+5, "Grand Total   : ");
		$pdf->Text(150, $Line_y+5, " ".$grand_total." ");
	}
	header_section($pdf,$CustomerType_name,$Profile);
	$i=0;
	$first_table_x=30;
	$grand_total=0;
	$Line_y=$first_table_x;
	foreach($invoice_array as $key=>$value) {
		$m=1;
		$l=1;
		$n=1;
		$pdf->SetFont('Arial','B',11);
		$product_name_position=65;
		$party_name=$value['party_name'];
		$inner_array_count=count($value['invoice_no']);
		if(($Line_y+($inner_array_count*5)+15)>290)
		{
			$Line_y=$first_table_x;
			header_section($pdf,$CustomerType_name,$Profile);
		}
		$party_name_y_axis=$first_table_x+($i*15)+($inner_array_count-1);
		$pdf->Text($product_name_position, $Line_y+5, $party_name); 
		$pdf->SetFont('Arial','',10);
		$pdf->SetFont('Arial','',9);
		$invoice_no=array_values($value['invoice_no']);
		foreach ($value['delivered_date'] as $key=>$value1)
		{
			$delivered_date=$value1;
			$pdf->Text(10, $Line_y+5+($m*5), date("d-m-Y", strtotime($delivered_date)));
			$now = time();
			$expected_days_diff=$now-strtotime($delivered_date);
			$expected_days=floor($expected_days_diff / (60 * 60 * 24))+1;
			$pdf->Text(30, $Line_y+5+($m*5), '('.$expected_days.' Days)');
			$m++;
		}
		foreach ($value['invoice_no'] as $key=>$value2)
		{
			$invoice_no=$value2;
			$pdf->Text(80,$Line_y+5+($l*5), "Sales Bill No:".$invoice_no);
			$l++;
		}
		$new_balance=0;
		foreach ($value['balance'] as $key=>$value3)
		{
			$balance=$value3;
			$pdf->Text(150,$Line_y+5+($n*5), $balance);
			$n++;
			$new_balance+=$balance;
		}
		$pdf->Text(80,$Line_y+($inner_array_count*5)+10, "Total Amount:");
		$pdf->Line(5,$Line_y+($inner_array_count*5)+10+5,205,$Line_y+($inner_array_count*5)+10+5);
		$pdf->Text(150,$Line_y+($inner_array_count*5)+10, $new_balance);
		$Line_y=$Line_y+($inner_array_count*5)+15;
		$i++;
		$grand_total+=$new_balance;
	}
	footer($pdf,$grand_total,$Line_y);
	$pdf->Output();
	exit;
}
public function PartyDueList()
{
	$Party_list=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('AccountHead.acc_sub_group_id'=>1)));
	$Party=$this->Party->find('all',array(
		'order'=>array('AccountHead.name ASC'),
		));
	$this->set('Party_list',$Party_list);
	$this->set('Party',$Party);
	$new_Party_array=array();
	foreach ($Party as $key => $value) {
		$result['Party']=$value;
		$result['Balance_Total']=$this->get_oustanding_amount($value['Party']['account_head_id'],$value['AccountHead']['opening_balance']);
		$result['Balance_Total']=-($result['Balance_Total']);
		array_push($new_Party_array, $result);
	}
	$this->set('new_Party_array',$new_Party_array);
}
public function party_due_report_ajax()
{
	$conditions=array();
	if(!empty($this->request->data['name']))
	{
		$AccountHead_id=$this->AccountHead->find('first',array('conditions'=>array('AccountHead.name'=>$this->request->data['name'])));
		$Balance_Total=0;
		if(!empty($AccountHead_id)){
			$conditions['Party.account_head_id']=$AccountHead_id['AccountHead']['id'];   
		}
	}
	$Party=$this->Party->find('all',array('conditions'=>$conditions));
	$data['row']='';
	if($Party)
	{
		foreach ($Party as $key => $value) {
			$Balance_Total=$this->get_oustanding_amount($value['Party']['account_head_id'],$value['AccountHead']['opening_balance']);
			$Balance_Total= -($Balance_Total);
			$data['row']= $data['row'].'<tr>';
			$data['row']= $data['row'].'<td><span style="display:none" class="AccountHead_id">'.$value['AccountHead']['id'].'</span>
			<span>'.$value['AccountHead']['name'].'</span></td>';
			$data['row']= $data['row'].'<td>'.substr($value['Party']['place'],0,80).'</td>';
			$data['row']= $data['row'].'<td style="text-align:right">'.$Balance_Total.'</td>';
			$data['row']= $data['row'].'</tr>';
		}
		$data['result']='Success';
	}
	else
	{
		$data['result']='Error';
	}
	echo json_encode($data);
	exit;
}
public function purchase_invoice_wise_print($party)
{
	$conditions=array();
	if($party!=0){
		$conditions['Party.account_head_id']=$party;
	}
	$Parties=$this->Party->find('all',array(
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			),
		'order' => array('Party.id' => 'ASC'),
		'conditions'=>$conditions,
		)
	);
	$invoice_array=array();
	foreach ($Parties as $key => $value)
	{
		$Purchases = $this->Purchase->find('all', array(
			'conditions' => array(
				'Purchase.account_head_id' =>$value['AccountHead']['id'],
				'Purchase.flag'=>1,
				'Purchase.status'=>array(2,3),
				),
			'fields' => array(
				'Purchase.*',
				'AccountHead.name',
				)
			)
		);
		$Journal_voucher=$this->Journal->find('list',array(
			'conditions'=>array(
				'Journal.debit'=>$value['AccountHead']['id'],
				'NOT' => array(
					'Journal.remarks LIKE' => 'Purchase Invoice No :%',
					),
				'Journal.flag=1',
				),
			'fields'=>array(
				'Journal.id',
				'Journal.amount',
				),
			));
		$voucher_amount=0;
		$voucher_balance=0;
		foreach ($Journal_voucher as $key3 => $value_amount) {
			$voucher_amount+=$value_amount;
		}
		foreach ($Purchases as $keyj =>$valuej)
		{
			$PurchasedItem=$this->PurchasedItem->find('list',array(
				'conditions'=>array(
					'PurchasedItem.purchase_id'=>$valuej['Purchase']['id'],
					),
				'fields'=>array(
					'PurchasedItem.discount',
					)
				));
		}
		$invoice_array_single=array();
		if (!empty($Purchases)) {
			$invoice_array_single['party_name'] = $value['AccountHead']['name'];
			$invoice_array_single['invoice_no'] = array();
			$invoice_array_single['balance'] = array();
			$invoice_array_single['delivered_date'] = array();
			$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
			if($AccountHead)
			{
				$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
				$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
			}
			if($outstanding_amount>$voucher_amount)
			{
				$outstanding_amount-=$voucher_amount;
				$voucher_amount=0;
			}
			else
			{
				$voucher_amount-=$outstanding_amount;
				$outstanding_amount=0;
			}
			if($outstanding_amount>=1)
			{
				array_push($invoice_array_single['delivered_date'],$outstanding_amount_date);
				array_push($invoice_array_single['invoice_no'], 'Opening Balance');
				array_push($invoice_array_single['balance'],$outstanding_amount);
			}
			foreach ($Purchases as $keySC2 => $valueSC2) {
// $Journal=$this->Journal->find('all',array(
// 	'conditions'=>array(
// 		'Journal.remarks'=>'Purchase Invoice No :'.$valueSC2['Purchase']['invoice_no'],
// 		'Journal.debit'=>$valueSC2['Purchase']['account_head_id'],
// 		'Journal.credit=1',
// 		'Journal.flag=1',
// 		),
// 	));
				$balance_amount=$valueSC2['Purchase']['grand_total'];
//$paid_amount=0;
// foreach ($Journal as $key0 => $amount) {
// 	$paid_amount+=$amount['Journal']['amount'];
// 	$balance_amount-=$amount['Journal']['amount'];
// }
				if($voucher_amount)
				{
					if($balance_amount<$voucher_amount)
					{
						$voucher_balance=$balance_amount;
						$balance_amount=0;
						$voucher_amount-=$voucher_balance;
					}
					else
					{
						$balance_amount-=$voucher_amount;
						$voucher_amount=0;
					}
				}
				if($valueSC2['Purchase']['grand_total']>0)
				{
					if($balance_amount){
						array_push($invoice_array_single['delivered_date'], $valueSC2['Purchase']['date_of_delivered']);
						array_push($invoice_array_single['invoice_no'], $valueSC2['Purchase']['invoice_no']);
						array_push($invoice_array_single['balance'],$balance_amount) ;  
					}
				}
			}
			if(count($invoice_array_single['invoice_no']))
			{
				array_push($invoice_array, $invoice_array_single);  
			}      
		}
		else
		{
			$invoice_array_single['party_name'] = $value['AccountHead']['name'];
			$invoice_array_single['invoice_no'] = array();
			$invoice_array_single['balance'] = array();
			$invoice_array_single['delivered_date'] = array();
			$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
			if($AccountHead)
			{
				$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
				$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
			}
			if($outstanding_amount>$voucher_amount)
			{
				$outstanding_amount-=$voucher_amount;
				$voucher_amount=0;
			}
			else
			{
				$voucher_amount-=$outstanding_amount;
				$outstanding_amount=0;
			}
			if($outstanding_amount>=1)
			{
				array_push($invoice_array_single['delivered_date'],$outstanding_amount_date);
				array_push($invoice_array_single['invoice_no'], 'Opening Balance');
				array_push($invoice_array_single['balance'],$outstanding_amount);
			}
			if(count($invoice_array_single['invoice_no']))
			{
				array_push($invoice_array, $invoice_array_single);  
			}      
		}
	}
	require('fpdf/fpdf.php');   
	$pdf = new FPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	function header_section($pdf,$Profile){
		$pdf->SetFont('Arial','B',11);
		$pdf->AddPage();
		$pdf->Text(10, 7,$Profile['company_name']);
		$pdf->SetFont('Arial','',9);
		$pdf->Text(10, 12,$Profile['address_line_1']);
		$pdf->Text(10, 17,$Profile['address_line_2']);
		$pdf->Text(10, 22,$Profile['mobile']);
		$pdf->SetFont('Arial','B',11);
		$pdf->Text(85,22,"INVOICE WISE DUE LIST");
		$pdf->rect(5, 30, 200, 260);
		$first_table_x=25;
		$pdf->SetFont('Arial','B',10);
		$pdf->SetFont('Arial','B',11);
		$i=0;
	}
	function footer($pdf,$grand_total,$Line_y)
	{
		$pdf->SetFont('Arial','B',10);
		$pdf->Text(45, $Line_y+5, "Grand Total   : ");
		$pdf->Text(150, $Line_y+5, " ".$grand_total." ");
	}
	header_section($pdf,$Profile);
	$i=0;
	$first_table_x=30;
	$grand_total=0;
	$Line_y=$first_table_x;
	foreach($invoice_array as $key=>$value) {
		$m=1;
		$l=1;
		$n=1;
		$pdf->SetFont('Arial','B',11);
		$product_name_position=65;
		$party_name=$value['party_name'];
		$inner_array_count=count($value['invoice_no']);
		if(($Line_y+($inner_array_count*5)+15)>290)
		{
			$Line_y=$first_table_x;
			header_section($pdf,$Profile);
		}
		$party_name_y_axis=$first_table_x+($i*15)+($inner_array_count-1);
		$pdf->Text($product_name_position, $Line_y+5, $party_name); 
		$pdf->SetFont('Arial','',10);
		$pdf->SetFont('Arial','',9);
		$invoice_no=array_values($value['invoice_no']);
		foreach ($value['delivered_date'] as $key=>$value1)
		{
			$delivered_date=$value1;
			$pdf->Text(10, $Line_y+5+($m*5), date("d-m-Y", strtotime($delivered_date)));
			$now = time();
			$expected_days_diff=$now-strtotime($delivered_date);
			$expected_days=floor($expected_days_diff / (60 * 60 * 24))+1;
			$pdf->Text(30, $Line_y+5+($m*5), '('.$expected_days.' Days)');
			$m++;
		}
		foreach ($value['invoice_no'] as $key=>$value2)
		{
			$invoice_no=$value2;
			$pdf->Text(80,$Line_y+5+($l*5), "Purchases Bill No:".$invoice_no);
			$l++;
		}
		$new_balance=0;
		foreach ($value['balance'] as $key=>$value3)
		{
			$balance=$value3;
			$pdf->Text(150,$Line_y+5+($n*5), $balance);
			$n++;
			$new_balance+=$balance;
		}
		$pdf->Text(80,$Line_y+($inner_array_count*5)+10, "Total Amount:");
		$pdf->Line(5,$Line_y+($inner_array_count*5)+10+5,205,$Line_y+($inner_array_count*5)+10+5);
		$pdf->Text(150,$Line_y+($inner_array_count*5)+10, $new_balance);
		$Line_y=$Line_y+($inner_array_count*5)+15;
		$i++;
		$grand_total+=$new_balance;
	}
	footer($pdf,$grand_total,$Line_y);
	$pdf->Output();
	exit;
}
public function VATReport()
{
	$data['GSTReport']['from_date']=$from_date=date('d-m-Y',strtotime('-1 month'));
	$data['GSTReport']['to_date']=$to_date=date('d-m-Y');
	$SaleItem_DISTINCT_cgst=$this->SaleItem->find('all',array(
		'fields'=>array('DISTINCT SaleItem.tax'),
		)
	);
	$PurchaseItem_DISTINCT_cgst=$this->PurchasedItem->find('all',array(
		'fields'=>array('DISTINCT PurchasedItem.vat'),
		)
	);
	$GST_RATE=[];
	foreach ($SaleItem_DISTINCT_cgst as $key => $value) {
		$GST_RATE[floatval($value['SaleItem']['tax'])]=floatval($value['SaleItem']['tax'])."%";
	}
	foreach ($PurchaseItem_DISTINCT_cgst as $key => $value) {
		$GST_RATE[floatval($value['PurchasedItem']['vat'])]=floatval($value['PurchasedItem']['vat'])."%";
	}
	unset($GST_RATE[0]);
	$GST_RATE['Zero']='0%';
	$this->set(compact('GST_RATE'));
	$this->request->data=$data;
	$ProductType=$this->ProductType->find('list');
	$this->set(compact('ProductType'));
	$Product=$this->Product->find('list');
	$this->set(compact('Product'));
	$GSTReport=['list'=>[],'total'=>[]];
	$cgst_in=0;
	$cgst_out=0;
	$sgst_in=0;
	$sgst_out=0;
	$igst_in=0;
	$igst_out=0;
	$total_in=0;
	$total_out=0;
	$taxable_value=0;
	foreach ($Product as $key => $value) {
		$single_GSTReport=$this->get_VATReport($key,$from_date,$to_date);
		if($single_GSTReport)
		{
			foreach ($single_GSTReport as $keyG => $valueG) {
// if($valueG['total']['in'] || $valueG['total']['out'])
// {
				$cgst_out+=$valueG['cgst']['out'];
				$cgst_in+=$valueG['cgst']['in'];
				$sgst_out+=$valueG['sgst']['out'];
				$sgst_in+=$valueG['sgst']['in'];
				$igst_out+=$valueG['igst']['out'];
				$igst_in+=$valueG['igst']['in'];
				$taxable_value+=$valueG['taxable_value'];
				$GSTReport['list'][]=$valueG;
// }
			}
		}
		$GSTReport['total']['cgst']['in']=$cgst_in;
		$GSTReport['total']['cgst']['out']=$cgst_out;
		$GSTReport['total']['sgst']['in']=$sgst_in;
		$GSTReport['total']['sgst']['out']=$sgst_out;
		$GSTReport['total']['igst']['in']=$igst_in;
		$GSTReport['total']['igst']['out']=$igst_out;
		$GSTReport['total']['taxable_value']=$taxable_value;
		$GSTReport['total']['total']['in']=$cgst_in+$sgst_in+$igst_in;
		$GSTReport['total']['total']['out']=$cgst_out+$sgst_out+$igst_out;
	}
	$this->set(compact('GSTReport'));
}
public function VATReport_ajax()
{
	$data=$this->request->data;
	$conditions=[];
	$product_type_id='';
	if(isset($data['product_type_id']))
	{
		if($data['product_type_id'])
		{
			$product_type_id=$data['product_type_id'];	
			$conditions['Product.product_type_id']=$product_type_id;
		}
	}
	if(isset($data['product_id']))
	{
		if($data['product_id'])
		{
			$product_id=$data['product_id'];	
			$conditions['Product.id']=$product_id;
		}
	}
	$type='';
	if(isset($data['type']))
	{
		if($data['type'])
		{
			$type=$data['type'];	
		}
	}
	$gst='';
	if(isset($data['gst']))
	{
		if($data['gst'])
		{
			$gst=$data['gst'];	
		}
	}
	$from_date=$data['from_date'];
	$to_date=$data['to_date'];
	$GSTReport=['list'=>[],'total'=>[]];
	$cgst_in=0;
	$cgst_out=0;
	$sgst_in=0;
	$sgst_out=0;
	$igst_in=0;
	$igst_out=0;
	$total_in=0;
	$total_out=0;
	$taxable_value=0;
	$Product=$this->Product->find('list',array(
		'conditions'=>$conditions,
		));
	foreach ($Product as $key => $value) {
		$single_GSTReport=$this->get_VATReport($key,$from_date,$to_date,$gst,$type,$product_type_id);
		if($single_GSTReport)
		{
			foreach ($single_GSTReport as $keysingle_ => $single_GSTReport_value) {
// if($single_GSTReport_value['total']['in'] || $single_GSTReport_value['total']['out'])
// {
				$cgst_out+=$single_GSTReport_value['cgst']['out'];
				$cgst_in+=$single_GSTReport_value['cgst']['in'];
				$sgst_out+=$single_GSTReport_value['sgst']['out'];
				$sgst_in+=$single_GSTReport_value['sgst']['in'];
				$igst_out+=$single_GSTReport_value['igst']['out'];
				$igst_in+=$single_GSTReport_value['igst']['in'];
				$taxable_value+=$single_GSTReport_value['taxable_value'];
				$GSTReport['list'][]=$single_GSTReport_value;
// }
			}
		}
		$GSTReport['total']['cgst']['in']=$cgst_in;
		$GSTReport['total']['cgst']['out']=$cgst_out;
		$GSTReport['total']['sgst']['in']=$sgst_in;
		$GSTReport['total']['sgst']['out']=$sgst_out;
		$GSTReport['total']['igst']['in']=$igst_in;
		$GSTReport['total']['igst']['out']=$igst_out;
		$GSTReport['total']['taxable_value']=$taxable_value;
		$GSTReport['total']['total']['in']=$cgst_in+$sgst_in+$igst_in;
		$GSTReport['total']['total']['out']=$cgst_out+$sgst_out+$igst_out;
	}
	echo json_encode($GSTReport);
	exit;
}	
public function SalesVATReport()
{
	$data=$this->request->data;
	$SalesGSTReport=['list'=>[],'total'=>[]];
	$single_GSTReport=[];
	$all_GSTReport=[];
	if(!$data)
	{
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0));
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 day",$time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('date1',$date1);
		$this->set('final',$final);
	}
	else
	{
		$from_date=$data['SalesVATReport']['from_date'];
		$to_date=$data['SalesVATReport']['to_date'];
		$from=date("Y-m-d", strtotime($from_date));
		$to=date("Y-m-d", strtotime($to_date));
		$this->set('date1',$to_date);
		$this->set('final',$from_date);
		$SalesGSTReport=$this->sales_vat_report($from,$to);
	}
	$this->set(compact('SalesGSTReport'));
}
public function sales_vat_report($from_date,$to_date)
{
	$SalesGSTReport=['list'=>[],'total'=>[]];
	$single_GSTReport=[];
	$all_GSTReport=[];
	$Sale=$this->Sale->find('all',array(
		'order' => 'Sale.invoice_no ASC',
		'conditions'=>array(
			'Sale.status=2',
//'Sale.account_head_id'=>$value['Customer']['account_head_id'],
			'Sale.date_of_delivered between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
			),
		'fields'=>array(
			'Sale.account_head_id',
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			'Sale.grand_total',
			),
		));
	foreach ($Sale as $keyS => $valueS) {
		$Customer_details=$this->Customer->findByAccountHeadId($valueS['Sale']['account_head_id']);
		$customer_name=$Customer_details['AccountHead']['name'];
		$customergstin=$Customer_details['Customer']['gstin'];
//$single_GSTReport['customer_name']=$customer_name;
		$single_GSTReport['customer_gstin']=$customergstin;
		$single_GSTReport['date']=date('d-m-Y',strtotime($valueS['Sale']['date_of_delivered']));
		$single_GSTReport['invoice_no']=$valueS['Sale']['invoice_no'];
		$single_GSTReport['invoice_amount']=$valueS['Sale']['grand_total'];
		$single_GSTReport['taxable_value']=0;
		$single_GSTReport['gross_total']=0;
		$single_GSTReport['vat_5_total']=0;
		$single_GSTReport['vat_amount']=0;
		foreach ($valueS['SaleItem'] as $keyitem => $valueitem) {
			$single_GSTReport['taxable_value']+=$valueitem['net_value'];
			$single_GSTReport['gross_total']+=$valueitem['total'];
			$single_GSTReport['vat_amount']+=$valueitem['tax_amount'];
			if($valueitem['tax']==5)
			{
				$single_GSTReport['vat_5_total']+=$valueitem['tax_amount'];
			}
		}
		$all_GSTReport[]=$single_GSTReport;
	}
	if($all_GSTReport)
	{
		foreach ($all_GSTReport as $keyG => $valueG) {
			$SalesGSTReport['invoice_date']=$valueG['date'];
			$SalesGSTReport['invoice_no']=$valueG['invoice_no'];
//$SalesGSTReport['customer_name']=$valueG['customer_name'];
			$SalesGSTReport['invoice_amount']=$valueG['invoice_amount'];
			$SalesGSTReport['taxable_value']=$valueG['taxable_value'];
			$SalesGSTReport['gross_total']=$valueG['gross_total'];
			$SalesGSTReport['vat_5_total']=$valueG['vat_5_total'];
			$SalesGSTReport['vat_amount']=$valueG['vat_amount'];
			$SalesGSTReport['gstin']=$valueG['customer_gstin'];
			$SalesGSTReport['list'][]=$valueG;
		}
	}
	return $SalesGSTReport;
}
public function export_sales_vat_report($from_date,$to_date) 
{
	$this->response->download("sales_vat_report.csv");
	$conditions=array();
	$from=date("Y-m-d", strtotime($from_date));
	$to=date("Y-m-d", strtotime($to_date));
	$SalesGSTReport=$this->sales_vat_report($from_date,$to_date);
	$stock_array=array();
	foreach($SalesGSTReport['list'] as $key1 => $value1) { 
		array_push($stock_array,$value1);
	}
	$this->set('stock_array', $stock_array);
	$this->layout = 'ajax';
	return false;
	exit;
}
public function sales_offline_vat_print($from_date,$to_date)
{
	try{
// $SalesGSTReport=['list'=>[],'total'=>[]];
// $single_GSTReport=[];
// $all_GSTReport=[];
		$SalesGSTReport=$this->sales_vat_report($from_date,$to_date);
		$final_array=array();
		$header=array(
			'Date',
			'Inv.No',
// 'Customer',
//'Inv.Amount',
			'Taxable Value',
			'Gross Total',
			'VAT Sales 5%',
// 'GST Sales 18%',
// 'GST Sales 28%',
			'VAT Amount',
// 'SGST',
// 'IGST',
// 'GST in No'
			);
		$width=array(
			'25',
			'20',
// '75',
//'30',
			'38',
			'38',
			'40',
// '37',
// '37',
// '18',
			'45',
// '20',
// '32'
			);
		foreach ($SalesGSTReport['list'] as $key => $value) {
			$final_array[$key][]=$value['date'];
			$final_array[$key][]=$value['invoice_no'];
//$final_array[$key][]=$value['customer_name'];
//$final_array[$key][]=$value['invoice_amount'];
			$final_array[$key][]=$value['taxable_value'];
			$final_array[$key][]=$value['gross_total'];
			$final_array[$key][]=$value['vat_5_total'];
// $final_array[$key][]=$value['18_total'];
// $final_array[$key][]=$value['28_total'];
// $final_array[$key][]=$value['cgst'];
			$final_array[$key][]=$value['vat_amount'];
// $final_array[$key][]=$value['igst'];
// $final_array[$key][]=$value['customer_gstin'];
		}
		require('fpdf/fpdf.php');
		$pdf = new FPDF('l');
		$pdf->addPage();
		$pdf->SetFont('Arial','',14);
// $pdf->Text(105, 10, 'NIGELLAS HERBAL HEALTH CARE');
// 	$pdf->setFont("Arial",'B','9');    
// 	$pdf->Text(80, 15, '7/77, Industrial Compound.Thiruvangoor,Calicut-673304,PH:+91 9745 005 600');
// 	$pdf->Text(110, 19, 'www.nigellas.in,info@nigellas.in');
// 	$pdf->Line(1,22,295,22);
		$pdf->SetFont('Arial','B','11');
// if($flag!='B2C')
// {
// 	$pdf->Text(15, 30, 'Sales Offline B2B GST Report of '.$customer_type_name.' for the Period From '.date('d-M-Y',strtotime($from_date)).' To '.date('d-M-Y',strtotime($to_date)));
// }
// else{
// 	$pdf->Text(15, 30, 'Sales Offline B2C GST Report of '.$customer_type_name.' for the Period From '.date('d-M-Y',strtotime($from_date)).' To '.date('d-M-Y',strtotime($to_date)));
// }
		$pdf -> SetY(32); 
// $pdf->text(120,20,'Sales Offline GST Report');
// $pdf->text(110,28,'Customer Type:');
// $pdf->text(155,28,$customer_type_name);
		$pdf->SetFont('Arial','',14);
		$pdf -> SetY(35); 
		for($i=0;$i<count($header);$i++)
			$pdf->Cell($width[$i],7,$header[$i],1,0,'C');
		$pdf->Ln();
		$pdf->SetFillColor(224,235,255);
		$fill = false;
		$pdf->SetFont('Arial','','10');
		foreach($final_array as $row)
		{
			$i=0;
			foreach($row as $col){
				$pdf->Cell($width[$i],6,$col,'LR',0,'L',$fill);
// $pdf->Cell($width[$i],6,$col,1,0,'L',$fill);
				$i++;
			}
			$pdf->Ln();
			$fill = !$fill;
		}
		$pdf->Cell(array_sum($width),0,'','T');
		$pdf->Output();
		exit;
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}
public function ItemwiseVATReport()
{
	$data=$this->request->data;
	$list_array=array();
	$VATReport=['list'=>[],'total'=>[]];
	if(!$this->request->is('post'))
	{
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0));
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 day",$time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('date1',$date1);
		$this->set('final',$final);
		$VATReport['total']['net_amount']=0;
		$VATReport['total']['taxable_value']=0;
		$VATReport['total']['vat_amount']=0;
		$VATReport['total']['quantity']=0;
		$VATReport['total']['total_amount']=0;
		$VATReport['total']['total_amount']=0;
	}
	else
	{
		$from_date=$data['ItemwiseVATReport']['from_date'];
		$to_date=$data['ItemwiseVATReport']['to_date'];
		$from=date("Y-m-d", strtotime($from_date));
		$to=date("Y-m-d", strtotime($to_date));
		$this->set('date1',$to_date);
		$this->set('final',$from_date);
		$list_array=$this->itemwise_vat_report($from,$to);
		$net_amount=0;
		$taxable_value=0;
		$vat_amount=0;
		$quantity=0;
		$total_amount=0;
		$single_VATReport=$this->itemwise_vat_report($from,$to);
		if($single_VATReport)
		{
			foreach ($single_VATReport as $keyG => $valueG) {
				foreach ($valueG['single_item'] as $keyG => $valuesingle) {
					$net_amount+=$valuesingle['net_value'];
					$taxable_value+=$valuesingle['taxable_value'];
					$vat_amount+=$valuesingle['vat_amount'];
					$quantity+=$valuesingle['quantity'];
					$total_amount+=$valuesingle['total'];
					$VATReport['list'][]=$valuesingle;
				}
			}
		}
		$VATReport['total']['net_amount']=$net_amount;
		$VATReport['total']['taxable_value']=$taxable_value;
		$VATReport['total']['vat_amount']=$vat_amount;
		$VATReport['total']['quantity']=$quantity;
		$VATReport['total']['total_amount']=$total_amount;
	}
	$this->set('VATReport',$VATReport);
}
public function itemwise_vat_report($from,$to)
{
	$list_array=array();
	$list_array_single=array();
	$cutomertype=array();
	$SaleItem=$this->SaleItem->find('all',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'LEFT',
				'conditions'=>array('Sale.account_head_id=AccountHead.id')
				),
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
// array(
//   'table'=>'units',
//   'alias'=>'Unit',
//   'type'=>'RIGHT',
//   'conditions'=>array('SaleItem.unit_id=Unit.id')
//   ),
			),
		'conditions'=>array(
// 'Sale.account_head_id'=>$customer_id,
			'Sale.flag=1',
			'Sale.status=2',
			'Sale.date_of_delivered between ? and ?' => array($from,$to)
			),
		'fields'=>array(
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			'AccountHead.name',
			'Customer.place',
			'Customer.gstin',
			'SaleItem.net_value',
			'SaleItem.tax',
			'SaleItem.tax_amount',
			'SaleItem.quantity',
			'SaleItem.total',
			'Product.name',
//'Unit.name',
			'Product.hsn_code',
			),
		  'order'=>array('Sale.id DESC'),
		));
	$cutomertype['single_item']=[];
	foreach ($SaleItem as $keySOff => $valueSoff) {
		$invoice_no=$valueSoff['Sale']['invoice_no'];
		$place_trim=$valueSoff['Customer']['place'];
		$place_explod=explode(',', $place_trim);
		$place=$place_explod[0];
		$siglecutomertype['invoice_no']=$invoice_no;
		$siglecutomertype['invoice_date']=date('d-m-Y',strtotime($valueSoff['Sale']['date_of_delivered']));
		$siglecutomertype['customer_name']=$valueSoff['AccountHead']['name'];
		$siglecutomertype['gststin']=$valueSoff['Customer']['gstin'];
		$siglecutomertype['place']=preg_replace('/[\n\r\t]+/', ' ', $place);
		$siglecutomertype['net_value']=$valueSoff['SaleItem']['net_value'];
		$siglecutomertype['vat']=$valueSoff['SaleItem']['tax'];
		$siglecutomertype['vat_amount']=$valueSoff['SaleItem']['tax_amount'];
		$siglecutomertype['taxable_value']=$valueSoff['SaleItem']['net_value'];
		$siglecutomertype['quantity']=$valueSoff['SaleItem']['quantity'];
		$siglecutomertype['total']=$valueSoff['SaleItem']['total'];
		$siglecutomertype['product_name']=str_replace(',', ' ', $valueSoff['Product']['name']);
//$siglecutomertype['unit_name']=$valueSoff['Unit']['name'];
		$siglecutomertype['hsn_code']=$valueSoff['Product']['hsn_code'];
		$cutomertype['single_item'][]=$siglecutomertype;
	}
	/******customer details hidden special case******/
// 	$Saleone=$this->Sale->find('first',array(
// 		'conditions'=>array('Sale.status'=>2,'Sale.flag'=>1,'Sale.invoice_no'=>"R196-1",'Sale.date_of_delivered between ? and ?'=>array($from,$to),),
// 		'fields'=>array(
// 			'AccountHead.id',
// 			'Sale.discount_amount',
// 			'AccountHead.name',
// 			'Executive.id',
// 			'Sale.id',
// 			'Sale.invoice_no',
// 			'Sale.date_of_delivered',
// 			'Executive.name',
// 		)
// 	));
// 					if($Saleone){
// 					$SaleItem1=$this->SaleItem->find('all',array(
// 					'conditions'=>array(
// 					'Sale.id'=>$Saleone['Sale']['id'],
// 					'Sale.date_of_delivered between ? and ?'=>array($from,$to),
// 					'Sale.status'=>2,'Sale.flag'=>1,
// 					),
// 					'fields'=>array(
// 						'Product.name',
// 					'SaleItem.net_value',
// 					'SaleItem.tax_amount',
// 					'SaleItem.total',
// 					'SaleItem.tax',
// 					'SaleItem.quantity'
// 					),
// 					));
// 					foreach ($SaleItem1 as $key => $value) {
// 			$siglecutomertype['invoice_no']=$Saleone['Sale']['invoice_no'];
// 		$siglecutomertype['invoice_date']=date('d-m-Y',strtotime($Saleone['Sale']['date_of_delivered']));
// 		$siglecutomertype['customer_name']="";
// 		$siglecutomertype['gststin']="";
// 		$siglecutomertype['place']="";
// 		$siglecutomertype['net_value']=$value['SaleItem']['net_value'];
// 		$siglecutomertype['vat']=$value['SaleItem']['tax'];
// 		$siglecutomertype['vat_amount']=$value['SaleItem']['tax_amount'];
// 		$siglecutomertype['taxable_value']=$value['SaleItem']['net_value'];
// 		$siglecutomertype['quantity']=$value['SaleItem']['quantity'];
// 		$siglecutomertype['total']=$value['SaleItem']['total'];
// 		$siglecutomertype['product_name']=str_replace(',', ' ', $value['Product']['name']);
// //$siglecutomertype['unit_name']=$SaleItem1['Unit']['name'];
// 		$siglecutomertype['hsn_code']="";
// 		$cutomertype['single_item'][]=$siglecutomertype;
// 	}
// 	}
	$list_array[]=$cutomertype;
	return $list_array;
}
public function print_itemwise_vat_report($from_date,$to_date) 
{
	$this->response->download("vat_report_itemwise.csv");
	$conditions=array();
	$from=date("Y-m-d", strtotime($from_date));
	$to=date("Y-m-d", strtotime($to_date));
	$list_array=array();
	$list_array=$this->itemwise_vat_report($from,$to);
	$stock_array=array();
	foreach($list_array[0] as $key1 => $value1) { 
		foreach($value1 as $keysingle => $valuesingle) { 
			array_push($stock_array,$valuesingle);
		}
	}
	$this->set('Stock_array', $stock_array);
	$this->layout = 'ajax';
	return false;
	exit;
}
public function TaxWiseVATReport()
{
	$data=$this->request->data;
	$list_array=array();
	$VATReport=['list'=>[],'total'=>[]];
	if(!$this->request->is('post'))
	{
		$date0= date('m/d/Y');
		$date1=date("d-m-Y", strtotime($date0));
		$time = strtotime($date1);
		$final1 = date('m/d/Y', strtotime("-1 day",$time));
		$final=date("d-m-Y", strtotime($final1) );
		$this->set('date1',$date1);
		$this->set('final',$final);
		$VATReport['total']['net_amount']=0;
		$VATReport['total']['taxable_value']=0;
		$VATReport['total']['vat_amount']=0;
		$VATReport['total']['quantity']=0;
		$VATReport['total']['total_amount']=0;
		$VATReport['total']['total_amount']=0;
	}
	else
	{
		$from_date=$data['TaxWiseVATReport']['from_date'];
		$to_date=$data['TaxWiseVATReport']['to_date'];
		$from=date("Y-m-d", strtotime($from_date));
		$to=date("Y-m-d", strtotime($to_date));
		$this->set('date1',$to_date);
		$this->set('final',$from_date);
		$VATReport=$this->tax_wise_vat_report($from,$to);
	}
	$this->set('VATReport',$VATReport);
}
public function tax_wise_vat_report($from,$to)
{
	$list_array=array();
	$list_array_single=array();
	$cutomertype=array();
	$all_taxes_array=array();
	$return['total']['taxable_value']=0;
	$return['total']['vat_amount']=0;
	$return['total']['quantity']=0;
	$return['total']['total_amount']=0;
	$Sale=$this->Sale->find('list',array(
		'conditions'=>array(
			'status=2',
			'flag=1',
			),
		'fields'=>array(
			'Sale.id',
			'Sale.invoice_no',
			),
		));
	$invoice_wise_tax_list=[];
	$state_id=$this->Global_Var_Profile['Profile']['state_id'];
	foreach ($Sale as $sale_id => $invoice_no) {
		$SaleItem=$this->SaleItem->find('all',array(
			'joins'=>array(
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'LEFT',
					'conditions'=>array('Sale.account_head_id=AccountHead.id')
					),
				array(
					'table'=>'customers',
					'alias'=>'Customer',
					'type'=>'INNER',
					'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
					),
// array(
// 	'table'=>'units',
// 	'alias'=>'Unit',
// 	'type'=>'RIGHT',
// 	'conditions'=>array('Product.unit_id=Unit.id')
// 	),
				),
			'conditions'=>array(
				'Sale.id'=>$sale_id,
				'Sale.flag=1',
				'Sale.status=2',
				'Sale.date_of_delivered between ? and ?' => array($from,$to)
				),
			'fields'=>array(
				'Sale.invoice_no',
				'Sale.date_of_delivered',
				'AccountHead.name',
				'Customer.place',
				'Customer.state_id',
//'Customer.gstin',
				'SaleItem.tax',
				'SaleItem.tax_amount',
				'SaleItem.net_value',
				'SaleItem.quantity',
				'SaleItem.total',
				'Product.name',
				'Sale.discount_amount'
//'Unit.name',
				),
			'order'=>array('Sale.id DESC'),
			));
		foreach ($SaleItem as $key => $valueSoff) {
			$invoice_no=$valueSoff['Sale']['invoice_no'];
			$place_trim=$valueSoff['Customer']['place'];
			$place_explod=explode(',', $place_trim);
			$place=$place_explod[0];
			$siglecutomertype['invoice_no']=$invoice_no;
			$siglecutomertype['invoice_date']=date('d-m-Y',strtotime($valueSoff['Sale']['date_of_delivered']));
			$siglecutomertype['customer_name']=$valueSoff['AccountHead']['name'];
			$siglecutomertype['customer_state_id']=$valueSoff['Customer']['state_id'];
//$siglecutomertype['gststin']=$valueSoff['Customer']['gstin'];
			$siglecutomertype['place']=preg_replace('/[\n\r\t]+/', ' ', $place);
			$siglecutomertype['vat']=$valueSoff['SaleItem']['tax'];
			$siglecutomertype['vat_amount']=$valueSoff['SaleItem']['tax_amount'];
			$siglecutomertype['taxable_value']=$valueSoff['SaleItem']['net_value'];
			$siglecutomertype['quantity']=$valueSoff['SaleItem']['quantity'];
			$siglecutomertype['total']=$valueSoff['SaleItem']['total'];
//$siglecutomertype['unit_name']=$valueSoff['Unit']['name'];
			$return['total']['taxable_value']+=$siglecutomertype['taxable_value'];
			$return['total']['vat_amount']+=$siglecutomertype['vat_amount'];
			$return['total']['quantity']+=$siglecutomertype['quantity'];
			$return['total']['total_amount']+=$siglecutomertype['total'];
			if(isset($invoice_wise_tax_list[$invoice_no]))
			{
				if(isset($invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]))
				{
					$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['quantity']+=$siglecutomertype['quantity'];
					$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['vat_amount']+=$siglecutomertype['vat_amount'];
					$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['taxable_value']+=$siglecutomertype['taxable_value'];
					$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['total']+=$siglecutomertype['total'];
				}
				else
				{
					$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]=$siglecutomertype;
				}
			}
			else
			{
				$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]=$siglecutomertype;
			}
		}

	}
	/*****special case*****/
		$Saleone=$this->Sale->find('first',array(
		'conditions'=>array('Sale.status'=>2,'Sale.flag'=>1,'Sale.invoice_no'=>"R196-1",'Sale.date_of_delivered between ? and ?'=>array($from,$to),),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Sale.id',
			'Sale.invoice_no',
			'Executive.name',
			'Sale.date_of_delivered'
		)
	));
		// $this->SaleItem->virtualFields = array(
		// 	'grand_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
		// 	'grand_tax_amount' => "SUM(SaleItem.tax_amount)",
		// 	'total_quantity' => "SUM(SaleItem.quantity)",
		// 	'tax' => "SaleItem.tax",
  //            'grand_total' => "SUM(((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount))",
		// );
		// 			if($Saleone){
		// 			$SaleItem1=$this->SaleItem->find('first',array(
		// 			'conditions'=>array(
		// 			'Sale.id'=>$Saleone['Sale']['id'],
		// 			'Sale.date_of_delivered between ? and ?'=>array($from,$to),
		// 			'Sale.status'=>2,'Sale.flag'=>1,
		// 			),
		// 			'fields'=>array(
		// 			'SaleItem.grand_net_value',
		// 			'SaleItem.grand_tax_amount',
		// 			'SaleItem.grand_total',
		// 			'SaleItem.tax',
		// 			'SaleItem.total_quantity'
		// 			),
		// 			));
		// 	$siglecutomertype['invoice_no']=$Saleone['Sale']['invoice_no'];
		// 	$siglecutomertype['invoice_date']=date('d-m-Y',strtotime($Saleone['Sale']['date_of_delivered']));
		// 	$siglecutomertype['customer_name']="";
		// 	$siglecutomertype['customer_state_id']="";
		// 	$siglecutomertype['place']="";
		// 	$siglecutomertype['vat']=$SaleItem1['SaleItem']['tax'];
		// 	$siglecutomertype['vat_amount']=$SaleItem1['SaleItem']['grand_tax_amount'];
		// 	$siglecutomertype['taxable_value']=$SaleItem1['SaleItem']['grand_net_value'];
		// 	$siglecutomertype['quantity']=$SaleItem1['SaleItem']['total_quantity'];
		// 	$siglecutomertype['total']=$SaleItem1['SaleItem']['grand_total'];
		// 	$return['total']['taxable_value']+=$siglecutomertype['taxable_value'];
		// 	$return['total']['vat_amount']+=$siglecutomertype['vat_amount'];
		// 	$return['total']['quantity']+=$siglecutomertype['quantity'];
		// 	$return['total']['total_amount']+=$siglecutomertype['total'];
		// 	if(isset($invoice_wise_tax_list[$invoice_no]))
		// 	{
		// 		if(isset($invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]))
		// 		{
		// 			$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['quantity']+=$siglecutomertype['quantity'];
		// 			$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['vat_amount']+=$siglecutomertype['vat_amount'];
		// 			$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['taxable_value']+=$siglecutomertype['taxable_value'];
		// 			$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]['total']+=$siglecutomertype['total'];
		// 		}
		// 		else
		// 		{
		// 			$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]=$siglecutomertype;
		// 		}
		// 	}
		// 	else
		// 	{
		// 		$invoice_wise_tax_list[$invoice_no][$siglecutomertype['vat']]=$siglecutomertype;
		// 	}
		// 			}
           
	$return['list']=$invoice_wise_tax_list;
	return $return;
}
public function print_tax_wise_vat_report($from_date,$to_date) 
{
	$this->response->download("gst_report_vat_wise.csv");
	$conditions=array();
	$from=date("Y-m-d", strtotime($from_date));
	$to=date("Y-m-d", strtotime($to_date));
	$list_array=array();
	$list_array=$this->tax_wise_vat_report($from,$to);
	$stock_array=array();
	foreach($list_array['list'] as $key1 => $value1) { 
		foreach($value1 as $keysingle => $valuesingle) { 
			array_push($stock_array,$valuesingle);
		}
	}
	$this->set('Stock_array', $stock_array);
	$this->layout = 'ajax';
	return false;
	exit;
}
public function General_Journal_Debit_Credit_function($account_head_id)
{
	$account_single=[];
	$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)",);
	$Journal_Debit=$this->Journal->findByDebitAndFlag($account_head_id,'1',['Journal.total_amount']);
	$Journal_Credit=$this->Journal->findByCreditAndFlag($account_head_id,'1',['Journal.total_amount']);
	$account_single['credit']=$Journal_Credit['Journal']['total_amount'];
	$account_single['debit']=$Journal_Debit['Journal']['total_amount'];
	return $account_single;
}
public function DayRegisterPrint()
{
	$this->request->data['Reports']['from']=date('d-m-Y');
	$this->request->data['Reports']['to']=date('d-m-Y');
	$routes=$this->Route->find('list');
	$this->set(compact('routes'));
}
public function DayRegisterPrintRoute()
{
	$this->request->data['Reports']['from']=date('d-m-Y');
	$this->request->data['Reports']['to']=date('d-m-Y');
	$routes=$this->Route->find('list');
	$this->set(compact('routes'));
}
public function CheckDayRegister($route_id,$register)
{
	$register = date('Y-m-d',strtotime($register));
	$ExecutiveRouteMapping=$this->ExecutiveRouteMapping->findByRouteId($route_id);
	$executive=$ExecutiveRouteMapping['ExecutiveRouteMapping']['executive_id'];
	$DayRegister = $this->DayRegister->findAllByExecutiveIdAndDate($executive,$register,['DayRegister.id','DayRegister.id']);
	$return['result']='Empty';
	if($DayRegister)
	{
		$return['result']='Success';
		$list=[];
		foreach ($DayRegister as $key => $value) {
			$list[$value['DayRegister']['id']]=$value['DayRegister']['id'];
		}
		$return['list']=$list;
	}
	echo json_encode($return); exit;
}
public function CheckDayRegister1($route_id,$register)
{
	$register = date('Y-m-d',strtotime($register));
	$ExecutiveRouteMapping=$this->ExecutiveRouteMapping->findByRouteId($route_id);
	$executive=$ExecutiveRouteMapping['ExecutiveRouteMapping']['executive_id'];
	$DayRegister = $this->DayRegister->findAllByExecutiveIdAndDate($executive,$register,['DayRegister.id','DayRegister.id']);
	$cash_id = $this->Route->field('Route.account_head_id',array('Route.id ' =>$route_id));
	$return['result']='Empty';
	if($DayRegister)
	{
		if($cash_id)
		{
			$return['result']='Success';
			$list=[];
			foreach ($DayRegister as $key => $value) {
				$list[$value['DayRegister']['id']]=$value['DayRegister']['id'];
			}
			$return['list']=$list;
		}
	}
	echo json_encode($return); exit;
}
public function dcrpdf($day_register_id,$register,$closed)
{
	$register = date('Y-m-d',strtotime($register));
	$closed = date('Y-m-d',strtotime($closed));
	$date_build = date_parse_from_format("Y-m-d", $register);
	$month = $date_build["month"];
	$content['display_date'] = date('d-m-Y',strtotime($register));
	$footer_content['initial_stock']=0;
	$footer_content['loading_stock']=0;
	$content['route_name']='';
	$day = $this->DayRegister->findById($day_register_id);
	$executive = $day['DayRegister']['executive_id'];
	if($day)
	{
		$footer_content['initial_stock'] = $day['DayRegister']['initial_stock'];
		$footer_content['loading_stock'] = $day['DayRegister']['loading_stock'];
		$content['route_name'] = $day['Route']['name'];
		$day_register_id=$day['DayRegister']['id'];
	}
	$closing_stock = 0;
	$close = $this->ClosingDay->findByExecutiveIdAndDate($executive,$closed);
	if($close)
		$closing_stock = $close['ClosingDay']['closing_stock'];
	$footer_content['closing_stock'] =$closing_stock;
	$warehouse_id = $this->Executive->field('Executive.warehouse_id',array('Executive.id ' => $executive));
	$content['warehouse_name'] = $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $warehouse_id));
	$cash_id = $this->Executive->field('Executive.account_head_id',array('Executive.id ' =>$executive));

	$CustomerList=$this->AccountHead->find('all',array(
		"joins"=>array(
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'inner',
				"conditions"=>array('Customer.account_head_id=AccountHead.id'),
				),
			array(
				"table"=>'executive_route_mappings',
				"alias"=>'ExecutiveRouteMapping',
				"type"=>'inner',
				"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
				),
			),
		'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
		'fields'=>array(
			'Customer.code',
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			),
		));
	$receipt_visit = 0;
	$All_Customer=[];
	if($CustomerList)
	{
		$j =0;
		$no_sale_count =0;
		$max_sale = 0;
		$max_credit_sale = 0;
		$max_cash_sale = 0;
		$max_receipt_sale = 0;
		$max_balance =0;
		$current_market_outstanding =0;
		foreach ($CustomerList as $key => $value) {
			$total_sale =0;
			$Single_Customer['id']=$value['AccountHead']['id'];
			$Single_Customer['name']=$value['AccountHead']['name'];
			$Single_Customer['shope_code']=$value['Customer']['code'];
			$Single_Customer['sort'] = 1;
			$Sale=$this->Sale->find('all',array(
				'conditions'=>array(
					'Sale.account_head_id'=>$value['AccountHead']['id'],
					'Sale.executive_id'=>$executive,
// 'Sale.date_of_delivered between ? and ?'=>[$register,$closed],
					'Sale.day_register_id'=>$day_register_id,
					'Sale.status'=>2,
					),
				'fields'=>array(
					'Sale.id',
					'Sale.invoice_no',
					'Sale.grand_total',
					'Sale.date_of_delivered',
					),
				));
			$All_sale=[];
			$cash_sale_total =0;
			$collected =0;
			foreach ($Sale as $key => $row) {
				$total_sale+=$row['Sale']['grand_total'];
				$remarks='Sale Invoice No :'. $row['Sale']['invoice_no'];
				$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
				$Journal_cash_sale=$this->Journal->find('first',array(
					'conditions'=>array(
						'credit'=>$value['AccountHead']['id'],
						'debit'=>$cash_id,
						'flag=1',
						'day_register_id'=>$day_register_id,
						'remarks'=>$remarks,
						'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
//'date'=>$row['Sale']['date_of_delivered'],
						),
					'fields'=>array('Journal.total_amount'),
					));
				$cash_sale_total+=$Journal_cash_sale['Journal']['total_amount'];
			}
			$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
			$Journal_credit=$this->Journal->find('first',array(
				'conditions'=>array(
					'credit'=>$value['AccountHead']['id'],
					'flag=1',
//'executive_id'=>$executive,
					'day_register_id'=>$day_register_id,
					'debit'=>$cash_id,
					'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
// 'date between ? and ?'=>[$register,$closed],
					),
				'fields'=>array('Journal.total_amount'),
				));
			$receipt_flag = 0;
			$collected+=$Journal_credit['Journal']['total_amount'];
			$credit_sale = $total_sale - $cash_sale_total;
			if($credit_sale<0) $credit_sale = 0.00;
			if($collected<0) $collected = 0.00;
			$collected-=$cash_sale_total;
			$max_sale+=$total_sale;
			$max_credit_sale+=$credit_sale;
			$max_cash_sale+=$cash_sale_total;
			$max_receipt_sale+=$collected;
			$Single_Customer['receipt'] = $collected;
			$Single_Customer['cash_sale'] = $cash_sale_total;
			$Single_Customer['credit_sale'] = $credit_sale;
			$No_flag = 0;
			if($total_sale==0 && $collected==0){
				$NoSale=$this->NoSale->find('all',array(
					'conditions'=>array(
						'NoSale.customer_account_head_id'=>$value['AccountHead']['id'],
						'NoSale.executive_id'=>$executive,
// 'NoSale.date between ? and ?'=>[$register,$closed],
						'NoSale.day_register_id'=>$day_register_id,
						),
					'fields'=>array(
						'NoSale.id',
						'NoSale.description',
						),
					));
				foreach ($NoSale as $key => $row) {
					$No_flag = 1;
					$no_sale_count++;
					$Single_Customer['sort'] = 0;
					$Single_Customer['credit_sale'] = 'No Sale';
					$Single_Customer['receipt'] = '';
					$Single_Customer['cash_sale'] = $row['NoSale']['description'];
				}
			}
			if($total_sale==0 && $collected!=0 && $No_flag==0){
				$receipt_visit++;
			}
			$Debit_N_Credit_function=$this->General_Journal_Debit_Credit_function($value['AccountHead']['id']);
			if($total_sale!=0 || $collected!=0 ||$No_flag!=0)
			{
				$Single_Customer['balance']=$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
				$max_balance+=$Single_Customer['balance'];
				array_push($All_Customer, $Single_Customer);
			}
			$single_market_outstanding =$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
			$current_market_outstanding =$current_market_outstanding + $single_market_outstanding;
		}
	}
	array_multisort( array_column($All_Customer, "sort"), SORT_DESC, $All_Customer );
	$content['All_Customer'] = $All_Customer;
	$postive_visit_sale=$this->Sale->find('count',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
// 'Sale.date_of_delivered between ? and ?'=>[$register,$closed],
			'Sale.day_register_id'=>$day_register_id,
			'Sale.is_erp'=>0
			),
		'group' => array('Sale.account_head_id'),
		));
	$footer_content['visit'] = count($All_Customer);
	$visit = count($All_Customer);
	$postive_visit = $postive_visit_sale + $receipt_visit;
	$footer_content['postive_visit'] = $postive_visit;
	$strike_rate =0;
	if($visit) $strike_rate = ($postive_visit/$visit)*100;
	$footer_content['strike_rate'] = number_format((float)$strike_rate, 2, '.', '');
	$footer_content['max_balance'] =number_format((float)$max_balance, 2, '.', '');
	$footer_content['max_credit_sale'] =number_format((float)$max_credit_sale, 2, '.', '');
	$footer_content['max_cash_sale'] = number_format((float)$max_cash_sale, 2, '.', '');
	$footer_content['max_receipt_sale'] = number_format((float)$max_receipt_sale, 2, '.', '');
	$footer_content['max_sale'] = number_format((float)$max_sale, 2, '.', '');
	$footer_content['current_market_outstanding'] = number_format((float)$current_market_outstanding, 2, '.', '');
	$Month_sale_amount =0;
	$MonthSale_list=$this->Sale->find('all',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>$month,
			'Sale.day_register_id'=>$day_register_id,
			'Sale.status'=>2
			),
		'fields'=>array(
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			),
		));
	$Monthly_cash_sale = 0;
	foreach ($MonthSale_list as $key => $value) {
		$remarks='Sale Invoice No :'. $value['Sale']['invoice_no'];
		$cash_sale_row = $this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$cash_id,
				'flag=1',
				'day_register_id'=>$day_register_id,
				'remarks'=>$remarks,
				'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
				'date'=>$value['Sale']['date_of_delivered'],
				),
			'fields'=>array('Journal.amount',),
			));
		if(!empty($cash_sale_row)){
			$Monthly_cash_sale = $Monthly_cash_sale + $cash_sale_row['Journal']['amount'];
		}
	}
	$MonthSale=$this->Sale->find('first',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>$month,
			'Sale.day_register_id'=>$day_register_id,
			'Sale.status'=>2
			),
		'fields'=>array(
			'sum(Sale.grand_total) as monthly_sale',
			),
		));
	if($MonthSale){
		$Month_sale_amount =$MonthSale[0]['monthly_sale'];
	}
	$footer_content['Month_sale_amount'] = number_format((float)$Month_sale_amount, 2, '.', '');
	$cash_id = $this->Executive->field('Executive.account_head_id',array('Executive.id ' => $executive));
	$Month_collection_amount =0;
	$MonthCollection=$this->Journal->find('first',array(
		'conditions'=>array(
			'debit'=>$cash_id,
			'flag=1',
			'day_register_id'=>$day_register_id,
			'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
			'month(date)'=>$month,
			),
		'fields'=>array(
			'sum(Journal.amount) as monthly_amount',
			),
		));
	if(!empty($MonthCollection)){
		$Month_collection_amount =$MonthCollection[0]['monthly_amount'];
	}
	$footer_content['Month_collection_amount'] = number_format((float)$Month_collection_amount, 2, '.', '');
	require('fpdf/fpdf.php');
	$pdf = new FPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	define('FPDF_FONTPATH','fpdf/font');
	function header_section($pdf,$Profile,$content) {
		$pdf->SetFont('Arial','B',8.4);
		$pdf->AddPage();
		$pdf->Image('profile/'.$Profile['logo'],-1,1,-220);
		$pdf->SetFont('Arial','B',20);
		$pdf->SetTextColor(0,100,0);
		$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(200,00,00);
		$pdf->SetTextColor(10,10,10);
		$pdf->SetFont('Arial','B',8);
		$head_table_x=28;
		$pdf->rect(150, $head_table_x-10, 55, 8);
		$pdf->Text(152, 21, "Route : ".$content['route_name']);
		$pdf->rect(15, $head_table_x, 120, 8);
		$pdf->SetFont('Arial','',11);
		$pdf->Text(18, 33, "Warehouse : ".$content['warehouse_name']);
		$pdf->SetFont('Arial','B',12);
		$pdf->Text(80, 26, "DAILY SALES REPORT");
		$pdf->rect(150, $head_table_x, 55, 8);
		$pdf->SetFont('Arial','B',9);
		$pdf->Text(152, 33, "Date : ".date('d-M-Y',strtotime($content['display_date'])));
		$first_table_x=$head_table_x+13;
		$pdf->rect(15, $first_table_x, 190, 215);
		$slno_x=16;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($slno_x, $first_table_x+5+5, "SlNo");
		$pdf->Line($slno_x+7, $first_table_x,$slno_x+7, 200);
		$item_x=$slno_x+34;
		$item_line_x=$item_x+100;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($item_x, $first_table_x+10, "Customer");
		$tax_line_x=$item_x+35;
		$tax_x=$item_x+35;
		$pdf->Line($tax_line_x, $first_table_x,$tax_line_x, 200);
		$pdf->Text($tax_x, $first_table_x+5, "");
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($tax_x+5, $first_table_x+5+5, "Credit Sale");
		$tax_line_x=$tax_x+35;
		$pdf->Line($tax_line_x, $first_table_x,$tax_line_x, 200);
		$unit_x=$tax_x+35;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($unit_x+5, $first_table_x+5+5, "Cash Sale");
		$pdf->Line($item_line_x-3, $first_table_x,$item_line_x-3, 200);
		$qty_x=$item_line_x+4;
		$qty_line_x=$qty_x+8;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($qty_x, $first_table_x+5+5, "Receipt");
		$unit_price_x=$qty_line_x+2;
		$unit_price_line_x=$unit_price_x+15;
		$pdf->SetFont('Arial','B',7);
		$net_amount_x=$qty_line_x+2;
		$net_amount_line_x=$net_amount_x+16;
		$pdf->Line($net_amount_line_x-4, $first_table_x,$net_amount_line_x-4, 200);
		$total_x=$unit_price_line_x+2;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($total_x, $first_table_x+5+5, "O/S Balance");
		$pdf->Line(15, $first_table_x+12, 205, $first_table_x+12);
		$pdf->Line(15, 200, 205, 200);
		$pdf->Line(15, 205, 205, 205);
		$i=0;
	}
	function footer($pdf,$footer_content)
	{
		$footer_line=203;
		$pdf->Line(177, 205,177, 215);
		$pdf->Text(190, 211, $footer_content['max_balance']);
		$pdf->Line(148, 205,148, 215);
		$pdf->Text(159, 211, $footer_content['max_receipt_sale']);
		$pdf->Line(120, 205,120, 215);
		$pdf->Text(131, 211, $footer_content['max_cash_sale']);
		$pdf->Line(85, 205,85, 215);
		$pdf->Text(93, 211, $footer_content['max_credit_sale']);
		$pdf->Text(20, 211, 'Total :');
		$pdf->Line(15, 215, 205, 215);
		$pdf->Line(185, 215,185, 235);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 220, $footer_content['Month_sale_amount']);
		$pdf->Text(135, 220, 'Monthly Sale');
		$pdf->Text(190, 227, $footer_content['Month_collection_amount']);
		$pdf->Text(135, 227, 'Monthly Collection');
		$pdf->Line(105, 215,105, 235);
		$pdf->Line(75, 215,75, 235);
		$pdf->Text(80, 219, $footer_content['initial_stock']);
		$pdf->Text(20, 219, 'Opening Stock ');
		$pdf->Text(80, 223, $footer_content['loading_stock']);
		$pdf->Text(20, 223, 'Today Loading ');
		$pdf->Text(80, 227, $footer_content['closing_stock']);
		$pdf->Text(20, 227, 'Closing Stock ');
		$pdf->Text(80, 231, $footer_content['current_market_outstanding']);
		$pdf->Text(20, 231, 'Current Market Outstanding ');
		$pdf->Line(15, 235, 205, 235);
		$totalSale = ($footer_content['max_cash_sale'] + $footer_content['max_credit_sale']);
		$today_collection = ($footer_content['max_cash_sale'] + $footer_content['max_receipt_sale']);
		$pdf->Line(185, 235,185, 256);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 241, number_format((float)$totalSale,2));
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(135, 241, 'Today Sale');
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 250, number_format((float)$today_collection,2));
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(135, 250, 'Today Collection');
		$pdf->Line(105, 235,105, 256);
		$pdf->Line(75, 235,75, 256);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(80, 240, $footer_content['visit']);
		$pdf->Text(20, 240, 'Total Visit');
		$pdf->Text(80, 245, $footer_content['postive_visit']);
		$pdf->Text(20, 245, 'Positive Visit');
		$pdf->Text(80, 250, $footer_content['strike_rate'].'%');
		$pdf->Text(20, 250, 'Strike Rate');
		$pdf->SetFont('Arial','B',8);
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(20, 295, "Salesman");
		$pdf->Text(60, 295, "Supervisor");
		$pdf->Text(100, 295, "Accountant");
		$pdf->Text(140, 295, "Manager ");
	}
	$slno =1;
	header_section($pdf,$Profile,$content);
	$i=0;
	$head_table_x=10;
	$first_table_x=$head_table_x+36;
	$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; 
	foreach($content['All_Customer'] as $key=>$value) {
		$sl_no_v_x=18;
		$pdf->Text($sl_no_v_x, $first_table_x+11+(5*$i), $slno);
		$product_name_v_x=$sl_no_v_x+10;
		$pdf->Text($product_name_v_x, $first_table_x+11+(5*$i), $value['name']);
		$tax_v_x=$product_name_v_x+65;
		$pdf->Text($tax_v_x, $first_table_x+11+(5*$i), $value['credit_sale']);
		$unit_v_x=$tax_v_x+35;
		$pdf->Text($unit_v_x, $first_table_x+11+(5*$i), $value['cash_sale']);
		$quantity_v_x=$product_name_v_x+125;
		$pdf->Text($quantity_v_x, $first_table_x+11+(5*$i),floatval($value['receipt']) );
		$unit_price_v_x=$quantity_v_x+12;
		$total_v_x=$unit_price_v_x+18;
		if($value['balance']=='-0.00'){
			$value['balance'] ='0.00';
		}
		$pdf->Text($total_v_x, $first_table_x+11+(5*$i),floatval($value['balance']));
		$i++;
		$slno++;
		if($i>28)
		{
			$i=0;
			header_section($pdf,$Profile,$content);
		}
	}
	footer($pdf,$footer_content);
	$pdf->Output();
	exit;
}

public function dcrpdf1($day_register_id,$register,$closed)
{
	$register = date('Y-m-d',strtotime($register));
	$closed = date('Y-m-d',strtotime($register));
	$date_build = date_parse_from_format("Y-m-d", $register);
	$month = $date_build["month"];
	$content['display_date'] = date('d-m-Y',strtotime($register));
	$footer_content['initial_stock']=0;
	$footer_content['loading_stock']=0;
	$content['route_name']='';
	$day = $this->DayRegister->findById($day_register_id);
//pr($day);
//exit;
	$route_id=$day['DayRegister']['route_id'];
// pr($route_id);
// exit;
	$executive = $day['DayRegister']['executive_id'];
	if($day)
	{
		$footer_content['day_km']= $day['DayRegister']['km'];
		$day_time = new DateTime($day['DayRegister']['created_at']);
		$day_time = $day_time->format('H:i');
		$footer_content['day_time']= $day_time;
		$footer_content['initial_stock'] = $day['DayRegister']['initial_stock'];
		$footer_content['loading_stock'] = $day['DayRegister']['loading_stock'];
		$content['route_name'] = $day['Route']['name'];
		$day_register_id=$day['DayRegister']['id'];
	}
	$closing_stock = 0;
	$close = $this->ClosingDay->findByExecutiveIdAndDateAndDayRegisterId($executive,$closed,$day_register_id);
//pr($close);
//exit;
	if($close){
		$closing_stock = $close['ClosingDay']['closing_stock'];
		$footer_content['closing_stock'] =$closing_stock;
		$footer_content['close_km']= $close['ClosingDay']['km'];
		$close_time = new DateTime($close['ClosingDay']['closing_at']);
		$close_time = $close_time->format('H:i');

		$footer_content['close_time']= $close_time;
	}
	$warehouse_id = $this->Executive->field('Executive.warehouse_id',array('Executive.id ' => $executive));
	$content['warehouse_name'] = $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $warehouse_id));
	$cash_id = $this->Route->field('Route.account_head_id',array('Route.id ' =>$day['Route']['id']));

	$CustomerList=$this->AccountHead->find('all',array(
		"joins"=>array(
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'inner',
				"conditions"=>array('Customer.account_head_id=AccountHead.id'),
				),
			array(
				"table"=>'executive_route_mappings',
				"alias"=>'ExecutiveRouteMapping',
				"type"=>'inner',
				"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
				),
			),
		'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
		'fields'=>array(
			'Customer.code',
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			),
		));
	$receipt_visit = 0;
	$All_Customer=[];
	if($CustomerList)
	{
		$j =0;
		$no_sale_count =0;
		$max_sale = 0;
		$max_credit_sale = 0;
		$max_cash_sale = 0;
		$max_receipt_sale = 0;
		$max_balance =0;
		$current_market_outstanding =0;
		foreach ($CustomerList as $key => $value) {
			$total_sale =0;
			$Single_Customer['id']=$value['AccountHead']['id'];
			$Single_Customer['name']=$value['AccountHead']['name'];
			$Single_Customer['shope_code']=$value['Customer']['code'];
			$Single_Customer['sort'] = 1;
			$Sale=$this->Sale->find('all',array(
				'conditions'=>array(
					'Sale.account_head_id'=>$value['AccountHead']['id'],
					'Sale.executive_id'=>$executive,
// 'Sale.date_of_delivered between ? and ?'=>[$register,$closed],
					'Sale.day_register_id'=>$day_register_id,
					'Sale.status'=>2,
					),
				'fields'=>array(
					'Sale.id',
					'Sale.invoice_no',
					'Sale.grand_total',
					'Sale.date_of_delivered',
					'Sale.created_at',
					),
				));

			$All_sale=[];
			$cash_sale_total =0;
			$collected =0;
			foreach ($Sale as $key => $row) {
				$total_sale+=$row['Sale']['grand_total'];
				$remarks='Sale Invoice No :'. $row['Sale']['invoice_no'];
				$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
				$Journal_cash_sale=$this->Journal->find('first',array(
					'conditions'=>array(
						'credit'=>$value['AccountHead']['id'],
						'debit'=>$cash_id,
						'flag=1',
						'day_register_id'=>$day_register_id,
						'remarks'=>$remarks,
						'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
//'date'=>$row['Sale']['date_of_delivered'],
						),
					'fields'=>array('Journal.total_amount'),
					));
				$cash_sale_total+=$Journal_cash_sale['Journal']['total_amount'];
			}
			$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)");
			$Journal_credit=$this->Journal->find('first',array(
				'conditions'=>array(
					'credit'=>$value['AccountHead']['id'],
					'flag=1',
					'executive_id'=>$executive,
					'day_register_id'=>$day_register_id,
					'debit'=>$cash_id,
					'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
// 'date between ? and ?'=>[$register,$closed],
					),
				'fields'=>array('Journal.total_amount'),
				));
//pr($Journal_credit);
			$Journal_date=$this->Journal->find('all',array(
				'conditions'=>array(
					'credit'=>$value['AccountHead']['id'],
					'flag=1',
					'executive_id'=>$executive,
					'day_register_id'=>$day_register_id,
					'debit'=>$cash_id,
					'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
// 'date between ? and ?'=>[$register,$closed],
					),
				'fields'=>array('Journal.created_at'),
				'order' => array('Journal.id DESC'),
				));
			$Single_Customer['time']="";
			if($Sale){
				$time = new DateTime($Sale[0]['Sale']['created_at']);
				$time = $time->format('H:i');
				$Single_Customer['time']=$time;
			}
			else{
				if($Journal_date){
					$time = new DateTime($Journal_date[0]['Journal']['created_at']);
					$time = $time->format('H:i');
					$Single_Customer['time']=$time;
				}
			}
			$receipt_flag = 0;
			$collected+=$Journal_credit['Journal']['total_amount'];
			$credit_sale = $total_sale - $cash_sale_total;
			if($credit_sale<0) $credit_sale = 0.00;
			if($collected<0) $collected = 0.00;
			$collected-=$cash_sale_total;
			$max_sale+=$total_sale;
			$max_credit_sale+=$credit_sale;
			$max_cash_sale+=$cash_sale_total;
			$max_receipt_sale+=$collected;
			$Single_Customer['receipt'] = number_format($collected);
			$Single_Customer['cash_sale'] = $cash_sale_total;
			$Single_Customer['credit_sale'] = $credit_sale;
			$No_flag = 0;
			if($total_sale==0 && $collected==0){
				$NoSale=$this->NoSale->find('all',array(
					'conditions'=>array(
						'NoSale.customer_account_head_id'=>$value['AccountHead']['id'],
						'NoSale.executive_id'=>$executive,
// 'NoSale.date between ? and ?'=>[$register,$closed],
						'NoSale.day_register_id'=>$day_register_id,
						),
					'fields'=>array(
						'NoSale.id',
						'NoSale.description',
						'NoSale.created_at',
						),
					));
				foreach ($NoSale as $key => $row) {
					$No_flag = 1;
					$no_sale_count++;
					$Single_Customer['sort'] = 0;

					$Single_Customer['credit_sale'] = 'No Sale';
					$Single_Customer['receipt'] = '';
					$Single_Customer['cash_sale'] = $row['NoSale']['description'];
					$Single_Customer['time']="";
					if($row['NoSale']['created_at']){
						$time = new DateTime($row['NoSale']['created_at']);
						$time = $time->format('H:i');
						$Single_Customer['time']=$time;
					}
				}
			}
			if($total_sale==0 && $collected!=0 && $No_flag==0){
				$receipt_visit++;
			}
			$Debit_N_Credit_function=$this->General_Journal_Debit_Credit_function($value['AccountHead']['id']);
			if($total_sale!=0 || $collected!=0 ||$No_flag!=0)
			{
				$Single_Customer['balance']=$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
				$max_balance+=$Single_Customer['balance'];
				array_push($All_Customer, $Single_Customer);
			}
			$single_market_outstanding =$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
			$current_market_outstanding =$current_market_outstanding + $single_market_outstanding;
		}
	}
//pr($All_Customer);
	array_multisort( array_column($All_Customer, "sort"), SORT_DESC, $All_Customer );
	$content['All_Customer'] = $All_Customer;
	$postive_visit_sale=$this->Sale->find('count',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
// 'Sale.date_of_delivered between ? and ?'=>[$register,$closed],
			'Sale.day_register_id'=>$day_register_id,
			'Sale.is_erp'=>0
			),
		'group' => array('Sale.account_head_id'),
		));
	$footer_content['visit'] = count($All_Customer);
	$visit = count($All_Customer);
	$postive_visit = $postive_visit_sale + $receipt_visit;
	$footer_content['postive_visit'] = $postive_visit;
	$strike_rate =0;
	if($visit) $strike_rate = ($postive_visit/$visit)*100;
	$footer_content['strike_rate'] = number_format((float)$strike_rate, 2, '.', '');
	$footer_content['max_balance'] =number_format((float)$max_balance, 2, '.', '');
	$footer_content['max_credit_sale'] =number_format((float)$max_credit_sale, 2, '.', '');
	$footer_content['max_cash_sale'] = number_format((float)$max_cash_sale, 2, '.', '');
	$footer_content['max_receipt_sale'] = number_format((float)$max_receipt_sale, 2, '.', '');
	$footer_content['max_sale'] = number_format((float)$max_sale, 2, '.', '');
	$footer_content['current_market_outstanding'] = number_format((float)$current_market_outstanding, 2, '.', '');
	$Month_sale_amount =0;
	$Month_sale_return_amount =0;
	$MonthSale_list=$this->Sale->find('all',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>$month,
			'Sale.day_register_id'=>$day_register_id,
			'Sale.status'=>2
			),
		'fields'=>array(
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			),
		));
	$Monthly_cash_sale = 0;
	foreach ($MonthSale_list as $key => $value) {
		$remarks='Sale Invoice No :'. $value['Sale']['invoice_no'];
		$cash_sale_row = $this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$cash_id,
				'flag=1',
				'day_register_id'=>$day_register_id,
				'remarks'=>$remarks,
				'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
				'date'=>$value['Sale']['date_of_delivered'],
				),
			'fields'=>array('Journal.amount',),
			));
		if(!empty($cash_sale_row)){
			$Monthly_cash_sale = $Monthly_cash_sale + $cash_sale_row['Journal']['amount'];
		}
	}
//pr($month);
	$MonthSale=$this->Sale->find('first',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>$month,
//'Sale.day_register_id'=>$day_register_id,
			'Sale.status'=>2
			),
		'fields'=>array(
			'sum(Sale.grand_total) as monthly_sale',
			),
		));


	$Customers=$this->Customer->find('all',array(
		'conditions'=>array('Customer.route_id'=>$route_id),
		'fields'=>[
		'Customer.*',
		]
		));
	$list=[];
	foreach ($Customers as $keyC => $valueC) {
		$this->SalesReturn->unbindModel(array('hasMany' => array('SalesReturnItem')));
		$this->SalesReturn->virtualFields = array('monthly_sale_return' => "SUM(SalesReturn.grand_total)");
		$MonthSaleReturn=$this->SalesReturn->find('first',array(
			'conditions'=>array(
				'SalesReturn.account_head_id'=>$valueC['Customer']['account_head_id'],
				'month(SalesReturn.date)'=>$month,
				'SalesReturn.status'=>2
				),
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
				'monthly_sale_return'
				)
			));
		if($MonthSaleReturn['SalesReturn']['monthly_sale_return'])
		{
			$list[]=$MonthSaleReturn;	
		}
	}
//pr($list);
//exit;
	if($MonthSale){
		$Month_sale_amount =$MonthSale[0]['monthly_sale'];
	}
	if($MonthSaleReturn){
		foreach ($list as $keyl => $valuel) {

			$Month_sale_return_amount +=-($valuel['SalesReturn']['monthly_sale_return']);
		}
//$Month_sale_return_amount =-($MonthSaleReturn['monthly_sale_return']);
	}
	$footer_content['Month_sale_amount'] = number_format((float)$Month_sale_amount, 2, '.', '');
	$footer_content['Month_sale_return_amount'] = number_format((float)$Month_sale_return_amount, 2, '.', '');
	$cash_id = $this->Route->field('Route.account_head_id',array('Route.id ' =>$day['Route']['id']));

	$Month_collection_amount =0;
	$MonthCollection=$this->Journal->find('first',array(
		'conditions'=>array(
			'debit'=>$cash_id,
			'flag=1',
//'day_register_id'=>$day_register_id,
			'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
			'month(date)'=>$month,
			),
		'fields'=>array(
			'sum(Journal.amount) as monthly_amount',
			),
		));

	if(!empty($MonthCollection)){
		$Month_collection_amount =$MonthCollection[0]['monthly_amount'];
	}
	$footer_content['Month_collection_amount'] = number_format((float)$Month_collection_amount, 2, '.', '');
	require('fpdf/fpdf.php');
	$pdf = new FPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	define('FPDF_FONTPATH','fpdf/font');
	function header_section($pdf,$Profile,$content) {
		$pdf->SetFont('Arial','B',8.4);
		$pdf->AddPage();
		$pdf->Image('profile/'.$Profile['logo'],1,-5,-650);
		$pdf->SetFont('Arial','B',20);
		$pdf->SetTextColor(0,100,0);
		$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(200,00,00);
		$pdf->SetTextColor(10,10,10);
		$pdf->SetFont('Arial','B',8);
		$head_table_x=28;
		$pdf->rect(150, $head_table_x-7, 55, 8);
		$pdf->Text(152, 25, "Route : ".$content['route_name']);
		$pdf->rect(15, $head_table_x+3.5, 120, 8);
		$pdf->SetFont('Arial','',11);
		$pdf->Text(18, 36.5, "Warehouse : ".$content['warehouse_name']);
		$pdf->SetFont('Arial','B',12);
		$pdf->Text(80, 29.5, "DAILY SALES REPORT");
		$pdf->rect(150, $head_table_x+3, 55, 8);
		$pdf->SetFont('Arial','B',9);
		$pdf->Text(152, 36.5, "Date : ".date('d-M-Y',strtotime($content['display_date'])));
		$first_table_x=$head_table_x+13;
		$pdf->rect(15, $first_table_x+3, 190, 222);
		$slno_x=16;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($slno_x, $first_table_x+5+5, "SlNo");
		$pdf->Line($slno_x+7, $first_table_x+3,$slno_x+7, 200);
		$item_x=$slno_x+34;
		$item_line_x=$item_x+100;
		$pdf->SetFont('Arial','B',7);
		$pdf->Line($slno_x+18, $first_table_x+3,$slno_x+18, 200);
		$pdf->Line($slno_x+33, $first_table_x+3,$slno_x+33, 200);
		$pdf->Text($slno_x+10, $first_table_x+5+5, "Time");
		$pdf->Text($slno_x+20, $first_table_x+5+5, "Code");
		$pdf->Text($item_x+15, $first_table_x+10, "Customer");
		$tax_line_x=$item_x+35;
		$tax_x=$item_x+35;
		$pdf->Line($tax_line_x+33, $first_table_x+3,$tax_line_x+33, 200);
		$pdf->Text($tax_x, $first_table_x+5, "");
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($tax_x+35, $first_table_x+5+5, "Credit Sale");
		$tax_line_x=$tax_x+35;
		$pdf->Line($tax_line_x+15, $first_table_x+3,$tax_line_x+15, 200);
		$unit_x=$tax_x+35;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($unit_x+17, $first_table_x+5+5, "Cash Sale");
		$pdf->Line($item_line_x+10, $first_table_x+3,$item_line_x+10, 200);
		$qty_x=$item_line_x+4;
		$qty_line_x=$qty_x+8;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($qty_x+9, $first_table_x+5+5, "Receipt");
		$unit_price_x=$qty_line_x+2;
		$unit_price_line_x=$unit_price_x+15;
		$pdf->SetFont('Arial','B',7);
		$net_amount_x=$qty_line_x+2;
		$net_amount_line_x=$net_amount_x+16;
		$pdf->Line($net_amount_line_x-2, $first_table_x+3,$net_amount_line_x-2, 200);
		$total_x=$unit_price_line_x+2;
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($total_x, $first_table_x+5+5, "O/S Balance");
		$pdf->Line(15, $first_table_x+12, 205, $first_table_x+12);
		$pdf->Line(15, 200, 205, 200);
		$pdf->Line(15, 205, 205, 205);
		$i=0;
	}
	function footer($pdf,$footer_content)
	{
		$footer_line=203;
		$f=0;
		$pdf->Line(178, 205,178, 215);
		$pdf->Text(190, 211, $footer_content['max_balance']);
		$pdf->Line(160, 205,160, 215);
		$pdf->Text(165, 211, $footer_content['max_receipt_sale']);
		$pdf->Line(135, 205,135, 215);
		$pdf->Text(140, 211, $footer_content['max_cash_sale']);
		$pdf->Line(118, 205,118, 215);
		$pdf->Text(121, 211, $footer_content['max_credit_sale']);
		$pdf->Text(100, 211, 'Total :');
		$pdf->Line(15, 215, 205, 215);
		$pdf->Line(185, 215,185, 235);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 220, $footer_content['day_time']);
		$pdf->Text(135, 220, 'Day Register Time');
		if (isset($footer_content['close_time'])){
			$pdf->Text(190, 225, $footer_content['close_time']);
		}
		else{
			$pdf->Text(190, 225, $f);
		}
		$pdf->Text(135, 225, 'Day Close Time');
		$pdf->Text(190, 230, $footer_content['day_km']);
		$pdf->Text(135, 230, 'Day Register KM');
		if (isset($footer_content['close_km'])){
			$pdf->Text(190, 235, $footer_content['close_km']);
		}
		else{
			$pdf->Text(190, 235, $f);
		}
		$pdf->Text(135, 235, 'Day Close KM');
		if (isset($footer_content['close_km'])){
			$pdf->Text(190, 240, $footer_content['close_km']-$footer_content['day_km']);
		}
		else{
			$pdf->Text(190, 240, $f-$footer_content['day_km']);
		}
		$pdf->Text(135, 240, 'Travelled KM');
		$pdf->Line(105, 215,105, 235);
		$pdf->Line(75, 215,75, 235);
		$pdf->Text(80, 219, $footer_content['initial_stock']);
		$pdf->Text(20, 219, 'Opening Stock ');
		$pdf->Text(80, 225, $footer_content['loading_stock']);
		$pdf->Text(20, 225, 'Today Loading ');
		if (isset($footer_content['closing_stock'])){
			$pdf->Text(80, 231, $footer_content['closing_stock']);
		}
		else{
			$pdf->Text(80, 231, $f);
		}
		$pdf->Text(20, 231, 'Closing Stock ');
		$pdf->Text(80, 237, $footer_content['current_market_outstanding']);
		$pdf->Text(20, 237, 'Current Market Outstanding ');
		$pdf->Line(15, 242, 205, 242);
		$totalSale = ($footer_content['max_cash_sale'] + $footer_content['max_credit_sale']);
		$today_collection = ($footer_content['max_cash_sale'] + $footer_content['max_receipt_sale']);
		$pdf->Line(185, 235,185, 265);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 246, $footer_content['Month_sale_amount']);
		$pdf->Text(135, 246, 'Monthly Sale');
		$pdf->Text(190, 250, $footer_content['Month_sale_return_amount']);
		$pdf->Text(135, 250, 'Monthly Sale Return');
		$pdf->Text(190, 254, $footer_content['Month_collection_amount']);
		$pdf->Text(135, 254, 'Monthly Collection');
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 258.5, number_format((float)$totalSale,2));
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(135, 258.5, 'Today Sale');
		$pdf->SetFont('Arial','',8);
		$pdf->Text(190, 262.5, number_format((float)$today_collection,2));
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(135, 262.5, 'Today Collection');
		$pdf->Line(105, 235,105, 265);
		$pdf->Line(75, 235,75, 265);
		$pdf->SetFont('Arial','',8);
		$pdf->Text(80, 249, $footer_content['visit']);
		$pdf->Text(20, 249, 'Total Visit');
		$pdf->Text(80, 255, $footer_content['postive_visit']);
		$pdf->Text(20, 255, 'Positive Visit');
		$pdf->Text(80, 261, $footer_content['strike_rate'].'%');
		$pdf->Text(20, 261, 'Strike Rate');
		$pdf->SetFont('Arial','B',8);
		$pdf->SetFont('Arial','B',8);
		$pdf->Text(20, 295, "Salesman");
		$pdf->Text(60, 295, "Supervisor");
		$pdf->Text(100, 295, "Accountant");
		$pdf->Text(140, 295, "Manager ");
	}
	$slno =1;
	header_section($pdf,$Profile,$content);
	$i=0;
	$head_table_x=10;
	$first_table_x=$head_table_x+36;
	$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; 
	$time  = array_column($content['All_Customer'], 'time');
	array_multisort($time, SORT_ASC,$content['All_Customer']);

	foreach($content['All_Customer'] as $key=>$value) {
		$sl_no_v_x=18;
		$pdf->Text($sl_no_v_x, $first_table_x+11+(5*$i), $slno);
		$product_name_v_x=$sl_no_v_x+10;
		$pdf->Text($product_name_v_x-3, $first_table_x+11+(5*$i), $value['time']);
		$pdf->Text($product_name_v_x+8, $first_table_x+11+(5*$i), $value['shope_code']);
		$pdf->Text($product_name_v_x+23, $first_table_x+11+(5*$i), $value['name']);
		$tax_v_x=$product_name_v_x+65;
		$pdf->Text($tax_v_x+28, $first_table_x+11+(5*$i), $value['credit_sale']);
		$unit_v_x=$tax_v_x+35;
		$pdf->Text($unit_v_x+10, $first_table_x+11+(5*$i), $value['cash_sale']);
		$quantity_v_x=$product_name_v_x+125;
		$pdf->Text($quantity_v_x+10, $first_table_x+11+(5*$i),$value['receipt']);
		$unit_price_v_x=$quantity_v_x+12;
		$total_v_x=$unit_price_v_x+18;
		if($value['balance']=='-0.00'){
			$value['balance'] ='0.00';
		}
		$pdf->Text($total_v_x, $first_table_x+11+(5*$i),number_format($value['balance']));
		$i++;
		$slno++;
		if($i>28)
		{
			$i=0;
			header_section($pdf,$Profile,$content);
		}
	}
	footer($pdf,$footer_content);
	$pdf->Output();
	exit;
}
public function DayRegisterPrintOld()
{
	$this->request->data['Reports']['from']=date('d-m-Y');
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));
}
public function dcrpdf_old($executive,$date)
{
	$date = date('Y-m-d',strtotime($date));
	$date_build = date_parse_from_format("Y-m-d", $date);
	$month = $date_build["month"];
	$content['display_date'] = date('d-m-Y',strtotime($date));
	$footer_content['initial_stock']=0;
	$footer_content['loading_stock']=0;
	$content['route_name'] = '';
	$day = $this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' =>$date)));
	if($day)
	{
		$footer_content['initial_stock'] = $day['DayRegister']['initial_stock'];
		$footer_content['loading_stock'] = $day['DayRegister']['loading_stock'];	
		$content['route_name'] = $day['Route']['name'];
	}
	$closing_stock = 0;
	$close = $this->ClosingDay->find('first', array('conditions' => array('ClosingDay.executive_id' => $executive,'ClosingDay.date' =>$date)));
	if(!empty($close)){
		$closing_stock = $close['ClosingDay']['closing_stock'];
	}
	$footer_content['closing_stock'] =$closing_stock;
	$warehouse_id = $this->Executive->field(
		'Executive.warehouse_id',
		array('Executive.id ' => $executive));
	$content['warehouse_name'] = $this->Warehouse->field(
		'Warehouse.name',
		array('Warehouse.id ' => $warehouse_id));
	$cash_id = $this->Executive->field(
		'Executive.account_head_id',
		array('Executive.id ' => $executive));
	$CustomerList=$this->AccountHead->find('all',array(
		"joins"=>array(
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'inner',
				"conditions"=>array('Customer.account_head_id=AccountHead.id'),
				),
			array(
				"table"=>'executive_route_mappings',
				"alias"=>'ExecutiveRouteMapping',
				"type"=>'inner',
				"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
				),
			),
		'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
		'fields'=>array(
			'Customer.*',
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			),
		));
	$receipt_visit = 0;
	$All_Customer=[];
	if($CustomerList)
	{
		$j =0;
		$no_sale_count =0;
		$max_sale = 0;
		$max_credit_sale = 0;
		$max_cash_sale = 0;
		$max_receipt_sale = 0;
		$max_balance =0;
		$current_market_outstanding =0;
		foreach ($CustomerList as $key => $value) {
			$total_sale =0;
			$Single_Customer['id']=$value['AccountHead']['id'];
			$Single_Customer['name']=$value['AccountHead']['name'];
			$Single_Customer['shope_code']=$value['Customer']['code'];
			$Single_Customer['sort'] = 1;
			$Sale=$this->Sale->find('all',array(
				'conditions'=>array(
					'Sale.account_head_id'=>$value['AccountHead']['id'],
					'Sale.executive_id'=>$executive,
					'Sale.date_of_delivered'=>$date,
					'Sale.status'=>2,
//'Sale.is_erp'=>0
					),
				'fields'=>array(
					'Sale.id',
					'Sale.invoice_no',
					'Sale.grand_total',
					),
				));
			$All_sale=[];
			$cash_sale_total =0;
			$collected =0;
			foreach ($Sale as $key => $row) {
				$total_sale = $total_sale + $row['Sale']['grand_total'];
				$remarks='Sale Invoice No :'. $row['Sale']['invoice_no'];
				$Journal_cash_sale=$this->Journal->find('all',array('conditions'=>array(
					'credit'=>$value['AccountHead']['id'],
					'debit'=>$cash_id,
					'flag=1',
					'remarks'=>$remarks,
					'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
					'date'=>$date
					)));
				foreach ($Journal_cash_sale as $key => $row) {
					if(!empty($row['Journal']['amount'])){
						$cash_sale_total = $cash_sale_total + $row['Journal']['amount'];
					}
				}
			}
			$Journal_credit=$this->Journal->find('all',array(
				'conditions'=>array(
					'credit'=>$value['AccountHead']['id'],
					'flag=1',
					'debit'=>$cash_id,
					'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
					'date'=>$date
					),
				'fields'=>array('Journal.amount'),
				));
			$receipt_flag = 0;
			foreach ($Journal_credit as $key => $row) {
				if(!empty($row['Journal']['amount'])){
					$collected = $collected + $row['Journal']['amount'];
					if($collected <0){
						$collected = 0.00;
					}
				}
			}
			$collected = $collected - $cash_sale_total;
			$max_sale = $max_sale + $total_sale;
			$credit_sale = $total_sale - $cash_sale_total;
			$max_credit_sale = $max_credit_sale + $credit_sale;
			$max_cash_sale = $max_cash_sale + $cash_sale_total;
			$max_receipt_sale = $max_receipt_sale + $collected;
			$Single_Customer['receipt'] = $collected;
			$Single_Customer['cash_sale'] = $cash_sale_total;
			$Single_Customer['credit_sale'] = $credit_sale;
			$No_flag = 0;
			if($total_sale==0 && $collected==0){
				$NoSale=$this->NoSale->find('all',array(
					'conditions'=>array(
						'NoSale.customer_account_head_id'=>$value['AccountHead']['id'],
						'NoSale.executive_id'=>$executive,
						'NoSale.date'=>$date,
						),
					'fields'=>array(
						'NoSale.id',
						'NoSale.description',
						),
					));
				foreach ($NoSale as $key => $row) {
					$No_flag = 1;
					$no_sale_count++;
					$Single_Customer['sort'] = 0;
					$Single_Customer['credit_sale'] = 'No Sale';
					$Single_Customer['receipt'] = '';
					$Single_Customer['cash_sale'] = $row['NoSale']['description'];
				}
			}
			if($total_sale==0 && $collected!=0 && $No_flag==0){
				$receipt_visit++;
			}
			$Debit_N_Credit_function=$this->General_Journal_Debit_Credit_function($value['AccountHead']['id']);
			if($total_sale!=0 || $collected!=0 ||$No_flag!=0)
			{
				$Single_Customer['balance']=$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
				$max_balance = $max_balance + $Single_Customer['balance'];
				$j++;
				$Single_Customer['sl_no'] = $j;
				array_push($All_Customer, $Single_Customer);
			}
			$single_market_outstanding =$value['AccountHead']['opening_balance']+($Debit_N_Credit_function['debit'] - $Debit_N_Credit_function['credit']);
			$current_market_outstanding =$current_market_outstanding + $single_market_outstanding;
		}
	}
	array_multisort( array_column($All_Customer, "sort"), SORT_DESC, $All_Customer );
	$k=0;
	foreach ($All_Customer as $key => $Customer) {
		$k++;
		$All_Customer[$key]['sl_no'] = $k;
	}
	$content['All_Customer'] = $All_Customer;
	$postive_visit_sale=$this->Sale->find('count',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'Sale.date_of_delivered'=>$date,
			'Sale.is_erp'=>0
			),
		'group' => array('Sale.account_head_id'),
		));
	$footer_content['visit'] = $k;
	$visit = $k;
	$postive_visit = $postive_visit_sale + $receipt_visit;
	$footer_content['postive_visit'] = $postive_visit;
	if($visit!=0){
		$strike_rate = ($postive_visit/$visit)*100;
	}
	else
	{
		$strike_rate =0;
	}
	$footer_content['strike_rate'] = number_format((float)$strike_rate, 2, '.', '');
	$footer_content['max_balance'] =number_format((float)$max_balance, 2, '.', '');
	$footer_content['max_credit_sale'] =number_format((float)$max_credit_sale, 2, '.', '');
	$footer_content['max_cash_sale'] = number_format((float)$max_cash_sale, 2, '.', '');
	$footer_content['max_receipt_sale'] = number_format((float)$max_receipt_sale, 2, '.', '');
	$footer_content['max_sale'] = number_format((float)$max_sale, 2, '.', '');
	$footer_content['current_market_outstanding'] = number_format((float)$current_market_outstanding, 2, '.', '');
	$Month_sale_amount =0;
	$MonthSale_list=$this->Sale->find('all',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>1,
			'Sale.status'=>2
			),
		'fields'=>array(
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			),
		));
	$Monthly_cash_sale = 0;
	foreach ($MonthSale_list as $key => $value) {
		$remarks='Sale Invoice No :'. $value['Sale']['invoice_no'];
		$cash_sale_row = $this->Journal->find('first',array('conditions'=>array(
			'debit'=>$cash_id,
			'flag=1',
			'remarks'=>$remarks,
			'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
			'date'=>$value['Sale']['date_of_delivered'],
			),
		'fields'=>array(
			'(Journal.amount)',
			),
		));
		if(!empty($cash_sale_row)){
			$Monthly_cash_sale = $Monthly_cash_sale + $cash_sale_row['Journal']['amount'];
		}
	}
	$MonthSale=$this->Sale->find('first',array(
		'conditions'=>array(
			'Sale.executive_id'=>$executive,
			'month(Sale.date_of_delivered)'=>$month,
			'Sale.status'=>2
			),
		'fields'=>array(
			'sum(Sale.grand_total) as monthly_sale',
			),
		));
	if(!empty($MonthSale)){
		$Month_sale_amount =$MonthSale[0]['monthly_sale'];
	}
	$footer_content['Month_sale_amount'] = number_format((float)$Month_sale_amount, 2, '.', '');
	$cash_id = $this->Executive->field(
		'Executive.account_head_id',
		array('Executive.id ' => $executive));
	$Month_collection_amount =0;
	$MonthCollection=$this->Journal->find('first',array('conditions'=>array(
		'debit'=>$cash_id,
		'flag=1',
		'work_flow IN'=>array('From Mobile','From Executive app','Sales'),
		'month(date)'=>$month,
		),
	'fields'=>array(
		'sum(Journal.amount) as monthly_amount',
		),
	));
	if(!empty($MonthCollection)){
		$Month_collection_amount =$MonthCollection[0]['monthly_amount'];
	}
	$footer_content['Month_collection_amount'] = number_format((float)$Month_collection_amount, 2, '.', '');
	require('tfpdf/tfpdf.php');
	$pdf = new tFPDF('p', 'mm', [297, 210]);
	$Profile=$this->Global_Var_Profile['Profile'];
	define('FPDF_FONTPATH','tfpdf/font');
	function header_section($pdf,$Profile,$content) {
		$pdf->SetFont('Arial','B',8.4);
		$pdf->AddPage();
		$pdf->Image('profile/'.$Profile['logo'],-2,-5,-250);
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
		$pdf->SetFont('Arial','B',20);
		$pdf->SetTextColor(0,100,0);
		$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(200,00,00);
		$pdf->SetTextColor(10,10,10);
		$pdf->SetFont('Arial','B',8);
		$head_table_x=28;
		$pdf->rect(150, $head_table_x-10, 55, 8);
		$pdf->Text(152, 21, "Route : ".$content['route_name']);
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
		$pdf->rect(15, $head_table_x, 120, 8);
		$pdf->SetFont('Arial','',11);
		$pdf->Text(18, 33, "Warehouse : ".$content['warehouse_name']);
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',10);
		$pdf->SetFont('Arial','B',12);
		$pdf->Text(80, 26, "DAILY SALES REPORT");
		$pdf->rect(150, $head_table_x, 55, 8);
		$pdf->SetFont('Arial','B',9);
		$pdf->Text(152, 33, "Date : ".date('d-M-Y',strtotime($content['display_date'])));
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',13);
		$first_table_x=$head_table_x+13;
		$pdf->rect(15, $first_table_x, 190, 215);
		$slno_x=16;
		$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
		$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
		$pdf->SetFont('DejaVu','',8);
		$pdf->SetFont('Arial','B',7);
		$pdf->Text($slno_x, $first_table_x+5+5, "SlNo");
$pdf->Line($slno_x+7, $first_table_x,$slno_x+7, 200); // vertical line
$item_x=$slno_x+34;
$item_line_x=$item_x+100;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->SetFont('Arial','B',7);
$pdf->Text($item_x, $first_table_x+10, "Customer");
$tax_line_x=$item_x+35;
$tax_x=$item_x+35;
$pdf->Line($tax_line_x, $first_table_x,$tax_line_x, 200); // vertical line
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Text($tax_x, $first_table_x+5, "");
$pdf->SetFont('Arial','B',7);
$pdf->Text($tax_x+5, $first_table_x+5+5, "Credit Sale");
$tax_line_x=$tax_x+35;
$pdf->Line($tax_line_x, $first_table_x,$tax_line_x, 200); // vertical line
$unit_x=$tax_x+35;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->SetFont('Arial','B',7);
$pdf->Text($unit_x+5, $first_table_x+5+5, "Cash Sale");
$pdf->Line($item_line_x-3, $first_table_x,$item_line_x-3, 200); // vertical line
$qty_x=$item_line_x+4;
$qty_line_x=$qty_x+8;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->SetFont('Arial','B',7);
$pdf->Text($qty_x, $first_table_x+5+5, "Receipt");
$unit_price_x=$qty_line_x+2;
$unit_price_line_x=$unit_price_x+15;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->SetFont('Arial','B',7);
$net_amount_x=$qty_line_x+2;
$net_amount_line_x=$net_amount_x+16;
$pdf->Line($net_amount_line_x-4, $first_table_x,$net_amount_line_x-4, 200); // vertical line
$total_x=$unit_price_line_x+2;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->SetFont('Arial','B',7);
$pdf->Text($total_x, $first_table_x+5+5, "O/S Balance");
$pdf->Line(15, $first_table_x+12, 205, $first_table_x+12); // horizontal line
$pdf->Line(15, 200, 205, 200); // horizontal line
$pdf->Line(15, 205, 205, 205); // horizontal line
$i=0;
}
function footer($pdf,$footer_content)
{
	$footer_line=203;
$pdf->Line(177, 205,177, 215); // vertical line
$pdf->Text(190, 211, $footer_content['max_balance']);
$pdf->Line(148, 205,148, 215);
$pdf->Text(159, 211, $footer_content['max_receipt_sale']);
$pdf->Line(120, 205,120, 215);
$pdf->Text(131, 211, $footer_content['max_cash_sale']);
$pdf->Line(85, 205,85, 215);
$pdf->Text(93, 211, $footer_content['max_credit_sale']);
$pdf->Text(20, 211, 'Total :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
$pdf->Line(15, 215, 205, 215);
$pdf->Line(185, 215,185, 235); // vertical line
$pdf->SetFont('Arial','',8);
$pdf->Text(190, 220, $footer_content['Month_sale_amount']);
$pdf->Text(135, 220, 'Monthly Sale');
$pdf->Text(190, 227, $footer_content['Month_collection_amount']);
$pdf->Text(135, 227, 'Monthly Collection');
$pdf->Line(105, 215,105, 235);
$pdf->Line(75, 215,75, 235);
$pdf->Text(80, 219, $footer_content['initial_stock']);
$pdf->Text(20, 219, 'Opening Stock ');
$pdf->Text(80, 223, $footer_content['loading_stock']);
$pdf->Text(20, 223, 'Today Loading ');
$pdf->Text(80, 227, $footer_content['closing_stock']);
$pdf->Text(20, 227, 'Closing Stock ');
$pdf->Text(80, 231, $footer_content['current_market_outstanding']);
$pdf->Text(20, 231, 'Current Market Outstanding ');
$pdf->Line(15, 235, 205, 235);
$totalSale = ($footer_content['max_cash_sale'] + $footer_content['max_credit_sale']);
$today_collection = ($footer_content['max_cash_sale'] + $footer_content['max_receipt_sale']);
$pdf->Line(185, 235,185, 256); // vertical line
$pdf->Text(190, 241, number_format((float)$totalSale,2));
$pdf->SetFont('Arial','B',8);
$pdf->Text(135, 241, 'Today Sale');
$pdf->SetFont('Arial','',8);
$pdf->Text(190, 250, number_format((float)$today_collection,2));
$pdf->SetFont('Arial','B',8);
$pdf->Text(135, 250, 'Today Collection');
$pdf->Line(105, 235,105, 256);
$pdf->Line(75, 235,75, 256);
$pdf->SetFont('Arial','',8);
$pdf->Text(80, 240, $footer_content['visit']);
$pdf->Text(20, 240, 'Total Visit');
$pdf->Text(80, 245, $footer_content['postive_visit']);
$pdf->Text(20, 245, 'Positive Visit');
$pdf->Text(80, 250, $footer_content['strike_rate'].'%');
$pdf->Text(20, 250, 'Strike Rate');
$pdf->SetFont('Arial','B',8);
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',11);
$pdf->SetFont('Arial','B',8);
$pdf->Text(20, 295, "Salesman");
$pdf->Text(60, 295, "Supervisor");
$pdf->Text(100, 295, "Accountant");
$pdf->Text(140, 295, "Manager ");
}
$slno =1;
header_section($pdf,$Profile,$content);
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+36;
$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; 
foreach($content['All_Customer'] as $key=>$value) {
	$sl_no_v_x=18;
	$pdf->Text($sl_no_v_x, $first_table_x+11+(5*$i), $slno);
	$product_name_v_x=$sl_no_v_x+10;
	$pdf->Text($product_name_v_x, $first_table_x+11+(5*$i), $value['name']);
	$tax_v_x=$product_name_v_x+65;
	$pdf->Text($tax_v_x, $first_table_x+11+(5*$i), $value['credit_sale']);
	$unit_v_x=$tax_v_x+35;
	$pdf->Text($unit_v_x, $first_table_x+11+(5*$i), $value['cash_sale']);
	$quantity_v_x=$product_name_v_x+125;
	$pdf->Text($quantity_v_x, $first_table_x+11+(5*$i),$value['receipt'] );
	$unit_price_v_x=$quantity_v_x+12;
	$total_v_x=$unit_price_v_x+18;
	if($value['balance']=='-0.00'){
		$value['balance'] ='0.00';
	}
	$pdf->Text($total_v_x, $first_table_x+11+(5*$i),$value['balance'] );
	$i++;
	$slno++;
	if($i>31)
	{
		$i=0;
		header_section($pdf,$Profile,$content);
	}
}
footer($pdf,$footer_content);
$pdf->Output();
exit;
}

public function SaleCollectionReport()
{
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));
	$customers=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('acc_sub_group_id'=>2)));
	$this->set(compact('customers'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}

public function SaleCollectionReport_Ajax()
{
		$requestData=$this->request->data;
		$columns=[];
		$columns[]='Route.name';
		$columns[]='AccountHead.name';
		$columns[]='Sale.total';
		$columns[]='Sale.taxable_amount';
		$columns[]='Sale.tax_amount';
		$columns[]='Sale.discount_amount';
		$columns[]='Sale.grandtotal';
		$conditions=[];
			$conditions['Sale.date_of_delivered between ? and ?']=[
			date('Y-m-d',strtotime($requestData['from_date'])),
			date('Y-m-d',strtotime($requestData['to_date']))
			];
			if(!empty($requestData['executive_id']))
			{
			if($requestData['executive_id'])
			$conditions['Sale.executive_id']=$requestData['executive_id'];
			}
			if(!empty($requestData['executive_id']))
			{
			if($requestData['customer_id'])
			$conditions['Sale.account_head_id']=$requestData['customer_id'];
			}
		$totalData=$this->Sale->find('count',[
				'joins'=>array(
				array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.account_head_id=Sale.account_head_id')
				),
				array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'LEFT',
				'conditions'=>array('Customer.route_id=Route.id')
			   ),
				),
		'conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'Sale.date_of_delivered LIKE' =>'%'. $q . '%',
    		'Sale.invoice_no LIKE' =>'%'. $q . '%',
    		'Sale.grand_total LIKE' =>'%'. $q . '%',
    		'AccountHead.name LIKE' =>'%'. $q . '%',
    		'Route.name LIKE' =>'%'. $q . '%',
    		);
    	$totalFiltered=$this->Sale->find('count',[
				'joins'=>array(
				array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.account_head_id=Sale.account_head_id')
				),
				array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'LEFT',
				'conditions'=>array('Customer.route_id=Route.id')
			    ),
				),
    		'conditions'=>$conditions,
    		]);
    }
    $Data=$this->Sale->find('all',array(
			'joins'=>array(
			array(
			'table'=>'customers',
			'alias'=>'Customer',
			'type'=>'INNER',
			'conditions'=>array('Customer.account_head_id=Sale.account_head_id')
			),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'LEFT',
				'conditions'=>array('Customer.route_id=Route.id')
			),
			),
    	'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	'order'=>$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
    	'fields'=>array(
    		'Sale.*',
    		'AccountHead.name',
    		'Route.name',
    		'AccountHead.id',
    		)
    	));
    foreach ($Data as $key => $value) {
    	$Data[$key]['Sale']['date_of_delivered']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
    	$Data[$key]['Sale']['net_value']=number_format($value['Sale']['total'],2,'.','');
    	$Data[$key]['Sale']['tax_amount']=number_format($value['Sale']['tax_amount'],2,'.','');
    	$Data[$key]['Sale']['taxable_total']=number_format($value['Sale']['taxable_amount'],2,'.','');
    	$Data[$key]['Sale']['grand_total']=number_format($value['Sale']['grand_total'],2,'.','');
    	$Data[$key]['Sale']['discount_amount']=number_format($value['Sale']['discount_amount'],2,'.','');
    }
$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;

}
public function SalesReturnCollectionReport()
{
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));
	$customers=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('acc_sub_group_id'=>2)));
	$this->set(compact('customers'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}

public function SalesReturnCollectionReport_Ajax()
{
		$requestData=$this->request->data;
		$columns=[];
		$columns[]='Route.name';
		$columns[]='AccountHead.name';
		$columns[]='net_value';
		$columns[]='taxable_total';
		$columns[]='tax_amount';
		$columns[]='grand_total';
		$conditions=[];
		$conditions['SalesReturn.grand_total !=']=0;
			$conditions['SalesReturn.date between ? and ?']=[
			date('Y-m-d',strtotime($requestData['from_date'])),
			date('Y-m-d',strtotime($requestData['to_date']))
			];
			if(!empty($requestData['executive_id']))
			{
			if($requestData['executive_id'])
			$conditions['SalesReturn.executive_id']=$requestData['executive_id'];
			}
			if(!empty($requestData['executive_id']))
			{
			if($requestData['customer_id'])
			$conditions['SalesReturn.account_head_id']=$requestData['customer_id'];
			}
		$totalData=$this->SalesReturn->find('count',[
				'joins'=>array(
				array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.account_head_id=SalesReturn.account_head_id')
				),
				array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'LEFT',
				'conditions'=>array('Customer.route_id=Route.id')
			   ),
				),
		'conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'SalesReturn.date LIKE' =>'%'. $q . '%',
    		'SalesReturn.invoice_no LIKE' =>'%'. $q . '%',
    		'SalesReturn.grand_total LIKE' =>'%'. $q . '%',
    		'AccountHead.name LIKE' =>'%'. $q . '%',
    		'Route.name LIKE' =>'%'. $q . '%',
    		);
    	$totalFiltered=$this->SalesReturn->find('count',[
				'joins'=>array(
				array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.account_head_id=SalesReturn.account_head_id')
				),
				array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'LEFT',
				'conditions'=>array('Customer.route_id=Route.id')
			    ),
				),
    		'conditions'=>$conditions,
    		]);
    }
     //order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column'] >2)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
    $Data=$this->SalesReturn->find('all',array(
			'joins'=>array(
			array(
			'table'=>'customers',
			'alias'=>'Customer',
			'type'=>'INNER',
			'conditions'=>array('Customer.account_head_id=SalesReturn.account_head_id')
			),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'LEFT',
				'conditions'=>array('Customer.route_id=Route.id')
			),
			),
    	'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	'order'=>$order,
    	'fields'=>array(
    		'SalesReturn.*',
    		'AccountHead.name',
    		'Route.name',
    		'AccountHead.id',
    		)
    	));
      foreach ($Data as $key => $value) {
    	$conditionsItem['SalesReturn.id']=$value['SalesReturn']['id'];
					$this->SalesReturnItem->virtualFields = array( 
				     'total_net_value' => "SUM(SalesReturnItem.unit_price*SalesReturnItem.quantity)",
						'total_tax_amount' => "SUM(SalesReturnItem.tax_amount)",
						'total_taxable_amount' => "SUM(SalesReturnItem.net_value)",
						);
					$SalesReturnItem=$this->SalesReturnItem->find('first',[
						'conditions'=>$conditionsItem,
						'fields'=>array(
							'SalesReturnItem.total_tax_amount',
							'SalesReturnItem.total_net_value',
							'SalesReturnItem.total_taxable_amount'
							),
						]);
    	$Data[$key]['SalesReturn']['net_value']=number_format($SalesReturnItem['SalesReturnItem']['total_net_value'],2,'.','');
    	$Data[$key]['SalesReturn']['taxable_total']=number_format($SalesReturnItem['SalesReturnItem']['total_taxable_amount'],2,'.','');
    	$Data[$key]['SalesReturn']['grand_total']=number_format($value['SalesReturn']['grand_total'],2,'.','');
	    $Data[$key]['SalesReturn']['tax_amount']=number_format($SalesReturnItem['SalesReturnItem']['total_tax_amount'],2,'.','');
    }
    // //sorting total/balance/recieved
		if( $requestData['order'][0]['column'] >1) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["SalesReturn"][$columnname] == $b["SalesReturn"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["SalesReturn"][$columnname] < $b["SalesReturn"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["SalesReturn"][$columnname] > $b["SalesReturn"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;

}
public function get_executive_by_route_list($route_id=null)
{
	$conditions=[];
	if($route_id){$conditions['ExecutiveRouteMapping.route_id']=$route_id;} 
	$Executive=$this->Executive->find('list',array(
		"joins"=>array(
			array(
				"table"=>'executive_route_mappings',
				"alias"=>'ExecutiveRouteMapping',
				"type"=>'INNER',
				"conditions"=>array('ExecutiveRouteMapping.executive_id=Executive.id'),
				),

			),
		'conditions'=>$conditions,
		)
	);
	echo json_encode($Executive); exit;
}
public function get_executive_by_product_type($executive_id=null)
{
	$conditions=[];
	if($executive_id){$conditions['Sale.executive_id']=$executive_id;} 
	$Product=$this->ProductType->find('list',array(
		"joins"=>array(

			array(
				"table"=>'products',
				"alias"=>'Product',
				"type"=>'INNER',
				"conditions"=>array('Product.product_type_id=ProductType.id'),
				),
			array(
				"table"=>'sale_items',
				"alias"=>'SaleItem',
				"type"=>'INNER',
				"conditions"=>array('SaleItem.product_id=Product.id'),
				),
			array(
				"table"=>'sales',
				"alias"=>'Sale',
				"type"=>'INNER',
				"conditions"=>array('Sale.id=SaleItem.sale_id'),
				),
			),
		'conditions'=>$conditions,
		)
	);
	echo json_encode($Product); exit;
}
public function get_product_type_by_brand($product_type_id=null)
{
	$conditions=[];
	if($product_type_id){$conditions['Product.product_type_id']=$product_type_id;} 
	$Brand=$this->Brand->find('list',array(
		"joins"=>array(
			array(
				"table"=>'products',
				"alias"=>'Product',
				"type"=>'INNER',
				"conditions"=>array('Product.brand_id=Brand.id'),
				),
			array(
				"table"=>'sale_items',
				"alias"=>'SaleItem',
				"type"=>'INNER',
				"conditions"=>array('SaleItem.product_id=Product.id'),
				),
			array(
				"table"=>'sales',
				"alias"=>'Sale',
				"type"=>'INNER',
				"conditions"=>array('Sale.id=SaleItem.sale_id'),
				),
			),
		'conditions'=>$conditions,
		)
	);
	echo json_encode($Brand); exit;
}
public function get_product_by_brand_list($brand_id=null)
{
	$conditions=[];
	if($brand_id){$conditions['Product.brand_id']=$brand_id;} 
	$Product=$this->Product->find('list',array(
		"joins"=>array(
			array(
				"table"=>'sale_items',
				"alias"=>'SaleItem',
				"type"=>'INNER',
				"conditions"=>array('SaleItem.product_id=Product.id'),
				),
			array(
				"table"=>'sales',
				"alias"=>'Sale',
				"type"=>'INNER',
				"conditions"=>array('Sale.id=SaleItem.sale_id'),
				),
			),
		'conditions'=>$conditions,
		)
	);
	echo json_encode($Product); exit;
}
public function GetCustomerForSaleCollectionReport()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['executive_id'])
		$conditions['executive_id']=$data['executive_id'];

	$Customer=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'conditions'=>$conditions,
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
	echo json_encode($Customer); exit;
}
public function GetRouteListByExecutive()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['executive_id'])
		$conditions['executive_id']=$data['executive_id'];

	$Route=$this->ExecutiveRouteMapping->find('list',array(
		'joins'=>array(
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Route.id=ExecutiveRouteMapping.route_id')
				),
			),
		'conditions'=>$conditions,
		'fields'=>array('Route.id','Route.name',)
		));
	echo json_encode($Route); exit;
}
public function SaleCollectionReportAjax()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['executive_id'])
		$conditions['Sale.executive_id']=$data['executive_id'];

// if($data['customer_id'])
// 	$conditions['account_head_id']=$data['customer_id'];

	$Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
//'Customer.circle_id',
//'CustomerCircle.name',
			)
		));
//pr($Customer);
//pr($conditions);
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($data['to_date']));
	$list=[];
	foreach ($Customer as $key => $value) {
		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array('grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)");
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
				'Sale.grand_total',
				),
			));
		if($value['Executive']['name']){$single['executive']=$value['Executive']['name'];}else{$single['executive']="No Executive";}

		$single['name']=$value['AccountHead']['name'];
		$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;
		$single['other_value']=$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;
		$single['net_value']=$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
		$single['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
		$single['total']=$SaleItem['SaleItem']['grand_total']?floatval($SaleItem['SaleItem']['grand_total']):0;
		$single['grandtotal']=$single['total']-$single['dicount_amount'];
		if($single['net_value'])
			$list[$value['AccountHead']['id']]=$single;
	}
	echo json_encode($list);
	exit;
}
Public function CollectionReport(){
	$executive_list=$this->Executive->find('list',array('fields'=>array('id','name')));
	$this->set(compact('executive_list'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}
Public function CollectionReportExecutive(){
	$executive_list=$this->Executive->find('list',array('fields'=>array('id','name')));
	$this->set(compact('executive_list'));
	$route_list=$this->Route->find('list',array('fields'=>array('id','name')));
	$this->set(compact('route_list'));
	$branch_list=$this->Branch->find('list',array('fields'=>array('id','name')));
	$this->set(compact('branch_list'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}

public function get_collection_amount($account_head_id,$from_date=null,$to_date=null) {
	$conditions=array();
	if(!empty($from_date)){
		$conditions['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
	}
	$conditions['Journal.credit']=$account_head_id;
	$conditions['AccountHeadDebit.sub_group_id']=['1','2'];
	$conditions['Journal.flag']=1;
	$this->Journal->virtualFields = array( 'total_amount' => "SUM(Journal.amount)" );
	$Journal=$this->Journal->find('first',array('conditions'=>$conditions,
		'fields'=>array('Journal.total_amount','Journal.date')
		));
	$sale_total_amount=0;
	$collection_date=$Journal['Journal']['date'];
	$sale_total_amount=0;
	if($Journal['Journal']['total_amount'])
	{
		$sale_total_amount=floatval($Journal['Journal']['total_amount']);
	}
	$return['sale_amount']=$sale_total_amount;
	$return['date']=$collection_date;
	return $return;

}

public function collection_report_ajax()
{
	$conditions=array();
	$data=$this->request->data;
//pr($data);
//exit;
	$Customer=$this->Customer->find('all',
		array(
			'fields'=>array(
				'Customer.account_head_id',
				'AccountHead.name',
				),
			));
	$data['row']='';
	$data['tfoot']='';
	$from_date=date('Y-m-d',strtotime($data['from_date']));
	$to_date=date('Y-m-d',strtotime($data['to_date']));
	$data['result']='Error';
	if($Customer)
	{
		$grand_total=0;


		foreach ($Customer as $key => $value) 
		{
			$get_collection_amount_return_value=$this->get_collection_amount($value['Customer']['account_head_id'],$from_date,$to_date,2);		
			if($this->request->data['check_id'] !=0)
			{
				if($get_collection_amount_return_value['sale_amount']!=0)
				{
					$data['row']= $data['row'].'<tr>';
					$data['row'].='<td>'.date('d-m-Y',strtotime($get_collection_amount_return_value['date'])).'</td>';
					$data['row'].='<td>'.$value["AccountHead"]["name"].'</td>';
					$data['row'].='<td>'.$get_collection_amount_return_value['sale_amount'].'</td>';
					$data['row'].='</tr>';
					$grand_total+=$get_collection_amount_return_value['sale_amount'];
				}
			}
			else
			{
				if($get_collection_amount_return_value['date']){$get_date=date('d-m-Y',strtotime($get_collection_amount_return_value['date']));}else{$get_date=date('d-m-Y');}
				$data['row']= $data['row'].'<tr>';
				$data['row'].='<td>'.$get_date.'</td>';
				$data['row'].='<td>'.$value["AccountHead"]["name"].'</td>';
				$data['row'].='<td>'.$get_collection_amount_return_value['sale_amount'].'</td>';
				$data['row'].='</tr>';
				$grand_total+=$get_collection_amount_return_value['sale_amount'];
			}
		}	
		$data['tfoot']= $data['tfoot'].'<tr>';
		$data['tfoot']= $data['tfoot'].'<td colspan="2"><h3>Total</h3></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$grand_total.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'</tr>';
		$data['result']='Success';
	}
	echo json_encode($data); exit;
}

public function ExpenseReport()
{
	$this->request->data['from_date']=date('d-m-Y',strtotime('-1 day'));
	$this->request->data['to_date']=date('d-m-Y');
}
public function ExpenseReport_ajax()
{
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='SubGroup.name';
	$columns[]='AccountHead.name';
	$columns[]='AccountHead.id';
	$columns[]='AccountHead.id';
	$conditions=[];
	$priliminary_expense_written_off=18;
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions['SubGroup.group_id']=['12','13'];
	$purchase=5;
	$salereturn=6;
	$discount=8;
	$tax=7;
	$conditions['SubGroup.id !=']=[
	$purchase,
	$salereturn,
	$discount,
	$tax

	];
	$conditions['AccountHead.id !=']=[
	$priliminary_expense_written_off
	];
	$totalData=$this->AccountHead->find('count',[
		'joins'=>array(
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),
			),
		'conditions'=>$conditions]
		);
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'SubGroup.name LIKE' =>'%'. $q . '%',
			'AccountHead.name LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->AccountHead->find('count',[
			'conditions'=>$conditions,
			]);
	}
	$Data=$this->AccountHead->find('all',array(
		'joins'=>array(
			array(
				'table'=>'groups',
				'alias'=>'Group',
				'type'=>'INNER',
				'conditions'=>array('Group.id=SubGroup.group_id')
				),),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.opening_balance',
			'SubGroup.name',
			'SubGroup.id',
			'AccountHead.sub_group_id',
			)
		));
	foreach ($Data as $key => $value) {
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Total_expense=$this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$value['AccountHead']['id'],
				'flag=1',
				'Journal.date between ? and ?'=>array($from_date,$to_date),
				),
			'fields'=>array( 'Journal.total_amount')
			));

		$Total_expense_piad=$this->Journal->find('first',array(
			'joins'=>array(
				array(
					'table'=>'sub_groups',
					'alias'=>'SubGroup',
					'type'=>'INNER',
					'conditions'=>array('SubGroup.id=AccountHeadCredit.sub_group_id')
					),
				),
			'conditions'=>array(
				'SubGroup.id'=>array('1','2'),
				'debit'=>$value['AccountHead']['id'],
				'flag=1',
				'Journal.date between ? and ?'=>array($from_date,$to_date),
				),
			'fields'=>array( 'Journal.total_amount')
			));
		$accounthead=explode(" ",$value['AccountHead']['name']);
		$c=count($accounthead);
		$c1=$c-1;
		$a=$accounthead[$c1];
//	pr($a);exit;
		if($a=="OUTSTANDING"){
			$Total_expense['Journal']['total_amount']+=$value['AccountHead']['opening_balance'];
		}
		if($a=="PAID"){
			$Total_expense['Journal']['total_amount']+=$value['AccountHead']['opening_balance'];
			$Total_expense_piad['Journal']['total_amount']+=$value['AccountHead']['opening_balance'];

		}
		if($a=="PREPAID"){
			$Total_expense_piad['Journal']['total_amount']+=$value['AccountHead']['opening_balance'];
		}
		if($this->request->data['check_id'])
		{
			if($Total_expense['Journal']['total_amount'])
			{
				$Data[$key]['AccountHead']['paid']=floatval($Total_expense_piad['Journal']['total_amount']);
				$Data[$key]['AccountHead']['exepense']=floatval($Total_expense['Journal']['total_amount']);
			}
			else
			{
				unset($Data[$key]);
			}
		}
		else
		{
			$Data[$key]['AccountHead']['paid']=floatval($Total_expense_piad['Journal']['total_amount']);
			$Data[$key]['AccountHead']['exepense']=floatval($Total_expense['Journal']['total_amount']);
		}
	}
	$Data = array_filter($Data);
	$Data = array_merge($Data);
	$json_data = array(
		"draw"               => intval( $requestData['draw'] ),
		"recordsTotal"       => intval( $totalData ),
		"recordsFiltered"    => intval( $totalFiltered ),
		"records"            => $Data
		);

	echo json_encode($json_data);
	exit;
}

public function CustomerAgeingReport()
{
	$Customer_list=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('sub_group_id'=>3)));
	$this->set('Customer_list',$Customer_list);
	$this->request->data['from_date']=date('d-m-Y');


}
Public function customer_ageing_report_ajax()
{
	$conditions=[];
	$data=$this->request->data;

	if($data['customer_id'])
	{
		$conditions['Customer.account_head_id']=$data['customer_id'];

	}
	$Customer=$this->Customer->find('all',array(
		'conditions'=>$conditions,
		'order' => array('Customer.id' => 'ASC'),
		'fields'=>array(
			'Customer.account_head_id',
			'AccountHead.opening_balance',
			'AccountHead.name',
			'CustomerType.name',
			'AccountHead.id',

			),
		));
	$data['row']='';
	$data['tfoot']='';
	$data['result']='Error';
	$balance=0;
	$Balance_Total=0;
	if($Customer)
	{

		$invoice_array=array();
		foreach ($Customer as $key => $value) {
			$account_id=$value['AccountHead']['id'];
			$Sales = $this->Sale->find('all', array(
				'conditions' => array(
					'Sale.account_head_id' =>$account_id,
//'Sale.executive_id'=>$value['Executive']['id'],
					'Sale.flag'=>1,
					'Sale.status'=>array(2,3),
					),
				'order' => array('Sale.date_of_delivered' => 'ASC'),
				'fields' => array(
					'Sale.id',
					'Sale.date_of_delivered',
					'Sale.invoice_no',
					'Sale.account_head_id',
					'Sale.executive_id',
					'Sale.grand_total',
					'AccountHead.name',
					'Executive.account_head_id',

					)
				)
			);
			$voucher_amount=0;
			$discount_paid_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DISCOUNT PAID'));
			$Journal_voucher=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.credit'=>$account_id,
					//'Journal.debit'=>$value['Executive']['account_head_id'],
					'NOT' => array(
						'Journal.debit'=>array($discount_paid_account_head_id),
						'Journal.remarks LIKE'=>'%Sale Invoice No :%',
					),
					'Journal.flag=1',
					),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
					),
				));

			foreach ($Journal_voucher as $key3 => $value_amount) {
				$voucher_amount+=$value_amount;
			}
			$Journal_extra_debited=$this->Journal->find('list',array(
			'conditions'=>array(
				'Journal.debit'=>$account_id,
				'Journal.work_flow'=>array('Cheques'),
				'Journal.flag=1',
				),
			'fields'=>array(
				'Journal.id',
				'Journal.amount',
				),
			));
			$other_debited_amount=0;
			foreach ($Journal_extra_debited as $key3 => $value_amount) {
				$other_debited_amount+=$value_amount;
			}
// $Journal_voucher1=$this->Journal->find('list',array(
// 	'conditions'=>array(
// 		'Journal.credit'=>$account_id,
// 		//'Journal.debit'=>$value['Executive']['account_head_id'],
// 		'NOT' => array(
// 			'Journal.remarks LIKE' => 'Sale Invoice No :%',
// 		),
// 		'Journal.flag=1',
// 	),
// 	'fields'=>array(
// 		'Journal.id',
// 		'Journal.amount',
// 	),
// ));
			$voucher_balance=0;
// foreach ($Journal_voucher1 as $key3 => $value_amount1) {
// 	$voucher_amount+=$value_amount1;
// }
			foreach ($Sales as $keyj =>$valuej)
			{
				$SaleItem=$this->SaleItem->find('list',array(
					'conditions'=>array(
						'SaleItem.sale_id'=>$valuej['Sale']['id'],
						),
					'fields'=>array(
//'SaleItem.discount',
						)
					));
			}
			$invoice_array_single=array();
			if (!empty($Sales)) {


				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
//$invoice_array_single['Executive_name'] = $value['Executive']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				$Journal_opening_received=$this->Journal->find('all',array(
				'conditions'=>array(
					'Journal.remarks'=>'Sale Invoice No :0',
					'Journal.credit'=>$value['AccountHead']['id'],
					'Journal.flag=1',
					),
				));
				$opening_paid_amount=0;
				foreach ($Journal_opening_received as $key_each_sale => $value_each_sale) {
					$opening_paid_amount+=$value_each_sale['Journal']['amount'];
				}
				$outstanding_amount+=$other_debited_amount;
				$outstanding_amount-=$opening_paid_amount;
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
//pr($voucher_amount);
				if($outstanding_amount>=1)
				{
					array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
					array_push($invoice_array_single['invoice_no'], 'Opening Balance');
					array_push($invoice_array_single['balance'],$outstanding_amount);

				}
				foreach ($Sales as $keySC2 => $valueSC2) {
					$Journal=$this->Journal->find('all',array(
					'conditions'=>array(
						'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
						'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
						// 'NOT' => array(
						// 	'Journal.work_flow'=>'From Mobile',
						// ),
						'AccountHeadDebit.sub_group_id=1',
						'Journal.flag=1',
						),
					));
					// $Journal=$this->Journal->find('all',array(
					// 	'conditions'=>array(
					// 		'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
					// 		'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
					// 		'Journal.debit=1',
					// 		'Journal.flag=1',
					// 		),
					// 	));
					$balance_amount=$valueSC2['Sale']['grand_total'];
					$paid_amount=0;
					foreach ($Journal as $key0 => $amount) {
						$paid_amount+=$amount['Journal']['amount'];
						$balance_amount-=$amount['Journal']['amount'];
					}
					if($voucher_amount)
					{
						if($balance_amount<=$voucher_amount)
						{
							$voucher_balance=$balance_amount;
							$balance_amount=0;
							$voucher_amount-=$voucher_balance;
						}
						else
						{
							$balance_amount-=$voucher_amount;
							$voucher_amount=0;
						}
					}
					if($valueSC2['Sale']['grand_total']>0)
					{
						if($balance_amount){
//if($valueSC2['Sale']['account_head_id']==$data['customer_id'])
//{
							$check_date=$valueSC2['Sale']['date_of_delivered'];


							array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Sale']['date_of_delivered'])));
							array_push($invoice_array_single['invoice_no'], $valueSC2['Sale']['invoice_no']);
							array_push($invoice_array_single['balance'],$balance_amount) ;  

//}
						}
					}
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      

			}
			else
			{


				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
//$invoice_array_single['Executive_name'] = $value['Executive']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				$Journal_opening_received=$this->Journal->find('all',array(
				'conditions'=>array(
					'Journal.remarks'=>'Sale Invoice No :0',
					'Journal.credit'=>$value['AccountHead']['id'],
					'Journal.flag=1',
					),
				));
				$opening_paid_amount=0;
				foreach ($Journal_opening_received as $key_each_sale => $value_each_sale) {
					$opening_paid_amount+=$value_each_sale['Journal']['amount'];
				}
				$outstanding_amount+=$other_debited_amount;
				$outstanding_amount-=$opening_paid_amount;
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{

					array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
					array_push($invoice_array_single['invoice_no'], 'Opening Balance');
					array_push($invoice_array_single['balance'],$outstanding_amount);

				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      
			}

		}
//pr($invoice_array);
		$grand_total=0;
		$q=0;$w=0;$p=0;$r=0;$t=0;$y=0;
		foreach($invoice_array as $key=>$value) {
			$party_name=$value['party_name'];
			$inner_array_count=count($value['invoice_no']);
			$a1=array();
			$b1=array();
			$c1=array();
			$f1=array();
			$g=array();
			$m=array();
			foreach ($value['delivered_date'] as $key=>$value1)
			{
				$delivered_date=$value1;
				$now = time();
				$expected_days_diff=$now-strtotime($delivered_date);
				$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
				array_push($m,$diff_day);
			}
			foreach($value['invoice_no'] as $key=>$value2)
			{
				$invoice_no=$value2;

			}
			$new_balance=0;

			foreach ($value['balance'] as $key=>$value3)
			{
				$balance=$value3;
				array_push($g,$balance);
				$new_balance+=$balance;
			}
			for($i=0;$i<count($m);$i++)
			{

				if($m[$i]<=30){ $a=$g[$i];array_push($a1,$a);}
				if($m[$i]<=60 && $m[$i]>30){$b=$g[$i];array_push($b1,$b);}
				if($m[$i]<=90 && $m[$i]>60){$c=$g[$i];array_push($c1,$c);}
				if($m[$i]>90){$f=$g[$i];array_push($f1,$f);}
			}

			$grand_total+=$new_balance;
			$b=array_sum($a1);if($b!=0){$b11=ROUND($b,3);}else{$b11='';}
			$b5=array_sum($b1);if($b5!=0){$b51=ROUND($b5,3);}else{$b51='';}
			$b19=array_sum($c1);if($b19!=0){$b12=ROUND($b19,3);}else{$b12='';}			
			$b4=array_sum($f1);if($b4!=0){$b41=ROUND($b4,3);}else{$b41='';}

			$q+=floatval($b11);$w+=floatval($b51);$p+=floatval($b12);$y+=floatval($b41);
//pr($b51);
			$data['row'].='<tr>';
			$data['row'].='<td>'.$party_name.'</td>';
			$data['row'].='<td style="text-align:right">'.$b11.'</td>';
			$data['row'].='<td style="text-align:right">'.$b51.'</td>';
			$data['row'].='<td style="text-align:right">'.$b12.'</td>';
			$data['row'].='<td style="text-align:right">'.$b41.'</td>';
			$data['row'].='<td style="text-align:right">'.ROUND($new_balance,3).'</td>';
			$data['row'].='</tr>';    
		}
		$data['tfoot']= $data['tfoot'].'<tr>';
		$data['tfoot']= $data['tfoot'].'<td><h3>Total</h3></td>';
		$data['tfoot']= $data['tfoot'].'<td style="text-align:right"><h3>'.$q.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td style="text-align:right"><h3>'.$w.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td style="text-align:right"><h3>'.$p.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td style="text-align:right"><h3>'.$y.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'<td style="text-align:right"><h3>'.$grand_total.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'</tr>';
		$data['result']='Success';

	}
	echo json_encode($data); exit;
}
public function SaleOutstanding()
{
	$sub_group_id=3;
	$Customer_list=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('sub_group_id'=>3)));
	$Customer=$this->Customer->find('all',array(
		'order'=>array('AccountHead.name ASC'),
		));
	$this->set('Customer_list',$Customer_list);
	$this->set('Customer',$Customer);
}
public function customer_due_report_ajax()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['customer_id'])
	{
		$conditions['Customer.account_head_id']=$data['customer_id'];
	}

	$Customer=$this->Customer->find('all',array(
		'conditions'=>$conditions,
		'fields'=>array(
			'Customer.account_head_id',
			'AccountHead.opening_balance',
			'AccountHead.name',
			'CustomerType.name',
			),
		));
	$data['row']='';
	$data['tfoot']='';
	$data['result']='Error';
	$Balance_Total=0;
	if($Customer)
	{
		foreach ($Customer as $key => $value) {
			$Balance_Total=$this->get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance']);
			$Customer[$key]['Customer']['Balance_Total']=round($Balance_Total,3);
		}
		//$data['data']=$Customer;
		$total_outstanding=0;
		foreach($Customer as $key=>$value) {
			$data['row'].='<tr>';
			$data['row'].='<td>'.$value['CustomerType']['name'].'</td>';
			$data['row'].='<td>'.$value['AccountHead']['name'].'</td>';
			$data['row'].='<td style="text-align:right">'.$value['Customer']['Balance_Total'].'</td>';
			$data['row'].='</tr>';    
			$total_outstanding+=$value['Customer']['Balance_Total'];
		}
		$data['tfoot']= $data['tfoot'].'<tr>';
		$data['tfoot']= $data['tfoot'].'<td><h3>Total</h3></td>';
		$data['tfoot']= $data['tfoot'].'<td></td>';
		$data['tfoot']= $data['tfoot'].'<td><h3>'.$total_outstanding.'<h3></td>';
		$data['tfoot']= $data['tfoot'].'</tr>';
		$data['result']='Success';
		
	}
	else
	{
		$data['result']='Error';
	}
	echo json_encode($data);
	exit;
}
public function expensegraph($user_branch_id=null)
{
	$direct_expense_group_id=12;
	$indirect_expense_group_id=13;
	$priliminary_expense_written_off=18;
	$purchase=5;
	$salereturn=6;
	$discount=8;
	$tax=7;
	$conditions=[];

	$conditions['SubGroup.group_id']=[
	$direct_expense_group_id,
	$indirect_expense_group_id
	];
	$conditions['SubGroup.id !=']=[
	$purchase,
	$salereturn,
	$discount,
	$tax

	];
	$conditions['AccountHead.id !=']=[
	$priliminary_expense_written_off
	];
	$totalData=$this->AccountHead->find('count',['conditions'=>$conditions]);
	$AccountHead=$this->AccountHead->find('all',array(
		'conditions'=>$conditions,
		'fields'=>[
		'AccountHead.id',
		'AccountHead.name',
		'AccountHead.opening_balance',
		'SubGroup.name',
		]
		));
	$paid=0;
	$from_date = date('Y-m-d',strtotime('first day of this month'));
	$to_date = date('Y-m-d',strtotime('last day of this month'));
	foreach ($AccountHead as $key => $value) {
		$conditions_journal=[];
		if(!empty($user_branch_id))
		{
			$conditions_journal['Journal.branch_id']=$user_branch_id;
		}
		$conditions_journal['flag']=1;
		$conditions_journal['debit']=$value['AccountHead']['id'];
		$conditions_journal['Journal.date between ? and ?']=array($from_date,$to_date);
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>$conditions_journal,

// 'limit'=>1,
			'fields'=>array( 'Journal.total_amount'),
			));
		$paid+=round($Journal_Debit['Journal']['total_amount']);
	}
	return $paid;
}
public function expensegraph1()
{
	$direct_expense_group_id=12;
	$indirect_expense_group_id=13;
	$priliminary_expense_written_off=18;
	$conditions=[];
	$conditions['SubGroup.group_id']=[
	$direct_expense_group_id,
	$indirect_expense_group_id
	];
	$conditions['AccountHead.id !=']=[
	$priliminary_expense_written_off
	];
	$totalData=$this->AccountHead->find('count',['conditions'=>$conditions]);
	$AccountHead=$this->AccountHead->find('all',array(
		'conditions'=>$conditions,
		'fields'=>[
		'AccountHead.id',
		'AccountHead.name',
		'AccountHead.opening_balance',
		'SubGroup.name',
		]
		));
	$from_date = date('Y-m-d',strtotime('first day of this month'));
	$to_date = date('Y-m-d',strtotime('last day of this month'));
	$paid=0;  
	foreach ($AccountHead as $key => $value) {
		$expense_id=$value['AccountHead']['id'];
		$expense_function=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id'],$from_date,$to_date);
		$single['key']=$key+1;
		$expense_name=explode(' ',$value['AccountHead']['name']);
		$removed = array_pop($expense_name);
		if($removed!='PAID')
		{
			$expense_name=explode(' ',$value['AccountHead']['name']);
		}
		$expense_name=implode($expense_name, ' ');
		$single['name']=$expense_name;
		$single['sub_group']=$value['SubGroup']['name'];
		$single['total']=$expense_function['debit'];
		$single['first']=0;
		$single['second']=0;
		$OUTSTANDING_AccountHead=$this->AccountHead->findByName($expense_name.' OUTSTANDING');
		if($OUTSTANDING_AccountHead)
		{
			$single['first']=$OUTSTANDING_AccountHead['AccountHead']['opening_balance'];
			$Journal_outstanding=$this->General_Journal_Debit_N_Credit_function($OUTSTANDING_AccountHead['AccountHead']['id'],$from_date,$to_date);
			$single['first']+=$Journal_outstanding['credit']-$Journal_outstanding['debit'];
		}
		$PREPAID_AccountHead=$this->AccountHead->findByName($expense_name.' PREPAID');
		if(!empty($PREPAID_AccountHead))
		{
			$single['second']=$PREPAID_AccountHead['AccountHead']['opening_balance'];
			$single['total']+=$PREPAID_AccountHead['AccountHead']['opening_balance'];
			$Journal_PREPAID=$this->General_Journal_Debit_N_Credit_function($PREPAID_AccountHead['AccountHead']['id'],$from_date,$to_date);
			$single['second']+=$Journal_PREPAID['debit']-$Journal_PREPAID['credit'];
		}
		$single['balance']=$single['total'];
		if($single['total']<$single['first'])
		{
			$single['balance']+=$single['first'];
			$single['total']+=$single['first'];
		}
		elseif($single['total']==0 && $single['first']!=0)
		{
			$single['second']+=$single['first']*-1;
			$single['first']=0;
		}
		$expense_function=$this->General_Journal_Debit_N_Credit_function($value['AccountHead']['id'],$from_date,$to_date);
		$single['paid']=$expense_function['debit']-$single['first']+$single['second'];
//pr($single['paid']);
		$paid+=$single['paid'];
	}
	return $paid;
}

public function Get_Master_Group_Ajax()
{
	
	$return['result']='Empty';
	$data=$this->request->data;
	$from_date=$data['from_date'];
	$to_date=$data['to_date'];
	$type_id=$data['type_id'];
	$MasterGroup=$this->AccSubGroup->find('list',['conditions'=>['main_group_id'=>$type_id],'order'=>['name Asc']]);
	$return['SubGroup']=$MasterGroup;
	echo json_encode($return); exit;
}
public function Get_Group_Ajax()
{
	$return['result']='Empty';
	$data=$this->request->data;
	$from_date=$data['from_date'];
	$to_date=$data['to_date'];
	$master_group_id=$data['master_group_id'];
	$Group=$this->Group->find('list',[
		'conditions'=>['master_group_id'=>$master_group_id],
		'order'=>['Group.name ASC'],
		]);
	$return['Group']=$Group;
	echo json_encode($return); exit;
}
public function Get_Group_Ajax_trial()
{
	$return['result']='Empty';
	$data=$this->request->data;
	$from_date=$data['from_date'];
	$to_date=$data['to_date'];
	$master_group_id=$data['master_group_id'];
	$Group=$this->Group->find('list',[
		'conditions'=>['master_group_id'=>$master_group_id],
		'order'=>['Group.name ASC'],
		]);
	$return['Group']=$Group;
	echo json_encode($return); exit;
}
public function Get_SubGroup_Ajax()
{
	$return['result']='Empty';
	$data=$this->request->data;
	$from_date=$data['from_date'];
	$to_date=$data['to_date'];
	$group_id=$data['group_id'];
	$SubGroup=$this->SubGroup->find('list',[
		'conditions'=>['group_id'=>$group_id],
		'order'=>['SubGroup.name ASC'],
		]);
	$return['SubGroup']=$SubGroup;
	echo json_encode($return); exit;
}
public function Get_AccountHead_Ajax()
{
	$user_branch_id=$this->Session->read('User.branch_id');
	// $data['type_id']=5;
	// $data['from_date']='2018-04-01';
	// $conditions['AccountHead.id']=2122;
	// $data['to_date']='2019-03-31';
	$data=$this->request->data;
	if($data['from_date'] && $data['to_date']){
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
	}
	$conditions=[];
	if(isset($data['sub_group_id']))
	{
		$sub_group_id=$data['sub_group_id'];
		$conditions['AccountHead.acc_sub_group_id']=$sub_group_id;
	}
	//if($data['branch']) $conditions['branch_id']=$branch;
	$type_id=$data['type_id'];
	$conditions['AccountHead.id !=']=19;
	$AccountHead=$this->AccountHead->find('list',['conditions'=>$conditions]);
	$GSTReport=['total'=>[]];
	$total['tax']['in']=0;
	$total['tax']['out']=0;
	$return['result']='Success';
	asort($AccountHead);
	foreach($AccountHead as $key => $value)
	{
		$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function1($key,$from_date);
		//print_r($opening_function_value_return);
		//exit;
		$function_value_return=$this->General_Journal_Debit_N_Credit_function1($key,$from_date,$to_date);
		$AccountHead=$this->AccountHead->findById($key);
		$return['AccountHead'][$key]['name']=$value;
		$return['AccountHead'][$key]['amount']=0;
		$return['AccountHead'][$key]['opening_balance']=0;
		$return['AccountHead'][$key]['current']=0;
		$return['AccountHead'][$key]['debit']=0;
		$return['AccountHead'][$key]['credit']=0;
		
		$return['AccountHead'][$key]['id']=$key;
		if(in_array($type_id, ['1','3']))
		{
			$created_at = date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])); 
			if($to_date >= $created_at){
			$return['AccountHead'][$key]['opening_balance']+=$AccountHead['AccountHead']['opening_balance'];
		}
		//$return['AccountHead'][$key]['created_at'] = $created_at;
			if($type_id=='1')
			{
				$return['AccountHead'][$key]['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
				$return['AccountHead'][$key]['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
				$return['AccountHead'][$key]['current']+=number_format($function_value_return['debit'],2,'.','');
				$return['AccountHead'][$key]['current']-=number_format($function_value_return['credit'],2,'.','');
			}
			if($type_id=='3')
			{
				$return['AccountHead'][$key]['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
				$return['AccountHead'][$key]['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
				$return['AccountHead'][$key]['current']+=number_format($function_value_return['debit'],2,'.','');
				$return['AccountHead'][$key]['current']-=number_format($function_value_return['credit'],2,'.','');
			}
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['opening_balance'];
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['current'];
		}
		if($type_id=='5')
			{
				$created_at = date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])); 
			if($to_date >= $created_at){
				$return['AccountHead'][$key]['opening_balance']+=$AccountHead['AccountHead']['opening_balance'];
			}
				$return['AccountHead'][$key]['opening_balance']+=$opening_function_value_return['debit'];
				 $return['AccountHead'][$key]['opening_balance']-=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['current']+=$function_value_return['debit']-$function_value_return['credit'];
				//$return['AccountHead'][$key]['current']-=$function_value_return['debit'];
				// }
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['opening_balance'];
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['current'];
			}
		if($type_id=='2')
		{
			$AccountHead_Name=$value;
			$AccountHead_Name_split=explode(' ',$AccountHead_Name);
			$created_at = date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])); 
			if($to_date >= $created_at){
			$return['AccountHead'][$key]['opening_balance']+=$AccountHead['AccountHead']['opening_balance'];
		}
			if($AccountHead_Name_split[1]=='CAPITAL')
			{
				$return['AccountHead'][$key]['opening_balance']+=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['opening_balance']-=$opening_function_value_return['debit'];
				$return['AccountHead'][$key]['current']+=$function_value_return['debit']-$function_value_return['credit'];
			}
			else
			{
				$return['AccountHead'][$key]['opening_balance']-=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['opening_balance']+=$opening_function_value_return['debit'];
				//$return['AccountHead'][$key]['current']-=$function_value_return['debit']-$function_value_return['credit'];
				$return['AccountHead'][$key]['current']+=$function_value_return['debit']-$function_value_return['credit'];
			}
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['opening_balance'];
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['current'];
		}
		// if($type_id=='3')
		// {
		// 	// $AccountHead_Name=$value;
		// 	// $AccountHead_Name_split=explode(' ',$AccountHead_Name);
		// 	$return['AccountHead'][$key]['opening_balance']+=$AccountHead['AccountHead']['opening_balance'];
		// 	$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['opening_balance'];
		// 	$return['AccountHead'][$key]['amount']+=$function_value_return['debit']-$function_value_return['credit'];
		// 	$return['AccountHead'][$key]['current']+=$function_value_return['debit']-$function_value_return['credit'];
		// }
		if($type_id=='4')
		{
			$AccountHead_Name=$value;
			$AccountHead_Name_split=explode(' ',$AccountHead_Name);
			$created_at = date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])); 
			if($to_date >= $created_at){
			$return['AccountHead'][$key]['opening_balance']+=$AccountHead['AccountHead']['opening_balance'];
		}
				$return['AccountHead'][$key]['opening_balance']+=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['opening_balance']-=$opening_function_value_return['debit'];
				$return['AccountHead'][$key]['current']+=$function_value_return['debit']-$function_value_return['credit'];			
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['opening_balance'];
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['current'];
		}
		// if($return['AccountHead'][$key]['opening_balance']<0){
  //      $return['AccountHead'][$key]['opening_balance']=number_format($return['AccountHead'][$key]['opening_balance'],2,'.','');
		// }else{
		//if( )
		//$return['AccountHead'][$key]['created_at']
		$return['AccountHead'][$key]['opening_balance']=number_format($return['AccountHead'][$key]['opening_balance'],2,'.','');
	    //}
		$return['AccountHead'][$key]['current']=number_format($return['AccountHead'][$key]['current'],2,'.','');
		$return['AccountHead'][$key]['amount']=number_format($return['AccountHead'][$key]['amount'],2,'.','');
		$return['AccountHead'][$key]['credit']+=$function_value_return['credit'];
		$return['AccountHead'][$key]['debit']+=$function_value_return['debit'];
		$return['AccountHead'][$key]['credit']=number_format($return['AccountHead'][$key]['credit'],2,'.','');
		$return['AccountHead'][$key]['debit']=number_format($return['AccountHead'][$key]['debit'],2,'.','');
	}
	echo json_encode($return); exit;
}
public function General_Journal_Debit_N_Credit_function1($account_head_id,$from_date,$to_date)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$Journal_Debit_conditions= [];
		$Journal_Debit_conditions['debit'] = $account_head_id;
		$Journal_Debit_conditions['credit !='] =19;
		$Journal_Debit_conditions['flag'] = '1';
		$Journal_Debit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		$Journal_Credit_conditions= [];
		$Journal_Credit_conditions['credit'] = $account_head_id;
		$Journal_Credit_conditions['debit !='] = 19;
		$Journal_Credit_conditions['flag'] = '1';
		$Journal_Credit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
			$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
						'conditions'=>$Journal_Debit_conditions,
						'fields'=>array( 'Journal.total_amount'),
					));
		$Journal_Credit=$this->Journal->find('first',array(
						'conditions'=>$Journal_Credit_conditions,
						'fields'=>array( 'Journal.total_amount'),
					));
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
public function General_Journal_Openging_Debit_N_Credit_function1($account_head_id,$from_date)
	{	
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$account_head_id,
				'credit !='=>19,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>array(
				'credit'=>$account_head_id,
				'debit !='=>19,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
public function Get_AccountHead_Ajax_balance_sheet()
{
	$user_branch_id=$this->Session->read('User.branch_id');
	// $data['type_id']=5;
	// $data['from_date']='2018-04-01';
	// $conditions['AccountHead.id']=2122;
	// $data['to_date']='2019-03-31';
	$data=$this->request->data;
	if($data['from_date'] && $data['to_date']){
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
	}
	$conditions=[];
	if(isset($data['sub_group_id']))
	{
		$sub_group_id=$data['sub_group_id'];
		$conditions['AccountHead.acc_sub_group_id']=$sub_group_id;
	}
	//if($data['branch']) $conditions['branch_id']=$branch;
	$type_id=$data['type_id'];
	//$conditions['AccountHead.acc_sub_group_id !=']=2;
	$AccountHead=$this->AccountHead->find('list',['conditions'=>$conditions]);
	//pr($AccountHead);exit;
	$GSTReport=['total'=>[]];
	$total['tax']['in']=0;
	$total['tax']['out']=0;
	$return['result']='Success';
	asort($AccountHead);
	foreach($AccountHead as $key => $value)
	{
		$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function2($key,$from_date);
		//print_r($opening_function_value_return);
		//exit;
		$function_value_return=$this->General_Journal_Debit_N_Credit_function2($key,$from_date,$to_date);
		$AccountHead=$this->AccountHead->findById($key);
		$profit_sub_group_id=$this->SystemParameter->field('value',array('id'=>14));
		$profit_loss_account_head_id=$this->SystemParameter->field('value',array('id'=>15));
		$return['AccountHead'][$key]['id']=$key;
		$return['AccountHead'][$key]['profit_sub_group_id']=$profit_sub_group_id;
		$return['AccountHead'][$key]['profit_loss_account_head_id']=$profit_loss_account_head_id;
		$return['AccountHead'][$key]['name']=$value;
		$return['AccountHead'][$key]['amount']=0;
		$return['AccountHead'][$key]['opening_balance']=0;
		$return['AccountHead'][$key]['current']=0;
		$return['AccountHead'][$key]['net_right']=0;
		$return['AccountHead'][$key]['net_left']=0;
		$return['AccountHead'][$key]['debit']=0;
		$return['AccountHead'][$key]['credit']=0;

		$ProfitLoss['result']='Success';
		$ProfitLoss['Stock']=$this->StockCalculator($from_date,$to_date);
		$ProfitLoss['Income']=$this->IncomeCalculator($from_date,$to_date);
		$ProfitLoss['Expense']=$this->ExpenseCalculator($from_date,$to_date);
		$ProfitLoss['Purchase']=$this->PurchaseCalculator($from_date,$to_date);
		$ProfitLoss['Sale']=$this->SaleCalculator($from_date,$to_date);
		// $Product=$this->Product_id_list_function($from_date,$to_date);
		$GSTReport=['total'=>[]];
		$gst_in=0;
		$gst_out=0;
		$total_in=0;
		$total_out=0;
		$GSTReport['total']['out']=0;
		$GSTReport['total']['in']=0;
		$tax_amount['left']=0;
		$tax_amount['right']=0;
		$ProfitLoss['Expense']['IndirectExpense']['amount']+=$tax_amount['left'];
		$ProfitLoss['Income']['IndirectIncome']['amount']+=$tax_amount['right'];
		$IndirectExpense_gst_array['name']='tax';
		$IndirectExpense_gst_array['PrePaid']=0;
		$IndirectExpense_gst_array['paid']=0;
		$IndirectExpense_gst_array['Outstanding']=$tax_amount['left'];
		$IndirectExpense_gst_array['total']=$tax_amount['left'];
		$ProfitLoss['Expense']['IndirectExpense']['single']['TAX']=$IndirectExpense_gst_array;
		$IndirectIncome_gst_array['name']='tax';
		$IndirectIncome_gst_array['Received']=0;
		$IndirectIncome_gst_array['Advance']=0;
		$IndirectIncome_gst_array['Accrued']=$tax_amount['right'];
		$IndirectIncome_gst_array['Total']=$tax_amount['right'];
		$ProfitLoss['Income']['IndirectIncome']['single']['TAX']=$IndirectIncome_gst_array;
		$ProfitLoss['First']['Left']=0;
		$ProfitLoss['First']['Right']=0;
		$ProfitLoss['Second']['Left']=0;
		$ProfitLoss['Second']['Right']=0;
		$ProfitLoss['FirstTotal']['Left']=0;
		$ProfitLoss['FirstTotal']['Right']=0;
		$ProfitLoss['Gross']['Left']=0;
		$ProfitLoss['Gross']['Right']=0;
		$ProfitLoss['Net']['Right']=0;
		$ProfitLoss['Net']['Left']=0;
		$ProfitLoss['SecondTotal']['Left']=0;
		$ProfitLoss['SecondTotal']['Right']=0;
		$FirstLeft=$ProfitLoss['Purchase']['PurchaseValue']-$ProfitLoss['Purchase']['PurchaseReturn']-$ProfitLoss['Purchase']['DebitNote']+$ProfitLoss['Expense']['DirectExpense']['amount']+$ProfitLoss['Stock']['open']+$tax_amount['right'];
		$FirstLeft=number_format($FirstLeft,2,'.','');
		$FirstRight=abs($ProfitLoss['Sale']['SaleValue']+$ProfitLoss['Sale']['SalesReturn']+$ProfitLoss['Sale']['CreditNote'])+abs($ProfitLoss['Income']['DirectIncome']['amount'])+$ProfitLoss['Stock']['close']+$tax_amount['left'];
		$FirstRight=number_format($FirstRight,2,'.','');
		if($FirstLeft>$FirstRight)
		{
			$ProfitLoss['First']['Right']=number_format(($FirstLeft-$FirstRight),2,'.','');
			$ProfitLoss['Gross']['Left']=number_format(($FirstLeft-$FirstRight),2,'.','');
		}
		else
		{
			$ProfitLoss['First']['Left']=number_format(($FirstRight-$FirstLeft),2,'.','');
			$ProfitLoss['Gross']['Right']=number_format(($FirstRight-$FirstLeft),2,'.','');
		}
		$ProfitLoss['FirstTotal']['Left']+=$FirstLeft+$ProfitLoss['First']['Left'];
		$ProfitLoss['FirstTotal']['Right']+=$FirstRight+$ProfitLoss['First']['Right'];
		$SecondLeft=$ProfitLoss['Expense']['IndirectExpense']['amount']+$ProfitLoss['Gross']['Left'];
		$SecondLeft=number_format($SecondLeft,2,'.','');
		$SecondRight=abs($ProfitLoss['Income']['IndirectIncome']['amount'])+$ProfitLoss['Gross']['Right'];
		$SecondRight=number_format($SecondRight,2,'.','');
		if($SecondLeft>$SecondRight)
		{
			$ProfitLoss['Second']['Right']=number_format(($SecondLeft-$SecondRight),2,'.','');
			$ProfitLoss['Net']['Right']=number_format(($SecondLeft-$SecondRight),2,'.','');
		}
		else
		{
			$ProfitLoss['Second']['Left']=number_format(($SecondRight-$SecondLeft),2,'.','');
			$ProfitLoss['Net']['Left']=number_format(($SecondRight-$SecondLeft),2,'.','');
		}
		$ProfitLoss['SecondTotal']['Left']+=$ProfitLoss['Net']['Left']+$SecondLeft;
		$ProfitLoss['SecondTotal']['Right']+=$ProfitLoss['Net']['Right']+$SecondRight;
// if($ProfitLoss['Net']['Left']>0){
		$return['AccountHead'][$key]['net_left']+=isset($ProfitLoss['Net']['Left'])?$ProfitLoss['Net']['Left']:0;
	// }else{
		$return['AccountHead'][$key]['net_right']+=isset($ProfitLoss['Net']['Right'])?$ProfitLoss['Net']['Right']:0;
	// }
		
		$return['AccountHead'][$key]['id']=$key;
		if(in_array($type_id, ['1','3']))
		{
			$created_at = date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])); 
			if($to_date >= $created_at){
			$return['AccountHead'][$key]['opening_balance']+=$AccountHead['AccountHead']['opening_balance'];
		}
		//$return['AccountHead'][$key]['created_at'] = $created_at;
			if($type_id=='1')
			{
				$return['AccountHead'][$key]['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
				$return['AccountHead'][$key]['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
				$return['AccountHead'][$key]['current']+=number_format($function_value_return['debit'],2,'.','');
				$return['AccountHead'][$key]['current']-=number_format($function_value_return['credit'],2,'.','');
			}
			if($type_id=='3')
			{
				$return['AccountHead'][$key]['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
				$return['AccountHead'][$key]['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
				$return['AccountHead'][$key]['current']+=number_format($function_value_return['debit'],2,'.','');
				$return['AccountHead'][$key]['current']-=number_format($function_value_return['credit'],2,'.','');
			}
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['opening_balance'];
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['current'];
		}
		if($type_id=='5')
			{
				$created_at = date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])); 
			if($to_date >= $created_at){
				$return['AccountHead'][$key]['opening_balance']+=$AccountHead['AccountHead']['opening_balance'];
			}
				$return['AccountHead'][$key]['opening_balance']+=$opening_function_value_return['debit'];
				 $return['AccountHead'][$key]['opening_balance']-=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['current']+=$function_value_return['debit']-$function_value_return['credit'];
				//$return['AccountHead'][$key]['current']-=$function_value_return['debit'];
				// }
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['opening_balance'];
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['current'];
			}
		if($type_id=='2')
		{
			$AccountHead_Name=$value;
			$AccountHead_Name_split=explode(' ',$AccountHead_Name);
			$created_at = date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])); 
			if($to_date >= $created_at){
			$return['AccountHead'][$key]['opening_balance']+=$AccountHead['AccountHead']['opening_balance'];
		}
			if($AccountHead_Name_split[1]=='CAPITAL')
			{
				$return['AccountHead'][$key]['opening_balance']+=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['opening_balance']-=$opening_function_value_return['debit'];
				$return['AccountHead'][$key]['current']+=$function_value_return['debit']-$function_value_return['credit'];
			}
			else
			{
				$return['AccountHead'][$key]['opening_balance']-=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['opening_balance']+=$opening_function_value_return['debit'];
				//$return['AccountHead'][$key]['current']-=$function_value_return['debit']-$function_value_return['credit'];
				$return['AccountHead'][$key]['current']+=$function_value_return['debit']-$function_value_return['credit'];
			}
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['opening_balance'];
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['current'];
		}
		// if($type_id=='3')
		// {
		// 	// $AccountHead_Name=$value;
		// 	// $AccountHead_Name_split=explode(' ',$AccountHead_Name);
		// 	$return['AccountHead'][$key]['opening_balance']+=$AccountHead['AccountHead']['opening_balance'];
		// 	$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['opening_balance'];
		// 	$return['AccountHead'][$key]['amount']+=$function_value_return['debit']-$function_value_return['credit'];
		// 	$return['AccountHead'][$key]['current']+=$function_value_return['debit']-$function_value_return['credit'];
		// }
		if($type_id=='4')
		{
			$AccountHead_Name=$value;
			$AccountHead_Name_split=explode(' ',$AccountHead_Name);
			$created_at = date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])); 
			if($to_date >= $created_at){
			$return['AccountHead'][$key]['opening_balance']+=$AccountHead['AccountHead']['opening_balance'];
		}
				$return['AccountHead'][$key]['opening_balance']+=$opening_function_value_return['credit'];
				$return['AccountHead'][$key]['opening_balance']-=$opening_function_value_return['debit'];
				$return['AccountHead'][$key]['current']+=$function_value_return['debit']-$function_value_return['credit'];			
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['opening_balance'];
			$return['AccountHead'][$key]['amount']+=$return['AccountHead'][$key]['current'];
		}
		// if($return['AccountHead'][$key]['opening_balance']<0){
  //      $return['AccountHead'][$key]['opening_balance']=number_format($return['AccountHead'][$key]['opening_balance'],2,'.','');
		// }else{
		//if( )
		//$return['AccountHead'][$key]['created_at']
		$return['AccountHead'][$key]['opening_balance']=number_format($return['AccountHead'][$key]['opening_balance'],2,'.','');
	    //}
		$return['AccountHead'][$key]['current']=number_format($return['AccountHead'][$key]['current'],2,'.','');
		$return['AccountHead'][$key]['amount']=number_format($return['AccountHead'][$key]['amount'],2,'.','');
		$return['AccountHead'][$key]['credit']+=$function_value_return['credit'];
		$return['AccountHead'][$key]['debit']+=$function_value_return['debit'];
		$return['AccountHead'][$key]['credit']=number_format($return['AccountHead'][$key]['credit'],2,'.','');
		$return['AccountHead'][$key]['debit']=number_format($return['AccountHead'][$key]['debit'],2,'.','');
		$return['AccountHead'][$key]['net_right']=number_format($return['AccountHead'][$key]['net_right'],2,'.','');
		$return['AccountHead'][$key]['net_left']=number_format(($return['AccountHead'][$key]['net_left']*(-1)),2,'.','');
	}
	echo json_encode($return); exit;
}
public function General_Journal_Debit_N_Credit_function3($account_head_id,$from_date,$to_date)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$customer_id=$this->SystemParameter->field('value',array('id'=>11));
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$Journal_Debit_conditions= [];
		$Journal_Debit_conditions['debit'] = $account_head_id;
		// $Journal_Debit_conditions['debit !='] = 19;
		$Journal_Debit_conditions['credit !='] = 19;
		$Journal_Debit_conditions['flag'] = '1';
		$Journal_Debit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		$Journal_Credit_conditions= [];
		$Journal_Credit_conditions['credit'] = $account_head_id;
		// $Journal_Credit_conditions['credit !='] = 19;
		$Journal_Credit_conditions['debit !='] = 19;
		$Journal_Credit_conditions['flag'] = '1';
		$Journal_Credit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		
			$Journal_Debit=$this->Journal->find('first',array(
							'conditions'=>$Journal_Debit_conditions,
							'fields'=>array( 'Journal.total_amount'),
						));
			$Journal_Credit=$this->Journal->find('first',array(
							'conditions'=>$Journal_Credit_conditions,
							'fields'=>array( 'Journal.total_amount'),
						));
		
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
public function General_Journal_Debit_N_Credit_function2($account_head_id,$from_date,$to_date)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$customer_id=$this->SystemParameter->field('value',array('id'=>11));
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$Journal_Debit_conditions= [];
		$Journal_Debit_conditions['debit'] = $account_head_id;
		// $Journal_Debit_conditions['credit !='] =19;
		$Journal_Debit_conditions['flag'] = '1';
		$Journal_Debit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		$Journal_Credit_conditions= [];
		$Journal_Credit_conditions['credit'] = $account_head_id;
		// $Journal_Credit_conditions['debit !='] = 19;
		$Journal_Credit_conditions['flag'] = '1';
		$Journal_Credit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
			$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
			if($AccountHead['AccountHead']['acc_sub_group_id']==$customer_id){
		$Journal_Debit=$this->Journal->find('first',array(
						'conditions'=>$Journal_Debit_conditions,
						'fields'=>array( 'Journal.total_amount'),
					));
		$Journal_Credit=$this->Journal->find('first',array(
						'conditions'=>array($Journal_Credit_conditions,'debit !='=>'13'),
						'fields'=>array( 'Journal.total_amount'),
					));
	}else{
		$Journal_Debit=$this->Journal->find('first',array(
						'conditions'=>$Journal_Debit_conditions,
						'fields'=>array( 'Journal.total_amount'),
					));
		$Journal_Credit=$this->Journal->find('first',array(
						'conditions'=>$Journal_Credit_conditions,
						'fields'=>array( 'Journal.total_amount'),
					));
	}
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
public function General_Journal_Openging_Debit_N_Credit_function2($account_head_id,$from_date)
	{	
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$customer_id=$this->SystemParameter->field('value',array('id'=>11));
		$AccountHead=$this->AccountHead->findById($account_head_id);
		$account_single['name']=$AccountHead['AccountHead']['name'];
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
			if($AccountHead['AccountHead']['acc_sub_group_id']==$customer_id){
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$account_head_id,
				'credit !='=>19,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>array(
				'credit'=>$account_head_id,
				'debit !='=>array('19','13'),
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
	}else if($account_head_id=='19'){
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$account_head_id,
				// 'credit !='=>19,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>array(
				'credit'=>$account_head_id,
				// 'debit !='=>19,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
	}else{
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>array(
				'debit'=>$account_head_id,
				 'credit !='=>19,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>array(
				'credit'=>$account_head_id,
				 'debit !='=>19,
				'flag=1',
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
	}
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
public function Product_id_list_function()
{
	$Product_list=[];
	$SaleItem_DISTINCT_Product=$this->SaleItem->find('all',array('conditions'=>array('Sale.status>=2'),'fields'=>array('DISTINCT SaleItem.product_id')));
	$SalesReturnItem_DISTINCT_Product=$this->SalesReturnItem->find('all',array('conditions'=>array('SalesReturn.status>=2'),'fields'=>array('DISTINCT SalesReturnItem.product_id')));
	$PurchaseReturnItem_DISTINCT_Product=$this->PurchaseReturnItem->find('all',array('conditions'=>array('PurchaseReturn.status>=2'),'fields'=>array('DISTINCT PurchaseReturnItem.product_id')));
	$PurchasedItem_DISTINCT_Product=$this->PurchasedItem->find('all',array('conditions'=>array('Purchase.status>=2'),'fields'=>array('DISTINCT PurchasedItem.product_id')));
	foreach ($PurchasedItem_DISTINCT_Product as $key => $value) {$Product_list[$value['PurchasedItem']['product_id']]=$value['PurchasedItem']['product_id'];}
	foreach ($PurchaseReturnItem_DISTINCT_Product as $key => $value) {$Product_list[$value['PurchaseReturnItem']['product_id']]=$value['PurchaseReturnItem']['product_id'];}
	foreach ($SaleItem_DISTINCT_Product as $key => $value) {$Product_list[$value['SaleItem']['product_id']]=$value['SaleItem']['product_id'];}
	foreach ($SalesReturnItem_DISTINCT_Product as $key => $value) {$Product_list[$value['SalesReturnItem']['product_id']]=$value['SalesReturnItem']['product_id'];}
	return $Product_list;
}
public function get_accounthead_by_sub_group_ajax()
{
	$requestData=$this->request->data;
	$sub_group_name=$requestData['sub_group_name'];
	$AccSubGroup=$this->AccSubGroup->findByName(trim($sub_group_name));
	$Data=[];
	$totalData=1;
	$totalFiltered=$totalData;
	if($AccSubGroup)
	{
		$from_date=date('Y-m-d',strtotime($requestData['from_date']));
		$to_date=date('Y-m-d',strtotime($requestData['to_date']));
		
		$gst_on_sale_account_head_id=9;
		$gst_on_purchase_account_head_id=8;		
		$type_name=$AccSubGroup['AccSubGroup']['main_group_id'];
		$sub_group_id=$AccSubGroup['AccSubGroup']['id'];
		$sub_group_name=$AccSubGroup['AccSubGroup']['name'];
		$AccoutHead_list=$this->AccountHead_list_By_SubGroup_id($sub_group_id);
		$return=[];
		foreach ($AccoutHead_list as $key => $name) {
			if($key!=$gst_on_purchase_account_head_id && $key!=$gst_on_sale_account_head_id)
			{
				$AccountHead=$this->AccountHead->findById($key,['AccountHead.id','AccountHead.opening_balance','AccountHead.name','AccountHead.created_at']);
				$single['name']=$name;
				$single['amount']=0;
				if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
				{
					$single['amount']=$AccountHead['AccountHead']['opening_balance'];
				}
				$single['first']=0;
				$single['second']=0;
				$function_value_return=$this->General_Journal_Debit_N_Credit_function1($key,$from_date,$to_date);
				if($type_name==5)
				{
					$Accrued_amount=0;
					$Advance_amount=0;
					$name=explode(' ',$name);
					array_pop($name);
					$name=implode(' ', $name);
					$single['amount']+=$function_value_return['debit']-$function_value_return['credit'];
					$Advance=$this->AccountHead->findByName($name.' ADVANCE');
					if(!empty($Advance))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function1($Advance['AccountHead']['id'],$from_date,$to_date);
						$Advance_amount=$Advance['AccountHead']['opening_balance'];
						$Advance_amount+=$return_function['credit']-$return_function['debit'];
					}
					$Accrued=$this->AccountHead->findByName($name.' ACCRUED');
					if(!empty($Accrued))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function1($Accrued['AccountHead']['id'],$from_date,$to_date);
						$Accrued_amount=$Accrued['AccountHead']['opening_balance'];
						$single['amount']+=$Accrued_amount;
						$Accrued_amount+=$return_function['debit']-$return_function['credit'];
					}
					$single['first']=$Accrued_amount;
					$single['second']=$Advance_amount;
					$single['total']=$single['amount'];
					if($single['total']<$single['first'])
					{
						$single['total']+=$single['first'];
					}
					elseif($single['total']==0 && $single['first']!=0)
					{
						$single['second']+=$single['first']*-1;
						$single['first']=0;
					}
				}
				if($type_name==3)
				{
					$outstanding_amount=0;
					$prePaid_amount=0;
					$name=explode(' ',$name);
					array_pop($name);
					$name=implode(' ', $name);
					$single['amount']+=$function_value_return['debit'];
					$PrePaid=$this->AccountHead->findByName($name.' PREPAID');
					if(!empty($PrePaid))
					{
						$return_function=$this->General_Journal_Debit_N_Credit_function($PrePaid['AccountHead']['id'],'0000-00-00',$to_date);
						$prePaid_amount=$PrePaid['AccountHead']['opening_balance'];
						$prePaid_amount+=$return_function['debit']-$return_function['credit'];
					}
					$Outstanding=$this->AccountHead->findByName($name.' OUTSTANDING');
					if(!empty($Outstanding))
					{
						$return_function=$this->General_Journal_Openging_Debit_N_Credit_function($Outstanding['AccountHead']['id'],$from_date);						
						$outstanding_amount=$Outstanding['AccountHead']['opening_balance'];
						$outstanding_amount+=$return_function['credit']-$return_function['debit'];
						$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
						$Journal_Debit=$this->Journal->find('first',array(
							'conditions'=>array(
								'debit'=>$Outstanding['AccountHead']['id'],
								'flag=1',
								'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
							),
							'fields'=>array( 'Journal.total_amount'),
						));
						$Journal_Credit=$this->Journal->find('first',array(
							'conditions'=>array(
								'credit'=>$Outstanding['AccountHead']['id'],
								'flag=1',
								'Journal.date between ? and ?'=>array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))),
							),
							'fields'=>array( 'Journal.total_amount'),
						));
						$outstanding_amount+=$Journal_Credit['Journal']['total_amount'];
						$outstanding_amount-=$Journal_Debit['Journal']['total_amount'];
						$outstanding_amount=number_format($outstanding_amount,2,'.','');
					}
					$single['second']+=$prePaid_amount;	
					$single['total']=$single['amount'];
					if($outstanding_amount!=0)
					{
						$single['first']+=$outstanding_amount;
						$single['total']+=$outstanding_amount;
					}
				}
				$single['amount']=number_format($single['amount'],2,'.','');
				$single['first']=number_format($single['first'],2,'.','');
				$single['second']=number_format($single['second'],2,'.','');
				$single['total']=number_format($single['total'],2,'.','');
				if($single['first'] || $single['second'] || $single['total'] )
				{
					$return[]=$single;
				}
			}
		}
		$Data=$return;
	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData), 
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
	);
	echo json_encode($json_data);
	exit;
}
public function BrandWiseReport()
{
	$executives=$this->Executive->find('list',array('conditions' => 'block!=1','fields'=>array('id','name')));
// $executives[0]='No Executive';
	$this->set(compact('executives'));
//	pr($executives);
//	exit;
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');

}
public function brand_wise_report_ajax()
{
	$conditions=array();
	$conditions_sale=array();
	$data=$this->request->data;
	if(!empty($data['executive_id'])){
		$conditions['Executive.id']=$data['executive_id'];
	}
	$conditions['Executive.block !=']=1;

	$Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
//'Customer.circle_id',
//'CustomerCircle.name',
			)
		));
	$return=array();
	$return['row']='';
	$return['tfoot']='';
	$return['executives']="All";
	if(!empty($data['executive_id'])){
		$Executivename=$this->Executive->findById($data['executive_id']);
		$return['executives']=$Executivename['Executive']['name'];
	}
// if($Customer)
// {
//  foreach ($Customer as $key => $customer_name) {
	$Brand=$this->Brand->find('list');
//pr($Brand);
//exit;
	$grand_total=0;
	$quantity_total=0;

	foreach($Brand as $brand_id => $brand_name)
	{

// $cutomertype['single_customer']=[];    //$AccountHead=$this->AccountHead->findById($customer_name['AccountHead']['id']);
		$Executive=$this->Executive->find('all',[
			'conditions'=>$conditions,
			]);
//$this->SaleItem->virtualFields = array('total_amount' => "SUM(SaleItem.net_value)");
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$conditions_sale['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
		$conditions_sale['Sale.flag']=1;
		$conditions_sale['Sale.status']=2;
		$conditions_sale['Brand.id']=$brand_id;

		foreach ($Executive as $keyE => $valueE) {
			$conditions_sale['Sale.executive_id']=$valueE['Executive']['id'];
			$SaleItem=$this->SaleItem->find('all',array(
				'joins'=>array(
					array(
						'table'=>'brands',
						'alias'=>'Brand',
						'type'=>'INNER',
						'conditions'=>array('Brand.id=Product.brand_id')
						),
// array(
// 	'table'=>'sub_brands',
// 	'alias'=>'SubBrand',
// 	'type'=>'INNER',
// 	'conditions'=>array('SubBrand.id=Product.sub_brand_id')
// ),
					),
				'conditions'=>$conditions_sale,
				'fields'=>array(
//'total_amount',
					'Product.name',
					'Brand.name',
//'SubBrand.name',
					'Brand.id',
					'SaleItem.net_value',
					'SaleItem.quantity',
					'SaleItem.id',
					'Sale.id',

					),
				)); 
// pr($SaleItem);exit;
			$totalamount=0;
			$quantity=0;
			foreach($SaleItem as $key => $value2)
			{
				$totalamount+=$value2['SaleItem']['net_value'];
				$quantity+=$value2['SaleItem']['quantity'];
			}
			if($totalamount){
				$return['row'].='<tr class="blue-pd" id="parent-'.$brand_id.'">';
				$return['row'].='<td>'.$valueE['Executive']['name'].'</td>';
				$return['row'].='<td>'.$brand_name.'</td>';
				$return['row'].='<td></td>';
				$return['row'].='<td>'.$quantity.'</td>';
				$return['row'].='<td>'.round($totalamount,3).'</td>';
				$return['row'].='</tr>';
				$grand_total+=$totalamount;
				$quantity_total+=$quantity;
				foreach($SaleItem as $key => $value1)
				{
					$return['row'].='<tr class="toggle_rows blue-pd child-parent-'.$value1['Brand']['id'].'">';
					$return['row'].='<td></td>';
					$return['row'].='<td>'.$value1['Brand']['name'].'</td>';
					$return['row'].='<td>'.$value1['Product']['name'].'</td>';
//$return['row'].='<td>'.$value1['SubBrand']['name'].'</td>';
					$return['row'].='<td>'.$value1['SaleItem']['quantity'].'</td>';
					$return['row'].='<td>'.round($value1['SaleItem']['net_value'],3).'</td>';
					$return['row'].='</tr>';
				}
			}
		}
	}

	$return['tfoot'].='<tr>';
	$return['tfoot'].='<td></td>';
	$return['tfoot'].='<td></td>';
	$return['tfoot'].='<td><h3>Total</h3></td>';
	$return['tfoot'].='<td><h3>'.$quantity_total.'<h3></td>';
	$return['tfoot'].='<td><h3>'.$grand_total.'<h3></td>';
	$return['tfoot'].='</tr>';
	$return['result']='Success';
	echo json_encode($return); exit;

}
public function BrandExport($from_date,$to_date)
{
	$date  =date('Y-m-d h:i:sa');
	$this->response->download("brand_export.csv");
	$brands=$this->Brand->find('all',array('fields'=>array('id','name')));
	$conditions=array();
	$conditions['Executive.block !=']=1;
	$conditions_sale=array();
	$Brand=$this->Brand->find('list');
	$grand_total=0;
	$quantity_total=0;
	$Array=[];
	$Brand_count=count($Brand);

	$total=[];
	foreach($Brand as $brand_id => $brand_name)
	{
		$column_total=0;
		$Executive=$this->Executive->find('all',[
			'conditions'=>$conditions,
			]);
		$Executive_count=count($Executive);
		$from_date=date('Y-m-d',strtotime($from_date));
		$to_date=date('Y-m-d',strtotime($to_date));
		$conditions_sale['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
		$conditions_sale['Sale.flag']=1;
		$conditions_sale['Sale.status']=2;
		$conditions_sale['Brand.id']=$brand_id;
		foreach ($Executive as $keyE => $valueE) {
			$conditions_sale['Sale.executive_id']=$valueE['Executive']['id'];
			$SaleItem=$this->SaleItem->find('all',array(
				'joins'=>array(
					array(
						'table'=>'brands',
						'alias'=>'Brand',
						'type'=>'INNER',
						'conditions'=>array('Brand.id=Product.brand_id')
						),
// array(
// 	'table'=>'sub_brands',
// 	'alias'=>'SubBrand',
// 	'type'=>'INNER',
// 	'conditions'=>array('SubBrand.id=Product.sub_brand_id')
// ),
					),
				'conditions'=>$conditions_sale,
				'fields'=>array(
					'Product.name',
					'Brand.name',
//'SubBrand.name',
					'Brand.id',
					'SaleItem.net_value',
					'SaleItem.quantity',
					'SaleItem.id',
					'Sale.id',
					'Sale.executive_id'
					),
				)); 
			$totalamount=0;
			foreach($SaleItem as $key => $value2)
			{
				$totalamount+=$value2['SaleItem']['net_value'];
				$column_total+=$value2['SaleItem']['net_value'];
			}	
			$single['executive']=$valueE['Executive']['name'];
			$single['brand']=$brand_name;
			$single['total']=$totalamount;
			$Array[]=$single;
		}
		$array_total['brand']=$brand_name;
		$array_total['total']=$column_total;
		$total[]=$array_total;
	}
	$new_array=[];
	foreach($Array as $key3 =>$value3){
		$new_array[$value3['executive']][]=$value3['total'];
	}
	$this->set('column_total',$total);
	$this->set('brand',$brands);
	$this->set('amount',$new_array);
	$this->set('Brand_count',$Brand_count);
	$this->layout = 'ajax';
	return false;
}
public function SupplierAgeingReport()
{
	$Party_list=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('AccountHead.acc_sub_group_id'=>1)));
	$this->set('Party_list',$Party_list);
	$this->request->data['from_date']=date('d-m-Y');

}
public function supplier_aging_report_ajax()
{
	$conditions=[];
	$data=$this->request->data;
	if($data['party_id'])
	{
		$conditions['Party.account_head_id']=$data['party_id'];

	}
	$Parties=$this->Party->find('all',array(
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			),
		'order' => array('Party.id' => 'ASC'),
		'conditions'=>$conditions,
		)
	);
	$data['row']='';
	$data['tfoot']='';
	$data['result']='Error';
	$balance=0;
	$Balance_Total=0;
	$to_date1=date('d-m-Y');
	$from1=$data['from_date'];
	$to1=date('d-m-Y');
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($to_date1));
	if($Parties)
	{
		$invoice_array=array();
		$total_opening=0;
		foreach ($Parties as $key => $value)
		{
			$Purchases = $this->Purchase->find('all', array(
				'conditions' => array(
					'Purchase.account_head_id' =>$value['AccountHead']['id'],
//'Purchase.date_of_delivered between ? and ?'=>array($from,$to),
					'Purchase.flag'=>1,
					'Purchase.status'=>array(2,3),
					),
				'order' => array('Purchase.date_of_purchase' => 'ASC'),
				'fields' => array(
					'Purchase.*',
					'AccountHead.name',
					)
				));
			$Journal_voucher=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.debit'=>$value['AccountHead']['id'],
					'NOT' => array(
						'Journal.remarks LIKE' => 'Purchase Invoice No :%',
						),
					'Journal.flag=1',
					),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
					),
				));
			$voucher_amount=0;
			$voucher_balance=0;
			foreach ($Journal_voucher as $key3 => $value_amount) {

				$voucher_amount+=$value_amount;
			}
			$voucher_bounce=0;
			$Journal_bounce=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.credit'=>$value['AccountHead']['id'],
					'NOT' => array(
						'Journal.remarks LIKE' => 'Purchase Invoice No :%',
					),
					'Journal.flag=1',
				),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
				),
			));
			foreach ($Journal_bounce as $key4 => $value_bounce) {
				$voucher_bounce+=$value_bounce;
			}
			$voucher_amount=$voucher_amount-$voucher_bounce;
			foreach ($Purchases as $keyj =>$valuej)
			{
				$PurchasedItem=$this->PurchasedItem->find('list',array(
					'conditions'=>array('PurchasedItem.purchase_id'=>$valuej['Purchase']['id']),
					'fields'=>array('PurchasedItem.discount')
					));
			}
			$invoice_array_single=array();
			if (!empty($Purchases)) {
				$invoice_array_single['due'] = array();
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['invoice_amount'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['invoice_date'] = array();
				$invoice_array_single['delivered_date'] = array();
				$invoice_array_single['date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{ 
					$from12=date("Y-m-d", strtotime($data['from_date']));
					$outstanding_amount_date1=date("Y-m-d", strtotime($outstanding_amount_date));

					if($outstanding_amount_date1>=$from12){
						array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['invoice_no'], 'Opening Balance');
						array_push($invoice_array_single['invoice_date'], date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['balance'],ROUND($outstanding_amount,3));
						array_push($invoice_array_single['invoice_amount'],ROUND($outstanding_amount,3));
						$now = time();
						$expected_days_diff=$now-strtotime($outstanding_amount_date);
						$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
						$diff_month=intval($diff_day/30);
						$actual_diff_month=$diff_day/30;
						$actual_days=$actual_diff_month-$diff_month;
						$actual_days*=30;
						if($diff_month)
						{
							array_push($invoice_array_single['due'],$diff_month.' Month'.' '.$actual_days.' Days'); 	
						}
						else
						{
							array_push($invoice_array_single['due'],$diff_day.' Days'); 	

						}
					}

				}
				foreach ($Purchases as $keySC2 => $valueSC2) {
					$balance_amount=$valueSC2['Purchase']['grand_total'];
					if($voucher_amount)
					{
						if($balance_amount<$voucher_amount)
						{
							$voucher_balance=$balance_amount;
							$balance_amount=0;
							$voucher_amount-=$voucher_balance;
						}
						else
						{
							$balance_amount-=$voucher_amount;
							$voucher_amount=0;
						}
					}
					if($valueSC2['Purchase']['grand_total']>0)
					{
						if($balance_amount){
							$check_date=$valueSC2['Purchase']['date_of_delivered'];
							$check_date=$valueSC2['Purchase']['date_of_purchase'];
							$from11=date("Y-m-d", strtotime($data['from_date']));
							$to11=date('Y-m-d');
							if($check_date>=$from11){
								array_push($invoice_array_single['delivered_date'], date("d-m-Y", strtotime($valueSC2['Purchase']['date_of_purchase'])));
								array_push($invoice_array_single['invoice_no'], $valueSC2['Purchase']['invoice_no']);
								array_push($invoice_array_single['invoice_date'], date("d-m-Y", strtotime($valueSC2['Purchase']['date_of_delivered'])));
								array_push($invoice_array_single['balance'],ROUND($balance_amount,3)) ; 
								array_push($invoice_array_single['invoice_amount'],$valueSC2['Purchase']['grand_total']) ;  
								$now = time();
								$expected_days_diff=$now-strtotime($valueSC2['Purchase']['date_of_purchase']);
								$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
								$diff_month=intval($diff_day/30);
								$actual_diff_month=$diff_day/30;
								$actual_days=$actual_diff_month-$diff_month;
								$actual_days*=30;
								if($diff_month)
								{
									array_push($invoice_array_single['due'],$diff_month.' Month'.' '.$actual_days.' Days'); 	
								}
								else
								{
									array_push($invoice_array_single['due'],$diff_day.' Days'); 	

								}
							}
// else if($check_date<$from11){

// 	$total_opening+=$balance_amount;
// }
						}

					}
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      
			}
			else
			{

				$invoice_array_single['due'] = array();
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['invoice_amount'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['delivered_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{
					$from12=date("Y-m-d", strtotime($data['from_date']));
					$outstanding_amount_date1=date("Y-m-d", strtotime($outstanding_amount_date));

					if($outstanding_amount_date>=$from1){
						array_push($invoice_array_single['delivered_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['invoice_no'], 'Opening Balance');
						array_push($invoice_array_single['invoice_date'], date("d-m-Y", strtotime($outstanding_amount_date)));
						array_push($invoice_array_single['balance'],ROUND($outstanding_amount,3));
						array_push($invoice_array_single['invoice_amount'],ROUND($outstanding_amount,3));
						$now = time();
						$expected_days_diff=$now-strtotime($outstanding_amount_date);
						$diff_day=floor($expected_days_diff / (60 * 60 * 24))+1;
						$diff_month=intval($diff_day/30);
						$actual_diff_month=$diff_day/30;
						$actual_days=$actual_diff_month-$diff_month;
						$actual_days*=30;
						if($diff_month)
						{
							array_push($invoice_array_single['due'],$diff_month.' Month'.' '.$actual_days.' Days'); 	
						}
						else
						{
							array_push($invoice_array_single['due'],$diff_day.' Days'); 	

						}
					}
// else if($outstanding_amount_date1<$from12){

// 	$total_opening+=$outstanding_amount;
// }
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      
			}
		}
		$grand_total=0;
		$data['result']='Success';
		$data['balance']=ROUND($total_opening,3);
		$data['data']=$invoice_array;
	}
	echo json_encode($data); exit;
}
public function ProductWise()
{

	$this->set('from',date("d-m-Y", strtotime('-1 day')));
	$this->set('to',date("d-m-Y"));
	$executives=$this->Executive->find('list');
	$route_list=$this->Route->find('list');

	$customer_group=$this->CustomerGroup->find('list');
	$this->set('Product_type',$this->ProductType->find('list',array('fields'=>array('id','name'),'order'=>array('name ASC'),)));
	$this->set('Product',$this->Product->find('list',array('fields'=>array('id','name'),'order'=>array('name ASC'),)));
	$Brand=$this->Brand->find('list',array('fields'=>array('id','Brand.name'),'order'=>array('Brand.name ASC'),));
	$Brand['G']='GENERAL';
	ksort($Brand);
	$this->set('Brand',$Brand);
	$this->set(compact('executives'));
	$this->set(compact('route_list'));
	$this->set(compact('customer_group'));

}
// public function SaleDetailFunction($account_head_id)
// 	{
// 		try {
// 			//$from_date=date('Y-m-d',strtotime($from_date));
// 			//$to_date=date('Y-m-d',strtotime($to_date));
// 			$Sale=$this->Sale->find('all',array(
// 				'conditions'=>array(
// 					'Sale.status>1',
// 					'Sale.account_head_id'=>$account_head_id,
// 					'Sale.date_of_delivered between ? and ?' => array($from_date,$to_date)
// 				),
// 				'fields'=>array(
// 				),
// 			));
// 			$SalesReturn=$this->SalesReturn->find('all',array(
// 				'conditions'=>array(
// 					'SalesReturn.status >1',
// 					'SalesReturn.account_head_id'=>$account_head_id,
// 					'SalesReturn.date between ? and ?' => array($from_date,$to_date)
// 				),
// 				'fields'=>array(
// 				),
// 			));
// 			$AccountHead=$this->AccountHead->findById($account_head_id);
// 			$AccountingsController = new AccountingsController;
// 			$Journalopen=$AccountingsController->General_Journal_Opening_balance_Debit_N_Credit_With_Date_function($account_head_id,$from_date);
// 			$open=$Journalopen['debit']+$AccountHead['AccountHead']['opening_balance']-$Journalopen['credit'];
// 			$datedif = time() - strtotime($AccountHead['AccountHead']['created_at']);
// 			$day=floor($datedif / (60 * 60 * 24));
// 			$Journal_all=array(
// 				array(
// 					'id'=>'',
// 					'Date'=>date('Y-m-d',strtotime($from_date)),
// 					'Voucher No'=>'',
// 					'Particulars'=>'Opening Balance',
// 					'Debit'=>$open,
// 					'Credit'=>0,
// 					'Balance'=>$open,
// 					// 'Days'=>'',
// 				)
// 			);
// 			$Journal_cheque=$this->Journal->find('all',array(
// 				'conditions'=>array(
// 					'Journal.debit'=>$account_head_id,
// 					'Journal.remarks="cheque Bounce"',
// 					'Journal.flag=1',
// 				)
// 			));
// 			//pr($Journal_cheque);

// 			$Journal_credit=$this->Journal->find('all',array(
// 				'conditions'=>array(
// 					'Journal.credit'=>$account_head_id,
// 					'AccountHeadDebit.sub_group_id'=>array('1','2'),
// 					'Journal.flag=1',
// 					'Journal.date between ? and ?'=>array($from_date,$to_date),
// 				)
// 			));
// 			foreach ($Journal_credit as $key => $value) 
// 			{
// 				$datedif = time() - strtotime($value['Journal']['date']);
// 				$day=floor($datedif / (60 * 60 * 24));
// 				$Journal_single['id']=$value['Journal']['id'];
// 				$Journal_single['Date']=$value['Journal']['date'];
// 				$Journal_single['Voucher No']=$value['Journal']['voucher_no'];
// 				$Journal_single['Particulars']=$value['AccountHeadDebit']['name'];
// 				$Journal_single['Debit']=0;
// 				$Journal_single['Credit']=$value['Journal']['amount'];
// 				$Journal_single['Balance']='';
// 				// $Journal_single['Days']=$day;
// 				$Journal_all[]=$Journal_single;
// 			}
// 			$discount_paid=135;
// 			$Journal_discount_paid=$this->Journal->find('all',array(
// 				'conditions'=>array(
// 					'Journal.debit'=>$discount_paid,
// 					'Journal.credit'=>$account_head_id,
// 					'Journal.flag=1',
// 					'Journal.date between ? and ?'=>array($from_date,$to_date),
// 				)
// 			));
// 			foreach ($Journal_discount_paid as $key => $value) 
// 			{
// 				$datedif = time() - strtotime($value['Journal']['date']);
// 				$day=floor($datedif / (60 * 60 * 24));
// 				$Journal_single['id']=$value['Journal']['id'];
// 				$Journal_single['Date']=$value['Journal']['date'];
// 				$Journal_single['Voucher No']=$value['Journal']['voucher_no'];
// 				$Journal_single['Particulars']=$value['AccountHeadDebit']['name'];
// 				$Journal_single['Debit']=0;
// 				$Journal_single['Credit']=$value['Journal']['amount'];
// 				$Journal_single['Balance']='';
// 				// $Journal_single['Days']=$day;
// 				$Journal_all[]=$Journal_single;
// 			}
// 			foreach ($Sale as $key => $value) {
// 				$datedif = time() - strtotime($value['Sale']['date_of_delivered']);
// 				$day=floor($datedif / (60 * 60 * 24));
// 				$Journal_single['id']='';
// 				$Journal_single['Date']=$value['Sale']['date_of_delivered'];
// 				$Journal_single['Voucher No']=$value['Sale']['invoice_no'];
// 				$Journal_single['Particulars']='Sale Account';
// 				$Journal_single['Debit']=$value['Sale']['grand_total'];
// 				$Journal_single['Credit']=0;
// 				$Journal_single['Balance']='';
// 				// $Journal_single['Days']=$day;
// 				$Journal_all[]=$Journal_single	;
// 			}
// 			foreach ($SalesReturn as $key => $value) {
// 				$datedif = time() - strtotime($value['SalesReturn']['date']);
// 				$day=floor($datedif / (60 * 60 * 24));
// 				$Journal_single['id']='';
// 				$Journal_single['Date']=$value['SalesReturn']['date'];
// 				$Journal_single['Voucher No']=$value['SalesReturn']['invoice_no'];
// 				$Journal_single['Particulars']='Sale Return';
// 				$Journal_single['Debit']=0;
// 				$Journal_single['Credit']=$value['SalesReturn']['grand_total'];
// 				$Journal_single['Balance']='';
// 				// $Journal_single['Days']=$day;
// 				$Journal_all[]=$Journal_single	;
// 			}
// 			foreach ($Journal_cheque as $key => $value) {
// 				$datedif = time() - strtotime($value['Journal']['date']);
// 				$day=floor($datedif / (60 * 60 * 24));
// 				$Journal_single['id']='';
// 				$Journal_single['Date']=$value['Journal']['date'];
// 				$Journal_single['Voucher No']='';
// 				$Journal_single['Particulars']='Cheque Bounce';
// 				$Journal_single['Debit']=$value['Journal']['amount'];
// 				$Journal_single['Credit']=0;
// 				$Journal_single['Balance']='';
// 				// $Journal_single['Days']=$day;
// 				$Journal_all[]=$Journal_single	;
// 			}
// 			$total_debit=0;
// 			$total_credit=0;
// 			$total_balance=0;
// 			$balance=0;
// 			$Journal_all = Set::sort($Journal_all, '{n}.id', 'asc');
// 			$Journal_all = Set::sort($Journal_all, '{n}.Date', 'asc');
// 			foreach ($Journal_all as $key => $value) {
// 				unset($Journal_all[$key]['id']);
// 				$total_debit+=$value['Debit'];
// 				$total_credit+=$value['Credit'];
// 				$balance=$total_debit-$total_credit;
// 				$total_balance=$balance;
// 				$Journal_all[$key]['Balance']=$balance;
// 				$Journal_all[$key]['Date']=date('d-M-Y',strtotime($value['Date']));
// 			}


// 			$foot_array=array(
// 				'Date'=>'',
// 				'Voucher No'=>'',
// 				'Particulars'=>'Total',
// 				'Debit'=>round($total_debit),
// 				'Credit'=>round($total_credit),
// 				'Balance'=>round($total_balance),
// 				// 'Days'=>'',
// 			);
// 			array_push($Journal_all, $foot_array);
// 			$header=array(
// 				'Date',
// 				'Voucher No',
// 				'Particulars',
// 				'Debit',
// 				'Credit',
// 				'Balance',
// 				// 'Days',
// 			);
// 			$width[0]=25;
// 			$width[1]=25;
// 			$width[2]=65;
// 			$width[3]=25;
// 			$width[4]=25;
// 			$width[5]=25;
// 			// $width[6]=12;
// 			require('fpdf/fpdf.php');
// 			$pdf = new FPDF('p');
// 		// $pdf->SetFont('Arial','B',14);
// 			$pdf->AddPage();
// 			$pdf->setFont("Arial",'B','9');           
// 			$pdf->Image("img/liyalams_logo.jpeg",85,5,-300);
// 			$pdf->Text(48, 20, '15/85 A.VALIYORA P O,VENGARA. MALAPPURAM-676304,TEL:0494 2450242');
// 			$pdf->Text(76, 24, 'www.liyalams.com,liyalams@gmail.com');
// 			$pdf->Line(1,26,209,26);
// 			$pdf->SetFont('Arial','B','11');
// 			//$pdf->Text(10, 33, 'Ledger Report Of '.$AccountHead['AccountHead']['name'].' for the Period From '.date('d-M-Y',strtotime($from_date)).' To '.date('d-M-Y',strtotime($to_date)));
// 			$pdf -> SetY(37); 
// 			for($i=0;$i<count($header);$i++)
// 				$pdf->Cell($width[$i],7,$header[$i],1,0,'C');
// 			$pdf->Ln();
// 			$pdf->SetFillColor(224,235,255);
// 			$fill = false;
// 			$pdf->SetFont('Arial','','10');
// 			foreach($Journal_all as $row)
// 			{
// 				$i=0;
// 				foreach($row as $col){
// 					$pdf->Cell($width[$i],6,$col,'LR',0,'L',$fill);
// 				// $pdf->Cell($width[$i],6,$col,1,0,'L',$fill);
// 					$i++;
// 				}
// 				$pdf->Ln();
// 				$fill = !$fill;
// 			}
// 			$pdf->Cell(array_sum($width),0,'','T');
// 			$pdf->Output();
// 			exit;
// 			$return['result']='Success';
// 		} catch (Exception $e) {
// 			$return['result']=$e->getMessage();
// 		}
// 		return $return;
// 	}
public function PurchaseItemWiseFilter(){
	$this->Session->delete('purchase_item_conditions');
	$conditions=array();
	if(!empty($this->request->data['product_type_id']))
	{
		$conditions['ProductType.id']=$this->request->data['product_type_id'];
	}
	if(!empty($this->request->data['product_id']))
	{
		$conditions['Product.id']=$this->request->data['product_id'];
	}
	if(!empty($this->request->data['from_date']) && !empty($this->request->data['to_date']))
	{
		$from_date=date('Y-m-d',strtotime($this->request->data['from']));
		$to_date=date('Y-m-d',strtotime($this->request->data['to']));
		$conditions['Purchase.date_of_delivered between ? and ?']=array($from_date,$to_date);
	}
	pr($conditions);
	$conditions['Purchase.flag' ]=1;	
	$conditions['Purchase.status' ]=2;	
	$this->Session->write('purchase_item_conditions',$conditions);
	echo json_encode(array('null'));
	exit;
}
public function ProductLog($name=null)
{
	$this->Product->unbindModel(array('hasMany' => array('SalesReturnItem','SaleItem','PurchasedItem','PurchaseReturnItem','UnwantedList',)));
	$this->Product->virtualFields = array(
		'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
		);
	$products=$this->Product->find('list',array(
		'fields'=>array(
			'Product.id',
			'product_name',
			)
		));
	$this->request->data['from_date']= date('d-m-Y',strtotime('first day of this month'));
	$this->request->data['to_date']= date('d-m-Y',strtotime('last day of this month'));
//$products=$this->Product->find('list');
	$warehouses=$this->Warehouse->find('list');
	$this->set(compact('products','warehouses'));
}
public function GetProductLogAjax()
{
		$requestData=$this->request->data;
		$columns=[];
		$columns[]='StockLog.id';
		$columns[]='StockLog.updated_at';
		$columns[]='StockLog.remark';
		$columns[]='StockLog.customer_name';
		$columns[]='Product.name';
		$columns[]='Warehouse.name';
		$columns[]='StockLog.quantity_in';
		$columns[]='StockLog.quantity_out';
		$columns[]='StockLog.quantity';
		
		$conditions=[];
		if(isset($requestData['warehouse_id']))
		{
			if($requestData['warehouse_id'])
				$conditions['StockLog.warehouse_id']=$requestData['warehouse_id'];
		}
		if(isset($requestData['product_id']))
		{
			if($requestData['product_id'])
				$conditions['StockLog.product_id']=$requestData['product_id'];
		}
		$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions['StockLog.updated_at between ? and ?']=[$from_date,$to_date];
		$totalData=$this->StockLog->find('count',['conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'StockLog.remark LIKE' =>'%'. $q . '%',
    	    'Product.code LIKE' =>'%'. $q . '%',
    		'Product.name LIKE' =>'%'. $q . '%',
    		'Warehouse.name LIKE' =>'%'. $q . '%',
    		);
    	$totalFiltered=$this->StockLog->find('count',[
    		'conditions'=>$conditions,
    		]);
    }
    $Data=$this->StockLog->find('all',array(
    	'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	//'order'=>$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
    	'fields'=>array(
    		'StockLog.*',
    		 'Warehouse.name',
    		 'Product.name',
    		 'Product.code'
    		)
    	));
    foreach ($Data as $key => $value) {
    	$remarks=explode(" --", $value['StockLog']['remark']);
    	$Data[$key]['StockLog']['remark']=$remarks[0];
    	$Data[$key]['StockLog']['customer_name']="";
    	if($remarks[0]=="Sale")
    	{
    	$remarks2=explode("(", $remarks[1]);
    	$Sale=$this->Sale->findByInvoiceNo($remarks2[0]);
    	$customer_name=$this->AccountHead->field('AccountHead.name',['AccountHead.id'=>$Sale['Sale']['account_head_id']]);
    	$Data[$key]['StockLog']['customer_name']=$customer_name;
    	}
    	if($remarks[0]=="SaleReturn")
    	{
    		$remarks2=explode("(", $remarks[1]);
    	$SaleReturn=$this->SaleReturn->findByInvoiceNo($remarks2[0]);
    	$customer_name=$this->AccountHead->field('AccountHead.name',['AccountHead.id'=>$SaleReturn['SaleReturn']['account_head_id']]);
    	$Data[$key]['StockLog']['customer_name']=$customer_name;
    	}
    	if($remarks[0]=="Stock Transfer")
    	{
    		$remarks2=explode("(", $remarks[1]);
    	$StockTransfer=$this->StockTransfer->findByTransferNo($remarks2[0]);
    	if($StockTransfer['StockTransfer']['transfer_type']==4)
    	{
    		$Data[$key]['StockLog']['remark']="Van to Van Transfer";
    	}
    	else if($StockTransfer['StockTransfer']['transfer_type']==3)
    	{
    		$Data[$key]['StockLog']['remark']="Van to Branch Transfer";
    	}
    	else if($StockTransfer['StockTransfer']['transfer_type']==3)
    	{
    		$Data[$key]['StockLog']['remark']="Daily Stock Transfer";
    	}
    	else if($StockTransfer['StockTransfer']['transfer_type']==5)
    	{
    		$Data[$key]['StockLog']['remark']="Main to Branch Transfer";
    	}
    	else
    	{
           $Data[$key]['StockLog']['remark']="Stock Transfer";
    	}
    	}
    	$Data[$key]['StockLog']['quantity_in']=floatval($value['StockLog']['quantity_in']);
    	$Data[$key]['StockLog']['wholesale_price']=number_format($value['StockLog']['wholesale_price'],2,'.','');
    	$Data[$key]['StockLog']['quantity']=floatval($value['StockLog']['quantity']);
    	$Data[$key]['StockLog']['quantity_out']=floatval($value['StockLog']['quantity_out']);
      	$Data[$key]['StockLog']['updated_at']=date('d-m-Y',strtotime($value['StockLog']['updated_at']));
    }
$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;

}
public function SaleCollectionReportCustomer()
{
	$customers=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
//pr($customers);
//exit;
	$this->set(compact('customers'));
// $executives=$this->Executive->find('list');
// $this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}

public function SaleCollectionReportCustomerAjax()
{
	$data=$this->request->data;
//pr($data);
//exit;
	$conditions=[];
	if($data['customer_id'])
		$conditions['Sale.account_head_id']=$data['customer_id'];
	if($data['executive_id'])
		$conditions['Sale.executive_id']=$data['executive_id'];



	$Executive=$this->Sale->find('all',array(
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
//'Customer.circle_id',
//'CustomerCircle.name',
			)
		));
//pr($Customer);
//pr($conditions);
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($data['to_date']));
	$list=[];
	foreach ($Executive as $key => $value) {
		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array('grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)");
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
				'Sale.grand_total',
				),
			));
		if($value['Executive']['name']){$single['executive']=$value['Executive']['name'];}else{$single['executive']="No Executive";}

		$single['name']=$value['AccountHead']['name'];
		$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;
		$single['other_value']=$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;
		$single['net_value']=$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
		$single['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
		$single['total']=$SaleItem['SaleItem']['grand_total']?floatval($SaleItem['SaleItem']['grand_total']):0;
		$single['grandtotal']=$single['total']-$single['dicount_amount'];
		if($single['net_value'])
			$list[$value['AccountHead']['id']]=$single;
	}
	echo json_encode($list);
	exit;
}
public function collection_report_executive_ajax()
{
		$requestData=$this->request->data;
		$columns=[];
		$columns[]='date';
		$columns[]='name';
		$columns[]='amount';
		$conditions=[];
			$from_date=$requestData['from_date'];
			$to_date=$requestData['to_date'];
			if(!empty($requestData['executive_id']))
			{
			if($requestData['executive_id'])
			$conditions['ExecutiveRouteMapping.executive_id']=$requestData['executive_id'];
			}
			if(!empty($requestData['route_id']))
			{
			if($requestData['route_id'])
			$conditions['Customer.route_id']=$requestData['route_id'];
			}
		$this->Executive->unbindModel(array('hasMany' => array('Sale')));
		$totalData=$this->Customer->find('count',[
				'joins'=>array(
				array(
					'table'=>'executive_route_mappings',
					'alias'=>'ExecutiveRouteMapping',
					'type'=>'LEFT',
					'conditions'=>array('ExecutiveRouteMapping.route_id=Route.id')
					),
				),
				'group'=>array('Customer.account_head_id'),
		'conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'AccountHead.name LIKE' =>'%'. $q . '%',
    		);
    	$totalFiltered=$this->Customer->find('count',[
				'joins'=>array(
				array(
					'table'=>'executive_route_mappings',
					'alias'=>'ExecutiveRouteMapping',
					'type'=>'LEFT',
					'conditions'=>array('ExecutiveRouteMapping.route_id=Route.id')
					),
				),
				'group'=>array('Customer.account_head_id'),
    		'conditions'=>$conditions,
    		]);
    }
     //order only fields that are field of datatabse table
		$order= '';
    $Data=$this->Customer->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executive_route_mappings',
					'alias'=>'ExecutiveRouteMapping',
					'type'=>'LEFT',
					'conditions'=>array('ExecutiveRouteMapping.route_id=Route.id')
					),
			),
		'group'=>array('Customer.account_head_id'),
    	'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	'order'=>$order,
    	'fields'=>array(
    		    'Customer.route_id',
				'Customer.account_head_id',
				'AccountHead.id',
				'AccountHead.name',
    		)
    	));
      foreach ($Data as $key => $value)
       {
    	$get_collection_amount_return_value=$this->get_collection_amount_executive($value['Customer']['account_head_id'],$from_date,$to_date,2);	
			
				$Data[$key]['AccountHead']['amount']=number_format($get_collection_amount_return_value['sale_amount'],2,'.','');
				$Data[$key]['AccountHead']['date']=date('d-m-Y',strtotime($get_collection_amount_return_value['date']));
				$Data[$key]['AccountHead']['name']=$value["AccountHead"]["name"];	
				if($requestData['check_id']==1)
				{
					if($get_collection_amount_return_value['sale_amount']==0)
					{
					$totalData=$totalData-1;
					$totalFiltered=$totalFiltered-1;
					unset($Data[$key]);
					}	
				}
	}
    // //sorting total/balance/recieved
		if( $requestData['order'][0]['column'] >=0) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["AccountHead"][$columnname] == $b["AccountHead"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["AccountHead"][$columnname] < $b["AccountHead"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["AccountHead"][$columnname] > $b["AccountHead"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
	$Data = array_filter($Data);
	$Data = array_merge($Data);
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        => $Data
		);
echo json_encode($json_data); exit;
}

public function get_collection_amount_executive($account_head_id,$from_date=null,$to_date=null) {
	$conditions=array();
	if(!empty($from_date)){
		$conditions['Journal.date between ? and ?']=[date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date))];
	}
	$conditions['Journal.credit']=$account_head_id;
	$conditions['AccountHeadDebit.sub_group_id']=['1','2'];
	$conditions['Journal.flag']=1;
	$this->Journal->virtualFields = array( 'total_amount' => "SUM(Journal.amount)" );
	$Journal=$this->Journal->find('first',array('conditions'=>$conditions,
		'fields'=>array('Journal.total_amount','Journal.date')
		));
	$sale_total_amount=0;
	$collection_date=$Journal['Journal']['date'];
	$sale_total_amount=0;
	if($Journal['Journal']['total_amount'])
	{
		$sale_total_amount=floatval($Journal['Journal']['total_amount']);
	}
	$return['sale_amount']=$sale_total_amount;
	$return['date']=$collection_date;
	return $return;

}
public function SaleCollectionReportAll()
{
	$customers=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
//pr($customers);
//exit;
	$this->set(compact('customers'));
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));

	$routes=$this->Route->find('list'
//,array(
//'conditions'=>$conditions,
//'fields'=>array('total_net_value','total_sale'),
//)
		);
	$this->set(compact('routes'));

	$ProductType=$this->ProductType->find('list');
	$this->set(compact('ProductType'));
	$Product=$this->Product->find('list');
	$this->set(compact('Product'));
	$Brand=$this->Brand->find('list');
	$this->set(compact('Brand'));
// $executives=$this->Executive->find('list');
// $this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}

public function SaleCollectionReportAllAjax()
{
	$data=$this->request->data;
//pr($data);
//exit;
	$conditions=[];
	if($data['product_type_id'])
		$conditions['Product.product_type_id']=$data['product_type_id'];
	if($data['product_id'])
		$conditions['SaleItem.product_id']=$data['product_id'];
	if($data['executive_id'])
		$conditions['Sale.executive_id']=$data['executive_id'];

	if($data['customer_id'])
		$conditions['Sale.account_head_id']=$data['customer_id'];

	if($data['route_id'])
		$conditions['Customer.route_id']=$data['route_id'];
	if($data['brand_id'])
		$conditions['Product.brand_id']=$data['brand_id'];
//if($data['customer_id']){
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
	$Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			array(
				'table'=>'sale_items',
				'alias'=>'SaleItem',
				'type'=>'INNER',
				'conditions'=>array('Sale.id=SaleItem.sale_id')
				),
			array(
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'INNER',
				'conditions'=>array('SaleItem.product_id=Product.id')
				),
			array(
				'table'=>'product_types',
				'alias'=>'ProductType',
				'type'=>'INNER',
				'conditions'=>array('Product.product_type_id=ProductType.id')
				),
			array(
				'table'=>'brands',
				'alias'=>'Brand',
				'type'=>'INNER',
				'conditions'=>array('Product.brand_id=Brand.id')
				),

			),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
			'Product.*',
			'ProductType.*',
			'Sale.*',
			'SaleItem.*',
			'Brand.*',
//'Customer.circle_id',
//}
//'CustomerCircle.name',
			)
		));
//}
//pr($Customer);
//pr($conditions);
//exit;
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($data['to_date']));
	$list=[];
	foreach ($Customer as $key => $value) {
		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'SaleItem.product_id',
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array('grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)");
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
				'Sale.grand_total',
				),
			));
		if($value['Executive']['name']){$single['executive']=$value['Executive']['name'];}else{$single['executive']="No Executive";}

		$single['name']=$value['AccountHead']['name'];
		$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;
		$single['other_value']=$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;
		$single['net_value']=$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
		$single['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
		$single['total']=$SaleItem['SaleItem']['grand_total']?floatval($SaleItem['SaleItem']['grand_total']):0;
		$single['grandtotal']=$single['total']-$single['dicount_amount'];
		if($single['net_value'])
			$list[$value['AccountHead']['id']]=$single;
	}
	echo json_encode($list);
	exit;
}
public function NosaleCustomerReport()
{

	$this->request->data['from_date']=date("d-m-Y", strtotime('-1 day'));
	$this->request->data['to_date']=date("d-m-Y");
	$route_list=$this->Route->find('list',array('fields'=>array('id','name')));
	$this->set(compact('route_list'));

}
public function nosale_report_ajax()
{
		$requestData=$this->request->data;
		$columns=[];
		$columns[]='Customer.code';
		$columns[]='AccountHead.name';
		$columns[]='amount';
		$conditions_sale=[];
		$conditions=[];
			$conditions_sale['Sale.date_of_delivered between ? and ?']=[
			date('Y-m-d',strtotime($requestData['from_date'])),
			date('Y-m-d',strtotime($requestData['to_date']))
			];
			$conditions['Sale.date_of_delivered between ? and ?']=[
			date('Y-m-d',strtotime($requestData['from_date'])),
			date('Y-m-d',strtotime($requestData['to_date']))
			];
			$conditions_sale['Sale.status']=2;
			$conditions_sale['Sale.flag']=1;
			$conditions['Sale.status']=2;
			$conditions['Sale.flag']=1;
			if(!empty($requestData['route_id']))
			{
			if($requestData['route_id'])
			$conditions['Customer.route_id']=$requestData['route_id'];
			}
			    $this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$totalData=$this->Sale->find('all',[
			"joins"=>array(
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'inner',
				"conditions"=>array('AccountHead.id=Customer.account_head_id'),
				),
			),
		'conditions'=>$conditions,
		 'fields' => array(
			 'SUM(grand_total) as Total_sale'
		    ),
		    'group' => array(
		        'Sale.account_head_id HAVING Total_sale < '.$requestData['Amount'] 
		    )]);
		$totalData=count($totalData);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'AccountHead.name LIKE' =>'%'. $q . '%',
    		'Customer.code LIKE' =>'%'. $q . '%',
    		);
    	$totalFiltered=$this->Sale->find('all',[
    		"joins"=>array(
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'inner',
				"conditions"=>array('AccountHead.id=Customer.account_head_id'),
				),
			),
			'conditions'=>$conditions,
			 'fields' => array(
			 'SUM(grand_total) as Total_sale'
		    ),
		    'group' => array(
		        'Sale.account_head_id HAVING Total_sale < '.$requestData['Amount'] 
		    )
    		]);
    	$totalFiltered=count($totalFiltered);
    }
    //order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column']<=1)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
    $Data=$this->Sale->find('all',array(
    	"joins"=>array(
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'inner',
				"conditions"=>array('AccountHead.id=Customer.account_head_id'),
				),
			),
    	  'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	'order'=>$order,
    	   'fields' => array(
            'Customer.code',
			'Customer.account_head_id',
			'AccountHead.id',
			'AccountHead.name',
			 'SUM(grand_total) as Total_sale'
    ),
    'group' => array(
        'Sale.account_head_id HAVING Total_sale < '.$requestData['Amount'] 
    )
    	));
    foreach ($Data as $key => $value) {
	$Data[$key]['Sale']['amount']=number_format($value[0]['Total_sale'],2,'.','');	
    }
    // //sorting total/balance/recieved
		if($requestData['order'][0]['column'] >1) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["Sale"][$columnname] == $b["Sale"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["Sale"][$columnname] < $b["Sale"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["Sale"][$columnname] > $b["Sale"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
	$Data = array_filter($Data);
	$Data = array_merge($Data);
$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;

}

public function SaleCollectionReportProduct()
{
	$customers=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
//pr($customers);
//exit;
	$this->set(compact('customers'));
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));

	$routes=$this->Route->find('list'
//,array(
//'conditions'=>$conditions,
//'fields'=>array('total_net_value','total_sale'),
//)
		);
	$this->set(compact('routes'));

	$ProductType=$this->ProductType->find('list');
	$this->set(compact('ProductType'));
	$Product=$this->Product->find('list');
	$this->set(compact('Product'));
	$Brand=$this->Brand->find('list');
	$this->set(compact('Brand'));
	$Branch=$this->Branch->find('list');
	$this->set(compact('Branch'));
	$CustomerGroup=$this->CustomerGroup->find('list');
	$this->set(compact('CustomerGroup'));
// $executives=$this->Executive->find('list');
// $this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}
public function sale_report_excel($from_date,$to_date,$executive_id,$route_id,$product_id,$product_type_id,$brand_id,$branch_id){
	   $this->response->download("sale_product_wise_report.csv");
	   $conditions=[];
	$from_date=date('Y-m-d',strtotime($from_date));
	$to_date=date('Y-m-d',strtotime($to_date));
	if($product_id!='null')
	{
		if($product_id)
			$conditions['Product.id']=$product_id;	
	}
	if($product_type_id!='null')
	{
		if($product_type_id)
			$conditions['Product.product_type_id']=$product_type_id;	
	}
	if($branch_id!='null')
	{
		if($branch_id)
			$conditions['Sale.branch_id']=$branch_id;
	}
	if($route_id!='null')
	{
		if($route_id)
			$conditions['Customer.route_id']=$route_id;
	}
if($executive_id!='null')
	{
		if($executive_id)
			$conditions['Sale.executive_id']=$executive_id;
	}
	$conditions['Sale.flag']=1;
	$conditions['Sale.status']=2;
	$conditions['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
	$Product_array=[];
		$Data=$this->SaleItem->find('all',array(
		"joins"=>array(
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'LEFT',
				"conditions"=>array('Sale.executive_id=Executive.id'),
				),
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			),
		'conditions'=>$conditions,
            'order'=>array('Sale.id ASC'),		
            'fields'=>array(
			'Product.name',
			'Sale.id',
			'Sale.account_head_id',
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			'SaleItem.unit_price',
			'SaleItem.quantity',
			'SaleItem.total',
			'SaleItem.tax_amount',
			'Executive.name',
			'Route.name',
			)
		));
		foreach ($Data as $key => $value) 
		{
		$single['Route']=$value['Route']['name'];
		$single['Executive']=$value['Executive']['name'];
		$single['Product']=$value['Product']['name'];
		$single['quantity']=$value['SaleItem']['quantity'];
		$single['unit_price']=$value['SaleItem']['unit_price'];
		$single['tax_amount']=$value['SaleItem']['tax_amount'];
		$single['total']=$value['SaleItem']['total'];
        $single['customer_name']=$this->AccountHead->field('AccountHead.name',array('AccountHead.id'=>$value['Sale']['account_head_id']));
		$single['date_of_delivered']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
		$Product_array[]=$single;
	}
   $this->set('Product_array', $Product_array);
		$this->layout = 'ajax';
		return false;
		exit;
}
public function SaleCollectionReportProductAjax()
{
	$data=$this->request->data;
//pr($data);
//exit;
	$conditions=[];
	if($data['branch_id'])
		$conditions['Sale.branch_id']=$data['branch_id'];
	if($data['route_id'])
		$conditions['Customer.route_id']=$data['route_id'];
// if($data['customer_id'])
// 	$conditions['Sale.account_head_id']=$data['customer_id'];
// if($data['customer_group_id'])
//     $conditions['Customer.customer_group_id']=$data['customer_group_id'];
	if($data['executive_id'])
		$conditions['Sale.executive_id']=$data['executive_id'];
	if($data['product_type_id'])
		$conditions['Product.product_type_id']=$data['product_type_id'];

	if($data['brand_id'])
		$conditions['Product.brand_id']=$data['brand_id'];
	if($data['product_id'])
		$conditions['SaleItem.product_id']=$data['product_id'];

//if($data['customer_id']){
	$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
// 	$Customer=$this->SaleItem->find('all',array(
// 		'joins'=>array(
// 			array(
// 				'table'=>'sales',
// 				'alias'=>'Sale',
// 				'type'=>'INNER',
// 				'conditions'=>array('SaleItem.sale_id=Sale.id')
// 			),
// 			array(
// 				'table'=>'executives',
// 				'alias'=>'Executive',
// 				'type'=>'INNER',
// 				'conditions'=>array('Sale.executive_id=Executive.id')
// 			),
// 			array(
// 				'table'=>'products',
// 				'alias'=>'Product',
// 				'type'=>'INNER',
// 				'conditions'=>array('SaleItem.product_id=Product.id')
// 			),
// 			// array(
// 			// 	'table'=>'product_types',
// 			// 	'alias'=>'ProductType',
// 			// 	'type'=>'INNER',
// 			// 	'conditions'=>array('Product.product_type_id=ProductType.id')
// 			// ),
// 			// array(
// 			// 	'table'=>'brands',
// 			// 	'alias'=>'Brand',
// 			// 	'type'=>'INNER',
// 			// 	'conditions'=>array('Product.brand_id=Brand.id')
// 			// ),
// 			// array(
// 			// 	'table'=>'customer_groups',
// 			// 	'alias'=>'CustomerGroup',
// 			// 	'type'=>'INNER',
// 			// 	'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
// 			// ),
// 			// array(
// 			// 	'table'=>'branches',
// 			// 	'alias'=>'Branch',
// 			// 	'type'=>'INNER',
// 			// 	'conditions'=>array('Sale.branch_id=Branch.id')
// 			// ),

// 		),
// 		'conditions'=>$conditions,
// 		'fields'=>array(
// 			//'AccountHead.id',
// 			//'AccountHead.name',
// 			'Executive.id',
// 			'Executive.name',
// 			'Product.*',
// 			//'ProductType.*',
// 			'Sale.*',
// 			'SaleItem.*',
// 			//'Brand.*',
// 			//'Route.name',
// 			//'Branch.*',
// 			//'Customer.circle_id',
// //}
// //'CustomerCircle.name',
// 		)
// 	));

	$Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			array(
				'table'=>'sale_items',
				'alias'=>'SaleItem',
				'type'=>'INNER',
				'conditions'=>array('Sale.id=SaleItem.sale_id')
				),
			array(
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'INNER',
				'conditions'=>array('SaleItem.product_id=Product.id')
				),
			array(
				'table'=>'product_types',
				'alias'=>'ProductType',
				'type'=>'INNER',
				'conditions'=>array('Product.product_type_id=ProductType.id')
				),
			array(
				'table'=>'brands',
				'alias'=>'Brand',
				'type'=>'INNER',
				'conditions'=>array('Product.brand_id=Brand.id')
				),
			array(
				'table'=>'customer_groups',
				'alias'=>'CustomerGroup',
				'type'=>'INNER',
				'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
				),
// array(
// 	'table'=>'branches',
// 	'alias'=>'Branch',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.branch_id=Branch.id')
// ),

			),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
			'Product.*',
			'ProductType.*',
			'Sale.*',
			'SaleItem.*',
			'Brand.*',
			'Route.name',
//'Branch.*',
//'Customer.circle_id',
//}
//'CustomerCircle.name',
			)
		));
//}
//pr($Customer);
//pr($conditions);
//exit;
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($data['to_date']));
	$list=[];
	foreach ($Customer as $key => $value) {

		if(!empty($value['Executive']['name'])){$exec=$value['Executive']['name'];}else{$exec="No Executive";}

		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'SaleItem.product_id'=>$value['Product']['id'],
				'Sale.executive_id'=>$exec,
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'SaleItem.product_id',
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
//pr($SaleItem);
//exit;
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
//if($SaleItem){
		$this->Sale->virtualFields = array('grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)");
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$exec,
//'SaleItem.product_id'=>$value['Product']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
				'Sale.grand_total',
				),
			));
//pr($value);
//}


		$single['name']=$value['Product']['name'];
// pr($single['name']);
// exit;
		if($value['Executive']['name']){$single['executive']=$value['Executive']['name'];}else{$single['executive']="No Executive";}
		$single['route']=$value['Route']['name'];
		$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;

		$single['other_value']=$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;

		$single['net_value']=$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
//pr($single['net_value']);
//exit;
		$single['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
		$single['total']=$SaleItem['SaleItem']['grand_total']?floatval($SaleItem['SaleItem']['grand_total']):0;
		$single['grandtotal']=$single['total']-$single['dicount_amount'];
		if($single['net_value'])
			$list[$value['AccountHead']['id']]=$single;
	}
//}
//exit;
//pr($single);
//exit;
	echo json_encode($list);
	exit;
}

public function SaleCollectionReportCustomerwiseold()
{
	$customers=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
//pr($customers);
//exit;
	$this->set(compact('customers'));
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));

	$routes=$this->Route->find('list'
//,array(
//'conditions'=>$conditions,
//'fields'=>array('total_net_value','total_sale'),
//)
		);
	$this->set(compact('routes'));

	$ProductType=$this->ProductType->find('list');
	$this->set(compact('ProductType'));
	$Product=$this->Product->find('list');
	$this->set(compact('Product'));
	$Brand=$this->Brand->find('list');
	$this->set(compact('Brand'));
	$Branch=$this->Branch->find('list');
	$this->set(compact('Branch'));
	$CustomerGroup=$this->CustomerGroup->find('list');
	$this->set(compact('CustomerGroup'));
// $executives=$this->Executive->find('list');
// $this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}

public function SaleCollectionReportCustomerwiseAjax0ld()
{
	$data=$this->request->data;
//pr($data);
//exit;
	$conditions=[];
//if($data['branch_id'])
// $conditions['Sale.branch_id']=$data['branch_id'];
	if($data['route_id'])
		$conditions['Customer.route_id']=$data['route_id'];
	if($data['customer_id'])
		$conditions['Sale.account_head_id']=$data['customer_id'];
	if($data['customer_group_id'])
		$conditions['Customer.customer_group_id']=$data['customer_group_id'];
	if($data['executive_id'])
		$conditions['Sale.executive_id']=$data['executive_id'];
	if($data['product_type_id'])
		$conditions['Product.product_type_id']=$data['product_type_id'];

	if($data['brand_id'])
		$conditions['Product.brand_id']=$data['brand_id'];
	if($data['product_id'])
		$conditions['SaleItem.product_id']=$data['product_id'];

//if($data['customer_id']){
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
	$Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			array(
				'table'=>'sale_items',
				'alias'=>'SaleItem',
				'type'=>'INNER',
				'conditions'=>array('Sale.id=SaleItem.sale_id')
				),
			array(
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'INNER',
				'conditions'=>array('SaleItem.product_id=Product.id')
				),
			array(
				'table'=>'product_types',
				'alias'=>'ProductType',
				'type'=>'INNER',
				'conditions'=>array('Product.product_type_id=ProductType.id')
				),
			array(
				'table'=>'brands',
				'alias'=>'Brand',
				'type'=>'INNER',
				'conditions'=>array('Product.brand_id=Brand.id')
				),
			array(
				'table'=>'customer_groups',
				'alias'=>'CustomerGroup',
				'type'=>'INNER',
				'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
				),
// array(
// 	'table'=>'branches',
// 	'alias'=>'Branch',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.branch_id=Branch.id')
// ),

			),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
			'Product.*',
			'ProductType.*',
			'Sale.*',
			'SaleItem.*',
			'Brand.*',
			'Route.name',
//'Branch.*',
//'Customer.circle_id',
//}
//'CustomerCircle.name',
			)
		));
//}
//pr($Customer);
//pr($conditions);
//exit;
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to=date('Y-m-d',strtotime($data['to_date']));
	$list=[];
	foreach ($Customer as $key => $value) {
		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'SaleItem.product_id',
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array('grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)");
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.executive_id'=>$value['Executive']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
				'Sale.grand_total',
				),
			));
		if($value['Executive']['name']){$single['executive']=$value['Executive']['name'];}else{$single['executive']="No Executive";}

		$single['name']=$value['AccountHead']['name'];
		$single['route']=$value['Route']['name'];
		$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;
		$single['other_value']=$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;
		$single['net_value']=$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
		$single['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
		$single['total']=$SaleItem['SaleItem']['grand_total']?floatval($SaleItem['SaleItem']['grand_total']):0;
		$single['grandtotal']=$single['total']-$single['dicount_amount'];
		if($single['net_value'])
			$list[$value['AccountHead']['id']]=$single;
	}
	echo json_encode($list);
	exit;
}
public function SaleCollectionReportProductwise()
{
	$PermissionList = $this->Session->read('PermissionList');
	$menu_id = $this->Menu->field(
		'Menu.id',
		array('action ' => 'Reports/SaleItemWise'));
	// if(!in_array($menu_id, $PermissionList))
	// {
	// 	$this->Session->setFlash("Permission denied");
	// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
	// }
	$this->set('from',date("d-m-Y", strtotime('-1 day')));
	$this->set('to',date("d-m-Y"));
	$this->set('Product_type',$this->ProductType->find('list',array('fields'=>array('id','name'))));
	//$this->set('Product',$this->Product->find('list',array('fields'=>array('id','name'))));
	$this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
   // $Product_list=$this->Product->find('list',array('fields'=>['id','product_name']));
	$this->set('Product',$this->Product->find('list',array('fields'=>array('id','product_name'))));
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));

	$routes=$this->Route->find('list'
//,array(
//'conditions'=>$conditions,
//'fields'=>array('total_net_value','total_sale'),
//)
		);
	$this->set(compact('routes'));

// $ProductType=$this->ProductType->find('list');
// 	$this->set(compact('ProductType'));
// 	$Product=$this->Product->find('list');
// 	$this->set(compact('Product'));
	$Brand=$this->Brand->find('list');
	$this->set(compact('Brand'));
	$Branch=$this->Branch->find('list');
	$this->set(compact('Branch'));

}
public function SaleCollectionReportProductwiseAjax()
{
	$requestData=$this->request->data;
//zpr($requestData);
//exit;
	$columns = [];
	$columns[]='Product.name';
	$columns[]='Executive.name';
	$columns[]='Route.name';
	$columns[]='SaleItem.unit_price';

	$columns[]='SaleItem.tax_amount';
	$columns[]='SaleItem.total';
	$conditions=[];
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	if(!empty($requestData['product_id']))
	{
		if($requestData['product_id'])
			$conditions['Product.id']=$requestData['product_id'];	
	}
	if(!empty($requestData['product_type_id']))
	{
		if($requestData['product_type_id'])
			$conditions['Product.product_type_id']=$requestData['product_type_id'];	
	}
	if(!empty($requestData['branch_id']))
	{
		if($requestData['branch_id'])
			$conditions['Sale.branch_id']=$requestData['branch_id'];
	}
	if(!empty($requestData['route_id']))
	{
		if($requestData['route_id'])
			$conditions['Customer.route_id']=$requestData['route_id'];
	}
// if($data['customer_id'])
// 	$conditions['Sale.account_head_id']=$data['customer_id'];
// if($data['customer_group_id'])
//     $conditions['Customer.customer_group_id']=$data['customer_group_id'];
	if(!empty($requestData['executive_id']))
	{
		if($requestData['executive_id'])
			$conditions['Sale.executive_id']=$requestData['executive_id'];
	}
	$conditions['Sale.flag']=1;
	$conditions['Sale.status']=2;
	$conditions['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
	$totalData=$this->SaleItem->find('count',array(
		"joins"=>array(
			 array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'LEFT',
				"conditions"=>array('Sale.executive_id=Executive.id'),
				),
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			),
		'conditions'=>$conditions,
		));

	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Product.name LIKE' =>'%'. $q . '%',
			'Route.name LIKE' =>'%'. $q . '%',
			'Executive.name LIKE' =>'%'. $q . '%',
			'Sale.date_of_delivered LIKE' =>'%'. $q . '%',
			'SaleItem.unit_price LIKE' =>'%'. $q . '%',
			'SaleItem.tax_amount LIKE' =>'%'. $q . '%',
			'SaleItem.total LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->SaleItem->find('count',array(
			"joins"=>array(
				array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'LEFT',
				"conditions"=>array('Sale.executive_id=Executive.id'),
				),
				array(
					'table'=>'customers',
					'alias'=>'Customer',
					'type'=>'INNER',
					'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
					),
				array(
					'table'=>'routes',
					'alias'=>'Route',
					'type'=>'INNER',
					'conditions'=>array('Customer.route_id=Route.id')
					),
				),
			'conditions'=>$conditions,
			));
	}
	$Data=$this->SaleItem->find('all',array(
		"joins"=>array(
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'LEFT',
				"conditions"=>array('Sale.executive_id=Executive.id'),
				),
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
			),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		//'order'=>'Sale.date_of_delivered desc',
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'Product.name',
			'Sale.id',
			'Sale.account_head_id',
			'Sale.invoice_no',
			'Sale.date_of_delivered',
			'SaleItem.unit_price',
			'SaleItem.quantity',
			'SaleItem.total',
			'SaleItem.tax_amount',
			'Executive.name',
			'Route.name',
			)
		));
	foreach ($Data as $key => $value) {
        $Data[$key]['Customer']['name']=$this->AccountHead->field('AccountHead.name',array('AccountHead.id'=>$value['Sale']['account_head_id']));
		$Data[$key]['Sale']['date_of_delivered']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
	}

	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        => $Data
		);
	echo json_encode($json_data);
	exit;

}

public function SaleCollectionReportCustomerWise()
{
	$customers=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
				),
			),
		'fields'=>array('AccountHead.id','AccountHead.name',)
		));
//pr($customers);
//exit;
	$this->set(compact('customers'));
	$executives=$this->Executive->find('list');
	$this->set(compact('executives'));

	$routes=$this->Route->find('list'
//,array(
//'conditions'=>$conditions,
//'fields'=>array('total_net_value','total_sale'),
//)
		);
	$this->set(compact('routes'));

	$ProductType=$this->ProductType->find('list');
	$this->set(compact('ProductType'));
	$Product=$this->Product->find('list');
	$this->set(compact('Product'));
	$Brand=$this->Brand->find('list');
	$this->set(compact('Brand'));
	$Branch=$this->Branch->find('list');
	$this->set(compact('Branch'));
	$CustomerGroup=$this->CustomerGroup->find('list');
	$this->set(compact('CustomerGroup'));
// $executives=$this->Executive->find('list');
// $this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}
public function SaleCollectionReportCustomerWiseAjax()
{
	$requestData=$this->request->data;
	$columns = [];
//$columns[]='Product.name';
	$columns[]='Executive.name';
	$columns[]='AccountHead.name';
	$columns[]='Route.name';
	$columns[]='SaleItem.unit_price';

	$columns[]='SaleItem.tax_amount';
	$columns[]='SaleItem.total';
	$conditions=[];
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
//if($data['branch_id'])
// $conditions['Sale.branch_id']=$data['branch_id'];
	if($requestData['route_id'])
		$conditions['Customer.route_id']=$requestData['route_id'];
	if($requestData['customer_id'])
		$conditions['Sale.account_head_id']=$requestData['customer_id'];
	if($requestData['customer_group_id'])
		$conditions['Customer.customer_group_id']=$requestData['customer_group_id'];
	if($requestData['executive_id'])
		$conditions['Sale.executive_id']=$requestData['executive_id'];
// if($requestData['product_type_id'])
// 	$conditions['Product.product_type_id']=$requestData['product_type_id'];

// if($requestData['brand_id'])
// 	$conditions['Product.brand_id']=$requestData['brand_id'];
// if($requestData['product_id'])
// 	$conditions['SaleItem.product_id']=$requestData['product_id'];
	$conditions['Sale.flag']=1;
	$conditions['Sale.status']=2;
	$conditions['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
	$totalData=$this->Sale->find('count',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
// array(
// 	'table'=>'sale_items',
// 	'alias'=>'SaleItem',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.id=SaleItem.sale_id')
// ),
// array(
// 	'table'=>'products',
// 	'alias'=>'Product',
// 	'type'=>'INNER',
// 	'conditions'=>array('SaleItem.product_id=Product.id')
// ),
// array(
// 	'table'=>'product_types',
// 	'alias'=>'ProductType',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.product_type_id=ProductType.id')
// ),
// array(
// 	'table'=>'brands',
// 	'alias'=>'Brand',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.brand_id=Brand.id')
// ),
			array(
				'table'=>'customer_groups',
				'alias'=>'CustomerGroup',
				'type'=>'INNER',
				'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
				),
// array(
// 	'table'=>'branches',
// 	'alias'=>'Branch',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.branch_id=Branch.id')
// ),

			),
		'conditions'=>$conditions,
		'group' => array('Sale.account_head_id'),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
//'Product.*',
//'ProductType.*',
			'Sale.*',
//'SaleItem.*',
//'Brand.*',
			'Route.name',
//'Branch.*',
//'Customer.circle_id',
			)
		));
//pr($totalData);
//exit;
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Executive.name LIKE' =>'%'. $q . '%',
			'AccountHead.name LIKE' =>'%'. $q . '%',
			'Route.name LIKE' =>'%'. $q . '%',
//'SaleItem.unit_price LIKE' =>'%'. $q . '%',
//'SaleItem.tax_amount LIKE' =>'%'. $q . '%',
//'SaleItem.total LIKE' =>'%'. $q . '%',
			);
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$totalFiltered=$this->Sale->find('count',array(
			'joins'=>array(
				array(
					'table'=>'customers',
					'alias'=>'Customer',
					'type'=>'INNER',
					'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
					),
				array(
					'table'=>'routes',
					'alias'=>'Route',
					'type'=>'INNER',
					'conditions'=>array('Customer.route_id=Route.id')
					),
// array(
// 	'table'=>'sale_items',
// 	'alias'=>'SaleItem',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.id=SaleItem.sale_id')
// ),
// array(
// 	'table'=>'products',
// 	'alias'=>'Product',
// 	'type'=>'INNER',
// 	'conditions'=>array('SaleItem.product_id=Product.id')
// ),
// array(
// 	'table'=>'product_types',
// 	'alias'=>'ProductType',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.product_type_id=ProductType.id')
// ),
// array(
// 	'table'=>'brands',
// 	'alias'=>'Brand',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.brand_id=Brand.id')
// ),
				array(
					'table'=>'customer_groups',
					'alias'=>'CustomerGroup',
					'type'=>'INNER',
					'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
					),
				),
			'conditions'=>$conditions,
			'group' => array('Sale.account_head_id'),
			));
	}
//$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
	$Data=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			array(
				'table'=>'routes',
				'alias'=>'Route',
				'type'=>'INNER',
				'conditions'=>array('Customer.route_id=Route.id')
				),
// array(
// 	'table'=>'sale_items',
// 	'alias'=>'SaleItem',
// 	'type'=>'INNER',
// 	'conditions'=>array('Sale.id=SaleItem.sale_id')
// ),
// array(
// 	'table'=>'products',
// 	'alias'=>'Product',
// 	'type'=>'INNER',
// 	'conditions'=>array('SaleItem.product_id=Product.id')
// ),
// array(
// 	'table'=>'product_types',
// 	'alias'=>'ProductType',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.product_type_id=ProductType.id')
// ),
// array(
// 	'table'=>'brands',
// 	'alias'=>'Brand',
// 	'type'=>'INNER',
// 	'conditions'=>array('Product.brand_id=Brand.id')
// ),
			array(
				'table'=>'customer_groups',
				'alias'=>'CustomerGroup',
				'type'=>'INNER',
				'conditions'=>array('Customer.customer_group_id=CustomerGroup.id')
				),

			),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'group' => array('Sale.account_head_id'),
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
//'Product.*',
//'ProductType.*',
			'Sale.*',
//'SaleItem.*',
//'Brand.*',
			'Route.name',
			)
		));
//pr($Data);
//exit;
	$from=date('Y-m-d',strtotime($requestData['from_date']));
	$to=date('Y-m-d',strtotime($requestData['to_date']));
	foreach ($Data as $key => $value) {
		$this->SaleItem->virtualFields = array('grand_total' => "SUM(SaleItem.total)",'grand_tax_amount' => "SUM(tax_amount)",'grand_net_value' => "SUM(net_value)");
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				),
			'fields'=>array(
//'SaleItem.product_id',
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total', 	
				),
			));
//pr($SaleItem);
//exit;
		$Data[$key]['AccountHead']['name']=$value['AccountHead']['name'];
		if($value['Executive']['name']){$Data[$key]['Executive']['name']=$value['Executive']['name'];}else{$Data[$key]['Executive']['name']="No Executive";}
		$Data[$key]['Route']['name']=$value['Route']['name'];
		$Data[$key]['SaleItem']['unit_price']=$SaleItem['SaleItem']['grand_net_value'];
		$Data[$key]['SaleItem']['tax_amount']=$SaleItem['SaleItem']['grand_tax_amount'];
		$Data[$key]['SaleItem']['total']=$SaleItem['SaleItem']['grand_total'];
		$Data[$key]['Sale']['date_of_delivered']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
//pr($Data);
//exit;
	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        => $Data
		);
	echo json_encode($json_data);
	exit;

}
public function DailyPerformanceAnalysis()
{
}
public function DailyPerformanceAnalysisExport($day)
{
	$date  =date('Y-m-d h:i:sa');
	$this->response->download("daily_performance_analysis_export.csv");
//$date  =date('Y-m-d h:i:sa');
//$this->response->download("brand_export.csv");
	$Executive=$this->ExecutiveRouteMapping->find('all',array(
		"joins"=>array(
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'inner',
				"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
				),
			),
		'fields'=>array('Route.name','Executive.name','Executive.id')));

	$conditions=array();
	$conditions['Executive.block !=']=1;
	$conditions_sale=array();
	$grand_total=0;
	$quantity_total=0;
	$total=[];
	$opening_total=[];
	$Array=[];
	$last_total=[];
	$Executive_count=count($Executive);

	$Route=$this->Route->find('list',array(
		"joins"=>array(
			array(
				"table"=>'executive_route_mappings',
				"alias"=>'ExecutiveRouteMapping',
				"type"=>'inner',
				"conditions"=>array('Route.id=ExecutiveRouteMapping.route_id'),
				),
			),
//'order' => array('Route.id ASC'),
		'fields'=>array('Route.id','Route.name')));
	$date_array=[];
	$date1 = date('Y-m-d',strtotime('first day of this month'));
	$date2=date('Y-m-d');
	$diff = strtotime($date2) - strtotime($date1); 
	$dateDiff=abs(round($diff / 86400))+1;
	$last_date1= date('Y-m-d', strtotime('first day of last month'));
	$last_date2=date('Y-m-d', strtotime('last day of last month'));
	for($i=1;$i<=$dateDiff;$i++)
	{
		$to_date = date('Y-m-'.$i);
		array_push($date_array,$to_date);
	}    $last_saleamount_total=0;$last_collection_total=0;
	$conditions=array();

	$executive1=$this->ExecutiveRouteMapping->find('all',array(
		"joins"=>array(
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'inner',
				"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
				),
			),
		'fields'=>array('ExecutiveRouteMapping.route_id')));
	foreach ($executive1 as $key => $value1) {
		$conditions['Customer.route_id']=$value1['ExecutiveRouteMapping']['route_id'];;
		$Customer=$this->Customer->find('all',array('conditions'=>$conditions));
		$balance=0;$opening_balance=0;$present_balance=0;
		if($Customer)
		{
			foreach ($Customer as $key => $value) {
				$opening_Balance_Total=$this->daily_get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance'],$last_date2);

				$Balance_Total=$this->get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance']);

				$Balance_Total = number_format($Balance_Total,2);
				$balance+=$Balance_Total;
				$opening_Balance_Total = number_format($opening_Balance_Total,2);
				$opening_balance+=$opening_Balance_Total;


			}
		}
		$array_opening_total['opening_balance']=$opening_balance;
		$array_opening_total['present_opening_balance']=$balance;
		$opening_total[]=$array_opening_total;
//pr($balance);
//pr($opening_balance);
	}
	foreach($Route as $key => $value)
	{

		$column_total_sale=0;$column_total_collection=0;
		$last_saleamount = $this->sale_amount($key,$last_date1,$last_date2);
		$data_last_saleamount = 0;
		if(!empty($last_saleamount))
		{
			$data_last_saleamount = $last_saleamount;
		}
		$last_saleamount_total+=$data_last_saleamount;
		$last_collection= $this->collection_amount($key,$last_date1,$last_date2);
		$last_collection_total+=$last_collection;

		foreach ($date_array as $key1 => $value1) {
			$saleamount = $this->sale_amount($key,$value1,$value1);
//pr($saleamount);
			$collection_total_amount= $this->collection_amount($key,$value1,$value1);
			$data['sale_amount'] =$saleamount;
			$data['collection_total_amount'] = 0;
			if(!empty($collection_total_amount))
			{
				$data['collection_total_amount'] = $collection_total_amount;
			}
			$single['sale_amount']=$data['sale_amount'];
			$single['collection_amount']=$data['collection_total_amount'];
			$single['date']=$value1;
			$column_total_sale+=$data['sale_amount'];
			$column_total_collection+=$data['collection_total_amount'];
			$Array[]=$single;
		} 
		$data['target'] = $this->sale_target($key,$date1);
		$data['collection_target'] = $this->collection_target($key,$date1);
		$array_total['total']['sale_amount']=$column_total_sale;
		$array_total['total']['collection_amount']=$column_total_collection;
		$array_total['total']['target']=$data['target'];

		if($data['collection_target'])
		{
			$array_total['total']['collection_target']= $data['collection_target'];

		}
		else
		{
			$array_total['total']['collection_target']=0;

		}
		if($column_total_collection==0 || $data['collection_target']==0)
		{
			$array_total['total']['collection_percentage']=0;
		}
		else
		{
			$array_total['total']['collection_percentage']=round((($column_total_collection*100)/$data['collection_target']),3);
		}
		if($column_total_sale==0 || $data['target']==0)
		{
			$array_total['total']['sale_percentage']=0;
		}
		else
		{
			$array_total['total']['sale_percentage']= round((($column_total_sale*100)/$data['target']),3);

		}

		$total[]=$array_total;
// exit;
	}
	$new_array=[];
	foreach($Array as $key3 =>$value3){
//$new_array[$value3['date']][]=$value3['sale_amount'];
		$new_array[$value3['date']]['collection_amount'][]=floatval($value3['collection_amount']);
		$new_array[$value3['date']]['sale_amount'][]=floatval($value3['sale_amount']);

	}
//pr($total);
	$last_collection_total=$last_collection_total;
	$last_saleamount_total=$last_saleamount_total;
	$Executive_count=count($Executive);
	$this->set('dva_day',$day);
	$this->set('last_collection_total',$last_collection_total);
	$this->set('last_saleamount_total',$last_saleamount_total);
	$this->set('column_total',$total);
	$this->set('opening_total',$opening_total);
	$this->set('last_total',$last_total);
	$this->set('amount',$new_array);
	$this->set('Route',$Executive);
	$this->set('Executive_count',$Executive_count);
	$this->layout = 'ajax';
	return false;
} 

public function daily_get_oustanding_amount($account_head_id,$opening_balance,$last_date){
	$Journal=$this->Journal->find('all',array(
		'conditions'=>array(
			'AND' => array(
				'OR' => array(
					'Journal.debit'=>$account_head_id,
					'Journal.credit'=>$account_head_id,
					),
				'AND' => array(
					'Journal.flag=1',
					'(Journal.date) <='=>$last_date,
					)
				)
			),
		));
	$Journal_credit=$this->Journal->find('all',array(
		'conditions'=>array(
			'Journal.credit'=>$account_head_id,
			'Journal.flag=1',
			'(Journal.date) <='=>$last_date,
			)
		));
	$category=[
	'Asset'=>'debit_account',
	'Expense'=>'debit_account',
	'Capital'=>[
	'Capital'=>'credit_account',
	'Drawing'=>'debit_account',
	],
	'Income'=>'credit_account',
	'Liabilities'=>'credit_account',
	];
	$AccountingsController = new AccountingsController;
	$type=$AccountingsController->get_type_by_account_dead($account_head_id);
	$Journal_all=[];
	foreach ($Journal_credit as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['mode']=$value['AccountHeadDebit']['name'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['credit']=$value['Journal']['amount'];
		$Journal_single['debit']=0.00;
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	$Journal_debit=$this->Journal->find('all',array(
		'conditions'=>array(
			'Journal.debit'=>$account_head_id,
			'Journal.flag=1',
			'(Journal.date) <='=>$last_date,
			)
		));
	foreach ($Journal_debit as $key => $value) 
	{
		$Journal_single['id']=$value['Journal']['id'];
		$Journal_single['date']=$value['Journal']['date'];
		$Journal_single['mode']=$value['AccountHeadCredit']['name'];
		$Journal_single['remarks']=$value['Journal']['remarks'];
		$Journal_single['credit']=0.00;
		$Journal_single['debit']=$value['Journal']['amount'];
		$Journal_all[$Journal_single['id']]=$Journal_single;
	}
	krsort($Journal_all);
	$return['row']['tbody']='';
	$return['row']['tfoot']='';
	$category_name='';
	$Balance_Total=0;
	$credit_Total=0;
	$debit_Total=0;
	if(!empty($type)){
		$Type_name=$type['Type']['name'];
		$category_name=$category[$type['Type']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['Type']['name']][$type['Group']['name']];
		}
	}
	if($category_name=='credit_account')
	{
		$credit=$opening_balance;
		$debit='0';
	}
	else
	{
		$debit=$opening_balance;
		$credit='0';
	}
	$Journal_all[0]=array(
		'id'=>0,
		'mode'=>'Opening Balance',
		'remarks'=>'',
		'credit'=>$credit,
		'debit'=>$debit,
		);
	foreach ($Journal_all as $key => $value) 
	{
		$credit_Total+=$value['credit'];
		$debit_Total+=$value['debit'];
	}
	$Balance_Total=$debit_Total-$credit_Total;	
	return $Balance_Total;
}
function sale_amount($id,$first_date,$last_date){
	$conditions=array();
	$total_sale=0;
	$conditions['Customer.route_id']=$id;
	$Customer=$this->Customer->find('all',array('conditions'=>$conditions));
	if($Customer)
	{
		foreach ($Customer as $key => $value) {

			$SaleAmountList=$this->Sale->find('first',array(
				'conditions'=>array('(Sale.date_of_delivered) between ? and ? '=>array($first_date,$last_date),'Sale.account_head_id'=>$value['Customer']['account_head_id']),
				'fields'=>'sum(grand_total) as amount',
				'group'=> 'Sale.account_head_id',

				));
			$total = 0;
			if(!empty($SaleAmountList))
			{
				$total = $SaleAmountList[0]['amount'];
			}
			$total_sale+=$total;

		}
	}
	return $total_sale;
}
function collection_amount($id,$first_date,$last_date){

	$conditions=array();
	$collection_total_amount=0;
	$conditions['Customer.route_id']=$id;
	$Customer=$this->Customer->find('all',array('conditions'=>$conditions));
	if($Customer)
	{
		foreach ($Customer as $key => $value) 
		{
			$get_collection_amount_return_value=$this->get_collection_amount($value['Customer']['account_head_id'],$first_date,$last_date,2);
			$collection_total_amount+=$get_collection_amount_return_value['sale_amount'];
		}
	}
	return $collection_total_amount;
}
function sale_target($id,$first_date){
	$executive1=$this->ExecutiveRouteMapping->find('first',array(
		"joins"=>array(
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'inner',
				"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
				),
			),
		'conditions'=>array('ExecutiveRouteMapping.route_id'=>$id),
		'fields'=>array('Executive.id')));
	$SaleTargetList=$this->SaleTarget->find('first',array(
		'conditions'=>array('(SaleTarget.wef_date) <='=>$first_date,'executive_id'=>$executive1['Executive']['id']),
		'fields'=>'max(SaleTarget.wef_date) as date',

		));
	$target = $this->SaleTarget->field(

		'SaleTarget.target',

		array('SaleTarget.wef_date' => $SaleTargetList[0]['date'],'executive_id'=>$executive1['Executive']['id']));
	return $target;
}
function collection_target($id,$first_date){
	$executive1=$this->ExecutiveRouteMapping->find('first',array(
		"joins"=>array(
			array(
				"table"=>'executives',
				"alias"=>'Executive',
				"type"=>'inner',
				"conditions"=>array('Executive.id=ExecutiveRouteMapping.executive_id'),
				),
			),
		'conditions'=>array('ExecutiveRouteMapping.route_id'=>$id),
		'fields'=>array('Executive.id')));
	$CollectionTargetList=$this->SaleTarget->find('first',array(
		'conditions'=>array('(SaleTarget.wef_date) <='=>$first_date,'executive_id'=>$executive1['Executive']['id']),
		'fields'=>'max(SaleTarget.wef_date) as date',

		));
	$collection_target = $this->SaleTarget->field(

		'SaleTarget.collection_target',

		array('SaleTarget.wef_date' => $CollectionTargetList[0]['date'],'executive_id'=>$executive1['Executive']['id']));
	return $collection_target;
}
public function ExecutiveBonusReport()
{
	$executives=$this->Executive->find('list',array('conditions' => 'block!=1','fields'=>array('id','name')));
	$this->set(compact('executives'));
	$this->request->data['from_date']= date('d-m-Y',strtotime('first day of this month'));
	$this->request->data['to_date']= date('d-m-Y',strtotime('last day of this month'));

}
public function executive_bonus_report_ajax()
{
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='Executive.name';
	$columns[]='Executive.id';
    $columns[]='Executive.id';
	$columns[]='Executive.id';
	$columns[]='Executive.id';
	$columns[]='Executive.id';
	$conditions=[];
	$conditions_executive=[];
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
		//$conditions['ExecutiveBonusCalculation.date between ? and ?']=[$from_date,$to_date];
	$executive_id=$requestData['executive_id'];
	if(!empty($executive_id)) { $conditions_executive['Executive.id']=$executive_id; }
	$totalData=$this->Executive->find('count',[
		'conditions'=>$conditions_executive,
			]);

// pr($totalData);
// exit;
	$totalFiltered=$totalData;
	if(!empty($requestData['search']['value'])){ 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Executive.name LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->Executive->find('count',[
					'conditions'=>$conditions_executive,
			]);

	}
		$this->Executive->unbindModel(array('hasMany' => array('Sale')));
	$Data=$this->Executive->find('all',array(
				'conditions'=>$conditions_executive,
'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'Executive.id',
			'Executive.name',
			),
		));
	if($Data){
	foreach ($Data as $key => $value) {
			$conditions['ExecutiveBonusCalculation.executive_id']=$value['Executive']['id'];
			$this->ExecutiveBonusCalculation->virtualFields = array(
			'total_bonus_amount' => "SUM(ExecutiveBonusCalculation.bonus_amount)",
			'total_bonus_return_amount' => "SUM(ExecutiveBonusCalculation.bonus_return_amount)",
		);
		$Bonus=$this->ExecutiveBonusCalculation->find('first',array(
		'conditions'=>$conditions,
		'fields'=>array(
		'ExecutiveBonusCalculation.total_bonus_amount',
		'ExecutiveBonusCalculation.total_bonus_return_amount',
			),
		));
		 $this->Journal->virtualFields = array('bonus_payment' => "SUM(Journal.amount)");    
              $bonus_payment_journal=$this->Journal->find('first',array(
                'conditions'=>array(
                    'Journal.remarks'=>'Bonus Payment',
                    'Journal.executive_id'=>$value['Executive']['id'],
                     //'Journal.date between ? and ?'=>[$from_date,$to_date],
                ),
                'fields'=>array(
                    'bonus_payment',
                ),
            ));
              $total_bonus_balance=($Bonus['ExecutiveBonusCalculation']['total_bonus_amount']-$Bonus['ExecutiveBonusCalculation']['total_bonus_return_amount'])-$bonus_payment_journal['Journal']['bonus_payment'];
        $Data[$key]['ExecutiveBonusCalculation']['bonus_paid']=number_format($bonus_payment_journal['Journal']['bonus_payment'],2,'.','');
	   $Data[$key]['ExecutiveBonusCalculation']['bonus_balance']=number_format($total_bonus_balance,2,'.','');
		$Data[$key]['ExecutiveBonusCalculation']['bonus_amount']=number_format($Bonus['ExecutiveBonusCalculation']['total_bonus_amount'],2,'.','');
		$Data[$key]['ExecutiveBonusCalculation']['bonus_return_amount']=number_format($Bonus['ExecutiveBonusCalculation']['total_bonus_return_amount'],2,'.','');
	   $Data[$key]['ExecutiveBonusCalculation']['total_bonus']=number_format(($Bonus['ExecutiveBonusCalculation']['total_bonus_amount']-$Bonus['ExecutiveBonusCalculation']['total_bonus_return_amount']),2,'.','');
}
}
//pr($Data);
 
	$json_data=array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData), 
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public function TailorBonusReport()
{
	$tailor_role_id=$this->Role->field('Role.id',array('Role.name'=>'TAILOR'));
	$tailors=$this->Staff->find('list',array('conditions'=>array('Staff.role_id'=>$tailor_role_id),'fields'=>array('Staff.id','Staff.name')));
	$this->set(compact('tailors'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');

}
public function tailor_bonus_report_ajax()
{
	$requestData=$this->request->data;

	$columns = [];
	$columns[]='Staff.name';
	$columns[]='Production.production_no';
	$columns[]='ExecutiveBonusDetails.receipt_date';
	$columns[]='Product.name';
	$columns[]='Production.production_cost';
	$columns[]='Production.quantity';
	$columns[]='ExecutiveBonusDetails.bonus_amount';
	$conditions=[];
	$conditions['ExecutiveBonusDetails.flag']=1;
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions['ExecutiveBonusDetails.receipt_date between ? and ?']=[$from_date,$to_date];
	$tailor_id=$requestData['tailor_id'];
	if(!empty($tailor_id)) { $conditions['ExecutiveBonusDetails.staff_id']=$tailor_id; }
	 
	$totalData=$this->ExecutiveBonusDetails->find('count',[
			'joins'=>[
			 [
				'table'=>'staffs',
				'alias'=>'Staff',
				'type'=>'LEFT',
				'conditions'=>array('Staff.id=ExecutiveBonusDetails.staff_id')
				],
				[
				'table'=>'productions',
				'alias'=>'Production',
				'type'=>'inner',
				'conditions'=>array('Production.production_no=ExecutiveBonusDetails.production_no')
				],
				[
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'inner',
				'conditions'=>array('Production.product_id=Product.id')
				],
			],
			'conditions'=>$conditions,
			]);

	$totalFiltered=$totalData;
	if(!empty($requestData['search']['value'])){ 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Staff.name LIKE' =>'%'. $q . '%',
			'ExecutiveBonusDetails.receipt_date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
			'Product.name LIKE'   =>'%'. $q . '%',
			'Production.production_no LIKE'   =>'%'. $q . '%',
			'Production.production_cost LIKE'   =>'%'. $q . '%',
			'Production.quantity LIKE'   =>'%'. $q . '%',
			'ExecutiveBonusDetails.bonus_amount LIKE' =>'%'. $q . '%',
			);
		$totalData=$this->ExecutiveBonusDetails->find('count',[
			'joins'=>[
			 [
				'table'=>'staffs',
				'alias'=>'Staff',
				'type'=>'LEFT',
				'conditions'=>array('Staff.id=ExecutiveBonusDetails.staff_id')
				],
				[
				'table'=>'productions',
				'alias'=>'Production',
				'type'=>'inner',
				'conditions'=>array('Production.production_no=ExecutiveBonusDetails.production_no')
				],
				[
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'inner',
				'conditions'=>array('Production.product_id=Product.id')
				],
			],
			'conditions'=>$conditions,
			]);

	}

	$Data=$this->ExecutiveBonusDetails->find('all',array(
		'conditions'=>$conditions,
		'joins'=>array(
			array(
				'table'=>'staffs',
				'alias'=>'Staff',
				'type'=>'LEFT',
				'conditions'=>array('Staff.id=ExecutiveBonusDetails.staff_id')
				),
			array(
				'table'=>'productions',
				'alias'=>'Production',
				'type'=>'inner',
				'conditions'=>array('Production.production_no=ExecutiveBonusDetails.production_no')
				),
			array(
				'table'=>'products',
				'alias'=>'Product',
				'type'=>'inner',
				'conditions'=>array('Production.product_id=Product.id')
				),
			),
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'Staff.name',
			'Production.production_no',
			'Production.quantity',
			'Product.name',
			'Production.production_cost',
			'ExecutiveBonusDetails.receipt_date',
			'ExecutiveBonusDetails.bonus_amount',
			'ExecutiveBonusDetails.amount',
			'ExecutiveBonusDetails.production_no',
			),
		));
	//pr($Data);exit;
	$conditions['ExecutiveBonusDetails.flag']=1;
	foreach ($Data as $key => $value) {
		$Data[$key]['ExecutiveBonusDetails']['receipt_date']=date('d-m-Y',strtotime($value['ExecutiveBonusDetails']['receipt_date']));
		$Data[$key]['ExecutiveBonusDetails']['bonus_amount']=number_format($value['ExecutiveBonusDetails']['bonus_amount'],3,'.','');
		$Data[$key]['ExecutiveBonusDetails']['amount']=number_format($value['ExecutiveBonusDetails']['amount'],3,'.','');
	}
	$json_data=array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData), 
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public function customer_due_list_report_ajax()
{
	$requestData=$this->request->data;
	$conditions=[];
	if($requestData['customer_id'])
	{
		$conditions['Customer.account_head_id']=$requestData['customer_id'];
	}
	$columns = [];
	$columns[]='CustomerType.name';
	$columns[]='AccountHead.name';
	$columns[]='AccountHead.opening_balance';
	$totalData=$this->Customer->find('count',['conditions'=>$conditions]);
	$totalFiltered=$totalData;
	if(!empty($requestData['search']['value'])){ 
		// $q=$requestData['search']['value'];
		// $conditions['OR']=array(
		// 	'CustomerType.name LIKE' =>'%'. $q . '%',
		// 	'AccountHead.name LIKE' =>'%'. $q . '%',
		// 	'AccountHead.opening_balance LIKE'   =>'%'. $q . '%',
		// 	);
		$totalFiltered=$this->Customer->find('count',[
			'conditions'=>$conditions,
			'fields'=>array(
				'Customer.account_head_id',
				'AccountHead.opening_balance',
				'AccountHead.name',
				'CustomerType.name',
				),
			]);
	}
	$Data=$this->Customer->find('all',array(
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
				'Customer.account_head_id',
				'AccountHead.opening_balance',
				'AccountHead.name',
				'CustomerType.name',
				),
			));
 //pr($Data);exit;
	$conditions['ExecutiveBonusDetails.flag']=1;
	$Balance_Total=0;
	if(!empty($Data))
	{
		foreach ($Data as $key => $value) {
			$Balance_Total=$this->get_oustanding_amount($value['Customer']['account_head_id'],$value['AccountHead']['opening_balance']);
			$Data[$key]['Customer']['Balance_Total']=round($Balance_Total,3);
		}
	}
	
	$json_data=array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData), 
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public function AllSaleReport()
{
	$executives=$this->Executive->find('list',array('conditions' => 'block!=1'));
//$executives[0]='No Executive';
	$routes=$this->Route->find('list');
	$this->set(compact('executives','routes'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
}
public function GetCustomerForSaleCollectionReportAll()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['executive_id'])
		$conditions['executive_id']=$data['executive_id'];
		$conditions['route_id']=$data['route_id'];
	$Customer=$this->Customer->find('list',array(
		'joins'=>array(
			array(
				'table'=>'account_heads',
				'alias'=>'AccountHead',
				'type'=>'INNER',
				'conditions'=>array('AccountHead.id=Customer.account_head_id')
			),
		),
		'conditions'=>$conditions,
		'fields'=>array('AccountHead.id','AccountHead.name')
	));
	echo json_encode($Customer); exit;
}
public function SaleCollectionReportAjaxAll()
{
	$data=$this->request->data;
	$conditions=[];
	if($data['executive_id']){
		$conditions['Sale.executive_id']=$data['executive_id'];
	}
	if($data['route_id']){
		$conditions['Customer.route_id']=$data['route_id'];
	}
	if($data['customer_id']){
		$conditions['Customer.account_head_id']=$data['customer_id'];
	}
	$conditions['Sale.status']=2;
	$conditions['Sale.flag']  =1;
	$from=date('Y-m-d',strtotime($data['from_date']));
	$to  =date('Y-m-d',strtotime($data['to_date']));
	$conditions['Sale.date_of_delivered  between ? and ?']=array($from,$to);
	$Sale_Customer=$this->Sale->find('all',array(
		'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
			),
		),
		'conditions'=>$conditions,
		'fields'=>array(
			'AccountHead.id',
			'AccountHead.name',
			'Executive.id',
			'Executive.name',
			'Customer.route_id',
		)
	));
	$list=[];
	foreach ($Sale_Customer as $key => $value) {
		$this->SaleItem->virtualFields = array(
			//'grand_total' => "SUM(SaleItem.total)",
			'grand_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
			'grand_tax_amount' => "SUM(SaleItem.tax_amount)",
			//'grand_net_value' => "SUM(SaleItem.net_value)",
 'grand_total' => "SUM(((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount))",
		);
		$SaleItem=$this->SaleItem->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				'Sale.status'=>2,'Sale.flag'=>1,
			),
			'fields'=>array(
				'SaleItem.grand_net_value',
				'SaleItem.grand_tax_amount',
				'SaleItem.grand_total',
			),
		));
		$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array(
			'grand_discount_amount' => "SUM(discount_amount)",'grand_other_value' => "SUM(other_value)"
		);
		$Sale=$this->Sale->find('first',array(
			'conditions'=>array(
				'Sale.account_head_id'=>$value['AccountHead']['id'],
				'Sale.date_of_delivered between ? and ?'=>array($from,$to),
				'Sale.status'=>2,'Sale.flag'=>1,
			),
			'fields'=>array(
				'Sale.grand_discount_amount',
				'Sale.grand_other_value',
			),
		));
		$single['executive']=$value['Executive']['name'];
		$single['name']=$value['AccountHead']['name'];
		$single['route']="";
		if($value['Customer']['route_id']){
			$route_id=$value['Customer']['route_id'];
			$Route=$this->Route->findById($route_id);
			$single['route']=$Route['Route']['name'];
		}
		if($Sale){
			$single['dicount_amount']=$Sale['Sale']['grand_discount_amount']?floatval($Sale['Sale']['grand_discount_amount']):0;
			$single['other_value']   =$Sale['Sale']['grand_other_value']?floatval($Sale['Sale']['grand_other_value']):0;
			$single['net_value']     =$SaleItem['SaleItem']['grand_net_value']?floatval($SaleItem['SaleItem']['grand_net_value']):0;
			$single['tax_amount']    =$SaleItem['SaleItem']['grand_tax_amount']?floatval($SaleItem['SaleItem']['grand_tax_amount']):0;
			$single['taxable_amount']    =$SaleItem['SaleItem']['grand_net_value']-$Sale['Sale']['grand_discount_amount'];
			$single['total']         =$single['net_value']+$single['tax_amount'];
			$single['total']        -=$single['dicount_amount'];
			$single['total']        +=$single['other_value'];
			$single['grandtotal']    =round($single['total'],2);
			if($single['net_value']) $list[$value['AccountHead']['id']]=$single;
		}
	}
	/******customer details hidden******/
// 		if(empty($data['route_id'])){

// 	$Saleone=$this->Sale->find('first',array(
// 		'conditions'=>array('Sale.status'=>2,'Sale.flag'=>1,'Sale.invoice_no'=>"R196-1",'Sale.date_of_delivered between ? and ?'=>array($from,$to),),
// 		'fields'=>array(
// 			'AccountHead.id',
// 			'Sale.discount_amount',
// 			'AccountHead.name',
// 			'Executive.id',
// 			'Sale.id',
// 			'Executive.name',
// 		)
// 	));
// 		$this->SaleItem->virtualFields = array(
// 			'grand_net_value' => "SUM(SaleItem.unit_price*SaleItem.quantity)",
// 			'grand_tax_amount' => "SUM(SaleItem.tax_amount)",
//  'grand_total' => "SUM(((SaleItem.unit_price*SaleItem.quantity)+SaleItem.tax_amount))",
// 		);
// 	// 	if($Saleone){
// 	// 	$SaleItem1=$this->SaleItem->find('first',array(
// 	// 		'conditions'=>array(
// 	// 			'Sale.id'=>$Saleone['Sale']['id'],
// 	// 			'Sale.date_of_delivered between ? and ?'=>array($from,$to),
// 	// 			'Sale.status'=>2,'Sale.flag'=>1,
// 	// 		),
// 	// 		'fields'=>array(
// 	// 			'SaleItem.grand_net_value',
// 	// 			'SaleItem.grand_tax_amount',
// 	// 			'SaleItem.grand_total',
// 	// 		),
// 	// 	));
// 	// 	$single['executive']=$Saleone['Executive']['name'];
// 	// 	$single['name']="";
// 	// 	$single['route']="";
// 	// 	if($Saleone){
// 	// 		$single['dicount_amount']=$Saleone['Sale']['discount_amount']?floatval($Saleone['Sale']['discount_amount']):0;
// 	// 		$single['other_value']   =0;
// 	// 		$single['net_value']     =$SaleItem1['SaleItem']['grand_net_value']?floatval($SaleItem1['SaleItem']['grand_net_value']):0;
// 	// 		$single['tax_amount']    =$SaleItem1['SaleItem']['grand_tax_amount']?floatval($SaleItem1['SaleItem']['grand_tax_amount']):0;
// 	// 		$single['taxable_amount']    =$SaleItem['SaleItem']['grand_net_value']-$Sale['Sale']['grand_discount_amount'];
// 	// 		$single['total']         =$single['net_value']+$single['tax_amount'];
// 	// 		$single['total']        -=$single['dicount_amount'];
// 	// 		$single['total']        +=$single['other_value'];
// 	// 		$single['grandtotal']    =round($single['total'],2);
// 	// 		if($single['net_value']) $list[]=$single;
// 	// 	}
// 	// }
// }
	echo json_encode($list); exit;
}
public function RouteReport()
{
	$this->request->data['from_date']=date('1-m-Y');
	$this->request->data['to_date']=date('d-m-Y');	
}
public function RouteReportAjax()
{
		$requestData=$this->request->data;
		$columns=[];
		$columns[]='Route.name';
		$columns[]='total';
		$columns[]='taxable_amount';
		$columns[]='tax_amount';
		$columns[]='discount_amount';
		$columns[]='grand_total';
		$conditions_sale=[];
		$conditions=[];
			$conditions_sale['Sale.date_of_delivered between ? and ?']=[
			date('Y-m-d',strtotime($requestData['from_date'])),
			date('Y-m-d',strtotime($requestData['to_date']))
			];
			$conditions_sale['Sale.status']=2;
			$conditions_sale['Sale.flag']=1;
			if(!empty($requestData['route_id']))
			{
			if($requestData['route_id'])
			$conditions['Route.id']=$requestData['route_id'];
			}
		$totalData=$this->Route->find('count',[
		'conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'Route.name LIKE' =>'%'. $q . '%',
    		);
    	$totalFiltered=$this->Route->find('count',[
    		'conditions'=>$conditions,
    		]);
    }
    //order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column']==0)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
    $Data=$this->Route->find('all',array(
    	'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	'order'=>$order,
    	'fields'=>array(
    		'Route.name',
    		'Route.id',
    		)
    	));
    foreach ($Data as $key => $value) {
    	$conditions_sale['Customer.route_id']=$value['Route']['id'];
    	$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
		$this->Sale->virtualFields = array('total_discount_amount' => "SUM(discount_amount)",'total_taxable_amount' => "SUM(taxable_amount)",
			'total_tax_amount' => "SUM(tax_amount)",'total_net_value' => "SUM(total)",'total_grand_total' => "SUM(grand_total)");
		$Sale=$this->Sale->find('first',array(
			'joins'=>array(
			array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Sale.account_head_id=Customer.account_head_id')
				),
			),
			'conditions'=>$conditions_sale,
			'fields'=>array(
				'Sale.total_discount_amount',
				'Sale.total_net_value',
				'Sale.total_grand_total',
				'Sale.total_taxable_amount',
				'Sale.total_tax_amount',
				),
			));
    	$Data[$key]['Sale']['net_value']=number_format($Sale['Sale']['total_net_value'],2,'.','');
    	$Data[$key]['Sale']['tax_amount']=number_format($Sale['Sale']['total_tax_amount'],2,'.','');
    	$Data[$key]['Sale']['taxable_amount']=number_format($Sale['Sale']['total_taxable_amount'],2,'.','');
    	$Data[$key]['Sale']['grand_total']=number_format($Sale['Sale']['total_grand_total'],2,'.','');
    	$Data[$key]['Sale']['discount_amount']=number_format($Sale['Sale']['total_discount_amount'],2,'.','');
    }
    // //sorting total/balance/recieved
		if($requestData['order'][0]['column'] >0) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["Executive"][$columnname] == $b["Executive"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["Executive"][$columnname] < $b["Executive"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["Executive"][$columnname] > $b["Executive"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;

}
// public function chart()
// {
// 	$this->AccountHead->virtualFields = array('total' => "SUM(opening_balance)");
// 	$AccountHead=$this->AccountHead->find('first',array(
// 			'joins'=>array(
// 				array(
// 					'table'=>'groups',
// 					'alias'=>'Group',
// 					'type'=>'INNER',
// 					'conditions'=>array('Group.id=SubGroup.group_id')
// 					),
// 				array(
// 					'table'=>'master_groups',
// 					'alias'=>'MasterGroup',
// 					'type'=>'INNER',
// 					'conditions'=>array('MasterGroup.id=Group.master_group_id')
// 					),
// 				array(
// 					'table'=>'types',
// 					'alias'=>'Type',
// 					'type'=>'INNER',
// 					'conditions'=>array('Type.id=MasterGroup.type_id')
// 					),
// 				),
// 			'fields'=>array(
// 				'AccountHead.total',
// 				),
// 			'conditions'=>array('Type.id'=>5)
// 			));
// 	pr($AccountHead);exit;
		
// }
public function expense_vat_migration()
{
	try {
	 $Journal=$this->Journal->find('all',array(
			  'conditions'=>array(
			    'work_flow'=>'Expense',
			    'credit'=>'23',
			     'flag'=>1,
			    ),
			  'fields'=>array(
			    'Journal.date',
			    'Journal.amount',
			    'Journal.id',
			    'Journal.debit',
			    'Journal.external_voucher'
			    )
			  ));
	 foreach ($Journal as $key => $value) 
	 {
	 	$last_id=$value['Journal']['id']-1;
	 	$jornal_last=$this->Journal->findByIdAndExternalVoucher($last_id,$value['Journal']['external_voucher']);
	 	$tax=($value['Journal']['amount']*100)/$jornal_last['Journal']['amount'];
	                $tax_data=[
							'expense_id'=>$value['Journal']['debit'],
							'date'=>$value['Journal']['date'],
							'amount'=>$jornal_last['Journal']['amount'],
							'vat'=>$tax,
							'vat_amount'=>$value['Journal']['amount'],
							'external_voucher'=>$value['Journal']['external_voucher'],
							];
						$this->ExpenseVat->create();
					if(!$this->ExpenseVat->save($tax_data)) 
					{ 
						$errors = $this->Expensesvat->validationErrors; 
						foreach ($errors as $key => $value)
						{ 
							throw new Exception($value[0], 1); 
					   } 
					}
	 }
	}
	catch (Exception $e)
    {
       
    }
exit;
}
public function expense_vat_flag()
{
		try {

	 $Journal=$this->Journal->find('all',array(
			  'conditions'=>array(
			    'Journal.work_flow'=>'Expense',
			    'Journal.credit'=>'23',
			     'Journal.flag'=>1,
			    ),
			  'fields'=>array(
			    'Journal.date',
			    'Journal.amount',
			    'Journal.id',
			    'Journal.debit',
			    'Journal.external_voucher'
			    ),
			  'order'=>array('Journal.id ASC'),
			  ));
	 foreach ($Journal as $key => $value1) 
	 {
	 	$this->Journal->id=$value1['Journal']['id'];
		if(!$this->Journal->saveField('flag',0))
		throw new Exception("Cant set flag", 1);
	               
	 }
	}
	catch (Exception $e)
    {
       
    }
exit;
	
}
public function Expense_CSV_Uploader()
{
  if($this->request->data)
  {
    //pr($this->request->data);
   // exit;
    $name=$this->request->data['ProductDetails']['Upload']['name'];
    $name_explod=explode('.', $name);
    if($name_explod[1]!="csv")
    {
      $this->Session->setFlash(__('It is Not in CSV Format'));
      $this->redirect(array('action' => 'Product_CSV_Uploader'));
    }
    else
    {
     $datasource_Journal = $this->Journal->getDataSource();
     $datasource_expense = $this->ExpenseVatDetail->getDataSource();
      $filename = WWW_ROOT. 'files'.DS.$name;         
      if (move_uploaded_file($this->data['ProductDetails']['Upload']['tmp_name'],$filename)) {
        define('CSV_PATH',WWW_ROOT. 'files'.DS);
        $csv_file = CSV_PATH . $name; 
       // pr($csv_file);
         // exit;
        if (($handle = fopen($csv_file, "r")) !== FALSE) {
          fgetcsv($handle);   
          $i=1;
          $code=12;
      $count=3;
          while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {   

            $num = count($data);

            for ($c=0; $c < $num; $c++) {
              $col[$c] = $data[$c];
            }
            $user_id=1;
            $sub_group_id=15;
            $date = date('Y-m-d');
            if($col[0])
            {
                
                try {
                $datasource_Journal->begin();
                $datasource_expense->begin();
                
                $id=$col[0];

                   $tax=$col[7];
                $tax_on_sale_account_head_id=23;
				$credit=$tax_on_sale_account_head_id;
				$journal_id=$this->Journal->findById($id);
			    $debit=$journal_id['Journal']['debit'];
			   
			    $voucher_no='';
			   $work_flow='Expense';
			   $date=$journal_id['Journal']['date'];
			   $remarks=$journal_id['Journal']['remarks'];
			   $user_id=1;
			   $external_voucher=$journal_id['Journal']['external_voucher'];
			    $branch_id=$journal_id['Journal']['branch_id'];
			    $route_id=$journal_id['Journal']['route_id'];
   if($tax==0)
 {
//  	 $AccountingsController = new AccountingsController;
// $function_return=$AccountingsController->JournalCreate($credit,$debit,$tax,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,'','',$route_id);
//            if($function_return['result']!='Success')
// 	throw new Exception($function_return['message'], 1);

            $tax_data=[
			'expense_id'=>$debit,
			'date'=>date('Y-m-d',strtotime($date)),
			'amount'=>$col[6],
			//'vat'=>$tax,
			'vat_amount'=>$tax,
			'total'=>$col[8],
			'external_voucher'=>$external_voucher,
			'trn_number'=>$col[2],
			'supplier_name'=>$col[3],
			'journal_id'=>0,
			];
				$this->ExpenseVatDetail->create();
				if(!$this->ExpenseVatDetail->save($tax_data)) { $errors = $this->ExpenseVatDetail->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }

              }
			    
                  $datasource_Journal->commit();
                  $datasource_expense->commit();
                } catch (Exception $e) {
                 $datasource_Journal->rollback();
                  $datasource_expense->rollback();
                  pr($e->getMessage());
                }
              // }

            }
               
          }
            // exit;
          fclose($handle);
        }

        $this->Session->setFlash('File uploaded successfuly');
        $this->redirect(array('action' => 'Expense_CSV_Uploader'));
      } else {
        $this->Session->setFlash(__('There was a problem uploading file. Please try again.'));
        $this->redirect(array('action' => 'Expense_CSV_Uploader'));
      }
    }
  }
}
public function Vat_CSV_Uploader()
{
  if($this->request->data)
  {
    //pr($this->request->data);
   // exit;
    $name=$this->request->data['ProductDetails']['Upload']['name'];
    $name_explod=explode('.', $name);
    if($name_explod[1]!="csv")
    {
      $this->Session->setFlash(__('It is Not in CSV Format'));
      $this->redirect(array('action' => 'Product_CSV_Uploader'));
    }
    else
    {
     $datasource_Journal = $this->Journal->getDataSource();
     $datasource_expense = $this->ExpenseVatDetail->getDataSource();
      $filename = WWW_ROOT. 'files'.DS.$name;         
      if (move_uploaded_file($this->data['ProductDetails']['Upload']['tmp_name'],$filename)) {
        define('CSV_PATH',WWW_ROOT. 'files'.DS);
        $csv_file = CSV_PATH . $name; 
       // pr($csv_file);
         // exit;
        if (($handle = fopen($csv_file, "r")) !== FALSE) {
          fgetcsv($handle);   
          $i=1;
          $code=12;
      $count=3;
          while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {   

            $num = count($data);

            for ($c=0; $c < $num; $c++) {
              $col[$c] = $data[$c];
            }
            $user_id=1;
            $sub_group_id=15;
            $date = date('Y-m-d');
            if($col[0])
            {
                
                try {
                $datasource_Journal->begin();
                $datasource_expense->begin();
                
                $id=$col[0];

                   $tax=$col[7];

			     	$this->ExpenseVatDetail->id=$count;
				if(!$this->ExpenseVatDetail->saveField('trn_number',$col[2]))
			throw new Exception("Error Processing Request", 1);
		if($col[3])
		{
		if(!$this->ExpenseVatDetail->saveField('supplier_name',$col[3]))
			throw new Exception("Error Processing Request", 1);
	   }
	   else
	   {
	   	if(!$this->ExpenseVatDetail->saveField('supplier_name',""))
			throw new Exception("Error Processing Request", 1);
	   }
			     $count++;
                  $datasource_Journal->commit();
                  $datasource_expense->commit();
                } catch (Exception $e) {
                 $datasource_Journal->rollback();
                  $datasource_expense->rollback();
                  pr($e->getMessage());
                }
              // }

            }
               
          }
            // exit;
          fclose($handle);
        }

        $this->Session->setFlash('File uploaded successfuly');
        $this->redirect(array('action' => 'Vat_CSV_Uploader'));
      } else {
        $this->Session->setFlash(__('There was a problem uploading file. Please try again.'));
        $this->redirect(array('action' => 'Vat_CSV_Uploader'));
      }
    }
  }
}
public function TraySummary()
	{
			$warehouses=$this->Warehouse->find('list');
		$this->set('warehouses',$warehouses);
	}
	public function TraySummary_ajax()
	{

		$user_branch_id=$this->Session->read('User.branch_id');
		$requestData=$this->request->data;
		$columns=[];
		$columns[]='Warehouse.name';
		$columns[]='Stock.quantity';
		$conditions=[];
		if(isset($requestData['warehouse_id']))
		{
			if($requestData['warehouse_id'])
				$conditions['Stock.warehouse_id']=$requestData['warehouse_id'];
		}
		$conditions['Stock.product_id']=1;
		$totalData=$this->Stock->find('count',['conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'Sale.date_of_delivered LIKE' =>'%'. $q . '%',
    		'Sale.invoice_no LIKE' =>'%'. $q . '%',
    		'Sale.grand_total LIKE' =>'%'. $q . '%',
    		'AccountHead.name LIKE' =>'%'. $q . '%',
    		);
    	$totalFiltered=$this->Stock->find('count',[
    		'conditions'=>$conditions,
    		]);
    }
    $Data=$this->Stock->find('all',array(
    	'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	'order'=>$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
    	'fields'=>array(
    		'Stock.*',
    		'Warehouse.id',
    		 'Warehouse.name',
    		)
    	));
    foreach ($Data as $key => $value) {
    	  $this->SaleItem->virtualFields = array('total_quantity'=>"SUM(SaleItem.quantity)");
    	$Sale = $this->SaleItem->find('first', array(
    		'conditions' => array(
    			'SaleItem.warehouse_id' => $value['Warehouse']['id'],
    			'SaleItem.product_id'=>1,
    			'SaleItem.net_value !='=>0,
    			),
    		'fields' => array(
    			'SaleItem.total_quantity',
    			)
    		));
    	    	$quantity = 0;
    	if($Sale['SaleItem']['total_quantity'])
    	{
    	$quantity+=$Sale['SaleItem']['total_quantity'];
    	}
    	$this->SalesReturnItem->virtualFields = array('total_quantity'=>"SUM(SalesReturnItem.quantity)");
    	$SalesReturn= $this->SalesReturnItem->find('first', array(
    		'conditions' => array(
    			'SalesReturnItem.warehouse_id' => $value['Warehouse']['id'],
    			'SalesReturnItem.product_id'=>1,
    			'SalesReturnItem.net_value !='=>0,
    			),
    		'fields' => array(
    			'SalesReturnItem.total_quantity',
    			)
    		));
    	if($SalesReturn['SalesReturnItem']['total_quantity'])
    	{
    	$quantity-=$SalesReturn['SalesReturnItem']['total_quantity'];
    	}
           $stock_quantity=$value['Stock']['quantity']+$quantity;
    	$Data[$key]['Stock']['quantity']=floatval($stock_quantity);
        $Data[$key]['Warehouse']['name']='<span class="warehouse_name">'.$value['Warehouse']['name'].'</span><span hidden class="warehouse_id">'.$value['Warehouse']['id'].'</span>';

    }
$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;

}
public function individual_tray_summary_ajax($warehouse_id)
{	    	
	    	$main_array=[];
	 $stock=$this->Stock->find('first',array(
	 	'conditions' => array(
        'Stock.warehouse_id' => $warehouse_id,
        'Stock.product_id'=>1,),
    	'fields'=>array(
    		'Stock.quantity',
    		"Warehouse.name"
    		)
    	));
	    $single['quantity']=floatval($stock['Stock']['quantity']);
		$single['name']=$stock['Warehouse']['name'];
	     $main_array[]=$single;
    	$Sale = $this->SaleItem->find('all', array(
    		'conditions' => array(
    			'SaleItem.warehouse_id' => $warehouse_id,
    			'SaleItem.product_id'=>1,
    			 'SaleItem.net_value !='=>0,
    			),
    		'group' => array('Sale.account_head_id'),
    		'fields' => array(
    			'SaleItem.quantity',
    			 'Sale.invoice_no',
    			 'Sale.account_head_id'
    			)
    		));
    	$quantity = 0;
    	foreach ($Sale as $key1 => $value1) {
    		$this->SaleItem->virtualFields = array('total_quantity'=>"SUM(SaleItem.quantity)");
    	$SaleItem= $this->SaleItem->find('first', array(
    		'conditions' => array(
    			'Sale.account_head_id' => $value1['Sale']['account_head_id'],
    			'SaleItem.warehouse_id' => $warehouse_id,
    			'SaleItem.product_id'=>1,
    			 'SaleItem.net_value !='=>0,
    			),
    		'fields' => array(
    			'SaleItem.total_quantity',
    			)
    		));
    	if($SaleItem['SaleItem']['total_quantity'])
    	{
    	$quantity=$SaleItem['SaleItem']['total_quantity'];
    	}
    	$this->SalesReturnItem->virtualFields = array('total_quantity'=>"SUM(SalesReturnItem.quantity)");
    	$SalesReturn= $this->SalesReturnItem->find('first', array(
    		'conditions' => array(
    			'SalesReturn.account_head_id' => $value1['Sale']['account_head_id'],
    			'SalesReturnItem.warehouse_id' => $warehouse_id,
    			'SalesReturnItem.net_value !='=>0,
    			'SalesReturnItem.product_id'=>1,
    			),
    		'fields' => array(
    			'SalesReturnItem.total_quantity',
    			)
    		));
    	if($SalesReturn['SalesReturnItem']['total_quantity'])
    	{
    	$quantity-=$SalesReturn['SalesReturnItem']['total_quantity'];
    	}
    	if($quantity!=0)
    	{
         $single['quantity']=$quantity;
        $AccountHead=$this->AccountHead->findById($value1['Sale']['account_head_id']);
		$single['name']=$AccountHead['AccountHead']['name'];
		$main_array[]=$single;
    	}
    	}
	$return['row']['tbody']='';
	foreach ($main_array as $key => $value) {
		$return['row']['tbody'].='<tr class="blue-pddng">';
		$return['row']['tbody'].='<td class="color_label">'.$value['name'].'</td>';
		$return['row']['tbody'].='<td class="color_label">'.$value['quantity'].'</td>';
		$return['row']['tbody'].='</tr>';
	}
	echo json_encode($return); exit;

}
public function DailyReport()
	{
$this->Executive->unbindModel(array('hasMany' => array('Sale')));
$conditions=[];
$conditions['Executive.block']=0;
$executives=$this->Executive->find('list',array('conditions'=>$conditions));
	$this->set('executives',$executives);
$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');
	}
//new day register report
	public function DailyReportAjax()
	{
		$requestData=$this->request->data;
	$columns = [];
	$columns[]='Executive.name';
    $columns[]='route';
	$columns[]='customer_group';
	$columns[]='day_register_time';
	$columns[]='day_close_time';
	$columns[]='starting_km';
	$columns[]='ending_km';
	$columns[]='expense';
	$columns[]='credit_sale';
	$columns[]='total_sale';
	$columns[]='receipt_amount';
	$columns[]='status';
	$columns[]='action';
	$conditions=[];
	$conditions_date=date('Y-m-d',strtotime($requestData['date']));
	$conditions['Executive.block']=0;
	$conditions['Executive.id']=$requestData['executive_id'];
		$this->Executive->unbindModel(array('hasMany' => array('Sale')));
	$totalData=$this->Executive->find('count',array(
		"joins"=>array(
			),
		'conditions'=>$conditions,
		));
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Executive.name LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->Executive->find('count',array(
			"joins"=>array(
				),
			'conditions'=>$conditions,
			));
	}
	//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column']==0)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
	$this->Executive->unbindModel(array('hasMany' => array('Sale')));
	$Data=$this->Executive->find('all',array(
		"joins"=>array(
			),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $order,
		'fields'=>array(
			'Executive.name',
			'Executive.id',
			)
		));
	foreach ($Data as $key => $value) {
       $this->DayRegister->unbindModel(array('belongsTo' => array('Executive')));
       $this->DayRegister->unbindModel(array('hasMany' => array('ClosingDay')));
		$DayRegister=$this->DayRegister->find('first',array(
			'joins'=>array(
					array(
						'table'=>'closing_days',
						'alias'=>'ClosingDay',
						'type'=>'Left',
						'conditions'=>array('ClosingDay.day_register_id=DayRegister.id'),
					),
				),
			'conditions'=>array(
				'DayRegister.executive_id'=>$value['Executive']['id'],
				'DayRegister.date'=>$conditions_date,
			),
			'fields'=>array(
				'DayRegister.*',
				'ClosingDay.*',
				'Route.name',
				'CustomerGroup.name',
				 'Route.id',
			),
		));
		      $Data[$key]['Executive']['name']='<span class="day_register_id" hidden>0</span><span class="executive_inhand_cash" hidden>0</span><span class="executive_id" hidden>'.$value['Executive']['id'].'</span>'.$value['Executive']['name'];
               $Data[$key]['Executive']['day_register_time']="";
				$Data[$key]['Executive']['starting_km']="";
				$Data[$key]['Executive']['route']="";
				$Data[$key]['Executive']['customer_group']="";
				$Data[$key]['Executive']['total_sale']=' ';
				$Data[$key]['Executive']['total_sales_return']=' ';
				$Data[$key]['Executive']['expense']=' ';
				$Data[$key]['Executive']['receipt_amount']='';
				$Data[$key]['Executive']['credit_sale']=' ';
				$Data[$key]['Executive']['cash_sale']=' ';
			    $Data[$key]['Executive']['day_close_time']=' ';
			    $Data[$key]['Executive']['ending_km']=' ';
			     $Data[$key]['Executive']['action']=' ';
			     $Data[$key]['Executive']['difference']="white";
			    $Data[$key]['Executive']['status']='<span style="color:red;">Leave<span>';
		if(!empty($DayRegister))
		{
				$this->Sale->virtualFields = array( 
				'sale_amount' => "SUM(Sale.grand_total)",
			);
				$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
				$Sale=$this->Sale->find('first',array(
				'conditions'=>array(
					'Sale.day_register_id'=>$DayRegister['DayRegister']['id'],
					'Sale.status'=>2,
				),
				'fields'=>array(
					'sale_amount',
				),
			));
				$this->SalesReturn->virtualFields = array( 
				'SalesReturn_amount' => "SUM(SalesReturn.grand_total)",
			);
				$this->SalesReturn->unbindModel(array('hasMany' => array('SalesReturnItem')));
				$SalesReturn=$this->SalesReturn->find('first',array(
				'conditions'=>array(
					'SalesReturn.day_register_id'=>$DayRegister['DayRegister']['id'],
					'SalesReturn.status'=>2,
				),
				'fields'=>array(
					'SalesReturn_amount',
				),
			));
				$this->Sale->virtualFields = array( 
				'cash_sale_amount' => "SUM(Sale.grand_total)",
			);
				$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
				$CashSale=$this->Sale->find('first',array(
				'conditions'=>array(
					'Sale.sale_type1'=>'CashSale',
					'Sale.day_register_id'=>$DayRegister['DayRegister']['id'],
					'Sale.status'=>2,
				),
				'fields'=>array(
					'cash_sale_amount',
				),
			));
				$this->Sale->virtualFields = array( 
				'credit_sale_amount' => "SUM(Sale.grand_total)",
			);
				$this->Sale->unbindModel(array('hasMany' => array('SaleItem')));
				$CreditSale=$this->Sale->find('first',array(
				'conditions'=>array(
					'Sale.sale_type1'=>'CreditSale',
					'Sale.day_register_id'=>$DayRegister['DayRegister']['id'],
					'Sale.status'=>2,
				),
				'fields'=>array(
					'credit_sale_amount',
				),
			));
				$this->ExecutiveExpenseDetail->virtualFields = array('total_expense'=>"SUM(ExecutiveExpenseDetail.amount)");
					$ExecutiveExpenseDetail=$this->ExecutiveExpenseDetail->find('first',array(
			'conditions'=>array('ExecutiveExpenseDetail.day_register_id'=>$DayRegister['DayRegister']['id'],'ExecutiveExpenseDetail.status'=>2),
			'fields'=>array('ExecutiveExpenseDetail.total_expense'),
			));   
					$total_expense=0;
					if($ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense'])
					{
						$total_expense=$ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense'];
					}
              $this->Journal->virtualFields = array('receipt_amount' => "SUM(Journal.amount)",'bonus_payment' => "SUM(Journal.amount)",'bonus_earned' => "SUM(Journal.amount)");	
              $receipt_total=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.receipt_no !='=>'',
					'Journal.day_register_id'=>$DayRegister['DayRegister']['id'],
				),
				'fields'=>array(
					'receipt_amount',
				),
			));
              $inhandcash_transfer=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.remarks'=>'Inhand Cash Transfer',
					'Journal.day_register_id'=>$DayRegister['DayRegister']['id'],
				),
				'fields'=>array(
					'receipt_amount',
				),
			));
              $bonus_payment_journal=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.remarks'=>'Bonus Payment',
					'Journal.credit !='=>1,
					'Journal.day_register_id'=>$DayRegister['DayRegister']['id'],
				),
				'fields'=>array(
					'bonus_payment',
				),
			));
              $bonus_earned_journal=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.remarks'=>'Bonus Earned',
					'Journal.day_register_id'=>$DayRegister['DayRegister']['id'],
				),
				'fields'=>array(
					'bonus_earned',
				),
			));
              $bonus_payment_journal_amount=0;
              if($bonus_payment_journal)
              {
              	$bonus_payment_journal_amount=$bonus_payment_journal['Journal']['bonus_payment'];
              }
               $bonus_earned_journal_amount=0;
              if($bonus_earned_journal)
              {
              	$bonus_earned_journal_amount=$bonus_earned_journal['Journal']['bonus_earned'];
              }
              $bonus_payment_journal_main=$this->Journal->find('first',array(
                'conditions'=>array(
                    'Journal.remarks'=>'Bonus Payment',
                    'Journal.credit'=>1,
                    'Journal.day_register_id'=>$DayRegister['DayRegister']['id'],
                ),
                'fields'=>array(
                    'bonus_payment',
                ),
            ));
               $bonus_payment_journal_main_amount=0;
              if($bonus_payment_journal_main)
              {
              	$bonus_payment_journal_main_amount=$bonus_payment_journal_main['Journal']['bonus_payment'];
              }
              $this->ExecutiveBonusCalculation->virtualFields = array(
				'total_bonus_amount' => "SUM(ExecutiveBonusCalculation.bonus_amount)",
				'total_bonus_return_amount' => "SUM(ExecutiveBonusCalculation.bonus_return_amount)",
				);
				$ExecutiveBonusCalculation=$this->ExecutiveBonusCalculation->find('first',array(
				'conditions'=>array(
				'ExecutiveBonusCalculation.day_register_id'=>$DayRegister['DayRegister']['id'],
				'ExecutiveBonusCalculation.flag'=>1,
				),
				'fields'=>array(
				'ExecutiveBonusCalculation.total_bonus_amount',
				'ExecutiveBonusCalculation.total_bonus_return_amount',
				),
				));
				$total_bonus=0;
				if($ExecutiveBonusCalculation['ExecutiveBonusCalculation']['total_bonus_amount'])
				{
              $total_bonus=$ExecutiveBonusCalculation['ExecutiveBonusCalculation']['total_bonus_amount'];
				}
				$total_bonus_return=0;
				if($ExecutiveBonusCalculation['ExecutiveBonusCalculation']['total_bonus_amount'])
				{
              $total_bonus_return=$ExecutiveBonusCalculation['ExecutiveBonusCalculation']['total_bonus_return_amount'];
				}
              $total_executive_bonus=$total_bonus-$total_bonus_return;
              $total_bonus_payment=($total_bonus-$total_bonus_return)-$bonus_payment_journal_amount-$bonus_payment_journal_main_amount;
               $total_bonus_earned=($total_bonus-$total_bonus_return)-$bonus_earned_journal_amount;
				$Data[$key]['Executive']['day_register_time']=date("d-m-Y h:i a", strtotime($DayRegister['DayRegister']['created_at']));
				$Data[$key]['Executive']['starting_km']=floatval($DayRegister['DayRegister']['km']);
				$Data[$key]['Executive']['route']='<span class="is_transfer" hidden>'.$DayRegister['DayRegister']['is_transfer'].'</span><span class="route_id" hidden>'.$DayRegister['Route']['id'].'</span><span class="route_name">'.$DayRegister['Route']['name'].'</span>';
				$Data[$key]['Executive']['customer_group']=$DayRegister['CustomerGroup']['name'];
				if($DayRegister['DayRegister']['status']==0){
				$Data[$key]['Executive']['status']='<span style="color:blue;">Day Registered<span>';
				$Data[$key]['Executive']['day_close_time']="";
			     $Data[$key]['Executive']['ending_km']="";
				}
				else
				{
				$Data[$key]['Executive']['status']='<span style="color:green;">Day Closed<span>';
				$Data[$key]['Executive']['day_close_time']=date("d-m-Y h:i a", strtotime($DayRegister['ClosingDay']['closing_at']));
			    $Data[$key]['Executive']['ending_km']=floatval($DayRegister['ClosingDay']['km']);
				}
					$inhand_cash=0;
					//if($DayRegister['DayRegister']['is_transfer']==0)
					//{
					$inhand_cash=floatval($receipt_total['Journal']['receipt_amount'])-floatval($inhandcash_transfer['Journal']['receipt_amount'])-floatval($total_expense)-floatval($bonus_payment_journal_amount);	
					//}
					$total_collection_amount=floatval($receipt_total['Journal']['receipt_amount'])-floatval($inhandcash_transfer['Journal']['receipt_amount'])-floatval($total_expense)-$total_executive_bonus;
			       $Data[$key]['Executive']['name']='<span class="bonus_hidden_payment" hidden>'.$total_bonus_earned.'</span><span class="bonus_payment" hidden>'.$total_bonus_payment.'</span><span class="day_register_id" hidden>'.$DayRegister['DayRegister']['id'].'</span><span class="executive_collected" hidden>'.$total_collection_amount.'</span><span class="executive_inhand_cash" hidden>'.$inhand_cash.'</span><span class="executive_id" hidden>'.$value['Executive']['id'].'</span>'.$value['Executive']['name'];
					$Data[$key]['Executive']['total_sale']=number_format((float)$Sale['Sale']['sale_amount'], 2, '.', '');
					$Data[$key]['Executive']['cash_sale']=number_format((float)$CashSale['Sale']['cash_sale_amount'], 2, '.', '');
					$Data[$key]['Executive']['receipt_amount']=number_format((float)$receipt_total['Journal']['receipt_amount'], 2, '.', '');
					$Data[$key]['Executive']['expense']=number_format((float)$total_expense, 2, '.', '');
					$Data[$key]['Executive']['total_sales_return']=number_format((float)$SalesReturn['SalesReturn']['SalesReturn_amount'], 2, '.', '');
					$Data[$key]['Executive']['credit_sale']=number_format((float)$CreditSale['Sale']['credit_sale_amount'], 2, '.', '');
				   $Data[$key]['Executive']['action']='<span><a  href="'.$this->webroot.'Reports/export_daily_report/'.$DayRegister['DayRegister']['id'].'"><i class="fa fa-2x fa-download"></i></span></a>';
					//$Data[$key]['Executive']['action']='<span><a target="_blank" href="'.$this->webroot.'Print/DailySummaryprint/'.$DayRegister['DayRegister']['id'].'"><i class="fa fa-2x fa-print"></i></span></a>';
				    $difference=round($Data[$key]['Executive']['cash_sale']-$Data[$key]['Executive']['total_sales_return']-$Data[$key]['Executive']['receipt_amount']);
				    $Data[$key]['Executive']['difference']="white";
				    if($difference>0)
				    {
                      $Data[$key]['Executive']['difference']="#ffcccb";
				    }
				    if($difference<0)
				    {
                      $Data[$key]['Executive']['difference']="#99ff66";
				    }
				
		}

	}
	// //sorting total/balance/recieved
		if($requestData['order'][0]['column'] >= 1) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["Executive"][$columnname] == $b["Executive"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["Executive"][$columnname] < $b["Executive"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["Executive"][$columnname] > $b["Executive"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        => $Data
		);
	echo json_encode($json_data);
	exit;
	}
	public function export_daily_report($day_register_id) 
	{
		$this->response->download("export_daily_report.csv");
				$list_array=array();
				$conditions=[];
				$DayRegister=$this->DayRegister->findById($day_register_id);
				$Route=$this->ExecutiveRouteMapping->find('list',array('conditions'=>array('ExecutiveRouteMapping.executive_id'=>$DayRegister['DayRegister']['executive_id']),'fields'=>array('ExecutiveRouteMapping.route_id','ExecutiveRouteMapping.route_id')));
				$conditions['Customer.route_id']=$Route;
				$totalData=$this->Customer->find('all',array(
				"joins"=>array(
					),	
				'conditions'=>$conditions,
				'fields'=>['Customer.account_head_id',
							'AccountHead.name',]
				));
				$general_customer=$this->Customer->find('first',array(
				'conditions'=>['Customer.account_head_id'=>3],
				'fields'=>['Customer.account_head_id',
							'AccountHead.name',]
				));
				$totalData[]=$general_customer;
             foreach ($totalData as $key => $value)
              {
             	   $single['credit_total']=0;
             	   $single['cash_total']=0;
             	   $single['sale_created_at']="";
                    $single['customer_name']=$value['AccountHead']['name'];
                      $this->Sale->virtualFields = array( 
				'Sale_amount' => "SUM(Sale.grand_total)",
			);
				$SaleCredit=$this->Sale->find('first',array(
				'conditions'=>array(
					'Sale.day_register_id'=>$day_register_id,
					'Sale.sale_type1'=>"CreditSale",
					'Sale.account_head_id'=>$value['Customer']['account_head_id'],
					'Sale.status'=>2,
				),
				'fields'=>array(
					'Sale_amount',
					'Sale.created_at'
				),
				'order'=>array('Sale.id ASC'),
			));
				$SaleCash=$this->Sale->find('first',array(
				'conditions'=>array(
					'Sale.day_register_id'=>$day_register_id,
					'Sale.sale_type1'=>"CashSale",
					'Sale.account_head_id'=>$value['Customer']['account_head_id'],
					'Sale.status'=>2,
				),
				'order'=>array('Sale.id ASC'),
				'fields'=>array(
					'Sale_amount',
					'Sale.created_at'
				),
			));   $single['credit_total']=0;$single['cash_total']=0;
                    if($SaleCredit['Sale']['Sale_amount'])
                    {
                    $single['credit_total']=$SaleCredit['Sale']['Sale_amount'];
                    $single['sale_created_at']=$SaleCredit['Sale']['created_at'];
                    }
                    if($SaleCash['Sale']['Sale_amount'])
                    {
                    	$single['cash_total']=$SaleCash['Sale']['Sale_amount'];
                    	$single['sale_created_at']=$SaleCash['Sale']['created_at'];
                    }

                   $this->SalesReturn->unbindModel(array('hasMany' => array('SalesReturnItem')));
                   $this->SalesReturn->virtualFields = array( 
				'SalesReturn_amount' => "SUM(SalesReturn.grand_total)",
			);
				$SalesReturn=$this->SalesReturn->find('first',array(
				'conditions'=>array(
					'SalesReturn.day_register_id'=>$day_register_id,
					'SalesReturn.account_head_id'=>$value['Customer']['account_head_id'],
					'SalesReturn.status'=>2,
				),
				'fields'=>array(
					'SalesReturn_amount',
				),
			));
				$single['sales_return']=0;
				if($SalesReturn['SalesReturn']['SalesReturn_amount'])
				{
                    $single['sales_return']=$SalesReturn['SalesReturn']['SalesReturn_amount'];
				}
				 $this->Journal->virtualFields = array( 
				'Journal_amount' => "SUM(Journal.amount)",
			);
              $receipt_total=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.receipt_no !='=>'',
					'Journal.day_register_id'=>$day_register_id,
					'Journal.credit'=>$value['Customer']['account_head_id'],
				),
				'fields'=>array(
					'Journal.Journal_amount',
					'Journal.created_at',
				),
			));
               $single['receipt_created_at']="";
                     $single['receipt']=0;
              if($receipt_total['Journal']['Journal_amount'])
              {
                     $single['receipt_created_at']=date('h:i a',strtotime($receipt_total['Journal']['created_at']));
                     $single['receipt']=$receipt_total['Journal']['Journal_amount'];
              }
              $single['difference']= floatval($single['cash_total'])-floatval($single['sales_return'])-floatval($single['receipt']);
              $single['difference']=round($single['difference']);
              if($single['receipt']!=0 || $single['cash_total']!=0 || $single['credit_total']!=0 || $single['sales_return']!=0)
              {
              	   $list_array[]=$single;
              }
		 }
		// pr($list_array);
		 //$time  = array_column($list_array, 'sale_created_at');
	//array_multisort($time, SORT_ASC,$list_array);
	for ($i=0; $i < count($list_array) ; $i++) { 
		//$list_array[$i]['sale_created_at']=date('h:i a',strtotime($list_array[$i]['sale_created_at']));
	}
		$this->set('dail_report', $list_array);
		$this->layout = 'ajax';
		return false;
		exit;
	}
	public function get_executive_denomination()
	{
	$data=$this->request->data;
	$conditions=[];$conditions_sale=[];
		$return=array();
	$conditions['ExecutiveDenomination.executive_id']=$data['executive_id'];
	$conditions['ExecutiveDenomination.day_register_id']=$data['day_register_id'];
	$conditions['ExecutiveDenomination.count !=']=0;
	$ExecutiveDenomination=$this->ExecutiveDenomination->find('all',array(
			'conditions'=>$conditions,
			)
		);
    $this->Sale->unbindModel(array('belongsTo'=>array('Executive'),'hasMany' => array('SaleItem')));
	$sale_first=$this->Sale->find('first',array(
			'conditions'=>['Sale.day_register_id'=>$data['day_register_id'],'Sale.executive_id'=>$data['executive_id']],
			'order'=>['Sale.id ASC'],
			'fields'=>[
			'Sale.created_at',
			],
			)
		);
	$sale_last=$this->Sale->find('first',array(
			'conditions'=>['Sale.day_register_id'=>$data['day_register_id'],'Sale.executive_id'=>$data['executive_id']],
			'order'=>['Sale.id DESC'],
			'fields'=>[
			'Sale.created_at',
			],
			)
		);
	   $return['sale_first']="";
      $return['sale_last']="";
	if($sale_first)
	{
      $return['sale_first']=date("d-m-Y H:i:s",strtotime($sale_first['Sale']['created_at']));
	}
	if($sale_last)
	{
      $return['sale_last']=date("d-m-Y H:i:s",strtotime($sale_last['Sale']['created_at']));
	}
     $return['row']['tbody']='';
     $return['row']['tfoot']='';
     $total_denomination=0;
      if($ExecutiveDenomination){
      foreach ($ExecutiveDenomination as $key => $value) {
      	$total_denomination+=$value['ExecutiveDenomination']['total_each'];
    $return['row']['tbody'].='<tr class="blue-pddng toggle_class">';
	$return['row']['tbody'].='<td>'.$value['ExecutiveDenomination']['denomination'].'</td>';
	$return['row']['tbody'].='<td>'.$value['ExecutiveDenomination']['count'].'</td>';
	$return['row']['tbody'].='<td>'.$value['ExecutiveDenomination']['total_each'].'</td>';
	if($value['ExecutiveDenomination']['is_confirm']==1)
	{
		$return['row']['tbody'].='<td>Verified</td>';
	
	}
	else
	{
	$return['row']['tbody'].='<td><span hidden  class="denomination_id">'.$value['ExecutiveDenomination']['id'].'</span><input type="checkbox" class="denomination_confirm" style="height:20px;width:20px"/></td>';
	}
	$return['row']['tbody'].='</tr>';
   }
    $return['row']['tfoot'].='<tr class="blue-pddng toggle_class">';
	$return['row']['tfoot'].='<td></td>';
	$return['row']['tfoot'].='<td style="font-size:20px; color:red;text-align:right">Total</td>';
	$return['row']['tfoot'].='<td style="font-size:20px; color:red;">'.$total_denomination.'</td>';
	$return['row']['tfoot'].='<td></td>';
 }
 	$this->SaleItem->unbindModel(array('belongsTo'=>array('Warehouse','Unit')));
 	$this->SaleItem->virtualFields = array(
			'price_difference' =>"(SaleItem.actual_price)-(SaleItem.unit_price)",
		);
	$conditions_sale['Sale.day_register_id']=$data['day_register_id'];
	$conditions_sale['SaleItem.price_difference >=']=0;
	$conditions_sale['SaleItem.is_return']=1;
	$conditions_sale['SaleItem.product_id !=']=1;
	$SaleItem=$this->SaleItem->find('all',array(
			'conditions'=>$conditions_sale,
			'fields'=>['Product.name','Sale.account_head_id','Sale.invoice_no','SaleItem.actual_price','SaleItem.unit_price','SaleItem.quantity','SaleItem.id'],
			)
		);
  $return['row']['tbody_sale']='';
      if($SaleItem){
      foreach ($SaleItem as $key => $value) {
      $bonus_reduction=$value['SaleItem']['actual_price']-$value['SaleItem']['unit_price'];
      	$customer_name=$this->AccountHead->field('AccountHead.name',array('AccountHead.id'=>$value['Sale']['account_head_id']));
      	//if($bonus_reduction>=0)
      	//{
    $return['row']['tbody_sale'].='<tr class="blue-pddng toggle_class">';
    $return['row']['tbody_sale'].='<td>'.$customer_name.'</td>';
    $return['row']['tbody_sale'].='<td style="width:5%">'.$value['Sale']['invoice_no'].'</td>';
	$return['row']['tbody_sale'].='<td>'.$value['Product']['name'].'</td>';
	$return['row']['tbody_sale'].='<td style="width:5%">'.floatval($value['SaleItem']['quantity']).'</td>';
	$return['row']['tbody_sale'].='<td style="width:5%">'.floatval($value['SaleItem']['actual_price']).'</td>';
	$return['row']['tbody_sale'].='<td style="width:5%">'.floatval($value['SaleItem']['unit_price']).'</td>';
	$return['row']['tbody_sale'].='<td style="width:5%">'.floatval($bonus_reduction*$value['SaleItem']['quantity']).'</td>';
	$return['row']['tbody_sale'].='<td style="width:20%"><span class="saleitem_id" hidden>'.$value['SaleItem']['id'].'</span><button class="btn btn-success aprv_bttn approve" value="1">Approve</button>&nbsp<button class="btn btn-danger aprv_bttn reject" value="0" >Reject</button></td>';
	$return['row']['tbody_sale'].='</tr>';
   // }
   }
 }
	echo json_encode($return);exit;
	}
	public function bonus_approval() {
				$return['result']='Error';
				if($this->request->data){
					$data=$this->request->data;
					$total_approved_amount=0;
					$total_amount=0;
					$datasource_SaleItem = $this->SaleItem->getDataSource();
					try {
						$datasource_SaleItem->begin();
						if($data['value']==0){
							$saleitem=$this->SaleItem->findById($data['id']);
							$saleitem=$this->SaleItem->find('first',array(
							'conditions'=>['SaleItem.id'=>$data['id']],
							'fields'=>['Sale.day_register_id','Sale.executive_id','Sale.invoice_no','SaleItem.id','SaleItem.sale_id','SaleItem.product_id','SaleItem.actual_price','SaleItem.unit_price','SaleItem.quantity'],
							)
							);
							$bonus_return_amount=($saleitem['SaleItem']['actual_price']-$saleitem['SaleItem']['unit_price'])*$saleitem['SaleItem']['quantity'];
					         $ProductBonus_data=[
					         'date'=>date('Y-m-d'),
					         'day_register_id'=>$saleitem['Sale']['day_register_id'],
							  'executive_id'=>$saleitem['Sale']['executive_id'],
					          'product_id'=>$saleitem['SaleItem']['product_id'],
					          'sale_item_id'=>$saleitem['SaleItem']['id'],
					          'sale_id'=>$saleitem['SaleItem']['sale_id'],
					          'unit_price'=>$saleitem['SaleItem']['unit_price'],
					          'executive_rate'=>$saleitem['SaleItem']['actual_price'],
					          'quantity'=>$saleitem['SaleItem']['quantity'],
					          'bonus_return_amount'=>$bonus_return_amount,
					          ];
					          $this->ProductBonusDetail->create();
					          if(!$this->ProductBonusDetail->save($ProductBonus_data))
					          {
					            $errors = $this->ProductBonusDetail->validationErrors;
					            foreach ($errors as $key => $value) {
					              throw new Exception($value[0], 1);
					            }
					          }
							$ProductBonus_data=[
							'day_register_id'=>$saleitem['Sale']['day_register_id'],
							  'executive_id'=>$saleitem['Sale']['executive_id'],
							'date'=>date('Y-m-d'),
							 'reference_no'=>$saleitem['Sale']['invoice_no'],
							'work_flow'=>"Bonus Return by Daily Report",
							'bonus_return_amount'=>$bonus_return_amount,
							];
							$this->ExecutiveBonusCalculation->create();
								if(!$this->ExecutiveBonusCalculation->save($ProductBonus_data))
								{
								$errors = $this->ExecutiveBonusCalculation->validationErrors;
								foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
								}
								}
								$this->SaleItem->id=$data['id'];	
							$SaleItem['is_return']=0;
							if(!$this->SaleItem->save($SaleItem))
								throw new Exception("Cant Updated SaleItem", 1);
						}
						else
						{
							$this->SaleItem->id=$data['id'];	
							$SaleItem['is_return']=2;
							if(!$this->SaleItem->save($SaleItem))
								throw new Exception("Cant Updated SaleItem", 1);

						}
								
						$datasource_SaleItem->commit();
						$return['result']='Success';
					} catch (Exception $e) {
						$return['result']='Error';
						$return['message']=$e->getMessage();
					}
				}
				echo json_encode($return);
				exit;
			}
			public function denomination_confirm() {
				$return['result']='Error';
				if($this->request->data){
					$data=$this->request->data;
					$total_approved_amount=0;
					$total_amount=0;
					$datasource_ExecutiveDenomination = $this->ExecutiveDenomination->getDataSource();
					try {
						$datasource_ExecutiveDenomination->begin();
								$this->ExecutiveDenomination->id=$data['id'];	
							$ExecutiveDenomination['is_confirm']=1;
							if(!$this->ExecutiveDenomination->save($ExecutiveDenomination))
								throw new Exception("Cant Updated SaleItem", 1);	
						$datasource_ExecutiveDenomination->commit();
						$return['result']='Success';
					} catch (Exception $e) {
						$return['result']='Error';
						$return['message']=$e->getMessage();
					}
				}
				echo json_encode($return);
				exit;
			}
	public  function inhandcash_transfer()
	{
		$return['result']="empty";
		$datasource_Journal = $this->Journal->getDataSource();
			try {
				$datasource_Journal->begin();
				$data=$this->request->data;
				$debit=1;
				$user_id=1;
				$executive_id=$data['executive_id'];
				$route_id=$data['route_id'];
				$day_register_id=$data['day_register_id'];
				$credit = $this->Route->field(
					'Route.account_head_id',
					array('id ' => $data['route_id']));
				if($credit==$debit)
					throw new Exception("Cant Use Same head", 1);
				$amount=(int)$data['amount'];
				$amount_transfer=(int)$data['amount']-(int)$data['bonus_payment_hidden'];
				$bonus_payment=(int)$data['bonus_payment'];
				$bonus_payment_hidden=(int)$data['bonus_payment_hidden'];
				$collection_amount=(int)$data['collection_amount'];
				$executive_cash_inhand=(int)$data['collection_amount']-(int)$amount;
				$date=$data['date'];
				$remarks="Inhand Cash Transfer";
				$voucher_no='';
				$external_voucher="";
				$work_flow='Daily Report';
			   $AccountingsController = new AccountingsController;
					$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,$day_register_id,'',$executive_id,$route_id);
				if($function_return['result']!='Success')
					throw new Exception($function_return['result'], 1);
				// if($executive_cash_inhand>0)
				// {
				// 	$remarks="Balance Inhand Cash Transfer";
				// $function_return=$AccountingsController->JournalCreate($debit,$credit,$executive_cash_inhand,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,$day_register_id,'',$executive_id,$route_id);
				// if($function_return['result']!='Success')
				// 	throw new Exception($function_return['result'], 1);
			 //    }
				$Staff_id =$this->Executive->field('staff_id',array('Executive.id'=>$executive_id));
				$account_head_id =$this->Staff->field('account_head_id',array('Staff.id'=>$Staff_id));
				$debit=4646;
				$remarks="Bonus Payment";
				$voucher_no='';
				$external_voucher="";
				$work_flow='Bonus Payable From Daily Report';
				$credit = $this->Route->field(
					'Route.account_head_id',
					array('id ' => $data['route_id']));
				$function_return=$AccountingsController->JournalCreate($credit,$account_head_id,$bonus_payment,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,$day_register_id,'',$executive_id,$route_id);
			   if($function_return['result']!='Success') 
					throw new Exception($function_return['result'], 1);
				if($bonus_payment_hidden>0)
				{
				$credit = $this->Route->field(
					'Route.account_head_id',
					array('id ' => $data['route_id']));
				$work_flow='Bonus Earned From Daily Report';
				$remarks="Bonus Earned";
				$function_return=$AccountingsController->JournalCreate($account_head_id,$debit,$bonus_payment_hidden,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,$day_register_id,'',$executive_id,$route_id);
			   if($function_return['result']!='Success') 
					throw new Exception($function_return['result'], 1);
				}
				$this->DayRegister->id=$day_register_id;
				if(!$this->DayRegister->saveField('is_transfer',1))
				throw new Exception("Cant update", 1);
				$return['result']='Success';
				$datasource_Journal->commit();
			} catch (Exception $e) {
				$datasource_Journal->rollback();
				$return['result']=$e->getMessage();
			}
			echo json_encode($return);
	exit;
	}
	public function DateWiseBonusReport()
{
	$executives=$this->Executive->find('list',array('fields'=>array('id','name')));
	$this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');

}
public function DateWiseBonusReport_ajax()
{
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='ExecutiveBonusCalculation.date';
	$columns[]='ExecutiveBonusCalculation.id';
	$columns[]='ExecutiveBonusCalculation.id';
	$columns[]='ExecutiveBonusCalculation.id';
	$columns[]='ExecutiveBonusCalculation.id';
	$columns[]='ExecutiveBonusCalculation.id';
	$columns[]='ExecutiveBonusCalculation.id';
	$conditions=[];$conditions_receipt=[];$conditions_cash=[];$conditions_inhand=[];
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	//$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$time = strtotime($requestData['to_date']);
	$to_date = date('Y-m-d', strtotime("+1 day",$time));
	$period = new DatePeriod(
     new DateTime($from_date),
     new DateInterval('P1D'),
     new DateTime($to_date)
);
	$Data=[];
	foreach ($period as $key1 => $value1) 
	{
	  $single['date']=$value1->format('Y-m-d');
	  $Data[]=$single;
	}
	$totalData=count($Data);
	$totalFiltered=$totalData;
	foreach ($Data as $key => $value) 
	{
		$executive_id=$requestData['executive_id'];
  $DayRegister = $this->DayRegister->findByExecutiveIdAndDate($executive_id,$value['date']);
      $Data[$key]['ExecutiveBonusCalculation']['date']=date('d-m-Y',strtotime($value['date']));
  if($DayRegister)
  {
  	$conditions_receipt['Journal.day_register_id']=$DayRegister['DayRegister']['id'];
  	$conditions_inhand['Journal.day_register_id']=$DayRegister['DayRegister']['id'];
	$conditions['ExecutiveBonusCalculation.day_register_id']=$DayRegister['DayRegister']['id'];
	if(!empty($executive_id)) { 
		$conditions['ExecutiveBonusCalculation.executive_id']=$executive_id; 
		$conditions_receipt['Journal.executive_id']=$executive_id;
	$conditions_inhand['Journal.executive_id']=$executive_id;
	}
			$this->ExecutiveBonusCalculation->virtualFields = array(
			'total_bonus_amount' => "SUM(ExecutiveBonusCalculation.bonus_amount)",
			'total_bonus_return_amount' => "SUM(ExecutiveBonusCalculation.bonus_return_amount)",
		);
		$Bonus=$this->ExecutiveBonusCalculation->find('first',array(
		'conditions'=>$conditions,
		'fields'=>array(
		'ExecutiveBonusCalculation.total_bonus_amount',
		'ExecutiveBonusCalculation.total_bonus_return_amount',
			),
		));
		$conditions_receipt['Journal.receipt_no !=']='';
	$conditions_inhand['Journal.remarks']="Inhand Cash Transfer";
		$this->Journal->virtualFields = array('receipt_amount' => "SUM(Journal.amount)",);	
              $receipt_total=$this->Journal->find('first',array(
				'conditions'=>$conditions_receipt,
				'fields'=>array(
					'receipt_amount',
				),
			));
              $inhandcash_transfer=$this->Journal->find('first',array(
				'conditions'=>$conditions_inhand,
				'fields'=>array(
					'receipt_amount',
				),
			));
               $this->Journal->virtualFields = array('bonus_payment' => "SUM(Journal.amount)");    
              $bonus_payment_journal=$this->Journal->find('first',array(
                'conditions'=>array(
                    'Journal.remarks'=>'Bonus Payment',
                    'Journal.executive_id'=>$executive_id,
                    'Journal.day_register_id'=>$DayRegister['DayRegister']['id'],
                ),
                'fields'=>array(
                    'bonus_payment',
                ),
            ));
               $bonus_payment_journal_amount=0;
              if($bonus_payment_journal)
              {
              	$bonus_payment_journal_amount=$bonus_payment_journal['Journal']['bonus_payment'];
              }
              $bonus_payment_journal_main=$this->Journal->find('first',array(
                'conditions'=>array(
                    'Journal.remarks'=>'Bonus Payment',
                    'Journal.credit'=>1,
                    'Journal.day_register_id'=>$DayRegister['DayRegister']['id'],
                ),
                'fields'=>array(
                    'bonus_payment',
                ),
            ));
               $bonus_payment_journal_main_amount=0;
              if($bonus_payment_journal_main)
              {
              	$bonus_payment_journal_main_amount=$bonus_payment_journal_main['Journal']['bonus_payment'];
              }
              $this->ExecutiveExpenseDetail->virtualFields = array('total_expense'=>"SUM(ExecutiveExpenseDetail.amount)");
					$ExecutiveExpenseDetail=$this->ExecutiveExpenseDetail->find('first',array(
			'conditions'=>array('ExecutiveExpenseDetail.day_register_id'=>$DayRegister['DayRegister']['id'],'ExecutiveExpenseDetail.status'=>2),
			'fields'=>array('ExecutiveExpenseDetail.total_expense'),
			));   
					$total_expense=0;
					if($ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense'])
					{
						$total_expense=$ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense'];
					}
         $total_executive_bonus=round(($Bonus['ExecutiveBonusCalculation']['total_bonus_amount']-$Bonus['ExecutiveBonusCalculation']['total_bonus_return_amount']),2);
         $balance=floatval($receipt_total['Journal']['receipt_amount']+$bonus_payment_journal_main_amount-$total_executive_bonus-$inhandcash_transfer['Journal']['receipt_amount']-$total_expense);
        $Data[$key]['ExecutiveBonusCalculation']['bonus_paid']=round($bonus_payment_journal_amount,2);
       $Data[$key]['ExecutiveBonusCalculation']['total_collected']=floatval($inhandcash_transfer['Journal']['receipt_amount']);
	   $Data[$key]['ExecutiveBonusCalculation']['total_collection_amt']=round(($receipt_total['Journal']['receipt_amount']-$total_expense),2);
	   $Data[$key]['ExecutiveBonusCalculation']['total_bonus']=round(($Bonus['ExecutiveBonusCalculation']['total_bonus_amount']-$Bonus['ExecutiveBonusCalculation']['total_bonus_return_amount']),2);
	 $Data[$key]['ExecutiveBonusCalculation']['action']='<span hidden class="day_register_id">'.$DayRegister['DayRegister']['id'].'</span>';
	  $Data[$key]['ExecutiveBonusCalculation']['balance']=round($balance,3);
	  if($balance < 0)
	  {
	 $Data[$key]['ExecutiveBonusCalculation']['balance']=round($balance,3);
	 $Data[$key]['ExecutiveBonusCalculation']['action']='<span hidden class="balance">'.round($balance,3).'</span><span hidden class="type_balance">PAY</span><span hidden class="day_register_id">'.$DayRegister['DayRegister']['id'].'</span><button class="button_inhandcash" style="background-color: #4CAF50;color: white;">PAY</button>';
      }
       if($balance > 0)
	  {
	  $Data[$key]['ExecutiveBonusCalculation']['balance']='<input type="text" class="form-control text-right" value="'.round($balance,3).'"/>';
	 $Data[$key]['ExecutiveBonusCalculation']['action']='<span hidden class="balance">'.round($balance,3).'</span><span hidden class="type_balance">RECEIVE</span><span hidden class="day_register_id">'.$DayRegister['DayRegister']['id'].'</span><button class="button_inhandcash" style="background-color: #FF0000;color: white;">RECEIVE</button>';
      }
     }
     else
     {
     $Data[$key]['ExecutiveBonusCalculation']['bonus_paid']=0;
       $Data[$key]['ExecutiveBonusCalculation']['total_collected']=0;
	   $Data[$key]['ExecutiveBonusCalculation']['total_collection_amt']=0;
	   $Data[$key]['ExecutiveBonusCalculation']['balance']=0;
	   $Data[$key]['ExecutiveBonusCalculation']['total_bonus']=0;
	    $Data[$key]['ExecutiveBonusCalculation']['action']='';	
     }
}
	$json_data=array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData), 
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public function ExecutiveDateWiseBonusReport()
{
	$executives=$this->Executive->find('list',array('fields'=>array('id','name')));
	$this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');

}
public function ExecutiveDateWiseBonusReport_ajax()
{
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='Executive.name';
	$columns[]='total_bonus';
	$columns[]='bonus_paid';
	$columns[]='total_collection_amt';
	$columns[]='total_collected';
	$columns[]='balance';
	$conditions=[];$conditions_bonus=[];$conditions_receipt=[];$conditions_cash=[];$conditions_inhand=[];
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	if(!empty($requestData['executive_id']) || $requestData['executive_id']=='0')
	{
			$conditions['Executive.id']=$requestData['executive_id'];	
	}
	//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column']==0)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
	$totalData=$this->Executive->find('count',[
		'conditions'=>$conditions,
			]);
	$totalFiltered=$totalData;
	if(!empty($requestData['search']['value'])){ 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Executive.name LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->Executive->find('count',[
		'conditions'=>$conditions,
			]);

	}
	$this->Executive->unbindModel(array('hasMany' => array('Sale')));
	$Data=$this->Executive->find('all',array(
	'conditions'=>$conditions,
	'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
      'order'=> $order,
		'fields'=>array(
			'Executive.id',
			'Executive.name',
			),
		));
	foreach ($Data as $key => $value) 
	{
		$executive_id=$value['Executive']['id'];
  	    $conditions_bonus['ExecutiveBonusCalculation.date between ? and ?']=[$from_date,$to_date];
  	    $conditions_bonus['ExecutiveBonusCalculation.executive_id']=$executive_id;
		$conditions_receipt['Journal.date between ? and ?']=[$from_date,$to_date];
		$conditions_receipt['Journal.executive_id']=$executive_id;
		$conditions_inhand['Journal.date between ? and ?']=[$from_date,$to_date];
	     $conditions_inhand['Journal.executive_id']=$executive_id;
	   
			$this->ExecutiveBonusCalculation->virtualFields = array(
			'total_bonus_amount' => "SUM(ExecutiveBonusCalculation.bonus_amount)",
			'total_bonus_return_amount' => "SUM(ExecutiveBonusCalculation.bonus_return_amount)",
		);
		$Bonus=$this->ExecutiveBonusCalculation->find('first',array(
		'conditions'=>$conditions_bonus,
		'fields'=>array(
		'ExecutiveBonusCalculation.total_bonus_amount',
		'ExecutiveBonusCalculation.total_bonus_return_amount',
			),
		));
		$conditions_receipt['Journal.receipt_no !=']='';
	$conditions_inhand['Journal.remarks']="Inhand Cash Transfer";
		$this->Journal->virtualFields = array('receipt_amount' => "SUM(Journal.amount)",);	
              $receipt_total=$this->Journal->find('first',array(
				'conditions'=>$conditions_receipt,
				'fields'=>array(
					'receipt_amount',
				),
			));
              $inhandcash_transfer=$this->Journal->find('first',array(
				'conditions'=>$conditions_inhand,
				'fields'=>array(
					'receipt_amount',
				),
			));
               $this->Journal->virtualFields = array('bonus_payment' => "SUM(Journal.amount)");    
              $bonus_payment_journal=$this->Journal->find('first',array(
                'conditions'=>array(
                    'Journal.remarks'=>'Bonus Payment',
                    'Journal.executive_id'=>$executive_id,
                    'Journal.date between ? and ?'=>array($from_date,$to_date),
                ),
                'fields'=>array(
                    'bonus_payment',
                ),
            ));
               $bonus_payment_journal_amount=0;
              if($bonus_payment_journal)
              {
              	$bonus_payment_journal_amount=$bonus_payment_journal['Journal']['bonus_payment'];
              }
              $bonus_payment_journal_main=$this->Journal->find('first',array(
                'conditions'=>array(
                    'Journal.remarks'=>'Bonus Payment',
                    'Journal.credit'=>1,
                     'Journal.executive_id'=>$executive_id,
                    'Journal.date between ? and ?'=>array($from_date,$to_date),
                ),
                'fields'=>array(
                    'bonus_payment',
                ),
            ));
               $bonus_payment_journal_main_amount=0;
              if($bonus_payment_journal_main)
              {
              	$bonus_payment_journal_main_amount=$bonus_payment_journal_main['Journal']['bonus_payment'];
              }
              $this->ExecutiveExpenseDetail->virtualFields = array('total_expense'=>"SUM(ExecutiveExpenseDetail.amount)");
					$ExecutiveExpenseDetail=$this->ExecutiveExpenseDetail->find('first',array(
			'conditions'=>array('ExecutiveExpenseDetail.date between ? and ?'=>array($from_date,$to_date),'ExecutiveExpenseDetail.executive_id'=>$executive_id,'ExecutiveExpenseDetail.status'=>2),
			'fields'=>array('ExecutiveExpenseDetail.total_expense'),
			));   
					$total_expense=0;
					if($ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense'])
					{
						$total_expense=$ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense'];
					}
        $total_executive_bonus=round(($Bonus['ExecutiveBonusCalculation']['total_bonus_amount']-$Bonus['ExecutiveBonusCalculation']['total_bonus_return_amount']),2);
       $balance=floatval($receipt_total['Journal']['receipt_amount']+$bonus_payment_journal_main_amount-$total_executive_bonus-$inhandcash_transfer['Journal']['receipt_amount']-$total_expense);
       $Data[$key]['Executive']['bonus_paid']=round($bonus_payment_journal_amount,2);
       $Data[$key]['Executive']['total_collected']=floatval($inhandcash_transfer['Journal']['receipt_amount']);
	   $Data[$key]['Executive']['total_collection_amt']=round(($receipt_total['Journal']['receipt_amount']-$total_expense),2);
	   $Data[$key]['Executive']['balance']=floatval($balance);
	   $Data[$key]['Executive']['total_bonus']=round(($Bonus['ExecutiveBonusCalculation']['total_bonus_amount']-$Bonus['ExecutiveBonusCalculation']['total_bonus_return_amount']),2);
}
//sorting total/balance/recieved
		if($requestData['order'][0]['column'] > 0) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["Executive"][$columnname] == $b["Executive"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["Executive"][$columnname] < $b["Executive"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["Executive"][$columnname] > $b["Executive"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
	$json_data=array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData), 
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public function ExecutiveWiseBlockAmountReport()
{
	$executives=$this->Executive->find('list',array('fields'=>array('id','name')));
	$this->set(compact('executives'));
	$this->request->data['from_date']=date('d-m-Y');
	$this->request->data['to_date']=date('d-m-Y');

}
public function ExecutiveWiseBlockAmountReport_ajax()
{
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='Executive.name';
	$columns[]='is_block';
	$columns[]='block_amount';
	$columns[]='amount';
	$conditions=[];
	if(!empty($requestData['executive_id']) || $requestData['executive_id']=='0')
	{
			$conditions['Executive.id']=$requestData['executive_id'];	
	}
	//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column']==0)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
	$totalData=$this->Executive->find('count',[
		'conditions'=>$conditions,
			]);
	$totalFiltered=$totalData;
	if(!empty($requestData['search']['value'])){ 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Executive.name LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->Executive->find('count',[
		'conditions'=>$conditions,
			]);

	}
	$this->Executive->unbindModel(array('hasMany' => array('Sale')));
	$Data=$this->Executive->find('all',array(
	'conditions'=>$conditions,
	'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
      'order'=> $order,
		'fields'=>array(
			'Executive.id',
			'Executive.name',
			),
		));
	foreach ($Data as $key => $value) 
	{
		$executive_id=$value['Executive']['id'];
		$block_days=$this->SystemParameter->field('value',array('id'=>8));
		$amount_limit=$this->SystemParameter->field('value',array('id'=>17));
		$block_days_new=$block_days-1;
		$date=date("Y-m-d", strtotime('-'.$block_days_new.'day'));
       // $DayRegister = $this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive_id,'DayRegister.date' => $date)));
		// if($DayRegister)
		// {
		$this->Journal->virtualFields = array('receipt_amount' => "SUM(Journal.amount)",'bonus_payment' => "SUM(Journal.amount)");	
              $receipt_total=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.receipt_no !='=>'',
					'Journal.executive_id'=>$value['Executive']['id'],
				),
				'fields'=>array(
					'receipt_amount',
				),
			));
              $receipt_total_block=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.receipt_no !='=>'',
					'Journal.executive_id'=>$value['Executive']['id'],
					'Journal.date <='=>$date,
				),
				'fields'=>array(
					'receipt_amount',
				),
			));
              $inhandcash_transfer=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.remarks'=>'Inhand Cash Transfer',
					'Journal.executive_id'=>$value['Executive']['id'],
				),
				'fields'=>array(
					'receipt_amount',
				),
			));
              $inhandcash_transfer_block=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.remarks'=>'Inhand Cash Transfer',
					'Journal.executive_id'=>$value['Executive']['id'],
					//'Journal.date <='=>$date,
				),
				'fields'=>array(
					'receipt_amount',
				),
			));
                $this->ExecutiveExpenseDetail->virtualFields = array('total_expense'=>"SUM(ExecutiveExpenseDetail.amount)");
					$ExecutiveExpenseDetail=$this->ExecutiveExpenseDetail->find('first',array(
			'conditions'=>array('ExecutiveExpenseDetail.executive_id'=>$value['Executive']['id'],'ExecutiveExpenseDetail.status'=>2),
			'fields'=>array('ExecutiveExpenseDetail.total_expense'),
			));   
					$total_expense=0;
					if($ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense'])
					{
						$total_expense=$ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense'];
					}
					$ExecutiveExpenseDetail_block=$this->ExecutiveExpenseDetail->find('first',array(
			'conditions'=>array('ExecutiveExpenseDetail.executive_id'=>$value['Executive']['id'],'ExecutiveExpenseDetail.date <='=>$date,'ExecutiveExpenseDetail.status'=>2),
			'fields'=>array('ExecutiveExpenseDetail.total_expense'),
			));   
					$total_expense_block=0;
					if($ExecutiveExpenseDetail_block['ExecutiveExpenseDetail']['total_expense'])
					{
						$total_expense_block=$ExecutiveExpenseDetail_block['ExecutiveExpenseDetail']['total_expense'];
					}

			 $bonus_payment_journal=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.remarks'=>'Bonus Payment',
					'Journal.credit !='=>1,
					'Journal.executive_id'=>$value['Executive']['id'],
				),
				'fields'=>array(
					'bonus_payment',
				),
			));
			 $bonus_payment_journal_amount=0;
              if($bonus_payment_journal)
              {
              	$bonus_payment_journal_amount=$bonus_payment_journal['Journal']['bonus_payment'];
              }
              $bonus_payment_journal_block=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.remarks'=>'Bonus Payment',
					'Journal.credit !='=>1,
					'Journal.date <='=>$date,
					'Journal.executive_id'=>$value['Executive']['id'],
				),
				'fields'=>array(
					'bonus_payment',
				),
			));
			 $bonus_payment_journal_amount_block=0;
              if($bonus_payment_journal_block)
              {
              	$bonus_payment_journal_amount_block=$bonus_payment_journal_block['Journal']['bonus_payment'];
              }
               $amount=floatval($receipt_total['Journal']['receipt_amount'])-floatval($inhandcash_transfer['Journal']['receipt_amount'])-floatval($total_expense)-floatval($bonus_payment_journal_amount);	
			   $block_amount=floatval($receipt_total_block['Journal']['receipt_amount'])-floatval($inhandcash_transfer_block['Journal']['receipt_amount'])-floatval($total_expense_block)-floatval($bonus_payment_journal_amount_block);	

				$is_block="NO";
	   $Data[$key]['Executive']['colour']="white";
					if($block_amount>$amount_limit)
					{
                      $is_block="YES";
	   $Data[$key]['Executive']['colour']="#ffcccb";
					}
	   $Data[$key]['Executive']['is_block']=$is_block;
	   $Data[$key]['Executive']['amount']=number_format($amount,2,'.','');
	   $Data[$key]['Executive']['block_amount']=number_format($block_amount,2,'.','');

	 //}
	 // else
	 // {
	 // 	 $Data[$key]['Executive']['is_block']="NO";
	 //   $Data[$key]['Executive']['block_amount']="0.00";
	 // }
}
//sorting total/balance/recieved
		if($requestData['order'][0]['column'] > 0) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["Executive"][$columnname] == $b["Executive"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["Executive"][$columnname] < $b["Executive"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["Executive"][$columnname] > $b["Executive"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
	$json_data=array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData), 
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public  function inhandcash_transfer_by_bonus_report()
	{
		$return['result']="empty";
		$datasource_Journal = $this->Journal->getDataSource();
			try {
				$datasource_Journal->begin();
				$data=$this->request->data;
				$credit=1;
				$debit=1;
				$user_id=1;
				$day_register_id=$data['day_register_id'];
				 $DayRegister = $this->DayRegister->findById($day_register_id);
                  $route_id=$DayRegister['DayRegister']['route_id'];
                   $executive_id=$DayRegister['DayRegister']['executive_id'];
				// $credit = $this->Route->field(
				// 	'Route.account_head_id',
				// 	array('id ' => $route_id));
				$Staff_id =$this->Executive->field('staff_id',array('Executive.id'=>$executive_id));
				$account_head_id =$this->Staff->field('account_head_id',array('Staff.id'=>$Staff_id));
				// if($credit==$debit)
				// 	throw new Exception("Cant Use Same head", 1);
				$voucher_no='';
				$external_voucher="";
				$date=date('d-m-Y');
			    $AccountingsController = new AccountingsController;
				if($data['type_balance']=="PAY")
				{
				$amount=-$data['balance'];
				$remarks="Bonus Payment";
				$work_flow='Bonus Payable From Daily Report';
				$function_return=$AccountingsController->JournalCreate($credit,$account_head_id,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,$day_register_id,'',$executive_id,$route_id);
				if($function_return['result']!='Success')
					throw new Exception($function_return['result'], 1);
			   }
			   if($data['type_balance']=="RECEIVE")
				{
				$credit = $this->Route->field(
					'Route.account_head_id',
					array('id ' => $route_id));
				$amount=$data['balance'];
				$remarks="Inhand Cash Transfer";
				$work_flow='Daily Report';
				$function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$external_voucher,$day_register_id,'',$executive_id,$route_id);
				if($function_return['result']!='Success')
					throw new Exception($function_return['result'], 1);
			  }
				$return['result']='Success';
				$datasource_Journal->commit();
			} catch (Exception $e) {
				$datasource_Journal->rollback();
				$return['result']=$e->getMessage();
			}
			echo json_encode($return);
	exit;
	}
public function get_ProductBonus_details()
	{
	$data=$this->request->data;
	$conditions=[];
		$return=array();
	$conditions['ProductBonusDetail.executive_id']=$data['executive_id'];
	$conditions['ProductBonusDetail.day_register_id']=$data['day_register_id'];
	$ProductBonusDetail=$this->ProductBonusDetail->find('all',array(
		'joins'=>array(
				array(
					'table'=>'products',
					'alias'=>'Product',
					'type'=>'INNER',
					'conditions'=>array('Product.id=ProductBonusDetail.product_id')
					),
				),
			'conditions'=>$conditions,
			'fields'=>array(
			'Product.name',
			'ProductBonusDetail.*'
			),
			)
		);
     $return['row']['tbody']='';
     	$return['row']['tfoot']='';
     	$total_bonus=0;$return_bonus=0;$total_amount=0;
      if($ProductBonusDetail){
      foreach ($ProductBonusDetail as $key => $value) {
      	$total_bonus+=$value['ProductBonusDetail']['bonus_amount'];
      	$return_bonus+=$value['ProductBonusDetail']['bonus_return_amount'];
      	$total_amount=$total_bonus-$return_bonus;
    $return['row']['tbody'].='<tr class="blue-pddng toggle_class">';
	$return['row']['tbody'].='<td>'.$value['Product']['name'].'</td>';
	$return['row']['tbody'].='<td>'.$this->Sale->field('Sale.invoice_no',array('Sale.id'=>$value['ProductBonusDetail']['sale_id'])).'</td>';
	$return['row']['tbody'].='<td>'.$this->SalesReturn->field('SalesReturn.invoice_no',array('SalesReturn.id'=>$value['ProductBonusDetail']['sales_return_id'])).'</td>';
	$return['row']['tbody'].='<td>'.$this->StockTransfer->field('StockTransfer.transfer_no',array('StockTransfer.id'=>$value['ProductBonusDetail']['vtob_id'])).'</td>';
	$return['row']['tbody'].='<td>'.floatval($value['ProductBonusDetail']['bonus_amount']).'</td>';
	$return['row']['tbody'].='<td>'.floatval($value['ProductBonusDetail']['bonus_return_amount']).'</td>';
	$return['row']['tbody'].='<td>'.floatval($value['ProductBonusDetail']['bonus_amount']-$value['ProductBonusDetail']['bonus_return_amount']).'</td>';
	$return['row']['tbody'].='</tr>';
   }
        $return['row']['tfoot']= $return['row']['tfoot'].'<tr>';
		$return['row']['tfoot']= $return['row']['tfoot'].'<td style="font-size:20px; color:red;text-align:right" colspan="4">Total</h3></td>';
		$return['row']['tfoot']= $return['row']['tfoot'].'<td style="font-size:20px; color:red;">'.$total_bonus.'</td>';
		$return['row']['tfoot']= $return['row']['tfoot'].'<td style="font-size:20px; color:red;">'.$return_bonus.'</td>';
		$return['row']['tfoot']= $return['row']['tfoot'].'<td style="font-size:20px; color:red;">'.$total_amount.'</td>';
		$return['row']['tfoot']= $return['row']['tfoot'].'</tr>';
 }
	echo json_encode($return);exit;
	}
	public function SaleReturnReport()
	{
	$brands=$this->Brand->find('list');
	$producttypes=$this->ProductType->find('list');
$routes=$this->Route->find('list',array('fields'=>array('id','name')));
			$this->set(compact('routes'));	
			$this->set(compact('brands','producttypes'));
		$customertypes=$this->CustomerType->find('list',array('fields'=>array('id','name')));
		$this->set('from',date("d-m-Y", strtotime('first day of this month')));
		$this->set('to',date("d-m-Y", strtotime('last day of this month')));
		$executives['']='All';
		$executives['0']='No Executive';
		$executives+=$this->Executive->find('list',array('conditions' => 'block!=1','fields'=>array('id','name')));
		$this->set(compact('executives'));
		$this->set(compact('customertypes'));
	}
public function return_report_ajax(){
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='Product.name';
	$columns[]='sale_qty';
	$columns[]='return_qty';
	//$columns[]='net_qty';
	$conditions=[];
	if(isset($requestData['product_type_id']))
	{
		if($requestData['product_type_id'])
			$conditions['Product.product_type_id']=$requestData['product_type_id'];	
	}
	if(isset($requestData['brand_id']))
	{
		if($requestData['brand_id'])
			$conditions['Product.brand_id']=$requestData['brand_id'];	
	}
	if(isset($requestData['product_id']))
	{
		if($requestData['product_id'])
			$conditions['Product.id']=$requestData['product_id'];	
	}
	$conditions['Product.type']=[2,8];	
	$this->Product->unbindModel(
		array('hasMany' => array(
			'PurchaseReturnItem',
			'PurchasedItem',
			'SaleItem',
			'SalesReturnItem',
			'StockLog',
			'UnwantedList',
			'Stock'
			)
		));
	$totalData=$this->Product->find('count',array(
		"joins"=>array(
			),
		'conditions'=>$conditions,
		));
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Product.name LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->Product->find('count',array(
			"joins"=>array(
				),
			'conditions'=>$conditions,
			));
	}
	$this->Product->unbindModel(
		array('hasMany' => array(
			'PurchaseReturnItem',
			'PurchasedItem',
			'SaleItem',
			'SalesReturnItem',
			'StockLog',
			'UnwantedList',
			'Stock'
			)
		));
	//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column']==0)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
	$Data=$this->Product->find('all',array(
		"joins"=>array(
			),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $order,
		'fields'=>array(
			'Product.cost',
			'Product.name',
			'Product.id',
			)
		));
	foreach ($Data as $key => $value) {
	$conditions_sale=[];$conditions_sales_return=[];
		if(!empty($requestData['executive_id']) || $requestData['executive_id']=='0')
	{
			$conditions_sale['Sale.executive_id']=$requestData['executive_id'];	
			$conditions_sales_return['SalesReturn.executive_id']=$requestData['executive_id'];	
	}
	if(isset($requestData['customer_id']))
	{
		if($requestData['customer_id'])
			{
			$conditions_sale['Sale.account_head_id']=$requestData['customer_id'];
			$conditions_sales_return['SalesReturn.account_head_id']=$requestData['customer_id'];
			}		
	}
	if(isset($requestData['customer_type_id']))
	{
		if($requestData['customer_type_id'])
			{
			$conditions_sale['Customer.customer_type_id']=$requestData['customer_type_id'];	
			$conditions_sales_return['Customer.customer_type_id']=$requestData['customer_type_id'];
			}	
	}
	if(isset($requestData['route_id']))
	{
		if($requestData['route_id'])
		{
			$conditions_sale['Customer.route_id']=$requestData['route_id'];
			$conditions_sales_return['Customer.route_id']=$requestData['route_id'];
    	}		
	}
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions_sale['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
   $conditions_sales_return['SalesReturn.date between ? and ?']=[$from_date,$to_date];
    $conditions_sale['SaleItem.product_id']=$value['Product']['id'];
    $conditions_sales_return['SalesReturnItem.product_id']=$value['Product']['id'];
    $conditions_sale['Sale.flag']=1;
    $conditions_sale['Sale.status']=2;
    $conditions_sales_return['SalesReturn.flag']=1;
    $conditions_sales_return['SalesReturn.status']=2;
   $this->SaleItem->virtualFields = array('Sale_Qty'=>"SUM(SaleItem.quantity)");
		$SaleItem=$this->SaleItem->find('first',array(
			"joins"=>array(	
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'INNER',
				"conditions"=>array('Customer.account_head_id=Sale.account_head_id'),
				),
			),
			'conditions'=>$conditions_sale,
			'fields'=>array(
				'SaleItem.Sale_Qty',
				)
			));

		$this->SalesReturnItem->virtualFields = array('SaleReturn_Qty'=>"SUM(SalesReturnItem.quantity)");
		$SalesReturnItem=$this->SalesReturnItem->find('first',array(
			"joins"=>array(
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'INNER',
				"conditions"=>array('Customer.account_head_id=SalesReturn.account_head_id'),
				),
			),
			'conditions'=>$conditions_sales_return,
			'fields'=>array(
				'SalesReturnItem.SaleReturn_Qty',
				)
			));
		$Data[$key]['Product']['sale_qty']=round($SaleItem['SaleItem']['Sale_Qty'],3);
		$Data[$key]['Product']['return_qty']= round($SalesReturnItem['SalesReturnItem']['SaleReturn_Qty'],3);
		$net_qty=$SaleItem['SaleItem']['Sale_Qty']-$SalesReturnItem['SalesReturnItem']['SaleReturn_Qty'];
		$Data[$key]['Product']['net_qty']=round($net_qty,3);
	}
	// //sorting total/balance/recieved
		if($requestData['order'][0]['column'] >= 1) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["Product"][$columnname] == $b["Product"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["Product"][$columnname] < $b["Product"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["Product"][$columnname] > $b["Product"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        => $Data
		);
	echo json_encode($json_data);
	exit;

}
public function BonusApprovalReport()
	{
	$brands=$this->Brand->find('list');
	$producttypes=$this->ProductType->find('list');
$routes=$this->Route->find('list',array('fields'=>array('id','name')));
			$this->set(compact('routes'));	
			$this->set(compact('brands','producttypes'));
		$customertypes=$this->CustomerType->find('list',array('fields'=>array('id','name')));
		$this->set('from',date("d-m-Y", strtotime('first day of this month')));
		$this->set('to',date("d-m-Y", strtotime('last day of this month')));
		$executives['']='All';
		$executives['0']='No Executive';
		$executives+=$this->Executive->find('list',array('conditions' => 'block!=1','fields'=>array('id','name')));
		$this->set(compact('executives'));
		$this->set(compact('customertypes'));
	}
	public function Bonus_approval_report_ajax(){
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='AccountHead.name';
	$columns[]='Sale.invoice_no';
	$columns[]='Product.name';
	$columns[]='SaleItem.quantity';
	$columns[]='SaleItem.unit_price';
	$columns[]='SaleItem.actual_price';
	$columns[]='bonus';
	$columns[]='status';
	$conditions=[];
	if(isset($requestData['product_type_id']))
	{
		if($requestData['product_type_id'])
			$conditions['Product.product_type_id']=$requestData['product_type_id'];	
	}
	if(isset($requestData['brand_id']))
	{
		if($requestData['brand_id'])
			$conditions['Product.brand_id']=$requestData['brand_id'];	
	}
	if(isset($requestData['product_id']))
	{
		if($requestData['product_id'])
			$conditions['Product.id']=$requestData['product_id'];	
	}
	if(!empty($requestData['executive_id']) || $requestData['executive_id']=='0')
	{
			$conditions['Sale.executive_id']=$requestData['executive_id'];	
	}
	if(isset($requestData['customer_id']))
	{
		if($requestData['customer_id'])
			{
			$conditions['Sale.account_head_id']=$requestData['customer_id'];
			}		
	}
	if(isset($requestData['customer_type_id']))
	{
		if($requestData['customer_type_id'])
			{
			$conditions['Customer.customer_type_id']=$requestData['customer_type_id'];	
			}	
	}
	if(isset($requestData['route_id']))
	{
		if($requestData['route_id'])
		{
			$conditions['Customer.route_id']=$requestData['route_id'];
    	}		
	}
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions['Sale.date_of_delivered between ? and ?']=[$from_date,$to_date];
    $conditions['Sale.flag']=1;
    $conditions['Sale.status']=2;
    $conditions['SaleItem.is_return']=[0,2];
	$totalData=$this->SaleItem->find('count',array(
		"joins"=>array(
			array(
				"table"=>'account_heads',
				"alias"=>'AccountHead',
				"type"=>'INNER',
				"conditions"=>array('AccountHead.id=Sale.account_head_id'),
				),
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'INNER',
				"conditions"=>array('Customer.account_head_id=Sale.account_head_id'),
				),
			),
		'conditions'=>$conditions,
		));
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'Product.name LIKE' =>'%'. $q . '%',
			'AccountHead.name LIKE' =>'%'. $q . '%',
			'Sale.invoice_no LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->SaleItem->find('count',array(
			"joins"=>array(
			array(
				"table"=>'account_heads',
				"alias"=>'AccountHead',
				"type"=>'INNER',
				"conditions"=>array('AccountHead.id=Sale.account_head_id'),
				),
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'INNER',
				"conditions"=>array('Customer.account_head_id=Sale.account_head_id'),
				),
			),
			'conditions'=>$conditions,
			));
	}
	//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column']<5)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
	$Data=$this->SaleItem->find('all',array(
		"joins"=>array(	
			array(
				"table"=>'account_heads',
				"alias"=>'AccountHead',
				"type"=>'INNER',
				"conditions"=>array('AccountHead.id=Sale.account_head_id'),
				),
			array(
				"table"=>'customers',
				"alias"=>'Customer',
				"type"=>'INNER',
				"conditions"=>array('Customer.account_head_id=Sale.account_head_id'),
				),
			),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $order,
		'fields'=>array(
			'SaleItem.*',
			'Product.name',
			'Sale.invoice_no',
			'AccountHead.name',
			)
		));
	foreach ($Data as $key => $value) {	
		$bonus_reduction=$value['SaleItem']['actual_price']-$value['SaleItem']['unit_price'];
		$Data[$key]['SaleItem']['status']="Rejected";
		if($value['SaleItem']['is_return']==2)
		{
			$Data[$key]['SaleItem']['status']="Approved";
		}
		$Data[$key]['SaleItem']['quantity']=floatval($value['SaleItem']['quantity']);
		$Data[$key]['SaleItem']['unit_price']=number_format($value['SaleItem']['unit_price'],2,'.','');
		$Data[$key]['SaleItem']['actual_price']=number_format($value['SaleItem']['actual_price'],2,'.','');
		$Data[$key]['SaleItem']['bonus']=number_format($bonus_reduction*$value['SaleItem']['quantity'],2,'.','');
	}
	// //sorting total/balance/recieved
		if($requestData['order'][0]['column'] > 5) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["SaleItem"][$columnname] == $b["SaleItem"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["SaleItem"][$columnname] < $b["SaleItem"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["SaleItem"][$columnname] > $b["SaleItem"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        => $Data
		);
	echo json_encode($json_data);
	exit;

}
public function print_profit_loss_report($from_date,$to_date) 
	{
		$this->response->download("reports_profit_loss.csv");
		$conditions=array();
		$from=date("Y-m-d", strtotime($from_date));
		$to  =date("Y-m-d", strtotime($to_date));
		$list_array=array();
		$list_array=$this->profit_loss_calculator($from,$to);
		$accounts_array=array();
		$data=[];
		$single=[
			'Opening Stock',
			'',
			'',
			$list_array['Stock']['open'],
			'Net Sales',
			'',
			'',
			//$list_array['Sale']['SaleValue']-$list_array['Sale']['SalesReturn']
			abs(number_format(($list_array['Sale']['SaleValue']+($list_array['Sale']['SalesReturn']+$list_array['Sale']['CreditNote'])),2,'.',''))
		];
		$data[]=$single;
		$single=[
			'',
			'',
			'',
			'',
			'Sale',
			'',
			//$list_array['Sale']['SaleValue'],
			abs(number_format($list_array['Sale']['SaleValue'],2,'.','')),
			'',
		];
		$data[]=$single;
		$single=[
			'',
			'',
			'',
			'',
			'(-)Sales Return',
			'',
			number_format($list_array['Sale']['SalesReturn'],2,'.',''),
			'',
		];
		$data[]=$single;
		// $single=[
		// 	'',
		// 	'',
		// 	'',
		// 	'',
		// 	'(-)Credit Note',
		// 	'',
		// 	number_format($list_array['Sale']['CreditNote'],2,'.',''),
		// 	'',
		// ];
		// $data[]=$single;
		$single=[
			'Net Purchase',
			'',
			'',
			number_format($list_array['Purchase']['PurchaseValue']-$list_array['Purchase']['PurchaseReturn']-$list_array['Purchase']['DebitNote'],2,'.',''),
			'Closing Stock',
			'',
			'',
			$list_array['Stock']['close']
		];
		$data[]=$single;
		$single=[
			'Purchase',
			'',
			number_format($list_array['Purchase']['PurchaseValue'],2,'.',''),
			'',
			'',
			'',
			'',
			'',
		];
		$data[]=$single;
		$single=[
			'(-)Purchase Return',
			'',
			number_format($list_array['Purchase']['PurchaseReturn'],2,'.',''),
			'',
			'',
			'',
			'',
			'',
		];
		$data[]=$single;
		// $single=[
		// 	'(-)Debit Note',
		// 	'',
		// 	number_format($list_array['Purchase']['DebitNote'],2,'.',''),
		// 	'',
		// 	'',
		// 	'',
		// 	'',
		// 	'',
		// ];
		// $data[]=$single;
		$single=[
			'Direct Expense',
			'',
			'',
			$list_array['Expense']['DirectExpense']['amount'],
			'Direct Income',
			'',
			'',
			$list_array['Income']['DirectIncome']['amount']
		];
		$data[]=$single;
		if($list_array['Income']['DirectIncome']['single']) {
			foreach ($list_array['Income']['DirectIncome']['single'] as $key => $value) {
				if($value['Total']){
					$single=[
						'',
						'',
						'',
						'',
						$key,
						'',
						$value['Total'],
						'',
						''
					];
					$data[]=$single;
					$SubGroup=$this->AccSubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllByAccSubGroupId($SubGroup['AccSubGroup']['id']);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						$ClosingBalance=$function_value_return['credit']-$function_value_return['debit'];
						if($ClosingBalance){
							$single=[
								'',
								'',
								'',
								'',
								'   '.$AccountHeadValue['AccountHead']['name'],
								$ClosingBalance,
								'',
								''
							];
							$data[]=$single;
						}
					}
				}
			}
		}
		if($list_array['Expense']['DirectExpense']['single']) {
			foreach ($list_array['Expense']['DirectExpense']['single'] as $key => $value) {
				if($value['total']){
					$single=[
						$key,
						'',
						$value['total'],
						'',
						'',
						'',
						'',
						''
					];
					$data[]=$single;
					$SubGroup=$this->AccSubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllByAccSubGroupId($SubGroup['AccSubGroup']['id']);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						$ClosingBalance=$function_value_return['debit']-$function_value_return['credit'];
						if($ClosingBalance){
							$single=[
								'   '.$AccountHeadValue['AccountHead']['name'],
								$ClosingBalance,
								'',
								'',
								'',
								'',
								'',
								''
							];
							$data[]=$single;
						}
					}
				}
			}
		}
		$single=[
			'Gross Profit c/d',
			'',
			'',
			$list_array['First']['Left'],
			'Gross Loss c/d',
			'',
			'',
			$list_array['First']['Right']
		];
		$data[]=$single;
		$single=[
			'Total',
			'',
			'',
			$list_array['FirstTotal']['Left'],
			'Total',
			'',
			'',
			$list_array['FirstTotal']['Right']
		];
		$data[]=$single;
		$single=[
			'Gross Loss b/d',
			'',
			'',
			$list_array['Gross']['Left'],
			'Gross Profit b/d',
			'',
			'',
			$list_array['Gross']['Right']
		];
		$data[]=$single;
		$single=[
			'Indirect Expense',
			'',
			'',
			$list_array['Expense']['IndirectExpense']['amount'],
			'Indirect Income',
			'',
			'',
			abs(number_format($list_array['Income']['IndirectIncome']['amount'],2,'.',''))
		];
		$data[]=$single;
		if($list_array['Income']['IndirectIncome']['single']) {
			foreach ($list_array['Income']['IndirectIncome']['single'] as $key => $value) {
				if($value['Total']){
					$single=[
						'',
						'',
						'',
						'',
						$key,
						'',
						abs(number_format($value['Total'],2,'.','')),
						''
					];
					$data[]=$single;
					$SubGroup=$this->SubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllBySubGroupId($SubGroup['SubGroup']['id']);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						$ClosingBalance=$function_value_return['credit']-$function_value_return['debit'];
						if($ClosingBalance){
							$single=[
								'',
								'',
								'',
								'',
								'   '.$AccountHeadValue['AccountHead']['name'],
								$ClosingBalance,
								'',
								''
							];
							$data[]=$single;
						}
					}
				}
			}
		}
		if($list_array['Expense']['IndirectExpense']['single']) {
			foreach ($list_array['Expense']['IndirectExpense']['single'] as $key => $value) {
				if($value['total']){
					$single=[
						$key,
						'',
						$value['total'],
						'',
						'',
						'',
						'',
						''
					];
					$data[]=$single;
					$SubGroup=$this->AccSubGroup->findByName($key);
					$AccountHeads=$this->AccountHead->findAllByAccSubGroupId($SubGroup['AccSubGroup']['id']);
					foreach ($AccountHeads as $AccountHeadkey => $AccountHeadValue) {
						$function_value_return=$this->General_Journal_Debit_N_Credit_function1($AccountHeadValue['AccountHead']['id'],$from,$to);
						$ClosingBalance=$function_value_return['debit']-$function_value_return['credit'];
						if($ClosingBalance){
							$single=[
								'   '.$AccountHeadValue['AccountHead']['name'],
								$ClosingBalance,
								'',
								'',
								'',
								'',
								'',
								''
							];
							$data[]=$single;
						}
					}
				}
			}
		}
		$single=[
			'Net Profit c/d',
			'',
			'',
			$list_array['Net']['Left'],
			'Net Loss c/d',
			'',
			'',
			$list_array['Net']['Right']
		];
		$data[]=$single;
		$single=[
			'Total',
			'',
			'',
			$list_array['SecondTotal']['Left'],
			'Total',
			'',
			'',
			$list_array['SecondTotal']['Right']
		];
		$data[]=$single;
		foreach($data as $key1 => $value1) { 
			array_push($accounts_array,$value1);
		}
		$this->set(compact('accounts_array','from','to'));
		$this->layout = 'ajax';
		return false;
		exit;
	}
	public function TaxReportSale()
	{
	$this->set('from_date',date("d-m-Y"));
	$this->set('to_date',date("d-m-Y"));
	}
	public function TaxReport_sale()
	{
	$system_state_id=$this->Global_Var_Profile['Profile']['state_id'];
		$requestData=$this->request->data;
		$columns=[];
		$columns[]='Sale.invoice_no';
		$columns[]='Sale.date_of_delivered';
		$columns[]='CustomerType.name';
		$columns[]='AccountHead.name';
		$columns[]='Customer.place';
		$columns[]='Customer.vat_no';
		$columns[]='Sale.total';
		$columns[]='Sale.taxable_amount';
		$columns[]='cgst';
		$columns[]='sgst';
		$columns[]='igst';
		$columns[]='Sale.discount_amount';
		$columns[]='Sale.grandtotal';
		$conditions=[];
			$conditions['Sale.date_of_delivered between ? and ?']=[
			date('Y-m-d',strtotime($requestData['from_date'])),
			date('Y-m-d',strtotime($requestData['to_date']))
			];
	    $this->Sale->unbindModel(array('belongsTo'=>array('Executive'),'hasMany' => array('SaleItem')));
		$totalData=$this->Sale->find('count',[
				'joins'=>array(
				array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.account_head_id=Sale.account_head_id')
				),
				array(
				'table'=>'customer_types',
				'alias'=>'CustomerType',
				'type'=>'LEFT',
				'conditions'=>array('CustomerType.id=Customer.customer_type_id')
				),
				),
		'conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'Sale.date_of_delivered LIKE' =>'%'. $q . '%',
    		'Sale.invoice_no LIKE' =>'%'. $q . '%',
    		'Sale.grand_total LIKE' =>'%'. $q . '%',
    		'AccountHead.name LIKE' =>'%'. $q . '%',
    		'CustomerType.name LIKE' =>'%'. $q . '%',
    		);
       $this->Sale->unbindModel(array('belongsTo'=>array('Executive'),'hasMany' => array('SaleItem')));
    	$totalFiltered=$this->Sale->find('count',[
				'joins'=>array(
				array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.account_head_id=Sale.account_head_id')
				),
				array(
				'table'=>'customer_types',
				'alias'=>'CustomerType',
				'type'=>'LEFT',
				'conditions'=>array('CustomerType.id=Customer.customer_type_id')
				),
				),
    		'conditions'=>$conditions,
    		]);
    }
    //order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column'] !=8 || $requestData['order'][0]['column'] !=9 || $requestData['order'][0]['column'] !=10)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
    $this->Sale->unbindModel(array('belongsTo'=>array('Executive'),'hasMany' => array('SaleItem')));
    $Data=$this->Sale->find('all',array(
			'joins'=>array(
			array(
			'table'=>'customers',
			'alias'=>'Customer',
			'type'=>'INNER',
			'conditions'=>array('Customer.account_head_id=Sale.account_head_id')
			),
			array(
			'table'=>'customer_types',
			'alias'=>'CustomerType',
			'type'=>'LEFT',
			'conditions'=>array('CustomerType.id=Customer.customer_type_id')
			),
			),
    	'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	'order'=>$order,
    	'fields'=>array(
    		'Sale.*',
    		'Customer.account_head_id','Customer.vat_no','Customer.state_id','Customer.place',
    		'AccountHead.name',
    		'CustomerType.name',
    		'AccountHead.id',
    		)
    	));
    foreach ($Data as $key => $value) {
    	$Data[$key]['Sale']['vat_no']=$value['Customer']['vat_no']?$value['Customer']['vat_no']:'';
    	$Data[$key]['Sale']['customer_type']=$value['CustomerType']['name']?$value['CustomerType']['name']:'';
	    $Data[$key]['Sale']['place']=$value['Customer']['place']?$value['Customer']['place']:'';
    	$Data[$key]['Sale']['date_of_delivered']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
    	$Data[$key]['Sale']['net_value']=number_format($value['Sale']['total'],2,'.','');
    	$Data[$key]['Sale']['taxable_total']=number_format($value['Sale']['taxable_amount'],2,'.','');
    	$Data[$key]['Sale']['grand_total']=number_format($value['Sale']['grand_total'],2,'.','');
    	$Data[$key]['Sale']['discount_amount']=number_format($value['Sale']['discount_amount'],2,'.','');
    	if($system_state_id==$value['Customer']['state_id'])
				{
					$Data[$key]['Sale']['sgst']=number_format(($value['Sale']['tax_amount']/2),2,'.','');
					$Data[$key]['Sale']['cgst']=number_format(($value['Sale']['tax_amount']/2),2,'.','');
					$Data[$key]['Sale']['igst']=0;
				}
				else
				{
					$Data[$key]['Sale']['sgst']=0;
					$Data[$key]['Sale']['cgst']=0;
					$Data[$key]['Sale']['igst']=number_format($value['Sale']['tax_amount'],2,'.','');
				}
    }
    // //sorting total/balance/recieved
		if( $requestData['order'][0]['column'] ==8 || $requestData['order'][0]['column']==10 || $requestData['order'][0]['column']==9) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["Sale"][$columnname] == $b["Sale"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["Sale"][$columnname] < $b["Sale"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["Sale"][$columnname] > $b["Sale"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;

}
public function TaxReport_saleReturn()
	{
			$system_state_id=$this->Global_Var_Profile['Profile']['state_id'];
		$requestData=$this->request->data;
		$columns=[];
		$columns[]='SalesReturn.invoice_no';
		$columns[]='SalesReturn.date';
		$columns[]='CustomerType.name';
		$columns[]='AccountHead.name';
		$columns[]='Customer.place';
		$columns[]='Customer.vat_no';
		$columns[]='net_value';
		$columns[]='taxable_total';
		$columns[]='cgst';
		$columns[]='sgst';
		$columns[]='igst';
		$columns[]='grand_total';
		$conditions=[];
		   $conditions['SalesReturn.grand_total !=']=0;
			$conditions['SalesReturn.date between ? and ?']=[
			date('Y-m-d',strtotime($requestData['from_date'])),
			date('Y-m-d',strtotime($requestData['to_date']))
			];
		 //order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column'] < 6)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
		$totalData=$this->SalesReturn->find('count',[
			'joins'=>array(
				array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.account_head_id=SalesReturn.account_head_id')
				),
				array(
				'table'=>'customer_types',
				'alias'=>'CustomerType',
				'type'=>'LEFT',
				'conditions'=>array('CustomerType.id=Customer.customer_type_id')
				),),
			'conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
    	$q=$requestData['search']['value'];
    	$conditions['OR']=array(
    		'SalesReturn.date LIKE' =>'%'. $q . '%',
    		'SalesReturn.invoice_no LIKE' =>'%'. $q . '%',
    		'SalesReturn.grand_total LIKE' =>'%'. $q . '%',
    		'AccountHead.name LIKE' =>'%'. $q . '%',
    		 'CustomerType.name LIKE' =>'%'. $q . '%',
    		);
    	$totalFiltered=$this->SalesReturn->find('count',[
    		'joins'=>array(
				array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.account_head_id=SalesReturn.account_head_id')
				),
				array(
				'table'=>'customer_types',
				'alias'=>'CustomerType',
				'type'=>'LEFT',
				'conditions'=>array('CustomerType.id=Customer.customer_type_id')
				),),
    		'conditions'=>$conditions,
    		]);
    }
   $this->SalesReturn->unbindModel(array('hasMany' => array('SalesReturnItem')));
    $Data=$this->SalesReturn->find('all',array(
    	'joins'=>array(
				array(
				'table'=>'customers',
				'alias'=>'Customer',
				'type'=>'INNER',
				'conditions'=>array('Customer.account_head_id=SalesReturn.account_head_id')
				),
				array(
				'table'=>'customer_types',
				'alias'=>'CustomerType',
				'type'=>'LEFT',
				'conditions'=>array('CustomerType.id=Customer.customer_type_id')
				),),
    	'conditions'=>$conditions,
    	'offset'=>$requestData['start'],
    	'limit'=>$requestData['length'],
    	'order'=>$order,
    	'fields'=>array(
    		'SalesReturn.*',
    		'Customer.account_head_id','Customer.vat_no','Customer.state_id','Customer.place',
    		'AccountHead.name',
    		'CustomerType.name',
    		'AccountHead.id',
    		)
    	));
    foreach ($Data as $key => $value) {
    	$Data[$key]['SalesReturn']['customer_type']=$value['CustomerType']['name']?$value['CustomerType']['name']:'';
    	$Data[$key]['SalesReturn']['vat_no']=$value['Customer']['vat_no']?$value['Customer']['vat_no']:'';
	    $Data[$key]['SalesReturn']['place']=$value['Customer']['place']?$value['Customer']['place']:'';
    	$Data[$key]['SalesReturn']['date_of_delivered']=date('d-m-Y',strtotime($value['SalesReturn']['date']));
    	$conditionsItem['SalesReturn.id']=$value['SalesReturn']['id'];
					$this->SalesReturnItem->virtualFields = array( 
				     'total_net_value' => "SUM(SalesReturnItem.unit_price*SalesReturnItem.quantity)",
						'total_tax_amount' => "SUM(SalesReturnItem.tax_amount)",
						'total_taxable_amount' => "SUM(SalesReturnItem.net_value)",
						);
					$SalesReturnItem=$this->SalesReturnItem->find('first',[
						'conditions'=>$conditionsItem,
						'fields'=>array(
							'SalesReturnItem.total_tax_amount',
							'SalesReturnItem.total_net_value',
							'SalesReturnItem.total_taxable_amount'
							),
						]);
    	$Data[$key]['SalesReturn']['net_value']=number_format($SalesReturnItem['SalesReturnItem']['total_net_value'],2,'.','');
    	$Data[$key]['SalesReturn']['taxable_total']=number_format($SalesReturnItem['SalesReturnItem']['total_taxable_amount'],2,'.','');
    	$Data[$key]['SalesReturn']['grand_total']=number_format($value['SalesReturn']['grand_total'],2,'.','');
    	if($system_state_id==$value['Customer']['state_id'])
				{
					$Data[$key]['SalesReturn']['sgst']=number_format(($SalesReturnItem['SalesReturnItem']['total_tax_amount']/2),2,'.','');
					$Data[$key]['SalesReturn']['cgst']=number_format(($SalesReturnItem['SalesReturnItem']['total_tax_amount']/2),2,'.','');
					$Data[$key]['SalesReturn']['igst']=0;
				}
				else
				{
					$Data[$key]['SalesReturn']['sgst']=0;
					$Data[$key]['SalesReturn']['cgst']=0;
					$Data[$key]['SalesReturn']['igst']=number_format($SalesReturnItem['SalesReturnItem']['total_tax_amount'],2,'.','');
				}
    }
    // //sorting total/balance/recieved
		if( $requestData['order'][0]['column'] >5) {

			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["SalesReturn"][$columnname] == $b["SalesReturn"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["SalesReturn"][$columnname] < $b["SalesReturn"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["SalesReturn"][$columnname] > $b["SalesReturn"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
$json_data = array(
	"draw"           =>intval($requestData['draw']),
	"recordsTotal"   =>intval($totalData),
	"recordsFiltered"=>intval($totalFiltered),
	"records"        =>$Data
	);
echo json_encode($json_data); exit;

}
public function print_trial_balance_report($from_date,$to_date) 
	{
		$this->response->download("report_trial_balance.csv");
		$conditions=array();
		$from_date=date("Y-m-d", strtotime($from_date));
		$to_date=date("Y-m-d", strtotime($to_date));
		$list_array=array();
		$list_array=$this->trial_balance_report($from_date,$to_date);
		$this->set('from', $from_date);
		$this->set('to', $to_date);
		$this->set('trial_balance', $list_array);
		$this->layout = 'ajax';
		return false;
		exit;
	}
	public function trial_balance_report($from_date,$to_date)
	{
		$Data=$this->AccMainGroup->find('all',array(
			'fields'=>array(
				'AccMainGroup.*',
				)
			));
		$maingroup=[];
		foreach ($Data as $key => $value) {
			$type_id=$value['AccMainGroup']['id'];
			$single_main['opening_balance']=0;
			$single_main['amount']=0;
			$single_main['current']=0;
			$single_main['debit']=0;
			$single_main['credit']=0;
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_trial_balance($value['AccMainGroup']['id'],$from_date,$to_date);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function_trial_balance($value['AccMainGroup']['id'],$from_date,$to_date);
			if(in_array($type_id, ['1','3']))
	     	{
					$single_main['opening_balance']+=$opening_function_value_return['opening_balance'];
					if($type_id=='1')
					{
						$single_main['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single_main['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single_main['current']+=number_format($function_value_return['debit'],2,'.','');
						$single_main['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					if($type_id=='3')
					{
						$single_main['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single_main['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single_main['current']+=number_format($function_value_return['debit'],2,'.','');
						$single_main['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					$single_main['amount']+=$single_main['opening_balance'];
					$single_main['amount']+=$single_main['current'];
		    }
		    if($type_id=='5')
			{					
					$single_main['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single_main['opening_balance']+=$opening_function_value_return['debit'];
					$single_main['opening_balance']-=$opening_function_value_return['credit'];
					$single_main['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$single_main['amount']+=$single_main['opening_balance'];
					$single_main['amount']+=$single_main['current'];
			}
			if($type_id=='2')
		     {
				    $single_main['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single_main['opening_balance']+=$opening_function_value_return['credit'];
					$single_main['opening_balance']-=$opening_function_value_return['debit'];
					$single_main['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$single_main['amount']+=$single_main['opening_balance'];
				    $single_main['amount']+=$single_main['current'];
		   }
		   if($type_id=='4')
		   {
					$single_main['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single_main['opening_balance']+=$opening_function_value_return['credit'];
					$single_main['opening_balance']-=$opening_function_value_return['debit'];
					$single_main['current']+=$function_value_return['debit']-$function_value_return['credit'];			
					$single_main['amount']+=$single_main['opening_balance'];
					$single_main['amount']+=$single_main['current'];
		   }
				$single_main['opening_balance']=number_format($single_main['opening_balance'],2,'.','');
				$single_main['current']=number_format($single_main['current'],2,'.','');
				$single_main['amount']=number_format($single_main['amount'],2,'.','');
				$single_main['credit']+=$function_value_return['credit'];
				$single_main['debit']+=$function_value_return['debit'];
				$single_main['credit']=number_format($single_main['credit'],2,'.','');
				$single_main['debit']=number_format($single_main['debit'],2,'.','');
			    $single_main['name']=$value['AccMainGroup']['name'];
		$conditions_subgroup=[];
	$conditions_subgroup['AccSubGroup.main_group_id']=$value['AccMainGroup']['id'];
   $conditions_subgroup['AccSubGroup.id !=']=14;
	$Data_subgroup=$this->AccSubGroup->find('all',array(
			'conditions'=>$conditions_subgroup,
			'fields'=>array(
				'AccSubGroup.*',
				)
			));
	$Subgroup=[];
		foreach ($Data_subgroup as $key1 => $value1) 
		{
			$single['sub_group_id']=$value1['AccSubGroup']['id'];
			$single['name']=$value1['AccSubGroup']['name'];
			$single['opening_balance']=0;
			$single['amount']=0;
			$single['current']=0;
			$single['debit']=0;
			$single['credit']=0;
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_trial_balance_subgroup($value1['AccSubGroup']['id'],$from_date,$to_date);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function_trial_balance_subgroup($value1['AccSubGroup']['id'],$from_date,$to_date);
		    		if(in_array($type_id, ['1','3']))
	     	{
					$single['opening_balance']+=$opening_function_value_return['opening_balance'];
					if($type_id=='1')
					{
						$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single['current']+=number_format($function_value_return['debit'],2,'.','');
						$single['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					if($type_id=='3')
					{
						$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single['current']+=number_format($function_value_return['debit'],2,'.','');
						$single['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					$single['amount']+=$single['opening_balance'];
					$single['amount']+=$single['current'];
		    }
		    if($type_id=='5')
			{					
					$single['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single['opening_balance']+=$opening_function_value_return['debit'];
					$single['opening_balance']-=$opening_function_value_return['credit'];
					$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$single['amount']+=$single['opening_balance'];
					$single['amount']+=$single['current'];
			}
			if($type_id=='2')
		     {
				    $single['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single['opening_balance']+=$opening_function_value_return['credit'];
					$single['opening_balance']-=$opening_function_value_return['debit'];
					$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$single['amount']+=$single['opening_balance'];
				    $single['amount']+=$single['current'];
		   }
		   if($type_id=='4')
		   {
					$single['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single['opening_balance']+=$opening_function_value_return['credit'];
					$single['opening_balance']-=$opening_function_value_return['debit'];
					$single['current']+=$function_value_return['debit']-$function_value_return['credit'];			
					$single['amount']+=$single['opening_balance'];
					$single['amount']+=$single['current'];
		   }
				$single['opening_balance']=number_format($single['opening_balance'],2,'.','');
				$single['current']=number_format($single['current'],2,'.','');
				$single['amount']=number_format($single['amount'],2,'.','');
				$single['credit']+=$function_value_return['credit'];
				$single['debit']+=$function_value_return['debit'];
				$single['credit']=number_format($single['credit'],2,'.','');
				$single['debit']=number_format($single['debit'],2,'.','');
			    $single['name']=$value1['AccSubGroup']['name'];
			    $Subgroup[]=$single;
		}
		$single_main['Subgroup']=$Subgroup;
			  $maingroup[]=$single_main;
	}
		return $maingroup;
	}
public function BalanceSheetNew()
	{
		$user_branch_id=$this->Session->read('User.branch_id');
		if(!$this->request->data)
		{
			$date=date('d-m-Y');
			//$from_date=date('d-m-Y',strtotime('-1 month'));
			$from_date=date('01-01-2021');
			$to_date=date('d-m-Y');
			$data['Reports']['from_date']=$from_date;
			$data['Reports']['to_date']=$to_date;
			$data['from_date']=$from_date;
			$data['to_date']=$to_date;
			if(isset($user_branch_id))
			{
			$BranchList = $this->Branch->findById($user_branch_id);
			}
			else
			{
			$BranchList=$this->Branch->find('list',array('conditions' =>  array(),'fields'=>array('id','name')));
			}
			$this->set(compact('BranchList'));
			$this->request->data=$data;
		}
	}
	public function balance_sheet_left_ajax()
	{
        $requestData=$this->request->data;
		$columns = [];
		$columns[]='AccMainGroup.name';
		$columns[]='AccMainGroup.id';
		$columns[]='AccMainGroup.id';
		$columns[]='AccMainGroup.id';
		$columns[]='AccMainGroup.id';
		$conditions=[];
		$conditions['AccMainGroup.id']=array(2,5);
		$from_date=$requestData['from_date'];
		$to_date=$requestData['to_date'];
		//$branch_id=$requestData['branch_id'];

		$totalData=$this->AccMainGroup->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		$Data=$this->AccMainGroup->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'AccMainGroup.*',
				)
			));
		$ProfitLossController = new ProfitLossController;
		$ProfitLoss=$ProfitLossController->profit_loss_calculator($from_date,$to_date);
		$profit_sub_group_id=$this->SystemParameter->field('value',array('id'=>14));
		$profit_loss_account_head_id=$this->SystemParameter->field('value',array('id'=>15));
		foreach ($Data as $key => $value) {
			$type_id=$value['AccMainGroup']['id'];
			$Data[$key]['AccMainGroup']['opening_balance']=0;
			$Data[$key]['AccMainGroup']['amount']=0;
			$Data[$key]['AccMainGroup']['current']=0;
			$Data[$key]['AccMainGroup']['debit']=0;
			$Data[$key]['AccMainGroup']['credit']=0;
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date);
			if($type_id=='5')
			{					
				$Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['opening_balance'];
				$Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['debit'];
				$Data[$key]['AccMainGroup']['opening_balance']-=$opening_function_value_return['credit'];
				$Data[$key]['AccMainGroup']['current']+=$function_value_return['debit']-$function_value_return['credit'];
				$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['opening_balance'];
				$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['current'];
				$Data[$key]['AccMainGroup']['credit']+=$function_value_return['credit'];
				$Data[$key]['AccMainGroup']['debit']+=$function_value_return['debit'];
			}
			if($type_id=='2')
		    {
				$Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['opening_balance'];
				$Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['debit'];
				$Data[$key]['AccMainGroup']['opening_balance']-=$opening_function_value_return['credit'];
				$Data[$key]['AccMainGroup']['current']+=($function_value_return['debit']+$ProfitLoss['Net']['Right'])-($function_value_return['credit']+$ProfitLoss['Net']['Left']);
				$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['opening_balance'];
				$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['current'];
				$Data[$key]['AccMainGroup']['credit']+=$function_value_return['credit']+$ProfitLoss['Net']['Left'];
				$Data[$key]['AccMainGroup']['debit']+=$function_value_return['debit']+$ProfitLoss['Net']['Right'];
		   	}
		   
			$opening_balance=number_format($Data[$key]['AccMainGroup']['opening_balance'],2,'.','');
			// if($opening_balance>0) $Data[$key]['AccMainGroup']['opening_balance']=$opening_balance.' Dr';
			// elseif($opening_balance<0) $Data[$key]['AccMainGroup']['opening_balance']=abs($opening_balance).' Cr';
			// else $Data[$key]['AccMainGroup']['opening_balance']=$opening_balance;
			$Data[$key]['AccMainGroup']['opening_balance']=$opening_balance;
			$Data[$key]['AccMainGroup']['current']=number_format($Data[$key]['AccMainGroup']['current'],2,'.','');
			$amount=number_format($Data[$key]['AccMainGroup']['amount'],2,'.','');
			if($amount>0) $Data[$key]['AccMainGroup']['amount']=number_format($amount,2,'.','').' Dr';
			elseif($amount<0) $Data[$key]['AccMainGroup']['amount']=number_format(abs($amount),2,'.','').' Cr';
			else $Data[$key]['AccMainGroup']['amount']=$amount;
			
			$Data[$key]['AccMainGroup']['credit']=number_format($Data[$key]['AccMainGroup']['credit'],2,'.','');
			$Data[$key]['AccMainGroup']['debit']=number_format($Data[$key]['AccMainGroup']['debit'],2,'.','');
			$Data[$key]['AccMainGroup']['name']='<span hidden class="type_name">'.$value['AccMainGroup']['name'].'</span><span hidden class="type_id">'.$value['AccMainGroup']['id'].'</span>'.$value['AccMainGroup']['name'];
		}
		
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	
	}
	public function balance_sheet_right_ajax()
	{
        $requestData=$this->request->data;
		$columns = [];
		$columns[]='AccMainGroup.name';
		$columns[]='AccMainGroup.id';
		$columns[]='AccMainGroup.id';
		$columns[]='AccMainGroup.id';
		$columns[]='AccMainGroup.id';
		$conditions=[];
		$conditions['AccMainGroup.id']=1;
		$from_date=$requestData['from_date'];
		$to_date=$requestData['to_date'];
		$totalData=$this->AccMainGroup->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		$Data=$this->AccMainGroup->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'AccMainGroup.*',
				)
			));
		//pr($Data);
		foreach ($Data as $key => $value) {
			$type_id=$value['AccMainGroup']['id'];
			$Data[$key]['AccMainGroup']['opening_balance']=0;
			$Data[$key]['AccMainGroup']['amount']=0;
			$Data[$key]['AccMainGroup']['current']=0;
			$Data[$key]['AccMainGroup']['debit']=0;
			$Data[$key]['AccMainGroup']['credit']=0;
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date);
			
			$Data[$key]['AccMainGroup']['opening_balance']+=$opening_function_value_return['opening_balance'];
			
			$Data[$key]['AccMainGroup']['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
			$Data[$key]['AccMainGroup']['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
			$Data[$key]['AccMainGroup']['current']+=number_format($function_value_return['debit'],2,'.','');
			$Data[$key]['AccMainGroup']['current']-=number_format($function_value_return['credit'],2,'.','');
			$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['opening_balance'];
			$Data[$key]['AccMainGroup']['amount']+=$Data[$key]['AccMainGroup']['current'];
		    
		    
			$opening_balance=number_format($Data[$key]['AccMainGroup']['opening_balance'],2,'.','');
			// if($opening_balance>0) $Data[$key]['AccMainGroup']['opening_balance']=$opening_balance.' Dr';
			// elseif($opening_balance<0) $Data[$key]['AccMainGroup']['opening_balance']=abs($opening_balance).' Cr';
			// else $Data[$key]['AccMainGroup']['opening_balance']=$opening_balance;
			$Data[$key]['AccMainGroup']['opening_balance']=$opening_balance;
			$Data[$key]['AccMainGroup']['current']=number_format($Data[$key]['AccMainGroup']['current'],2,'.','');
			$amount=number_format($Data[$key]['AccMainGroup']['amount'],2,'.','');
			if($amount>0) $Data[$key]['AccMainGroup']['amount']=$amount.' Dr';
			elseif($amount<0) $Data[$key]['AccMainGroup']['amount']=abs($amount).' Cr';
			else $Data[$key]['AccMainGroup']['amount']=$amount;
			$Data[$key]['AccMainGroup']['credit']+=$function_value_return['credit'];
			$Data[$key]['AccMainGroup']['debit']+=$function_value_return['debit'];
			$Data[$key]['AccMainGroup']['credit']=number_format($Data[$key]['AccMainGroup']['credit'],2,'.','');
			$Data[$key]['AccMainGroup']['debit']=number_format($Data[$key]['AccMainGroup']['debit'],2,'.','');
			$Data[$key]['AccMainGroup']['name']='<span hidden class="type_name">'.$value['AccMainGroup']['name'].'</span><span hidden class="type_id">'.$value['AccMainGroup']['id'].'</span>'.$value['AccMainGroup']['name'];
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	
	}
	public function General_Journal_Openging_Debit_N_Credit_function_balance_sheet($type_id,$from_date,$to_date)
	{	
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
      $opening_conditions=[];$conditions=[];
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		//if($branch_id) $conditions['Journal.branch_id']=$branch_id;
		$conditions['Journal.flag']=1;
		$Journal_Debit=$this->Journal->find('first',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccountHeadDebit.acc_sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
                ),),
			'conditions'=>array(
				'AccMainGroup.id'=>$type_id,
				'credit !='=>19,
				$conditions,
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$Journal_Credit=$this->Journal->find('first',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccountHeadCredit.acc_sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
                ),),
			'conditions'=>array(
			'AccMainGroup.id'=>$type_id,
				'debit !='=>19,
				$conditions,
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$this->AccountHead->virtualFields = array('total_opening_balance' => "SUM(AccountHead.opening_balance)");
		//if($branch_id) $opening_conditions['AccountHead.branch_id']=$branch_id;
		// $opening_conditions['AccountHead.id']!=19;
		$opening_balance=$this->AccountHead->find('first',array(
				"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccountHead.acc_sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
                ),),
                'conditions'=>array(
				'AccMainGroup.id'=>$type_id,
				//'AccountHead.acc_sub_group_id !='=>14,
				$opening_conditions,
				'AccountHead.created_at <='=>date('Y-m-d',strtotime($to_date)),
				),
			'fields'=>array( 'AccountHead.total_opening_balance'),
			));
		$account_single['opening_balance']=number_format($opening_balance['AccountHead']['total_opening_balance'],2,'.','');
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
	public function General_Journal_Debit_N_Credit_function_balance_sheet($type_id,$from_date,$to_date,$branch_id=null)
	{
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$Journal_Debit_conditions= [];
		$Journal_Debit_conditions['AccMainGroup.id'] = $type_id;
		if($branch_id) $Journal_Debit_conditions['Journal.branch_id']=$branch_id;
		$Journal_Debit_conditions['credit !='] =19;
		$Journal_Debit_conditions['flag'] = '1';
		$Journal_Debit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		$Journal_Credit_conditions= [];
		$Journal_Credit_conditions['AccMainGroup.id'] = $type_id;
		if($branch_id) $Journal_Credit_conditions['Journal.branch_id']=$branch_id;
		$Journal_Credit_conditions['debit !='] = 19;
		$Journal_Credit_conditions['flag'] = '1';
		$Journal_Credit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
			$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccountHeadDebit.acc_sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
                ),),
						'conditions'=>$Journal_Debit_conditions,
						'fields'=>array( 'Journal.total_amount'),
					));
		$Journal_Credit=$this->Journal->find('first',array(
			"joins"=>array(
                array(
                    "table"=>'acc_sub_groups',
                    "alias"=>'AccSubGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccountHeadCredit.acc_sub_group_id=AccSubGroup.id'),
                ),
                array(
                    "table"=>'acc_main_groups',
                    "alias"=>'AccMainGroup',
                    "type"=>'inner',
                    "conditions"=>array('AccSubGroup.main_group_id=AccMainGroup.id'),
                ),),
						'conditions'=>$Journal_Credit_conditions,
						'fields'=>array( 'Journal.total_amount'),
					));
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
	public function balance_sheet_subgroupwise()
	{
		$requestData=$this->request->data;
		$conditions=[];
		$return=[];
		$conditions['AccSubGroup.main_group_id']=$requestData['type_id'];
		//$conditions['AccSubGroup.id !=']=14;
		$return['status']="success";
		$type_id=$requestData['type_id'];
		$from_date=$requestData['from_date'];
		$to_date=$requestData['to_date'];
	  $Data=$this->AccSubGroup->find('all',array(
				'conditions'=>$conditions,
				'fields'=>array(
					'AccSubGroup.*',
				),
				'order'=>array('AccSubGroup.name DESC')
				));
	  $net_left=0;$net_right=0;
		$Subgroup=[];
		$ProfitLossController = new ProfitLossController;
		$ProfitLoss=$ProfitLossController->profit_loss_calculator($from_date,$to_date);
		$profit_sub_group_id=$this->SystemParameter->field('value',array('id'=>14));
		$profit_loss_account_head_id=$this->SystemParameter->field('value',array('id'=>15));
		foreach ($Data as $key => $value) 
		{
			$single['net_left']=0;
			$single['net_right']=0;
			$single['net_left']+=isset($ProfitLoss['Net']['Left'])?$ProfitLoss['Net']['Left']:0;
			$single['net_right']+=isset($ProfitLoss['Net']['Right'])?$ProfitLoss['Net']['Right']:0;
			$net_left+=$ProfitLoss['Net']['Left'];
			$net_right+=$ProfitLoss['Net']['Right'];
			$single['sub_group_id']=$value['AccSubGroup']['id'];
			$single['name']=$value['AccSubGroup']['name'];
			$single['opening_balance']=0;
			$single['amount']=0;
			$single['current']=0;
			$single['debit']=0;
			$single['credit']=0;
			
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_balance_sheet_subgroup($value['AccSubGroup']['id'],$from_date,$to_date);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function_balance_sheet_subgroup($value['AccSubGroup']['id'],$from_date,$to_date);
		    if(in_array($type_id, ['1','3']))
	     	{
					$single['opening_balance']+=$opening_function_value_return['opening_balance'];
					if($type_id=='1')
					{
						$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single['current']+=number_format($function_value_return['debit'],2,'.','');
						$single['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					if($type_id=='3')
					{
						$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single['current']+=number_format($function_value_return['debit'],2,'.','');
						$single['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					$single['amount']+=$single['opening_balance'];
					$single['amount']+=$single['current'];
		    }
		    if($type_id=='5')
			{					
					$single['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single['opening_balance']+=$opening_function_value_return['debit'];
					$single['opening_balance']-=$opening_function_value_return['credit'];
					$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$single['amount']+=$single['opening_balance'];
					$single['amount']+=$single['current'];
			}
			if($type_id=='2')
		    {
				    $single['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single['opening_balance']+=$opening_function_value_return['debit'];
					$single['opening_balance']-=$opening_function_value_return['credit'];
					$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$single['amount']+=$single['opening_balance'];
				    $single['amount']+=$single['current'];
		   	}
			if($type_id=='4')
			{
				$single['opening_balance']+=$opening_function_value_return['opening_balance'];
				$single['opening_balance']+=$opening_function_value_return['debit'];
					$single['opening_balance']-=$opening_function_value_return['credit'];
				$single['current']+=$function_value_return['debit']-$function_value_return['credit'];			
				$single['amount']+=$single['opening_balance'];
				$single['amount']+=$single['current'];
			}
			$single['opening_balance']=number_format($single['opening_balance'],2,'.','');
			$single['current']=number_format($single['current'],2,'.','');
			$single['amount']=number_format($single['amount'],2,'.','');
			$single['credit']+=$function_value_return['credit'];
			$single['debit']+=$function_value_return['debit'];
			$single['credit']=number_format($single['credit'],2,'.','');
			$single['debit']=number_format($single['debit'],2,'.','');
			$single['net_right']=number_format($single['net_right'],2,'.','');
			$single['net_left']=number_format(($single['net_left']*(-1)),2,'.','');
			if($ProfitLoss['Net']['Left'])
			$profit=-$ProfitLoss['Net']['Left'];
			else
			$profit=$ProfitLoss['Net']['Right'];
			$single['profit']=number_format($profit,2,'.','');
			$single['name']=ucwords(strtolower($value['AccSubGroup']['name']));
			$single['sub_group_id']=$value['AccSubGroup']['id'];
			$single['profit_sub_group_id']=$profit_sub_group_id;
			$Subgroup[]=$single;
		}
		$return['Subgroup']=$Subgroup;
		
		echo json_encode($return);exit;
	
	}
	public function General_Journal_Debit_N_Credit_function_balance_sheet_subgroup($type_id,$from_date,$to_date,$branch_id=null)
	{
		
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$Journal_Debit_conditions= [];
		if($branch_id) $Journal_Debit_conditions['Journal.branch_id']=$branch_id;
		$Journal_Debit_conditions['AccountHeadDebit.acc_sub_group_id'] = $type_id;
		// $Journal_Debit_conditions['credit !='] =19;
		$Journal_Debit_conditions['flag'] = '1';
		$Journal_Debit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
		$Journal_Credit_conditions= [];
		if($branch_id) $Journal_Credit_conditions['Journal.branch_id']=$branch_id;
		$Journal_Credit_conditions['AccountHeadCredit.acc_sub_group_id'] = $type_id;
		// if($type_id==2) $Journal_Credit_conditions['AccountHeadDebit.id !='] = 13;
		// $Journal_Credit_conditions['debit !='] = 19;
		$Journal_Credit_conditions['flag'] = '1';
		$Journal_Credit_conditions['Journal.date between ? and ?'] = array(date('Y-m-d',strtotime($from_date)),date('Y-m-d',strtotime($to_date)));
			$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		$Journal_Debit=$this->Journal->find('first',array(
						'conditions'=>array('credit!=19',$Journal_Debit_conditions),
						'fields'=>array( 'Journal.total_amount'),
					));
		$Journal_Credit=$this->Journal->find('first',array(
						'conditions'=>array('debit!=19',$Journal_Credit_conditions),
						'fields'=>array( 'Journal.total_amount'),
					));
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
	public function General_Journal_Openging_Debit_N_Credit_function_balance_sheet_subgroup($type_id,$from_date,$to_date,$branch_id=null)
	{	
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$this->Journal->virtualFields = array('total_amount' => "SUM(amount)");
		if($branch_id) $conditions['Journal.branch_id']=$branch_id;
		$conditions['Journal.flag']=1;
		$Journal_Debit=$this->Journal->find('first',array(
			'conditions'=>array(
				'AccountHeadDebit.acc_sub_group_id'=>$type_id,
				'credit !='=>19,
				$conditions,
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$Journal_Credit=$this->Journal->find('first',array(
			'conditions'=>array(
				'AccountHeadCredit.acc_sub_group_id'=>$type_id,
				'debit !='=>19,
				$conditions,
				'Journal.date <'=>date('Y-m-d',strtotime($from_date)),
				),
			'fields'=>array( 'Journal.total_amount'),
			));
		$this->AccountHead->virtualFields = array('total_opening_balance' => "SUM(AccountHead.opening_balance)");
		$opening_conditions=[];
		if($branch_id) $opening_conditions['AccountHead.branch_id']=$branch_id;
		//$opening_conditions['AccountHead.id !=']=19;
		$opening_balance=$this->AccountHead->find('first',array(
			'conditions'=>array(
				'AccountHead.acc_sub_group_id'=>$type_id,
				$opening_conditions,
				'AccountHead.created_at <='=>date('Y-m-d',strtotime($to_date)),
				),
			'fields'=>array( 'AccountHead.total_opening_balance'),
			));
		$account_single['opening_balance']=number_format($opening_balance['AccountHead']['total_opening_balance'],2,'.','');
		$account_single['credit']=number_format($Journal_Credit['Journal']['total_amount'],2,'.','');
		$account_single['debit']=number_format($Journal_Debit['Journal']['total_amount'],2,'.','');
		return $account_single;
	}
	public function balance_sheet_accounthead()
	{
        $requestData=$this->request->data;
		$columns = [];
		$columns[]='AccountHead.name';
		$columns[]='AccountHead.id';
		$columns[]='AccountHead.id';
		$columns[]='AccountHead.id';
		$columns[]='AccountHead.id';
		$conditions=[];
		//$conditions['AccMainGroup.id']=1;
		$from_date=$requestData['from_date'];
		$to_date=$requestData['to_date'];
        $conditions['AccountHead.acc_sub_group_id']=$requestData['sub_group_id'];
	    // $conditions['AccountHead.id']=19;
		$type_id=$requestData['type_id'];
		$totalData=$this->AccountHead->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		$Data=$this->AccountHead->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'AccountHead.*',
				)
			));
		$net_left=0;$net_right=0;
		$ProfitLossController = new ProfitLossController;
		$ProfitLoss=$ProfitLossController->profit_loss_calculator($from_date,$to_date);
		$profit_sub_group_id=$this->SystemParameter->field('value',array('id'=>14));
		$profit_loss_account_head_id=$this->SystemParameter->field('value',array('id'=>15));
		foreach ($Data as $key => $value) {
			$Data[$key]['AccountHead']['net_left']=0;
			$Data[$key]['AccountHead']['net_right']=0;
			$Data[$key]['AccountHead']['net_left']+=isset($ProfitLoss['Net']['Left'])?$ProfitLoss['Net']['Left']:0;
			$Data[$key]['AccountHead']['net_right']+=isset($ProfitLoss['Net']['Right'])?$ProfitLoss['Net']['Right']:0;
			$net_left+=$ProfitLoss['Net']['Left'];
			$net_right+=$ProfitLoss['Net']['Right'];
			$Data[$key]['AccountHead']['opening_balance']=0;
			$Data[$key]['AccountHead']['amount']=0;
			$Data[$key]['AccountHead']['current']=0;
			$Data[$key]['AccountHead']['debit']=0;
			$Data[$key]['AccountHead']['credit']=0;
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function2($value['AccountHead']['id'],$from_date,$to_date);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function3($value['AccountHead']['id'],$from_date,$to_date);
			if(in_array($type_id, ['1','3']))
			{     
				$created_at = date('d-m-Y',strtotime($value['AccountHead']['created_at'])); 
				// if($to_date >= $created_at)
				// {
				$Data[$key]['AccountHead']['opening_balance']+=$value['AccountHead']['opening_balance'];
				// }
				if($type_id=='1')
				{
					$Data[$key]['AccountHead']['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
					$Data[$key]['AccountHead']['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
					$Data[$key]['AccountHead']['current']+=number_format($function_value_return['debit'],2,'.','');
					$Data[$key]['AccountHead']['current']-=number_format($function_value_return['credit'],2,'.','');
				}
				if($type_id=='3')
				{
					$Data[$key]['AccountHead']['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
					$Data[$key]['AccountHead']['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
					$Data[$key]['AccountHead']['current']+=number_format($function_value_return['debit'],2,'.','');
					$Data[$key]['AccountHead']['current']-=number_format($function_value_return['credit'],2,'.','');
				}
				$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['opening_balance'];
				$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['current'];
		    }
		    if($type_id=='5')
			{					
				$created_at = date('d-m-Y',strtotime($value['AccountHead']['created_at'])); 
				// if($to_date >= $created_at)
				// {
				$Data[$key]['AccountHead']['opening_balance']+=$value['AccountHead']['opening_balance'];
				// }
				$Data[$key]['AccountHead']['opening_balance']+=$opening_function_value_return['debit'];
				$Data[$key]['AccountHead']['opening_balance']-=$opening_function_value_return['credit'];
				$Data[$key]['AccountHead']['current']+=$function_value_return['debit']-$function_value_return['credit'];
				$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['opening_balance'];
				$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['current'];
			}
			if($type_id=='2')
		    {
				$created_at = date('d-m-Y',strtotime($value['AccountHead']['created_at'])); 
				// if($to_date >= $created_at)
				// {
				$Data[$key]['AccountHead']['opening_balance']+=$value['AccountHead']['opening_balance'];
				// }
				$Data[$key]['AccountHead']['opening_balance']+=$opening_function_value_return['credit'];
				$Data[$key]['AccountHead']['opening_balance']-=$opening_function_value_return['debit'];
				$Data[$key]['AccountHead']['current']+=$function_value_return['debit']-$function_value_return['credit'];
				$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['opening_balance'];
				$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['current'];
		   	}
			if($type_id=='4')
			{
				$created_at = date('d-m-Y',strtotime($value['AccountHead']['created_at'])); 
				// if($to_date >= $created_at)
				// {
				$Data[$key]['AccountHead']['opening_balance']+=$value['AccountHead']['opening_balance'];
				// }
				$Data[$key]['AccountHead']['opening_balance']+=$opening_function_value_return['credit'];
				$Data[$key]['AccountHead']['opening_balance']-=$opening_function_value_return['debit'];
				$Data[$key]['AccountHead']['current']+=$function_value_return['debit']-$function_value_return['credit'];			
				$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['opening_balance'];
				$Data[$key]['AccountHead']['amount']+=$Data[$key]['AccountHead']['current'];
		   	}
			//    print_r($profit_sub_group_id);
			if($profit_sub_group_id==$value['AccountHead']['acc_sub_group_id']){
				
				if($profit_loss_account_head_id==$value['AccountHead']['id']){
					$Data[$key]['AccountHead']['debit']=number_format($Data[$key]['AccountHead']['net_right'],2,'.','');
					$amount=$Data[$key]['AccountHead']['net_right']-0;
					$Data[$key]['AccountHead']['amount']=number_format($amount,2,'.','');
				}else{
					$Data[$key]['AccountHead']['credit']=number_format($Data[$key]['AccountHead']['net_left'],2,'.','');
					$amount=0-$Data[$key]['AccountHead']['net_left'];
					$Data[$key]['AccountHead']['amount']=number_format($amount,2,'.','');
				}
				
			}else{
				$Data[$key]['AccountHead']['credit']+=$function_value_return['credit'];
				$Data[$key]['AccountHead']['debit']+=$function_value_return['debit'];
				$Data[$key]['AccountHead']['credit']=number_format($Data[$key]['AccountHead']['credit'],2,'.','');
				$Data[$key]['AccountHead']['debit']=number_format($Data[$key]['AccountHead']['debit'],2,'.','');
				$Data[$key]['AccountHead']['current']=number_format($Data[$key]['AccountHead']['current'],2,'.','');
				$Data[$key]['AccountHead']['amount']=number_format($Data[$key]['AccountHead']['amount'],2,'.','');
			}
			$Data[$key]['AccountHead']['opening_balance']=number_format($Data[$key]['AccountHead']['opening_balance'],2,'.','');
			$Data[$key]['AccountHead']['name']=$value['AccountHead']['name'];
			// $Data[$key]['AccountHead']['name']='<span hidden class="hidden_AccountHead_id">'.$value['AccountHead']['id'].'</span>'.$value['AccountHead']['name'];
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
			);
		echo json_encode($json_data); exit;
	
	}
	public function print_balance_sheet_report($from_date,$to_date,$branch_id=null)
	{
		$this->response->download("reports_balance_sheet.csv");
		$Data=$this->AccMainGroup->find('all',array(
			'conditions'=>array('AccMainGroup.id IN'=>[2,5]),
			'fields'=>array(
				'AccMainGroup.*',
			)
		));
		$from=date("Y-m-d", strtotime($from_date));
		$to  =date("Y-m-d", strtotime($to_date));
		$accounts_array=array();
		$data=[];
		$single_data=[
			'Particular',
			'Closing',
			'',
			'Particular',
			'Closing',
		];
		$data[]=$single_data;
		$maingroup=[];
		$ProfitLossController = new ProfitLossController;
		$profit_sub_group_id=$this->SystemParameter->field('value',array('id'=>14));
		$profit_loss_account_head_id=$this->SystemParameter->field('value',array('id'=>15));
		$ProfitLoss=$ProfitLossController->profit_loss_calculator($from_date,$to_date,$branch_id);
		$total_left=0;
		foreach ($Data as $key => $value) {
			$type_id=$value['AccMainGroup']['id'];
			$single_main['id']=$value['AccMainGroup']['id'];
			$single_main['opening_balance']=0;
			$single_main['amount']=0;
			$single_main['current']=0;
			$single_main['debit']=0;
			$single_main['credit']=0;
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date,$branch_id);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date,$branch_id);
			
		    if($type_id=='5')
			{					
				$single_main['opening_balance']+=$opening_function_value_return['opening_balance'];
				$single_main['opening_balance']+=$opening_function_value_return['debit'];
				$single_main['opening_balance']-=$opening_function_value_return['credit'];
				$single_main['current']+=$function_value_return['debit']-$function_value_return['credit'];
				$single_main['amount']+=$single_main['opening_balance'];
				$single_main['amount']+=$single_main['current'];
				$single_main['credit']+=$function_value_return['credit'];
				$single_main['debit']+=$function_value_return['debit'];
			}
			if($type_id=='2')
		    {
				
				$single_main['opening_balance']+=$opening_function_value_return['opening_balance'];
				$single_main['opening_balance']+=$opening_function_value_return['debit'];
				$single_main['opening_balance']-=$opening_function_value_return['credit'];
				$single_main['current']+=($function_value_return['debit']+$ProfitLoss['Net']['Right'])-($function_value_return['credit']+$ProfitLoss['Net']['Left']);
				$single_main['amount']+=$single_main['opening_balance'];
				$single_main['amount']+=$single_main['current'];
				$single_main['credit']+=$function_value_return['credit']+$ProfitLoss['Net']['Left'];
				$single_main['debit']+=$function_value_return['debit']+$ProfitLoss['Net']['Right'];
		   	}
		   
			$single_main['opening_balance']=number_format($single_main['opening_balance'],2,'.','');
			$single_main['current']=number_format($single_main['current'],2,'.','');
			$single_main['amount']=number_format($single_main['amount'],2,'.','');
			// $single_main['credit']+=$function_value_return['credit'];
			// $single_main['debit']+=$function_value_return['debit'];
			$single_main['credit']=number_format($single_main['credit'],2,'.','');
			$single_main['debit']=number_format($single_main['debit'],2,'.','');
			$single_main['name']=$value['AccMainGroup']['name'];
			$single_data=[
				$single_main['name'],
				$single_main['amount'],
				'',
				'',
				''
			];
			$data[]=$single_data;
			$single_data=[
				'',
				'',
				'',
				'',
				'',
			];
			$data[]=$single_data;
			$conditions_subgroup=[];
			$conditions_subgroup['AccSubGroup.main_group_id']=$value['AccMainGroup']['id'];
			// $conditions_subgroup['AccSubGroup.id !=']=14;
			$Data_subgroup=$this->AccSubGroup->find('all',array(
					'conditions'=>$conditions_subgroup,
					'fields'=>array(
						'AccSubGroup.*',
						)
					));
			$Subgroup=[];
			foreach ($Data_subgroup as $key1 => $value1) 
			{
				$single['sub_group_id']=$value1['AccSubGroup']['id'];
				$single['profit_sub_group_id']=$profit_sub_group_id;
				$single['name']=$value1['AccSubGroup']['name'];
				$single['opening_balance']=0;
				$single['amount']=0;
				$single['current']=0;
				$single['debit']=0;
				$single['credit']=0;
				$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_balance_sheet_subgroup($value1['AccSubGroup']['id'],$from_date,$to_date,$branch_id);
				$function_value_return=$this->General_Journal_Debit_N_Credit_function_balance_sheet_subgroup($value1['AccSubGroup']['id'],$from_date,$to_date,$branch_id);
				if($type_id=='5')
				{					
						$single['opening_balance']+=$opening_function_value_return['opening_balance'];
						$single['opening_balance']+=$opening_function_value_return['debit'];
						$single['opening_balance']-=$opening_function_value_return['credit'];
						$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
						$single['amount']+=$single['opening_balance'];
						$single['amount']+=$single['current'];
				}
				if($type_id=='2')
				{
						$single['opening_balance']+=$opening_function_value_return['opening_balance'];
						$single['opening_balance']+=$opening_function_value_return['debit'];
						$single['opening_balance']-=$opening_function_value_return['credit'];
						$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
						$single['amount']+=$single['opening_balance'];
						$single['amount']+=$single['current'];
				}
				$single['opening_balance']=number_format($single['opening_balance'],2,'.','');
				$single['current']=number_format($single['current'],2,'.','');
				$single['amount']=number_format($single['amount'],2,'.','');
				$single['credit']+=$function_value_return['credit'];
				$single['debit']+=$function_value_return['debit'];
				$single['credit']=number_format($single['credit'],2,'.','');
				$single['debit']=number_format($single['debit'],2,'.','');
				$single['name']=$value1['AccSubGroup']['name'];
				if($ProfitLoss['Net']['Left'])
				$profit=-$ProfitLoss['Net']['Left'];
				else
				$profit=$ProfitLoss['Net']['Right'];
				$single['profit']=number_format($profit,2,'.','');
				$Subgroup[]=$single;
				if($single['sub_group_id']==$profit_sub_group_id)
				{
					$single_data=[
						$single['name'],
						$single['profit'],
						'',
						'',
						''
					];
				}else{
					$single_data=[
						$single['name'],
						$single['amount'],
						'',
						'',
						''
					];
				}
				$data[]=$single_data;
				
			}
			$single_data=[
				'',
				'',
				'',
				'',
				'',
			];
			$data[]=$single_data;
			$total_left+=$single_main['opening_balance']+$single_main['debit']-$single_main['credit'];
		}
		$total_left=number_format(abs($total_left),2,".","");
		$Data1=$this->AccMainGroup->find('all',array(
			'conditions'=>array('AccMainGroup.id'=>1),
			'fields'=>array(
				'AccMainGroup.*',
			)
		));
		$total_right=0;
		foreach ($Data1 as $key => $value) {
			
			$type_id=$value['AccMainGroup']['id'];
			$single_main['id']=$value['AccMainGroup']['id'];
			$single_main['opening_balance']=0;
			$single_main['amount']=0;
			$single_main['current']=0;
			$single_main['debit']=0;
			$single_main['credit']=0;
			$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date,$branch_id);
			$function_value_return=$this->General_Journal_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date,$branch_id);
			
				$single_main['opening_balance']+=$opening_function_value_return['opening_balance'];
				
				$single_main['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
				$single_main['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
				$single_main['current']+=number_format($function_value_return['debit'],2,'.','');
				$single_main['current']-=number_format($function_value_return['credit'],2,'.','');
				
				$single_main['amount']+=$single_main['opening_balance'];
				$single_main['amount']+=$single_main['current'];
				$single_main['credit']+=$function_value_return['credit'];
				$single_main['debit']+=$function_value_return['debit'];
		    
			$single_main['opening_balance']=number_format($single_main['opening_balance'],2,'.','');
			$single_main['current']=number_format($single_main['current'],2,'.','');
			$single_main['amount']=number_format($single_main['amount'],2,'.','');
			// $single_main['credit']+=$function_value_return['credit'];
			// $single_main['debit']+=$function_value_return['debit'];
			$single_main['credit']=number_format($single_main['credit'],2,'.','');
			$single_main['debit']=number_format($single_main['debit'],2,'.','');
			$single_main['name']=$value['AccMainGroup']['name'];
			$single_data=[
				'',
				'',
				'',
				$single_main['name'],
				$single_main['amount'],
				
			];
			$data[]=$single_data;
			$single_data=[
				'',
				'',
				'',
				'',
				'',
			];
			$data[]=$single_data;
			$conditions_subgroup=[];
			$conditions_subgroup['AccSubGroup.main_group_id']=$value['AccMainGroup']['id'];
			// $conditions_subgroup['AccSubGroup.id !=']=14;
			$Data_subgroup=$this->AccSubGroup->find('all',array(
					'conditions'=>$conditions_subgroup,
					'fields'=>array(
						'AccSubGroup.*',
						)
					));
			$Subgroup=[];
			foreach ($Data_subgroup as $key1 => $value1) 
			{
				$single['sub_group_id']=$value1['AccSubGroup']['id'];
				$single['profit_sub_group_id']=$profit_sub_group_id;
				$single['name']=$value1['AccSubGroup']['name'];
				$single['opening_balance']=0;
				$single['amount']=0;
				$single['current']=0;
				$single['debit']=0;
				$single['credit']=0;
				$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_balance_sheet_subgroup($value1['AccSubGroup']['id'],$from_date,$to_date,$branch_id);
				$function_value_return=$this->General_Journal_Debit_N_Credit_function_balance_sheet_subgroup($value1['AccSubGroup']['id'],$from_date,$to_date,$branch_id);
				if(in_array($type_id, ['1','3']))
				{
					$single['opening_balance']+=$opening_function_value_return['opening_balance'];
					if($type_id=='1')
					{
						$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single['current']+=number_format($function_value_return['debit'],2,'.','');
						$single['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					if($type_id=='3')
					{
						$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
						$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
						$single['current']+=number_format($function_value_return['debit'],2,'.','');
						$single['current']-=number_format($function_value_return['credit'],2,'.','');
					}
					$single['amount']+=$single['opening_balance'];
					$single['amount']+=$single['current'];
				}
				$single['opening_balance']=number_format($single['opening_balance'],2,'.','');
				$single['current']=number_format($single['current'],2,'.','');
				$single['amount']=number_format($single['amount'],2,'.','');
				$single['credit']+=$function_value_return['credit'];
				$single['debit']+=$function_value_return['debit'];
				$single['credit']=number_format($single['credit'],2,'.','');
				$single['debit']=number_format($single['debit'],2,'.','');
				$single['name']=$value1['AccSubGroup']['name'];
				$Subgroup[]=$single;
				$single_data=[
					'',
					'',
					'',
					$single['name'],
					$single['amount'],
					
				];
				$data[]=$single_data;
			}
			$single_data=[
				'',
				'',
				'',
				'',
				'',
			];
			$data[]=$single_data;
			$total_right+=$single_main['opening_balance']+$single_main['debit']-$single_main['credit'];
		}
		$total_right=number_format($total_right,2,".","");
		if($total_left>$total_right){
			$balance=$total_left-$total_right;
			$total_right=$balance+$total_right;
			$single_data=[
				'Difference',
				'0',
				'',
				'',
				$balance,
			];
			$data[]=$single_data;
		}else{
			$balance=$total_right-$total_left;
			$total_left=$balance+$total_left;
			$single_data=[
				'Difference',
				$balance,
				'',
				'',
				'0',
			];
			$data[]=$single_data;
		}
		$single_data=[
			'',
			'',
			'',
			'',
			'',
		];
		$data[]=$single_data;
		$single_data=[
			'Total',
			$total_left,
			'',
			'',
			$total_right,
		];
		$data[]=$single_data;
		// return $maingroup;
		foreach($data as $key1 => $value1) { 
			array_push($accounts_array,$value1);
		}
		$this->set(compact('accounts_array','from','to','branch_id'));
		$this->layout = 'ajax';
		return false;
		exit;
	}
	public function BalanceSheet_Print($from_date=null,$to_date=null,$branch_id=null)
	{
		$decimalpoint =2;
		$userid = $this->Session->read('User.name');
		
		$Profile=$this->Global_Var_Profile['Profile'];
		// $from_date=date('Y-m-d',strtotime($from_date));
		// $to_date=date('Y-m-t',strtotime($to_date));
		// print_r($from_date.' - '.$to_date);
		// $branch_id=4;
		$name='';
		$print_field_array=[];
		$print_field_array[0]="Original For Customer";
		// $print_field_array[1]="Duplicate For Executive";
		$Checkstate=0;
		$Data_left=$this->AccMainGroup->find('all',array(
			'conditions'=>array('AccMainGroup.id IN'=>[2,5]),
			'fields'=>array(
				'AccMainGroup.*',
			)
		));
		$Data_right=$this->AccMainGroup->find('all',array(
			'conditions'=>array('AccMainGroup.id'=>1),
			'fields'=>array(
				'AccMainGroup.*',
			)
		));
		if($branch_id) $branch=$branch_id;
		else $branch=4;
		// $Address=$this->Branch->find('first',array(
		// 	'conditions'=>array('Branch.id'=>$branch),
		// 	'fields'=>array('Branch.*')
		// ));
		$Address="PKK";
		$ProfitLossController = new ProfitLossController;
		$profit_sub_group_id=$this->SystemParameter->field('value',array('id'=>14));
		$profit_loss_account_head_id=$this->SystemParameter->field('value',array('id'=>15));
		$ProfitLoss=$ProfitLossController->profit_loss_calculator($from_date,$to_date);
		require('fpdf/fpdf.php');
		define('FPDF_FONTPATH','fpdf/font');
		$pdf = new FPDF('p', 'mm', [297, 210]);

		$page=1;
		$total_page=1;
		$i=0;
		
		$igst_x_position=0;
		if($Checkstate==0)
		{
		$igst_x_position=0;
		}
		function header_section($pdf,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value,$Data_left,$Data_right,$from_date,$to_date,$branch_id,$Address)
		{
			$invoice_pos=0;
			$pdf->AddPage();
			$image_file ='profile/'.$Profile['logo'];
			$image_line_width=0;
			$invoice_x_starting=$image_line_width;
			$pdf->SetFont('times', 'B', 8, "", 'false');
			$pdf->SetFont('Arial', 'B', 12);
			// $pdf->SetTextColor(0,0,100);
			// $pdf->text(100,$invoice_pos+8,$Profile['company_name']);
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos+2);
			$pdf->MultiCell(0, $invoice_pos+10,$Profile['company_name'], 0, 'C');
			$pdf->Ln(0);
			$pdf->SetFont('Arial', '', 10);
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos+8);
			$pdf->MultiCell(0, $invoice_pos+11,$Profile['address_line_1'], 0, 'C');
			$pdf->Ln(0);
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos+16);
			$pdf->MultiCell(0, $invoice_pos+3,$Profile['address_line_2'].' ,'.$Profile['mobile'], 0, 'C');
			$pdf->Ln(0);
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos+18);
			$pdf->MultiCell(0, $invoice_pos+7,$Profile['mail_address'], 0, 'C');
			$pdf->Ln(0);
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos+23);
			$pdf->MultiCell(0, $invoice_pos+7,'GST Number : '.$Profile['vat_code'], 0, 'C');
			$pdf->Ln(0);
			$pdf->Line(68, $invoice_pos+29,142, $invoice_pos+29); // horizontal 
			$pdf->SetFont('Arial', 'B', 12);
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos+23);
			$pdf->MultiCell(0, $invoice_pos+21,'Balance Sheet', 0, 'C');
			$pdf->Ln(0);
			// $pdf->text(93,$invoice_pos+32,'Balance Sheet');
			$pdf->SetFont('Arial', '', 10);
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos+23);
			$pdf->MultiCell(0, $invoice_pos+32,date('d-M-Y',strtotime($from_date)).' To '.date('d-M-Y',strtotime($to_date)), 0, 'C');
			$pdf->Ln(0);
			// $pdf->text(84,$invoice_pos+38,date('d-M-Y',strtotime($from_date)).' To '.date('d-M-Y',strtotime($to_date)));
			$gst_details_y_staring=25;
			$pdf->Line(7, $gst_details_y_staring+18,204, $gst_details_y_staring+18); // horizontal line
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->text(10,$gst_details_y_staring+23,'Liabilities');
			$pdf->SetFont('Arial', '', 10);
			$pdf->text(70,$gst_details_y_staring+23,'as at '.date('d-M-Y',strtotime($to_date)));
			$pdf->Line(7, $gst_details_y_staring+26,204, $gst_details_y_staring+26); // horizontal line
			$pdf->SetFont('times', '', 10, "", 'false');
			$gst_pos=$gst_details_y_staring+2;
			$gst_line_width=120;
			$pdf->SetFont('Arial','B',12);
			// $pdf->SetTextColor(0,0,100);
			// $pdf->rect(7, 10, 197, 280);
			$pdf->SetXY($invoice_x_starting, 5);
			$invoice_pos=8;
			$pdf->SetXY($invoice_x_starting, 5);
			$invoice_pos+=10;
			$pdf->SetXY($invoice_x_starting, 5);
			$invoice_pos+=15;
			$pdf->SetFont('Arial','B',10);
			$pdf->SetXY($invoice_x_starting, 5);
			$invoice_pos+=10;
			$pdf->SetXY($invoice_x_starting, 5);
			$invoice_pos+=10;
			$pdf->SetFont('Arial','U',10);
			$pdf->SetXY($invoice_x_starting, 5);
			$invoice_pos+=10;
			$gst_details_y_staring=53;
			$pdf->SetFont('Arial','B',10);
			$gst_pos=$gst_details_y_staring+1;
			$gst_line_width=110;
			$pdf->Line($gst_line_width-3, $gst_details_y_staring-10,$gst_line_width-3, $gst_details_y_staring+122); // vertical line
			$pdf->text($gst_line_width,$gst_details_y_staring-5,'Assets');
			$pdf->SetFont('Arial', '', 10);
			$pdf->text($gst_line_width+60,$gst_details_y_staring-5,'as at '.date('d-M-Y',strtotime($to_date)));
			
			// $pdf->Line(7, $gst_details_y_staring+0+13+25,204, $gst_details_y_staring+0+13+25); // horizontal line
			// $pdf->SetFont('Arial', 'B', 10);
			// $pdf->text(90,$gst_pos+43,'Expense');
			// $pdf->Line(7, $gst_details_y_staring+0+13+35,204, $gst_details_y_staring+0+13+35); // horizontal line
			// $pdf->Line($gst_line_width+25, $gst_details_y_staring+28,$gst_line_width+25, $gst_details_y_staring+237); // vertical line
			
			
		}
		function footer($pdf,$data,$decimalpoint)
		{
			$footer_start_y=211;
			$invoce_word_width=121;
			$grand_total_length=30;
			
			$pdf->rect(7, $footer_start_y+132, 197,11);
			$pdf->SetFont('Arial','B','8');
			$pdf->SetFont('Arial','',7.5);
			//$pdf->StartTransform();
			//Rotate 20 degrees counter-clockwise centered by (50,60) which is the lower left corner of the rectangle
			//$pdf->Rotate(90, 50, 60);
			// $pdf->Text(-2, 16, date('d/m/Y  h:i A'));
			//$pdf->StopTransform();
		}
		$page=0;
		foreach($print_field_array as $field_key=>$field_value)
		{
			//$page=1;
			$page++;
			header_section($pdf,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position,$field_value,$Data_left,$Data_right,$from_date,$to_date,$branch_id,$Address);
			$total_right=0;
			$total_left=0;
			$balance=0;
			$item_table_head_y_start='104';
			$table_length='145';
			$i=0;
			// for ($i=0; $i <$count ; $i++) { 
			
			$account_all1=[];
			$Total['Expense']=0;
			$gst_pos=50;
			$gst_line_width=25;
			$pdf->SetFont('Arial', 'B', 8);
			
			$pdf_row = 0;
			$pdf_rows=0;
			foreach ($Data_left as $key => $value) {
				$type_id=$value['AccMainGroup']['id'];
				$single_main['id']=$value['AccMainGroup']['id'];
				$single_main['opening_balance']=0;
				$single_main['amount']=0;
				$single_main['current']=0;
				$single_main['debit']=0;
				$single_main['credit']=0;
				$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date,$branch_id);
				$function_value_return=$this->General_Journal_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date,$branch_id);
				
				if($type_id=='5')
				{					
					$single_main['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single_main['opening_balance']+=$opening_function_value_return['debit'];
					$single_main['opening_balance']-=$opening_function_value_return['credit'];
					$single_main['current']+=$function_value_return['debit']-$function_value_return['credit'];
					$single_main['amount']+=$single_main['opening_balance'];
					$single_main['amount']+=$single_main['current'];
					$single_main['credit']+=$function_value_return['credit'];
					$single_main['debit']+=$function_value_return['debit'];
				}
				if($type_id=='2')
				{
					
					$single_main['opening_balance']+=$opening_function_value_return['opening_balance'];
					$single_main['opening_balance']+=$opening_function_value_return['debit'];
					$single_main['opening_balance']-=$opening_function_value_return['credit'];
					$single_main['current']+=($function_value_return['debit']+$ProfitLoss['Net']['Right'])-($function_value_return['credit']+$ProfitLoss['Net']['Left']);
					$single_main['amount']+=$single_main['opening_balance'];
					$single_main['amount']+=$single_main['current'];
					$single_main['credit']+=$function_value_return['credit']+$ProfitLoss['Net']['Left'];
					$single_main['debit']+=$function_value_return['debit']+$ProfitLoss['Net']['Right'];
				}
			
				$single_main['opening_balance']=number_format($single_main['opening_balance'],2,'.','');
				$single_main['current']=number_format($single_main['current'],2,'.','');
				$single_main['amount']=abs(number_format($single_main['amount'],2,'.',''));
				// $single_main['credit']+=$function_value_return['credit'];
				// $single_main['debit']+=$function_value_return['debit'];
				$single_main['credit']=number_format($single_main['credit'],2,'.','');
				$single_main['debit']=number_format($single_main['debit'],2,'.','');
				$single_main['name']=ucwords(strtolower($value['AccMainGroup']['name']));
				// $pdf->SetFont('Arial', 'B', 10);
				// $pdf->text(10,$gst_pos+10+($pdf_row*8),$single_main['name']);
				// $pdf->text($gst_line_width+58,$gst_pos+10+($pdf_row*8),number_format($single_main['amount'],2));
				$pdf->SetFont('Arial', 'B', 10);
				$pdf->text(8,$gst_pos+9+($pdf_row*8),$single_main['name']);
				 $pdf->SetX(0);
	             $pdf->SetY($gst_pos+9+($pdf_row*8));
	            $pdf->MultiCell($gst_line_width+70,$gst_pos-53,number_format(abs($single_main['amount']),2,'.',''),0,'R');
	            $pdf->Ln(3);
				$pdf_row++;
				$conditions_subgroup=[];
				$conditions_subgroup['AccSubGroup.main_group_id']=$value['AccMainGroup']['id'];
				// $conditions_subgroup['AccSubGroup.id !=']=14;
				$Data_subgroup=$this->AccSubGroup->find('all',array(
					'conditions'=>$conditions_subgroup,
					'fields'=>array(
						'AccSubGroup.*',
					),
					'order'=>array('AccSubGroup.name ASC')
				));
				
				foreach ($Data_subgroup as $key1 => $value1) 
				{
					$single['sub_group_id']=$value1['AccSubGroup']['id'];
					$single['profit_sub_group_id']=$profit_sub_group_id;
					$single['name']=ucwords(strtolower($value1['AccSubGroup']['name']));
					$single['opening_balance']=0;
					$single['amount']=0;
					$single['current']=0;
					$single['debit']=0;
					$single['credit']=0;
					$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_balance_sheet_subgroup($value1['AccSubGroup']['id'],$from_date,$to_date,$branch_id);
					$function_value_return=$this->General_Journal_Debit_N_Credit_function_balance_sheet_subgroup($value1['AccSubGroup']['id'],$from_date,$to_date,$branch_id);
					if($type_id=='5')
					{					
							$single['opening_balance']+=$opening_function_value_return['opening_balance'];
							$single['opening_balance']+=$opening_function_value_return['debit'];
							$single['opening_balance']-=$opening_function_value_return['credit'];
							$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
							$single['amount']+=$single['opening_balance'];
							$single['amount']+=$single['current'];
					}
					if($type_id=='2')
					{
							$single['opening_balance']+=$opening_function_value_return['opening_balance'];
							$single['opening_balance']+=$opening_function_value_return['debit'];
							$single['opening_balance']-=$opening_function_value_return['credit'];
							$single['current']+=$function_value_return['debit']-$function_value_return['credit'];
							$single['amount']+=$single['opening_balance'];
							$single['amount']+=$single['current'];
					}
					$single['opening_balance']=number_format($single['opening_balance'],2,'.','');
					$single['current']=number_format($single['current'],2,'.','');
					$single['amount']=number_format($single['amount'],2,'.','');
					$single['credit']+=$function_value_return['credit'];
					$single['debit']+=$function_value_return['debit'];
					$single['credit']=number_format($single['credit'],2,'.','');
					$single['debit']=number_format($single['debit'],2,'.','');
					$single['name']=ucwords(strtolower($value1['AccSubGroup']['name']));
					if($ProfitLoss['Net']['Left'])
					$profit=-$ProfitLoss['Net']['Left'];
					else
					$profit=$ProfitLoss['Net']['Right'];
					$single['profit']=number_format($profit,2,'.','');
					$Subgroup[]=$single;
					if($single['sub_group_id']==$profit_sub_group_id)
					{
						// $pdf->SetFont('Arial', 'I', 10);
						// $pdf->text(15,$gst_pos+10+($pdf_row*7),$single['name']);
						// $pdf->text($gst_line_width+45,$gst_pos+10+($pdf_row*7),number_format($single['profit'],2));
						$pdf->SetFont('Arial', 'I', 10);
						$pdf->text(15,$gst_pos+10+($pdf_row*7),$single['name']);
						$pdf->SetX(0);
	                    $pdf->SetY($gst_pos+10+($pdf_row*7));
	                    $pdf->MultiCell($gst_line_width+58,$gst_pos-51,number_format($single['profit'],2,'.',''),0,'R');
	                    $pdf->Ln(3);
						$pdf_row++;
					}else{
						// $pdf->SetFont('Arial', 'I', 10);
						// $pdf->text(15,$gst_pos+10+($pdf_row*7),$single['name']);
						// $pdf->text($gst_line_width+45,$gst_pos+10+($pdf_row*7),number_format($single['amount'],2));
						$pdf->SetFont('Arial', 'I', 10);
						$pdf->text(15,$gst_pos+10+($pdf_row*7),$single['name']);
						$pdf->SetX(0);
	                    $pdf->SetY($gst_pos+10+($pdf_row*7));
	                    $pdf->MultiCell($gst_line_width+58,$gst_pos-51,number_format($single['amount'],2,'.',''),0,'R');
	                    $pdf->Ln(3);
						$pdf_row++;
					}
					$pdf_rows+=$pdf_row;
					$pdf_rows++;
				}
				$total_left+=$single_main['opening_balance']+$single_main['debit']-$single_main['credit'];
				
			}
			$total_left=number_format(abs($total_left),2,'.','');
			//$pdf->Line($gst_line_width+42, $gst_pos+26,95, $gst_pos+26); // horizontal line
			//$pdf->Line($gst_line_width+42, $gst_pos+26+$pdf_rows,95, $gst_pos+26+$pdf_rows); // horizontal line
			$pdf_row1 = 0;
			$pdf_rows1 = 0;
			foreach ($Data_right as $key => $value) {
			
				$type_id=$value['AccMainGroup']['id'];
				$single_main['id']=$value['AccMainGroup']['id'];
				$single_main['opening_balance']=0;
				$single_main['amount']=0;
				$single_main['current']=0;
				$single_main['debit']=0;
				$single_main['credit']=0;
				$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date,$branch_id);
				$function_value_return=$this->General_Journal_Debit_N_Credit_function_balance_sheet($value['AccMainGroup']['id'],$from_date,$to_date,$branch_id);
				
					$single_main['opening_balance']+=$opening_function_value_return['opening_balance'];
					
					$single_main['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
					$single_main['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
					$single_main['current']+=number_format($function_value_return['debit'],2,'.','');
					$single_main['current']-=number_format($function_value_return['credit'],2,'.','');
					
					$single_main['amount']+=$single_main['opening_balance'];
					$single_main['amount']+=$single_main['current'];
					$single_main['credit']+=$function_value_return['credit'];
					$single_main['debit']+=$function_value_return['debit'];
				
				$single_main['opening_balance']=number_format($single_main['opening_balance'],2,'.','');
				$single_main['current']=number_format($single_main['current'],2,'.','');
				$single_main['amount']=number_format($single_main['amount'],2,'.','');
				// $single_main['credit']+=$function_value_return['credit'];
				// $single_main['debit']+=$function_value_return['debit'];
				$single_main['credit']=number_format($single_main['credit'],2,'.','');
				$single_main['debit']=number_format($single_main['debit'],2,'.','');
				$single_main['name']=ucwords(strtolower($value['AccMainGroup']['name']));
				$pdf->SetFont('Arial', 'B', 10);
				$pdf->text($gst_line_width+85,$gst_pos+9+($pdf_row1*7),$single_main['name']);
				$pdf->SetX(0);
				$pdf->SetY($gst_pos+9+($pdf_row1*7));
				$pdf->MultiCell($gst_line_width+171,$gst_pos-52,number_format(abs($single_main['amount']),2,'.',''),0,'R');
				$pdf->Ln(3);
				// $pdf->text($gst_line_width+158,$gst_pos+10+($pdf_row1*7),number_format($single_main['amount'],2));
				$pdf_row1++;
				$conditions_subgroup=[];
				$conditions_subgroup['AccSubGroup.main_group_id']=$value['AccMainGroup']['id'];
				// $conditions_subgroup['AccSubGroup.id !=']=14;
				$Data_subgroup=$this->AccSubGroup->find('all',array(
					'conditions'=>$conditions_subgroup,
					'fields'=>array(
						'AccSubGroup.*',
					),
					'order'=>array('AccSubGroup.name ASC')
				));
				
				foreach ($Data_subgroup as $key1 => $value1) 
				{
					$single['sub_group_id']=$value1['AccSubGroup']['id'];
					$single['profit_sub_group_id']=$profit_sub_group_id;
					$single['name']=ucwords(strtolower($value1['AccSubGroup']['name']));
					$single['opening_balance']=0;
					$single['amount']=0;
					$single['current']=0;
					$single['debit']=0;
					$single['credit']=0;
					$opening_function_value_return=$this->General_Journal_Openging_Debit_N_Credit_function_balance_sheet_subgroup($value1['AccSubGroup']['id'],$from_date,$to_date,$branch_id);
					$function_value_return=$this->General_Journal_Debit_N_Credit_function_balance_sheet_subgroup($value1['AccSubGroup']['id'],$from_date,$to_date,$branch_id);
					if(in_array($type_id, ['1','3']))
					{
						$single['opening_balance']+=$opening_function_value_return['opening_balance'];
						if($type_id=='1')
						{
							$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
							$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
							$single['current']+=number_format($function_value_return['debit'],2,'.','');
							$single['current']-=number_format($function_value_return['credit'],2,'.','');
						}
						if($type_id=='3')
						{
							$single['opening_balance']+=number_format($opening_function_value_return['debit'],2,'.','');
							$single['opening_balance']-=number_format($opening_function_value_return['credit'],2,'.','');
							$single['current']+=number_format($function_value_return['debit'],2,'.','');
							$single['current']-=number_format($function_value_return['credit'],2,'.','');
						}
						$single['amount']+=$single['opening_balance'];
						$single['amount']+=$single['current'];
					}
					$single['opening_balance']=number_format($single['opening_balance'],2,'.','');
					$single['current']=number_format($single['current'],2,'.','');
					$single['amount']=number_format($single['amount'],2,'.','');
					$single['credit']+=$function_value_return['credit'];
					$single['debit']+=$function_value_return['debit'];
					$single['credit']=number_format($single['credit'],2,'.','');
					$single['debit']=number_format($single['debit'],2,'.','');
					$single['name']=ucwords(strtolower($value1['AccSubGroup']['name']));
					$pdf->SetFont('Arial', 'I', 10);
					$pdf->text($gst_line_width+90,$gst_pos+10+($pdf_row1*7),$single['name']);
					$pdf->SetX(0);
					$pdf->SetY($gst_pos+10+($pdf_row1*7));
					$pdf->MultiCell($gst_line_width+160,$gst_pos-52,number_format($single['amount'],2,'.',''),0,'R');
					$pdf->Ln(3);
					// $pdf->text($gst_line_width+150,$gst_pos+10+($pdf_row1*7),number_format($single['amount'],2));
					$pdf_row1++;
					$pdf_rows1+=$pdf_row1;
				}
				 
				$total_right+=$single_main['opening_balance']+$single_main['debit']-$single_main['credit'];
			}
			//$pdf->Line($gst_line_width+170, $gst_pos+$pdf_rows1,170, $gst_pos+$pdf_rows1); // horizontal
			$total_right=number_format(abs($total_right),2,'.','');
			$pdf->Line(7, $gst_pos+115,204, $gst_pos+115); // horizontal line
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->text(10,$gst_pos+120,'Total ');
			$pdf->text($gst_line_width+58,$gst_pos+120,$total_left);
			$pdf->text($gst_line_width+85,$gst_pos+120,'Total ');
			$pdf->text($gst_line_width+158,$gst_pos+120,$total_left);
			$pdf->Line(7, $gst_pos+125,204, $gst_pos+125); // horizontal line
			if($total_left>$total_right){
				$balance=$total_left-$total_right;
				$total_right=$balance+$total_right;
			}else{
				$balance=$total_right-$total_left;
				$total_left=$balance+$total_left;
			}
			$data=array(
				'total_left'=>$total_left,
				'total_right'=>$total_right,
				'balance'=>$balance,
			);
			
			footer($pdf,$data,$decimalpoint);
		}
		$filename='Balance Sheet - '.$from_date.' To '.$to_date.'.pdf';
		$pdf->Output('I',$filename);
		exit;
	}
	public function trial_balance_print($from_date,$to_date,$branch_id=null) 
	{
		// $this->response->download("reports_profit_loss.csv");
		$conditions=array();
		$from=date("Y-m-d", strtotime($from_date));
		$to  =date("Y-m-d", strtotime($to_date));
		$from_date=date("d-M-Y", strtotime($from_date));
		$to_date  =date("d-M-Y", strtotime($to_date));
		// $list_array=array();
		// $list_array=$this->trial_balance_report($from,$to);
		$accounts_array=array();
		$accounts_array=$this->trial_balance_report($from,$to);
		
     $print_field_array=[];
	 $print_field_array[0]="";
		require('fpdf/fpdf.php');
		$pdf = new FPDF('p', 'mm', [297, 210]);

		$Profile=$this->Global_Var_Profile['Profile'];
		$page=1;
		$total_page=1;
		$i=0;
		$igst_x_position=0;
		function header_section($pdf,$accounts_array,$Profile,$total_page,$page,$igst_x_position,$field_value,$from_date,$to_date) 
		{
			$pdf->AddPage();
			
			$image_line_width=40;
			$invoice_x_starting=$image_line_width;

$pdf->rect(1, 1, 208, 295);

// $pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$invoice_pos+=3;
// $pdf->SetFont('Arial','B',15);
$pdf->SetFont('Arial', 'B', 10, "", 'false');

	$invoice_pos+=2;
	// $pdf->SetFont('dejavusans', 'B', 10);
	$pdf->SetFont('Arial', 'B', 12, "", 'false');
	$pdf->text(98,$invoice_pos+5,$Profile['company_name']);
	// $pdf->SetFont('dejavusans', '', 10);

	// $pdf->text(140,$invoice_pos+3,$Profile['company_name_arabic']);

	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->SetFont('Arial', '', 12, "", 'false');
	// $pdf->SetFont('dejavusans', '', 10);
// $pdf->SetFont('Arial','B',10);    مركز القوافل - شارع  الملك فيصل           
	$pdf->text(75,$invoice_pos+5,$Profile['address_line_1']);
	// $pdf->text(140,$invoice_pos+3,$Profile['address_line_1_arabic'] );

	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	$pdf->text(165,33,$field_value);
	// $pdf->SetFont('dejavusans', '', 10);
	$pdf->text(72,$invoice_pos+5,$Profile['address_line_2']);

	$pdf->text(90,$invoice_pos+12,'Ph :'.$Profile['mobile']);
	// $pdf->text(145,$invoice_pos+3,$Profile['address_line_2_arabic']);

	$pdf->SetXY($invoice_x_starting, 5);
	$invoice_pos+=6;
	// $pdf->SetFont('times', 'U', 12, "", 'false');
	$pdf->SetFont('Arial', 'B', 12, "", 'false');
	// $pdf->text(90,$invoice_pos+10,$Profile['mail_address']);
	$pdf->text(94,$invoice_pos+17,'Trial Balance');
	$invoice_pos+=10;
	// $pdf->SetFont('dejavusans', '', 10);
//$pdf->text(2,$gst_pos,'VAT Number : '.$Profile['vat_code']);
$pdf->SetFont('Arial', '', 12, "", 'false');
	// $pdf->text(78,$invoice_pos+15,'VAT Number : '.$Profile['vat_code']);
		$pdf->text(80,$invoice_pos+15,$from_date."  to  ".$to_date );

	$gst_details_y_staring=40;
 $pdf->Line(1, $gst_details_y_staring+15,209, $gst_details_y_staring+15); // horizontal line
 $pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
 $pdf->Line(65, $gst_details_y_staring-8,150, $gst_details_y_staring-8); // horizontal line
$pdf->SetFont('Arial', '', 10, "", 'false');
$gst_pos=$gst_details_y_staring+2;
$gst_line_width=120;

 $gst_pos+=28;

$gst_pos+=7;


$pdf->SetXY(0, 5);
// $pdf->Line(40,$gst_details_y_staring,40, $gst_details_y_staring+17+21+5); // vertical line
$gst_pos+=3;

$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+20;
$Consignee_lin_width=100+1;

$pdf->SetFont('Arial', '', 10, "", 'false');



$customer_detail_startin+=7;

$pdf->SetFont('Arial', '', 10, "", 'false');
$customer_detail_startin+=4;
$customer_detail_startin+=4;
$customer_detail_startin+=1;

$pdf->SetFont('Arial', '', 10, "", 'false');

// $pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+7;
//  table head start
$table_length=135;

$table_column=9;
$pdf->SetFont('Arial', 'B', 10, "", 'false');
// $pdf->Line($table_column+95, $item_table_head_y_start-28,$table_column+95, $item_table_head_y_start+60+$table_length); // vertical line
$pdf->text($table_column,$item_table_head_y_start-22,'Particulars');
$pdf->SetFont('Arial', '', 9, "", 'false');
// $pdf->text($table_column+35,$item_table_head_y_start-22,$from_date."  to  ".$to_date);

$table_column+=70+$igst_x_position;
$pdf->SetFont('Arial', 'B', 10, "", 'false');

$table_column+=16;
// $pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+35,$item_table_head_y_start-22,'Debit');
$pdf->SetFont('Arial', 'B', 10, "", 'false');
// $pdf->text($table_column+55,$item_table_head_y_start-22,$from_date."  to  ".$to_date);
$pdf->text($table_column+96,$item_table_head_y_start-22,"Credit");

$pdf->SetFont('Arial', '', 10, "", 'false');


}

function footer($pdf,$data,$accounts_array)
{
	$footer_start_y=218;
 // $pdf->Line(1, $footer_start_y+60,209, $footer_start_y+60); // horizontal line


}
$page=0;


foreach($print_field_array as $field_key=>$field_value)
{
//$page=1;
	$page++;
	header_section($pdf,$accounts_array,$Profile,$total_page,$page,$igst_x_position,$field_value,$from_date,$to_date);
	$total=0;
	$total_cgst_tax=0;
	$total_sgst_tax=0;
	$total_igst_tax=0;
	$item_table_head_y_start='85';
	$table_length='128';
	$cgst_grand_amount=0;
	$sgst_grand_amount=0;
	$igst_grand_amount=0;
	$rate_grand_amount=0;
	$discount_grand_amount=0;
	$taxable_value_grand_amount=0;
	$product_grand_total=0;
	$count=count($accounts_array);
	$i=0;
// for ($i=0; $i <$count ; $i++) { 
	$data=array(
		'total'=>$total,
		'total_cgst_tax'=>$total_cgst_tax,
		'total_sgst_tax'=>$total_sgst_tax,
		'total_igst_tax'=>$total_igst_tax,
		);

 // pr($accounts_array);
 // exit;
	$total_debit=0;
	$total_credit=0;
	$pdf_row=0;
	$pdf_rows=0;
	foreach ($accounts_array as $keySI => $value) {

		$k=0;

		if($k)
			$k--;
		$item_pos=0;
		$item_pos+=1;
		
		$pdf->SetFont('Arial', '', 10, "", 'false');
	
		$item_pos+=5;
	// 	if(in_array($value[0], ['Opening Stock','Net Purchase','Indirect Expense','Direct Expense','Gross Profit c/d','Gross Loss b/d','Net Profit c/d'])){
	// 	$pdf->SetFont('Arial', 'B', 10, "", 'false');
	// 	$pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),($value[0]));
	// }else if(in_array($value[0], ['Totals'])){
	// 	$totals='Total';
	// 	$pdf->SetFont('Arial', 'B', 10, "", 'false');
	// 	$pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),($totals));
	// }
	// else if(in_array($value[0], ['Debit Note'])){
	// 	// $totals='Total';
	// 	$pdf->SetFont('Arial', 'I', 10, "", 'false');
	// 	$pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),($value[0]));
	//    $pdf->Line(50, $item_table_head_y_start+8+($i*8-19),78, $item_table_head_y_start+8+($i*8-19)); // horizontal line
	// }
	// else if(in_array($value[0], ['Total'])){
	// 	// $totals='Total';
	// 	$pdf->SetFont('Arial', 'B', 10, "", 'false');
	// 	$pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),($value[0]));
	//    $pdf->Line(80, $item_table_head_y_start+8+($i*8-17),105, $item_table_head_y_start+8+($i*8-17)); // horizontal line
	// }
	// else if(in_array($value[0], ['Purchase','Purchase Return'])){
	// 	$pdf->SetFont('Arial', 'I', 10, "", 'false');
	// 	$pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),($value[0]));
	// }
	// else{
		$pdf->SetFont('Arial', 'B', 11, "", 'false');
		// $pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),$value['name']);
		$pdf->text(8,$item_table_head_y_start+9+($pdf_row*8)-20,$value['name']);
		 $pdf->SetX(0);
	     $pdf->SetY($item_table_head_y_start+9+($pdf_row*8)-20);
	     if($value['amount']>0){
	     	$total_debit+=number_format($value['amount'],2,'.','');

	     $pdf->MultiCell(130,$item_table_head_y_start-87,number_format($value['amount'],2,'.',''),0,'R');
	     }else{
	     	$total_credit+=number_format(abs($value['amount']),2,'.','');
	     	$pdf->MultiCell(192,$item_table_head_y_start-87,number_format(abs($value['amount']),2,'.',''),0,'R');
	     }
	     $pdf->Ln(3);
		$pdf_row++;
	// }
		foreach ($value['Subgroup'] as $keySI1 => $value1) {
// pr($value1['name']);
        $pdf->SetFont('Arial', 'I', 10, "", 'false');
        if($value1['amount']!=0){
         // $pdf->SetY($item_table_head_y_start+8+($i*8-20));
		 // $pdf->text($item_pos+5,$item_table_head_y_start,$value1['name']);
        	$n=strtolower($value1['name']);
		 $pdf->text(14,$item_table_head_y_start-10+($pdf_row*8),ucwords($n));
		 $pdf->SetX(0);
	     $pdf->SetY($item_table_head_y_start-4+($pdf_row*8));
	     if($value1['amount']>0){
	      $pdf->MultiCell(130,$item_table_head_y_start-100,number_format($value1['amount'],2,'.',''),0,'R');
	  }else{
	  	$pdf->MultiCell(192,$item_table_head_y_start-100,number_format(abs($value1['amount']),2,'.',''),0,'R');
	  }
	     $pdf->Ln(3);
		  $pdf_row++;
		   $pdf_rows+=$pdf_row;
		}
         // $pdf->MultiCell(10,$item_table_head_y_start,$value1['name'],0,'C');
		// $pdf->Ln(3);

		}

		
	    // $pdf->text($item_pos+58,$item_table_head_y_start+8+($i*8-20),($value[1]));
	// $pdf->SetX(0);
	// $pdf->SetY($item_table_head_y_start+8+($i*8-20));
	//      $pdf->MultiCell(65,$item_table_head_y_start-87,number_format($value[1],2,'.',''),0,'R');
	//      $pdf->Ln(3);
	//      // $pdf->MultiCell( 0, 10, $value[1], 0, 0, 'R' );
	// 	$pdf->SetFont('Arial', 'I', 10, "", 'false');
	// 	// $pdf->text($item_pos+50,$item_table_head_y_start+8+($i*8-20),($value[2]));
	// 	$pdf->SetX(0);
	//     $pdf->SetY($item_table_head_y_start+8+($i*8-20));
	//      $pdf->MultiCell(65,$item_table_head_y_start-87,number_format($value[2],2,'.',''),0,'R');
	//      $pdf->Ln(3);
	// 	$pdf->SetFont('Arial', 'B', 10, "", 'false');
	// 	// $pdf->text($item_pos+80,$item_table_head_y_start+8+($i*8-20),($value[3]));
	// 	$pdf->SetX(0);
	//     $pdf->SetY($item_table_head_y_start+8+($i*8-20));
	//      $pdf->MultiCell(95,$item_table_head_y_start-87,number_format($value[3],2,'.',''),0,'R');
	//      $pdf->Ln(3);
	// 	if(in_array($value[4], ['Net Sales','Closing Stock','Direct Income','Gross Loss c/d','Gross Profit b/d','Indirect Income','Net Loss c/d'])){
	// 	$pdf->SetFont('Arial', 'B', 10, "", 'false');
	// 	$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($value[4]));
	// }
	// else if(in_array($value[4], ['Totals'])){
	// 	// $table_column=12;
	// 	$totals='Total';
	// 	$pdf->SetFont('Arial', 'B', 10);
	// 	$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($totals));
 //        $pdf->Line(1, $item_table_head_y_start+8+($i*8-20-5),209, $item_table_head_y_start+8+($i*8-20-5)); // horizontal line
	// 	$pdf->Line(1, $item_table_head_y_start+8+($i*8-20+4),209, $item_table_head_y_start+8+($i*8-20+4)); // horizontal line
	// 	 // $pdf->Line($table_column+95, $item_table_head_y_start-30,$table_column+95, $item_table_head_y_start+8+($i*8-20+4)); // vertical line
	// }
	// else if(in_array($value[4], ['Credit Note'])){
	// 	// $totals='Total';
	// 	$pdf->SetFont('Arial', 'I', 10);
	// 	$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($value[4]));
	//    $pdf->Line(150, $item_table_head_y_start+8+($i*8-19),175, $item_table_head_y_start+8+($i*8-19)); // horizontal line
	// }
	// else if(in_array($value[4], ['Total'])){
	// 	// $totals='Total';
	// 	$pdf->SetFont('Arial', 'B', 10);
	// 	$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($value[4]));
	//    $pdf->Line(180, $item_table_head_y_start+8+($i*8-17),205, $item_table_head_y_start+8+($i*8-17)); // horizontal line
	// }
	// else if(in_array($value[4], ['Sale','Sales Return'])){
	// 	$pdf->SetFont('Arial', 'I', 10, "", 'false');
	// 	$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($value[4]));
	// }
	// else{
	// 	$pdf->SetFont('Arial', 'I', 9, "", 'false');
	// 	$pdf->text($item_pos+110,$item_table_head_y_start+8+($i*8-20),($value[4]));
	// }
	   $table_column=12;
		// $pdf->SetFont('Arial', 'B', 10, "", 'false');
		// $pdf->text($item_pos+170,$item_table_head_y_start+8+($i*8-20),($value[5]));
		// $pdf->SetFont('Arial', 'I', 10, "", 'false');
		// // $pdf->text($item_pos+150,$item_table_head_y_start+8+($i*8-20),($value[6]));
		// $pdf->SetX(0);
	 //    $pdf->SetY($item_table_head_y_start+8+($i*8-20));
	 //     $pdf->MultiCell(165,$item_table_head_y_start-87,number_format($value[6],2,'.',''),0,'R');
	 //     $pdf->Ln(3);
		// $pdf->SetFont('Arial', 'B', 10, "", 'false');
		// // $pdf->text($item_pos+180,$item_table_head_y_start+8+($i*8-20),($value[7]));
		// $pdf->SetX(0);
	 //    $pdf->SetY($item_table_head_y_start+8+($i*8-20));
	 //     $pdf->MultiCell(195,$item_table_head_y_start-87,number_format($value[7],2,'.',''),0,'R');
	 //     $pdf->Ln(3);
	//$pdf->Line($table_column+70, $item_table_head_y_start-30,$table_column+70, $item_table_head_y_start+9+($pdf_row*8)+13); // vertical line
	 // $pdf->Line($table_column+70, $item_table_head_y_start-30,$table_column+70, $item_table_head_y_start+9+($pdf_row*8)+3); // vertical line
		// $pdf->Line($table_column+135, $item_table_head_y_start-30,$table_column+135, $item_table_head_y_start+9+($pdf_row*8)+3); 
	    $pdf->Line($table_column+70, $item_table_head_y_start-30,$table_column+70, $item_table_head_y_start+9+(19*8)+3); // vertical line
		$pdf->Line($table_column+135, $item_table_head_y_start-30,$table_column+135, $item_table_head_y_start+9+(19*8)+3); 
		// $pdf->Line($table_column+135, $item_table_head_y_start-30,$table_column+135, $item_table_head_y_start+9+($pdf_row*8)+13); // vertical line
		// if(count($SaleItem)==$keySI+1)
// 		{

// //$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$igst_grand_amount);
// //$pdf->text($item_pos,$item_table_head_y_start+$table_length,number_format($product_grand_total,2));
// 		}
		$i+=$k;
		$count+=$k;
		if($i+$k>=10)
		{
//footer($pdf,$data,$Sale,$route_mobile_no);
			 $page++;
			header_section($pdf,$accounts_array,$Profile,$total_page,$page,$igst_x_position,$field_value,$from_date,$to_date);
			// $i-=25;
			// $footer_start_y=218;
           // $pdf->Line(1, ($i+$k),209, ($i+$k)); // horizontal line
		   // $pdf->Line($table_column+95, $item_table_head_y_start-30,$table_column+95, $item_table_head_y_start+8+($i*8-20+4));
		  // $count-2;
		    $i=-1;
			// $count-=23;
		}
		$i++;
	}
	 $balance=$total_debit-$total_credit;
	 $balance=abs($balance);
	//$balance=0;
		// if($total_debit-$total_credit){
		// 		$balance=$total_left-$total_right;
		// 		$total_right=$balance+$total_right;
	 $pdf->Line(1, $item_table_head_y_start+154,209, $item_table_head_y_start+154);// horizontal
		      $pdf->Line(1, $item_table_head_y_start+164,209, $item_table_head_y_start+164);//horizontal
		      	$pdf->SetFont('Arial', 'B', 11);
			$pdf->text(8,$item_table_head_y_start+160,'Total ');
			// $pdf->MultiCell(192,$item_table_head_y_start-100,number_format(abs($),2,'.',''),0,'R');
			$pdf->SetX(0);
	        $pdf->SetY($item_table_head_y_start+160);
	        $pdf->MultiCell(130,$item_table_head_y_start-85,number_format($total_debit,2,'.',''),0,'R');
	        $pdf->Ln(0);
	        $pdf->SetX(0);
	        $pdf->SetY($item_table_head_y_start+160);
	        $pdf->MultiCell(192,$item_table_head_y_start-85,number_format($total_credit,2,'.',''),0,'R');
	        $pdf->Ln(0);

				if($balance>0){
					 $pdf->Line($table_column+70, $item_table_head_y_start-30,$table_column+70, $item_table_head_y_start+9+(19*8)+13); // vertical line
		             $pdf->Line($table_column+135, $item_table_head_y_start-30,$table_column+135, $item_table_head_y_start+9+(19*8)+13);
		     // $pdf->Line(1, $item_table_head_y_start+154,209, $item_table_head_y_start+154);
		     //  $pdf->Line(1, $item_table_head_y_start+164,209, $item_table_head_y_start+164);
		       $pdf->Line(1, $item_table_head_y_start+174,209, $item_table_head_y_start+174);
				$pdf->SetFont('Arial', 'B', 11);
			$pdf->text(8,$item_table_head_y_start+170,'Difference ');
			// $pdf->MultiCell(192,$item_table_head_y_start-100,number_format(abs($),2,'.',''),0,'R');
			if($total_debit>$total_credit){
			$pdf->SetX(0);
	        $pdf->SetY($item_table_head_y_start+170);
	        $pdf->MultiCell(192,$item_table_head_y_start-85,number_format($balance,2,'.',''),0,'R');
	        $pdf->Ln(0);
	       }else{
	       	$pdf->SetX(0);
	        $pdf->SetY($item_table_head_y_start+170);
	        $pdf->MultiCell(130,$item_table_head_y_start-85,number_format($balance,2,'.',''),0,'R');
	        $pdf->Ln(0);
	       }
	    }
	  //   else if($balance<0){
			// 		 $pdf->Line($table_column+70, $item_table_head_y_start-30,$table_column+70, $item_table_head_y_start+9+(19*8)+13); // vertical line
		 //             $pdf->Line($table_column+135, $item_table_head_y_start-30,$table_column+135, $item_table_head_y_start+9+(19*8)+13);
		 //     // $pdf->Line(1, $item_table_head_y_start+154,209, $item_table_head_y_start+154);
		 //     //  $pdf->Line(1, $item_table_head_y_start+164,209, $item_table_head_y_start+164);
		 //       $pdf->Line(1, $item_table_head_y_start+174,209, $item_table_head_y_start+174);
			// 	$pdf->SetFont('Arial', 'B', 11);
			// $pdf->text(8,$item_table_head_y_start+170,'Difference ');
			// // $pdf->MultiCell(192,$item_table_head_y_start-100,number_format(abs($),2,'.',''),0,'R');
			// if($total_debit>$total_credit){
			// $pdf->SetX(0);
	  //       $pdf->SetY($item_table_head_y_start+170);
	  //       $pdf->MultiCell(192,$item_table_head_y_start-85,number_format($balance,2,'.',''),0,'R');
	  //       $pdf->Ln(0);
	  //      }else{
	  //      	$pdf->SetX(0);
	  //       $pdf->SetY($item_table_head_y_start+170);
	  //       $pdf->MultiCell(130,$item_table_head_y_start-85,number_format($balance,2,'.',''),0,'R');
	  //       $pdf->Ln(0);
	  //      }
	  //   }

			// }else{
			// 	$balance=$total_right-$total_left;
			// 	$total_left=$balance+$total_left;
			//   if($balance>0){
			//   	$pdf->Line(7, $gst_pos+133,107, $gst_pos+133);
			// 	$pdf->SetFont('Arial', 'B', 10);
			// $pdf->text(10,$gst_pos+138,'Difference ');
			// $pdf->SetX(0);
	  //       $pdf->SetY($gst_pos+120);
	  //       $pdf->MultiCell($gst_line_width+69,$gst_pos-15,number_format($balance,2,'.',''),0,'R');
	  //       $pdf->Ln(0);
	  //   }
			// }

	footer($pdf,$data,$accounts_array);
}
		
		$pdf->Output();
		exit;
	}
	public function trial_balance_main_print($from_date,$to_date,$branch_id=null) 
	{
		// $this->response->download("reports_profit_loss.csv");
		$conditions=array();
		$from=date("Y-m-d", strtotime($from_date));
		$to  =date("Y-m-d", strtotime($to_date));
		$from_date=date("d-M-Y", strtotime($from_date));
		$to_date  =date("d-M-Y", strtotime($to_date));
		// $list_array=array();
		// $list_array=$this->trial_balance_report($from,$to);
		$accounts_array=array();
		$accounts_array=$this->trial_balance_report($from,$to,$branch_id);
		// if($branch_id) $branch=$branch_id;
		// else $branch=4;
		// $Address=$this->Branch->find('first',array(
		// 	'conditions'=>array('Branch.id'=>$branch),
		// 	'fields'=>array('Branch.*')
		// ));
		$Address="";
		$print_field_array=[];
		$print_field_array[0]="";
		require('fpdf/fpdf.php');
		$pdf = new FPDF('p', 'mm', [297, 210]);

		$Profile=$this->Global_Var_Profile['Profile'];
		$page=1;
		$total_page=1;
		$i=0;
		$igst_x_position=0;
		function header_section($pdf,$accounts_array,$Profile,$total_page,$page,$igst_x_position,$field_value,$from_date,$to_date,$branch_id,$Address) 
		{
			$pdf->AddPage();
			$image_line_width=40;
			$invoice_x_starting=$image_line_width;
			$pdf->rect(1, 1, 208, 295);
			$invoice_pos=0;
			$invoice_pos+=3;
			$pdf->SetFont('Arial', 'B', 10, "", 'false');
			$invoice_pos+=2;
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos);
			$pdf->MultiCell(0, $invoice_pos+3,$Profile['company_name'], 0, 'C');
			$pdf->Ln(0);
			$pdf->SetXY($invoice_x_starting, 5);
			$invoice_pos+=5;
			$pdf->SetFont('Arial', '', 10, "", 'false'); 
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos);
			$pdf->MultiCell(0, $invoice_pos,$Profile['address_line_1'], 0, 'C');
			$pdf->Ln(0);
			$pdf->SetXY($invoice_x_starting, 5);
			$invoice_pos+=6;
			$pdf->text(165,33,$field_value);
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos);
			$pdf->MultiCell(0, $invoice_pos-7,$Profile['address_line_2'].' ,'.$Profile['mobile'], 0, 'C');
			$pdf->Ln(0);
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos);
			$pdf->MultiCell(0, $invoice_pos+4,$Profile['mail_address'], 0, 'C');
			$pdf->Ln(0);
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos);
			$pdf->MultiCell(0, $invoice_pos+16,'GST Number : '.$Profile['vat_code'], 0, 'C');
			$pdf->Ln(0);
			$pdf->SetXY($invoice_x_starting, 5);
			$invoice_pos+=6;
			$pdf->SetFont('Arial', 'B', 12, "", 'false');
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos);
			$pdf->MultiCell(0, $invoice_pos+16,'Trial Balance', 0, 'C');
			$pdf->Ln(0);
			$invoice_pos+=8;
			$pdf->SetFont('Arial', '', 10, "", 'false');
			$pdf->SetX(0);
			$pdf->SetY($invoice_pos);
			$pdf->MultiCell(0, $invoice_pos+5,$from_date."  to  ".$to_date, 0, 'C');
			$pdf->Ln(0);
			$gst_details_y_staring=40;
			$pdf->Line(1, $gst_details_y_staring+15,209, $gst_details_y_staring+15); // horizontal line
			$pdf->Line(1, $gst_details_y_staring+25,209, $gst_details_y_staring+25); // horizontal line
			$pdf->Line(62, $gst_details_y_staring-3,150, $gst_details_y_staring-3); // horizontal line
			$pdf->SetFont('Arial', '', 10, "", 'false');
			$gst_pos=$gst_details_y_staring+2;
			$gst_line_width=120;
			$gst_pos+=28;
			$gst_pos+=7;
			$pdf->SetXY(0, 5);
			$gst_pos+=3;
			$gst_line_width=100;
			$customer_detail_startin=$gst_details_y_staring+20;
			$Consignee_lin_width=100+1;
			$pdf->SetFont('Arial', '', 10, "", 'false');
			$customer_detail_startin+=7;
			$pdf->SetFont('Arial', '', 10, "", 'false');
			$customer_detail_startin+=4;
			$customer_detail_startin+=4;
			$customer_detail_startin+=1;
			$pdf->SetFont('Arial', '', 10, "", 'false');
			$item_table_head_y_start=$customer_detail_startin+7;
			//  table head start
			$table_length=135;

			$table_column=9;
			$pdf->SetFont('Arial', 'B', 10, "", 'false');
			$pdf->text($table_column,$item_table_head_y_start-22,'Particulars');
			$pdf->SetFont('Arial', '', 9, "", 'false');
			$table_column+=70+$igst_x_position;
			$pdf->SetFont('Arial', 'B', 10, "", 'false');

			$table_column+=16;
			$pdf->text($table_column+35,$item_table_head_y_start-22,'Debit');
			$pdf->SetFont('Arial', 'B', 10, "", 'false');
			$pdf->text($table_column+96,$item_table_head_y_start-22,"Credit");

			$pdf->SetFont('Arial', '', 10, "", 'false');

		}

		function footer($pdf,$data,$accounts_array)
		{
			$footer_start_y=218;
		// $pdf->Line(1, $footer_start_y+60,209, $footer_start_y+60); // horizontal line


		}
		$page=0;


		foreach($print_field_array as $field_key=>$field_value)
		{

			$page++;
			header_section($pdf,$accounts_array,$Profile,$total_page,$page,$igst_x_position,$field_value,$from_date,$to_date,$branch_id,$Address);
			$total=0;
			$item_table_head_y_start='85';
			$table_length='128';
			$count=count($accounts_array);
			$i=0;
			$data=array(
				'total'=>$total,
				
				);

		
			$total_debit=0;
			$total_credit=0;
			$pdf_row=0;
			$pdf_rows=0;
			foreach ($accounts_array as $keySI => $value) {

				$k=0;

				if($k)
					$k--;
				$item_pos=0;
				$item_pos+=1;
				
				$pdf->SetFont('Arial', '', 10, "", 'false');
		
				$item_pos+=5;
		
				$pdf->SetFont('Arial', 'B', 11, "", 'false');
				// $pdf->text($item_pos+2,$item_table_head_y_start+8+($i*8-20),$value['name']);
				$pdf->text(8,$item_table_head_y_start+9+($pdf_row*8)-20,$value['name']);
				$pdf->SetX(0);
				$pdf->SetY($item_table_head_y_start+9+($pdf_row*8)-20);
				if($value['amount']>0){
					$total_debit+=number_format($value['amount'],2,'.','');

				$pdf->MultiCell(130,$item_table_head_y_start-87,number_format($value['amount'],2,'.',''),0,'R');
				}else{
					$total_credit+=number_format(abs($value['amount']),2,'.','');
					$pdf->MultiCell(192,$item_table_head_y_start-87,number_format(abs($value['amount']),2,'.',''),0,'R');
				}
				$pdf->Ln(3);
				$pdf_row++;
				$table_column=12;
				$pdf->Line($table_column+70, $item_table_head_y_start-30,$table_column+70, $item_table_head_y_start+9+($pdf_row*8)-5); // vertical line
				$pdf->Line($table_column+135, $item_table_head_y_start-30,$table_column+135, $item_table_head_y_start+9+($pdf_row*8-5)); 
		
				$i+=$k;
				$count+=$k;
				if($i+$k>=10)
				{
					//footer($pdf,$data,$Sale,$route_mobile_no);
					$page++;
					header_section($pdf,$accounts_array,$Profile,$total_page,$page,$igst_x_position,$field_value,$from_date,$to_date,$branch_id,$Address);
					$i=-1;
					// $count-=23;
				}
				$i++;
			}
			$balance=$total_debit-$total_credit;
			$balance=abs($balance);
			$pdf->Line(1, $item_table_head_y_start+34,209, $item_table_head_y_start+34);
		    $pdf->Line(1, $item_table_head_y_start+44,209, $item_table_head_y_start+44);
		    $pdf->SetFont('Arial', 'B', 11);
			$pdf->text(8,$item_table_head_y_start+40,'Total ');
			// $pdf->MultiCell(192,$item_table_head_y_start-100,number_format(abs($),2,'.',''),0,'R');
			$pdf->SetX(0);
	        $pdf->SetY($item_table_head_y_start+39);
	        $pdf->MultiCell(130,$item_table_head_y_start-85,number_format($total_debit,2,'.',''),0,'R');
	        $pdf->Ln(0);
	        $pdf->SetX(0);
	        $pdf->SetY($item_table_head_y_start+39);
	        $pdf->MultiCell(192,$item_table_head_y_start-85,number_format($total_credit,2,'.',''),0,'R');
	        $pdf->Ln(0);

			if($balance>0){
				$pdf->Line($table_column+70, $item_table_head_y_start-30,$table_column+70, $item_table_head_y_start+9+($pdf_row*8)+5); // vertical line
				$pdf->Line($table_column+135, $item_table_head_y_start-30,$table_column+135, $item_table_head_y_start+9+($pdf_row*8)+5);
		     
		       	$pdf->Line(1, $item_table_head_y_start+54,209, $item_table_head_y_start+54);
				$pdf->SetFont('Arial', 'B', 11);
				$pdf->text(8,$item_table_head_y_start+50,'Difference ');
				// $pdf->MultiCell(192,$item_table_head_y_start-100,number_format(abs($),2,'.',''),0,'R');
				if($total_debit>$total_credit){
					$pdf->SetX(0);
					$pdf->SetY($item_table_head_y_start+50);
					$pdf->MultiCell(192,$item_table_head_y_start-85,number_format($balance,2,'.',''),0,'R');
					$pdf->Ln(0);
				}else{
					$pdf->SetX(0);
					$pdf->SetY($item_table_head_y_start+50);
					$pdf->MultiCell(130,$item_table_head_y_start-85,number_format($balance,2,'.',''),0,'R');
					$pdf->Ln(0);
				}
			}

			footer($pdf,$data,$accounts_array);
		}
		$filename='Trial Balance Main - '.$from_date.' To '.$to_date.'.pdf';
		$pdf->Output('I',$filename);
		exit;
	}
	
}
