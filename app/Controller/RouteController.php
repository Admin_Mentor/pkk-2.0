<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
App::import('Controller', 'Accounts');
class RouteController extends AppController {
	public $uses=array(
		'Customer',
		'Route',
		'ExecutiveRouteMapping',
		'AccLedger'
		);
	public function add_customer_type_ajax()
	{
		try {
			$name=strtoupper(trim($this->request->data['name']));
                        $NameArray = str_split($name);

            $cnt = count($NameArray);
            $i = 0;
            while ($i <= $cnt) {
                if($i==0)
                {
                     $code = strtoupper(trim($NameArray[$i]));
                }
                else{
                $code = $code . strtoupper(trim($NameArray[$i]));
                }
                $RowCount = $this->CustomerType->find('count', array('conditions' => array('CustomerType.code' => $code)));
               
                if ($RowCount < 1) {
                    $i = $cnt;
                }
                $i++;
            }
            
			$data=array(
				'name'=>$name,
                                'code'=>$code,
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s'),
				);
			$this->CustomerType->create();
			if(!$this->CustomerType->save($data))
				throw new Exception("CustomerType Creation Error", 1);
			$last_id=$this->CustomerType->getLastInsertId();
			$CustomerType=$this->CustomerType->findById($last_id);
			$return['key']=$CustomerType['CustomerType']['id'];
			$return['value']=$CustomerType['CustomerType']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	
	public function index()
	{
		$Route = $this->Route->find('list', array('fields' => array('id','name')));
		$this->set('Route',$Route);
		$Routes = $this->Route->find('all', array('order'=>array('Route.id DESC'),'fields' => array('id','name','code','account_head_id','mobile')));
		$this->set('Routes',$Routes);
	}
	public function delete_by_ajax($id)
	{
		$Customer=$this->Customer->findByRouteId($id);
		if(empty($Customer))
		{
			if($this->Route->delete($id))
			{
				$this->ExecutiveRouteMapping->deleteAll(array('ExecutiveRouteMapping.route_id' => $id));

				// $this->ExecutiveRouteMapping->route_id=$id;
				// $this->ExecutiveRouteMapping->delete();
				$return['result']='Success';
			}
		}
		else
		{
			$return['result']='It Is Used';
		}
		echo json_encode($return);
		exit;
	}
	public function edit_ajax(){
		try {
			$id=$this->request->data['id'];
			$mobile=$this->request->data['mobile'];
			// $discount_percentage=$this->request->data['discount_percentage'];
			if(empty($id))
				throw new Exception("Empty Route Id");
			$name=strtoupper(trim($this->request->data['name']));
			if(empty($name))
				throw new Exception("Empty Route Name");
			$code=strtoupper(trim($this->request->data['code']));
			if(empty($code))
				throw new Exception("Empty Code");
			if(empty($mobile))
				throw new Exception("Empty Mobile");
			$tableData = [
				'name'=>$name,
				'mobile'=>$mobile
			];
			$this->Route->id=$id;
			if(!$this->Route->save($tableData))
				throw new Exception("Error In Route Request");
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();			
		}
		echo json_encode($return);
		exit;
	}
	public function add_route_ajax() {

        $return = ['result' => 'Empty'];

        try {
            $data = $this->request->data;
            $name = $data['modal_location_name'];
            $code = 'R'.$data['modal_location_codes'];
            $mobile = $data['modal_location_mobile'];
             //$cashname =  data.Staff.name+data.Staff.code+'_CASH';
            $cashname=$name.'_CASH';
                $sub_group_id =5;
                $main_group_id =1;
			$name1 = $cashname;
			$opening_balance = 0;
			$date = date('Y-m-d');
			$description = "";
          if (empty($data['modal_location_name']))
                throw new Exception("Empty Route", 1);
            if ($data['modal_location_codes'] == '')
                throw new Exception("Empty Route Code", 1);
          
           if ($mobile == '')
                throw new Exception("Empty Route mobile", 1);
 $AccountsController = new AccountsController;
			$function_return = $AccountsController->AccountHeadCreate($main_group_id,$sub_group_id, $name1, 0, $date, '');
			 if ($function_return['result'] != 'Success')
                throw new Exception($function_return['message']);
        $CashAccountHead_id = $this->AccountHead->getLastInsertId();
          $LedgerRow = $this->AccLedger->find('first', array(
                'order' => array('AccLedger.id' => 'DESC') ));
            if(!empty($LedgerRow))
            {
                $ledger_code = $LedgerRow['AccLedger']['code']+1;
            }
            else{
                $ledger_code = 1001;
            }
			$Ledger_data = [
            'name'=>$name1,
            'code'=>$ledger_code,
            'account_head_id' => $CashAccountHead_id,
            'sub_group_id' => 5,
            'main_group_id'=>1,
            'opening_balance' => 0,
            'other_notes' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ];
            $this->AccLedger->create();
            if (!$this->AccLedger->save($Ledger_data)){
                throw new Exception("Error Ledger Creation", 1);
            }
            $route_date = array(
                'name' => strtoupper($name),
                'code' => strtoupper($code),
                'mobile' => $mobile,
               'account_head_id'=>$CashAccountHead_id,

            );

            $this->Route->create();
            if (!$this->Route->save($route_date))
                throw new Exception("Error In Route Create", 1);
            $return['result'] = 'Success';
            $Route_id = $this->Route->getLastInsertId();
            $Route = $this->Route->findById($Route_id);
          
        } catch (Exception $e) {
            $return['result'] = $e->getMessage();
        }
       echo json_encode($return);
        exit;
    }
    public function route_search()
	{
		$code=$this->request->data['code'];
		$code=trim($code);
		$Route=$this->Route->find('first',array('conditions'=>array('Route.code'=>$code)));
		if(!empty($Route)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}	
}
