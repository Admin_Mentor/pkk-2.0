<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
App::import('Controller', 'Purchase');
App::import('Controller', 'Stock');
ini_set('memory_limit', '1024M');
class SaleController extends AppController
{
  public $uses = array(
    'ProductType',
    'Unit',
    'Product',
    'Stock',
    'Customer',
    'CustomerType',
    'StockLog',
    'Sale',
    'SaleItem',
    'SalesReturn',
    'SalesReturnItem',
    'Brand',
    'Journal',
    'State',
    'Profile',
    'Executive',
    'Warehouse',
    'Location',
    'Bill',
    'Route',
    'CustomerGroup',
    'Division',
    'DayRegister',
    'ClosingDay',
    'CustomerSetting',
    'Branch',
    'CreditNote',
    'CreditDetail',
    'DebitNote',
    'DebitDetail',
    'ProductBonusDetail',
    'ExecutiveBonusDetail',
    'ExecutiveBonus',
    'Production',
    'ExecutiveBonusCalculation',
    'ExecutiveRouteMapping',
     'ProductWiseOffer',
    'ProductWiseOffersItem',
    'CustomerPrice'
    );
  public function SaleIndex()
  {
    $Sale = $this->Sale->find('all', array(
      'order' => array('Sale.id DESC'),
      'fields' => array(
        'AccountHead.name',
        'Sale.date_of_order',
        'Sale.invoice_no',
        'Sale.grand_total',
        'Sale.status',
        'Sale.id',
        )
      ));
    $this->set('Sale', $Sale);
  }
  public function InvoiceList()
  {
    $PermissionList = $this->Session->read('PermissionList');
    $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Sale/InvoiceList'));
    $from_date = date('Y-m-d',strtotime('first day of this month'));
    $to_date = date('Y-m-d');
    // if(!in_array($menu_id, $PermissionList))
    // {
    //   $this->Session->setFlash("Permission denied");
    //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
    // }
    $this->request->data['from_date']=date('d-m-Y',strtotime('first day of this month'));
    $this->request->data['to_date']=date('d-m-Y');
    $branchs=$this->Branch->find('list',array(
      'fields'=>['Branch.id','Branch.name']
      ));
    $this->Executive->unbindModel(array('hasMany' => array('Sale')));
     $executives=$this->Executive->find('list',array(
      'fields'=>['Executive.id','Executive.name']
    ));
    $this->set(compact('executives'));
    $this->set(compact('branchs'));
    $type='Invoice';
   // $Sale=$this->SaleList_function($type,$from_date,$to_date);
   // $this->set(compact('Sale'));
  }
  public function QuotationList()
  {
    $PermissionList = $this->Session->read('PermissionList');
    $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Sale/QuotationList'));
    $from_date = date('Y-m-d',strtotime('first day of this month'));
    $to_date = date('Y-m-d');
    // if(!in_array($menu_id, $PermissionList))
    // {
    //   $this->Session->setFlash("Permission denied");
    //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
    // }
    $branchs=$this->Branch->find('list',array(
      'fields'=>['Branch.id','Branch.name']
      ));
    $this->set(compact('branchs'));
    $type='Quotation';
    $Sale=$this->SaleList_function($type,$from_date,$to_date);
    $this->set(compact('Sale'));
  }
  public function SaleList_function($type,$from_date,$to_date,$branch_id=null)
  {
    $conditions=[];
    if($type=='Invoice')
    {
      $conditions['Sale.status =']=2;
    }
    else
    {
      $conditions['Sale.status !=']=2;
    }
    if(!empty($branch_id))
    {
     
    }

    $user_branch_id=$this->Session->read('User.branch_id');
    if($user_branch_id)
    {
      
    }
    $from_date = date('Y-m-d',strtotime($from_date));
    $to_date = date('Y-m-d',strtotime($to_date));
    $conditions['Sale.date_of_order between ? and ?']=array($from_date,$to_date);
    $Sale = $this->Sale->find('all', array(
      "joins" => array(
        array(
          "table" => 'customers',
          "alias" => 'Customer',
          "type" => 'inner',
          "conditions" => array('Customer.account_head_id=Sale.account_head_id'),
          ),
        array(
          "table" => 'routes',
          "alias" => 'Route',
          "type" => 'left',
          "conditions" => array('Customer.route_id=Route.id'),
          ),
        ),
      'conditions' => $conditions,
      'order' => array('Sale.id DESC'),
      'fields' => array(
        'Sale.date_of_order',
        'Sale.invoice_no',
        'Sale.status',
        'Sale.branch_id',
        'Sale.grand_total',
        'Sale.id',
        'AccountHead.name',
        'Executive.name',
        'Customer.code',
        'Route.name',
        'Route.id',
        )
      ));
//pr($Sale);
    return $Sale;
    
  }
  public function invoice_search_ajax()
  {
    $data=$this->request->data;
    $branch_id=$data['branch_id'];
    $first_date = date('Y-m-d',strtotime('first day of this month'));
    $to_date = date('Y-m-d',strtotime('last day of this month'));
    if(!empty($data['from_date'])){
      $first_date = date('Y-m-d',strtotime($data['from_date']));
    }
    if(!empty($data['to_date'])){
      $to_date = date('Y-m-d',strtotime($data['to_date']));
    }

    $type='Invoice';
    $Sale=$this->SaleList_function($type,$first_date,$to_date,$branch_id);
    echo json_encode($Sale);
    exit;
  }
  public function quotation_search_ajax()
  {
    $data=$this->request->data;
    $branch_id=$data['branch_id'];
    $from_date = date('Y-m-d',strtotime('first day of this month'));
    $to_date = date('Y-m-d',strtotime('last day of this month'));
    if(!empty($data['from_date'])){
      $from_date = date('Y-m-d',strtotime($data['from_date']));
    }
    if(!empty($data['to_date'])){
      $to_date = date('Y-m-d',strtotime($data['to_date']));
    }
    $type='Quotation';
    $Sale=$this->SaleList_function($type,$from_date,$to_date,$branch_id);
    echo json_encode($Sale);
    exit;
  }
  public function sale($id=null,$state=null)
  {
    $user_branch_id=$this->Session->read('User.branch_id');
    $PermissionList = $this->Session->read('PermissionList');
    $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Sale/sale'));
    // if(!in_array($menu_id, $PermissionList))
    // {
    //   $this->Session->setFlash("Permission denied");
    //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
    // }
    $show_cost = $this->Session->read('UserRole.show_cost');
    $sub_group_id=3;
    $user_id=1;
    $sale_account_head_id=6;
    //$tax_on_sale_account_head_id=9;
    $tax_on_sale_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DUTIES & TAXES'));
    $discount_received_account_head_id=12;
    $discount_paid_account_head_id=13;
    $stock_account_head_id=19;
    $roundoff_received_account_head_id=14;
    $roundoff_paid_account_head_id=15;
    $this->CustomerType->virtualFields = array('customer_type_name' => "CONCAT(CustomerType.name, ' ', CustomerType.code)");
    $CustomerType_list=$this->CustomerType->find('list',array('order'=>['CustomerType.id Asc'],'fields'=>['id','CustomerType.customer_type_name']));
    $CustomerType_model = $CustomerType_list;
    unset($CustomerType_model[1]);
    $Location_list=$this->Location->find('list');
    $this->Product->virtualFields = array('product_name' => "CONCAT(Product.name, ' ', Product.code)");
    $Product_list=$this->Product->find('list',array('conditions'=>array('Product.active=1','type ='=>[2,8]),'fields'=>['id','product_name']));
    $Unit=$this->Unit->find('list',array('fields'=>['id','name']));
    $Unit[0]='Select';
    ksort($Unit);
    $Warehouse_list=$this->Warehouse->find('list',array('order'=>['Warehouse.id Asc'],'conditions'=>['Warehouse.id'=>1],'fields'=>['id','name']));
    $Bill_list=$this->Bill->find('list',array('fields'=>['id','name']));
    $this->Executive->virtualFields = array('executive_name' => "CONCAT(Executive.name, ' ', Executive.username)");
    $Executive_list=$this->Executive->find('list',array('fields'=>['id','executive_name']));
    $Division_list=$this->Division->find('list');
    $this->Route->virtualFields = array('route_name' => "CONCAT(Route.name, ' ', Route.code)");
    $Route_list=$this->Route->find('list',array('fields'=>array('id','route_name')));
    $CustomerGroup_list=$this->CustomerGroup->find('list',array('fields'=>array('id','name')));
    $CustomerSettingList=$this->CustomerSetting->find('list',array('fields'=>array('function','value'),'conditions'=>array('module'=>'Sale')));
    $Sale_count=$this->Sale->find('count',array('order' => 'Sale.id DESC'));
    $this->set(compact('CustomerSettingList','show_cost','CustomerType_list','CustomerType_model','Location_list','Product_list','Unit','Warehouse_list','Bill_list','Executive_list','Division_list','Route_list','CustomerGroup_list'));
    $invoice_no=1;
    $order_no=1;
    $invoice_no_old=45;
    if(!empty($Sale_count))
    { 
      $Sale=$this->Sale->findByInvoiceNo($Sale_count);
      while($Sale)
      {
        $Sale_count++;
        $Sale=$this->Sale->findByInvoiceNo($Sale_count);
      }
      $invoice_no=$Sale_count;
      $order_no=$Sale_count;
    }
    $Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
    if(!empty($Journals))
    {
      $JournalsNo = $Journals['Journal']['voucher_no'];
      $Journals_no=$JournalsNo+1;
    }
    else
    {
      $Journals_no=1;
    }
    if (!$this->request->data)
    {
      if(empty($id))
      {
        $data['Sale']['invoice_no']=$invoice_no;
        $data['Sale']['order_no']=$order_no;
        $data['Sale']['date']=date('d-m-Y');
        $data['Sale']['status']=1;
        $data['Sale']['type']='WholeSale';
        $data['Sale']['sale_type']='WholeSale';
        $data['Sale']['sale_type1']='CashSale';
        $data['Sale']['type1']='CashSale';
        $data['Sale']['customer_type_id']=0;
        // $Customer_list=$this->Customer->find('all',array(
        //   'conditions'=>['Customer.customer_type_id'=>1],
        //   'fields'=>['AccountHead.id','AccountHead.name','Customer.code'])
        // );
        $Customers_list =[];
        // foreach ($Customer_list as $key => $value) {
        //   $single['id'] = $value['AccountHead']['id'];
        //   $single['customer_name'] = $value['AccountHead']['name'].''.$value['Customer']['code'];
        //   array_push($Customers_list, $single);
        // }
        $this->set('Customer_list',$Customers_list);

        $this->request->data=$data;
        //pr($this->request->data);exit;
      }
      elseif((!empty($Sale)) && empty($Sale['Sale']['account_head_id'])){
        $Sale=$this->Sale->findById($id);
        $Customer=$this->Customer->findByAccountHeadId($Sale['Sale']['account_head_id']);
        $this->request->data=$Sale;
        $this->request->data['Sale']['date']=date('d-m-Y',strtotime($Sale['Sale']['date_of_order']));
        $this->request->data['Sale']['customer_name']=$Customer['Sale']['customer_name'];
        $this->request->data['Sale']['customer_mobile']=$Customer['Sale']['customer_mobile'];
        $this->request->data['Sale']['address']=$Customer['Customer']['place'];
        $this->request->data['Sale']['paid']=$Sale['Sale']['grand_total']-$Sale['Sale']['balance'];
        $this->request->data['Sale']['customer_type_id']=$Customer['Customer']['customer_type_id'];
        $this->request->data['Sale']['account_head_id']=$Sale['Sale']['account_head_id'];
        $this->request->data['Sale']['type1']=$Sale['Sale']['sale_type1'];


        $Customer_list=$this->Customer->find('all',array(
          'conditions'=>['Customer.customer_type_id'=>$Customer['Customer']['customer_type_id']],
          'fields'=>['AccountHead.id','AccountHead.name'])
        );
        $Customers_list =[];
        foreach ($Customer_list as $key => $value) {
          $single['id'] = $value['AccountHead']['id'];
          $single['customer_name'] = $value['AccountHead']['name'].''.$value['Customer']['code'];
          array_push($Customers_list, $single);
        }
        $this->set('Customer_list',$Customers_list);
        $SaleItem=$this->SaleItem->find('all',array(
          "joins" => array(
            array(
              "table" => 'brands',
              "alias" => 'Brand',
              "type" => 'left',
              "conditions" => array('Brand.id=Product.brand_id'),
              ),
            array(
              "table" => 'product_types',
              "alias" => 'ProductType',
              "type" => 'inner',
              "conditions" => array('ProductType.id=Product.product_type_id'),
              ),

            ),
          'conditions'=>array(
            'sale_id'=>$id,

            ),
          'fields'=>array(
            'SaleItem.*',
            'Product.name',
            'Product.code',
            'Product.cost',
            'Product.barcode',
            'Brand.name',
            'ProductType.name',
            'Warehouse.name',
            )
          ));
        foreach ($SaleItem as $key => $value) {
          $product_id=$value['SaleItem']['product_id'];
          $unit_id=$value['SaleItem']['unit_id'];
          $UnitLevelConvert=$this->UnitLevelConvert($product_id,$unit_id);
          $sale_unit_level=$UnitLevelConvert['sale_unit_level'];
          $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
          $product_unit_level=$UnitLevelConvert['product_unit_level'];
          if($sale_unit_level != 1){
            if($sale_unit_level == 2){
              $SaleItem[$key]['SaleItem']['quantity']=$value['SaleItem']['quantity']/$no_of_piece_per_unit;
              $SaleItem[$key]['SaleItem']['free_qty']=$value['SaleItem']['free_qty']/$no_of_piece_per_unit;
              $SaleItem[$key]['SaleItem']['unit_price']=$value['SaleItem']['unit_price']*$no_of_piece_per_unit;
              $SaleItem[$key]['SaleItem']['cost']=$value['Product']['cost']*$no_of_piece_per_unit;
              
            }
          }
          
        }
        $this->set('SaleItem',$SaleItem);

      }
      else
      {
        $Sale=$this->Sale->findById($id);

        $Customer=$this->Customer->findByAccountHeadId($Sale['Sale']['account_head_id']);
        $this->request->data=$Sale;
        $this->request->data['Sale']['date']=date('d-m-Y',strtotime($Sale['Sale']['date_of_order']));
        $this->request->data['Sale']['address']=$Customer['Customer']['place'];
        $this->request->data['Sale']['paid']=$Sale['Sale']['grand_total']-$Sale['Sale']['balance'];
        $this->request->data['Sale']['customer_type_id']=$Customer['Customer']['customer_type_id'];
        $this->request->data['Sale']['credit_limit']=$Customer['Customer']['credit_limit'];
        $this->request->data['Sale']['account_head_id']=$Sale['Sale']['account_head_id'];
        $this->request->data['Sale']['warehouse_id']=$Sale['Sale']['warehouse_id'];
        $this->request->data['Sale']['type1']=$Sale['Sale']['sale_type1'];
        $Customer_list=$this->Customer->find('all',array(
          'conditions'=>['Customer.customer_type_id'=>$Customer['Customer']['customer_type_id']],
          'fields' => array('AccountHead.id', 'AccountHead.name', 'Customer.code')
          ));
        $Customers_list =[];
        foreach ($Customer_list as $key => $value) {
          $single['id'] = $value['AccountHead']['id'];
          $single['customer_name'] = $value['AccountHead']['name'].''.$value['Customer']['code'];
          array_push($Customers_list, $single);
        }
        $this->set('Customer_list',$Customers_list);
        $SaleItem=$this->SaleItem->find('all',array(
          "joins" => array(
            array(
              "table" => 'brands',
              "alias" => 'Brand',
              "type" => 'left',
              "conditions" => array('Brand.id=Product.brand_id'),
              ),
            array(
              "table" => 'product_types',
              "alias" => 'ProductType',
              "type" => 'inner',
              "conditions" => array('ProductType.id=Product.product_type_id'),
              ),
            array(
              "table"=>'stocks',
              "alias"=>'Stock',
              "type"=>'inner',
              "conditions"=>array('SaleItem.product_id=Stock.product_id'),
              ),
            ),
          'conditions'=>array(
            'sale_id'=>$id,
            'Stock.warehouse_id'=>1,
            ),
          'order'=>array('SaleItem.id'),
          'fields'=>array(
            'SaleItem.*',
            'Product.name',
            'Product.no_of_piece_per_unit',
            'Product.barcode',
            'Unit.name',
            'Unit.level',
            'Product.code',
            'Brand.name',
            'ProductType.name',
            'Product.cost',
            'Stock.quantity',
            'Warehouse.name',
            )
          ));
        foreach ($SaleItem as $key => $value) {
          $product_id=$value['SaleItem']['product_id'];
          $unit_id=$value['SaleItem']['unit_id'];
          $UnitLevelConvert=$this->UnitLevelConvert($product_id,$unit_id);
          $sale_unit_level=$UnitLevelConvert['sale_unit_level'];
          $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
          $product_unit_level=$UnitLevelConvert['product_unit_level'];
          if($sale_unit_level != 1){
            if($sale_unit_level == 2){
              $SaleItem[$key]['SaleItem']['quantity']=$value['SaleItem']['quantity']/$no_of_piece_per_unit;
              $SaleItem[$key]['SaleItem']['free_qty']=$value['SaleItem']['free_qty']/$no_of_piece_per_unit;
              $SaleItem[$key]['SaleItem']['unit_price']=$value['SaleItem']['unit_price']*$no_of_piece_per_unit;
              $SaleItem[$key]['SaleItem']['cost']=$value['Product']['cost']*$no_of_piece_per_unit;
              $SaleItem[$key]['Product']['cost']=$value['Product']['cost']*$no_of_piece_per_unit;
              
            }
          }
          
        }
        $this->set('SaleItem',$SaleItem);
        //pr( $this->request->data);exit;
        //pr($SaleItem);exit;
      }
    }
    else
    {
      $datasource_Sale = $this->Sale->getDataSource();
      $datasource_SaleItem = $this->SaleItem->getDataSource();
      $datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
      $datasource_Product = $this->Product->getDataSource();
      $datasource_Stock = $this->Stock->getDataSource();
      $datasource_Journal = $this->Journal->getDataSource();
      $datasource_ExecutiveBonusDetail = $this->ExecutiveBonusCalculation->getDataSource();
      $datasource_ExecutiveBonus = $this->ExecutiveBonus->getDataSource();
       $datasource_CustomerPrice = $this->CustomerPrice->getDataSource();
      try {
        $datasource_Sale->begin();
        $datasource_SaleItem->begin();
        $datasource_ProductBonusDetail->begin();
         $datasource_CustomerPrice->begin();
        $data=$this->request->data;
        if(empty($id))
        {
          $data=$data['Sale'];
          if($data['balance']<0)
          {
            $data['balance']=0;
          }
          if(!empty($data['discount'])){
            $data['discount'] = $data['discount'];
          }
          else{
            $data['discount'] = 0;
          }
          $free_flag = 0;
          foreach ($data['free_qty'] as $key_free_qty => $value_free_qty) {
            if($value_free_qty>0)
            {
              $free_flag = 1;
            }
          }
          $receipt_flag=0;
          if(!empty($data['type1']))
          {

            if($data['type1']=='CashSale')
            {
              $receipt_flag=2;
            }
          }
          if(empty($data['total_tax']))
            $data['total_tax']=5;
          if($data['customer_type_id']==1)
            $data['account_head_id']=3;
          $data['other_name']='Round Off';
          $Sale_invoice=$this->Sale->findByInvoiceNo($data['invoice_no']);
          while($Sale_invoice)
          {
            throw new Exception('Existing Invoice', 1);
          }
          $Sale_data=[
          'account_head_id'=>$data['account_head_id'],
          'executive_id'=>$data['executive_id'],
          'warehouse_id'=>$data['warehouse_id'],
          'customer_name'=>$data['customer_name'],
          'customer_mobile'=>$data['customer_mobile'],
          'sale_type'=>$data['type'],
          'sale_type1'=>$data['type1'],
          'invoice_no'=>$data['invoice_no'],
          'date_of_order'=>date('Y-m-d',strtotime($data['date'])),
          'total'=>$data['taxable_value_total']+$data['tax_amount_total'],
          'branch_id'=>$user_branch_id,
          'other_name'=>$data['other_name'],
          'other_value'=>$data['other_value'],
          'discount'=>$data['discount'],
          'discount_amount'=>$data['discount_amount'],
          'taxable_amount'=>$data['taxable_value_total'],
          'tax'=>$data['total_tax'],
          'tax_amount'=>$data['tax_amount_total'],
          'grand_total'=>$data['grand_total'],
          'balance'=>$data['balance'],
          'receipt_flag'=>$receipt_flag,
          'free_flag'=>$free_flag,
          'created_by'=>$user_id,
          'modified_by'=>$user_id,
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s'),
          ];
          if(isset($data['sale_type']))
          {
            $Sale_data['sale_type']='WholeSale';
          }
          $process=$data['process'];
          if($process=='delivery')
          {
            $Sale_data['date_of_delivered']=date('Y-m-d',strtotime($data['date']));
            $Sale_data['status']=2;
          }
//pr($Sale_data);exit;
          $this->Sale->create();
          if(!$this->Sale->save($Sale_data))
          {
            $errors = $this->Sale->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
          $id=$this->Sale->getLastInsertId();
          $return_function=$this->sale_item_insert($data,$id);
          if($return_function['result']!='Success')
            throw new Exception($return_function['result']);
          $receipt=$data['paid'];
        }
        else
        {
          $data_Sale=$data['Sale'];
          $process=$data_Sale['process'];
          if($process=='altration') { goto altration_process; }
          if($process=='after_altration') { goto after_altration_process; }
          if($process=='cancel') { goto cancel_process; }
          if($process=='delete') { goto delete_process; }
          if(isset($data['SaleItem'])) { $old_SaleItem=$data['SaleItem']; }
          after_altration_process :
          if($process=='after_altration')
          {
            $sale_rollback_function=$this->sale_rollback($id);
            if($sale_rollback_function['result']!='Success')
              throw new Exception($sale_rollback_function['result']);
            $process='delivery';
          }
          if($data_Sale['balance']<0)
            { $data_Sale['balance']=0; }
          if(empty($data_Sale['discount']))
            $data_Sale['discount'] =0;
          $this->Sale->unbindModel(
            array('hasMany' => array(
              'SaleItem',
              ))
            );
          $Sale=$this->Sale->findById($id);
          $free_flag = 0;
          if(!empty($data_Sale['free_qty']))
          {
            // foreach ($data_Sale['free_qty'] as $key_free_qty => $value_free_qty) {
            //   if($value_free_qty>0)
            //   {
            //     $free_flag = 1;
            //   }
            // }
            if($data_Sale['free_qty']>0)
            {
              $free_flag = 1;
            }
          }
          if($Sale['Sale']['free_flag']==1)
          {
            $free_flag = 1;
          }
          $data_Sale['other_name']='Round Off';
// pr($data_Sale);exit;
          if(empty($data_Sale['total_tax']))
            $data_Sale['total_tax']=5;
          if(!empty($data_Sale['discount']))
            { $data_Sale['discount'] = $data_Sale['discount']; }
          $Sale_data=[
          'account_head_id'=>$data_Sale['account_head_id'],
          'warehouse_id'=>$data_Sale['warehouse_id'],
          'customer_name'=>$data_Sale['customer_name'],
          'customer_mobile'=>$data_Sale['customer_mobile'],
          'executive_id'=>$data_Sale['executive_id'],
          'invoice_no'=>''.$data_Sale['invoice_no'].'',
          'sale_type'=>$data_Sale['type'],
          'sale_type1'=>$data_Sale['type1'],
          'total'=>$data_Sale['taxable_value_total']+$data_Sale['tax_amount_total'],
          'branch_id'=>$user_branch_id,
          'other_name'=>$data_Sale['other_name'],
          'other_value'=>$data_Sale['other_value'],
          'discount'=>$data_Sale['discount'],
          'discount_amount'=>$data_Sale['discount_amount'],
          'taxable_amount'=>$data_Sale['taxable_value_total'],
          'tax'=>$data_Sale['total_tax'],
          'tax_amount'=>$data_Sale['tax_amount_total'],
          'grand_total'=>$data_Sale['grand_total'],
          'balance'=>''.$data_Sale['balance'].'',
          'free_flag'=>$free_flag,
          'modified_by'=>$user_id,
          'updated_at'=>date('Y-m-d H:i:s'),
          ];
          if($process=='delivery')
          {
            $Sale_data['date_of_delivered']=date('Y-m-d',strtotime($data_Sale['date']));
            $Sale_data['status']=2;
          }
          $receipt=$data_Sale['paid'];
          $this->Sale->id=$id;
          if(!$this->Sale->save($Sale_data))
          {
            $errors = $this->Sale->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
          if(isset($data_Sale['product_id'])){
            $return_function=$this->sale_item_insert($data_Sale,$id);
            if($return_function['result']!='Success')
              throw new Exception($return_function['result']);
          }
          if(isset($old_SaleItem)){
            $return_function=$this->sale_item_update($old_SaleItem,$id);
            if($return_function['result']!='Success')
              throw new Exception($return_function['result']);
          }
          altration_process :
          if($process=='altration')
          {
            $this->Session->setFlash(__('Altration Activated'));
            $this->Sale->id=$id;
            if(!$this->Sale->saveField('status','3'))
              throw new Exception('Error While Updating', 1);
            $datasource_Sale->commit();
            $datasource_SaleItem->commit();
            $datasource_ProductBonusDetail->commit();
            return $this->redirect(array('controller' => 'Sale', 'action' => 'Sale',$id));
          }
// update_process:
// if($process=='update')
// {
//   $this->Session->setFlash(__('Rpdated'));
//   $this->Sale->id=$id;

//    $free_flag = 0;
// foreach ($Sale_data['free_qty'] as $key_free_qty => $value_free_qty) {
//   if($value_free_qty>0)
//   {
//     $free_flag = 1;
//   }
// }
//   if(!$this->Sale->saveField('free_flag',$free_flag))
//     throw new Exception('Error While Updating', 1);
//   $datasource_Sale->commit();
//   $datasource_SaleItem->commit();
//   $datasource_ProductBonusDetail->commit();
//   return $this->redirect(array('controller' => 'Sale', 'action' => 'Sale',$id));
// }
          delete_process :
          if($process=='delete')
          {
            $Sale=$this->Sale->findById($id);
            $status=$Sale['Sale']['status'];
            if($status==2)
            {
              $sale_return_rollback_function=$this->sale_rollback($id);
              if($sale_return_rollback_function['result']!='Success')
                throw new Exception($sale_return_rollback_function['result']);
            }
            $SaleItem=$this->SaleItem->find('list',array(
              'conditions'=>array(
                'sale_id'=>$id
                ),
              ));
            foreach ($SaleItem as $key => $value) {
              if(!$this->SaleItem->delete($key))
                throw new Exception("Error Processing SaleItem deletion", 1);
            }
            if(!$this->Sale->delete($id))
              throw new Exception("Error Processing Sale deletion", 1);
            $datasource_Sale->commit();
            $datasource_SaleItem->commit();
            $datasource_ProductBonusDetail->commit();
            $return['result']='Success';
            $this->Session->setFlash(__($return['result']));
            return $this->redirect(array('controller' => 'Sale', 'action' => 'Sale'));
          }
          cancel_process :
          if($process=='cancel')
          {
            $this->Sale->id=$id;
            if(!$this->Sale->saveField('flag','0'))
              throw new Exception('Error While Cancelling', 1);
            if(!$this->Sale->saveField('status','0'))
              throw new Exception('Error While Cancelling', 1);
            if(!$this->Sale->saveField('date_of_delivered',date('Y-m-d',strtotime($data_Sale['date']))))
              throw new Exception('Error While Cancelling', 1);
          }
        }
        if($process=='delivery')
        {
          $SaleItem = $this->SaleItem->find('all',array(
            'conditions' => array(
              'SaleItem.sale_id' => $id,
              ),
            'fields' => array(
              'SaleItem.product_id',
              'SaleItem.quantity',
              'SaleItem.free_qty',
              'SaleItem.unit_price',
              'SaleItem.tax_amount',
              'SaleItem.net_value',
              'Sale.invoice_no',
              'Sale.account_head_id',
              'Sale.date_of_delivered',
              'Sale.discount_amount',
              'Sale.other_value',
              'Sale.executive_id',
              'Sale.tax_amount',
              'Sale.taxable_amount',
              'Product.cost',
              )
            ));
          $tax_value=0;
          $discount=0;
          $net_value=0;
          $stock_value=0;$final_value=0;
          $datasource_Stock->begin();
          $date=date('Y-m-d',strtotime($SaleItem['0']['Sale']['date_of_delivered']));
          foreach ($SaleItem as $key => $value) {
            $quantity = $value['SaleItem']['quantity'];
            $free_quantity = $value['SaleItem']['free_qty'];
            $unit_price = $value['SaleItem']['unit_price'];
            $stock_cost_single=$value['Product']['cost']*$quantity;
            $stock_value+=$stock_cost_single;
            $product_id=$value['SaleItem']['product_id'];
            $tax_value+=$value['SaleItem']['tax_amount'];
            $net_value+=$value['SaleItem']['net_value'];
            $Stock = $this->Stock->findByProductId($product_id);
            $stock_id=$Stock['Stock']['id'];
            $stock_quantity=$Stock['Stock']['quantity'];
            $remark='Sale --'.$value['Sale']['invoice_no'].'('.$quantity.')';
            $stock_quantity-=$quantity;
            $stock_quantity-=$free_quantity;
            $StockController = new StockController;
            $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
            if($Stock_function_return['result']!='Success')
              throw new Exception($Stock_function_return['result']); 
          }
          $datasource_Stock->commit();
          $datasource_Journal->begin();
          $datasource_ExecutiveBonusDetail->begin();
          $datasource_ExecutiveBonus->begin();
          $credit=$sale_account_head_id;
          $exe_id=$SaleItem['0']['Sale']['executive_id'];
          $executive_id=$exe_id;
//pr($exe_id);
//exit;
          $account_head_id=$SaleItem['0']['Sale']['account_head_id'];
          $debit=$account_head_id;
          //$tax_value=floatval($tax_value);
          $discount=floatval($SaleItem['0']['Sale']['discount_amount']);
          $tax_value=floatval($SaleItem['0']['Sale']['tax_amount']);
          //$sale_amount=floatval($net_value);
          $sale_amount=floatval($SaleItem['0']['Sale']['taxable_amount']);
          $work_flow='Sales';
         // pr($work_flow);
         // exit;
          $invoice_no=$SaleItem['0']['Sale']['invoice_no'];
          $roundoff=floatval($SaleItem['0']['Sale']['other_value']);
          $remarks='Sale Invoice No :'.$invoice_no;
          $AccountingsController = new AccountingsController;
          $voucher_no=$AccountingsController->GetVoucherNo();
          $branch_id=$this->Session->read('User.branch_id');

         // $sale_amount+=$discount;
          $route_id=$this->Customer->field('route_id',array('Customer.account_head_id'=>$account_head_id));
          $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$sale_amount,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,$executive_id,$route_id);
          if($Accountings_function_return['result']!='Success')
            throw new Exception($Stock_function_return['message']);
          if($stock_value)
          {
            $debit=$sale_account_head_id;
            $credit=$stock_account_head_id;

            $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$stock_value,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,$executive_id,$route_id);
            if($Accountings_function_return['result']!='Success')
              throw new Exception($Accountings_function_return['message']);
          }
          if($tax_value)
          {
            $debit=$SaleItem['0']['Sale']['account_head_id'];
            $credit=$tax_on_sale_account_head_id;
            $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$tax_value,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,$executive_id,$route_id);
            if($Accountings_function_return['result']!='Success')
              throw new Exception($Accountings_function_return['message']);
          }
          if(floatval($discount))
          {
            if($discount>0)
            {
              $debit=$discount_paid_account_head_id;
              $credit=$SaleItem['0']['Sale']['account_head_id'];
            }
            else
            {
              $discount=$discount*-1;
              $credit=$discount_received_account_head_id;
              $debit=$SaleItem['0']['Sale']['account_head_id'];
            }
            $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$discount,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,$executive_id,$route_id);
            if($Accountings_function_return['result']!='Success')
              throw new Exception($Stock_function_return['message']);
          }
          if(floatval($roundoff))
          {
            if($roundoff>0)
            {
              $credit=$roundoff_received_account_head_id;
              $debit=$SaleItem['0']['Sale']['account_head_id'];
            }
            else
            {
              $roundoff=$roundoff*-1;
              $credit=$SaleItem['0']['Sale']['account_head_id'];
              $debit=$roundoff_paid_account_head_id;
            }
            $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$roundoff,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,$executive_id,$route_id);
            if($Accountings_function_return['result']!='Success')
              throw new Exception($Stock_function_return['message']);
          }
          if(floatval($receipt))
          {
            $credit=$SaleItem['0']['Sale']['account_head_id'];
            $debit=1;
            $voucher_no='';
            $AccountingsController = new AccountingsController;
            $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$receipt,$date,$remarks,$work_flow,$user_id,$voucher_no,'','',$branch_id,$executive_id,$route_id);
            if($Accountings_function_return['result']!='Success')
              throw new Exception($Stock_function_return['message']);
          }
       
          $datasource_Journal->commit();
          $datasource_ExecutiveBonusDetail->commit();
          $datasource_ExecutiveBonus->commit();
        }
        $datasource_Sale->commit();
        $datasource_SaleItem->commit();
        $datasource_CustomerPrice->commit();
        $datasource_ProductBonusDetail->commit();
        $return['result']='Success';
        $this->Session->setFlash(__($return['result']));
        $this->redirect(array('controller' => 'Sale', 'action' => 'Sale',$id));
      }
      catch (Exception $e)
      {
        $datasource_Sale->rollback();
        $datasource_SaleItem->rollback();
        $datasource_CustomerPrice->rollback();
        $datasource_ProductBonusDetail->rollback();
       // if($process=='delivery')
       // {
          $datasource_Stock->rollback();
          $datasource_Journal->rollback();
          $datasource_ExecutiveBonusDetail->rollback();
          $datasource_ExecutiveBonus->rollback();
        //}
        $return['result']=$e->getMessage();
        $this->Session->setFlash(__($return['result']));
        $this->redirect( Router::url( $this->referer(), true ) );
      }
    }
  }
  public function stock_availability_check_ajax($product_id)
  {
    $return['result']='Empty';
    $Stock=$this->Stock->find('first',array('conditions'=>array('product_id'=>$product_id,'flag'=>1)));
    if($Stock)
    {
      $return['result']='Success';
      $return['quantity']=$Stock['Stock']['quantity'];
    }
    echo json_encode($return);
    exit;
  }
  public function sale_item_insert($sale_items,$id)
  {
    try {
      if($sale_items)
      {
           $bonus_amount_total=0;
      $Sale=$this->Sale->findById($id);
    $invoice_no=$Sale['Sale']['invoice_no'];
        for ($i=0; $i <count($sale_items['product_id']) ; $i++) {
          $item_quantity=$sale_items['quantity'][$i];
          $item_freequantity=$sale_items['free_qty'][$i];
          $unit_price=$sale_items['unit_price'][$i];
          $UnitLevelConvert=$this->UnitLevelConvert($sale_items['product_id'][$i],$sale_items['unit_id'][$i]);
          $sale_unit_level=$UnitLevelConvert['sale_unit_level'];
          $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
          $product_unit_level=$UnitLevelConvert['product_unit_level'];
          $quantity=$item_quantity;
          $net_value=$sale_items['net_value'][$i];
          $free_quantity=$item_freequantity;
          if($sale_unit_level != 1){
            if($sale_unit_level == 2){
              $quantity=$no_of_piece_per_unit*$item_quantity;
              $free_quantity=$no_of_piece_per_unit*$item_freequantity;
              $unit_price=$unit_price/$no_of_piece_per_unit;
              //$net_value=$net_value/$no_of_piece_per_unit;
            }
          }  
          $unit=$this->Unit->findById($sale_items['unit_id'][$i]);     
          //$net_value=$unit_price*$quantity;
          $executive_rate = $this->Product->field(
                  'Product.wholesale_price',
                  array('Product.id ' => $sale_items['product_id'][$i]));
          $row_total=$net_value+$sale_items['tax_amount'][$i];
          $SaleItem_data=[
          'warehouse_id'=>$sale_items['warehouse_id'],
          'product_id'=>$sale_items['product_id'][$i],
          'quantity_mode'=>$unit['Unit']['name'],
          'unit_id'=>$sale_items['unit_id'][$i],
          'sale_id'=>$id,
          'actual_price'=>$sale_items['actual_price'][$i],
          'unit_price'=>$unit_price,
          'quantity'=>$quantity,
          'net_value'=>$net_value,
          'tax'=>$sale_items['tax'][$i],
          'tax_amount'=>$sale_items['tax_amount'][$i],
          'total'=>$row_total,
          'free_qty'=>$free_quantity,
          ];
          $this->SaleItem->create();
          if(!$this->SaleItem->save($SaleItem_data))
          {
            $errors = $this->SaleItem->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
          $CustomerPrice=$this->CustomerPrice->findByProductIdAndCustomerId($sale_items['product_id'][$i],$Sale['Sale']['account_head_id']);
                pr($CustomerPrice);
                if(empty($CustomerPrice))
                {
                $CustomerPrice_data=[
                'customer_id'=>$Sale['Sale']['account_head_id'],
                'product_id'=>$sale_items['product_id'][$i],
                'updated_on'=>date('Y-m-d H:i:s'),
                'product_wholesale_price'=>$unit_price,
                ];
                $this->CustomerPrice->create();
                  if(!$this->CustomerPrice->save($CustomerPrice_data))
                  {
                  $errors = $this->CustomerPrice->validationErrors;
                  foreach ($errors as $key => $value) {
                  throw new Exception($value[0], 1);
                  }
                  }
                }
                else
                {
                  $this->CustomerPrice->id=$CustomerPrice['CustomerPrice']['id'];
                  $CustomerPrice_data=[
                'product_wholesale_price'=>$unit_price,
                       ];
                  if(!$this->CustomerPrice->save($CustomerPrice_data))
                  {
                  $errors = $this->CustomerPrice->validationErrors;
                  foreach ($errors as $key => $value) {
                  throw new Exception($value[0], 1);
                  }
                  }
                }
        $sales_item_id=$this->SaleItem->getLastInsertId();
        $bonus_amount=($sale_items['unit_price'][$i]-$executive_rate)*$quantity;
        if($bonus_amount>0)
            {
          $bonus_amount_total=$bonus_amount_total+$bonus_amount;
         $ProductBonus_data=[
          'date'=>date('Y-m-d',strtotime($sale_items['date'])),
         'executive_id'=>$sale_items['executive_id'],
          'product_id'=>$sale_items['product_id'][$i],
          'sale_item_id'=>$sales_item_id,
          'sale_id'=>$id,
          'unit_price'=>$unit_price,
          'executive_rate'=>$executive_rate,
          'quantity'=>$quantity,
          'bonus_amount'=>$bonus_amount,
          ];
          $this->ProductBonusDetail->create();
          if(!$this->ProductBonusDetail->save($ProductBonus_data))
          {
            $errors = $this->ProductBonusDetail->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
        }
        }
                if($bonus_amount_total>0)
                {
                $ProductBonus_data=[
                'executive_id'=>$sale_items['executive_id'],
                'date'=>date('Y-m-d',strtotime($sale_items['date'])),
                'work_flow'=>"SALE",
                'reference_no'=>$invoice_no,
                'bonus_amount'=>$bonus_amount_total,
                ];
                $this->ExecutiveBonusCalculation->create();
                if(!$this->ExecutiveBonusCalculation->save($ProductBonus_data))
                {
                  $errors = $this->ExecutiveBonusCalculation->validationErrors;
                  foreach ($errors as $key => $value) {
                    throw new Exception($value[0], 1);
                  }
                }
              }
      }
      $return['result']='Success';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
    }
    return $return;
  }
  public function sale_item_update($sale_items,$id)
  {
    try {
      if($sale_items)
      {
        // pr($sale_items);exit;
        for ($i=0; $i <count($sale_items['SaleItem_id']) ; $i++) {
          $item_quantity=$sale_items['quantity'][$i];
          $executive_rate = $this->Product->field(
                  'Product.wholesale_price',
                  array('Product.id ' => $sale_items['product_id'][$i]));
          $item_freequantity=$sale_items['free_qty'][$i];
          $unit_price=$sale_items['unit_price'][$i];
          $UnitLevelConvert=$this->UnitLevelConvert($sale_items['product_id'][$i],$sale_items['unit_id'][$i]);
          $sale_unit_level=$UnitLevelConvert['sale_unit_level'];
          $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
          $product_unit_level=$UnitLevelConvert['product_unit_level'];
          $quantity=$item_quantity;
          $free_quantity=$item_freequantity;
          if($sale_unit_level != 1){
            if($sale_unit_level == 2){
              $quantity=$no_of_piece_per_unit*$item_quantity;
              $free_quantity=$no_of_piece_per_unit*$item_freequantity;
              $unit_price=$unit_price/$no_of_piece_per_unit;
            }
          }  
          //$net_value=$unit_price*$quantity;
          $net_value=$sale_items['net_value'][$i];
          $row_total=$net_value+$sale_items['tax_amount'][$i];
          $SaleItem_data=[
          'tax'=>$sale_items['tax'][$i],
          'tax_amount'=>$sale_items['tax_amount'][$i],
          'net_value'=>$net_value,
          'total'=>$row_total,
          ];
          $sales_item_id=$sale_items['SaleItem_id'][$i];
          $this->SaleItem->id=$sales_item_id;
          if(!$this->SaleItem->save($SaleItem_data))
          {
            $errors = $this->SaleItem->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
         
          $product_bonus=0;
           $sales_item_id=$this->SaleItem->getLastInsertId();
         $bonus_amount=($sale_items['unit_price'][$i]-$executive_rate)*$quantity;
           if($bonus_amount>0)
                {
          $ProductBonus_data=[
          'unit_price'=>$sale_items['unit_price'][$i],
          'cost'=>$executive_rate,
          'quantity'=>$quantity,
          'net_value'=>$net_value,
          'bonus_percentage'=>$product_bonus,
          'bonus_amount'=>$bonus_amount,
          ];
          $ProductBonusDetail=$this->ProductBonusDetail->findBySaleItemId($sales_item_id);
          $this->ProductBonusDetail->id=$ProductBonusDetail['ProductBonusDetail']['id'];
          if(!$this->ProductBonusDetail->save($ProductBonus_data))
          {
            $errors = $this->ProductBonusDetail->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
        }
        }
      }
      $return['result']='Success';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
    }
    return $return;
  }
  public function sale_rollback($id)
  {
    $user_id=1;
    $datasource_Sale = $this->Sale->getDataSource();
    $datasource_Stock = $this->Stock->getDataSource();
    $datasource_Journal = $this->Journal->getDataSource();
    $datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
    $datasource_ExecutiveBonusCalculation = $this->ExecutiveBonusCalculation->getDataSource();
    try {
      $datasource_Sale->begin();
      $datasource_Stock->begin();
      $datasource_Journal->begin();
      $datasource_ProductBonusDetail->begin();
      $datasource_ExecutiveBonusCalculation->begin();
      $Sale=$this->Sale->findById($id);
      if(!$Sale)
        throw new Exception("Empty Sales", 1);
      $invoice_no=$Sale['Sale']['invoice_no'];
      $executive_id=$Sale['Sale']['executive_id'];
     //  $this->Sale->id=$id;
     // if(!$this->Sale->saveField('status','0'))
     //  throw new Exception('Error While Updating', 1);
     // if(!$this->Sale->saveField('flag','0'))
     //   throw new Exception('Error While Updating', 1);
      foreach ($Sale['SaleItem'] as $key => $value) {
        $quantity = $value['quantity'];
        $free_quantity = $value['free_qty'];
        $product_id=$value['product_id'];
        $warehouse_id=$value['warehouse_id'];
        $Stock = $this->Stock->find('first',array(
          'conditions'=>array(
            'Stock.product_id'=>$product_id,
            'Stock.warehouse_id'=>$warehouse_id,
            )
          ));
        if(!$Stock)
          throw new Exception("Empty Stock", 1);
        $stock_id=$Stock['Stock']['id'];
        $stock_quantity=$Stock['Stock']['quantity'];
        $date=date('Y-m-d');
      $remark='Cancel Sale --'.$invoice_no.'('.$quantity.')';
        $stock_quantity+=$quantity;
        $stock_quantity+=$free_quantity;
        $StockController = new StockController;
        $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
        if($Stock_function_return['result']!='Success')
          throw new Exception($Stock_function_return['result']);
      }
      $Journal=$this->Journal->find('all',array(
        'conditions'=>array(
          'remarks'=>'Sale Invoice No :'.$Sale['Sale']['invoice_no'],
          ),
        'fields'=>array(
          'Journal.*',
          )
        ));
      foreach ($Journal as $key => $value) {
        if(!$this->Journal->delete($value['Journal']['id']))
          throw new Exception("Error Processing Journal deletion", 1);
      }
      $ProductBonusDetail=$this->ProductBonusDetail->find('all',array(
        'conditions'=>array(
          'sale_id'=>$id,
          ),
        'fields'=>array(
          'ProductBonusDetail.*',
          )
      ));
      $bonus_amount=0;
      foreach ($ProductBonusDetail as $key => $value) {
        $bonus_amount+=$value['ProductBonusDetail']['bonus_amount'];
        if(!$this->ProductBonusDetail->delete($value['ProductBonusDetail']['id']))
          throw new Exception("Error Processing Product Bonus Detail deletion", 1);
      }
      $ExecutiveBonusCalculation=$this->ExecutiveBonusCalculation->findByReferenceNo($Sale['Sale']['invoice_no']);
      if(!empty($ExecutiveBonusCalculation))
      {
         if(!$this->ExecutiveBonusCalculation->delete($ExecutiveBonusCalculation['ExecutiveBonusCalculation']['id']))
          throw new Exception("Error Processing ExecutiveBonusCalculation deletion", 1);
      }
      else
      {
        $ExecutiveBonusCalculation=$this->ExecutiveBonusCalculation->findByWorkFlowAndBonusAmountAndExecutiveId("SALE",$bonus_amount,$executive_id);
           if(!empty($ExecutiveBonusCalculation))
            {
         if(!$this->ExecutiveBonusCalculation->delete($ExecutiveBonusCalculation['ExecutiveBonusCalculation']['id']))
          throw new Exception("Error Processing ExecutiveBonusCalculation deletion", 1);
           }
      }
      
      $datasource_Sale->commit();
      $datasource_Stock->commit();
      $datasource_Journal->commit();
      $datasource_ProductBonusDetail->commit();
      $datasource_ExecutiveBonusCalculation->commit();
      $return['result']='Success';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
      $datasource_Sale->rollback();
      $datasource_Stock->rollback();
      $datasource_Journal->rollback();
      $datasource_ProductBonusDetail->rollback();
      $datasource_ExecutiveBonusCalculation->rollback();
    }
    return $return;
  }
  public function DeleteInvoice($id)
  {
    try {
      $Sale = $this->sale_rollback($id);
      if($Sale['result']!='Success')
        throw new Exception("Error in deleting invoice", 1);
      $Sales = $this->Sale->delete($id);
      $return['result']='Invoice Deleted Successfully';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
    }
    $this->Session->setFlash(__($return['result']));
    $this->redirect( Router::url( $this->referer(), true ) );
  }
  public function SalesReturnIndex()
  {
    $this->request->data['from_date']=date('d-m-Y',strtotime('first day of this month'));
  $this->request->data['to_date']=date('d-m-Y');
   $executives=$this->Executive->find('list',array(
      'fields'=>['Executive.id','Executive.name']
    ));
    $this->set(compact('executives'));
  }
  public function SalesReturnIndex_ajax()
{
  $requestData=$this->request->data;
  $columns = [];
  $columns[]='SalesReturn.date';
  $columns[]='Executive.name';
  $columns[]='AccountHead.name';
  $columns[]='SalesReturn.invoice_no';
  $columns[]='SalesReturn.grand_total';
  $columns[]='SalesReturn.status';
  $columns[]='SalesReturn.id';
  $conditions=[];
  if(isset($requestData['from_date']) && isset($requestData['to_date']) )
  {
    $conditions['SalesReturn.date between ? and ?']=[
      date('Y-m-d',strtotime($requestData['from_date'])),
      date('Y-m-d',strtotime($requestData['to_date']))
    ];
  }
if(isset($requestData['executive_id']))
  {
    if($requestData['executive_id'])
      $conditions['SalesReturn.executive_id']=$requestData['executive_id'];
  }
  $totalData=$this->SalesReturn->find('count',[
        "joins"=>array(
          array(
            "table"=>'executives',
            "alias"=>'Executive',
            "type"=>'inner',
            "conditions"=>array('Executive.id=SalesReturn.executive_id'),
            ), ),
    'conditions'=>$conditions]);
  $totalFiltered=$totalData;
  if( !empty($requestData['search']['value']) ) { 
    $q=$requestData['search']['value'];
    $conditions['OR']=array(
      'SalesReturn.date LIKE' =>'%'. $q . '%',
      'AccountHead.name LIKE' =>'%'. $q . '%',
        'Executive.name LIKE' =>'%'. $q . '%',
      'SalesReturn.invoice_no LIKE' =>'%'. $q . '%',
      'SalesReturn.grand_total LIKE' =>'%'. $q . '%',
    );
    $totalFiltered=$this->SalesReturn->find('count',[
         "joins"=>array(
          array(
            "table"=>'executives',
            "alias"=>'Executive',
            "type"=>'inner',
            "conditions"=>array('Executive.id=SalesReturn.executive_id'),
            ), ),
      'conditions'=>$conditions,
    ]);
  }
  $Data=$this->SalesReturn->find('all',array(
    "joins"=>array(
          array(
            "table"=>'executives',
            "alias"=>'Executive',
            "type"=>'inner',
            "conditions"=>array('Executive.id=SalesReturn.executive_id'),
            ), ),
    'conditions'=>$conditions,
    'offset'=>$requestData['start'],
    'limit'=>$requestData['length'],
    'order'=>$columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
    'fields'=>array(
      'SalesReturn.*',
      'AccountHead.id',
      'AccountHead.name',
      'Executive.name',
    )
  ));
  function get_status_name($status)
  {
    $name='';
    if($status==0) { $name='Cancelled'; }
    if($status==1) { $name='Order Placed'; }
    if($status==2) { $name='Order Delivered'; }
    return $name;
  }
  foreach ($Data as $key => $value) {
   $this->SalesReturnItem->virtualFields = array('SalesReturnItem_qty' => "SUM(SalesReturnItem.quantity)");
   $SalesReturnItem=$this->SalesReturnItem->findBySalesReturnId($value['SalesReturn']['id'],['SalesReturnItem_qty']);
   $Data[$key]['SalesReturn']['quantity']=floatval($SalesReturnItem['SalesReturnItem']['SalesReturnItem_qty']);
   $Data[$key]['SalesReturn']['date']=date('d-m-Y',strtotime($value['SalesReturn']['date']));
   $Data[$key]['SalesReturn']['grand_total']=str_replace(',','',number_format((float)$value['SalesReturn']['grand_total'],2));
   $Data[$key]['SalesReturn']['status']=get_status_name($value['SalesReturn']['status']);
   $Data[$key]['SalesReturn']['action']='<span><a target="_blank" href="'.$this->webroot.'Sale/SalesReturn/'.$value['SalesReturn']['id'].'"><i class="fa fa-2x fa-eye"></i></span></a>';
 } 

 $json_data = array(
  "draw"           =>intval($requestData['draw']),
  "recordsTotal"   =>intval($totalData),
  "recordsFiltered"=>intval($totalFiltered),
  "records"        =>$Data
);
 echo json_encode($json_data); exit;
}
  public function SalesReturnNew($id=null)
  {
    $sub_group_id=3;
    $user_id=1;
    $CustomerType_list=$this->CustomerType->find('list',array('order'=>['CustomerType.name Asc'],'fields'=>['id','CustomerType.name']));
    $this->set('CustomerType_list',$CustomerType_list);
    $Product_list=$this->Product->find('list',array('fields'=>['id','name']));
    $this->set('Product_list',$Product_list);
    $Bill_list=$this->Bill->find('list',array('fields'=>['id','name']));
    $this->set('Bill_list',$Bill_list);
    $SalesReturn=$this->SalesReturn->find('count');
    if(!empty($SalesReturn))
    {
      $invoice_no=$SalesReturn+1;
    }
    else
    {
      $invoice_no=1;
    }
    if (!$this->request->data)
    {
      if(empty($id))
      {
        $data['SalesReturn']['invoice_no']=$invoice_no;
        $data['SalesReturn']['date']=date('d-m-Y');
        $data['SalesReturn']['status']=1;
        $data['SalesReturn']['customer_type_id']=1;
        $Customer_list=$this->Customer->find('list',array(
          'joins'=>array(
            array(
              'table'=>'account_heads',
              'alias'=>'AccountHead',
              'type'=>'INNER',
              'conditions'=>array('AccountHead.id=Customer.account_head_id')
              ),
            ),
          'conditions'=>['Customer.customer_type_id'=>1],
          'fields'=>['AccountHead.id','AccountHead.name'])
        );
        $this->set('Customer_list',$Customer_list);
        $invoice_no_list=$this->Sale_invoice_no_list_by_customer_id(3);
        $this->set('invoice_no_list',$invoice_no_list);
        $this->request->data=$data;
      }
      else
      {
        $SalesReturn=$this->SalesReturn->findById($id);
        $Customer=$this->Customer->findByAccountHeadId($SalesReturn['SalesReturn']['account_head_id']);
        $this->request->data=$SalesReturn;
        $this->request->data['SalesReturn']['date']=date('d-m-Y',strtotime($SalesReturn['SalesReturn']['date']));
        $this->request->data['SalesReturn']['address']=$Customer['Location']['name'];
        $this->request->data['SalesReturn']['customer_type_id']=$Customer['Customer']['customer_type_id'];
        $Customer_list=$this->Customer->find('list',array(
          'joins'=>array(
            array(
              'table'=>'account_heads',
              'alias'=>'AccountHead',
              'type'=>'INNER',
              'conditions'=>array('AccountHead.id=Customer.account_head_id')
              ),
            ),
          'conditions'=>['Customer.customer_type_id'=>$Customer['Customer']['customer_type_id']],
          'fields'=>['AccountHead.id','AccountHead.name'])
        );
        $this->set('Customer_list',$Customer_list);
        $SalesReturnItem=$this->SalesReturnItem->find('all',array(
          "joins" => array(
            array(
              "table" => 'brands',
              "alias" => 'Brand',
              "type" => 'left',
              "conditions" => array('Brand.id=Product.brand_id'),
              ),
            array(
              "table" => 'product_types',
              "alias" => 'ProductType',
              "type" => 'inner',
              "conditions" => array('ProductType.id=Product.product_type_id'),
              ),
            ),
          'conditions'=>array(
            'sales_return_id'=>$id,
            ),
          'fields'=>array(
            'SalesReturnItem.*',
            'Product.name',
            'Brand.name',
            'ProductType.name',
            )
          ));
        $invoice_no_list=$this->Sale_invoice_no_list_by_customer_id($SalesReturn['SalesReturn']['account_head_id']);
        $this->set('invoice_no_list',$invoice_no_list);
        $this->set('SalesReturnItem',$SalesReturnItem);
      }
    }
    else
    {
      $datasource_SalesReturn = $this->SalesReturn->getDataSource();
      $datasource_SalesReturnItem = $this->SalesReturnItem->getDataSource();
      $datasource_Product = $this->Product->getDataSource();
      $datasource_Stock = $this->Stock->getDataSource();
      try {
        $datasource_SalesReturn->begin();
        $datasource_SalesReturnItem->begin();
        $data=$this->request->data;
        if(empty($id))
        {
          $data=$data['SalesReturn'];
          $SalesReturn_data=[
          'account_head_id'=>$data['account_head_id'],
          'invoice_no'=>$data['invoice_no'],
          'date'=>date('Y-m-d',strtotime($data['date'])),
          'total'=>$data['grand_total'],
          'discount'=>$data['discount'],
          'grand_total'=>$data['net_amount'],
          'created_by'=>$user_id,
          'modified_by'=>$user_id,
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s'),
          ];
          $this->SalesReturn->create();
          if(!$this->SalesReturn->save($SalesReturn_data))
          {
            $errors = $this->SalesReturn->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
          $sales_return_id=$this->SalesReturn->getLastInsertId();
          for ($i=0; $i <count($data['product_id']) ; $i++) {
            $SalesReturnItem_data=[
            'product_id'=>$data['product_id'][$i],
            'sales_return_id'=>$sales_return_id,
            'invoice_no'=>$data['invoice_no_list'][$i],
            'invoice_price'=>$data['invoice_price'][$i],
            'unit_price'=>$data['unit_price'][$i],
            'quantity'=>$data['quantity'][$i],
            'net_value'=>$data['net_value'][$i],
            'total'=>$data['row_total'][$i],
            ];
            $this->SalesReturnItem->create();
            if(!$this->SalesReturnItem->save($SalesReturnItem_data))
            {
              $errors = $this->SalesReturnItem->validationErrors;
              foreach ($errors as $key => $value) {
                throw new Exception($value[0], 1);
              }
            }
          }
        }
        else
        {
          $data_SalesReturn=$data['SalesReturn'];
          if(isset($data['SalesReturnItem']))
          {
            $old_SalesReturnItem=$data['SalesReturnItem'];
          }
          $SalesReturn_data=[
          'account_head_id'=>$data_SalesReturn['account_head_id'],
          'invoice_no'=>''.$data_SalesReturn['invoice_no'].'',
          'date'=>date('Y-m-d',strtotime($data_SalesReturn['date'])),
          'total'=>$data_SalesReturn['grand_total'],
          'discount'=>$data_SalesReturn['discount'],
          'grand_total'=>$data_SalesReturn['net_amount'],
          'updated_at'=>date('Y-m-d H:i:s'),
          ];
          $process=$data_SalesReturn['process'];
          if($process!='cancel')
          {
            if($process=='delivery')
            {
              $SalesReturn_data['status']=2;
            }
            $this->SalesReturn->id=$id;
            if(!$this->SalesReturn->save($SalesReturn_data))
            {
              $errors = $this->SalesReturn->validationErrors;
              foreach ($errors as $key => $value) {
                throw new Exception($value[0], 1);
              }
            }
            if(isset($data_SalesReturn['product_id'])){
// add new items
              for ($i=0; $i <count($data_SalesReturn['product_id']) ; $i++) {
                $SalesReturnItem_data=[
                'product_id'=>$data_SalesReturn['product_id'][$i],
                'sales_return_id'=>$id,
                'invoice_no'=>$data_SalesReturn['invoice_no_list'][$i],
                'invoice_price'=>$data_SalesReturn['invoice_price'][$i],
                'unit_price'=>$data_SalesReturn['unit_price'][$i],
                'quantity'=>$data_SalesReturn['quantity'][$i],
                'net_value'=>$data_SalesReturn['net_value'][$i],
// 'tax'=>$data_SalesReturn['tax'][$i],
// 'tax_amount'=>$data_SalesReturn['tax_amount'][$i],
                'total'=>$data_SalesReturn['row_total'][$i],
                ];
                $this->SalesReturnItem->create();
                if(!$this->SalesReturnItem->save($SalesReturnItem_data))
                {
                  $errors = $this->SalesReturnItem->validationErrors;
                  foreach ($errors as $key => $value) {
                    throw new Exception($value[0], 1);
                  }
                }
              }
            }
// update new items
// for ($i=0; $i <count($old_SaleItem['SaleItem_id']) ; $i++) {
//   $SaleItem_old_data=[
//   'unit_price'=>$old_SaleItem['unit_price'][$i],
//   'quantity'=>$old_SaleItem['quantity'][$i],
//   'net_value'=>$old_SaleItem['net_value'][$i],
//   'tax'=>$old_SaleItem['tax'][$i],
//   'tax_amount'=>$old_SaleItem['tax_amount'][$i],
//   'total'=>$old_SaleItem['row_total'][$i],
//   ];
//   $this->SaleItem->id=$old_SaleItem['SaleItem_id'][$i];
//   if(!$this->SaleItem->save($SaleItem_old_data))
//   {
//     $errors = $this->SaleItem->validationErrors;
//     foreach ($errors as $key => $value) {
//       throw new Exception($value[0], 1);
//     }
//   }
// }
            if($process=='delivery')
            {
              $SalesReturnItem = $this->SalesReturnItem->find('all',array(
                'conditions' => array(
                  'SalesReturnItem.sales_return_id' => $id,
                  ),
                'fields' => array(
                  'SalesReturnItem.product_id',
                  'SalesReturnItem.quantity',
                  'SalesReturnItem.unit_price',
                  'SalesReturn.invoice_no',
                  )
                ));
              $datasource_Stock->begin();
              foreach ($SalesReturnItem as $key => $value) {
                $quantity = $value['SalesReturnItem']['quantity'];
                $unit_price = $value['SalesReturnItem']['unit_price'];
                $product_id=$value['SalesReturnItem']['product_id'];
                $Stock = $this->Stock->find('first',array(
                  'conditions'=>array(
                    'product_id'=>$product_id,
                    'warehouse_id'=>1,
                    )
                  ));
                $stock_id=$Stock['Stock']['id'];
                $stock_quantity=$Stock['Stock']['quantity'];
                $date=date('Y-m-d',strtotime($data_SalesReturn['date']));
                $StockController = new StockController;
                $remark='SalesReturn --'.$value['SalesReturn']['invoice_no'].'('.$quantity.')';
                $quantity+=$stock_quantity;
                $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$quantity,$date,$user_id,$remark);
                if($Stock_function_return['result']!='Success')
                  throw new Exception($Stock_function_return['result']);
              }
              $datasource_Stock->commit();
            }
          }
          else
          {
            $this->SalesReturn->id=$id;
            if(!$this->SalesReturn->saveField('flag','0'))
              throw new Exception('Error While Cancelling', 1);
            if(!$this->SalesReturn->saveField('status','0'))
              throw new Exception('Error While Cancelling', 1);
            if(!$this->SalesReturn->saveField('date',date('Y-m-d',strtotime($data_Sale['date']))))
              throw new Exception('Error While Cancelling', 1);
          }
        }
        $datasource_SalesReturn->commit();
        $datasource_SalesReturnItem->commit();
        if(empty($id))
        {
          $return['result']='Success';
          $this->Session->setFlash(__($return['result']));
          return $this->redirect(array('controller' => 'Sale', 'action' => 'SalesReturn',$sales_return_id));
        }
        else
        {
          $return['result']='Updated';
          $this->Session->setFlash(__($return['result']));
          return $this->redirect(array('controller' => 'Sale', 'action' => 'SalesReturn',$id));
        }
      }
      catch (Exception $e)
      {
        $datasource_SalesReturn->rollback();
        $datasource_SalesReturnItem->rollback();
        $datasource_Stock->rollback();
        $return['result']=$e->getMessage();
      }
      $this->Session->setFlash(__($return['result']));
      return $this->redirect(array('controller' => 'Sale', 'action' => 'SalesReturn'));
    }
  }
  public function SalesReturn($id=null)
  {
    $PermissionList = $this->Session->read('PermissionList');
    $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Sale/SalesReturn'));
    // if(!in_array($menu_id, $PermissionList))
    // {
    //   $this->Session->setFlash("Permission denied");
    //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
    // }
    $sale_return_account_head_id=4;
    $discount_received_account_head_id=12;
    $discount_paid_account_head_id=13;
    $stock_account_head_id=19;
    //$tax_on_sale_account_head_id=9;
    $tax_on_sale_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DUTIES & TAXES'));
    $sub_group_id=3;
    $user_id=1;
    $Unit=$this->Unit->find('list',array('fields'=>['id','name']));
    $this->set('Unit',$Unit);
    $CustomerType_list=$this->CustomerType->find('list',array('order'=>['CustomerType.name Asc'],'fields'=>['id','CustomerType.name']));
    $this->set('CustomerType_list',$CustomerType_list);
    $Product_list=$this->Product->find('list',array('conditions'=>array('Product.active=1','type ='=>2),'fields'=>['id','name']));
    $this->set('Product_list',$Product_list);
    $this->set('warehouse',$this->Warehouse->find('list',array(
      'order'=>array('name ASC'),
      'conditions'=>array('Warehouse.warehouse_id'=>""),
      'fields'=>array('Warehouse.id','Warehouse.name'),
      )));
    $SalesReturn_count=$this->SalesReturn->find('count',array('order' => 'SalesReturn.id DESC',));
    $invoice_no=1;
    if(!empty($SalesReturn_count))
    { 
      $SalesReturn=$this->SalesReturn->findByInvoiceNo($SalesReturn_count);
      while($SalesReturn)
      {
        $SalesReturn_count++;
        $SalesReturn=$this->SalesReturn->findByInvoiceNo($SalesReturn_count);
      }
      $invoice_no=$SalesReturn_count;
    }
    if (!$this->request->data) 
    {
      if(empty($id)) 
      {
        $data['SalesReturn']['type']=1;
        $data['SalesReturn']['invoice_no']=$invoice_no;
        $data['SalesReturn']['date']=date('d-m-Y');
        $data['SalesReturn']['status']=1;
        $data['SalesReturn']['customer_type_id']=1;
        $data['SalesReturn']['refund']=0.00;
//         $this->Customer->virtualFields = array(
//           'customer_name' => "CONCAT(AccountHead.name, ' ', Customer.code)"
//           );
//         $Customer_list=$this->Customer->find('list',array(
//           'joins'=>array(
//             array(
//               'table'=>'account_heads',
//               'alias'=>'AccountHead',
//               'type'=>'INNER',
//               'conditions'=>array('AccountHead.id=Customer.account_head_id')
//               ),
//             ),
// // 'conditions'=>['Customer.customer_type_id'=>1],
//           'fields'=>['AccountHead.id','customer_name'])
//         );

//         $this->set('Customer_list',$Customer_list);
        $invoice_no_list=$this->Sale_invoice_no_list_by_customer_id(3);
        $this->set('invoice_no_list',$invoice_no_list);
        $this->request->data=$data;
      }
      else
      {
        $SalesReturn=$this->SalesReturn->findById($id);
        $Customer=$this->Customer->findByAccountHeadId($SalesReturn['SalesReturn']['account_head_id']);
        $this->request->data=$SalesReturn;
        $this->request->data['SalesReturn']['date']=date('d-m-Y',strtotime($SalesReturn['SalesReturn']['date']));
        $this->request->data['SalesReturn']['address']=$Customer['Customer']['place'];
        $this->request->data['SalesReturn']['customer_type_id']=$Customer['Customer']['customer_type_id'];
        $this->request->data['SalesReturn']['account_head_id']=$Customer['AccountHead']['name'];

        // $this->Customer->virtualFields = array(
        //   'customer_name' => "CONCAT(AccountHead.name, ' ', Customer.code)"
        //   );
        // $Customer_list=$this->Customer->find('list',array(
        //   'joins'=>array(
        //     array(
        //       'table'=>'account_heads',
        //       'alias'=>'AccountHead',
        //       'type'=>'INNER',
        //       'conditions'=>array('AccountHead.id=Customer.account_head_id')
        //       ),
        //     ),
        //   'conditions'=>['Customer.customer_type_id'=>$Customer['Customer']['customer_type_id']],
        //   'fields'=>['AccountHead.id','customer_name'])
        // );

        // $this->set('Customer_list',$Customer_list);
        $SalesReturnItem=$this->SalesReturnItem->find('all',array(
          "joins" => array(
            array(
              "table" => 'brands',
              "alias" => 'Brand',
              "type" => 'left',
              "conditions" => array('Brand.id=Product.brand_id'),
              ),
            array(
              "table" => 'product_types',
              "alias" => 'ProductType',
              "type" => 'inner',
              "conditions" => array('ProductType.id=Product.product_type_id'),
              ),
            array(
              "table" => 'units',
              "alias" => 'Unit',
              "type" => 'inner',
              "conditions" => array('Unit.id=SalesReturnItem.unit_id'),
              ),
            ),
          'conditions'=>array(
            'sales_return_id'=>$id,
            ),
          'fields'=>array(
            'SalesReturnItem.*',
            'Product.name',
            'Product.no_of_piece_per_unit',
            'Brand.name',
            'ProductType.name',
            'Unit.name',
            )
          ));
        $invoice_no_list=$this->Sale_invoice_no_list_by_customer_id($SalesReturn['SalesReturn']['account_head_id']);
        $this->set('invoice_no_list',$invoice_no_list);
        $this->set('SalesReturnItem',$SalesReturnItem);
      }
    } 
    else 
    {
      $datasource_SalesReturn = $this->SalesReturn->getDataSource();
      $datasource_SalesReturnItem = $this->SalesReturnItem->getDataSource();
      $datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
      $datasource_ExecutiveBonusDetail = $this->ExecutiveBonusCalculation->getDataSource();
      $datasource_ExecutiveBonus = $this->ExecutiveBonus->getDataSource();
      $datasource_Product = $this->Product->getDataSource();
      $datasource_Stock = $this->Stock->getDataSource();
      $datasource_Journal = $this->Journal->getDataSource();

      
      try {
        $datasource_SalesReturn->begin();
        $datasource_SalesReturnItem->begin();
        $datasource_ProductBonusDetail->begin();
        $datasource_ExecutiveBonusDetail->begin();
        $datasource_ExecutiveBonus->begin();
        $data=$this->request->data;
        if(empty($id)) 
        {
          $data=$data['SalesReturn'];
          $executive=0;
            $route_id=$this->Customer->field('Customer.route_id',array('Customer.account_head_id'=>$data['account_head_id']));
            if($route_id)
            {
            $executive=$this->ExecutiveRouteMapping->field('ExecutiveRouteMapping.executive_id',array('ExecutiveRouteMapping.route_id'=>$route_id));
            if(!$executive)
            {
            $executive=0;
            }
            }
          $SalesReturn_data=[
          'account_head_id'=>$data['account_head_id'],
          'executive_id'=>$executive,
          'invoice_no'=>$data['invoice_no'],
          'date'=>date('Y-m-d',strtotime($data['date'])),
          'warehouse_id'=>$data['warehouse_id_hidden'],
          'total'=>$data['grand_total'],
          'discount'=>$data['discount'],
          'grand_total'=>$data['net_amount'],
          'refund'=>$data['refund'],
         // 'type'=>$data['payment_type'],
          'created_by'=>$user_id,
          'modified_by'=>$user_id,
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s'),
          ];
          $process=$data['process'];
          if($process=='delivery')
          {
            $SalesReturn_data['status']=2;
          }
          $this->SalesReturn->create();
          if(!$this->SalesReturn->save($SalesReturn_data)) 
          {
            $errors = $this->SalesReturn->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
          $id=$this->SalesReturn->getLastInsertId();
          if(isset($data['product_id']))
          {
            $return_function=$this->sale_return_item_insert($data,$id);
            if($return_function['result']!='Success')
              throw new Exception($return_function['result']);  
          }
        }
        else
        {
          $data_SalesReturn=$data['SalesReturn'];
          if(isset($data['SalesReturnItem']))
          {
            $old_SalesReturnItem=$data['SalesReturnItem'];  
          }
          $SalesReturn_data=[
          'account_head_id'=>$data_SalesReturn['account_head_id'],
           'warehouse_id'=>$data['warehouse_id_hidden'],
          'invoice_no'=>''.$data_SalesReturn['invoice_no'].'',
          'date'=>date('Y-m-d',strtotime($data_SalesReturn['date'])),
          'total'=>$data_SalesReturn['grand_total'],
          'discount'=>$data_SalesReturn['discount'],
          'type'=>$data_SalesReturn['payment_type'],
          'grand_total'=>$data_SalesReturn['net_amount'],
          'updated_at'=>date('Y-m-d H:i:s'),
          ];
          $process=$data_SalesReturn['process'];
          if($process=='cancel')
          {
            goto cancel_process;
          }
          if($process=='delete')
          {
            goto delete_process;
          }
          if($process=='delivery')
          {
            $SalesReturn_data['status']=2;
          }
          $this->SalesReturn->id=$id;
          if(!$this->SalesReturn->save($SalesReturn_data))
          {
            $errors = $this->SalesReturn->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
          if(isset($data_SalesReturn['product_id'])){
            $return_function=$this->sale_return_item_insert($data_SalesReturn,$id);
            if($return_function['result']!='Success')
              throw new Exception($return_function['result']);  
          }
          delete_process :
          if($process=='delete')
          {
            $SalesReturn=$this->SalesReturn->findById($id);
            $status=$SalesReturn['SalesReturn']['status'];
            if($status==2)
            {
              $sale_return_rollback_function=$this->sale_return_rollback($id);
              if($sale_return_rollback_function['result']!='Success')
                throw new Exception($sale_return_rollback_function['result']);
            }
            $SalesReturnItem=$this->SalesReturnItem->find('list',array(
              'conditions'=>array(
                'sales_return_id'=>$id
                ),
              ));
            foreach ($SalesReturnItem as $key => $value) {
              if(!$this->SalesReturnItem->delete($key))
                throw new Exception("Error Processing SalesReturnItem deletion", 1);
            }
            if(!$this->SalesReturn->delete($id))
              throw new Exception("Error Processing SalesReturn deletion", 1);
            $datasource_SalesReturn->commit();
            $datasource_SalesReturnItem->commit();
            $datasource_ProductBonusDetail->commit();
            $datasource_ExecutiveBonusDetail->commit();
            $datasource_ExecutiveBonus->commit();
            $return['result']='Success';
            $this->Session->setFlash(__($return['result']));
            return $this->redirect(array('controller' => 'Sale', 'action' => 'SalesReturn'));
          }
          cancel_process :
          if($process=='cancel')
          {
            $this->SalesReturn->id=$id;
            if(!$this->SalesReturn->saveField('flag','0'))
              throw new Exception('Error While Cancelling', 1);
            if(!$this->SalesReturn->saveField('status','0'))
              throw new Exception('Error While Cancelling', 1);
            if(!$this->SalesReturn->saveField('date',date('Y-m-d',strtotime($data_SalesReturn['date']))))
              throw new Exception('Error While Cancelling', 1);
          }
        }
        if($process=='delivery')
        {
           // $payment_type=$data['payment_type'];
          $SalesReturnItem = $this->SalesReturnItem->find('all',array(
            'conditions' => array(
              'SalesReturnItem.sales_return_id' => $id,
              ),
            'fields' => array(
              'SalesReturnItem.product_id',
              'SalesReturnItem.quantity',
              'SalesReturnItem.warehouse_id',
              'SalesReturnItem.unit_price',
              'SalesReturnItem.net_value',
              'SalesReturnItem.tax_amount',
              'SalesReturnItem.sale_executive_id',
              'SalesReturn.account_head_id',
              'SalesReturn.invoice_no',
              'SalesReturn.date',
              'SalesReturn.grand_total',
              'SalesReturn.discount',
              'SalesReturnItem.type',
              'Product.cost',
              'SalesReturnItem.invoice_no',
              )
            ));
          $net_value=0;
          $tax=0;
          $stock_value=0;
          $datasource_Stock->begin();
          foreach ($SalesReturnItem as $key => $value) {
            $work_flow_cash = 'Sales Refund :'.$value['SalesReturnItem']['invoice_no'];
            $net_value+= $value['SalesReturnItem']['net_value'];
            $warehouse_id = $value['SalesReturnItem']['warehouse_id'];
            $quantity = $value['SalesReturnItem']['quantity'];
            $stock_value+= $value['Product']['cost']*$quantity;
            $tax+= $value['SalesReturnItem']['tax_amount'];
            $unit_price = $value['SalesReturnItem']['unit_price'];
            $product_id=$value['SalesReturnItem']['product_id'];
             $Stock = $this->Stock->findByProductIdAndWarehouseId($product_id,$warehouse_id);
             if(!$Stock){
              $remark='SalesReturn --'.$value['SalesReturn']['invoice_no'].'('.$quantity.')';
              $flag=1;
              $StockController = new StockController;
              $Stock_function_return=$StockController->GeneralStock_Add_Function($warehouse_id,$product_id,$quantity,date('Y-m-d'),$user_id,$remark,$flag);
              if($Stock_function_return['result']!='Success')
                throw new Exception($Stock_function_return['result'], 1);
            }else{
              $stock_id=$Stock['Stock']['id'];
              $stock_damaged_quantity=$Stock['Stock']['quantity'];
              $date=$SalesReturnItem[0]['SalesReturn']['date'];
              $StockController = new StockController;
              $remark='SalesReturn --'.$value['SalesReturn']['invoice_no'].'('.$quantity.')';
              $stock_damaged_quantity+=$quantity;
              $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_damaged_quantity,$date,$user_id,$remark);
              if($Stock_function_return['result']!='Success')
                throw new Exception($Stock_function_return['result']);
            }
          }
          $datasource_Stock->commit();
          $datasource_Journal->begin();
          $debit=$sale_return_account_head_id;
          $exe_id=$SalesReturnItem['0']['SalesReturnItem']['sale_executive_id'];
          //pr($exe_id);
         // exit;
          $executive_id=$exe_id;
          $credit=$SalesReturnItem['0']['SalesReturn']['account_head_id'];
          $account_head_id=$SalesReturnItem[0]['SalesReturn']['account_head_id'];
          $Customer=$this->Customer->findByAccountHeadId($account_head_id);
          $tax_amount=floatval($tax);
          $discount=$SalesReturnItem['0']['SalesReturn']['discount'];
          $work_flow='Sales Return';
          $invoice_no=$SalesReturnItem['0']['SalesReturn']['invoice_no'];
          $date=$SalesReturnItem['0']['SalesReturn']['date'];
          $remarks='SalesReturn Invoice No :'.$invoice_no;
          $route_id=$this->Customer->field('route_id',array('Customer.account_head_id'=>$account_head_id));
          $AccountingsController = new AccountingsController;
          $voucher_no=$AccountingsController->GetVoucherNo();
          $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$net_value,$date,$remarks,$work_flow,$user_id,$voucher_no,'','','',$executive_id,$route_id);
          if($Accountings_function_return['result']!='Success')
            throw new Exception($Stock_function_return['message']);
          if($stock_value)
          {
            //$debit=$sale_return_account_head_id;
           // $credit=$stock_account_head_id;
            $credit=$sale_return_account_head_id;
                $debit=$stock_account_head_id;
            $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$stock_value,$date,$remarks,$work_flow,$user_id,$voucher_no,'','','',$executive_id,$route_id);
            if($Accountings_function_return['result']!='Success')
              throw new Exception($Accountings_function_return['message']);
          }
          if($tax_amount)
          {
            $debit=$tax_on_sale_account_head_id;
            $credit=$account_head_id;
            $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$tax_amount,$date,$remarks,$work_flow,$user_id,$voucher_no,'','','',$executive_id,$route_id);
            if($Accountings_function_return['result']!='Success')
              throw new Exception($Accountings_function_return['message']);
          }
          {
            if($discount>0)
            {
              $debit=$SalesReturnItem['0']['SalesReturn']['account_head_id'];
              $credit=$discount_received_account_head_id;
            }
            else
            {
              $discount=$discount*-1;
              $debit=$discount_paid_account_head_id; 
              $credit=$SalesReturnItem['0']['SalesReturn']['account_head_id'];
            }
            $AccountingsController = new AccountingsController;
            $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$discount,$date,$remarks,$work_flow,$user_id,$voucher_no,'','','',$executive_id,$route_id);
            if($Accountings_function_return['result']!='Success')
              throw new Exception($Stock_function_return['message']);
          }
//$credit= 1;
// $debit=$SalesReturnItem['0']['SalesReturn']['account_head_id'];
// $amount = $data['refund'];
// $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow_cash,$user_id,$voucher_no);
// if($Accountings_function_return['result']!='Success')
//   throw new Exception($Stock_function_return['message']);
          $datasource_Journal->commit();
        }
        $datasource_SalesReturn->commit();
        $datasource_SalesReturnItem->commit();
        $datasource_ProductBonusDetail->commit();
        $datasource_ExecutiveBonusDetail->commit();
        $datasource_ExecutiveBonus->commit();
        $return['result']='Success';
        $this->Session->setFlash(__($return['result']));
        return $this->redirect(array('controller' => 'Sale', 'action' => 'SalesReturn',$id)); 
      } 
      catch (Exception $e) 
      {
        $datasource_SalesReturn->rollback();
        $datasource_SalesReturnItem->rollback();
        $datasource_ProductBonusDetail->rollback();
        $datasource_ExecutiveBonusDetail->rollback();
        $datasource_ExecutiveBonus->rollback();
        $datasource_Stock->rollback();
        $datasource_Journal->rollback();
        $return['result']=$e->getMessage();
        $this->Session->setFlash(__($return['result']));
        $this->redirect( Router::url( $this->referer(), true ) );
      }
    }
  }
  public function sale_return_item_insertfree($sales_return_items,$sales_return_id)
  {
    try {
      if($sales_return_items)
      {
        for ($i=0; $i <count($sales_return_items['product_id']) ; $i++) {
          $item_quantity=$sales_return_items['quantity'][$i];
          $item_freequantity=$sales_return_items['free_qty'][$i];
          $unit_price=$sales_return_items['unit_price'][$i];
          $UnitLevelConvert=$this->UnitLevelConvert($sales_return_items['product_id'][$i],$sales_return_items['sale_unit_level'][$i]);
// $sale_unit_level=$UnitLevelConvert['sale_unit_level'];
          $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
          $product_unit_level=$UnitLevelConvert['sale_unit_level'];
// $quantity_mode="Pieces";
          $sale_unit_level=$sales_return_items['sale_unit_level'][$i];
          $quantity=$item_quantity;
          $freequantity=$item_freequantity;
          if($sale_unit_level != 1){
            if($sale_unit_level == 2){
              $quantity=$no_of_piece_per_unit*$item_quantity;
              $freequantity=$no_of_piece_per_unit*$item_freequantity;
              $unit_price=$unit_price/$no_of_piece_per_unit;
// $quantity_mode="Cases";
            }
          }  

// for ($i=0; $i <count($sales_return_items['product_id']) ; $i++) {
          $SalesReturnItem_data=[
          'product_id'=>$sales_return_items['product_id'][$i],
          'sales_return_id'=>$sales_return_id,
          'invoice_no'=>$sales_return_items['invoice_no_list'][$i],
          'invoice_price'=>$sales_return_items['invoice_price'][$i],
          'unit_price'=>$unit_price,
          'quantity_mode'=>$sale_unit_level,
// 'quantity'=>$sales_return_items['quantity'][$i],
          'quantity'=>$quantity,
          'net_value'=>$sales_return_items['net_value'][$i],
          'tax'=>$sales_return_items['tax'][$i],
          'tax_amount'=>$sales_return_items['tax_amount'][$i],
          'total'=>$sales_return_items['row_total'][$i],
          'free_qty'=>$freequantity,
          ];
          $this->SalesReturnItem->create();
          if(!$this->SalesReturnItem->save($SalesReturnItem_data)) 
          {
            $errors = $this->SalesReturnItem->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
// }
        }
        $return['result']='Success';
      }
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
    }
    return $return;
  }
  public function sale_return_item_insert($sales_return_items,$sales_return_id)
  {
    try {
      if($sales_return_items)
      {
        $bonus_amount_total=0;
        $SalesReturn=$this->SalesReturn->findById($sales_return_id);
      $reference_no=$SalesReturn['SalesReturn']['invoice_no'];
        for ($i=0; $i <count($sales_return_items['product_id']) ; $i++) {
          $sales_return_invoice_no=$sales_return_items['invoice_no'];
          $sale_invoice_no=$sales_return_items['invoice_no_list'][$i];
          $sale_executive_id=0;
            $route_id=$this->Customer->field('Customer.route_id',array('Customer.account_head_id'=>$sales_return_items['account_head_id']));
           $executive_id=0;
            if($route_id)
            {
            $executive_id=$this->ExecutiveRouteMapping->field('ExecutiveRouteMapping.executive_id',array('ExecutiveRouteMapping.route_id'=>$route_id));
            if(!$executive_id)
            {
      $executive_id=0;
            }
            }
           $sale_id=$this->Sale->field('Sale.id',array('Sale.invoice_no'=>$sale_invoice_no));
          $sale_warehouse_id=$this->SaleItem->field('SaleItem.warehouse_id',array('SaleItem.sale_id'=>$sale_id));
           $type=$sales_return_items['type_id'][$i];
        if($type=="Damage")
        {
           if($sales_return_items['product_id'][$i]==1)
           {
          $warehouse_id=$sales_return_items['warehouse_id_hidden'];
           }
           else
           {
            $warehouse_id=$sales_return_items['damage_warehouse_id_hidden'];
           }
        }
        else
        {
            $warehouse_id=$sales_return_items['warehouse_id_hidden'];
        }
          if(!empty($sale_invoice_no))
          {
            $executive_id=$this->Sale->field('Sale.executive_id',array('Sale.invoice_no'=>$sale_invoice_no));
          }
          $item_quantity=$sales_return_items['quantity'][$i];
          $unit_price=$sales_return_items['invoice_price'][$i];
          $unit_id=$sales_return_items['unit_id'][$i];
          $UnitLevelConvert=$this->UnitLevelConvert($sales_return_items['product_id'][$i],$unit_id);
          $no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
          $product_unit_level=$UnitLevelConvert['sale_unit_level'];
          $sale_unit_level=$sales_return_items['sale_unit_level'][$i];
          $quantity=$item_quantity;
          if($sale_unit_level != 1){
            if($sale_unit_level == 2){
              $quantity=$no_of_piece_per_unit*$item_quantity;
              $unit_price=$unit_price/$no_of_piece_per_unit;
            }
          }  
          $net_value=$sales_return_items['net_value'][$i];
          $single_item_bonus_amount=$sales_return_items['single_item_bonus_amount'][$i];
          $bonus_return_amount=$single_item_bonus_amount*$quantity;
             $executive_rate = $this->Product->field(
                  'Product.wholesale_price',
                  array('Product.id ' => $sales_return_items['product_id'][$i]));
// for ($i=0; $i <count($sales_return_items['product_id']) ; $i++) {
          $SalesReturnItem_data=[
          'product_id'=>$sales_return_items['product_id'][$i],
          'sales_return_id'=>$sales_return_id,
          'sale_executive_id'=>$executive_id,
          'warehouse_id'=>$warehouse_id,
          'invoice_no'=>$sale_invoice_no,
          'invoice_price'=>$sales_return_items['invoice_price'][$i],
          'unit_id'=>$unit_id,
         'type'=>$sales_return_items['type_id'][$i],
          'unit_price'=>$unit_price,
          'quantity_mode'=>$sale_unit_level,
          'quantity'=>$quantity,
          'net_value'=>$net_value,
          'bonus_return_amount'=>$bonus_return_amount,
          'tax'=>$sales_return_items['tax'][$i],
          'tax_amount'=>$sales_return_items['tax_amount'][$i],
          'total'=>$sales_return_items['row_total'][$i],
          ];
          $this->SalesReturnItem->create();
          if(!$this->SalesReturnItem->save($SalesReturnItem_data)) 
          {
            $errors = $this->SalesReturnItem->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }  
         $bonus_amount=($sales_return_items['invoice_price'][$i]-$executive_rate)*$quantity;
          $sales_return_item_id=$this->SalesReturnItem->getLastInsertId();
          if($bonus_amount>0)
            {
        $bonus_amount_total=$bonus_amount_total+$bonus_amount;
         $ProductBonus_data=[
         'date'=>date('Y-m-d',strtotime($sales_return_items['date'])),
         'executive_id'=>$executive_id,
          'product_id'=>$sales_return_items['product_id'][$i],
          'sales_return_id'=>$sales_return_id,
          'sales_return_item_id'=>$sales_return_item_id,
          'unit_price'=>$sales_return_items['invoice_price'][$i],
          'executive_rate'=>$executive_rate,
          'quantity'=>$quantity,
          'bonus_return_amount'=>$bonus_amount,
          ];
          $this->ProductBonusDetail->create();
          if(!$this->ProductBonusDetail->save($ProductBonus_data))
          {
            $errors = $this->ProductBonusDetail->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
        }
        }
                if($bonus_amount_total>0)
                {
                $ProductBonus_data=[
                'executive_id'=>$executive_id,
                'date'=>date('Y-m-d',strtotime($sales_return_items['date'])),
                'work_flow'=>"SALE RETURN",
                'reference_no'=>$reference_no,
                'bonus_return_amount'=>$bonus_amount_total,
                ];
                $this->ExecutiveBonusCalculation->create();
                if(!$this->ExecutiveBonusCalculation->save($ProductBonus_data))
                {
                  $errors = $this->ExecutiveBonusCalculation->validationErrors;
                  foreach ($errors as $key => $value) {
                    throw new Exception($value[0], 1);
                  }
                }
              }
      }
        $return['result']='Success';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
    }
    return $return;
  }
  public function sale_return_rollback($id)
  {
    $user_id=1;
    $datasource_SalesReturn = $this->SalesReturn->getDataSource();
    $datasource_Stock = $this->Stock->getDataSource();
    $datasource_Journal = $this->Journal->getDataSource();
    $datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
    $datasource_ExecutiveBonusDetail = $this->ExecutiveBonusDetail->getDataSource();
    $datasource_ExecutiveBonus = $this->ExecutiveBonus->getDataSource();
    try {
      $datasource_SalesReturn->begin();
      $datasource_Stock->begin();
      $datasource_Journal->begin();
      $datasource_ProductBonusDetail->begin();
      $datasource_ExecutiveBonusDetail->begin();
      $datasource_ExecutiveBonus->begin();
      $SalesReturn=$this->SalesReturn->findById($id);
      if(!$SalesReturn)
        throw new Exception("Empty SalesReturns", 1);
      $this->SalesReturn->id=$id;
      if(!$this->SalesReturn->saveField('status','1'))
        throw new Exception('Error While Updating', 1);
      $sales_return_invoice_no=$SalesReturn['SalesReturn']['invoice_no'];
      foreach ($SalesReturn['SalesReturnItem'] as $key => $value) {
        $quantity = $value['quantity'];
        $product_id=$value['product_id'];
        $warehouse_id=1;
        $Stock = $this->Stock->find('first',array(
          'conditions'=>array(
            'product_id'=>$product_id,
            'warehouse_id'=>$warehouse_id,
            )
          ));
        if(!$Stock)
          throw new Exception("Empty Stock", 1);
        $stock_id=$Stock['Stock']['id'];
        $stock_quantity=$Stock['Stock']['quantity'];
        $date=date('Y-m-d');
        $remark='Change SalesReturn Order --'.$id.'('.$quantity.')';
        $stock_quantity-=$quantity;
        $StockController = new StockController;
        $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
        if($Stock_function_return['result']!='Success')
          throw new Exception($Stock_function_return['result']);
      }
      $Journal=$this->Journal->find('all',array(
        'conditions'=>array(
          'remarks'=>'SalesReturn Invoice No :'.$SalesReturn['SalesReturn']['invoice_no'],
          ),
        'fields'=>array(
          'Journal.*',
          )
        ));
      foreach ($Journal as $key => $value) {
        if(!$this->Journal->delete($value['Journal']['id']))
          throw new Exception("Error Processing Journal deletion", 1);
      }
      $ProductBonusDetail=$this->ProductBonusDetail->find('all',array(
        'conditions'=>array(
          'sales_return_id'=>$id,
          ),
        'fields'=>array(
          'ProductBonusDetail.*',
          )
      ));
      foreach ($ProductBonusDetail as $key => $value) {
        if(!$this->ProductBonusDetail->delete($value['ProductBonusDetail']['id']))
          throw new Exception("Error Processing Product Bonus Detail deletion", 1);
      }
      $ExecutiveBonusDetail=$this->ExecutiveBonusDetail->find('all',array(
        'conditions'=>array(
          'credit_note_no'=>$sales_return_invoice_no,
          ),
        'fields'=>array(
          'ExecutiveBonusDetail.*',
          )
      ));
      $total_returned_bonus=0;
      // foreach ($ExecutiveBonusDetail as $key => $value) {
        
      // }
      if(!empty($ExecutiveBonusDetail))
      {
        foreach ($ExecutiveBonusDetail as $key => $value) {
          $executive_id=$value['ExecutiveBonusDetail']['executive_id'];
          $total_returned_bonus+=$value['ExecutiveBonusDetail']['bonus_return_amount'];
          if(!$this->ExecutiveBonusDetail->delete($value['ExecutiveBonusDetail']['id']))
            throw new Exception("Error Processing Product Bonus Detail deletion", 1);
        }
        $staff_id=$this->Executive->field('Executive.staff_id',array('Executive.id'=>$executive_id));
        $ExecutiveBonus=$this->ExecutiveBonus->findByExecutiveIdAndMonthYear($executive_id,date('m-Y'));
        if(!$ExecutiveBonus)
        {
          $this->ExecutiveBonus->create();
          $ExecutiveBonus_data=[
            'executive_id'=>$executive_id,
            'staff_id'=>$staff_id,
            'month_year'=>date('m-Y'),
            'amount'=>0,
          ];
          if(!$this->ExecutiveBonus->save($ExecutiveBonus_data)) { $errors = $this->ExecutiveBonus->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }
          $ExecutiveBonus=$this->ExecutiveBonus->findById($this->ExecutiveBonus->getLastInsertId());
        }
        $this->ExecutiveBonus->id=$ExecutiveBonus['ExecutiveBonus']['id'];
        $this->ExecutiveBonus->saveField('amount',$this->ExecutiveBonus->field('amount')+$total_returned_bonus);
      }
      
      $datasource_SalesReturn->commit();
      $datasource_Stock->commit();
      $datasource_Journal->commit();
      $datasource_ProductBonusDetail->commit();
      $datasource_ExecutiveBonusDetail->commit();
      $datasource_ExecutiveBonus->commit();
      $return['result']='Success';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
      $datasource_SalesReturn->rollback();
      $datasource_Stock->rollback();
      $datasource_Journal->rollback();
      $datasource_ProductBonusDetail->rollback();
      $datasource_ExecutiveBonusDetail->rollback();
      $datasource_ExecutiveBonus->rollback();
    }
    return $return;
  }
  public function get_product_sale_item_detials_ajax($sale_id=null,$product_id=null)
  {
    $data=$this->request->data;
    $return['result']='Error';
    $product_id=$data['product_id'];
    $sale_id=$data['sale_id'];  
    $conditions=[];
    $joins=[];
    $fields=[
    'Product.mrp',
    'Product.no_of_piece_per_unit',
    'Product.tax',
    'Product.wholesale_price',
    ];
    if($sale_id)
    {
      $Sales_details=$this->Sale->findByInvoiceNo($sale_id);
      $conditions['SaleItem.sale_id']=$Sales_details['Sale']['id'];
      $joins=[
      array(
        'table'=>'sale_items',
        'alias'=>'SaleItem',
        'type'=>'INNER',
        'conditions'=>array('SaleItem.product_id=Product.id')
        )
      ];
      $fields=[
      'SaleItem.quantity_mode',
      'SaleItem.unit_price',
      'SaleItem.tax',
      'SaleItem.quantity',
      'SaleItem.free_qty',
      'Product.mrp',
      'Product.wholesale_price',
      'Product.no_of_piece_per_unit',
      'Product.tax',
      ];
    }
    if($product_id)
    {

      $conditions['Product.id']=$product_id;
      $Product=$this->Product->find('first',array(
        'joins'=>$joins,
        'fields'=>$fields,
        'conditions'=>$conditions,
        ));

      $return['Product']['no_of_piece_per_unit']=$Product['Product']['no_of_piece_per_unit'];
      $return['Product']['level'] =1;
      if($sale_id)
      {

        $return['Product']['mrp']=$Product['SaleItem']['unit_price'];
        $return['Product']['quantity']=$Product['SaleItem']['quantity'];
        $return['Product']['freequantity']=$Product['SaleItem']['free_qty'];

        $SalesReturnItem=$this->SalesReturnItem->find('all',array(
          'conditions'=>array(
            'SalesReturnItem.invoice_no'=>$sale_id,
            'SalesReturnItem.product_id'=>$product_id,
            ),
          'fields'=>array(
            'SalesReturnItem.quantity',
//'SalesReturnItem.free_qty',
            )
          ));
        if($SalesReturnItem)
        {
          foreach ($SalesReturnItem as $key => $value) {
            $return['Product']['quantity']-=$value['SalesReturnItem']['quantity'];
// $return['Product']['freequantity']-=$value['SalesReturnItem']['free_qty'];
          }
        }

        $return['Product']['tax']=$Product['SaleItem']['tax'];
        if($Product['SaleItem']['quantity_mode']=='Cases'){
          $return['Product']['level'] =2;
        }
      }
      else
      {

        $return['Product']['mrp']=$Product['Product']['wholesale_price'];
        $return['Product']['quantity']=0;
        $return['Product']['freequantity']=0;
        $return['Product']['tax']=$Product['Product']['tax'];
      }

      $return['result']='Success';
    }
    else
    {
      $return['Product']['level'] =1;
      $return['Product']['no_of_piece_per_unit']=1;
      $return['Product']['mrp']=0;
      $return['Product']['quantity']=0;
      $return['Product']['freequantity']=0;
      $return['Product']['tax']=0;
      $return['result']='Empty';
    }
    echo json_encode($return);
    exit;
  }
  public function get_product_sale_item_detials_ajax_unit_change($sale_id=null,$product_id=null)
  {
    $data=$this->request->data;
    $return['result']='Error';
    $product_id=$data['product_id'];
    $sale_id=$data['sale_id'];  
    $conditions=[];
    $joins=[];
    $fields=[
    'Product.mrp',
    'Product.no_of_piece_per_unit',
    'Product.tax',
    'Product.wholesale_price',
    ];
    if($sale_id)
    {
      $Sales_details=$this->Sale->findByInvoiceNo($sale_id);
      $conditions['SaleItem.sale_id']=$Sales_details['Sale']['id'];
      $joins=[
      array(
        'table'=>'sale_items',
        'alias'=>'SaleItem',
        'type'=>'INNER',
        'conditions'=>array('SaleItem.product_id=Product.id')
        ),
      array(
        'table'=>'product_bonus_details',
        'alias'=>'ProductBonusDetail',
        'type'=>'INNER',
        'conditions'=>array('ProductBonusDetail.sale_item_id=SaleItem.id')
        )
      ];
      $fields=[
      'SaleItem.quantity_mode',
      'SaleItem.unit_price',
      'SaleItem.tax',
      'SaleItem.quantity',
      'SaleItem.free_qty',
      'ProductBonusDetail.quantity',
      'ProductBonusDetail.bonus_amount',
      'ProductBonusDetail.bonus_percentage',
      'Product.mrp',
      'Product.no_of_piece_per_unit',
      'Product.tax',
      'Product.wholesale_price',
      ];
    }

    if($product_id)
    {

      $conditions['Product.id']=$product_id;
      $Product=$this->Product->find('first',array(
        'joins'=>$joins,
        'fields'=>$fields,
        'conditions'=>$conditions,
        ));
      if(empty($sale_id))
      {
               $return['Product']['single_item_bonus_amount']=0;
                     $return['Product']['bonus_percentage']=0;
      }
      else
      {
        $return['Product']['bonus_percentage']=$Product['ProductBonusDetail']['bonus_percentage'];
      if(!empty($Product['ProductBonusDetail']['bonus_amount']))
        $return['Product']['single_item_bonus_amount']=number_format(($Product['ProductBonusDetail']['bonus_amount']/$Product['ProductBonusDetail']['quantity']),3,'.','');
      }
      $return['Product']['no_of_piece_per_unit']=$Product['Product']['no_of_piece_per_unit'];
      $return['Product']['level'] =1;
      if($sale_id)
      {
        if($Product['SaleItem']['quantity_mode']=="Cases")
        {
          $return['Product']['new_mrp']=$Product['SaleItem']['unit_price']/$Product['Product']['no_of_piece_per_unit'];
        }
        else
        {
          $return['Product']['new_mrp']=$Product['SaleItem']['unit_price'];
        }
        $return['Product']['mrp']=$Product['SaleItem']['unit_price'];
        $return['Product']['quantity']=$Product['SaleItem']['quantity'];
        $return['Product']['freequantity']=$Product['SaleItem']['free_qty'];
        $SalesReturnItem=$this->SalesReturnItem->find('all',array(
          'conditions'=>array(
            'SalesReturnItem.invoice_no'=>$sale_id,
            'SalesReturnItem.product_id'=>$product_id,
            ),
          'fields'=>array(
            'SalesReturnItem.quantity',
            )
          ));
        if($SalesReturnItem)
        {
          foreach ($SalesReturnItem as $key => $value) {
            $return['Product']['quantity']-=$value['SalesReturnItem']['quantity'];
          }
        }

        $return['Product']['tax']=$Product['SaleItem']['tax'];
        if($Product['SaleItem']['quantity_mode']=='Cases'){
          $return['Product']['level'] =2;
        }
      }
      else
      {

        $return['Product']['mrp']=$Product['Product']['wholesale_price'];
        $return['Product']['new_mrp']=$Product['Product']['wholesale_price'];
        $return['Product']['quantity']=0;
        $return['Product']['freequantity']=0;
        $return['Product']['tax']=$Product['Product']['tax'];
      }

      $return['result']='Success';
    }
    else
    {
      $return['Product']['level'] =1;
      $return['Product']['no_of_piece_per_unit']=1;
      $return['Product']['mrp']=0;
      $return['Product']['quantity']=0;
      $return['Product']['freequantity']=0;
      $return['Product']['tax']=0;
      $return['Product']['single_item_bonus_amount']=0;
      $return['result']='Empty';
    }
    echo json_encode($return);
    exit;
  }
  public function Sale_invoice_no_list_by_customer_id($account_head_id)
  {
    $invoice_no_list_array=$this->Sale->find('list',array(
      'conditions'=>[
      'Sale.account_head_id'=>$account_head_id,
      'Sale.status >'=>1,
      ],
      'fields'=>array(
        'Sale.invoice_no',
        'Sale.invoice_no',
        )
      )
    );
    $invoice_no_list_array[0]='General Invoice';
    ksort($invoice_no_list_array);
    return $invoice_no_list_array;
  }
  public function get_product_list_ajax($sale_id=null)
  {
    $conditions=[];
    $joins=[];
    if($sale_id)
    {
      $Sales_details=$this->Sale->findByInvoiceNo($sale_id);
      $conditions['SaleItem.sale_id']=$Sales_details['Sale']['id'];
      $joins=[
      array(
        'table'=>'sale_items',
        'alias'=>'SaleItem',
        'type'=>'INNER',
        'conditions'=>array('SaleItem.product_id=Product.id')
        )
      ];
    }
    $return['result']='Empty';
    $conditions['Product.type']=2;
     $conditions['Product.active']=1;
    $Product=$this->Product->find('list',array(
      'joins'=>$joins,
      'fields'=>array('Product.id','Product.name'),
      'conditions'=>$conditions,
      ));
    if($Product)
    {
      $return['result']='Success';
      $return['options']=$Product;  
    }
    $return['options']['']='Select';
// arsort($return['options']);
    echo json_encode($return);
    exit;
  }
  public function get_invoice_list_ajax($customer_id)
  {
    $invoice_no_list=$this->Sale_invoice_no_list_by_customer_id($customer_id);
    $return['options']=$invoice_no_list;
// arsort($return['options']);
    echo json_encode($return);
    exit;
  }
  public function SaleItem_delete($id)
  {
    $datasource_SaleItem = $this->SaleItem->getDataSource();
    $datasource_Stock = $this->Stock->getDataSource();
    $datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
    try {
      $datasource_SaleItem->begin();
      $datasource_Stock->begin();
      $datasource_ProductBonusDetail->begin();
      $user_id=1;
      $SaleItem=$this->SaleItem->findById($id);
      if($SaleItem['Sale']['status']==3)
      {
        $product_id=$SaleItem['SaleItem']['product_id'];
        $quantity=$SaleItem['SaleItem']['quantity'];
        $remark='Altration Delete Sale :'.$SaleItem['Sale']['invoice_no'];
        $Stock = $this->Stock->findByProductId($product_id);
        $stock_id=$Stock['Stock']['id'];
        $stock_quantity=$Stock['Stock']['quantity'];
        $date=date('Y-m-d');
        $stock_quantity+=$quantity;
        $StockController = new StockController;
        $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
        if($Stock_function_return['result']!='Success')
          throw new Exception($Stock_function_return['result']); 
      }
      $return['total']=$SaleItem['SaleItem']['total'];
      $return['net_value']=$SaleItem['SaleItem']['net_value'];
      $ProductBonusDetail=$this->ProductBonusDetail->findBySaleItemId($id);
      if(!$this->ProductBonusDetail->delete($ProductBonusDetail['ProductBonusDetail']['id']))
        throw new Exception("Error while deleting", 1);
      if(!$this->SaleItem->delete($id))
        throw new Exception("Error while deleting", 1);
      $datasource_SaleItem->commit();
      $datasource_Stock->commit();
      $datasource_ProductBonusDetail->commit();
      $return['result']='Success';
    } catch (Exception $e) {
      $datasource_SaleItem->rollback();
      $datasource_Stock->rollback();
      $datasource_ProductBonusDetail->rollback();
      $return['result']=$e->getMessage();
    }
    echo json_encode($return);
    exit;
  }
  public function SalesReturnItem_delete($id)
  {
    try {
      if(!$this->SalesReturnItem->delete($id))
        throw new Exception("Error while deleting", 1);
      $return['result']='Success';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
    }
    echo json_encode($return);
    exit;
  }
  public function fpdfs($id=null)
  {
    $Sale = $this->Sale->find('first', array(
      "joins" => array(
        array(
          "table" => 'customers',
          "alias" => 'Customer',
          "type" => 'inner',
          "conditions" => array('Customer.account_head_id=Sale.account_head_id'),
          ),
        array(
          "table" => 'customer_types',
          "alias" => 'CustomerType',
          "type" => 'inner',
          "conditions" => array('CustomerType.id=Customer.customer_type_id'),
          ),
// array(
//   "table" => 'states',
//   "alias" => 'State',
//   "type" => 'inner',
//   "conditions" => array('State.id=Customer.state_id'),
// ),
        ),
      'conditions' => array('Sale.id'=>$id),
      'fields' => array(
        'Sale.*',
        'AccountHead.name',
        'CustomerType.name',
        'Customer.place',
        'State.name',
        'State.code',
        'Customer.gstin',
        )
      ));
    $Checkstate=0;
    if($Sale['CustomerType']['name'] != "GENERALCUSTOMER" && $Sale['State']['code'] !="KL")
    {
      $Checkstate=0;
    }
    else
    {
      $Checkstate=1;
    }
    if($Sale['AccountHead']['name'] == "GENERALCUSTOMER"){
      $name=$Sale['Sale']['customer_name'];
    }else{
      $name=$Sale['AccountHead']['name'];
    }
    $invoice_no=$Sale['Sale']['invoice_no'];
    $date=$Sale['Sale']['date_of_delivered'];
    $this->SaleItem->virtualFields = array(
      'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
      );
    $SaleItem = $this->SaleItem->find('all', array(
      'joins'=>array(
        array(
          "table" => 'units',
          "alias" => 'Unit',
          "type" => 'inner',
          "conditions" => array('Unit.id=Product.unit_id'),
          ),
        ),
      'conditions' => array('SaleItem.sale_id'=>$id),
      'fields' => array(
        'SaleItem.*',
        'Product.id',
        'Product.name',
        'Product.hsn_code',
        'Unit.name',
        'product_name'
        )
      ));
// pr($SaleItem); exit;
    function convert_number_to_words($number) {
      $hyphen      = '-';
      $conjunction = ' and ';
      $separator   = ', ';
      $negative    = 'negative ';
      $decimal     = ' point ';
      $dictionary  = array(
        0                   => 'zero',
        1                   => 'one',
        2                   => 'two',
        3                   => 'three',
        4                   => 'four',
        5                   => 'five',
        6                   => 'six',
        7                   => 'seven',
        8                   => 'eight',
        9                   => 'nine',
        10                  => 'ten',
        11                  => 'eleven',
        12                  => 'twelve',
        13                  => 'thirteen',
        14                  => 'fourteen',
        15                  => 'fifteen',
        16                  => 'sixteen',
        17                  => 'seventeen',
        18                  => 'eighteen',
        19                  => 'nineteen',
        20                  => 'twenty',
        30                  => 'thirty',
        40                  => 'fourty',
        50                  => 'fifty',
        60                  => 'sixty',
        70                  => 'seventy',
        80                  => 'eighty',
        90                  => 'ninety',
        100                 => 'hundred',
        1000                => 'thousand',
        1000000             => 'million',
        1000000000          => 'billion',
        1000000000000       => 'trillion',
        1000000000000000    => 'quadrillion',
        1000000000000000000 => 'quintillion'
        );
      if (!is_numeric($number)) {
        return false;
      }
      if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
// overflow
        trigger_error(
          'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
          E_USER_WARNING
          );
        return false;
      }
      if ($number < 0) {
        return $negative . convert_number_to_words(abs($number));
      }
      $string = $fraction = null;
      if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
      }
      switch (true) {
        case $number < 21:
        $string = $dictionary[$number];
        break;
        case $number < 100:
        $tens   = ((int) ($number / 10)) * 10;
        $units  = $number % 10;
        $string = $dictionary[$tens];
        if ($units) {
          $string .= $hyphen . $dictionary[$units];
        }
        break;
        case $number < 1000:
        $hundreds  = $number / 100;
        $remainder = $number % 100;
        $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
        if ($remainder) {
          $string .= $conjunction . convert_number_to_words($remainder);
        }
        break;
        default:
        $baseUnit = pow(1000, floor(log($number, 1000)));
        $numBaseUnits = (int) ($number / $baseUnit);
        $remainder = $number % $baseUnit;
        $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
        if ($remainder) {
          $string .= $remainder < 100 ? $conjunction : $separator;
          $string .= convert_number_to_words($remainder);
        }
        break;
      }
      if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string) $fraction) as $number) {
          $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
      }
      return $string;
    }
    require('fpdf/fpdf.php');
    $pdf = new FPDF('p', 'mm', [297, 210]);
    $Profile=$this->Global_Var_Profile['Profile'];
    $page=1;
    $total_page=1;
    $i=0;
//For Total Page Count
    foreach ($SaleItem as $key => $value) {
      $description = str_replace('"', "'", $value['Product']['name']);
      $description_length=strlen($description);
      $description_words=explode(' ', $description);
      $word_count=count($description_words);
      $k=0;
      $poped_array=[];
      for ($j=0; $j <$word_count; $j++) {
        $poped_array[]=array_pop($description_words);
        $product_name=implode($description_words,' ');
        $product_name_length=strlen($product_name);
        if($product_name_length<=42)
        {
          $poped_array=array_reverse($poped_array);
          $k++;
          $description_words=$poped_array;
          $word_count=count($description_words);
          $poped_array=[];
          $j=0;  
        }
      }
      if($k)
        $k--;
      $i+=$k;
      if($i+$k>=20)
      {
        $total_page++;
        $i=-1;
      }
      $i++;
    }
    $igst_x_position=0;
    if($Checkstate==0)
    {
      $igst_x_position=20;
    } 
    function header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position) 
    {
      $pdf->AddPage();
      $pdf->Image('profile/'.$Profile['logo'],5,5,-600);
      $image_line_width=40;
      $invoice_x_starting=$image_line_width;
$pdf->Line($image_line_width, 1,$image_line_width, 35); // vertical line
$pdf->SetFont('Arial','B',12);
$pdf->SetTextColor(0,0,100);
$pdf->rect(1, 1, 208, 295);
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos=0;
$pdf->Cell(0,$invoice_pos,'COMMON INVOICE ',0,0,'C');
$pdf->Cell(0,$invoice_pos,'Page : '.$page.'/'.$total_page,0,0,'R');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
$pdf->Cell(0,$invoice_pos,$Profile['company_name'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=15;
$pdf->SetFont('Arial','B',10);
$pdf->Cell(0,$invoice_pos,$Profile['address_line_1'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
$pdf->Cell(0,$invoice_pos,$Profile['address_line_2'].' ,'.$Profile['mobile'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
$pdf->SetFont('Arial','U',10);
$pdf->Cell(0,$invoice_pos,$Profile['mail_address'],0,0,'C');
$pdf->SetXY($invoice_x_starting, 5);
$invoice_pos+=10;
$pdf->Cell(0,$invoice_pos,$Profile['web_site_adddress'],0,0,'C');
$gst_details_y_staring=35;
$pdf->Line(1, $gst_details_y_staring,209, $gst_details_y_staring); // horizontal line
$pdf->SetFont('Arial','B',8);
$gst_pos=$gst_details_y_staring+3;
$gst_line_width=120;
$pdf->Line($gst_line_width, $gst_details_y_staring,$gst_line_width, $gst_details_y_staring+17); // vertical line
//$pdf->text(2,$gst_pos,'Gstin Number : '.$Profile['gst_tin_code']);
$pdf->text($gst_line_width+1,$gst_pos,'Transportation Mode:(Apply for Supply of Goods only)');
$gst_pos+=4;
//$pdf->text(2,$gst_pos-3,'Tax is Payable on Reverse Charge : (Yes/No)');
$pdf->text($gst_line_width+1,$gst_pos,'Veh.No :');
$gst_pos+=4;
$pdf->text(2,$gst_pos-8,'Invoice Serial Number : '.$Sale['Sale']['invoice_no']);
$pdf->text($gst_line_width+1,$gst_pos,'Date & Time of Supply:');
$gst_pos+=4;
if(!$Sale['Sale']['date_of_delivered'])
  $invoice_date=$Sale['Sale']['date_of_order'];
else
  $invoice_date=$Sale['Sale']['date_of_delivered'];
$pdf->text(2,$gst_pos-8,'Invoice Date : '.date("d-m-Y",strtotime($invoice_date)));
$pdf->text($gst_line_width+1,$gst_pos,'Place Of Supply:');
$pdf->Line(1, $gst_details_y_staring+17,209, $gst_details_y_staring+17); // horizontal line
$pdf->SetXY(0, 5);
$pdf->Line(100, $gst_details_y_staring+17,100, $gst_details_y_staring+17+21+5); // vertical line
$pdf->text(35,$gst_pos+5,'Detail Of Receiver (Billed to)');
$pdf->SetXY(100, 27);
$pdf->Cell(0,$gst_pos+5,'Detail Of Consignee (Shipped to)',0,0,'C');
$pdf->Line(1, $gst_details_y_staring+17+5,209, $gst_details_y_staring+17+5); // horizontal line
$gst_line_width=100;
$customer_detail_startin=$gst_details_y_staring+25;
$Consignee_lin_width=100+1;
$pdf->text(2,$customer_detail_startin,'Name : '.$name);
$pdf->text($Consignee_lin_width,$customer_detail_startin,'Name :');
$customer_detail_startin+=4;
$pdf->text(2,$customer_detail_startin,'Address : '.$Sale['Customer']['place']);
$pdf->text($Consignee_lin_width,$customer_detail_startin,'Address :');
$customer_detail_startin+=4;
$pdf->text(2,$customer_detail_startin,'Phone number :'.$Sale['Customer']['mobile']);
$pdf->text($Consignee_lin_width,$customer_detail_startin,'Phone number :');
$customer_detail_startin+=4;
$pdf->text(2,$customer_detail_startin,'State :'.$Sale['State']['name']);
$pdf->text($Consignee_lin_width,$customer_detail_startin,'State :');
$customer_detail_startin+=4;
$pdf->text(2,$customer_detail_startin,'State Code :'.$Sale['State']['code']);
$pdf->text($Consignee_lin_width,$customer_detail_startin,'State Code :');
$pdf->Line(1, $gst_details_y_staring+17+5+21,209, $gst_details_y_staring+17+5+21); // horizontal line
$item_table_head_y_start=$customer_detail_startin+2;
// table head start
$table_length=135;
$pdf->text(2,$item_table_head_y_start+3,'SL');
$pdf->text(2,$item_table_head_y_start+7,'No');
$table_column=7;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length); // vertical line
$pdf->text($table_column+20,$item_table_head_y_start+5,'Description of Goods');
$table_column+=70+$igst_x_position;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
//$pdf->text($table_column+2+2,$item_table_head_y_start+3,'HSN');
//$pdf->text($table_column+2+1.5,$item_table_head_y_start+6,'CODE');
//$pdf->text($table_column+2+2,$item_table_head_y_start+9,'(GST)');
$table_column+=16;
$pdf->Line($table_column-10, $item_table_head_y_start,$table_column-10, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column-5,$item_table_head_y_start+6,'Qty');
$table_column+=7;
$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column+5,$item_table_head_y_start+6,'UOM');
$table_column+=14;
$pdf->Line($table_column+5, $item_table_head_y_start,$table_column+5, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column+14,$item_table_head_y_start+6,'Rate');
$table_column+=15;
$pdf->Line($table_column+15, $item_table_head_y_start,$table_column+15, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column+24,$item_table_head_y_start+6,'Discount');
$table_column+=25;
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
//$pdf->text($table_column+3+3,$item_table_head_y_start+4,'Taxable');
//$pdf->text($table_column+2+1+3,$item_table_head_y_start+8,'Value');
$table_column+=14;
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
//$pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
if($Checkstate==1)
{
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,195, $gst_details_y_staring+17+5+21+4); // horizontal line
// //$pdf->text($table_column+6,$item_table_head_y_start+3,'CGST');
// $pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
// $table_column+=8;
// $pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
// $pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
// $table_column+=15;
// $pdf->Line($table_column, $item_table_head_y_start,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
// //$pdf->text($table_column+6,$item_table_head_y_start+3,'SGST');
// $pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
// $table_column+=8;
// $pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
// $pdf->text($table_column+2,$item_table_head_y_start+8,'Amount');
}
else
{
// $pdf->Line($table_column, $gst_details_y_staring+17+5+21+4,172+$igst_x_position, $gst_details_y_staring+17+5+21+4); // horizontal line
// $pdf->text($table_column+6,$item_table_head_y_start+3,'IGST');
// $pdf->text($table_column+1,$item_table_head_y_start+8,'Rate');
// $table_column+=8;
// $pdf->Line($table_column, $item_table_head_y_start+4,$table_column, $item_table_head_y_start+$table_length+5); // vertical line
// $pdf->text($table_column+1,$item_table_head_y_start+8,'Amount');
}
$table_column+=14;
$pdf->Line($table_column-5, $item_table_head_y_start,$table_column-5, $item_table_head_y_start+$table_length+5); // vertical line
$pdf->text($table_column+8,$item_table_head_y_start+6,'Total');
$pdf->Line(1, $gst_details_y_staring+17+5+21+10,209, $gst_details_y_staring+17+5+21+10); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length,209, $item_table_head_y_start+$table_length); // horizontal line
$pdf->Line(1, $item_table_head_y_start+$table_length+5,209, $item_table_head_y_start+$table_length+5); // horizontal line
$pdf->text(12,$item_table_head_y_start+$table_length+3,'Total');
}
function footer($pdf,$data,$Sale)
{
  $footer_start_y=218;
$pdf->Line(1, $footer_start_y,209, $footer_start_y); // horizontal line
$invoce_word_width=141;
$grand_total_length=30;
$pdf->Line($invoce_word_width, $footer_start_y,$invoce_word_width, $footer_start_y+$grand_total_length+4); // vertical line
$pdf->text(50,$footer_start_y+3,'Invoice Value (In Words)');
$convert_number_to_words=strtoupper(convert_number_to_words($data['total']));
$pdf->text(5,$footer_start_y+3+10,$convert_number_to_words);
$pdf->Line($invoce_word_width+46, $footer_start_y,$invoce_word_width+46, $footer_start_y+$grand_total_length); // vertical line
$Total_pos=3;
$actual_total=$Sale['Sale']['total'];
$pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Total');
//$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,$data['total']);
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,$actual_total);
$pdf->Line(1, $footer_start_y+4,209, $footer_start_y+4); // horizontal line
$Total_pos+=5;
$pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Freight Charges');
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,'0');
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Looking and packing Charges');
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,'0');
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Insurance Charges');
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,'0');
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Round Off');
//$other_value=round($data['total'])-$data['total'];
$other_value=$Sale['Sale']['other_value'];
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,$other_value);
$pdf->Line($invoce_word_width, $footer_start_y+$Total_pos+1,209, $footer_start_y+$Total_pos+1); // horizontal line
$Total_pos+=5;
$actual_grand_total=$Sale['Sale']['grand_total'];
$pdf->text($invoce_word_width+3,$footer_start_y+$Total_pos,'Invoice Total');
//$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,$data['total']+$other_value);
$pdf->text($invoce_word_width+47,$footer_start_y+$Total_pos,$actual_grand_total);
$pdf->Line(1, $footer_start_y+$grand_total_length,209, $footer_start_y+$grand_total_length); // horizontal line
$tax_subject_y=$footer_start_y+$grand_total_length;
$pdf->Line(1, $tax_subject_y+4,209, $tax_subject_y+4); // horizontal line
$pdf->text(50,$tax_subject_y+3,'Amount of Tax Subject To Reverse Charge');
$pdf->Line($invoce_word_width+46, $tax_subject_y,$invoce_word_width+46, $tax_subject_y+4); // vertical line
$signature_area_y=$tax_subject_y+4;
$signature_length=14;
$signature_width=100;
$pdf->Line(1, $signature_area_y+6,209, $signature_area_y+6); // horizontal line
$pdf->text(10,$signature_area_y+4,'Certified that the Particulars given above are true and correct');
$pdf->SetXY($signature_width, $signature_area_y);
$pdf->Cell(0,5,'Electronic Reference No :',0,0,'C');
$pdf->Line($signature_width, $signature_area_y,$signature_width, $signature_area_y+$signature_length); // vertical line
$pdf->Line(1, $signature_area_y+$signature_length,209, $signature_area_y+$signature_length); // horizontal line
$condition_area_y=$signature_area_y+$signature_length;
$condition_length=30;
$condition_width=100;
$pdf->Line(1, $condition_area_y+6,209, $condition_area_y+6); // horizontal line
$pdf->text(25,$condition_area_y+4,'TERMS & CONDITIONS OF SALE');
$pdf->SetXY($condition_width, $condition_area_y);
$pdf->Cell(0,5,'COMPANY NAME',0,0,'C');
$pdf->SetXY($condition_width, $condition_area_y);
$pdf->text($condition_width+2,$condition_area_y+13,'Signature');
$pdf->Line($condition_width, $condition_area_y+15,209, $condition_area_y+15); // horizontal line
$pdf->text($condition_width+35,$condition_area_y+18,'Autherized Signature');
$pdf->Line($condition_width, $condition_area_y+20,209, $condition_area_y+20); // horizontal line
$pdf->text($condition_width+2,$condition_area_y+24,'Name:');
$pdf->text($condition_width+2,$condition_area_y+24+4,'Designation :');
$pdf->Line($condition_width, $condition_area_y,$condition_width, $condition_area_y+$condition_length); // vertical line
}
header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate,$igst_x_position);
$total=0;
$item_table_head_y_start='85';
$table_length='128';
$cgst_grand_amount=0;
$sgst_grand_amount=0;
$igst_grand_amount=0;
$rate_grand_amount=0;
$discount_grand_amount=0;
$taxable_value_grand_amount=0;
$product_grand_total=0;
$count=count($SaleItem);
$i=0;
// for ($i=0; $i <$count ; $i++) { 
foreach ($SaleItem as $key => $value) {
  $description = str_replace('"', "'", $value['SaleItem']['product_name']);
  $hsn_code=$value['Product']['hsn_code'];
  $qty=floatval($value['SaleItem']['quantity']);
  $uom=$value['Unit']['name'];
  $rate=floatval($value['SaleItem']['unit_price']);
  $rate_grand_amount+=$rate;
  $discount=$value['SaleItem']['discount'];
  $discount_grand_amount+=$discount;
  $taxable_value=floatval($value['SaleItem']['taxable_value']);
  $taxable_value_grand_amount+=$taxable_value;
  $net_value=floatval($value['SaleItem']['net_value']);
  $tax_value=floatval($value['SaleItem']['cgst_amount'])+floatval($value['SaleItem']['sgst_amount'])+floatval($value['SaleItem']['igst_amount']);
  $total+=$net_value+$tax_value;
  $data=array(
    'total'=>$total,
    );
  $product_total=floatval($value['SaleItem']['total']);
  $product_grand_total+=$product_total;
  $cgst_rate=floatval($value['SaleItem']['cgst']);
  $cgst_amount=floatval($value['SaleItem']['cgst_amount']);
  $cgst_grand_amount+=$cgst_amount;
  $sgst_rate=floatval($value['SaleItem']['sgst']);
  $sgst_amount=floatval($value['SaleItem']['sgst_amount']);
  $sgst_grand_amount+=$sgst_amount;
  $igst_rate=floatval($value['SaleItem']['igst']);
  $igst_amount=floatval($value['SaleItem']['igst_amount']);
  $igst_grand_amount+=$igst_amount;
  $item_pos=2;
  $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$key+1);
  $item_pos+=6;
  $description_length=strlen($description);
  $description_words=explode(' ', $description);
  $word_count=count($description_words);
  $k=0;
  $poped_array=[];
  for ($j=0; $j <$word_count; $j++) {
    $product_name=implode($description_words,' ');
    $poped_array[]=array_pop($description_words);
    $product_name_length=strlen($product_name);
    if($product_name_length<=40)
    {
      $pdf->text($item_pos,$item_table_head_y_start+7+(($i+$k)*6),$product_name);
      $poped_array=array_reverse($poped_array);
      $k++;
      $description_words=$poped_array;
      $word_count=count($description_words);
      $poped_array=[];
      $j=0;  
    }
  }
  if($k)
    $k--;
  $item_pos+=70+$igst_x_position;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$hsn_code);
  $item_pos+=16;
  $pdf->text($item_pos-4,$item_table_head_y_start+7+($i*6),$qty);
  $item_pos+=7;
  $pdf->text($item_pos+5,$item_table_head_y_start+7+($i*6),$uom);
  $item_pos+=15;
  $pdf->text($item_pos+12,$item_table_head_y_start+7+($i*6),$rate);
  if($count==$i+1)
  {
    $pdf->text($item_pos+12,$item_table_head_y_start+$table_length+3,$rate_grand_amount);
  }
  $item_pos+=26;
  $pdf->text($item_pos+12,$item_table_head_y_start+7+($i*6),$discount);
  if($count==$i+1)
  {
    $pdf->text($item_pos+12,$item_table_head_y_start+$table_length+3,$discount_grand_amount);
  }
  $item_pos+=24;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$taxable_value);
  if($count==$i+1)
  {
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$taxable_value_grand_amount);
  }
  $item_pos+=15;
  if($Checkstate==1)
  {
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$cgst_rate);
// $item_pos+=7;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$cgst_amount);
// if($count==$i+1)
// {
//   $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$cgst_grand_amount);
// }
// $item_pos+=15;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$sgst_rate);
// $item_pos+=8;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$sgst_amount);
// if($count==$i+1)
// {
// $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$sgst_grand_amount);
// }
  }
  else
  {
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_rate);
// $item_pos+=7;
// $pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_amount);
// if($count==$i+1)
// {
//   $pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$igst_grand_amount);
// }
  }
  $item_pos+=15;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_rate);
  $item_pos+=2;
//$pdf->text($item_pos,$item_table_head_y_start+7+($i*6),$igst_amount);
  $pdf->text($item_pos-7,$item_table_head_y_start+7+($i*6),$product_total);
  if($count==$i+1)
  {
//$pdf->text($item_pos,$item_table_head_y_start+$table_length+3,$igst_grand_amount);
    $pdf->text($item_pos-7,$item_table_head_y_start+$table_length+3,$product_grand_total);
  }
  $i+=$k;
  $count+=$k;
  if($i+$k>=20)
  {
    footer($pdf,$data,$Sale);
    $page++;
    header_section($pdf,$Sale,$Profile,$total_page,$page,$name,$Checkstate);
    $i=-1;
    $count-=20;
  }
  $i++;
// }
}
footer($pdf,$data,$Sale);
$pdf->Output();
exit;
}
public function fpdf_old($id=null)
{
  $Sale = $this->Sale->find('first', array(
    "joins" => array(
      array(
        "table" => 'customers',
        "alias" => 'Customer',
        "type" => 'inner',
        "conditions" => array('Customer.account_head_id=Sale.account_head_id'),
        ),
      array(
        "table" => 'customer_types',
        "alias" => 'CustomerType',
        "type" => 'inner',
        "conditions" => array('CustomerType.id=Customer.customer_type_id'),
        ),
      ),
    'conditions' => array('Sale.id'=>$id),
    'fields' => array(
      'Sale.*',
      'AccountHead.name',
      'CustomerType.name',
      )
    ));
  $name=$Sale['AccountHead']['name'];
  $invoice_no=$Sale['Sale']['invoice_no'];
  $date=$Sale['Sale']['date_of_delivered'];
  $SaleItem = $this->SaleItem->find('all', array(
    'conditions' => array('SaleItem.sale_id'=>$id),
    'fields' => array(
      'SaleItem.*',
      'Product.id',
      'Product.name',
      )
    ));
  require('fpdf/fpdf.php');
  $pdf = new FPDF('p', 'mm', [297, 210]);
  function header_section($pdf,$name,$date,$invoice_no) {
    $pdf->SetFont('Arial','B',8.4);
    $pdf->AddPage();
    $pdf->Image('img/logo.png',5,5,-1100);
// $pdf->Text(175, 5, 'TIN PIN: 32111247519');
// $pdf->Text(175, 9, "CFT No.32111247519c");
// $pdf->Text(120, 13, "THE KERALA VALUE ADDED TAX RULES 2005 FORM NO.8B");
// $pdf->Text(107, 17, "(For Customers when input tax credit is not required) [See Rule 58(10)]");
    $pdf->SetFont('Arial','B',20);
    $pdf->SetTextColor(0,0,100);
    $pdf->Text(23, 13, "ICRESP");
    $pdf->SetFont('Arial','B',12);
    $pdf->Text(23, 18, "INTELLIGENT CUSTOMIZED RESOURCE PLANNER");
    $pdf->SetTextColor(10,10,10);
// $pdf->Text(10, 20, "PH : 0496 2589933");
    $pdf->SetFont('Arial','B',8);
    $pdf->Text(10, 26, "Invoice No : ".$invoice_no);
    $pdf->Text(10, 31, "Name & Address of the Purchaser : ".$name );
    $pdf->SetFont('Arial','B',12);
// $pdf->Text(70, 26, "RETAIL INVOICE - CASH/CREDIT");
    $pdf->SetFont('Arial','B',9);
    $pdf->Text(150, 26, "Date : ".date('d-M-Y',strtotime($date)));
    $pdf->rect(5, 33, 200, 200);
    $first_table_x=33;
    $pdf->SetFont('Arial','B',7);
    $slno_x=6;
    $pdf->Text($slno_x, $first_table_x+5, "SlNo");
$pdf->Line($slno_x+7, $first_table_x,$slno_x+7, 200); // vertical line
$item_x=$slno_x+50;
$item_line_x=$item_x+60;
$pdf->Text($item_x, $first_table_x+5, "Commodity/Item");
$pdf->Line($item_line_x, $first_table_x,$item_line_x, 200); // vertical line
$tax_x=$item_line_x+2;
$tax_line_x=$tax_x+15;
$pdf->Text($tax_x, $first_table_x+5, "Tax");
$pdf->Line($tax_line_x, $first_table_x,$tax_line_x, 200); // vertical line
$unit_price_x=$tax_line_x+2;
$unit_price_line_x=$unit_price_x+13;
$pdf->Text($unit_price_x, $first_table_x+5, "Unit Price");
$pdf->Line($unit_price_line_x, $first_table_x,$unit_price_line_x, 200); // vertical line
$qty_x=$unit_price_line_x+2;
$qty_line_x=$qty_x+8;
$pdf->Text($qty_x, $first_table_x+5, "Qty");
$pdf->Line($qty_line_x, $first_table_x,$qty_line_x, 200); // vertical line
$net_amount_x=$qty_line_x+2;
$net_amount_line_x=$net_amount_x+16;
$pdf->Text($net_amount_x, $first_table_x+5, "Net Amount");
$pdf->Line($net_amount_line_x, $first_table_x,$net_amount_line_x, 200); // vertical line
$tax_amount_x=$net_amount_line_x+1;
$tax_amount_line_x=$tax_amount_x+15;
$pdf->Text($tax_amount_x, $first_table_x+5, "Tax Amount");
$pdf->Line($tax_amount_line_x, $first_table_x,$tax_amount_line_x, 200); // vertical line
$total_x=$tax_amount_line_x+2;
$pdf->Text($total_x, $first_table_x+5, "Total");
$pdf->Line(5, $first_table_x+8, 205, $first_table_x+8); // horizontal line
$pdf->Line(5, 200, 205, 200); // horizontal line
$pdf->Line(5, 205, 205, 205); // horizontal line
$i=0;
}
function footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount)
{
  $pdf->SetFont('Arial','B',11);
  $pdf->Text(167, 210, "Total         :    ".$grand_total."");
// $pdf->Text(167, 212, "Discount  :    ".$discount."");
  $net_value=$grand_total-$discount;
// $pdf->Text(167, 216, "Net Value :    ".$net_value."");
  $footer_line=203;
  $pdf->SetFont('Arial','B',8);
// $pdf->Text(10, $footer_line+35+(5*0), "E&OE");
// $pdf->Text(10, $footer_line+35+(5*1), "Terms of Delivery and payment if any");
// $pdf->Text(100, $footer_line+35+(5*2), "DECLARATION");
// $pdf->Text(10, $footer_line+35+(5*3), "Certified that all the particulars shown in the above Tax invoice are true and correct and that my/our Registration under KVAT Act 2003 is");
// $pdf->Text(10, $footer_line+35+(5*4), "valid as on the date of this Bill.");
// $pdf->Text(160, $footer_line+35+(5*5), "For <company name>");
// $pdf->Text(165, $footer_line+35+(5*6), "Autherised Signatory");
// $pdf->Text(10, $footer_line+35+(5*7), " * Original for the buyer for the purpose of claiming Input Tax Credit, Duplicate for the Transport Copy,Triplicate for the filling at the");
// $pdf->Text(10, $footer_line+35+(5*8), " Check post/Extra Copy & Quadriplicate to be retained with the seller");
}
header_section($pdf,$name,$date,$invoice_no);
$i=0;
$first_table_x=33;
$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; foreach($SaleItem as $key=>$value) { 
  $sl_no_v_x=8;
  $pdf->Text($sl_no_v_x, $first_table_x+11+(5*$i), $i+1);
  $product_name_v_x=$sl_no_v_x+10;
  $pdf->Text($product_name_v_x, $first_table_x+11+(5*$i), $value['Product']['name']);
  $tax_v_x=$product_name_v_x+100;
// pr($value); exit;
  $pdf->Text($tax_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['cgst']);
  $unit_price_v_x=$tax_v_x+17;
  $pdf->Text($unit_price_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['unit_price']);
  $quantity_v_x=$unit_price_v_x+15;
  $pdf->Text($quantity_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['quantity']);
  $unit_price=$value['SaleItem']['unit_price'];
  $quantity=$value['SaleItem']['quantity'];
  $net_amount_v_x=$quantity_v_x+11;
  $pdf->Text($net_amount_v_x, $first_table_x+11+(5*$i), $unit_price*$quantity);
  $tax_amount_v_x=$net_amount_v_x+17;
  $pdf->Text($tax_amount_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['cgst_amount']);
  $total_v_x=$tax_amount_v_x+16;
  $pdf->Text($total_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['total']);
  $i++;
  if($i>31)
  {
    $i=0;
    footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
    header_section($pdf,$name,$date,$invoice_no);
  }
  $grand_amount+=$unit_price*$quantity;
  $grand_tax_amount+=$value['SaleItem']['cgst_amount'];
  $grand_total+=$value['SaleItem']['total'];
// $discount=$grand_total-$Sale['Sale']['total'];
  $discount=$Sale['Sale']['discount_amount'];
  $grand_total = $grand_amount - $discount;
}
footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
$pdf->Output();
exit;
}
public function GeneralStock_Edit_Function_in_Purchase($id,$date)
{
  $user_id=1;
  try {
    $SaleItem = $this->SaleItem->find('all',array(
      'conditions' => array(
        'SaleItem.sale_id' => $id,
        ),
      'fields' => array(
        'SaleItem.warehouse_id',
        'SaleItem.product_id',
        'SaleItem.quantity',
        'SaleItem.unit_price',
        'Sale.invoice_no',
        )
      ));
    foreach ($SaleItem as $key => $value) {
      $quantity = $value['SaleItem']['quantity'];
      $unit_price = $value['SaleItem']['unit_price'];
      $product_id=$value['SaleItem']['product_id'];
      $warehouse_id=$value['SaleItem']['warehouse_id'];
      $Stock = $this->Stock->find('first',array(
        'conditions'=>array(
          'product_id'=>$product_id,
          'warehouse_id'=>$warehouse_id,
          )
        ));
      $stock_id=$Stock['Stock']['id'];
      $stock_quantity=$Stock['Stock']['quantity'];
      $date=date('Y-m-d',strtotime($date));
      $StockController = new StockController;
      $remark='Sale --'.$value['Sale']['invoice_no'].'('.$quantity.')';
      $stock_quantity-=$quantity;
      $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
      if($Stock_function_return['result']!='Success')
        throw new Exception($Stock_function_return['result']);
    }
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  return $return;
}
public function JournalCreate_in_Sale($data,$paid,$account_head_id,$date,$cashhead=1,$voucher_no=null)
{
  $user_id=1;
  try {
    $credit=$account_head_id;
// $debit=1;
    $debit=$cashhead;
    $amount=$paid;
    $date=$date;
// $remarks='Direct Payment';
    $work_flow='Sales';
    $AccountingsController = new AccountingsController;
    $function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no);
    if($function_return['result']!='Success')
      throw new Exception($function_return['message'], 1);
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  return $return;
}
public function JournalCreate_in_Sales($invoice_no,$paid,$account_head_id,$date,$cashhead=1,$voucher_no=null)
{
  $user_id=1;
  try {
    $credit=$account_head_id;
    $debit=$cashhead;
    $amount=$paid;
    $date=$date;
    $remarks='Sale Invoice No :'.$invoice_no;
    $work_flow='Sales';
    $AccountingsController = new AccountingsController;
    $function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no);
    if($function_return['result']!='Success')
      throw new Exception($function_return['message'], 1);
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  return $return;
}
public function fpdf($id=null)
{
  $Sale = $this->Sale->find('first', array(
    "joins" => array(
      array(
        "table" => 'customers',
        "alias" => 'Customer',
        "type" => 'inner',
        "conditions" => array('Customer.account_head_id=Sale.account_head_id'),
        ),
      array(
        "table" => 'customer_types',
        "alias" => 'CustomerType',
        "type" => 'inner',
        "conditions" => array('CustomerType.id=Customer.customer_type_id'),
        ),
      ),
    'conditions' => array('Sale.id'=>$id),
    'fields' => array(
      'Sale.*',
      'AccountHead.name',
      'CustomerType.name',
      )
    ));
  $name=$Sale['AccountHead']['name'];
  $invoice_no=$Sale['Sale']['invoice_no'];
  $date=$Sale['Sale']['date_of_delivered'];
  $SaleItem = $this->SaleItem->find('all', array(
    'conditions' => array('SaleItem.sale_id'=>$id),
    'fields' => array(
      'SaleItem.*',
      'Product.id',
      'Product.name',
      )
    ));
  require('tfpdf/tfpdf.php');
  $pdf = new tFPDF('p', 'mm', [297, 210]);
  define('FPDF_FONTPATH','tfpdf/font');
  function header_section($pdf,$name,$date,$invoice_no) {
    $pdf->SetFont('Arial','B',8.4);
    $pdf->AddPage();
    $pdf->Image('img/logo.png',0,5,-400);
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',10);
// $pdf->Text(148, 5, 'للتجارة المواد الغدائية  و احلويات');
// $pdf->Text(160, 9, "تجارة – استيراد – تصدير");
// $pdf->Text(152, 13, "For Food Stuff & Sweets Trading");
// $pdf->Text(152, 17, "Global Trade - Experts & Import");
    $pdf->SetFont('Arial','B',20);
    $pdf->SetTextColor(0,100,0);
// $pdf->Text(43, 13, "IDREES JOMA");
    $pdf->SetFont('Arial','B',12);
// $pdf->Text(43, 18, "Delivering Taste");
    $pdf->SetTextColor(200,00,00);
    $pdf->Text(10, 26, "Invoice No : ".$invoice_no);
    $pdf->SetTextColor(10,10,10);
    $pdf->SetFont('Arial','B',8);
    $head_table_x=28;
    $pdf->rect(150, $head_table_x-10, 55, 8);
    $pdf->Text(152, 21, "Customer:");
    $pdf->Text(152, 25, "No:");
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',10);
// $pdf->Text(195, 21, ":زبون");
// $pdf->Text(199, 25, ":لا");
    $pdf->rect(5, $head_table_x, 140, 8);
    $pdf->Text(10, 33, "M/S : ".$name );
// $pdf->Text(135, 33, ": M/S" );
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',10);
// $pdf->Text(80, 20, "السيولة النقدية/ائتمان فاتورة");
    $pdf->SetFont('Arial','B',12);
    $pdf->Text(80, 26, "CASH/CREDIT INVOICE");
    $pdf->rect(150, $head_table_x, 55, 8);
    $pdf->SetFont('Arial','B',9);
    $pdf->Text(152, 33, "Date : ".date('d-M-Y',strtotime($date)));
// $pdf->AddFont('Arabic','','arabic.php');
// $pdf->SetFont('Arabic','',1);
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',13);
// $pdf->Text(188, 33, ": تاريخ");
// $strp_txt = stripslashes("تاريخ");
// $strp_txt = iconv('UTF-8', 'windows-1252', $strp_txt);
// $strp_txt = utf8_decode($strp_txt);
    $first_table_x=$head_table_x+13;
    $pdf->rect(5, $first_table_x, 200, 200);
    $slno_x=6;
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',8);
// $pdf->Text($slno_x, $first_table_x+5, "SlNo");
    $pdf->SetFont('Arial','B',7);
    $pdf->Text($slno_x, $first_table_x+5+5, "SlNo");
$pdf->Line($slno_x+7, $first_table_x,$slno_x+7, 200); // vertical line
$item_x=$slno_x+50;
$item_line_x=$item_x+100;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($item_x, $first_table_x+5, "وصف");
$pdf->SetFont('Arial','B',7);
$pdf->Text($item_x+50, $first_table_x+5+5, "Description");
// $pdf->Line($item_line_x, $first_table_x,$item_line_x, 200); // vertical line
// $tax_x=$item_line_x+20;
// $tax_line_x=$tax_x+15;
// $pdf->Text($tax_x, $first_table_x+5, "Tax");
$pdf->Line($item_line_x, $first_table_x,$item_line_x, 200); // vertical line
$qty_x=$item_line_x+4;
$qty_line_x=$qty_x+8;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($qty_x, $first_table_x+5, "كمية");
$pdf->SetFont('Arial','B',7);
$pdf->Text($qty_x, $first_table_x+5+5, "Qty");
$pdf->Line($qty_line_x, $first_table_x,$qty_line_x, 200); // vertical line
$unit_price_x=$qty_line_x+2;
$unit_price_line_x=$unit_price_x+15;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($unit_price_x, $first_table_x+5, "سعر الوحدة");
$pdf->SetFont('Arial','B',7);
$pdf->Text($unit_price_x, $first_table_x+5+5, "Unit Price");
$pdf->Line($unit_price_line_x, $first_table_x,$unit_price_line_x, 200); // vertical line
// $net_amount_x=$qty_line_x+2;
// $net_amount_line_x=$net_amount_x+16;
// $pdf->Text($net_amount_x, $first_table_x+5, "Net Amount");
// $pdf->Line($net_amount_line_x, $first_table_x,$net_amount_line_x, 200); // vertical line
// $tax_amount_x=$unit_price_x+1;
// $tax_amount_line_x=$tax_amount_x+15;
// $pdf->Text($tax_amount_x, $first_table_x+5, "Tax Amount");
// $pdf->Line($tax_amount_line_x, $first_table_x,$tax_amount_line_x, 200); // vertical line
$total_x=$unit_price_line_x+6;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($total_x, $first_table_x+5, "مجموع");
$pdf->SetFont('Arial','B',7);
$pdf->Text($total_x, $first_table_x+5+5, "Total");
$pdf->Line(5, $first_table_x+12, 205, $first_table_x+12); // horizontal line
$pdf->Line(5, 200, 205, 200); // horizontal line
$pdf->Line(5, 205, 205, 205); // horizontal line
$i=0;
}
function footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount)
{
//discount portion
//  $pdf->SetFont('Arial','B',8);
// $pdf->Line(185, 205,185, 215); // vertical line
// $pdf->Text(190, 211, $discount);
// $pdf->Text(10, 211, 'Discount :');
// $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
// $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
// $pdf->SetFont('DejaVu','',8);
// $pdf->Text(170, 211, ': مجموع');
// $pdf->Line(5, 215, 205, 215); 
//discount portion
  $pdf->SetFont('Arial','B',11);
$pdf->Line(185, 205,185, 215); // vertical line
$pdf->Text(190, 211, number_format($grand_amount,2));
$pdf->Text(10, 211, 'Total :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',11);
// $pdf->Text(170, 211, ': مجموع');
$pdf->Line(5, 215, 205, 215); // horizontal line
/****/
/*****/
$pdf->SetFont('Arial','B',11);
$pdf->Line(185, 215,185, 224); // vertical line
$pdf->Text(190, 220, number_format($discount,2));
$pdf->Text(10, 220, 'Discount :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',11);
// $pdf->Text(170, 220, ': مجموع');//add later
$pdf->Line(5, 224, 205, 224); // horizontal line
/****/
/*****/
$pdf->SetFont('Arial','B',11);
$pdf->Line(185, 225,185, 234); // vertical line
$pdf->Text(190, 229, number_format($grand_total,2));
$pdf->Text(10, 229, 'Net Total :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',11);
// $pdf->Text(170, 229, ': مجموع');
$pdf->Line(5, 234, 205, 234); // horizontal line
/*****/
/****/
// $pdf->Text(167, 212, "Discount  :    ".$discount."");
$net_value=$grand_total-$discount;
// $pdf->Text(167, 216, "Net Value :    ".$net_value."");
$footer_line=203;
$pdf->SetFont('Arial','B',8);
// $pdf->Text(10, $footer_line+35+(5*0), "E&OE");
// $pdf->Text(10, $footer_line+35+(5*1), "Terms of Delivery and payment if any");
// $pdf->Text(100, $footer_line+35+(5*2), "DECLARATION");
// $pdf->Text(10, $footer_line+35+(5*3), "Certified that all the particulars shown in the above Tax invoice are true and correct and that my/our Registration under KVAT Act 2003 is");
// $pdf->Text(10, $footer_line+35+(5*4), "valid as on the date of this Bill.");
// $pdf->Text(160, $footer_line+35+(5*5), "For <company name>");
// $pdf->Text(165, $footer_line+35+(5*6), "Autherised Signatory");
// $pdf->Text(10, $footer_line+35+(5*7), " * Original for the buyer for the purpose of claiming Input Tax Credit, Duplicate for the Transport Copy,Triplicate for the filling at the");
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',11);
// $pdf->Text(185, 295, "تم الاستلام");
// $pdf->Text(60, 295, "بائع");
$pdf->SetFont('Arial','B',8);
$pdf->Text(10, 295, "Salesman");
$pdf->Text(120, 295, "Recieved By");
}
header_section($pdf,$name,$date,$invoice_no);
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+36;
$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; foreach($SaleItem as $key=>$value) {
  $sl_no_v_x=8;
  $pdf->Text($sl_no_v_x, $first_table_x+11+(5*$i), $i+1);
  $product_name_v_x=$sl_no_v_x+10;
  $pdf->Text($product_name_v_x, $first_table_x+11+(5*$i), $value['Product']['name']);
// $tax_v_x=$product_name_v_x+100;
// $pdf->Text($tax_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['tax']);
  $quantity_v_x=$product_name_v_x+140;
  $pdf->Text($quantity_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['quantity']);
  $unit_price_v_x=$quantity_v_x+12;
  $pdf->Text($unit_price_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['unit_price']);
  $unit_price=$value['SaleItem']['unit_price'];
  $quantity=$value['SaleItem']['quantity'];
// $net_amount_v_x=$quantity_v_x+11;
// $pdf->Text($net_amount_v_x, $first_table_x+11+(5*$i), $unit_price*$quantity);
// $tax_amount_v_x=$net_amount_v_x+17;
// $pdf->Text($tax_amount_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['tax_amount']);
  $total_v_x=$unit_price_v_x+18;
  $pdf->Text($total_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['total']);
  $i++;
// if($i>31)
  if($i>25)
  {
    $i=0;
    footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
    header_section($pdf,$name,$date,$invoice_no);
  }
  $grand_amount+=$unit_price*$quantity;
  $grand_tax_amount+=$value['SaleItem']['tax_amount'];
  $grand_total+=$value['SaleItem']['total'];
  $discount=$Sale['Sale']['discount_amount'];
// $discount=$grand_total-$Sale['Sale']['total'];
  $grand_total = $grand_amount - $discount;
}
footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
$pdf->Output();
exit;
}
public function qutofpdf($id=null)
{
  $Sale = $this->Sale->find('first', array(
    "joins" => array(
      array(
        "table" => 'customers',
        "alias" => 'Customer',
        "type" => 'inner',
        "conditions" => array('Customer.account_head_id=Sale.account_head_id'),
        ),
      array(
        "table" => 'customer_types',
        "alias" => 'CustomerType',
        "type" => 'inner',
        "conditions" => array('CustomerType.id=Customer.customer_type_id'),
        ),
      ),
    'conditions' => array('Sale.id'=>$id),
    'fields' => array(
      'Sale.*',
      'AccountHead.name',
      'CustomerType.name',
      )
    ));
  $name=$Sale['AccountHead']['name'];
  $invoice_no=$Sale['Sale']['invoice_no'];
// $date=$Sale['Sale']['date_of_delivered'];
  $date=$Sale['Sale']['date_of_order'];
  $SaleItem = $this->SaleItem->find('all', array(
    'conditions' => array('SaleItem.sale_id'=>$id),
    'fields' => array(
      'SaleItem.*',
      'Product.id',
      'Product.name',
      )
    ));
  require('tfpdf/tfpdf.php');
  $pdf = new tFPDF('p', 'mm', [297, 210]);
  define('FPDF_FONTPATH','tfpdf/font');
  function header_section($pdf,$name,$date,$invoice_no) {
    $pdf->SetFont('Arial','B',8.4);
    $pdf->AddPage();
    $pdf->Image('img/logo.png',0,5,-400);
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',10);
// $pdf->Text(148, 5, 'للتجارة المواد الغدائية  و احلويات');
// $pdf->Text(160, 9, "تجارة – استيراد – تصدير");
// $pdf->Text(152, 13, "For Food Stuff & Sweets Trading");
// $pdf->Text(152, 17, "Global Trade - Experts & Import");
    $pdf->SetFont('Arial','B',20);
    $pdf->SetTextColor(0,100,0);
// $pdf->Text(43, 13, "IDREES JOMA");
    $pdf->SetFont('Arial','B',12);
// $pdf->Text(43, 18, "Delivering Taste");
    $pdf->SetTextColor(200,00,00);
    $pdf->Text(10, 26, "Quotation No : ".$invoice_no);
    $pdf->SetTextColor(10,10,10);
    $pdf->SetFont('Arial','B',8);
    $head_table_x=28;
    $pdf->rect(150, $head_table_x-10, 55, 8);
    $pdf->Text(152, 21, "Customer:");
    $pdf->Text(152, 25, "No:");
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',10);
// $pdf->Text(195, 21, ":زبون");
// $pdf->Text(199, 25, ":لا");
    $pdf->rect(5, $head_table_x, 140, 8);
    $pdf->Text(10, 33, "M/S : ".$name );
// $pdf->Text(135, 33, ": M/S" );
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',10);
// $pdf->Text(80, 20, "السيولة النقدية/ائتمان فاتورة");
// $pdf->Text(80, 20, "سابتقا");
    $pdf->SetFont('Arial','B',12);
    $pdf->Text(80, 26, "Quotation");
    $pdf->rect(150, $head_table_x, 55, 8);
    $pdf->SetFont('Arial','B',9);
    $pdf->Text(152, 33, "Date : ".date('d-M-Y',strtotime($date)));
// $pdf->AddFont('Arabic','','arabic.php');
// $pdf->SetFont('Arabic','',1);
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',13);
// $pdf->Text(188, 33, ": تاريخ");
// $strp_txt = stripslashes("تاريخ");
// $strp_txt = iconv('UTF-8', 'windows-1252', $strp_txt);
// $strp_txt = utf8_decode($strp_txt);
    $first_table_x=$head_table_x+13;
    $pdf->rect(5, $first_table_x, 200, 200);
    $slno_x=6;
    $pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
    $pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
    $pdf->SetFont('DejaVu','',8);
// $pdf->Text($slno_x, $first_table_x+5, "SlNo");
    $pdf->SetFont('Arial','B',7);
    $pdf->Text($slno_x, $first_table_x+5+5, "SlNo");
$pdf->Line($slno_x+7, $first_table_x,$slno_x+7, 200); // vertical line
$item_x=$slno_x+50;
$item_line_x=$item_x+100;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($item_x, $first_table_x+5, "وصف");
$pdf->SetFont('Arial','B',7);
$pdf->Text($item_x+50, $first_table_x+5+5, "Description");
// $pdf->Line($item_line_x, $first_table_x,$item_line_x, 200); // vertical line
// $tax_x=$item_line_x+20;
// $tax_line_x=$tax_x+15;
// $pdf->Text($tax_x, $first_table_x+5, "Tax");
$pdf->Line($item_line_x, $first_table_x,$item_line_x, 200); // vertical line
$qty_x=$item_line_x+4;
$qty_line_x=$qty_x+8;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($qty_x, $first_table_x+5, "كمية");
$pdf->SetFont('Arial','B',7);
$pdf->Text($qty_x, $first_table_x+5+5, "Qty");
$pdf->Line($qty_line_x, $first_table_x,$qty_line_x, 200); // vertical line
$unit_price_x=$qty_line_x+2;
$unit_price_line_x=$unit_price_x+15;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($unit_price_x, $first_table_x+5, "سعر الوحدة");
$pdf->SetFont('Arial','B',7);
$pdf->Text($unit_price_x, $first_table_x+5+5, "Unit Price");
$pdf->Line($unit_price_line_x, $first_table_x,$unit_price_line_x, 200); // vertical line
// $net_amount_x=$qty_line_x+2;
// $net_amount_line_x=$net_amount_x+16;
// $pdf->Text($net_amount_x, $first_table_x+5, "Net Amount");
// $pdf->Line($net_amount_line_x, $first_table_x,$net_amount_line_x, 200); // vertical line
// $tax_amount_x=$unit_price_x+1;
// $tax_amount_line_x=$tax_amount_x+15;
// $pdf->Text($tax_amount_x, $first_table_x+5, "Tax Amount");
// $pdf->Line($tax_amount_line_x, $first_table_x,$tax_amount_line_x, 200); // vertical line
$total_x=$unit_price_line_x+6;
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text($total_x, $first_table_x+5, "مجموع");
$pdf->SetFont('Arial','B',7);
$pdf->Text($total_x, $first_table_x+5+5, "Total");
$pdf->Line(5, $first_table_x+12, 205, $first_table_x+12); // horizontal line
$pdf->Line(5, 200, 205, 200); // horizontal line
$pdf->Line(5, 205, 205, 205); // horizontal line
$i=0;
}
function footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount)
{
  $pdf->SetFont('Arial','B',8);
$pdf->Line(185, 205,185, 215); // vertical line
$pdf->Text(190, 211, number_format($grand_amount,2));
$pdf->Text(10, 211, 'Grand Total :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text(170, 211, ': مجموع');
$pdf->Line(5, 215, 205, 215); // horizontal line
/*****/
$pdf->SetFont('Arial','B',8);
$pdf->Line(185, 215,185, 224); // vertical line
$pdf->Text(190, 220, number_format($discount,2));
$pdf->Text(10, 220, 'Discount :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text(170, 220, ': مجموع');//add later
$pdf->Line(5, 224, 205, 224); // horizontal line
/****/
/*****/
$pdf->SetFont('Arial','B',8);
$pdf->Line(185, 225,185, 234); // vertical line
$pdf->Text(190, 229, number_format($grand_total,2));
$pdf->Text(10, 229, 'Net Total :');
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',8);
// $pdf->Text(170, 229, ': مجموع');
$pdf->Line(5, 234, 205, 234); // horizontal line
/*****/
// $pdf->Text(167, 212, "Discount  :    ".$discount."");
$net_value=$grand_total-$discount;
// $pdf->Text(167, 216, "Net Value :    ".$net_value."");
$footer_line=203;
$pdf->SetFont('Arial','B',8);
// $pdf->Text(10, $footer_line+35+(5*0), "E&OE");
// $pdf->Text(10, $footer_line+35+(5*1), "Terms of Delivery and payment if any");
// $pdf->Text(100, $footer_line+35+(5*2), "DECLARATION");
// $pdf->Text(10, $footer_line+35+(5*3), "Certified that all the particulars shown in the above Tax invoice are true and correct and that my/our Registration under KVAT Act 2003 is");
// $pdf->Text(10, $footer_line+35+(5*4), "valid as on the date of this Bill.");
// $pdf->Text(160, $footer_line+35+(5*5), "For <company name>");
// $pdf->Text(165, $footer_line+35+(5*6), "Autherised Signatory");
// $pdf->Text(10, $footer_line+35+(5*7), " * Original for the buyer for the purpose of claiming Input Tax Credit, Duplicate for the Transport Copy,Triplicate for the filling at the");
$pdf->AddFont('DejaVu','','DejaVuSansCondensed.ttf',true);
$pdf->AddFont('DejaVu', 'B', 'DejaVuSansCondensed-Bold.ttf', true);
$pdf->SetFont('DejaVu','',11);
// $pdf->Text(185, 295, "تم الاستلام");
// $pdf->Text(60, 295, "بائع");
$pdf->SetFont('Arial','B',8);
$pdf->Text(10, 295, "Salesman");
$pdf->Text(120, 295, "Recieved By");
}
header_section($pdf,$name,$date,$invoice_no);
$i=0;
$head_table_x=10;
$first_table_x=$head_table_x+36;
$grand_amount=0; $grand_tax_amount=0; $grand_total=0;  $discount=0; foreach($SaleItem as $key=>$value) {
  $sl_no_v_x=8;
  $pdf->Text($sl_no_v_x, $first_table_x+11+(5*$i), $i+1);
  $product_name_v_x=$sl_no_v_x+10;
  $pdf->Text($product_name_v_x, $first_table_x+11+(5*$i), $value['Product']['name']);
// $tax_v_x=$product_name_v_x+100;
// $pdf->Text($tax_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['tax']);
  $quantity_v_x=$product_name_v_x+140;
  $pdf->Text($quantity_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['quantity']);
  $unit_price_v_x=$quantity_v_x+12;
  $pdf->Text($unit_price_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['unit_price']);
  $unit_price=$value['SaleItem']['unit_price'];
  $quantity=$value['SaleItem']['quantity'];
// $net_amount_v_x=$quantity_v_x+11;
// $pdf->Text($net_amount_v_x, $first_table_x+11+(5*$i), $unit_price*$quantity);
// $tax_amount_v_x=$net_amount_v_x+17;
// $pdf->Text($tax_amount_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['tax_amount']);
  $total_v_x=$unit_price_v_x+18;
  $pdf->Text($total_v_x, $first_table_x+11+(5*$i), $value['SaleItem']['total']);
  $i++;
// if($i>31)
  if($i>25)
  {
    $i=0;
    footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
    header_section($pdf,$name,$date,$invoice_no);
  }
  $grand_amount+=$unit_price*$quantity;
  $grand_tax_amount+=$value['SaleItem']['tax_amount'];
  $grand_total+=$value['SaleItem']['total'];
// $discount=$grand_total-$Sale['Sale']['total'];
  $discount=$Sale['Sale']['discount_amount'];
  $grand_total = $grand_amount - $discount;
}
footer($pdf,$grand_amount,$grand_tax_amount,$grand_total,$discount);
$pdf->Output();
exit;
}
public function DayRegister()
{
// $PermissionList=$this->Session->read('PermissionList');
// $menu_id = $this->Menu->field( 'Menu.id', array('action ' => 'Sale/DayRegister'));
// if(!in_array($menu_id, $PermissionList))
// {
//   $this->Session->setFlash("Permission denied");
//   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
// }
  $this->Executive->virtualFields = array( 'executive_name' => "CONCAT(Executive.name, ' ', Executive.username)" );
  $executives=$this->Executive->find('list',array('fields'=>['id','executive_name']));
  $routes=$this->Route->find('list');
  $CustomerGroup_list=$this->CustomerGroup->find('list',array('fields'=>array('id','name')));
  $this->set(compact('CustomerGroup_list','routes','executives'));
  $from_date = date('d-m-Y',strtotime('-1 day'));
  $to_date = date('d-m-Y');
    $this->set('to_date', $to_date);
        $this->set('from_date', $from_date);
  if($this->request->data)
  {
    $data=$this->request->data['DayRegister'];
    $datasource_DayRegister = $this->DayRegister->getDataSource();
    try {
      $datasource_DayRegister->begin();
      if(!isset($data['executive_id']))
        throw new Exception("Executive Required", 1);
      $date=$data['date'];
      $explode_date=explode('-',$date);
      $from=date('Y-m-d',strtotime(trim($explode_date[0])));
      $to=date('Y-m-d',strtotime(trim($explode_date[1])));
      $first_date = date('Y-m-d',strtotime('01-'.date('m-Y',strtotime($from))));
      $date = $from;
      $executive_id=$data['executive_id'];
      $Executive=$this->Executive->findById($executive_id);
      if(!isset($Executive))
        throw new Exception("Invalid Executive Id", 1); 
      $SettingsController = new SettingsController;
      $target = $SettingsController->sale_target($executive_id,$first_date);
      $return['SaleTargets']=0;
      if($target)
        $return['SaleTargets']=$target;
      $DayRegister=$this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive_id,'DayRegister.date' => $date)));
// $DayRegisterPrev=$this->DayRegister->find('first', array(
//   'conditions'=>array(
//     'DayRegister.executive_id'=>$executive_id,
//     'DayRegister.status'=>0,
//   )
// ));
// if($DayRegisterPrev)
//   $DayRegister=$DayRegisterPrev;
      if($DayRegister)
      {
        $return['status']='Already Registered';
        $return['day_register_id']=$DayRegister['DayRegister']['id'];
        $this->DayRegister->id=$DayRegister['DayRegister']['id'];
        if(!$this->DayRegister->saveField('status',0))
          throw new Exception("Cant Updated the Status", 1);
        if(!$DayRegister['DayRegister']['present'])
          throw new Exception("Absent Already Registered", 1);
      }
      else
      {
        $tableData = [
        'executive_id' => $executive_id,
        'date' => $from,
        'route_id' => $data['route_id'],
        'customer_group_id' => $data['customer_group_id'],
        'km' => $data['km'],
        'present' => $data['present'],
        ];
        $this->DayRegister->create();
        if (!$this->DayRegister->save($tableData))
        {
          $errors = $this->DayRegister->validationErrors;
          foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
        }
        $DayRegister=$this->DayRegister->findById($this->DayRegister->getLastInsertId());
        $return['status']='success';
      }
      $datasource_DayRegister->commit();
    } catch (Exception $e) {
      $datasource_DayRegister->rollback();
      $return['status']=$e->getMessage();
    }
    $this->Session->setFlash(__($return['status']));
  }
}
public function get_dayregister_details_ajax($id)
{
  $return=[
  'status'=>'Empty',
  'DayRegister'=>[],
  ];
  if(isset($id))
  {
    $DayRegister=$this->DayRegister->findById($id);
    $return['DayRegister']=$DayRegister['DayRegister'];
  }
  echo json_encode($return);
  exit;
}
public function EditDayRegister()
{
  try {
    $data=$this->request->data['DayRegister'];
    $Table_data=array(
      'route_id' => $data['route_id'],
      'customer_group_id' => $data['customer_group_id'],
      'km' => $data['km'],
      'present' => $data['present'],
      );
    $this->DayRegister->id=$data['id'];
    if(!$this->DayRegister->save($Table_data))
    {
      $errors = $this->DayRegister->validationErrors;
      foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
    }
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  $this->Session->setFlash(__($return['result']));
  $this->redirect( Router::url( $this->referer(), true ) );
}
public function DayRegister_search()
{
  if(!empty($this->request->data['customer_group_id']))
  {
    if($this->request->data['customer_group_id']!='select')
    {
      $conditions['DayRegister.customer_group_id']=$this->request->data['customer_group_id'];
    }
  }
  if(!empty($this->request->data['customer_group_id']))
  {
    if($this->request->data['customer_group_id']!='select')
    {
      $conditions['DayRegister.customer_group_id']=$this->request->data['customer_group_id'];
    }
  }
  if(!empty($this->request->data['route_id']))
  {
    if($this->request->data['route_id']!='select')
    {
      $conditions['DayRegister.route_id']=$this->request->data['route_id'];
    }
  }
  $DayRegister = $this->DayRegister->find('all', array(
    'conditions' => array(
      'DayRegister.date'=>$date
// 'Sale.status=2',
// '(Sale.date_of_order) between ? and ? '=>array($first_date,$last_date),
      ),
    'order' => array('DayRegister.id DESC'),
    'fields' => array(
      'DayRegister.*',
      'Route.name',
      'Executive.*',
      'CustomerGroup.name',
      )
    ));
  $this->set('DayRegister', $DayRegister);
  $data['row']='';
  if($Customer)
  {
    foreach ($Customer as $key => $value) {
      $data['row']= $data['row'].'<tr>';
// $data['row']= $data['row'].'<td>'.$value["Executive"]["name"].'</td>';
      $data['row']= $data['row'].'<td>'.$value["Route"]["name"].'</td>';
      $data['row']= $data['row'].'<td>'.$value["CustomerGroup"]["name"].'</td>';
      $data['row']= $data['row'].'<td>'.$value["CustomerType"]["name"].'</td>';
      $data['row']= $data['row'].'<td><span style="display:none" class="AccountHead_id">'.$value['AccountHead']['id'].'</span><span>'.$value['AccountHead']['name'].'</span></td>';
      $data['row']= $data['row'].'<td>'.$value["Customer"]["code"].'</td>';
      $data['row']= $data['row'].'<td>'.$value["AccountHead"]["opening_balance"].'</td>';
      $data['row']= $data['row'].'<td><i class="fa fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#Account_modal"></i></td>';
      $data['row']= $data['row'].'<td><a><i class="fa fa-trash blue-col blue-col"></i></a></td>';
      $data['row']= $data['row'].'</tr>';
    }
    $data['result']='Success';
  }
  else
  {
    $data['result']='Error';
  }
  echo json_encode($data);
  exit;
}
// public function DeleteDayClose($id)
// {
//   try {
//     if(!$this->ClosingDay->delete($id))
//       throw new Exception("Cant Reopen", 1);
//     $return['result']='Success';
//   } catch (Exception $e) {
//     $return['result']=$e->getMessage();
//   }
//   $this->Session->setFlash(__($return['result']));
//   $this->redirect( Router::url( $this->referer(), true ) );
// }
public function UnitLevelConvert($product_id,$unit_id)
{
  $this->Product->unbindModel(
    array('hasMany' => array(
      'PurchaseReturnItem',
      'PurchasedItem',
      'SaleItem',
      'SalesReturnItem',
      'StockLog',
      'Stock',
      'UnwantedList',
      ))
    );
  $result=array();
  $Unit=$this->Unit->find('list',array('conditions'=>array('Unit.id'=>$unit_id),'fields'=>array('Unit.id','Unit.level')));
  $result['sale_unit_level']=$Unit[$unit_id];

  $Product=$this->Product->find('first',array(
    'conditions'=>array(
      'Product.id'=>$product_id
      ),
    'fields'=>array(
      'Product.*',
      ),
    ));
  if($Product){
    $result['no_of_piece_per_unit']=$Product['Product']['no_of_piece_per_unit'];
    $result['product_cost']=$Product['Product']['cost'];
    $result['product_bonus']=$Product['Product']['bonus_percentage'];
    $ProductUnit=$this->Unit->find('list',array('conditions'=>array('Unit.id'=>$Product['Product']['unit_id']),'fields'=>array('Unit.id','Unit.level')));
    $result['product_unit_level']=$ProductUnit[$Product['Product']['unit_id']];
  }else{
    $result['product_unit_level']="";
  }
  return $result;
}
public function UpdateCustomerSettings() {
  $data=$this->request->data['CustomerSettings'];
  $CustomerSettingList=$this->CustomerSetting->find('list',array('fields'=>array('id','function'),'conditions'=>array('module'=>'Sale')));
  foreach ($CustomerSettingList as $key => $value) {
    $this->CustomerSetting->id = $key;
    if($this->CustomerSetting->saveField('value',$data[$value])){
      $return['result']='Success';
    }else{
      $return['result']='Error';
    }
  }
  echo json_encode($return);
  exit;
}
public function GetCustomerSettings($module)
{
  $CustomerSettingList=$this->CustomerSetting->find('list',array('fields'=>array('function','value'),'conditions'=>array('module'=>$module)));
  echo json_encode($CustomerSettingList);
  exit;
}
public function get_Stock_ajax()
{
  $data=$this->request->data;
  $quantity=$data['quantity'];
  $quantity_mode=$data['quantity_mode'];
  $product_id= $data['product'];
  $warehouse_id= $data['warehouse_id'];

  $stock_list=$this->Stock->find('first',array(

    'conditions'=>array(
      'Stock.product_id'=>$product_id,
      'Stock.warehouse_id'=>$warehouse_id,
      ),
    'fields' => array(
      'Stock.quantity',
      'Product.no_of_piece_per_unit',
      )
    ));
  $return['available_quantity']=$stock_list['Stock']['quantity'];
  if($quantity_mode==1)
  {
    if($quantity>$stock_list['Stock']['quantity'])
    {
      $return['result']="No available quantity in Stock";
    }
    else
    {
      $return['result']="Quantity in Stock";

    }
  }
  else
  {
    $unitpiece=$stock_list['Product']['no_of_piece_per_unit'];
    $stock_quantity=$unitpiece*$quantity;
    if($stock_quantity>$stock_list['Stock']['quantity'])
    {
      $return['result']="No available quantity in Stock";

    }
    else
    {
      $return['result']="Quantity in Stock";

    }
  }
  echo json_encode($return);
  exit;
}
public function get_Stock_ajax1()
{
  $data=$this->request->data;
  $quantity=$data['quantity'];
  $product_id= $data['product'];
  $warehouse_id= $data['warehouse_id'];

  $stock_list=$this->Stock->find('first',array(

    'conditions'=>array(
      'Stock.product_id'=>$product_id,
      'Stock.warehouse_id'=>$warehouse_id,
      ),
    'fields' => array(
      'Stock.quantity',
      'Product.no_of_piece_per_unit',
      )
    ));

  if($quantity>$stock_list['Stock']['quantity'])
  {
    $return['result']="No available quantity in Stock";
  }
  else
  {
    $return['result']="Quantity in Stock";

  }
  echo json_encode($return);
  exit;
}
public function CreditNote($id=null)
{
  $user_id=1;
// $Customer_list=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('sub_group_id'=>3)));
  $this->Customer->virtualFields = array('customer_name' => "CONCAT(Customer.code,' - ',AccountHead.name)");
  $Customer_list=$this->Customer->find('list',array(
    'joins'=>array(
      array(
        'table'=>'account_heads',
        'alias'=>'AccountHead',
        'type'=>'INNER',
        'conditions'=>array('AccountHead.id=Customer.account_head_id')
        ),
      ),
    'conditions'=>array('sub_group_id'=>3),
    'fields'=>['AccountHead.id','customer_name']
    ));

  $this->set('Customer_list',$Customer_list);
//pr($Customer_list);
// exit;
  $this->set('CustomerType',$this->CustomerType->find('list',array('fields'=>array('id','name'))));
//$this->set(compact('Customer'));
  $CreditNote=$this->CreditNote->find('first',array('order' => 'CreditNote.id DESC',));
  if(!empty($CreditNote))
  {
    $CreditNo = $CreditNote['CreditNote']['credit_no'];
    $credit_no=$CreditNo+1;
  }
  else
  {
    $credit_no=1;
  }
  if (!$this->request->data)
  {
    if(empty($id))
    {
      $data['CreditNote']['credit_no']=$credit_no;
      $data['CreditNote']['date']=date('d-m-Y');
      $data['CreditNote']['id']="";
      $this->request->data=$data;
    }
    else
    {
      $CreditNote=$this->CreditNote->findById($id);
      $Customer=$this->Customer->findByAccountHeadId($CreditNote['CreditNote']['account_head_id']);
      $this->request->data=$CreditNote;
      $this->request->data['CreditNote']['date']=date('d-m-Y',strtotime($CreditNote['CreditNote']['date']));
      $this->request->data['CreditNote']['credit_no']=$CreditNote['CreditNote']['credit_no'];
      $this->request->data['CreditNote']['customer_type_id']=$Customer['Customer']['customer_type_id'];
      $this->request->data['CreditNote']['customer_id']=$CreditNote['CreditNote']['account_head_id'];

      $CreditDetail=$this->CreditDetail->find('all',array(
        "joins" => array(),
        'conditions'=>array(
          'credit_id'=>$id,

          ),
        'fields'=>array(
          'CreditDetail.*',

          )
        ));
      $this->set('CreditDetail',$CreditDetail);
    }
  }
  else
  {
    $datasource_Creditnote = $this->CreditNote->getDataSource();
    $datasource_Journal = $this->Journal->getDataSource();
    $datasource_CreditItem=$this->CreditDetail->getDataSource();
    try { 
      $datasource_Creditnote->begin();
      $datasource_CreditItem->begin();
      $data=$this->request->data['CreditNote'];
      if(empty($id))
      {
        $Save_data=[
        'account_head_id'=>$data['customer_id'],
        'credit_no'=>$data['credit_no'],
        'date'=>date('Y-m-d',strtotime($data['date'])),
        'total'=>$data['total'],
        'created_at'=>date('Y-m-d H:i:s'),
        'updated_at'=>date('Y-m-d H:i:s'),
        ];
        $process=$data['process'];
        if($process=='delivery')
        {
          $Save_data['date']=date('Y-m-d',strtotime($data['date']));
        }
        $this->CreditNote->create();
        if(!$this->CreditNote->save($Save_data))
        {
          $errors = $this->CreditNote->validationErrors;

          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
        $id=$this->CreditNote->getLastInsertId();
        for ($i=0; $i <count($data['amount']) ; $i++) {
          $SaveItem_data=[
          'credit_id'=>$id,
          'description'=>$data['description'][$i],
          'amount'=>$data['amount'][$i],
          ];
          $this->CreditDetail->create();
          if(!$this->CreditDetail->save($SaveItem_data))
          {
            $errors = $this->CreditDetail->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
        }
      }
      else
      {

      }
      if($process=='delivery')
      {
        $CreditNote=$this->CreditNote->findById($id);
        $datasource_Journal->begin();
        $credit=$CreditNote['AccountHead']['id'];;
        $debit=1;
        $date=$CreditNote['CreditNote']['date'];
        $amount=$CreditNote['CreditNote']['total'];
        $work_flow='Credit Note';
        $remarks='Credit No :'.$CreditNote['CreditNote']['credit_no'];
        $AccountingsController = new AccountingsController;
        $voucher_no=$AccountingsController->GetVoucherNo();

        $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,'','','');
        if($Accountings_function_return['result']!='Success')
          throw new Exception($Stock_function_return['message']);
        $datasource_Journal->commit();
      }
      $datasource_Creditnote->commit();
      $datasource_CreditItem->commit();
      $return['result']='Success';
      $this->Session->setFlash(__($return['result']));
      $this->redirect(array('controller' => 'Sale', 'action' => 'CreditNote',$id));
    }
    catch (Exception $e)
    {
      $datasource_Creditnote->rollback();
      $datasource_Journal->rollback();
      $datasource_CreditItem->rollback();
      $return['result']=$e->getMessage();
      $this->redirect( Router::url( $this->referer(), true ) );
    }

  }
}
public function CreditNoteIndex()
{

}
public function DebitNote($id=null)
{
  $user_id=1;
  $sub_group_id=15;
  $PurchaseController = new PurchaseController;
  $Accounting_function_return=$PurchaseController->AccountHead_Option_ListBySubGroupId($sub_group_id);
  $this->set('Party_list',$Accounting_function_return);
  $Customer_list=$this->AccountHead->find('list',array('fields'=>array('id','name'),'conditions'=>array('sub_group_id'=>3)));
  $this->set('Customer_list',$Customer_list);
  $this->set('CustomerType',$this->CustomerType->find('list',array('fields'=>array('id','name'))));
//$this->set(compact('Customer'));
  $DebitNote=$this->DebitNote->find('first',array('order' => 'DebitNote.id DESC',));
  if(!empty($DebitNote))
  {
    $DebitNo = $DebitNote['DebitNote']['debit_no'];
    $debit_no=$DebitNo+1;
  }
  else
  {
    $debit_no=1;
  }
  if (!$this->request->data)
  {
    if(empty($id))
    {
      $data['DebitNote']['debit_no']=$debit_no;
      $data['DebitNote']['date']=date('d-m-Y');
      $data['DebitNote']['id']="";
      $this->request->data=$data;
    }
    else
    {
      $DebitNote=$this->DebitNote->findById($id);
      $Customer=$this->Customer->findByAccountHeadId($DebitNote['DebitNote']['account_head_id']);
      $this->request->data=$DebitNote;
      $this->request->data['DebitNote']['date']=date('d-m-Y',strtotime($DebitNote['DebitNote']['date']));
      $this->request->data['DebitNote']['debit_no']=$DebitNote['DebitNote']['debit_no'];
// $this->request->data['DebitNote']['customer_type_id']=$Customer['Customer']['customer_type_id'];
      $this->request->data['DebitNote']['customer_id']=$DebitNote['DebitNote']['account_head_id'];

      $DebitDetail=$this->DebitDetail->find('all',array(
        "joins" => array(),
        'conditions'=>array(
          'debit_id'=>$id,

          ),
        'fields'=>array(
          'DebitDetail.*',

          )
        ));
      $this->set('DebitDetail',$DebitDetail);
    }
  }
  else
  {
    $datasource_DebitNote = $this->DebitNote->getDataSource();
    $datasource_Journal = $this->Journal->getDataSource();
    $datasource_DebitItem=$this->DebitDetail->getDataSource();
    try { 
      $datasource_DebitNote->begin();
      $datasource_DebitItem->begin();
      $data=$this->request->data['DebitNote'];
      if(empty($id))
      {
        $Save_data=[
        'account_head_id'=>$data['customer_id'],
        'debit_no'=>$data['debit_no'],
        'date'=>date('Y-m-d',strtotime($data['date'])),
        'total'=>$data['total'],
        'created_at'=>date('Y-m-d H:i:s'),
        'updated_at'=>date('Y-m-d H:i:s'),
        ];
        $process=$data['process'];
        if($process=='delivery')
        {
          $Save_data['date']=date('Y-m-d',strtotime($data['date']));
        }
        $this->DebitNote->create();
        if(!$this->DebitNote->save($Save_data))
        {
          $errors = $this->DebitNote->validationErrors;

          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
        $id=$this->DebitNote->getLastInsertId();
        for ($i=0; $i <count($data['amount']) ; $i++) {
          $SaveItem_data=[
          'debit_id'=>$id,
          'description'=>$data['description'][$i],
          'amount'=>$data['amount'][$i],
          ];
          $this->DebitDetail->create();
          if(!$this->DebitDetail->save($SaveItem_data))
          {
            $errors = $this->DebitDetail->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
        }
      }
      else
      {

      }
      if($process=='delivery')
      {
        $DebitNote=$this->DebitNote->findById($id);
        $datasource_Journal->begin();
        $debit=$DebitNote['AccountHead']['id'];;
        $credit=1;
        $date=$DebitNote['DebitNote']['date'];
        $amount=$DebitNote['DebitNote']['total'];
        $work_flow='Debit Note';
        $remarks='Debit No :'.$DebitNote['DebitNote']['debit_no'];
        $AccountingsController = new AccountingsController;
        $voucher_no=$AccountingsController->GetVoucherNo();

        $Accountings_function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,'','','');
        if($Accountings_function_return['result']!='Success')
          throw new Exception($Stock_function_return['message']);
        $datasource_Journal->commit();
      }
      $datasource_DebitNote->commit();
      $datasource_DebitItem->commit();
      $return['result']='Success';
      $this->Session->setFlash(__($return['result']));
      $this->redirect(array('controller' => 'Sale', 'action' => 'DebitNote',$id));
    }
    catch (Exception $e)
    {
      $datasource_DebitNote->rollback();
      $datasource_Journal->rollback();
      $datasource_DebitItem->rollback();
      $return['result']=$e->getMessage();
      $this->redirect( Router::url( $this->referer(), true ) );
    }

  }
}
public function DebitNoteIndex()
{

}
public function credit_note_index()
{
  $requestData=$this->request->data;
  $columns = [];
  $columns[]='CreditNote.date';
  $columns[]='AccountHead.name';
  $columns[]='CreditNote.credit_no';
  $columns[]='CreditNote.id';
  $columns[]='CreditNote.id';
  $conditions=[];

  $totalData=$this->CreditNote->find('count',['conditions'=>$conditions]);
  $totalFiltered=$totalData;
  if( !empty($requestData['search']['value']) ) { 
    $q=$requestData['search']['value'];
    $conditions['OR']=array(
      'CreditNote.date LIKE' =>'%'. $q . '%',
      'AccountHead.name LIKE' =>'%'. $q . '%',
      'CreditNote.credit_no LIKE' =>'%'. $q . '%',
      );
    $totalFiltered=$this->CreditNote->find('count',[
      'conditions'=>$conditions,
      ]);
  }
  $Data=$this->CreditNote->find('all',array(
    'conditions'=>$conditions,
    'offset'=>$requestData['start'],
    'limit'=>$requestData['length'],
    'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
    'fields'=>array(
      'CreditNote.*',
      'AccountHead.id',
      'AccountHead.name',
      )
    ));
  foreach ($Data as $key => $value) {

    $Data[$key]['CreditNote']['date']=date('d-m-Y',strtotime($value['CreditNote']['date']));
    $Data[$key]['CreditNote']['action']='<span><a target="_blank" href="'.$this->webroot.'Sale/CreditNote/'.$value['CreditNote']['id'].'"><i class="fa fa-2x fa-eye"></i></a></span><span><a target="_blank" href="'.$this->webroot.'Print/creditnote/'.$value['CreditNote']['id'].'"><i class="fa fa-2x fa-print"></i></a></span>';
  }
  $json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData),
    "recordsFiltered"=>intval($totalFiltered),
    "records"        =>$Data
    );
  echo json_encode($json_data); exit;
}
public function debit_note_index()
{
  $requestData=$this->request->data;
  $columns = [];
  $columns[]='DebitNote.date';
  $columns[]='AccountHead.name';
  $columns[]='DebitNote.debit_no';
  $columns[]='DebitNote.id';
  $columns[]='DebitNote.id';
  $conditions=[];

  $totalData=$this->DebitNote->find('count',['conditions'=>$conditions]);
  $totalFiltered=$totalData;
  if( !empty($requestData['search']['value']) ) { 
    $q=$requestData['search']['value'];
    $conditions['OR']=array(
      'DebitNote.date LIKE' =>'%'. $q . '%',
      'AccountHead.name LIKE' =>'%'. $q . '%',
      'DebitNote.credit_no LIKE' =>'%'. $q . '%',
      );
    $totalFiltered=$this->DebitNote->find('count',[
      'conditions'=>$conditions,
      ]);
  }
  $Data=$this->DebitNote->find('all',array(
    'conditions'=>$conditions,
    'offset'=>$requestData['start'],
    'limit'=>$requestData['length'],
    'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
    'fields'=>array(
      'DebitNote.*',
      'AccountHead.id',
      'AccountHead.name',
      )
    ));
  foreach ($Data as $key => $value) {

    $Data[$key]['DebitNote']['date']=date('d-m-Y',strtotime($value['DebitNote']['date']));
    $Data[$key]['DebitNote']['action']='<span><a target="_blank" href="'.$this->webroot.'Sale/DebitNote/'.$value['DebitNote']['id'].'"><i class="fa fa-2x fa-eye"></i></a></span><span><a target="_blank" href="'.$this->webroot.'Print/debitnote/'.$value['DebitNote']['id'].'"><i class="fa fa-2x fa-print"></i></a></span>';
  }
  $json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData),
    "recordsFiltered"=>intval($totalFiltered),
    "records"        =>$Data
    );
  echo json_encode($json_data); exit;
}
public function day_register_table()
{
  $user_role_id=$this->Session->read('UserRole.id');
  $menu_id = $this->Menu->field('Menu.id',array('action ' => 'Sale/DayRegister'));
  $requestData=$this->request->data;
  $columns = [];
  $columns[]='DayRegister.id';
  $columns[]='Executive.name';
  $columns[]='DayRegister.date';
  $columns[]='Route.name';
  $columns[]='CustomerGroup.name';
  $columns[]='DayRegister.present';
  $columns[]='DayRegister.km';
  $columns[]='DayRegister.km';
  $columns[]='DayRegister.km';
  $columns[]='DayRegister.drivername';
  $columns[]='DayRegister.id';
  $columns[]='DayRegister.vehicle_condition';
  $columns[]='DayRegister.physical_condition';
  $columns[]='DayRegister.document_status';
  $columns[]='day_close';
  $conditions=[];
  // if(isset($requestData['date']))
  // {
   // $date=$requestData['date'];
   // $explode_date=explode('-',$date);
    $from=date('Y-m-d',strtotime(trim($requestData['from_date'])));
    $to=date('Y-m-d',strtotime(trim($requestData['to_date'])));
    $conditions['DayRegister.date between ? and ?']=[$from,$to];
 // }
  if(isset($requestData['executive_id']))
  {
    if($requestData['executive_id'])
      $conditions['DayRegister.executive_id']=$requestData['executive_id'];
  }
  if(isset($requestData['route_id']))
  {
    if($requestData['route_id'])
      $conditions['DayRegister.route_id']=$requestData['route_id'];
  }
  if(isset($requestData['present']))
  {
    if(in_array($requestData['present'], ['0','1']))
      $conditions['DayRegister.present']=$requestData['present'];
  }
  if(isset($requestData['customer_group_id']))
  {
    if($requestData['customer_group_id'])
      $conditions['DayRegister.customer_group_id']=$requestData['customer_group_id'];
  }
  $totalData=$this->DayRegister->find('count',['conditions'=>$conditions]);
  $totalFiltered=$totalData;
  if( !empty($requestData['search']['value']) ) { 
    $q=$requestData['search']['value'];
    $conditions['OR']=array(
      'Executive.name LIKE' =>'%'. $q . '%',
      'Route.name LIKE' =>'%'. $q . '%',
      'CustomerGroup.name LIKE' =>'%'. $q . '%',
      'DayRegister.km LIKE' =>'%'. $q . '%',
      'DayRegister.date LIKE' =>'%'. date('Y-m-d',strtotime($q)) . '%',
      );
    $totalFiltered=$this->DayRegister->find('count',[
      'conditions'=>$conditions,
      ]);
  }
  $Data=$this->DayRegister->find('all', array(
    'conditions' => $conditions,
    'offset'=>$requestData['start'],
    'limit'=>$requestData['length'],
    'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
    'fields' => array(
      'DayRegister.*',
      'Route.name',
      'Executive.*',
      'CustomerGroup.name',
      )
    ));
//pr($Data);exit;
  foreach ($Data as $key => $value) {
    $Data[$key]['DayRegister']['present']=$value['DayRegister']['present']?'Yes':"No";
    $Data[$key]['DayRegister']['to_km']=floatval($value['DayRegister']['km']);
     $Data[$key]['DayRegister']['km']=floatval($value['DayRegister']['km']);
    $Data[$key]['DayRegister']['time']="";
    if($value['DayRegister']['created_at'])
    {
      $time = new DateTime($value['DayRegister']['created_at']);
      $time = $time->format('H:i');
      $Data[$key]['DayRegister']['time']=$time;
    }
    $Data[$key]['DayRegister']['closetime']="";
    if($value['ClosingDay'])
    {
      if($value['ClosingDay'][0]['created_at'])
      {
        $time1 = new DateTime($value['ClosingDay'][0]['created_at']);
        $time1= $time1->format('H:i');
        $Data[$key]['DayRegister']['closetime']=$time1;
      }
    }
    $Data[$key]['DayRegister']['date']=date('d-m-Y',strtotime($value['DayRegister']['date']));
    $Data[$key]['DayRegister']['day_close_delete']='';
    if($value['ClosingDay'])
    {
      $Data[$key]['DayRegister']['to_km']=floatval($value['ClosingDay'][0]['km']);
      $Data[$key]['DayRegister']['day_close_delete']="<a onclick='return confirm('Are you sure?')' href='".$this->webroot.'Sale/DeleteDayClose/'.$value["ClosingDay"][0]["id"]."'><i class='fa fa-2x fa-trash delete_action'></i></a>";
    }
    if($value['DayRegister']['status']==1)
    {
      $Data[$key]['DayRegister']['day_close']="Yes";
    }
    else
    {
         $Data[$key]['DayRegister']['day_close']='<span hidden  class="day_register_id">'.$value['DayRegister']['id'].'</span><input type="checkbox" class="erp_dayclose" style="height:20px;width:20px;color: #3c8dbc;"/>'; 
    }
    $Data[$key]['DayRegister']['diffrence']=floatval($Data[$key]['DayRegister']['to_km']-$value['DayRegister']['km']);

    $Data[$key]['DayRegister']['action']='<i data-id="'.$value['DayRegister']['id'].'" class="fa fa-edit edit_dayRegister" aria-hidden="true" style="color: #3c8dbc;"></i>';

    $Data[$key]['DayRegister']['day_register_delete']='<a href="'.$this->webroot.'Sale/DeleteDayRegister/'.$value["DayRegister"]["id"].'"><i class="fa fa-trash delete_action"></i></a>';

    $Data[$key]['DayRegister']['VehicleNo']=$this->VehicleNo->field('VehicleNo.number',array('VehicleNo.id'=>$value['DayRegister']['vehicle_id']));

  }
  $json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData),
    "recordsFiltered"=>intval($totalFiltered),
    "records"        =>$Data
    );
  echo json_encode($json_data);
  exit;
}
public function erp_dayclose() {
        $return['status']='Error';
        if($this->request->data)
        {
          $data=$this->request->data;
          $datasource_ClosingDay = $this->ClosingDay->getDataSource();
          $datasource_DayRegister = $this->DayRegister->getDataSource();
          try {
            $datasource_ClosingDay->begin();
            $datasource_DayRegister->begin();
            $DayRegister=$this->DayRegister->findById($data['id']); 
           $date = $DayRegister['DayRegister']['date'];
          $tableData = [
          'executive_id' => $DayRegister['DayRegister']['executive_id'],
          'date' => $date,
          'km' => $DayRegister['DayRegister']['km'],
          'closing_stock'=>0,
          'closing_at'=>date('Y-m-d H:i:s'),
          'status' => 1,
          'created_at'=>date('Y-m-d H:i:s'),
          'day_register_id' =>$data['id'],
          ];

          $this->ClosingDay->create();
          if (!$this->ClosingDay->save($tableData))
            throw new Exception("Error in day closing", 1);
          $this->DayRegister->id=$data['id'];
          if (!$this->DayRegister->saveField('status','1'))
            throw new Exception("Error in DayRegister", 1);
          $datasource_ClosingDay->commit();
          $datasource_DayRegister->commit();
          $return['status']='success';
        } catch (Exception $e) 
        {
          $datasource_ClosingDay->rollback();
          $datasource_DayRegister->rollback();
          $return['status']=$e->getMessage();
        }
      }
        echo json_encode($return);
        exit;
      }
public function DeleteDayClose($id)
{
  try {
    if(!$this->ClosingDay->delete($id)) 
      throw new Exception("Cant Delete ClosingDay", 1);
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  $this->Session->setFlash(__($return['result']));
  $this->redirect( Router::url( $this->referer(), true ) );
}
public function DeleteDayRegister($id)
{
  try {
    if($this->ClosingDay->findByDayRegisterId($id))
      throw new Exception("Day Closed So Cant Delete DAy Register", 1);
    if(!$this->DayRegister->delete($id))
      throw new Exception("Cant Delete DayRegister", 1);
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  $this->Session->setFlash(__($return['result']));
  $this->redirect( Router::url( $this->referer(), true ) );
}
public function ProductWiseOffers($id=null)
{
    $user_id=1;
    $warehouses=$this->Warehouse->find('list',array(
            'fields'=>array('id','name'),
            'order'=>array('name ASC'),
            "joins"=>array(
      ),
      'conditions'=>array('Warehouse.warehouse_id !='=>"")
           ));
    $this->set('Warehouse',$warehouses);
    $Executive=$this->Executive->find('list',array(
             'fields'=>array('id','name'),
            'order'=>array('name ASC'),
            "joins"=>array(
      ),
           ));
    $this->set('Executive',$Executive);
     $Product_list=$this->Product->find('list',array(
             'fields'=>array('id','name'),
            'order'=>array('name ASC'),
            "joins"=>array(
      ),
        'conditions'=>array('Product.active=1','Product.type ='=>2)
           ));
    $this->set('Product_list',$Product_list);
  if (!$this->request->data)
  {
      if(empty($id)){
      $data['ProductWiseOffer']['date']=date('d-m-Y');
      $this->request->data=$data;
      } 
      else
      {
        $ProductWiseOffer=$this->ProductWiseOffer->findById($id);
      $ProductWiseOfferItem=$this->ProductWiseOffersItem->find('all',array('conditions'=>array('productwiseoffer_id'=>$id)));
      $this->request->data=$ProductWiseOffer;
      $this->set('ProductWiseOffersItem',$ProductWiseOfferItem);
      $this->request->data['ProductWiseOffer']['date']=date('d-m-Y',strtotime($ProductWiseOffer['ProductWiseOffer']['date']));
      }
  }
  else
  {
    $datasource_productwiseoffer=$this->ProductWiseOffer->getDataSource();
    $datasource_productwiseofferItem=$this->ProductWiseOffersItem->getDataSource();
    $datasource_Stock=$this->Stock->getDataSource();
    try{
    $datasource_productwiseoffer->begin();
    $datasource_productwiseofferItem->begin();
    $datasource_Stock->begin();
        $data=$this->request->data['ProductWiseOffer'];
        if(empty($id))
        {
          $ProductWiseOffers=$this->ProductWiseOffer->find('first',array('order' => 'ProductWiseOffer.id DESC',));
          if(!empty($ProductWiseOffers))
          {
          $TransferNo = $ProductWiseOffers['ProductWiseOffer']['transfer_no'];
          $transfer_no=$TransferNo+1;
          }
          else
          {
          $transfer_no=1;
          }
          $data['warehouse_id']=1;
          $Save_data=[
          'date'=>date('Y-m-d',strtotime($data['date'])),
          'warehouse_id'=>$data['warehouse_id'],
          'remarks'=>$data['remarks'],
          'executive_id'=>$data['executive_id'],
          'transfer_no'=>$transfer_no,
          'created_by'=>$user_id,
          'modified_by'=>$user_id,
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s'),
          ];
          $this->ProductWiseOffer->create();
          if(!$this->ProductWiseOffer->save($Save_data))
          {
            $errors = $this->ProductWiseOffers->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
          $productwiseoffer_id=$this->ProductWiseOffer->getLastInsertId();
          for ($i=0; $i <count($data['product_id']) ; $i++) 
          {
            $Item_data=[
            'product_id'=>$data['product_id'][$i],
            'productwiseoffer_id'=>$productwiseoffer_id,
            'quantity'=>$data['quantity'][$i],
            ];
                $this->ProductWiseOffersItem->create();
                if(!$this->ProductWiseOffersItem->save($Item_data))
                {
                  $errors = $this->ProductWiseOffersItem->validationErrors;
                  foreach ($errors as $key => $value) {
                    throw new Exception($value[0], 1);
                  }
                }
                else
                {
                  $fromStock=$this->Stock->find('first',array(
                    'conditions'=>array(
                      'Stock.product_id'=>$data['product_id'][$i],
                      'Stock.warehouse_id'=>$data['warehouse_id']
                      ),
                    'fields'=>array('Stock.id','Stock.quantity')
                    ));
                    if($fromStock)
                    {
                      $date=date('Y-m-d',strtotime($data['date']));
                    $fromQty=$fromStock['Stock']['quantity']-$data['quantity'][$i];
                    $remark='F&C --'.$transfer_no.'('.$data['quantity'][$i].')';
                    if($fromQty<0)
                    throw new Exception("Empty From Stock", 1);
                    $StockController = new StockController;
                    $Stock_function_return=$StockController->GeneralStock_Edit_Function($fromStock['Stock']['id'],$fromQty,$date,$user_id,$remark);
                    if($Stock_function_return['result']!='Success')
                    throw new Exception($Stock_function_return['result']);
                    }
                }
              }
          }
          $return['result']="Success";
        $datasource_productwiseoffer->commit();
      $datasource_productwiseofferItem->commit();
      $datasource_Stock->commit();
    }
    catch (Exception $e)
    {
      $return['result']=$e->getMessage();
      $datasource_productwiseoffer->rollback();
     $datasource_productwiseofferItem->rollback();
            $datasource_Stock->commit();
    }
    $this->Session->setFlash(__($return['result']));
  $this->redirect(array('controller' => 'Sale','action' =>'ProductWiseOffersList'));
}
}

public function ProductWiseOffersList()
{
$this->set('from',date("d-m-Y", strtotime('-30 day')));
  $this->set('to',date("d-m-Y"));
   $Executive=$this->Executive->find('list',array(
            'fields'=>array('id','name'),
            'order'=>array('name DESC'),
            "joins"=>array(
      ),
           ));
    $this->set('Executive',$Executive);
}
public function product_wise_table_ajax()
{
  $user_role_id=$this->Session->read('UserRole.id');
  $requestData=$this->request->data;
  $columns = [];
  $columns[]='ProductWiseOffer.transfer_no';
  $columns[]='ProductWiseOffer.date';
  $columns[]='Executive.name';
  $columns[]='ProductWiseOffer.remark';
  $columns[]='ProductWiseOffer.id'; 
  $conditions=[];
$from_date = date('Y-m-d',strtotime($requestData['from_date']));
  $to_date   = date('Y-m-d',strtotime($requestData['to_date']));
  if(isset($from_date) && isset($to_date))
  {
    $conditions['ProductWiseOffer.date BETWEEN ? and ?'] =  array($from_date, $to_date);
  }
  if(isset($requestData['executive_id']))
  {
    if($requestData['executive_id'])
    $conditions['ProductWiseOffer.executive_id']=$requestData['executive_id'];
  }
  $totalData=$this->ProductWiseOffer->find('count',['conditions'=>$conditions]);
  $totalFiltered=$totalData;
  if( !empty($requestData['search']['value']) ) { 
    $q=$requestData['search']['value'];
    $conditions['OR']=array(
      'Warehouse.name LIKE' =>'%'. $q . '%',
      'ProductWiseOffer.remark LIKE' =>'%'. $q . '%',
    );
    $totalFiltered=$this->ProductWiseOffer->find('count',[
      'conditions'=>$conditions,
    ]);
  }
    $Data=$this->ProductWiseOffer->find('all',array(
    'conditions'=>$conditions,
    'offset'=>$requestData['start'],
    'limit'=>$requestData['length'],
    'fields'=>array(
      'ProductWiseOffer.*',
      'Executive.name',
    )
  ));
  foreach ($Data as $key => $value) {
    $table_id=$value['ProductWiseOffer']['id'];
    $Data[$key]['ProductWiseOffer']['action']='<span hidden class="table_id">'.$table_id.'</span>';
     $Data[$key]['ProductWiseOffer']['action']='<a href="'.$this->webroot.'Sale/ProductWiseOffers/'.$table_id.'" title=""><span<i class="fa fa-2x fa-eye" aria-hidden="true"></i></span></a>';
  }
  $json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData),
    "recordsFiltered"=>intval($totalFiltered),
    "records"        =>$Data
  );
  echo json_encode($json_data);
  exit; 
}


public function ProductWiseOffer_delete($id)
{
    $datasource_ProductwiseOffers = $this->ProductWiseOffer->getDataSource();
    try {
      $datasource_ProductwiseOffers->begin();
      $ProductwiseOffer=$this->ProductWiseOffer->findById($id); 
      $status=$ProductwiseOffer['ProductWiseOffer']['status'];
      if($status=='Pending')
      {
      if(!$this->ProductWiseOffer->delete($id))
        throw new Exception("Cant Delete", 1);
        $return['result']='Success';
      }
      else
      {
        $return['result']='Cant Delete';
      }
      $datasource_ProductwiseOffers->commit();
    } catch (Exception $e) {
      $datasource_ProductwiseOffers->rollback();
      $return['result']=$e->getMessage();
    }
    echo json_encode($return); 
    exit;
}
public function invoice_list_ajax()
{
  $user_branch_id=$this->Session->read('User.branch_id');
  $requestData=$this->request->data;
  $columns=[];
  $columns[]='Sale.date_of_delivered';
  $columns[]='Executive.name';
  $columns[]='AccountHead.name';
  $columns[]='Route.name';
  $columns[]='Sale.invoice_no';
  $columns[]='quantity';
  $columns[]='Sale.grand_total';
  $columns[]='Sale.status';
  $conditions=[];
   $order= '';
  if(!in_array($requestData['order'][0]['column'],[5]))
  {
    $order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
  }
  if(isset($requestData['from_date']) && isset($requestData['to_date']) )
  {
    $conditions['Sale.date_of_delivered between ? and ?']=[
      date('Y-m-d',strtotime($requestData['from_date'])),
      date('Y-m-d',strtotime($requestData['to_date']))
    ];
  }
  if(isset($requestData['executive_id']))
  {
    if($requestData['executive_id'])
      $conditions['Sale.executive_id']=$requestData['executive_id'];
  }
 if(!empty($user_branch_id))
 {
   $conditions['Sale.branch_id']=$user_branch_id;
 }
 $this->Sale->unbindModel(
            array('hasMany' => array(
              'SaleItem',
              ))
            );
  $totalData=$this->Sale->find('count',[ "joins" => array(
        array(
          "table" => 'customers',
          "alias" => 'Customer',
          "type" => 'inner',
          "conditions" => array('Customer.account_head_id=Sale.account_head_id'),
          ),
        array(
          "table" => 'routes',
          "alias" => 'Route',
          "type" => 'left',
          "conditions" => array('Customer.route_id=Route.id'),
          ),
        ),'conditions'=>$conditions]);
    $totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
    if( !empty($requestData['search']['value']) ) { 
      $q=$requestData['search']['value'];
      $conditions['OR']=array(
        'Sale.date_of_delivered LIKE' =>'%'. $q . '%',
        'Executive.name LIKE' =>'%'. $q . '%',
         'Route.name LIKE' =>'%'. $q . '%',
        'AccountHead.name LIKE' =>'%'. $q . '%',
        'Sale.invoice_no LIKE' =>'%'. $q . '%',
        'Sale.grand_total LIKE' =>'%'. $q . '%',
      );
      $totalFiltered=$this->Sale->find('count',[ "joins" => array(
        array(
          "table" => 'customers',
          "alias" => 'Customer',
          "type" => 'inner',
          "conditions" => array('Customer.account_head_id=Sale.account_head_id'),
          ),
        array(
          "table" => 'routes',
          "alias" => 'Route',
          "type" => 'left',
          "conditions" => array('Customer.route_id=Route.id'),
          ),
        ),
        'conditions'=>$conditions,
      ]);
    }
     $this->Sale->unbindModel(
            array('hasMany' => array(
              'SaleItem',
              ))
            );
    $Data=$this->Sale->find('all',array(
       "joins" => array(
        array(
          "table" => 'customers',
          "alias" => 'Customer',
          "type" => 'inner',
          "conditions" => array('Customer.account_head_id=Sale.account_head_id'),
          ),
        array(
          "table" => 'routes',
          "alias" => 'Route',
          "type" => 'left',
          "conditions" => array('Customer.route_id=Route.id'),
          ),
        ),
      'conditions'=>$conditions,
      'offset'=>$requestData['start'],
      'limit'=>$requestData['length'],
      'order'=>$order,
      'fields'=>array(
        'Sale.*',
        'AccountHead.id',
        'AccountHead.name',
        'Executive.name',
         'Route.name',
      )
    ));
    function get_status_name($status)
    {
      $name='';
      if($status==0) { $name='Cancelled'; }
      if($status==1) { $name='Order Placed'; }
      if($status==2) { $name='Order Delivered'; }
      if($status==3) { $name='Bill Edited'; }
      return $name;
    }
    foreach ($Data as $key => $value) {
    $this->SaleItem->unbindModel(array('belongsTo'=>array('Product','Warehouse','Sale')));
     $this->SaleItem->virtualFields = array('SaleItem_qty' => "SUM(SaleItem.quantity)");
     $SaleItem=$this->SaleItem->find('first',array('fields'=>array('SaleItem.SaleItem_qty'),'conditions'=>['SaleItem.sale_id'=>$value['Sale']['id']]));
     $Data[$key]['Sale']['updated_at']=date('d-m-Y h:i:s',strtotime($value['Sale']['updated_at']));
     $Data[$key]['Sale']['created_at']=date('d-m-Y h:i:s',strtotime($value['Sale']['created_at']));
     $Data[$key]['Sale']['quantity']=floatval($SaleItem['SaleItem']['SaleItem_qty']);
    // $Data[$key]['Sale']['no_of_items']=$this->SaleItem->find('count',['conditions'=>['SaleItem.sale_id'=>$value['Sale']['id']]]);
     $Data[$key]['Sale']['date_of_delivered']=date('d-m-Y',strtotime($value['Sale']['date_of_delivered']));
     $Data[$key]['Sale']['grand_total']=str_replace(',','',number_format((float)$value['Sale']['grand_total'],2));
     if($value['Sale']['status']==0) { $name='Cancelled'; }
      if($value['Sale']['status']==1) { $name='Order Placed'; }
      if($value['Sale']['status']==2) { $name='Order Delivered'; }
      if($value['Sale']['status']==3) { $name='Bill Edited'; }
     $Data[$key]['Sale']['status']=$name;
     $Data[$key]['Sale']['action']='<span><a target="_blank" href="'.$this->webroot.'Print/fpdf/'.$value['Sale']['id'].'"><i class="fa fa-2x fa-print"></i></a></span>';
     $Data[$key]['Sale']['action'].='&nbsp;&nbsp;<span><a target="_blank" href="'.$this->webroot.'Sale/Sale/'.$value['Sale']['id'].'"><i class="fa fa-2x fa-eye"></i></span></a>';
   }
   //sorting total/balance/recieved
  if(in_array($requestData['order'][0]['column'],[5])) {

    //callbackfunction
    function cmp($a, $b,$columnname,$columndir)
    {
      if ($a["Sale"][$columnname] == $b["Sale"][$columnname]) {
        return 0;
      }
      if($columndir == 'asc') {
        return (str_replace(',','',$a["Sale"][$columnname]) < str_replace(',','',$b["Sale"][$columnname])) ? -1 : 1;
      }
      else if($columndir == 'desc')
      {
        return (str_replace(',','',$a["Sale"][$columnname]) > str_replace(',','',$b["Sale"][$columnname])) ? -1 : 1;
      }
    }

    $columnname = $columns[$requestData['order'][0]['column']]; //column name
    $columndir = $requestData['order'][0]['dir']; //column order
    //sorting function
    usort($Data, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
  }
   $json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData),
    "recordsFiltered"=>intval($totalFiltered),
    "records"        =>$Data
  );
   echo json_encode($json_data); exit;
 }
 public function update_salereturn_data()
{
      //$this->SalesReturn->unbindModel(array('hasMany' => array('SalesReturnItem')));
  $SalesReturn=$this->SalesReturn->find('all',array(
    'conditions'=>array(
      )
    ));
      foreach ($SalesReturn as $key => $value) 
    {
        // $net_value=0;$tax_amount=0;
       $SalesReturnItem=$value['SalesReturnItem'];
       foreach ($SalesReturnItem as $key1 => $value1) 
       {
        if($value1['total']==0)
       {
       $net_value =$value1['net_value'];
       $tax_amount = $value1['tax_amount'];
       $total=$net_value+$tax_amount;
        $this->SalesReturnItem->id=$value1['id'];
        if(!$this->SalesReturnItem->saveField('total',$total))
          throw new Exception("Error Processing in Updation", 1);
        }
       
     }
   }
  exit;
}
public function find_mismatch()
{
      //$this->SalesReturn->unbindModel(array('hasMany' => array('SalesReturnItem')));
  $Sale=$this->Sale->find('all',array(
    'conditions'=>array(
      )
    ));
      foreach ($Sale as $key => $value) 
    {
        $net_value=0;$tax_amount=0;
        $taxable_value=$value['Sale']['taxable_amount'];
       $SaleItem=$value['SaleItem'];
       foreach ($SaleItem as $key1 => $value1) 
       {
       $netvalue =$value1['net_value'];
        $net_value = $net_value + $netvalue;
         $taxamount=$value1['tax_amount'];
            $tax_amount = $tax_amount + $taxamount;
        }
        $this->Sale->id=$value['Sale']['id'];
        if(!$this->Sale->saveField('taxable_amount',$net_value))
         throw new Exception("Error Processing in Updation", 1);
        if(!$this->Sale->saveField('tax_amount',$tax_amount))
         throw new Exception("Error Processing in Updation", 1);
       
   }
  exit;
}
}
