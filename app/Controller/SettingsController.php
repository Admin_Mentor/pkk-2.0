<?php
App::uses('Controller', 'Controller');
App::uses('CakeTime', 'Utility');
App::uses('AppController', 'Controller');
App::import('Controller', 'Sale');
App::import('Controller', 'Reports');
ini_set('max_execution_time', 1000);
ini_set('memory_limit', '512M');
// App::import('Controller', 'Accountings');
App::import('Controller', 'Sale');
App::import('Controller', 'Accounts');
/**
* Settings Controller
*/
class SettingsController extends AppController {
	public $uses=[
	'Type',
	'MasterGroup',
	'Group',
	'SubGroup',
	'AccountHead',
	'Executive',
	'Location',
	'Route',
	'Customer',
	'Warehouse',
	'Sale',
	'Staff',
	'ExecutiveRouteMapping',
	'SaleItem',
	'Stock',
	'Journal',
	'Product',
	'CustomerType',
	'ClosingDay',
	'NoSale',
	'CustomerGroup',
	'DayRegister',
	'SalesReturn',
	'SalesReturnItem',
	'Division',
	'CustomerType',
	'StockLog',
	'ProductTypeBrandMapping',
	'ProductType',
	'SaleTarget',
	'Profile',
	'StockTransfer',
	'StockTransferItem',
	'BankDetails',
	'ExecutiveBonusDetail',
	'ExecutiveBonus',
	'ProductBonusDetail',
	'Unit',
	'CustomerDiscount',
	'Cheque',
	'SystemParameter',
	'Order',
	'OrderItem',
	'ExecutiveExpenseDetail',
	'ExecutiveDenomination',
	'VehicleNo',
	'State',
	'ExecutiveBonusCalculation',
	'CustomerPrice',
	'ExecutiveRate'
	];
	
	public function index()
	{
		pr('Settings Controller '); exit;
	}
	public $components = array('RequestHandler','Flash','Session');
//executive login --ubm
	public function executive_login()
	{
		$result=[];
		$data=$this->request->data;
		if(isset($data['user_name']))
		{
			$username=$data['user_name'];	
		}
		if(isset($data['password']))
		{
			$password=$data['password'];	
		}
		if(isset($username) && isset($password))
		{
			$Executive=$this->Executive->find('first',array('conditions'=>array('username'=>$username,'password'=>$password)));
		  $block_days_check=$this->block_days_check($Executive['Executive']['id']);
		}
		if(!isset($Executive))
		{
			$result['status']="Invalid Username or Password";	
		}
		else
		{
			if(!$Executive)
			{ 
				$result['status']="Invalid Username or Password";
			}
			else
			{
				if($Executive['Executive']['block']==1){
					$result['status']="Blocked Executive";
				}
				else if($block_days_check['is_block']==1)
				{
				$result['status']="Blocked Executive.....".$block_days_check['collection']." Cash Transfer pending";
				}
				else{
					$this->Session->write('ExUser.id', $Executive['Executive']['id']);
					$this->Session->write('ExUser.name', $Executive['Executive']['username']);
					$this->Session->write('ExUser.status', "Active"); 
					$result['status']="Login Successful";
					$result['id']=$Executive['Executive']['id'];
					$result['name']=$Executive['Executive']['username'];
				}
			}
		}
		echo json_encode($result);
		exit;
	}
   public function block_days_check($executive_id)
	{
		$is_block=0;
		$return['collection']=0;
	   $return['is_block']=0;
		$block_days=$this->SystemParameter->field('value',array('id'=>8));
		$amount_limit=$this->SystemParameter->field('value',array('id'=>17));
					$date=date("Y-m-d", strtotime('-'.$block_days.'day'));
	                // $DayRegister = $this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive_id,'DayRegister.date' => $date)));
	                // if($DayRegister){
	               $credit = $this->Route->field(
					'Route.account_head_id',
					array('id ' => $DayRegister['DayRegister']['route_id']));
	                $this->Journal->virtualFields = array('receipt_amount' => "SUM(Journal.amount)",'contra_total' => "SUM(Journal.amount)",'bonus_payment' => "SUM(Journal.amount)");	
              $receipt_total=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.receipt_no !='=>'',
					'Journal.executive_id'=>$executive_id,
					'Journal.date <='=>$date,
				),
				'fields'=>array(
					'receipt_amount',
				),
			));
              $inhandcash_transfer=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.remarks'=>'Inhand Cash Transfer',
					'Journal.executive_id'=>$executive_id,
					//'Journal.date <='=>$date,
				),
				'fields'=>array(
					'receipt_amount',
				),
			));
   //             $contra=$this->Journal->find('first',array(
			// 	'conditions'=>array(
			// 		'Journal.remarks'=>'Voucher Contra',
			// 		'Journal.date >='=> $DayRegister['DayRegister']['date'],
			// 		'Journal.credit'=> $credit,
			// 	),
			// 	'fields'=>array(
			// 		'contra_total',
			// 	),
			// ));
               $this->ExecutiveExpenseDetail->virtualFields = array('total_expense'=>"SUM(ExecutiveExpenseDetail.amount)");
					$ExecutiveExpenseDetail=$this->ExecutiveExpenseDetail->find('first',array(
			'conditions'=>array('ExecutiveExpenseDetail.executive_id'=>$executive_id,'ExecutiveExpenseDetail.date <='=>$date,'ExecutiveExpenseDetail.status'=>2),
			'fields'=>array('ExecutiveExpenseDetail.total_expense'),
			));   
					$total_expense=0;
					if($ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense'])
					{
						$total_expense=$ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense'];
					}

			 $bonus_payment_journal=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.remarks'=>'Bonus Payment',
					'Journal.credit !='=>1,
					'Journal.date <='=>$date,
					'Journal.executive_id'=>$executive_id,
				),
				'fields'=>array(
					'bonus_payment',
				),
			));
			 $bonus_payment_journal_amount=0;
              if($bonus_payment_journal)
              {
              	$bonus_payment_journal_amount=$bonus_payment_journal['Journal']['bonus_payment'];
              }
             $collection=floatval($receipt_total['Journal']['receipt_amount'])-floatval($inhandcash_transfer['Journal']['receipt_amount'])-floatval($total_expense)-floatval($bonus_payment_journal_amount);	
					$is_block=0;
					if($collection>$amount_limit)
					{
                      $is_block=1;
					}
					$return['collection']=$collection;
	                $return['is_block']=$is_block;
				//}
		return $return;
	}
//executive 
//executive reset password --ubm
	public function executive_reset_password()
	{
		$result=[];
		$data=$this->request->data;
		if(isset($this->request->data['executive_id']))
		{
			$Executive_id=$data['executive_id'];	
		}
		if(isset($data['old_password']))
		{
			$oldpassword=$data['old_password'];	
		}
		if(isset($data['new_password']))
		{
			$password=$data['new_password'];	
		}
		if(isset($Executive_id) && isset($oldpassword))
		{
			$Executive=$this->Executive->find('first',array('conditions'=>array('Executive.id'=>$Executive_id,'password'=>$oldpassword)));
			if(empty($Executive))
			{
				$result['status']="Incorrect exist Password";	
			}
			else
			{
				$this->Executive->id=$Executive_id;
				if(!$this->Executive->saveField('password', $password))
				{
					$errors = $this->Executive->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$Executive_new=$this->Executive->find('first',array('conditions'=>array('Executive.id'=>$Executive_id)));
				$result['status']="Reset Password Successful";
				$result['id']=$Executive_new['Executive']['id'];
				$result['name']=$Executive_new['Executive']['username'];
			}
		}
		else{
			$result['status']="Reset password unsuccesfull";
		}
		echo json_encode($result);
		exit;
	}
//executive eset password
// route list for executives --ubm
	public function api_get_route_list()
	{
		$return=[
		'status'=>'Empty',
		'Routes'=>[],
		];
		$data=$this->request->data;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
		}
		else{
			$return[ 'status' ]= 'Executive Requered';
		}
		if(isset($executive))
		{
			$ExecutiveRoute=$this->ExecutiveRouteMapping->find('list',array(
				"joins"=>array(
					array(
						"table"=>'routes',
						"alias"=>'Route',
						"type"=>'inner',
						"conditions"=>array('Route.id=ExecutiveRouteMapping.route_id'),
						),
					),
				'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
				'fields'=>
				'Route.name',
				'Route.id',
				'ExecutiveRouteMapping.route_id',
				));
			if($ExecutiveRoute)
			{
				$return[ 'status' ]='success';
				$All_Location=[];
				foreach ($ExecutiveRoute as $key => $value) {
					$Single_Location['id']=$key;
					$Single_Location['name']=$value;
					array_push($All_Location, $Single_Location);
				}
				$return[ 'Routes' ]=$All_Location;
			}
		}
		echo json_encode($return);
		exit;
	}
// route list for executives --ubm
// group lists --ubm
	public function api_get_group_list()
	{
		$return=[
		'status'=>'Empty',
		'Groups'=>[],
		];
		$data=$this->request->data;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
		}
		else{
			$return[ 'status' ]= 'Executive Requered';
		}
		if(isset($executive))
		{
			$CustomerGroups=$this->CustomerGroup->find('list',array(
				'fields'=>
				'CustomerGroup.name',
				'CustomerGroup.id',
				));
			if($CustomerGroups)
			{
				$return[ 'status' ]='success';
				$All_Group=[];
				foreach ($CustomerGroups as $key => $value) {
					$Single_Group['id']=$key;
					$Single_Group['name']=$value;
					array_push($All_Group, $Single_Group);
				}
				$return[ 'Groups' ]=$All_Group;
			}
		}
		echo json_encode($return);
		exit;
	}
// group lists --ubm
//route and group
	public function api_get_route_group_list()
	{
		$return=[
		'status'=>'Empty',
		'Routes'=>[],
		'Groups'=>[],
		];
		$data=$this->request->data;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
		}
		else{
			$return[ 'status' ]= 'Executive Required';
		}
		if(isset($executive))
		{
			$ExecutiveRoute=$this->Route->find('list',array(
				"joins"=>array(
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Route.id=ExecutiveRouteMapping.route_id'),
						),
					),
				'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
				'fields'=>
				'Route.name',
				'Route.id',
				));
			if($ExecutiveRoute)
			{
				$return[ 'status' ]='success';
				$All_Location=[];
				foreach ($ExecutiveRoute as $key => $value) {
					$Single_Location['id']=$key;
					$Single_Location['name']=$value;
					array_push($All_Location, $Single_Location);
				}
				$return[ 'Routes' ]=$All_Location;
			}
			$CustomerGroups=$this->CustomerGroup->find('list',array(
				'fields'=>
				'CustomerGroup.name',
				'CustomerGroup.id',
				));
			if($CustomerGroups)
			{
				$return[ 'status' ]='success';
				$All_Group=[];
				foreach ($CustomerGroups as $key => $value) {
					$Single_Group['id']=$key;
					$Single_Group['name']=$value;
					array_push($All_Group, $Single_Group);
				}
				$return[ 'Groups' ]=$All_Group;
			}
		}
		echo json_encode($return);
		exit;
	}
//route and group
//Day register for Executive
	public function api_executive_day_register()
	{
		$user_id=1;
		$return=[
		'status'=>'Empty',
		'ExecutiveMonthlyBonus'=>0.00,
		'SaleTargets' => 0.00,
		'ExecutiveRate'=>[],
		'ExProfile' =>[],
		'CompProfile' =>[],
		'RoutesList' =>[]
		];
		$data=$this->request->data;
		if(isset($data['executive_id']))
		{
			$first_date = date('Y-m-d',strtotime('first day of this month'));
			$date = date('Y-m-d');
			$executive=$data['executive_id'];
			$Executive=$this->Executive->findById($executive);
			$staff_id=$this->Executive->field('Executive.staff_id',array('Executive.id'=>$executive));
			// $ExecutiveBonus=$this->ExecutiveBonus->findByExecutiveIdAndMonthYear($executive,date('m-Y'));
			// if(!$ExecutiveBonus)
			// {
			// 	$this->ExecutiveBonus->create();
			// 	$ExecutiveBonus_data=[
			// 	'executive_id'=>$executive,
			// 	'staff_id'=>$staff_id,
			// 	'month_year'=>date('m-Y'),
			// 	'amount'=>0,
			// 	];
			// 	if(!$this->ExecutiveBonus->save($ExecutiveBonus_data)) { $errors = $this->ExecutiveBonus->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); } }
			// 	$ExecutiveBonus=$this->ExecutiveBonus->findById($this->ExecutiveBonus->getLastInsertId());
			// }
			$return['ExecutiveMonthlyBonus']=0;
			//pr($return);exit;
			$target = $this->sale_target($executive,$first_date);
			if($target)
			{
				$return[ 'SaleTargets' ]=$target;
			}
		}
		else
		{
			$return[ 'status' ]= 'Executive Required';
		}
		if(isset($executive))
		{
			$block_days_check=$this->block_days_check($executive);
			if($block_days_check['is_block']==0)
			{
			$rowcount = $this->DayRegister->find('count', array('conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' => $date)));
			if($rowcount){
				$DayRegister = $this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' => $date)));
				$return['day_register_id']=$DayRegister['DayRegister']['id'];
				$return[ 'status' ]= 'Already Registered';
				if($data['present']==0)
				{
					$return[ 'status' ]= 'Absent Already Registered';

				}
				$this->DayRegister->id=$DayRegister['DayRegister']['id'];
				$this->DayRegister->saveField('status','0');
				if($DayRegister['DayRegister']['present']==0)
				{
					if($data['present']==1)
					{
						if(!$this->DayRegister->saveField('status',0))
							throw new Exception("Cant Updated the Status", 1);
						if(!$this->DayRegister->saveField('present',$data['present']))
							throw new Exception("Cant Updated the present", 1);		
						if(!$this->DayRegister->saveField('km',$data['km']))
							throw new Exception("Cant Updated the KM", 1);	
					}
				}

			}
			else
			{
				$datasource_DayRegister = $this->DayRegister->getDataSource();
				try {
					$datasource_DayRegister->begin();
					$warehouse_id = $Executive['Executive']['warehouse_id'];
					$intial_total_stock = $this->current_van_stock($executive);
					$loading_stock =0;
					$TransferList=$this->StockTransfer->find('first',array(
						'conditions'=>array('(StockTransfer.date) '=>$date,'warehouse_to'=>$warehouse_id),
						'fields'=>'max(StockTransfer.date) as date,StockTransfer.id as id',
						));
					if(!empty($TransferList)){
						if(!empty($TransferList['StockTransfer']['id'])){
							$id = $TransferList['StockTransfer']['id'];
							$loading_stock = $this->stock_transfer_cost($TransferList['StockTransfer']['id']);
						}
					}
					$initial_stock = $intial_total_stock - $loading_stock;
					if($initial_stock <0){
						$initial_stock = 0.00;
					}
					$version_code="";
					if(isset($data['version_code']))
					{
						$version_code=$data['version_code'];
					}
					if($data['present']==1){
						$tableData = [
						'executive_id' => $executive,
						'date' => date('Y-m-d'),
						'route_id' => $data['route_id'],
						'customer_group_id' => $data['customer_group_id'],
						'km' => $data['km'],
						'present' => $data['present'],
						'initial_stock' =>$initial_stock,
						'loading_stock'=>$loading_stock,
						'created_at'=>$data['datetime'],
						'drivername'=>$data['drivername'],
						'physical_condition'=>$data['physical_condition'],
						'document_status'=>$data['document_status'],
						'vehicle_condition'=>$data['vehicle_condition'],
					    'vehicle_id'=>$data['vehicle_id'],
					    'version_code'=>$version_code,
						];
					}
					else
					{
						$tableData = [
						'executive_id' => $executive,
						'date' => date('Y-m-d'),
						'route_id' => $data['route_id'],
						'customer_group_id' => $data['customer_group_id'],
						'km' => $data['km'],
						'present' => $data['present'],
						'status'=>1,
						'initial_stock' =>$initial_stock,
						'loading_stock'=>$loading_stock,
						'created_at'=>$data['datetime'],
						'drivername'=>$data['drivername'],
						'physical_condition'=>$data['physical_condition'],
						'document_status'=>$data['document_status'],
						'vehicle_condition'=>$data['vehicle_condition'],
						'vehicle_id'=>$data['vehicle_id'],
						'version_code'=>$version_code,
						];
					}
					$this->DayRegister->create();
					if (!$this->DayRegister->save($tableData))
						throw new Exception("Error in day Registering", 1);
					$return['day_register_id']=$this->DayRegister->getLastInsertId();
					$DayRegister=$this->DayRegister->findById($this->DayRegister->getLastInsertId());
					$datasource_DayRegister->commit();
					$return['status']='success';
				} catch (Exception $e) {
					$datasource_DayRegister->rollback();
					$return['status']=$e->getMessage();
				}
			}
//if($DayRegister['DayRegister']['present']==1)
//{        
			$this->Journal->virtualFields = array();
			$return['ExProfile']['id']=$Executive['Executive']['id'];
			$return['ExProfile']['username']=$Executive['Executive']['username'];
			$return['ExProfile']['name']=$Executive['Executive']['name'];
			$return['ExProfile']['mobile']=$Executive['Executive']['mobile'];
			$return['ExProfile']['warehouse_id']=$Executive['Executive']['warehouse_id'];
			$return['ExProfile']['warehouse_name']=$Executive['Warehouse']['name'];
			$return['ExProfile']['executive_credit_limit']=$Executive['Executive']['executive_credit_limit'];
			$return['drivername']=$DayRegister['DayRegister']['drivername'];
			$return['physical_condition']=$DayRegister['DayRegister']['physical_condition'];
			$return['document_status']=$DayRegister['DayRegister']['document_status'];
			$return['vehicle_condition']=$DayRegister['DayRegister']['vehicle_condition'];
			$return['vehicle_id']=$DayRegister['DayRegister']['vehicle_id'];
			$return['version_code']=$DayRegister['DayRegister']['version_code'];
			$return['vehicle_name']=$this->VehicleNo->field('VehicleNo.number',array('VehicleNo.id'=>$DayRegister['DayRegister']['vehicle_id']));
			$return['customer_credit_limit']=$this->SystemParameter->field('value',array('id'=>18));
             $ExecutiveRate=$this->ExecutiveRate->find('all',array(
				'conditions'=>array('ExecutiveRate.executive_id'=>$executive),
				'fields'=>array(
					'ExecutiveRate.product_id',
					'ExecutiveRate.executive_rate',
					),
				));
             $return['ExecutiveRate']=[];
             if($ExecutiveRate)
             {
              $return['ExecutiveRate']=$ExecutiveRate;	
             }
			if(isset($data['route_id']))
			{
				$route_id=$data['route_id'];


			}

	//$array=[];
			$value=0;

			$debit=$this->Route->find('first',array('conditions'=>array('id'=>$route_id),'fields'=>array('account_head_id')));

			if(!$debit){

				$return['ExProfile']['cash_in_hand']=$value;

			}
			else{
				$debit=$debit['Route']['account_head_id'];

			}
			$AccountHeads=$this->AccountHead->find('first',array(
				'fields'=>[
				'AccountHead.id',
				'AccountHead.name',
				'AccountHead.created_at',
				'AccountHead.opening_balance',
				],
				'conditions'=>['AccountHead.id'=>$debit],
				));
//pr($AccountHeads);exit;
			$Journal=$this->Journal->find('all',array('conditions'=>array(
//'route_id'=>$route_id,
				'flag'=>'1',
				'debit'=>$debit

				)));
			$Journalcredit=$this->Journal->find('all',array('conditions'=>array(

				'credit'=>$debit,
				'flag'=>'1'

				)));
	//		pr($Journal);exit;
	// pr($datas);exit;

			$Data=[];
			$data=0;
			$Datas=[];
//pr($debit);
			$datas=0;
			foreach($Journal as $key=>$value){

				$Data=$value['Journal']['amount'];
				$data+=$Data;
			}
//pr($data);exit;
			foreach($Journalcredit as $key=>$value2){

				$Datas=$value2['Journal']['amount'];
				$datas+=$Datas;
			}

			$value=($data-$datas);
			$return['ExProfile']['cash_in_hand']=number_format($value,3,'.','');


			$Profile=$this->Profile->findById(1);
			$return['CompProfile']['address_line_1']=$Profile['Profile']['address_line_1'];
			$return['CompProfile']['address_line_1_arabic']=$Profile['Profile']['address_line_1_arabic'];
			$return['CompProfile']['address_line_2' ]=$Profile['Profile']['address_line_2'];
			$return['CompProfile']['address_line_2_arabic' ]=$Profile['Profile']['address_line_2_arabic'];
			$return['CompProfile']['mail_address' ]=$Profile['Profile']['mail_address'];
			$return['CompProfile']['mobile' ]=$Profile['Profile']['mobile'];
			$return['CompProfile']['cust_care_mobile' ]=$Profile['Profile']['extra_mobile'];
			$return['CompProfile']['company_name' ]=$Profile['Profile']['company_name'];
			$return['CompProfile']['company_name_arabic' ]=$Profile['Profile']['company_name_arabic'];
			$return['CompProfile']['vat_code' ]=$Profile['Profile']['vat_code'];
			$return['CompProfile']['cr_no' ]='4030594850';
		    $return['km']=$DayRegister['DayRegister']['km'];

			$Regs = $this->DayRegister->find('first', array(
				'conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' => date('Y-m-d'))));
			$return[ 'route_id' ]=$Regs['DayRegister']['route_id'];
			$route_code = $this->Route->field(
						'Route.code',
						array('id ' => $Regs['DayRegister']['route_id']));
					$return['route_code'] = $route_code;
					$Sale=$this->Sale->find('first',array('conditions'=>array('is_erp'=>0,'Sale.invoice_no LIKE'=>$route_code."%"),'order' => 'Sale.id DESC',));
				    $SalesReturn=$this->SalesReturn->find('first',array('conditions'=>array('SalesReturn.invoice_no LIKE'=>$route_code."%"),'order' => 'SalesReturn.id DESC',));
					//$Sale=$this->Sale->find('first',array('order' => 'Sale.id DESC',));
					$Receipt_Sale=$this->Journal->find('first',array('conditions'=>array('Journal.receipt_no LIKE'=>$route_code."%"),
						'order' => 'Journal.id DESC',));
					if(!empty($Receipt_Sale)){
						$ReceiptNostr = $Receipt_Sale['Journal']['receipt_no'];
						$ReceiptNo = str_replace($route_code, '', $ReceiptNostr);
					}else{
						$ReceiptNo = 110;
					}

					if(!empty($Sale))
					{
						$SaleNostr = $Sale['Sale']['invoice_no'];
						$SaleNo = str_replace($route_code, '', $SaleNostr);
					}
					else{
						$SaleNo = 000;
					}
					if(!empty($SalesReturn))
					{
						$SalesReturnNostr = $SalesReturn['SalesReturn']['invoice_no'];
						$SalesReturnNo = str_replace($route_code, '', $SalesReturnNostr);
					}
					else{
						$SalesReturnNo = 000;
					}
			$return['invoice_no']=$SaleNo;
			$return['return_invoice_no']=$SalesReturnNo;
			$return['receipt_no']=$ReceiptNo;
			$route_name = $this->Route->field(
				'Route.name',
				array('id ' => $Regs['DayRegister']['route_id']));

			$route_mobile = $this->Route->field(
				'Route.mobile',
				array('id ' => $Regs['DayRegister']['route_id']));
			$return[ 'route_name' ]=$route_name;
			$return[ 'route_mobile' ]=$route_mobile;
          
			$ExecutiveRoute=$this->Route->find('list',array(
				"joins"=>array(
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Route.id=ExecutiveRouteMapping.route_id'),
						),
					),
				'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
				'fields'=>
				'Route.name',
				'Route.id',
				));
			if($ExecutiveRoute)
			{
				$All_Location=[];
				foreach ($ExecutiveRoute as $key => $value) {
					$route_code = $this->Route->field(
						'Route.code',
						array('id ' => $key));
					$Single_Location['route_code'] = $route_code;
					// $Sale=$this->Sale->find('first',array('conditions'=>array('is_erp'=>0,'Sale.invoice_no LIKE'=>$route_code."%"),'order' => 'Sale.id DESC',));
					// //$Sale=$this->Sale->find('first',array('order' => 'Sale.id DESC',));
					// $Receipt_Sale=$this->Journal->find('first',array('conditions'=>array('Journal.receipt_no LIKE'=>$route_code."%"),
					// 	'order' => 'Journal.id DESC',));
					// if(!empty($Receipt_Sale)){
					// 	$ReceiptNostr = $Receipt_Sale['Journal']['receipt_no'];
					// 	$ReceiptNo = str_replace($route_code, '', $ReceiptNostr);
					// }else{
					// 	$ReceiptNo = 110;
					// }

					// if(!empty($Sale))
					// {
					// 	$SaleNostr = $Sale['Sale']['invoice_no'];
					// 	$SaleNo = str_replace($route_code, '', $SaleNostr);
					// }
					// else{
					// 	$SaleNo = 000;
					// }
					// $Single_Location['invoice_no']=$SaleNo;
					// $Single_Location['receipt_no']=$ReceiptNo;
					array_push($All_Location, $Single_Location);
				}
				$return[ 'RoutesList' ]=$All_Location;
			}
			$Reg_Group=[];
			$group_name = $this->CustomerGroup->field(
				'CustomerGroup.name',
				array('id ' => $Regs['DayRegister']['customer_group_id']));
			$return[ 'group_nid' ]=$Regs['DayRegister']['customer_group_id'];
			$return[ 'group_name' ]=$group_name;
// }
		}
		else
		{
			$return['status']="Blocked Executive.....".$block_days_check['collection']." Cash Transfer pending";
		}
		}
		echo json_encode($return);
		exit;
	}
//Day register for Executive
// customer list for executives --ubm
	public function api_get_customer_list()
	{
		$return=[
		'status'=>'Empty',
		'Customers'=>[],
		];
		$data=$this->request->data;
		if(isset($data['executive_id']))
		{
			$date = date('Y-m-d');
			$executive=$data['executive_id'];
			$register_row = $this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' => $date)));
			if(!empty($register_row)){
				$customer_group_id = $register_row['DayRegister']['customer_group_id'];
				$route_id = $register_row['DayRegister']['route_id'];
			}
		}
		else{
			$return[ 'status' ]= 'Executive Required';
		}
		if(isset($customer_group_id))
		{
			$CustomerList=$this->AccountHead->find('first',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('Customer.account_head_id=AccountHead.id'),
						),
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
						),
					),
				'conditions'=>array('Customer.customer_group_id'=>$customer_group_id,
					'Customer.route_id'=>$route_id
					),
				'fields'=>array(
					'AccountHead.id',
					'AccountHead.name',
					),
				));
			$general_customer=$this->AccountHead->find('first',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('Customer.account_head_id=AccountHead.id'),
						),
					),
				'conditions'=>array('Customer.account_head_id'=>3,
					),
				'fields'=>array(
					'AccountHead.id',
					'AccountHead.name',
					),
				));
			$CustomerList[]=$general_customer;
			sort($CustomerList);
			if($CustomerList)
			{
				$return[ 'status' ]='success';
				$All_Customer=[];
				foreach ($CustomerList as $key => $value) {
					$Single_Customer['id']=$key;
					$Single_Customer['name']=$value;
					array_push($All_Customer, $Single_Customer);
				}
				$return[ 'Customers' ]=$All_Customer;
			}
		}
		else{
			$return[ 'status' ]= 'Group is not registerd';
		}
		echo json_encode($return);
		exit;
	}
// customer list for executives --ubm
//sale action   --ubm
	public function api_order_place()
	{
		$user_id=1;
		$sale_account_head_id=6;
		$discount_received_account_head_id=12;
		$discount_paid_account_head_id=13;
		$return=[
		'status'=>'empty',
		];
		$data=$this->request->data;
// $data=$this->request->data['Sale'];
		if($data)
		{
			$executive=$data['executive_id'];
			$date = date('Y-m-d');
			$DayRegister=$this->DayRegister->findByExecutiveIdAndStatus($executive,'0');
			if(!$DayRegister){
				$return[ 'status' ]= 'Day Closed';
			}
			else
			{
				$datasource_Sale = $this->Sale->getDataSource();
				$datasource_SaleItem = $this->SaleItem->getDataSource();
				$datasource_Stock = $this->Stock->getDataSource();
				$datasource_Journal = $this->Journal->getDataSource();
				try {
					$Sale=$this->Sale->find('count');
					if(!empty($Sale))
					{
						$invoice_no=$Sale+1;
					}
					else
					{
						$invoice_no=1;
					}
					$grand_total=$data['total_amount']-$data['discount_amount'];
					$Sale_data=[
					'account_head_id'=>$data['customer_id'],
					'bill_id'=>'1',
					'executive_id'=>$data['executive_id'],
					'customer_name'=>'',
					'customer_mobile'=>'',
					'invoice_no'=>$invoice_no,
					'date_of_order'=>$date,
					'date_of_delivered'=>$date,
					'total'=>$data['total_amount'],
					'other_name'=>'',
					'other_value'=>'0',
					'discount'=>'0',
					'status'=>'2',
					'sale_type'=>$data['sale_type'],
					'discount_amount'=>$data['discount_amount'],
					'grand_total'=>$grand_total,
					'balance'=>$grand_total-$data['paid_amount'],
					'created_by'=>$user_id,
					'modified_by'=>$user_id,
					'created_at'=>$data['date'],
// 'created_at'=>date('Y-m-d H:i:s'),
					'updated_at'=>date('Y-m-d H:i:s'),
					];
					$this->Sale->create();
					if(!$this->Sale->save($Sale_data))
					{
						$errors = $this->Sale->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					$Executive=$this->Executive->findById($data['executive_id']);
					$warehouse_id=$Executive['Executive']['warehouse_id'];
					$ordered_products=$data['ordered_products'];
					$sale_id=$this->Sale->getLastInsertId();
					for ($i=0; $i <count($ordered_products) ; $i++) {
						$SaleItem_data=[
						'warehouse_id'=>$warehouse_id,
						'product_id'=>$ordered_products[$i]['product_id'],
						'sale_id'=>$sale_id,
						'unit_price'=>$ordered_products[$i]['unit_price'],
						'quantity'=>$ordered_products[$i]['product_quantity'],
						'net_value'=>$ordered_products[$i]['product_total'],
						'total'=>$ordered_products[$i]['product_total'],
						];
						$this->SaleItem->create();
						if(!$this->SaleItem->save($SaleItem_data))
						{
							$errors = $this->SaleItem->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}
					}
					$date=date('Y-m-d');
					$datasource_Stock->begin();
					$Stock_function_return=$this->GeneralStock_Edit_Function_in_Purchase($sale_id,$date);
					if($Stock_function_return['result']!='Success')
						throw new Exception($Stock_function_return['result']);
					$datasource_Stock->commit();
					$datasource_Journal->begin();
					$credit=$sale_account_head_id;
					$debit=$data['customer_id'];
					$sale_amount=$data['total_amount'];
					$work_flow='Sales';
					$invoice_no=$invoice_no;
					$date=date('Y-m-d');
					$remarks='Sale Invoice No :'.$invoice_no;
					$Accountings_function_return=$this->JournalCreate($credit,$debit,$sale_amount,$date,$remarks,$work_flow,$user_id);
					if($Accountings_function_return['result']!='Success')
						throw new Exception($Stock_function_return['message']);
					$datasource_Journal->commit();
					$discount = $data['discount_amount'];
					if($discount>0)
					{
						$debit=$discount_paid_account_head_id;
						$credit=$data['customer_id'];
					}
					else
					{
						$discount=$discount*-1;
						$credit=$discount_received_account_head_id;
						$debit=$data['customer_id'];
					}
					$Accountings_function_return=$this->JournalCreate($credit,$debit,$discount,$date,$remarks,$work_flow,$user_id);
					if($Accountings_function_return['result']!='Success')
						throw new Exception($Stock_function_return['message']);
					$paid=$data['paid_amount'];
					if($paid>0)
					{
						$cashhead = $this->Executive->field(
							'Executive.account_head_id',
							array('id ' => $data['executive_id']));
						$datasource_Journal->begin();
						$account_head_id=$data['customer_id'];
						$function_return=$this->JournalCreate_in_Sale($data,$paid,$account_head_id,$date,$cashhead,$executive);
						if($function_return['result']!='Success')
							throw new Exception($function_return['result'], 1);
						$datasource_Journal->commit();
					}
					$return['status']='success';
					$return['invoice_no']=$invoice_no;
				} catch (Exception $e) {
					$datasource_Sale->rollback();
					$datasource_SaleItem->rollback();
					$return['status']=$e->getMessage(); 
				}
			}
		}
		echo json_encode($return);
		exit;
	}
//sale action  --ubm
	public function api_multiple_order_place()
	{

		$offline=$this->SystemParameter->field('value',array('id'=>2));
			$user_id=1;
			$sale_account_head_id=6;
			$discount_received_account_head_id=12;
			$discount_paid_account_head_id=13;
		//$tax_on_sale_account_head_id=9;
			$tax_on_sale_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DUTIES & TAXES'));
			$return=[
			'status'=>'empty',
			];
			$data=$this->request->data['Sale'];

			if($data)
			{
				$date = date('Y-m-d');
				$executive=$this->request->data['executive_id'];
				if(isset($this->request->data['day_register_id']))
				{
					$day_register_id=$this->request->data['day_register_id'];	
				}
				else
				{
					$DayRegister=$this->DayRegister->find('first',[
						'conditions'=>['DayRegister.executive_id'=>$executive],
						'order'=>['DayRegister.id DESC']]);
					$day_register_id=$DayRegister['DayRegister']['id'];
				}
				$route_id=$this->DayRegister->field(
					'DayRegister.route_id',
					array('DayRegister.id ' => $day_register_id));
				$cashhead = $this->Route->field(
					'Route.account_head_id',
					array('id ' => $route_id));
				$DayRegister=$this->DayRegister->findByExecutiveIdAndStatus($executive,'0');
				if(!$DayRegister){
					$return['status']='Day Closed';
				}
				else
				{
					$route_code = $DayRegister['Route']['code'];
					$datasource_Sale = $this->Sale->getDataSource();
					$datasource_SaleItem = $this->SaleItem->getDataSource();
					$datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
					$datasource_ExecutiveBonusCalculation = $this->ExecutiveBonusCalculation->getDataSource();
					$datasource_Stock = $this->Stock->getDataSource();
					$datasource_Journal = $this->Journal->getDataSource();
					try {
						for ($j=0; $j <count($data) ; $j++) {
							$invoice_no = $data[$j]['invoice_no'];
							$order_no = $data[$j]['invoice_no'];
							$grand_total=$data[$j]['total_amount'];
							$Sale_exist=$this->Sale->findByInvoiceNoAndGrandTotal($invoice_no,$grand_total);
							if(empty($Sale_exist)){
							$datasource_Sale->begin();
							$datasource_SaleItem->begin();
							$datasource_ProductBonusDetail->begin();
							$datasource_ExecutiveBonusCalculation->begin();
							if(empty($data[$j]['without_tax_total']))
								$data[$j]['without_tax_total']=0.00;
							if(empty($data[$j]['tax_percentage']))
								$data[$j]['tax_percentage']=0.00;
							if(empty($data[$j]['tax_amount']))
								$data[$j]['tax_amount']=0.00;
                              $Executive=$this->Executive->findById($executive);
							$warehouse_id=$Executive['Executive']['warehouse_id'];
							$Sale_data=[
							'account_head_id'=>$data[$j]['customer_id'],
							'warehouse_id'=>$warehouse_id,
							'bill_id'=>'1',
							'executive_id'=>$executive,
							'day_register_id'=>$day_register_id,
							'customer_name'=>'',
							'customer_mobile'=>'',
							'invoice_no'=>$invoice_no,
							'order_no'=>$order_no,
							'date_of_order'=>$date,
							'date_of_delivered'=>$date,
							'total'=>$data[$j]['total_amount'],
							'other_name'=>'',
							'other_value'=>'0',
							'discount'=>'0',
							'status'=>'2',
							'is_erp'=>0,
							'sale_type'=>$data[$j]['sale_type'],
							'sale_type1'=>$data[$j]['invoice_type'],
							'brand_id'=>'',
							'discount_amount'=>'0',
							'taxable_amount'=>$data[$j]['without_tax_total'],
							'tax'=>$data[$j]['tax_percentage'],
							'tax_amount'=>$data[$j]['tax_amount'],
							'grand_total'=>$grand_total,
							'balance'=>$grand_total-$data[$j]['paid_amount'],
							'day_register_id'=>$day_register_id,
							'created_by'=>$user_id,
							'modified_by'=>$user_id,
							'created_at'=>$data[$j]['bill_date'],
                            // 'created_at'=>date('Y-m-d H:i:s'),
							'updated_at'=>date('Y-m-d H:i:s'),
							];
							$this->Sale->create();
							if(!$this->Sale->save($Sale_data))
							{
								$errors = $this->Sale->validationErrors;
								foreach ($errors as $key => $value) {
									throw new Exception($value[0], 1);
								}
							}
							$ordered_products=$data[$j]['ordered_products'];
							$sale_id=$this->Sale->getLastInsertId();
							$tax_Am =0;
							$stock_value=0; $bonus_amount_total=0;
							for ($i=0; $i <count($ordered_products) ; $i++) {
								$tax = $this->Product->field(
									'Product.tax',
									array('Product.id ' => $ordered_products[$i]['product_id']));
								$cost = $this->Product->field(
									'Product.cost',
									array('Product.id ' => $ordered_products[$i]['product_id']));
								$executive_rate = $this->ExecutiveRate->field(
									'ExecutiveRate.executive_rate',
									array('ExecutiveRate.executive_id ' =>$executive,'ExecutiveRate.product_id ' => $ordered_products[$i]['product_id']));
								if(empty($executive_rate))
								{
                                  $executive_rate = $this->Product->field(
									'Product.wholesale_price',
									array('Product.id ' => $ordered_products[$i]['product_id']));
								}
								if($ordered_products[$i]['unit_price']==0)
								{
									$incl_tax=0;
									$tax_rate=0;
								}
								else
								{
									$incl_tax=($ordered_products[$i]['unit_price']*($tax/100));
									$tax_rate=($incl_tax)/($ordered_products[$i]['unit_price']+$incl_tax)*100;


								}
								$basic_rate=($ordered_products[$i]['unit_price'])-($ordered_products[$i]['unit_price']*($tax_rate/100));
//$basic_rate=($ordered_products[$i]['unit_price'])-($ordered_products[$i]['unit_price']*($tax_rate/100));
//$tax_amount = $ordered_products[$i]['product_total']-($ordered_products[$i]['product_quantity']*$basic_rate);
								//$basic_rate=($ordered_products[$i]['unit_price']);
								$tax_amount = ($ordered_products[$i]['product_quantity']*$basic_rate)*($tax/100);
								$tax_Am+=$tax_amount;
								$net_value=$ordered_products[$i]['product_quantity']*$basic_rate;
								$quantity=$ordered_products[$i]['product_quantity'];
								$stock_value+=$cost*$quantity;
								$unit_id=$this->Unit->field('Unit.id',array('Unit.name'=>'Pieces'));
								$SaleController= new SaleController;
								$UnitLevelConvert=$SaleController->UnitLevelConvert($ordered_products[$i]['product_id'],$unit_id);
								$product_cost=0;
//$product_cost=$this->Product->field('Product.cost',array('Product.id'=>$ordered_products[$i]['product_id']));
								if(!empty($UnitLevelConvert['product_cost']))
									$product_cost=$UnitLevelConvert['product_cost'];
								if($ordered_products[$i]['product_unit']=="Cases")
								{
									$product_unit=2;
									$quantity_mode="Cases";
								}
								else
								{
									$product_unit=1;
									$quantity_mode="Pieces";
								}
								$SaleItem_data=[
								'warehouse_id'=>$warehouse_id,
								'product_id'=>$ordered_products[$i]['product_id'],
								'sale_id'=>$sale_id,
								'unit_price'=>$ordered_products[$i]['unit_price'],
								'actual_price'=>$executive_rate,
								'tax'=>$tax,
								'unit_id'=>$product_unit,
								'quantity_mode'=>$quantity_mode,
								'tax_amount'=>$tax_amount,
								'quantity'=>$quantity,
								'net_value'=>$net_value,
								'total'=>$ordered_products[$i]['product_total'],
								];
								
								$this->SaleItem->create();
								if(!$this->SaleItem->save($SaleItem_data))
								{
									$errors = $this->SaleItem->validationErrors;
									foreach ($errors as $key => $value) {
										throw new Exception($value[0], 1);
									}
								}
					$CustomerPrice=$this->CustomerPrice->findByProductIdAndCustomerId($ordered_products[$i]['product_id'],$data[$j]['customer_id']);
								if(empty($CustomerPrice))
								{
								$CustomerPrice_data=[
								'customer_id'=>$data[$j]['customer_id'],
								'product_id'=>$ordered_products[$i]['product_id'],
								'updated_on'=>date('Y-m-d H:i:s'),
								'product_wholesale_price'=>$ordered_products[$i]['unit_price'],
								];
								$this->CustomerPrice->create();
									if(!$this->CustomerPrice->save($CustomerPrice_data))
									{
									$errors = $this->CustomerPrice->validationErrors;
									foreach ($errors as $key => $value) {
									throw new Exception($value[0], 1);
									}
									}
								}
								else
								{
									$this->CustomerPrice->id=$CustomerPrice['CustomerPrice']['id'];
									$CustomerPrice_data=[
								'product_wholesale_price'=>$ordered_products[$i]['unit_price'],
								       ];
									if(!$this->CustomerPrice->save($CustomerPrice_data))
									{
									$errors = $this->CustomerPrice->validationErrors;
									foreach ($errors as $key => $value) {
									throw new Exception($value[0], 1);
									}
									}
								}
								$bonus_amount=($ordered_products[$i]['unit_price']-$executive_rate)*$quantity;
								$sales_item_id=$this->SaleItem->getLastInsertId();
							if($bonus_amount>0)
						         {
						  $bonus_amount_total=$bonus_amount_total+$bonus_amount;
					         $ProductBonus_data=[
					         'date'=>date('Y-m-d'),
					         'day_register_id'=>$day_register_id,
							  'executive_id'=>$executive,
					          'product_id'=>$ordered_products[$i]['product_id'],
					          'sale_item_id'=>$sales_item_id,
					          'sale_id'=>$sale_id,
					          'unit_price'=>$ordered_products[$i]['unit_price'],
					          'executive_rate'=>$executive_rate,
					          'quantity'=>$quantity,
					          'bonus_amount'=>$bonus_amount,
					          ];
					          $this->ProductBonusDetail->create();
					          if(!$this->ProductBonusDetail->save($ProductBonus_data))
					          {
					            $errors = $this->ProductBonusDetail->validationErrors;
					            foreach ($errors as $key => $value) {
					              throw new Exception($value[0], 1);
					            }
					          }
					        }
							}
							if($bonus_amount_total>0)
							{
							$Bonus_data=[
							 'day_register_id'=>$day_register_id,
							'executive_id'=>$executive,
							'date'=>date('Y-m-d'),
							'work_flow'=>"SALE",
						    'reference_no'=>$invoice_no,
							'bonus_amount'=>$bonus_amount_total,
							];
							$this->ExecutiveBonusCalculation->create();
							if(!$this->ExecutiveBonusCalculation->save($Bonus_data))
							{
							$errors = $this->ExecutiveBonusCalculation->validationErrors;
							foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
							}
							}
							}
							$tax_value = $tax_Am;
							$date=date('Y-m-d');
							$datasource_Stock->begin();
							$Stock_function_return=$this->GeneralStock_Edit_Function_in_Purchase($sale_id,$date);
							if($Stock_function_return['result']!='Success')
								throw new Exception($Stock_function_return['result']);
							$datasource_Stock->commit();
							$datasource_Sale->commit();
							$datasource_SaleItem->commit();
							$datasource_ProductBonusDetail->commit();
							$datasource_ExecutiveBonusCalculation->commit();
							$datasource_Journal->begin();
							$credit=$sale_account_head_id;
							$debit=$data[$j]['customer_id'];
							$sale_amount=$data[$j]['total_amount'];
							$work_flow='Sales From Executive app';
							$invoice_no=$invoice_no;
						//$date=date('Y-m-d',strtotime($data[$j]['bill_date']));
							$remarks='Sale Invoice No :'.$invoice_no;
							$Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
							if(!empty($Journals))
							{
								$JournalsNo = $Journals['Journal']['voucher_no'];
								$Journals_no=$JournalsNo+1;
							}
							else
							{
								$Journals_no=1;
							}
							$receipt_no="";
							$sale_net_value = $sale_amount - $tax_value;
						// $route_id=$this->ExecutiveRouteMapping->field('ExecutiveRouteMapping.route_id',array('ExecutiveRouteMapping.executive_id'=>$executive));
							$Accountings_function_return=$this->JournalCreate($credit,$debit,$sale_net_value,$date,$remarks,$work_flow,$user_id,$Journals_no,$receipt_no,$day_register_id,$executive,'',$route_id);
							$datasource_Journal->commit();
							if($Accountings_function_return['result']!='Success')
								throw new Exception($Stock_function_return['result']);
							$discount = 0;
							$Accountings_function_return=$this->JournalCreate($credit,$debit,$discount,$date,$remarks,$work_flow,$user_id,$Journals_no,$receipt_no,$day_register_id,$executive,'',$route_id);
							if($Accountings_function_return['result']!='Success')
								throw new Exception($Stock_function_return['result']);
							$stock_account_head_id=19;
							if($stock_value)
							{
								$debit=$sale_account_head_id;
								$credit=$stock_account_head_id;

								$Accountings_function_return=$this->JournalCreate($credit,$debit,$stock_value,$date,$remarks,$work_flow,$user_id,$Journals_no,$receipt_no,$day_register_id,$executive,'',$route_id);
								if($Accountings_function_return['result']!='Success')
									throw new Exception($Accountings_function_return['message']);
							}
							if($tax_value){
								$debit=$data[$j]['customer_id'];
								$credit=$tax_on_sale_account_head_id;
								$Accountings_function_return=$this->JournalCreate($credit,$debit,$tax_value,$date,$remarks,$work_flow,$user_id,$Journals_no,$receipt_no,$day_register_id,$executive,'',$route_id);
								if($Accountings_function_return['result']!='Success')
									throw new Exception($Accountings_function_return['result']);
							}
							$paid=$data[$j]['paid_amount'];
							$paid=0;
							if($paid>0)
							{
								$receipt_voucher_no='';
								$datasource_Journal->begin();
								$account_head_id=$data[$j]['customer_id'];
								$function_return=$this->JournalCreate_in_Sale($invoice_no,$paid,$account_head_id,$date,$cashhead,$receipt_voucher_no,$day_register_id,$executive);
								if($function_return['result']!='Success')
									throw new Exception($function_return['result'], 1);
							}
							$datasource_Journal->commit();
						}
					}
						$return['status']='Success';
					}
					catch (Exception $e) {
						$datasource_Sale->rollback();
						$datasource_SaleItem->rollback();
					    $datasource_ProductBonusDetail->rollback();
						$datasource_ExecutiveBonusCalculation->rollback();
						$datasource_Stock->rollback();
						$datasource_Journal->rollback();
						$return['status']=$e->getMessage(); 
					}
				}
			}
	//}
			echo json_encode($return);
			exit;
		}

//sale action  --ubm
	public function api_multiple_quotation()
	{
		$user_id=1;
		$return=[
		'status'=>'empty',
		];
		$data=$this->request->data['Sale'];
		if($data)
		{
			$date = date('Y-m-d');
			$executive=$this->request->data['executive_id'];
			if(isset($this->request->data['day_register_id']))
			{
				$day_register_id=$this->request->data['day_register_id'];	
			}
			else
			{
				$DayRegister=$this->DayRegister->find('first',[
					'conditions'=>['DayRegister.executive_id'=>$executive],
					'order'=>['DayRegister.id DESC']]);
				$day_register_id=$DayRegister['DayRegister']['id'];
			}
			$DayRegister=$this->DayRegister->findByExecutiveIdAndStatus($executive,'0');
			if(!$DayRegister){
				$return[ 'status' ]= 'Day Closed';
			}
			else
			{				
				$route_code = $DayRegister['Route']['code'];
				$datasource_Sale = $this->Sale->getDataSource();
				$datasource_SaleItem = $this->SaleItem->getDataSource();
				try {
					for ($j=0; $j <count($data) ; $j++) {
						$Sale=$this->Sale->find('first',array('conditions'=>array('is_erp'=>0,'Sale.invoice_no LIKE'=>"%".$route_code."%"),'order' => 'Sale.id DESC',));
						if(!empty($Sale))
						{
							$SaleNostr = $Sale['Sale']['invoice_no'];
							$SaleNo = str_replace($route_code, '', $SaleNostr);
							$SaleNo = $SaleNo+1;
							$invoice_no=$route_code.$SaleNo;
							$order_no=$route_code.$SaleNo;
						}
						else
						{
							$invoice_no=$route_code.'111';
							$order_no=$route_code.'111';
						}
						$grand_total=$data[$j]['total_amount'];
						$Sale_data=[
						'account_head_id'=>$data[$j]['customer_id'],
						'bill_id'=>'1',
						'executive_id'=>$executive,
						'day_register_id'=>$day_register_id,
						'customer_name'=>'',
						'customer_mobile'=>'',
						'invoice_no'=>$invoice_no,
						'order_no'=>$order_no,
						'date_of_order'=>$date,
						'date_of_delivered'=>$date,
						'total'=>$data[$j]['total_amount'],
						'other_name'=>'',
						'other_value'=>'0',
						'discount'=>'0',
						'status'=>'1',
						'is_erp'=>'0',
						'sale_type'=>$data[$j]['sale_type'],
						'discount_amount'=>'0',
						'grand_total'=>$grand_total,
						'balance'=>$grand_total,
						'created_by'=>$user_id,
						'modified_by'=>$user_id,
						'created_at'=>$data[$j]['bill_date'],
// 'created_at'=>date('Y-m-d H:i:s'),
						'updated_at'=>date('Y-m-d H:i:s'),
						];
						$this->Sale->create();
						if(!$this->Sale->save($Sale_data))
						{
							$errors = $this->Sale->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}
						$Executive=$this->Executive->findById($executive);
						$warehouse_id=$Executive['Executive']['warehouse_id'];
						$ordered_products=$data[$j]['ordered_products'];
						$sale_id=$this->Sale->getLastInsertId();
						for ($i=0; $i <count($ordered_products) ; $i++) {
							$tax = $this->Product->field(
								'Product.vat',
								array('Product.id ' => $ordered_products[$i]['product_id']));
							$incl_tax=($ordered_products[$i]['unit_price']*($tax/100));
							$tax_rate=($incl_tax)/($ordered_products[$i]['unit_price']+$incl_tax)*100;
							$basic_rate=($ordered_products[$i]['unit_price'])-($ordered_products[$i]['unit_price']*($tax_rate/100));
							$tax_amount = $ordered_products[$i]['product_total'] - ($basic_rate*$ordered_products[$i]['product_quantity']);
							$SaleItem_data=[
							'warehouse_id'=>$warehouse_id,
							'product_id'=>$ordered_products[$i]['product_id'],
							'sale_id'=>$sale_id,
							'unit_price'=>$basic_rate,
							'tax'=>$tax,
							'tax_amount'=>$tax_amount,
							'quantity'=>$ordered_products[$i]['product_quantity'],
							'net_value'=>$basic_rate*$ordered_products[$i]['product_quantity'],
							'total'=>$ordered_products[$i]['product_total'],
							];
							$this->SaleItem->create();
							if(!$this->SaleItem->save($SaleItem_data))
							{
								$errors = $this->SaleItem->validationErrors;
								foreach ($errors as $key => $value) {
									throw new Exception($value[0], 1);
								}
							}
						}
					}
					$return['status']='success';
				} catch (Exception $e) {
					$datasource_Sale->rollback();
					$datasource_SaleItem->rollback();
					$return['status']=$e->getMessage(); 
				}
			}
		}
		echo json_encode($return);
		exit;
	}
//receipt   --ubm
	public function api_get_reciept()
	{
		$return=[
		'status'=>'Empty',
		'receipt'=>'Empty',
		];
		$data=$this->request->data;
		if($data)
		{
			$customer_id=$data['customer_id'];
			$Sale=$this->Sale->find('all',array(
				'conditions'=>array(
					'Sale.account_head_id'=>$customer_id,
					'Sale.status'=>2,
					),
				'fields'=>array(
					'Sale.id',
					'Sale.invoice_no',
					'Sale.grand_total',
					'Sale.balance',
					'AccountHead.id',
					'AccountHead.name',
					),
				));
			$All_sale=['customer'=>[],'Sale'=>[],'total_sale'=>'0','total_balance'=>'0'];
			foreach ($Sale as $key => $value) {
				$Single_sale['sale_id']=$value['Sale']['id'];
				$Single_sale['invoice_no']=$value['Sale']['invoice_no'];
				$Single_sale['total']=$value['Sale']['grand_total'];
				$Single_sale['balance']=$value['Sale']['balance'];
				array_push($All_sale['Sale'], $Single_sale);
				$All_sale['customer']=[
				'id'=>$value['AccountHead']['id'],
				'name'=>$value['AccountHead']['name'],
				];
				$All_sale['total_sale']+=$value['Sale']['grand_total'];
				$All_sale['total_balance']+=$value['Sale']['balance'];
			}
			$return['receipt']=$All_sale;
		}
		$return['status']='Success';
		echo json_encode($return);
		exit;
	}
//receipt 
//pending receipt   --ubm
	public function api_get_pending_reciept()
	{
		$return=[
		'status'=>'Empty',
		'receipt'=>'Empty',
		];
		$data=$this->request->data;
		if($data)
		{
			$customer_id=$data['customer_id'];
			$Sale=$this->Sale->find('all',array(
				'conditions'=>array(
					'Sale.account_head_id'=>$customer_id,
					'Sale.status'=>2
					),
				'fields'=>array(
					'Sale.id',
					'Sale.invoice_no',
					'Sale.grand_total',
					'Sale.total',
					'Sale.balance',
					'AccountHead.id',
					'AccountHead.name',
					),
				));
			$All_sale=['customer'=>[],'Sale'=>[],'total_sale'=>'0','total_balance'=>'0'];
			foreach ($Sale as $key => $value) {
				$Single_sale['sale_id']=$value['Sale']['id'];
				$Single_sale['invoice_no']=$value['Sale']['invoice_no'];
				$Single_sale['total']=$value['Sale']['grand_total'];
				$Single_sale['balance']=$value['Sale']['balance'];
				$Journal = $this->Journal->find('list', array(
					'conditions' => array(
						'Journal.remarks' => 'Sale Invoice No :' . $value['Sale']['invoice_no'],
						'Journal.credit' => $value['AccountHead']['id'],
						'Journal.flag=1',
						),
					'fields'=>['Journal.amount']
					));
				$paid_amount = 0;
				foreach ($Journal as $key0 => $amount) {
					$paid_amount+=$amount;
				}
				$balance = $value['Sale']['total'] - $paid_amount;
				$Single_sale['balance']=$balance;
				if($balance>0){
					array_push($All_sale['Sale'], $Single_sale);
				}
				$All_sale['customer']=[
				'id'=>$value['AccountHead']['id'],
				'name'=>$value['AccountHead']['name'],
				];
				$All_sale['total_sale']+=$value['Sale']['total'];
				$All_sale['total_balance']+=$balance;
			}
			$return['receipt']=$All_sale;
		}
		$return['status']='Success';
		echo json_encode($return);
		exit;
	}
//pending receipt 
//receipt payment  --ubm
	public function api_receipt_payment_old()
	{
		$user_id=1;
		$return=[
		'status'=>'Empty',
		];
		$data=$this->request->data;
		if($data)
		{
			$date = date('Y-m-d');
			$executive=$data['executive_id'];
			$day_register_id=$data['day_register_id'];
			$cashhead = $this->Executive->field('Executive.account_head_id',array('id ' => $executive));
			if(empty($cashhead)){
				$cashhead =1;
			}
			$DayRegister=$this->DayRegister->findByExecutiveIdAndStatus($executive,'0');
			if(!$DayRegister){
				$return[ 'status' ]= 'Day Closed';
			}
			else{
				$datesourse_Sale=$this->Sale->getDataSource();
				$datesourse_Journal=$this->Journal->getDataSource();
				try {
					$datesourse_Sale->begin();
					$datesourse_Journal->begin();
					$PaidList = $data['PaidList'];
					foreach ($PaidList as $key => $value) {
						$invoice_no=$value['invoice_no'];
						$receipt_no=$value['receipt_no'];
						$amount=$value['amount'];
						$Sale=$this->Sale->findByInvoiceNo($invoice_no);
						$this->Sale->id=$Sale['Sale']['id'];
						if(!$this->Sale->saveField('balance',$this->Sale->field('balance')-$amount))
							throw new Exception("Error Processing Request in Sale Updation", 1);
						$work_flow='From Mobile';
						$remarks='Sale Invoice No :'.$invoice_no;
						$date=date('Y-m-d');
						$debit=$cashhead;
						$credit=$Sale['Sale']['account_head_id'];
						$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,'',$receipt_no,$day_register_id);
						if($function_return['result']!='Success')
							throw new Exception($function_return['message'], 1);
					}
					$datesourse_Sale->commit();
					$datesourse_Journal->commit();
					$openList = $data['openList'];
					foreach ($openList as $key => $value) {
						$invoice_no='0';
						$amount=$value['amount'];
						$work_flow='From Mobile';
						$remarks='Sale Invoice No :'.$invoice_no;
						$date=date('Y-m-d');
						$debit=$cashhead;
						$credit=$value['customer_id'];
						$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,'',$receipt_no=null,$day_register_id);
						if($function_return['result']!='Success')
							throw new Exception($function_return['message'], 1);
					}
					$return['status']='success';
				} catch (Exception $e) {
					$datesourse_Sale->rollback();
					$datesourse_Journal->rollback();
					$return['status']=$e->getMessage();
				}
			}
		}
		echo json_encode($return);
		exit;
	}
	public function api_receipt_payment()
	{
		$user_id=1;
		$return=[
		'status'=>'Empty',
		];
		$data=$this->request->data;
		if($data)
		{
			$date = date('Y-m-d');
			$executive=$data['executive_id'];
			$day_register_id=$data['day_register_id'];
			$route_id=$this->DayRegister->field(
				'DayRegister.route_id',
				array('DayRegister.id ' => $day_register_id));
			$cashhead = $this->Route->field(
				'Route.account_head_id',
				array('id ' => $route_id));
//$cashhead = $this->Executive->field('Executive.account_head_id',array('id ' => $executive));
			if(empty($cashhead)){
				$cashhead =1;
			}
			$DayRegister=$this->DayRegister->findByExecutiveIdAndStatus($executive,'0');
			if(!$DayRegister){
				$return[ 'status' ]= 'Day Closed';
			}
			else{
				$datesourse_Journal=$this->Journal->getDataSource();
				try {
					$datesourse_Journal->begin();
					$PaidList = $data['PaidList'];
					foreach ($PaidList as $key => $value) {
						$receipt_no=$value['receipt_no'];
						$amount=$value['amount'];
						$work_flow='Cash Receipt From Executive app';
						$remarks="Cash Receipt No :".$receipt_no;
						$voucher_no="";
						$created_at=$value['datetime'];
						$date=date('Y-m-d');
						$debit=$cashhead;
						$credit=$value['customer_id'];
						$Journal =$this->Journal->findByReceiptNoAndAmountAndDate($receipt_no,$amount,$date);
					    if(empty($Journal)){
							$route_id=$this->ExecutiveRouteMapping->field('ExecutiveRouteMapping.route_id',array('ExecutiveRouteMapping.executive_id'=>$executive));
							$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$receipt_no,$day_register_id,$executive,$created_at,$route_id);
							if($function_return['result']!='Success')
								throw new Exception($function_return['message'], 1);
							
						}
						// else{
						// 	$return['status']=$receipt_no.' Receipt No Already Registered';
						// }
						$return['status']='success';
					}
					$datesourse_Journal->commit();

				} catch (Exception $e) {
					$datesourse_Journal->rollback();
					$return['status']=$e->getMessage();
				}
			}
		}
		echo json_encode($return);
		exit;
	}
	public function api_multiple_sale_return()
	{

		$sale_return_account_head_id=4;
		//$tax_on_sale_return_account_head_id=11;
		$tax_on_sale_return_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DUTIES & TAXES'));
		$stock_account_head_id=19;
		$user_id=1;
		$return=[
		'status'=>'empty',
		];

		$executive=$this->request->data['executive_id'];
		$date = date('Y-m-d');
		if(isset($this->request->data['day_register_id']))
		{
			$day_register_id=$this->request->data['day_register_id'];	
		}
		else
		{
			$DayRegister=$this->DayRegister->find('first',[
				'conditions'=>['DayRegister.executive_id'=>$executive],
				'order'=>['DayRegister.id DESC']]);
			$day_register_id=$DayRegister['DayRegister']['id'];
		}
		$route_id=$this->DayRegister->field(
			'DayRegister.route_id',
			array('DayRegister.id ' => $day_register_id));

		$DayRegister=$this->DayRegister->findByExecutiveIdAndStatus($executive,'0');
//$rowcount = $this->ClosingDay->find('count', array('conditions' => array('ClosingDay.executive_id' => $executive,'ClosingDay.date' => $date)));
		if(!$DayRegister){
			$return['status']='Day Closed';
		}
		else{
			$datasource_SalesReturn = $this->SalesReturn->getDataSource();
			$datasource_SalesReturnItem = $this->SalesReturnItem->getDataSource();
			$datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
			$datasource_ExecutiveBonusCalculation = $this->ExecutiveBonusCalculation->getDataSource();
			$datasource_ExecutiveBonus = $this->ExecutiveBonus->getDataSource();
			$datasource_Stock = $this->Stock->getDataSource();
			$datasource_Journal = $this->Journal->getDataSource();
			try {
				$datasource_SalesReturn->begin();
				$datasource_SalesReturnItem->begin();
				$datasource_ProductBonusDetail->begin();
				$datasource_ExecutiveBonusCalculation->begin();
				$datasource_ExecutiveBonus->begin();
				$datasource_Journal->begin();
				$data=$this->request->data['SalesReturn'];
				// $data_return=$this->request->data['SalesReturn'];
				// if(!empty($data_return[0]))
				// 	$data=$data_return;
				// else
				// 	$data=[];
				// 	$data[0]=$data_return;
				// $data=$data_return;
				//pr($data);exit;
				for ($j=0; $j <count($data) ; $j++) {
					$SalesReturn=$this->SalesReturn->find('first',array('order' => 'SalesReturn.id DESC',));
					// if(!empty($SalesReturn))
					// {
					// 	$SalesReturnNo = $SalesReturn['SalesReturn']['invoice_no'];
					// 	$invoice_no=$SalesReturnNo+1;
					// }
					// else
					// {
					// 	$invoice_no=1;
					// }
					$invoice_no=$data[$j]['return_invoice_no'];
					$sales_return_invoice_no=$invoice_no;
					$grand_total=$data[$j]['grand_total'];
					$SaleReturn_exist=$this->SalesReturn->findByInvoiceNoAndGrandTotal($invoice_no,$grand_total);
					if(empty($SaleReturn_exist)){
					$executive_warehouse=$this->Executive->findById($this->request->data['executive_id']);
					$SalesReturn_data=[
					'warehouse_id'=>$executive_warehouse['Executive']['warehouse_id'],
					'account_head_id'=>$data[$j]['customer_id'],
					'day_register_id'=>$day_register_id,
					'executive_id'=>$executive,
					'invoice_no'=>$invoice_no,
					'date'=>date('Y-m-d'),
					'total'=>$data[$j]['grand_total'],
					'discount'=>'0',
					'grand_total'=>$data[$j]['grand_total'],
					'status'=>2,
					'created_by'=>$user_id,
					'modified_by'=>$user_id,
					'created_at'=>date('Y-m-d H:i:s'),
					'updated_at'=>date('Y-m-d H:i:s'),
					];

					$this->SalesReturn->create();
					if(!$this->SalesReturn->save($SalesReturn_data))
					{
						$errors = $this->SalesReturn->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);

						}
					}
					$tax_amount = 0;
					$net_value = 0 ;
					$sales_return_id=$this->SalesReturn->getLastInsertId();
					$ReturnedProduct = $data[$j]['ReturnedProduct'];
					$stock_value=0;$bonus_amount_total=0;
					for ($i=0; $i <count($ReturnedProduct) ; $i++) {
						$unit_id=1;
						$SaleController= new SaleController;
						$UnitLevelConvert=$SaleController->UnitLevelConvert($ReturnedProduct[$i]['product_id'],$unit_id);
				        //pr($UnitLevelConvert);exit;
						$no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
						$product_unit_level=$UnitLevelConvert['sale_unit_level'];
				        //$sale_unit_level=$sales_return_items['sale_unit_level'][$i];
						$tax = $ReturnedProduct[$i]['tax'];
						$taxable_value=$ReturnedProduct[$i]['return_quantity']*$ReturnedProduct[$i]['unit_price'];
						$netvalue=($taxable_value*100)/($tax+100);
                         $taxamount=$netvalue*$tax/100;
						$net_value = $net_value + $netvalue;
						$tax_amount = $tax_amount + $taxamount;

						$return_quantity=$ReturnedProduct[$i]['return_quantity'];
						$bonus_return_amount=$ReturnedProduct[$i]['product_bonus']*$return_quantity;
						$cost = $this->Product->field(
							'Product.cost',
							array('Product.id ' => $ReturnedProduct[$i]['product_id']));
							$executive_rate = $this->ExecutiveRate->field(
							'ExecutiveRate.executive_rate',
							array('ExecutiveRate.executive_id ' =>$executive,'ExecutiveRate.product_id ' => $ReturnedProduct[$i]['product_id']));
							if(empty($executive_rate))
							{
							$executive_rate = $this->Product->field(
							'Product.wholesale_price',
							array('Product.id ' => $ReturnedProduct[$i]['product_id']));
							}
						$stock_value+=$cost*$ReturnedProduct[$i]['return_quantity'];
						$sale_executive_id=0;
						if(empty($data[$j]['invoice_id']))
							$data[$j]['invoice_id']=0;
						$sale_invoice_no=$data[$j]['invoice_id'];
				       if($ReturnedProduct[$i]['product_id']==1)
				       {
				       	$warehouse_id= $executive_warehouse['Executive']['warehouse_id'];
				       }
				       else
				       {
                        $warehouse_id= $this->Warehouse->field('Warehouse.id',array('Warehouse.warehouse_id ' =>$executive_warehouse['Executive']['warehouse_id']));
				        if(empty($warehouse_id))
				         throw new Exception("Damage Warehouse Required", 1);
				       }
						if(!empty($sale_invoice_no))
						{
					$executive=$this->Sale->field('Sale.executive_id',array('Sale.invoice_no'=>$sale_invoice_no));	
						}
						$SalesReturnItem_data=[
						'type'=>"Damage",
						'warehouse_id'=>$warehouse_id,
						'product_id'=>$ReturnedProduct[$i]['product_id'],
						'sales_return_id'=>$sales_return_id,
						'sale_executive_id'=>$executive,
						'invoice_no'=>$sale_invoice_no,
						'invoice_price'=>$ReturnedProduct[$i]['unit_price'],
						'unit_price'=>$ReturnedProduct[$i]['unit_price'],
						'quantity'=>$return_quantity,
						'net_value'=>$netvalue,
						'bonus_return_amount'=>$bonus_return_amount,
						'tax'=>$ReturnedProduct[$i]['tax'],
						'tax_amount'=>$taxamount,
						'total'=>$ReturnedProduct[$i]['refund_Amount'],
						];

						$this->SalesReturnItem->create();
						if(!$this->SalesReturnItem->save($SalesReturnItem_data))
						{
							$errors = $this->SalesReturnItem->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}
						$sales_return_item_id=$this->SalesReturnItem->getLastInsertId();
						$bonus_return_amount=($ReturnedProduct[$i]['unit_price']-$executive_rate)*$return_quantity;
						if($bonus_return_amount>0)
						{
					$bonus_amount_total=$bonus_amount_total+$bonus_return_amount;
				         $ProductBonus_data=[
				          'date'=>date('Y-m-d'),
				         'day_register_id'=>$day_register_id,
                          'executive_id'=>$executive,
				          'product_id'=>$ReturnedProduct[$i]['product_id'],
				          'sales_return_id'=>$sales_return_id,
				          'sales_return_item_id'=>$sales_return_item_id,
				          'unit_price'=>$ReturnedProduct[$i]['unit_price'],
				          'executive_rate'=>$executive_rate,
				          'quantity'=>$return_quantity,
				          'bonus_return_amount'=>$bonus_return_amount,
				          ];
				          $this->ProductBonusDetail->create();
				          if(!$this->ProductBonusDetail->save($ProductBonus_data))
				          {
				            $errors = $this->ProductBonusDetail->validationErrors;
				            foreach ($errors as $key => $value) {
				              throw new Exception($value[0], 1);
				            }
				          }
				      }
				}
				if($bonus_amount_total>0)
                {
                $ProductBonus_data=[
                'day_register_id'=>$day_register_id,
                'executive_id'=>$executive,
                'date'=>date('Y-m-d'),
                'work_flow'=>"SALE RETURN",
                'reference_no'=>$invoice_no,
                'bonus_return_amount'=>$bonus_amount_total,
                ];
                $this->ExecutiveBonusCalculation->create();
                if(!$this->ExecutiveBonusCalculation->save($ProductBonus_data))
                {
                  $errors = $this->ExecutiveBonusCalculation->validationErrors;
                  foreach ($errors as $key => $value) {
                    throw new Exception($value[0], 1);
                  }
                }
              }
					$Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
					if(!empty($Journals))
					{
						$JournalsNo = $Journals['Journal']['voucher_no'];
						$Journals_no=$JournalsNo+1;
					}
					else
					{
						$Journals_no=1;
					}
					$user_id = 1;
					$debit=$sale_return_account_head_id;
					$credit=$data[$j]['customer_id'];
					$amount=$net_value+$tax_amount;
					$discount=0;
					$net_value=round($net_value,2);
					$tax_amount=round($tax_amount,2);
					$work_flow='Sales Return From Executive app';
					$invoice_no= $invoice_no;
					$date=date('Y-m-d');
					$remarks='SalesReturn Invoice No :'.$invoice_no;
					$route_id=$this->ExecutiveRouteMapping->field('ExecutiveRouteMapping.route_id',array('ExecutiveRouteMapping.executive_id'=>$executive));
					$Accountings_function_return=$this->JournalCreate($credit,$debit,$net_value,$date,$remarks,$work_flow,$user_id,$Journals_no,'',$day_register_id,$executive,'',$route_id);
					if($Accountings_function_return['result']!='Success')
						throw new Exception($Stock_function_return['message']);
					if($stock_value)
					{
						$credit=$sale_return_account_head_id;
						$debit=$stock_account_head_id;

						$Accountings_function_return=$this->JournalCreate($credit,$debit,$stock_value,$date,$remarks,$work_flow,$user_id,$Journals_no,'',$day_register_id,$executive,'',$route_id);
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}
					if(floatval($tax_amount))
					{
						$credit=$data[$j]['customer_id'];
						$debit=$tax_on_sale_return_account_head_id;
						$Accountings_function_return=$this->JournalCreate($credit,$debit,$tax_amount,$date,$remarks,$work_flow,$user_id,$Journals_no,'',$day_register_id,$executive,'',$route_id);
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}
					$credit = $this->Route->field(
						'Route.account_head_id',
						array('id ' => $route_id));
					$debit=$data[$j]['customer_id'];
					// if($Accountings_function_return['result']!='Success')
					// 	throw new Exception($Stock_function_return['message']);
					$SalesReturnItem = $this->SalesReturnItem->find('all',array(
						'conditions' => array(
							'SalesReturnItem.sales_return_id' => $sales_return_id,
							),
						'fields' => array(
							'SalesReturnItem.product_id',
							'SalesReturnItem.quantity',
							'SalesReturnItem.unit_price',
							'SalesReturn.invoice_no',
							'SalesReturnItem.warehouse_id',
							)
						));
					$datasource_Stock->begin();
					foreach ($SalesReturnItem as $key => $value) {
						$quantity = $value['SalesReturnItem']['quantity'];
						$unit_price = $value['SalesReturnItem']['unit_price'];
						$product_id=$value['SalesReturnItem']['product_id'];
						$warehouse_id=$value['SalesReturnItem']['warehouse_id'];
						$executive_warehouse=$this->Executive->findById($this->request->data['executive_id']);
						$Stock = $this->Stock->find('first',array(
							'conditions'=>array(
								'Stock.product_id'=>$product_id,
								'Stock.warehouse_id'=>$warehouse_id,
								)
							));
						 if(!$Stock){
							$remark='SalesReturn --'.$value['SalesReturn']['invoice_no'].'('.$quantity.')';
							$flag=1;
							$StockController = new StockController;
							$Stock_function_return=$StockController->GeneralStock_Add_Function($warehouse_id,$product_id,$quantity,date('Y-m-d'),$user_id,$remark,$flag);
							if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result'], 1);
							}else{
							$stock_id=$Stock['Stock']['id'];
							$stock_damaged_quantity=$Stock['Stock']['quantity'];
							$date=date('Y-m-d');
							$StockController = new StockController;
							$remark='SalesReturn --'.$value['SalesReturn']['invoice_no'].'('.$quantity.')';
							$stock_damaged_quantity+=$quantity;
							$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_damaged_quantity,$date,$user_id,$remark);
							if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
							}
					}
					$datasource_Stock->commit();
				}
			}
				$return['status']='Success';

				$datasource_SalesReturn->commit();
				$datasource_SalesReturnItem->commit();
				$datasource_ProductBonusDetail->commit();
				$datasource_ExecutiveBonusCalculation->commit();
				$datasource_ExecutiveBonus->commit();
				$datasource_Journal->commit();
			}
			catch (Exception $e)
			{
				$datasource_SalesReturn->rollback();
				$datasource_SalesReturnItem->rollback();
				$datasource_ProductBonusDetail->rollback();
				$datasource_ExecutiveBonusCalculation->rollback();
				$datasource_ExecutiveBonus->rollback();
				$datasource_Stock->rollback();
				$datasource_Journal->rollback();
				$return['status']=$e->getMessage();
			}
		}

		echo json_encode($return);
		exit;
	}
	public function GeneralStock_Add_Function($warehouse_id,$product_id,$quantity,$date,$user_id,$remark,$flag=1)
	{
		try {
			$date =date('Y-m-d');
			if(!$user_id)
				throw new Exception("Empty user Id", 1);
			if(!$product_id)
				throw new Exception("Empty product Id", 1);
			if(!$date)
				throw new Exception("Empty date", 1);
			if(!$quantity && $quantity!=0)
				throw new Exception("Empty quantity Id", 1);
			if(!$remark)
				throw new Exception("Empty remark", 1);
			$stock_data=[
			'product_id'=>$product_id,
			'warehouse_id'=>$warehouse_id,
			'quantity'=>$quantity,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'flag'=>$flag,
			'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
			'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
			];
			$this->Stock->create();
			if(!$this->Stock->save($stock_data))
			{
				$errors = $this->Stock->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$Stock=$this->Stock->read();
			unset($Stock['Stock']['id']);
			unset($Stock['Stock']['flag']);
			unset($Stock['Stock']['modified_by']);
			$Stock['Stock']['remark']=$remark;
			$Stock['Stock']['quantity_in']=$quantity;
			$work_flow='Product Creation';
			$function_stockLog_return=$this->stock_logs($Stock['Stock'],$work_flow);
			if($function_stockLog_return['result']!='Success')
				throw new Exception($function_stockLog_return['result'], 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
//receipt payment
//sale return
	public function api_sale_return()
	{
		$sale_return_account_head_id=4;
		//$tax_on_sale_return_account_head_id=11;
		$tax_on_sale_return_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DUTIES & TAXES'));
		$stock_account_head_id=19;
		$user_id=1;
		$return=[
		'status'=>'empty',
		];
		$SalesReturn=$this->SalesReturn->find('first',array('order' => 'SalesReturn.id DESC',));
		
//$SalesReturn=$this->SalesReturn->find('count');
		if(!empty($SalesReturn))
		{
			$SalesReturnNo = $SalesReturn['SalesReturn']['invoice_no'];
			$invoice_no=$SalesReturnNo+1;
		}
		else
		{
			$invoice_no=1;
		}
		$executive=$this->request->data['executive_id'];
		$date = date('Y-m-d');
		if(isset($this->request->data['day_register_id']))
		{
			$day_register_id=$this->request->data['day_register_id'];	
		}
		else
		{
			$DayRegister=$this->DayRegister->find('first',[
				'conditions'=>['DayRegister.executive_id'=>$executive],
				'order'=>['DayRegister.id DESC']]);
			$day_register_id=$DayRegister['DayRegister']['id'];
		}
		$route_id=$this->DayRegister->field(
			'DayRegister.route_id',
			array('DayRegister.id ' => $day_register_id));

		$DayRegister=$this->DayRegister->findByExecutiveIdAndStatus($executive,'0');
		if(!$DayRegister){
			$return['status']='Day Closed';
		}
		else{
			$datasource_SalesReturn = $this->SalesReturn->getDataSource();
			$datasource_SalesReturnItem = $this->SalesReturnItem->getDataSource();
			$datasource_Product = $this->Product->getDataSource();
			$datasource_Stock = $this->Stock->getDataSource();
			$datasource_Journal = $this->Journal->getDataSource();
			$datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
			$datasource_ExecutiveBonusDetail = $this->ExecutiveBonusDetail->getDataSource();
			$datasource_ExecutiveBonus = $this->ExecutiveBonus->getDataSource();
			try {
				$datasource_SalesReturn->begin();
				$datasource_SalesReturnItem->begin();
				$datasource_Journal->begin();
				$datasource_ProductBonusDetail->begin();
				$datasource_ExecutiveBonusDetail->begin();
				$datasource_ExecutiveBonus->begin();
				$data=$this->request->data;
				$data=$data['SalesReturn'];
				$SalesReturn_data=[
				'account_head_id'=>$data['customer_id'],
				'invoice_no'=>$invoice_no,
				'date'=>date('Y-m-d'),
				'total'=>$data['total'],
				'discount'=>'0',
				'grand_total'=>$data['grand_total'],
				'status'=>2,
				'created_by'=>$user_id,
				'modified_by'=>$user_id,
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				$this->SalesReturn->create();
				if(!$this->SalesReturn->save($SalesReturn_data))
				{
					$errors = $this->SalesReturn->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				if(empty($data['invoice_id'])){
					$data['invoice_id'] = 0;
				}
				$tax_amount = 0;
				$net_value = 0 ;
				$stock_value=0;
				$sales_return_id=$this->SalesReturn->getLastInsertId();
				$ReturnedProduct = $data['ReturnedProduct'];
				for ($i=0; $i <count($ReturnedProduct) ; $i++) {
					$cost = $this->Product->field(
						'Product.cost',
						array('Product.id ' => $ReturnedProduct[$i]['product_id']));
					$tax = $ReturnedProduct[$i]['tax'];
					$incl_tax=($ReturnedProduct[$i]['unit_price']*($tax/100));
					$tax_rate=($incl_tax)/($ReturnedProduct[$i]['unit_price']+$incl_tax)*100;
//$basic_rate=($ReturnedProduct[$i]['unit_price'])-($ReturnedProduct[$i]['unit_price']*($tax_rate/100));
					$basic_rate=($ReturnedProduct[$i]['unit_price']);
					$taxamount=($ReturnedProduct[$i]['return_quantity']*$basic_rate)*($tax/100);
					$ReturnedProduct[$i]['net_value'] = $ReturnedProduct[$i]['return_quantity']*$ReturnedProduct[$i]['unit_price'];
					$tax_amount = $tax_amount + $taxamount;
					$net_value = $net_value + $ReturnedProduct[$i]['net_value'];
					$stock_value+=$cost*$ReturnedProduct[$i]['return_quantity'];
					$SalesReturnItem_data=[
					'product_id'=>$ReturnedProduct[$i]['product_id'],
					'sales_return_id'=>$sales_return_id,
					'invoice_no'=>$data['invoice_id'],
					'invoice_price'=>$ReturnedProduct[$i]['unit_price'],
					'unit_price'=>$ReturnedProduct[$i]['unit_price'],
					'quantity'=>$ReturnedProduct[$i]['return_quantity'],
					'net_value'=>$ReturnedProduct[$i]['net_value'],
					'tax'=>$ReturnedProduct[$i]['tax'],
					'tax_amount'=>$taxamount,
					'total'=>$ReturnedProduct[$i]['refund_Amount'],
					];
					$this->SalesReturnItem->create();
					if(!$this->SalesReturnItem->save($SalesReturnItem_data))
					{
						$errors = $this->SalesReturnItem->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					$SaleController= new SaleController;
					$UnitLevelConvert=$SaleController->UnitLevelConvert($ReturnedProduct[$i]['product_id'],1);
					$sales_return_item_id=$this->SalesReturnItem->getLastInsertId();
					$product_bonus=0;
					if(!empty($UnitLevelConvert['product_bonus']))
						$product_bonus=$UnitLevelConvert['product_bonus'];
					$bonus_amount=number_format(($ReturnedProduct[$i]['net_value']*$product_bonus/100),3,'.','');
					$sale_id=$this->Sale->field('Sale.id',array('Sale.invoice_no'=>$data['invoice_id']));
					$ProductBonusDetail=$this->ProductBonusDetail->find('first',array(
						'joins'=>array(
							array(
								'table'=>'sale_items',
								'alias'=>'SaleItem',
								'type'=>'inner',
								'conditions'=>array('SaleItem.id=ProductBonusDetail.sale_item_id')
								),
							),
						'conditions'=>array(
							'ProductBonusDetail.sale_id'=>$sale_id,
							'SaleItem.product_id'=>$ReturnedProduct[$i]['product_id'],
							),
						));


					if(!empty($ProductBonusDetail))
					{
						$single_item_bonus_amount=number_format(($ProductBonusDetail['ProductBonusDetail']['bonus_amount']/$ProductBonusDetail['ProductBonusDetail']['quantity']),3,'.','');
						$bonus_return_amount=$single_item_bonus_amount*$ReturnedProduct[$i]['return_quantity'];
						$product_bonus=$ProductBonusDetail['ProductBonusDetail']['bonus_percentage'];
						$net=$ReturnedProduct[$i]['unit_price']*$ReturnedProduct[$i]['return_quantity'];
						$ProductBonus_data=[
						'product_id'=>$ReturnedProduct[$i]['product_id'],
						'sales_return_id'=>$sales_return_id,
						'sales_return_item_id'=>$sales_return_item_id,
						'unit_price'=>$ReturnedProduct[$i]['unit_price'],
						'cost'=>$UnitLevelConvert['product_cost'],
						'quantity'=>$ReturnedProduct[$i]['return_quantity'],
						'net_value'=>$net,
						'bonus_percentage'=>$product_bonus,
						'bonus_return_amount'=>$bonus_return_amount,
						];
						$this->ProductBonusDetail->create();
						if(!$this->ProductBonusDetail->save($ProductBonus_data))
						{
							$errors = $this->ProductBonusDetail->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}
					}
				}
				$Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
				if(!empty($Journals))
				{
					$JournalsNo = $Journals['Journal']['voucher_no'];
					$Journals_no=$JournalsNo+1;
				}
				else
				{
					$Journals_no=1;
				}
				$user_id = 1;
				$debit=$sale_return_account_head_id;
				$credit=$data['customer_id'];
				$amount=$net_value+$tax_amount;
				$discount=0;
				$work_flow='Sales Return From Executive app';
				$invoice_no= $invoice_no;
				$date=date('Y-m-d');
				$remarks='SalesReturn Invoice No :'.$invoice_no;
				// $route_id=$this->ExecutiveRouteMapping->field('ExecutiveRouteMapping.route_id',array('ExecutiveRouteMapping.executive_id'=>$executive));
				$Accountings_function_return=$this->JournalCreate($credit,$debit,$net_value,$date,$remarks,$work_flow,$user_id,$Journals_no,'','',$executive,'',$route_id);
				if($Accountings_function_return['result']!='Success')
					throw new Exception($Accountings_function_return['message']);
				if($stock_value)
				{
					$credit=$sale_return_account_head_id;
					$debit=$stock_account_head_id;

					$Accountings_function_return=$this->JournalCreate($credit,$debit,$stock_value,$date,$remarks,$work_flow,$user_id,$Journals_no,'','',$executive,'',$route_id);
					if($Accountings_function_return['result']!='Success')
						throw new Exception($Accountings_function_return['message']);
				}
				if(floatval($tax_amount))
				{
					$credit=$data['customer_id'];
					$debit=$tax_on_sale_return_account_head_id;
					$Accountings_function_return=$this->JournalCreate($credit,$debit,$tax_amount,$date,$remarks,$work_flow,$user_id,$Journals_no,'','',$executive,'',$route_id);
					if($Accountings_function_return['result']!='Success')
						throw new Exception($Accountings_function_return['message']);
				}
				$credit = $this->Route->field(
					'Route.account_head_id',
					array('id ' => $route_id));
				$debit=$data['customer_id'];
				
				$SalesReturnItem = $this->SalesReturnItem->find('all',array(
					'conditions' => array(
						'SalesReturnItem.sales_return_id' => $sales_return_id,
						),
					'fields' => array(
						'SalesReturnItem.product_id',
						'SalesReturnItem.quantity',
						'SalesReturnItem.unit_price',
						'SalesReturn.invoice_no',
						)
					));
				$datasource_Stock->begin();
				foreach ($SalesReturnItem as $key => $value) {
					$quantity = $value['SalesReturnItem']['quantity'];
					$unit_price = $value['SalesReturnItem']['unit_price'];
					$product_id=$value['SalesReturnItem']['product_id'];
					$executive_warehouse=$this->Executive->findById($this->request->data['executive_id']);
					$Stock = $this->Stock->find('first',array(
						'conditions'=>array(
							'product_id'=>$product_id,
							'warehouse_id'=>$executive_warehouse['Executive']['warehouse_id'],
							)
						));
					if($Stock){
						$stock_id=$Stock['Stock']['id'];
						$stock_quantity=$Stock['Stock']['quantity'];
						$date=date('Y-m-d');
						$remark='SalesReturn --'.$value['SalesReturn']['invoice_no'].'('.$quantity.')';
						$quantity+=$stock_quantity;
						$Stock_function_return=$this->GeneralStock_Edit_Function($stock_id,$quantity,$date,$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
					}
				}
				$return['status']='Success';
				$datasource_Stock->commit();
				$datasource_SalesReturn->commit();
				$datasource_SalesReturnItem->commit();
				$datasource_ProductBonusDetail->commit();
				$datasource_ExecutiveBonusDetail->commit();
				$datasource_ExecutiveBonus->commit();
				$datasource_Journal->commit();
			}
			catch (Exception $e)
			{
				$datasource_SalesReturn->rollback();
				$datasource_SalesReturnItem->rollback();
				$datasource_Stock->rollback();
				$datasource_Journal->rollback();
				$datasource_ProductBonusDetail->rollback();
				$datasource_ExecutiveBonusDetail->rollback();
				$datasource_ExecutiveBonus->rollback();
				$return['status']=$e->getMessage();
			}
		}
		echo json_encode($return);
		exit;
	}
//sale return
//customer search functionality
	public function api_get_route_customer_search()
	{
		$return=[
		'status'=>'Empty',
		'Customers'=>[],
		];
		$data=$this->request->data;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
			$route_id=$data['route_id'];
			$warehouse_id = $this->Executive->field(
				'Executive.warehouse_id',
				array('Executive.id ' => $executive));
		}
		else
		{
			$return[ 'status' ]= 'Executive Required';
		}
		if(isset($executive))
		{
			$CustomerList=$this->AccountHead->find('all',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('Customer.account_head_id=AccountHead.id'),
						),
					array(
						"table"=>'divisions',
						"alias"=>'Division',
						"type"=>'inner',
						"conditions"=>array('Customer.division_id=Division.id'),
						),
					array(
						"table"=>'customer_types',
						"alias"=>'CustomerType',
						"type"=>'inner',
						"conditions"=>array('Customer.customer_type_id=CustomerType.id'),
						),
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
						),
					 array(
						"table"=>'states',
						"alias"=>'State',
						"type"=>'inner',
						"conditions"=>array('Customer.state_id=State.id'),
						),
					),
				'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,'ExecutiveRouteMapping.route_id !='=>$route_id),
				'order'=>array('Customer.priority Asc'),
				'fields'=>array(
					'Customer.*',
					'AccountHead.id',
					'AccountHead.name',
					'AccountHead.opening_balance',
					'Division.name',
					'State.name',
					'CustomerType.name'
					),
				));
			if($CustomerList)
			{
				$return[ 'status' ]='success';
				$All_Customer=[];
				foreach ($CustomerList as $key => $value) {
			          $CustomerPrice=$this->CustomerPrice->find('all',array(
				'conditions'=>array('CustomerPrice.customer_id'=>$value['AccountHead']['id']),
				'fields'=>array(
					'CustomerPrice.product_id',
					'CustomerPrice.product_wholesale_price',
					'CustomerPrice.customer_id',
					),
				));
				   $Single_Customer['stock']=$CustomerPrice;
				   $Single_Customer['route_code']="";
				   if($value['Customer']['route_id'])
				   {
				   	$Single_Customer['route_code'] = $this->Route->field(
						'Route.code',
						array('Route.id ' => $value['Customer']['route_id']));
				   }				
					$Single_Customer['id']=$value['AccountHead']['id'];
					$Single_Customer['name']=$value['AccountHead']['name'];
					$Single_Customer['Division']=$value['Division']['name'];
					$Single_Customer['CustomerType']=$value['CustomerType']['name'];
				     $Single_Customer['State']=$value['State']['name'];
					$Single_Customer['opening_balance']=$value['AccountHead']['opening_balance'];
					$date = date('Y-m-d');
					$customer_account_info = $this->General_Journal_Openging_Debit_N_Credit_function($value['AccountHead']['id'],$date);
					$Single_Customer['debit']=$customer_account_info['debit']+$value['AccountHead']['opening_balance'];
					$Single_Customer['credit']=$customer_account_info['credit'];
					$balance = $customer_account_info['debit'] - $customer_account_info['credit']+$customer_account_info['opening_balance'];
					$Single_Customer['balance'] = $balance;
					$Single_Customer['customer_product_list']=array();
					$Single_Customer['email']=$value['Customer']['email'];
					$Single_Customer['shope_code']=$value['Customer']['code'];
					$Single_Customer['credit_limit']=$value['Customer']['credit_limit'];
					$Single_Customer['address']=$value['Customer']['place'];
					$Single_Customer['latitude']="";$Single_Customer['longitude']="";$Single_Customer['arabic_name']="";
					if($value['Customer']['latitude']){
						$Single_Customer['latitude']=$value['Customer']['latitude'];
					}
					if($value['Customer']['longitude']){
						$Single_Customer['longitude']=$value['Customer']['longitude'];
					}
					if($value['Customer']['arabic_name']){
						$Single_Customer['arabic_name']=$value['Customer']['arabic_name'];
					}
					$Single_Customer['mobile']=$value['Customer']
					['mobile'];
					$Single_Customer['vat_no']=$value['Customer']
					['vat_no'];
//
					array_push($All_Customer, $Single_Customer);
				}
				$return[ 'Customers' ]=$All_Customer;
			}
		}
		echo json_encode($return);
		exit;
	}
// customer list for executive routes --ubm
	public function api_get_route_customer_list()
	{
		$return=[
		'status'=>'Empty',
		'Customers'=>[],
		];
		$data=$this->request->data;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
		}
		else{
			$return[ 'status' ]= 'Executive Requered';
		}
		if(isset($executive))
		{
			$CustomerList=$this->AccountHead->find('list',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('Customer.account_head_id=AccountHead.id'),
						),
					
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
						),
					),
				'conditions'=>array('ExecutiveRouteMapping.executive_id'=>$executive,),
				'fields'=>array(
					'AccountHead.id',
					'AccountHead.name',
					),
				));
			if($CustomerList)
			{
				$return[ 'status' ]='success';
				$All_Customer=[];
				foreach ($CustomerList as $key => $value) {
					$Single_Customer['id']=$key;
					$Single_Customer['name']=$value;
					array_push($All_Customer, $Single_Customer);
				}
				$return[ 'Customers' ]=$All_Customer;
			}
		}
		echo json_encode($return);
		exit;
	}
// route list for executive routes --ubm
// van stock for executives --ubm
	public function api_get_van_stock()
	{
		$return=[
		'status'=>'Empty',
		'Stocks'=>[],
		'total_stock'=>'0',
		'total_quantity'=>'0',
		];
		$data=$this->request->data;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
			$warehouse_id = $this->Executive->field(
				'Executive.warehouse_id',
				array('Executive.id ' => $executive));
		}
		else{
			$return[ 'status' ]= 'Executive Required';
		}
		if(isset($executive))
		{
		$this->Product->unbindModel(array('hasMany' => array('SalesReturnItem','SaleItem','PurchasedItem','PurchaseReturnItem','UnwantedList','Stock','StockLog')));
			$Stock=$this->Product->find('all',array(
				"joins"=>array(
					array(
						"table"=>'stocks',
						"alias"=>'Stock',
						"type"=>'inner',
						"conditions"=>array('Product.id=Stock.product_id'),
						),
					array(
						"table"=>'warehouses',
						"alias"=>'Warehouse',
						"type"=>'inner',
						"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
						),
					),
				'conditions'=>array('Product.active=1','type ='=>[2,5,8],'Stock.flag'=>1,'Stock.warehouse_id'=>$warehouse_id),
			//	'limit'=>500,
				'fields'=>array(
					'Product.*',
					'Unit.name',
					'ProductType.name',
					'Warehouse.name',
					'Brand.name',
					'Stock.*',
					)
				));
			$All_Stock=[];
			$total_cost = 0;
			$total_quantity = 0;
			foreach($Stock as $value){
				if($value['Stock']['quantity']>0){
					$Single_stock['product_id']=$value['Product']['id'];
					$Single_stock['product_name']=$value['Product']['name'];
					$Single_stock['hsn_code']=$value['Product']['hsn_code'];
					$Single_stock['product_code']=$value['Product']['code'];
					$Single_stock['priority']=$value['Product']['priority'];
					$Single_stock['product_type_name']=$value['ProductType']['name'];
					$Single_stock['brand_name']=$value['Brand']['name'];
					$Single_stock['product_cost']=$value['Product']['cost'];
					$Single_stock['product_mrp']=$value['Product']['mrp'];
				    $Single_stock['type']=$value['Product']['type'];
				    $Single_stock['tray_capacity']=$value['Product']['tray_occupation'];
					$Single_stock['product_wholesale_price']=$value['Product']['wholesale_price'];
					$Single_stock['product_vat']=$value['Product']['tax'];
					$Single_stock['piece_per_cart']=$value['Product']['no_of_piece_per_unit'];
					$Single_stock['stock_quantity']=$value['Stock']['quantity'];
					$total_cost+=$value['Product']['cost']*$value['Stock']['quantity'];
					$total_quantity+=$value['Stock']['quantity'];
					array_push($All_Stock, $Single_stock);
				}
			}
			if($Stock)
			{
				$return[ 'status' ]='success';
				$return[ 'Stocks' ]=$All_Stock;
				$return[ 'total_stock' ]=$total_cost;
				$return[ 'total_quantity' ]=$total_quantity;
			}
		}
		echo json_encode($return);
		exit;
	}
// van stock for executives --ubm
// Current Sale Target for executives --ubm
	public function api_get_sale_target()
	{
		$first_date = date('Y-m-d',strtotime('first day of this month'));
		$return=[
		'status'=>'Empty',
		'SaleTargets'=>[],
		];
		$data=$this->request->data;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
		}
		else{
			$return[ 'status' ]= 'Executive Requered';
		}
		if(isset($executive))
		{
			$target = $this->sale_target($executive,$first_date);
			if($target)
			{
				$return[ 'status' ]='success';
				$return[ 'SaleTargets' ]=$target;
			}
		}
		echo json_encode($return);
		exit;
	}
// Current Sale Target for executives --ubm
// get product details of product_name/product_code
	public function api_get_product_details()
	{
		$data=$this->request->data;
		$product = $data['product'];
		$Product=$this->Product->find('first',array(
			'conditions'=>array(
				'OR' => array(
					array('Product.code' => $product),
					array('Product.name LIKE' => $product),
// array('Product.name LIKE' => "%".$product."%"),
					)
				)
			));
		if(!empty($Product))
		{
			$return['status']='Success';
			$return['Product']=$Product['Product'];
		}
		else
		{
			$return['status']='Empty';
		}
		echo json_encode($return);
		exit;
	}
// get product details of product_name/product_code
// Customer credit limit and balance amount --ubm
	public function api_get_credit_customer()
	{
		$first_date = date('Y-m-d',strtotime('first day of this month'));
		$return=[
		'status'=>'Empty',
		'Customer'=>[],
		];
		$data=$this->request->data;
		if(isset($data['customer_id']))
		{
			$account_head_id=$data['customer_id'];
			$credit_limit = $this->Customer->field(
				'Customer.credit_limit',
				array('Customer.account_head_id ' => $account_head_id));
			$opening_balance = $this->AccountHead->field(
				'AccountHead.opening_balance',
				array('AccountHead.id ' => $account_head_id));
			$customer['credit_limit'] = $credit_limit;
		}
		else{
			$return[ 'status' ]= 'Customer Required';
		}
		if(isset($account_head_id))
		{
			$total = $opening_balance;
			$Recieved='0';
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
			$total+=$Debit_N_Credit_function['debit'];
			$Recieved+=$Debit_N_Credit_function['credit'];
			$customer['balance'] = $total - $Recieved;
			if($customer)
			{
				$return[ 'status' ]='success';
				$return[ 'Customer' ]=$customer;
			}
		}
		echo json_encode($return);
		exit;
	}
// Customer credit limit and balance amount
//invoice  detailes
	public function api_get_invoice_details()
	{
		$return=[
		'status'=>'Empty',
		'Sale'=>[],
		];
		$data=$this->request->data;
		if(isset($data['invoice_no']))
		{
			$invoice_no=$data['invoice_no'];
			$return['status']='success';
			$Sale=$this->Sale->find('first',array('conditions'=>array(
				'invoice_no'=>$invoice_no,
				),));
			if(!empty($Sale)){
				$return['Sale']['invoice_no']= $Sale['Sale']['invoice_no'];
				$return['Sale']['grand_total']= $Sale['Sale']['grand_total'];
				$return['Sale']['discount_amount']= $Sale['Sale']['discount_amount'];
				$return['Sale']['sale_type']= $Sale['Sale']['sale_type'];
				$return['Sale']['date']= $Sale['Sale']['date_of_delivered'];
				$return['Sale']['customer_name']= $Sale['AccountHead']['name'];
				$SaleItems=$this->Sale->find('all',array(
					"joins"=>array(
						array(
							"table"=>'sale_items',
							"alias"=>'SaleItem',
							"type"=>'inner',
							"conditions"=>array('Sale.id=SaleItem.sale_id'),
							),
						array(
							"table"=>'products',
							"alias"=>'Product',
							"type"=>'inner',
							"conditions"=>array('Product.id=SaleItem.product_id'),
							),
						),
					'conditions'=>array(
						'invoice_no'=>$invoice_no,
						),
					'fields'=>array(
						'SaleItem.unit_price',
						'SaleItem.quantity',
						'SaleItem.total',
// 'Product.piecepercart',
						'Product.code',
						'Product.id',
						'Product.name',
						'SaleItem.tax',
						'SaleItem.tax_amount',
						),
					));
			}
			else{
				$return['status'] ="This invoice doesn't exist";
			}
		}
		if(isset($SaleItems))
		{
			$All_Sale=[];
			foreach ($SaleItems as $key => $value) {
				$Single_Sale['unit_price']=$value['SaleItem']['unit_price'];
				$Single_Sale['quantity']=$value['SaleItem']['quantity'];
				$Single_Sale['total']=$value['SaleItem']['total'];
				$Single_Sale['name']=$value['Product']['name'];
				$Single_Sale['code']=$value['Product']['code'];
				$Single_Sale['tax']=$value['SaleItem']['tax'];
				$Single_Sale['tax_amount']=$value['SaleItem']['tax_amount'];
				$Single_Sale['id']=$value['Product']['id'];
				array_push($All_Sale, $Single_Sale);
			}
			$return['Sale']['SaleItem']=$All_Sale;	
		}
		echo json_encode($return);
		exit;
	}
//invoice  detailes
//invoice item detailes
	public function api_get_invoice_item_details()
	{

		$return=[
		'status'=>'Empty',
		'Sale'=>[],
		];
		$data=$this->request->data;
		if(isset($data['invoice_no']))
		{
			$invoice_no=$data['invoice_no'];
			$return['status']='success';
			$Sale=$this->Sale->find('all',array(
				"joins"=>array(
					array(
						"table"=>'sale_items',
						"alias"=>'SaleItem',
						"type"=>'inner',
						"conditions"=>array('Sale.id=SaleItem.sale_id'),
						),
					array(
						"table"=>'products',
						"alias"=>'Product',
						"type"=>'inner',
						"conditions"=>array('Product.id=SaleItem.product_id'),
						),
					),
				'conditions'=>array(
					'invoice_no'=>$invoice_no,
					),
				'fields'=>array(
					'Sale.is_erp',
					'SaleItem.unit_price',
					'SaleItem.quantity',
					'SaleItem.total',
					'SaleItem.quantity_mode',
					'SaleItem.tax',
					'SaleItem.tax_amount',
// 'Product.piecepercart',
					'Product.code',
					'Product.no_of_piece_per_unit',
					'Product.id',
					'Product.name',
					'Product.cost',
					),
				));
		}
		if(isset($Sale))
		{
			$All_Sale=[];
			foreach ($Sale as $key => $value) {
				if($value['SaleItem']['quantity_mode']=="Cases")
				{
					if($value['Sale']['is_erp'] == 0){
						$Single_Sale['unit_price']=$value['SaleItem']['unit_price']*$value['Product']['no_of_piece_per_unit'];
					}else{
						$Single_Sale['unit_price']=$value['SaleItem']['unit_price']/$value['Product']['no_of_piece_per_unit'];
					}
				}
				else
				{
					$Single_Sale['unit_price']=$value['SaleItem']['unit_price'];
				}
				$Single_Sale['quantity']=$value['SaleItem']['quantity'];
				$SalesReturnItem=$this->SalesReturnItem->find('all',array(
					'conditions'=>array(
						'SalesReturnItem.invoice_no'=>$invoice_no,
						'SalesReturnItem.product_id'=>$value['Product']['id'],
						),
					'fields'=>array(
						'SalesReturnItem.quantity',
						)
					));
				if($SalesReturnItem)
				{
					foreach ($SalesReturnItem as $key => $value1) {
						$Single_Sale['quantity']-=$value1['SalesReturnItem']['quantity'];
					}
				}
				$conditions['SaleItem.sale_id']=$value['Sale']['id'];
				$joins=[
				array(
					'table'=>'sale_items',
					'alias'=>'SaleItem',
					'type'=>'INNER',
					'conditions'=>array('SaleItem.product_id=Product.id')
					),
				array(
					'table'=>'product_bonus_details',
					'alias'=>'ProductBonusDetail',
					'type'=>'INNER',
					'conditions'=>array('ProductBonusDetail.sale_item_id=SaleItem.id')
					)
				];
				$fields=[
				'SaleItem.quantity_mode',
				'SaleItem.unit_price',
				'SaleItem.tax',
				'SaleItem.quantity',
				'SaleItem.free_qty',
				'ProductBonusDetail.quantity',
				'ProductBonusDetail.bonus_amount',
				'Product.mrp',
				'Product.no_of_piece_per_unit',
				'Product.tax',
				];
				$conditions['Product.id']=$value['Product']['id'];
				$this->Product->unbindModel(array('hasMany' => array('SalesReturnItem','SaleItem','PurchasedItem','PurchaseReturnItem','UnwantedList','Stock','StockLog')));
				$Product=$this->Product->find('first',array(
					'joins'=>$joins,
					'fields'=>$fields,
					'conditions'=>$conditions,
					));
				$Single_Sale['single_item_bonus_amount']=0;
				if(!empty($Product['ProductBonusDetail']['bonus_amount']))
					$Single_Sale['single_item_bonus_amount']=number_format(($Product['ProductBonusDetail']['bonus_amount']/$Product['ProductBonusDetail']['quantity']),3,'.','');
				$Single_Sale['total']=$value['SaleItem']['total'];
				$Single_Sale['name']=$value['Product']['name'];
				$Single_Sale['code']=$value['Product']['code'];
				$Single_Sale['cost']=$value['Product']['cost'];
				$Single_Sale['tax']=$value['SaleItem']['tax'];
				$Single_Sale['tax_amount']=$value['SaleItem']['tax_amount'];
// $Single_Sale['piecepercart']=$value['Product']['piecepercart'];
				$Single_Sale['id']=$value['Product']['id'];
				array_push($All_Sale, $Single_Sale);
			}
			$return['Sale']=$All_Sale;	
		}
		echo json_encode($return);
		exit;

	}
//invoice item detailes
// Customer balance amount --ubm
	public function api_get_customer_balance()
	{
		$first_date = date('Y-m-d',strtotime('first day of this month'));
		$return=[
		'status'=>'Empty',
		'Customer'=>[],
		];
		$data=$this->request->data;
		if(isset($data['customer_id']))
		{
			$account_head_id=$data['customer_id'];
			$opening_balance = $this->AccountHead->field(
				'AccountHead.opening_balance',
				array('AccountHead.id ' => $account_head_id));
		}
		else{
			$return[ 'status' ]= 'Customer Requered';
		}
		if(isset($account_head_id))
		{
			$total = $opening_balance;
			$Recieved='0';
			$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($account_head_id);
			$total+=$Debit_N_Credit_function['debit'];
			$Recieved+=$Debit_N_Credit_function['credit'];
			$customer['balance'] = $total - $Recieved;
			if($customer)
			{
				$return[ 'status' ]='success';
				$return[ 'Customer' ]=$customer;
			}
		}
		echo json_encode($return);
		exit;
	}
// Customer balance amount
// Division/Type lists --ubm
	public function api_get_division_type_list()
	{
		$return=[
		'status'=>'Empty',
		'Divisions'=>[],
		'Types'=>[],
		];
		{
			$Divisions=$this->Division->find('list',array(
				'fields'=>
				'Division.name',
				'Division.id',
				));
			if($Divisions)
			{
				$return[ 'status' ]='success';
				$All_Division=[];
				foreach ($Divisions as $key => $value) {
					$Single_Division['id']=$key;
					$Single_Division['name']=$value;
					array_push($All_Division, $Single_Division);
				}
				$return[ 'Divisions' ]=$All_Division;
			}
			$CustomerTypes=$this->CustomerType->find('list',array(
				'fields'=>
				'CustomerType.name',
				'CustomerType.id',
				));
			if($CustomerTypes)
			{
				unset($CustomerTypes[1]);
				$return[ 'status' ]='success';
				$All_Type=[];
				foreach ($CustomerTypes as $key => $value) {
					$Single_Type['id']=$key;
					$Single_Type['name']=$value;
					array_push($All_Type, $Single_Type);
				}
				$return[ 'Types' ]=$All_Type;
			}
		}
		echo json_encode($return);
		exit;
	}
// Division/Customer lists --ubm
// Division lists --ubm
	public function api_get_division_list()
	{
		$return=[
		'status'=>'Empty',
		'Divisions'=>[],
		];
		{
			$Divisions=$this->Division->find('list',array(
				'fields'=>
				'Division.name',
				'Division.id',
				));
			if($Divisions)
			{
				$return[ 'status' ]='success';
				$All_Division=[];
				foreach ($Divisions as $key => $value) {
					$Single_Division['id']=$key;
					$Single_Division['name']=$value;
					array_push($All_Division, $Single_Division);
				}
				$return[ 'Divisions' ]=$All_Division;
			}
		}
		echo json_encode($return);
		exit;
	}
// Division lists --ubm
// Customer Type lists --ubm
	public function api_get_customer_type_list()
	{
		$return=[
		'status'=>'Empty',
		'Types'=>[],
		];
		{
			$CustomerTypes=$this->CustomerType->find('list',array(
				'fields'=>
				'CustomerType.name',
				'CustomerType.id',
				));
			if($CustomerTypes)
			{
				unset($CustomerTypes[1]);
				$return[ 'status' ]='success';
				$All_Type=[];
				foreach ($CustomerTypes as $key => $value) {
					$Single_Type['id']=$key;
					$Single_Type['name']=$value;
					array_push($All_Type, $Single_Type);
				}
				$return[ 'Types' ]=$All_Type;
			}
		}
		echo json_encode($return);
		exit;
	}
// Customer Type lists --ubm
//Customer creation  --ubm
	public function api_customer_creation()
	{
		$user_id=1;
		$return=[
		'status'=>'Empty',
		];
		$main_group_id = 1;
		$sub_group_id = $this->SystemParameter->field('value',array('id'=>11));
		$data=$this->request->data['Customer'];
		$executive=$this->request->data['executive_id'];
		if($data)
		{
			$datasource_Customer = $this->Customer->getDataSource();
			$datasource_AccountHead = $this->AccountHead->getDataSource();
			try {
				$datasource_Customer->begin();
				$datasource_AccountHead->begin();
        $opening_balance = $data['opening_balance'];
				//$data['division_id'] = 1;
				$division_id = $data['division_id'];
				$warehouse_id = $this->Executive->field(
				'Executive.warehouse_id',
				array('Executive.id ' => $executive));
				if ($division_id == '')
					throw new Exception("Empty Division", 1);
				if ($opening_balance == '')
					$opening_balance =0;
				$CustomerRow = $this->Customer->find('first', array(
//'conditions'=>array('Customer.customer_type_id'=>$data['customer_type_id']),
					'order' => array('Customer.id' => 'DESC') ));
				$code = 1001;
				if(!empty($CustomerRow))
				{
					$code = $CustomerRow['Customer']['code']+1;
				}
				$name = trim($data['name']);
				if (!$name)
					throw new Exception("Empty Customer Name", 1);
				$date = date('Y-m-d');
				$place = $data['place'];
				$customer_name_arabic_text=trim(strtoupper($data['arabic_name']));
				$email = $data['email'];
				$customer_type_id = $data['customer_type_id'];
				$mobile = $data['mobile'];
				$description = $data['description'];
				$route_id = $data['route_id'];
				$customer_group_id = $data['customer_group_id'];
				$contact_person = $data['contact_person'];
				$credit_limit = $data['credit_limit'];
				$vat_no= trim(strtoupper($data['trn_no']));
				$function_return = $this->AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description,$main_group_id);
				if ($function_return['result'] != 'Success')
					throw new Exception($function_return['message']);
				$AccountHead_id = $this->AccountHead->getLastInsertId();
				$Customer_date = [
				'account_head_id' => $AccountHead_id,
				'customer_type_id' => $customer_type_id,
				'arabic_name'=>$customer_name_arabic_text,
				'place' => $place,
				'route_id' => $route_id,
				'division_id' => $division_id,
				 'sub_group_id' => $sub_group_id,
                 'main_group_id' => $main_group_id,
				'state_id'=>$data['state_id'],
				'customer_group_id' => $customer_group_id,
				'code' => $code,
				'vat_no'=>$vat_no,
				'email' => $email,
				'mobile' => $mobile,
				 'name' => $name,
				'contact_person' => $contact_person,
				'opening_balance' => $opening_balance,
				'credit_limit' => $credit_limit,
				'created_by' => $user_id,
				'modified_by' => $user_id,
				'created_at' => date('Y-m-d H:i:s', strtotime($date)),
				'updated_at' => date('Y-m-d H:i:s', strtotime($date)),
				];
				$this->Customer->create();
				if (!$this->Customer->save($Customer_date))
					throw new Exception("Error Customer Creation", 1);
				$return['status'] = 'Success';
				$last_iserted_customer = $this->Customer->findById($this->Customer->getLastInsertId());
				$Single_Customer['route_code'] = $this->Route->field(
					'Route.code',
					array('Route.id ' => $last_iserted_customer['Customer']['route_id']));
			   $CustomerPrice=$this->CustomerPrice->find('all',array(
				'conditions'=>array('CustomerPrice.customer_id'=>$last_iserted_customer['AccountHead']['id']),
				'fields'=>array(
					'CustomerPrice.product_id',
					'CustomerPrice.product_wholesale_price',
					),
				));
				   $Single_Customer['stock']=$CustomerPrice;
				$Single_Customer['id']=$last_iserted_customer['AccountHead']['id'];
				$Single_Customer['name']=$last_iserted_customer['AccountHead']['name'];
				$Single_Customer['Division']=$last_iserted_customer['Division']['name'];
				$Single_Customer['opening_balance']=$last_iserted_customer['AccountHead']['opening_balance'];
				$Single_Customer['email']=$last_iserted_customer['Customer']['email'];
				$Single_Customer['shope_code']=$last_iserted_customer['Customer']['code'];
				$Single_Customer['credit_limit']=$last_iserted_customer['Customer']['credit_limit'];
				$Single_Customer['address']=$last_iserted_customer['Customer']['place'];
				$Single_Customer['mobile']=$last_iserted_customer['Customer']['mobile'];
				$State=$this->State->field('name',['State.id'=>$last_iserted_customer['Customer']['state_id']]);
				$Single_Customer['State']=$State;
				$total = $Single_Customer['opening_balance'];
				$Recieved='0';
				$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($last_iserted_customer['AccountHead']['id']);
				$total+=$Debit_N_Credit_function['debit'];
				$Recieved+=$Debit_N_Credit_function['credit'];
				$Single_Customer['debit']=$total;
				$Single_Customer['credit']=$Recieved;
				$Sale=$this->Sale->find('all',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$last_iserted_customer['AccountHead']['id']
						),
					'fields'=>array(
						'Sale.id',
						'Sale.invoice_no',
						'Sale.grand_total',
						'Sale.balance',
						'AccountHead.id',
						'AccountHead.name',
						),
					));
				$All_sale=[];
				$Single_Customer['receipt']=$All_sale;
				$return['customer']= $Single_Customer;
				$return['key'] = $last_iserted_customer['AccountHead']['id'];
				$return['value'] = $last_iserted_customer['AccountHead']['name'];
				$datasource_Customer->commit();
				$datasource_AccountHead->commit();
				$return['status']='success';
			} catch (Exception $e) {
				$datasource_Customer->rollback();
				$datasource_AccountHead->rollback();
				$return['status']=$e->getMessage();
			}
		}
		echo json_encode($return);
		exit;
	}
//Customer creation 
//Day Close for Executive
	public function api_executive_day_close()
	{
		$user_id=1;
		$return=[
		'status'=>'Empty',
		];

		$data=$this->request->data;
// if(!$data)
// {
// 	$data=[
// 	"executive_id"=>7,
// 	"closing_stock"=>12500,
// 	"day_register_id"=>461];
// }
		if(isset($data['executive_id'])) $executive=$data['executive_id'];
		else $return[ 'status' ]='Executive Required';
		if(isset($executive))
		{
			$DayRegister=$this->DayRegister->findByExecutiveIdAndStatusAndId($executive,'0',$data['day_register_id']);

			if(!$DayRegister){
				$return[ 'status' ]= 'Already Closed';
			}
			else
			{

				$datasource_ClosingDay = $this->ClosingDay->getDataSource();
				$datasource_DayRegister = $this->DayRegister->getDataSource();
				try {
					$datasource_ClosingDay->begin();
					$datasource_DayRegister->begin();
					if(isset($data['closing_stock']))
						$closing_stock=$data['closing_stock'];
					else
						$closing_stock=0;
					if(floatval($data['km'])<floatval($DayRegister['DayRegister']['km']))
						throw new Exception("closing km must be greater than Starting km", 1);
					$date = $DayRegister['DayRegister']['date'];
					$tableData = [
					'executive_id' => $executive,
					'date' => $date,
					'km' => $data['km'],
					'closing_stock'=>$closing_stock,
					'closing_at'=>$data['datetime'],
					'status' => 1,
					'created_at'=>date('Y-m-d H:i:s'),
					'day_register_id' => $data['day_register_id'],
					];

					$this->ClosingDay->create();
					if (!$this->ClosingDay->save($tableData))
						throw new Exception("Error in day closing", 1);
					$this->DayRegister->id=$DayRegister['DayRegister']['id'];
					if (!$this->DayRegister->saveField('status','1'))
						throw new Exception("Error in DayRegister", 1);
					$datasource_ClosingDay->commit();
					$datasource_DayRegister->commit();
					$return['status']='success';
				} catch (Exception $e) {
					$datasource_ClosingDay->rollback();
					$datasource_DayRegister->rollback();
					$return['status']=$e->getMessage();
				}
			}
		}
		echo json_encode($return);
		exit;
	}
//Day Close for Executive
//No sale  --ubm
	public function api_no_sale()
	{
		$user_id=1;
		$return=[
		'status'=>'Empty',
		];
		$sub_group_id = 3;
		$data=$this->request->data;
		if($data)
		{
			if(isset($data['executive_id']))
			{
				$executive=$data['executive_id'];
				$datasource_NoSale = $this->NoSale->getDataSource();
				try {
					$datasource_NoSale->begin();
					$account_head_id=$data['customer_id'];
					if(empty($data['reason'])){
						$data['reason'] ="other";
					}
					$date = date('Y-m-d');
					$created_at=date('Y-m-d H:i:s');
					$tableData = [
					'executive_id' => $executive,
					'customer_account_head_id' => $account_head_id,
					'date' => $date,
					'description' => $data['reason'],
					'created_at'=>$created_at,
					];
					$this->NoSale->create();
					if (!$this->NoSale->save($tableData))
						throw new Exception("Error in No Sale", 1);
					$return['status'] = 'Success';
					$datasource_NoSale->commit();
					$return['status']='success';
				} catch (Exception $e) {
					$datasource_NoSale->rollback();
					$return['status']=$e->getMessage();
				}
			}
			else{
				$return[ 'status' ]= 'Executive Requered';
			}
		}
		echo json_encode($return);
		exit;
	}
//No sale
//Multiple No sale  --ubm
	public function api_multiple_no_sale()
	{
		$user_id=1;
		$return=[
		'status'=>'Empty',
		];
		$sub_group_id = 3;
		$data=$this->request->data['Visits'];
		if($data)
		{
			if(isset($this->request->data['executive_id']))
			{
				$executive=$this->request->data['executive_id'];
				if(isset($this->request->data['day_register_id']))
				{
					$day_register_id=$this->request->data['day_register_id'];	
				}
				else
				{
					$DayRegister=$this->DayRegister->find('first',[
						'conditions'=>['DayRegister.executive_id'=>$executive],
						'order'=>['DayRegister.id DESC']]);
					$day_register_id=$DayRegister['DayRegister']['id'];
				}
				$datasource_NoSale = $this->NoSale->getDataSource();
				try {
					for ($j=0; $j <count($data) ; $j++) {
						$datasource_NoSale->begin();
						$account_head_id=$data[$j]['customer_id'];
						if(empty($data[$j]['reason'])){
							$data[$j]['reason'] ="other";
						}
						$date = date('Y-m-d');
						$created_at=date('Y-m-d H:i:s');

						$tableData = [
						'executive_id' => $executive,
						'day_register_id' => $day_register_id,
						'customer_account_head_id' => $account_head_id,
						'date' => $date,
						'description' => $data[$j]['reason'],
						'created_at'=>$data[$j]['datetime'],
						];
						$this->NoSale->create();
						if (!$this->NoSale->save($tableData))
							throw new Exception("Error in No Sale", 1);
						$return['status'] = 'Success';
						$datasource_NoSale->commit();
					}
					$return['status']='success';
				} catch (Exception $e) {
					$datasource_NoSale->rollback();
					$return['status']=$e->getMessage();
				}
			}
			else{
				$return[ 'status' ]= 'Executive Requered';
			}
		}
		echo json_encode($return);
		exit;
	}
//Multiple No sale end
	public function GeneralStock_Edit_Function_in_Purchase($id,$date)
	{
		$user_id=1;
		try {
			$SaleItem = $this->SaleItem->find('all',array(
				'conditions' => array(
					'SaleItem.sale_id' => $id,
					),
				'fields' => array(
					'SaleItem.warehouse_id',
					'SaleItem.product_id',
					'SaleItem.quantity',
					'SaleItem.unit_price',
					'Sale.invoice_no',
					)
				));
			foreach ($SaleItem as $key => $value) {
				$quantity = $value['SaleItem']['quantity'];
				$unit_price = $value['SaleItem']['unit_price'];
				$product_id=$value['SaleItem']['product_id'];
				$warehouse_id=$value['SaleItem']['warehouse_id'];
				$Stock = $this->Stock->find('first',array(
					'conditions'=>array(
						'Stock.product_id'=>$product_id,
						'Stock.warehouse_id'=>$warehouse_id,
						)
					));
				$stock_id=$Stock['Stock']['id'];
				$stock_quantity=$Stock['Stock']['quantity'];
				$date=date('Y-m-d',strtotime($date));
				$remark='Sale --'.$value['Sale']['invoice_no'].'('.$quantity.')';
				$stock_quantity-=$quantity;
				$StockController = new StockController;
				$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
				if($Stock_function_return['result']!='Success')
					throw new Exception($Stock_function_return['result']);
			}
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function JournalCreate_in_Sale($invoice_no,$paid,$account_head_id,$date,$cashhead=1,$voucher_no=null,$day_register_id,$executive)
	{
		$user_id=1;
		try {

			if(empty($voucher_no)){
				$Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
				if($Journals)
				{
					$JournalsNo = $Journals['Journal']['voucher_no'];
					$voucher_no=$JournalsNo+1;
				}
				else { $voucher_no=1; }
			}
			$credit=$account_head_id;
			$debit=$cashhead;
			$amount=$paid;
			$date=$date;
			$remarks='Sale Invoice No :'.$invoice_no;
			$receipt_no="";
			$executive=$executive;
			$route_id=$this->ExecutiveRouteMapping->field('ExecutiveRouteMapping.route_id',array('ExecutiveRouteMapping.executive_id'=>$executive));
			$work_flow='Sales';
			$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no,$receipt_no,$day_register_id,$executive,'',$route_id);
			if($function_return['result']!='Success')
				throw new Exception($function_return['message'], 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,$voucher_no=null,$receipt_no=null,$day_register_id=null,$executive_id=null,$created_at=null,$route_id=null,$externalvoucher=null,$sales_return_invoice_no=null)
	{
		
		try {
			if(empty($voucher_no)){
				$Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
				if($Journals)
				{
					$JournalsNo = $Journals['Journal']['voucher_no'];
					$voucher_no=$JournalsNo+1;
				}
				else { $voucher_no=1; }
			}
			if($amount)
			{
				$Journal_data=[
				'remarks'=>$remarks,
				'credit'=>(float)$credit,
				'debit'=>(float)$debit,
				'amount'=>(float)$amount,
				'date'=>date('Y-m-d',strtotime($date)),
				'work_flow'=>$work_flow,
				'voucher_no'=>$voucher_no,
				'external_voucher'=>$externalvoucher,
				'receipt_no'=>$receipt_no,
				'flag'=>'1',
				'day_register_id'=>$day_register_id,
				'executive_id'=>$executive_id,
				'route_id'=>$route_id,
				'created_by'=>(int)$user_id,
				'modified_by'=>(int)$user_id,
				'created_at'=>$created_at,
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				$journal_id=$this->Journal->find('first',array(
					'order'=>'Journal.id DESC',
					));
				$journal_id_bonus=$journal_id['Journal']['id']+1;
				$credit_account=$this->AccountHead->findById($credit);
				$debit_account=$this->AccountHead->findById($debit);
				if($debit_account['AccountHead']['sub_group_id']==3 && $credit_account['AccountHead']['sub_group_id']==9)
				{}
				else if($debit_account['AccountHead']['sub_group_id']==5 && $credit_account['AccountHead']['sub_group_id']==3)
				{}
				$this->Journal->create();
				if(!$this->Journal->save($Journal_data))
				{
					$errors = $this->Journal->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
			}
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']='Error';
			$return['message']=$e->getMessage();
		}
		return $return;
	
	}
	public function GeneralStock_Edit_Function($stock_id,$quantity,$date,$user_id,$remark)
	{
		try {
			if(!$user_id)
				throw new Exception("Empty user Id", 1);
			if(!$stock_id)
				throw new Exception("Empty Stock Id", 1);
			if(!$date)
				throw new Exception("Empty date", 1);
			if(!$quantity && $quantity!=0)
				throw new Exception("Empty quantity Id", 1);
			if(!$remark)
				throw new Exception("Empty remark", 1);
			$this->Stock->id=$stock_id;
			if(!$this->Stock->saveField('quantity',$quantity ))
				throw new Exception("Error Processing Stock quantity Updation", 1);
			if(!$this->Stock->saveField('modified_by',$user_id ))
				throw new Exception("Error Processing Stock modified_by Updation", 1);
			if(!$this->Stock->saveField('flag','1' ))
				throw new Exception("Error Processing Stock flag Updation", 1);
			if(!$this->Stock->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date)) ))
				throw new Exception("Error Processing Stock updated_at Updation", 1);
			$Stock=$this->Stock->read();
			unset($Stock['Stock']['id']);
			unset($Stock['Stock']['flag']);
			unset($Stock['Stock']['modified_by']);
			$Stock['Stock']['remark']=$remark;
			$function_stockLog_return=$this->stock_logs($Stock['Stock']);
			if($function_stockLog_return['result']!='Success')
				throw new Exception($function_stockLog_return['result'], 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	function sale_target($id,$first_date){
		$SaleTargetList=$this->SaleTarget->find('first',array(
			'conditions'=>array('(SaleTarget.wef_date) <='=>$first_date,'executive_id'=>$id),
			'fields'=>'max(SaleTarget.wef_date) as date',
			));
		$target = $this->SaleTarget->field(
			'SaleTarget.target',
			array('SaleTarget.wef_date' => $SaleTargetList[0]['date'],'executive_id'=>$id));
		return $target;
	}
	public function General_Journal_Debit_N_Credit_invoice($account_head_id,$invoice_no)
	{
		$remarks='Sale Invoice No :'.$invoice_no;
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$Journal_Debit=$this->Journal->find('all',array('conditions'=>array(
			'debit'=>$account_head_id,
			'remarks'=>$remarks,
			'flag=1',
			)));
		foreach ($Journal_Debit as $key => $value) {
			$account_single['debit']+=$value['Journal']['amount'];
		}
		$Journal_Credit=$this->Journal->find('all',array('conditions'=>array(
			'credit'=>$account_head_id,
			'remarks'=>$remarks,
			'flag=1',
			)));
		foreach ($Journal_Credit as $key => $value) {
			$account_single['credit']+=$value['Journal']['amount'];
		}
		return $account_single;
	}
	public function General_Journal_Debit_N_Credit_function($account_head_id)
	{
		$account_single=[];
		$this->Journal->virtualFields = array('total_amount' => "SUM(Journal.amount)",);
		$Journal_Debit=$this->Journal->findByDebitAndFlag($account_head_id,'1',['Journal.total_amount']);
		$Journal_Credit=$this->Journal->findByCreditAndFlag($account_head_id,'1',['Journal.total_amount']);
		$account_single['credit']=$Journal_Credit['Journal']['total_amount'];
		$account_single['debit']=$Journal_Debit['Journal']['total_amount'];
		return $account_single;
	}
	public function AccountHeadCreate($sub_group_id,$name,$opening_balance,$date,$description,$main_group_id)
	{
		try {
			$AccountHead_date=[
			'name'=>strtoupper($name),
			'opening_balance'=>$opening_balance,
			'description'=>$description,
			'sub_group_id'=>$sub_group_id,
		   'acc_sub_group_id'=>$sub_group_id,
		   'acc_main_group_id'=>$main_group_id,
			'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
			'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
			];
			$this->AccountHead->create();
			if(!$this->AccountHead->save($AccountHead_date))
			{
				$errors = $this->AccountHead->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$return['result']='Success';
		} catch (Exception $e) {
			
			$return['result']='Error';
			$return['message']=$e->getMessage();
		}
		return $return;
	}
	public function AccountHead_Option_ListBySubGroupId($sub_group_id)
	{
		$AccountHeads=$this->AccountHead->find('all',array(
			'conditions'=>array(
				'SubGroup.id'=>$sub_group_id,
				),
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
				'SubGroup.id',
				)
			));
		$AccountHead_list=[];
		foreach ($AccountHeads as $key => $value) {
			$AccountHead_list[$value['AccountHead']['id']] = $value['AccountHead']['name'];
		}
		return $AccountHead_list;
	}
	public function api_get_customer_info()
	{
		$return=[
		'status'=>'Empty',
		'Customers'=>[],
		];
		$data=$this->request->data;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
			$warehouse_id = $this->Executive->field(
				'Executive.warehouse_id',
				array('Executive.id ' => $executive));
			$date = date('Y-m-d');
			$register_row = $this->DayRegister->find('first', array('conditions' => array('DayRegister.executive_id' => $executive,'DayRegister.date' => $date)));
			if(!empty($register_row)){
				$customer_group_id = $register_row['DayRegister']['customer_group_id'];
				$route_id = $register_row['DayRegister']['route_id'];
			}
		}
		else
		{
			$return[ 'status' ]= 'Executive Required';
		}
		if(isset($customer_group_id))
		{
			$CustomerList=$this->AccountHead->find('all',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('Customer.account_head_id=AccountHead.id'),
						),
					array(
						"table"=>'divisions',
						"alias"=>'Division',
						"type"=>'inner',
						"conditions"=>array('Customer.division_id=Division.id'),
						),
					array(
						"table"=>'customer_types',
						"alias"=>'CustomerType',
						"type"=>'inner',
						"conditions"=>array('Customer.customer_type_id=CustomerType.id'),
						),
					array(
						"table"=>'states',
						"alias"=>'State',
						"type"=>'inner',
						"conditions"=>array('State.id=Customer.state_id'),
						),
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
						),
					),
				'conditions'=>array('Customer.customer_group_id'=>$customer_group_id,
					'Customer.route_id'=>$route_id
					),
				'order'=>array('Customer.priority Asc'),
				'group' => array('Customer.id'),
				'fields'=>array(
					'Customer.*',
					'AccountHead.id',
					'AccountHead.name',
					'AccountHead.opening_balance',
					'Division.name',
					'CustomerType.name',
					'State.name',
					),
				));
			$general_customer=$this->AccountHead->find('first',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('Customer.account_head_id=AccountHead.id'),
						),
					array(
						"table"=>'divisions',
						"alias"=>'Division',
						"type"=>'left',
						"conditions"=>array('Customer.division_id=Division.id'),
						),
					array(
						"table"=>'customer_types',
						"alias"=>'CustomerType',
						"type"=>'left',
						"conditions"=>array('Customer.customer_type_id=CustomerType.id'),
						),
					array(
						"table"=>'states',
						"alias"=>'State',
						"type"=>'left',
						"conditions"=>array('State.id=Customer.state_id'),
						),
					),
				'conditions'=>array('Customer.account_head_id'=>3,
					),
				'fields'=>array(
					'Customer.*',
					'AccountHead.id',
					'AccountHead.name',
					'AccountHead.opening_balance',
					'Division.name',
					'State.name',
					'CustomerType.name'
					),
				));
			$CustomerList[]=$general_customer;
			sort($CustomerList);
			if($CustomerList)
			{
				$return[ 'status' ]='success';
				$All_Customer=[];
				foreach ($CustomerList as $key => $value) {
					$CustomerPrice=$this->CustomerPrice->find('all',array(
				'conditions'=>array('CustomerPrice.customer_id'=>$value['AccountHead']['id']),
				'fields'=>array(
					'CustomerPrice.product_id',
					'CustomerPrice.product_wholesale_price',
					'CustomerPrice.customer_id',
					),
				));
				   $Single_Customer['stock']=$CustomerPrice;
					 $Single_Customer['route_code']="";
				   if($value['Customer']['route_id'])
				   {
				   	$Single_Customer['route_code'] = $this->Route->field(
						'Route.code',
						array('Route.id ' => $value['Customer']['route_id']));
				   }
					$Single_Customer['id']=$value['AccountHead']['id'];
					$Single_Customer['name']=$value['AccountHead']['name'];
					$Single_Customer['opening_balance']=$value['AccountHead']['opening_balance'];
					$Single_Customer['Division']=$value['Division']['name'];
					$Single_Customer['State']=$value['State']['name'];
					$Single_Customer['CustomerType']=$value['CustomerType']['name'];
					$date = date('Y-m-d');
					$customer_account_info = $this->General_Journal_Openging_Debit_N_Credit_function($value['AccountHead']['id'],$date);
					$Single_Customer['debit']=$customer_account_info['debit']+$value['AccountHead']['opening_balance'];
					$Single_Customer['credit']=$customer_account_info['credit'];
					$balance = $customer_account_info['debit'] - $customer_account_info['credit']+$customer_account_info['opening_balance'];
					$Single_Customer['balance'] = $balance;
					$CustomerDiscount=$this->CustomerDiscount->find('all',array(
						'conditions'=>array('CustomerDiscount.account_head_id'=>$value['AccountHead']['id']),
						'fields'=>array('Product.id','Product.name','Product.code','Product.unit_id','CustomerDiscount.selling_rate'),
						));
					$Single_Customer['customer_product_list']=array();
					foreach ($CustomerDiscount as $key_product_rate => $value_product_rate) {
						$product_id=$value_product_rate['Product']['id'];
						$unit_id=$value_product_rate['Product']['unit_id'];

						$Single_Customer['customer_product_list'][$key_product_rate]['product_id']=$value_product_rate['Product']['id'];
						$Single_Customer['customer_product_list'][$key_product_rate]['product_code']=$value_product_rate['Product']['code'];
						$SaleController= new SaleController;
						$UnitLevelConvert=$SaleController->UnitLevelConvert($product_id,$unit_id);
						$sale_unit_level=$UnitLevelConvert['sale_unit_level'];
						$no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
						$product_unit_level=$UnitLevelConvert['product_unit_level'];
						$Single_Customer['customer_product_list'][$key_product_rate]['product_selling_rate']=$value_product_rate['CustomerDiscount']['selling_rate'];
						if($sale_unit_level != 1){
							if($sale_unit_level == 2){
								$Single_Customer['customer_product_list'][$key_product_rate]['product_selling_rate']=$value_product_rate['CustomerDiscount']['selling_rate']/$no_of_piece_per_unit;
							}
						} 
					}
					$Single_Customer['email']=$value['Customer']['email'];
					$Single_Customer['shope_code']=$value['Customer']['code'];
					$Single_Customer['credit_limit']=$value['Customer']['credit_limit'];
					$Single_Customer['address']=$value['Customer']['place'];
					$Single_Customer['latitude']="";$Single_Customer['longitude']="";$Single_Customer['arabic_name']="";
					if($value['Customer']['latitude']){
						$Single_Customer['latitude']=$value['Customer']['latitude'];
					}
					if($value['Customer']['longitude']){
						$Single_Customer['longitude']=$value['Customer']['longitude'];
					}
					if($value['Customer']['arabic_name']){
						$Single_Customer['arabic_name']=$value['Customer']['arabic_name'];
					}
					$Single_Customer['mobile']=$value['Customer']
					['mobile'];
					$Single_Customer['vat_no']=$value['Customer']
					['vat_no'];
					array_push($All_Customer, $Single_Customer);
				}
				$return[ 'Customers' ]=$All_Customer;
			}
			
		}
		else{
			$return[ 'status' ]= 'Group is not registerd';
		}
		echo json_encode($return,JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	}
	public function api_get_customer_info_by_search()
	{
		$conditions=[];
		$return=[
		'status'=>'Empty',
		'Customers'=>[],
		];
		$data=$this->request->data;
// $conditions['AccountHead.id']=869;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
			$warehouse_id = $this->Executive->field(
				'Executive.warehouse_id',
				array('Executive.id ' => $executive));
		}
		if(isset($data['route_id']))
		{
			$conditions['Customer.route_id']=$data['route_id'];
		}
		if(isset($data['customer_group_id']))
		{
			$conditions['Customer.customer_group_id']=$data['customer_group_id'];
		}
		{
			$CustomerList=$this->AccountHead->find('all',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('Customer.account_head_id=AccountHead.id'),
						),
					array(
						"table"=>'divisions',
						"alias"=>'Division',
						"type"=>'inner',
						"conditions"=>array('Customer.division_id=Division.id'),
						),
					array(
						"table"=>'customer_types',
						"alias"=>'CustomerType',
						"type"=>'inner',
						"conditions"=>array('Customer.customer_type_id=CustomerType.id'),
						),
					array(
						"table"=>'states',
						"alias"=>'State',
						"type"=>'inner',
						"conditions"=>array('State.id=Customer.state_id'),
						),
					array(
						"table"=>'executive_route_mappings',
						"alias"=>'ExecutiveRouteMapping',
						"type"=>'inner',
						"conditions"=>array('Customer.route_id=ExecutiveRouteMapping.route_id'),
						),
					),
				'conditions'=>$conditions,
				'order'=>array('Customer.priority Asc'),
				'group' => array('Customer.id'),
				'fields'=>array(
					'Customer.*',
					'AccountHead.id',
					'AccountHead.name',
					'AccountHead.opening_balance',
					'Division.name',
					'State.name',
					'CustomerType.name'
					),
				));
			$general_customer=$this->AccountHead->find('first',array(
				"joins"=>array(
					array(
						"table"=>'customers',
						"alias"=>'Customer',
						"type"=>'inner',
						"conditions"=>array('Customer.account_head_id=AccountHead.id'),
						),
					array(
						"table"=>'divisions',
						"alias"=>'Division',
						"type"=>'left',
						"conditions"=>array('Customer.division_id=Division.id'),
						),
					array(
						"table"=>'customer_types',
						"alias"=>'CustomerType',
						"type"=>'left',
						"conditions"=>array('Customer.customer_type_id=CustomerType.id'),
						),
					array(
						"table"=>'states',
						"alias"=>'State',
						"type"=>'left',
						"conditions"=>array('State.id=Customer.state_id'),
						),
					),
				'conditions'=>array('Customer.account_head_id'=>3,
					),
				'fields'=>array(
					'Customer.*',
					'AccountHead.id',
					'AccountHead.name',
					'AccountHead.opening_balance',
					'Division.name',
					'CustomerType.name',
					'State.name',
					),
				));
			$CustomerList[]=$general_customer;
			sort($CustomerList);
			if($CustomerList)
			{
				$return[ 'status' ]='success';
				$All_Customer=[];
				foreach ($CustomerList as $key => $value) {
			$CustomerPrice=$this->CustomerPrice->find('all',array(
				'conditions'=>array('CustomerPrice.customer_id'=>$value['AccountHead']['id']),
				'fields'=>array(
					'CustomerPrice.product_id',
					'CustomerPrice.product_wholesale_price',
					'CustomerPrice.customer_id',
					),
				));
				   $Single_Customer['stock']=$CustomerPrice;
					 $Single_Customer['route_code']="";
				   if($value['Customer']['route_id'])
				   {
				   	$Single_Customer['route_code'] = $this->Route->field(
						'Route.code',
						array('Route.id ' => $value['Customer']['route_id']));
				   }
					$Single_Customer['id']=$value['AccountHead']['id'];
					$Single_Customer['name']=$value['AccountHead']['name'];
					$Single_Customer['opening_balance']=$value['AccountHead']['opening_balance'];
				    $Single_Customer['Division']=$value['Division']['name'];
				    $Single_Customer['State']=$value['State']['name'];
				    $Single_Customer['CustomerType']=$value['CustomerType']['name'];
					$CustomerDiscount=$this->CustomerDiscount->find('all',array(
						'conditions'=>array('CustomerDiscount.account_head_id'=>$value['AccountHead']['id']),
						'fields'=>array('Product.id','Product.name','Product.code','CustomerDiscount.selling_rate'),
						));
					$Single_Customer['customer_product_list']=array();
					foreach ($CustomerDiscount as $key_product_rate => $value_product_rate) {
						$Single_Customer['customer_product_list'][$key_product_rate]['product_id']=$value_product_rate['Product']['id'];
						$Single_Customer['customer_product_list'][$key_product_rate]['product_code']=$value_product_rate['Product']['code'];
						$Single_Customer['customer_product_list'][$key_product_rate]['product_selling_rate']=$value_product_rate['CustomerDiscount']['selling_rate'];
					}
//$Single_Customer['customer_product_list']=$CustomerDiscount;
					$Single_Customer['email']=$value['Customer']['email'];
					$Single_Customer['shope_code']=$value['Customer']['code'];

					$Single_Customer['latitude']="";$Single_Customer['longitude']="";$Single_Customer['arabic_name']="";
					if($value['Customer']['latitude']){
						$Single_Customer['latitude']=$value['Customer']['latitude'];
					}
					if($value['Customer']['longitude']){
						$Single_Customer['longitude']=$value['Customer']['longitude'];
					}
					if($value['Customer']['arabic_name']){
						$Single_Customer['arabic_name']=$value['Customer']['arabic_name'];
					}
					$Single_Customer['address']=$value['Customer']['place'];
					$Single_Customer['mobile']=$value['Customer']		
					['mobile'];
					$Single_Customer['vat_no']=$value['Customer']		
					['vat_no'];

					$date = date('Y-m-d');
					$customer_account_info = $this->General_Journal_Openging_Debit_N_Credit_function($value['AccountHead']['id'],$date);
					$balance = $customer_account_info['debit'] - $customer_account_info['credit']+$customer_account_info['opening_balance'];
					$Single_Customer['debit']=$customer_account_info['debit']+$value['AccountHead']['opening_balance'];
					$Single_Customer['credit']=$customer_account_info['credit'];
					$Single_Customer['balance'] = $balance;
					$Single_Customer['credit_limit']=$value['Customer']['credit_limit'];
					array_push($All_Customer, $Single_Customer);
				}
				$return[ 'Customers' ]=$All_Customer;
			}
		}
//pr($All_Customer);exit;
		echo json_encode($return,JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE);
		exit;
	}
	public function General_Journal_Openging_Debit_N_Credit_function($account_head_id,$from_date)
	{	
		$account_single=[];
		$account_single['debit']=0;
		$account_single['credit']=0;
		$account_head_balance =$this->AccountHead->findById($account_head_id);
		$account_single['opening_balance'] = $account_head_balance['AccountHead']['opening_balance'];
		$Journal_Debit=$this->Journal->find('all',array('conditions'=>array(
			'Journal.date <='=>$from_date,
			'debit'=>$account_head_id,
			'flag=1',
			)));
		foreach ($Journal_Debit as $key => $value) {
			$account_single['debit']+=$value['Journal']['amount'];
		}
		$Journal_Credit=$this->Journal->find('all',array('conditions'=>array(
			'Journal.date <='=>$from_date,
			'credit'=>$account_head_id,
			'flag=1',
			)));
		foreach ($Journal_Credit as $key => $value) {
			$account_single['credit']+=$value['Journal']['amount'];
		}
		return $account_single;
	}
	public function stock_logs($stock_logs_array)
	{
		try {
			$this->StockLog->create();
			if(!$this->StockLog->save($stock_logs_array))
				throw new Exception("Cant Create Stock Log Data", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function api_get_product_type()
	{
		$return=[
		'status'=>'Empty',
		'ProductTypes'=>[],
		];
		$ProductType = $this->ProductType->find('list',array(
			"joins"=>array(
				array(
					"table"=>'products',
					"alias"=>'Product',
					"type"=>'inner',
					"conditions"=>array('ProductType.id=Product.product_type_id'),
					),
				),
			'group'=>array('ProductType.id'),
			'fields'=>array('id','name'),'order'=>array('name ASC'),));
		if($ProductType)
		{
			$return[ 'status' ]='success';
			$All_Type=[];
			foreach ($ProductType as $key => $value) {
				$Single_Type['id']=$key;
				$Single_Type['name']=$value;
				$ProductTypeBrandMapping = $this->ProductTypeBrandMapping->find('all', array(
					'conditions' => array('ProductTypeBrandMapping.product_type_id' => $key),
					'order'=>array('Brand.name ASC'),
					'fields' => array(
						'Brand.id',
						'Brand.name',
						)
					)
				);
				$All_Brand=[];
				$singl_Brand['id'] = 0;
				$singl_Brand['name'] = 'GENERAL';
				array_push($All_Brand, $singl_Brand);
				if(!empty($ProductTypeBrandMapping)){
					foreach ($ProductTypeBrandMapping as $keys => $row) {
						$singl_Brand['id'] = $row['Brand']['id'];
						$singl_Brand['name'] = $row['Brand']['name'];
						array_push($All_Brand, $singl_Brand);
					}
				}
				$Single_Type['Brands']=$All_Brand;
				array_push($All_Type, $Single_Type);
			}
			$return[ 'ProductTypes' ]=$All_Type;
		}
		echo json_encode($return);
		exit;
	}
//search stock  by product type id and brand id
	public function api_get_van_stock_search()
	{
		$return=[
		'status'=>'Empty',
		'Stocks'=>[],
		'total_stock'=>'0',
		'total_quantity'=>'0',
		];

		$data=$this->request->data;
		//pr($data);
		//exit;
//$data['executive_id']=1;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
			$warehouse_id = $this->Executive->field(
				'Executive.warehouse_id',
				array('Executive.id ' => $executive));
			//pr($warehouse_id);
			//exit;
			$conditions['Stock.flag'] = 1;
			$conditions['Stock.warehouse_id'] = $warehouse_id;
		}
		else{
			$return[ 'status' ]= 'Executive Required';
		}
		if(isset($executive))
		{
			if(isset($data['product_type_id'])){
				$conditions['Product.product_type_id'] = $data['product_type_id'];
			}
			if(isset($data['brand_id'])){
				$conditions['Product.brand_id'] = $data['brand_id'];
			}
			$conditions['Product.type'] = [2,5,8];
			$conditions['Product.active'] =1;
			//pr($conditions);
			//exit;
			$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchasedItem','PurchaseReturnItem','StockLog','Stock','UnwantedList')));
			$Stock=$this->Product->find('all',array(
				"joins"=>array(
					array(
						"table"=>'stocks',
						"alias"=>'Stock',
						"type"=>'inner',
						"conditions"=>array('Product.id=Stock.product_id'),
						),
					array(
						"table"=>'warehouses',
						"alias"=>'Warehouse',
						"type"=>'inner',
						"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
						),
					array(
						"table"=>'units',
						"alias"=>'SubUnit',
						"type"=>'left',
						"conditions"=>array('Unit.id=SubUnit.sub_unit_id'),
						),
					),
				'order'=>array('Product.id'),
				'conditions'=>$conditions,
				//'limit'=>500,
				'fields'=>array(
					'Product.*',
					'Unit.name',
					'ProductType.name',
					'Warehouse.name',
					'Brand.name',
					'Stock.*',
					'Unit.id',
					'Unit.name',
					'Unit.sub_unit_id',
					)
				));
//pr($Stock);
//exit;
			$All_Stock=[];
			$total_cost = 0;
			$total_quantity = 0;
			foreach($Stock as $value){
				if($value['Stock']['quantity']>0){
					$Single_stock['product_id']=$value['Product']['id'];
					$Single_stock['product_name']=$value['Product']['name'];
					$Single_stock['hsn_code']=$value['Product']['hsn_code'];
					$Single_stock['arabic_name']="";
					if($value['Product']['arabic_name']){
						$Single_stock['arabic_name']=$value['Product']['arabic_name'];
					}
					$Single_stock['product_code']=$value['Product']['code'];
					$Single_stock['priority']=$value['Product']['priority'];
					$Single_stock['product_bonus_percentage']=$value['Product']['bonus_percentage'];
					$Single_stock['product_barcode']=$value['Product']['barcode'];
					$Single_stock['product_type_name']=$value['ProductType']['name'];
					$Single_stock['type']=$value['Product']['type'];
					$Single_stock['tray_capacity']=$value['Product']['tray_occupation'];
					$Unit=[];
					$this->Unit->unbindModel(
						array('hasMany' => array(
							'Product',
							))
						);
					$Units_list=[];
					$Unit['Unit'][$value['Unit']['id']]=$value['Unit']['name'];
					if($value['Unit']['sub_unit_id']!=0)
					{
						$SubUnit=$this->Unit->find('first',array(
							'conditions'=>array(
//'Unit.conversion >1',
								'Unit.id'=>$value['Unit']['sub_unit_id']
								),
							'fields'=>array('Unit.id','Unit.name'),
							));
						$Unit['Unit'][$SubUnit['Unit']['id']]=$SubUnit['Unit']['name'];
					}
					foreach ($Unit['Unit'] as $key_unit => $value_unit) {
						$Single_unit['id']=$key_unit;
						$Single_unit['name']=$value_unit;
						array_push($Units_list, $Single_unit);
					}
					$Single_stock['Units_list']=$Units_list;
					$Single_stock['brand_name']=$value['Brand']['name'];
					$Single_stock['product_cost']=$value['Product']['cost'];
					$Single_stock['product_mrp']=$value['Product']['mrp'];
					//$Single_stock['product_wholesale_price']=$value['Product']['wholesale_price'];
					if(isset($data['customer_id'])){
					$conditions_sale=[];
					$conditions_sale['SaleItem.product_id']=$value['Product']['id'];
					$conditions_sale['Sale.account_head_id']=$data['customer_id'];
					$SaleItem=$this->SaleItem->find('first',array(
					"joins" => array(
					),
					'conditions'=>$conditions_sale,
					'order' => array('Sale.id DESC'),
					'fields'=>array(
					'SaleItem.unit_price',
					)
					));
					if(!empty($SaleItem)){
					$Single_stock['product_wholesale_price']=round($SaleItem['SaleItem']['unit_price'],2);
					}
					else
					{
					$Single_stock['product_wholesale_price']=$value['Product']['wholesale_price'];
					}
				    }
					else
					{
					$Single_stock['product_wholesale_price']=$value['Product']['wholesale_price'];	
					}
					$Single_stock['product_vat']=$value['Product']['tax'];
					$Single_stock['piece_per_cart']=$value['Product']['no_of_piece_per_unit'];
					$Single_stock['stock_quantity']=$value['Stock']['quantity'];
					$total_cost+=$value['Product']['cost']*$value['Stock']['quantity'];
					$total_quantity+=$value['Stock']['quantity'];
					array_push($All_Stock, $Single_stock);
				}
			}
//pr($Stock);exit;
			if($Stock)
			{
				$return[ 'status' ]='success';
				$return[ 'Stocks' ]=$All_Stock;
				$return[ 'total_stock' ]=$total_cost;
				$return[ 'total_quantity' ]=$total_quantity;
			}
			else{
				$return[ 'status' ]='No Stock';
			}
		}
		echo json_encode($return);
		exit;
	}
		public function api_get_damage_van_stock_search()
	{
		$return=[
		'status'=>'Empty',
		'Stocks'=>[],
		'total_stock'=>'0',
		'total_quantity'=>'0',
		];

		$data=$this->request->data;
		//pr($data);
		//exit;
//$data['executive_id']=1;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
			$warehouse_id = $this->Executive->field(
				'Executive.warehouse_id',
				array('Executive.id ' => $executive));
			//pr($warehouse_id);
			//exit;
			$conditions['Stock.flag'] = 1;
			//$conditions['Stock.warehouse_id'] = $warehouse_id;
			 $damage_warehouse_id= $this->Warehouse->field('Warehouse.id',array('Warehouse.warehouse_id ' =>$warehouse_id));
            $conditions['Stock.warehouse_id'] = $damage_warehouse_id;
		}
		else{
			$return[ 'status' ]= 'Executive Required';
		}
		if(isset($executive))
		{
			if(isset($data['product_type_id'])){
				$conditions['Product.product_type_id'] = $data['product_type_id'];
			}
			if(isset($data['brand_id'])){
				$conditions['Product.brand_id'] = $data['brand_id'];
			}
			$conditions['Product.type'] =[2,5,8];
			$conditions['Product.active'] =1;
			//pr($conditions);
			//exit;
			$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchasedItem','PurchaseReturnItem','StockLog','Stock','UnwantedList')));
			$Stock=$this->Product->find('all',array(
				"joins"=>array(
					array(
						"table"=>'stocks',
						"alias"=>'Stock',
						"type"=>'inner',
						"conditions"=>array('Product.id=Stock.product_id'),
						),
					array(
						"table"=>'warehouses',
						"alias"=>'Warehouse',
						"type"=>'inner',
						"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
						),
					array(
						"table"=>'units',
						"alias"=>'SubUnit',
						"type"=>'left',
						"conditions"=>array('Unit.id=SubUnit.sub_unit_id'),
						),
					),
				'order'=>array('Product.id'),
				'conditions'=>$conditions,
				//'limit'=>500,
				'fields'=>array(
					'Product.*',
					'Unit.name',
					'ProductType.name',
					'Warehouse.name',
					'Brand.name',
					'Stock.*',
					'Unit.id',
					'Unit.name',
					'Unit.sub_unit_id',
					)
				));
//pr($Stock);
//exit;
			$All_Stock=[];
			$total_cost = 0;
			$total_quantity = 0;
			foreach($Stock as $value){
				if($value['Stock']['quantity']>0){
					$Single_stock['product_id']=$value['Product']['id'];
					$Single_stock['product_name']=$value['Product']['name'];
					$Single_stock['hsn_code']=$value['Product']['hsn_code'];
					$Single_stock['arabic_name']="";
					if($value['Product']['arabic_name']){
						$Single_stock['arabic_name']=$value['Product']['arabic_name'];
					}
					$Single_stock['product_code']=$value['Product']['code'];
					$Single_stock['priority']=$value['Product']['priority'];
					$Single_stock['product_bonus_percentage']=$value['Product']['bonus_percentage'];
					$Single_stock['product_barcode']=$value['Product']['barcode'];
					$Single_stock['product_type_name']=$value['ProductType']['name'];
					$Single_stock['type']=$value['Product']['type'];
					$Unit=[];
					$this->Unit->unbindModel(
						array('hasMany' => array(
							'Product',
							))
						);
					$Units_list=[];
					$Unit['Unit'][$value['Unit']['id']]=$value['Unit']['name'];
					if($value['Unit']['sub_unit_id']!=0)
					{
						$SubUnit=$this->Unit->find('first',array(
							'conditions'=>array(
//'Unit.conversion >1',
								'Unit.id'=>$value['Unit']['sub_unit_id']
								),
							'fields'=>array('Unit.id','Unit.name'),
							));
						$Unit['Unit'][$SubUnit['Unit']['id']]=$SubUnit['Unit']['name'];
					}
					foreach ($Unit['Unit'] as $key_unit => $value_unit) {
						$Single_unit['id']=$key_unit;
						$Single_unit['name']=$value_unit;
						array_push($Units_list, $Single_unit);
					}
					$Single_stock['Units_list']=$Units_list;
					$Single_stock['brand_name']=$value['Brand']['name'];
					$Single_stock['product_cost']=$value['Product']['cost'];
					$Single_stock['product_mrp']=$value['Product']['mrp'];
					$Single_stock['product_wholesale_price']=$value['Product']['wholesale_price'];
					$Single_stock['product_vat']=$value['Product']['tax'];
					$Single_stock['piece_per_cart']=$value['Product']['no_of_piece_per_unit'];
					$Single_stock['stock_quantity']=$value['Stock']['quantity'];
					$total_cost+=$value['Product']['cost']*$value['Stock']['quantity'];
					$total_quantity+=$value['Stock']['quantity'];
					array_push($All_Stock, $Single_stock);
				}
			}
//pr($Stock);exit;
			if($Stock)
			{
				$return[ 'status' ]='success';
				$return[ 'Stocks' ]=$All_Stock;
				$return[ 'total_stock' ]=$total_cost;
				$return[ 'total_quantity' ]=$total_quantity;
			}
			else{
				$return[ 'status' ]='No Stock';
			}
		}
		echo json_encode($return);
		exit;
	}
//search stock  by product type id and brand id
//stock transfer cost
	public function stock_transfer_cost($id)
	{
		$total_stock= 0;
		$StockTransferitems=$this->StockTransferItem->find('all',array(
			"joins"=>array(
// array(
// 	"table"=>'products',
// 	"alias"=>'Product',
// 	"type"=>'inner',
// 	"conditions"=>array('Product.id=StockTransferItem.product_id'),
// 	),
				),
			"conditions"=>array('stock_transfer_id'=>$id),
			"fields"=>array(
				'StockTransferItem.quantity',
				'Product.mrp',
				),
			));
		$total_cost = 0;
		$total_quantity = 0;
		foreach($StockTransferitems as $value){
			if($value['StockTransferItem']['quantity']>0){
				$total_cost+=$value['Product']['mrp']*$value['StockTransferItem']['quantity'];
				$total_quantity+=$value['StockTransferItem']['quantity'];
			}
		}
		$total_stock=$total_cost;
		return $total_stock;
	}
// current van stock value
	public function current_van_stock($executive)
	{
		$total_stock= 0;
		$warehouse_id = $this->Executive->field(
			'Executive.warehouse_id',
			array('Executive.id ' => $executive));
		if(isset($executive))
		{
			$Stock = $this->Stock->find('all',array(
				'conditions'=>array('Stock.flag'=>1,'Stock.warehouse_id'=>$warehouse_id),
				'fields'=>array(
					'Product.mrp',
					'Stock.quantity',
					)
				));
// $Stock=$this->Product->find('all',array(
// 	"joins"=>array(
// 		// array(
// 		// 	"table"=>'stocks',
// 		// 	"alias"=>'Stock',
// 		// 	"type"=>'inner',
// 		// 	"conditions"=>array('Product.id=Stock.product_id'),
// 		// 	),
// 		// array(
// 		// 	"table"=>'warehouses',
// 		// 	"alias"=>'Warehouse',
// 		// 	"type"=>'inner',
// 		// 	"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
// 		// 	),
// 		),
// 	// 'conditions'=>array('Stock.flag'=>1,'Stock.warehouse_id'=>$warehouse_id),
// 	'fields'=>array(
// 		'Product.mrp',
// 		'Stock.*',
// 		)
// 	));
			$All_Stock=[];
			$total_cost = 0;
			$total_quantity = 0;
			foreach($Stock as $value){
				if($value['Stock']['quantity']>0){
					$total_cost+=$value['Product']['mrp']*$value['Stock']['quantity'];
					$total_quantity+=$value['Stock']['quantity'];
				}
			}
			if($Stock)
			{
				$total_stock=$total_cost;
			}
		}
		return $total_stock;
	}
	public function api_gps_location()
	{
		$return=[
		'status'=>'Empty',
		];
		$data=$this->request->data;
		if(isset($this->request->data['customer_id']))
		{
			$customer_id=$data['customer_id'];	
		}
		if(isset($data['latitude']))
		{
			$latitude=$data['latitude'];	
		}
		if(isset($data['longitude']))
		{
			$longitude=$data['longitude'];	
		}
		$this->Customer->id=$this->Customer->field('id', array('account_head_id' => $customer_id));
		if(!$this->Customer->saveField('latitude', $latitude))
			throw new Exception("Cant Updated the latitude", 1);
		if(!$this->Customer->saveField('longitude', $longitude))
			throw new Exception("Cant Updated the longitude", 1);
		$return[ 'status' ]='success';
		echo json_encode($return);
		exit;
	}
	public function api_get_bank_list()
	{
		$BankDetails=$this->BankDetails->find('all',array(
			'joins'=>array(
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'inner',
					'conditions'=>array('BankDetails.account_head_id=AccountHead.id')
					),
				),
			'fields'=>array('BankDetails.account_head_id','AccountHead.name','BankDetails.shwn_in_vouchr'),
			));
//pr($BankDetails);
//exit;
		if(!empty($BankDetails))
		{
			$return['status']="Success";
			$All_Banks=[];
			foreach ($BankDetails as $key => $value) {
				//pr($key);
				$Single_bank['id']=$value['BankDetails']['account_head_id'];
				$Single_bank['name']=$value['AccountHead']['name'];
				$Single_bank['shwn_in_vouchr']=$value['BankDetails']['shwn_in_vouchr'];
				array_push($All_Banks, $Single_bank);
			}
			//exit;
			$return[ 'Banks' ]=$All_Banks;

		}
		else
		{
			$return['status']="Empty";
		}

		echo json_encode($return);
		exit;

	}
	public function api_save_cheque_details()
	{
		$data=$this->request->data;
		$datasource_Cheque = $this->Cheque->getDataSource();
		try {
			for ($j=0; $j <count($data['ChequeList']) ; $j++) {
				$datasource_Cheque->begin();
				$executive=$this->Executive->field('Executive.name',array('Executive.id'=>$data['executive_id']));
				$Cheque_data=[
				'cheque_no'=>$data['ChequeList'][$j]['cheque_no'],
				'executive_id'=>$data['executive_id'],
				'cheque_amount'=>$data['ChequeList'][$j]['amount'],
				'bank_account_head_id'=>$data['ChequeList'][$j]['company_bank_id'],
				'account_head_id'=>$data['ChequeList'][$j]['customer_id'],
				'clearing_date'=>date('Y-m-d',strtotime($data['ChequeList'][$j]['clearing_date'])),
				'deposited_by'=>$executive,
				'cheque_date'=>date('Y-m-d',strtotime($data['ChequeList'][$j]['cheque_date'])),
				'recieved_date'=>date('Y-m-d',strtotime($data['ChequeList'][$j]['received_date'])),
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s'),

				];
				$this->Cheque->create();
				if(!$this->Cheque->save($Cheque_data))
				{
					$errors = $this->Cheque->validationErrors; foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
				}
				$datasource_Cheque->commit();
			}
			$return['result']='Success';
		} catch (Exception $e) {
			$datasource_Cheque->rollback();
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function api_executive_bonus_report()
	{
		
		$data=$this->request->data;
		$conditions=[];
		$from_date=date('Y-m-d',strtotime($data['from_date']));
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$conditions['ExecutiveBonusDetail.receipt_date between ? and ?']=[$from_date,$to_date];
		//$data['executive_id']=5;
		$conditions['ExecutiveBonusDetail.executive_id']=$data['executive_id'];
		$ExecutiveBonusDetail=$this->ExecutiveBonusDetail->find('all',array(
			'conditions'=>$conditions,
			'joins'=>array(
				array(
					'table'=>'sales',
					'alias'=>'Sale',
					'type'=>'INNER',
					'conditions'=>array('Sale.invoice_no=ExecutiveBonusDetail.invoice_no')
					),
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'INNER',
					'conditions'=>array('Executive.id=ExecutiveBonusDetail.executive_id')
					),
				array(
					'table'=>'account_heads',
					'alias'=>'AccountHead',
					'type'=>'INNER',
					'conditions'=>array('AccountHead.id=Sale.account_head_id')
					),
				),
			'fields'=>array(
				'Executive.name',
				'AccountHead.name',
				'Sale.grand_total',
				'ExecutiveBonusDetail.*',
				),
			));

		foreach ($ExecutiveBonusDetail as $key => $value) {
			$ExecutiveBonusDetail[$key]['ExecutiveBonusDetail']['sale_date']=date('d-m-Y',strtotime($value['ExecutiveBonusDetail']['sale_date']));
			if($value['ExecutiveBonusDetail']['sales_return_date']!=null)
				$ExecutiveBonusDetail[$key]['ExecutiveBonusDetail']['sales_return_date']=date('d-m-Y',strtotime($value['ExecutiveBonusDetail']['sales_return_date']));
			else
				$ExecutiveBonusDetail[$key]['ExecutiveBonusDetail']['sales_return_date']='';
			$ExecutiveBonusDetail[$key]['ExecutiveBonusDetail']['receipt_date']=date('d-m-Y',strtotime($value['ExecutiveBonusDetail']['receipt_date']));
			$sale_type=$this->Sale->field('Sale.sale_type1',array('Sale.invoice_no'=>$value['ExecutiveBonusDetail']['invoice_no']));
			if($sale_type=='CreditSale')
				$ExecutiveBonusDetail[$key]['ExecutiveBonusDetail']['amount']=0.00;
			else
				$ExecutiveBonusDetail[$key]['ExecutiveBonusDetail']['amount']=round($value['ExecutiveBonusDetail']['amount'],2);
			$ExecutiveBonusDetail[$key]['ExecutiveBonusDetail']['bonus_amount']=round($value['ExecutiveBonusDetail']['bonus_amount'],2);
			$ExecutiveBonusDetail[$key]['ExecutiveBonusDetail']['bonus_return_amount']=round($value['ExecutiveBonusDetail']['bonus_return_amount'],2);
			$ExecutiveBonusDetail[$key]['Sale']['grand_total']=round($value['Sale']['grand_total'],2);
		}

		if(!empty($ExecutiveBonusDetail))
		{
			$return['status']="Success";
			$All_list=[];
			foreach ($ExecutiveBonusDetail as $key => $value) {
				$Single_Executive['executive_name']=$value['Executive']['name'];
				$Single_Executive['invoice_no']=$value['ExecutiveBonusDetail']['invoice_no'];
				if(!empty($value['ExecutiveBonusDetail']['credit_note_no']))
					$Single_Executive['credit_note_no']=$value['ExecutiveBonusDetail']['credit_note_no'];
				else
					$Single_Executive['credit_note_no']='';
				$Single_Executive['customer_name']=$value['AccountHead']['name'];
				if(!empty($value['ExecutiveBonusDetail']['sale_date']))
					$Single_Executive['sale_date']=$value['ExecutiveBonusDetail']['sale_date'];
				else
					$Single_Executive['sale_date']='';
				if(!empty($value['ExecutiveBonusDetail']['sales_return_date']))
					$Single_Executive['sales_return_date']=$value['ExecutiveBonusDetail']['sales_return_date'];
				else
					$Single_Executive['sales_return_date']='';
				$Single_Executive['receipt_date']=$value['ExecutiveBonusDetail']['receipt_date'];
				$Single_Executive['eligibility']=$value['ExecutiveBonusDetail']['eligibility'];
				$Single_Executive['sale_amount']=$value['Sale']['grand_total'];
				$Single_Executive['amount_received']=$value['ExecutiveBonusDetail']['amount'];
				$Single_Executive['bonus_amount']=$value['ExecutiveBonusDetail']['bonus_amount'];
				$Single_Executive['bonus_return_amount']=$value['ExecutiveBonusDetail']['bonus_return_amount'];
				array_push($All_list, $Single_Executive);
			}
			$return[ 'ExecutiveBonusDetail' ]=$All_list;

		}
		else
		{
			$return['status']="Empty";
		}

		echo json_encode($return);
		exit;

	}
	public function api_customer_outstanding_report()
	{
		
		$data=$this->request->data;
		//$data['customer_id']=35;
		$conditions=[];
		//if(!empty($data['customer_id']))
		$customer=$data['customer_id'];
		$conditions['Customer.account_head_id']=$data['customer_id'];
		$Customers=$this->Customer->find('all',array(
			'fields'=>array(
				'AccountHead.id',
				'AccountHead.name',
				),
			'order' => array('Customer.id' => 'ASC'),
			'conditions'=>$conditions,
			)
		);
		$invoice_array=array();
		foreach ($Customers as $key => $value)
		{
			$account_id=$value['AccountHead']['id'];
			$Sales = $this->Sale->find('all', array(
				'conditions' => array(
					'Sale.account_head_id' =>$account_id,
					'Sale.flag'=>1,
					'Sale.status'=>array(2,3),
					),
				'order' => array('Sale.date_of_delivered' => 'ASC'),
				'fields' => array(
					'Sale.id',
					'Sale.date_of_delivered',
					'Sale.invoice_no',
					'Sale.account_head_id',
					'Sale.executive_id',
					'Sale.grand_total',
					'Sale.balance',
					'AccountHead.name',
					'Executive.account_head_id',

					)
				)
			);
			$voucher_amount=0;
			$discount_paid_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DISCOUNT PAID'));
			$Journal_voucher=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.credit'=>$account_id,
					//'Journal.debit'=>$value['Executive']['account_head_id'],
					'NOT' => array(
						'Journal.debit'=>array($discount_paid_account_head_id),
						'Journal.remarks LIKE'=>'%Sale Invoice No :%',
						),
					'Journal.flag=1',
					),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
					),
				));
			foreach ($Journal_voucher as $key3 => $value_amount) {
				$voucher_amount+=$value_amount;
			}
			$Journal_extra_debited=$this->Journal->find('list',array(
				'conditions'=>array(
					'Journal.debit'=>$account_id,
					'Journal.work_flow'=>array('Cheques'),
					'Journal.flag=1',
					),
				'fields'=>array(
					'Journal.id',
					'Journal.amount',
					),
				));
			$other_debited_amount=0;
			foreach ($Journal_extra_debited as $key3 => $value_amount) {
				$other_debited_amount+=$value_amount;
			}
			$voucher_balance=0;
			foreach ($Sales as $keyj =>$valuej)
			{
				$SaleItem=$this->SaleItem->find('list',array(
					'conditions'=>array(
						'SaleItem.sale_id'=>$valuej['Sale']['id'],
						),
					'fields'=>array(
						)
					));
			}
			$invoice_array_single=array();
			if (!empty($Sales)) {


				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['sale_amount'] = array();
				$invoice_array_single['paid'] = array();
				$invoice_array_single['sale_date'] = array();
				$invoice_array_single['days']=array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				$Journal_opening_received=$this->Journal->find('all',array(
					'conditions'=>array(
						'Journal.remarks'=>'Sale Invoice No :0',
						'Journal.credit'=>$value['AccountHead']['id'],
					// 'NOT' => array(
					// 	'Journal.work_flow'=>'From Mobile',
					// ),
					// 'Journal.debit=1',
						'Journal.flag=1',
						),
					));
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				$opening_paid_amount=0;
				foreach ($Journal_opening_received as $key_each_sale => $value_each_sale) {
					$opening_paid_amount+=$value_each_sale['Journal']['amount'];
				}
				$outstanding_amount+=$other_debited_amount;
				$outstanding_amount-=$opening_paid_amount;
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{
					$now = time();
					$expected_days_diff=$now-strtotime($outstanding_amount_date);
					$expected_days=floor($expected_days_diff / (60 * 60 * 24))+1;
					array_push($invoice_array_single['sale_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
					array_push($invoice_array_single['invoice_no'], 'Opening Balance');
					array_push($invoice_array_single['sale_amount'],$AccountHead['AccountHead']['opening_balance']) ;
					array_push($invoice_array_single['paid'],$AccountHead['AccountHead']['opening_balance']-$outstanding_amount) ;
					array_push($invoice_array_single['balance'],$outstanding_amount);
					array_push($invoice_array_single['days'],$expected_days);

				}
				foreach ($Sales as $keySC2 => $valueSC2) {
					$Journal=$this->Journal->find('all',array(
						'conditions'=>array(
							'Journal.remarks'=>'Sale Invoice No :'.$valueSC2['Sale']['invoice_no'],
							'Journal.credit'=>$valueSC2['Sale']['account_head_id'],
							'Journal.debit=1',
							'Journal.flag=1',
							),
						));
					$balance_amount=$valueSC2['Sale']['grand_total'];
					$paid_amount=0;
					if($voucher_amount)
					{
						if($balance_amount<=$voucher_amount)
						{
							$voucher_balance=$balance_amount;
							$balance_amount=0;
							$voucher_amount-=$voucher_balance;
						}
						else
						{
							$balance_amount-=$voucher_amount;
							$voucher_amount=0;
						}
					}
					if($valueSC2['Sale']['grand_total']>0)
					{
						if($balance_amount){
							if($valueSC2['Sale']['account_head_id']==$customer)
							{
								$check_date=$valueSC2['Sale']['date_of_delivered'];
								$now = time();
								$expected_days_diff=$now-strtotime($valueSC2['Sale']['date_of_delivered']);
								$expected_days=floor($expected_days_diff / (60 * 60 * 24))+1;

								array_push($invoice_array_single['sale_date'], date("d-m-Y", strtotime($valueSC2['Sale']['date_of_delivered'])));
								array_push($invoice_array_single['invoice_no'], $valueSC2['Sale']['invoice_no']);
								array_push($invoice_array_single['sale_amount'],$valueSC2['Sale']['grand_total']) ;
								array_push($invoice_array_single['balance'],$valueSC2['Sale']['balance']) ;
								array_push($invoice_array_single['paid'],$valueSC2['Sale']['grand_total']-$valueSC2['Sale']['balance']) ;  
								array_push($invoice_array_single['days'],$expected_days);

							}
						}
					}
				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      

			}
			else
			{
				$invoice_array_single['party_name'] = $value['AccountHead']['name'];
				$invoice_array_single['invoice_no'] = array();
				$invoice_array_single['balance'] = array();
				$invoice_array_single['sale_date'] = array();
				$AccountHead=$this->AccountHead->findById($value['AccountHead']['id']);
				if($AccountHead)
				{
					$outstanding_amount=$AccountHead['AccountHead']['opening_balance'];
					$outstanding_amount_date=date('d-m-Y',strtotime($AccountHead['AccountHead']['created_at']));
				}
				$Journal_opening_received=$this->Journal->find('all',array(
					'conditions'=>array(
						'Journal.remarks'=>'Sale Invoice No :0',
						'Journal.credit'=>$value['AccountHead']['id'],
						'Journal.flag=1',
						),
					));
				$opening_paid_amount=0;
				foreach ($Journal_opening_received as $key_each_sale => $value_each_sale) {
					$opening_paid_amount+=$value_each_sale['Journal']['amount'];
				}
				$outstanding_amount+=$other_debited_amount;
				$outstanding_amount-=$opening_paid_amount;
				if($outstanding_amount>$voucher_amount)
				{
					$outstanding_amount-=$voucher_amount;
					$voucher_amount=0;
				}
				else
				{
					$voucher_amount-=$outstanding_amount;
					$outstanding_amount=0;
				}
				if($outstanding_amount>=1)
				{

					$now = time();
					$expected_days_diff=$now-strtotime($outstanding_amount_date);
					$expected_days=floor($expected_days_diff / (60 * 60 * 24))+1;
					array_push($invoice_array_single['sale_date'],date("d-m-Y", strtotime($outstanding_amount_date)));
					array_push($invoice_array_single['invoice_no'], 'Opening Balance');
					array_push($invoice_array_single['sale_amount'],$AccountHead['AccountHead']['opening_balance']) ;
					array_push($invoice_array_single['paid'],$AccountHead['AccountHead']['opening_balance']-$outstanding_amount) ;
					array_push($invoice_array_single['balance'],$outstanding_amount);
					array_push($invoice_array_single['days'],$expected_days);

				}
				if(count($invoice_array_single['invoice_no']))
				{
					array_push($invoice_array, $invoice_array_single);  
				}      
			}

		}
		//pr($invoice_array);exit;
		$Balance_Total=0;
		if(!empty($invoice_array))
		{
			$return['status']='Success';
			$All_list=[];
			for($j=0;$j<count($invoice_array[0]['invoice_no']);$j++)
			{
				$Single_Customer['customer_name']=$invoice_array[0]['party_name'];
				$Single_Customer['invoice_no']=$invoice_array[0]['invoice_no'][$j];
				$Single_Customer['sale_date']=$invoice_array[0]['sale_date'][$j];
				$Single_Customer['sale_amount']=$invoice_array[0]['sale_amount'][$j];
				$Single_Customer['paid']=$invoice_array[0]['paid'][$j];
				$Single_Customer['balance']=$invoice_array[0]['balance'][$j];
				$Single_Customer['days']=$invoice_array[0]['days'][$j];
				array_push($All_list, $Single_Customer);
			}
			$return[ 'Invoice_list' ]=$All_list;
		}
		else
		{
			$return['status']="Empty";
		}

		echo json_encode($return);
		exit;

	}
	public function stock_value_migration_in_journal()
	{
		$Sale=$this->Sale->find('all',array(
			'conditions'=>array('Sale.is_erp'=>0),
			));
		$user_id=1;
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			foreach ($Sale as $key => $value) {
				$stock_value=0;
				$date=$value['Sale']['date_of_delivered'];
				$remarks='Sale Invoice No :'.$value['Sale']['invoice_no'];
				$work_flow='From Executive app';
				$SaleItem=$this->SaleItem->find('all',array(
					'conditions'=>array('SaleItem.sale_id'=>$value['Sale']['id'],),
					'fields'=>array('SaleItem.quantity','Product.cost'),
					));
				foreach ($SaleItem as $key_item => $value_item) {
					$stock_value=$value_item['SaleItem']['quantity']*$value_item['Product']['cost'];
				}
				$sale_account_head_id=6;
				$stock_account_head_id=19;
				$receipt_no="";
				$Journal=$this->Journal->find('first',array(
					'conditions'=>array('Journal.remarks'=>'Sale Invoice No :'.$value['Sale']['invoice_no'],),
					'fields'=>array(
						'Journal.voucher_no',
						'Journal.day_register_id',
						'Journal.executive_id',
						'Journal.route_id',
						),
					));
				$Journals_no=$Journal['Journal']['voucher_no'];
				$day_register_id=$Journal['Journal']['day_register_id'];
				$executive=$Journal['Journal']['executive_id'];
				$route_id=$Journal['Journal']['route_id'];
				if($stock_value)
				{
					$debit=$sale_account_head_id;
					$credit=$stock_account_head_id;

		            // $Accountings_function_return=$this->JournalCreate($credit,$debit,$stock_value,$date,$remarks,$work_flow,$user_id,$Journals_no,$receipt_no,$day_register_id,$executive,'',$route_id);
		            // if($Accountings_function_return['result']!='Success')
		            //   throw new Exception($Accountings_function_return['message']);
				}
				$datasource_Journal->commit();
			}
		}
		catch (Exception $e)
		{
			$datasource_Journal->rollback();
			$return['result']=$e->getMessage();
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
		exit;
	}
	
	public function sale_tax_migration_in_journal()
	{
		// $Sale=$this->Sale->find('all',array(
		// 	'conditions'=>array('Sale.is_erp'=>0),
		// ));
		$Journal=$this->Journal->find('all',array(
			'conditions'=>array('Journal.remarks LIKE'=>'%Sale Invoice No :%','Journal.credit'=>9),
			'fields'=>array(
				'Journal.id',
				'Journal.executive_id',
				'Journal.route_id',
				),
			));
		$user_id=1;
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			foreach ($Journal as $key => $value) {				

				$this->Journal->id=$value['Journal']['id'];
				$this->Journal->saveField('credit',23);
					//$this->Journal->saveField('executive_id',$executive_id);
				$datasource_Journal->commit();
			}
		}
		catch (Exception $e)
		{
			$datasource_Journal->rollback();
			$return['result']=$e->getMessage();
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
		exit;
	}

	public function api_get_expense()
	{
		$return=[
		'status'=>'Empty',
		'Expense'=>[],
		];
		try {
			$All_Account=[];
			$AccountHeads=$this->AccountHead->find('all',array(
				'joins'=>array(
				array(
				'table'=>'acc_sub_groups',
				'alias'=>'AccSubGroup',
				'type'=>'INNER',
				'conditions'=>array('AccSubGroup.id=AccountHead.acc_sub_group_id')
				),
				),
				'conditions'=>array(
					'AccountHead.acc_sub_group_id'=>array(11,12),
					'AccountHead.show_in_app'=>1,
					),
				'fields'=>array(
					'AccountHead.id',
					'AccountHead.name',
					'AccountHead.acc_sub_group_id',
					)
				));
			$return[ 'status' ]='success';
			foreach ($AccountHeads as $key => $value) {
				$single_Account['id'] = $value['AccountHead']['id'];
				$single_Account['name'] = $value['AccountHead']['name'];
				array_push($All_Account, $single_Account);
			}
			$return[ 'Expense' ]=$All_Account;
		} catch (Exception $e) {
			$return[ 'status' ]=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function route_migration_in_journal()
	{
		$DayRegister=$this->DayRegister->find('all',array(
			));
		$user_id=1;
		$datasource_Journal = $this->Journal->getDataSource();
		try {
			$datasource_Journal->begin();
			foreach ($DayRegister as $key => $value) {
				$Journal=$this->Journal->find('all',array(
					'conditions'=>array('Journal.day_register_id'=>$value['DayRegister']['id']),
					'fields'=>array(
						'Journal.id',
						'Journal.route_id',
						),
					));
				if($Journal)
				{
					foreach ($Journal as $key => $value1) {
						$this->Journal->id=$value1['Journal']['id'];
						$this->Journal->saveField('route_id',$value['DayRegister']['route_id']);
					}
				}
				$datasource_Journal->commit();
			}
		}
		catch (Exception $e)
		{
			$datasource_Journal->rollback();
			$return['result']=$e->getMessage();
			$this->Session->setFlash(__($return['result']));
			$this->redirect( Router::url( $this->referer(), true ) );
		}
		exit;
	}
	public function api_expense_add($id=NULL) {
		$return['result']='Error';
		if($this->request->data){
			$total_approved_amount=0;
			$total_amount=0;
			$count=count($this->request->data['expense_list']);
			$data=$this->request->data['expense_list'];
			$day_register_id=$this->request->data['day_register_id'];
			$executive_id=$this->request->data['executive_id'];
			$route_id=$this->request->data['route_id'];
			$route_account_head_id=$this->Route->findById($this->request->data['route_id']);
			$datasource_Journal = $this->Journal->getDataSource();
			try {
				$datasource_Journal->begin();
				for ($i=0; $i < $count ; $i++) {
					$credit=$route_account_head_id['Route']['account_head_id'];
					$debit=$data[$i]['expense_id'];
					$amount=$data[$i]['amount'];
					$remarks=$data[$i]['remarks'];
					$date=$data[$i]['date'];
					$receipt_no=$data[$i]['receipt_no'];
					$work_flow='Expense';
					$user_id=1;
							//$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id);
					$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,'',$receipt_no,$day_register_id,$executive_id,'',$route_id);
					if($function_return['result']!='Success')
						throw new Exception($function_return['message'], 1);
				}
				$datasource_Journal->commit();
				$return['result']='Success';
			} catch (Exception $e) {
				$return['result']='Error';
				$return['message']=$e->getMessage();
			}
		}
		echo json_encode($return);
		exit;
	}
	public function api_invoice_multiple_sale_return()
	{
		$sale_return_account_head_id=4;
		//$tax_on_sale_return_account_head_id=11;
		$tax_on_sale_return_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'DUTIES & TAXES'));
		$stock_account_head_id=19;
		$user_id=1;
		$return=[
		'status'=>'empty',
		];

		$executive=$this->request->data['executive_id'];
		$date = date('Y-m-d');
		if(isset($this->request->data['day_register_id']))
		{
			$day_register_id=$this->request->data['day_register_id'];	
		}
		else
		{
			$DayRegister=$this->DayRegister->find('first',[
				'conditions'=>['DayRegister.executive_id'=>$executive],
				'order'=>['DayRegister.id DESC']]);
			$day_register_id=$DayRegister['DayRegister']['id'];
		}
		$route_id=$this->DayRegister->field(
			'DayRegister.route_id',
			array('DayRegister.id ' => $day_register_id));

		$DayRegister=$this->DayRegister->findByExecutiveIdAndStatus($executive,'0');
//$rowcount = $this->ClosingDay->find('count', array('conditions' => array('ClosingDay.executive_id' => $executive,'ClosingDay.date' => $date)));
		if(!$DayRegister){
			$return['status']='Day Closed';
		}
		else{
			$datasource_SalesReturn = $this->SalesReturn->getDataSource();
			$datasource_SalesReturnItem = $this->SalesReturnItem->getDataSource();
			$datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
			$datasource_ExecutiveBonusCalculation = $this->ExecutiveBonusCalculation->getDataSource();
			$datasource_ExecutiveBonus = $this->ExecutiveBonus->getDataSource();
			$datasource_Stock = $this->Stock->getDataSource();
			$datasource_Journal = $this->Journal->getDataSource();
			try {
				$datasource_SalesReturn->begin();
				$datasource_SalesReturnItem->begin();
				$datasource_ProductBonusDetail->begin();
				$datasource_ExecutiveBonusCalculation->begin();
				$datasource_ExecutiveBonus->begin();
				$datasource_Journal->begin();
				$data=$this->request->data['SalesReturn'];
				// $data_return=$this->request->data['SalesReturn'];
				// if(!empty($data_return[0]))
				// 	$data=$data_return;
				// else
				// 	$data=[];
				// 	$data[0]=$data_return;
				// $data=$data_return;
				//pr($data);exit;

				$SalesReturn=$this->SalesReturn->find('first',array('order' => 'SalesReturn.id DESC',));
				// if(!empty($SalesReturn))
				// {
				// 	$SalesReturnNo = $SalesReturn['SalesReturn']['invoice_no'];
				// 	$invoice_no=$SalesReturnNo+1;
				// }
				// else
				// {
				// 	$invoice_no=1;
				// }
				$invoice_no=$data[$j]['return_invoice_no'];
			    $executive_warehouse=$this->Executive->findById($this->request->data['executive_id']);
				$sales_return_invoice_no=$invoice_no;
				$SalesReturn_data=[
				 'warehouse_id'=>$executive_warehouse['Executive']['warehouse_id'],
				'account_head_id'=>$data[0]['customer_id'],
				'day_register_id'=>$day_register_id,
				'executive_id'=>$executive,
				'invoice_no'=>$invoice_no,
				'date'=>date('Y-m-d'),
				'total'=>$data[0]['grand_total'],
				'discount'=>'0',
				'grand_total'=>$data[0]['grand_total'],
				'status'=>2,
				'created_by'=>$user_id,
				'modified_by'=>$user_id,
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s'),
				];

				$this->SalesReturn->create();


				for ($j=0; $j <count($data) ; $j++) {
					
					if(!$this->SalesReturn->save($SalesReturn_data))
					{
						$errors = $this->SalesReturn->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);

						}
					}
					$tax_amount = 0;
					$net_value = 0 ;
					$sales_return_id=$this->SalesReturn->getLastInsertId();
					$ReturnedProduct = $data[$j]['ReturnedProduct'];
					$stock_value=0; $bonus_amount_total=0;
					for ($i=0; $i <count($ReturnedProduct) ; $i++) {
						$unit_id=1;
						$SaleController= new SaleController;
						$UnitLevelConvert=$SaleController->UnitLevelConvert($ReturnedProduct[$i]['product_id'],$unit_id);
				        //pr($UnitLevelConvert);exit;
						$no_of_piece_per_unit=$UnitLevelConvert['no_of_piece_per_unit'];
						$product_unit_level=$UnitLevelConvert['sale_unit_level'];
				        //$sale_unit_level=$sales_return_items['sale_unit_level'][$i];
						$tax = $ReturnedProduct[$i]['tax'];
						$taxable_value=$ReturnedProduct[$i]['return_quantity']*$ReturnedProduct[$i]['unit_price'];
						$netvalue=($taxable_value*100)/($tax+100);
                         $taxamount=$netvalue*$tax/100;
						$net_value = $net_value + $netvalue;
						$tax_amount = $tax_amount + $taxamount;
						$ReturnedProduct[$i]['net_value'] = $ReturnedProduct[$i]['refund_Amount'] - $taxamount;
						$tax_amount = $tax_amount + $taxamount;
						$net_value = $net_value + $ReturnedProduct[$i]['net_value'];
						$return_quantity=$ReturnedProduct[$i]['return_quantity'];
						$bonus_return_amount=$ReturnedProduct[$i]['product_bonus']*$return_quantity;
						$cost = $this->Product->field(
							'Product.cost',
							array('Product.id ' => $ReturnedProduct[$i]['product_id']));
							$executive_rate = $this->ExecutiveRate->field(
							'ExecutiveRate.executive_rate',
							array('ExecutiveRate.executive_id ' =>$executive,'ExecutiveRate.product_id ' => $ordered_products[$i]['product_id']));
							if(empty($executive_rate))
							{
							$executive_rate = $this->Product->field(
							'Product.wholesale_price',
							array('Product.id ' => $ordered_products[$i]['product_id']));
							}
						$stock_value+=$cost*$ReturnedProduct[$i]['return_quantity'];
						$sale_executive_id=0;
						if(empty($data[$j]['invoice_id']))
							$data[$j]['invoice_id']=0;
						$sale_invoice_no=$data[$j]['invoice_id'];
				       if($ReturnedProduct[$i]['product_id']==1)
				       {
				       	$warehouse_id= $executive_warehouse['Executive']['warehouse_id'];
				       }
				       else
				       {
                        $warehouse_id= $this->Warehouse->field('Warehouse.id',array('Warehouse.warehouse_id ' =>$executive_warehouse['Executive']['warehouse_id']));
				        if(empty($warehouse_id))
				         throw new Exception("Damage Warehouse Required", 1);
				       }
						if(!empty($sale_invoice_no))
						{
						$executive=$this->Sale->field('Sale.executive_id',array('Sale.invoice_no'=>$sale_invoice_no));
						}
						$SalesReturnItem_data=[
						'type'=>"Damage",
						'warehouse_id'=>$warehouse_id,
						'product_id'=>$ReturnedProduct[$i]['product_id'],
						'sales_return_id'=>$sales_return_id,
						'sale_executive_id'=>$executive,
						'invoice_no'=>$sale_invoice_no,
						'invoice_price'=>$ReturnedProduct[$i]['unit_price'],
						'unit_price'=>$ReturnedProduct[$i]['unit_price'],
						'quantity'=>$return_quantity,
						'net_value'=>$netvalue,
						'bonus_return_amount'=>$bonus_return_amount,
						'tax'=>$ReturnedProduct[$i]['tax'],
						'tax_amount'=>$taxamount,
						'total'=>$ReturnedProduct[$i]['refund_Amount'],
						];

						$this->SalesReturnItem->create();
						if(!$this->SalesReturnItem->save($SalesReturnItem_data))
						{
							$errors = $this->SalesReturnItem->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}
						$sales_return_item_id=$this->SalesReturnItem->getLastInsertId();
						$bonus_return_amount=($ReturnedProduct[$i]['unit_price']-$executive_rate)*$return_quantity;
						  if($bonus_return_amount>0)
						{
						 $bonus_amount_total=$bonus_amount_total+$bonus_return_amount;
						  $ProductBonus_data=[
				         'day_register_id'=>$day_register_id,
                          'executive_id'=>$executive,
				          'product_id'=>$ReturnedProduct[$i]['product_id'],
				          'sales_return_id'=>$sales_return_id,
				          'sales_return_item_id'=>$sales_return_item_id,
				          'unit_price'=>$ReturnedProduct[$i]['unit_price'],
				          'executive_rate'=>$executive_rate,
				          'quantity'=>$return_quantity,
				          'bonus_return_amount'=>$bonus_return_amount,
				          ];
				          $this->ProductBonusDetail->create();
				          if(!$this->ProductBonusDetail->save($ProductBonus_data))
				          {
				            $errors = $this->ProductBonusDetail->validationErrors;
				            foreach ($errors as $key => $value) {
				              throw new Exception($value[0], 1);
				            }
				          }
				        }
					}
					if($bonus_amount_total>0)
                {
                $ProductBonus_data=[
                'executive_id'=>$executive,
                'day_register_id'=>$day_register_id,
                'date'=>date('Y-m-d'),
                'work_flow'=>"SALE RETURN",
                'reference_no'=>$invoice_no_journal,
                'bonus_return_amount'=>$bonus_amount_total,
                ];
                $this->ExecutiveBonusCalculation->create();
                if(!$this->ExecutiveBonusCalculation->save($ProductBonus_data))
                {
                  $errors = $this->ExecutiveBonusCalculation->validationErrors;
                  foreach ($errors as $key => $value) {
                    throw new Exception($value[0], 1);
                  }
                }
              }
					$Journals=$this->Journal->find('first',array('order' => 'Journal.id DESC',));
					if(!empty($Journals))
					{
						$JournalsNo = $Journals['Journal']['voucher_no'];
						$Journals_no=$JournalsNo+1;
					}
					else
					{
						$Journals_no=1;
					}
					$user_id = 1;
					$debit=$sale_return_account_head_id;
					$credit=$data[$j]['customer_id'];
					$amount=$net_value+$tax_amount;
					$net_value=round($net_value,2);
					$tax_amount=round($tax_amount,2);
					$discount=0;
					$work_flow='Sales Return From Executive app';
					//$invoice_no= $invoice_no_journal;
					$date=date('Y-m-d');
					$remarks='SalesReturn Invoice No :'.$invoice_no_journal;
					$route_id=$this->ExecutiveRouteMapping->field('ExecutiveRouteMapping.route_id',array('ExecutiveRouteMapping.executive_id'=>$executive));
					$Accountings_function_return=$this->JournalCreate($credit,$debit,$net_value,$date,$remarks,$work_flow,$user_id,$Journals_no,'',$day_register_id,$executive,'',$route_id,'',$invoice_no);
					if($Accountings_function_return['result']!='Success')
						throw new Exception($Accountings_function_return['message']);
					if($stock_value)
					{
						$credit=$sale_return_account_head_id;
						$debit=$stock_account_head_id;

						$Accountings_function_return=$this->JournalCreate($credit,$debit,$stock_value,$date,$remarks,$work_flow,$user_id,$Journals_no,'',$day_register_id,$executive,'',$route_id,'',$invoice_no);
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}
					if(floatval($tax_amount))
					{
						$credit=$data[$j]['customer_id'];
						$debit=$tax_on_sale_return_account_head_id;
						$Accountings_function_return=$this->JournalCreate($credit,$debit,$tax_amount,$date,$remarks,$work_flow,$user_id,$Journals_no,'',$day_register_id,$executive,'',$route_id,'',$invoice_no);
						if($Accountings_function_return['result']!='Success')
							throw new Exception($Accountings_function_return['message']);
					}
					$credit = $this->Route->field(
						'Route.account_head_id',
						array('id ' => $route_id));
					$debit=$data[$j]['customer_id'];
					// if($Accountings_function_return['result']!='Success')
					// 	throw new Exception($Stock_function_return['message']);
					$SalesReturnItem = $this->SalesReturnItem->find('all',array(
						'conditions' => array(
							'SalesReturnItem.sales_return_id' => $sales_return_id,
							),
						'fields' => array(
							'SalesReturnItem.product_id',
							'SalesReturnItem.quantity',
							'SalesReturnItem.unit_price',
							'SalesReturn.invoice_no',
							'SalesReturnItem.warehouse_id',
							)
						));
					$datasource_Stock->begin();
					foreach ($SalesReturnItem as $key => $value) {
						$quantity = $value['SalesReturnItem']['quantity'];
						$unit_price = $value['SalesReturnItem']['unit_price'];
						$product_id=$value['SalesReturnItem']['product_id'];
						$warehouse_id=$value['SalesReturnItem']['warehouse_id'];
						$executive_warehouse=$this->Executive->findById($this->request->data['executive_id']);
						$Stock = $this->Stock->find('first',array(
							'conditions'=>array(
								'Stock.product_id'=>$product_id,
								'Stock.warehouse_id'=>$warehouse_id,
								)
							));
                            if(!$Stock){
							$remark='SalesReturn --'.$value['SalesReturn']['invoice_no'].'('.$quantity.')';
							$flag=1;
							$StockController = new StockController;
							$Stock_function_return=$StockController->GeneralStock_Add_Function($warehouse_id,$product_id,$quantity,date('Y-m-d'),$user_id,$remark,$flag);
							if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result'], 1);
							}else{
							$stock_id=$Stock['Stock']['id'];
							$stock_damaged_quantity=$Stock['Stock']['quantity'];
							$date=date('Y-m-d');
							$StockController = new StockController;
							$remark='SalesReturn --'.$value['SalesReturn']['invoice_no'].'('.$quantity.')';
							$stock_damaged_quantity+=$quantity;
							$Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_damaged_quantity,$date,$user_id,$remark);
							if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
							}
					}
					$datasource_Stock->commit();
				}
				$return['status']='Success';

				$datasource_SalesReturn->commit();
				$datasource_SalesReturnItem->commit();
				$datasource_ProductBonusDetail->commit();
				$datasource_ExecutiveBonusCalculation->commit();
				$datasource_ExecutiveBonus->commit();
				$datasource_Journal->commit();
			}
			catch (Exception $e)
			{
				$datasource_SalesReturn->rollback();
				$datasource_SalesReturnItem->rollback();
				$datasource_ProductBonusDetail->rollback();
				$datasource_ExecutiveBonusCalculation->rollback();
				$datasource_ExecutiveBonus->rollback();
				$datasource_Stock->rollback();
				$datasource_Journal->rollback();
				$return['status']=$e->getMessage();
			}
		}

		echo json_encode($return);
		exit;

	}
	public function api_contra_voucher($id=NULL) {
		$return['result']='Error';
		if($this->request->data){
			$total_approved_amount=0;
			$total_amount=0;
			$count=count($this->request->data['ContraVoucher']);
			$data=$this->request->data['ContraVoucher'];
			$day_register_id=$this->request->data['day_register_id'];
			$executive_id=$this->request->data['executive_id'];
			$route_id=$this->request->data['route_id'];
			$cashhead = $this->Route->field(
				'Route.account_head_id',
				array('id ' => $route_id));
			$datasource_Journal = $this->Journal->getDataSource();
			try {
				$datasource_Journal->begin();
				for ($i=0; $i < $count ; $i++) {
					$credit=$cashhead;
					$debit=$data[$i]['to'];
					$amount=$data[$i]['amount'];
					$remarks=$data[$i]['remarks'];
					$date=$data[$i]['date'];
					$voucher_no='';
					$externalvoucher=$data[$i]['externalvoucher'];
					$work_flow='Voucher Contra';
					$user_id=1;
							//pr($externalvoucher);
							//exit;
					$function_return=$this->JournalCreate($credit,$debit,$amount,$date,$remarks,$work_flow,$user_id,'','',$day_register_id,$executive_id,'',$route_id,$externalvoucher);
					if($function_return['result']!='Success')
						throw new Exception($function_return['message'], 1);
				}
				$datasource_Journal->commit();
				$return['result']='Success';
			} catch (Exception $e) {
				$return['result']='Error';
				$return['message']=$e->getMessage();
			}
		}
		echo json_encode($return);
		exit;
	}
	/****************Stock All********************/
	public function api_get_van_stock_search_all()
	{
		$return=[
		'status'=>'Empty',
		'Stocks'=>[],
		'total_stock'=>'0',
		'total_quantity'=>'0',
		];

		$data=$this->request->data;
//$data['executive_id']=1;
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
			$warehouse_id = $this->Executive->field(
				'Executive.warehouse_id',
				array('Executive.id ' => $executive));
			$conditions['Stock.flag'] = 1;
			//$conditions['Stock.warehouse_id'] = $warehouse_id;
		}
		else{
			$return[ 'status' ]= 'Executive Required';
		}
		if(isset($executive))
		{
			if(isset($data['product_type_id'])){
				$conditions['Product.product_type_id'] = $data['product_type_id'];
			}
			if(isset($data['brand_id'])){
				$conditions['Product.brand_id'] = $data['brand_id'];
			}
			$conditions['Product.type'] = [2,5,8];
			$conditions['Product.active'] =1;
			$this->Product->unbindModel(array('hasMany' => array('SaleItem','SalesReturnItem','PurchasedItem','PurchaseReturnItem','StockLog','Stock','UnwantedList')));
			$Stock=$this->Product->find('all',array(
				"joins"=>array(
					array(
						"table"=>'stocks',
						"alias"=>'Stock',
						"type"=>'inner',
						"conditions"=>array('Product.id=Stock.product_id'),
						),
					array(
						"table"=>'warehouses',
						"alias"=>'Warehouse',
						"type"=>'inner',
						"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
						),
					array(
						"table"=>'units',
						"alias"=>'SubUnit',
						"type"=>'left',
						"conditions"=>array('Unit.id=SubUnit.sub_unit_id'),
						),
					),
				'order'=>array('Product.id'),
				'conditions'=>$conditions,
				//'limit'=>500,
				'fields'=>array(
					'Product.*',
					'Unit.name',
					'ProductType.name',
					'Warehouse.name',
					'Brand.name',
					'Stock.*',
					'Unit.id',
					'Unit.name',
					'Unit.sub_unit_id',
					)
				));

			$All_Stock=[];
			$total_cost = 0;
			$total_quantity = 0;
			foreach($Stock as $value){
					$Single_stock['product_id']=$value['Product']['id'];
					$Single_stock['product_name']=$value['Product']['name'];
					$Single_stock['hsn_code']=$value['Product']['hsn_code'];
					$Single_stock['arabic_name']="";
					if($value['Product']['arabic_name']){
						$Single_stock['arabic_name']=$value['Product']['arabic_name'];
					}
					$Single_stock['product_code']=$value['Product']['code'];
					$Single_stock['priority']=$value['Product']['priority'];
					$Single_stock['product_bonus_percentage']=$value['Product']['bonus_percentage'];
					$Single_stock['product_barcode']=$value['Product']['barcode'];
					$Single_stock['product_type_name']=$value['ProductType']['name'];
					$Single_stock['type']=$value['Product']['type'];
					$Single_stock['tray_capacity']=$value['Product']['tray_occupation'];
					$Single_stock['production_days']=$value['Product']['production_days'];
					$Unit=[];
					$this->Unit->unbindModel(
						array('hasMany' => array(
							'Product',
							))
						);
					$Units_list=[];
					$Unit['Unit'][$value['Unit']['id']]=$value['Unit']['name'];
					if($value['Unit']['sub_unit_id']!=0)
					{
						$SubUnit=$this->Unit->find('first',array(
							'conditions'=>array(
//'Unit.conversion >1',
								'Unit.id'=>$value['Unit']['sub_unit_id']
								),
							'fields'=>array('Unit.id','Unit.name'),
							));
						$Unit['Unit'][$SubUnit['Unit']['id']]=$SubUnit['Unit']['name'];
					}
					foreach ($Unit['Unit'] as $key_unit => $value_unit) {
						$Single_unit['id']=$key_unit;
						$Single_unit['name']=$value_unit;
						array_push($Units_list, $Single_unit);
					}
					$Single_stock['Units_list']=$Units_list;
					$Single_stock['brand_name']=$value['Brand']['name'];
					$Single_stock['product_cost']=$value['Product']['cost'];
					$Single_stock['product_mrp']=$value['Product']['mrp'];
					$Single_stock['product_wholesale_price']=$value['Product']['wholesale_price'];
					$Single_stock['product_vat']=$value['Product']['tax'];
					$Single_stock['piece_per_cart']=$value['Product']['no_of_piece_per_unit'];
					$Single_stock['stock_quantity']=$value['Stock']['quantity'];
					$total_cost+=$value['Product']['cost']*$value['Stock']['quantity'];
					$total_quantity+=$value['Stock']['quantity'];
					array_push($All_Stock, $Single_stock);
			}
//pr($Stock);exit;
			if($Stock)
			{
				$return[ 'status' ]='success';
				$return[ 'Stocks' ]=$All_Stock;
				$return[ 'total_stock' ]=$total_cost;
				$return[ 'total_quantity' ]=$total_quantity;
			}
			else{
				$return[ 'status' ]='No Stock';
			}
		}
		echo json_encode($return);
		exit;
	}
	public function api_get_cashinhand()
	{

		$data=$this->request->data;

		if(isset($data['route_id']))
		{
			$route_id=$data['route_id'];


		}

		$array=[];
		$value=0;

		$debit=$this->Route->find('first',array('conditions'=>array('id'=>$route_id),'fields'=>array('account_head_id')));
		
		if(!$debit){

			$array['cash_in_hand']=$value;
			
		}
		else{
			$debit=$debit['Route']['account_head_id'];

		}
		$AccountHeads=$this->AccountHead->find('first',array(
			'fields'=>[
			'AccountHead.id',
			'AccountHead.name',
			'AccountHead.created_at',
			'AccountHead.opening_balance',
			],
			'conditions'=>['AccountHead.id'=>$debit],
			));
//pr($AccountHeads);exit;
		$Journal=$this->Journal->find('all',array('conditions'=>array(
			'route_id'=>$route_id,
			'flag'=>'1',
			'debit'=>$debit

			)));
		$Journalcredit=$this->Journal->find('all',array('conditions'=>array(

			'credit'=>$debit,
			'flag'=>'1'

			)));
	//		pr($Journal);exit;
	// pr($datas);exit;

		$Data=[];
		$data=0;
		$Datas=[];
//pr($debit);
		$datas=0;
		foreach($Journal as $key=>$value){

			$Data=$value['Journal']['amount'];
			$data+=$Data;
		}
//pr($data);exit;
		foreach($Journalcredit as $key=>$value2){

			$Datas=$value2['Journal']['amount'];
			$datas+=$Datas;
		}

		$value=($data-$datas);
		$array['cash_in_hand']=number_format($value,3,'.','');
		echo json_encode($array);
		exit;
		
	}

	public function SystemParameter()
	{
		$return[ 'currency' ]=$this->SystemParameter->field('value',array('id'=>4));
		$return[ 'mode' ]=$this->SystemParameter->field('value',array('id'=>2));
		$piece=$this->SystemParameter->field('value',array('id'=>1));

		if($piece=='yes'){
			$return['casepiece']='piece';
		}else{
			$return['casepiece']='case';
		}
		echo json_encode($return);
		exit;

	}
	public function customer_production_order()
	{
		$user_id=1;
		$return=[
		'status'=>'empty',
		];
		$data=$this->request->data['Order'];

		if($data)
		{
			$date = date('Y-m-d');
			$executive=$this->request->data['executive_id'];
			$route_id=$this->request->data['route_id'];
			if(isset($this->request->data['day_register_id']))
			{
				$day_register_id=$this->request->data['day_register_id'];   
			}
			else
			{
				$DayRegister=$this->DayRegister->find('first',[
					'conditions'=>['DayRegister.executive_id'=>$executive],
					'order'=>['DayRegister.id DESC']]);
				$day_register_id=$DayRegister['DayRegister']['id'];
			}
			$DayRegister=$this->DayRegister->findByExecutiveIdAndStatus($executive,'0');
			if(!$DayRegister){
				$return['status']='Day Closed';
			}
			else
			{
				$datasource_Order = $this->Order->getDataSource();
				$datasource_OrderItem = $this->OrderItem->getDataSource();

				try {
                     $datasource_Order->begin();
				     $datasource_OrderItem->begin();
					for ($j=0; $j <count($data) ; $j++) {
						$Order=$this->Order->find('first',array('fields' => array('MAX(Order.order_no) as order_no')));
						if(!empty($Order))
						{
							$order_no=$Order[0]['order_no']+1;
						}
						else
						{
							$order_no=1;
						}
						$order_no_check=$this->Order->findByOrderNo($order_no);
						while ($order_no_check) {
							$order_no+=1;
							$order_no_check=$this->Order->findByOrderNo($order_no);        
						}
						$order_data=[
						'executive_id'=>$executive,
						'route_id'=>$route_id,
						'day_register_id'=>$day_register_id,
						'account_head_id'=>$data[$j]['customer_id'],
						'order_no'=>$order_no,
						'delivery_days'=>$data[$j]['production_days'],
						'date'=>date('Y-m-d',strtotime(($data[$j]['order_date']))),
						'date_of_delivered'=>date('Y-m-d',strtotime(($data[$j]['order_date']. $data[$j]['production_days'].'days'))),
						'date_of_production'=>date('Y-m-d',strtotime(($data[$j]['order_date']. ' + 1 days'))),
						'created_by'=>$user_id,
						'modified_by'=>$user_id,
						'created_at'=>date('Y-m-d',strtotime(($data[$j]['order_date']))),
						'updated_at'=>date('Y-m-d',strtotime(($data[$j]['order_date']))),
						];
						
						$this->Order->create();
						if(!$this->Order->save($order_data))
						{
							$errors = $this->Order->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}
						$ordered_products=$data[$j]['order_products'];
						$Order_id=$this->Order->getLastInsertId();

						for ($i=0; $i <count($ordered_products) ; $i++) {
							$items=[
							'product_id'=>$ordered_products[$i]['product_id'],
							'order_id'=>$Order_id,
							'quantity'=>$ordered_products[$i]['product_quantity'],
							];
							$this->OrderItem->create();
							if(!$this->OrderItem->save($items))
							{
								$errors = $this->OrderItem->validationErrors;
								foreach ($errors as $key => $value) {
									throw new Exception($value[0], 1);
								}
							}
						}

					}
					$datasource_Order->commit();
					$datasource_OrderItem->commit();
					$return['status']='success';
				} catch (Exception $e) {
					$datasource_OrderItem->rollback();
					$datasource_Order->rollback();
					$return['result']=$e->getMessage();
				}
			}
		}

	echo json_encode($return);
	exit;
}
public function api_get_warehouse_list()
	{
		$return=[
		'status'=>'Empty',
		'warehouse'=>[],
		];
		$warehouses=$this->Warehouse->find('list',array(
			'order'=>array('name ASC'),
			'fields'=>array('Warehouse.id','Warehouse.name'),
			));
		if($warehouses)
		{
			$return[ 'status' ]='success';
			$All_warehouse=[];
			foreach ($warehouses as $key => $value) {
				$Single['id']=$key;
				$Single['name']=$value;
				array_push($All_warehouse, $Single);
			}
			$return[ 'warehouse' ]=$All_warehouse;
		}

		echo json_encode($return);
		exit;
	}
	public function api_get_van_warehouse_list()
	{
		$return=[
		'status'=>'Empty',
		'warehouse'=>[],
		];
		$this->Warehouse->unbindModel(
			array('hasMany' => array(
				'Executive',
				'StockLog',
				'Stock',
				))
			);
	$warehouses = $this->Warehouse->find('list', array(
		"joins"=>array(array(
			"table"=>'executives',
			"alias"=>'Executive',
			"type"=>'inner',
			"conditions"=>array('Executive.warehouse_id=Warehouse.id'),
			),
		),
		'order'=>array('Warehouse.name ASC'),
		'fields' => array(
			'Warehouse.id',
			'Warehouse.name',
			)
		)
	);
		if($warehouses)
		{
			$return[ 'status' ]='success';
			$All_warehouse=[];
			foreach ($warehouses as $key => $value) {
				$Single['id']=$key;
				$Single['name']=$value;
				array_push($All_warehouse, $Single);
			}
			$return[ 'warehouse' ]=$All_warehouse;
		}

		echo json_encode($return);
		exit;
	}
	public function api_get_production_days_list()
	{
		$return=[
		'status'=>'Empty',
		'production_days'=>[],
		];
		$product=$this->Product->find('list',array(
			'order'=>array('name ASC'),
			'fields'=>array('Product.production_days','Product.production_days'),
			));
		if($product)
		{
			$return[ 'status' ]='success';
			$All_production_days=[];
			foreach ($product as $key => $value) {
				$Single['id']=$key;
				$Single['name']=$value;
				array_push($All_production_days, $Single);
			}
			$return[ 'production_days' ]=$All_production_days;
		}
		echo json_encode($return);
		exit;
	}
	public function api_get_last_sale_rate()
	{
		$return=[
		'status'=>'Empty',
		'product_wholesale_price'=>[0],
		];
		$conditions=[];
		$conditions['SaleItem.product_id']=$this->request->data['product_id'];
		$conditions['Sale.account_head_id']=$this->request->data['customer_id'];
		$SaleItem=$this->SaleItem->find('first',array(
		"joins" => array(
		),
		'conditions'=>$conditions,
		'order' => array('Sale.id DESC'),
		'fields'=>array(
		'SaleItem.unit_price',
		)
		));
		if(!empty($SaleItem)){
		$lastunitcost=round($SaleItem['SaleItem']['unit_price'],2);
		}
		else
		{
		$product_id=$this->request->data['product_id'];
		$this->Product->unbindModel(array('hasMany' => array('Stock','StockLog','SaleItem','SalesReturnItem','PurchasedItem','PurchaseReturnItem')));
		$Product=$this->Product->find('first',array(
		'conditions'=>array('Product.id'=>$product_id)
		));
		$lastunitcost=round($Product['Product']['wholesale_price'],2);
        }
        if($lastunitcost)
        {
        $return['status' ]='success';
        $return['product_wholesale_price' ]=$lastunitcost;	
        }
		echo json_encode($return);
		exit;
	}
	public function api_get_order_history()
	{
		$return=[
		'status'=>'Empty',
		'Order'=>[],
		];
		$conditions=[];
		$conditions['Order.flag']=1;
		$conditions['Order.date_of_delivered']=date('Y-m-d',strtotime($this->request->data['date']));
		$conditions['Order.executive_id']=$this->request->data['executive_id'];
        $this->Product->unbindModel(array('belongsTo'=>array('ProductType','Brand','Unit'),'hasMany' => array('PurchaseReturnItem','UnwantedList','Stock','StockLog','SalesReturnItem','SaleItem','PurchasedItem','Unit','Brand')));
				$Product_list=$this->Product->find('all',array('conditions'=>['Product.active=1','type'=>2],
					'fields'=>['Product.priority','Product.id','Product.name']));
				$orderitem=[];
				if(!empty($Product_list)){
					$return[ 'status' ]='success';
					foreach ($Product_list as $key => $value) {
						$conditions['OrderItem.product_id']=$value['Product']['id'];
						$quantity=0;
						$this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
						$OrderItem=$this->OrderItem->find('first',array(
							'conditions'=>$conditions,
							'fields'=>array('OrderItem.total_quantity'),
							));
						if(!empty($OrderItem['OrderItem']['total_quantity']))
						{
							$single['Product_name']=$value['Product']['name'];
							$single['Product_id']=$value['Product']['id'];
							$single['Product_quantity']=$OrderItem['OrderItem']['total_quantity'];
							array_push($orderitem,$single);
						}
					}
				$return[ 'Order' ]=$orderitem;
				}

		echo json_encode($return);
		exit;
	}
	public function api_van_van_stock_transfer()
	{

		$return=[
		'status'=>'Empty',
		];
		$user_id=1;
		$datasource_StockTransfer = $this->StockTransfer->getDataSource();
			$datasource_StockTransferItem = $this->StockTransferItem->getDataSource();
			$datasource_Stock = $this->Stock->getDataSource();
		try {
			$datasource_StockTransfer->begin();
				$datasource_StockTransferItem->begin();
				$datasource_Stock->begin();
		$data=$this->request->data;
		if($data)
		{
			$$user_id=1;
			$Transfer=$this->StockTransfer->find('first',array('order' => 'StockTransfer.id DESC',));
			if(!empty($Transfer))
			{
				$TransferNo = $Transfer['StockTransfer']['transfer_no'];
				$transfer_no=$TransferNo+1;
			}
			else
			{
				$transfer_no=1;
			}
			$Executive=$this->Executive->findById($data['executive_id']);
	         $type=$data['stock_type'];
           $warehouse_to=$data['to_warehouse'];
           $warehouse_from= $Executive['Executive']['warehouse_id'];
			$StockTransfer_data=[
			'transfer_no'=>$transfer_no,
			'day_register_id'=>$data['day_register_id'],
			'date'=>date('Y-m-d'),
			'warehouse_from'=>$warehouse_from,
			'warehouse_to'=>$warehouse_to,
			'remarks'=>"",
			'status'=>1,
			'transfer_type'=>4,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_at'=>date('Y-m-d H:i:s'),
			];
			$this->StockTransfer->create();
			if(!$this->StockTransfer->save($StockTransfer_data))
			{
				$errors = $this->StockTransfer->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$stock_transfer_id=$this->StockTransfer->getLastInsertId();
			$item_list = $data['product_list'];
			foreach ($item_list as $key => $value) 
			{
				$StockTransferItem_data=[
				'product_id'=>$value['id'],
				'stock_transfer_id'=>$stock_transfer_id,
				'quantity'=>$value['movementQty'],
				'unit_id'=>1,
				];
				$this->StockTransferItem->create();
				if(!$this->StockTransferItem->save($StockTransferItem_data))
				{
					$errors = $this->StockTransferItem->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				else
				{}
			}
			
			$return['status']='Success';
		}
	            $datasource_Stock->commit();
				$datasource_StockTransfer->commit();
				$datasource_StockTransferItem->commit();
		} catch (Exception $e){
			$datasource_StockTransfer->rollback();
				$datasource_StockTransferItem->rollback();
				$datasource_Stock->rollback();
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;		
	}
	public function api_van_branch_stock_transfer()
	{

		$return=[
		'status'=>'Empty',
		];
		$user_id=1;
		$datasource_StockTransfer = $this->StockTransfer->getDataSource();
			$datasource_StockTransferItem = $this->StockTransferItem->getDataSource();
			$datasource_Stock = $this->Stock->getDataSource();
		try {
			$datasource_StockTransfer->begin();
				$datasource_StockTransferItem->begin();
				$datasource_Stock->begin();
		$data=$this->request->data;
		if($data)
		{
			$$user_id=1;
			$Transfer=$this->StockTransfer->find('first',array('order' => 'StockTransfer.id DESC',));
			if(!empty($Transfer))
			{
				$TransferNo = $Transfer['StockTransfer']['transfer_no'];
				$transfer_no=$TransferNo+1;
			}
			else
			{
				$transfer_no=1;
			}
			$Executive=$this->Executive->findById($data['executive_id']);
	         $type=$data['stock_type'];
			 if($type=="Damage")
            {
            $warehouse_to= $this->Warehouse->field('Warehouse.id',array('Warehouse.warehouse_id ' =>1));
            if(empty($warehouse_to))
				throw new Exception("Damage Warehouse Required", 1);
			$warehouse_from= $this->Warehouse->field('Warehouse.id',array('Warehouse.warehouse_id ' =>$Executive['Executive']['warehouse_id']));
			if(empty($warehouse_from))
			throw new Exception("Damage Warehouse Required", 1);
            }
            else
            {
           $warehouse_to=1;
           $warehouse_from= $Executive['Executive']['warehouse_id'];
            }
			$StockTransfer_data=[
			'transfer_no'=>$transfer_no,
			'day_register_id'=>$data['day_register_id'],
			'date'=>date('Y-m-d'),
			'warehouse_from'=>$warehouse_from,
			'warehouse_to'=>$warehouse_to,
			'remarks'=>"",
			'status'=>1,
			'transfer_type'=>3,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_at'=>date('Y-m-d H:i:s'),
			];
			$this->StockTransfer->create();
			if(!$this->StockTransfer->save($StockTransfer_data))
			{
				$errors = $this->StockTransfer->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$stock_transfer_id=$this->StockTransfer->getLastInsertId();
			$item_list = $data['product_list'];
			foreach ($item_list as $key => $value) 
			{
				$StockTransferItem_data=[
				'product_id'=>$value['id'],
				'stock_transfer_id'=>$stock_transfer_id,
				'quantity'=>$value['movementQty'],
				'unit_id'=>1,
				];
				$this->StockTransferItem->create();
				if(!$this->StockTransferItem->save($StockTransferItem_data))
				{
					$errors = $this->StockTransferItem->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				else
				{}
			}
			
			$return['status']='Success';
		}
	            $datasource_Stock->commit();
				$datasource_StockTransfer->commit();
				$datasource_StockTransferItem->commit();
		} catch (Exception $e){
			$datasource_StockTransfer->rollback();
				$datasource_StockTransferItem->rollback();
				$datasource_Stock->rollback();
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;		
	}
public function api_save_expense($id=NULL) {
		$return['result']='Error';
		if($this->request->data){
			$total_approved_amount=0;
			$total_amount=0;
			$data=$this->request->data;
			$day_register_id=$this->request->data['day_register_id'];
			$executive_id=$this->request->data['executive_id'];
			$route_id=$this->request->data['route_id'];
			$route_account_head_id=$this->Route->findById($this->request->data['route_id']);
			$daily_expense=$this->Executive->field('daily_expense',array('Executive.id'=>$executive_id));
				$datasource_ExecutiveExpenseDetail = $this->ExecutiveExpenseDetail->getDataSource();
				$datasource_Journal = $this->Journal->getDataSource();
			try {
				$datasource_ExecutiveExpenseDetail->begin();
				$datasource_Journal->begin();
					$credit=$route_account_head_id['Route']['account_head_id'];
					$debit=$data['expense_id'];
					$amount=$data['amount'];
					$remarks=$data['remarks'];
					$date=date('Y-m-d');
					$receipt_no="";
					$work_flow='Expense';
					$user_id=1;
						$this->ExecutiveExpenseDetail->virtualFields = array('total_expense'=>"SUM(ExecutiveExpenseDetail.amount)");
					$ExecutiveExpenseDetail=$this->ExecutiveExpenseDetail->find('first',array(
			'conditions'=>array('ExecutiveExpenseDetail.executive_id'=>$executive_id,'ExecutiveExpenseDetail.date'=>$date),
			'fields'=>array('ExecutiveExpenseDetail.total_expense'),
			));        $total_expense=0;
					if($ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense'])
					{
						$total_expense=$ExecutiveExpenseDetail['ExecutiveExpenseDetail']['total_expense']+$data['amount'];
					}
					$ExecutiveExpenseDetail_data=[
						'executive_id'=>$executive_id,
						'day_register_id'=>$day_register_id,
						'route_id'=>$route_id,
						'expense_id'=>$data['expense_id'],
						'remarks'=>$data['remarks'],
						'amount'=>$data['amount'],
						'date'=>date('Y-m-d'),
						'approved_amount'=>$data['amount'],
						];
						if($daily_expense < $total_expense )
						{
						$ExecutiveExpenseDetail_data['status']=1;
						}
						else
						{
						$ExecutiveExpenseDetail_data['status']=2;
							$credit=$route_account_head_id['Route']['account_head_id'];
							$debit=$data['expense_id'];
							$amount=$data['amount'];
							$remarks=$data['remarks'];
							$work_flow='ExpenseApproval';
							$user_id=1;
							$AccountingsController = new AccountingsController;
						   $function_return=$AccountingsController->JournalCreate($credit,$debit,$amount,date('d-m-Y'),$remarks,$work_flow,$user_id,'','','','',$executive_id,$route_id);
							if($function_return['result']!='Success')
								throw new Exception($function_return['message'], 1);	
						}
						$this->ExecutiveExpenseDetail->create();
						if(!$this->ExecutiveExpenseDetail->save($ExecutiveExpenseDetail_data))
						{
							$errors = $this->ExecutiveExpenseDetail->validationErrors;
							foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
						}
				$datasource_ExecutiveExpenseDetail->commit();
				$datasource_Journal->commit();
				$return['result']='Success';
			} catch (Exception $e) {
			$datasource_ExecutiveExpenseDetail->rollback();
			$datasource_Journal->rollback();
				$return['result']='Error';
				$return['message']=$e->getMessage();
			}
		}
		echo json_encode($return);
		exit;
	}
	public function api_get_expense_list()
	{
		$return=[
		'status'=>'Empty',
		'expense_list'=>[],
		];
			$data=$this->request->data;
			$day_register_id=$this->request->data['day_register_id'];
			$executive_id=$this->request->data['executive_id'];
		    $route_id=$this->request->data['route_id'];
			$route_account_head_id=$this->Route->findById($this->request->data['route_id']);
			$credit=$route_account_head_id['Route']['account_head_id'];
		   $ExecutiveExpenseDetail=$this->ExecutiveExpenseDetail->find('all',array(
			'order'=>array('ExecutiveExpenseDetail.id DESC'),
			'conditions'=>array('ExecutiveExpenseDetail.executive_id'=>$executive_id),
			'fields'=>array('ExecutiveExpenseDetail.*','AccountHead.name'),
			));
		if($ExecutiveExpenseDetail)
		{
			$return[ 'status' ]='success';
			$All_expense=[];
			foreach ($ExecutiveExpenseDetail as $key => $value) {
				$Single['date']=date('d-m-Y',strtotime($value['ExecutiveExpenseDetail']['date']));
				$Single['expense_head']=$value['AccountHead']['name'];
				$Single['remarks']=$value['ExecutiveExpenseDetail']['remarks'];
				$Single['amount']=$value['ExecutiveExpenseDetail']['approved_amount'];
				if($value['ExecutiveExpenseDetail']['status']==1)
				{
					$Single['status']="Pending";
				}
				elseif($value['ExecutiveExpenseDetail']['status']==2)
				{
					$Single['status']="Approved";
				}
				else
				{
					$Single['status']="Rejected";
				}
				array_push($All_expense, $Single);
			}
			$return[ 'expense_list' ]=$All_expense;
		}

		echo json_encode($return);
		exit;
	}
	public function api_daily_van_transfer()
	{
		$data=$this->request->data;
		$conditions=[];
		$return=array();
		$return['status']='empty';
		if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];			
			$Executive=$this->Executive->findById($executive);
			$warehouse_id=$Executive['Executive']['warehouse_id'];
			$conditions['StockTransfer.warehouse_to']=$warehouse_id;
			$conditions['StockTransfer.transfer_type']=2;
			$conditions['StockTransfer.status']=1;
			$conditions['StockTransfer.flag']=1;

		}
		$StockTransfer=$this->StockTransfer->find('all',array(
			"joins"=>array(
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseFrom',
					"type"=>'inner',
					"conditions"=>array('WarehouseFrom.id=StockTransfer.warehouse_from'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseTo',
					"type"=>'inner',
					"conditions"=>array('WarehouseTo.id=StockTransfer.warehouse_to'),
					),
				),
			'order'=>array('StockTransfer.id DESC'),
			'conditions'=>$conditions,
			"fields"=>array(
				'StockTransfer.id',
				'StockTransfer.transfer_no',
				'StockTransfer.date',
				'StockTransfer.remarks',
				'StockTransfer.status',
				'WarehouseFrom.name',
				'WarehouseTo.name',
				),
			));
		$return=[];
		$return['status']='Empty';
		$Transfer=[];
		if($StockTransfer){
			foreach($StockTransfer as $key => $value)
			{
				$single['id']=$value['StockTransfer']['id'];
				$single['transfer_no']=$value['StockTransfer']['transfer_no'];
				$single['date']=$value['StockTransfer']['date'];
				$single['WarehouseFrom']=$value['WarehouseFrom']['name'];
				$single['WarehouseTo']=$value['WarehouseTo']['name'];
				$single['remarks']=$value['StockTransfer']['remarks'];
				$Transfer[]=$single;
			}
			$return['status']='Success';
			$return['transfer_list']=$Transfer;
		}
		echo json_encode($return);exit;
	}
	public function api_daily_van_transfer_items()
	{
		$data=$this->request->data;
		$conditions=[];
		$return['status']='empty';
		if(isset($data['transfer_id']))
		{
			$conditions['StockTransfer.transfer_type']=2;
			$conditions['StockTransfer.status']=1;
			$conditions['StockTransferItem.stock_transfer_id']=$this->StockTransfer->field('StockTransfer.id',array('transfer_no'=>$data['transfer_id']));
		}
		$StockTransferItem=$this->StockTransferItem->find('all',array('conditions'=>$conditions));
		$return=[];
		$return['status']='Empty';
		$Transfer=[];
		if($StockTransferItem){
			foreach($StockTransferItem as $key => $value)
			{
				$single['item_id']=$value['StockTransferItem']['id'];
				$single['product_id']=$value['StockTransferItem']['product_id'];
				$single['transfer_id']=$value['StockTransferItem']['stock_transfer_id'];
				$single['quantity']=$value['StockTransferItem']['quantity'];
				$single['Product_name']=$value['Product']['name'];
				$single['Product_code']=$value['Product']['code'];
				$Transfer[]=$single;
			}
			$return['status']='Success';
			$return['transfer_item_list']=$Transfer;
		}
		echo json_encode($return);exit;
	}
	public function api_daily_van_transfer_items_approve()
	{
		$return=[
		'status'=>'Empty',
		];
		$user_id=1;
		$data=$this->request->data;
		if($data)
		{
			$transfer_id=$data['transfer_id'];
			$this->StockTransfer->id=$this->StockTransfer->field('StockTransfer.id',array('transfer_no'=>$transfer_id));;
			if($data['status']=="approve")
			{
				$approved_data=[
				'status'=>2,
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				if(!$this->StockTransfer->save($approved_data))
				{
					$errors = $this->StockTransfer->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$item_list = $data['item_list'];
				foreach ($item_list as $key => $value) 
				{
					$quantity=$value['quantity'];
					$item=$this->StockTransferItem->findById($value['id']);
					$this->StockTransferItem->id=$value['id'];
					if(!$this->StockTransferItem->saveField('quantity',$quantity))
						throw new Exception("Error Processing Request in quantity Updation", 1);
					$toStock=$this->Stock->find('first',array(
						'conditions'=>array(
							'Stock.product_id'=>$item['StockTransferItem']['product_id'],
							'Stock.warehouse_id'=>$item['StockTransfer']['warehouse_to']
							),
						'fields'=>array('Stock.id','Stock.quantity')));
				
					if(!$toStock)
					{
					$remark='Stock Transfer --'.$transfer_id.'('.$quantity.')';	
					$flag=1;
					$StockController = new StockController;
					$Stock_function_return=$StockController->GeneralStock_Add_Function($item['StockTransfer']['warehouse_to'],$item['StockTransferItem']['product_id'],$quantity,date('Y-m-d'),$user_id,$remark,$flag);
					if($Stock_function_return['result']!='Success')
					throw new Exception($Stock_function_return['result'], 1);
					}
					else
					{
					$toQty=$toStock['Stock']['quantity']+$quantity;
					$remark='Stock Transfer --'.$transfer_id.'('.$quantity.')';	
					$StockController = new StockController;
					$Stock_function_return=$StockController->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
					if($Stock_function_return['result']!='Success')
					throw new Exception($Stock_function_return['result']);
					}


				}
			}
			else
			{

				$approved_data=[
				'status'=>2,
				'flag'=>0,
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				if(!$this->StockTransfer->save($approved_data))
				{
					$errors = $this->StockTransfer->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				else
				{
					$item_list = $data['item_list'];
					foreach ($item_list as $key => $value) 
					{
						$quantity=$value['quantity'];
						$item=$this->StockTransferItem->findById($value['id']);
						$toStock=$this->Stock->find('first',array(
							'conditions'=>array(
								'Stock.product_id'=>$item['StockTransferItem']['product_id'],
								'Stock.warehouse_id'=>$item['StockTransfer']['warehouse_from']
								),
							'fields'=>array('Stock.id','Stock.quantity')));
						$toQty=$toStock['Stock']['quantity']+$quantity;
						$remark='Stock Transfer --'.$transfer_id.'('.$quantity.')';		
						$StockController = new StockController;
						$Stock_function_return=$StockController->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
					}
				}

			}
			$return['status']='Success';
		}
		echo json_encode($return);
		exit;

	}
	public  function api_denomination_save()
	{
		$return['result']="empty";
		$datasource_ExecutiveDenomination = $this->ExecutiveDenomination->getDataSource();
			try {
				$datasource_ExecutiveDenomination->begin();
				$data=$this->request->data;
			   $list = $data['Denomination'];
			   for ($i=0; $i < count($list) ; $i++)
				{
					$Save_Data = [
							'date' => date('Y-m-d'),
							'executive_id' =>$data['executive_id'],
							'day_register_id' => $data['day_register_id'],
							'denomination' => $list[$i]['denominator'],
							'total_each'=>$list[$i]['total_each'],
							'count' => $list[$i]['qty'],
							];
					$this->ExecutiveDenomination->create();
					if(!$this->ExecutiveDenomination->save($Save_Data))
					{
						$errors = $this->ExecutiveDenomination->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
						$return['result']='Success';
			$datasource_ExecutiveDenomination->commit();
				}
			} catch (Exception $e) {
				$datasource_ExecutiveDenomination->rollback();
				$return['result']=$e->getMessage();
			}
			echo json_encode($return);exit;
	}
	public function api_get_vehicle_list()
	{
		$return=[
		'status'=>'Empty',
		'vehicle'=>[],
		];
		$VehicleNo=$this->VehicleNo->find('list',array(
			'order'=>array('id ASC'),
			'fields'=>array('VehicleNo.id','VehicleNo.number'),
			));
		if($VehicleNo)
		{
			$return[ 'status' ]='success';
			$All_Vehicle=[];
			foreach ($VehicleNo as $key => $value) {
				$Single['id']=$key;
				$Single['VehicleNo']=$value;
				array_push($All_Vehicle, $Single);
			}
			$return['vehicle']=$All_Vehicle;
		}

		echo json_encode($return);
		exit;
	}
	public function api_get_state_list()
	{
		$return=[
		'status'=>'Empty',
		'State'=>[],
		];
		$State=$this->State->find('list',array(
			'order'=>array('id ASC'),
			'fields'=>array('State.id','State.name'),
			));
		if($State)
		{
			$return[ 'status' ]='success';
			$All_State=[];
			foreach ($State as $key => $value) {
				$Single['id']=$key;
				$Single['State']=$value;
				array_push($All_State, $Single);
			}
			$return['State']=$All_State;
		}

		echo json_encode($return);
		exit;
	}
	public function api_customer_edit()
	{
		$return=[
		'status'=>'Empty',
		];
		$user_id=1;
		$data=$this->request->data;
		if($data)
		{
	$datasource_Customer = $this->Customer->getDataSource();
	$datasource_AccountHead = $this->AccountHead->getDataSource();
	try {
		$datasource_Customer->begin();
		$datasource_AccountHead->begin();
		$AccountHead_id=$data['Customerid'];
		$executive=$data['executive_id'];
		$warehouse_id = $this->Executive->field(
				'Executive.warehouse_id',
				array('Executive.id ' => $executive));
		$customer_id=$this->Customer->field('Customer.id',array('Customer.account_head_id'=>$AccountHead_id));
		$customer_data=$data['Customer'];
		$name=$customer_data['name'];
		$opening_balance=0;
		if(!empty($customer_data['opening_balance']))
		{
		 $opening_balance=$customer_data['opening_balance'];	
		}
		$date=date('Y-m-d H:i:s');
		$description='';
		$address=$customer_data['place'];
        $contact_no = $customer_data['mobile'];
        $state = $customer_data['state_id'];
		if(empty($customer_data['customer_type_id']))
		{
			$customer_type_id='';
		}
		else
		{
		$customer_type_id=$customer_data['customer_type_id'];
	    }
		$email=$customer_data['email'];
		$contact_person=$customer_data['contact_person'];
		$credit_limit=$customer_data['credit_limit'];
		$division_id=$customer_data['division_id'];
		$other_notes=$customer_data['description'];
		$function_return=$this->AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description);
		if($function_return['result']!='Success')
			throw new Exception($function_return['message']);
		$this->Customer->id=$customer_id;
         if(!empty($name)){
			$customer_data['name']= $name;
			if(!$this->Customer->saveField('name',$name))
				throw new Exception("Cant Updated Name", 1);
		}
		if(!empty($customer_data['other_notes'])){
			if(!$this->Customer->saveField('other_notes',$customer_data['other_notes']))
				throw new Exception("Cant Updated description", 1);
		}
		if(!empty($customer_type_id)){
			$customer_data['customer_type_id']= $customer_type_id;
			if(!$this->Customer->saveField('customer_type_id',$customer_type_id))
				throw new Exception("Cant Updated Customer Type", 1);
		}
		if(!empty($address)){
			$customer_data['address']= $address;
			if(!$this->Customer->saveField('place',$address))
				throw new Exception("Cant Updated Address", 1);
		}
		if(!empty($state)){
			$customer_data['state']= $state;
			if(!$this->Customer->saveField('state_id',$state))
				throw new Exception("Cant Updated State", 1);
		}
		if(!empty($email)){
			$customer_data['email']= $email;
			if(!$this->Customer->saveField('email',$email))
				throw new Exception("Cant Updated Email", 1);
		}
		if(!empty($contact_person)){
			$customer_data['contact_person']= $contact_person;
			if(!$this->Customer->saveField('contact_person',$contact_person))
				throw new Exception("Cant Updated Contact person", 1);
		}
		if(!empty($contact_no)){
			$customer_data['contact_no']= $contact_no;
			if(!$this->Customer->saveField('contact_no',$contact_no))
				throw new Exception("Cant Updated Contact No", 1);
		}
		 if(!empty($credit_limit)){

			$customer_data['credit_limit']= $credit_limit;
			if(!$this->Customer->saveField('credit_limit',$credit_limit))
				throw new Exception("Cant Updated Credit limit", 1);
		 }
		if(!empty($opening_balance)){
			$customer_data['opening_balance']= $opening_balance;
			if(!$this->Customer->saveField('opening_balance',$opening_balance))
				throw new Exception("Cant Updated Opening Balance", 1);
		}
		$customer_data['updated_at']= date('Y-m-d H:i:s',strtotime($date));
		if(!$this->Customer->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date))))
			throw new Exception("Cant Updated ", 1);
         $last_iserted_customer = $this->Customer->findById($customer_id);
				$Single_Customer['route_code'] = $this->Route->field(
					'Route.code',
					array('Route.id ' => $last_iserted_customer['Customer']['route_id']));
				$this->Product->unbindModel(array('hasMany' => array('SalesReturnItem','SaleItem','PurchasedItem','PurchaseReturnItem','UnwantedList','Stock','StockLog')));
				$Stock=$this->Product->find('all',array(
				"joins"=>array(
					array(
						"table"=>'stocks',
						"alias"=>'Stock',
						"type"=>'inner',
						"conditions"=>array('Product.id=Stock.product_id'),
						),
					array(
						"table"=>'warehouses',
						"alias"=>'Warehouse',
						"type"=>'inner',
						"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
						),
					),
				'conditions'=>array('Product.active=1','type ='=>[2,5,8],'Stock.flag'=>1,'Stock.warehouse_id'=>$warehouse_id),
				'fields'=>array(
					'Product.priority',
					'Product.id',
					'Product.name',
					'Product.wholesale_price',
					'Stock.quantity'
					)
				));
			$All_Stock=[];
			foreach($Stock as $key1 => $value1){
				if($value1['Stock']['quantity']>0){
					$Single_stock['product_id']=$value1['Product']['id'];
					$conditions_sale=[];
					$conditions_sale['SaleItem.product_id']=$value1['Product']['id'];
					$conditions_sale['Sale.account_head_id']=$last_iserted_customer['AccountHead']['id'];
					 $CustomerPrice=$this->CustomerPrice->find('all',array(
				'conditions'=>array('CustomerPrice.customer_id'=>$last_iserted_customer['AccountHead']['id']),
				'fields'=>array(
					'CustomerPrice.product_id',
					'CustomerPrice.product_wholesale_price',
					),
				));
					 }
			  }
				   $Single_Customer['stock']=$CustomerPrice;
				$Single_Customer['id']=$last_iserted_customer['AccountHead']['id'];
				$Single_Customer['name']=$last_iserted_customer['AccountHead']['name'];
				$Single_Customer['Division']=$last_iserted_customer['Division']['name'];
				$Single_Customer['opening_balance']=$last_iserted_customer['AccountHead']['opening_balance'];
				$Single_Customer['email']=$last_iserted_customer['Customer']['email'];
				$Single_Customer['shope_code']=$last_iserted_customer['Customer']['code'];
				$Single_Customer['credit_limit']=$last_iserted_customer['Customer']['credit_limit'];
				$Single_Customer['address']=$last_iserted_customer['Customer']['place'];
				$Single_Customer['mobile']=$last_iserted_customer['Customer']['mobile'];
				$State=$this->State->field('name',['State.id'=>$last_iserted_customer['Customer']['state_id']]);
				$Single_Customer['State']=$State;
				$total = $Single_Customer['opening_balance'];
				$Recieved='0';
				$Debit_N_Credit_function=$this->General_Journal_Debit_N_Credit_function($last_iserted_customer['AccountHead']['id']);
				$total+=$Debit_N_Credit_function['debit'];
				$Recieved+=$Debit_N_Credit_function['credit'];
				$Single_Customer['debit']=$total;
				$Single_Customer['credit']=$Recieved;
				$Sale=$this->Sale->find('all',array(
					'conditions'=>array(
						'Sale.account_head_id'=>$last_iserted_customer['AccountHead']['id']
						),
					'fields'=>array(
						'Sale.id',
						'Sale.invoice_no',
						'Sale.grand_total',
						'Sale.balance',
						'AccountHead.id',
						'AccountHead.name',
						),
					));
				$All_sale=[];
				$Single_Customer['receipt']=$All_sale;
				$return['customer']= $Single_Customer;
				$return['key'] = $last_iserted_customer['AccountHead']['id'];
				$return['value'] = $last_iserted_customer['AccountHead']['name'];
	    $datasource_Customer->commit();
		$datasource_AccountHead->commit();
		$return['status']='Success';
	} catch (Exception $e) {
		$datasource_Customer->rollback();
		$datasource_AccountHead->rollback();
		$return['status']='Error';
		$return['message']=$e->getMessage();
	}
  }
	echo json_encode($return);
	exit;

	}
	public function AccountHeadEdit($AccountHead_id,$name,$opening_balance,$date,$description)
{
	try {
		$this->AccountHead->id=$AccountHead_id;
		if(!$this->AccountHead->saveField('opening_balance',$opening_balance)) throw new Exception("Cant Updated the Opening Balance", 1);
		if(!$this->AccountHead->saveField('name',strtoupper($name))) throw new Exception("Cant Updated the name", 1);
		if(!$this->AccountHead->saveField('description',$description)) throw new Exception("Cant Updated the description", 1);
		if(!$this->AccountHead->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date)))) throw new Exception("Error updation updated_at", 1);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']='Error';
		$return['message']=$e->getMessage();
	}
	return $return;
}
public function api_sale_delete() {
		     $return['result']='Empty';
			$data=$this->request->data;
					$datasource_Sale = $this->Sale->getDataSource();
					$datasource_SaleItem = $this->SaleItem->getDataSource();
					$datasource_SalesReturn = $this->SalesReturn->getDataSource();
			        $datasource_SalesReturnItem = $this->SalesReturnItem->getDataSource();
			try {
					$datasource_Sale->begin();
					$datasource_SaleItem->begin();
					$datasource_SalesReturn->begin();
			        $datasource_SalesReturnItem->begin();
					if(!empty($data['invoice_no']))
					{
					$invoice_no=$data['invoice_no'];
					$Sale=$this->Sale->findByInvoiceNo($invoice_no);
					if(!$Sale)
					throw new Exception("Empty Sales", 1);
				    $id=$Sale['Sale']['id'];
					$sale_rollback_function=$this->sale_rollback($id);
              if($sale_rollback_function['result']!='Success')
                throw new Exception($sale_return_rollback_function['result']);
							$SaleItem=$this->SaleItem->find('list',array(
							'conditions'=>array(
							'sale_id'=>$id
							),
							));
							foreach ($SaleItem as $key => $value) {
							if(!$this->SaleItem->delete($key))
							throw new Exception("Error Processing SaleItem deletion", 1);
							}
							if(!$this->Sale->delete($id))
							throw new Exception("Error Processing Sale deletion", 1);
					}
					if(!empty($data['return_invoice_no']))
					{
					$return_invoice_no=$data['return_invoice_no'];
					$SalesReturn=$this->SalesReturn->findByInvoiceNo($return_invoice_no);
					if(!$SalesReturn)
					throw new Exception("Empty Sale Return", 1);
				    $return_id=$SalesReturn['SalesReturn']['id'];
					$sale_return_rollback_function=$this->sales_return_rollback($return_id);
              if($sale_return_rollback_function['result']!='Success')
                throw new Exception($sale_return_rollback_function['result']);
							$SalesReturnItem=$this->SalesReturnItem->find('list',array(
							'conditions'=>array(
							'sales_return_id'=>$return_id
							),
							));
							foreach ($SalesReturnItem as $key => $value) {
							if(!$this->SalesReturnItem->delete($key))
							throw new Exception("Error Processing SalesReturnItem deletion", 1);
							}
							if(!$this->SalesReturn->delete($return_id))
							throw new Exception("Error Processing SalesReturn deletion", 1);
					}
					if(!empty($data['receipt_no']))
					{
						$receipt_no=$data['receipt_no'];
						if(!$this->Journal->DeleteAll(array('Journal.receipt_no'=>$receipt_no)))
						throw new Exception("Error Processing Journal deletion", 1);
					}
					$datasource_Sale->commit();
					$datasource_SaleItem->commit();
					$datasource_SalesReturn->commit();
					$datasource_SalesReturnItem->commit();
				$return['result']='Success';
			} catch (Exception $e) {
				$return['result']='Error';
				$return['message']=$e->getMessage();
				$datasource_Sale->rollback();
				$datasource_SaleItem->rollback();
				$datasource_SalesReturn->rollback();
			    $datasource_SalesReturnItem->rollback();
			}
		echo json_encode($return);
		exit;
	}
	public function sale_rollback($id)
  {
    $user_id=1;
    $datasource_Stock = $this->Stock->getDataSource();
    $datasource_Journal = $this->Journal->getDataSource();
    $datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
    $datasource_ExecutiveBonusCalculation = $this->ExecutiveBonusCalculation->getDataSource();
    try {
      $datasource_Stock->begin();
      $datasource_Journal->begin();
      $datasource_ProductBonusDetail->begin();
      $datasource_ExecutiveBonusCalculation->begin();
      $Sale=$this->Sale->findById($id);
      if(!$Sale)
        throw new Exception("Empty Sales", 1);
      $invoice_no=$Sale['Sale']['invoice_no'];
      $executive_id=$Sale['Sale']['executive_id'];
      foreach ($Sale['SaleItem'] as $key => $value) {
        $quantity = $value['quantity'];
        $free_quantity = $value['free_qty'];
        $product_id=$value['product_id'];
        $warehouse_id=$value['warehouse_id'];
        $Stock = $this->Stock->find('first',array(
          'conditions'=>array(
            'Stock.product_id'=>$product_id,
            'Stock.warehouse_id'=>$warehouse_id,
            )
          ));
        if(!$Stock)
          throw new Exception("Empty Stock", 1);
        $stock_id=$Stock['Stock']['id'];
        $stock_quantity=$Stock['Stock']['quantity'];
        $date=date('Y-m-d');
      $remark='Cancel Sale --'.$invoice_no.'('.$quantity.')';
        $stock_quantity+=$quantity;
        $stock_quantity+=$free_quantity;
        $StockController = new StockController;
        $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
        if($Stock_function_return['result']!='Success')
          throw new Exception($Stock_function_return['result']);
      }
      $Journal=$this->Journal->find('all',array(
        'conditions'=>array(
          'remarks'=>'Sale Invoice No :'.$Sale['Sale']['invoice_no'],
          ),
        'fields'=>array(
          'Journal.*',
          )
        ));
      foreach ($Journal as $key => $value) {
        if(!$this->Journal->delete($value['Journal']['id']))
          throw new Exception("Error Processing Journal deletion", 1);
      }
      $ProductBonusDetail=$this->ProductBonusDetail->find('all',array(
        'conditions'=>array(
          'sale_id'=>$id,
          ),
        'fields'=>array(
          'ProductBonusDetail.*',
          )
      ));
      $bonus_amount=0;
      if(!empty($ProductBonusDetail)){
      foreach ($ProductBonusDetail as $key => $value) {
        $bonus_amount+=$value['ProductBonusDetail']['bonus_amount'];
        if(!$this->ProductBonusDetail->delete($value['ProductBonusDetail']['id']))
          throw new Exception("Error Processing Product Bonus Detail deletion", 1);
      }
      }
      $ExecutiveBonusCalculation=$this->ExecutiveBonusCalculation->findByReferenceNo($Sale['Sale']['invoice_no']);
      if(!empty($ExecutiveBonusCalculation))
      {
         if(!$this->ExecutiveBonusCalculation->delete($ExecutiveBonusCalculation['ExecutiveBonusCalculation']['id']))
          throw new Exception("Error Processing ExecutiveBonusCalculation deletion", 1);
      }
      else
      {
        $ExecutiveBonusCalculation=$this->ExecutiveBonusCalculation->findByWorkFlowAndBonusAmountAndExecutiveId("SALE",$bonus_amount,$executive_id);
       if(!empty($ExecutiveBonusCalculation))
         {
        if(!$this->ExecutiveBonusCalculation->delete($ExecutiveBonusCalculation['ExecutiveBonusCalculation']['id']))
          throw new Exception("Error Processing ExecutiveBonusCalculation deletion", 1);
          }
      }
      
      $datasource_Stock->commit();
      $datasource_Journal->commit();
      $datasource_ProductBonusDetail->commit();
      $datasource_ExecutiveBonusCalculation->commit();
      $return['result']='Success';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
      $datasource_Stock->rollback();
      $datasource_Journal->rollback();
      $datasource_ProductBonusDetail->rollback();
      $datasource_ExecutiveBonusCalculation->rollback();
    }
    return $return;
  }
  public function sales_return_rollback($id)
  {
    $user_id=1;
    $datasource_Stock = $this->Stock->getDataSource();
    $datasource_Journal = $this->Journal->getDataSource();
    $datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
    $datasource_ExecutiveBonusCalculation = $this->ExecutiveBonusCalculation->getDataSource();
    try {
      $datasource_Stock->begin();
      $datasource_Journal->begin();
      $datasource_ProductBonusDetail->begin();
      $datasource_ExecutiveBonusCalculation->begin();
      $SalesReturn=$this->SalesReturn->findById($id);
      if(!$SalesReturn)
        throw new Exception("Empty SalesReturn", 1);
      $invoice_no=$SalesReturn['SalesReturn']['invoice_no'];
      $executive_id=$SalesReturn['SalesReturn']['executive_id'];
      foreach ($SalesReturn['SalesReturnItem'] as $key => $value) {
        $quantity = $value['quantity'];
       // $free_quantity = $value['free_qty'];
        $product_id=$value['product_id'];
        $warehouse_id=$value['warehouse_id'];
        $Stock = $this->Stock->find('first',array(
          'conditions'=>array(
            'Stock.product_id'=>$product_id,
            'Stock.warehouse_id'=>$warehouse_id,
            )
          ));
        if(!$Stock)
          throw new Exception("Empty Stock", 1);
        $stock_id=$Stock['Stock']['id'];
        $stock_quantity=$Stock['Stock']['quantity'];
        $date=date('Y-m-d');
      $remark='Cancel SalesReturn --'.$invoice_no.'('.$quantity.')';
        $stock_quantity+=$quantity;
       // $stock_quantity+=$free_quantity;
        $StockController = new StockController;
        $Stock_function_return=$StockController->GeneralStock_Edit_Function($stock_id,$stock_quantity,$date,$user_id,$remark);
        if($Stock_function_return['result']!='Success')
          throw new Exception($Stock_function_return['result']);
      }
      $Journal=$this->Journal->find('all',array(
        'conditions'=>array(
          'remarks'=>'SalesReturn Invoice No :'.$invoice_no,
          ),
        'fields'=>array(
          'Journal.*',
          )
        ));
      foreach ($Journal as $key => $value) {
        if(!$this->Journal->delete($value['Journal']['id']))
          throw new Exception("Error Processing Journal deletion", 1);
      }
      $ProductBonusDetail=$this->ProductBonusDetail->find('all',array(
        'conditions'=>array(
          'sales_return_id'=>$id,
          ),
        'fields'=>array(
          'ProductBonusDetail.*',
          )
      ));
      $bonus_amount=0;
      if(!empty($ProductBonusDetail)){
      foreach ($ProductBonusDetail as $key => $value) {
        $bonus_amount+=$value['ProductBonusDetail']['bonus_return_amount'];
        if(!$this->ProductBonusDetail->delete($value['ProductBonusDetail']['id']))
          throw new Exception("Error Processing Product Bonus Detail deletion", 1);
      }
      }
      $ExecutiveBonusCalculation=$this->ExecutiveBonusCalculation->findByReferenceNo($invoice_no);
      if(!empty($ExecutiveBonusCalculation))
      {
         if(!$this->ExecutiveBonusCalculation->delete($ExecutiveBonusCalculation['ExecutiveBonusCalculation']['id']))
          throw new Exception("Error Processing ExecutiveBonusCalculation deletion", 1);
      }
      else
      {
        $ExecutiveBonusCalculation=$this->ExecutiveBonusCalculation->findByWorkFlowAndBonusReturnAmountAndExecutiveId("SALE RETURN",$bonus_amount,$executive_id);
       if(!empty($ExecutiveBonusCalculation))
         {
        if(!$this->ExecutiveBonusCalculation->delete($ExecutiveBonusCalculation['ExecutiveBonusCalculation']['id']))
          throw new Exception("Error Processing ExecutiveBonusCalculation deletion", 1);
          }
      }
      
      $datasource_Stock->commit();
      $datasource_Journal->commit();
      $datasource_ProductBonusDetail->commit();
      $datasource_ExecutiveBonusCalculation->commit();
      $return['result']='Success';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
      $datasource_Stock->rollback();
      $datasource_Journal->rollback();
      $datasource_ProductBonusDetail->rollback();
      $datasource_ExecutiveBonusCalculation->rollback();
    }
    return $return;
  }
  public function api_executive_ledger_report()
  {
	 $return['status']="Empty";
		$data=$this->request->data;
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$from_date=date('Y-m-d',strtotime('-5 days'.$data['to_date']));
         if(isset($data['executive_id']))
		{
			$executive=$data['executive_id'];
		$staff_id=$this->Executive->field('Executive.staff_id',array('Executive.id'=>$executive));
		$account_head_id=$this->Staff->field('Staff.account_head_id',array('Staff.id'=>$staff_id));
		$AccountHead_id=$account_head_id;
		$AccountHead=$this->AccountHead->findById($AccountHead_id);
		if(empty($AccountHead))
			throw new Exception("Empty AccountHead", 1);
		$this->Journal->virtualFields = array(
			'total_amount' => "SUM(Journal.amount)",
			);
		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						//'Journal.debit!=23',
					   // 'Journal.credit!=23',
						'Journal.debit!=19',
						'Journal.credit!=19',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						//'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.narration',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$Journal_all=[];$balance=0;
		foreach ($Journal as $key => $value) 
		{
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['narration']=$value['Journal']['narration'];
			$Journal_single['amount']=$value['Journal']['amount'];
			$Journal_single['work_flow']=$value['Journal']['work_flow'];
			if(!empty($value['Executive']['name'])){
				$Journal_single['executive']=$value['Executive']['name'];
			}
			else{
				$Journal_single['executive']="No Executive";
			}
			$Journal_single['external_voucher']=$value['Journal']['external_voucher'];
			if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
			{
				$Journal_single['mode']=$value['AccountHeadCredit']['name'];
				$Journal_single['debit']=$value['Journal']['amount'];
				$Journal_single['credit']=0.00;
			}
			else
			{
				$Journal_single['mode']=$value['AccountHeadDebit']['name'];
				$Journal_single['credit']=$value['Journal']['amount'];
				$Journal_single['debit']=0.00;
			}
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		$Balance_Total=0;
		$credit_Total=0;
		$debit_Total=0;
			$AccountsController = new AccountsController;
		$type=$AccountsController->get_type_by_account_dead($AccountHead['AccountHead']['id']);
		$Type_name=$type['AccMainGroup']['name'];
		$category_name=$category[$type['AccMainGroup']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
		}
			if($category_name=='credit_account')
		{
			$debit=$AccountHead['AccountHead']['opening_balance'];
			$credit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$AccountHead['AccountHead']['opening_balance'];
			$credit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		if(!in_array($Type_name, ['Income']))
		{
			$JournalCLosginDebit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit !='=>19,
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					//'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array( 'total_amount'),
				));
			$JournalCLosginCredit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					'Journal.debit !='=>19,
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					//'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array('total_amount'),
				));
			$debit+=$JournalCLosginDebit['Journal']['total_amount'];
			$credit+=$JournalCLosginCredit['Journal']['total_amount'];
			$balance1=$debit-$credit;
				if($balance<0) {
					$credit=$balance1;
					$debit='0.00';
				}else{
					$debit=$balance1;
					$credit='0.00';
				}
			$Journal_all[0]=array(
				'id'=>0,
				'date'=>date('d-m-Y',strtotime($from_date)),
				'strtotime'=>0,
				'mode'=>'Opening Balance',
				'executive'=>'',
				'voucher_no'=>'',
				'external_voucher'=>'',
				'narration'=>'',
				'remarks'=>'',
				'credit'=>floatval($credit),
				'debit'=>floatval($debit),
				'balance'=>number_format($balance1,2,'.',''),
				);
		}
		else
		{
			if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
			{

			}
			else
			{
				$debit=0;
				$credit=0;
			}
		}
		if(!in_array($Type_name, ['Income']))
		{
			$Journal_voucher_no=$this->Journal->find('list',array(
				'conditions'=>array(
					'AND' => array(
						'OR' => array(
							'Journal.debit'=>$AccountHead['AccountHead']['id'],
							'Journal.credit'=>$AccountHead['AccountHead']['id'],
							),
						'AND' => array(
							'Journal.flag=1',
							'Journal.date between ? and ?'=>array($from_date,$to_date),
							//'Journal.branch_id'=>$branch_id,
							)
						)
					),
				'fields'=>array(
					'Journal.id',
					'Journal.date',
					'Journal.voucher_no',
					),
				));
			foreach ($Journal_voucher_no as $voucher_no => $lists) {
				if(count($lists)>1)
				{
					$j=0;
					foreach ($lists as $journal_id => $date) {
						if($j)
						{
							if($date==$Journal_all[key($lists)]['date'] && $voucher_no==$Journal_all[key($lists)]['voucher_no'])
							{
								$Journal_all[key($lists)]['credit']+=$Journal_all[$journal_id]['credit'];
								$Journal_all[key($lists)]['debit']+=$Journal_all[$journal_id]['debit'];
								unset($Journal_all[$journal_id]);
								$GetJournal=$this->Journal->findById($Journal_all[key($lists)]['id'],['credit']);
								if($AccountHead['AccountHead']['id']==$GetJournal['Journal']['credit'])
								{
									$Journal_all[key($lists)]['credit']-=$Journal_all[key($lists)]['debit'];
									$Journal_all[key($lists)]['debit']=0;
									$Journal_all[key($lists)]['credit']=($Journal_all[key($lists)]['credit']);
								}
								else
								{
									$Journal_all[key($lists)]['debit']-=$Journal_all[key($lists)]['credit'];
									$Journal_all[key($lists)]['credit']=0;
									$Journal_all[key($lists)]['debit']=($Journal_all[key($lists)]['debit']);
								}
							}								
						}
						$j++;
					}
				}
			}
		}
		// pr($Journal_all);
		// exit;
		$date=[];
		foreach ($Journal_all as $i => $row) {
			$date[$i]  = $row['strtotime'];
		}
		array_multisort($date, SORT_ASC, $Journal_all);
		 $ledger_report=[];
		 if(!empty($Journal_all))
		 {
		foreach ($Journal_all as $key => $value) 
		{
			$single['date']=date('d-m-Y',strtotime($value['date']));
			$single['voucher_no']=$value['voucher_no'];
			$single['mode']=$value['mode'];
			$single['remarks']=$value['remarks'];
			$single['debit']=number_format($value['debit'],2,'.','');
			$single['credit']=number_format($value['credit'],2,'.','');
			if($category_name=='credit_account') {
			 $balance=$balance+$value['credit']-$value['debit']; }
			else { $balance=$balance+$value['debit']-$value['credit']; }
			if($balance>=0){
				 $single['balance']=number_format($balance,2,'.','').' '.'Dr';
				}
				else{
					$balance1 = str_replace("-", "", $balance);
					$single['balance']=number_format($balance1,2,'.','').' '.'Cr';
				}
		  $credit_Total+=$value['credit'];
			$debit_Total+=$value['debit'];
			$single['narration']=$value['narration']?$value['narration']:"";
			$ledger_report[]=$single;
		}
		$return['ledger_report']=$ledger_report;
		$return['total_debit']=$debit_Total;
		$return['total_credit']=$credit_Total;
		$Type_name=$type['AccMainGroup']['name'];
		$category_name=$category[$type['AccMainGroup']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
		}
		if($category_name=='credit_account')
		{

			$Balance_Total=$debit_Total-$credit_Total;
		}
		else
		{
			$Balance_Total=$debit_Total-$credit_Total;	
		}
		if($Balance_Total>=0){
			$return['total_balance']=number_format($Balance_Total,2,'.','').' '.'Dr';
		}else{
			$Balance_Total1 = str_replace("-", "", $Balance_Total);
			$return['total_balance']=number_format($Balance_Total1,2,'.','').' '.'Cr';
		}
		$return['status']='Success';
		}
	}
		else{
			$return[ 'status' ]= 'Executive Required';
		}
		echo json_encode($return);
		exit;
}
public function api_customer_ledger_report()
  {
	 $return['status']="Empty";
		$data=$this->request->data;
		$to_date=date('Y-m-d',strtotime($data['to_date']));
		$from_date=date('Y-m-d',strtotime('-5 days'.$data['to_date']));
         if(isset($data['customer_id']))
		{
			$AccountHead_id=$data['customer_id'];
		$AccountHead=$this->AccountHead->findById($AccountHead_id);
		if(empty($AccountHead))
			throw new Exception("Empty AccountHead", 1);
		$this->Journal->virtualFields = array(
			'total_amount' => "SUM(Journal.amount)",
			);
		$Journal=$this->Journal->find('all',array(
			'joins'=>array(
				array(
					'table'=>'executives',
					'alias'=>'Executive',
					'type'=>'LEFT',
					'conditions'=>array('Journal.executive_id=Executive.id')
					),
				),
			'conditions'=>array(
				'AND' => array(
					'OR' => array(
						'Journal.debit'=>$AccountHead['AccountHead']['id'],
						'Journal.credit'=>$AccountHead['AccountHead']['id'],
//'Journal.branch_id'=>$branch_id,
						),
					'AND' => array(
						//'Journal.debit!=23',
					   // 'Journal.credit!=23',
						'Journal.debit!=19',
						'Journal.credit!=19',
						'Journal.date between ? and ?'=>array($from_date,$to_date),
						//'Journal.branch_id'=>$branch_id,
						)
					)
				),
			'order'=>array('Journal.date ASC'),
			'fields'=>array(
				'Journal.id',
				'Journal.remarks',
				'Journal.narration',
				'Journal.amount',
				'Journal.date',
				'Journal.voucher_no',
				'Journal.external_voucher',
				'Journal.work_flow',
				'Journal.created_at',
				'Journal.executive_id',
				'Executive.id',
				'Executive.name',
				'AccountHeadCredit.name',
				'AccountHeadCredit.opening_balance',
				'AccountHeadDebit.name',
				'AccountHeadDebit.opening_balance',

				),
			));
		$category=[
		'Asset'=>'debit_account',
		'Expense'=>'debit_account',
		'Capital'=>array(
			'Capital'=>'credit_account',
			'Drawing'=>'debit_account',
			),
		'Income'=>'credit_account',
		'Liabilities'=>'credit_account',
		];
		$Journal_all=[];$balance=0;
		foreach ($Journal as $key => $value) 
		{
			$Journal_single['id']=$value['Journal']['id'];
			$Journal_single['date']=$value['Journal']['date'];
			$Journal_single['strtotime']=strtotime($value['Journal']['date'])+$value['Journal']['id'];
			$Journal_single['voucher_no']=$value['Journal']['voucher_no'];
			$Journal_single['remarks']=$value['Journal']['remarks'];
			$Journal_single['narration']=$value['Journal']['narration'];
			$Journal_single['amount']=$value['Journal']['amount'];
			$Journal_single['work_flow']=$value['Journal']['work_flow'];
			if(!empty($value['Executive']['name'])){
				$Journal_single['executive']=$value['Executive']['name'];
			}
			else{
				$Journal_single['executive']="No Executive";
			}
			$Journal_single['external_voucher']=$value['Journal']['external_voucher'];
			if($AccountHead['AccountHead']['name']==$value['AccountHeadDebit']['name'])
			{
				$Journal_single['mode']=$value['AccountHeadCredit']['name'];
				$Journal_single['debit']=$value['Journal']['amount'];
				$Journal_single['credit']=0.00;
			}
			else
			{
				$Journal_single['mode']=$value['AccountHeadDebit']['name'];
				$Journal_single['credit']=$value['Journal']['amount'];
				$Journal_single['debit']=0.00;
			}
			$Journal_all[$Journal_single['id']]=$Journal_single;
		}
		$Balance_Total=0;
		$credit_Total=0;
		$debit_Total=0;
			$AccountsController = new AccountsController;
		$type=$AccountsController->get_type_by_account_dead($AccountHead['AccountHead']['id']);
		$Type_name=$type['AccMainGroup']['name'];
		$category_name=$category[$type['AccMainGroup']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
		}
			if($category_name=='credit_account')
		{
			$debit=$AccountHead['AccountHead']['opening_balance'];
			$credit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		else
		{
			$debit=$AccountHead['AccountHead']['opening_balance'];
			$credit='0';
			if($AccountHead['AccountHead']['opening_balance']<0)
			{
				$debit=0;
				$credit=$AccountHead['AccountHead']['opening_balance']*-1;
			}
		}
		if(!in_array($Type_name, ['Income']))
		{
			$JournalCLosginDebit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.debit'=>$AccountHead['AccountHead']['id'],
					'Journal.credit !='=>19,
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					//'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array( 'total_amount'),
				));
			$JournalCLosginCredit=$this->Journal->find('first',array(
				'conditions'=>array(
					'Journal.credit'=>$AccountHead['AccountHead']['id'],
					'Journal.debit !='=>19,
					'Journal.flag=1',
					'Journal.date <'=>$from_date,
					//'Journal.branch_id'=>$branch_id,
					),
				'fields'=>array('total_amount'),
				));
			$debit+=$JournalCLosginDebit['Journal']['total_amount'];
			$credit+=$JournalCLosginCredit['Journal']['total_amount'];
			$balance1=$debit-$credit;
				if($balance<0) {
					$credit=$balance1;
					$debit='0.00';
				}else{
					$debit=$balance1;
					$credit='0.00';
				}
			$Journal_all[0]=array(
				'id'=>0,
				'date'=>date('d-m-Y',strtotime($from_date)),
				'strtotime'=>0,
				'mode'=>'Opening Balance',
				'executive'=>'',
				'voucher_no'=>'',
				'external_voucher'=>'',
				'narration'=>'',
				'remarks'=>'',
				'credit'=>floatval($credit),
				'debit'=>floatval($debit),
				'balance'=>number_format($balance1,2,'.',''),
				);
		}
		else
		{
			if(date('Y-m-d',strtotime($from_date))<=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])) && date('Y-m-d',strtotime($to_date))>=date('Y-m-d',strtotime($AccountHead['AccountHead']['created_at'])))
			{

			}
			else
			{
				$debit=0;
				$credit=0;
			}
		}
		if(!in_array($Type_name, ['Income']))
		{
			$Journal_voucher_no=$this->Journal->find('list',array(
				'conditions'=>array(
					'AND' => array(
						'OR' => array(
							'Journal.debit'=>$AccountHead['AccountHead']['id'],
							'Journal.credit'=>$AccountHead['AccountHead']['id'],
							),
						'AND' => array(
							'Journal.flag=1',
							'Journal.date between ? and ?'=>array($from_date,$to_date),
							//'Journal.branch_id'=>$branch_id,
							)
						)
					),
				'fields'=>array(
					'Journal.id',
					'Journal.date',
					'Journal.voucher_no',
					),
				));
			foreach ($Journal_voucher_no as $voucher_no => $lists) {
				if(count($lists)>1)
				{
					$j=0;
					foreach ($lists as $journal_id => $date) {
						if($j)
						{
							if($date==$Journal_all[key($lists)]['date'] && $voucher_no==$Journal_all[key($lists)]['voucher_no'])
							{
								$Journal_all[key($lists)]['credit']+=$Journal_all[$journal_id]['credit'];
								$Journal_all[key($lists)]['debit']+=$Journal_all[$journal_id]['debit'];
								unset($Journal_all[$journal_id]);
								$GetJournal=$this->Journal->findById($Journal_all[key($lists)]['id'],['credit']);
								if($AccountHead['AccountHead']['id']==$GetJournal['Journal']['credit'])
								{
									$Journal_all[key($lists)]['credit']-=$Journal_all[key($lists)]['debit'];
									$Journal_all[key($lists)]['debit']=0;
									$Journal_all[key($lists)]['credit']=($Journal_all[key($lists)]['credit']);
								}
								else
								{
									$Journal_all[key($lists)]['debit']-=$Journal_all[key($lists)]['credit'];
									$Journal_all[key($lists)]['credit']=0;
									$Journal_all[key($lists)]['debit']=($Journal_all[key($lists)]['debit']);
								}
							}								
						}
						$j++;
					}
				}
			}
		}
		// pr($Journal_all);
		// exit;
		$date=[];
		foreach ($Journal_all as $i => $row) {
			$date[$i]  = $row['strtotime'];
		}
		array_multisort($date, SORT_ASC, $Journal_all);
		 $ledger_report=[];
		 if(!empty($Journal_all))
		 {
		foreach ($Journal_all as $key => $value) 
		{
			$single['date']=date('d-m-Y',strtotime($value['date']));
			$single['voucher_no']=$value['voucher_no'];
			$single['mode']=$value['mode'];
			$single['remarks']=$value['remarks'];
			$single['debit']=number_format($value['debit'],2,'.','');
			$single['credit']=number_format($value['credit'],2,'.','');
			if($category_name=='credit_account') {
			 $balance=$balance+$value['credit']-$value['debit']; }
			else { $balance=$balance+$value['debit']-$value['credit']; }
			if($balance>=0){
				 $single['balance']=number_format($balance,2,'.','').' '.'Dr';
				}
				else{
					$balance1 = str_replace("-", "", $balance);
					$single['balance']=number_format($balance1,2,'.','').' '.'Cr';
				}
		  $credit_Total+=$value['credit'];
			$debit_Total+=$value['debit'];
			$single['narration']=$value['narration']?$value['narration']:"";
			$ledger_report[]=$single;
		}
		$return['ledger_report']=$ledger_report;
		$return['total_debit']=$debit_Total;
		$return['total_credit']=$credit_Total;
		$Type_name=$type['AccMainGroup']['name'];
		$category_name=$category[$type['AccMainGroup']['name']];
		if($Type_name=='Capital')
		{
			$category_name=$category[$type['AccMainGroup']['name']][$type['AccSubGroup']['name']];
		}
		if($category_name=='credit_account')
		{

			$Balance_Total=$debit_Total-$credit_Total;
		}
		else
		{
			$Balance_Total=$debit_Total-$credit_Total;	
		}
		if($Balance_Total>=0){
			$return['total_balance']=number_format($Balance_Total,2,'.','').' '.'Dr';
		}else{
			$Balance_Total1 = str_replace("-", "", $Balance_Total);
			$return['total_balance']=number_format($Balance_Total1,2,'.','').' '.'Cr';
		}
		$return['status']='Success';
		}
	}
		else{
			$return[ 'status' ]= 'Customer Required';
		}
		echo json_encode($return);
		exit;
}
}
