<?php
App::uses('AppController', 'Controller');
App::import('Controller', 'Accountings');
App::import('Controller', 'Sale');

class StockController extends AppController {
	public $uses=array(
		'ProductType',
		'Unit',
		'Product',
		'Stock',
		'FixedStock',
		'FixedStockLog',
		'StockLog',
		'SaleProductsOffline',
		'SalesOffline',
		'Brand',
		'Model',
		'Warehouse',
		'StockTransfer',
		'StockTransferItem',
		'DamagedStock',
		'Branch',
		'MslStockOrder',
		'MslStockOrderItem',
		'MslStock',
		'Sale',
		'SaleItem',
		'AccountHead',
		'TypeOfProduct',
		'Executive',
		'ProductBonusDetail',
		'StationaryIssue',
       'StationaryIssueItem',
       'ExecutiveBonusCalculation',
       'DayRegister',
       'BranchWarehouseMapping',
       'Order',
       'OrderItem',
       'ProductionPlan',
       'ExecutiveRate'
		);
	public function index()
	{
		$conditions=[];
		$this->Session->delete('stock_conditions');
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field('Menu.id', array('action ' => 'Stock/Index'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$this->set('Warehouse_all',$this->Warehouse->find('list',array(
		'fields'=>array('id','name'),
		'order'=>array('name ASC'),
		'conditions'=>array('Warehouse.is_damage'=>"",'Warehouse.warehouse_id'=>"")
		)));
		$show_cost = $this->Session->read('UserRole.show_cost');
		$this->set('show_cost',$show_cost);
		$userid =1;
		$sub_group_id=15;
		$AccountingsController = new AccountingsController;
		$Accounting_function_return=$AccountingsController->AccountHead_Option_ListBySubGroupId($sub_group_id);
		$this->set('PartyList',$Accounting_function_return);
// $this->ProductType->virtualFields = array('product_type_name' => "CONCAT(ProductType.name, ' ', ProductType.code)");
		$this->set('Product_type',$this->ProductType->find('list',array('fields'=>array('id','name'),'order'=>array('name ASC'),)));
		$user_branch_id=$this->Session->read('User.branch_id');
		if($user_branch_id)
		{
			$branch=$this->Branch->findById($user_branch_id);
			$conditions['Warehouse.id']=$branch['Branch']['warehouse_id'];
		}
		$Warehouse=$this->Warehouse->find('list',array(
			'fields'=>array('id','name'),
			'order'=>array('id ASC'),
			'conditions'=> $conditions,
			));
		$this->set(compact('Warehouse'));
		 $TypeOfProduct = $this->TypeOfProduct->find('list', array(
      'fields' => array('id','name'),
      'order'=>array('name ASC'),
      )
    );
    $this->set('Type',$TypeOfProduct);
		$this->set('Unit',$this->Unit->find('list',array('fields'=>array('id','name'))));
		$this->set('Product',array());
		$Brand=$this->Brand->find('list',array('fields'=>array('id','Brand.name'),'order'=>array('Brand.name ASC'),));
		$Brand['G']='GENERAL';
		ksort($Brand);
		$this->set('Brand',$Brand);
		$Stock=array();
		$total_cost=0;
		foreach ($Stock as $key => $value) {
			$total_cost+=$value['Product']['cost']*$value['Stock']['quantity'];
		}
		$this->set('total_cost',$total_cost);
		$this->set('Stock',$Stock);
		$date= date('d-m-Y');
		$this->set('date',$date);
		$data['ProductAdd']['stock_date']=date('d-m-Y');
		$data['ProductEdit']['stock_date']=date('d-m-Y');
		$this->request->data=$data;
	}
	public function GeneralStock_Add_Function($warehouse_id,$product_id,$quantity,$date,$user_id,$remark,$flag=1)
	{
		try {
			$date =date('Y-m-d');
			if(!$user_id)
				throw new Exception("Empty user Id", 1);
			if(!$product_id)
				throw new Exception("Empty product Id", 1);
			if(!$date)
				throw new Exception("Empty date", 1);
			if(!$quantity && $quantity!=0)
				throw new Exception("Empty quantity Id", 1);
			if(!$remark)
				throw new Exception("Empty remark", 1);
			$stock_data=[
			'product_id'=>$product_id,
			'warehouse_id'=>$warehouse_id,
			'quantity'=>$quantity,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'flag'=>$flag,
			'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
			'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
			];
			$this->Stock->create();
			if(!$this->Stock->save($stock_data))
			{
				$errors = $this->Stock->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$Stock=$this->Stock->read();
			unset($Stock['Stock']['id']);
			unset($Stock['Stock']['flag']);
			unset($Stock['Stock']['modified_by']);
			$Stock['Stock']['remark']=$remark;
			$Stock['Stock']['quantity_in']=$quantity;
			$work_flow='Product Creation';
			$function_stockLog_return=$this->stock_logs($Stock['Stock'],$work_flow);
			if($function_stockLog_return['result']!='Success')
				throw new Exception($function_stockLog_return['result'], 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function General_FixedStock_Add_Function($account_head_id,$quantity,$date,$user_id,$remark,$flag=1)
	{
		try {
			if(!$user_id)
				throw new Exception("Empty user Id", 1);
			if(!$account_head_id)
				throw new Exception("Empty Account Head Id", 1);
			if(!$date)
				throw new Exception("Empty date", 1);
			if(!$quantity && $quantity!=0)
				throw new Exception("Empty quantity Id", 1);
			if(!$remark)
				throw new Exception("Empty remark", 1);
			$Fixedstock_data=[
			'account_head_id'=>$account_head_id,
			'quantity'=>$quantity,
			'created_by'=>$user_id,
			'modified_by'=>$user_id,
			'flag'=>$flag,
			'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
			'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
			];
			$this->FixedStock->create();
			if(!$this->FixedStock->save($Fixedstock_data))
			{
				$errors = $this->FixedStock->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$FixedStock=$this->FixedStock->read();
			unset($FixedStock['FixedStock']['id']);
			unset($FixedStock['FixedStock']['flag']);
			unset($FixedStock['FixedStock']['modified_by']);
			$FixedStock['FixedStock']['remark']=$remark;
			$function_FixedstockLog_return=$this->Fixedstock_logs($FixedStock['FixedStock']);
			if($function_FixedstockLog_return['result']!='Success')
				throw new Exception($function_FixedstockLog_return['result'], 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function product_type_add_ajax()
	{
		$data=array(
			'name'=>strtoupper($this->request->data['prdct_type']),
			'created_at'=>date('Y-m-d H:i:s'),
			'updated_at'=>date('Y-m-d H:i:s'),
			);
		$this->ProductType->create();
		if($this->ProductType->save($data))
		{
			$this->Session->setFlash('data is saved');
			$last_iserted_pt=$this->ProductType->findById($this->ProductType->getLastInsertId());
			echo "<option value='".$last_iserted_pt['ProductType']['id']."'>".$last_iserted_pt['ProductType']['name']." </option>";
		}
		exit;
	}
	public function warehouse_add_ajax()
	{
		$data=array(
			'name'=>strtoupper($this->request->data['modal_warehouse_name']),
			'description'=>$this->request->data['modal_warehouse_desc'],
			);
		$this->Warehouse->create();
		if($this->Warehouse->save($data))
		{
			$this->Session->setFlash('data is saved');
			$last_iserted_pt=$this->Warehouse->findById($this->Warehouse->getLastInsertId());
			echo "<option value='".$last_iserted_pt['Warehouse']['id']."'>".$last_iserted_pt['Warehouse']['name']." </option>";
		}
		exit;
	}
	public function stock_check_ajax()
	{
		$this->autoRender = false;
		$return = true;
		$data=array();
		if(isset($this->request->data['StockProductId']))
		{
			$StockProductId=$this->request->data['StockProductId'];
		}
		$inStock=$this->Stock->find('first',array(
			'conditions'=>array(
				'product_id_fk'=>$StockProductId,
				'flag'=>1)));
		if($inStock)
		{
			echo "Yes";
		}
		else
		{
			echo "No";
		}
		exit;
	}
	public function product_type_select_ajax()
	{
		$data=$this->request->data;
		$conditions=array();
		if(!empty($data['product_type_id']))
		{
			$conditions['Product.product_type_id']=$data['product_type_id'];
		}
		if(isset($data['brand_id']))
		{
			$conditions['Product.brand_id']=$data['brand_id'];
		}
		if(isset($data['type_products']))
		{
		$conditions['Product.type']=$data['type_products'];
		}
		$conditions['Product.active']=1;
		$this->Product->virtualFields = array(
			'product_name' => "CONCAT('(', Product.code , ') ',Product.name )"
			);
		$Product=$this->Product->find('list',array(
			'conditions'=>$conditions,
			'fields'=>array(
				'Product.id',
				'product_name',
				)
			));
		echo json_encode($Product);
		exit;
	}
	public function product_list_get_ajax()
	{
		$data=$this->request->data;
		if(!empty($data['product_type_id']))
		{
			$product_type_id=$data['product_type_id'];
		}
		if(!empty($data['brand_name_id']))
		{
			$brand_name_id=$data['brand_name_id'];
		}
		$conditions=array();
		if(!empty($product_type_id))
		{
			$conditions['Product.product_type_id']=$product_type_id;
		}
		if(!empty($data['warehouse_id']))
		{
			$conditions['Warehouse.Id']=$data['warehouse_id'];
		}
		if(!empty($brand_name_id))
		{
			$conditions['Product.brand_id']=$brand_name_id;
		}
		$conditions['Product.active']=1;
		$this->Product->virtualFields = array(
			'product_name' => "CONCAT(Product.name, ' ', Product.code)"
			);
		$product=$this->Product->find('all',
			array(
				"joins"=>array(
					array(
						"table"=>'stocks',
						"alias"=>'Stock',
						"type"=>'inner',
						"conditions"=>array('Product.id=Stock.product_id'),
						),
					array(
						"table"=>'warehouses',
						"alias"=>'Warehouse',
						"type"=>'inner',
						"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
						),
					),
				'conditions'=>$conditions,
				'fields'=>array(
					'Product.*',
// 'Warehouse.*',
					)
				));
		$return['option']='<option value="">SELECT</option>';
		if(!empty($product))
		{
			$return['result']='Success';
			foreach ($product as $key => $value) {
				$return['option']=$return['option'].'<option value='.$value['Product']['id'].'>'.$value['Product']['product_name'].'</option>';
			}
		}
		else
		{
			$return['result']='Error';
			$return['message']='Empty';
		}
		echo json_encode($return);
		exit;
	}
	public function product_list_get_ajax_search()
	{
		$data=$this->request->data;
		if(!empty($data['product_type_id']))
		{
			$product_type_id=$data['product_type_id'];
		}
		if(!empty($data['brand_name_id']))
		{
			$brand_name_id=$data['brand_name_id'];
		}
		$conditions=array();
		$conditions['Stock.quantity >']=0;
		if(!empty($product_type_id))
		{
			$conditions['Product.product_type_id']=$product_type_id;
		}
		if(!empty($data['warehouse_id']))
		{
			$conditions['Warehouse.Id']=$data['warehouse_id'];
		}
		if(!empty($brand_name_id))
		{
			$conditions['Product.brand_id']=$brand_name_id;
		}
		$conditions['Product.active']=1;
		$conditions['Product.type']=array(2,5,8);
		$this->Product->virtualFields = array(
			'product_name' => "CONCAT(Product.name, ' ', Product.code)"
			);
		$this->Product->unbindModel(
			array('hasMany' => array(
				'PurchaseReturnItem',
				'PurchasedItem',
				'SaleItem',
				'SalesReturnItem',
				'StockLog',
				'Stock',
				'UnwantedList',
				))
			);
		$product=$this->Product->find('all',
			array(
				"joins"=>array(
					array(
						"table"=>'stocks',
						"alias"=>'Stock',
						"type"=>'inner',
						"conditions"=>array('Product.id=Stock.product_id'),
						),
					array(
						"table"=>'warehouses',
						"alias"=>'Warehouse',
						"type"=>'inner',
						"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
						),
					),
				'conditions'=>$conditions,
				'order' => array('Product.priority' => 'ASC'),
				'fields'=>array(
					'Product.*',
					)
				));
		$return['option']='<option value="">SELECT</option>';
		if(!empty($product))
		{
			$return['result']='Success';
			foreach ($product as $key => $value) {
				$return['option']=$return['option'].'<option value='.$value['Product']['id'].'>'.$value['Product']['product_name'].'</option>';
			}
		}
		else
		{
			$return['result']='Error';
			$return['message']='Empty';
		}
		echo json_encode($return);
		exit;
	}
	public function get_product_quantity()
	{
		$warehouse_id=$this->request->data['warehouse_id'];
		$product_id=$this->request->data['product_id'];
		$quantity=$this->Stock->find('first',array(
			"joins"=>array(
				array(
					"table"=>'units',
					"alias"=>'Unit',
					"type"=>'inner',
					"conditions"=>array('Product.unit_id=Unit.id'),
					),
				),
			'conditions'=>array('Stock.product_id'=>$product_id,'Stock.warehouse_id'=>$warehouse_id),
			'fields'=>array('Stock.quantity','Unit.level','Product.no_of_piece_per_unit','Unit.id','Product.tray_occupation')
			));
// }else{
		if($quantity['Product']['tray_occupation']!=0)
		{
$Stock_quantity=$quantity['Stock']['quantity']/$quantity['Product']['tray_occupation'];
							$whole = floor($Stock_quantity);      
							$fraction = $quantity['Stock']['quantity']%$quantity['Product']['tray_occupation']; 
							$pieces=number_format($fraction);
							$tray=$whole;
					$quantity['Product']['number_of_tray']=$tray.'/'.$pieces;
		}
		else
		{
       $quantity['Product']['number_of_tray']=0;
		}
		$unit_id=$this->request->data['unit_id'];
		if($unit_id != ""){
			$SaleController = new SaleController;
			$UnitLevelConvert=$SaleController->UnitLevelConvert($product_id,$unit_id);
			$sale_unit_level=$UnitLevelConvert['sale_unit_level'];
			$quantity['Unit']['level']=$sale_unit_level;
		}


// }
		echo json_encode($quantity);
		exit;
	}
	public function stock_search_ajax()
	{
		$conditions=array();
		$conditions['Stock.flag']=1;
		$data=$this->request->data;
		if(!empty($data['brand_in_display']))
		{
			$conditions['Product.brand_id']=$data['brand_in_display'];
			if($data['brand_in_display']=='G')
			{
				$conditions['Product.brand_id']=0;
			}
		}
		if(!empty($data['product_type_id']))
		{
			$conditions['ProductType.id']=$data['product_type_id'];
		}
		if(!empty($data['warehouse_id']))
		{
			$conditions['Warehouse.id']=$data['warehouse_id'];
		}
		if(!empty($data['product_id']))
		{
			$conditions['Product.id']=$data['product_id'];
		}
		if(!empty($data['modal']))
		{
			$conditions['VehicleModal.id']=$data['modal'];
		}
				$conditions['Product.active']=1;
		$inStock=$this->Product->find('all',array(
			"joins"=>array(
				array(
					"table"=>'stocks',
					"alias"=>'Stock',
					"type"=>'inner',
					"conditions"=>array('Product.id=Stock.product_id'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'Warehouse',
					"type"=>'inner',
					"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
					),
				),
			'conditions'=>$conditions,
			'limit'=>500,
			'fields'=>array(
				'Product.*',
				'Product.name',
				'Product.cost',
				'Product.code',
				'Product.threashold',
				'Product.mrp',
				'Unit.name',
				'ProductType.name',
				'Warehouse.name',
				'Brand.name',
				'Stock.id',
				'Stock.quantity',
				)
			));
		$total_cost=0;
		$data['row']='';
		if($inStock)
		{
			$data['data']=$inStock;
			$data['result']='Success';
		}
		else
		{
			$data['result']='Error';
		}
		pr($data);exit;
		echo json_encode($data);
		exit;
	}
	public function product_id_select_ajax()
	{
		$product_id=$this->request->data['product_id'];
		$product=$this->Product->findById($product_id);
		echo $product['Product']['product_id'];
		exit;
	}
	public function EditStock()
	{
		$datasource_Stock = $this->Stock->getDataSource();
		try {
			$datasource_Stock->begin();
			$userid=1;
			$data=$this->request->data['EditStock'];
			$id=$data['id'];
			$quantity=trim($data['stock_Quantity_new']);
			$remark=trim($data['stock_Remark']);
			$date=date('Y-m-d H:i:s');
			$work_flow='Product Updation';
			$Stock_function_return=$this->GeneralStock_Edit_Function($id,$quantity,$date,$userid,$remark,$work_flow);
			if($Stock_function_return['result']!='Success')
				throw new Exception($Stock_function_return['result'], 1);
			$datasource_Stock->commit();
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
			$datasource_Stock->rollback();
		}
		echo json_encode($return);
		exit;
	}
	public function GeneralStock_Edit_Function($stock_id,$quantity,$date,$user_id,$remark,$work_flow=null)
	{
		try {
			if(!$user_id)
				throw new Exception("Empty user Id", 1);
			if(!$stock_id)
				throw new Exception("Empty Stock Id", 1);
			if(!$date)
				throw new Exception("Empty date", 1);
			if(!$quantity && $quantity!=0)
				throw new Exception("Empty quantity Id", 1);
			if(!$remark)
				throw new Exception("Empty remark", 1);
			$this->Stock->id=$stock_id;
			$Old_Stock=$this->Stock->read();
			if(!$this->Stock->saveField('quantity',$quantity ))
				throw new Exception("Error Processing Stock quantity Updation", 1);
			if(!$this->Stock->saveField('modified_by',$user_id ))
				throw new Exception("Error Processing Stock modified_by Updation", 1);
			if(!$this->Stock->saveField('flag','1' ))
				throw new Exception("Error Processing Stock flag Updation", 1);
			if(!$this->Stock->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date)) ))
				throw new Exception("Error Processing Stock updated_at Updation", 1);
			$Stock=$this->Stock->read();
			unset($Stock['Stock']['id']);
			unset($Stock['Stock']['flag']);
			unset($Stock['Stock']['modified_by']);
			$old_quantity=$Old_Stock['Stock']['quantity'];
			$new_quantity=$Stock['Stock']['quantity'];
			$Stock['Stock']['quantity_in']=0;
			$Stock['Stock']['quantity_out']=0;
			if($old_quantity>$new_quantity)
			{
				$quantity_diff=$old_quantity-$new_quantity;
				$Stock['Stock']['quantity_out']=$quantity_diff;
			}
			else
			{
				$quantity_diff=$new_quantity-$old_quantity;
				$Stock['Stock']['quantity_in']=$quantity_diff;
			}
			$Stock['Stock']['remark']=$remark;
			$function_stockLog_return=$this->stock_logs($Stock['Stock'],$work_flow);
			if($function_stockLog_return['result']!='Success')
				throw new Exception($function_stockLog_return['result'], 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function GeneralStock_PurchaseEdit_Function($stock_id,$quantity,$date,$user_id,$remark,$work_flow=null)
	{
		try {
			if(!$user_id)
				throw new Exception("Empty user Id", 1);
			if(!$stock_id)
				throw new Exception("Empty Stock Id", 1);
			if(!$date)
				throw new Exception("Empty date", 1);
			if(!$quantity && $quantity!=0)
				throw new Exception("Empty quantity Id", 1);
			if(!$remark)
				throw new Exception("Empty remark", 1);
			$this->Stock->id=$stock_id;
			$Old_Stock=$this->Stock->read();
			if(!$this->Stock->saveField('quantity',$quantity ))
				throw new Exception("Error Processing Stock quantity Updation", 1);
			if(!$this->Stock->saveField('modified_by',$user_id ))
				throw new Exception("Error Processing Stock modified_by Updation", 1);
			if(!$this->Stock->saveField('flag','1' ))
				throw new Exception("Error Processing Stock flag Updation", 1);
			if(!$this->Stock->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date)) ))
				throw new Exception("Error Processing Stock updated_at Updation", 1);
			$Stock=$this->Stock->read();
			unset($Stock['Stock']['id']);
			unset($Stock['Stock']['flag']);
			unset($Stock['Stock']['modified_by']);
			$old_quantity=$Old_Stock['Stock']['quantity'];
			$new_quantity=$Stock['Stock']['quantity'];
			$Stock['Stock']['quantity_in']=0;
			$Stock['Stock']['quantity_out']=0;
			if($old_quantity>$new_quantity)
			{
				$quantity_diff=$old_quantity-$new_quantity;
				$Stock['Stock']['quantity_out']=$quantity_diff;
			}
			else
			{
				$quantity_diff=$new_quantity-$old_quantity;
				$Stock['Stock']['quantity_in']=$quantity_diff;
			}
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function General_FixedStock_Edit_Function($fixed_stock_id,$quantity,$date,$user_id,$remark)
	{
		try {
			if(!$user_id)
				throw new Exception("Empty user Id", 1);
			if(!$fixed_stock_id)
				throw new Exception("Empty Stock Id", 1);
			if(!$date)
				throw new Exception("Empty date", 1);
			if(!$quantity && $quantity!=0)
				throw new Exception("Empty quantity Id", 1);
			if(!$remark)
				throw new Exception("Empty remark", 1);
			$this->FixedStock->id=$fixed_stock_id;
			if(!$this->FixedStock->saveField('quantity',$quantity ))
				throw new Exception("Error Processing FixedStock quantity Updation", 1);
			if(!$this->FixedStock->saveField('modified_by',$user_id ))
				throw new Exception("Error Processing FixedStock modified_by Updation", 1);
			if(!$this->FixedStock->saveField('flag','1' ))
				throw new Exception("Error Processing FixedStock flag Updation", 1);
			if(!$this->FixedStock->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date)) ))
				throw new Exception("Error Processing FixedStock updated_at Updation", 1);
			$FixedStock=$this->FixedStock->read();
			unset($FixedStock['FixedStock']['id']);
			unset($FixedStock['FixedStock']['flag']);
			unset($FixedStock['FixedStock']['modified_by']);
			$FixedStock['FixedStock']['remark']=$remark;
			$function_FixedStockLog_return=$this->Fixedstock_logs($FixedStock['FixedStock']);
			if($function_FixedStockLog_return['result']!='Success')
				throw new Exception($function_FixedStockLog_return['result'], 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function delete($id)
	{
		$this->Stock->id =$id;
		if($this->Stock->delete())
		{
			$this->Session->setFlash(__('Stock  has been deleted Successfully!.'));
			$this->redirect(array('action'=>'index'));
		}
		else
		{
			$this->Session->setFlash(__('Error Occured'));
			$this->redirect(array('action'=>'index'));
		}
	}
	public function delete_stock_ajax($id)
	{
		$this->Stock->id =$id;
		if($this->Stock->delete())
		{
			$return['result']='Success';
		}
		else
		{
			$return['result']='Error';
		}
		echo json_encode($return);
		exit;
	}
	public function product_name_search()
	{
		$product_name=$this->request->data['product_name'];
		$product_name=trim($product_name);
		$product=$this->Product->find('first',array('conditions'=>array('Product.name'=>$product_name)));
		if(!empty($product)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}
	public function product_id_search()
	{
		$product_id=$this->request->data['product_id'];
		$product_id=trim($product_id);
		$product=$this->Product->find('first',array('conditions'=>array('Product.code'=>$product_id)));
		if(!empty($product)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}
	public function product_type_search()
	{
		$prdct_type=$this->request->data['prdct_type'];
		$prdct_type=trim($prdct_type);
		$product_type=$this->ProductType->find('first',array('conditions'=>array('ProductType.name'=>$prdct_type)));
		if(!empty($product_type)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}
	public function warehouse_search()
	{
		$warehouse_name=$this->request->data['warehouse_name'];
		$warehouse_name=trim($warehouse_name);
		$Warehouse=$this->Warehouse->find('first',array('conditions'=>array('Warehouse.name'=>$warehouse_name)));
		if(!empty($Warehouse)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}
	public function stock_logs($stock_logs_array,$work_flow=null)
	{
		try {
			$this->Product->unbindModel(
				array('hasMany' => array(
					'PurchaseReturnItem',
					'PurchasedItem',
					'SaleItem',
					'SalesReturnItem',
					'StockLog',
					'Stock',
					'UnwantedList',
					)
				));
			$Product=$this->Product->find('first',array(
				'conditions'=>array('Product.id'=>$stock_logs_array['product_id']),
				'fields'=>array(
					'Product.mrp',
					'Product.cost',
					'Product.wholesale_price',
					)
				));
			if(!$Product)
				throw new Exception("Empty Product", 1);
			$stock_logs_array['mrp']=$Product['Product']['mrp'];
			$stock_logs_array['cost']=$Product['Product']['cost'];
			$stock_logs_array['wholesale_price']=$Product['Product']['wholesale_price'];
			$stock_logs_array['created_at']=date('Y-m-d H:i:s');
			$this->StockLog->create();
			if(!$this->StockLog->save($stock_logs_array))
				throw new Exception("Cant Create Stock Log Data", 1);
			if(empty($stock_logs_array['quantity_in']))
				$stock_logs_array['quantity_in']=0;
			if(empty($stock_logs_array['quantity_out']))
				$stock_logs_array['quantity_out']=0;
			if($work_flow=='Product Creation' || $work_flow=='Product Updation'){
				$stock_logs_quantity=$stock_logs_array['quantity_in']-$stock_logs_array['quantity_out'];
				$total_cost=$Product['Product']['cost']*$stock_logs_quantity;
				$stock_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'STOCK'));
				//$this->AccountHead->id=$stock_account_head_id;
				//$this->AccountHead->saveField('opening_balance',$this->AccountHead->field('opening_balance')+$total_cost);
			}
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();


		}
		return $return;
	}
	public function Fixedstock_logs($fixed_stock_logs_array)
	{
		try {
			$this->FixedStockLog->create();
			if(!$this->FixedStockLog->save($fixed_stock_logs_array))
				throw new Exception("Cant Create Stock Log Data", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		return $return;
	}
	public function StockUpdation()
	{
		$data=$this->request->data;
		$stock_id=$data['id'];
		$quantity=$data['quantity'];
		$modified_by=$data['modified_by'];
		$page_id=$data['page_id'];
		$this->Stock->id=$stock_id;
		$old_stock_qty=$this->Stock->field('quantity');
		$qty=$old_stock_qty+$quantity;
		if($qty>=0)
		{
			if($quantity!=$old_stock_qty){
				$Stock_data=array(
					'product_id_fk'=>$this->Stock->field('product_id_fk'),
					'quantity'=>$this->Stock->field('quantity')+$quantity,
					'batch_no'=>$this->Stock->field('batch_no'),
					'remark'=>'Stock Updated',
					'modified_by'=>$modified_by,
					'created_by'=>$modified_by,
					);
				$this->Stock->saveField('quantity',$this->Stock->field('quantity')+$quantity );
				$this->Stock->saveField('modified_by',$modified_by );
				$this->Session->setFlash(__('Stock Edited Successfully'));
				$this->stock_logs($Stock_data);
			}
		}
		else
		{
			$this->Session->setFlash(__('Insufficient Stock!'));
			return $this->redirect(array('controller'=>$data['controller'],'action' => $data['action'],$data['page_id']));
		}
	}
	public function brand_name_search()
	{
		$prdct_type=$this->request->data['brand_name'];
		$prdct_type=trim($prdct_type);
		$product_type=$this->Brand->find('first',array('conditions'=>array('Brand.name'=>$prdct_type)));
		if(!empty($product_type)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}
	public function stock_updation()
	{	
		$Stock=$this->Stock->find('all');
		$datasource_Stock = $this->Stock->getDataSource();
		$datasource_Product = $this->Product->getDataSource();
// pr(count($Stock));
		foreach ($Stock as $key => $value) {
			try {
// $mrp=$value['Stock']['quantity'];
				$mrp=$value['Product']['mrp'];
// $datasource_Stock->begin();
				$datasource_Product->begin();
// $this->Stock->id=$value['Stock']['id'];
// if(!$this->Stock->saveField('quantity',$quantity))
// 	throw new Exception("quantity Updation Error $quantity", 1);
				$this->Product->id=$value['Product']['id'];
// if(!$this->Product->saveField('mrp',$mrp))
// 	throw new Exception("Mrp Updation Error $mrp", 1);
				if(!$this->Product->saveField('wholesale_price',$mrp))
					throw new Exception("Mrp Updation Error $mrp", 1);
				$datasource_Product->commit();
// $datasource_Stock->commit();
			} catch (Exception $e) {
				pr($e->getMessage());
				$datasource_Product->rollback();
// $datasource_Stock->rollback();
			}
		}
		exit;
	}
	public function StockTransferList(){
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Stock/StockTransferList'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$conditions=[];
		$conditions['StockTransfer.status']=2;
		$conditions['StockTransfer.transfer_type']=1;
		$user_branch_id = $this->Session->read('User.branch_id');
		if(!empty($user_branch_id))
		{
					$branch=$this->Branch->findById($user_branch_id);
		$conditions['OR']=array(
			"StockTransfer.warehouse_to" => $branch['Branch']['warehouse_id'], 
			"StockTransfer.warehouse_from" => $branch['Branch']['warehouse_id'],	
			);
		}
		$StockTransfer_List=$this->StockTransfer->find('all',array(
			"joins"=>array(
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseFrom',
					"type"=>'inner',
					"conditions"=>array('WarehouseFrom.id=StockTransfer.warehouse_from'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseTo',
					"type"=>'inner',
					"conditions"=>array('WarehouseTo.id=StockTransfer.warehouse_to'),
					),
				),
			'conditions'=>$conditions,
			"fields"=>array(
				'StockTransfer.id',
				'StockTransfer.transfer_no',
				'StockTransfer.date',
				'StockTransfer.created_at',
				'StockTransfer.remarks',
				'WarehouseFrom.name',
				'WarehouseTo.name',
				),
			"order"=>array('StockTransfer.id desc')
			));
		$StockTransfer =[];
		foreach ($StockTransfer_List as $key => $value) {
			$SingleTransfer['id'] = $value['StockTransfer']['id'];
			$SingleTransfer['transfer_no'] = $value['StockTransfer']['transfer_no'];
			$SingleTransfer['date'] = $value['StockTransfer']['date'];
			$SingleTransfer['created_at'] = $value['StockTransfer']['created_at'];
			$SingleTransfer['WarehouseFrom'] = $value['WarehouseFrom']['name'];
			$SingleTransfer['WarehouseTo'] = $value['WarehouseTo']['name'];
			$SingleTransfer['remarks'] = $value['StockTransfer']['remarks'];
			array_push($StockTransfer, $SingleTransfer);
		}
		$this->set(compact('StockTransfer'));
	}
	public function StockTransfer($id = null){
		$user_id=1;
		$this->set('Unit',$this->Unit->find('list',array(
			'fields'=>array('id','name'),
			'order'=>array('name ASC'),
			)));
		$branch_id = $this->Session->read('User.branch_id');
	if($branch_id)
	{
		$this->set('Warehouse',$this->Warehouse->find('list',array(
		'fields'=>array('id','name'),
		'order'=>array('id ASC'),
		)));
		$warehouse_name=$this->Branch->find('first',array(
			'order'=>array('name ASC'),
			'conditions'=>array('Branch.id' =>$branch_id),
			'fields'=>array('Warehouse.id','Warehouse.name'),
			));
		$type=0;
		$warehouse=$warehouse_name['Warehouse']['name'];
		$warehouse_id=$warehouse_name['Warehouse']['id'];
		$conditions_branch=[];
$conditions_branch['BranchWarehouseMapping.branch_id']=$branch_id;
$this->Warehouse->unbindModel(
			array('hasMany' => array(
				'Executive',
				'StockLog',
				'Stock',
				))
			);
	$this->set('warehouse_to',$this->Warehouse->find('list', array(
		"joins"=>array(array(
			"table"=>'branch_warehouse_mappings',
			"alias"=>'BranchWarehouseMapping',
			"type"=>'inner',
			"conditions"=>array('BranchWarehouseMapping.warehouse_id=Warehouse.id'),
			),
		),
		'conditions' =>$conditions_branch ,
		'order'=>array('Warehouse.name ASC'),
		'fields' => array(
			'Warehouse.id',
			'Warehouse.name',
				))));
		
	
	}
	else
	{
		$this->set('Warehouse',$this->Warehouse->find('list',array(
		'fields'=>array('id','name'),
		'order'=>array('id ASC'),
		)));
		$this->set('warehouse_to',$this->Warehouse->find('list',array(
		'fields'=>array('id','name'),
		'order'=>array('id ASC'),
		)));
		$type=1;
		$warehouse="";
		$warehouse_id='';
  }
		$Transfer=$this->StockTransfer->find('first',array('order' => 'StockTransfer.id DESC',));
		if(!empty($Transfer))
		{
			$TransferNo = $Transfer['StockTransfer']['transfer_no'];
			$transfer_no=$TransferNo+1;
		}
		else
		{
			$transfer_no=1;
		}
		if (!$this->request->data){
			if(empty($id)){
				$data['StockTransfer']['type']=$type;
				$data['StockTransfer']['warehouse_from']=$warehouse_id;
			    $data['StockTransfer']['warehouse_name']=$warehouse;
				$data['StockTransfer']['transfer_no']=$transfer_no;
				$data['StockTransfer']['date']=date('d-m-Y');
				$this->request->data=$data;
			} else {
				$StockTransfer=$this->StockTransfer->findById($id);
				$StockTransferItem=$this->StockTransferItem->find('all',array('conditions'=>array('stock_transfer_id'=>$id)));
				$this->request->data=$StockTransfer;
				$this->set('StockTransferItem',$StockTransferItem);
				$this->request->data['StockTransfer']['date']=date('d-m-Y',strtotime($StockTransfer['StockTransfer']['date']));
			}
		} else {
			$datasource_StockTransfer = $this->StockTransfer->getDataSource();
			$datasource_StockTransferItem = $this->StockTransferItem->getDataSource();
			$datasource_Stock = $this->Stock->getDataSource();
			try {
				$datasource_StockTransfer->begin();
				$datasource_StockTransferItem->begin();
				$datasource_Stock->begin();
				$data=$this->request->data['StockTransfer'];
				$Transfer=$this->StockTransfer->find('first',array('order' => 'StockTransfer.id DESC',));
			if(!empty($Transfer))
			{
				$TransferNo = $Transfer['StockTransfer']['transfer_no'];
				$transfer_no=$TransferNo+1;
			}
			else
			{
				$transfer_no=1;
			}
				if(empty($id))
				{
					$StockTransfer_data=[
					'transfer_no'=>$transfer_no,
					'date'=>date('Y-m-d',strtotime($data['date'])),
					'warehouse_from'=>$data['warehouse_from'],
					'warehouse_to'=>$data['warehouse_to'],
					'remarks'=>$data['remarks'],
					'status'=>2,
					'transfer_type'=>1,
					'created_by'=>$user_id,
					'modified_by'=>$user_id,
					'created_at'=>date('Y-m-d H:i:s'),
					'updated_at'=>date('Y-m-d H:i:s'),
					'branch_id'=>$branch_id,
					];
					$stock_warehouse=$this->Executive->find('first',array('conditions'=>array('Executive.warehouse_id'=>$data['warehouse_to'])));
					if(!empty($stock_warehouse))
					{
					$StockTransfer_data['status']=1;
					$StockTransfer_data['transfer_type']=2;
					}
					$this->StockTransfer->create();
					if(!$this->StockTransfer->save($StockTransfer_data))
					{
						$errors = $this->StockTransfer->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					$stock_transfer_id=$this->StockTransfer->getLastInsertId();
					$tray_quantity=0;
					for ($k=0; $k <count($data['tray_occupation']) ; $k++) 
					{
						if($data['product'][$k]!=1)
						{
							$tray_quantity+=(int)$data['tray_occupation'][$k];
						}
					}
					if(!in_array(1, $data['product']))
					{
				 			$Product=$this->Product->findById(1);
						$StockTransferItem_data=[
						'product_id'=>1,
	                	'unit_id'=>$Product['Product']['unit_id'],
						'stock_transfer_id'=>$stock_transfer_id,
						'quantity'=>$tray_quantity,
						];
						$this->StockTransferItem->create();
						if(!$this->StockTransferItem->save($StockTransferItem_data))
						{
							$errors = $this->StockTransferItem->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}
						else
						{
							$fromStock=$this->Stock->find('first',array(
								'conditions'=>array(
									'Stock.product_id'=>1,
									'Stock.warehouse_id'=>$data['warehouse_from']
									),
								'fields'=>array('Stock.id','Stock.quantity')
								));
							if($fromStock)
							{
								$fromQty=$fromStock['Stock']['quantity']-$tray_quantity;
								$remark='Stock Transfer --'.$transfer_no.'('.$tray_quantity.')';	
								$Stock_function_return=$this->GeneralStock_Edit_Function($fromStock['Stock']['id'],$fromQty,date('Y-m-d'),$user_id,$remark);
								if($Stock_function_return['result']!='Success')
									throw new Exception($Stock_function_return['result']);
							}
							if(empty($stock_warehouse))
					        {         
							$toStock=$this->Stock->find('first',array(
								'conditions'=>array(
									'Stock.product_id'=>1,
									'Stock.warehouse_id'=>$data['warehouse_to']
									),
								'fields'=>array('Stock.id','Stock.quantity')));
							if(!$toStock)
							{
								$remark='Stock Transfer --'.$transfer_no.'('.$tray_quantity.')';	
								$flag=1;
								$Stock_function_return=$this->GeneralStock_Add_Function($data['warehouse_to'],1,$tray_quantity,date('Y-m-d'),$user_id,$remark,$flag);
								if($Stock_function_return['result']!='Success')
									throw new Exception($Stock_function_return['result'], 1);
							}
							else
							{
								$toQty=$toStock['Stock']['quantity']+$tray_quantity;
								$remark='Stock Transfer --'.$transfer_no.'('.$tray_quantity.')';	
								$Stock_function_return=$this->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
								if($Stock_function_return['result']!='Success')
									throw new Exception($Stock_function_return['result']);
							}
						}
						}
					}
					for ($i=0; $i <count($data['product']) ; $i++) {
						if($data['product'][$i]==1)
						{
						$data['quantity'][$i]=$data['quantity'][$i]+$tray_quantity;
						}
						$StockTransferItem_data=[
						'product_id'=>$data['product'][$i],
						'unit_id'=>$data['unit'][$i],
						'stock_transfer_id'=>$stock_transfer_id,
						'quantity'=>$data['quantity'][$i],
						];
						$this->StockTransferItem->create();
						if(!$this->StockTransferItem->save($StockTransferItem_data))
						{
							$errors = $this->StockTransferItem->validationErrors;
							foreach ($errors as $key => $value) {
								throw new Exception($value[0], 1);
							}
						}
						else
						{
							$fromStock=$this->Stock->find('first',array(
								'conditions'=>array(
									'Stock.product_id'=>$data['product'][$i],
									'Stock.warehouse_id'=>$data['warehouse_from']
									),
								'fields'=>array('Stock.id','Stock.quantity')
								));
							if($fromStock)
							{
								$fromQty=$fromStock['Stock']['quantity']-$data['quantity'][$i];
								$remark='Stock Transfer --'.$transfer_no.'('.$data['quantity'][$i].')';	
								$Stock_function_return=$this->GeneralStock_Edit_Function($fromStock['Stock']['id'],$fromQty,date('Y-m-d'),$user_id,$remark);
								if($Stock_function_return['result']!='Success')
									throw new Exception($Stock_function_return['result']);
							}
							if(empty($stock_warehouse))
					        {         
							$toStock=$this->Stock->find('first',array(
								'conditions'=>array(
									'Stock.product_id'=>$data['product'][$i],
									'Stock.warehouse_id'=>$data['warehouse_to']
									),
								'fields'=>array('Stock.id','Stock.quantity')));
							if(!$toStock)
							{
								$remark='Stock Transfer --'.$transfer_no.'('.$data['quantity'][$i].')';	
								$flag=1;
								$Stock_function_return=$this->GeneralStock_Add_Function($data['warehouse_to'],$data['product'][$i],$data['quantity'][$i],date('Y-m-d'),$user_id,$remark,$flag);
								if($Stock_function_return['result']!='Success')
									throw new Exception($Stock_function_return['result'], 1);
							}
							else
							{
								$toQty=$toStock['Stock']['quantity']+$data['quantity'][$i];
								$remark='Stock Transfer --'.$transfer_no.'('.$data['quantity'][$i].')';	
								$Stock_function_return=$this->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
								if($Stock_function_return['result']!='Success')
									throw new Exception($Stock_function_return['result']);
							}
						}
						}
					}
				}
				else
				{
					$data=$this->request->data['StockTransfer'];
				}
				$datasource_Stock->commit();
				$datasource_StockTransfer->commit();
				$datasource_StockTransferItem->commit();
				$return['result']="success";
			} catch (Exception $e){
				$datasource_StockTransfer->rollback();
				$datasource_StockTransferItem->rollback();
				$datasource_Stock->rollback();
				$return['result']=$e->getMessage();
			}
			$this->Session->setFlash(__($return['result']));
			$this->redirect(array('controller' => 'Stock','action' =>'StockTransferList'));
		}
	}
	public function DamagedStock(){
		$PermissionList = $this->Session->read('PermissionList');
		$menu_id = $this->Menu->field(
			'Menu.id',
			array('action ' => 'Stock/DamagedStock'));
		// if(!in_array($menu_id, $PermissionList))
		// {
		// 	$this->Session->setFlash("Permission denied");
		// 	return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
		// }
		$damaged_stock_id=2;
		$user_id=1;
		$ProductType=$this->ProductType->find('list');
		$this->set(compact('ProductType'));
		$Brand=$this->Brand->find('list');
		$Brand['G']='GENERAL';
		ksort($Brand);
		$this->set(compact('Brand'));
		$Product=$this->Product->find('list');
		$this->set(compact('Product'));
		$Warehouse=$this->Warehouse->find('list');
		unset($Warehouse[2]);
		$this->set(compact('Warehouse'));
		$DamagedStock=$this->DamagedStock->find('all',array(
			'joins'=>array(
				array(
					"table"=>'product_types',
					"alias"=>'ProductType',
					"type"=>'inner',
					"conditions"=>array('Product.product_type_id=ProductType.id'),
					),
				array(
					"table"=>'brands',
					"alias"=>'Brand',
					"type"=>'inner',
					"conditions"=>array('Product.brand_id=Brand.id'),
					),
				),
			'fields'=>array(
				'DamagedStock.*',
				'Brand.name',
				'ProductType.name',
				'Warehouse.name',
				'Product.cost',
				'Product.mrp',
				'Product.name',
				)
			));
		$this->set(compact('DamagedStock'));
		if(!$this->request->data)
		{
			$this->request->data='';
		}
		else
		{
			$datasource_DamagedStock = $this->DamagedStock->getDataSource();
			$datasource_Stock = $this->Stock->getDataSource();
			try {
				$datasource_DamagedStock->begin();
				$datasource_Stock->begin();
				$data=$this->request->data['DamagedStock'];
				$quantity=$data['quantity'];
				$product_id=$data['product_id'];
				$warehouse_id=$data['warehouse_id'];
				$remark=$data['remark'].'('.$quantity.')';
				unset($data['brand']);
				unset($data['actual_qty']);
				unset($data['product_type']);
				$data['created_at']=date('Y-m-d H:i:s');
				$data['updated_at']=date('Y-m-d H:i:s');
				$this->DamagedStock->create();
				if(!$this->DamagedStock->save($data))
				{
					$errors = $this->DamagedStock->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$fromStock=$this->Stock->find('first',array(
					'conditions'=>array(
						'Stock.product_id'=>$product_id,
						'Stock.warehouse_id'=>$warehouse_id
						),
					'fields'=>array('Stock.id','Stock.quantity')
					));
				if($fromStock)
				{
					$fromQty=$fromStock['Stock']['quantity']-$quantity;
					$Stock_function_return=$this->GeneralStock_Edit_Function($fromStock['Stock']['id'],$fromQty,date('Y-m-d'),$user_id,$remark);
					if($Stock_function_return['result']!='Success')
						throw new Exception($Stock_function_return['result']);
				}
				$toStock=$this->Stock->find('first',array(
					'conditions'=>array(
						'Stock.product_id'=>$product_id,
						'Stock.warehouse_id'=>$damaged_stock_id
						),
					'fields'=>array('Stock.id','Stock.quantity')));
				if(!$toStock)
				{
					$flag=1;
					$Stock_function_return=$this->GeneralStock_Add_Function(2,$data['product_id'],$quantity,strtotime($data['created_at']),$user_id,$remark,$flag);
					if($Stock_function_return['result']!='Success')
						throw new Exception($Stock_function_return['result'], 1);
				}
				else
				{
					$toQty=$toStock['Stock']['quantity']+$quantity;
					$Stock_function_return=$this->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
					if($Stock_function_return['result']!='Success')
						throw new Exception($Stock_function_return['result']);
				}
				$datasource_DamagedStock->commit();
				$datasource_Stock->commit();
				$this->Session->setFlash('Success');
				$this->request->data='';
			} catch (Exception $e) {
				$datasource_DamagedStock->rollback();
				$datasource_Stock->rollback();
				$this->Session->setFlash($e->getMessage());
			}
			$this->redirect(array('controller' => 'Stock','action' =>'DamagedStock'));
		}
	}
	public function ViewStockTransfer($id){
		$StockTransfer = $this->StockTransfer->findById($id);
		$StockTransfer['StockTransfer']['from_warehouse']= $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $StockTransfer['StockTransfer']['warehouse_from']));
		$StockTransfer['StockTransfer']['to_warehouse']= $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $StockTransfer['StockTransfer']['warehouse_to']));
		$StockTransferitems=$this->StockTransferItem->find('all',array(
			"joins"=>array(
				array(
					"table"=>'units',
					"alias"=>'Unit',
					"type"=>'left',
					"conditions"=>array('StockTransferItem.unit_id=Unit.id'),
					),
				),
			"conditions"=>array('stock_transfer_id'=>$id),
			"fields"=>array(
				'StockTransferItem.*',
				'Product.name',
				'Product.no_of_piece_per_unit',
				'Unit.name',
				'Unit.level',
				),
			));

		$this->set(compact('StockTransferitems','StockTransfer'));
	}
//Filter Excel generation
	public function FilterExcel($brand_id,$product_type_id,$warehouse_id,$product_id)
	{
		$date  =date('Y-m-d h:i:sa');
		$this->response->download($date.".csv");
		$conditions=array();
		$conditions['Stock.flag']=1;
		if(($brand_id)!='All')
		{
			$conditions['Product.brand_id']=$brand_id;
			if($brand_id=='G')
			{
				$conditions['Product.brand_id']=0;
			}
		}
		if(($product_type_id)!='All')
		{
			$conditions['ProductType.id']=$product_type_id;
		}
		if(($warehouse_id)!='All')
		{
			$conditions['Warehouse.id']=$warehouse_id;
		}
		if(($product_id)!='All')
		{
			$conditions['Product.id']=$product_id;
		}
		$user_branch_id=$this->Session->read('User.branch_id');
		if($user_branch_id)
		{
			$branch=$this->Branch->findById($user_branch_id);
			$conditions['Warehouse.id']=$branch['Branch']['warehouse_id'];
		}
				$conditions['Product.active']=1;
		$inStock=$this->Product->find('all',array(
			"joins"=>array(
				array(
					"table"=>'stocks',
					"alias"=>'Stock',
					"type"=>'inner',
					"conditions"=>array('Product.id=Stock.product_id'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'Warehouse',
					"type"=>'inner',
					"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
					),
				),
			'conditions'=>$conditions,
// 'limit'=>500,
			'fields'=>array(
				'Product.*',
				'Unit.name',
				'ProductType.name',
				'Warehouse.name',
				'Brand.name',
				'Stock.*',
				)
			));
		$total_cost=0;
		$data['row']='';
		if($inStock)
		{
			foreach ($inStock as $key => $value) {
				$total_cost+=$value['Product']['cost']*$value['Stock']['quantity'];
			}
			$data['total_cost']=$total_cost;
			$this->set('Stock',$inStock);
		}

		else
		{
			$data['result']='Error';
		}

		$this->layout = 'ajax';
		return false;
		exit;
	}
	public function FilterExcelNew($brand_id,$product_type_id,$warehouse_id,$product_id)
	{
		$date  =date('Y-m-d h:i:sa');
		$user_branch_id=$this->Session->read('User.branch_id');
		if($user_branch_id)
		{
			$branch=$this->Branch->findById($user_branch_id);
			$conditions['Warehouse.id']=$branch['Branch']['warehouse_id'];
		}
		$this->response->download($date.".csv");
		$conditions=array();
		$conditions['Stock.flag']=1;
		if(($brand_id)!='All')
		{
			$conditions['Product.brand_id']=$brand_id;
			if($brand_id=='G')
			{
				$conditions['Product.brand_id']=0;
			}
		}
		if(($product_type_id)!='All')
		{
			$conditions['ProductType.id']=$product_type_id;
		}
		if(($warehouse_id)!='All')
		{
			$conditions['Warehouse.id']=$warehouse_id;
		}
		if(($product_id)!='All')
		{
			$conditions['Product.id']=$product_id;
		}
				$conditions['Product.active']=1;
		$inStock=$this->Product->find('all',array(
			"joins"=>array(
				array(
					"table"=>'stocks',
					"alias"=>'Stock',
					"type"=>'inner',
					"conditions"=>array('Product.id=Stock.product_id'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'Warehouse',
					"type"=>'inner',
					"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
					),
				),
			'conditions'=>$conditions,
// 'limit'=>500,
			'fields'=>array(
				'Product.*',
				'Unit.name',
				'ProductType.name',
				'Warehouse.name',
				'Brand.name',
				'Stock.*',
				)
			));

		$total_cost=0;
		$data['row']='';
		if($inStock)
		{
			foreach ($inStock as $key => $value) {
				$total_cost+=$value['Product']['cost']*$value['Stock']['quantity'];
			}
			$data['total_cost']=$total_cost;
			$this->set('Stock',$inStock);
		}
		else
		{
			$data['result']='Error';
		}
		$this->layout = 'ajax';
		return false;
		exit;
	}
	public function stock_data_datatable() {
		$requestData=$this->request->data;
		$columns = array( 
// datatable column index  => database column name
			0 => 'Product.code',
			1 => 'Product.name',
			2 =>'ProductType.name', 
			3 =>'TypeOfProduct.name', 
			4 => 'Brand.name',
			5 => 'Unit.name',
			6 => 'Product.cost',
			7 =>'Warehouse.name', 
			8 => 'Stock.quantity',
			9 => 'Product.mrp',
			10 => 'line_total',
			11 => 'Product.cost',
			12 => 'Product.action',
			);
		$user_branch_id=$this->Session->read('User.branch_id');


		$conditions=$this->Session->read('stock_conditions');
		if(!$conditions)
		{
			$conditions['Stock.quantity !=']=0;
			if($user_branch_id)
			{
				$branch=$this->Branch->findById($user_branch_id);
				$conditions['Warehouse.id']=$branch['Branch']['warehouse_id'];
			}
		}
				$conditions['Product.active']=1;
$this->Product->unbindModel(array('hasMany' => array('PurchaseReturnItem','PurchasedItem','SaleItem','SalesReturnItem','StockLog','UnwantedList','Stock')));
// getting total number records without any search
		$totalData=$this->Product->find('count',array(
			"joins"=>array(
				array(
					"table"=>'stocks',
					"alias"=>'Stock',
					"type"=>'inner',
					"conditions"=>array('Product.id=Stock.product_id'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'Warehouse',
					"type"=>'inner',
					"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
					),
				),
			'conditions'=>$conditions,
			));
$totalFiltered=$totalData;  // when there is no search parameter then total number rows = total number filtered rows.
//pr($conditions);
if( !empty($requestData['search']['value']) ) { 
	$q=$requestData['search']['value'];
	$conditions['OR']=array(
		 'TypeOfProduct.name LIKE' =>'%'. $q . '%',
		'Product.code LIKE' =>'%'. $q . '%',
		'Product.name LIKE' =>'%'. $q . '%',
		'ProductType.name LIKE' =>'%'. $q . '%',
		'Brand.name LIKE' =>'%'. $q . '%',
		'Unit.name LIKE' =>'%'. $q . '%',
		'Warehouse.name LIKE' =>'%'. $q . '%',
		'Stock.quantity LIKE' =>'%'. $q . '%',
		'Product.mrp LIKE' =>'%'. $q . '%',
		'Product.cost LIKE' =>'%'. $q . '%',
		'Product.wholesale_price LIKE' =>'%'. $q . '%',
		'Product.threashold LIKE' =>'%'. $q . '%',
		);

	$totalFiltered=$this->Product->find('count',array(
		"joins"=>array(
			array(
				"table"=>'stocks',
				"alias"=>'Stock',
				"type"=>'inner',
				"conditions"=>array('Product.id=Stock.product_id'),
				),
			array(
				"table"=>'warehouses',
				"alias"=>'Warehouse',
				"type"=>'inner',
				"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
				),
			),
		'conditions'=>$conditions,
		));
}
//order only fields that are field of datatabse table
		$order= '';
		if($requestData['order'][0]['column']<= 9)
		{
			$order =  $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'];
		}
$this->Product->unbindModel(array('hasMany' => array('PurchaseReturnItem','PurchasedItem','SaleItem','SalesReturnItem','StockLog','UnwantedList','Stock')));
$StockData=$this->Product->find('all',array(
	"joins"=>array(
		array(
			"table"=>'stocks',
			"alias"=>'Stock',
			"type"=>'inner',
			"conditions"=>array('Product.id=Stock.product_id'),
			),
		array(
			"table"=>'warehouses',
			"alias"=>'Warehouse',
			"type"=>'inner',
			"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
			),
		),
	'conditions'=>$conditions,
	'offset'=>$requestData['start'],
	'limit'=>$requestData['length'],
	'order'=> $order,
	'fields'=>array(
		'Warehouse.name',
		'Product.name',
		'Product.code',
		'Product.threashold',
		'Unit.name',
		'ProductType.name',
		'Brand.name',
		'Product.id',
		'Stock.id',
		'Stock.quantity',
		'Product.name',
		'Product.cost',
		'Product.type',
		'Product.mrp',
		'Product.wholesale_price',
		'Product.no_of_piece_per_unit',
		'Unit.level',
		 'TypeOfProduct.name',

		)
	));

$StockData_array=[];
$total_quantity=0;
$total_rate=0;
foreach ($StockData as $key => $value) {

	if(!$value['Brand']['name'])
		$StockData[$key]['Brand']['name']='GENERAL';			
	$StockData[$key]['Product']['name']='<span class="product_name">'.$value['Product']['name'].'</span><span class="stock_id" style="display:none;">'.$value['Stock']['id'].'</span>';
	$total_quantity+=$value['Stock']['quantity'];
	if($value['Product']['type']==2 || $value['Product']['type']==8)
	{
		$value['Product']['mrp']=$value['Product']['wholesale_price'];
	}
	$StockData[$key]['Product']['mrp']=floatval($value['Product']['mrp']);
	$total_rate+=$value['Product']['mrp'];
	$line_total=$value['Stock']['quantity']*$value['Product']['mrp'];
	$StockData[$key]['Stock']['line_total']=number_format($line_total,2);
	$pieces_qty=number_format($value['Stock']['quantity']);
	if($value['Unit']['level'] == 2){
		if($value['Stock']['quantity']!= 0){
			$quantity=$value['Stock']['quantity']/$value['Product']['no_of_piece_per_unit'];
		}else{
			$quantity=number_format($value['Stock']['quantity']);
		}
		if($quantity<1){
			$pieces=number_format($value['Stock']['quantity']);
			$cases=0;
		}else{
			$whole = floor($quantity);      
			$fraction = floatval($value['Stock']['quantity'])%$value['Product']['no_of_piece_per_unit']; 
			$pieces=number_format($fraction);
			$cases=$whole;
		}
	}

	else{
		$quantity=floatval($value['Stock']['quantity']);
		$cases=0;
		$pieces=$quantity;
	}

	if($value['Unit']['level'] == 2){
		$StockData[$key]['Stock']['quantity']='<span class="stock_quantity">'.$cases.'/'.$pieces.'</span>';
	}
	else
	{
		$StockData[$key]['Stock']['quantity']='<span class="stock_quantity">'.$pieces.'</span>';
	}
	$cost=$StockData[$key]['Product']['cost']*$StockData[$key]['Product']['no_of_piece_per_unit'];
	$StockData[$key]['Product']['cost']=$cost;
	$mrp=$StockData[$key]['Product']['mrp']*$StockData[$key]['Product']['no_of_piece_per_unit'];
	$StockData[$key]['Product']['mrp']=number_format($mrp,2);

	$StockData[$key]['Product']['action']='<a><i id="'.$value['Stock']['id'].'" class="fa fa-2x fa-edit edit_stock_btn"></i></a>';	
}
//sorting total/balance/recieved

		if($requestData['order'][0]['column']==10) {
			//callbackfunction
			function cmp($a, $b,$columnname,$columndir)
			{
				if ($a["Stock"][$columnname] == $b["Stock"][$columnname]) {
					return 0;
				}
				if($columndir == 'asc') {
					return ($a["Stock"][$columnname] < $b["Stock"][$columnname]) ? -1 : 1;
				}
				else if($columndir == 'desc')
				{
					return ($a["Stock"][$columnname] > $b["Stock"][$columnname]) ? -1 : 1;
				}
			}


			$columnname = $columns[$requestData['order'][0]['column']]; //column name
			$columndir = $requestData['order'][0]['dir']; //column order
			//sorting function
			usort($StockData, create_function('$a, $b', 'return cmp($a, $b, "'.$columnname.'", "'.$columndir.'");'));
		}
//}

//pr($StockData);exit;
$json_data = array(
"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
"recordsTotal"    => intval( $totalData ),  // total number of records
"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
"records"            => $StockData   // total data array
);
echo json_encode($json_data);  // send data as json format
exit;
}
public function StockFilter()
{
	$this->Session->delete('stock_conditions');
	$conditions=array();

	if(!empty($this->request->data['brand_in_display']) && $this->request->data['brand_in_display'] != 'A')
	{
		$conditions['Product.brand_id']=$this->request->data['brand_in_display'];
		if($this->request->data['brand_in_display']=='G')
		{
			$conditions['Product.brand_id']=0;
		}
	}
	if(!empty($this->request->data['product_type_id']) && $this->request->data['product_type_id'] != 'A' )
	{
		$conditions['ProductType.id']=$this->request->data['product_type_id'];
	}
	if(!empty($this->request->data['type_product']) && $this->request->data['type_product'] != 'A' )
	{
		$conditions['TypeOfProduct.id']=$this->request->data['type_product'];
	}
	if(!empty($this->request->data['product_id']) && $this->request->data['product_id'] != 'A')
	{
		$conditions['Product.id']=$this->request->data['product_id'];
	}
	if(!empty($this->request->data['warehouse_id']) && $this->request->data['warehouse_id'] != 'A')
	{
		$conditions['Stock.warehouse_id']=$this->request->data['warehouse_id'];
	}
// if(!empty($this->request->data['check_id']))
// {

	if($this->request->data['check_id']==1){
		$conditions['Stock.quantity !=']=0;	
	}
	else if($this->request->data['check_id']==0)
	{

		$conditions['Stock.quantity !=']=null;
	}
	$user_branch_id=$this->Session->read('User.branch_id');
	if($user_branch_id)
	{
		$branch=$this->Branch->findById($user_branch_id);
		$conditions['Warehouse.id']=$branch['Branch']['warehouse_id'];
	}
// }
// pr($conditions);
// exit;
	$this->Session->write('stock_conditions',$conditions);
	echo json_encode(array('null'));
	exit;
}


public function get_total_cost()
{
	$result=array();
	$conditions=[];
	$user_branch_id=$this->Session->read('User.branch_id');
	if($user_branch_id)
	{
		$branch_warehouse=$this->Branch->findById($user_branch_id);
		$conditions['Stock.warehouse_id']=$branch_warehouse['Branch']['warehouse_id'];
	}
//$conditions=$this->Session->read('stock_conditions');
	$this->Product->unbindModel(array('hasMany' => array('PurchaseReturnItem','PurchasedItem','SaleItem','SalesReturnItem','StockLog','UnwantedList','Stock')));
	$conditions['Product.active']=1;
	$StockData=$this->Product->find('all',array(
		"joins"=>array(
			array(
				"table"=>'stocks',
				"alias"=>'Stock',
				"type"=>'inner',
				"conditions"=>array('Product.id=Stock.product_id'),
				),
			array(
				"table"=>'warehouses',
				"alias"=>'Warehouse',
				"type"=>'inner',
				"conditions"=>array('Warehouse.id=Stock.warehouse_id'),
				),
			),
		'conditions'=>$conditions,
		'fields' => array('sum(Product.cost * Stock.quantity)   AS Total'),
		));
//pr($conditions);exit;

//pr($StockData);exit;

	$result=$StockData[0][0];
	echo json_encode($result);
	exit;
}
public function MslStockOrder($id=null){
	$MslStock_warehouse=$this->MslStockOrder->find('list',array(
		'fields'=>array('MslStockOrder.id','MslStockOrder.warehouse_id'),
		'order'=>array('id ASC'),
		));
	if(empty($id))
	{
		$Warehouse=$this->Warehouse->find('list',array(
			'fields'=>array('id','name'),
			'order'=>array('id ASC'),
			'conditions'=>array('Warehouse.id !='=>$MslStock_warehouse),
			));
	}
	else
	{
		$Warehouse=$this->Warehouse->find('list',array(
			'fields'=>array('id','name'),
			'order'=>array('id ASC'),
//'conditions'=>array('Warehouse.id !='=>$MslStock_warehouse),
			));
	}

	$this->Product->virtualFields = array(
		'product_name' => "CONCAT(Product.name, ' ', Product.code)"
		);
	$Product_list=$this->Product->find('list',array(
		'conditions'=>array(
			'Product.active=1',
			),
		'fields'=>[
		'Product.id',
		'Product.product_name',
		])
	);  
	$Unit=$this->Unit->find('list',array('fields'=>['id','name']));

	$this->set(compact('Product_list'));$this->set(compact('Unit'));


	$this->set(compact('Warehouse'));
	$MslStock=$this->MslStockOrder->find('first',array('fields' => array('MAX(MslStockOrder.order_no) as order_no')));
	if(!empty($MslStock))
	{
		$order_no=$MslStock[0]['order_no']+1;
	}
	else
	{
		$invoice_no=1;
		$order_no=1;
	} 
	if (!$this->request->data) 
	{
		if(empty($id))
		{
			$data['MslStockOrder']['order_no']=$order_no;
			$data['MslStockOrder']['other_value']='0';
			$data['MslStockOrder']['date']=date('d-m-Y');
			$data['MslStockOrder']['id']='';
			$data['MslStockOrder']['status']=1;
			$data['MslStockOrder']['status_flag']=0;
			$this->request->data=$data;
		}
		else
		{
			$MslStockOrder=$this->MslStockOrder->findById($id);
			$this->request->data=$MslStockOrder;
			$this->request->data['MslStockOrder']['status_flag']=1;
			$this->request->data['MslStockOrder']['id']=$id;
			$this->request->data['MslStockOrder']['date']=date('d-m-Y',strtotime($MslStockOrder['MslStockOrder']['date']));

			$MslStockOrderItem='Yes';
			$this->set('MslStockOrderItem',$MslStockOrderItem);
		}
	}
}
public function SearchProducts() {
	$query=$this->request->query['q'];
	$Product_list=$this->Product->find('list',array(
		'conditions'=>array(
			'Product.active=1',
			'Product.name LIKE' => '%'. $query . '%',
			),
		'fields'=>[
		'Product.id',
		'Product.name',
		])
	);
	$i=0;
	$ProductList=array();
	foreach ($Product_list as $key => $value) {
		if (false !== stripos($value, $query)) {
			$ProductList[$i]['text'] = $value;
			$ProductList[$i]['id'] = $key;
			$i++;
		}
	}

	$result['items'] = $ProductList;
	echo json_encode($result);
	exit;
}
public function add_msl_order_item_ajax()
{
	$user_id=1;
	$data=$this->request->data;
	$order_no=$data['order_no'];
	$warehouse_id=$data['warehouse_id'];
	$datasource_MslStockOrder = $this->MslStockOrder->getDataSource();
	$datasource_MslStockOrderItem = $this->MslStockOrderItem->getDataSource();
	$datasource_MslStock = $this->MslStock->getDataSource();
	try {
		$datasource_MslStockOrder->begin();
		$datasource_MslStockOrderItem->begin();
		$datasource_MslStock->begin();
		$MslStockOrder=$this->MslStockOrder->findByOrderNo($order_no);
		if(!$MslStockOrder)
		{
			$Msl_data=array(
				'order_no'=>$order_no,
				'warehouse_id'=>$warehouse_id,
				'date'=>date('Y-m-d',strtotime($data['date'])),
				'created_by'=>$user_id,
				'modified_by'=>$user_id,
				'created_at'=>date('Y-m-d H:i:s',strtotime($data['date'])),
				'updated_at'=>date('Y-m-d H:i:s',strtotime($data['date'])),
				);
			$this->MslStockOrder->create();
			if(!$this->MslStockOrder->save($Msl_data))
			{
				$errors = $this->MslStockOrder->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$MslStockOrder=$this->MslStockOrder->findByOrderNo($order_no);
		}
		$order_id=$MslStockOrder['MslStockOrder']['id'];
		$MslStockOrderItem=$this->MslStockOrderItem->findByProductIdAndMslOrderId($data['product_id'],$order_id);
		if($MslStockOrderItem)
			throw new Exception("Already Inserted", 1);
		$product_id=$this->Product->findById($data['product_id']);
		if($data['unit_id']==2)
		{
			$quantity=$data['quantity']*$product_id['Product']['no_of_piece_per_unit'];
		}
		else
		{
			$quantity=$data['quantity'];
		}
//$avg_qty=$data['avg'];
		$items=[
		'product_id'=>$data['product_id'],
		'quantity'=>$quantity,
		'unit'=>$data['unit_id'],
		'msl_order_id'=>$order_id,
//'last_avg_qty'=>$avg_qty,
		];
		$this->MslStockOrderItem->create();
		if(!$this->MslStockOrderItem->save($items))
		{
			$errors = $this->MslStockOrderItem->validationErrors;
			foreach ($errors as $key => $value) { throw new Exception($value[0], 1); }
		} 
		else
		{  
			$toStock=$this->MslStock->find('first',array(
				'conditions'=>array(
					'MslStock.product_id'=>$data['product_id'],
					'MslStock.warehouse_id'=>$data['warehouse_id'],
					),
				'fields'=>array('MslStock.id','MslStock.quantity')));
			if(!$toStock)
			{
				$flag=1;
				$remark='MslStock Stock ['.$order_id.','.$quantity.','.$data['warehouse_id'].']';

				$Stock_function_return=$this->GeneralMslStock_Add_Function($data['product_id'],$quantity,$data['warehouse_id'],date('Y-m-d'),$user_id,$remark,$flag);
				if($Stock_function_return['result']!='Success')
					throw new Exception($Stock_function_return['result'], 1);
			}
			else
			{
				$remark='MslStock Stock ['.$order_id.','.$quantity.','.$data['warehouse_id'].']';
				$toQty=$toStock['MslStock']['quantity']+$quantity;
				$Stock_function_return=$this->GeneralMslStock_Edit_Function($toStock['MslStock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
				if($Stock_function_return['result']!='Success')
					throw new Exception($Stock_function_return['result']);
			}
		} 
		$datasource_MslStockOrder->commit();
		$datasource_MslStockOrderItem->commit();
		$datasource_MslStock->commit();
		$return['result']='Success';
		$return['key']=$order_id;
		$return['website']=$this->webroot.'Stock/MslStockOrder/'.$order_id;
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
		$datasource_MslStockOrder->rollback();
		$datasource_MslStockOrderItem->rollback();
		$datasource_MslStock->rollback();
	}
	echo json_encode($return); exit;
}
public function GeneralMslStock_Add_Function($product_id,$quantity,$warehouse_id,$date,$user_id,$remark,$flag=1)
{
	try {
		$date =date('Y-m-d');
		if(!$user_id)
			throw new Exception("Empty user Id", 1);
		if(!$product_id)
			throw new Exception("Empty product Id", 1);
		if(!$date)
			throw new Exception("Empty date", 1);
		if(!$warehouse_id)
			throw new Exception("Empty Warehouse Id", 1);
		if(!$quantity && $quantity!=0)
			throw new Exception("Empty quantity Id", 1);
		if(!$remark)
			throw new Exception("Empty remark", 1);
		$stock_data=[
		'product_id'=>$product_id,
		'warehouse_id'=>$warehouse_id,
		'quantity'=>$quantity,
		'created_by'=>$user_id,
		'modified_by'=>$user_id,
		'flag'=>$flag,
		'created_at'=>date('Y-m-d H:i:s',strtotime($date)),
		'updated_at'=>date('Y-m-d H:i:s',strtotime($date)),
		];
		$this->MslStock->create();
		if(!$this->MslStock->save($stock_data))
		{
			$errors = $this->MslStock->validationErrors;
			foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
			}
		}
		$Stock=$this->MslStock->read();
		unset($Stock['MslStock']['id']);
		unset($Stock['MslStock']['flag']);
		unset($Stock['MslStock']['modified_by']);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}
public function GeneralMslStock_Edit_Function($stock_id,$quantity,$date,$user_id,$remark)
{
	try {
		if(!$user_id)
			throw new Exception("Empty user Id", 1);
		if(!$stock_id)
			throw new Exception("Empty Stock Id", 1);
		if(!$date)
			throw new Exception("Empty date", 1);
		if(!$quantity && $quantity!=0)
			throw new Exception("Empty quantity Id", 1);
		if(!$remark)
			throw new Exception("Empty remark", 1);
		$this->MslStock->id=$stock_id;
		if(!$this->MslStock->saveField('quantity',$quantity ))
			throw new Exception("Error Processing Stock quantity Updation", 1);
		if(!$this->MslStock->saveField('modified_by',$user_id ))
			throw new Exception("Error Processing Stock modified_by Updation", 1);
		if(!$this->MslStock->saveField('flag','1' ))
			throw new Exception("Error Processing Stock flag Updation", 1);
		if(!$this->MslStock->saveField('updated_at',date('Y-m-d H:i:s',strtotime($date)) ))
			throw new Exception("Error Processing Stock updated_at Updation", 1);

		$Stock=$this->MslStock->read();
		unset($Stock['MslStock']['id']);
		unset($Stock['MslStock']['flag']);
		unset($Stock['MslStock']['modified_by']);
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	return $return;
}
public function get_mslstockorder_item_ajax($id)
{

	$user_role_id=$this->Session->read('UserRole.id');    
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='MslStockOrderItem.id';
	$columns[]='Product.name';
	$columns[]='MslStockOrderItem.unit';
	$columns[]='MslStockOrderItem.avg';
	$columns[]='MslStockOrderItem.quantity';
	$columns[]='MslStockOrder.id';
	$conditions=[];
	$conditions['MslStockOrderItem.msl_order_id']=$id;
	$totalData=$this->MslStockOrderItem->find('count',['conditions'=>$conditions]);
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'MslStockOrderItem.id LIKE' =>'%'. $q . '%',
			'Product.name LIKE' =>'%'. $q . '%',
			'MslStockOrderItem.quantity LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->MslStockOrderItem->find('count',[
			'conditions'=>$conditions,
			]);
	}
	$Data=$this->MslStockOrderItem->find('all',array(
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
//'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'order'=>array('MslStockOrderItem.id DESC'),
		'fields'=>array(
			'MslStockOrder.id',
			'MslStockOrder.status',
			'MslStockOrderItem.id',
			'MslStockOrderItem.quantity',
			'MslStockOrderItem.unit',
			'MslStockOrderItem.product_id',
			'Product.id',
			'Product.name',
			'Product.no_of_piece_per_unit',
			)
		));
	foreach ($Data as $key => $value) {
		$Data[$key]['MslStockOrderItem']['key']=$key+1;
		$Data[$key]['Product']['name']="<input value='".$value['Product']['name']."' class='form-control' type='text' style='width:100%;text-align:right' readonly><input value='".$value['Product']['id']."' class='table_id' type='hidden'>";
		$Data[$key]['Product']['name'].="<input value='".$value['MslStockOrderItem']['id']."' class='table_id' type='hidden'>";
		$Data[$key]['MslStockOrderItem']['status']='';
		$readonly='readonly';
		$unit=$this->Unit->findById($value['MslStockOrderItem']['unit']);
		$first_date=date('Y-m-d',strtotime('first day of previous month'));
//pr($first_date);
//exit;
		$last_date= date('Y-m-d',strtotime('last day of previous month'));
//pr($last_date);
		$avg=$this->Sale->find('all',array(
			'joins'=>array(
				array(
					'table'=>'sale_items',
					'alias'=>'SaleItem',
					'type'=>'INNER',
					'conditions'=>array('Sale.id=SaleItem.sale_id')
					),

				),
			'conditions'=>array('Sale.date_of_delivered between ? and ?'=>array($first_date,$last_date),
				'SaleItem.product_id'=>$value['Product']['id'],
				),
			'fields'=>array('SaleItem.quantity'),
			));
//pr($avg);
//exit;

		$avg_q=0;
		foreach ($avg as $key5 => $value5) {
//pr($value5);
//exit;
			if($value['MslStockOrderItem']['unit']==2)
			{

				$av=$value5['SaleItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
//pr($av);
//exit;
//$avg_q+=$value5['SaleItem']['quantity'];
			}
			else{
				$av=$value5['SaleItem']['quantity'];
			}
			$avg_q+=$av;
		}
//pr($avg_q);
//exit;
		$avg_quantity=ROUND($avg_q/26,3);
		$quantity=floatval($value['MslStockOrderItem']['quantity']);
		$product_id=$this->Product->findById($value['MslStockOrderItem']['product_id']);
		if($value['MslStockOrderItem']['unit']==2)
		{
			$new_quantity=$quantity/$product_id['Product']['no_of_piece_per_unit'];
//$avg_quantity=ROUND($avg_q/26,2);
		}
		else
		{
			$new_quantity=$quantity;
//$avg_quantity=ROUND($avg_q/26,2);
		}
		$Data[$key]['MslStockOrderItem']['status']='<a onclick="return confirm("Are you sure?")" href="'.$this->webroot.'Stock/MslStockOrderItem_delete/'.$value['MslStockOrderItem']['id'].'"><i class="fa fa-2x fa-trash blue-col"></i></a>';
		$Data[$key]['MslStockOrderItem']['avg']="<input value='".$avg_quantity."' class='form-control cart_single_item number quantity  type='text' id='avg'".$readonly."><input  value='".$avg_quantity."' class='form-control cart_single_item number quantity item_id' ' type='hidden' >";
		$Data[$key]['MslStockOrderItem']['quantity']="<input value='".$new_quantity."' class='form-control cart_single_item number quantity  type='text' id='quantity'><input  value='".$new_quantity."' class='form-control cart_single_item number quantity item_id' ' type='hidden' >";
		$Data[$key]['MslStockOrderItem']['unit']="<input value='".$unit['Unit']['name']."' class='form-control cart_single_item number quantity' type='text' ".$readonly."><input value='".$unit['Unit']['id']."' class='form-control cart_single_item number quantity' type='hidden'>";

	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data);
	exit;

}
public function MslStockOrderItem_delete($id)
{
	$user_id=1;
	$MslStockOrderItem=$this->MslStockOrderItem->find('first',array(
		'conditions'=>array(
			'MslStockOrderItem.id'=>$id,
			),
		'fields'=>array('MslStockOrderItem.product_id','MslStockOrderItem.quantity','MslStockOrder.warehouse_id')));
	$toStock=$this->MslStock->find('first',array(
		'conditions'=>array(
			'MslStock.product_id'=>$MslStockOrderItem['MslStockOrderItem']['product_id'],
			'MslStock.warehouse_id'=>$MslStockOrderItem['MslStockOrder']['warehouse_id'],
			),
		'fields'=>array('MslStock.id','MslStock.quantity')));
	if(($toStock['MslStock']['quantity']-$MslStockOrderItem['MslStockOrderItem']['quantity'])>=0)
	{

		try {
			if(!$this->MslStockOrderItem->delete($id)){
				throw new Exception("Error while deleting", 1);
			}
			else{

				if($toStock)
				{
					$fromQty=$toStock['MslStock']['quantity']-$MslStockOrderItem['MslStockOrderItem']['quantity'];
					$remark='Msl Stock ['.$MslStockOrderItem['MslStockOrderItem']['quantity'].']';;
					$Stock_function_return=$this->GeneralMslStock_Edit_Function($toStock['MslStock']['id'],$fromQty,date('Y-m-d'),$user_id,$remark);
					if($Stock_function_return['result']!='Success')
						throw new Exception($Stock_function_return['result']);
				}
			}
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
	}
	else{
		$return['result']='Cant Delete This Item';
	}
	$this->Session->setFlash(__($return['result']));
	$this->redirect( Router::url( $this->referer(), true ) );
}
public function MslStockOrderIndex()
{
	$first_date = date('Y-m-d',strtotime('first day of this month'));
	$firstdate = date('d-m-Y',strtotime('first day of this month'));
	$this->set('firstdate', $firstdate);
	$last_date = date('Y-m-d');
	$todate = date('d-m-Y');
	$this->set('todate', $todate);
}
public function msl_stock_order_search_ajax()
{
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='MslStockOrder.date';
	$columns[]='Warehouse.name';
// $columns[]='MslStockOrder.order_no';
	$columns[]='MslStockOrder.id';
	$columns[]='MslStockOrder.id';
	$conditions=[];
// if(isset($requestData['from_date']) && isset($requestData['to_date']) )
// {
//   $conditions['MslStockOrder.date between ? and ?']=[
//     date('Y-m-d',strtotime($requestData['from_date'])),
//     date('Y-m-d',strtotime($requestData['to_date']))
//   ];
// }
	$totalData=$this->MslStockOrder->find('count',['conditions'=>$conditions]);
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'MslStockOrder.date LIKE' =>'%'. $q . '%',
			'Warehouse.name LIKE' =>'%'. $q . '%',
// 'MslStockOrder.order_no LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->MslStockOrder->find('count',[
			'conditions'=>$conditions,
			]);
	}
	$Data=$this->MslStockOrder->find('all',array(
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'MslStockOrder.*',
			'Warehouse.id',
			'Warehouse.name',
			)
		));
	foreach ($Data as $key => $value) {
		$Data[$key]['MslStockOrder']['no_of_items']=$this->MslStockOrderItem->find('count',['conditions'=>['msl_order_id'=>$value['MslStockOrder']['id']]]);
		$Data[$key]['MslStockOrder']['date']=date('d-m-Y',strtotime($value['MslStockOrder']['date']));
		$Data[$key]['MslStockOrder']['action']='<span><a target="_blank" href="'.$this->webroot.'Stock/MslStockOrder/'.$value['MslStockOrder']['id'].'"><i class="fa fa-2x fa-eye"></i></span></a>';
	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data); exit;
}
public function MslStockTransfer($id = null){
	$user_id=1;
	$this->set('Warehouse',$this->Warehouse->find('list',array(
		'fields'=>array('id','name'),
		'order'=>array('name ASC'),
		)));
	$this->set('Unit',$this->Unit->find('list',array(
		'fields'=>array('id','name'),
		'order'=>array('name ASC'),
		)));
	$Transfer=$this->StockTransfer->find('first',array('order' => 'StockTransfer.id DESC',));
	if(!empty($Transfer))
	{
		$TransferNo = $Transfer['StockTransfer']['transfer_no'];
		$transfer_no=$TransferNo+1;
	}
	else
	{
		$transfer_no=1;
	}
	if (!$this->request->data){
		if(empty($id)){
			$data['MslStockTransfer']['transfer_no']=$transfer_no;
			$data['MslStockTransfer']['date']=date('d-m-Y');
			$this->request->data=$data;
		} else {
			$MslStockTransfer=$this->StockTransfer->findById($id);
			$MslStockTransferItem=$this->StockTransferItem->find('all',array('conditions'=>array('stock_transfer_id'=>$id)));
			$this->request->data=$MslStockTransfer;
			$this->set('StockTransferItem',$MslStockTransferItem);
			$this->request->data['MslStockTransfer']['date']=date('d-m-Y',strtotime($MslStockTransfer['MslStockTransfer']['date']));
		}
	} else {
		$datasource_MslStockTransfer = $this->StockTransfer->getDataSource();
		$datasource_MslStockTransferItem = $this->StockTransferItem->getDataSource();
		$datasource_Stock = $this->Stock->getDataSource();
		try {
			$datasource_MslStockTransfer->begin();
			$datasource_MslStockTransferItem->begin();
			$datasource_Stock->begin();
			$data=$this->request->data['MslStockTransfer'];
//pr($data);exit;
			if(empty($id))
			{
				$MslStockTransfer_data=[
				'transfer_no'=>$data['transfer_no'],
				'date'=>date('Y-m-d',strtotime($data['date'])),
				'warehouse_from'=>$data['warehouse_from'],
				'warehouse_to'=>$data['warehouse_to'],
				'remarks'=>$data['remarks'],
				'created_by'=>$user_id,
				'modified_by'=>$user_id,
				'created_at'=>date('Y-m-d H:i:s'),
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				$this->StockTransfer->create();
				if(!$this->StockTransfer->save($MslStockTransfer_data))
				{
					$errors = $this->StockTransfer->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				$stock_transfer_id=$this->StockTransfer->getLastInsertId();
				for ($i=0; $i <count($data['product']) ; $i++) {
					$MslStockTransferItem_data=[
					'product_id'=>$data['product'][$i],
					'unit_id'=>$data['unit'][$i],
					'stock_transfer_id'=>$stock_transfer_id,
					'quantity'=>$data['quantity'][$i],
					];
					$this->StockTransferItem->create();
					if(!$this->StockTransferItem->save($MslStockTransferItem_data))
					{
						$errors = $this->StockTransferItem->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					else
					{
						$fromStock=$this->Stock->find('first',array(
							'conditions'=>array(
								'Stock.product_id'=>$data['product'][$i],
								'Stock.warehouse_id'=>$data['warehouse_from']
								),
							'fields'=>array('Stock.id','Stock.quantity')
							));
						if($fromStock)
						{
							$fromQty=$fromStock['Stock']['quantity']-$data['quantity'][$i];
							$remark='Stock Transfer ['.$stock_transfer_id.','.$data['quantity'][$i].']';
							$Stock_function_return=$this->GeneralStock_Edit_Function($fromStock['Stock']['id'],$fromQty,date('Y-m-d'),$user_id,$remark);
							if($Stock_function_return['result']!='Success')
								throw new Exception($Stock_function_return['result']);
						}
						$toStock=$this->Stock->find('first',array(
							'conditions'=>array(
								'Stock.product_id'=>$data['product'][$i],
								'Stock.warehouse_id'=>$data['warehouse_to']
								),
							'fields'=>array('Stock.id','Stock.quantity')));
						if(!$toStock)
						{
							$remark='Stock Transfer ['.$stock_transfer_id.','.$data['quantity'][$i].']';
							$flag=1;
							$Stock_function_return=$this->GeneralStock_Add_Function($data['warehouse_to'],$data['product'][$i],$data['quantity'][$i],date('Y-m-d'),$user_id,$remark,$flag);
							if($Stock_function_return['result']!='Success')
								throw new Exception($Stock_function_return['result'], 1);
						}
						else
						{
							$toQty=$toStock['Stock']['quantity']+$data['quantity'][$i];
							$remark='Stock Transfer ['.$stock_transfer_id.','.$data['quantity'][$i].']';
							$Stock_function_return=$this->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
							if($Stock_function_return['result']!='Success')
								throw new Exception($Stock_function_return['result']);
						}
					}
				}
			}
			else
			{
				$data=$this->request->data['MslStockTransfer'];
			}
			$datasource_Stock->commit();
			$datasource_MslStockTransfer->commit();
			$datasource_MslStockTransferItem->commit();
			$return['result']="success";
		} catch (Exception $e){
			$datasource_MslStockTransfer->rollback();
			$datasource_MslStockTransferItem->rollback();
			$datasource_Stock->rollback();
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		$this->redirect(array('controller' => 'Stock','action' =>'StockTransferList'));
	}
}
public function get_msl_product()
{
	$return['result']='Error';

	$warehouse_from=$this->request->data['warehouse_from'];
	$warehouse_to=$this->request->data['warehouse_to'];
	$MslStockOrderItem=$this->MslStockOrderItem->find('all',array(
		"joins"=>array(
			array(
				"table"=>'stocks',
				"alias"=>'Stock',
				"type"=>'inner',
				"conditions"=>array('MslStockOrderItem.product_id=Stock.product_id'),
				),
			),
		'conditions'=>array('MslStockOrder.warehouse_id'=>$warehouse_to,'Stock.warehouse_id'=>$warehouse_from),

		'fields'=>array('Stock.quantity','MslStockOrder.warehouse_id','MslStockOrderItem.*','Product.no_of_piece_per_unit','Product.id','Product.name')
		));
//pr($MslStockOrderItem);
//exit;
	$stocktransfer=[];
	if(!empty($MslStockOrderItem)){
		foreach ($MslStockOrderItem as $key => $value) {
//pr($value);
			$single['Stock']=$value['Stock'];
			$single['MslStockOrderItem']=$value['MslStockOrderItem'];
			$single['Product']=$value['Product'];
			$to_stock=$this->Stock->find('first',array(
				'conditions'=>array(
					'Stock.product_id'=>$value['MslStockOrderItem']['product_id'],
					'Stock.warehouse_id'=>$value['MslStockOrder']['warehouse_id'],
					),
				'fields'=>array('Stock.quantity')));
//pr($to_stock);
//exit;
			if($to_stock)
			{
				$single['to_stock']=$to_stock['Stock'];

			}
			else
			{
				$single['to_stock']['quantity']=0;

			}

			array_push($stocktransfer,$single);
		}
		$return['MslStockOrderItem']=$stocktransfer;
//pr($return);
//exit;
		$return['result']='Success';
	}
	echo json_encode($return);
	exit;
}
public function add_msl_order_item_update()
{
	$data=$this->request->data;
	try {
		$user_id=1;
		$this->MslStockOrderItem->id=$data['item_id'];
		$product_id=$this->Product->findById($data['product_id']);
		if($data['unit_id']==2)
		{
			$quantity=$data['quantity']*$product_id['Product']['no_of_piece_per_unit'];
			$quantity_old=$data['quantity_old']*$product_id['Product']['no_of_piece_per_unit'];
		}
		else
		{
			$quantity=$data['quantity'];
			$quantity_old=$data['quantity_old'];
		}
		if(!$this->MslStockOrderItem->saveField('quantity',$quantity))
			throw new Exception("Error Processing quantity Updation", 1);
		$toStock=$this->MslStock->find('first',array(
			'conditions'=>array(
				'MslStock.product_id'=>$data['product_id'],
				'MslStock.warehouse_id'=>$data['warehouse_id'],
				),
			'fields'=>array('MslStock.id','MslStock.quantity')));
		if($toStock)
		{
			$MslStockOrderItem=$this->MslStockOrderItem->find('first',array(
				'conditions'=>array('MslStockOrderItem.id'=>$data['item_id'],
					),
				'fields'=>array('MslStockOrderItem.*')
				));
			$quantity_new=$MslStockOrderItem['MslStockOrderItem']['quantity'];
			$remark='MslStock Stock ['.$data['product_id'].','.$data['quantity'].','.$data['warehouse_id'].']';
			$toQty=$toStock['MslStock']['quantity']-$quantity_old+$quantity_new;
			$Stock_function_return=$this->GeneralMslStock_Edit_Function($toStock['MslStock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
			if($Stock_function_return['result']!='Success')
				throw new Exception($Stock_function_return['result']);
		}
		$return['result']='Success';
	} catch (Exception $e) {
		$return['result']=$e->getMessage();
	}
	echo json_encode($return);exit;
}
public function stock_opening_update()
{
	$StockLog=$this->StockLog->find('all',array(
		'conditions'=>array(
			'StockLog.remark'=>'Product Created',
			),
	));

	foreach ($StockLog as $key => $value) {
		$this->StockLog->id=$value['StockLog']['id'];
		if(!$this->StockLog->saveField('quantity_in',$value['StockLog']['quantity']))
			throw new Exception("Error Processing Request", 1);
		$total_cost=$value['StockLog']['quantity']*$value['Product']['cost'];
		if($value['StockLog']['quantity_in']==0)
		{
			$stock_account_head_id=$this->AccountHead->field('AccountHead.id',array('AccountHead.name'=>'STOCK'));
			$this->AccountHead->id=$stock_account_head_id;
			$this->AccountHead->saveField('opening_balance',$this->AccountHead->field('opening_balance')+$total_cost);
		}
			
	}
	exit;
}
public function VanBranchTransferList()
{

	$conditions = [];
	$this->set('from',date("d-m-Y", strtotime('-1 day')));
	$this->set('to',date("d-m-Y"));

}
public function VanBranchStockTransfer($id=null)
{
      
	$user_id=1;
	$branch_id = $this->Session->read('User.branch_id');
	$this->set('Van',$this->Warehouse->find('list',array(
		'fields'=>array('id','name'),
		'order'=>array('name ASC'),
		)));
	$this->set('from',date("d-m-Y", strtotime('-31 day')));
	$this->set('to',date("d-m-Y"));
		$this->set('Branch',$this->Warehouse->find('list',array(
			'order'=>array('name ASC'),
			'fields'=>array('Warehouse.id','Warehouse.name'),
			)));				
	$Transfer=$this->StockTransfer->find('first',array('order' => 'StockTransfer.id DESC',));
	if(!empty($Transfer))
	{
		$TransferNo = $Transfer['StockTransfer']['transfer_no'];
		$transfer_no=$TransferNo+1;
	}
	else
	{
		$transfer_no=1;
	}
	if (!$this->request->data)
	{
		if(empty($id)){
			$data['StockTransfer']['transfer_no']=$transfer_no;
			$data['StockTransfer']['date']=date('d-m-Y');
			$this->request->data=$data;
		}
		else
		{
			$StockTransfer=$this->StockTransfer->findById($id);
			$this->StockTransferItem->virtualFields = array('product_name' => "CONCAT(Product.name)");
			$StockTransferItem=$this->StockTransferItem->find('all',array('conditions'=>array('stock_transfer_id'=>$id)));
			$this->request->data=$StockTransfer;
			$this->set('StockTransferItem',$StockTransferItem);
			$this->request->data['StockTransfer']['warehouse_from_name']= $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $StockTransfer['StockTransfer']['warehouse_from']));
		    $this->request->data['StockTransfer']['warehouse_to_name']= $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $StockTransfer['StockTransfer']['warehouse_to']));
			$this->request->data['StockTransfer']['date']=date('d-m-Y',strtotime($StockTransfer['StockTransfer']['date']));
		}
	} 
	else 
	{
      	$datasource_ExecutiveBonusCalculation = $this->ExecutiveBonusCalculation->getDataSource();
         $datasource_ProductBonusDetail = $this->ProductBonusDetail->getDataSource();
		$datasource_StockTransfer = $this->StockTransfer->getDataSource();
		$datasource_StockTransferItem = $this->StockTransferItem->getDataSource();
		$datasource_Stock = $this->Stock->getDataSource();
		try {
			$datasource_ExecutiveBonusCalculation->begin();
			$datasource_ProductBonusDetail->begin();
			$datasource_StockTransfer->begin();
			$datasource_StockTransferItem->begin();
			$datasource_Stock->begin();
			$data=$this->request->data['StockTransfer'];
			if(empty($id))
			{}
			else
			{
				$data=$this->request->data['StockTransfer'];
			$day_register_id=$this->StockTransfer->field('day_register_id',array('StockTransfer.id'=>$id));
			$executive_id=$this->DayRegister->field('executive_id',array('DayRegister.id'=>$day_register_id));
			$this->StockTransfer->id=$id;
			$approved_data=[
			'status'=>2,
			'updated_at'=>date('Y-m-d H:i:s'),
			];								
			if(!$this->StockTransfer->save($approved_data))
			{
				$errors = $this->StockTransfer->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$stock_transfer_id=$id;
			$bonus_amount_total=0;
			for ($i=0; $i <count($data['product']) ; $i++)
			{
				$acceptable=$data['quantity'][$i]-$data['non_acceptable_qty'][$i];
				$this->StockTransferItem->id=$data['item_id'][$i];
				$ApprovedItem_data=[
				'non_acceptable_qty'=>$data['non_acceptable_qty'][$i],
				'approve_qty'=>$acceptable,
				'remarks'=>$data['remarks'][$i],
				];
				if(!$this->StockTransferItem->save($ApprovedItem_data))
				{
					$errors = $this->StockTransferItem->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
					$fromStock=$this->Stock->find('first',array(
						'conditions'=>array(
							'Stock.product_id'=>$data['product'][$i],
							'Stock.warehouse_id'=>$data['warehouse_from'],
							),
						'fields'=>array('Stock.id','Stock.quantity')
						));
					if(!$fromStock)
				      throw new Exception("Empty Stock", 1);
						$fromQty=$fromStock['Stock']['quantity']-$data['quantity'][$i];
						if($fromQty<0)
				      throw new Exception("Empty From Stock", 1);
				  		$remark='Stock Transfer --'.$data['transfer_no'].'('.$data['quantity'][$i].')';	
					$Stock_function_return=$this->GeneralStock_Edit_Function($fromStock['Stock']['id'],$fromQty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
					$toStock=$this->Stock->find('first',array(
						'conditions'=>array(
							'Stock.product_id'=>$data['product'][$i],
							'Stock.warehouse_id'=>$data['warehouse_to'],
							),
						'fields'=>array('Stock.id','Stock.quantity')
						));
					if(!$toStock)
					{
						$flag=1;
						$remark='Stock Transfer --'.$data['transfer_no'].'('.$acceptable.')';	
						$Stock_function_return=$this->GeneralStock_Add_Function($data['warehouse_to'],$data['product'][$i],$acceptable,date('Y-m-d'),$user_id,$remark,$flag);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result'], 1);
					}
					else
					{
						
						$toQty=$toStock['Stock']['quantity']+$acceptable;
						$remark='Stock Transfer --'.$data['transfer_no'].'('.$acceptable.')';	
						$Stock_function_return=$this->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
					}
					$warehouse_to= $this->Warehouse->field('Warehouse.id',array('Warehouse.warehouse_id ' =>1));
					if(empty($warehouse_to))
					throw new Exception("Damage Warehouse Required", 1);
		             $executive_rate = $this->ExecutiveRate->field(
									'ExecutiveRate.executive_rate',
									array('ExecutiveRate.executive_id ' =>$executive_id,'ExecutiveRate.product_id ' =>$data['product'][$i]));
								if(empty($executive_rate))
								{
                                 $product=$this->Product->findById($data['product'][$i]);
                                 $executive_rate=$product['Product']['wholesale_price'];

								}
				    $bonus_amount=$executive_rate*$data['non_acceptable_qty'][$i];
							if($bonus_amount>0)
						         {
			$bonus_amount_total=$bonus_amount_total+$bonus_amount;
					         $ProductBonus_data=[
					         'date'=>date('Y-m-d'),
					         'day_register_id'=>$day_register_id,
							  'executive_id'=>$executive_id,
					          'product_id'=>$data['product'][$i],
					          'vtob_id'=>$id,
					          'vtob_item_id'=>$data['item_id'][$i],
					          'unit_price'=>$executive_rate,
					          'executive_rate'=>$executive_rate,
					          'quantity'=>$data['non_acceptable_qty'][$i],
					          'bonus_return_amount'=>$bonus_amount,
					          ];
					          $this->ProductBonusDetail->create();
					          if(!$this->ProductBonusDetail->save($ProductBonus_data))
					          {
					            $errors = $this->ProductBonusDetail->validationErrors;
					            foreach ($errors as $key => $value) {
					              throw new Exception($value[0], 1);
					            }
					          }
					        }
					$non_acceptable_stock=$this->Stock->find('first',array(
						'conditions'=>array(
							'Stock.product_id'=>$data['product'][$i],
							'Stock.warehouse_id'=>$warehouse_to,
							),
						'fields'=>array('Stock.id','Stock.quantity')
						));
					if(!$non_acceptable_stock)
					{
						$flag=1;
						$remark='Stock Transfer --'.$data['transfer_no'].'('.$data['non_acceptable_qty'][$i].')';	
						$Stock_function_return=$this->GeneralStock_Add_Function($warehouse_to,$data['product'][$i],$data['non_acceptable_qty'][$i],date('Y-m-d'),$user_id,$remark,$flag);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result'], 1);
					}
					else
					{
						$toQty=$non_acceptable_stock['Stock']['quantity']+$data['non_acceptable_qty'][$i];
						$remark='Stock Transfer --'.$data['transfer_no'].'('.$data['non_acceptable_qty'][$i].')';	
						$Stock_function_return=$this->GeneralStock_Edit_Function($non_acceptable_stock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
					}
				}
				if($bonus_amount_total>0)
                {
                $ProductBonus_data=[
                'executive_id'=>$executive_id,
                 'day_register_id'=>$day_register_id,
                'date'=>date('Y-m-d'),
                'work_flow'=>"NON ACCEPTABLE",
                 'reference_no'=>$data['transfer_no'],
                'bonus_return_amount'=>$bonus_amount_total,
                ];
                $this->ExecutiveBonusCalculation->create();
                if(!$this->ExecutiveBonusCalculation->save($ProductBonus_data))
                {
                  $errors = $this->ExecutiveBonusCalculation->validationErrors;
                  foreach ($errors as $key => $value) {
                    throw new Exception($value[0], 1);
                  }
                }
              }
			}
			$datasource_ExecutiveBonusCalculation->commit();
			$datasource_ProductBonusDetail->commit();
			$datasource_Stock->commit();
			$datasource_StockTransfer->commit();
			$datasource_StockTransferItem->commit();
			$return['result']="success";
		} catch (Exception $e){
			$datasource_ExecutiveBonusCalculation->rollback();
			$datasource_ProductBonusDetail->rollback();
			$datasource_StockTransfer->rollback();
			$datasource_StockTransferItem->rollback();
			$datasource_Stock->rollback();
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		$this->redirect(array('controller' => 'Stock','action' =>'VanBranchTransferList'));

	
	}

}
public function StationaryIssue($id=null)
{
    $user_id=1;
    $warehouses=$this->Warehouse->find('list',array(
            'fields'=>array('id','name'),
            'order'=>array('id ASC'),
            "joins"=>array(
            	array(
					"table"=>'executives',
					"alias"=>'Executive',
					"type"=>'left',
					"conditions"=>array('Warehouse.id=Executive.warehouse_id'),
					),
				),
      'conditions'=>array('Executive.warehouse_id'=>"",'Warehouse.warehouse_id'=>"",'Warehouse.name !='=>"")
           ));
    $this->set('Warehouse',$warehouses);
    $Products=$this->Product->find('list',array(
            'fields'=>array('id','name'),
            'order'=>array('name ASC'),
            "joins"=>array(
      ),
      'conditions'=>array('Product.type'=>7,'Product.active=1')
           ));
    $this->set('Product',$Products);
  if (!$this->request->data)
  {
      if(empty($id)){
      $data['StationaryIssue']['date']=date('d-m-Y');
      $this->request->data=$data;
      } 
      else
      {
        $StationaryIssue=$this->StationaryIssue->findById($id);
      $StationaryIssueItem=$this->StationaryIssueItem->find('all',array('conditions'=>array('issue_id'=>$id)));
      $this->request->data=$StationaryIssue;
      $this->set('StationaryIssueItem',$StationaryIssueItem);
      $this->request->data['StationaryIssue']['date']=date('d-m-Y',strtotime($StationaryIssue['StationaryIssue']['date']));
      }
  }
  else
  {
  	$datasource_StationaryIssue=$this->StationaryIssue->getDataSource();
    $datasource_StationaryIssueItem=$this->StationaryIssueItem->getDataSource();
    $datasource_Stock=$this->Stock->getDataSource();
    try{
    $datasource_StationaryIssue->begin();
    $datasource_StationaryIssueItem->begin();
    $datasource_Stock->begin();
        $data=$this->request->data['StationaryIssue'];
        if(empty($id))
        {
          $StationaryIssue=$this->StationaryIssue->find('first',array('order' => 'StationaryIssue.id DESC',));
          if(!empty($StationaryIssue))
          {
          $IssueNo = $StationaryIssue['StationaryIssue']['issue_no'];
          $issue_no=$IssueNo+1;
          }
          else
          {
          $issue_no=1;
          }
          $Save_data=[
          'date'=>date('Y-m-d',strtotime($data['date'])),
          'warehouse_id'=>$data['warehouse_id'],
          'remarks'=>$data['remarks'],
          'issue_no'=>$issue_no,
          'created_at'=>date('Y-m-d H:i:s'),
          'updated_at'=>date('Y-m-d H:i:s'),
          ];
          $this->StationaryIssue->create();
          if(!$this->StationaryIssue->save($Save_data))
          {
            $errors = $this->StationaryIssue->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
          $issue_id=$this->StationaryIssue->getLastInsertId();
          for ($i=0; $i <count($data['product_id']) ; $i++) 
          {
            $Item_data=[
            'product_id'=>$data['product_id'][$i],
            'issue_id'=>$issue_id,
            'quantity'=>$data['quantity'][$i],
            ];
                $this->StationaryIssueItem->create();
                if(!$this->StationaryIssueItem->save($Item_data))
                {
                  $errors = $this->StationaryIssueItem->validationErrors;
                  foreach ($errors as $key => $value) {
                    throw new Exception($value[0], 1);
                  }
                }
                else
                {

                  $fromStock=$this->Stock->find('first',array(
                    'conditions'=>array(
                      'Stock.product_id'=>$data['product_id'][$i],
                      'Stock.warehouse_id'=>$data['warehouse_id']
                      ),
                    'fields'=>array('Stock.id','Stock.quantity')
                    ));
                  $fromQty=$fromStock['Stock']['quantity']-$data['quantity'][$i];
						if($fromQty<0)
				      throw new Exception("Empty From Stock", 1);
                      $remark='Stationary Issue ['.$issue_id.','.$data['quantity'][$i].']';
					$Stock_function_return=$this->GeneralStock_Edit_Function($fromStock['Stock']['id'],$fromQty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);

                }
              }
          }
          $return['result']="Success";
        $datasource_StationaryIssue->commit();
      $datasource_StationaryIssueItem->commit();
      $datasource_Stock->commit();
    }
    catch (Exception $e)
    {
      $return['result']=$e->getMessage();
      $datasource_StationaryIssue->rollback();
     $datasource_StationaryIssueItem->rollback();
            $datasource_Stock->commit();
    }
    $this->Session->setFlash(__($return['result']));
  $this->redirect(array('controller' => 'Stock','action' =>'StationaryIssueList'));
}
}
public function get_warehousewise_Product_ajax()
{
	$data=$this->request->data;
  $Stock=$this->Stock->find('first',array(
       'conditions'=>array(
     'Stock.product_id'=>$data['product_id'],
     'Stock.warehouse_id'=>$data['warehouse_id'],
      ),
       'fields'=>array(
        'Stock.quantity',
      )));
  $return['result']='Success';
  $return['quantity']=0;
  $return['stock_quantity']=0;
if(!empty($Stock)){
$return['quantity']=floatval($Stock['Stock']['quantity']);
$return['stock_quantity']=floatval($Stock['Stock']['quantity']);
$return['result']='Success';
}
echo json_encode($return);
exit;
}
public function StationaryIssueList()
{

	$conditions = [];
	$this->set('from',date("d-m-Y", strtotime('-30 day')));
	$this->set('to',date("d-m-Y"));
 $warehouses=$this->Warehouse->find('list',array(
            'fields'=>array('id','name'),
            'order'=>array('id DESC'),
            "joins"=>array(
      ),
      'conditions'=>array('Warehouse.warehouse_id'=>"",'Warehouse.name !='=>"")
           ));
    $this->set('Warehouse',$warehouses);
}
public function get_StationaryIssue()
{
  $requestData=$this->request->data;
  $columns = [];
  $columns[]='StationaryIssue.issue_no';
  $columns[]='StationaryIssue.date';
  $columns[]='Warehouse.name';
  $columns[]='StationaryIssue.remark';
  $columns[]='StationaryIssue.id'; 
  $conditions=[];
$from_date = date('Y-m-d',strtotime($requestData['from_date']));
  $to_date   = date('Y-m-d',strtotime($requestData['to_date']));
  if(isset($from_date) && isset($to_date))
  {
    $conditions['StationaryIssue.date BETWEEN ? and ?'] =  array($from_date, $to_date);
  }
  if(isset($requestData['warehouse_id']))
  {
    if($requestData['warehouse_id'])
    $conditions['StationaryIssue.warehouse_id']=$requestData['warehouse_id'];
  }
  $totalData=$this->StationaryIssue->find('count',['conditions'=>$conditions]);
  $totalFiltered=$totalData;
  if( !empty($requestData['search']['value']) ) { 
    $q=$requestData['search']['value'];
    $conditions['OR']=array(
      'Warehouse.name LIKE' =>'%'. $q . '%',
      'StationaryIssue.remark LIKE' =>'%'. $q . '%',
    );
    $totalFiltered=$this->StationaryIssue->find('count',[
      'conditions'=>$conditions,
    ]);
  }
    $Data=$this->StationaryIssue->find('all',array(
    'conditions'=>$conditions,
    'offset'=>$requestData['start'],
    'limit'=>$requestData['length'],
    'fields'=>array(
      'StationaryIssue.*',
      'Warehouse.name',
    )
  ));
  foreach ($Data as $key => $value) {
    $table_id=$value['StationaryIssue']['id'];
    $Data[$key]['StationaryIssue']['action']='<span hidden class="table_id">'.$table_id.'</span>';
     $Data[$key]['StationaryIssue']['action']='<a href="'.$this->webroot.'Stock/StationaryIssue/'.$table_id.'" title=""><span<i class="fa fa-2x fa-eye" aria-hidden="true"></i></span></a>';
  }
  $json_data = array(
    "draw"           =>intval($requestData['draw']),
    "recordsTotal"   =>intval($totalData),
    "recordsFiltered"=>intval($totalFiltered),
    "records"        =>$Data
  );
  echo json_encode($json_data);
  exit; 
}

public function get_V2B_ajax()
{
	$user_role_id=$this->Session->read('UserRole.id');
		$user_branch_id=$this->Session->read('User.branch_id');
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='StockTransfer.transfer_no';
	$columns[]='StockTransfer.date';
	$columns[]='WarehouseFrom.name'; 
	$columns[]='WarehouseTo.name'; 
	$columns[]='StockTransfer.remarks'; 
	$columns[]='StockTransfer.status';
	$columns[]='StockTransfer.id';
	$conditions=[];
	if(!empty($user_branch_id))
	{
		$branch=$this->Branch->findById($user_branch_id);
		$conditions['StockTransfer.warehouse_to']=$branch['Branch']['warehouse_id'];
	}
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions['StockTransfer.date between ? and ?']=[$from_date,$to_date];
	if(isset($requestData['status']))
	{
		if($requestData['status']==2)
		{
			$conditions['StockTransfer.status']=2;
			$conditions['StockTransfer.flag'] = 1;
		    $status="Approved";
		}
		else
		{
			$conditions['StockTransfer.flag']=1;
			$conditions['StockTransfer.status']=1;	
		    $status="Pending";
		}
	}
	$conditions['StockTransfer.transfer_type']	 = 3;
	$totalData=$this->StockTransfer->find('count',['conditions'=>$conditions]);
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'StockTransfer.date LIKE' =>'%'. $q . '%',
			'WarehouseFrom.name LIKE' =>'%'. $q . '%',
			'WarehouseTo.name LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->StockTransfer->find('count',[
			"joins"=>array(
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseFrom',
					"type"=>'inner',
					"conditions"=>array('WarehouseFrom.id=StockTransfer.warehouse_from'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseTo',
					"type"=>'inner',
					"conditions"=>array('WarehouseTo.id=StockTransfer.warehouse_to'),
					),
				),
			'conditions'=>$conditions,
			]);
	}
		//pr($conditions);
	$Data=$this->StockTransfer->find('all',array(
		"joins"=>array(
			array(
				"table"=>'warehouses',
				"alias"=>'WarehouseFrom',
				"type"=>'inner',
				"conditions"=>array('WarehouseFrom.id=StockTransfer.warehouse_from'),
				),
			array(
				"table"=>'warehouses',
				"alias"=>'WarehouseTo',
				"type"=>'inner',
				"conditions"=>array('WarehouseTo.id=StockTransfer.warehouse_to'),
				),
			),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'StockTransfer.id',
			'StockTransfer.transfer_no',
			'StockTransfer.date',
			'StockTransfer.status',
			'StockTransfer.remarks',
			'WarehouseFrom.name',
			'WarehouseTo.name',
			)
		));
	foreach ($Data as $key => $value) {
		$Data[$key]['StockTransfer']['status']=$status;
		$Data[$key]['StockTransfer']['date']=date('d-m-Y',strtotime($value['StockTransfer']['date']));
		$table_id=$value['StockTransfer']['id'];
		$Data[$key]['StockTransfer']['action']='<a href="'.$this->webroot.'Stock/VanBranchStockTransfer/'.$table_id.'" title=""><span<i class="fa fa-2x fa-eye" aria-hidden="true"></i></span></a>';
	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data);
	exit;
}
public function get_M2B_ajax()
{
	$user_role_id=$this->Session->read('UserRole.id');
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='StockTransfer.transfer_no';
	$columns[]='StockTransfer.date';
	$columns[]='WarehouseFrom.name'; 
	$columns[]='WarehouseTo.name'; 
	$columns[]='StockTransfer.remarks'; 
	$columns[]='StockTransfer.status';
	$columns[]='StockTransfer.id';
	$conditions=[];
	//	pr($requestData);
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions['StockTransfer.date between ? and ?']=[$from_date,$to_date];
	$user_branch_id = $this->Session->read('User.branch_id');
			if(!empty($user_branch_id))
			{
			$branch=$this->Branch->findById($user_branch_id);
			$conditions['OR']=array(
			"StockTransfer.warehouse_to" => $branch['Branch']['warehouse_id'], 
			"StockTransfer.warehouse_from" => $branch['Branch']['warehouse_id'],	
			);
			}
	if(isset($requestData['status']))
	{
		if($requestData['status']==2)
		{
			$conditions['StockTransfer.status']=2;
			$conditions['StockTransfer.flag'] = 1;
		    $status="Approved";
		}
		else
		{
			$conditions['StockTransfer.flag']=1;
			$conditions['StockTransfer.status']=1;	
		    $status="Pending";
		}
	}
	$conditions['StockTransfer.transfer_type']	 = 5;
	$totalData=$this->StockTransfer->find('count',['conditions'=>$conditions]);
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'StockTransfer.date LIKE' =>'%'. $q . '%',
			'WarehouseFrom.name LIKE' =>'%'. $q . '%',
			'WarehouseTo.name LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->StockTransfer->find('count',[
			"joins"=>array(
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseFrom',
					"type"=>'inner',
					"conditions"=>array('WarehouseFrom.id=StockTransfer.warehouse_from'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseTo',
					"type"=>'inner',
					"conditions"=>array('WarehouseTo.id=StockTransfer.warehouse_to'),
					),
				),
			'conditions'=>$conditions,
			]);
	}
		//pr($conditions);
	$Data=$this->StockTransfer->find('all',array(
		"joins"=>array(
			array(
				"table"=>'warehouses',
				"alias"=>'WarehouseFrom',
				"type"=>'inner',
				"conditions"=>array('WarehouseFrom.id=StockTransfer.warehouse_from'),
				),
			array(
				"table"=>'warehouses',
				"alias"=>'WarehouseTo',
				"type"=>'inner',
				"conditions"=>array('WarehouseTo.id=StockTransfer.warehouse_to'),
				),
			),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'StockTransfer.id',
			'StockTransfer.transfer_no',
			'StockTransfer.date',
			'StockTransfer.status',
			'StockTransfer.remarks',
			'WarehouseFrom.name',
			'WarehouseTo.name',
			)
		));
	foreach ($Data as $key => $value) {
		$Data[$key]['StockTransfer']['status']=$status;
		$Data[$key]['StockTransfer']['date']=date('d-m-Y',strtotime($value['StockTransfer']['date']));
		$table_id=$value['StockTransfer']['id'];
		$Data[$key]['StockTransfer']['action']='<a href="'.$this->webroot.'Stock/MainToBranchStockApproval/'.$table_id.'" title=""><span<i class="fa fa-2x fa-eye" aria-hidden="true"></i></span></a>';
	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data);
	exit;
}
public function VanVanTransferList()
{

	$conditions = [];
	$this->set('from',date("d-m-Y", strtotime('-1 day')));
	$this->set('to',date("d-m-Y"));

}
public function VanVanStockTransfer($id=null)
{
      
	$user_id=1;
	$branch_id = $this->Session->read('User.branch_id');
	$this->set('Van',$this->Warehouse->find('list',array(
		'fields'=>array('id','name'),
		'order'=>array('name ASC'),
		)));
	$this->set('from',date("d-m-Y", strtotime('-31 day')));
if (!$this->request->data)
	{
		if(empty($id))
		{
		}
		else
		{
			$StockTransfer=$this->StockTransfer->findById($id);
			$this->StockTransferItem->virtualFields = array('product_name' => "CONCAT(Product.name)");
			$StockTransferItem=$this->StockTransferItem->find('all',array('conditions'=>array('stock_transfer_id'=>$id)));
			$this->request->data=$StockTransfer;
			$this->set('StockTransferItem',$StockTransferItem);
			$this->request->data['StockTransfer']['warehouse_from_name']= $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $StockTransfer['StockTransfer']['warehouse_from']));
		    $this->request->data['StockTransfer']['warehouse_to_name']= $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $StockTransfer['StockTransfer']['warehouse_to']));
			$this->request->data['StockTransfer']['date']=date('d-m-Y',strtotime($StockTransfer['StockTransfer']['date']));
		}
	} 	
}						
public function get_V2V_ajax()
{
	$user_role_id=$this->Session->read('UserRole.id');
	$requestData=$this->request->data;
	$columns = [];
	$columns[]='StockTransfer.transfer_no';
	$columns[]='StockTransfer.date';
	$columns[]='WarehouseFrom.name'; 
	$columns[]='WarehouseTo.name'; 
	$columns[]='StockTransfer.remarks'; 
	$columns[]='StockTransfer.status';
	$columns[]='StockTransfer.id';
	$conditions=[];
	//	pr($requestData);
	$from_date=date('Y-m-d',strtotime($requestData['from_date']));
	$to_date=date('Y-m-d',strtotime($requestData['to_date']));
	$conditions['StockTransfer.date between ? and ?']=[$from_date,$to_date];
	if(isset($requestData['status']))
	{
		if($requestData['status']==2)
		{
			$conditions['StockTransfer.status']=2;
			$conditions['StockTransfer.flag'] = 1;
		    $status="Approved";
		}
		else
		{
			$conditions['StockTransfer.flag']=1;
			$conditions['StockTransfer.status']=1;	
		    $status="Pending";
		}
	}
	$conditions['StockTransfer.transfer_type']	 = 4;
	$totalData=$this->StockTransfer->find('count',['conditions'=>$conditions]);
	$totalFiltered=$totalData;
	if( !empty($requestData['search']['value']) ) { 
		$q=$requestData['search']['value'];
		$conditions['OR']=array(
			'StockTransfer.date LIKE' =>'%'. $q . '%',
			'WarehouseFrom.name LIKE' =>'%'. $q . '%',
			'WarehouseTo.name LIKE' =>'%'. $q . '%',
			);
		$totalFiltered=$this->StockTransfer->find('count',[
			"joins"=>array(
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseFrom',
					"type"=>'inner',
					"conditions"=>array('WarehouseFrom.id=StockTransfer.warehouse_from'),
					),
				array(
					"table"=>'warehouses',
					"alias"=>'WarehouseTo',
					"type"=>'inner',
					"conditions"=>array('WarehouseTo.id=StockTransfer.warehouse_to'),
					),
				),
			'conditions'=>$conditions,
			]);
	}
		//pr($conditions);
	$Data=$this->StockTransfer->find('all',array(
		"joins"=>array(
			array(
				"table"=>'warehouses',
				"alias"=>'WarehouseFrom',
				"type"=>'inner',
				"conditions"=>array('WarehouseFrom.id=StockTransfer.warehouse_from'),
				),
			array(
				"table"=>'warehouses',
				"alias"=>'WarehouseTo',
				"type"=>'inner',
				"conditions"=>array('WarehouseTo.id=StockTransfer.warehouse_to'),
				),
			),
		'conditions'=>$conditions,
		'offset'=>$requestData['start'],
		'limit'=>$requestData['length'],
		'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
		'fields'=>array(
			'StockTransfer.id',
			'StockTransfer.transfer_no',
			'StockTransfer.date',
			'StockTransfer.status',
			'StockTransfer.remarks',
			'WarehouseFrom.name',
			'WarehouseTo.name',
			)
		));
	foreach ($Data as $key => $value) {
		$Data[$key]['StockTransfer']['status']=$status;
		$Data[$key]['StockTransfer']['date']=date('d-m-Y',strtotime($value['StockTransfer']['date']));
		$table_id=$value['StockTransfer']['id'];
		$Data[$key]['StockTransfer']['action']='<a href="'.$this->webroot.'Stock/VanBranchStockTransfer/'.$table_id.'" title=""><span<i class="fa fa-2x fa-eye" aria-hidden="true"></i></span></a>';
	}
	$json_data = array(
		"draw"           =>intval($requestData['draw']),
		"recordsTotal"   =>intval($totalData),
		"recordsFiltered"=>intval($totalFiltered),
		"records"        =>$Data
		);
	echo json_encode($json_data);
	exit;
}
public function MainToBranchTransfer()
{
$branch_id = $this->Session->read('User.branch_id');
$todate = date('d-m-Y');
$this->set('date', $todate);
$date_order=date('Y-m-d',strtotime('-1 day'));
if($branch_id)
{
		$Warehouse_from=$this->Warehouse->find('list', array(
		"joins"=>array(array(
			"table"=>'branches',
			"alias"=>'Branch',
			"type"=>'inner',
			"conditions"=>array('Branch.warehouse_id=Warehouse.id'),
			),
		),
			'conditions'=>array('Branch.id' =>$branch_id),
		'order'=>array('Warehouse.name ASC'),
		'fields' => array(
			'Warehouse.id',
			'Warehouse.name',
				)));
		$conditions_branch=[];
$conditions_branch['BranchWarehouseMapping.branch_id']=$branch_id;
$this->Warehouse->unbindModel(
			array('hasMany' => array(
				'Executive',
				'StockLog',
				'Stock',
				))
			);
	$this->set('warehouse_to',$this->Warehouse->find('list', array(
		"joins"=>array(array(
			"table"=>'branch_warehouse_mappings',
			"alias"=>'BranchWarehouseMapping',
			"type"=>'inner',
			"conditions"=>array('BranchWarehouseMapping.warehouse_id=Warehouse.id'),
			),
		),
		'conditions' =>$conditions_branch ,
		'order'=>array('Warehouse.name ASC'),
		'fields' => array(
			'Warehouse.id',
			'Warehouse.name',
				))));
}
else
{
$Warehouse_from=$this->Warehouse->find('list',array(
	'fields'=>array('id','name'),
	'conditions'=>['Warehouse.id'=>1],
	'order'=>array('id ASC'),
	));
$this->set('warehouse_to',$this->Warehouse->find('list',array(
		"joins"=>array(array(
			"table"=>'branches',
			"alias"=>'Branch',
			"type"=>'inner',
			"conditions"=>array('Branch.warehouse_id=Warehouse.id'),
			),
		),
		'fields'=>array('Warehouse.id','Warehouse.name'),
		'order'=>array('name ASC'),
		)));
}
$this->set('warehouse_from',$Warehouse_from);
if (!$this->request->data)
{}
else
{

$datasource_StockTransfer = $this->StockTransfer->getDataSource();
$datasource_StockTransferItem = $this->StockTransferItem->getDataSource();
$datasource_Stock = $this->Stock->getDataSource();
$datasource_Order = $this->Order->getDataSource();
try {
	$datasource_StockTransfer->begin();
	$datasource_StockTransferItem->begin();
	$datasource_Stock->begin();
	$datasource_Order->begin();
	$user_id=1;
	$data=$this->request->data['MainToBranchTransfer'];
	$Transfer=$this->StockTransfer->find('first',array('order' => 'StockTransfer.id DESC',));
	if(!empty($Transfer))
	{
		$TransferNo = $Transfer['StockTransfer']['transfer_no'];
		$transfer_no=$TransferNo+1;
	}
	else
	{
		$transfer_no=1;
	}
	$StockTransfer_data=[
	'transfer_no'=>$transfer_no,
	'date'=>date('Y-m-d'),
	'warehouse_from'=>$data['warehouse_from'],
	'warehouse_to'=>$data['warehouse_to'],
	'remarks'=>$data['remarks'],
	'status'=>1,
	'transfer_type'=>5,
	'created_by'=>1,
	'modified_by'=>1,
	'created_at'=>date('Y-m-d H:i:s'),
	'updated_at'=>date('Y-m-d H:i:s'),
	'branch_id'=>$branch_id,
	];
	$this->StockTransfer->create();
	if(!$this->StockTransfer->save($StockTransfer_data))
	{
		$errors = $this->StockTransfer->validationErrors;
		foreach ($errors as $key => $value) {
			throw new Exception($value[0], 1);
		}
	}
	$stock_transfer_id=$this->StockTransfer->getLastInsertId();
	$tray_quantity=0;
	for ($k=0; $k <count($data['tray_occupation']) ; $k++) 
	{
		if($data['product_id'][$k]!=1)
		{
			$tray_quantity+=$data['tray_occupation'][$k];
		}
	}
	for ($i=0; $i <count($data['order_id']) ; $i++) 
	{
		if($data['product_id'][$i]==1)
		{
        $data['quantity'][$i]=$data['quantity'][$i]+$tray_quantity;
		}
		 $Product=$this->Product->findById($data['product_id'][$i]);
		$StockTransferItem_data=[
		'product_id'=>$data['product_id'][$i],
		'unit_id'=>$Product['Product']['unit_id'],
		'stock_transfer_id'=>$stock_transfer_id,
		'quantity'=>$data['quantity'][$i],
		];
		$this->StockTransferItem->create();
		if(!$this->StockTransferItem->save($StockTransferItem_data))
		{
			$errors = $this->StockTransferItem->validationErrors;
			foreach ($errors as $key => $value) {
				throw new Exception($value[0], 1);
			}
		}
		else
		{
			if($data['order_id'][$i]!=0){
				$order_id=explode(',',$data['order_id'][$i]);
				for ($j=0; $j < count($order_id); $j++) { 
				$this->Order->id=$order_id[$j];
				if(!$this->Order->saveField('status','2'))
					throw new Exception('Error While status updating', 1);
				$this->OrderItem->id=$this->OrderItem->field('OrderItem.id',array('OrderItem.product_id'=>$data['product_id'][$i],'OrderItem.order_id'=>$order_id[$j]));
				if(!$this->OrderItem->saveField('second_status','4'))
					throw new Exception('Error While status updating', 1);
				}
			}
			$fromStock=$this->Stock->find('first',array(
				'conditions'=>array(
					'Stock.product_id'=>$data['product_id'][$i],
					'Stock.warehouse_id'=>$data['warehouse_from'],
					),
				'fields'=>array('Stock.id','Stock.quantity')
				));
			if(!$fromStock)
				      throw new Exception("Empty Stock", 1);
				$fromQty=$fromStock['Stock']['quantity']-$data['quantity'][$i];
				if($fromQty<0)
				      throw new Exception("Empty From Stock", 1);
				    $remark='Stock Transfer --'.$transfer_no.'('.$data['quantity'][$i].')';	
					$StockController = new StockController;
					$Stock_function_return=$StockController->GeneralStock_Edit_Function($fromStock['Stock']['id'],$fromQty,date('Y-m-d'),$user_id,$remark);
					if($Stock_function_return['result']!='Success')
						throw new Exception($Stock_function_return['result']);
			// $toStock=$this->Stock->find('first',array(
			// 	'conditions'=>array(
			// 		'Stock.product_id'=>$data['product_id'][$i],
			// 		'Stock.warehouse_id'=>$data['warehouse_id']
			// 		),
			// 	'fields'=>array('Stock.id','Stock.quantity')));
			// if(!$toStock)
			// {
			// 	$remark='Stock Transfer ['.$stock_transfer_id.','.$data['quantity'][$i].']';
			// 	$flag=1;
			// 	$StockController = new StockController;
			// 	$Stock_function_return=$StockController->GeneralStock_Add_Function($data['warehouse_id'],$data['product_id'][$i],$data['quantity'][$i],date('Y-m-d'),$user_id,$remark,$flag);
			// 	if($Stock_function_return['result']!='Success')
			// 		throw new Exception($Stock_function_return['result'], 1);
			// }
			// else
			// {
			// 	$toQty=$toStock['Stock']['quantity']+$data['quantity'][$i];
			// 	$remark='Stock Transfer ['.$stock_transfer_id.','.$data['quantity'][$i].']';
			// 	$StockController = new StockController;
			// 	$Stock_function_return=$StockController->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
			// 	if($Stock_function_return['result']!='Success')
			// 		throw new Exception($Stock_function_return['result']);
			// }
		}
	}
	$datasource_Stock->commit();
	$datasource_StockTransfer->commit();
	$datasource_StockTransferItem->commit();
	$datasource_Order->commit();
	$return['result']="success";
} catch (Exception $e){
	$datasource_StockTransfer->rollback();
	$datasource_StockTransferItem->rollback();
	$datasource_Stock->rollback();
	$datasource_Order->rollback();
	$return['result']=$e->getMessage();
	$this->Session->setFlash(__($return['result']));
	$this->redirect(array('controller' => 'Stock','action' =>'MainToBranchTransfer'));

}
$this->Session->setFlash(__($return['result']));
$this->redirect(array('controller' => 'ProductionPlan','action' =>'MainToBranchTransferList'));


}
}
public function MainToBranchTransferList()
{

	$conditions = [];
	$this->set('from',date("d-m-Y", strtotime('-1 day')));
	$this->set('to',date("d-m-Y"));

}
public function MainToBranchStockApproval($id=null)
{
      
	$user_id=1;
	$branch_id = $this->Session->read('User.branch_id');
	$this->set('Van',$this->Warehouse->find('list',array(
		'fields'=>array('id','name'),
		'order'=>array('name ASC'),
		)));
	$this->set('from',date("d-m-Y", strtotime('-31 day')));
if (!$this->request->data)
	{
		if(empty($id))
		{
		}
		else
		{
			$StockTransfer=$this->StockTransfer->findById($id);
			$this->StockTransferItem->virtualFields = array('product_name' => "CONCAT(Product.name)");
			$StockTransferItem=$this->StockTransferItem->find('all',array('conditions'=>array('stock_transfer_id'=>$id)));
			$this->request->data=$StockTransfer;
			$this->set('StockTransferItem',$StockTransferItem);
			$this->request->data['StockTransfer']['warehouse_from_name']= $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $StockTransfer['StockTransfer']['warehouse_from']));
		    $this->request->data['StockTransfer']['warehouse_to_name']= $this->Warehouse->field('Warehouse.name',array('Warehouse.id ' => $StockTransfer['StockTransfer']['warehouse_to']));
			$this->request->data['StockTransfer']['date']=date('d-m-Y',strtotime($StockTransfer['StockTransfer']['date']));
		}
	} 
	else{

		$datasource_StockTransfer = $this->StockTransfer->getDataSource();
		$datasource_StockTransferItem = $this->StockTransferItem->getDataSource();
		$datasource_Stock = $this->Stock->getDataSource();
		try {
			$datasource_StockTransfer->begin();
			$datasource_StockTransferItem->begin();
			$datasource_Stock->begin();
			$data=$this->request->data['StockTransfer'];
			if(!empty($id))
			{
			$this->StockTransfer->id=$id;
			if($data['action']=="approve")
			{
			$approved_data=[
			'status'=>2,
			'updated_at'=>date('Y-m-d H:i:s'),
			];								
				if(!$this->StockTransfer->save($approved_data))
				{
					$errors = $this->StockTransfer->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
			$stock_transfer_id=$id;
			     for ($i=0; $i <count($data['product']) ; $i++)
			    {
					$this->StockTransferItem->id=$data['item_id'][$i];
					$ApprovedItem_data=[
					'quantity'=>$data['quantity'][$i],
					];
					if(!$this->StockTransferItem->save($ApprovedItem_data))
					{
						$errors = $this->StockTransferItem->validationErrors;
						foreach ($errors as $key => $value) {
							throw new Exception($value[0], 1);
						}
					}
					$toStock=$this->Stock->find('first',array(
						'conditions'=>array(
							'Stock.product_id'=>$data['product'][$i],
							'Stock.warehouse_id'=>$data['warehouse_to'],
							),
						'fields'=>array('Stock.id','Stock.quantity')
						));
					if(!$toStock)
					{
						$flag=1;
						$remark='Stock Transfer --'.$data['transfer_no'].'('.$data['quantity'][$i].')';	
						$Stock_function_return=$this->GeneralStock_Add_Function($data['warehouse_to'],$data['product'][$i],$data['quantity'][$i],date('Y-m-d'),$user_id,$remark,$flag);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result'], 1);
					}
					else
					{
						
						$toQty=$toStock['Stock']['quantity']+$data['quantity'][$i];
						$remark='Stock Transfer --'.$data['transfer_no'].'('.$data['quantity'][$i].')';	
						$Stock_function_return=$this->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
					}
					
              }
            }
            else
            {
                  
				$approved_data=[
				'status'=>2,
				'flag'=>0,
				'updated_at'=>date('Y-m-d H:i:s'),
				];
				if(!$this->StockTransfer->save($approved_data))
				{
					$errors = $this->StockTransfer->validationErrors;
					foreach ($errors as $key => $value) {
						throw new Exception($value[0], 1);
					}
				}
				else
				{
			     for ($i=0; $i <count($data['product']) ; $i++)
				    {
						$quantity=$data['quantity'][$i];
						$item=$this->StockTransferItem->findById($id);
						$toStock=$this->Stock->find('first',array(
						'conditions'=>array(
							'Stock.product_id'=>$data['product'][$i],
							'Stock.warehouse_id'=>$data['warehouse_from'],
							),
						'fields'=>array('Stock.id','Stock.quantity')
						));
						$toQty=$toStock['Stock']['quantity']+$quantity;
						$remark='Stock Transfer --'.$data['transfer_no'].'('.$data['quantity'][$i].')';	
						$StockController = new StockController;
						$Stock_function_return=$StockController->GeneralStock_Edit_Function($toStock['Stock']['id'],$toQty,date('Y-m-d'),$user_id,$remark);
						if($Stock_function_return['result']!='Success')
							throw new Exception($Stock_function_return['result']);
					}
				}
            }
        }
			$datasource_Stock->commit();
			$datasource_StockTransfer->commit();
			$datasource_StockTransferItem->commit();
			$return['result']="success";
		} catch (Exception $e){
			$datasource_StockTransfer->rollback();
			$datasource_StockTransferItem->rollback();
			$datasource_Stock->rollback();
			$return['result']=$e->getMessage();
		}
		$this->Session->setFlash(__($return['result']));
		$this->redirect(array('controller' => 'Stock','action' =>'MainToBranchTransferList'));

	
	
	}	
}				
public function main_to_branch_transfer($warehouse_from,$warehouse_to=null,$date)
{

$return['result']='Empty';
$return['orderitem']=[];
$conditions=[];$conditions_branch=[];$condition_products=[];
$condition_products['Product.type']=[2,8];
$branch_id=$this->Branch->findByWarehouseId($warehouse_to);
$conditions_branch['BranchWarehouseMapping.branch_id']=$branch_id['Branch']['id'];
$this->Warehouse->unbindModel(
			array('hasMany' => array(
				'Executive',
				'StockLog',
				'Stock',
				))
			);
	$warehouses = $this->Warehouse->find('list', array(
		"joins"=>array(array(
			"table"=>'branch_warehouse_mappings',
			"alias"=>'BranchWarehouseMapping',
			"type"=>'inner',
			"conditions"=>array('BranchWarehouseMapping.warehouse_id=Warehouse.id'),
			),
		),
		'conditions' =>$conditions_branch ,
		'order'=>array('Warehouse.name ASC'),
		'fields' => array(
			'Warehouse.id',
			'Warehouse.id',
				// 'Warehouse.branch_id',
			)
		)
	);
	$conditions['Executive.warehouse_id']=$warehouses;
$this->Product->unbindModel(array('belongsTo'=>array('ProductType','Brand','Unit'),'hasMany' => array('PurchaseReturnItem','UnwantedList','Stock','StockLog','SalesReturnItem','SaleItem','PurchasedItem','Unit','Brand')));
$Product_list=$this->Product->find('all',array(
	"joins"=>array(
		// array(
		// 	"table"=>'order_items',
		// 	"alias"=>'OrderItem',
		// 	"type"=>'inner',
		// 	"conditions"=>array('Product.id=OrderItem.product_id'),
		// 	),
		// array(
		// 	"table"=>'orders',
		// 	"alias"=>'Order',
		// 	"type"=>'inner',
		// 	"conditions"=>array('Order.id=OrderItem.order_id'),
		// 	),
		// array(
		// 	"table"=>'executives',
		// 	"alias"=>'Executive',
		// 	"type"=>'inner',
		// 	"conditions"=>array('Executive.id=Order.executive_id'),
		// 	),
		// array(
		// 	"table"=>'warehouses',
		// 	"alias"=>'Warehouse',
		// 	"type"=>'inner',
		// 	"conditions"=>array('Warehouse.id=Executive.warehouse_id'),
		// 	),
		),
	//  'group' => 'OrderItem.product_id',
	  'order' => array('Product.priority' => 'ASC'),
	'conditions'=>$condition_products,
	'fields'=>['Product.name','Product.id','Product.tray_occupation']
	));
$orderitem=[];
if(!empty($Product_list)){
	$tary_list=$this->Product->find('first',array('conditions'=>['type'=>5],
		'fields'=>['Product.id','Product.name','Product.tray_occupation']));
	$single['name']=$tary_list['Product']['name'];
	$single['id']=$tary_list['Product']['id'];
	$single['order_id']=0;
	$single['warehouse_name']="";
	$single['warehouse_id']="";
	$single['hidden_tray_occupation']=$tary_list['Product']['tray_occupation'];
	$fromStock_tray=$this->Stock->find('first',array(
		'conditions'=>array(
			'Stock.product_id'=>$tary_list['Product']['id'],
			'Stock.warehouse_id'=>1,
			),
		'fields'=>array('Stock.id','Stock.quantity')
		));
	$single['stock_quantity']=floatval($fromStock_tray['Stock']['quantity']);
	$single['quantity']=0;
	$single['tray_occupation']=0;
	array_push($orderitem,$single);
		foreach ($Product_list as $key => $value) {
	$single['order_id']=0;
	$single['name']=$value['Product']['name'];
	$single['id']=$value['Product']['id'];
	$single['hidden_tray_occupation']=$value['Product']['tray_occupation'];
	$fromStock=$this->Stock->find('first',array(
	'conditions'=>array(
	'Stock.product_id'=>$value['Product']['id'],
	'Stock.warehouse_id'=>$warehouse_from,
	),
	'fields'=>array('Stock.id','Stock.quantity')
	));
	$single['stock_quantity']=floatval($fromStock['Stock']['quantity']);
    $single['quantity']=0;
	$single['tray_occupation']=0;
	$conditions['OrderItem.second_status']=3;	
	$conditions['Order.date_of_delivered']=date('Y-m-d',strtotime($date));	
	$conditions['Order.type_order']=1;	
	$conditions['Order.flag']=1;	
	$conditions['OrderItem.quantity !=']=0;
    $conditions['OrderItem.product_id']=$value['Product']['id'];
    $order_list=$this->OrderItem->find('all',array(
    		"joins"=>array(
		array(
			"table"=>'executives',
			"alias"=>'Executive',
			"type"=>'inner',
			"conditions"=>array('Executive.id=Order.executive_id'),
			),
		array(
			"table"=>'warehouses',
			"alias"=>'Warehouse',
			"type"=>'inner',
			"conditions"=>array('Warehouse.id=Executive.warehouse_id'),
			),
		),
	'conditions'=>$conditions,
	'fields'=>['OrderItem.order_id']
	));
$order_id='';
if(!empty($order_list))
{
foreach ($order_list as $key2 => $value_order) {
	if($order_id=='')
	{
	$order_id=$value_order['OrderItem']['order_id'];
	}
	else
	{
	$order_id=$order_id.','.$value_order['OrderItem']['order_id'];
	}
    }
    $single['order_id']=$order_id;
    $this->OrderItem->virtualFields=['total_quantity'=>"SUM(OrderItem.quantity)"];
	$OrderItem_all=$this->OrderItem->find('first',array(
	"joins"=>array(
		array(
			"table"=>'executives',
			"alias"=>'Executive',
			"type"=>'inner',
			"conditions"=>array('Executive.id=Order.executive_id'),
			),
		array(
			"table"=>'warehouses',
			"alias"=>'Warehouse',
			"type"=>'inner',
			"conditions"=>array('Warehouse.id=Executive.warehouse_id'),
			),
		),
	'conditions'=>$conditions,
	'fields'=>['OrderItem.total_quantity','Order.id','Warehouse.id','Warehouse.name']
	));
		//$single['warehouse_name']=$OrderItem_all['Warehouse']['name'];
		//$single['warehouse_id']=$OrderItem_all['Warehouse']['id'];
		$single['quantity']=floatval($OrderItem_all['OrderItem']['total_quantity']);
		if($value['Product']['tray_occupation']!=0)
		{
			$quantity=$OrderItem_all['OrderItem']['total_quantity']/$value['Product']['tray_occupation'];
			$whole = round($quantity,2);        
			$fraction =$OrderItem_all['OrderItem']['total_quantity']%$value['Product']['tray_occupation']; 
			$pieces=number_format($fraction);
			$tray=$whole;
			$tray_piece=0;
			if($pieces>=1)
			{
				$tray_piece=1;
			}
			$single['tray_occupation']=$whole;
		}
		else
		{
			$single['tray_occupation']=0;
		}
	 }
		array_push($orderitem,$single);
	}
}
$return['orderitem']=$orderitem;    
$return['result']='Success';
echo json_encode($return);
exit;
}
public function get_branch_warehouse($id=null)
{
	$return['result']='Error';
	$conditions=array();
		 // pr($id);
	if(!empty($id))
	{
		$branch_id=$this->Branch->findByWarehouseId($id);
		$conditions['BranchWarehouseMapping.branch_id']=$branch_id['Branch']['id'];
	}
$this->Warehouse->unbindModel(
			array('hasMany' => array(
				'Executive',
				'StockLog',
				'Stock',
				))
			);
	$return['option']='<option value="">Select To Warehouse</option>';
	$warehouses = $this->Warehouse->find('all', array(
		"joins"=>array(array(
			"table"=>'branch_warehouse_mappings',
			"alias"=>'BranchWarehouseMapping',
			"type"=>'inner',
			"conditions"=>array('BranchWarehouseMapping.warehouse_id=Warehouse.id'),
			),
		),
		'conditions' =>$conditions ,
		'order'=>array('Warehouse.name ASC'),
		'fields' => array(
			'Warehouse.id',
			'Warehouse.name',
				// 'Warehouse.branch_id',
			)
		)
	);
	foreach ($warehouses as $key => $value) {
		$return['result']='Success';
		$return['option']=$return['option'].'<option value='.$value['Warehouse']['id'].'>'.$value['Warehouse']['name'].'</option>';
	}
	echo json_encode($return);
	exit;
}
}
