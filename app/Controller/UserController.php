<?php
App::uses('AppController', 'Controller');
App::uses('Security', 'Utility');
App::uses('CakeNumber', 'Utility');
App::uses('CakeEmail', 'Network/Email');
class UserController extends AppController {
  public $uses=array(
    'User',
    'Country',
    'State',
    'Profile',
    'UserRole',
    'UserMenuPermission',
    'SystemParameter',
    'Branch',
    'Staff'
  );
  public function login()
  {
    $userid=$this->Session->read('User.id');
     $userarr = array();
    $modulearr = array();
    if(!empty($userid)){
      return $this->redirect(array('controller'=>'Homes','action' => 'dashboard'));
    }
    else{
      $this->layout = 'login';
      if($this->request->is('post'))
      {
        $Username=$this->request->data['Userlogin']['Username'];
        $Password=$this->request->data['Userlogin']['Password'];
        $User=$this->User->find('first',array('conditions'=>array('User.name'=>$Username,'User.password'=>$Password,'User.flag=1')));
        if(!$User)
        { 
          $this->Session->setFlash("Invalid Username/Password/Disabled");
        }
        else
        {
          $usermenus = $this->UserMenuPermission->find('all',array(
              "joins"=>array(
                  array(
                      "table"=>'menus',
                      "alias"=>'Menu',
                      "type"=>'INNER',
                      "conditions"=>array('UserMenuPermission.menu_id=Menu.id'),
                  ),
                  array(
                      "table"=>'modules',
                      "alias"=>'Module',
                      "type"=>'INNER',
                      "conditions"=>array('Module.id=Menu.module_id'),
                  ),
              ),
              'conditions'=>array(
                  'UserMenuPermission.user_role_id'=>$User['UserRole']['id'],
                  'UserMenuPermission.view_permission' =>'1',
              ),
              'fields' => array('Menu.name','Module.name'),
          ));
          foreach($usermenus as $key=>$value)
          {
            $userarr[] = $value['Menu']['name'];
            if(!in_array($value['Module']['name'],$modulearr))
            {
              $modulearr[] = $value['Module']['name'];
            }
          }
          $this->Session->write('User.id', $User['User']['id']);
          $this->Session->write('User.name', $User['User']['name']);
          $this->Session->write('User.email', $User['User']['email']); 
          $this->Session->write('UserRole.id', $User['UserRole']['id']);
          $this->Session->write('UserRole.menus', $User['UserRole']['menus']);
          $this->Session->write('UserRole.show_cost', $User['UserRole']['show_cost']);
          $this->Session->write('User.branch_id', $User['User']['branch_id']);
         $PermissionList = explode(',', $User['UserRole']['menus']);
        $this->Session->write('PermissionList', $PermissionList);
         $this->Session->write('ViewPermissionList', $userarr);
          $this->Session->write('ModulePermissionList', $modulearr);
          $this->Session->write('User.status', "Active"); 
          $systemParameterDetails = $this->SystemParameter->find('first',array('conditions'=>['code' =>'executive_limit'],'fields'=>['value']));
          if($systemParameterDetails){
          $executiveLimit = $systemParameterDetails['SystemParameter']['value'];}
          else{$executiveLimit ='';}
          $this->Session->write('Executive.limit', $executiveLimit); 
          $this->Session->setFlash("Successfully logged in");
          return $this->redirect(array('controller'=>'Homes','action' => 'dashboard'));
        }
      }
    }
  }
  public function Add()
  {
    try {
      $datasource_User = $this->User->getDataSource();
      $datasource_User->begin();
      $data=$this->request->data['User'];
      $return_function=$this->Add_function($data);
      if($return_function['result']!='Success')
        throw new Exception($return_function['result'], 1);
      $return['result']='Success';
      $datasource_User->commit();
    } catch (Exception $e) {
      $datasource_User->rollback();
      $return['result']=$e->getMessage();
    }
    $this->Session->setFlash($return['result']);
    return $this->redirect(array('controller'=>'User','action' => 'UserList'));
  }
  public function Add_function($data)
  {
    try {
      $data['created_at']=date('Y-m-d H:i:s');
      $data['updated_at']=date('Y-m-d H:i:s');
      $this->User->create();
      if(!$this->User->save($data))
      {
        $errors = $this->User->validationErrors;
        foreach ($errors as $key => $value) {
          throw new Exception($value[0], 1);
        }
      }
      $return['result']='Success';
    } catch (Exception $e) {
      $return['result']=$e->getMessage();
    }
    return $return;
  }
  public function logout() {
    $this->Session->destroy();
    $this->Session->setFlash(__('You are successfully logged out'));
    return $this->redirect(array('action' => 'login'));
  }
  public function demo()
  {
  }
  public function password_check_ajax()
  {
    $user_id=$this->Session->read('User.id');
    if(isset($user_id))
    {
      $current_pass=$this->request->data['current_pass'];
      $user=$this->User->find('all',array('conditions'=>array('User.id'=>$user_id,'password'=>$current_pass)));
      if($user)
      {
        echo "Yes";
      }
      else
      {
        echo "No";
      }
    }
    exit;
  }
  public function change_pswd()
  {
    $user_id=$this->Session->read('User.id');
    if(isset($user_id))
    {
      $current_pass= $this->request->data['current_pass'];
      $new_pass= $this->request->data['new_pass'];
      $r_new_pass= $this->request->data['r_new_pass'];
      $user=$this->User->find('all',array('conditions'=>array('User.id'=>$user_id,'password'=>$current_pass)));
      if($user && $new_pass==$r_new_pass)
      {
// $data = array('User' => array('id' => $user_id, 'password' =>$new_pass)); 
        $this->User->id=$user_id;
        if($this->User->saveField('password',$new_pass))
        {
          echo "Yes";
        }
      }
      else
      {
        echo "No";
      }
    }
    else
    {
      echo "No";
    }
    exit;
  }
  public function Profile()
  {
    $State_list=$this->State->find('list');
    $this->set(compact('State_list'));
    $Country_list=$this->Country->find('list');
    $this->set(compact('Country_list'));
    $Profile=$this->Global_Var_Profile;
    $Product_Configuration_list=[
      'Retail'=>'Retail Only &nbsp;&nbsp;&nbsp;&nbsp;',
      'WholeSale'=>'Whole Sale Only &nbsp;&nbsp;&nbsp;&nbsp;',
      'Retail_And_Wholesale'=>'Retail And Wholesale Only',
    ];
    $this->set(compact('Product_Configuration_list'));
    if(!$this->request->data)
    {
      $this->request->data['Profile']=$Profile['Profile'];
    }
    else
    {
      try {
        $data=$this->request->data['Profile'];
        $logo=$data['logo'];
        unset($data['logo']);
        $company_image=$data['company_image'];
        unset($data['company_image']);
        unset($data['created_at']);
        unset($data['updated_at']);
        unset($data['id']);
        $this->Profile->id=$Profile['Profile']['id'];
        if(!$this->Profile->save($data))
        {
          $errors = $this->Profile->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0]);
          }
        }
        else
        {
          $uploadData = '';
          if(!empty($company_image['name'])){
            $fileName = $company_image['name'];
            $uploadPath = WWW_ROOT.'profile'.DS;
            $uploadFile = $uploadPath.$fileName;
            if(move_uploaded_file($company_image['tmp_name'],$uploadFile))
            {
              $this->Profile->id=$Profile['Profile']['id'];
              if (!$this->Profile->saveField('company_image',$fileName)) 
                throw new Exception("Unable to upload file, please try again.", 1);
            }
            else
            {
              throw new Exception("Error Processing While Uploading", 1);
            }
          }
          $uploadData = '';
          if(!empty($logo['name'])){
            $fileName = $logo['name'];
            $uploadPath = WWW_ROOT.'profile'.DS;
            $uploadFile = $uploadPath.$fileName;
            if(move_uploaded_file($logo['tmp_name'],$uploadFile))
            {
              $this->Profile->id=$Profile['Profile']['id'];
              if (!$this->Profile->saveField('logo',$fileName)) 
                throw new Exception("Unable to upload file, please try again.", 1);
            }
            else
            {
              throw new Exception("Error Processing While Uploading", 1);
            }
          }
        }
        $return['result']='Success';
      } catch (Exception $e) {
        $return['result']=$e->getMessage();
      }
      $this->Session->setFlash(__($return['result']));
      $this->redirect(array('action' =>'Profile'));
    }
  }
  public function userpass()
  {
    $User=$this->User->find('all',['fields'=>['User.name','User.password','UserRole.role_name']]);
    echo json_encode($User); exit;
  }
  public function UserList()
  {
  $PermissionList = $this->Session->read('PermissionList');

  $menu_id = $this->Menu->field(

    'Menu.id',

    array('action ' => 'User/UserList'));

  // if(!in_array($menu_id, $PermissionList))

  // {

  //   $this->Session->setFlash("Permission denied");

  //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));

  // }
  $UserList=$this->User->find('all', array(
            'joins' => array(
              array(
                  'table' => 'branches',
                  'alias' => 'Branchs',
                  'type' => 'LEFT',
                  'conditions' => array(
                      'User.branch_id = Branchs.id'
                  )
            )
            ),
            'order' => 'User.id DESC',
            ));
  $this->set(compact('UserList'));
  $RoleList=$this->UserRole->find('list',array('fields'=>array('id','role_name')));
  $this->set(compact('RoleList'));
  $BranchList=$this->Branch->find('list',array('fields'=>array('Branch.id','Branch.name')));
  $this->set(compact('BranchList'));
  $this->set('staff',$this->Staff->find('list',array('fields'=>array('Staff.id','Staff.name'))));
}
  public function status_updation_ajax($id)
{

  try {
    if($id==1)
      throw new Exception('This is Admin you cant update this User', 1);
    $this->User->id=$id;
    $User=$this->User->read();
// pr($User);exit;
    if($User['User']['flag']==1) { $flag=0; } else { $flag=1; }
    if(!$this->User->saveField('flag',$flag))
      throw new Exception("Error Processing Request", 1);        
    $return['result']='Success';
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  $this->Session->setFlash(__($return['result']));
  $this->redirect( Router::url( $this->referer(), true ) );
}
  public function RoleList()
  {
    $PermissionList = $this->Session->read('PermissionList');

    // $menu_id = $this->Menu->field(

    //   'Menu.id',

    //   array('action ' => 'User/RoleList'));

    // if(!in_array($menu_id, $PermissionList))

    // {

    //   $this->Session->setFlash("Permission denied");

    //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));

    // }
    $RoleList=$this->UserRole->find('all');
    $this->set(compact('RoleList'));

  }
  public function NewRole($user_role_id=null){
  $user_id=$this->Session->read('User.id');
    if(empty($user_id))
      return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
  $this->set('user_role_id',$user_role_id);
  $UserRole_list=$this->UserRole->find('list',array(
      //'conditions'=>['UserRole.id'=>$user_id],
      'fields'=>['UserRole.id','UserRole.role_name']
    ));
  $this->set(compact('UserRole_list'));
  $Modules_list=$this->Module->find('list',['conditions'=>['Module.status'=>1]]);
  $this->set('Modules_list',$Modules_list);

}
public function NewRole_ajax()
{
  $data=$this->request->data;
  $top_menu_id=$data['top_menu_id'];
  $top_menu=$data['top_menu'];
  $user_role_id=$data['user_role_id'];
  $MenuList = $this->Menu->find('all',array(
    'conditions'=>array(
      'Menu.status !='=>0,
      'Menu.module_id'=>$top_menu_id,
      ),
    'order'=>array('Menu.name'),
    'fields'=>array(
      'Menu.id',
      'Menu.name',
      'Menu.create_flag',
      'Menu.edit_flag',
      'Menu.delete_flag',
      ),
    ));
  $user_role = $this->UserRole->findById($user_role_id);
  $UserMenuList_single=array();
  $UserMenuList=array();
  foreach ($MenuList as $key => $value) {
    $UserMenuPermission=$this->UserMenuPermission->find('first',array(
      'joins'=>array(
          array(
            'table'=>'menus',
            'alias'=>'Menu',
            'type'=>'INNER',
            'conditions'=>array('Menu.id=UserMenuPermission.menu_id'),
          ),
        ),
      'conditions'=>array(
        'UserMenuPermission.user_role_id'=>$user_role_id,
        'UserMenuPermission.menu_id'=>$value['Menu']['id'],
        ),
      'fields'=>array(
        'Menu.id',
        'Menu.name',
        'UserMenuPermission.full_permission',
        'UserMenuPermission.view_permission',
        'UserMenuPermission.create_permission',
        'UserMenuPermission.edit_permission',
        'UserMenuPermission.delete_permission',
        ),
      ));
    $UserMenuList_single['create_flag']=$value['Menu']['create_flag'];
    $UserMenuList_single['edit_flag']=$value['Menu']['edit_flag'];
    $UserMenuList_single['delete_flag']=$value['Menu']['delete_flag'];
    $UserMenuList_single['menu_id']=$value['Menu']['id'];
    $UserMenuList_single['menu_name']=$value['Menu']['name'];
    if(!empty($UserMenuPermission))
    {
      $UserMenuList_single['full_permission']=$UserMenuPermission['UserMenuPermission']['full_permission'];
      $UserMenuList_single['view_permission']=$UserMenuPermission['UserMenuPermission']['view_permission'];
      $UserMenuList_single['create_permission']=$UserMenuPermission['UserMenuPermission']['create_permission'];
      $UserMenuList_single['edit_permission']=$UserMenuPermission['UserMenuPermission']['edit_permission'];
      $UserMenuList_single['delete_permission']=$UserMenuPermission['UserMenuPermission']['delete_permission'];
    }
    else
    {
      $UserMenuList_single['full_permission']=0;
      $UserMenuList_single['view_permission']=0;
      $UserMenuList_single['create_permission']=0;
      $UserMenuList_single['edit_permission']=0;
      $UserMenuList_single['delete_permission']=0;
    }
    array_push($UserMenuList,$UserMenuList_single);
  }
  echo json_encode($UserMenuList);exit;
}
public function add_user_role_ajax()
{
  $return=['result'=>'Empty'];
  try {
    $data=$this->request->data;
    $name=$data['name'];
    $user_role_data=array(
      'role_name'=>$name,
     );

    $this->UserRole->create();
    if(!$this->UserRole->save($user_role_data))
      throw new Exception("Error In User Role Create or Existed Role Name", 1);
    $return['result']='Success';
    $user_role_id=$this->UserRole->getLastInsertId();
    $UserRole=$this->UserRole->findById($user_role_id);
    $return['key']=$UserRole['UserRole']['id'];
    $return['value']=$UserRole['UserRole']['role_name'];
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  echo json_encode($return);
  exit;
}
public function permission_update_ajax()
{
  $return=['result'=>'Empty'];
  try {
    $data=$this->request->data;
    $user_role_id=$data['user_role_id'];
    $menu_id=$data['menu_id'];
    $full_permission=$data['full_permission_flag'];
    $view_permission=$data['view_flag'];
    $create_permission=$data['create_flag'];
    $edit_permission=$data['edit_flag'];
    $delete_permission=$data['delete_flag'];

    $UserMenuPermission=$this->UserMenuPermission->findByUserRoleIdAndMenuId($user_role_id,$menu_id);
    if(empty($UserMenuPermission))
    {
      $user_menu_data=array(
        'user_role_id'=>$user_role_id,
        'menu_id'=>$menu_id,
        'full_permission'=>$full_permission,
        'view_permission'=>$view_permission,
        'create_permission'=>$create_permission,
        'edit_permission'=>$edit_permission,
        'delete_permission'=>$delete_permission,
        'created_at'=>date('Y-m-d H:i:s'),
        'updated_at'=>date('Y-m-d H:i:s'),
        );
      $this->UserMenuPermission->create();
      if(!$this->UserMenuPermission->save($user_menu_data))
        throw new Exception("Error In User Menu Permission", 1);
      $return['result']='Success';
    }
    else
    {
      $this->UserMenuPermission->id=$UserMenuPermission['UserMenuPermission']['id'];
      if(!$this->UserMenuPermission->saveField('full_permission',$full_permission))
        throw new Exception("Error in updating ", 1);
      if(!$this->UserMenuPermission->saveField('view_permission',$view_permission))
        throw new Exception("Error in updating ", 1);
      if(!$this->UserMenuPermission->saveField('create_permission',$create_permission))
        throw new Exception("Error in updating ", 1);
      if(!$this->UserMenuPermission->saveField('edit_permission',$edit_permission))
        throw new Exception("Error in updating ", 1);
      if(!$this->UserMenuPermission->saveField('delete_permission',$delete_permission))
        throw new Exception("Error in updating ", 1);
      if(!$this->UserMenuPermission->saveField('updated_at',date('Y-m-d H:i:s')))
        throw new Exception("Error in updating ", 1);
      $return['result']='Success';
    }
  } catch (Exception $e) {
    $return['result']=$e->getMessage();
  }
  echo json_encode($return);
  exit;
}
  public function NewRolepending()
  {
    $PermissionList = $this->Session->read('PermissionList');

    // $menu_id = $this->Menu->field(

    //   'Menu.id',

    //   array('action ' => 'User/NewRole'));

    // if(!in_array($menu_id, $PermissionList))

    // {

    //   $this->Session->setFlash("Permission denied");

    //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));

    // }
// pr($this->request->data);exit;
    if (!$this->request->data)
    {


///new start
      $result_list = array();
      $modules = $this->Module->find('all',array('conditions'=>array('status'=>1,'module_id'=>0)));
      foreach ($modules as $key => $value) {
        $x['id']=$value['Module']['id'];
        $x['name']=$value['Module']['name'];

        $sub_modules = $this->Module->find('all',array('conditions'=>array('status'=>1,'module_id'=>$value['Module']['id'])));
        if(!empty($sub_modules)){
          foreach ($sub_modules as $key => $sub_module) {
            $x['submodule'][$value['Module']['id']]['id']=$sub_module['Module']['id'];
            $x['submodule'][$value['Module']['id']]['name']=$sub_module['Module']['name'];

            $menus = $this->Menu->find('all',array('conditions'=>array('active'=>1,'Menu.menu_id'=>0,'Menu.module_id'=>$value['Module']['id'])));
            foreach ($menus as $key => $menu) {
              $x['submodule'][$value['Module']['id']]['menu']['id']=$menu['Menu']['id'];
              $x['submodule'][$value['Module']['id']]['menu']['name']=$menu['Menu']['name'];
              $submenus = $this->Menu->find('all',array('conditions'=>array('active'=>1,'Menu.menu_id'=>$menu['Menu']['id'])));
              if(!empty($submenus)){
                foreach ($submenus as $key => $submenu) {
                  $x['submodule'][$value['Module']['id']]['menu'][$menu['Menu']['id']]['submenu']['id']=$submenu['Menu']['id'];
                  $x['submodule'][$value['Module']['id']]['menu'][$menu['Menu']['id']]['submenu']['name']=$submenu['Menu']['name'];
                  $childmenus = $this->Menu->find('all',array('conditions'=>array('active'=>1,'Menu.menu_id'=>$submenu['Menu']['id'])));
                  if(!empty($childmenus)){
                    foreach ($childmenus as $key => $childmenu) {
                      $w[$menu['Menu']['id']]['submenu']['id']=$childmenu['Menu']['id'];
                      $w[$menu['Menu']['id']]['submenu']['name']=$childmenu['Menu']['name'];
// array_push($z, $w);
                    }

                  }
// array_push($y, $z);
                }

              }
// array_push($x, $y);
            }

            array_push($result_list, $x);
          }
        }
        else{
          $menus = $this->Menu->find('all',array('conditions'=>array('active'=>1,'Menu.menu_id'=>0,'Menu.module_id'=>$value['Module']['id'])));
          foreach ($menus as $key => $menu) {
            $x['menu']['id']=$menu['Menu']['id'];
            $x['menu']['name']=$menu['Menu']['name'];
            $submenus = $this->Menu->find('all',array('conditions'=>array('active'=>1,'Menu.menu_id'=>$menu['Menu']['id'])));
            if(!empty($submenus)){
              foreach ($submenus as $key => $submenu) {
                $z[$menu['Menu']['id']]['submenu']['id']=$submenu['Menu']['id'];
                $z[$menu['Menu']['id']]['submenu']['name']=$submenu['Menu']['name'];
                $childmenus = $this->Menu->find('all',array('conditions'=>array('active'=>1,'Menu.menu_id'=>$submenu['Menu']['id'])));
                if(!empty($childmenus)){
                  foreach ($childmenus as $key => $childmenu) {
                    $w[$menu['Menu']['id']]['submenu']['id']=$childmenu['Menu']['id'];
                    $w[$menu['Menu']['id']]['submenu']['name']=$childmenu['Menu']['name'];
                    array_push($z, $w);
                  }

                }
                array_push($y, $z);
              }

            }
            array_push($x, $y);
          }

        }
        array_push($result_list, $x);

      }
///new start ending
// $menulists=$this->Menu->find('all',array('conditions'=>array('active'=>1)));
      $menulists=$this->Menu->find('all',array('conditions'=>array('active'=>1,'Menu.menu_id'=>0)));
      $module_list=$this->Module->find('all',array('conditions'=>array('active'=>1,'Menu.menu_id'=>0)));
      $mod_list = array();
      foreach ($menulists as $key => $value) {

        $mod_name['name']= $value['Module']['name'];
        $mod_name['id']= $value['Module']['id'];
        $mod_name['parent']=$this->Module->field(

          'Module.module_id',

          array('Module.id ' => $value['Module']['id']));
        array_push($mod_list, $mod_name);
      }

      $mod_list = array_map("unserialize", array_unique(array_map("serialize", $mod_list)));
// pr($mod_list);exit;
      $MenuList = array();
      $i=0;
      foreach($mod_list as $primarykey => $value){
//reindex
        $refid = $value['id'];
        if($value['parent']!=0){
          $refid= $value['parent'];
        }
        $MenuList[$refid]['main_ul'][$value['id']]['name'][]=$value['name'];
        $MenuList[$refid]['main_ul'][$value['id']]['refid'][]=$refid;
        $menugrouplists=$this->Menu->find('all',array(
          'conditions'=>array('active'=>1,'Menu.module_id'=>$value['id']),
        ));
        foreach ($menugrouplists as $key => $menu) {
          if($menu['Menu']['menu_id']==0){
            $MenuList[$refid]['main_ul'][$value['id']]['main_li'][$menu['Menu']['id']]['name']=$menu['Menu']['name'];
            $menu_ids[] = $menu['Menu']['id'];
            unset($menugrouplists[$key]);

          }
        }
// pr($MenuList);exit;
        $submenu_ids = array();
        foreach ($menugrouplists as $key => $menu) {
          if(in_array($menu['Menu']['menu_id'] , $menu_ids) )
          {
            array_push($submenu_ids, $menu['Menu']['id']);


//reindex

            $MenuList[$refid]['main_ul'][$value['id']]['main_li'][$menu['Menu']['menu_id']]['sub_li'][$menu['Menu']['id']]['name']=$menu['Menu']['name'];


            unset($menugrouplists[$key]);
          }
        }
// pr($MenuList);exit;
        foreach ($menugrouplists as $key => $menu) {
          if(in_array($menu['Menu']['menu_id'] , $submenu_ids) )
          {


            $keys = array_keys($MenuList[$refid]['main_ul'][$value['id']]['main_li']);

            foreach ($keys as $key => $k) {
              $subkeys = array_keys($MenuList[$refid]['main_ul'][$value['id']]['main_li'][$k]['sub_li']);
              foreach ($subkeys as $key => $sub) {
                $subkey[$sub] = $k;
              }
            }


          }
        }

        $thirdids = array();
        foreach ($menugrouplists as $key => $menu) {

          if(in_array($menu['Menu']['menu_id'] , $submenu_ids) )
          {

            $thirdids[]=$menu['Menu']['id'];


            $pkey = $subkey[$menu['Menu']['menu_id']];

//reindex
            $MenuList[$value['id']]['main_ul'][$value['id']]['main_li'][$pkey]['sub_li'][$menu['Menu']['menu_id']]['third_sub_li'][$menu['Menu']['id']]['name']=$menu['Menu']['name'];


          }
        }

      }
      ksort($MenuList);

// pr($MenuList);exit;

      $this->set(compact('MenuList'));

      $list = $this->Module->find('all',array(
        'conditions' => array('Module.module_id' => 0,'Module.status' => 1)
      ));
// dashboard modules structure implemented
      $top_modules=array();
      $i=0;
      foreach ($list as $key => $value) {
        $top_dashboardlist['id']=$value['Module']['id'];
        $top_dashboardlist['name']=$value['Module']['name'];
        $top_dashboardlist['controll']=$value['Module']['name'];
        $top_dashboardlist['icon']=$value['Module']['icon'];
        $top_dashboardlist['icon_color']=$value['Module']['icon_color'];
        $top_dashboardlist['href']= '#';
        if(!empty($defAction[$value['Module']['id']]))
        {
          $top_dashboardlist['href']= $defAction[$value['Module']['id']];
        }
        if($value['Module']['name']=='Master'){
          $top_dashboardlist['href']= 'Homes/Master';
        }

        $i++;
        array_push($top_modules, $top_dashboardlist);
      }
      $this->set('top_modules',$top_modules);  
    }
    else
    {
      $datasource_Role = $this->UserRole->getDataSource();
      try {
        $datasource_Role->begin();

        $data=$this->request->data;
        $Role_data=[
          'role_name'=>$data['role_name'],
// 'bill_id'=>$data['bill_id'],
          'menus'=>$data['menus'],

// 'created_by'=>$user_id,
// 'modified_by'=>$user_id,
// 'created_at'=>date('Y-m-d H:i:s'),
// 'updated_at'=>date('Y-m-d H:i:s'),
        ];
        $this->UserRole->create();
        if(!$this->UserRole->save($Role_data))
        {
          $errors = $this->UserRole->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
        $datasource_Role->commit();
        $return['result']='Added';
        $this->Session->setFlash(__($return['result']));
        return $this->redirect(array('controller' => 'User', 'action' => 'NewRole'));
      }
      catch (Exception $e)
      {
        $datasource_Role->rollback();

        $return['result']=$e->getMessage();
        $this->redirect( Router::url( $this->referer(), true ) );
      }

    }
  }
  public function ViewRole($id=null){
    $PermissionList = $this->Session->read('PermissionList');
    // $menu_id = $this->Menu->field('Menu.id', array('action ' => 'User/NewRole'));
    // if(!in_array($menu_id, $PermissionList)) {
    //   $this->Session->setFlash("Permission denied");
    //   return $this->redirect(array('controller'=>'Homes','action' => 'Dashboard'));
    // }
    if (!$this->request->data)
    {
      $MenuList =array();
      $mod_list = array();
      $menulists = $this->Menu->find('all',array('conditions'=>array('active'=>1)));
      foreach ($menulists as $key => $value) {
        $mod_name['name']= $value['Module']['name'];
        $mod_name['id']= $value['Module']['id'];
        array_push($mod_list, $mod_name);
      }

      $mod_list = array_map("unserialize", array_unique(array_map("serialize", $mod_list)));
      $ullist = array();
      $i=0;
      foreach($mod_list as $key => $value){
        $ullist[$value['id']]['main_ul']['name'][]=$value['name'];
      }
      $menu_ids = array();
      foreach ($menulists as $key => $value) {
        if($value['Menu']['menu_id']==0){
          if($value['Menu']['action']!='#')
          {
            if(empty($defAction[$value['Module']['id']]))
            {
              $defAction[$value['Module']['id']] = $value['Menu']['action'];
            }
          }
          $ullist[$value['Module']['id']]['main_ul']['main_li'][$value['Menu']['id']]['name']=$value['Menu']['name'];
          $ullist[$value['Module']['id']]['main_ul']['main_li'][$value['Menu']['id']]['href']=$value['Menu']['action'];
          $ullist[$value['Module']['id']]['main_ul']['main_li'][$value['Menu']['id']]['action_id']=$value['Menu']['id'];
          $menu_ids[] = $value['Menu']['id'];
          unset($menulists[$key]);
        }
      }
      $submenu_ids = array();
      foreach ($menulists as $key => $value) {
        if(in_array($value['Menu']['menu_id'] , $menu_ids) )
        {
          array_push($submenu_ids, $value['Menu']['id']);

          if($value['Menu']['action']!='#')
          {
            if(empty($defAction[$value['Module']['id']]))
            {

              $defAction[$value['Module']['id']] = $value['Menu']['action'];
            }
          }
          $ullist[$value['Module']['id']]['main_ul']['main_li'][$value['Menu']['menu_id']]['sub_li'][$value['Menu']['id']]['name']=$value['Menu']['name'];
          $ullist[$value['Module']['id']]['main_ul']['main_li'][$value['Menu']['menu_id']]['sub_li'][$value['Menu']['id']]['href']=$value['Menu']['action'];
          $ullist[$value['Module']['id']]['main_ul']['main_li'][$value['Menu']['menu_id']]['sub_li'][$value['Menu']['id']]['action_id']=$value['Menu']['id'];
          unset($menulists[$key]);
        }
      }
      foreach ($menulists as $key => $value) {
        if(in_array($value['Menu']['menu_id'] , $submenu_ids) ) {
          if($value['Menu']['action']!='#') {
            if(empty($defAction[$value['Module']['id']])) {
              $defAction[$value['Module']['id']] = $value['Menu']['action'];
            }
          }
          $keys = array_keys($ullist[$value['Module']['id']]['main_ul']['main_li']);
          foreach ($keys as $key => $k) {
            $subkeys = array_keys($ullist[$value['Module']['id']]['main_ul']['main_li'][$k]['sub_li']);
            foreach ($subkeys as $key => $sub) {
              $subkey[$sub] = $k;
            }
          }
        }
      }
      $thirdids = array();
      foreach ($menulists as $key => $value) {
        if(in_array($value['Menu']['menu_id'] , $submenu_ids) ) {
          if($value['Menu']['action']!='#') {
            if(empty($defAction[$value['Module']['id']])) {
              $defAction[$value['Module']['id']] = $value['Menu']['action'];
            }
          }
          $thirdids[]=$value['Menu']['id'];
          $pkey = $subkey[$value['Menu']['menu_id']];
          $ullist[$value['Module']['id']]['main_ul']['main_li'][$pkey]['sub_li'][$value['Menu']['menu_id']]['third_sub_li'][$value['Menu']['id']]['name']=$value['Menu']['name'];
          $ullist[$value['Module']['id']]['main_ul']['main_li'][$pkey]['sub_li'][$value['Menu']['menu_id']]['third_sub_li'][$value['Menu']['id']]['href']=$value['Menu']['action'];
          $ullist[$value['Module']['id']]['main_ul']['main_li'][$pkey]['sub_li'][$value['Menu']['menu_id']]['third_sub_li'][$value['Menu']['id']]['action_id']=$value['Menu']['id'];
        }
      }
      $this->set('MenuList',$ullist);
      $user_role = $this->UserRole->findById($id);
      $this->set('user_role',$user_role);
    }
    else
    {
      $datasource_Role = $this->UserRole->getDataSource();
      try {
        $datasource_Role->begin();

        $data=$this->request->data['UpdateRole'];
        $show_cost = 0;
        if(isset($data['show_cost'])){
          $show_cost = $data['show_cost'];
        }
        $this->UserRole->id = $data['id'];
        $Role_data=[
          'role_name'=>$data['role_name'],
          'menus'=>$data['menus'],
          'show_cost'=>$show_cost,

        ];
// $this->UserRole->create();
        $this->UserRole->id;
        if(!$this->UserRole->save($Role_data))
        {
          $errors = $this->UserRole->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0], 1);
          }
        }
        $datasource_Role->commit();
        $return['result']='Updated Roles';
        $this->Session->setFlash(__($return['result']));
        return $this->redirect(array('controller' => 'User', 'action' => 'RoleList'));
      }
      catch (Exception $e)
      {
        $datasource_Role->rollback();

        $return['result']=$e->getMessage();
        $this->redirect( Router::url( $this->referer(), true ) );
      }
    }
  }
  public function only_full_group_by()
  {
    $this->User->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");
    return $this->redirect(array('controller'=>'Stock','action' => 'index'));
  }
  public function Settings()
  {
    $State_list=$this->State->find('list');
    $this->set(compact('State_list'));
    $options=array('Yes'=>'Yes &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;','No'=>'No',);
    $Country_list=$this->Country->find('list');
    $this->set(compact('Country_list'));
    $this->set(compact('options'));
    $Tax_type_list=[
      'Inclusive'=>'Inclusive &nbsp;&nbsp;&nbsp;&nbsp;',
      'Exclusive'=>'Exclusive',
    ];
    $this->set(compact('Tax_type_list'));
    $Timezone_cities_list=[
      'Kolkata'=>'Kolkata',
      'Riyadh'=>'Riyadh',
      'Qatar'=>'Qatar',
      'Muscat'=>'Muscat',
      'Dubai'=>'Dubai',
      'Kuwait'=>'Kuwait',
      'Singapore'=>'Singapore',
    ];
    $this->set(compact('Timezone_cities_list'));
    $Profile=$this->Global_Var_Profile;

    if(!$this->request->data){
      $this->request->data['settings']=$Profile['Profile'];
    }
    else
    {
      try{
        $data=$this->request->data['settings'];
        $this->Profile->id=$Profile['Profile']['id'];
        if(!$this->Profile->save($data))
        {
          $errors = $this->Profile->validationErrors;
          foreach ($errors as $key => $value) {
            throw new Exception($value[0]);
          }
        } 
      }catch (Exception $e) {
        $return['result']=$e->getMessage();
      }
    }
  }
   public function User_edit()
  {
    //$user_id=1;
    $datasource_User = $this->User->getDataSource();
    try {
      $datasource_User->begin();
      $User_data=$this->request->data['EditUser'];
      $name=$User_data['edit_name'];
      $edit_password=$User_data['edit_password'];
      $edit_user_role_id=$User_data['edit_user_role_id'];
      $mobile=$User_data['edit_mobile'];
      $email=$User_data['edit_email'];
     if(isset($User_data['edit_staff_id']))
     {
       $edit_staff_id=$User_data['edit_staff_id'];
       $userdata=[
           //'user_role_id'=>$edit_user_role_id,
            'email'=>$email,
            'mobile'=>$mobile,
            'password'=>$edit_password,
            'name'=>$name,
            'staff_id'=>$edit_staff_id,
          ];
     }
     else
     {
       $userdata=[
           //'user_role_id'=>$edit_user_role_id,
            'email'=>$email,
            'mobile'=>$mobile,
            'password'=>$edit_password,
            'name'=>$name,
          ];
     }
     $this->User->id=$User_data['user_edit_id'];
          if(!$this->User->save($userdata))
          {
            $errors = $this->User->validationErrors;
            foreach ($errors as $key => $value) {
              throw new Exception($value[0], 1);
            }
          }
        $datasource_User->commit();
        $return['result']='Success';
        $this->Session->setFlash(__($return['result']));
      } catch (Exception $e) {
        $datasource_User->rollback();
        $return['result']='Error';
        $return['message']=$e->getMessage();
        //$this->Session->setFlash(__($return['message']));
      }

      echo json_encode($return);
    exit;
    }
    public function staff_ajax($staff_id)
    {
    $return['result']='Success';
    $staff=$this->Staff->find('first',array('conditions'=>array('Staff.id'=>$staff_id),'fields'=>array('Staff.id','Staff.name','Staff.contact_no','Staff.code')));
    $return['code']=$staff['Staff']['code'];
    $return['contact_no']=$staff['Staff']['contact_no'];
    echo json_encode($return);
    exit;

    }
    public function User_get_ajax($id)
  {
    $return['result']='Error';
    $User=$this->User->find('first',array(
      'conditions'=>array('User.id'=>$id),
      'fields'=>array(
        'User.*',
        'UserRole.role_name',       
      )
    ));
    if(!empty($User))
    {
      $return['data']=$User;
      $return['result']='Success';  
    }
    else
    {
      $return['message']='Empty';
    }
    echo json_encode($return);
    exit;
  }
} 