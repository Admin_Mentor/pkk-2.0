<?php
App::uses('AppController', 'Controller');
/**
* Executive Controller
*/
class VehicleController extends AppController {
public $uses=[
'Sale',
'VehicleNo',
'Staff',
'DayRegister'
];
public $components = array('RequestHandler');

public function AddVehicle()
{

 $VehicleAll=$this->VehicleNo->find('all');
    $this->set(compact('VehicleAll'));
if($this->request->data)
{
$data=$this->request->data['Vehicle'];
$datasource_Vehicle = $this->VehicleNo->getDataSource();

try {
$number=$data['registration_number'];
$number=$this->VehicleNo->find('first',array(
'conditions'=>array('VehicleNo.number'=>$number)
));
$vehicle_data=[
'number'=>$data['registration_number'],
'make'=>$data['make'],
'model'=>$data['model'],

];
$datasource_Vehicle->begin();
$this->VehicleNo->create();
if(!$this->VehicleNo->save($vehicle_data))
{
$errors = $this->VehicleNo->validationErrors;
foreach ($errors as $key => $value) 
{
throw new Exception($value[0], 1);
}
}
else
{}
$datasource_Vehicle->commit();
$return['result']='success';
$this->Session->setFlash(__($return['result']));

} 
catch (Exception $e) {
$return['result']=$e->getMessage();
$this->Session->setFlash(__($return['result']));
$datasource_Vehicle->rollback();
}
$this->redirect( Router::url( $this->referer(), true ) );
}	
}
public function get_vehiclel_details_ajax($id)
	{
		$return=[
			'status'=>'Empty',
			'VehicleNo'=>[],
		];
		if(isset($id))
		{
			$VehicleNo=$this->VehicleNo->findById($id);
			$return['VehicleNo']=$VehicleNo['VehicleNo'];
			$return['status']="Success";
		
	   }
		echo json_encode($return);
		exit;
	}
	public function EditVehicle()
{
	if($this->request->data)
	{
		$data=$this->request->data['Vehicle'];
		$datasource_Vehicle = $this->VehicleNo->getDataSource();
		try {
			 $vehicle_data=[
			'number'=>$data['registration_number'],
			'make'=>$data['make'],
			'model'=>$data['model'],

			];
          $datasource_Vehicle->begin();
 			$this->VehicleNo->id=$data['id'];
			if(!$this->VehicleNo->save($vehicle_data))
			{
				$errors = $this->VehicleNo->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
		    
			$datasource_Vehicle->commit();
			$return['result']='success';
		$this->Session->setFlash(__($return['result']));
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
			$this->Session->setFlash(__($return['result']));

			$datasource_Vehicle->rollback();
		}

		$this->redirect( Router::url( $this->referer(), true ) );
	}
}
public function DeleteVehicleNo($id)
	{
		try {
			//$Sale=$this->Sale->findByVehicleNoId($id);
			$DayRegister=$this->DayRegister->findByVehicleId($id);
			// if($Sale)
			// 	throw new Exception("Used In Sale", 1);
			if($DayRegister)
				throw new Exception("Used In Day Register", 1);
			if(!$this->VehicleNo->delete($id))
				throw new Exception("Cant Delete This Vehicle", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);exit;
	}
	public function VehicleLog()
	{

	}
	public function get_vehicle_ajax()
	{

		$user_role_id=$this->Session->read('UserRole.id');
		$menu_id = $this->Menu->field('Menu.id',array('action ' => 'Vehicle/AddVehicle'));
		$UserMenuPermission=$this->UserMenuPermission->findByUserRoleIdAndMenuId($user_role_id,$menu_id);
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='VehicleNo.id';
		$columns[]='VehicleNo.number';
		$columns[]='VehicleNo.make';
		$columns[]='VehicleNo.model';
		$columns[]='VehicleNo.id';
		//$columns[]='VehicleNo.id';

		$conditions=[];
		$totalData=$this->VehicleNo->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'VehicleNo.number LIKE' =>'%'. $q . '%',
				'VehicleNo.make LIKE' =>'%'. $q . '%',
				'VehicleNo.model LIKE' =>'%'. $q . '%',
			);
			$totalFiltered=$this->VehicleNo->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$Data=$this->VehicleNo->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'VehicleNo.*',
			)
		));
		$start=$requestData['start'];
		$sl_no=$start+1;
		foreach ($Data as $key => $value) {
                $Data[$key]['VehicleNo']['id']=$sl_no++;
				$Data[$key]['VehicleNo']['action']='<span><i data-id="'.$value['VehicleNo']['id'].'" class="fa fa-2x fa-edit edit_VehicleNo" style="color: #3c8dbc;"></i></span>&nbsp;&nbsp;';
				$Data[$key]['VehicleNo']['action'].='<span><i data-id="'.$value['VehicleNo']['id'].'" class="fa fa-2x fa-trash blue-col delete_vehicle_no"></i>&nbsp;&nbsp;</span>';
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data);
		exit;
	
	}
	public function get_vehiclelog_ajax()
	{}
}