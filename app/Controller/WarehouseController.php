<?php
App::uses('AppController', 'Controller');
class WarehouseController extends AppController {
	public $helpers= array('Html','Form');
	public $uses=array('Product','Warehouse');
	public function warehouse_add_ajax()
	{
		$data=array(
			'name'=>strtoupper($this->request->data['modal_warehouse_name']),
			'description'=>$this->request->data['modal_warehouse_desc'],
			'warehouse_id'=>$this->request->data['damage_warehouse_id']
			);
		$this->Warehouse->create();
		if($this->Warehouse->save($data))
		{
			$this->Session->setFlash('data is saved');
			$last_iserted_pt=$this->Warehouse->findById($this->Warehouse->getLastInsertId());
			echo "<option value='".$last_iserted_pt['Warehouse']['id']."'>".$last_iserted_pt['Warehouse']['name']." </option>";
		$this->Warehouse->id=$this->request->data['damage_warehouse_id'];
		if(!$this->Warehouse->saveField('is_damage',1 ))
		throw new Exception("Error Processing is_damge Updation", 1);
		}
		exit;
	}
	public function new_warehouse_add_ajax(){
        $data=$this->request->data;
        $exist=$this->Warehouse->findByName($data['modal_warehouse_name']);
        if(!$exist){
            $save_data=array(
			'name'=>strtoupper($this->request->data['modal_warehouse_name']),
			'description'=>$this->request->data['modal_warehouse_desc'],
			'warehouse_id'=>$this->request->data['damage_warehouse_id']
			);
            $this->Warehouse->create();
            if(!$this->Warehouse->save($save_data))
                throw new Exception("Error Processing Request", 1);
			if($this->request->data['damage_warehouse_id'])
			{
			$this->Warehouse->id=$this->request->data['damage_warehouse_id'];
			if(!$this->Warehouse->saveField('is_damage',1 ))
			throw new Exception("Error Processing is_damge Updation", 1);
			}
            $return['result'] = 'Success';
            $last_iserted_id = $this->Warehouse->findById($this->Warehouse->getLastInsertId());
            $return['key'] = $last_iserted_id['Warehouse']['id'];
            $return['value'] = $last_iserted_id['Warehouse']['name'];
        }else{
            $return['result'] = 'Already exist';
        }
        echo json_encode($return);
        exit;

    }
	public function edit_ajax(){
		try {
			$prdct_id=$this->request->data['prdct_id'];
			if(!$prdct_id)
				throw new Exception("Empty ProductType Id", 1);
			$prdct_typ=strtoupper(trim($this->request->data['prdct_typ']));
			$this->ProductType->id=$prdct_id;
			if(!$this->ProductType->saveField('name',$prdct_typ ))
				throw new Exception("Error Processing ProductType Name Updation", 1);
			if(!$this->ProductType->saveField('updated_at',date('Y-m-d H:i:s')))
				throw new Exception("Error Processing ProductType updated_at Updation", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function get_warehouse_ajax()
	{
		$return['result']='Error';
		$return['option']='<option value="">select</option>';
		$Warehouse = $this->Warehouse->find('list',array(
		'fields'=>array('id','name'),
		'order'=>array('name ASC'),
		'conditions'=>array('Warehouse.is_damage'=>"",'Warehouse.warehouse_id'=>"")
		));
		foreach ($Warehouse as $key => $value) {			
			$return['result']='Success';
			$return['option']=$return['option'].'<option value='.$key.'>'.$value.'</option>';
	    }
		echo json_encode($return);
		exit;
	}
	public function warehouse_type_delete_ajax($id)
	{
		try {
			$Product=$this->Product->findByProductTypeId($id);
			if($Product)
				throw new Exception("This is Used In ".$Product['Product']['name'], 1);
			if(!$this->ProductType->delete($id))
				throw new Exception("Error Processing Request While delete", 1);
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function warehouse_get_ajax($id)
	{
		try {
			$Warehouse=$this->Warehouse->findById($id);
			if(!$Warehouse)
				throw new Exception("Empty Warehouse", 1);
			$return['data']=$Warehouse['Warehouse'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function warehouse_edit_ajax()
	{
		try {
			$data=$this->request->data['WarehouseEdit'];
			$Table_data=array(
				'name'=>trim(strtoupper($data['name'])),
				'description'=>$data['edit_description'],
				'updated_at'=>trim(date('Y-m-d h:i:s')),
				// 'code'=>trim(strtoupper($data['code'])),
				);
			$this->Warehouse->id=$data['id'];
			if(!$this->Warehouse->save($Table_data))
			{
				$errors = $this->Warehouse->validationErrors;
				foreach ($errors as $key => $value) {
					throw new Exception($value[0], 1);
				}
			}
			$Warehouse=$this->Warehouse->read();
			$return['key']=$Warehouse['Warehouse']['id'];
			$return['value']=$Warehouse['Warehouse']['name'];
			$return['result']='Success';
		} catch (Exception $e) {
			$return['result']=$e->getMessage();
		}
		echo json_encode($return);
		exit;
	}
	public function delete()
	{
		$this->before_load();
		$id=$this->request->params['pass'][0];
		$Product=$this->Product->findByWarehouseIdFk($id);
		if(!empty($Product))
		{
			$this->Flash->set('product Type Is Used.');
			$this->redirect(array('controller'=>'Stock','action'=>'index'));
		}
		else
		{
			if($this->Warehouse->delete($id))
			{
				$this->Flash->set(__('product type deleted.',h($id)));
				$this->redirect(array('controller'=>'Stock','action'=>'index'));
			}
			else
			{
				$this->Flash->set(__('Error Occured'));
			}
		}
	}
	public function check_producttype_ajax(){
		$product_type=$this->request->data['prdct_type'];
		$result=$this->ProductType->find('all',array('conditions'=>array('ProductType.name'=>$product_type)));
		if(empty($result)){
			echo "No";
		}
		else{
			echo "Yes";
		}
		exit;
	}
	public function delete_ProductType_ajax($id)
	{
		$userid = $this->Session->read('User.id');
		if($this->ProductType->delete($id))
		{
			$return['result']="Success";
		}
		else
		{
			$return['result']="Error";
		}
		echo json_encode($return);
		exit;
	}
	public function get_damage_warehouse($id)
	{
		$return=[
			'status'=>'Empty',
			'Warehouse'=>[],
		];
		if(isset($id))
		{
			$Warehouse=$this->Warehouse->findByWarehouseId($id);
			$return['Warehouse']=$Warehouse['Warehouse']['id'];
			$return['status']="Success";
		
	   }
		echo json_encode($return);
		exit;
	}
	public function index()
	{
		$this->Warehouse->unbindModel(
			array('hasMany' => array('StockLog',
				'Stock',
				'Executive',
				))
			);
		$this->set('Warehouse_all',$this->Warehouse->find('list',array(
		'fields'=>array('id','name'),
		'order'=>array('name ASC'),
		'conditions'=>array('Warehouse.is_damage'=>"",'Warehouse.warehouse_id'=>"")
		)));

	}
	public function Warehouse_Table_ajax()
	{
		$requestData=$this->request->data;
		$columns = [];
		$columns[]='id';
		$columns[]='name';
		$columns[]='id';
		$columns[]='id';
		$columns[]='id';
		$columns[]='id';

		$conditions=[];
		$this->Warehouse->unbindModel(
			array('hasMany' => array('StockLog',
				'Stock',
				'Executive',
				))
			);
		$totalData=$this->Warehouse->find('count',['conditions'=>$conditions]);
		$totalFiltered=$totalData;
		if( !empty($requestData['search']['value']) ) { 
			$q=$requestData['search']['value'];
			$conditions['OR']=array(
				'Warehouse.name LIKE' =>'%'. $q . '%',
			);
			$this->Warehouse->unbindModel(
			array('hasMany' => array('StockLog',
				'Stock',
				'Executive',
				))
			);
			$totalFiltered=$this->Warehouse->find('count',[
				'conditions'=>$conditions,
			]);
		}
		$this->Warehouse->unbindModel(
			array('hasMany' => array('StockLog',
				'Stock',
				'Executive',
				))
			);
		$Data=$this->Warehouse->find('all',array(
			'conditions'=>$conditions,
			'offset'=>$requestData['start'],
			'limit'=>$requestData['length'],
			'order'=> $columns[$requestData['order'][0]['column']]." ".$requestData['order'][0]['dir'],
			'fields'=>array(
				'Warehouse.*',
			)
		));
		$start=$requestData['start'];
		$sl_no=$start+1;
		// $sl_no=1;
		foreach ($Data as $key => $value) {
			if(!empty($value['Warehouse']['warehouse_id'])){
		        $this->Warehouse->unbindModel(array('hasMany' => array('StockLog','Stock','Executive',)));
				$damage_warehouse_ids=$this->Warehouse->findById($value['Warehouse']['warehouse_id']);
				$damage_warehouse_id=$damage_warehouse_ids['Warehouse']['name'];
				

			}
			else{
				$damage_warehouse_id=' ';
			}
			if($value['Warehouse']['is_damage']==1){
				//$damage_warehouse_ids=$this->Warehouse->findById($value['Warehouse']['warehouse_id']);
				$is_damage='Yes';
				

			}
			else{
				$is_damage='No';
			}
			$Data[$key]['Warehouse']['warehouse_id']=$damage_warehouse_id;
			$Data[$key]['Warehouse']['is_damage']=$is_damage;
			$Data[$key]['Warehouse']['id']=$sl_no++;
			$Data[$key]['Warehouse']['action'] ='<span><i warehouse_name="'.$value['Warehouse']['name'].'" table_id="'.$value['Warehouse']['id'].'" class="fa fa-2x fa-edit   edit_Warehouses  blue-col"></i></span>&nbsp;&nbsp;';
			// $Data[$key]['ProductType']['action'] .="<span><i table_id='".$value['ProductType']['id']."' class='fa fa-2x fa-trash delete_ProductType blue-col'></i></span>";
		}
		$json_data = array(
			"draw"           =>intval($requestData['draw']),
			"recordsTotal"   =>intval($totalData),
			"recordsFiltered"=>intval($totalFiltered),
			"records"        =>$Data
		);
		echo json_encode($json_data); exit;
	}
	public function warehouse_search()
	{
		$warehouse_name=$this->request->data['warehouse_name'];
		$warehouse_name=trim($warehouse_name);
		$Warehouse=$this->Warehouse->find('first',array('conditions'=>array('Warehouse.name'=>$warehouse_name)));
		if(!empty($Warehouse)){
			echo "Yes";
		}
		else{
			echo "No";
		}
		exit;
	}
}