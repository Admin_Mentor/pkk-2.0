<?php
App::uses('AppModel', 'Model');
class Attendance extends AppModel {
public $validate = array(
	'staff_id' => array(
		'notEmpty' => array(
			'rule' => array('notEmpty'),
			'message' => 'Name is Required',
			),
		),
	'month' => array(
		'date' => array(
			'rule' => array('date'),
				'message' => 'month is Required',
			),
		),
	);
public $belongsTo = array(
	'Staff' => array(
		'className' => 'Staff',
		'foreignKey' => 'staff_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		)
	);
}
