<?php
App::uses('AppModel', 'Model');
class AttendanceFreeze extends AppModel {
	public $validate = array(
		'date' => array(
			'date' => array(
				'rule' => array('date'),
			),
		),
		'freeze' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
	);

}
