<?php
App::uses('AppModel', 'Model');
/**
 * BomItem Model
 *
 * @property Product $Product
 * @property Bom $Bom
 */
class BomItem extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'product_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Product Is required',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'bom_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Bom is required',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'quantity' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'quantity is required and numeric',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Bom' => array(
			'className' => 'Bom',
			'foreignKey' => 'bom_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Unit' => array(
		'className' => 'Unit',
		'foreignKey' => 'unit_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		)
	);
}
