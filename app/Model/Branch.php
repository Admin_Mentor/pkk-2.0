<?php
App::uses('AppModel', 'Model');
/**
 * Executive Model
 *
 * @property Sale $Sale
 */
class Branch extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'isUnique' => array(
			'rule' => array('isUnique'),
			'message' => 'Name Already Registered'
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
 * belongsTo associations
 *
 * @var array
 */
public $belongsTo = array(
	// 'Location' => array(
	// 	'className' => 'Location',
	// 	'foreignKey' => 'location_id',
	// 	'conditions' => '',
	// 	'fields' => '',
	// 	'order' => ''
	// 	),
	'Warehouse' => array(
		'className' => 'Warehouse',
		'foreignKey' => 'warehouse_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		),
	
	);

/**
 * hasMany associations
 *
 * @var array
 */
	// public $hasMany = array(
	// 	'Sale' => array(
	// 		'className' => 'Sale',
	// 		'foreignKey' => 'executive_id',
	// 		'dependent' => false,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'exclusive' => '',
	// 		'finderQuery' => '',
	// 		'counterQuery' => ''
	// 	)
	// );

}
