<?php
App::uses('AppModel', 'Model');
/**
 * SaleItem Model
 *
 * @property Product $Product
 * @property Sale $Sale
 */
class CreditDetail extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'credit_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Credit is required',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'amount' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'amount is required and numeric',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CreditNote' => array(
		'className' => 'CreditNote',
		'foreignKey' => 'credit_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		)
	);
}
