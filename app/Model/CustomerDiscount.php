<?php
App::uses('AppModel', 'Model');
/**
 * Customer Model
 *
 * @property AccountHead $AccountHead
 * @property CustomerType $CustomerType
 * @property State $State
 */
class CustomerDiscount extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'account_head_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'product_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'AccountHead' => array(
			'className' => 'AccountHead',
			'type' => 'INNER',
			'foreignKey' => 'account_head_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),	
	);
// 	public $virtualFields = array(
//     'full_name' => 'CONCAT(Customer.name, " ", Customer.code)'
// );
}
