<?php
App::uses('AppModel', 'Model');

class DamageRecycleItem extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */

	public $validate = array(
		'product_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
		),
		),
		
    );
    
	// The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),		
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'recycled_as',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),		
	);
}
