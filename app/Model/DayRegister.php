<?php
App::uses('AppModel', 'Model');
class DayRegister extends AppModel {
	public $belongsTo = array(
		'Executive' => array(
			'className' => 'Executive',
			'foreignKey' => 'executive_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Route' => array(
			'className' => 'Route',
			'foreignKey' => 'route_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CustomerGroup' => array(
			'className' => 'CustomerGroup',
			'foreignKey' => 'customer_group_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public $hasMany = array(
		'ClosingDay' => array(
			'className' => 'ClosingDay',
			'foreignKey' => 'day_register_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
