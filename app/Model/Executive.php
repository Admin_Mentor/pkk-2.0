<?php
App::uses('AppModel', 'Model');
/**
 * Executive Model
 *
 * @property Sale $Sale
 */
class Executive extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
 * belongsTo associations
 *
 * @var array
 */
public $belongsTo = array(
	'Warehouse' => array(
		'className' => 'Warehouse',
		'foreignKey' => 'warehouse_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		),
	'Staff' => array(
		'className' => 'Staff',
		'foreignKey' => 'staff_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Sale' => array(
			'className' => 'Sale',
			'foreignKey' => 'executive_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		// 'ExecutiveRouteMapping' => array(
		// 	'className' => 'ExecutiveRouteMapping',
		// 	'foreignKey' => 'executive_id',
		// 	'dependent' => false,
		// 	'conditions' => '',
		// 	'fields' => '',
		// 	'order' => '',
		// 	'limit' => '',
		// 	'offset' => '',
		// 	'exclusive' => '',
		// 	'finderQuery' => '',
		// 	'counterQuery' => ''
		// )
	);

}
