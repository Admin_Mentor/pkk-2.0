<?php
App::uses('AppModel', 'Model');
class ExecutiveBonusDetails extends AppModel {
    public $validate = array(
        'executive_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
    public $belongsTo = array(
        'Executive' => array(
            'className' => 'Executive',
            'foreignKey' => 'executive_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
}