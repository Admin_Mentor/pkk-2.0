<?php
App::uses('AppModel', 'Model');
class ExecutiveExpenseDetail extends AppModel {
    public $validate = array(
    );
public $belongsTo = array(
    // 'ExecutiveExpense' => array(
    //         'className' => 'ExecutiveExpense',
    //         'foreignKey' => 'executive_expense_id',
    //         'conditions' => '',
    //         'fields' => '',
    //         'order' => ''
    //     ),
    'AccountHead' => array(
            'className' => 'AccountHead',
            'foreignKey' => 'expense_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    'Executive' => array(
            'className' => 'Executive',
            'foreignKey' => 'executive_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
}
