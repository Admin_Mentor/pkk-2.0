<?php
App::uses('AppModel', 'Model');
/**
 * PurchasedItem Model
 *
 * @property Product $Product
 * @property Purchase $Purchase
 */
class MaterialRequestItem extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array();

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		
		'MaterialRequest' => array(
			'className' => 'MaterialRequest',
			'foreignKey' => 'm_r_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Unit' => array(
			'className' => 'Unit',
			'foreignKey' => 'unit_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
