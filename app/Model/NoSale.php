<?php
App::uses('AppModel', 'Model');
/**
 * Sale Model
 *
 * @property AccountHead $AccountHead
 * @property Executive $Executive
 * @property SaleItem $SaleItem
 */
class NoSale extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'customer_account_head_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Accountig Head is required',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'executive_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Executive is required',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		
		
		
	);

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'AccountHead' => array(
			'className' => 'AccountHead',
			'foreignKey' => 'customer_account_head_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Executive' => array(
			'className' => 'Executive',
			'foreignKey' => 'executive_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Customer' => array(
			'className' => 'Customer',
			'foreignKey' => 'customer_account_head_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		
	);

}
