<?php
App::uses('AppModel', 'Model');
class OverTime extends AppModel {
	public $validate = array(
		'staff_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Staff is Required',
			),
		),
		'amount' => array(
			'numeric' => array(
				'rule' => array('numeric'),
			),
		),
	);
	public $belongsTo = array(
		'Staff' => array(
			'className' => 'Staff',
			'foreignKey' => 'staff_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);

}
