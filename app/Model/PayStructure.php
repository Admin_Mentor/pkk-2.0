<?php
App::uses('AppModel', 'Model');
class  PayStructure extends AppModel {

	public $validate = array(
		'staff_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'staff_id required',
			),
		),
		'month_year' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'month_year required',
			),
		),
		'basic_salary' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'basic_salary required',
			),
		),
		'bonus' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'bonus required',
			),
		),
		'allowance' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'allowance required',
			),
		),
		'deduction' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'deduction required',
			),
		),
		'net_salary' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'net_salary required',
			),
		),
	);
	public $belongsTo = array(
		'Staff' => array(
			'className' => 'Staff',
			'foreignKey' => 'staff_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}
