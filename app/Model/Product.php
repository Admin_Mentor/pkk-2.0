<?php
App::uses('AppModel', 'Model');
/**
 * Product Model
 *
 * @property ProductType $ProductType
 * @property Brand $Brand
 * @property Unit $Unit
 * @property PurchaseReturnItem $PurchaseReturnItem
 * @property PurchasedItem $PurchasedItem
 * @property SaleItem $SaleItem
 * @property SalesReturnItem $SalesReturnItem
 * @property StockLog $StockLog
 * @property Stock $Stock
 * @property UnwantedList $UnwantedList
 */
class Product extends AppModel {
/**
 * Validation rules
 *
 * @var array
 */
public $validate = array(
	'name' => array(
		'notBlank' => array(
			'rule' => array('notBlank'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		'isUnique' => array(
			'rule' => array('isUnique'),
			'message' => 'Name Already Registered'
			),
		),
	// 'code' => array(
	// 	'isUnique' => array(
	// 		'rule' => array('isUnique'),
	// 		'message' => 'Code Already Registered'
	// 		),
	// 	),
	'product_type_id' => array(
		'numeric' => array(
			'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	'brand_id' => array(
		'numeric' => array(
			'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	'unit_id' => array(
		'numeric' => array(
			'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	'cost' => array(
		'numeric' => array(
			'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	'mrp' => array(
		'numeric' => array(
			'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
	//The Associations below have been created with all possible keys, those that are not needed can be removed
/**
 * belongsTo associations
 *
 * @var array
 */
public $belongsTo = array(
	'ProductType' => array(
		'className' => 'ProductType',
		'foreignKey' => 'product_type_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		),
	'Brand' => array(
		'className' => 'Brand',
		'foreignKey' => 'brand_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		),
	'TypeOfProduct' => array(
		'className' => 'TypeOfProduct',
		'foreignKey' => 'type',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		),
	'Unit' => array(
		'className' => 'Unit',
		'foreignKey' => 'unit_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		)
	);
/**
 * hasMany associations
 *
 * @var array
 */
public $hasMany = array(
	'PurchaseReturnItem' => array(
		'className' => 'PurchaseReturnItem',
		'foreignKey' => 'product_id',
		'dependent' => false,
		'conditions' => '',
		'fields' => '',
		'order' => '',
		'limit' => '',
		'offset' => '',
		'exclusive' => '',
		'finderQuery' => '',
		'counterQuery' => ''
		),
	'PurchasedItem' => array(
		'className' => 'PurchasedItem',
		'foreignKey' => 'product_id',
		'dependent' => false,
		'conditions' => '',
		'fields' => '',
		'order' => '',
		'limit' => '',
		'offset' => '',
		'exclusive' => '',
		'finderQuery' => '',
		'counterQuery' => ''
		),
	'SaleItem' => array(
		'className' => 'SaleItem',
		'foreignKey' => 'product_id',
		'dependent' => false,
		'conditions' => '',
		'fields' => '',
		'order' => '',
		'limit' => '',
		'offset' => '',
		'exclusive' => '',
		'finderQuery' => '',
		'counterQuery' => ''
		),
	'SalesReturnItem' => array(
		'className' => 'SalesReturnItem',
		'foreignKey' => 'product_id',
		'dependent' => false,
		'conditions' => '',
		'fields' => '',
		'order' => '',
		'limit' => '',
		'offset' => '',
		'exclusive' => '',
		'finderQuery' => '',
		'counterQuery' => ''
		),
	'StockLog' => array(
		'className' => 'StockLog',
		'foreignKey' => 'product_id',
		'dependent' => false,
		'conditions' => '',
		'fields' => '',
		'order' => '',
		'limit' => '',
		'offset' => '',
		'exclusive' => '',
		'finderQuery' => '',
		'counterQuery' => ''
		),
	'Stock' => array(
		'className' => 'Stock',
		'foreignKey' => 'product_id',
		'dependent' => false,
		'conditions' => '',
		'fields' => '',
		'order' => '',
		'limit' => '',
		'offset' => '',
		'exclusive' => '',
		'finderQuery' => '',
		'counterQuery' => ''
		),
	'UnwantedList' => array(
		'className' => 'UnwantedList',
		'foreignKey' => 'product_id',
		'dependent' => false,
		'conditions' => '',
		'fields' => '',
		'order' => '',
		'limit' => '',
		'offset' => '',
		'exclusive' => '',
		'finderQuery' => '',
		'counterQuery' => ''
		)
	);
}