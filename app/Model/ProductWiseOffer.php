<?php
App::uses('AppModel', 'Model');

class ProductWiseOffer extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */

	public $validate = array(
		'executive_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
		),
		),
		
    );
    
	// The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Executive' => array(
			'className' => 'Executive',
			'foreignKey' => 'executive_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),		
	);
}
