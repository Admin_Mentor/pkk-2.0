<?php
App::uses('AppModel', 'Model');
/**
 * Production Model
 *
 * @property Product $Product
 * @property Production $Production
 */
class ProductionPlan extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'ProductionPlanItem' => array(
			'className' => 'ProductionPlanItem',
			'foreignKey' => 'production_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}