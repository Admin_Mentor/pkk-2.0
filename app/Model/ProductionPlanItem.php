<?php
App::uses('AppModel', 'Model');
/**
 * ProductionItem Model
 *
 * @property Product $Product
 * @property Production $Production
 */
class ProductionPlanItem extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'product_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Product Is required',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'production_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Production is required',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'total_quantity' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Total quantity is required and numeric',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Product' => array(
			'className' => 'Product',
			'foreignKey' => 'product_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ProductionPlan' => array(
			'className' => 'ProductionPlan',
			'foreignKey' => 'production_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		
	);
}
