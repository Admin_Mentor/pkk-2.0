<?php
App::uses('AppModel', 'Model');
/**
 * Profile Model
 *
 * @property State $State
 */
class SaleTarget extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Executive' => array(
			'className' => 'Executive',
			'foreignKey' => 'executive_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
