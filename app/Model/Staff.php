<?php
App::uses('AppModel', 'Model');
class Staff extends AppModel {

	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'name required',
			),
		),
		'code' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'code required',
			),
		),
		'contact_no' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'contact_no required',
			),
		),
		'is_executive' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'is_executive required',
			),
		),
	);
	public $belongsTo = array(
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Paid' => array(
			'className' => 'AccountHead',
			'foreignKey' => 'paid_account_head_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Outstanding' => array(
			'className' => 'AccountHead',
			'foreignKey' => 'outstanding_account_head_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PrePaid' => array(
			'className' => 'AccountHead',
			'foreignKey' => 'prepaid_account_head_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Bonus' => array(
			'className' => 'AccountHead',
			'foreignKey' => 'bonus_head_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
}

