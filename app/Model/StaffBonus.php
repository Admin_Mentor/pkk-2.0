<?php
App::uses('AppModel', 'Model');
class StaffBonus extends AppModel {
	public $belongsTo = array(
		'Staff' => array(
			'className' => 'Staff',
			'foreignKey' => 'staff_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
