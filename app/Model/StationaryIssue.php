<?php
App::uses('AppModel', 'Model');

class StationaryIssue extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */

	public $validate = array(
		'warehouse_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
		),
		),
		
    );
    
	// The Associations below have been created with all possible keys, those that are not needed can be removed
	public $belongsTo = array(
		'Warehouse' => array(
			'className' => 'Warehouse',
			'foreignKey' => 'warehouse_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),		
	);
}
