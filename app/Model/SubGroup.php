<?php
App::uses('AppModel', 'Model');
/**
 * SubGroup Model
 *
 * @property Group $Group
 * @property AccountHead $AccountHead
 */
class SubGroup extends AppModel {
/**
 * Validation rules
 *
 * @var array
 */
public $validate = array(
	'name' => array(
		'notBlank' => array(
			'rule' => array('notBlank'),
			'message' => 'Name is Required',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		'isUnique' => array(
			'rule' => array('isUnique'),
			'message' => 'Name Already Registered'
			),
		),
	'group_id' => array(
		'numeric' => array(
			'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		'notBlank' => array(
			'rule' => array('notBlank'),
			'message' => 'group_id is Required',
			)
		),
	);
	// The Associations below have been created with all possible keys, those that are not needed can be removed
/**
 * belongsTo associations
 *
 * @var array
 */
public $belongsTo = array(
	'Group' => array(
		'className' => 'Group',
		'foreignKey' => 'group_id',
		'conditions' => '',
		'fields' => '',
		'order' => ''
		)
	);
/**
 * hasMany associations
 *
 * @var array
 */
public $hasMany = array(
	'AccountHead' => array(
		'className' => 'AccountHead',
		'foreignKey' => 'sub_group_id',
		'dependent' => false,
		'conditions' => '',
		'fields' => '',
		'order' => '',
		'limit' => '',
		'offset' => '',
		'exclusive' => '',
		'finderQuery' => '',
		'counterQuery' => ''
		)
	);
}
