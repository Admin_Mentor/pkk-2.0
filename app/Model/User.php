<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {
/**
 * Validation rules
 *
 * @var array
 */
public $validate = array(
	'name' => array(
		'notBlank' => array(
			'rule' => array('notBlank'),
			               ),
		'isUnique' => array(
			'rule' => array('isUnique'),
			'message' => 'Name Already Registered'
			),
		),
	'password' => array(
		'notBlank' => array(
			'rule' => array('notBlank'),
		),
		),
	'flag' => array(
		'boolean' => array(
			'rule' => array('boolean'),
		),
		),
	);

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'UserRole' => array(
			'className' => 'UserRole',
			'foreignKey' => 'user_role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Branch' => array(
			'className' => 'Branch',
			'foreignKey' => 'branch_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Staff' => array(
			'className' => 'Staff',
			'foreignKey' => 'staff_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		
	);
}