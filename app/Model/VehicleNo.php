<?php
App::uses('AppModel', 'Model');
/**
 * Executive Model
 *
 * @property Sale $Sale
 */
class VehicleNo extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'number' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
			),
			'isUnique' => array(
			'rule' => array('isUnique'),
			'message' => 'Number Already Registered'
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
 * belongsTo associations
 *
 * @var array
 */
public $hasMany = array();
}
