<?php
App::uses('CompanyWorkingsController', 'Controller');

/**
 * CompanyWorkingsController Test Case
 */
class CompanyWorkingsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.company_working',
		'app.type',
		'app.master_group',
		'app.group',
		'app.sub_group',
		'app.account_head',
		'app.journal',
		'app.bank_detail',
		'app.profile',
		'app.state',
		'app.module',
		'app.menu',
		'app.user_role'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
