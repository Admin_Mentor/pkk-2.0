<?php
App::uses('CompanyWorking', 'Model');

/**
 * CompanyWorking Test Case
 */
class CompanyWorkingTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.company_working'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CompanyWorking = ClassRegistry::init('CompanyWorking');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CompanyWorking);

		parent::tearDown();
	}

}
