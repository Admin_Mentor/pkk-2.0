<?php
/**
 * Staff Fixture
 */
class StaffFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 196, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'date_of_joining' => array('type' => 'date', 'null' => true, 'default' => null),
		'designation' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 250, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'role' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'prepaid_account_head_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'outstanding_account_head_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'paid_account_head_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'account_head_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'code' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 196, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'contact_no' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 196, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'month_leave' => array('type' => 'float', 'null' => false, 'default' => '0.00', 'length' => '8,2', 'unsigned' => false),
		'carry_leave' => array('type' => 'float', 'null' => false, 'default' => '0.00', 'length' => '8,2', 'unsigned' => false),
		'address' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'is_executive' => array('type' => 'tinyinteger', 'null' => false, 'default' => '0', 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'date_of_joining' => '2018-10-01',
			'designation' => 'Lorem ipsum dolor sit amet',
			'role' => 1,
			'prepaid_account_head_id' => 1,
			'outstanding_account_head_id' => 1,
			'paid_account_head_id' => 1,
			'account_head_id' => 1,
			'code' => 'Lorem ipsum dolor sit amet',
			'contact_no' => 'Lorem ipsum dolor sit amet',
			'month_leave' => 1,
			'carry_leave' => 1,
			'address' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'is_executive' => 1
		),
	);

}
