<div id="adddivision" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Add Division</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Division Name</label>
            <div class="col-sm-9">
              <input class="form-control division_disable toUpperCase " placeholder="" type="text" id="modal_division_name">
              <span id="divison_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
        </div>
      </div>
      <div class="modal-footer">
        <button  type='button' class="save btn btn-success" id="add_division">Save</button>
      </div>
    </div>
  </div>
</div>