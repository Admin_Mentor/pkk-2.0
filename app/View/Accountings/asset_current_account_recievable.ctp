 <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
<section class="content-header">
    <h1> Customers</h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary box_tp_brdr">
                <div class="row-wrapper">
                    <div class="row">
                        <?php echo $this->Form->create('AccountHead', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'AccountHead_Form']); ?>   
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                     <div class="col-md-2 col-lg-2 col-sm-2">
                                        <!-- <div class="form-group"> -->
                                            <!-- <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12"> -->
                                                <?= $this->Form->input('executive_id',array('class'=>'form-control select2','type'=>'select','empty'=>'select','options'=>$Executive_list,'id'=>'executive_view_id',)); ?>
                                            <!-- </div> -->
                                        <!-- </div> -->
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2">
                                       <!--  <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12"> -->
                                                <?= $this->Form->input('route_id',array('class'=>'form-control select2 routes','type'=>'select','empty'=>'select','options'=>$Route_list,'id'=>'route_view_id',)); ?>
                                            <!-- </div>
                                        </div> -->
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2">
                                       <!--  <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12"> -->
                                                <?= $this->Form->input('group',array('class'=>'form-control select2 customergroup','type'=>'select','empty'=>'select','options'=>$CustomerGroup_list,'id'=>'group_view',)); ?>
                                            <!-- </div> -->
                                            <!-- <br> -->
                                            <!-- <div class="col-md-1 col-lg-1 col-sm-1 col-xs-6"> -->
                                                <!-- <a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#group_add_modal"></i></a> -->
                                            <!-- </div> -->

                                        <!-- </div> -->
                                    </div>

                                      <div class="col-md-1 col-lg-1 col-sm-1">
                                        <br>
                                                <a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#group_add_modal"></i></a>
                                            </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2" style="margin-bottom: 1%;margin-left: -4% ">
                                        <!-- <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12"> -->

                                                <?= $this->Form->input('customer_type',array('class'=>'form-control select2','type'=>'select','empty'=>'select','required','options'=>$CustomerType_list,'id'=>'customer_type',)); ?>
                                            <!-- </div> -->
                                            <!-- <br> -->
                                           <!--  <div class="col-md-1 col-lg-1 col-sm-1 col-xs-6"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_type_modal"></i></a></div> -->
                                        <!-- </div> -->
                                    </div>
                                     <div class="col-md-1 col-lg-1 col-sm-1 ">
                                        <br>
                                        <a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_type_modal"></i></a>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2" style="margin-left:75%;margin-top: -5.5% ">
                                        <!-- <div class="form-group"> -->
                                            <!-- <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12"> -->
                                                <?= $this->Form->input('name',array('class'=>'form-control select2 name','type'=>'select','required','empty'=>'select','id'=>'name','label'=>'Customer')); ?>
                                            <!-- </div> -->
                                            <br>
                                            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12" hidden><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_modal_add"></i></a></div>
                                        <!-- </div> -->
                                    <!-- </div> -->

                                </div>

                            </div>

                            <?= $this->Form->end(); ?>
                        </div>
                        <br>
                        <div class="row-wrapper" style="margin-left:30px;width:97%">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body table-responsive no-padding">
                                        <table class="table table-condensed table boder table-bordered" id="AccountHead_table" style="margin-left:10px;" data-order='[[ 5, "asc" ]]' >
                                            <thead>
                                                <tr class="blue-bg">
                                                    <!-- <th>Executive </th> -->
                                                    <th>Customer Order</th>
                                                    <th>Route</th>
                                                    <th>Group</th>
                                                    <th>Customer Type</th>
                                                    <th>Code</th>
                                                    <th>Customer</th>
                                                    <th>Priority</th>
                                                    <th>Credit Limit</th>
<!--                                                     <th>Map</th>
 -->                                                    <th>Action</th>
                                                    <!-- <th></th> -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                            <th class="total_amount" colspan="6" style="font-size:20px; color:red;text-align:right">Total:</th>
                                            <th class="total_amount text-right" style="font-size:20px; color:red;"></th>
                                            <th></th><th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="customer_type_modal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Customer Type</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Customer Type</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="customer_type_name" type="text">
                                        </div>
                                        <br>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id='add_customer_type'>Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <div id="Location_Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-body">
      <div class="box-body table-responsive no-padding">
    
    <div id="map"></div>
      </div>
      </div>
      
    </div>
  </div>
</div>
<?php require('customer_modal.php') ?> 
<?php require('customer_add_modal.php') ?> 
<?php require('state_modal.php') ?>
<?php require('route_modal.php') ?>
<?php require('customer_group_modal.php') ?>
<?php require('Division_Add_modal.php') ?>
</section>
<script type="text/javascript">
  $(document).ready(function () {
 $("#name").select2({
    placeholder: "Search Customer",
    width: '100%',
    ajax: {
      url: '<?= $this->webroot ?>ProductionPlan/Searchcustomer',
      dataType: 'json',
      delay: 250,
      data: function (params) {
            return {
                      q: params.term, // search term
                      page: params.page
                   };
          },
          processResults: function (data, params) {
            params.page = params.page || 2;
            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: false
        },
      minimumInputLength: 2,
      templateResult: formatRepo,
      templateSelection: formatRepoSelection
    });

  function formatRepo (repo) 
  {
    if (repo.loading) return repo.text;
    return repo.text;
  }
  function formatRepoSelection (repo)
  {
    return repo.text || repo.text;
  }
  });
</script>
<script type="text/javascript">
    <?php require('division.js') ?>
    $('.opening_balance').on('keyup change', function () {
        $('#opening_balance').val($(this).val());
        $('#opening_balance_modal').val($(this).val());
    });
    $(document).on('change', '#customer_type,#modal_customer_type', function () {
        $('#modal_customer_type').val($(this).val()).trigger('change.select2');
        $('#customer_type').val($(this).val()).trigger('change.select2');
    });
</script>
<!-- for edit  -->
<script type="text/javascript">
$('#AccountHead_table').DataTable( {
    "processing": true,
    "serverSide": true,
   "searching": true,
    "ajax": {
        "url": "<?= $this->webroot ?>Accountings/get_All_customer_list_ajax",
        "type": "POST",
        data:function( d ) {
            d.executive_id=$('#executive_view_id option:selected').val();
            d.route_id=$('#route_view_id option:selected').val();
            d.group=$('#group_view option:selected').val();
            d.customer_type_id=$('#customer_type option:selected').val();
            d.account_head_id=$('#name').val();
            },
            "dataSrc": "records",
        },
        "columns": [
        { "data" : "AccountHead.customer_order" },
        { "data" : "Route.name",},
        { "data" : "CustomerGroup.name",},
        { "data" : "CustomerType.name",},
        { "data" : "Customer.code",},
        { "data" : "AccountHead.name" },
        { "data" : "AccountHead.priority" },
        { "data" : "Customer.credit_limit",className:"text-right" },
        { "data" : "AccountHead.action" },
        ],
         "dom": 'Bfrtip',
    lengthMenu: [
    [10,25, 50,100,-1],
    ['10 rows','25 rows', '50 rows','100 rows','Show all' ]
    ],
    buttons: [
    'pageLength',
    ],
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                i.replace(/[\$,]/g, '')*1 :
                typeof i === 'number' ?
                i : 0;
            };
            pageTotal = api.column( 7, { page: 'current'} ).data().reduce( function (a, b) {
                return intVal(a) + intVal(b);
            }, 0 );
            $( api.column( 7 ).footer() ).html(''+pageTotal+'');
        },
        "columnDefs": [],
    });
     $(document).on('click','.edit_priority',function(){
        var priority=$(this).closest('tr').find('td input.priority_edit').val();
        var edit_id= $. trim($(this).closest('tr').find('td span.AccountHead_id').text());
        $.post("<?= $this->webroot ?>Accountings/update_customer_priority/"+edit_id+'/'+priority,function(response)
        {
           if(response.status!="Success")
           {
            alert(response.status);
                return false;
           }

        },"json");
        location.reload();
    });
     $('#state_id').val(19).trigger('change');
     $(document).on('click', '.edit_head', function () {
        var id = $(this).closest('tr').find('td span.AccountHead_id').text();
        $.post("<?= $this->webroot ?>Accountings/Customer_get_ajax/" + id, function (responds) {
            console.log(responds);
            if (responds.result != 'Success')
            {
                alert(responds.message);
                return false;
            }
            if(responds.data.Customer.state_id)
            {
            $('#edit_state_id').val(responds.data.Customer.state_id).trigger('change');   
            }
            else
            {
            $('#edit_state_id').val(19).trigger('change');
            }
            $('#name_edit').val(responds.data.AccountHead.name);
            console.log(responds.data.Customer.arabic_name);
            $('#name_edit_arabi').val(responds.data.Customer.arabic_name);
            $('#opening_balance_edit').val(responds.data.AccountHead.opening_balance).trigger('change');
            $('#edit_customer_type_id').val(responds.data.CustomerType.id).trigger('change');
            $('#route_id_edit').val(responds.data.Customer.route_id).trigger('change');
            $('#customer_group_edit').val(responds.data.Customer.customer_group_id).trigger('change');
            $('#code_edit').val(responds.data.Customer.code);
            $('#email_edit').val(responds.data.Customer.email);
            $('#mobile_edit').val(responds.data.Customer.mobile);
            $('#address_edit').val(responds.data.Customer.place);
            $('#description_edit').val(responds.data.AccountHead.description);
            $('#customer_id_hide').val(responds.data.Customer.id);
            $('#AccountHead_id').val(responds.data.AccountHead.id);
            $('#place_edit').val(responds.data.Customer.place);
            $('#contact_person_edit').val(responds.data.Customer.contact_person);
            $('#credit_limit_edit').val(responds.data.Customer.credit_limit);
            $('#credit_limit_hidden').val(responds.data.Customer.credit_limit);
            $('#credit_period_edit').val(responds.data.Customer.credit_period);
            $('#division_edit').val(responds.data.Customer.division_id);
            $('#vat_no_edit').val(responds.data.Customer.vat_no);
            $('#discount_percentage_edit').val(responds.data.Customer.discount_percentage);
            $('#collection_edit').val(responds.data.Customer.collection_discount);
            $('#customer_discount_Edit tbody').empty();
            $.each(responds.discount_data,function(key, value){
                $('#customer_discount_Edit tbody').append('<tr>\
                    <td><input type="text" hidden name="data[CustomerEdit][discount_id][]"  value="'+value.CustomerDiscount.id+'" class="customer_discount_id"><input class="productsrow" type="text" hidden name="data[CustomerEdit][products][]" value="'+value.CustomerDiscount.product_id+'">\
                        <input type="text" value="'+value.Product.name+' '+value.Product.code+'" readonly class="form-control"></td>\
                        <td><input type="text" name="data[CustomerEdit][selling_rates][]" value="'+value.CustomerDiscount.selling_rate+'" class="form-control"></td>\
                        <td><i class="fa fa-minus-circle fa-2x ad-mar remove_old_tr"></i></td>\
                    </tr>');
            });

        }, "json");
    });
    $(document).on('click', '#edit_customer_button', function () {
         var data=$('#CustomerEdit').serialize();
  console.log(data);
  $.post( "<?= $this->webroot ?>Accountings/Customer_account_edit",data ,function( data ) {
    if(data.result!='Success')
     {
       alert(data.result);
       return false;
     }
    // $('#Route_Form')[0].reset();
     //location.reload();
    // $('#add_location').modal('toggle');
   }, "json");
        });
</script>
<!-- For delete -->
<script type="text/javascript">
    $(document).on('click', '.delete', function () {
        if (!confirm("Are you sure?"))
        {
            return false;
        }
        var id = $(this).closest('tr').find('td span.AccountHead_id').text();
        var rowindex = $(this).closest('tr').index();
        $.post("<?= $this->webroot ?>Accountings/customer_delete/" + id, function (data) {
            if (data.result != 'Success')
            {
                alert(data.message);
                return false;
            }
            $('#AccountHead_table tbody tr:eq(' + rowindex + ')').remove();
        }, "json");
    });
</script>
<script type="text/javascript">
    $('#add_customer_type').click(function () {
        var name = $('#customer_type_name').val();
        if (name == '')
        {
            $('#customer_type_name').focus();
            return false;
        }
        var website_url = '<?php echo $this->webroot; ?>CustomerType/add_customer_type_ajax';
        var data = {
            name: name,
        };
        $.ajax({
            method: "POST",
            url: website_url,
            data: data,
            dataType: 'json',
        }).done(function (data) {
            if (data.result != 'Success')
            {
                alert(data.result);
                return false;
            }
            $('#customer_type_modal').modal('toggle');
            $('#customer_type_name').val('');
            $('#customer_type').append($("<option></option>").attr("value", data.key).text(data.value));
            $('#modal_customer_type').append($("<option></option>").attr("value", data.key).text(data.value));
            $('#customer_type').val(data.key).trigger('change');
            $('#modal_customer_type').val(data.key).trigger('change');
        });
    });
</script>
<script type="text/javascript">
    $('#executive_view_id').change(function () {
        var executive_id = $(this).val();
        $.fn.executive_change(executive_id);
        $.fn.table_search();
    });
    $.fn.executive_change = function (executive_id) {
        var data = {executive_id: executive_id};
        var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/executive_select_ajax';
        $.ajax({
type: "post", // Request method: post, get
url: url_address,
data: data, // post data
success: function (response) {
    $('#route_view_id').html(response);
},
error: function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
}
});
    }
    $('#route_view_id').change(function () {
        var route_id = $(this).val();
        $.fn.route_change(route_id);
        $.fn.table_search();
    });
    $.fn.route_change = function (route_id) {
        var data = {route_id: route_id};
        var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/route_select_ajax';
        $.ajax({
            type: "post",
            url: url_address,
            data: data,
            success: function (response) {
//  $('#day_view').html(response);
$('#group_view').html(response);
},
error: function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
}
});
    }
// $('#day_view').change(function () {
//     var day = $('#day_view option:selected').text();
//     var route_id = $('#route_view_id option:selected').val();
//     $.fn.day_change(day, route_id);
//     $.fn.table_search();
// });
//
$('#group_view').change(function () {
    var group = $('#group_view option:selected').val();
    var route_id = $('#route_view_id option:selected').val();
    $.fn.group_change(group, route_id);
    $.fn.table_search();
});
$.fn.group_change = function (group, route_id) {
    var data = {group: group, route_id: route_id};
    var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/group_select_ajax';
    $.ajax({
        type: "post",
        url: url_address,
        data: data,
        success: function (response) {
            $('#customer_type').html(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
}
//
$.fn.day_change = function (day, route_id) {
    var data = {day: day, route_id: route_id};
    var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/day_select_ajax';
    $.ajax({
        type: "post",
        url: url_address,
        data: data,
        success: function (response) {
            $('#customer_type').html(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
}
$('#customer_type').change(function () {
    var customer_type = $(this).val();
    var executive_id = $('#executive_view_id option:selected').val();
    var route_id = $('#route_view_id option:selected').val();
    var day = $('#day_view option:selected').text();
    $.fn.customer_type_change(customer_type, executive_id, route_id, day);

    $.fn.table_search();
});
$.fn.customer_type_change = function (customer_type, executive_id, route_id, day) {
    var data = {customer_type: customer_type, executive_id: executive_id, route_id: route_id,
// day: day
};
var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/customer_type_select_ajax';
$.ajax({
    type: "post",
    url: url_address,
    data: data,
    success: function (response) {
        $('#name').html(response);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
    }
});
}
$('#name').change(function () {
    $.fn.table_search();
});
// $('#customer_add_button').click(function(){
//     var data=$('#Customer_Form').serialize();
//     $.post( "<?= $this->webroot ?>Customer/add_ajax",data ,function( data ) {

//         if(data.result!='Success')
//         {
//             alert(data.result);
//             return false;
//         }
    
//         $('#SaleAccountHeadId').append($("<option></option>").attr("value",data.key).text(data.value));
//         $('#SaleAccountHeadId').val(data.key);
//         $('#SaleAccountHeadId').trigger('change');

//         $('#name').append($("<option></option>").attr("value",data.key).text(data.value));
//         $('#customer_modal_add').modal('toggle');
//         // $('#customer_modal').modal('toggle');
//         $('#Customer_Form')[0].reset();
//         $('#division_id').trigger('change');
//         $('#route_id').trigger('change');
//         $('#modal_customer_type_id').trigger('change');
//         $('#customer_group_id').trigger('change');
//         $('#name').val(data.key);
//         $('#name').trigger('change');
//         $('#customer_discount_table tbody').empty();
//         $.fn.table_search();
//     }, "json");
// });
$(document).on('click', '#customer_update_button', function () {
        var website_url = '<?php echo $this->webroot; ?>Accountings/Customer_account_edit';
        var data=$('#CustomerEdit_Form').serialize();
        $.ajax({
            method: "POST",
            url: website_url,
            data: data,
            dataType:'json',
        }).done(function (data) {
            if (data.result != 'Success')
            {
                alert(data.result);
                return false;
            }
            location.reload();
        });
    });
</script>
<!-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFVITJEAwer4-t9yNbDpJBd_n1j8-YQ1U&callback=initMap">
    </script> -->
<script type="text/javascript">
    <?php require('state.js'); ?>
        <?php require('customer.js'); ?>

</script>
