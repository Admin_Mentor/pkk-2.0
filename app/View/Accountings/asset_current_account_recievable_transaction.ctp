<section class="content-header">
	<h1>Debters Transaction </h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
						<?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" hidden="hidden">
						<?= $this->Form->input('customer_type',array('type'=>'select','class'=>'form-control select_two_class ','style'=>'width: 100%;','id'=>'customer_type','empty'=>'All','label'=>'Customer Type',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select_two_class','style'=>'width: 100%;','id'=>'account_head','empty'=>'All','options'=>$Customer_list,'required','label'=>'Acc/Name',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select_two_class ','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" >
						<?= $this->Form->input('mode',array('type'=>'select','class'=>'form-control select_two_class ','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher')) ?>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('amount',array('class'=>'form-control number','type'=>'text','required','id'=>'amount',)); ?>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
						<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
					</div>
					<div class="col-md-3"> 
						<div class="create-wrapper"><br>
							<button class="user_add_btn">Save & Print</button>
						</div>
					</div>
					<!-- <div class="col-md-1 col-lg-1 col-sm-1"><br>
						<button class="btn btn btn-default print" id="ledger_pdf">Print</button>
					</div> -->
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body table-responsive no-padding">
			<table class="boder table table-condensed table boder" id="table_data">
				<thead>
					<tr class="blue-bg">
						<th>Code</th>
						<th>Account Name</th>
						<th style="text-align:left">Total</th>
						<th style="text-align:left">Recieved</th>
						<th style="text-align:left">Balance</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
					<tr>
						<th></th>
						<th  style="font-size:20px; color:red;">Total:</th>
						<th class="" style="font-size:20px; color:red; text-align: right">0</th>
						<th class="" style="font-size:20px; color:red;text-align: right">0</th>
						<th class="" style="font-size:20px; color:red;text-align: right">0</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</section>
<?php require('general_journal_transaction_modal.php') ?>
<script type="text/javascript">
	<?php require('asset_current_account_recievable_transaction.js'); ?>
</script>
<script type="text/javascript">
	$('#table_data').DataTable( {
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Accountings/AssetCurrentAccountRecievableTransaction_table_ajax",
			"type": "POST",data:function( d ) {
				d.customer_type_id= $('#customer_type').val();
				d.account_head_id= $('#account_head').val();
			},
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "Customer.code" },
		{ "data" : "AccountHead.name" },
		{ "data" : "AccountHead.total",'class':"text-right" },
		{ "data" : "AccountHead.recieved",'class':"text-right" },
		{ "data" : "AccountHead.balance",'class':"text-right" },
		],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};
			total = api.column( 2, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 2 ).footer() ).html(''+parseFloat(total.toFixed(3))+'');
			total = api.column( 3, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 3 ).footer() ).html(''+parseFloat(total.toFixed(3))+'');
			total = api.column( 4, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 4 ).footer() ).html(''+parseFloat(total.toFixed(3))+'');
		},
		"columnDefs": [
		{ className: "customer_flag", "targets": [ 0 ],"width": "30%",},
		{ className: "name", "targets": [ 1 ],"width": "30%", },
		{"targets":[2 ],className:""},
		{"targets":[3 ],className:""},
		{"targets":[4 ],className:""},
		],
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$(nRow).addClass('view_transaction');
			return nRow;
		}
	});
	$('#customer_type,#account_head').change(function(){
		table = $('#table_data').dataTable();
		table.fnDraw();
	});
</script>
<script type="text/javascript">
	var flash='<?= $flash=$this->Session->flash(); ?>';
	if(flash){
		var current_url=window.location.href;
		var url=current_url.split("/");
		var length=url.length;
		var id=url[length-2];
		var amount=url[length-1];
		if(id){
			var link = "<?= $this->webroot.'Print/account_receivable_fpdf/'?>"+id+'/'+amount;
			window.open(link, '_blank');
		}
	}
// 	$( document ).ready(function() {
//     $('#account_head').select2("open");
// });
</script>