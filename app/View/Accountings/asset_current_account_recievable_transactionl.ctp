<section class="content-header">
	<h1>Debters Transaction </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="row-wrapper">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<?php echo $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
								<div class="box-body">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('customer_type',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','id'=>'customer_type','options'=>$CustomerType_list,'required','label'=>'Customer Type',)); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','id'=>'account_head','options'=>$Customer_list,'required','label'=>'Acc/Name',)); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?= $this->Form->input('amount',array('class'=>'form-control','type'=>'number','step'=>'any','required','id'=>'amount',)); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select2','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" >
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id='mode_field' style="display:none">
														<?php echo $this->Form->input('mode',array('type'=>'select','class'=>'form-control select2','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
													</div>
												</div>
											</div>
											<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12" style="display:none;">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
													</div>
												</div>
											</div>
											<div class="col-md-1"> 
												<div class="create-wrapper"><br>
													<button class="user_add_btn">ADD</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<?= $this->Form->end(); ?>
							</div>
							<div class="row-wrapper">
								<div class="row">
									<div class="col-md-12">
										<div class="box-body table-responsive no-padding">
											<table class="boder table table-condensed table datatable boder" id="myTable">
												<thead>
													<tr class="blue-bg">
														<th class="padding_left">Date</th>
														<th>Acc Name</th>
														<th>Total</th>
														<th>Received</th>
														<th>Balance</th>
													</tr>
												</thead>
												<tbody>
													<?php $total=0; $Recieved=0; $Balance_Total=0; foreach ($All_Account as $key => $value): ?>
													<tr class="blue-pddng view_transaction">
														<td><?= date('d-m-Y',strtotime($value['date'])); ?></td>
														<td class='name'><?= $value['name']; ?></td>
														<td><?= $value['total']; $total+=$value['total'] ?></td>
														<td><?= $value['Recieved']; $Recieved+=$value['Recieved'] ?></td>
														<td><?= $value['total']-$value['Recieved']; $Balance_Total+=$value['total']-$value['Recieved'] ?></td>
													</tr>
												<?php endforeach ?>
											</tbody>
											<tfoot>
												<tr class="blue-pddng">
													<td></td>
													<td class="total_amount"><label>Total</label></td>
													<td class="total_amount"><?= $total; ?></td>
													<td class="total_amount"><?= $Recieved; ?></td>
													<td class="total_amount"><?= $Balance_Total; ?></td>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
	</div>
</div>
</section>
<div id="view_transaction_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><span id='account_holder_name'></span></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
						<div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="control-label">From Date</label></div>
						<div class="col-md-7">
							<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>false,)); ?>
						</div>
					</div>
					<div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
						<div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="control-label">To Date</label></div>
						<div class="col-md-7">
							<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>false,)); ?>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
					</div>
					<div class="col-md-6 col-lg-6 col-xs-12 col-sm-6" style="display:none;">
						<a href="#"><h4>Sale Print <i id='sale_print' class="fa fa-2x fa-print blue-col"></i></h4></a>
					</div>
				</div>
				<div class="box-body table-responsive no-padding xs_tp">
					<table class="table table-bordered table-hover" id="transaction_details_table">
						<thead>
							<tr class="blue-bg">
								<th>Date</th>
								<th>Cask/Bank</th>
								<th>Voucher NO</th>
								<th>Remarks</th>
								<th>Credit</th>
								<th>Debit</th>
								<th>Balance</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Save</button>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	<?php require('asset_current_account_recievable_transaction.js'); ?>
	<?php require('sale_print.js'); ?>
</script>