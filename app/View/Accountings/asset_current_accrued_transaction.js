 $('#category').change(function(){
  var category=$(this).val();
  $.post( "<?= $this->webroot ?>Accountings/AccountHead_Option_ListBySubGroupId_ajax/"+category, function( data ) {
    $('#account_head').html('');
    $.each(data.options,function(key,value){
      $('#account_head').append($("<option></option>").attr("value",key).text(value));
    });
    $('#account_head').trigger('change');
  }, "json");
});
 $('#account_head').change(function(){
  var account_head= $(this).val();
  $.post( "<?= $this->webroot ?>Accountings/get_accrued_balance/"+account_head, function( data ) {
    $('#accrued').val(data);
    $('#recieved').val(data);
  }, "json");
});
 $('#account_head').change();
 $('#recieved').keyup(function(){
  var recieved=$('#recieved').val();
  var accrued=$('#accrued').val();
  var balance=parseFloat(accrued)-parseFloat(recieved);
  $('#balance').val(balance);
});