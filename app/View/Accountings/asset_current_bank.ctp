<section class="content-header">
	<h1> Bank </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="row-wrapper">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<?php echo $this->Form->create('Bank', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Bank_Form']); ?>
								<div class="box-body">
									<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
										<div class="form-group">
											<div class="col-md-7 col-xs-12 col-sm-7 col-lg-7">
												<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
											</div>
										</div>
									</div>
									<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
												<?= $this->Form->input('name',array('class'=>'form-control name','type'=>'text','required','id'=>'name','label'=>'ACC/Name','placeholder'=>'Enter The Bank Name Completely')); ?>
											</div>
											<br>
											<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#Account_modal"></i></div>
										</div>
									</div>
									<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
												<?= $this->Form->input('opening_balance',array('class'=>'form-control opening_balance','type'=>'number','step'=>'any','required','readonly','id'=>'opening_balance',)); ?>
											</div>
										</div>
									</div>
									<div class="col-md-2"> 
										<div class="create-wrapper">
											<br>
											<button type='Submit' id='add_button' class="user_add_btn">ADD</button>
											<button type='Submit' id='edit_button' class="user_add_btn" style='display: none'>EDIT</button>
										</div>
									</div>
								</div>
								<!-- ============================= ACCOUNT NAME_MODAL_START_HERE ================================-->
								<div id="Account_modal" class="modal fade" role="dialog">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
												<h4 class="modal-title" id="myModalLabel">Account's Details</h4>
											</div>
											<?php echo $this->Form->create('BankDetails', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Bank_Form']); ?>
											<div class="modal-body">
												<div class="form-horizontal">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 control-label">Account's Name</label>
														<div class="col-sm-6">
															<?= $this->Form->input('name',array('class'=>'form-control name','type'=>'text','step'=>'any','required','id'=>'name_modal','placeholder'=>'Enter The Bank Name Completely','label'=>false,)); ?>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 control-label">Opening Balance</label>
														<div class="col-sm-6">
															<?= $this->Form->input('opening_balance',array('class'=>'form-control opening_balance','type'=>'number','step'=>'any','required','id'=>'opening_balance_modal','label'=>false,)); ?>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 control-label">Account No</label>
														<div class="col-sm-6">
															<?= $this->Form->input('account_no',array('class'=>'form-control','type'=>'text','id'=>'account_no','label'=>false,)); ?>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 control-label">Branch</label>
														<div class="col-sm-6">
															<?= $this->Form->input('branch',array('class'=>'form-control','type'=>'text','step'=>'any','id'=>'branch','label'=>false,)); ?>
														</div>
													</div>
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 control-label">Show in App</label>
														<div class="col-sm-6">
															<?= $this->Form->input('shwn_in_vouchr',array('class'=>'chkbx_icn_clk','type'=>'checkbox','id'=>'shwn_in_vouchr','step'=>'any','label'=>false,)); ?>

														</div>
													</div>
													
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-primary" data-dismiss="modal">Save</button>
											</div>
											<?= $this->Form->end(); ?>
										</div>
									</div>
								</div>
								<!-- =============================== ACCOUNT NAME_MODAL_END_HERE ===================================--> 
								<?= $this->Form->end(); ?>
							</div>
							<div class="row-wrapper">
								<div class="row">
									<div class="col-md-12">
										<div class="box-body table">
											<table class="table table-condensed table boder datatable" id="AccountHead_table">
												<thead>
													<tr class="blue-bg">
														<th class="padding_left">Date</th>
														<th>Account's Name</th>
														<th>Opening Balance</th>
														<th width="10%">Action</th>
														<!-- <th></th> -->
													</tr>
												</thead>
												<tbody>
													<?php foreach ($BankDetail as $key => $value): ?>
														<tr>
															<td><?= date('d-m-Y',strtotime($value['AccountHead']['created_at'])) ?></td>
															<td><span style="display:none" class='AccountHead_id'><?= $value['AccountHead']['id']; ?></span><span><?= $value['AccountHead']['name']; ?></span></td>
															<td class="text-right"><?= $value['AccountHead']['opening_balance']; ?></td>
															<td><i class="fa fa-2x fa-pencil-square-o blue-col edit"></i>
															<i class="fa fa-2x  fa-trash blue-col blue-col delete"></i></td>
														</tr>
													<?php endforeach ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$('.name').on('keyup change',function(){
		$('#name').val($(this).val());
		$('#name_modal').val($(this).val());
	});
	$('.opening_balance').on('keyup change',function(){
		$('#opening_balance').val($(this).val());
		$('#opening_balance_modal').val($(this).val());
	});
</script>
<!-- for edit  -->
<script type="text/javascript">
	$(document).on('click','.edit',function(){
		var id=$(this).closest('tr').find('td span.AccountHead_id').text();
		$.post( "<?= $this->webroot ?>Accountings/BankDetails_get_ajax/"+id, function( data ) {
			$('#name').val(data.Bank.name).trigger('change');
			$('#opening_balance').val(data.Bank.opening_balance).trigger('change');
			$('#account_no').val(data.BankDetail.account_no);
			//$('#currency').val(data.BankDetail.currency).trigger('change');;
			$('#branch').val(data.BankDetail.branch);
			//$('#shwn_in_vouchr').val(data.BankDetail.shwn_in_vouchr);
			if(data.BankDetail.shwn_in_vouchr=='1'){
				//$('#shwn_in_vouchr').is(':checked');
				$('#shwn_in_vouchr').attr('checked', true);
				//$("input[type=checkbox][value=1]").attr("checked","true");
			}
			$("#name").append("<input type='text' id='Bank_id' name='data[Bank][id]' value='"+data.Bank.id+"'>");
			$("#name_modal").append("<input type='text' id='BankDetail_id' name='data[BankDetails][id]' value='"+data.BankDetail.id+"'>");
			$('#add_button').css('display','none');
			$('#edit_button').css('display','');
			$('#Bank_Form').attr('action','<?= $this->webroot; ?>Accountings/Bank_account_edit');
		}, "json");
	});
</script>
<!-- For delete -->
<script type="text/javascript">
	$(document).on('click','.delete',function(){
		if(!confirm("Are you sure?"))
		{
			return false;
		}
		var id=$(this).closest('tr').find('td span.AccountHead_id').text();
		var rowindex = $(this).closest('tr').index();
		$.post( "<?= $this->webroot ?>Accountings/BankAccount_delete/"+id,function( data ) {
			if(data.result!='Success')
			{
				alert(data.message);
				return false;
			}
			$('#AccountHead_table tbody tr:eq(' + rowindex + ')').remove();
		}, "json");
	});
</script>