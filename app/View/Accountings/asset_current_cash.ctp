<section class="content-header">
	<h1> Cash </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="row-wrapper">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<?php echo $this->Form->create('Cash', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Cash_Form']); ?>
								<div class="box-body">
									<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
										<div class="form-group">
											<div class="col-md-7 col-xs-12 col-sm-7 col-lg-7">
												<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
											</div>
										</div>
									</div>
									<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
												<?= $this->Form->input('name',array('class'=>'form-control name','type'=>'text','required','id'=>'name','label'=>'ACC/Name','readonly'=>'readonly','placeholder'=>'')); ?>
											</div>
											<br>
											
										</div>
									</div>
									<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
												<?= $this->Form->input('opening_balance',array('class'=>'form-control opening_balance','type'=>'number','step'=>'any','required','id'=>'opening_balance',)); ?>
											</div>
										</div>
									</div>
									<div class="col-md-2"> 
										<div class="create-wrapper">
											<br>
											
											<button type='Submit' id='edit_button' class="user_add_btn" style='display: none'>EDIT</button>
										</div>
									</div>
								</div>
								
								<?= $this->Form->end(); ?>
							</div>
							<div class="row-wrapper">
								<div class="row">
									<div class="col-md-12">
										<div class="box-body table">
											<table class="table table-condensed table boder datatable" id="AccountHead_table">
												<thead>
													<tr class="blue-bg">
														<th class="padding_left">Date</th>
														<th>Account's Name</th>
														<th>Opening Balance</th>
														<th width="10%">Action</th>
														<!-- <th></th> -->
													</tr>
												</thead>
												<tbody>
													<?php foreach ($AccountHead as $key => $value): ?>
														<tr>
															<td><?= date('d-m-Y',strtotime($value['AccountHead']['created_at'])) ?></td>
															<td><span style="display:none" class='AccountHead_id'><?= $value['AccountHead']['id']; ?></span><span><?= $value['AccountHead']['name']; ?></span></td>
															<td class="text-right"><?= $value['AccountHead']['opening_balance']; ?></td>
															<td><i class="fa fa-2x fa-pencil-square-o blue-col cash_edit"></i>
															<a  href='<?= $this->webroot."Accountings/AccountHead_delete/".$value['AccountHead']['id']; ?>'><i class="fa fa-2x fa-trash blue-col blue-col cash_delete"></i></a></td>
														</tr>
													<?php endforeach ?>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$('.name').on('keyup change',function(){
		$('#name').val($(this).val());
		$('#name_modal').val($(this).val());
	});
	$('.opening_balance').on('keyup change',function(){
		$('#opening_balance').val($(this).val());
		$('#opening_balance_modal').val($(this).val());
	});
</script>
<!-- for edit  -->
<script type="text/javascript">
	$(document).on('click','.cash_edit',function(){
    var id=$(this).closest('tr').find('td span.AccountHead_id').text();
    $.post( "<?= $this->webroot ?>Accountings/AccountHeadGet_ajax/"+id, function( data ) {
      $('#name').val(data.AccountHead.name);
      $('#opening_balance').val(data.AccountHead.opening_balance).trigger('keyup');
      $('#description').val(data.AccountHead.description);
      $("#name").append("<input type='text' id='account_id' name='data[Cash][account_id]' value='"+data.AccountHead.id+"'>");
      $('#add_button').css('display','none');
      $('#edit_button').css('display','');
      //$('#AccountHead_Form').attr('action','<?= $this->webroot; ?>Accountings/EditAccountHead');
    }, "json");
  });
</script>