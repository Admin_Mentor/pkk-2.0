<section class="content-header">
	<h1>Cash</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="row-wrapper">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<?php echo $this->Form->create('Cash', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Cash_Form']); ?>
								<div class="box-body">
									<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
										<div class="form-group">
											<label for="inputEmail3" class="col-md-3 col-lg-3 col-sm-3 col-xs-12 control-label">Date</label>
											<div class="col-md-4 col-xs-12 col-sm-4 col-lg-4">
												<?php echo $this->Form->input('date',array('type'=>'text',
													'class'=>'form-control pull-right date_picker datepicker',
													'id'=>'date',
													'required',
													'data-inputmask'=>"'alias': 'dd-mm-yyyy'",
													'data-mask'=>'data-mask',
													'label'=>false,
													)); ?>
												</div>
											</div>
										</div>
										<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
											<div class="form-group">
												<label for="inputEmail3" class="col-md-5 col-lg-5 col-xs-12 col-sm-5 control-label">Opening Balance</label>
												<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
													<?= $this->Form->input('opening_balance',
														array('class'=>'form-control','type'=>'number',
															'step'=>'any',
															'required',
															'id'=>'opening_balance',
															'label'=>false,
															)); ?>
														</div>
													</div>
												</div>
												<div class="col-md-2"> 
													<div class="create-wrapper">
														<button type='submit' class="user_add_btn">Edit</button>
													</div>
												</div>
											</div>
											<?= $this->Form->end(); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<script type="text/javascript">
				$('#asset_li').css('display','');
				$('#asset_li').attr('class','treeview active');
			</script>