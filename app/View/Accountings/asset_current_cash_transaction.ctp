<section class="content-header">
	<h1>Cash Transaction 
		<!-- <a href="<?= $this->webroot ?>Accountings/AssetCurrentCash"><input type="button" class="btn btn-success save pull-right" value="Create"></input></a> -->
	</h1>
</section>
<section class="content">
	<div class="box box-primary">
			<div class="col-md-12">
				<?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Cash_Form']); ?>
				<div class="form-group">
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
						<?= $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'account_head','style'=>'width: 100%;','empty'=>'ALL','required','label'=>'Acc/Name',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
				</div>
				<?= $this->Form->end(); ?>
			</div>
		<div class="row-wrapper">
			<div class="box-body">
			<table class="table table-condensed table table-bordered" id="cash_table" data-order='[[ 0, "desc" ]]' data-page-length='10'>

					<thead>
						<tr class="blue-bg">
							<th width="9%">Date</th>
							<th width="30%">Account's Name</th>
							<th width="30%">Remark</th>
							<th>Debit</th>
							<th>Credit</th>
							<th>Closing Balance</th>
							<!-- <th></th> -->
<!-- 							<th>Action</th>
 -->						</tr>
					</thead>
					<tbody>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="2" style="font-size:20px; color:red;text-align:right">Total:</th>
						<th style="font-size:20px; color:red;"></th>
						<th style="font-size:20px; color:red;"></th>
						<th style="font-size:20px; color:red;"></th>
						<th style="font-size:20px; color:red;"></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
</section>
<script type="text/javascript">
<?php require('general_journal_transaction_modal.php') ?>
</script>
<script type="text/javascript">
  $(document).ready(function () {
 $("#account_head").select2({
    placeholder: "Search here...",
    width: '100%',
    ajax: {
      url: '<?= $this->webroot ?>Accounts/Searchcustomer',
      dataType: 'json',
      delay: 250,
      data: function (params) {
            return {
                      q: params.term, // search term
                      page: params.page
                   };
          },
          processResults: function (data, params) {
            params.page = params.page || 2;
            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: false
        },
      minimumInputLength: 2,
      templateResult: formatRepo,
      templateSelection: formatRepoSelection
    });

  function formatRepo (repo) 
  {
    if (repo.loading) return repo.text;
    return repo.text;
  }
  function formatRepoSelection (repo)
  {
    return repo.text || repo.text;
  }
  });
</script>
<script type="text/javascript">
$('#cash_table').DataTable( {
		"processing": true,
		"serverSide": true,
        "order": [[ 0, 'asc' ]],
		"ajax": {
			"url": "<?= $this->webroot ?>Accountings/Cash_transaction_ajax",
			"type": "POST",
			data:function( d ) {
				d.from_date= $('#from_date').val();
				d.to_date= $('#to_date').val();
			 d.account_head= $('#account_head').val();
			},
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "Journal.date" },
		{ "data" : "AccountHeadCredit.name" },
		{ "data" : "Journal.remarks" },
		{ "data" : "Journal.debit",className:"text-right" },
		{ "data" : "Journal.credit",className:"text-right" },
		{ "data" : "Journal.closing_balance",className:"text-right" },
		],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};
			pageTotal = api.column( 3, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 3 ).footer() ).html(''+pageTotal+'');
			pageTotal = api.column( 4, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 4 ).footer() ).html(''+pageTotal+'');
		},
	});
	<?php require('asset_current_cash_transaction.js'); ?>
</script>