<section class="content-header">
	<h1> Capital Create </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary box_tp_brdr">
				<div class="row-wrapper">
					<?= $this->Form->create('Capital',array('class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Capital_Form')) ?>
					<div class="box-body">
						<div class="col-md-12">
							<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
								<div class="form-group">
									<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
										<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date',)); ?>
									</div>
								</div>
							</div>
							<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
								<div class="col-md-12">
									<div class="form-group">
										<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
											<?php echo $this->Form->input('category',array('type'=>'select','class'=>'form-control select2','style'=>'width: 100%;','id'=>'category','empty'=>'Select','options'=>$SubGroup_list,'label'=>'Group',)); ?>
										</div>
										<br>
										<div class="col-md-4 col-lg-4 col-xs-12 col-sm-4">
											<div class="col-md-2"><i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#category_modal"></i></div>
											<div class="col-md-2"><i class="fa fa-pencil fa-2x plus-btn" id="edit_category_icon"></i></div>
											<div class="col-md-2"><i class="fa fa-trash fa-2x plus-btn" id="delete_category_icon"></i></div>
										</div>
									</div>
								</div>
							</div>
							<a href="#"><button class='btn btn-primary' data-toggle="modal" style="margin-top: 1%"  data-target="#account_details_modal">Add Capital Account Head</button></a>
						</div>
					</div>
					<div id="account_details_modal" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Accounts Details</h4>
								</div>

								<div class="modal-body">

									<div class="form-horizontal">
										<div class="form-group">
											<label class="col-sm-4 control-label">Group</label>
											<div class="col-sm-6">
												<?php echo $this->Form->input('category_add_modal',array('type'=>'select','class'=>'form-control select2','style'=>'width: 100%;','id'=>'category_add_modal','empty'=>'Select','options'=>$SubGroup_list,'required','label'=>false,)); ?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Account's Name</label>
											<div class="col-sm-6">
												<?= $this->Form->input('name',array('class'=>'form-control name','type'=>'text','step'=>'any','required','id'=>'name_modal','label'=>false,)); ?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Description</label>
											<div class="col-sm-6">
												<?= $this->Form->input('description',array('class'=>'form-control','type'=>'textarea','step'=>'any','rows'=>2,'id'=>'description','label'=>false,)); ?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Capital Acc Opening Balance</label>
											<div class="col-sm-6">
												<?= $this->Form->input('opening_balance_capital',array('class'=>'form-control opening_balance_capital','type'=>'number','step'=>'any','required','id'=>'opening_balance_capital','label'=>false,)); ?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Drawing Acc Opening Balance</label>
											<div class="col-sm-6">
												<?= $this->Form->input('opening_balance_drawing',array('class'=>'form-control opening_balance_drawing','type'=>'number','step'=>'any','required','id'=>'opening_balance_drawing','label'=>false,)); ?>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" id="Account_Add_Btn" data-dismiss="modal">Save</button>
								</div>

							</div>
						</div>
					</div>
					<div id="edit_account_details_modal" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h4 class="modal-title" id="myModalLabel">Account Details Updation </h4>
								</div>
								<div class="modal-body">

									<div class="form-horizontal">
										<div class="form-group">
											<label class="col-sm-4 control-label">Group</label>
											<div class="col-sm-6">
												<?= $this->Form->input('category_edit_modal',array('type'=>'select','class'=>'form-control select2','style'=>'width: 100%;','id'=>'category_edit_modal','empty'=>'Select','options'=>$SubGroup_list,'required','label'=>'',)); ?>
											</div>
										</div>

										<div class="form-group">
											<label class="col-sm-4 control-label">Account's Name</label>
											<div class="col-sm-6">
												<?= $this->Form->input('edit_modal_id',array('class'=>'form-control name','type'=>'hidden','step'=>'any','required','id'=>'edit_modal_id','label'=>false,)); ?>
												<?= $this->Form->input('edit_name_modal',array('class'=>'form-control name','type'=>'text','step'=>'any','required','id'=>'edit_name_modal','readonly','label'=>false,)); ?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Description</label>
											<div class="col-sm-6">
												<?= $this->Form->input('edit_description',array('class'=>'form-control','type'=>'textarea','step'=>'any','rows'=>2,'id'=>'edit_description','label'=>false,)); ?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Capital Acc Opening Balance</label>
											<div class="col-sm-6">
												<?= $this->Form->input('edit_opening_balance_capital',array('class'=>'form-control opening_balance_capital','type'=>'number','step'=>'any','required','id'=>'edit_opening_balance_capital','label'=>false,)); ?>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Drawing Acc Opening Balance</label>
											<div class="col-sm-6">
												<?= $this->Form->input('edit_opening_balance_drawing',array('class'=>'form-control opening_balance_drawing','type'=>'number','step'=>'any','required','id'=>'edit_opening_balance_drawing','label'=>false,)); ?>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-primary" id="Account_Edit_Btn" data-dismiss="modal">Update</button>
								</div>
							</div>
						</div>
					</div>
					<?= $this->Form->end(); ?>
				</div>
				<br>
				<div>
					<div class="row-wrapper">
						<div class="row">
							<div class="col-md-12">
								<div class="box-body table">
									<table class="table table-hover boder" id='myTable'>
								<!-- 	<table class="table table-condensed table table boder boder datatable" id="myTable"> -->
										<thead>
											<tr class="blue-bg">
												<!-- <th class="padding_left">#</th> -->
												<th class="padding_left">Date</th>
												<th>Group</th>
												<th>Account's Name</th>
												<th style="text-align: left">Opening Balance</th>
												<th width="10%">Action</th>
												<!-- <th>Delete</th> -->
											</tr>
										</thead>
										<tbody>
											
										</tbody>
										<tfoot>
								            <tr>
								              <th colspan="3" style="font-size:20px; color:red;text-align:right">Total:</th>
								              <th style="font-size:20px; color:red;text-align:right">0</th>
								              <th style="font-size:20px; color:red;"></th>
								              <!-- <th style="font-size:20px; color:red;"></th> -->
								            </tr>
							            </tfoot>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div id="category_modal" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h4 class="modal-title" id="myModalLabel">Category</h4>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label class="col-sm-4 control-label">Category Name</label>
										<div class="col-sm-8">
											<input class="form-control" id="catagory_name" type="text">
										</div>
										<br>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary" id='add_category'>Save</button>
								</div>
							</div>
						</div>
					</div>
					<div id="edit_category_modal" class="modal fade" role="dialog">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
									<h4 class="modal-title" id="myModalLabel">Edit Category</h4>
								</div>
								<div class="modal-body">
									<div class="form-group">
										<label class="col-sm-4 control-label">Category Name</label>
										<div class="col-sm-8">
											<input class="form-control" id="edit_catagory_name" type="text">
											<input id="edit_catagory_id" type="hidden">
										</div>
										<br>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary" id='edit_category'>Update</button>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
table_data=$('#myTable').dataTable({
	
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Accountings/Capital_Table_ajax",
      "type": "POST",
      data:function( d ) {
        d.category= $('#category').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    //{ "data" : "AccountHead.id" },
    { "data" : "AccountHead.created_at" },
    { "data" : "SubGroup.name" },
    { "data" : "AccountHead.name" },
    { "data" : "AccountHead.opening_balance",'class':"text-right"},
    { "data" : "AccountHead.action" },
   // { "data" : "AccountHead.delete" },
    ],
    "columnDefs": [
    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data; var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
      var pagecount=3;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
    },

  });
	$("[data-mask]").inputmask().val();
	$('.name').on('keyup change',function(){
		$(this).val($(this).val().trim());
		$('#name').val($(this).val());
		$('#name_modal').val($(this).val());
	});
$('#acc_name').keyup(function(){
	$(this).val($(this).val().trim());
	var acc_name=$(this).val();
	var capital_account_name=acc_name+' Capital';
	var drawing_account_name=acc_name+' Drawing';
	$('#capital_account_name').val(capital_account_name);
	$('#drawing_account_name').val(drawing_account_name);
});
$('#Account_Add_Btn').click(function(){
	var name=$('#name_modal').val();
	if($.trim(name)=='')
	{
		$('#name_modal').focus();
		return false;
	}
	var data=$('#Capital_Form').serialize();
	//var url_address= "/alhudaibah/Accountings/Capital_account_add_ajax";
	var url_address= '<?php echo $this->webroot; ?>'+'Accountings/Capital_account_add_ajax/';
	$.post( url_address,data, function( response ) {
		if(response.result!='Success')
		{
			alert(response.message);
			return false;
		}
		else
		{
			location.reload();
		}
	}, "json");
});

$(document).on('click','.edit_head',function(){
	var id=$(this).attr('table_id');
	//var id=$(this).closest('tr').find('td span.AccountHead_id').text();
	$.post( "<?= $this->webroot ?>Accountings/CapitalAccount_get_ajax/"+id, function( data ) {
		$('#edit_modal_id').val(id);
		$('#category_edit_modal').val(data.Capital_group.id).change();
		$('#edit_name_modal').val(data.AccountHead.name);
		$('#edit_opening_balance_capital').val(data.AccountHead_Capital.opening_balance);
		$('#edit_opening_balance_drawing').val(data.AccountHead_Drawing.opening_balance);
		$('#edit_description').val(data.AccountHead_Capital.description);
		$("#edit_capital_account_name").append("<input type='text' id='capital_account_id' name='data[Capital][account_id][]' value='"+data.AccountHead_Capital.id+"'>");
		$("#edit_drawing_account_name").append("<input type='text' id='drawing_account_id' name='data[Capital][account_id][]' value='"+data.AccountHead_Drawing.id+"'>");
		$('#edit_account_details_modal').modal('show');
	}, "json");
});
$(document).on('click','.delete_head',function(){
    if(!confirm("Are you sure?"))
    {
      return false;
    }
    id=$(this).attr('table_account_name');
    //var id=$(this).closest('tr').find('td.Product_hidden_feilds span.Product_id').text();
    //var Product_name=$(this).closest('tr').find('td.Product_name').text();
    //var rowindex = $(this).closest('tr').index();
    var url_address= '<?php echo $this->webroot; ?>'+'Accountings/Capital_delete/'+$.trim(id);
    $.ajax({
      type: "post",  
      url:url_address,
    // data: data, 
    dataType:'json',
    success: function(response) {
      if(response.result=="Success")
      {
        location.reload();
      }
      else
      {
        $('#myTable tbody tr:eq(' + rowindex + ')').attr('style','color:red');
        $('#myTable tbody tr:eq(' + rowindex + ')').attr('class','blue-pd flash');
        setTimeout(timer_for_table, 2000);
      }
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  });
$('#Account_Edit_Btn').click(function(){
	var name=$('#edit_name_modal').val();
	if($.trim(name)=='')
	{
		$('#edit_name_modal').focus();
		return false;
	}
	var data=$('#Capital_Form').serialize();
	var url_address= '<?php echo $this->webroot; ?>'+'Accountings/Capital_account_edit_ajax/';
	$.post( url_address,data, function( response ) {
		if(response.result!='Success')
		{
			alert(response.message);
			return false;
		}
		else
		{
			location.reload();
		}
	}, "json");
});
$('#catagory_name').keyup(function(){
	$('#catagory_name').val($(this).val().trim());
})
$('#add_category').click(function(){
	var catagory_name=$('#catagory_name').val();
	var date=$('#date').val();
	var group_id=10;
	if(!catagory_name)
	{
		$('#catagory_name').focus();
		return false;
	}
	var data={
		name:catagory_name,
		group_id:group_id,
		date:date,
	}
	var url_address= '<?php echo $this->webroot; ?>'+'Accountings/capital_sub_category_add_ajax';
	$.ajax({
		type: "POST",  
		url:url_address,
		data:data,
		dataType:'json',
		success: function(response) {
			if(response.result!='Success')
			{
				return false;
			}
			$('#category').append($("<option></option>").attr("value",response.key).text(response.value));
			$('#category').val(response.key).trigger('change');
			$('#catagory_name').val('');
			$('#category_modal').modal('toggle');
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
});
$('#edit_catagory_name').keyup(function(){
	$(this).val($(this).val().trim());
});
$('#edit_category_icon').click(function(){
	var name=$('#category option:selected').text();
	if(!name)
	{
		$('#category').select2('open');
		return false;
	}
	var split_name=name.split(' ');
	var id=$('#category').val();
	$('#edit_catagory_name').val(split_name[0]);
	$('#edit_catagory_id').val(id);
	$('#edit_category_modal').modal('toggle');
});
$('#delete_category_icon').click(function(){
	var id=$('#category option:selected').val();
	if(!id)
	{
		$('#category').select2('open');
		return false;
	}
	if(!confirm("Are you sure?"))
    {
      return false;
    }
	var url_address= '<?php echo $this->webroot; ?>'+'Accountings/capital_sub_category_delete_ajax/'+$.trim(id);
	$.ajax({
      type: "post",  
      url:url_address,
    // data: data, 
    dataType:'json',
    success: function(response) {
      if(response.result=="Success")
      {
        $('#category option[value="' + id + '"]').text(name).remove();
      }
      else
      {
        alert(response.result);
		return false;
      }
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
});
$('#edit_category').click(function(){
//var name=$('#edit_catagory_name').val();
var name=$('#edit_catagory_name').val().toUpperCase();
var id=$('#edit_catagory_id').val();
var data={
	name:name,
	id:id
}
$.post( "<?= $this->webroot ?>Accountings/capital_sub_category_edit_ajax",data, function( data ) {
	if(data.result!='Success')
	{
		alert(data.result);
		return false;
	}
	$('#category option[value="' + id + '"]').text(name).change();
	$('#category').select2();
	$('#edit_category_modal').modal('toggle');
}, "json");
});

$('#category').change(function(){
	table_data.fnDraw();
});
</script>