<section class="content-header">
  <h1>Capital Transaction 
    <a href="<?= $this->webroot ?>Accountings/Capital"><input type="button" class="btn btn-success save pull-right" value="Create"></input></a> </h1>
  </section>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header">
        <?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Capital_Form']); ?>
        <div class="col-md-12">
          <div class="form-group">
            <div class="col-md-2 col-xs-12 col-sm-2 col-lg-2">
              <?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
              <?= $this->Form->input('Type',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','options'=>$Type,'required',)); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
              <?= $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'account_head','style'=>'width: 100%;','options'=>$Accounts,'required','label'=>'Acc/Name',)); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
              <?= $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select2','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" >
              <?= $this->Form->input('mode',array('type'=>'select','class'=>'form-control select2','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
              <?= $this->Form->input('amount',array('class'=>'form-control','type'=>'number','step'=>'any','required','id'=>'amount','label'=>'Amount')); ?>
            </div>
            <div class="col-md-4">
              <?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
              <?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher')) ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12 col-md-offset-0"> 
              <div class="create-wrapper"><br>
                <button class="user_add_btn">ADD</button>
              </div>
            </div>
          </div>
        </div>
        <?= $this->Form->end(); ?>
      </div>
      <div class="box-body table">
        <table class="table table-bordered table-hover boder boder datatable" id="All_Account_Table">
          <thead>
            <tr class="blue-bg">
              <th class="padding_left">Date</th>
              <th>Account</th>
              <th class="capital_th">Capital</th>
              <th class="capital_th">Drawing</th>
              <th>Balance</th>
            </tr>
          </thead>
          <tbody>
            <?php $Capital=0; $Drawing=0; $Total=0; foreach ($All_Account as $key => $value): ?>
            <tr class="blue-pddng view_transaction">
              <td><?= date('d-m-Y',strtotime($value['date'])); ?></td>
              <td class='name'><?= $value['name']; ?></td>
              <td class="text-right"><?= $value['Capital']; $Capital+=$value['Capital'] ?></td>
              <td class="text-right"><?= $value['Drawing']; $Drawing+=$value['Drawing'] ?></td>
              <td class="text-right"><?= $value['Capital']-$value['Drawing']; $Total+=$value['Capital']-$value['Drawing'] ?></td>
            </tr>
          <?php endforeach ?>
        </tbody>
        <tfoot>
          <tr class="blue-pddng">
            <td></td>
            <td class="total_amount"><label>Total</label></td>
            <td class="total_amount text-right"><?= $Capital; ?></td>
            <td class="total_amount text-right"><?= $Drawing; ?></td>
            <td class="total_amount text-right"><?= $Total; ?></td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</section>
<?php require('general_journal_transaction_modal.php') ?>
<script type="text/javascript">
  <?php require('capital_transactions.js'); ?>
</script>