$('#type').change(function(){
	var id=$(this).val();
	$.post( "<?= $this->webroot ?>Accountings/Capital_Drawing_select_ajax/"+id, function( data ) {
		$('#account_head').empty();
		$.each(data,function(key,value){
			$('#account_head').append( new Option(value,key) );
		});
		$('#account_head').trigger("chosen:updated");
	}, "json");
});
$.fn.search_transaction_ajax=function(data_array){
	$.post( "<?= $this->webroot ?>Accountings/Capital_Drawing_transaction_ajax",data_array ,function( data ) {
		$('#transaction_details_table tbody').empty();
		$('#transaction_details_table tfoot').empty();
		$('#transaction_details_table tbody').html(data.row.tbody);
		$('#transaction_details_table tfoot').html(data.row.tfoot);
	}, "json");
}
$('.view_transaction').click(function(){
	var name=$(this).closest('tr').find('td.name').text();
	var from_date=$('#from_date').val();
	var to_date=$('#to_date').val();
	$('#account_holder_name').text(name);
	var data_array={
		name:name,from_date:from_date,to_date:to_date
	}
	$.fn.search_transaction_ajax(data_array);
	$('#view_transaction_modal').modal('show');
});
$(document).on('change keyup','#from_date,#to_date',function(){
	var from_date=$('#from_date').val();
	var to_date=$('#to_date').val();
	var name=$('#account_holder_name').text();
	var data_array={
		name:name,from_date:from_date,to_date:to_date
	}
	$.fn.search_transaction_ajax(data_array);
});