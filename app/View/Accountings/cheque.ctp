<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1> Cheque Management </h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="box-header">
        <?= $this->Form->create('Cheque', array( 'url' => array('controller' => 'Accountings', 'action' => 'cheque') ));?>
        <div class="col-md-12">
          <div class="row" style="margin-top: 1%;">
            <div class="col-md-3">
              <div class="form-group" >
                <?= $this->Form->input('account_head_id',array('type'=>'select','options'=>$AccountHeadList,'empty' =>'Select','class'=>'form-control select_two_class','label'=>'Account Head','required'=>'required')) ?>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <?= $this->Form->input('bank_account_head_id',array('type'=>'select','options'=>$BankList,'empty' =>'Select','class'=>'form-control select_two_class','required'=>'required','label'=>'Bank')) ?>
              </div>
            </div> 
            <div class="col-md-3">
              <?= $this->Form->input('cheque_no',array('class'=>"form-control form-group",'required'=>'required','label'=>'Cheque No')); ?>  
            </div>      
            <div class="col-md-2">
              <?= $this->Form->input('cheque_amount',array('class'=>"form-control form-group number",'required'=>'required','type'=>'text','label'=>'Cheque Amount')); ?>  
            </div> 
            <div class="col-md-2">
              <?= $this->Form->input('remarks',array('label'=>'From Bank','class'=>"form-control form-group",'type'=>'text')); ?>  
            </div>  
          </div>
        </div>
        <div class="row"> 
          <div class="col-md-12"> 
            <div class="col-md-2">
              <?= $this->Form->input('deposited_by',array('label'=>'Recieved By','class'=>"form-control form-group",'type'=>'text')); ?>  
            </div>     
            <div class="col-md-2">
              <div class="form-group">
                <?= $this->Form->input('recieved_date',array('type'=>'text','class'=>'form-control datepicker','label'=>'Received Date')); ?>
              </div>
            </div>                                                        
            <div class="col-md-2">
              <div class="form-group">
                <?= $this->Form->input('clearing_date',array('type'=>'text','class'=>'form-control datepicker')); ?>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <?= $this->Form->input('Cheque_date',array('type'=>'text','class'=>'form-control datepicker','label'=>'Cheque Date')); ?>
              </div>
            </div>
            <div class="col-md-3">
              <br>
              <button type="submit" class="btn btn-success save_btn_new create_icon" style="margin-top: 5px;">Save</button>
            </div> 
          </div>
        </div>
        <div class="row">
          <div class="col-md-12"> 
            <div class="col-md-3">
              <?= $this->Form->input('mode',array('type'=>'select','class'=>'form-control select_two_class account_head','id'=>'account_id','label'=>'Account Type','options'=>array('0'=>'ALL','1'=>'Creditors/Suppliers','2'=>'Customers','3'=>'Expense'))); ?>
            </div>
            <div class="col-md-3">
              <?= $this->Form->input('from_date',array('type'=>'text','id'=>'from_date','value'=>$from,'class'=>'form-control datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
            <div class="col-md-3">
              <?= $this->Form->input('to_date',array('type'=>'text','id'=>'to_date','value'=>$to,'class'=>'form-control datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
            <div class="col-md-2"><br>
              <button class='btn' type='button' id='fetch_button'>Fetch</button>
            </div>
          </div>
        </div>
        <br><br>
        <?= $this->Form->end(); ?>
      </div>
      <div class="box-body">
        <table class="table table-condensed" id="Cheques_table" data-order='[[ 6, "asc" ]]' data-page-length='25'>
          <thead>
            <tr class="blue-bg">
              <th>Account Head</th>
              <th>Bank Name</th>
              <th>Cheque Number</th>
              <th>Cheque Amount</th>
              <th>From Bank</th>
              <th>Received Date</th>
              <th>Clearing Date</th>
              <th>Cheque Date</th>
              <th>Status</th>
              <th>Action</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <?php foreach( $Cheques as $cheques ):
            if($cheques['Cheque']['status']!='1' && $cheques['Cheque']['status']!='2'){ ?>
            <tr class="blue"> 
              <td class='Cheques_table'>
                <span class='Cheques_table_id' style="display:none"><?= $cheques['Cheque']['id']; ?></span>
                <span><?= $cheques['AccountHead']['name']; ?></span>
                <span hidden class="account_head_class"><?= $cheques['AccountHead']['id']; ?></span>
                <span hidden class="sub_group_class"><?= $cheques['AccountHead']['sub_group_id']; ?></span>
              </td>
              <td><?= $cheques['BankAccountHead']['name']; ?></td>
              <td><?= $cheques['Cheque']['cheque_no']; ?></td>
              <td class="Cheques_amount"><?= $cheques['Cheque']['cheque_amount']; ?></td>
              <td class="remarks"><?= $cheques['Cheque']['remarks']; ?></td>
              <td class='recieved_date'><?php if($cheques['Cheque']['recieved_date']) { echo date('d-m-Y',strtotime($cheques['Cheque']['recieved_date'])); } ?></td>
              <td class='clearing_date'><?= date('d-m-Y',strtotime($cheques['Cheque']['clearing_date'])); ?></td>
              <td><?= date('d-m-Y',strtotime($cheques['Cheque']['cheque_date'])); ?></td>
              <?php if($cheques['Cheque']['status']=='1') { $state_color='green'; $status='Cleared'; } else if($cheques['Cheque']['status']=='2') { $state_color='red'; $status='Bounce'; } else if($cheques['Cheque']['status']=='3') { $state_color='red'; $status='Cancelled'; } else if($cheques['Cheque']['status']=='4') { $state_color='green'; $status='Submitted'; } else { $state_color='orange'; $status='Uncleared'; } ?>
              <td class='status_name' style=' color:<?= $state_color; ?>'><?= $status; ?></td>
              <?php if($cheques['Cheque']['status'] == 0 || $cheques['Cheque']['status'] == 4) { ?>
              <td class="status_dt">
                <i class="fa fa-pencil-square-o blue-col fa-1x ad-mar circle_add"> </i>
              </td>
              <td>
                <a href="#" ><i class="fa fa-trash blue-col cheque_delete" data-id="<?= $cheques['Cheque']['id'];?>"></i></a>
              </td>
              <?php }else{ ?>
              <td></td>
              <td></td>
              <?php } ?>
            </tr>
            <?php } endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
<div id="myModal9014" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Depositor Add</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal row-fluid"> 
          <div class="col-md-2">
            <label  class="control-label chk_name">Name</label>
          </div>
          <div class="col-md-10">
            <?= $this->Form->input('name',array('class'=>'form-control user-success','label'=>false,'id'=>'adddepname')) ?>
          </div>
          <div class="modal-footer Cheques_pop_color">
            <button type="button" class="btn btn-primary btn_cls_sav" data-dismiss="modal" id="adddep">Add Depositor</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="status_update_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Update Date & Status</h4>
      </div>
      <?= $this->Form->create('UpdateCheque'); ?>
      <div class="modal-body">
        <div class="form-group">
          <div class="col-md-3">
            <label  class="control-label chk_name" id="">Clearing Date</label>
          </div>
          <div class="col-md-9">
            <?= $this->Form->input('clearing_date_update',array('class'=>'form-control form-group user-success datepicker','id'=>'clearing_date_update','label'=>false,'value'=>date('d-m-Y'))); ?>
            <?= $this->Form->input('Cheques_id_update',array('type'=>'hidden','id'=>'Cheques_id_update')); ?>
            <?= $this->Form->input('sub_group_id_update',array('type'=>'hidden','id'=>'sub_group_id_update')); ?>
            <?= $this->Form->input('account_head_id_update',array('type'=>'hidden','id'=>'account_head_id_update')); ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-3">
            <label  class="control-label status_label">Status</label>
          </div>
          <div class="col-md-9">
            <?= $this->Form->input('Cheques_status_update',array('class'=>'form-control user-success','id'=>'clear_status','label'=>false,'options'=>array('0'=>'Select'))); ?>
            <br>
            <br>
          </div>
        </div>
        <div class="form-group cheque_method">
          <div class="col-md-3">
            <label  class="control-label">Method</label>
          </div>
          <div class="col-md-9">
            <?= $this->Form->input('cheque_entry_method',array('class'=>'form-control user-success','id'=>'cheque_entry_method','label'=>false,'options'=>$cheque_entry_method)); ?>
            <br>
            <br>
          </div>
        </div>
        <div id="customer_invoice_list_div">
          <div class="form-group">
            <div class="col-md-3">
              <label  class="control-label status_label">Amount</label>
            </div>
            <div class="col-md-3">
              <?= $this->Form->input('invoice_amount',array('class'=>'form-control number_field money_keyup field_select','id'=>'invoice_amount','label'=>false,'value'=>'0','readonly')) ?>
              <br>
              <br>
            </div>
          </div>
        </div>
        <div id="customer_invoice_list_div_bounce" hidden>
          <div class="form-group">
            <div class="col-md-3">
              <label  class="control-label status_label">Service Charge</label>
            </div>
            <div class="col-md-9">
              <?= $this->Form->input('service_amount',array('class'=>'form-control number_field money_keyup field_select','id'=>'service_amount','label'=>false,'value'=>'0')) ?>
              <br>
              <br>
            </div>
          </div>
        </div>
        <div class="form-group" hidden=true id="service_expense" hidden>
          <div class="col-md-3">
            <label  class="control-label status_label">Service Charge</label>
          </div>
          <div class="col-md-9">
            <input type="text" class="form-control form-group " id="service_charge" value="0">
          </div>
        </div>
      </div>
      <div class="modal-footer Cheques_pop_color">
        <button style="margin-bottom: 10px;" type="button" class="btn btn-primary btn_cls_sav" id="update">Update</button>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function(){
    $('#Cheques_table').DataTable({
      "ordering": false,
      dom: 'Bfrtip',
      buttons: [
      {
        extend: 'csv',
        footer: true,
        exportOptions: {
         columns: [ 0, 1, 2, 3, 4,5,6,7,8 ]
       }
     },
     {
      extend: 'excel',
      title:'Cheque Management',
      footer: true,
      exportOptions: {
       columns: [ 0, 1, 2, 3, 4,5,6,7,8 ]
     }
   },
   {
    extend: 'pdf',
    title:'Cheque Management',
    footer: true,
    exportOptions: {
     columns: [ 0, 1, 2, 3, 4,5,6,7,8 ]
   }
 },
 {
  extend: 'pageLength',
},
]
});
    $(document).on('click', '.status_dt', function () { 
      $("#service_charge").attr('hidden',true);
      var Cheques_table_id=$(this).closest('tr').find('td span.Cheques_table_id').text();
      var clearing_date=$(this).closest('tr').find('td.clearing_date').text();
      var status_name=$(this).closest('tr').find('td.status_name').text();
      $('#clear_status').empty();
      if(status_name=='Uncleared')
      {
        //$('#clear_status').append($("<option></option>").attr("value",4).text('Submitted')).change();
        $('#clear_status').append($("<option></option>").attr("value",1).text('Cleared')).change();
        $('#clear_status').append($("<option></option>").attr("value",2).text('Bounce'));
      }
      else if(status_name=='Submitted')
      {
        
        $('#clear_status').append($("<option></option>").attr("value",1).text('Cleared')).change();
        $('#clear_status').append($("<option></option>").attr("value",2).text('Bounce'));
      }
      if(status_name=='Cleared')
      {
// $('#clear_status').append($("<option></option>").attr("value",2).text('Bounce')).change();
}
if(status_name=='Bounce')
{
// $('#clear_status').append($("<option></option>").attr("value",1).text('Cleared')).change();
}
$('#Cheques_table_id').val(Cheques_table_id);
$('#clear_date').val(clearing_date);

var clearing_date_update=$(this).closest('tr').find('td.clearing_date').text();
var Cheques_id_update=$(this).closest('tr').find('td span.Cheques_table_id').text();
var account_head_id=$(this).closest('tr').find('td span.account_head_class').text();
var sub_group_id=$(this).closest('tr').find('td span.sub_group_class').text();
$('#cheque_entry_method').empty();
if(sub_group_id==3)
{
  $('#cheque_entry_method').append($("<option></option>").attr("value",'Receipt').text('Receipt')).change();
}
else
{
   $('#cheque_entry_method').append($("<option></option>").attr("value",'Payment').text('Payment')).change();
}
$('#clearing_date_update').val(clearing_date_update);
$('#Cheques_id_update').val(Cheques_id_update);
$('#sub_group_id_update').val(sub_group_id);
$('#account_head_id_update').val(account_head_id);

// if(sub_group_id == 3){
  var Cheques_amount=$(this).closest('tr').find('td.Cheques_amount').text();
  $('#invoice_amount').val(Cheques_amount);
//$('#customer_invoice_list_div').show();
// $.post( "<?= $this->webroot ?>Customer/get_customer_pending_invoice_list_ajax/"+account_head_id ,function( data ) {
//   $('#customer_invoice_list tbody').html('');
//   if(data.result!='Success')
//   {
//     alert(data.result);
//     return false;
//   }
//   var invoice_amount_input_field="<input type='text' name='data[Sales][amount][]' class='form-control single_amount'>"; 
//   var invoice_discount_input_field="<input type='text' name='data[Sales][discount][]' class='form-control single_discount'>"; 
//   $.each(data.sales,function(key,value){
//     var invoice_amount_value_field="<input type='text' class='form-control'  value='"+value.invoice_no+"'' readonly>\
//     <input type='text' name='data[Sales][id][]' value='"+key+"' class='invoice' hidden readonly>";
//     $('#customer_invoice_list tbody').append('<tr>\
//       <td class="invoice">'+invoice_amount_value_field+'</td>\
//       <td class="invoice_amount">'+value.balance+'</td>\
//       <td>'+invoice_amount_input_field+'</td>\
//       <td>'+invoice_discount_input_field+'</td>\
//       <td class="single_balance_amount"></td>\
//     </tr>');
//   });
$('#invoice_amount').keyup();
// }, "json");
// }else{
//   $('#customer_invoice_list_div').hide();
// }

$('#status_update_modal').modal('show');

});

    $(document).on('change', '#clear_status', function () {
      var status_cl=$('#clear_status option:selected').val();
      if (status_cl == '2')
      {
        $("#customer_invoice_list_div").hide();
        $("#customer_invoice_list_div_bounce").show();

      }
      else
      {
        $("#customer_invoice_list_div").show();
        $("#customer_invoice_list_div_bounce").hide();

      }
    });
    $('#update').click(function() { 
      var clearing_date=$('#clear_date').val();
      var status=$('#clear_status option:selected').val();
      if(status==0)
      {
        $('#clear_status').select_two_class('open'); return false;  
      }
      var customer_type =$('#chk_cus_typ').val();
      var Cheques_id    =$('#Cheques_table_id').val();
      var service_amount=$('#service_amount').val();
      var url_address   ='<?= $this->webroot; ?>'+'Accountings/Cheques_update_status_ajax';
      var data=$('#UpdateChequeChequeForm').serialize();
      $.ajax({
        type: "post",  
        url:url_address,
        data: data, 
        dataType: 'json', 
        success: function(response) {
          if(response.result!='Success') { alert(response.result);  return false;  }
          location.reload(true);
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); }
      });
    });
    $.fn.searchtype = function() {
      var ChequesCustomerType=$('#ChequesCustomerType').val();
      var ChequesCustomer=$('#ChequesCustomer').val();
      var ChequesBank=$('#ChequesBank').val();
      var ChequesDepositedBy=$('#ChequesDepositedBy').val();
      var data={ChequesCustomerType:ChequesCustomerType,ChequesCustomer:ChequesCustomer,ChequesBank:ChequesBank,ChequesDepositedBy:ChequesDepositedBy};
      var url_address= '<?= $this->webroot; ?>'+'Chequess/type_name_search_ajax';
      $.ajax({
        type: "post",  
        url:url_address,
        data: data,  
        success: function(response) {
          $('#Cheques_table tbody').html(response);

        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    };
  });
$(document).on('keyup','.single_discount',function(){
  var paid_amount=$(this).closest('tr').find('td input.single_amount').val();
  var invoice_amount=$(this).closest('tr').find('td.invoice_amount').text()

  var single_discount = $(this).val();
  if(!single_discount)
    single_discount=0;
  invoice_amount-=paid_amount;
  $(this).closest('tr').find('td.single_balance_amount').text(parseFloat(invoice_amount-single_discount).toFixed(2));
  $.fn.table_loop();
});
$(document).on('keyup','#invoice_amount',function(){
  $('#amount').text($(this).val());

  var invoice_amount = parseFloat($(this).val());
});
$.fn.table_loop=function(){
  var total_invoice_amount=0;
  var total_discount=0;
  $('#customer_invoice_list tbody tr').each(function(){
    var single_amount=$(this).closest('tr').find('td input.single_amount').val();
    var single_discount=$(this).closest('tr').find('td input.single_discount').val();
    if(isNaN(single_amount) || single_amount=='' )
    {
      single_amount=0;
    }
    if(isNaN(single_discount) || single_discount=='' )
    {
      single_discount=0;
    }
    total_invoice_amount=parseFloat(total_invoice_amount)+parseFloat(single_amount);
    total_discount=parseFloat(total_discount)+parseFloat(single_discount);

  });
  var invoice_amount=$('#invoice_amount').val();
  $('#balance_amount').val((parseFloat(invoice_amount)-parseFloat(total_invoice_amount)).toFixed());
  balance_amount=invoice_amount-total_invoice_amount;
  if($('#invoice_amount').val() >0 && parseFloat(balance_amount)==0)
  {
    $('#update').attr('disabled',false);
  }
  else
  {
    $('#update').attr('disabled',true);
  } 
  return balance_amount;
}
$(document).on('keyup','.single_amount',function(){
  var balance_amount=$('#balance_amount').val();
  var invoice_amount=$(this).closest('tr').find('td.invoice_amount').text();
  var single_amount=$(this).val();
  var balance=invoice_amount-single_amount;
  $(this).closest('tr').find('td.single_balance_amount').text(parseFloat(balance).toFixed(2));
  $('.single_discount').keyup();
  $.fn.table_loop();
});
</script>
<script type="text/javascript">
  $(document).on('click', '.cheque_delete', function(e) {
    if (!confirm("Are you sure?")) { return false; }
    var id= $(this).data('id');
    var rowindex = $(this).closest('tr').index();
    $.post("<?= $this->webroot ?>Accountings/Cheque_delete/" + id, function (data) {
      if (data.result != 'Success')
      {
        alert(data.message);
        return false;
      }
      $('#Cheques_table tbody tr:eq(' + rowindex + ')').remove();
      location.reload();
    }, "json");
  });
</script>

<script type="text/javascript">
  $('#fetch_button').on('click',function(){
    $.fn.table_search();
  });
  $.fn.table_search=function(){  
   var from_date=$('#from_date').val();
   var to_date=$('#to_date').val();
   var account_id=$('.account_head').val();
   if(account_id == ""){
    alert("Please choose any one");
    return false;
  }
  $.post("<?= $this->webroot ?>Accountings/account_head_search_ajax/" + account_id+'/'+from_date+'/'+to_date, function (responds) {
    if (responds.result != 'Success')
    {
      alert(responds.message);
    }
    $('#Cheques_table').DataTable().destroy();
    $('#Cheques_table tbody').empty();
    $.each(responds.data,function(key,cheques){
      var tr='<tr class="blue-pddng">';
      tr+='<td class="Cheques_table"><span class="Cheques_table_id" style="display:none">'+cheques.Cheque.id+'</span><span>'+cheques.AccountHead.name+'</span><span hidden class="account_head_class">'+cheques.AccountHead.id+'</span><span hidden class="sub_group_class">'+cheques.AccountHead.sub_group_id+'</span></td>';
      tr+='<td>'+cheques.BankAccountHead.name+'</td>';
      tr+='<td>'+cheques.Cheque.cheque_no+'</td>';
      tr+='<td class="Cheques_amount">'+cheques.Cheque.cheque_amount+'</td>';
      tr+='<td class="remarks">'+cheques.Cheque.remarks+'</td>';
      tr+='<td class="recieved_date">'+cheques.Cheque.recieved_date+'</td>'
      tr+='<td class="clearing_date">'+cheques.Cheque.clearing_date+'</td>';
      tr+='<td>'+cheques.Cheque.cheque_date+'</td>';
      if(cheques.Cheque.status==1) { state_color='green'; status='Cleared'; } else if(cheques.Cheque.status==2) { state_color='red'; status='Bounce'; }  else if(cheques.Cheque.status==3) { state_color='red'; status='Cancelled'; } else if(cheques.Cheque.status==4) { state_color='green'; status='Submitted'; } else { state_color='orange'; status='Uncleared'; }
      tr+='<td class="status_name"><span style="color:'+state_color+'">'+status+'</span></td>';
      if(cheques.Cheque.status == 0 || cheques.Cheque.status == 4) {
        tr+='<td class="status_dt"><i class="fa fa-plus-circle fa-1x ad-mar circle_add"> </i></td>';
        tr+='<td><a href="#" ><i class="fa fa-trash blue-col cheque_delete" data-id='+cheques.Cheque.id+'></i></a></td>';
      }
      else
      {
        tr+='<td></td>';
        tr+='<td></td>';
      } 
      tr+='</tr>';
      $('#Cheques_table tbody').append(tr);
    }
    );
    $('#Cheques_table').DataTable({
      "ordering": false,
      dom: 'Bfrtip',
      buttons: [
      { extend: 'csv',   title:'Cheque Management', footer: true,exportOptions: { columns: [ 0, 1, 2, 3, 4,5,6,7,8 ]    } },
      { extend: 'excel', title:'Cheque Management', footer: true,exportOptions: { columns: [ 0, 1, 2, 3, 4,5,6,7,8 ]  } },
      { extend: 'pdf',   title:'Cheque Management', footer: true, exportOptions: { columns: [ 0, 1, 2, 3, 4,5,6,7,8 ] }     },
      { extend: 'pageLength', },
      ]
    });
  },
  "json")
}
</script>