<section class="content-header">
  <h1>Advance Income Transaction 
    <a href="<?= $this->webroot ?>Accountings/CurrentLiabilityAdvanceIncome"><input type="button" class="btn btn-success save pull-right" value="Edit"></input></a>
  </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
      <div class="col-md-12">
        <div class="form-group">
          <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <?= $this->Form->input('category',array('type'=>'select','class'=>'form-control select2','style'=>'width: 100%;','id'=>'category','options'=>$SubGroup_list,'required','label'=>'Category',)); ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('amount',array('class'=>'form-control number','type'=>'number','step'=>'any','required','id'=>'amount',)); ?>
          </div>
          <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
          </div>
          <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12" style="display:none;">
            <?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date',)); ?>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <?= $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','id'=>'account_head','options'=>$AccountHead,'required','label'=>'Sub Category',)); ?>
          </div>
          <div class="col-md-2">
            <?= $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select2','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
            <?= $this->Form->input('mode',array('type'=>'select','class'=>'form-control select2','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher')) ?>
          </div>
          <div class="col-md-1">
            <div class="create-wrapper"><br>
              <button type="submit" id='add_button' class="user_add_btn">ADD</button>
            </div>
          </div>
        </div>
      </div>
      <?= $this->Form->end(); ?>
    </div>
    <div class="box-body table-responsive no-padding">
      <table class="table table-condensed table datatable table boder boder" id="journal_table">
        <thead>
          <tr class="blue-bg">
            <th class="padding_left">Date</th>
            <th>Category</th>
            <th>Sub Category</th>
            <th>Amount</th>
            <th>Paid</th>
          </tr>
        </thead>
        <tbody>
          <?php $credit=0; $debit=0; foreach ($All_Accounts as $key => $value): ?>
          <tr class="blue-pddng view_transaction"> 
            <td class='padding_left'><?= date('d-m-Y',strtotime($value['date'])); ?></td>
            <td><?= $value['SubGroup']; ?></td>
            <td class='name'><?= $value['name']; ?></td>
            <td class="text-right"><?= $value['debit']; $debit+=$value['debit']; ?></td>
            <td class="text-right"><?= $value['credit']; $credit+=$value['credit']; ?></td>
          </tr>
        <?php endforeach ?>
      </tbody>
      <tfoot>
        <tr class="blue-pddng">
          <td></td>
          <td></td>
          <td class="total_amount text-right">Total</td>
          <td class="total_amount text-right"><?= $debit; ?></td>
          <td class="total_amount text-right"><?= $credit; ?></td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
<?php require('general_journal_transaction_modal.php') ?>
</section>
<script type="text/javascript">
  <?php require('current_liability_advance_income_transaction.js'); ?>
</script>