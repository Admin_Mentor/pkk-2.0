 $("input[name='data[Journal][group]']").change(function(){
  var group=$(this).val();
  $.post( "<?= $this->webroot ?>Accountings/SubGroup_Option_ListByGroupId_ajax/"+group, function( data ) {
    $('#category').html('');
    if(data.result!='Success')
    {
      alert(data.result);
      return false;
    }
    $.each(data.options,function(key,value){
      $('#category').append($("<option></option>").attr("value",key).text(value));
    })
    $('#category').trigger('change');
  }, "json");
});
 $('#category').change(function(){
  var category=$(this).val();
  $.post( "<?= $this->webroot ?>Accountings/AccountHead_Option_ListBySubGroupId_ajax/"+category, function( data ) {
    $('#account_head').html('');
    $.each(data.options,function(key,value){
      $('#account_head').append($("<option></option>").attr("value",key).text(value));
    });
    $('#account_head').trigger('change');
  }, "json");
});
 $('#paid').keyup(function(){
  var outstanding=$('#outstanding').val();
  var paid=$('#paid').val();
  $('#balance').val(parseFloat(outstanding)-parseFloat(paid));
});
 $('#account_head').change(function(){
  var account_head=$(this).val();
  $.post( "<?= $this->webroot ?>Accountings/get_outstanding_balance/"+account_head, function( data ) {
    $('#outstanding').val(data);
  }, "json");
});
 $('#account_head').change();