<section class="content-header">
	<!-- <h1>Creditor Supplier Create </h1> -->
	<h1>Vendor </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary box_tp_brdr">
				<div class="row-wrapper" hidden>
					<?php echo $this->Form->create('AccountHead', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'AccountHead_Form']); ?>
					<div class="box-body">
						<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
							<div class="form-group">
								<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
									<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
									<?= $this->Form->input('name',array('class'=>'form-control name','type'=>'text','required','id'=>'name','label'=>'ACC/Name',)); ?>
								</div>
								<br>
								<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#Account_modal"></i></a></div>
							</div>
						</div>
						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
							<div class="form-group">
								<div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
									<?= $this->Form->input('opening_balance',array('class'=>'form-control opening_balance','type'=>'number','step'=>'any','required','readonly','id'=>'opening_balance',)); ?>
								</div>
							</div>
						</div>
						<div class="col-md-1"> 
							<br>
							<div class="create-wrapper">
								<button type='Submit' id='add_button' class="user_add_btn">ADD</button>
								<button type='Submit' id='edit_button' class="user_add_btn" style='display: none'>EDIT</button>
							</div>
						</div>
					</div>
					<?php require('party_modal.php') ?>
					<?= $this->Form->end(); ?>
				</div>
				<br>
				<div class="row-wrapper">
					<div class="row">
						<div class="col-md-12">
							<div class="box-body table">
								<table class="table table-condensed table table datatable boder table-bordered" id="AccountHead_table">
									<thead>
										<tr class="blue-bg">
											<th class="padding_left">Date</th>
											<th>Account's Name</th>
											<th class="text-right">Opening Balance</th>
											<th>Address</th>
											<th hidden width="10%">Action</th>
											<!-- <th></th> -->
										</tr>
									</thead>
									<tbody>
										<?php foreach ($Party as $key => $value): ?>
											<tr class="blue-pddng">
												<td><?= date('d-m-Y',strtotime($value['AccountHead']['created_at'])) ?></td>
												<td><span style="display:none" class='AccountHead_id'><?= $value['AccountHead']['id']; ?></span><span><?= $value['AccountHead']['name']; ?></span></td>
												<td class="text-right"><?= number_format($value['AccountHead']['opening_balance'],2,'.',''); ?></td>
												<td><?= $value['AccountHead']['description']; ?></td>
												<td hidden><i hiddenclass="fa  fa-2x fa-pencil-square-o blue-col edit_head"></i>
												<i class="fa fa-2x fa-trash blue-col blue-col Party_delete"></i></td>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
$('#state_id').val(19).trigger('change');
$('.name').on('keyup change',function(){
$('#name').val($(this).val());
$('#name_modal').val($(this).val());
});
$('.opening_balance').on('keyup change',function(){
$('#opening_balance').val($(this).val());
$('#opening_balance_modal').val($(this).val());
});
</script>
<!-- for edit  -->
<script type="text/javascript">
$(document).on('click','.edit_head',function(){
var id=$(this).closest('tr').find('td span.AccountHead_id').text();
$.post( "<?= $this->webroot ?>Accountings/Party_get_ajax/"+id, function( responds ) {
if(responds.result!='Success')
{
alert(responds.message);
return false;
}
$('#name').val(responds.data.AccountHead.name).trigger('change');
$('#opening_balance').val(responds.data.AccountHead.opening_balance).trigger('change');
$('#description').val(responds.data.AccountHead.description);
$("#name").append("<input type='text' id='AccountHead_id' name='data[AccountHead][id]' value='"+responds.data.AccountHead.id+"'>");
$('#place').val(responds.data.Party.place);
$('#code').val(responds.data.Party.code);
$('#email').val(responds.data.Party.email);
$('#mobile').val(responds.data.Party.mobile);
$('#vat_no').val(responds.data.Party.vat_no);

// $('#gstin').val(responds.data.Party.gstin);
$('#state_id').val(responds.data.Party.state_id).trigger('change');
$("#name_modal").append("<input type='text' id='Party_id' name='data[Party][id]' value='"+responds.data.Party.id+"'>");
$('#add_button').css('display','none');
$('#edit_button').css('display','');
$('#AccountHead_Form').attr('action','<?= $this->webroot; ?>Accountings/Party_account_edit');
}, "json");
});
</script>
<!-- For Party_delete -->
<script type="text/javascript">
$(document).on('click','.Party_delete',function(){
if(!confirm("Are you sure?"))
{
return false;
}
var id=$(this).closest('tr').find('td span.AccountHead_id').text();
var rowindex = $(this).closest('tr').index();
$.post( "<?= $this->webroot ?>Accountings/Party_delete/"+id,function( data ) {
if(data.result!='Success')
{
alert(data.message);
return false;
}
$('#AccountHead_table tbody tr:eq(' + rowindex + ')').remove();
window.location.reload();
}, "json");
});
</script>
<script type="text/javascript">
<?php require('state.js'); ?>
</script>