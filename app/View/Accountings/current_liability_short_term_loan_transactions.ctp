<section class="content-header">
	<h1>Short Term Loan Transaction 
		<a href="<?= $this->webroot ?>Accountings/CurrentLiabilityShortTermLoan"><input type="button" class="btn btn-success save pull-right" value="Create"></input></a>
	</h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
						<?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','id'=>'account_head', 'empty'=>'All','options'=>$AccountHead,'required','label'=>'Acc/Name',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('type',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','options'=>array('1'=>'Paid','2'=>'Received'),'required','label'=>'Received/Paid',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12" style="display:none;">
						<?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher')) ?>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('amount',array('class'=>'form-control number text-right','type'=>'text','required','id'=>'amount',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('balance',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'balance',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select2','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" >
						<?= $this->Form->input('mode',array('type'=>'select','class'=>'form-control select2','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
					</div>
					<div class="col-md-1"><br>
						<button class="user_add_btn">ADD</button>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body table">
			<table class="boder table table-condensed table  boder" id="myTable">
				<thead>
					<tr class="blue-bg">
						<th class="padding_left">Date</th>
						<th>Acc Name</th>
						<th>Received</th>
						<th>Debit Paid</th>
						<th>Balance</th>
					</tr>
				</thead>
				<tbody>
					
			</tbody>
			<tfoot>
				<tr>
						<th colspan="2" style="font-size:20px; color:red;text-align:right">Total:</th>
						<th class="text-right" style="font-size:20px; color:red;">0</th>
						<th class="text-right" style="font-size:20px; color:red;">0</th>
						<th class="text-right" style="font-size:20px; color:red;">0</th>
					</tr>
			</tfoot>
		</table>
	</div>
</div>
</section>
<?php require('general_journal_transaction_modal.php') ?>
<script type="text/javascript">
	<?php require('long_term_liability_transactions.js'); ?>
</script>
<script type="text/javascript">
	$('#myTable').DataTable( {
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Accountings/CurrentLiabilityShortTermLoanTransactions_table_ajax",
			"type": "POST",data:function( d ) {
				//d.customer_type_id= $('#customer_type').val();
				d.account_head_id= $('#account_head').val();
			},
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "AccountHead.date" },
		{ "data" : "AccountHead.name" },
		{ "data" : "AccountHead.received" },
		{ "data" : "AccountHead.paid" },
		{ "data" : "AccountHead.balance" },
		],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};
			total = api.column( 2, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 2 ).footer() ).html(''+parseFloat(total.toFixed(3))+'');
			total = api.column( 3, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 3 ).footer() ).html(''+parseFloat(total.toFixed(3))+'');
			total = api.column( 4, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 4 ).footer() ).html(''+parseFloat(total.toFixed(3))+'');
		},
		"columnDefs": [
		{ className: "customer_flag", "targets": [ 0 ],"width": "30%",},
		{ className: "name", "targets": [ 1 ],"width": "30%", },
		{"targets":[2 ],className:"dt-body-right"},
		{"targets":[3 ],className:"dt-body-right"},
		{"targets":[4 ],className:"dt-body-right"},
		],
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$(nRow).addClass('view_transaction');
			return nRow;
		}
	});
	$('#account_head').change(function(){
		//alert('k');
		table = $('#myTable').dataTable();
		table.fnDraw();
	});
</script>