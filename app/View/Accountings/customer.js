$('#customer_add_button').click(function(){
	var data=$('#Customer_Form').serialize();
	$.post( "<?= $this->webroot ?>Customer/add_ajax",data ,function( data ) {
		if(data.result!='Success')
		{
			alert(data.result);
			return false;
		}
		$('#Customer_Form')[0].reset();
		$('#division_id').trigger('change');
		$('#modal_customer_type_id').trigger('change');
		$('#customer_group_id').trigger('change');
		$('#route_id').val('').trigger('change');
		$('#name').append($("<option></option>").attr("value",data.key).text(data.value));
		$('#customer_modal_add').modal('toggle');
		$('#name').val(data.key);
		$('#name').trigger('change');
		$('#customer_discount_table tbody').empty();
		$.fn.table_search();
	}, "json");
});
$('#route_id,#route_id_edit').change(function(){
	var id=$(this).val();
	$.get( "<?= $this->webroot ?>Executive/get_executive_credit_limit/"+id,function( data ) {
		$('#credit_limit_balance').val(data.credit_limit_balance);
		$('#credit_limit_balance_edit').val(data.credit_limit_balance);
	}, "json");
});
$(document).on('keyup','#credit_limit_modal,#credit_limit_edit',function(){
 $('#creditlimit_error').html('');
 $('#creditlimit_error_edit').html('');
});
$('#route_id,#route_id_edit').change(function(){
	var id=$(this).val();
	$.get( "<?= $this->webroot ?>Executive/get_executive_credit_limit/"+id,function( data ) {
		$('#credit_limit_balance').val(data.credit_limit_balance);
		$('#credit_limit_balance_edit').val(data.credit_limit_balance);
	}, "json");
});
$('#save_continue').click(function(){
	var route_id=$('#route_id').val();
	var group=$('#customer_group_id').val();
	var customer_type=$('#modal_customer_type_id').val();
	var name=$('#name_modal').val();
	var credit_limit_modal=$('#credit_limit_modal').val();
	var credit_limit_balance=$('#credit_limit_balance').val();
	if(!customer_type)
	{
		$('#modal_customer_type_id').select2('open');
		return false;
	}
    if(!name)
	{
		$('#name_modal').focus();
		return false;
	}
	$('#creditlimit_error').html('');
	if(parseFloat(credit_limit_modal)>parseFloat(credit_limit_balance))
	{
		$('#creditlimit_error').html('Executive Credit Limit Exceeded.Balance is '+credit_limit_balance);
		$('#credit_limit_modal').focus();
		return false;
	}
	if(!route_id)
	{
		$('#route_id').select2('open');
		return false;
	}if(!group)
	{
		$('#customer_group_id').select2('open');
		return false;
	}
});
$('#save_continue_edit').click(function(){
	var route_id=$('#route_id_edit').val();
	var group=$('#customer_group_edit').val();
	var customer_type=$('#edit_customer_type_id').val();
	var name=$('#name_edit').val();
	var credit_limit_modal=$('#credit_limit_edit').val();
    var credit_limit_hidden=$('#credit_limit_hidden').val();
	var credit_limit_balance=$('#credit_limit_balance_edit').val();
	if(!customer_type)
	{
		$('#edit_customer_type_id').select2('open');
		return false;
	}
    if(!name)
	{
		$('#name_edit').focus();
		return false;
	}
	if(parseFloat(credit_limit_modal)>parseFloat(credit_limit_hidden))
	if(parseFloat(credit_limit_modal)>parseFloat(credit_limit_balance))
	{
		$('#creditlimit_error_edit').html('Executive Credit Limit Exceeded.Balance is '+credit_limit_balance);
		$('#credit_limit_edit').focus();
		return false;
	}
	if(!route_id)
	{
		$('#route_id_edit').select2('open');
		return false;
	}if(!group)
	{
		$('#customer_group_edit').select2('open');
		return false;
	}
});
function ValidateEmail() {
        var email = document.getElementById("email").value;
        var lblError = document.getElementById("lblError");
        lblError.innerHTML = "";
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (!expr.test(email)) {
            lblError.innerHTML = "Invalid email address.";
        }
    }
// $.fn.table_search=function(){
// 	var executive_id=$('#executive_view_id option:selected').val();
// 	var route_id=$('#route_view_id option:selected').val();
// 	var group=$('#group_view option:selected').val();
// 	var customer_type=$('#customer_type option:selected').val();
// 	var account_head_id=$('#name').val();
// 	$('#AccountHead_table tfoot').html('');
// 	var data={
// 		route_id:route_id,
// 		group:group,
// 		executive_id:executive_id,
// 		customer_type:customer_type,
// 		account_head_id:account_head_id,
// 	};
// 	var url_address= '<?php echo $this->webroot; ?>'+'Accountings/customer_search_ajax';
// 	$.ajax({
// 		type: "post",
// 		url:url_address,
// 		data: data,
// 		dataType:'json',
// 		success: function(response) {
// 			console.log(response);
// 			$('#AccountHead_table').DataTable().destroy();
// 			$('#AccountHead_table tbody').html(response.row);
// 			$('#AccountHead_table tfoot').html(response.foot);
// 			$('#AccountHead_table').DataTable();
// 		},
// 		error:function (XMLHttpRequest, textStatus, errorThrown) {
// 			alert(textStatus);
// 		}
// 	});
// }
$.fn.table_search=function(){
	table = $('#AccountHead_table').dataTable();
	table.fnDraw();
}
$('#add_state_button').click(function(){
	var name=$('#state_name_modal').val();
	if(name=='')
	{
		$('#state_name_modal').focus();
		return false;
	}
	var code=$('#state_code_modal').val();
	if(code=='')
	{
		$('#state_code_modal').focus();
		return false;
	}
	var website_url='<?php echo $this->webroot; ?>Customer/add_state_ajax';
	var data={
		name:name,
		code:code,
	};
	$.ajax({
		method: "POST",
		url: website_url,
		data: data,
		dataType:'json',
	}).done(function( data ) {
		if(data.result!='Success')
		{
			alert(data.result);
			return false;
		}
		$('#state_add_modal').modal('toggle');
		$('#state_name_modal').val('');
		$('#state_code_modal').val('');
		$('#state_id').append($("<option></option>").attr("value",data.key).text(data.value));
		$('#state_id').val(data.key).trigger('change');
	});
});
$('#add_route_button').click(function(){
	var name=$('#route_name_modal').val();
	var code=$('#route_code_modal').val();
	
	if(name=='')
	{
		$('#route_name_modal').focus();
		return false;
	}
	if(code=='')
	{
		$('#route_code_modal').focus();
		return false;
	}
	var website_url='<?php echo $this->webroot; ?>Customer/add_route_ajax';
	var data={
		name:name,
			code:code,
		
	};
	$.ajax({
		method: "POST",
		url: website_url,
		data: data,
		dataType:'json',
	}).done(function( data ) {
		if(data.result!='Success')
		{
			alert(data.result);
			return false;
		}
		$('#route_add_modal').modal('toggle');
		$('#route_name_modal').val('');
		$('#route_code_modal').val('');
		$('.routes').append($("<option></option>").attr("value",data.key).text(data.value));
		$('.routes').val(data.key).trigger('change');
	});
});
$('#state_id').change(function(){
	var id=$(this).val();
	$.get( "<?= $this->webroot ?>Customer/get_state_code_ajax/"+id,function( data ) {
		$('#state_code').val(data.code);
	}, "json");
});
$('#state_id').trigger('change');

// $('#add_group_button').click(function(){
// 	var name=$('#group_name_modal').val();

// 	if(name=='')
// 	{
// 		$('#group_name_modal').focus();
// 		return false;
// 	}
// 	var website_url='<?php echo $this->webroot; ?>Customer/add_customer_group_ajax';
// 	var data={
// 		name:name,

// 	};
// 	$.ajax({
// 		method: "POST",
// 		url: website_url,
// 		data: data,
// 		dataType:'json',
// 	}).done(function( data ) {
// 		if(data.result!='Success')
// 		{
// 			alert(data.result);
// 			return false;
// 		}
// 		$('#group_add_modal').modal('toggle');
// 		$('#group_name_modal').val('');

// 		$('.customergroup').append($("<option></option>").attr("value",data.key).text(data.value));
// 		$('.customergroup').val(data.key).trigger('change');
// 	});
// });
//route functions
// $('#add_route_button').click(function(){
// 	$(this).attr('disabled',true);
// 	var name=$('#route_name_modal').val();
// 	var code=$('#route_code_modal').val();

// 	if(name=='')
// 	{
// 		$('#route_name_modal').focus();
// 		return false;
// 	}
// 	var website_url='<?php echo $this->webroot; ?>Customer/add_route_ajax';
// 	var data={
// 		name:name,
// 		code:code,
// 	};
// 	$.ajax({
// 		method: "POST",
// 		url: website_url,
// 		data: data,
// 		dataType:'json',
// 	}).done(function( data ) {
// 		if(data.result!='Success')
// 		{
// 			alert(data.result);
// 			return false;
// 		}
// 		$('#route_add_modal').modal('toggle');
// 		$('#route_name_modal').val('');
// 		$('#route_code_modal').val('');

// 		$('.routes').append($("<option></option>").attr("value",data.key).text(data.value));
// 		$('.routes').val(data.key).trigger('change');
// 	});
// });

$('#add_group_button').click(function(){
	var name=$('#group_name_modal').val();
	
	if(name=='')
	{
		$('#group_name_modal').focus();
		return false;
	}
	var website_url='<?php echo $this->webroot; ?>Customer/add_customer_group_ajax';
	var data={
		name:name,
		
	};
	$.ajax({
		method: "POST",
		url: website_url,
		data: data,
		dataType:'json',
	}).done(function( data ) {
		if(data.result!='Success')
		{
			alert(data.result);
			return false;
		}
		$('#group_add_modal').modal('toggle');
		$('#group_name_modal').val('');
		
		$('.customergroup').append($("<option></option>").attr("value",data.key).text(data.value));
		$('.customergroup').val(data.key).trigger('change');
	});
});
$('#route_code_modal').keyup(function(){
	var modal_location_code=$(this).val();
	var modal_location_name=$('#route_name_modal').val();
	$('#add_route_button').attr('disabled',true);
	var data={
		code:modal_location_code,


	};
	var url_address= '<?php echo $this->webroot; ?>'+'Route/route_search';
	$.ajax({
		type: "post",
		url:url_address,
		data: data,
		success: function(response) {
			if(response=="Yes")
			{
				$('#location_code_error').html('This Code is already taken');
				$('#add_route_button').attr('disabled',true);
			}
			else
			{
				$('#location_code_error').html('');
				$('#add_route_button').attr('disabled',false);
			}
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});

});
$('#route_name_modal').keyup(function(){
	var modal_location_name=$(this).val();
	var modal_location_code=$('#route_code_modal').val();
	$('#add_route_button').attr('disabled',true);
	var data={
		location_name:modal_location_name,
	};
	var url_address= '<?php echo $this->webroot; ?>'+'Executive/route_search';
	$.ajax({
		type: "post",
		url:url_address,
		data: data,
		success: function(response) {
			if(response=="Yes")
			{
				$('#location_error').html('This Location is already taken');
				$('#add_route_button').attr('disabled',true);
			}
			else
			{
				$('#location_error').html('');
				$('#add_route_button').attr('disabled',false);
			}
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
});
$('#add_discount').click(function(){
	var product_id=$('#product_id').val();
	var product_text=$('#product_id option:selected').text();
	var discount=$('#additional_discount').val();
	var ProductExist=0;
	if(!product_id){
		$('#product_id').select2("open");
		return false;
	}
	if(!discount){
		$('#additional_discount').focus();
		return false;
	}
	$("#customer_discount_table tbody tr").each(function () {
  		var productId = $(this).closest('tr').find('td input.productsrow').val();
  		if (product_id== productId) {
      		ProductExist = 1;
      		return false;
  		}
	});
	if(ProductExist){
		alert("product Already exist");
		return false;
	}
	$('#customer_discount_table tbody').append('<tr>\
		<td> <input class="productlist productsrow" hidden name="data[Customer][products][]" value="'+product_id+'">\
  		<input class="form-control" value="'+product_text+'" readonly>\
  		</td>\
  		<td><input class="productlist productsrow form-control" name="data[Customer][selling_rates][]" value="'+discount+'"></td>\
  		<td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
	</tr>');
	$('#product_id').val('').trigger('change.select2');
	$('#additional_discount').val("");
	$('#unit').val("");
});
$('#edit_discount').click(function(){
	var product_id=$('#product_edit').val();
	var product_text=$('#product_edit option:selected').text();
	var discount=$('#additional_discount_edit').val();
	var ProductExist=0;
	if(!product_id){
		$('#product_edit').select2("open");
		return false;
	}
	if(!discount){
		$('#additional_discount_edit').focus();
		return false;
	}
	$("#customer_discount_Edit tbody tr").each(function () {
  		var productId = $(this).closest('tr').find('td input.productsrow').val();
  		if (product_id== productId) {
      		ProductExist = 1;
      		return false;
  		}
	});
	if(ProductExist){
		alert("product Already exist");
		return false;
	}
	$('#customer_discount_Edit tbody').append('<tr>\
		<td>\
		<input class="productlist productsrow" hidden name="data[CustomerEdit][products][]" value="'+product_id+'">\
  		<input class="form-control" value="'+product_text+'" readonly>\
  		</td>\
  		<td><input class="productlist productsrow form-control" name="data[CustomerEdit][selling_rates][]" value="'+discount+'"></td>\
  		<td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
	</tr>');
	$('#product_edit').val('').trigger('change.select2');
	$('#unit_edit').val("");
	$('#additional_discount_edit').val("");
});
$(document).on('click','.remove_tr',function(){
  $(this).closest('tr').remove();
});
$(document).on('click','.remove_old_tr',function(){
var row_index=$(this).parents('tr').index();
var id=$(this).closest('tr').find('td input.customer_discount_id').val();
var url_address= '<?php echo $this->webroot; ?>'+'Customer/CustomerDiscount_delete/'+id;
$.ajax({
  type: "POST",
  url:url_address,
  dataType:'json',
  success: function(data) {
    if(data.result!='Success')
    {
      alert(data.result);
      return false;
    }
    $('#customer_discount_Edit tbody tr:eq('+row_index+')').remove();
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
// $(this).closest('tr').remove();
$.fn.button_disable();
});
$(document).on('change','.product_id',function(){
	var product_name=$(this).val();
	var data={product_name:product_name};
	var url_address= '<?php echo $this->webroot; ?>'+'Customer/get_product_unit';
	$.ajax({
		type: "post",
		url:url_address,
		dataType:"json",
		data: data,
		success: function(response) {
			$('.unit_id').val(response.unit_name);
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
});
});
$(document).on('keypress','#credit_limit_modal,#credit_period_modal,#opening_balance_modal',function(e){
     if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57 || e.which==47)) {
       return false;
     }
   });
$(document).on('keypress','#name_edit,#name_modal',function(e){
     if (e.which != 8 && e.which != 0 && (e.which < 46 || 98 > e.which > 122 || e.which==47)) {
       return false;
     }
   });