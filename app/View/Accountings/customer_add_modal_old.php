<div id="customer_modal_add" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Customer Details</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('Customer', ['class' => 'form-horizontal', 'id' => 'Customer_Form']); ?>
                <div class="form-group" style="display:none;">
                    <label class="col-sm-4 control-label">Executive</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('executive_id', array('class' => 'form-control select2', 'type' => 'select', 'required', 'style' => 'width:100%', 'options' => '', 'id' => 'executive_id', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Customer Type</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('customer_type_id', array('class' => 'form-control select2', 'type' => 'select', 'required', 'style' => 'width:100%', 'options' => $modalCustomerType_list, 'id' => 'modal_customer_type_id', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Customer Name</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('name', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'name_modal', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Customer Name In Arabic</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('arabic_name', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'name_modal', 'label' => false,'dir'=>'rtl')); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Division</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('division_id', array('class' => 'form-control select2 division', 'type' => 'select', 'style' => 'width:100%','empty'=>array(''=>'Select') ,'options' => $Division_list, 'id' => 'division_id', 'label' => false,)); ?>
                    </div>
                    <div class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#adddivision"></i></div>
                </div>
             
                <div class="form-group">
                    <label class="col-sm-4 control-label">Description</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('description', array('class' => 'form-control', 'type' => 'textarea', 'step' => 'any', 'rows' => 2, 'id' => 'description', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Opening Balance</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('opening_balance', array('class' => 'form-control opening_balance', 'type' => 'number', 'step' => 'any', 'required', 'id' => 'opening_balance_modal', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Credit Period</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('credit_period',array('class'=>'form-control credit_period','type'=>'number','step'=>'any','required','id'=>'credit_period','value'=>0,'label'=>false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Credit Limit</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('credit_limit',array('class'=>'form-control credit_limit','type'=>'number','step'=>'any','required','id'=>'credit_limit_edit','value'=>0,'label'=>false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Route</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('route_id', array('class' => 'form-control select2 routes', 'type' => 'select', 'style' => 'width:100%','empty'=>'Select', 'options' => $Route_list, 'id' => 'route_id', 'label' => false,)); ?>
                    </div>
                    <div style="" class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#route_add_modal"></i></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Group</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('customer_group_id', array('class' => 'form-control select2 customergroup', 'type' => 'select', 'options' => $CustomerGroup_list, 'style' => 'width:100%', 'id' => 'customer_group_id', 'label' => false,)); ?>
                    </div>
                    <div class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#group_add_modal"></i></div>

                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Address</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('place', array('class' => 'form-control', 'type' => 'text', 'id' => 'place', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group" style="display:none;">
                    <label class="col-sm-4 control-label">Customer Code</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('code', array('class' => 'form-control', 'type' => 'text', 'id' => 'code', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group" style="display: none;">
                    <label class="col-sm-4 control-label">State</label>
                    <div class="col-sm-5">
                        <?= $this->Form->input('state_id', array('class' => 'form-control select2', 'type' => 'select', 'style' => 'width:100%', 'options' => $State_list, 'id' => 'state_id', 'label' => false,)); ?>
                    </div>
                    <div class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#state_add_modal"></i></div>
                </div>
                <div class="form-group" style="display:none;">
                    <label class="col-sm-4 control-label">State Code</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('state_code', array('class' => 'form-control', 'type' => 'text', 'readonly', 'id' => 'state_code', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Email</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('email', array('class' => 'form-control', 'type' => 'text', 'id' => 'email', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Mobile No</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('mobile', array('class' => 'form-control', 'type' => 'text', 'id' => 'mobile', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Contact Person</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('contact_person', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'contact_person_modal', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group" style="">
                    <label class="col-sm-4 control-label">VAT No</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('vat_no', array('class' => 'form-control', 'type' => 'text', 'id' => 'vat_no', 'label' => false,)); ?>
                    </div>
                </div>
                <?= $this->Form->end(); ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn_radious" id='customer_add_button'>Save</button>
            </div>
        </div>
    </div>
</div>

