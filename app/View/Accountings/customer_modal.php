<style type="text/css">

.add_btn_btm a {
    background-color: #9e0c0c;
    color: #fff !important;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Print_Buttn {
    margin-bottom: 5px;
    margin-top: 20px;
}
.Print_Buttn a {
    background-color: #13689e;
    color: #fff;
    letter-spacing: 0.6px;
    text-transform: capitalize;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
}
.Table_Border {
    border: 1px solid #bbb;
}
.Print_Buttn a:hover{
    transition: ease all 0.9s;
    background-color: #00416b;
}
.hr_btm_0px{
    margin-bottom: 0px !important
}
.Rfq_Btn{
    margin-bottom: 5px;

}
.del_color {
    color: #005082;
}
.btn_btm_modal_spc {
    margin-bottom: 3px;
    margin-top: 31% !important;
}
.Rfq_Btn a {
    background-color: #9e0c0c;
    color: #fff !important;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Rfq_Btn a:hover{
    transition: ease all 0.9s;
    background-color: #5d0505;
}
.no_padding{
    padding-left: 0px;
}
.Quote_btn a {
    background-color: #0b8686;
    color: #fff;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Quote_btn{
    margin-bottom: 5px;
    margin-top: 33px;
}
.btn_modal_in_save {
    border: none;
    background-color: #ab4d0a;
    color: white;
    letter-spacing: 0.6px;
    border-radius: 3px !important;
}

.col_md_1_cstm{
    width: 10.666667% !important;
}

.col_md_1_cstm_2 {
    width: 12.666667% !important;
}

.Sav_btn a {
    background-color: #10631e;
    color: #fff !important;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Get_btn{
    margin-left: 16px;
}
.Get_btn a{
    background-color: #9e134d;
    color: #fff !important;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Sav_btn a:hover{
    transition: ease all 0.9s;
    background-color: #04350c;
}
.col_md_off {
    margin-left: 25px;
}
.Total_color {
    font-weight: 600;
    color: #7d1010;
}

.chk_wrnty{
    margin-top: 32px !important;
}
.warranty_prnt{
    margin-top: 31px;

}
.send_btn_modal {
    margin-top: 32px;
    margin-left: 13px;
}
label{
    white-space: nowrap !important;
}
.det_btn {
    margin-top: 3px;
    margin-bottom: 3px;
}
.Add_Service a{
    background-color: #980303;
    color: #fff !important;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Add_Service {
    margin-top: 32px;
}
.compl_mdl_btn {
    margin-top: 36px;
}
.sav_in_modal{
    margin-top: 32px;
}
.label_Radio_font {
    color: #5f5f5f;
    font-weight: 600;
}
/*    a {
color: #484848 !important;
}*/
.nav-tabs {
    border-bottom: 1px solid #ddd0 !important;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #fff !important;
    cursor: default;
    background-color: #0b867a !important;
    border: 1px solid #616161 !important;

    border-radius: 3px !important;
}
.nav_nav_border_all {
    border: 1px solid #dcdcdc !important;
    padding: 11px;
    border-top-left-radius: 10px !important;
}
.bt_suc {
    margin-left: 10px;
    margin-top: 20px;
    margin-bottom: 20px;
    border-radius: 3px !important;
}

.add_btn_sucess {
    margin-top: 25px;
    border-radius: 3px !important;
}
</style>
</head>
<div id="Account_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Customer</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('CustomerEdit', [ 'id' => 'CustomerEdit_Form']); ?>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <ul class="nav nav-tabs some">
                            <li class="active customer_detials"><a data-toggle="tab" href="#home1">Customer Details</a></li>
                            <li class="price_details"><a data-toggle="tab" href="#menu2">Price Details</a></li>
                        </ul>
                    </div>
                </div>


                <hr>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">

                        <div class="tab-content">
                            <div id="home1" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-3 col-lg-3 col-sm-3 no-padding">
                                            <div class="form-group">
                                                <label>Customer Type</label>
                                                <?= $this->Form->input('customer_type_edit', array('class' => 'form-control select2', 'type' => 'select', 'required', 'style' => 'width:100%', 'options' => $modalCustomerType_list, 'id' => 'edit_customer_type_id', 'label' => false,'empty'=>'SELECT')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Customer Name</label>
                                                <?= $this->Form->input('name', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'name_edit', 'label' => false,)); ?>
                                                <?= $this->Form->input('AccountHead_id', array('class' => 'form-control', 'type' => 'hidden',  'id' => 'AccountHead_id', 'label' => false,)); ?>
                                                 <?= $this->Form->input('customer_id_hide', array('class' => 'form-control', 'type' => 'hidden',  'id' => 'customer_id_hide', 'label' => false,)); ?>

                                            </div>
                                        </div>
                                        <div class="col-md-3 col-lg-3 col-sm-3">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">State</label>
                                        <?= $this->Form->input('edit_state_id',array('class'=>'form-control select2','type'=>'select','style'=>'width:100%','options'=>$State_list,'id'=>'edit_state_id','label'=>false,)); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4" hidden>
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Customer Name In Arabic</label>
                                                <?= $this->Form->input('arabic_name', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'name_edit_arabi', 'label' => false,'dir'=>'rtl')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-4 col-lg-4 col-sm-4 no-padding">
                                            <div class="col-md-6 col-lg-6 col-sm-6 no-padding">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Description</label>
                                                    <?= $this->Form->input('description', array('class' => 'form-control', 'type' => 'textarea', 'step' => 'any', 'rows' => 2, 'id' => 'description_edit', 'label' => false,)); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-sm-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Opening Balance</label>
                                                    <?= $this->Form->input('opening_balance', array('class' => 'form-control opening_balance_edit', 'type' => 'number', 'step' => 'any', 'required', 'id' => 'opening_balance_edit', 'label' => false,)); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Credit Limit</label>
                                                <?= $this->Form->input('credit_limit_balance', array('class' => 'form-control credit_limit_balance_edit', 'type' => 'hidden', 'step' => 'any', 'required', 'id' => 'credit_limit_balance_edit', 'value' => 0, 'label' => false,)); ?>
                                                <?= $this->Form->input('credit_limit', array('class' => 'form-control credit_limit_edit', 'type' => 'number', 'step' => 'any', 'required', 'id' => 'credit_limit_edit', 'value' => 0, 'label' => false,)); ?>
                                          <?= $this->Form->input('credit_limit_hidden', array('class' => 'form-control credit_limit_hidden', 'type' => 'hidden', 'step' => 'any', 'required', 'id' => 'credit_limit_hidden', 'value' => 0, 'label' => false,)); ?>

                                                <span id="creditlimit_error_edit" style="color:#db1802" class="help-inline"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Credit Period</label>

                                                    <?= $this->Form->input('credit_period_edit',array('class'=>'form-control credit_period','type'=>'number','step'=>'any','required','id'=>'credit_period_edit','value'=>0,'label'=>false,)); ?>


                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-4 col-lg-4 col-sm-4 no-padding">
                                            <div class="col-md-10 col-sm-10 col-lg-10 no-padding">
                                                <label class="col-sm-4 control-label">Route</label>
                                                <?= $this->Form->input('route_id', array('class' => 'form-control select2 routes', 'type' => 'select', 'style' => 'width:100%', 'options' => $Route_list, 'id' => 'route_id_edit', 'label' => false,'empty'=>'SELECT')); ?>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-lg-2">
                                                <i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#route_add_modal" style="margin-top: 30px;"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                              <div class="col-md-10 col-sm-10 col-lg-10 no-padding">
                                                <label class="col-sm-4 control-label">Group</label>
                                                <?= $this->Form->input('customer_group_edit', array('class' => 'form-control select2 customergroup', 'type' => 'select', 'options' => $CustomerGroup_list, 'style' => 'width:100%', 'id' => 'customer_group_edit', 'label' => false,'empty'=>'SELECT')); ?>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-lg-2">
                                            <i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#group_add_modal"style="margin-top: 30px;"></i>
                                        </div>
                                        </div>
                                       
                                        
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Category</label>
                                                <?= $this->Form->input('division_edit', array('class' => 'form-control select2 division', 'type' => 'select', 'style' => 'width:100%', 'options' => $Division_list, 'id' => 'division_edit', 'label' => false,)); ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-4 col-lg-4 col-sm-4 no-padding">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Customer Code</label>
                                                <?= $this->Form->input('code', array('class' => 'form-control', 'type' => 'text', 'id' => 'code_edit', 'label' => false,)); ?>

                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4" hidden>
                                           
                                                <label class="col-sm-4 control-label">Collection Discount</label>
                                                <?= $this->Form->input('collection_edit', array('class' => 'form-control', 'type' => 'text', 'style' => 'width:100%','id' => 'collection_edit', 'label' => false,)); ?>
                                            
                                      
                                        </div>
                                       
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Email</label>
                                                <?= $this->Form->input('email', array('class' => 'form-control', 'type' => 'text', 'id' => 'email_edit', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                         <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Address</label>
                                                <?= $this->Form->input('place_edit', array('class' => 'form-control', 'type' => 'text', 'id' => 'place_edit', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-4 col-sm-4 col-lg-4 no-padding">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Mobile No</label>
                                                <?= $this->Form->input('mobile', array('class' => 'form-control number','maxlength'=>12, 'type' => 'text', 'id' => 'mobile_edit', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Contact Person</label>
                                                <?= $this->Form->input('contact_person', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'contact_person_edit', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-lg-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">GST No</label>
                                                
                                                    <?= $this->Form->input('vat_no_edit', array('class' => 'form-control', 'type' => 'text', 'id' => 'vat_no_edit', 'label' => false,'maxlength'=>15)); ?>
                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row" hidden>
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="pull-right">
                                            <button class="btn btn-danger btn_btm_modal_spc save_continue" data-toggle="tab" type="button"  href="#menu2" style="border-radius: 3px !important; margin-top: 20px;" id="save_continue_edit">Update & Continue</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-6 col-lg-6 col-sm-6">
                                             <div class="col-md-8 col-lg-8 col-sm-8">
                                            <div class="form-group">
                                                <label>Product</label>
                                                <?= $this->Form->input('product_id', array('class' => 'form-control select_two_class select_hide brand_id product_id', 'type' => 'select', 'required', 'style' => 'width:100%', 'options' => $Product_list, 'id' => 'product_edit', 'label' => false,'empty'=>'Select')); ?>
                                            </div>
                                        </div>
                                         <div class="col-md-4 col-lg-4 col-sm-4">
                                            <label>Unit</label>
                                               <?= $this->Form->input('unit', array('class' => 'form-control unit_id', 'type' => 'text', 'required', 'style' => 'width:100%','id' => 'unit_edit', 'label' => false,'readonly')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6">
                                            <div class="col-md-8 col-lg-8 col-sm-8">
                                                <div class="form-group">
                                                    <label>Selling Rate</label>
                                                    <?= $this->Form->input('additional_rate', array('class' => 'form-control', 'type' => 'text', 'required', 'style' => 'width:100%', 'id' => 'additional_discount_edit', 'label' => false,)); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4 col-sm-4">
                                                <button class="btn btn-success add_btn_sucess" type="button" id="edit_discount">Add</button>
                                            </div>
                                        </div>



                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="box-body table-responsive no-padding">
                                            <table class="table Table_Border" style="margin-top: 2%;" id="customer_discount_Edit">
                                                <thead>
                                                     <tr class="blue-bg">
                                                        <th>Product</th>
                                                        <th>Selling Rate</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                                <tfoot></tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" hidden>
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="pull-right">
                                            <button class="btn btn-danger" style="border-radius: 3px !important; margin-top: 20px;" type="button" id="customer_update_button" >Save & Finish</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end(); ?>

            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
            </div>
        </div>

    </div>
</div>