<div id="Account_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Customer Details</h4>
			</div>
			<div class="modal-body">
				<?php echo $this->Form->create('CustomerEdit', ['class'=>'form-horizontal','id'=>'CustomerEdit']); ?>
				<div class="form-group" style="display: none;">
					<label class="col-sm-4 control-label">Executive Type</label>
					<div class="col-sm-6">
						<?= $this->Form->input('executive_id_edit',array('class'=>'form-control select2','type'=>'select','style'=>'width:100%','options'=>'','id'=>'executive_id_edit','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Customer Type</label>
					<div class="col-sm-6">
						<?= $this->Form->input('customer_type_edit',array('class'=>'form-control select2','type'=>'select','required','style'=>'width:100%','options'=>$CustomerType_list,'id'=>'customer_type_edit','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Customer Name</label>
					<div class="col-sm-6">
						<?= $this->Form->input('name_edit',array('class'=>'form-control name','type'=>'text','required','id'=>'name_edit','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
                    <label class="col-sm-4 control-label">Customer Name In Arabic</label>
                    <div class="col-sm-6">
                    	<?= $this->Form->input('arabic_name_edit',array('class'=>'form-control name','type'=>'text','required','id'=>'arabic_name_edit','label'=>false,)); ?>
                    </div>
                </div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Customer Code</label>
					<div class="col-sm-6">
						<?= $this->Form->input('code_edit',array('class'=>'form-control','readonly'=>'readonly','type'=>'text','id'=>'code_edit','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
                    <label class="col-sm-4 control-label">Division</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('division_edit', array('class' => 'form-control select2 division', 'type' => 'select', 'style' => 'width:100%', 'options' => $Division_list, 'id' => 'division_edit', 'label' => false,)); ?>
                    </div>
                    <div class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#Division_Add_modal"></i></div>
                </div>
				
				<div class="form-group">
					<label class="col-sm-4 control-label">Description</label>
					<div class="col-sm-6">
						<?= $this->Form->input('description_edit',array('class'=>'form-control','type'=>'textarea','step'=>'any','rows'=>2,'id'=>'description_edit','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group" style="display: none">
					<label class="col-sm-4 control-label"></label>
					<div class="col-sm-6">
						<?= $this->Form->input('customer_id_hide',array('class'=>'form-control','type'=>'hidden','step'=>'any','rows'=>2,'id'=>'customer_id_hide','label'=>false,)); ?>
						<?= $this->Form->input('AccountHead_id',array('class'=>'form-control','type'=>'hidden','step'=>'any','rows'=>2,'id'=>'AccountHead_id','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Address</label>
					<div class="col-sm-6">
						<?= $this->Form->input('place_edit',array('class'=>'form-control','type'=>'textarea','step'=>'any','rows'=>2,'id'=>'place_edit','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Opening Balance</label>
					<div class="col-sm-6">
						<?= $this->Form->input('opening_balance_edit',array('class'=>'form-control opening_balance','type'=>'number','step'=>'any','required','id'=>'opening_balance_edit','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Credit Period</label>
					<div class="col-sm-6">
						<?= $this->Form->input('credit_period_edit',array('class'=>'form-control credit_period','type'=>'number','step'=>'any','required','id'=>'credit_period_edit','value'=>0,'label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Credit Limit</label>
					<div class="col-sm-6">
						<?= $this->Form->input('credit_limit_edit',array('class'=>'form-control credit_limit','type'=>'number','step'=>'any','required','id'=>'credit_limit_edit','value'=>0,'label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Route</label>
					<div class="col-sm-6">
						<?= $this->Form->input('route_id_edit',array('class'=>'form-control select2 routes','type'=>'select','required','style'=>'width:100%','options'=>$Route_list,'id'=>'route_id_edit','label'=>false,)); ?>
					</div>
					<div style="display: none;" class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#route_add_modal"></i></div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Group</label>
					<div class="col-sm-6">
						<?= $this->Form->input('customer_group_id_edit',array('class'=>'form-control select2','type'=>'select','options'=>$CustomerGroup_list,'style'=>'width:100%','id'=>'customer_group_id_edit','label'=>false,)); ?>
					</div>
				</div>
				
				
				
				<div class="form-group">
					<label class="col-sm-4 control-label">Email</label>
					<div class="col-sm-6">
						<?= $this->Form->input('email_edit',array('class'=>'form-control','type'=>'text','id'=>'email_edit','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Mobile No</label>
					<div class="col-sm-6">
						<?= $this->Form->input('mobile_edit',array('class'=>'form-control','type'=>'text','id'=>'mobile_edit','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Contact Person</label>
					<div class="col-sm-6">
						<?= $this->Form->input('contact_person_edit',array('class'=>'form-control name','type'=>'text','required','id'=>'contact_person_edit','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group" style="">
					<label class="col-sm-4 control-label">VAT No</label>
					<div class="col-sm-6">
						<?= $this->Form->input('vat_no',array('class'=>'form-control','type'=>'text','id'=>'vat_no','label'=>false,)); ?>
					</div>
				</div>
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn_radious" data-dismiss="modal" id="edit_customer_button">Edit</button>
			</div>
			<?= $this->Form->end(); ?>
		</div>
	</div>
</div>

