<style type="text/css">
.approve {
    border: none;
    border-radius: 3px !important;
    background-color: #156915;
    letter-spacing: 0.4px;
}
.reject {
    border: none;
    border-radius: 3px !important;
    background-color: red;
    letter-spacing: 0.4px;
}
.a_view {
    font-size: 17px;
    letter-spacing: 0.6px;
    font-weight: 600;
    color: #005082;
}
</style>

<section class="content-header">
    <h1>Expense Approval</h1>
</section>
<section class="content">
    <div class="box box-primary">
        <?php echo $this->Form->create('ExpenseApproval', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'ExpenseApproval']); ?>
        <div class="row" style="margin-top: 1%;">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="col-md-3 col-lg-3 col-sm-3">                            
                    <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker expense_search','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'From','value'=>date('d-m-Y',strtotime('first day of this month')))); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker expense_search','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'To','value'=>date('d-m-Y'))); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <?php echo $this->Form->input('executive',array('type'=>'select','class'=>'form-control select_two_class expense_search','id'=>'executive','style'=>'width: 100%;','options'=>$ExecutiveList,'empty'=>'All')); ?>
                </div>
            </div>
        </div>
        <?= $this->Form->end(); ?>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="box-body">
                    <table class="table table-condensed table table datatable table-bordered " style="margin-top: 0%;" id="expense_approval_table">
                        <thead>
                            <tr class="blue-bg">
                                <th>Executive</th>
                                <th>Date</th>
                                <th width="30%">Remarks</th>
                                <th class="text-right">Amount</th>
                                <th class="text-right">Approved Amount</th>
                                <th width="15%">Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($Executive_expense)) 
                        { 
                        foreach ($Executive_expense as $key => $value){?>
                        <tr class="blue-pddng">
                        <td><span class='expense_id' hidden><?= $value['ExecutiveExpenseDetail']['id']?></span><span class='executive_name'><?=$value['Executive']['name'];?></span></td>
                        <td><?= date('d-m-Y',strtotime($value['ExecutiveExpenseDetail']['date']));?> </td>
                        <td><?= $value['ExecutiveExpenseDetail']['remarks']?></td>
                        <td class="text-right"><?= $value['ExecutiveExpenseDetail']['amount']?> </td>
                        <?php if($value['ExecutiveExpenseDetail']['status']==1)
                        {
                        ?><td class="text-right"><input type='text' class='form-control approved_amount text-right' value="<?= $value['ExecutiveExpenseDetail']['approved_amount']?>"></td>
                        <td><button class="btn btn-success create_icon aprv_bttn approve" value="2">Approve</button><button class="btn btn-danger create_icon aprv_bttn reject" value="3" >Reject</button></td>   
                        <?php }
                        else{
                            ?><td class="text-right"><input type='text' class='form-control approved_amount text-right' readonly value="<?= $value['ExecutiveExpenseDetail']['approved_amount']?>"></td> <?php
                          if($value['ExecutiveExpenseDetail']['status']==2)
                          {
                            ?><td><p style="color:green;" font-weight="18px;">Approved</p></td> <?php
                          }
                          else
                          {
                             ?><td><p style="color:red;" font-weight="18px;">Rejected</p></td> <?php
                          }
                         } ?>
                        </tr>
                        <?php } 
                        }?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

    $.fn.table_search=function(){
        var from_date=$('#from_date').val();
        var to_date=$('#to_date').val();
        var executive=$('#executive').val();
        var data={
            to_date:to_date,
            from_date : from_date,
            executive : executive
        };
        var url_address= '<?php echo $this->webroot; ?>'+'Accountings/expense_search_ajax';
        $.ajax({
            type: "post",
            url:url_address,
            data: data,
            dataType:'json',
            success: function(response) {
                $('#expense_approval_table').DataTable().destroy();
                $('#expense_approval_table tbody').empty();
                $.each(response,function(key,value){
                    var expense_approval_table='<tr class="blue-pddng">';
                    var date=value.ExecutiveExpenseDetail.date;
                    var date_split = date.split('-');
                    expense_approval_table+="<td><span class='expense_id' hidden>"+value.ExecutiveExpenseDetail.id+"</span><span class='executive_name'>"+value.Executive.name+"</span></td>";
                    expense_approval_table+="<td class='executive_expense_date'>"+date_split[2]+'-'+date_split[1]+'-'+date_split[0]+"</td>";
                     expense_approval_table+="<td>"+value.ExecutiveExpenseDetail.remarks+"</td>";
                    expense_approval_table+="<td class='text-right'>"+value.ExecutiveExpenseDetail.amount+"</td>";
                    if(value.ExecutiveExpenseDetail.status == 1){
                         expense_approval_table+="<td class='text-right'><input type='text' class='form-control approved_amount text-right' value='"+value.ExecutiveExpenseDetail.approved_amount+"'></td>";
                        expense_approval_table+='<td><button class="btn btn-success create_icon aprv_bttn approve" value="2">Approve</button><button class="btn btn-danger create_icon aprv_bttn reject" value="3" >Reject</button></td>';

                    } else{
                         expense_approval_table+="<td class='text-right'><input type='text' class='form-control approved_amount text-right' value='"+value.ExecutiveExpenseDetail.approved_amount+"' readonly></td>";
                         if(value.ExecutiveExpenseDetail.status==2)
                         {
                        expense_approval_table+='<td><p style="color:green;" font-weight="18px;">Approved</p></td>';

                         }
                         else
                         {
                        expense_approval_table+='<td><p style="color:red;" font-weight="18px;">Rejected</p></td>'; 
                         }
                    } 
                    
                    // expense_approval_table+="<td><a href='<?= $this->webroot; ?>Accountings/expense_approval_delete/"+value.ExecutiveExpenseDetail.id+"'><i class='fa fa-trash blue-col delete'></i></a></td>";               
                    expense_approval_table+="</tr>";
                    $('#expense_approval_table tbody').append(expense_approval_table);
                });
                $('#expense_approval_table').DataTable({
       // "bSort" : false,
"columnDefs": [
      {"targets": [ 4 ],"orderable": false ,},   
     {"targets": [ 5 ],"orderable": false ,},

        ],    });
                   
            },
            error:function (XMLHttpRequest, textStatus, errorThrown) {
                alert(textStatus);
            }
        });
    }

    $(document).on('change','.expense_search',function(){
        $.fn.table_search();
    });
</script>
<script type="text/javascript">
    $(document).on('keyup','.change_field',function(){
        $('#Approve_expense').removeAttr('disabled');
    });
    $(document).on('keyup','.approved_amount',function(){
        var id=$(this).closest('tr').find('td span.expense_id').text();
    var approved_amount=$(this).val();
        $.post("<?= $this->webroot ?>Accountings/executive_expense_update_ajax/" + id+'/'+approved_amount, function (responds){
            if(responds.result != 'Success'){
                alert(responds.result);
                return false;
            }
        },"json");
    });
    $(document).on('click', '.aprv_bttn', function () {
        var value=$(this).val();
            if(!confirm("Are you sure?"))
        {
            return false;
        }
        var id=$(this).closest('tr').find('td span.expense_id').text();
        var data={
            id:id,
            value:value
        };
        var website_url = '<?php echo $this->webroot; ?>Accountings/expense_approval_ajax';
        $.ajax({
            method: "POST",
            url: website_url,
            data: data,
        }).done(function (data) {
            $.fn.table_search();
        });
    });
</script>