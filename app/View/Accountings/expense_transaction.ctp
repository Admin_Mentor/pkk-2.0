<section class="content-header">
  <h1> Expense Transaction 
    <a href="<?= $this->webroot ?>Accountings/Expense"><input type="button" class="btn btn-success save pull-right" value="Create"></input></a>
  </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
      <div class="col-md-12">
        <div class="col-md-2 pd-lt-0" id='route_visibility' >
          <input class='nav-toggle' type="checkbox" id='toggle_button_for_route_visibility' data-width="130" data-toggle="toggle" data-off="Show Route" data-on="Hide Route">
        </div>
        <div class="col-md-2">
          <div class="radio_groups">
            <?php  $attributes = array( 'legend' => false, ); echo $this->Form->radio('group', $group_list, $attributes); ?>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
            <div class="route-field" style="display: none">
              <?= $this->Form->input('route_id',array('type'=>'select','class'=>'form-control select_two_class','style'=>'width: 100%;','id'=>'route_id','empty'=>'Select','label'=>'Route',)); ?>
            </div>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
              <?= $this->Form->input('amount',array('class'=>'form-control number','type'=>'text','required','id'=>'amount','value'=>0)); ?>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
              <?= $this->Form->input('paid',array('class'=>'form-control number expense_amount','type'=>'text','required','value'=>0,'id'=>'paid','label'=>'Paid',)); ?>
            </div>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" style="display:none;">
            <?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" hidden="">
            <?= $this->Form->input('external_voucher',array('type'=>'text','class'=>'form-control','id'=>'external_voucher','label'=>'Ext Voucher NO')) ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date',)); ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-6">
            <?= $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select_two_class','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
          </div>
          <div class="col-md-2 col-lg-2 col-xs-2 col-sm-2" id='mode_fields' >
            <?= $this->Form->input('mode',array('type'=>'select','class'=>'form-control select_two_class','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
            <?= $this->Form->input('category',array('type'=>'select','class'=>'form-control select_two_class','style'=>'width: 100%;','id'=>'category','options'=>$SubGroup_list,'required','label'=>'Group',)); ?>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
              <?= $this->Form->input('existing_outstanding',array('class'=>'form-control number','type'=>'text','id'=>'existing_outstanding','value'=>'0','readonly')); ?>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
              <?= $this->Form->input('outstanding',array('class'=>'form-control number','type'=>'text','step'=>'any','required','readonly','value'=>0,'id'=>'outstanding')); ?>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
              <?= $this->Form->input('tax_amount',array('class'=>'form-control number','type'=>'text','id'=>'tax_amount','value'=>'0')); ?>
          </div>
          <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
              <?= $this->Form->input('supplier_name',array('class'=>'form-control','type'=>'text','id'=>'supplier_name','value'=>'')); ?>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
            <?= $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select_two_class','id'=>'type','style'=>'width: 100%;','id'=>'account_head','options'=>$AccountHead,'required','label'=>'Account Name',)); ?>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
              <?= $this->Form->input('existing_prepaid',array('class'=>'form-control number','type'=>'text','id'=>'existing_prepaid','value'=>'0','readonly')); ?>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
              <?= $this->Form->input('prepaid',array('class'=>'form-control number','type'=>'text','step'=>'any','required','readonly','value'=>0,'id'=>'prepaid')); ?>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher')) ?>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
              <?= $this->Form->input('t_r_n_number',array('class'=>'form-control','type'=>'text','id'=>'trn_number','value'=>'')); ?>
          </div>
          <div class="col-md-1"><br>
            <button type="submit" id='add_button' class="btn btn-success">ADD</button>
          </div>
        </div>
      </div>
          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
            <?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
      </div>
      <?= $this->Form->end(); ?>
    </div>
    <div class="box-body table">
      <table class="table table-condensed table datatable table boder boder" id="journal_table">
        <thead>
          <tr class="blue-bg">
            <th class="padding_left">Date</th>
            <th>Group</th>
            <th hidden>Actual Account Name</th>
            <th>Account Name</th>
            <th>Amount</th>
            <th>Paid</th>
            <th>Outstanding</th>
            <th>PrePaid</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
        </tfoot>
      </table>
    </div>
  </div>
</section>
<?php require('general_journal_transaction_modal.php') ?>
<script type="text/javascript">
  <?php require('expense_transaction.js'); ?>
</script>