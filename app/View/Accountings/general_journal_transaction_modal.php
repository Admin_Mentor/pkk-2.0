<?php require('datatable_print.php'); ?>
<section class="content">
  <div id="view_transaction_modal" class="modal fade" role="dialog" >
    <div class="modal-dialog modal-lg" style="width: 80%">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span id='account_holder_name'></span></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
              <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'modal_from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
              <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'modal_to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
            <br>
            <div class="col-md-6 col-lg-6 col-sm-6">
              <div class="col-md-2 col-lg-2 col-sm-2">
                <button class="btn btn-primary btn_mdl_tp_mrgn" id='fetch_button_journal'>Fetch</button>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-4 party_flag">
                <button type="button" id="customer_statement_print" class="print btn_mdl_tp_mrgn" style="white-space: nowrap;">Customer Statement</button>
              </div>
               <div class="col-md-4 col-lg-4 col-sm-4 supplier_flag">
                <button type="button" id="supplier_statement_print" class="print btn_mdl_tp_mrgn" style="white-space: nowrap;">Supplier Statement</button>
              </div>
              <div class="col-md-4 col-lg-4 col-sm-4 party_flag" style="display: none
              ">
                <button type="button" id="customer_Due_statement_print" class="print btn_mdl_tp_mrgn" style="white-space: nowrap;">Customer Outstanding</button>
              </div>
            </div>




          <!--   <div class="col-md-3"><br>
              <button class="btn btn-primary" id='fetch_button_journal'>Fetch</button>
            </div> -->
          <!--   <div class="col-md-3">
                  <button type="button" id="customer_statement_print" class="print">Customer Statement</button>
                </div>
                 <div class="col-md-3">
                  <button type="button" id="customer_Due_statement_print" class="print">Customer Duelist Statement</button>
                </div -->
              </div>
              <div class="box-body table-responsive no-padding xs_tp">
                <table class="table table-bordered table-hover" id="transaction_details_table" width="100%">
                  <thead>
                    <tr class="blue-bg">
                      <th>Date</th>
                      <th>Cash/Bank</th>
                      <th>Executive</th>
                      <th>Voucher No</th>
                      <th>Ext Voucher No</th>
                      <th>Remarks</th>
                      <th>Debit</th>
                      <th>Credit</th>
                      <th>Balance</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
      <div id="view_transaction_by_voucher_no_modal" class="modal fade" role="dialog" >
        <div class="modal-dialog modal-lg" style="width: 70%">
          <div class="modal-content pull-right">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title"><span id='account_holder_name'></span></h4>
            </div>
            <div class="modal-body">
              <div class="box-body table-responsive no-padding xs_tp">
                <table class="table table-bordered table-hover" id="transaction_details_voucher_table">
                  <thead>
                    <tr class="blue-bg">
                      <th>Date</th>
                      <th>Name</th>
                      <th>Debit</th>
                      <th>Credit</th>
                      <th>Voucher No</th>
                      <th>Remark</th>
                      <th>Work Flow</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    </section>
    <script type="text/javascript">
     <?php require('general_journal_transaction_ajax.js'); ?>
   </script>