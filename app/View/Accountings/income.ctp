<section class="content-header">
  <h1>Income Create </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary box_tp_brdr">
        <div class="row-wrapper">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <?php echo $this->Form->create('AccountHead', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'AccountHead_Form']); ?>
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="radio_groups">
                        <?php 
                        $attributes = array(
                          'legend' => false,
                          );
                          echo $this->Form->radio('group', $group_list, $attributes); ?>
                        </div>
                      </div>
                    </div>
                    <br>
                    <br>
                    <div class="col-md-12">
                      <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                        <div class="form-group">
                          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date',)); ?>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
                              <?php echo $this->Form->input('category',array('type'=>'select','class'=>'form-control select2','style'=>'width: 100%;','id'=>'category','options'=>$SubGroup_list,'required','label'=>'Group',)); ?>
                            </div>
                            <br>
                            <div class="col-md-4 col-lg-4 col-xs-12 col-sm-4">
                              <div class="col-md-2"><i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#category_modal"></i></div>
                              <div class="col-md-2"><i class="fa fa-pencil fa-2x plus-btn" id="edit_category_icon"></i></div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                        <div class="form-group">
                          <div class="col-md-12">
                            <div class="col-md-10 col-xs-10 col-sm-10 col-lg-10">
                              <?= $this->Form->input('name',array('class'=>'form-control name','type'=>'text','required','id'=>'name','label'=>'Account Name',)); ?>
                            </div>
                            <br>
                            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#account_details_modal"></i></div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                        <div class="form-group">
                          <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                            <?= $this->Form->input('opening_balance',array('class'=>'form-control opening_balance','type'=>'number','step'=>'any','required','readonly','id'=>'opening_balance','label'=>'Opening Balance',)); ?>
                          </div>
                        </div>
                      </div> 
                      <div class="col-md-1">
                        <div class="create-wrapper pull-right">
                          <br>
                          <button type="submit" id='add_button' class="user_add_btn">ADD</button>
                          <button type="submit" id='edit_button' class="user_add_btn" style="display:none">EDIT</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="account_details_modal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Accounts Details</h4>
                        </div>
                        <div class="modal-body">
                          <div class="form-horizontal">
                            <div class="form-group">
                              <label class="col-sm-4 control-label">Account's Name</label>
                              <div class="col-sm-6">
                                <?= $this->Form->input('name',array('class'=>'form-control name','type'=>'text','step'=>'any','required','id'=>'name_modal','label'=>false,)); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">Description</label>
                              <div class="col-sm-6">
                                <?= $this->Form->input('description',array('class'=>'form-control','type'=>'textarea','step'=>'any','rows'=>2,'id'=>'description','label'=>false,)); ?>
                              </div>
                            </div>
                            <div class="form-group">
                              <label class="col-sm-4 control-label">Opening Balance</label>
                              <div class="col-sm-6">
                                <?= $this->Form->input('opening_balance',array('class'=>'form-control opening_balance','type'=>'number','step'=>'any','required','id'=>'opening_balance_modal','label'=>false,)); ?>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?= $this->Form->end(); ?>
                </div>
                <div class="row-wrapper">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="box-body table">
                        <table class="table table-condensed table table boder boder datatable" id="myTable">
                          <thead>
                            <tr class="blue-bg">
                              <th class="padding_left">Date</th>
                              <th>Type</th>
                              <th>Group</th>
                              <th>Account Name</th>
                              <th>Opening Balance</th>
                              <th>Description</th>
                              <th>Action</th>
                              <!-- <th></th> -->
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($AccountHead as $key => $value): ?>
                              <tr class="blue-pddng">
                                <td><?= date('d-m-Y',strtotime($value['AccountHead']['created_at'])) ?></td>
                                <td><?= $value['Group']['name']; ?></td>
                                <td><?= $value['SubGroup']['name']; ?></td>
                                <td><span style="display:none" class='AccountHead_id'><?= $value['AccountHead']['id']; ?></span><span><?= $value['AccountHead']['name']; ?></span></td>
                                <td class="text-right"><?= $value['AccountHead']['opening_balance']; ?></td>
                                <td><?= $value['AccountHead']['description']; ?></td>
                                <td><i class="fa fa-2x fa-pencil-square-o blue-col edit_head"></i>
                                <a onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Accountings/AccountHead_delete/<?= $value['AccountHead']['id']; ?>"><i class="fa fa-2x fa-trash blue-col blue-col"></i></a></td>
                              </tr>
                            <?php endforeach ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="category_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">Category</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label class="col-sm-4 control-label">Category Name</label>
                <div class="col-sm-8">
                  <input class="form-control" id="catagory_name" type="text">
                </div>
                <br>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" id='add_category'>Save</button>
            </div>
          </div>
        </div>
      </div>
      <div id="edit_category_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h4 class="modal-title" id="myModalLabel">Edit Category</h4>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <label class="col-sm-4 control-label">Category Name</label>
                <div class="col-sm-8">
                  <input class="form-control" id="edit_catagory_name" type="text">
                  <input id="edit_catagory_id" type="hidden">
                </div>
                <br>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary" id='edit_category'>Update</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('.name').on('keyup change',function(){
    $(this).val($(this).val().trim());
    $('#name').val($(this).val());
    $('#name_modal').val($(this).val());
  });
  $('.opening_balance').on('keyup change',function(){
    $('#opening_balance').val($(this).val());
    $('#opening_balance_modal').val($(this).val());
  });
</script>
<script type="text/javascript">
  $('#catagory_name').keyup(function(){
    $('#catagory_name').val($(this).val().trim());
  });
  $('#add_category').click(function(){
    var catagory_name=$('#catagory_name').val();
    var group_id=$("input[name='data[AccountHead][group]']:checked").val();
    alert(group_id);
    if(!catagory_name)
    {
      $('#catagory_name').focus();
      return false;
    }
    if(!group_id)
    {
      alert('Please Select Any option from check box');
      return false;
    }
    var data={
      name:catagory_name,
      group_id:group_id
    }
    var url_address= '<?php echo $this->webroot; ?>'+'Accountings/income_sub_category_add_ajax';
    $.ajax({
      type: "POST",  
      url:url_address,
      data:data,
      dataType:'json',
      success: function(response) {
        if(response.result!='Success')
        {
          return false;
        }
        $('#category').append($("<option></option>").attr("value",response.key).text(response.value));
        $('#category').val(response.key).trigger('change');
        $('#catagory_name').val('');
        $('#category_modal').modal('toggle');
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).on('click','.edit_head',function(){
    var id=$(this).closest('tr').find('td span.AccountHead_id').text();
    $.post( "<?= $this->webroot ?>Accountings/AccountHeadGet_ajax/"+id, function( data ) {
      var name=data.AccountHead.name;
      var split_name=name.split(' ');
      $('#name').val(split_name[0]).trigger('keyup');
      $('#opening_balance').val(data.AccountHead.opening_balance).trigger('keyup');
      $('#description').val(data.AccountHead.description);
      $("#name").append("<input type='text' id='account_id' name='data[AccountHead][account_id]' value='"+data.AccountHead.id+"'>");
      $('#add_button').css('display','none');
      $('#edit_button').css('display','');
      $('#AccountHead_Form').attr('action','<?= $this->webroot; ?>Accountings/EditAccountHead_Income');
    }, "json");
  });
</script>
<script type="text/javascript">
  $("input[name='data[AccountHead][group]']").change(function(){
    var group=$(this).val();
    $.post( "<?= $this->webroot ?>Accountings/SubGroup_Option_ListByGroupId_ajax/"+group, function( data ) {
      $('#category').html('');
      if(data.result!='Success')
      {
        alert(data.result);
        return false;
      }
      $.each(data.options,function(key,value){
        $('#category').append($("<option></option>").attr("value",key).text(value));
      })
    }, "json");
  });
</script>
<script type="text/javascript">
  $('#edit_category_icon').click(function(){
    var name=$('#category option:selected').text();
    if(!name)
    {
      $('#category').select2('open');
      return false;
    }
    var split_name=name.split(' ');
    var id=$('#category').val();
    $('#edit_catagory_name').val(split_name[0]);
    $('#edit_catagory_id').val(id);
    $('#edit_category_modal').modal('toggle');
  });
  $('#edit_category').click(function(){
    var name=$('#edit_catagory_name').val();
    var id=$('#edit_catagory_id').val();
    var data={
      name:name,
      id:id
    }
    $.post( "<?= $this->webroot ?>Accountings/income_sub_category_edit_ajax",data, function( data ) {
      if(data.result!='Success')
      {
        alert(data.result);
        return false;
      }
      $('#category option[value="' + id + '"]').text(name).change();
      $('#category').select2();
      $('#edit_category_modal').modal('toggle');
    }, "json");
  });
  $('#edit_catagory_name').keyup(function(){
    $(this).val($(this).val().trim());
  });
</script>