<section class="content-header">
  <h1> Income Transaction 
    <a href="<?php echo $this->webroot ?>Accountings/Income"><input type="button" class="btn btn-success save pull-right" value="Create Income"></input></a></h1>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary box_tp_brdr">
          <div class="row-wrapper">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <?php echo $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="radio_groups">
                          <?php $attributes = array( 'legend' => false, ); echo $this->Form->radio('group', $group_list, $attributes); ?>
                        </div>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                          <div class="form-group">
                            <div class="col-md-10 col-lg-10 col-xs-12 col-sm-10">
                              <?php echo $this->Form->input('category',array('type'=>'select','class'=>'form-control select2','style'=>'width: 100%;','id'=>'category','options'=>$SubGroup_list,'required','label'=>'Group',)); ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                          <div class="form-group">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                              <?= $this->Form->input('amount',array('class'=>'form-control number','type'=>'number','step'=>'any','required','id'=>'amount',)); ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12" style="display:none">
                          <div class="form-group">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                              <?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                          <div class="form-group">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                              <?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                          <div class="form-group">
                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                              <?php echo $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','id'=>'account_head','options'=>$AccountHead,'required','label'=>'Account Name',)); ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                          <div class="form-group">
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-9">
                              <?= $this->Form->input('recieved',array('class'=>'form-control number','type'=>'number','step'=>'any','required','value'=>0,'id'=>'recieved',)); ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                          <div class="col-md-6">
                            <div class="form-group">
                              <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                <?php echo $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select2','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                              <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12" id='mode_field' style="display:none">
                                <?php echo $this->Form->input('mode',array('type'=>'select','class'=>'form-control select2','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                          <div class="form-group">
                            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
                              <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date',)); ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                          <?= $this->Form->input('accrued',array('class'=>'form-control number','type'=>'number','step'=>'any','required','readonly','value'=>0,'id'=>'accrued',)); ?>
                          <?= $this->Form->input('accrued_hidden',array('type'=>'hidden','id'=>'accrued_hidden','value'=>'0')); ?>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                          <?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher')) ?>
                        </div>
                        <div class="col-md-1">
                          <div class="create-wrapper">
                            <br>
                            <button type="submit" id='add_button' class="user_add_btn">ADD</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?= $this->Form->end(); ?>
                </div>
                <br>
                <div class="row-wrapper">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="box-body table">
                        <table class="table table-condensed table datatable table boder boder" id="journal_table">
                          <thead>
                            <tr class="blue-bg">
                              <th class="padding_left">Date</th>
                              <th>Group</th>
                              <th>Account Name</th>
                              <th>Amount</th>
                              <th>Recieved</th>
                              <th>Accrued</th>
                            </tr>
                          </thead>
                          <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php require('general_journal_transaction_modal.php') ?>
  </section>
  <script type="text/javascript">
    <?php require('income_transaction.js'); ?>
  </script>