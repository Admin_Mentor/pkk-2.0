 $("input[name='data[Journal][group]']").change(function(){
  var group=$(this).val();
  $.post( "<?= $this->webroot ?>Accountings/SubGroup_Option_ListByGroupId_ajax/"+group, function( data ) {
    $('#category').html('');
    $('#account_head').html('');
    $.each(data.options,function(key,value){
      $('#category').append($("<option></option>").attr("value",key).text(value));
    })
    $('#category').trigger('change');
  }, "json");
  $.post( "<?= $this->webroot ?>Accountings/income_table_list_by_group_id_ajax/"+group, function( data ) {
    $('#journal_table').DataTable().destroy();
    $('#journal_table tbody').empty();
    $('#journal_table tfoot').empty();
    if(data.result=='success')
    {
      $('#journal_table tbody').html(data.row.body);
    }
    $('#journal_table tfoot').html(data.row.foot);  
    $('#journal_table').DataTable();
  }, "json");
});
  $("input[name='data[Journal][group]']:checked").change();
 $('#category').change(function(){
  var category=$(this).val();
  $.post( "<?= $this->webroot ?>Accountings/AccountHead_Option_ListBySubGroupId_ajax/"+category, function( data ) {
    $('#account_head').html('');
    $.each(data.options,function(key,value){
      $('#account_head').append($("<option></option>").attr("value",key).text(value));
    });
    $('#account_head').change();
  }, "json");
});
 $('#amount').keyup(function(){
  var amount=$(this).val();
  var accrued=$('#accrued_hidden').val();
  $('#recieved').val(parseFloat(amount)+parseFloat(accrued));
  var recieved=$('#recieved').val();
  var accrued_hidden=$('#accrued_hidden').val();
  $('#accrued').val(parseFloat(accrued_hidden)+parseFloat(amount)-parseFloat(recieved));
});
 $('#recieved').keyup(function(){
  var amount=$('#amount').val();
  var recieved=$('#recieved').val();
  var accrued_hidden=$('#accrued_hidden').val();
  $('#accrued').val(parseFloat(accrued_hidden)+parseFloat(amount)-parseFloat(recieved));
});
 $('#account_head').change(function(){
  var account_head=$('#account_head option:selected').text();
  $.post( "<?= $this->webroot ?>Accountings/get_accrued_balance_from_income/"+account_head, function( data ) {  
    $('#accrued').val(data);
    $('#accrued_hidden').val(data);
  }, "json");
});
 $('#account_head').change();