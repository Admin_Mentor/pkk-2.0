<script type="text/javascript" src='<?= $this->webroot.'js/bootstable.js';?>'></script>
<section class="content-header">
	<div class="box">
		<div class="box-body">
			<table class="table table-bordered">
				<thead>
					<tr class="blue-bg">
						<th>#</th>
						<th>Type</th>
						<th>Master Group</th>
						<th>Group</th>
						<th>Sub Group</th>
						<th>Account Head</th>
						<th>Opening Balance</th>
						<th>Opening Date</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($AccountHeads as $key => $AccountHead): ?>
						<?php $colors=[
							'#00c0ef',
							'#3c8dbc',
							'#30bbbb',
							'#D81B60',
							'#00a65a',
						];
						?>
						<tr class="blue-pddng" bgcolor='<?= $colors[$AccountHead['Type_id']-1]; ?>'>
							<td><?=$key+1; ?></td>
							<td><font color="#000"><?= $AccountHead['Type_id'].')'.$AccountHead['Type']; ?></font></td>
							<td><font color="#000"><?= $AccountHead['MasterGroup_id'].')'.$AccountHead['MasterGroup']; ?></font></td>
							<td><font color="#000"><?= $AccountHead['Group_id'].')'.$AccountHead['Group']; ?></font></td>
							<td><font color="#000"><?= $AccountHead['SubGroup_id'].')'.$AccountHead['SubGroup']; ?></font></td>
							<td color="#000"><span class='table_id'><?= $AccountHead['AccountHead_id']; ?></span>)<?= $AccountHead['AccountHead']; ?></font></td>
							<td color="#000" class='text-right'><?= floatval($AccountHead['opening_balance']); ?></font></td>
							<td><?= date('d-m-Y',strtotime($AccountHead['created_at'])); ?></font></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
<script>
	$('table').SetEditable();
	function rowEdit(but) {
		var $row = $(but).parents('tr');
		var $cols = $row.find('td');
		if (ModoEdicion($row)) return;
		var i=0;
		IterarCamposEdit($cols, function($td) {
			i++;
			if(i==7){
				var cont = $td.html()
				var div = '<div hidden>' + cont + '</div>';
				var input = '<input class="form-control text-right number" type="text" value="' + cont + '">';
				$td.html(div + input);
			}
			if(i==8){
				var cont = $td.html()
				var div = '<div hidden>' + cont + '</div>';
				var input = '<input class="form-control date_picker text-right datepicker" value="'+cont+'">';
				$td.html(div + input);
			}
		});
		FijModoEdit(but);
	}
	function rowAcep(but) {
		var $row = $(but).parents('tr');  
		var $cols = $row.find('td');  
		console.log($cols);
		var i=0;
		if (!ModoEdicion($row)) return;  
		IterarCamposEdit($cols, function($td) {  
			i++;
			if(i==6)
			{
				id=$td.find('span.table_id').text();
			}
			if(i==7)
			{
				opening_balance = $td.find('input').val(); 
				$td.html(opening_balance); 
			}
			if(i==8)
			{
				var created_at = $td.find('input').val(); 
				$td.html(created_at); 
				var data_array={
					created_at : created_at,
					opening_balance : opening_balance,
					id : id,
				};
				$.post( "<?= $this->webroot ?>Accountings/UpdateOpeningBalance",data_array ,function( data ) {
				}, "json");
			}
		});
		FijModoNormal(but);
		params.onEdit($row);
	}
</script>
