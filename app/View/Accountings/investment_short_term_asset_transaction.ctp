<section class="content-header">
  <h1>Short Term Asset Transaction 
    <a href="<?= $this->webroot ?>Accountings/InvestmentShortTermAsset"><input type="button" class="btn btn-success save pull-right" value="Create"></input></a>
  </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
      <div class="col-md-12">
        <div class="form-group">
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date',)); ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('category',array('type'=>'select','class'=>'form-control select2','style'=>'width: 100%;','id'=>'category','options'=>$SubGroup_list,'required','label'=>'Category',)); ?>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
            <?= $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','id'=>'account_head','options'=>$AccountHead,'required','label'=>'Sub Category',)); ?>
          </div>
          <div class="col-md-2">
            <?= $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select2','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
            <?= $this->Form->input('mode',array('type'=>'select','class'=>'form-control select2','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="form-group">
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('amount',array('class'=>'form-control number','type'=>'number','step'=>'any','required','id'=>'amount',)); ?>
          </div>
          <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
            <?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','required','id'=>'remarks','label'=>'Remarks')) ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12" style="display:none">
            <?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher')) ?>
          </div>
          <div class="col-md-1"><br>
            <button type="submit" id='add_button' class="user_add_btn">ADD</button>
          </div>
        </div>
      </div>
      <?= $this->Form->end(); ?>
    </div>
    <div class="box-body table">
      <table class="table table-condensed table datatable table boder boder" id="journal_table">
        <thead>
          <tr class="blue-bg">
            <th class="padding_left">Date</th>
            <th>Category</th>
            <th>Sub Category</th>
            <th>Amount</th>
          </tr>
        </thead>
        <tbody>
          <?php $credit=0; $debit=0; $outstanding=0; foreach ($All_Accounts as $key => $value): ?>
          <tr class="blue-pddng view_transaction"> 
            <td class='padding_left'><?= date('d-m-Y',strtotime($value['date'])); ?></td>
            <td><?= $value['SubGroup']; ?></td>
            <td class='name'><?= $value['name']; ?></td>
            <td class="text-right"><?= $value['debit']; $debit+=$value['debit']; ?></td>
          </tr>
        <?php endforeach ?>
      </tbody>
      <tfoot>
        <tr class="blue-pddng">
          <td></td>
          <td></td>
          <td class="total_amount">Total</td>
          <td class="total_amount text-right"><?= $debit; ?></td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
</section>
<?php require('general_journal_transaction_modal.php') ?>
<script type="text/javascript">
  <?php require('investment_long_term_asset_transaction.js'); ?>
</script>