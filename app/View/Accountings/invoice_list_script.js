function roundToTwo(num) {
        return +(Math.round(num + "e+3")  + "e-3");
    }
$(document).on('click','#amount',function(){
    var account_head_id=$('#account_head').val();
    var account_head_text=$('#account_head option:selected').text();
    if(!account_head)
    {
        $('#account_head').select2('open');
        return false;
    }
    $('#customer_name_modal').text(account_head_text);
    $('#invoice_amount').attr('autofocus');
    $.post( "<?= $this->webroot ?>Customer/get_customer_pending_invoice_list_ajax/"+account_head_id ,function( data ) {
        if(data.result!='Success')
        {
            alert(data.result);
            return false;
        }
        $('#customer_invoice_list tbody').empty();
      var invoice_discount_input_field="<input type='text' id='single_discount_amount' name='data[Sales][discount][]' class='form-control single_discount'>";
        var invoice_amount_input_field="<input type='text' name='data[Sales][amount][]' class='form-control single_amount'>";   
        $.each(data.sales,function(key,value){
            if(key =='0'){
                key="Opening Balance";
            }
            var value=roundToTwo(value);
            var invoice_amount_value_field="<input type='text' name='data[Sales][id][]' value='"+key+"' class='invoice' readonly>";
            $('#customer_invoice_list tbody').append('<tr>\
                <td class="invoice">'+invoice_amount_value_field+'</td>\
                <td class="invoice_amount">'+value+'</td>\
                <td>'+invoice_amount_input_field+'</td>\
                <td>'+invoice_discount_input_field+'</td>\
                <td class="single_balance_amount"></td>\
                </tr>');
        });
        $('#invoice_amount').keyup();
        $('#invoice_check_modal').modal();
    }, "json");
});
$(document).on('keyup','#invoice_amount',function(){
    $('#amount').text($(this).val());
    $('#customer_invoice_list tbody tr').each(function(){
        $(this).closest('tr').find('td input.single_amount').val('0');
        $(this).closest('tr').find('td input.single_discount').val('0');
    });

    
    var invoice_amount = parseFloat($(this).val());
    $('#customer_invoice_list tbody tr').each(function(){
        var balance=$(this).closest('tr').find('td.invoice_amount').text();
        if(balance>=invoice_amount)
        {
            $(this).closest('tr').find('td input.single_amount').val(parseFloat(invoice_amount).toFixed(3));
            $(this).closest('tr').find('td.single_balance_amount').text(parseFloat(balance-invoice_amount).toFixed(3));
            invoice_amount=0;
        }
        else
        {
            $(this).closest('tr').find('td input.single_amount').val(parseFloat(balance).toFixed(3));
            $(this).closest('tr').find('td.single_balance_amount').text('0');
            invoice_amount-=balance;
        }
    });
    $.fn.table_loop();
});
$.fn.table_loop=function(){
    var total_invoice_amount=0;
    $('#customer_invoice_list tbody tr').each(function(){
        var single_amount=$(this).closest('tr').find('td input.single_amount').val();
        if(isNaN(single_amount) || single_amount=='' )
        {
            single_amount=0;
        }
        total_invoice_amount=parseFloat(total_invoice_amount)+parseFloat(single_amount);
    });
    var invoice_amount=$('#invoice_amount').val();
    $('#balance_amount').val((parseFloat(invoice_amount)-parseFloat(total_invoice_amount)).toFixed());
    balance_amount=invoice_amount-total_invoice_amount;
    if($('#invoice_amount').val()>0 && parseFloat(balance_amount)==0)
    {
        $('#invoice_button').attr('disabled',false);
    }
    else
    {
        $('#invoice_button').attr('disabled',true);
    }
    return balance_amount;
}
$(document).on('keyup','.single_amount',function(){
    var balance_amount=$('#balance_amount').val();
    var invoice_amount=$(this).closest('tr').find('td.invoice_amount').text();
    var single_amount=$(this).val();
    var balance=invoice_amount-single_amount;
    $(this).closest('tr').find('td.single_balance_amount').text(parseFloat(balance).toFixed(3));
    $.fn.table_loop();
});
$(document).on('keyup','.single_discount',function(){
    var balance_amount=$('#balance_amount').val();
    var invoice_amount=$(this).closest('tr').find('td.invoice_amount').text();
    var single_amount=$(this).closest('tr').find('td input:eq(1)').val();
    var single_discount=$(this).val();
    console.log(single_discount);
    var balance=invoice_amount-single_amount-single_discount;
    $(this).closest('tr').find('td.single_balance_amount').text(parseFloat(balance).toFixed(2));
     $.fn.table_loop();
});
$('#add_button').click(function(){
    var balance_amount=$('#balance_amount').val();
    var invoice_amount=$('#invoice_amount').val();
    if(!invoice_amount)
    {
        alert('Enter A Amount');
        return false;
    }
    if(balance_amount>0)
    {
        alert('Balance Must Be Zero');
        return false;
    }
});