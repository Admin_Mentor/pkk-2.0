<section class="content-header">
	<h1>Reserves Transaction 
		<a href="<?= $this->webroot ?>Accountings/LiabilityReserves"><input type="button" class="btn btn-success save pull-right" value="Create"></input></a>
	</h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
						<?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','id'=>'account_head','options'=>$AccountHead,'required','label'=>'Acc/Name',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('amount',array('class'=>'form-control','type'=>'number','step'=>'any','required','id'=>'amount',)); ?>
					</div>
					<div class="col-md-1 col-lg-1 col-sm-1 col-xs-12" style="display:none">
						<?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','required','id'=>'remarks','label'=>'Remarks')) ?>
					</div>
					<div class="col-md-1"> <br>
						<button class="user_add_btn">ADD</button>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body table">
			<table class="boder table table-condensed table datatable boder" id="myTable">
				<thead>
					<tr class="blue-bg">
						<th class="padding_left">Date</th>
						<th>Acc Name</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody>
					<?php $amount=0; $Paid=0; $Total=0; foreach ($All_Account as $key => $value): ?>
					<tr class="blue-pddng view_transaction">
						<td><?= date('d-m-Y',strtotime($value['date'])); ?></td>
						<td class='name'><?= $value['name']; ?></td>
						<td class="text-right"><?= $value['amount']; $amount+=$value['amount'] ?></td>
					</tr>
				<?php endforeach ?>
			</tbody>
			<tfoot>
				<tr class="blue-pddng">
					<td></td>
					<td class="total_amount"><label>Total</label></td>
					<td class="total_amount text-right"><?= $amount; ?></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
</section>
<?php require('general_journal_transaction_modal.php') ?>