<section class="content-header">
	<h1>Long Term Liability Transaction 
		<a href="<?= $this->webroot ?>Accountings/LongTermLiability"><input type="button" class="btn btn-success save pull-right" value="Create"></input></a>
	</h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
						<?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','id'=>'account_head','options'=>$AccountHead,'required','label'=>'Acc/Name',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('type',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','options'=>array('1'=>'Paid','2'=>'Received'),'required','label'=>'Received/Paid',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','required','id'=>'remarks','label'=>'Remarks')) ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12" style="display:none">
						<?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher')) ?>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('amount',array('class'=>'form-control','type'=>'number','step'=>'any','required','id'=>'amount',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('balance',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'balance',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select2','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" >
						<?= $this->Form->input('mode',array('type'=>'select','class'=>'form-control select2','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
					</div>
					<div class="col-md-1"> 
						<div class="create-wrapper"><br>
							<button class="user_add_btn">ADD</button>
						</div>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body table">
			<table class="boder table table-condensed table datatable boder" id="myTable">
				<thead>
					<tr class="blue-bg">
						<th class="padding_left">Date</th>
						<th>Acc Name</th>
						<th>Received</th>
						<th>Debit Paid</th>
						<th>Balance</th>
					</tr>
				</thead>
				<tbody>
					<?php $Received=0; $Paid=0; $Total=0; foreach ($All_Account as $key => $value): ?>
					<tr class="blue-pddng view_transaction">
						<td><?= date('d-m-Y',strtotime($value['date'])); ?></td>
						<td class='name'><?= $value['name']; ?></td>
						<td class="text-right"><?= $value['Received']; $Received+=$value['Received'] ?></td>
						<td class="text-right"><?= $value['Paid']; $Paid+=$value['Paid'] ?></td>
						<td class="text-right"><?= $value['Received']-$value['Paid']; $Total+=$value['Received']-$value['Paid'] ?></td>
					</tr>
				<?php endforeach ?>
			</tbody>
			<tfoot>
				<tr class="blue-pddng">
					<td></td>
					<td class="total_amount"><label>Total</label></td>
					<td class="total_amount text-right"><?= $Received; ?></td>
					<td class="total_amount text-right"><?= $Paid; ?></td>
					<td class="total_amount text-right"><?= $Total; ?></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>
</section>
<?php require('general_journal_transaction_modal.php') ?>
<script type="text/javascript">
	<?php require('long_term_liability_transactions.js'); ?>
</script>