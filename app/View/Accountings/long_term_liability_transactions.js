$(document).on('change keyup','#amount,#account_head,#type',function(){
	var account_head_id=$('#account_head').val();
	var amount=$('#amount').val();
	var type=$('#type').val();

	var data={
		account_head_id:account_head_id,
		amount:amount,
		type:type,
	};
	$.post( "<?= $this->webroot ?>Accountings/liability_balance_get_ajax",{data},function( data ) {
		if(data.result!='Success')
		{
			alert(data.message);
			return false;
		}
		$('#balance').val(data.balance);
	}, "json");
});