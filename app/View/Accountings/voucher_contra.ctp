<section class="content-header">
	<h1>Contra Voucher 
	  <div class="pull-right">
            <a href="<?php echo $this->webroot ?>Accountings/VoucherContraList"><button class='btn btn-success pull-right'>Contra Voucher List</button></a>&nbsp;
        </div></h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?php echo $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
			<?php if(isset($contravoucherlist)) $readonly='disabled'; else $readonly=''; ?>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','disabled'=>$readonly,)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?php echo $this->Form->input('to_account',array('type'=>'select','class'=>'form-control account_head_class select2','id'=>'type','style'=>'width: 100%;','id'=>'to_account','empty'=>[''=>'Select'],'options'=>$AccountHead_list,'required','label'=>'From Account *','disabled'=>$readonly,)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?php echo $this->Form->input('by_account',array('type'=>'select','class'=>'form-control account_head_class select2','id'=>'by_account','style'=>'width: 100%;','empty'=>[''=>'Select'],'options'=>$AccountHead_list,'required','label'=>'To Account *','disabled'=>$readonly,)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher','disabled'=>$readonly,)) ?>
					</div>
					<div class="col-md-1"> <br>
						<span id='to_account_lbl' class='currency-lbl'></span>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('amount',array('class'=>'form-control number text-right','type'=>'text','required','id'=>'amount','disabled'=>$readonly,'label'=>'Amount *')); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" style="display:none">
						<?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
						<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'3','id'=>'remarks','label'=>'Remarks *','required','disabled'=>$readonly)) ?>
					</div>
					 <?php if(!isset($contravoucherlist)) :?>
					<div class="col-md-1"> <br>
						<button type='submit' class="user_add_btn">ADD</button>
					</div>
					 <?php endif; ?>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
</div>
</section>
<?php require('general_journal_transaction_modal.php') ?>
<script type="text/javascript">
	$('#myTable').DataTable( {
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Accountings/VoucherContra_table_ajax",
			//"type": "POST",
			"type": "POST",data:function( d ) {
				//d.customer_type_id= $('#customer_type').val();
				d.account_head_id= $('#to_account').val();
			},
			"dataSrc": "records",
		},
		dom: 'Bfrtip',
     lengthMenu: [
     [10,25, 50,100,-1],
     ['10 rows','25 rows', '50 rows','100 rows','Show all' ]
     ],
    buttons: [
    { extend: 'colvis', },
      {
          extend: 'print',
          // text: 'Print' ,
          // title: 'Executive Brand Wise Sale Report',
           footer: true,
       exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                   
           '<h3 align="center">Voucher Contra Transaction </h3>',
            
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
                // .prepend(
                //   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
                //   )
              }
            },
            {
              extend: 'csv',
              // text: 'CSV' ,
            title:'Voucher Contra Transaction ',

               footer: true,
               customize: function (csv) {
                 return "                     "+"\tVoucher Contra Transaction \n"+  csv ;
               },
       exportOptions: { columns: ':visible'},
            },
            {
              extend: 'excel',
              // text: 'Excel' ,
              // title:'Executive Brand Wise Sale Report',
               title:'Voucher Contra Transaction ',

               footer: true,
       exportOptions: { columns: ':visible'},
            },
       //      {
       //        extend: 'pdf',
       //       // text: 'Pdf' ,
       //        //title:'Executive Brand Wise Sale Report',
       //        title:'Customer Profit'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',

       //         footer: true,
       // //exportOptions: { columns: ':visible'},
       //      },
    'pageLength',
    ],
		"columns": [
		{ "data" : "AccountHead.created_at" },
		{ "data" : "AccountHead.name" },
		{ "data" : "AccountHead.total" },
		],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};
			total = api.column( 2, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 2 ).footer() ).html(''+parseFloat(total.toFixed(3))+'');
			
			
		},
		"columnDefs": [
		{ className: "name", "targets": [ 1 ] },
		{ className: "text-right", "targets": [ 2 ]},
		],
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$(nRow).addClass('view_transaction');
			return nRow;
		}
	});
	
	$('button[type="Submit"]').click(function(){
		var to_account=$('#to_account').val();
		if(!to_account)
		{
			$('#to_account').select2('open');
			return false;
		}
		var by_account=$('#by_account').val();
		if(!by_account)
		{
			$('#by_account').select2('open');
			return false;
		}
	});
	$('.account_head_class').on('change',function(){
		var to_account=$('#to_account').val();
		var by_account=$('#by_account').val();
		if(to_account==by_account)
		{
			alert('Please select Diffrent Option');
			$(this).val('');
		}
	});
	$('#to_account').change(function(){
		//alert("k");
		table = $('#myTable').dataTable();
		table.fnDraw();
	});
</script>