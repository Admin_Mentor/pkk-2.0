<section class="content-header">
	<h1> Voucher Receipt Transaction </h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?php echo $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?php echo $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','id'=>'account_head','empty'=>'All','options'=>$AccountHead_list,'required','label'=>'Acc/Name',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?php echo $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select2','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" >
						<?php echo $this->Form->input('mode',array('type'=>'select','class'=>'form-control select2','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher')) ?>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<?= $this->Form->input('amount',array('class'=>'form-control number text-right','type'=>'text','required','id'=>'amount',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" style="display:none">
						<?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
						<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','required','id'=>'remarks','label'=>'Remarks')) ?>
					</div>
					<div class="col-md-1"> <br>
						<button class="user_add_btn">ADD</button>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body table-responsive no-padding">
			<table class="boder table table-condensed table boder" id="myTable">
				<thead>
					<tr class="blue-bg">
						<th class="padding_left">Date</th>
						<th>Acc Name</th>
						<th style="text-align:left">Total</th>
					</tr>
				</thead>
				<tbody>
					
			</tbody>
			<tfoot>
				<tr>
						<th colspan="2" style="font-size:20px; color:red;text-align:right">Total:</th>
						<th class="text-right" style="font-size:20px; color:red;">0</th>
					</tr>
			</tfoot>
		</table>
	</div>
</div>
</section>
<?php require('general_journal_transaction_modal.php') ?>
<script type="text/javascript">
	$('#myTable').DataTable( {
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Accountings/VoucherReceipt_table_ajax",
			//"type": "POST",
			"type": "POST",data:function( d ) {
				//d.customer_type_id= $('#customer_type').val();
				d.account_head_id= $('#account_head').val();
			},
			"dataSrc": "records",
		},
		 dom: 'Bfrtip',
     lengthMenu: [
     [10,25, 50,100,-1],
     ['10 rows','25 rows', '50 rows','100 rows','Show all' ]
     ],
    buttons: [
    { extend: 'colvis', },
      {
          extend: 'print',
          // text: 'Print' ,
          // title: 'Executive Brand Wise Sale Report',
           footer: true,
       exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                   
           '<h3 align="center">Voucher Receipt Transaction </h3>',
            
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
                // .prepend(
                //   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
                //   )
              }
            },
            {
              extend: 'csv',
              // text: 'CSV' ,
            title:'Voucher Receipt Transaction ',

               footer: true,
               customize: function (csv) {
                 return "                     "+"\tVoucher Receipt Transaction \n"+  csv ;
               },
       exportOptions: { columns: ':visible'},
            },
            {
              extend: 'excel',
              // text: 'Excel' ,
              // title:'Executive Brand Wise Sale Report',
               title:'Voucher Receipt Transaction ',

               footer: true,
       exportOptions: { columns: ':visible'},
            },
       //      {
       //        extend: 'pdf',
       //       // text: 'Pdf' ,
       //        //title:'Executive Brand Wise Sale Report',
       //        title:'Customer Profit'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',

       //         footer: true,
       // //exportOptions: { columns: ':visible'},
       //      },
    'pageLength',
    ],
		"columns": [
		{ "data" : "AccountHead.created_at" },
		{ "data" : "AccountHead.name" },
		{ "data" : "AccountHead.total" },
		],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};
			total = api.column( 2, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 2 ).footer() ).html(''+parseFloat(total.toFixed(3))+'');
			
			
		},
		"columnDefs": [
		{ className: "name", "targets": [ 1 ] },
		{ className: "text-right", "targets": [ 2 ]},
		//{ className: "text-right", "targets": [ 2 ],"bSortable": false },
		],
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$(nRow).addClass('view_transaction');
			return nRow;
		}
	});
	// $('button[type="Submit"]').click(function(){
	// 	var to_account=$('#to_account').val();
	// 	if(!to_account)
	// 	{
	// 		$('#to_account').select2('open');
	// 		return false;
	// 	}
	// 	var by_account=$('#by_account').val();
	// 	if(!by_account)
	// 	{
	// 		$('#by_account').select2('open');
	// 		return false;
	// 	}
	// });
	// $('.account_head_class').change(function(){
	// 	var to_account=$('#to_account').val();
	// 	var by_account=$('#by_account').val();
	// 	if(to_account==by_account)
	// 	{
	// 		alert('Please select Diffrent Option');
	// 		$(this).val('');
	// 	}
	// });
	$('#account_head').change(function(){
		//alert("k");
		table = $('#myTable').dataTable();
		table.fnDraw();
	});
</script>