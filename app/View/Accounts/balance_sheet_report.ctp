 <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
    <h1>Balance Sheet</h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary box_tp_brdr">
                <div class="row-wrapper">
                    <div class="row">
                        <?php echo $this->Form->create('AccountHead', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'AccountHead_Form']); ?>   
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                    <!-- <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('executive_id',array('class'=>'form-control select2','type'=>'select','empty'=>'select','options'=>$Executive_list,'id'=>'executive_view_id',)); ?>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-3 col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('main_group_ids',array('class'=>'form-control select2 main_group_id','type'=>'select','empty'=>'All','options'=>$MainGroup_list,'id'=>'main_group_ids','label'=>'Main Group')); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-3" >
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('sub_group_ids',array('class'=>'form-control select2 sub_group_id','type'=>'select','empty'=>'All','options'=>$SubGroup_list,'id'=>'sub_group_ids','label'=>'Sub Group')); ?>
                                            </div>
                                            <br>
                                            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-6" style="display: none"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#group_add_modal"></i></a></div>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-3" style="display: none">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('customer_type',array('class'=>'form-control select2','type'=>'select','empty'=>'select','required','options'=>$CustomerType_list,'id'=>'customer_type',)); ?>
                                            </div>
                                            <br>
                                            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-6"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_type_modal"></i></a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2" style="display: none">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12" >
                                                <?= $this->Form->input('name',array('class'=>'form-control select2 name','type'=>'select','required','empty'=>'select','options'=>$Customer_list,'id'=>'name','label'=>'Customer')); ?>
                                            </div>
                                            <br>
                                            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_modal_add"></i></a></div>
                                        </div>
                                    </div>
                             <div class="text-right" style="margin-top: 1.5%;margin-right: 2%;display: none">
                   <!--  <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#adddivision"></i></a> -->
                   <button data-toggle="modal"  data-target="#customer_modal_add" class="btn btn-success">Create Ledger</button>   
                    
                  </div>

                                </div>

                            </div>

                            <?= $this->Form->end(); ?>
                        </div>
                        <br>
                        <div class="row-wrapper">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body table-responsive no-padding" style="margin-left:20px;margin-right:50px;">
                                        <table class="table table-condensed table table boder" id="AccountHead_table" border="1" >
                                            <thead>
                                                <tr class="blue-bg">
                                                    <!-- <th>Executive </th> -->
                                                    <th>Code</th>
                                                    <th>Name</th>
                                                    <th>Main Group</th>
                                                    <th>Sub Group</th>
                                                    <th>Debit</th>
                                                    <th>Credit</th>
                                                    <th>Balance</th>
                                                    <!-- <th width="20%"></th> -->
                                                    <!-- <th width="20%" style="display:none"></th>  -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                             
                                        </tbody>
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="customer_type_modal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Customer Type</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Customer Type</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="customer_type_name" type="text">
                                        </div>
                                        <br>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id='add_customer_type'>Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <div id="Location_Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-body">
      <div class="box-body table-responsive no-padding">
    
    <div id="map"></div>
      </div>
      </div>
      
    </div>
  </div>
</div>
<?php require('ledger_edit_modal.php') ?> 
<?php require('ledger_add_modal.php') ?> 
<?php require('state_modal.php') ?>
<?php require('main_group_modal.php') ?>
<?php require('sub_group_modal.php') ?>
</section>
<?php require('general_journal_transaction_trial_modal.php'); ?>
<script type="text/javascript">
   $(document).ready(function(){
   // alert("k");
    $.fn.search();
  });
   // $('#main_group_ids').change(function () {
   //      var main_group_ids = $(this).val();
   //      $.fn.search();
   //  });
    $('#sub_group_ids').change(function () {
        var sub_group_ids = $(this).val();
        $.fn.search();
    });
 $.fn.search = function() {
   // alert('K');
    // var from_date=$('#from_date').val();
   var main_group_ids=$('#main_group_ids').val();
    var sub_group_ids=$('#sub_group_ids').val();
    // var to_date=$('#to_date').val();
    var data={
        main_group_ids:main_group_ids,
        sub_group_ids:sub_group_ids
     // to_date:to_date,
     // from_date:from_date
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Accounts/BalanceSheet_Table_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {
        $('#AccountHead_table').DataTable().destroy();
        $('#AccountHead_table tbody').html(response.row);
        $('#AccountHead_table').DataTable( {
         "ordering": false,
        dom: 'Bfrtip',
lengthMenu: [
    [25, 50,100,-1],
    ['25 rows', '50 rows','100 rows','Show all' ]
    ],
buttons: [
 { extend: 'colvis', },
//'print',

{
extend: 'print',
footer: true,
//exportOptions: { columns: ':visible' },
customize: function ( win ) {
$(win.document.body)
.css( 'font-size', '8pt' )
.prepend(
// '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
// '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
// '<h5 align="center">Thiruvangoor</h5',
// '<h5 align="center">Calicut-673 304</h5>',
// '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
'<h3 align="center">Ledger</h3>',
//'<h5>CustomerType :'+$('#customer_type_id').val()+'</h5>',
//'<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+'</h5>'
);
$(win.document.body).find( 'table' )
.addClass( 'compact' )
.css( 'font-size', 'inherit' )
// .prepend(
//   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
//   )
},
exportOptions: { columns: ':visible'},

},
{
//extend: 'csv',
// title:"Customer Due List Route Wise Report",
//footer: true,
extend: 'csvHtml5',
title:"Ledger",
footer: true,
customize: function (csv) {
return "\t   Ledger\n"+  csv ;
},
exportOptions: { columns: ':visible'},
// exportOptions: { columns: ':visible' },
//  title:'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
},
 'pageLength',
],
 "columnDefs": [
    // {"targets": [ 4 ],"visible": false,},
    // {"targets": [ 5 ],"visible": false,},
    // {"targets": [ 6 ],"visible": false,},
    // {"targets": [ 7 ],"visible": false,},
    // {"targets": [ 8 ],"visible": false,},
    // {"targets": [ 9 ],"visible": false,},
    // {"targets": [ 10 ],"visible": false,},
    //  {"targets": [ 11 ],"visible": false,},
    //   {"targets": [ 12 ],"visible": false,},
    //    {"targets": [ 13 ],"visible": false,},
    //    {"targets": [ 15 ],"visible": false,},
    //    {"targets": [ 16 ],"visible": false,},
    //    {"targets": [ 17 ],"visible": false,},
    //    {"targets": [ 18 ],"visible": false,},
    ],
      } );
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  };
      
  </script>

<script type="text/javascript">
  
    
     $('#add_main_group').click(function(){
  var modal_main_group_name=$('#modal_main_group_name').val();
  var modal_main_group_code=$('#modal_main_group_code').val();
  
  var data={
    name:modal_main_group_name,
    code:modal_main_group_code,
  };
  var website_url= '<?php echo $this->webroot; ?>Accounts/add_main_group_ajax_modal';
 
  $.ajax({
    method: "POST",
        url: website_url,
        data: data,
        dataType:'json',
  }).done(function( data ) {
    if(data.result!='Success')
        {
            alert(data.result);
            return false;
        }
        $('#addmain_group').modal('toggle');
        $('#modal_main_group_name').val('');
        $('#modal_main_group_code').val('');
        $('.main_groups').append($("<option></option>").attr("value",data.key).text(data.value));
        $('.main_groups').val(data.key).trigger('change');
  });
});
      $('#add_sub_group').click(function(){
 var main_group_id=$('#main_group_id').val();
  var modal_sub_group_name=$('#modal_sub_group_name').val();
  var modal_sub_group_code=$('#modal_sub_group_code').val();
  
  var data={
    main_group_id:main_group_id,
    name:modal_sub_group_name,
    code:modal_sub_group_code,
  };
  var website_url= '<?php echo $this->webroot; ?>Accounts/add_sub_group_ajax_modal';
 
  $.ajax({
    method: "POST",
        url: website_url,
        data: data,
        dataType:'json',
  }).done(function( data ) {
    if(data.result!='Success')
        {
            alert(data.result);
            return false;
        }
        $('#addsub_group').modal('toggle');
        $('#modal_sub_group_name').val('');
        $('#modal_sub_group_code').val('');
        $('.sub_groups').append($("<option></option>").attr("value",data.key).text(data.value));
        $('.sub_groups').val(data.key).trigger('change');
  });
});
    $('.opening_balance').on('keyup change', function () {
        $('#opening_balance').val($(this).val());
        $('#opening_balance_modal').val($(this).val());
    });
    $(document).on('change', '#customer_type,#modal_customer_type', function () {
        $('#modal_customer_type').val($(this).val()).trigger('change.select2');
        $('#customer_type').val($(this).val()).trigger('change.select2');
    });
</script>
<!-- for edit  -->
<script type="text/javascript">
     $(document).on('click','.edit_priority',function(){
        var priority=$(this).closest('tr').find('td input.priority_edit').val();
        var edit_id= $. trim($(this).closest('tr').find('td span.AccountHead_id').text());
        $.post("<?= $this->webroot ?>Accountings/update_customer_priority/"+edit_id+'/'+priority,function(response)
        {
           if(response.status!="Success")
           {
            alert(response.status);
                return false;
           }
           else{
            location.reload();
           }

        },"json");
        
    });
     $(document).on('click', '.edit_head', function () {
        //var id = $(this).closest('tr').find('td span.AccountHead_id').text();
        var id=$(this).attr('table_id');
        var sub_group_ids = $(this).closest('tr').find('td span.SubGroup_id').text();
        //alert(sub_group_ids);
        $.post("<?= $this->webroot ?>Accounts/Ledger_get_ajax/" + id +'/'+sub_group_ids, function (responds) {
            console.log(responds);
            if (responds.result != 'Success')
            {
                alert(responds.message);
                return false;
            }

            $('#name_edit').val(responds.name);
            //console.log(responds.data.Customer.arabic_name);
           // $('#name_edit_arabi').val(responds.data.Customer.arabic_name);
            $('#opening_balance_edit').val(responds.opening_balance).trigger('change');
            $('#customer_type_id_edit').val(responds.customer_type_id).trigger('change');
            $('#route_id_edit').val(responds.route_id).trigger('change');
            $('#customer_group_id_edit').val(responds.customer_group_id).trigger('change');
            $('#sub_group_id_edit').val(responds.sub_group_id).trigger('change');
            $('#main_group_id_edit').val(responds.main_group_id).trigger('change');
             $('#status_edit').val(responds.status).trigger('change');
             $('#cost_center_edit').val(responds.cost_center).trigger('change');
           // $(".sub_groupdiv").css("pointer-events",'none');
             $('#district_edit').val(responds.district).trigger('change');
            $('#pin_code_edit').val(responds.pin_code);
            $('#state_edit').val(responds.state).trigger('change');
            $('#country_edit').val(responds.country).trigger('change');
            $('#code_edit').val(responds.code);
            $('#email_edit').val(responds.email);
            $('#website_edit').val(responds.website);
            $('#telephone_edit').val(responds.telephone);
            $('#address_edit').val(responds.address);
            $('#other_notes_edit').val(responds.other_notes);
            $('#customer_id_hide').val(responds.AccountHead_id);
            $('#AccountHead_id').val(responds.AccountHead_id);
           // $('#place_edit').val(responds.data.Customer.place);
            $('#contact_person_edit').val(responds.contact_person);
            $('#contact_no_edit').val(responds.contact_no);
            $('#credit_limit_edit').val(responds.credit_limit);
            $('#credit_period_edit').val(responds.credit_period);
           // $('#division_edit').val(responds.data.Customer.division_id);
            $('#gstin_edit').val(responds.gstin);
      

        }, "json");
    });
    $(document).on('click', '#edit_customer_button', function () {
         var data=$('#CustomerEdit').serialize();
  //console.log(data);
  $.post( "<?= $this->webroot ?>Accountings/Customer_account_edit",data ,function( data ) {
    if(data.result!='Success')
     {
       alert(data.result);
       return false;
     }
    // $('#Route_Form')[0].reset();
     //location.reload();
    // $('#add_location').modal('toggle');
   }, "json");
        });
</script>
<!-- For delete -->
<script type="text/javascript">
    $(document).on('click', '.delete', function () {
        if (!confirm("Are you sure?"))
        {
            return false;
        }
        var id = $(this).closest('tr').find('td span.AccountHead_id').text();
        var rowindex = $(this).closest('tr').index();
        $.post("<?= $this->webroot ?>Accountings/customer_delete/" + id, function (data) {
            if (data.result != 'Success')
            {
                alert(data.message);
                return false;
            }
            $('#AccountHead_table tbody tr:eq(' + rowindex + ')').remove();
        }, "json");
    });
</script>
<script type="text/javascript">
    $('#add_customer_type').click(function () {
        var name = $('#customer_type_name').val();
        if (name == '')
        {
            $('#customer_type_name').focus();
            return false;
        }
        var website_url = '<?php echo $this->webroot; ?>CustomerType/add_customer_type_ajax';
        var data = {
            name: name,
        };
        $.ajax({
            method: "POST",
            url: website_url,
            data: data,
            dataType: 'json',
        }).done(function (data) {
            if (data.result != 'Success')
            {
                alert(data.result);
                return false;
            }
            $('#customer_type_modal').modal('toggle');
            $('#customer_type_name').val('');
            $('#customer_type').append($("<option></option>").attr("value", data.key).text(data.value));
            $('#modal_customer_type').append($("<option></option>").attr("value", data.key).text(data.value));
            $('#customer_type').val(data.key).trigger('change');
            $('#modal_customer_type').val(data.key).trigger('change');
        });
    });
</script>
<script type="text/javascript">
    $('#executive_view_id').change(function () {
        var executive_id = $(this).val();
        $.fn.executive_change(executive_id);
        $.fn.table_search();
    });
    $.fn.executive_change = function (executive_id) {
        var data = {executive_id: executive_id};
        var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/executive_select_ajax';
        $.ajax({
type: "post", // Request method: post, get
url: url_address,
data: data, // post data
success: function (response) {
    $('#route_view_id').html(response);
},
error: function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
}
});
    }
    $('#route_view_id').change(function () {
        var route_id = $(this).val();
        $.fn.route_change(route_id);
        $.fn.table_search();
    });
    $.fn.route_change = function (route_id) {
        var data = {route_id: route_id};
        var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/route_select_ajax';
        $.ajax({
            type: "post",
            url: url_address,
            data: data,
            success: function (response) {
//  $('#day_view').html(response);
$('#group_view').html(response);
},
error: function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
}
});
    }

$('#group_view').change(function () {
    var group = $('#group_view option:selected').val();
    var route_id = $('#route_view_id option:selected').val();
    $.fn.group_change(group, route_id);
    $.fn.table_search();
});
$.fn.group_change = function (group, route_id) {
    var data = {group: group, route_id: route_id};
    var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/group_select_ajax';
    $.ajax({
        type: "post",
        url: url_address,
        data: data,
        success: function (response) {
            $('#customer_type').html(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
}
//
$.fn.day_change = function (day, route_id) {
    var data = {day: day, route_id: route_id};
    var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/day_select_ajax';
    $.ajax({
        type: "post",
        url: url_address,
        data: data,
        success: function (response) {
            $('#customer_type').html(response);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
}
$('#customer_type').change(function () {
    var customer_type = $(this).val();
    var executive_id = $('#executive_view_id option:selected').val();
    var route_id = $('#route_view_id option:selected').val();
    var day = $('#day_view option:selected').text();
    $.fn.customer_type_change(customer_type, executive_id, route_id, day);

    $.fn.table_search();
});
$.fn.customer_type_change = function (customer_type, executive_id, route_id, day) {
    var data = {customer_type: customer_type, executive_id: executive_id, route_id: route_id,
// day: day
};
var url_address = '<?php echo $this->webroot; ?>' + 'Accountings/customer_type_select_ajax';
$.ajax({
    type: "post",
    url: url_address,
    data: data,
    success: function (response) {
        $('#name').html(response);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
    }
});
}
$('#name').change(function () {
    $.fn.table_search();
});

$(document).on('click', '#ledger_update_button', function () {
        var website_url = '<?php echo $this->webroot; ?>Accounts/Ledger_account_edit';
        var data=$('#LedgerEdit_Form').serialize();
        $.ajax({
            method: "POST",
            url: website_url,
            data: data,
            dataType:'json',
        }).done(function (data) {
            if (data.result != 'Success')
            {
                alert(data.result);
                return false;
            }
            location.reload();
        });
    });

$(document).on('change','#main_group_ids',function(){
  var main_group_ids=$(this).val();
  $.post( "<?= $this->webroot ?>Accounts/GetSubGroup/"+main_group_ids ,function( data ) {
   // $('#sub_group_ids').html('');
    $('#sub_group_ids').empty();
      $('#sub_group_ids').append($("<option></option>").attr("value",'').text('All'));
    $.each(data.options,function(key,value){
       //$('#sub_group_ids').append($("<option></option>").attr("value",key).text('All'));
      $('#sub_group_ids').append($("<option></option>").attr("value",key).text(value));
      //$.fn.search();
    })
    $.fn.search();
  }, "json");
});


</script>
<!-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFVITJEAwer4-t9yNbDpJBd_n1j8-YQ1U&callback=initMap">
    </script> -->
<script type="text/javascript">
    <?php require('state.js'); ?>
              <?php require('trial_balance_report.js'); ?>

</script>
