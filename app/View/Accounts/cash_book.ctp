
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1> CashBook </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary box_tp_brdr">
        <div class="row-wrapper">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="box-body">
                  <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <div class="form-group">
                      <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$from_date)); ?>
                    </div>
                  </div>
                  <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <div class="form-group">
                      <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$to_date)); ?>
                    </div>
                  </div>
                   <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <div class="form-group">
                  <?= $this->Form->input('cash_id',array('type'=>'select','id'=>'cash_id','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>$AccountHead,'default' =>'Select','label'=>'Cash Head')); ?>
                  </div>
                   </div>
                  <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <div class="form-group"><br>
                      <button class='btn btn-success' type="button" id='fetch_button'>Get</button>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <div class="row-wrapper">
                <div class="row">
                  <div class="col-md-12">
                    <div class="box-body table-responsive no-padding xs_tp">
 <table class="table table-bordered table-hover" id="transaction_details_table" width="100%">
                        <thead>
                          <tr class="blue-bg">
                            <th>Date</th>
                            <th>Particulars</th>
                            <th>Voucher No</th>
                            <th>Narration</th>
                            <th class="text-right">Debit</th>
                            <th class="text-right">Credit</th>
                            <th class="text-right">Balance</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require('general_journal_transaction_cash_modal.php'); ?>
<style>
/*tr.day-book:first-child {
  color: #a52130;
  font-weight: 600;
  font-size: 15px;
}*/
</style>
<script type="text/javascript">
$(document).ready(function(){
    $("#fetch_button").click(); 
});
$.fn.search_transaction_ajax=function(data_array){
  $.post( "<?= $this->webroot ?>Accounts/CashBook_ajax",data_array ,function( data ) {
    $('#transaction_details_table').DataTable().destroy();
    $('#transaction_details_table tbody').empty();
    $('#transaction_details_table tfoot').empty();
    $('#transaction_details_table tbody').html(data.row.tbody);
    $('#transaction_details_table tfoot').html(data.row.tfoot);
    $('#transaction_details_table').DataTable({
      "bSort": false,
      // 'paging':false,
      'searching': true,
      'info': false,
      dom: 'Bfrtip',
      lengthMenu: [
          [10, 25, 50, 100, -1],
          ['10 rows', '25 rows', '50 rows', '100 rows', 'Show all']
      ],
      buttons: [
      {
       extend: 'print',
       footer: true,
       exportOptions: { columns: [0,1,2,3,4,5] },
       customize: function ( win ) {
        $(win.document.body)
        .css( 'font-size', '10pt' )
        .prepend( '<tr><td colspan="8"><h4>'+$('#account_holder_name').text()+'</h4></td></tr>' )
        .prepend( '<h5>Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+'</h5>' );
        $(win.document.body).find( 'table' )
        .addClass( 'compact' )
        .css( 'font-size', 'inherit' )
      }
    },
    {
     extend: 'csv',
     footer: true,
     exportOptions: {
      columns: [0,1,2,3,4,5]
    },
  },
  {
   extend: 'excel',
   footer: true,
   exportOptions: { columns: [0,1,2,3,4,5]
   },
   customize: function (xlsx) {
    var sheet = xlsx.xl.worksheets['sheet1.xml'];
    var numrows = 5;
    var clR = $('row', sheet);
    clR.each(function () {
      var attr = $(this).attr('r');
      var ind = parseInt(attr);
      ind = ind + numrows;
      $(this).attr("r",ind);
    });
    $('row c ', sheet).each(function () {
      var attr = $(this).attr('r');
      var pre = attr.substring(0, 1);
      var ind = parseInt(attr.substring(1, attr.length));
      ind = ind + numrows;
      $(this).attr("r", pre + ind);
    });
    function Addrow(index,data) {
      msg='<row r="'+index+'">'
      for(i=0;i<data.length;i++){
        var key=data[i].key;
        var value=data[i].value;
        msg += '<c t="inlineStr" r="' + key + index + '">';
        msg += '<is>';
        msg +=  '<t>'+value+'</t>';
        msg+=  '</is>';
        msg+='</c>';
      }
      msg += '</row>';
      return msg;
    }
    var r1 = Addrow(1, [{ key: 'A', value: '' }]);
    var r2 = Addrow(2, [{ key: 'A', value: 'Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+'' }]);
    var r3 = Addrow(3, [{ key: 'A', value: '' }]);
    var r4 = Addrow(4, [{ key: 'A', value: ''+$('#account_holder_name').text()+'' }]);
    var r5 = Addrow(5, [{ key: 'A', value: '' }]);
    sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2+ r3+ r4+ r5+ sheet.childNodes[0].childNodes[1].innerHTML;
  }
  },
  'pageLength',
  ],
  "columnDefs": [
  {"width": "20%",   "targets": 0},
  {"width": "30%",   "targets": 1},
  {"width": "30%",   "targets": 4}
  ],
  });
  }, "json");
};
 $(document).on('click','.daybook_delete',function(){
    if(!confirm("Are you sure?"))
    {
      return false;
    }
    var journal_id=$(this).closest('tr').find('td span.journal_id').text();
    $.post( "<?= $this->webroot ?>Accountings/Journal_delete/"+journal_id,function( data ) {
      if(data.result!='Success')
      {
        alert(data.message);
        return false;
      }
      table = $('#table_data').dataTable();
      table.fnDraw();
    }, "json");
  });
$(document).on('click','#fetch_button',function(){
  var from_date=$('#from_date').val();
  var to_date=$('#to_date').val();
  var cash_id=$('#cash_id').val();
  // var name=$('#account_holder_name').text();
  var data_array={
    from_date:from_date,to_date:to_date,cash_id:cash_id
  }
  $.fn.search_transaction_ajax(data_array);
});

</script>