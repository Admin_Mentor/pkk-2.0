<section class="content-header">
	<h1>Cost Center</h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?php echo $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
			<div class="col-md-12" style="display: none">
				<div class="form-group">
					<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
					</div>
					 <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" style="pointer-events:none;">
                <?= $this->Form->input('journal_no',array('class'=>'form-control','type'=>'text','id'=>'journal_no','label'=>'Document No')); ?>
              </div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?php echo $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','id'=>'account_head','empty'=>'All','options'=>$AccountHead_list,'required','label'=>'Acc/Name',)); ?>
					</div>
					
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher')) ?>
					</div>
				</div>
			</div>
					<div class="col-md-12" style="display: none">
				<div class="form-group">
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?php echo $this->Form->input('mode_category_ids',array('type'=>'select','class'=>'form-control select2','id'=>'mode_category_ids','style'=>'width: 100%;','options'=>$mode_category,'empty'=>'Select','required','label'=>'Mode Category')); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 cash" style="display: none" >
						<?php echo $this->Form->input('mode',array('type'=>'select','class'=>'form-control select2','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 cheque" style="display: none">
						 <?= $this->Form->input('mode',array('type'=>'select','id'=>'mode','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>array($banks),'label'=>'Bank')); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 cheque" style="display: none">
					<?= $this->Form->input('cheque_no',array('class'=>'form-control','type'=>'text','id'=>'cheque_no','label'=>'Cheque No')); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 cheque" style="display: none">
						<?php echo $this->Form->input('cheque_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'cheque_date','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Cheque Date')); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 bank" style="display: none">
						 <?= $this->Form->input('mode',array('type'=>'select','id'=>'mode','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>array($banks),'label'=>'Bank')); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 bank" style="display: none">
						 <?= $this->Form->input('reference_no',array('class'=>'form-control','type'=>'text','id'=>'reference_no','label'=>'Reference No')); ?>
					</div>
				</div>
			</div>
			<div class="col-md-12" style="display: none">
				<div class="form-group">
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
						<?= $this->Form->input('amount',array('class'=>'form-control number text-right','type'=>'text','required','id'=>'amount',)); ?>
					</div>
					<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" style="display:none">
						<?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
					</div>
					<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
						<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','required','id'=>'remarks','label'=>'Remarks')) ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                <?= $this->Form->input('currency',array('class'=>'form-control','type'=>'text','required','id'=>'currency','label'=>'Currency','readonly')); ?>
              </div>
					<div class="col-md-1"> <br>
						<button class="user_add_btn">Save & Print</button>
					</div>
				</div>
			</div>
			<!-- <div class="col-md-12">
				<div class="form-group"> -->
			<div class="text-right" style="margin-top: 1%;margin-right: 2%">
                   <!--  <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#adddivision"></i></a> -->
                   <button data-toggle="modal"  data-target="#cost_center_add_modal" class="btn btn-success">Create Cost Center</button> 
                  <!--  </div>
                   </div>   -->
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body table-responsive no-padding" style="margin-top: 1%">
			<table class="boder table table-condensed table boder" id="myTable">
				<thead>
					<tr class="blue-bg">
						<!-- <th class="padding_left">Date</th> -->
						<th>Name</th>
						<!-- <th style="text-align:left">Total</th> -->
					</tr>
				</thead>
				<tbody>
					
			</tbody>
			<tfoot>
				<!-- <tr>
						<th colspan="1" style="font-size:20px; color:red;text-align:right">Total:</th>
						<th class="text-right" style="font-size:20px; color:red;">0</th>
					</tr> -->
			</tfoot>
		</table>
	</div>
</div>
</section>
<?php require('general_journal_transactions_modal.php') ?>
<?php require('cost_center_modal.php') ?>
<script type="text/javascript">
	$('#myTable').DataTable( {
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Accounts/CostCenter_table_ajax",
			//"type": "POST",
			"type": "POST",data:function( d ) {
				//d.customer_type_id= $('#customer_type').val();
				d.account_head_id= $('#account_head').val();
			},
			"dataSrc": "records",
		},
		 dom: 'Bfrtip',
     lengthMenu: [
     [10,25, 50,100,-1],
     ['10 rows','25 rows', '50 rows','100 rows','Show all' ]
     ],
    buttons: [
    //{ extend: 'colvis', },
      {
          extend: 'print',
          // text: 'Print' ,
          // title: 'Executive Brand Wise Sale Report',
           footer: true,
       exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                   
           '<h3 align="center">Cost Center </h3>',
            
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
                // .prepend(
                //   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
                //   )
              }
            },
            {
              extend: 'csv',
              // text: 'CSV' ,
            title:'Cost Center ',

               footer: true,
               customize: function (csv) {
                 return "                     "+"\tCost Center \n"+  csv ;
               },
       exportOptions: { columns: ':visible'},
            },
            {
              extend: 'excel',
              // text: 'Excel' ,
              // title:'Executive Brand Wise Sale Report',
               title:'Cost Center',

               footer: true,
       exportOptions: { columns: ':visible'},
            },
       //      {
       //        extend: 'pdf',
       //       // text: 'Pdf' ,
       //        //title:'Executive Brand Wise Sale Report',
       //        title:'Customer Profit'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',

       //         footer: true,
       // //exportOptions: { columns: ':visible'},
       //      },
    'pageLength',
    ],
		"columns": [
		//{ "data" : "AccountHead.created_at" },
		{ "data" : "CostCenter.name" },
		//{ "data" : "AccountHead.total" },
		],
		// "footerCallback": function ( row, data, start, end, display ) {
		// 	var api = this.api(), data;
		// 	var intVal = function ( i ) {
		// 		return typeof i === 'string' ?
		// 		i.replace(/[\$,]/g, '')*1 :
		// 		typeof i === 'number' ?
		// 		i : 0;
		// 	};
		// 	total = api.column( 1, { page: 'current'} ).data().reduce( function (a, b) {
		// 		return intVal(a) + intVal(b);
		// 	}, 0 );
		// 	$( api.column( 1 ).footer() ).html(''+parseFloat(total.toFixed(3))+'');
			
			
		// },
		"columnDefs": [
		{ className: "name", "targets": [ 0 ] },
		//{ className: "text-right", "targets": [ 1 ]},
		//{ className: "text-right", "targets": [ 2 ],"bSortable": false },
		],
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$(nRow).addClass('view_transactions');
			return nRow;
		}
	});
	// $('button[type="Submit"]').click(function(){
	// 	var to_account=$('#to_account').val();
	// 	if(!to_account)
	// 	{
	// 		$('#to_account').select2('open');
	// 		return false;
	// 	}
	// 	var by_account=$('#by_account').val();
	// 	if(!by_account)
	// 	{
	// 		$('#by_account').select2('open');
	// 		return false;
	// 	}
	// });
	// $('.account_head_class').change(function(){
	// 	var to_account=$('#to_account').val();
	// 	var by_account=$('#by_account').val();
	// 	if(to_account==by_account)
	// 	{
	// 		alert('Please select Diffrent Option');
	// 		$(this).val('');
	// 	}
	// });
	$('#account_head').change(function(){
		//alert("k");
		table = $('#myTable').dataTable();
		table.fnDraw();
	});

$('#mode_category_ids').change(function(){
//alert('k');
  var mode_category_id=$(this).val();
//alert(mode_category_id);
 // var data={sub_group_id,sub_group_id};
 //var url_address= "<?= $this->webroot; ?>Accounts/GetSystemParameter/"+mode_category_id;
 // $.post( url_address,data, function( response ) {
     //console.log(response) ;

    if(mode_category_id==2)
    {

      $('.bank').hide();
      $('.cheque').show();
      $('.cash').hide();
     // $('#main_group_id_edit').val(response.option);
     // $('#main_group_id_edit').attr('readonly',true);
     //$('#main_group_id_edit').val('');
    }
    else if(mode_category_id==3)
    {
     
     $('.bank').show();
     $('.cheque').hide();
      $('.cash').hide();
     //$('#main_group_id_edit').val(response.option);
      //$('#main_group_id_edit').attr('readonly',true);
       //$('#main_group_id_edit').val('');

    }
    else{
      $('.bank').hide();
      $('.cheque').hide();
       $('.cash').show();

    }

 // },'json');
//}
});
</script>
<script type="text/javascript">
	var flash='<?= $flash=$this->Session->flash(); ?>';
	if(flash){
		var current_url=window.location.href;
		var url=current_url.split("/");
		var length=url.length;
		var id=url[length-2];
		var amount=url[length-1];
		if(id){
			//alert(id);
			var link = "<?= $this->webroot.'Print/receipt_voucher_fpdf/'?>"+id+'/'+amount;
			window.open(link, '_blank');
		}
	}
// 	$( document ).ready(function() {
//     $('#account_head').select2("open");
// });
$('#add_cost_button').click(function(){
  var name=$('#cost_center_name_modal').val();
  if(name=='')
  {
    $('#cost_center_name_modal').focus();
    return false;
  }
  // var code=$('#state_code_modal').val();
  // if(code=='')
  // {
  //   $('#state_code_modal').focus();
  //   return false;
  // }
  var website_url='<?php echo $this->webroot; ?>Accounts/add_cost_center_ajax';
  var data={
    name:name,
   // code:code,
  };
  $.ajax({
    method: "POST",
    url: website_url,
    data: data,
    dataType:'json',
  }).done(function( data ) {
    if(data.result!='Success')
    {
      alert(data.result);
      return false;
    }
   // $('#cost_center_add_modal').modal('toggle');
   // $('#cost_center_name_modal').val('');
    //$('#state_code_modal').val('');
   // $('#cost_center_id').append($("<option></option>").attr("value",data.key).text(data.value));
  //  $('#cost_center_id').val(data.key).trigger('change');
    location.reload();


  });
});
</script>