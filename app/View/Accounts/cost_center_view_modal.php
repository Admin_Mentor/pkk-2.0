<style type="text/css">
    .top_mdl_pop{
        margin-top: 29px;
    }
    .buton_click_advance {
        background-color: #005e6d;
        color: white;
        padding-top: 13px;
        padding-bottom: 13px;
        padding-left: 15px;
        padding-right: 15px;
        border-radius: 3px !important;
        cursor: pointer;

    }
    /*new lines */
    .TriSea-technologies-Switch > input[type="checkbox"] {
        display: none;
    }

    .add_btn_styl {
        padding-top: 11px;
        padding-bottom: 11px;
        padding-left: 15px;
        padding-right: 15px;
        letter-spacing: 0.6px;
        border-radius: 3px !important;
    }

    .TriSea-technologies-Switch > label {
        cursor: pointer;
        height: 0px;
        position: relative;
        width: 40px;
    }

    .form-group-1{
        margin-bottom: 15px !important;
    }

    .TriSea-technologies-Switch > label::before {
        background: rgb(0, 0, 0);
        box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
        border-radius: 8px;
        content: '';
        height: 16px;
        margin-top: -8px;
        position:absolute;
        opacity: 0.3;
        transition: all 0.4s ease-in-out;
        width: 40px;
    }
    .TriSea-technologies-Switch > label::after {
        background: rgb(255, 255, 255);
        border-radius: 16px;
        box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
        content: '';
        height: 24px;
        left: -4px;
        margin-top: -8px;
        position: absolute;
        top: -4px;
        transition: all 0.3s ease-in-out;
        width: 24px;
    }
    .TriSea-technologies-Switch > input[type="checkbox"]:checked + label::before {
        background: inherit;
        opacity: 0.5;
    }
    .TriSea-technologies-Switch > input[type="checkbox"]:checked + label::after {
        background: inherit;
        left: 20px;
    }
</style>
<div class="modal fade" id="CostCenter_View_modal" tabindex="" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Cost Center</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <?php echo $this->Form->create('CostCenterAdd',array('id'=>'Product_Add_Form','onsubmit'=>'return false')); ?>
                    <input type="hidden" id="prod_id">
                    <input type="hidden" id="count">
                    <input type="hidden" id="slno">
                </div>
                <div class="row" id="rolDiv" style="margin-top: 1%;">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="box-body table-responsive no-padding" >
                            <table class="table table-hover boder no-footer" id="productslno_view_table">
                                <thead>
                                <tr class="blue-bg">
                                    <th style="width: 30%;" >Cost Center</th>
                                    <th style="width: 50%;">Amount</th>
                                    <!-- <th style="width: 22%;alignment: center;">Action</th> -->
                                </tr>
                                </thead>
                                <tbody id="">
                                <tr>
                                    <td class="cost_center_id"><?php echo $this->Form->input('cost_center_id',['type'=>'select','style'=>'width:100%','id'=>'cost_center_id','class'=>'form-control select_two_class cost_center_id','empty'=>[''=>'Select'],'options'=>$CostCenter,'label'=>false]); ?></td>
                                    <td class="Cost_amount"><input type="text" id="cost_amount" name="cost_amount" class="form-control cost_amount"></td>
                                    <!-- <td><i style="cursor:pointer;" class="fa_purchase fa fa-plus-circle create_icon fa-2x add_costcenter"></i></td> -->
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success" data-dismiss="modal" id="add_sale_size_qty">Close</button>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
</div>
<script type="text/javascript">
    $( "#Product_Add_modal" ).on('shown.bs.modal', function (e) {
        $("#ProductAddName").focus();
        $("#party_div").attr('disabled',true);
        $("#party_ids").val($("#party_id").val()).trigger('change');
    });
    shortcut.add("alt+s", function() {
        $.fn.button_disable();
        $('#Product_Add_Btn').click();
    });
    $(document).on('click','#StockAdvanceBtn',function(){
        if($('#StockAdvance').is(":visible")){
            $('#StockAdvance').hide();
        }else{
            $('#StockAdvance').show();
        }
    });
    $(document).on('click','#RolBtn',function(){
        if($('#rolDiv').is(":visible")){
            $('#rolDiv').hide();
        }else{
            $('#rolDiv').show();
        }
    });
    $(document).on('change','#barcodeOn',function()
    {
        var status = $(this).prop('checked');
        if(status==true)
        {
            $.fn.auto_generated_barcode();
            $('.barcode-div').show();
        }
        else
        {
            $('.barcode-div').hide();
        }
    });

    $(document).on('change','#custombarcodeOn',function(){
        var status = $(this).prop('checked');
        if(status==true)
        {
            $('#custom_barcode').show();
            $('#product_barcode').hide();
        }else{
            $('#product_barcode').show();
            $('#custom_barcode').hide();
        }
    });
    product_barcode='';
    $.fn.auto_generated_barcode=function(){
        var values =
        {
            'product_type' : $("#product_type_id_modal").val(),
            'sub_type':$("#type_id_modal").val(),
            'brand':$("#Brand_id_modal").val(),
            'model':$("#Model_id_modal").val(),
            'specification':$("#specification_id_modal").val(),
            'vendor':$("#party_ids").val(),
            'other':$("#other_codes").val(),
        }
    }
    $('#barcodeOn').attr('checked',false);
    $('#barcodeOn').change();
    $('#custombarcodeOn').attr('checked',false);
    $('#custombarcodeOn').change();
    $("#barcodeOn,#product_type_id_modal,#type_id_modal,#Brand_id_modal,#Model_id_modal,#specification_id_modal,#party_ids,#other_codes").change(function()
    {
        var value = {
            'product_type' : $("#product_type_id_modal").val(),
            'sub_type' : $("#type_id_modal").val(),
            'brand' : $("#Brand_id_modal").val(),
            'model' : $("#Model_id_modal").val(),
            'specification' : $("#specification_id_modal").val(),
            'vendor' : $("#party_ids").val(),
            'other' : $("#other_codes").val(),
        }
        var url_address= "<?= $this->webroot; ?>Product/get_product_code";
        $.post( url_address,value,function( response ) {
            if(response.result == 'Success')
            {
                $("#ProductAddProductBarcode").val(response.data.toUpperCase());
                $("#productcode").html(response.data.toUpperCase());
            }
        }, "json");
    });

    $("#cost_amount").keypress(function(e){
        if(e.keyCode == 13)
        {
            $('.add_costcenter').trigger('click');
        }

        // the rest of your code ...
    });
</script>