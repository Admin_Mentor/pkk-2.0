 <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
       #AccountHead_table_filter .input-sm{
    height: 35px !important;
    font-size: 18px !important;
}
#AccountHead_table_filter{margin-top: 25px !important;}
#AccountHead_table_length{margin-top: 25px !important;}
    </style>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
    <h1> Customer Ledger Report </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary box_tp_brdr">
                <div class="row-wrapper">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                     <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group" >
                                                <?= $this->Form->input('executive_id',array('class'=>'form-control select2 executive_id','type'=>'select','empty'=>'All','options'=>$Executive_list,'id'=>'executive_id','label'=>'Executive')); ?>
                                        </div>
                                    </div>
                                     <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group" >
                                                <?= $this->Form->input('route_id',array('class'=>'form-control select2 route_id','type'=>'select','empty'=>'All','options'=>$Route_list,'id'=>'route_id','label'=>'Route')); ?>
                                            </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2" style="display: none">
                                        <div class="form-group" >
                                                <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$from_date)); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group" >
                                                <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$to_date)); ?>
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-6" style="margin-top:0%">
                                        <div class="form-group">
                                          <div class="col-md-4 col-lg-4 col-sm-4" id='cost_visibility' hidden>
                                          <input class='nav-toggle' type="checkbox" id='toggle_button_for_amount_visibility' data-width="130" data-toggle="toggle" data-off="Show Amount" data-on="Hide Amount">
                                          </div>
                                          <div class="col-md-8 col-lg-8 col-sm-8" >
                                          <h1 hidden class="total-amt amount-field">Total Amount : <span id='main_total_amount'>Loading .....</span></h1>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                             </div>   
                     <div class="row-wrapper">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body" style="margin-left:20px;margin-right:50px;">
                                        <table class="table table-condensed table table boder" id="AccountHead_table" border="1" data-order='[[2,"asc"]]' >
                                            <thead>
                                                <tr class="blue-bg">
                                                    <th width="10%">Code</th>
                                                    <th>Name</th>
                                                    <th>Address</th>
                                                    <th>Gstin</th>
                                                    <th style="text-align: right;">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                             
                                        </tbody>
                                       <!-- <tfoot></tfoot> -->
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<?php require('general_journal_transaction_ledger_modal.php'); ?>
<script type="text/javascript">
    $('#route_id,#executive_id,#to_date').change(function () {
         table = $('#AccountHead_table').dataTable();
        table.fnDraw();
    });
     $('#executive_id').change(function(){
    var executive_id=$('#executive_id').val();
    var data={
      executive_id:executive_id,
    };
    var url_address= "<?= $this->webroot; ?>Reports/GetRouteListByExecutive";
    $.post( url_address,data, function( response ) {
      $('#route_id').empty();
      $('#route_id').append($("<option></option>").attr("value",'').text('ALL'));
      $.each(response,function(key,value){
        $('#route_id').append($("<option></option>").attr("value",key).text(value));
      });
    }, "json");
  });
    $('#AccountHead_table').DataTable( {
  "processing": true,
  "serverSide": true,
  "ajax": {
    "url": "<?= $this->webroot ?>Accounts/Customer_Ledger_report_ajax",
    "type": "POST",
    data:function( d ) {
        d.executive_id=$('#executive_id').val();
        d.route_id=$('#route_id').val();
        d.to_date=$('#to_date').val();
      },
      "dataSrc": "records",
    },
      dom: 'Bfrtip',
      "lengthMenu": [[10,25,50,100], [10,25,50,100]],
        buttons: [
           {extend: 'print', title:"Customer Ledger Report", exportOptions: { columns: ':visible' } },
        {extend: 'excel',title:"Customer Ledger Report", exportOptions: { columns: ':visible' } },
        {extend: 'pageLength', },
        ],
    "columns": [
    { "data" : "AccountHead.code" },
    { "data" : "AccountHead.name",},
    { "data" : "AccountHead.address",},
     { "data" : "AccountHead.gstin",},
      { "data" : "AccountHead.balance",className:"text-right"},

    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;
      var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
        i : 0;
      };
      pageTotal = api.column( 1, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 1 ).footer() ).html(''+pageTotal+'');
    },
    "columnDefs": [
    ],
  });
$('#modal_from_date').val($('#from_date').val());
$('#modal_to_date').val($('#to_date').val());
$(document).on('change','#executive_id,#route_id',function(){
     $('#main_total_amount').html('Loading .....')
     var to_date=$('#to_date').val();
       var route_id=$('#route_id').val();
       var executive_id=$('#executive_id').val();
    $.fn.amount_visiblility(route_id,to_date,executive_id);
  });

  $.fn.amount_visiblility= function(route_id,to_date,executive_id){
    var data={
      executive_id:executive_id,
      route_id:route_id,
      to_date:to_date,
    };
    if(route_id!='' || executive_id!= '')
    {
      var url_address= "<?= $this->webroot; ?>Accounts/get_total_amount_customer_wise";
      $.post( url_address,data,function( response ) {
        $('#main_total_amount').html(response);
      }, "json");
      $('.amount-field').show();
    }else{
      $('.amount-field').hide();
    }
  }
</script>
