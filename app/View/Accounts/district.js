$('#add_district_button').click(function(){
	var state_id=$('#state_id_modal').val();
	//alert(state_id);
	var name=$('#district_name_modal').val();
	if(name=='')
	{
		$('#district_name_modal').focus();
		return false;
	}
	var code=$('#district_code_modal').val();
	if(code=='')
	{
		$('#district_code_modal').focus();
		return false;
	}
	var website_url='<?php echo $this->webroot; ?>Accountings/add_district_ajax';
	var data={
		state_id:state_id,
		name:name,
		code:code,
	};
	$.ajax({
		method: "POST",
		url: website_url,
		data: data,
		dataType:'json',
	}).done(function( data ) {
		if(data.result!='Success')
		{
			alert(data.result);
			return false;
		}
		$('#district_add_modal').modal('toggle');
		$('#district_name_modal').val('');
		$('#district_code_modal').val('');
		$('#district_id').append($("<option></option>").attr("value",data.key).text(data.value));
		$('#district_id').val(data.key).trigger('change');
	});
});
$('#district_id').change(function(){
	var id=$(this).val();
	$.get( "<?= $this->webroot ?>Accountings/get_district_code_ajax/"+id,function( data ) {
		$('#state_code').val(data.code);
	}, "json");
});
$('#district_id').trigger('change');