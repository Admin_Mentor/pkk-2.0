<div id="district_add_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add District</h4>
			</div>
			<div class="modal-body">
				<?php echo $this->Form->create('District', ['class'=>'form-horizontal','id'=>'District_Form']); ?>
				<div class="form-group">
					<label class="col-sm-4 control-label">State</label>
					<div class="col-sm-6">
						<?= $this->Form->input('state_id_modal',array('class'=>'form-control select2','type'=>'select','style'=>'width:100%','options'=>$State_list,'id'=>'state_id_modal','label'=>false,'empty'=>'Select')); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Name</label>
					<div class="col-sm-6">
						<?= $this->Form->input('name',array('class'=>'form-control','type'=>'text','required','id'=>'district_name_modal','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Code</label>
					<div class="col-sm-6">
						<?= $this->Form->input('code',array('class'=>'form-control','type'=>'text','required','id'=>'district_code_modal','label'=>false,)); ?>
					</div>
				</div>
				<?= $this->Form->end(); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn_radious" id='add_district_button'>Save</button>
			</div>
		</div>
	</div>
</div>