<style>
        input.show_in_app {
            width: 20px;
            height: 20px;
        }
    </style>
<section class="content-header">
  <h1> Expense
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="box box-primary box_tp_brdr">
        <div class="row-wrapper">
      <div class="col-md-12">
        <div class="row" hidden>
          <div class="col-md-3">
            <div class="col-md-6">
             <?php  echo $this->Form->input('show_in_app', array('type' => 'checkbox','class'=>'checkbox show_in_app', 'id'=>'show_in_app','label'=>'Show for Executive'));?>
                   <?php  echo $this->Form->input('hidden_id', array('type' => 'hidden','class'=>'form-control', 'id'=>'hidden_id','style'=>'width:20%'));?>
            </div>
          </div>
          <div class="col-md-3">
            <div class="col-md-6">
             <?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date','value'=>$date)); ?>
            </div>
          </div>
          <div class="col-md-1"><br>
          <button type="button" class="btn btn-primary edit_button" style="display: none">EDIT</button>
          </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-condensed table table datatable table-bordered" id="myTable">
                          <thead>
                            <tr class="blue-bg">
                              <th class="padding_left">Date</th>
<!--                               <th>Type</th>
 -->                              
<!--                                   <th>Category</th>
 -->                              <th>Account Name</th>
                              <th class="text-right">Opening Balance</th>
                              <th>Show for Executive</th>
                              <th>Description</th>
<!--                               <th>Action</th>
 -->                              <!-- <th>Delete</th> -->
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach ($AccountHead as $key => $value): ?>
                              <tr class="blue-pddng">
                                <td><?= date('d-m-Y',strtotime($value['AccountHead']['created_at'])) ?></td>
                                <!-- <td><?= $value['Group']['name']; ?></td>
                                <td><?= $value['SubGroup']['name']; ?></td> -->
                                <td><span hidden class='AccountHead_id'><?= $value['AccountHead']['id']; ?></span><span><?= $value['AccountHead']['name']; ?></span></td>
                                <td class="text-right" ><?= floatval($value['AccountHead']['opening_balance']); ?></td>
                                <?php 
                                $status="";
                                if($value['AccountHead']['show_in_app']==1)
                                {
                                  $status="checked";
                                }?>
                                <td><input type="checkbox" class="checkbox show_in_app" <?php echo $status;?> id="show_in_app" ></td>
                                <td><?= $value['AccountHead']['description']; ?></td>
                                <!-- <td><i class="fa fa-2x fa-pencil-square-o blue-col edit_head"></i> -->
                                <!-- <a  hidden onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Accountings/AccountHead_delete/<?= $value['AccountHead']['id']; ?>"><i class="fa fa-2x fa-trash blue-col blue-col"></i></a></td> -->
                              </tr>
                            <?php endforeach ?>
                          </tbody>
                        </table>
      </div>
    </div>
  </div>
   </div>
</section>
<script type="text/javascript">
$(document).on('click','.edit_head',function(){
    var id=$(this).closest('tr').find('td span.AccountHead_id').text();
    $.post( "<?= $this->webroot ?>Accountings/ExpenseAccountHeadGet_ajax/"+id, function( data ) {
      var name=data.AccountHead.name;
      var show_in_app=data.AccountHead.show_in_app;
      var split_name=name.split(' ');
      $('#hidden_id').val(id);
      if(show_in_app==1)
      {
      $('#show_in_app').prop('checked',true);
      }
      else
      {
        $('#show_in_app').prop('checked',false); 
      }
      $('.edit_button').css('display','');
    }, "json");
  });
 $(document).on('click','.show_in_app',function(){
       var id=$(this).closest('tr').find('td span.AccountHead_id').text();
      var show_in_app=$(this).closest('tr').find('td input.show_in_app').is(':checked');
       if(show_in_app==false)
      {
      var show_in_app=0;
      }
      else
      {
        var show_in_app=1; 
      }
      var data={id:id,show_in_app:show_in_app}
      var url_address   ='<?= $this->webroot; ?>'+'Accountings/update_expense_app_show';
      var data=data;
      $.ajax({
        type: "post",  
        url:url_address,
        data: data, 
        dataType: 'json', 
        success: function(response) {
          if(response.result!='Success') { alert(response.result);  return false;  }
          location.reload(true);
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) { alert(textStatus); }
      });
    });
</script>