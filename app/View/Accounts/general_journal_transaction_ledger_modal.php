<?php require('datatable_print.php'); ?>
<section class="content">
  <div id="view_transaction_modal" class="modal fade" role="dialog" >
    <div class="modal-dialog modal-lg" style="width: 80%">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title" id='head_name'><span>Statement of Account</span></h3>
          <h4 class="modal-title"><span id='account_id' hidden></span><span id='account_holder_name'></span></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
              <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'modal_from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
              <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'modal_to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
            <div class="col-md-3"><br>
              <button class="btn btn-success" id='fetch_button_journal'>Get</button>
            </div>
          </div>
          <div class="box-body table-responsive no-padding xs_tp">
            <table class="table table-bordered table-hover" id="transaction_details_table" width="100%">
              <thead>
                <tr class="blue-bg">
                  <th>Date</th>
                  <th>Voucher No</th>
                  <th>Particulars</th>
                  <!-- <th>Executive</th>
                  <th>Ext Voucher No</th> -->
                  <th>Refference/Narration</th>
                  <th class="text-right">Debit</th>
                  <th class="text-right">Credit</th>
                  <th class="text-right">Balance</th>
                  <!-- <th>Narration</th> -->
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <div id="view_transaction_by_voucher_no_modal" class="modal fade" role="dialog" >
    <div class="modal-dialog modal-lg" style="width: 70%">
      <div class="modal-content pull-right">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span id='account_holder_name'></span></h4>
        </div>
        <div class="modal-body">
          <div class="box-body table-responsive no-padding xs_tp">
             <table class="table table-bordered table-condensed" id="transaction_details_voucher_table">
              <thead>
                <tr class="blue-bg">
                 <tr class="blue-bg">
                  <th>Date</th>
                  <th>Voucher No</th>
                  <th>Particulars</th>
                  <th>Reference</th>
                  <th>Debit</th>
                  <th>Credit</th>
                  <!-- <th>Voucher No</th> -->
                 
                  <th>Narration</th>
                </tr>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</section>
<style>
#head_name{text-align: center;}
.text-right{text-align: right;}
table.dataTable tfoot td {
  padding: 5px 5px !important;
}
</style>
<script type="text/javascript">
  <?php require('general_journal_transaction_ledger_report.js'); ?>
</script>