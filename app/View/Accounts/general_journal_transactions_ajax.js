$.fn.search_transaction_ajax=function(data_array){
  $.post( "<?= $this->webroot ?>Accounts/general_journal_transactions_ajax",data_array ,function( data ) {
    $('#transaction_details_table').DataTable().destroy();
    $('#transaction_details_table tbody').empty();
    $('#transaction_details_table tfoot').empty();
    $('#transaction_details_table tbody').html(data.row.tbody);
    $('#transaction_details_table tfoot').html(data.row.tfoot);
    $('#transaction_details_table').DataTable({
      "bSort": false,
      dom: 'Bfrtip',
      buttons: [
      {
       extend: 'print',
       footer: true,
       exportOptions: { columns: [0,1,2,3,4,5,6,7,8] },
       customize: function ( win ) {
        $(win.document.body)
        .css( 'font-size', '10pt' )
        .prepend( '<tr><td colspan="8"><h4>'+$('#account_holder_name').text()+'</h4></td></tr>' )
        .prepend( '<h5>Period : From '+$('#modal_from_date').val()+'    To : '+$('#modal_to_date').val()+'</h5>' );
        $(win.document.body).find( 'table' )
        .addClass( 'compact' )
        .css( 'font-size', 'inherit' )
      }
    },
    {
     extend: 'csv',
     footer: true,
     exportOptions: {
      columns: [0,1,2,3,4,5,6,7,8]
    },
  },
  {
   extend: 'excel',
   footer: true,
   exportOptions: { columns: [0,1,2,3,4,5,6,7,8]
   },
   customize: function (xlsx) {
    var sheet = xlsx.xl.worksheets['sheet1.xml'];
    var numrows = 5;
    var clR = $('row', sheet);
    clR.each(function () {
      var attr = $(this).attr('r');
      var ind = parseInt(attr);
      ind = ind + numrows;
      $(this).attr("r",ind);
    });
    $('row c ', sheet).each(function () {
      var attr = $(this).attr('r');
      var pre = attr.substring(0, 1);
      var ind = parseInt(attr.substring(1, attr.length));
      ind = ind + numrows;
      $(this).attr("r", pre + ind);
    });
    function Addrow(index,data) {
      msg='<row r="'+index+'">'
      for(i=0;i<data.length;i++){
        var key=data[i].key;
        var value=data[i].value;
        msg += '<c t="inlineStr" r="' + key + index + '">';
        msg += '<is>';
        msg +=  '<t>'+value+'</t>';
        msg+=  '</is>';
        msg+='</c>';
      }
      msg += '</row>';
      return msg;
    }
    var r1 = Addrow(1, [{ key: 'A', value: '' }]);
    var r2 = Addrow(2, [{ key: 'A', value: 'Period : From '+$('#modal_from_date').val()+'    To : '+$('#modal_to_date').val()+'' }]);
    var r3 = Addrow(3, [{ key: 'A', value: '' }]);
    var r4 = Addrow(4, [{ key: 'A', value: ''+$('#account_holder_name').text()+'' }]);
    var r5 = Addrow(5, [{ key: 'A', value: '' }]);
    sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2+ r3+ r4+ r5+ sheet.childNodes[0].childNodes[1].innerHTML;
  }
},
],
"columnDefs": [
{"width": "20%",   "targets": 0},
{"width": "30%",   "targets": 1},
{"width": "30%",   "targets": 4}
],
});
  }, "json");
};
$(document).on('click','.view_transactions',function(){
  var name=$(this).closest('tr').find('td.name').text();
  if(!$('.customer_flag').text())
  {
   $('.party_flag').hide();
  }
  if(!$('.supplier_class').text())
  {
   $('.supplier_flag').hide();
  }
  var from_date=$('#modal_from_date').val();
  var to_date=$('#modal_to_date').val();
  $('#account_holder_name').text(name);
  var data_array={
    name:name,from_date:from_date,to_date:to_date
  }
  $.fn.search_transaction_ajax(data_array);
  $('#view_transaction_modal').modal('show');
});
$(document).on('click','#fetch_button_journal',function(){
  var from_date=$('#modal_from_date').val();
  var to_date=$('#modal_to_date').val();
  var name=$('#account_holder_name').text();
  var data_array={
    name:name,from_date:from_date,to_date:to_date
  }
  $.fn.search_transaction_ajax(data_array);
});
$(document).on('click','span.voucher_no',function(){
   var voucher_no=$(this).closest('tr').find('span.voucher_no').text();
  var name=$('#account_holder_name').text();
  $.get( "<?= $this->webroot ?>Accountings/general_journal_transaction_by_voucher_no_ajax/"+voucher_no+'/'+name ,function( data ) {
    $('#transaction_details_voucher_table').DataTable().destroy();
    $('#transaction_details_voucher_table tbody').empty();
    $('#transaction_details_voucher_table tfoot').empty();
    $('#transaction_details_voucher_table tbody').html(data.row.tbody);
    $('#transaction_details_voucher_table tfoot').html(data.row.tfoot);
    $('#transaction_details_voucher_table').DataTable({
      "columnDefs": [
      { "width": "50px",  "targets": 0 },
      { "width": "100px", "targets": 1 },
      { "width": "100px", "targets": 2 },
      { "width": "40px",  "targets": 3 },
      { "width": "40px",  "targets": 4 },
      { "width": "150px", "targets": 5 },
      { "width": "100px", "targets": 6 }
      ],
    });
    $('#view_transaction_by_voucher_no_modal').modal('show');
  }, "json");
});
$(document).on('click','#customer_statement_print',function(){
  var account_holder_name=$('#account_holder_name').text();
  var from_date=$('#modal_from_date').val();
  var to_date=$('#modal_to_date').val();
  var data={
    account_head_name:account_holder_name,
  }
  //console.log(from_date);
  $.post( "<?= $this->webroot ?>Accountings/Customer_print_ajax",data ,function( data ) {
    if(data.result!='Success')
    {
      alert(data.result);
      return false;
    }
url='<?= $this->webroot."Accountings/CustomerStatement/"; ?>'+data.id+'/'+from_date+'/'+to_date;
    // url='<?= $this->webroot."Reports/SaleDetailFunction/"; ?>'+data.id+'/'+from_date+'/'+to_date;
    window.open(url);
  }, "json");
});
$(document).on('click','#supplier_statement_print',function(){
  var account_holder_name=$('#account_holder_name').text();
  var from_date=$('#modal_from_date').val();
  var to_date=$('#modal_to_date').val();
  var data={
    account_head_name:account_holder_name,
  }
  //console.log(from_date);
  $.post( "<?= $this->webroot ?>Accountings/Customer_print_ajax",data ,function( data ) {
    if(data.result!='Success')
    {
      alert(data.result);
      return false;
    }
url='<?= $this->webroot."Accountings/SupplierStatement/"; ?>'+data.id+'/'+from_date+'/'+to_date;
    // url='<?= $this->webroot."Reports/SaleDetailFunction/"; ?>'+data.id+'/'+from_date+'/'+to_date;
    window.open(url);
  }, "json");
});
$(document).on('click','#customer_Due_statement_print',function(){
  var account_holder_name=$('#account_holder_name').text();
  var from_date=$('#modal_from_date').val();
  var to_date=$('#modal_to_date').val();
  var data={
    account_head_name:account_holder_name,
  }
  //console.log(from_date);
  $.post( "<?= $this->webroot ?>Accountings/Customer_print_ajax",data ,function( data ) {
    if(data.result!='Success')
    {
      alert(data.result);
      return false;
    }
url='<?= $this->webroot."Accountings/CustomerOutstanding/"; ?>'+data.id+'/'+from_date+'/'+to_date;
    // url='<?= $this->webroot."Reports/SaleDetailFunction/"; ?>'+data.id+'/'+from_date+'/'+to_date;
    window.open(url);
  }, "json");
});
  