<style type="text/css">
.algn_lft{
  text-align: center !important;
}
.pdng_tp{
  margin-top: 15px;
}
.clr_star {
  color: #a29da2;
}
.cart_clr {
  color: #a29da2;
}
.pgn_btm_rht{
  margin-bottom: 15px;
}
.color_of{
  color :red !important;
}
.size_of
{
  font-size:25px !important;
  padding-left: 55px !important;
}
.disable_field
{
pointer-events:none;
}
.simple_single_calculator,.quantity { text-align:right; }
</style>
<section class="content-header">
  <h1>Journal Voucher
    <div class="pull-right">
       <a href="<?php echo $this->webroot ?>Accounts/JournalVoucherList"><button class='btn btn-success pull-right'>Journal Voucher List</button></a>&nbsp;
    </div></h1>
</section>
<section class="content">
  <div class="row-wrapper">
    <div class="box box-primary">
    <?= $this->Form->create('JournalVoucher', array('url' => array('controller' => 'Accounts', 'action' => 'JournalVoucher')));?>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
          <div class="form-group">
              <div class="col-md-2" style="display: none">
                 <?= $this->Form->input('branch_id',array('type'=>'select','id'=>'branch_id','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>array(''=>'Select',$BranchList),'label'=>'Branch')); ?>
              </div>
             <div class="col-md-2" style="display: none">
                      <?= $this->Form->input('lctn_id',array('type'=>'select','id'=>'lctn_id','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>array(''=>'Select'),'label'=>'Location')); ?>
             </div>
               <div class="col-md-3" <?php if(strtolower($this->Session->read('UserRole.name') != 'admin')) { ?><?php } ?>>
                <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date')); ?>
              </div>
              <div class="col-md-3" style="pointer-events:none;">
                <?= $this->Form->input('journal_no',array('class'=>'form-control','type'=>'text','id'=>'journal_no','label'=>'Document No')); ?>
              </div>
             
               <div class="col-md-3">
                <?= $this->Form->input('currency',array('class'=>'form-control','type'=>'text','required','id'=>'currency','label'=>'Currency','readonly')); ?>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </div>
   <div class="box-body table-responsive no-padding">
         <div class="col-md-12">
           <table class="table table-condensed table table-bordered" id="journal_table">
             <thead>
               <tr class="blue-bg">
                 <th>Dr/Cr</th>
                 <th width="30%">Particulars</th>
                <th width="20%">Reference</th>
                 <th>Dr</th>
                 <th>Cr</th>
                 <th style="display: none">Cost Center</th>
                 <th style="display: none"></th>
                 <th style="display: none"></th>
                 <?php //if(!isset($JournalVoucherItems)) :?>
                 <th width="10%">Action</th>
                 <?php //endif; ?>
               </tr>
               <tr>
                <?php //if(!isset($JournalVoucherItems)) :?>
                <td><?php echo $this->Form->input('dr_cr',['type'=>'select','style'=>'width:100%','id'=>'dr_cr','class'=>'form-control select_two_class','options'=>$DebitCredit,'label'=>false]); ?></td>
                 <td><?php echo $this->Form->input('account',['type'=>'select','style'=>'width:100%','id'=>'account','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'options'=>$AccountHead_list,'label'=>false]); ?></td>
                 <td><?= $this->Form->input('reference',array('class'=>'form-control','type'=>'text','id'=>'reference','label'=>false)); ?></td>
                 <td><?= $this->Form->input('amount_dr',array('class'=>'form-control number debit_amount','type'=>'text','id'=>'amount_dr','label'=>false)); ?></td>
                 <td><?= $this->Form->input('amount_cr',array('class'=>'form-control number credit_amount','type'=>'text','id'=>'amount_cr','label'=>false,'style'=>'display:none')); ?></td>
                 <td style="display: none">
               <a href="#" onclick="return costcenteradd('',0)">
               <i style="cursor:pointer;" class="fa_purchase fa fa-plus-square-o create_icon fa-2x"></i></a></td>
               <td style="display: none">
               <?= $this->Form->input('cost_center',['id'=>'cost_center','type'=>'hidden','class'=>'form-control','label'=>false]); ?></td>
               <td style="display: none">
               <?= $this->Form->input('cost_id',['id'=>'cost_id','type'=>'hidden','class'=>'form-control cost_id','label'=>false]); ?></td>
                 <td><i style="cursor:pointer" class="fa fa-plus-circle create_icon fa-2x add_to_list"></i></td>
                 <?php //endif; ?>
               </tr>
             </thead>
             <tbody>
                            <?php if(isset($JournalVoucherItems)) :?>
                            <?php foreach ($JournalVoucherItems as $key => $value): ?>
                            <tr>
                                <input value='<?= $value['JournalVoucherDetail']['id']; ?>' class='form-control' id="id" type='hidden'>
                                <?php 
                                   if($value['JournalVoucherDetail']['dr_cr']==1){
                                      $dr_cr='Debit';
                                    }
                                    else{
                                      $dr_cr='Credit';
                                    }
                                ?>
                                <td><input value='<?= $dr_cr; ?>' class='form-control dr_cr' id="dr_cr" readonly type='text'>
                                </td>
                                <td><input value='<?= $value['AccountHead']['name']; ?>' class='form-control'
                                        id="account" readonly type='text'>
                                </td>
                                <td><input value='<?= $value['JournalVoucherDetail']['reference']; ?>'
                                        class='form-control' readonly type='text'>
                                </td>
                                <td><input value='<?= number_format($value['JournalVoucherDetail']['dr_amount'], 2, '.', ''); ?>' readonly class='form-control amounts_total amount_dr number' type='text'>
                                </td>
                                <td><input value='<?= number_format($value['JournalVoucherDetail']['cr_amount'], 2, '.', ''); ?>' readonly class='form-control amounts_total amount_cr number' type='text'>
                                </td>
                                <td style="width:5%;text-align:center;display: none">
                                    <a href="#">
                                        <i style="cursor:pointer;"
                                            class="fa_purchase fa fa-plus-square-o create_icon fa-2x cost_center_btn"
                                            data-toggle="modal" data-target="#CostCenter_View_modal"
                                            data-id='<?= $value['Journal']['id']; ?>'></i></a>
                                </td>
                                <td><i class="fa fa-minus-circle fa-2x ad-mar remove_journal" id="<?= $value['JournalVoucherDetail']['id']; ?>"></i></td>
                                <td style="display: none">
                                    <?= $this->Form->input('cost_center',['id'=>'cost_center','type'=>'hidden','class'=>'form-control','label'=>false]); ?>
                                </td>
                                <td style="display: none">
                                    <?= $this->Form->input('cost_id',['id'=>'cost_id','type'=>'hidden','class'=>'form-control cost_id','label'=>false]); ?>
                                </td>
                            </tr>
                            <?php endforeach ?>
                            <?php endif; ?>
                        </tbody>
             <tfoot>
            <tr class = "blue-pddng">
             <td></td>
             <td></td>
             <td><h5><label class="control-label pull-right">Total</label></h5></td>
             <td class="settings_unit_price"><?= $this->Form->input('total_dr',array('class'=>'form-control number main_calculator','readonly','type'=>'text','required','id'=>'total_dr','label'=>false)); ?></td>
              <td class="settings_unit_price"><?= $this->Form->input('total_cr',array('class'=>'form-control number main_calculator','readonly','type'=>'text','required','id'=>'total_cr','label'=>false)); ?></td>
             <td colspan="2"></td>
             </tr>
              <tr class = "blue-pddng">
             <td></td>
             <td></td>
             <td></td>
             <td><h5><label class="control-label pull-right">Narration</label></h5></td>
             <td class="settings_unit_price"><?= $this->Form->input('narration',array('class'=>'form-control','type'=>'text','id'=>'narration','label'=>false)); ?></td>
             <td colspan="2"></td>
             </tr>
             <tr class = "blue-pddng" style="display: none">
             <td class="settings_unit_price"></td>
             <td class="settings_unit_price"></td>
             <td class="settings_unit_price"></td>
             <td class="settings_unit_price"></td>
             <td class="settings_unit_price"></td>
             <td class="settings_unit_price"><h5><label class="control-label" style="float: right;">Total Amount Of Debit</label></h5></td>
             <td class="settings_unit_price"><?= $this->Form->input('grand_debittotal',array('class'=>'form-control number','readonly','type'=>'text','step'=>'any','required','id'=>'grand_debittotal','label'=>false)); ?></td>
             <td colspan="2"></td>
             </tr>
             <tr class = "blue-pddng" style="display: none">
             <td></td>
             <td></td>
             <td></td>
             <td></td>
             <td></td>
             <td><h5><label class="control-label" style="float: right;">Total Amount Of Credit</label></h5></td>
             <td class="settings_unit_price"><?= $this->Form->input('grand_credittotal',array('class'=>'form-control number main_calculator','readonly','type'=>'text','step'=>'any','required','id'=>'grand_credittotal','label'=>false)); ?></td>
             <td colspan="2"></td>
             </tr>
              <tr class = "blue-pddng">
                       <td class="settings_unit_price"></td>
                       <td class="settings_unit_price"></td>
                       <td class="settings_unit_price"></td>
                       <td></td>
                       <!-- <td class="settings_unit_price"></td>
                       <td class="settings_unit_price"></td> -->
                       <?php //if(!isset($JournalVoucherItems)) :?>
                       <td class="settings_unit_price pull-right"><button type='Submit' name='data[Sale][process]' value='delivery' id="delivery" class="btn btn-primary invoice-action create_icon btnclick">Save</button>
                       </td>
                        <?php// endif; ?>
                       <!-- <td colspan="2"></td> -->
              </tr>
             </tfoot>
           </table>
         </div>
       </div>
       	<div class="modal fade" id="invoice_check_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
       					<div class="modal-dialog">
       						<div class="modal-content modal_height" style="width:150%" >
       							<div class="modal-header">
       								<h4 class="modal-title" align='center'><span id='name_modal'></span>:  Invoice List</h4>
       							</div>
       							<div class="modal-body">
       								<div class="form-group">
       									<div class="col-sm-9">
       										<div class="col-sm-4">
       											<?= $this->Form->input('invoice_amount',array('class'=>'form-control number number_field money_keyup field_select','id'=>'invoice_amount','label'=>'Amount','value'=>'0','type'=>'text','step'=>1)) ?>
       										</div>
       										<div class="col-sm-4">
       											<?= $this->Form->input('balance_amount',array('class'=>'form-control number_field money_keyup field_select','id'=>'balance_amount','label'=>'Balance','readonly','value'=>'0')) ?>
       											<?= $this->Form->input('pending_balance',array('class'=>'form-control number_field money_keyup field_select','id'=>'pending_balance','label'=>'Balance','readonly','value'=>'0','type'=>'hidden')) ?>
       										</div>
       										<div class="col-sm-4">
       											<br>
       											<button type="button" class="btn-sm btn-primary btn" id='invoice_button'  data-dismiss="modal">Submit</button>
       										</div>
       									</div>
       								</div>
       								<br>
       								<br>
       								<br>
       								<div  id="customer_invoice_listdiv" style="display:none;" >
       								<table class="table table-condensed table-bordered" id="customer_invoice_list" data-page-length='100'>
       									<thead>
       										<tr class='blue-bg'>
       											<th width="2%" hidden></th>
       											<th width="2%" hidden></th>
       											<th width="15%">Date</th>
       											<th width="16%">Invoice</th>
       											<th  width="14%">Balance</th>
       											<th width="15%">Invoice Amount</th>
       											<th width="15%">Remaining</th>
       										</tr>
       									</thead>
       									<tbody>
       									</tbody>
       								</table>
       								</div>
       								<div  id="general_invoice_listdiv" style="display:none;" >
       								<table class="table table-condensed table-bordered"  id="general_invoice_list" data-page-length='100'>
                                           			<thead>
                                           			<tr class='blue-bg'>
                                           				<th width="2%" hidden></th>
                                           				<th width="2%" hidden></th>
                                           				<th width="40%">Account</th>
                                           				<th  width="20%">Balance</th>
                                           				<th width="20%">Invoice Amount</th>
                                           				<th width="20%" hidden>Remaining</th>
                                           			</tr>
                                           			</thead>
                                           			<tbody>
                                           			</tbody>
                                       </table>
                             			</div>
       							</div>
       						</div>
       					</div>
       				</div>
      <br/> <?= $this->Form->end(); ?>
     </div>
     <?php require('cost_center_add_modal.php') ?>
     <?php require('cost_center_modal.php') ?>
 <div id="deletemodel" class="modal fade">
   <div class="modal-dialog modal-confirm">
       <div class="modal-content">
           <div class="modal-header" style="background-color: steelblue;color:white;"><b>NOTE</b>  </div>
           <div class="modal-body" style="text-align:center">
           <div style="display:none;" id="confirmmsg">
               <b>Are you sure to continue??<br/>Please confirm invoice payments before proceeding..</b>
           </div>
             <div style="display:none;" id="advancemsg">
               <b>Do you want to credit the balance amount as  advance??</b>
           </div>
           </div>
           <div class="modal-footer">
               <button type="button" id="cancelmodal" class="btn btn-info deleteCancel">Cancel</button>
               <button type="button" id="continuemodal" class="btn btn-success">Yes</button>
           </div>
       </div>
   </div>
    <div id="confirmadvancemodel" class="modal fade">
          <div class="modal-dialog modal-confirm">
              <div class="modal-content">
                  <div class="modal-header" style="background-color: steelblue;color:white;"><b>NOTE</b>  </div>
                  <div class="modal-body" style="text-align:center">
                      <b>Do you want to credit the balance amount as  advance??</b>
                  </div>
                  <div class="modal-footer">
                      <button type="button" id="canceladd" class="btn btn-info deleteCancel">Cancel</button>
                      <button type="button" id="advanceadd" class="btn btn-success">Yes</button>
                  </div>
              </div>
          </div>
      
   </section>
   <script type="text/javascript">

$('#dr_cr').change(function(){
//alert('k');
  var dr_cr_id=$(this).val();
//alert(mode_category_id);
 // var data={sub_group_id,sub_group_id};
 //var url_address= "<?= $this->webroot; ?>Accounts/GetSystemParameter/"+mode_category_id;
 // $.post( url_address,data, function( response ) {
     //console.log(response) ;

    if(dr_cr_id==1)
    {

      $('.debit_amount').show();
      $('.credit_amount').hide();
     // $('.cash').hide();
     // $('#main_group_id_edit').val(response.option);
     // $('#main_group_id_edit').attr('readonly',true);
     //$('#main_group_id_edit').val('');
    }
    else{
      //$('.bank').hide();
      $('.debit_amount').hide();
       $('.credit_amount').show();

    }

 // },'json');
//}
});

// $(".add_to_list").click(function()
//    {
//           $("#deletemodel").modal('show');
//           $("#advancemsg").css('display','none');
//           $("#confirmmsg").css('display','block');
//    });
   $('.add_to_list').click(function(event){
   // alert("k");
event.preventDefault();
stock_quantity=3000;
var ProductExist = 0;
var ProductExist1 = 0;
var check1=0;
var tablecount=$("#journal_table tbody tr").length;
var account_text=$('#account option:selected').text();
//var barcode=$('#barcode').val();
var dr_cr_id=$('#dr_cr').val();
var dr_cr_text=$('#dr_cr option:selected').text();
var account=$('#account').val();
var reference=$('#reference').val();
var amount_cr=$('#amount_cr').val();
var amount_dr=$('#amount_dr').val();
var cost_id=$('#cost_id').val();
var cost_center=$('#cost_center').val();
// var sl_no=1;
var products_dr = [];
var products_cr = [];
var dr=0;
var cr=0;
$("#journal_table tbody tr").each(function () {
// sl_no++;
var productId = $(this).closest('tr').find('td input.productsrows').val();
if(productId==1){
  dr++;
  //var dr_id=productId;
  //products_dr.push(dr_id);
}else{
  cr++;
   //var cr_id=productId;
   //products_cr.push(cr_id);
}
if(cr>=2 && dr==1 && dr_cr_id==1){
   ProductExist = 1;
    // return false;

}else if(dr>=2 && cr==1 && dr_cr_id==2){
   ProductExist1 = 1;
}
// console.log(cr);
// var C_cr=count(cr_id);
 
// var quantiry = $(this).closest('tr').find('td:eq(6) input').val();
// var free_quantiry = $(this).closest('tr').find('td:eq(7) input').val();
// var table_product_unit = $(this).closest('tr').find('td:eq(2) input:eq(0)').val();
// if (dr_cr_id== productId) {
// // if(table_product_unit==unit){
// ProductExist = 1;
// // return false;
// // }
// }
});
// products_cr.count();
// console.log(products_cr);
// alert(products_cr);
if(ProductExist)
{
alert("Debit Already Added!");
$('#dr_cr').select2('open');
return false;
}
if(ProductExist1)
{
alert("Credit Already Added!");
$('#dr_cr').select2('open');
return false;
}
// if(!amount)
// {
// $('#amount').focus();
// return false;
// }
if(!amount_cr) amount_cr=0.00; else amount_cr=parseFloat(amount_cr).toFixed(2);
    if(!amount_dr) amount_dr=0.00; else amount_dr=parseFloat(amount_dr).toFixed(2);
    if(!dr_cr_id)
    {
        $('#dr_cr').select2('open');
        return false;
    }
    if(!account)
    {
        $('#account').select2('open');
        return false;
    }
    // if(!reference)
    // {
    //     $('#reference').focus();
    //     return false;
    // }
    if(dr_cr_id==1&&!amount_dr)
    {
        $('#amount_dr').focus();
        return false;
    }
    if(dr_cr_id==2&&!amount_cr)
    {
        $('#amount_cr').focus();
        return false;
    }
    if(dr_cr_id==1) var dr_cr="dr";
    if(dr_cr_id==2) var dr_cr="cr";
$('#journal_table tbody').append('<tr>\
<td colspan="1">\
<input class="dr_cr productsrows" hidden name="data[JournalVoucher][dr_cr][]" value="'+dr_cr_id+'" id="' + dr_cr + '">\
<input class="form-control dr_cr" value="'+dr_cr_text+'" readonly>\
</td>\
<td>\
<input class="productlist productsrow" hidden name="data[JournalVoucher][acount_head_id][]" value="'+account+'">\
<input class="form-control account_name" value="'+account_text+'" readonly>\
</td>\
<td><input name="data[JournalVoucher][reference][]" class="form-control reference" value="'+reference+'" readonly>\
<td><input name="data[JournalVoucher][amount_dr][]" class="form-control amount_dr" value="'+amount_dr+'" readonly>\
</td>\
<td><input name="data[JournalVoucher][amount_cr][]" class="form-control amount_cr" value="'+amount_cr+'" readonly>\
</td>\
<td style="display: none"><a href="#" onclick="return costcenteradd('+account+','+(parseInt(tablecount)+1)+')"><i style="cursor:pointer;" class="fa_purchase fa fa-plus-square-o create_icon fa-2x"></i></a></td>\
<td style="display:none">\
<input name="data[JournalVoucher][cost_center][]" class="form-control cost_center" type="hidden" value="'+cost_center+'" readonly>\
</td>\
<td style="display:none">\
<input name="data[JournalVoucher][cost_id][]" class="form-control cost_ids" value="'+cost_id+'" readonly>\
</td>\
<td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
</tr>');
$('.main_calculator').val('0');
$('#amount_dr').val('0');
$('#amount_cr').val('0');
$('#reference').val('');
// $('#barcode').val('');
// $('#retail_price').val('0');
// $('#whole_sale_price').val('0');
// $('#size_id').val('');
//$('#quantity_mode').val(0).trigger('change.select2');
$('#dr_cr').val('').trigger('change.select2');
$('#account').val('').trigger('change.select2');
$.fn.main_calculator();
//$.fn.button_disable();
//$.fn.total_tax();
// if(barcode) {
// $('#barcode').focus();
// } else {
// $('#product').select2('open');
// //$('#whole_sale_price').val('0');
// }
});
$(document).on('click','.remove_tr',function(){
  if (confirm('Are you sure?')) {
$(this).closest('tr').remove();
$('#total_cr').val(0);
$('#total_dr').val(0);

//$.fn.button_disable();
}
});
$.fn.main_calculator=function(){
//var total_quantity=0;
var each_total_cr=0;
var each_total_dr=0;

//var each_taxable_total=0;
//var each_tax_amount_total=0;
//var each_discount_val=0;
$('#journal_table tbody tr').each(function(){

var single_total_cr=$(this).closest('tr').find('td input.amount_cr').val();
if(single_total_cr!=''){
each_total_cr+=parseFloat(single_total_cr);
}
else{
  each_total_cr=0;
}
var single_total_dr=$(this).closest('tr').find('td input.amount_dr').val();
if(single_total_dr!=''){
each_total_dr+=parseFloat(single_total_dr);
}
else{
  each_total_dr=0;
}
//alert(single_total_cr);
// var single_taxable_value=$(this).closest('tr').find('td input.row_taxable_value').val();
// each_taxable_total+=parseFloat(single_taxable_value);
// var single_tax_amount=$(this).closest('tr').find('td input.row_tax_amount').val();
// each_tax_amount_total+=parseFloat(single_tax_amount);
// var quantity=$(this).closest('tr').find('td input.quantity').val();
// var free_qty=$(this).closest('tr').find('td input.free_qty').val();
// var discount_val=$(this).closest('tr').find('td input.total_net_value').val();
// each_discount_val+=parseFloat(discount_val);
// //var quantity=$('.quantity').val();
// var quantity_mode=$(this).closest('tr').find('td input.quantity_mode').val();
// // if(quantity_mode=='Cases')
// // {
// total_quantity=parseFloat(total_quantity)+parseFloat(quantity);
// // }
// //total_quantity=1;
 });
// $('#total_quantity').val(total_quantity);
var main_total_cr=each_total_cr;
$('#total_cr').val(main_total_cr.toFixed(2));
var main_total_dr=each_total_dr;
$('#total_dr').val(main_total_dr.toFixed(2));

}

$("#delivery").click(function()
{
var total_cr=$('#total_cr').val();
var total_dr=$('#total_dr').val();

if(total_dr!=total_cr){
  alert('Total Amount mismatch!');
 // $('total_cr').focus();
  //$('#dr_cr').val('2').trigger('change.select2');
  return false;

}


  });
//$('#total_cr').val(0);
//$('#total_dr').val(0);
//$('#cheque_no').val('');
//$('#reference_no').val('');

// function costcenteradd(id,count){
//   $("#count").val('');
//   $("#prod_id").val('');
//   $('#productslno_table tbody').children( 'tr:not(:first)' ).remove();
//   if(!id)
//   {
//     if(!$("#account").val())
//     {
//       alert("Please Select Account");
//       $("#account").select2('open');
//       return false;
//     }
//     else
//     {
//       var id = $("#account").val();
//       $("#prod_id").val(id);
//       $("#CostCenter_Add_modal").modal('show');
//       var cost_amount = $("#cost_center").val();
//     }
//   }
//   if(id)
//   {
//     //$('#productslno_table tbody').children( 'tr:' ).remove();
//     //alert(id);
//     $("#prod_id").val(id);
//     // $("#CostCenter_Add_modal").modal('show');
//     // if(count ==0)
//     // {
//     //   var itemslno = $("#cost_center").val();
//     // }
//     // else{
//     var each_cost_amount = $('#journal_table tbody tr:nth-child('+count+')').find('td:nth-child(6)').find('input').val();
//     //}
//   //var each_cost_amount=$(this).closest('tr').find('td input.cost_center').val();
//    //var each_cost_amount=$(this).closest('tr').find('td.cost_center').val();
//    //console.log(each_cost_amount);
//     //alert(typeof(each_size_qty));
//    // alert(each_size_qty);
//    // var values=each_size_qty.split('<input');
//    if(each_cost_amount){
//    var values = new Array();
//   values= each_cost_amount.split(",");
//   //  alert(typeof(values));
//   console.log(values);
//   //  $('#product_details_table tbody tr').each(function(i,e){
//     $('#productslno_view_table tbody').html('');
//    // $('#productslno_table tfoot').html('');
// $.each(values,function(i,e){
//  // console.log(e);
//     ///var product_td=$(this).closest('tr').find('.each_size_qty').html();
//    // var product_td=$(this).closest('tr').find('.each_size_qty').val();
//     //alert(product_td);
//      var valuess = new Array();
//   valuess= e.split("-");
//   //console.log(valuess);
    
//     var costcenter_ids=valuess[0];
//     var two=valuess[1];
  
//     // var size=$(this).closest('tr').find('td:eq(0) input').val();
//     // var total_qty_append=$(this).closest('tr').find('td:eq(1) input').val();
//     //var available_qty_append=$(this).closest('tr').find('td:eq(3)').html()

//    //  html += '<tr class="blue-pddng">';
//    //  html += '   <td style="display:none">'+product_name_append+'</td>';
//    //  html += '   <td>'+product_name_append+'</td>';
//    //  html += '   <td>'+two+'</td>';
//    //  html += '<tr>';
//    // grand_total_qty+=two;
//   //   //tfoot+=

//   $('#productslno_view_table tbody').append('<tr class="blue-pddng">\
//     <td>'+costcenter_ids+'</td>\
//     <td>'+two+'</td>\
//     <td style="display:none">'+'Saved'+'</td>\
//     </tr>');
//  // grand_total_qty+=parseFloat(two);
 
//    });
// $("#CostCenter_View_modal").modal('show');
// // $('#productslno_table tfoot').append('<tr class="blue-pddng">\
// //     <td>Total</td>\
// //     <td>'+grand_total_qty+'</td>\
// //     </tr>');
 
//   // });
//    }
//   }
//   $("#count").val(count);
//   // if(itemslno) {
//   //   $("#slno").val(itemslno);
//   //   var temp = new Array();
//   //   temp = itemslno.split(",");
//   //   for (var i = 0; i < temp.length; i++) {
//   //     var tr = '<tr class="blue-pddng">';
//   //     tr += '<td>'+(parseInt(i)+1)+'</td>';
//   //     tr += '<td class="itemslno">' + temp[i] + '</td>';
//   //     tr += '<td><i style="curosr:pointer;" class="fa fa-minus-circle delete_icon fa-2x remove_slno_tr" ></i></td>';
//   //     tr += '</tr>';
//   //     $('#productslno_table tbody').append(tr);
//   //   }
//   // }
//   // else
//   // {
// //   $("#slno").val('');
//   // }
//   return true;
// }

// $(".add_costcenter").click(function()
// {
//   var itemslno = $("#cost_amount").val();
//   var costcenter_id=$('#cost_center_id').val();
//   var costcenter_text=$('#cost_center_id option:selected').text();
//   // if(itemslno) {
//   //   var costcenter_id=$('#cost_center_id option:selected').text();
//   //   var slnostring = $("#slno").val() + ',' + itemslno;
//   //   $("#slno").val(slnostring.replace(/^,/, ''));
//   //   var tr = '<tr class="blue-pddng">';
//   //   tr += '<td>'+costcenter_id+'</td>';
//   //   tr += '<td class="itemslno">' + itemslno + '</td>';
//   //   tr += '<td><i style="cursor:pointer;" class="fa fa-minus-circle delete_icon fa-2x remove_slno_tr" ></i></td>';
//   //   tr += '</tr>';
//   //   $('#productslno_table tbody').append(tr);
//   // }
//   $('#productslno_table tbody').append('<tr class="blue-pddng">\
// <td>\
// <input class="productlist costcenter_id" hidden name="data[JournalVoucher][costcenter_id][]" value="'+costcenter_id+'">\
// <input class="form-control cost_center_text" value="'+costcenter_text+'" readonly>\
// </td>\
// <td>\
// </td>\
// <td><input class="cost_amount" hidden name="data[JournalVoucher][cost_amount][]" value="'+itemslno+'">\
// <input  class="form-control" value="'+itemslno+'" readonly>\
// </td>\
// <td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
// </tr>');
//   $("#cost_amount").val('');
//   $('#cost_center_id').val('').trigger('change.select2');
//   //$("#cost_amount").focus();

// });

// $("#CostCenter_Add_Btn").click(function()
// {
//   $("#CostCenter_Add_modal").modal('hide');
//   var count = $("#count").val();
//   var cost_center_id = $("#cost_center_id").val();
//   var slno = $("#slno").val();
// //    var new_arry=[0,0,0,0,0,0,0,0,0,0,0,0,0];
// //   $('.costcenter_id').each(function(){
// //     //if($(this).closest('tr').find('td.costcenter_id input').val()){
// //       // if($(this).closest('tr').find('td.costcenter_id input').val() != 0){
// //         new_arry[$(this).val()-1]=$(this).val();
// //       // }
// //     //}
// //   });
// // console.log(new_arry);
// //  var new_amount_arry=[0,0,0,0,0,0,0,0,0,0,0,0,0];
// //   $('.cost_amount').each(function(){
// //     //if($(this).closest('tr').find('td.cost_amount input').val()){
// //       // if($(this).closest('tr').find('td.cost_amount input').val() != 0){
// //         new_amount_arry[$(this).val()-1]=$(this).val();
// //      // }
// //     //}
// //   });
// // console.log(new_amount_arry);
//  var myarray = []; 
//  var myarray1 = []; 
//   $('#productslno_table tbody tr').each(function(){ 
//     var eq_size=$(this).closest('tr').find('td:eq(0) input').val();
//     var cost_center_text=$(this).closest('tr').find('td input.cost_center_text').val();
//     //alert(cost_center_text);
//     var table_item_case=$(this).closest('tr').find('td:eq(2) input').val();
//   //  alert(table_item_case);
//     if(table_item_case!='' && table_item_case!=0)
//     {
//       myarray1.push(eq_size+'-'+table_item_case);
//       myarray.push(cost_center_text+'-'+table_item_case);
//     }
//   });
//   //console.log(myarray);
//  // console.log(myarray1);
//   $("#cost_id").val(myarray);
//   $("#cost_center").val(myarray1);
// //var s1111=$.fn.size_convertion(new_arry);  
//   // if(parseInt(count)<=0)
//   // {
//   //   $("#cost_id").val(cost_center_id);
//   //   $("#cost_center").val(slno);
//   // }
//   // else{
//   //   $('#product_table tbody tr:nth-child('+count+')').find('td:nth-child(12)').find('input').val(slno);
//   // }
// });
// $('#add_cost_button').click(function(){
//   var name=$('#cost_center_name_modal').val();
//   if(name=='')
//   {
//     $('#cost_center_name_modal').focus();
//     return false;
//   }
//   // var code=$('#state_code_modal').val();
//   // if(code=='')
//   // {
//   //   $('#state_code_modal').focus();
//   //   return false;
//   // }
//   var website_url='<?php echo $this->webroot; ?>Accounts/add_cost_center_ajax';
//   var data={
//     name:name,
//    // code:code,
//   };
//   $.ajax({
//     method: "POST",
//     url: website_url,
//     data: data,
//     dataType:'json',
//   }).done(function( data ) {
//     if(data.result!='Success')
//     {
//       alert(data.result);
//       return false;
//     }
//     $('#cost_center_add_modal').modal('toggle');
//     $('#cost_center_name_modal').val('');
//     //$('#state_code_modal').val('');
//     $('#cost_center_id').append($("<option></option>").attr("value",data.key).text(data.value));
//     $('#cost_center_id').val(data.key).trigger('change');
//     //location.reload();


//   });
// });
$(document).on('click','.remove_journal',function(){
    var id=$(this).attr('id');
    var url_address= "<?= $this->webroot; ?>Accounts/journal_voucher_detail_delete_ajax";
    if(confirm("Are you sure want to delete?")){
        $.ajax({
            url: url_address,
            type: "POST",
            data: {id:id},
            success: function (respnse) {
            var response =JSON.parse(respnse);
                    if(response.result!='Success')
                    {
                        alert(response.message);
                        return false;
                    }
                    else
                    {
                        location.reload();
                        // var table = $('#journal_table').dataTable();
                        // table.fnDraw();
                    }
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    }else{
        return false;
    }
});
   </script>
