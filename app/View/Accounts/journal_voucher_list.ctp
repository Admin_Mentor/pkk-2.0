
<section class="content-header">
	<h1>Journal Voucher List
        <div class="pull-right">
            <a href="<?php echo $this->webroot ?>Accounts/JournalVoucher"><button class='btn btn-success pull-right'>New Journal Voucher</button></a>&nbsp;
        </div>
    </h1>
</section>
<section class="content">
	<div class="box">
		<!-- <div class="box-header">
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control invoice-search date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control invoice-search date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
				</div>
			</div>
			<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?= $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','empty'=>'All','class'=>'form-control select_two_class','style'=>'width: 100%')); ?>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12"><br>
				<button class='btn btn-success' id='get_button'>Get</button>
			</div>
		</div> -->
		<div class="box-body">
			<table class="table table-condensed table table-bordered" id="table_data" data-order='[[ 0, "desc" ]]' data-page-length='10'>
				<thead>
					<tr class="blue-bg">
						<th width="10%">Date</th>
						<th width="10%">Document No</th>
						<th width="30%">Particulars</th>
						<th width="30%">Reference</th>
						<th>Amount</th>
						<!-- <th>Total Credit</th> -->
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<!-- <tfoot>
					<tr>
						<th colspan="4" style="font-size:20px; color:red;text-align:right">Total:</th>
						<th style="font-size:20px; color:red;"></th>
						<th style="font-size:20px; color:red;"></th>
						<th></th>
						<th></th>

					</tr>
				</tfoot> -->
			</table>
		</div>
	</div>
</div>
</section>
<script type="text/javascript">
	$('#table_data').DataTable( {
		"processing": true,
		"serverSide": true,
        "order": [[ 0, 'asc' ]],
		"ajax": {
			"url": "<?= $this->webroot ?>Accounts/journal_voucher_ajax",
			"type": "POST",
			data:function( d ) {
				// d.from_date= $('#from_date').val();
				// d.to_date= $('#to_date').val();
				// d.executive_id= $('#executive_id').val();
			},
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "JournalVoucher.date" },
		{ "data" : "JournalVoucher.id" },
		{ "data" : "JournalVoucher.account_head" },
		{ "data" : "JournalVoucher.reference" },
		{ "data" : "JournalVoucher.debit_total",className:"text-right"},
		//{ "data" : "JournalVoucher.credit_total" },
		{ "data" : "JournalVoucher.action" },
		],
		"columnDefs": [{"targets": [ 5 ],"bSortable": false }, ],
	});
	$('#get_button').click(function(){
		table = $('#table_data').dataTable();
		table.fnDraw();
	});

	function openInvoicePrint(id)
	{
			var newWindow=window.open(<?php echo $this->webroot ?>+'Print/fpdf/'+id);
			newWindow.focus();
			newWindow.print();
			
	}
	$(document).on('click','.journal_delete',function(){
		var journal_id=$(this).data('id');
		var voucher_no=$(this).data('voucher');
		var url_address= "<?= $this->webroot; ?>Accounts/journal_voucher_delete_ajax";
		if(confirm("Are you sure want to delete?")){
			$.ajax({
				url: url_address,
				type: "POST",
				data: {journal_id:journal_id,voucher_no:voucher_no},
				success: function (respnse) {
				var response =JSON.parse(respnse);
						if(response.result!='Success')
						{
							alert(response.message);
							return false;
						}
						else
						{
							
							var table = $('#table_data').dataTable();
							table.fnDraw();
							}
				},
				error: function (msg) {
					console.log(msg);
				}
			});
		}else{
            return false;
        }
	});
</script>