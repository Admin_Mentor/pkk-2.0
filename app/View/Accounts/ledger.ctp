 <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
       #AccountHead_table_filter .input-sm{
    height: 35px !important;
    font-size: 18px !important;
}
#AccountHead_table_filter{margin-top: 25px !important;}
#AccountHead_table_length{margin-top: 25px !important;}
    </style>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
    <h1> Ledger
<div class="col-sm-1 pull-right" style="margin-right: 5%">
              <!-- <a href="#"> <i class="fa fa-plus-circle ad-mar" id="Product_modal_add"></i></a> -->
             <button data-toggle="modal"  data-target="#customer_modal_add" class="btn btn-success">Create Ledger</button> 
            </div>
    </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary box_tp_brdr">
                <div class="row-wrapper">
                    <div class="row">
                        <?php echo $this->Form->create('AccountHead', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'AccountHead_Form']); ?>   
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                    <!-- <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('executive_id',array('class'=>'form-control select2','type'=>'select','empty'=>'select','options'=>$Executive_list,'id'=>'executive_view_id',)); ?>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="col-md-3 col-lg-3 col-sm-3">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('main_group_ids',array('class'=>'form-control select2 main_group_id','type'=>'select','empty'=>'All','options'=>$MainGroup_list,'id'=>'main_group_ids','label'=>'Main Group')); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-3" >
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('sub_group_ids',array('class'=>'form-control select2 sub_group_id','type'=>'select','empty'=>'All','options'=>$SubGroup_list,'id'=>'sub_group_ids','label'=>'Sub Group')); ?>
                                            </div>
                                            <br>
                                            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-6" style="display: none"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#group_add_modal"></i></a></div>

                                        </div>
                                    </div>
                                    <div class="col-md-3 col-lg-3 col-sm-3" style="display: none">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                                                <?= $this->Form->input('customer_type',array('class'=>'form-control select2','type'=>'select','empty'=>'select','required','options'=>$CustomerType_list,'id'=>'customer_type',)); ?>
                                            </div>
                                            <br>
                                            <div class="col-md-1 col-lg-1 col-sm-1 col-xs-6"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_type_modal"></i></a></div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2" style="display: none">
                                        <div class="form-group">
                                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12" >
                                                <?= $this->Form->input('name',array('class'=>'form-control select2 name','type'=>'select','required','empty'=>'select','options'=>$Customer_list,'id'=>'name','label'=>'Customer')); ?>
                                            </div>
                                            <br>
                                            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_modal_add"></i></a></div>
                                        </div>
                                    </div>
                             <!-- <div class="text-right" style="margin-top: 1.5%;margin-right: 2%"> -->
                   <!--  <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#adddivision"></i></a> -->
                  <!--  <button data-toggle="modal"  data-target="#customer_modal_add" class="btn btn-success">Create Ledger</button>  -->  
                    
                  <!-- </div> -->

                                </div>

                            </div>

                            <?= $this->Form->end(); ?>
                        </div>
                        <br>
                        <div class="row-wrapper">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body" style="margin-left:20px;margin-right:50px;">
                                        <table class="table table-condensed table table boder" id="AccountHead_table" border="1" data-order='[[2,"asc"]]' >
                                            <thead>
                                                <tr class="blue-bg">
                                                    <!-- <th>Executive </th> -->
                                                    <th width="10%">Code</th>
                                                    <th>Name</th>
                                                    <th>Main Group</th>
                                                    <th>Sub Group</th>
                                                    <th>Address</th>
                                                    <th>Gstin</th>
                                                    <th style="text-align: right;">Amount</th>
                                                    <th></th>
                                                    <!-- <th width="20%" style="display:none"></th>  -->
                                                </tr>
                                            </thead>
                                            <tbody>
                                             
                                        </tbody>
                                       <!-- <tfoot></tfoot> -->
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="customer_type_modal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h4 class="modal-title" id="myModalLabel">Customer Type</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Customer Type</label>
                                        <div class="col-sm-8">
                                            <input class="form-control" id="customer_type_name" type="text">
                                        </div>
                                        <br>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" id='add_customer_type'>Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 <div id="Location_Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-body">
      <div class="box-body table-responsive no-padding">
    
    <div id="map"></div>
      </div>
      </div>
      
    </div>
  </div>
</div>
<?php require('ledger_edit_modal.php') ?> 
<?php require('ledger_add_modal.php') ?> 
<?php require('state_modal.php') ?>
<?php require('main_group_modal.php') ?>
<?php require('sub_group_modal.php') ?>
</section>
<?php require('general_journal_transaction_modal.php'); ?>
<script type="text/javascript">
    $('#AccountHead_table').DataTable( {
  "processing": true,
  "serverSide": true,
  "ajax": {
    "url": "<?= $this->webroot ?>Accounts/Ledger_Table_ajax",
    "type": "POST",
    data:function( d ) {
        d.main_group_ids=$('#main_group_ids').val();
        d.sub_group_ids=$('#sub_group_ids').val();
      },
      "dataSrc": "records",
    },
      dom: 'Bfrtip',
      "lengthMenu": [[10,25,50,100], [10,25,50,100]],
        buttons: [
           {extend: 'print', title:"Ledger", exportOptions: { columns: ':visible' } },
        {extend: 'excel',title:"Ledger", exportOptions: { columns: ':visible' } },
        {extend: 'pageLength', },
        ],
    "columns": [
    { "data" : "AccountHead.code" },
    { "data" : "AccountHead.name",},
    { "data" : "AccountHead.main_group",},
    { "data" : "AccountHead.sub_group",},
    { "data" : "AccountHead.address",},
     { "data" : "AccountHead.gstin",},
      { "data" : "AccountHead.balance",className:"text-right"},
      { "data" : "AccountHead.action",},

    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;
      var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
        i : 0;
      };
      pageTotal = api.column( 1, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 1 ).footer() ).html(''+pageTotal+'');
    },
  "columnDefs": [
  { className: "text-right", "targets": [ 5 ],"bSortable": false },
    ],
  });
     $('#sub_group_ids').change(function () {
        var sub_group_ids = $(this).val();
         table = $('#AccountHead_table').dataTable();
        table.fnDraw();
    });
     $('#add_main_group').click(function(){
  var modal_main_group_name=$('#modal_main_group_name').val();
  var modal_main_group_code=$('#modal_main_group_code').val();
  
  var data={
    name:modal_main_group_name,
    code:modal_main_group_code,
  };
  var website_url= '<?php echo $this->webroot; ?>Accounts/add_main_group_ajax_modal';
 
  $.ajax({
    method: "POST",
        url: website_url,
        data: data,
        dataType:'json',
  }).done(function( data ) {
    if(data.result!='Success')
        {
            alert(data.result);
            return false;
        }
        $('#addmain_group').modal('toggle');
        $('#modal_main_group_name').val('');
        $('#modal_main_group_code').val('');
        $('.main_groups').append($("<option></option>").attr("value",data.key).text(data.value));
        $('.main_groups').val(data.key).trigger('change');
  });
});
      $('#add_sub_group').click(function(){
 var main_group_id=$('#main_group_id_add').val();
  var modal_sub_group_name=$('#modal_sub_group_name').val();
  var modal_sub_group_code=$('#modal_sub_group_code').val();
  
  var data={
    main_group_id:main_group_id,
    name:modal_sub_group_name,
    code:modal_sub_group_code,
  };
  var website_url= '<?php echo $this->webroot; ?>Accounts/add_sub_group_ajax_modal';
 
  $.ajax({
    method: "POST",
        url: website_url,
        data: data,
        dataType:'json',
  }).done(function( data ) {
    if(data.result!='Success')
        {
            alert(data.result);
            return false;
        }
        $('#addsub_group').modal('toggle');
        $('#modal_sub_group_name').val('');
        $('#modal_sub_group_code').val('');
        $('.sub_groups').append($("<option></option>").attr("value",data.key).text(data.value));
        $('.sub_groups').val(data.key).trigger('change');
  });
});
    // $('.opening_balance').on('keyup change', function () {
    //     $('#opening_balance').val($(this).val());
    //     $('#opening_balance_modal').val($(this).val());
    // });
    $(document).on('change', '#customer_type,#modal_customer_type', function () {
        $('#modal_customer_type').val($(this).val()).trigger('change.select2');
        $('#customer_type').val($(this).val()).trigger('change.select2');
    });
</script>
<!-- for edit  -->
<script type="text/javascript">
     $(document).on('click','.edit_priority',function(){
        var priority=$(this).closest('tr').find('td input.priority_edit').val();
        var edit_id= $. trim($(this).closest('tr').find('td span.AccountHead_id').text());
        $.post("<?= $this->webroot ?>Accountings/update_customer_priority/"+edit_id+'/'+priority,function(response)
        {
           if(response.status!="Success")
           {
            alert(response.status);
                return false;
           }
           else{
            location.reload();
           }

        },"json");
        
    });
     $(document).on('click', '.edit_head', function () {
        //var id = $(this).closest('tr').find('td span.AccountHead_id').text();
        var id=$(this).attr('table_id');
        var sub_group_ids = $(this).closest('tr').find('td span.SubGroup_id').text();
        //alert(sub_group_ids);
        $.post("<?= $this->webroot ?>Accounts/Ledger_get_ajax/" + id +'/'+sub_group_ids, function (responds) {
            console.log(responds);
            if (responds.result != 'Success')
            {
                alert(responds.message);
                return false;
            }
            $('#name_edit').val(responds.name);
            //console.log(responds.data.Customer.arabic_name);
           // $('#name_edit_arabi').val(responds.data.Customer.arabic_name);
            $('#opening_balance_edit').val(responds.opening_balance).trigger('change');
            $('#customer_type_id_edit').val(responds.customer_type_id).trigger('change');
            $('#route_id_edit').val(responds.route_id).trigger('change');
            $('#customer_group_id_edit').val(responds.customer_group_id).trigger('change');
            $('#sub_group_id_edit').val(responds.sub_group_id).trigger('change');
            $('#sub_group_id_edit_id').val(responds.sub_group_id);
            
            $('#main_group_id_edit').val(responds.main_group_id).trigger('change');
             $('#status_edit').val(responds.status).trigger('change');
             $('#cost_center_edit').val(responds.cost_center).trigger('change');
           // $(".sub_groupdiv").css("pointer-events",'none');
             $('#district_edit').val(responds.district).trigger('change');
            $('#pin_code_edit').val(responds.pin_code);
            $('#state_edit').val(responds.state).trigger('change');
            $('#country_edit').val(responds.country).trigger('change');
            $('#code_edit').val(responds.code);
            $('#email_edit').val(responds.email);
            $('#website_edit').val(responds.website);
            $('#telephone_edit').val(responds.telephone);
            $('#address_edit').val(responds.address);
            $('#other_notes_edit').val(responds.other_notes);
            $('#customer_id_hide').val(responds.AccountHead_id);
            $('#AccountHead_id').val(responds.AccountHead_id);
           // $('#place_edit').val(responds.data.Customer.place);
            $('#contact_person_edit').val(responds.contact_person);
            $('#contact_no_edit').val(responds.contact_no);
            $('#credit_limit_edit').val(responds.credit_limit);
            $('#credit_period_edit').val(responds.credit_period);
           // $('#division_edit').val(responds.data.Customer.division_id);
            $('#gstin_edit').val(responds.gstin);
            $('#sub_group_id_edit').attr('disabled',false);
            $('#sub_group_id_edit_id').attr('disabled',true);  
            if(responds.sub_group_id==1 || responds.sub_group_id==2)
            {
            $('#sub_group_id_edit').attr('disabled',true);
            $('#sub_group_id_edit_id').attr('disabled',false);  
            }
        }, "json");
    });
$(document).on('click', '.ledger_delete', function () {
        if(!confirm("Are you sure?"))
    {
      return false;
    }
        var id=$(this).attr('table_id');
        var sub_group_ids = $(this).closest('tr').find('td span.SubGroup_id').text();
        //alert(sub_group_ids);
        $.post("<?= $this->webroot ?>Accounts/Ledger_delete_ajax/" + id +'/'+sub_group_ids, function (responds) {
            if (responds.result != 'Success')
            {
                alert(responds.result);
                return false;
            }
             table = $('#AccountHead_table').dataTable();
             table.fnDraw();
        }, "json");
    });
$(document).on('click', '#ledger_update_button', function () {
        var website_url = '<?php echo $this->webroot; ?>Accounts/Ledger_account_edit';
        var data=$('#LedgerEdit_Form').serialize();
        console.log(data)
        $.ajax({
            method: "POST",
            url: website_url,
            data: data,
            dataType:'json',
        }).done(function (data) {
            if (data.result != 'Success')
            {
                alert(data.result);
                return false;
            }
            //location.reload();
             $('#customer_modal_edit').modal('toggle');
            table = $('#AccountHead_table').dataTable();
        table.fnDraw();
        });
    });

$(document).on('change','#main_group_ids',function(){
  var main_group_ids=$(this).val();
  $.post( "<?= $this->webroot ?>Accounts/GetSubGroup/"+main_group_ids ,function( data ) {
   // $('#sub_group_ids').html('');
    $('#sub_group_ids').empty();
      $('#sub_group_ids').append($("<option></option>").attr("value",'').text('All'));
    $.each(data.options,function(key,value){
       //$('#sub_group_ids').append($("<option></option>").attr("value",key).text('All'));
      $('#sub_group_ids').append($("<option></option>").attr("value",key).text(value));
    })
    table = $('#AccountHead_table').dataTable();
  table.fnDraw();
  }, "json");
});


</script>
<!-- <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDFVITJEAwer4-t9yNbDpJBd_n1j8-YQ1U&callback=initMap">
    </script> -->
<script type="text/javascript">
    <?php require('state.js'); ?>
              <?php require('ledger.js'); ?>

</script>
