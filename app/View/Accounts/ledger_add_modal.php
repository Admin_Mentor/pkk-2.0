<style type="text/css">

.add_btn_btm a {
    background-color: #9e0c0c;
    color: #fff !important;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Print_Buttn {
    margin-bottom: 5px;
    margin-top: 20px;
}
.Print_Buttn a {
    background-color: #13689e;
    color: #fff;
    letter-spacing: 0.6px;
    text-transform: capitalize;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
}
.Table_Border {
    border: 1px solid #bbb;
}
.Print_Buttn a:hover{
    transition: ease all 0.9s;
    background-color: #00416b;
}
.hr_btm_0px{
    margin-bottom: 0px !important
}
.Rfq_Btn{
    margin-bottom: 5px;

}
.del_color {
    color: #005082;
}
.btn_btm_modal_spc {
    margin-bottom: 3px;
    margin-top: 31% !important;
}
.Rfq_Btn a {
    background-color: #9e0c0c;
    color: #fff !important;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Rfq_Btn a:hover{
    transition: ease all 0.9s;
    background-color: #5d0505;
}
.no_padding{
    padding-left: 0px;
}
.Quote_btn a {
    background-color: #0b8686;
    color: #fff;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Quote_btn{
    margin-bottom: 5px;
    margin-top: 33px;
}
.btn_modal_in_save {
    border: none;
    background-color: #ab4d0a;
    color: white;
    letter-spacing: 0.6px;
    border-radius: 3px !important;
}

.col_md_1_cstm{
    width: 10.666667% !important;
}

.col_md_1_cstm_2 {
    width: 12.666667% !important;
}

.Sav_btn a {
    background-color: #10631e;
    color: #fff !important;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Get_btn{
    margin-left: 16px;
}
.Get_btn a{
    background-color: #9e134d;
    color: #fff !important;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Sav_btn a:hover{
    transition: ease all 0.9s;
    background-color: #04350c;
}
.col_md_off {
    margin-left: 25px;
}
.Total_color {
    font-weight: 600;
    color: #7d1010;
}

.chk_wrnty{
    margin-top: 32px !important;
}
.warranty_prnt{
    margin-top: 31px;

}
.send_btn_modal {
    margin-top: 32px;
    margin-left: 13px;
}
label{
    white-space: nowrap !important;
}
.det_btn {
    margin-top: 3px;
    margin-bottom: 3px;
}
.Add_Service a{
    background-color: #980303;
    color: #fff !important;
    letter-spacing: 0.6px;
    padding-top: 7px;
    padding-bottom: 7px;
    padding-left: 13px;
    padding-right: 13px;
    border-radius: 3px !important;
    white-space: nowrap;
}
.Add_Service {
    margin-top: 32px;
}
.compl_mdl_btn {
    margin-top: 36px;
}
.sav_in_modal{
    margin-top: 32px;
}
.label_Radio_font {
    color: #5f5f5f;
    font-weight: 600;
}
/*    a {
color: #484848 !important;
}*/
.nav-tabs {
    border-bottom: 1px solid #ddd0 !important;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
    color: #fff !important;
    cursor: default;
    background-color: #0b867a !important;
    border: 1px solid #616161 !important;

    border-radius: 3px !important;
}
.nav_nav_border_all {
    border: 1px solid #dcdcdc !important;
    padding: 11px;
    border-top-left-radius: 10px !important;
}
.bt_suc {
    margin-left: 10px;
    margin-top: 20px;
    margin-bottom: 20px;
    border-radius: 3px !important;
}

.add_btn_sucess {
    margin-top: 25px;
    border-radius: 3px !important;
}
 .TriSea-technologies-Switch > input[type="checkbox"] {
    display: none;   
  }

  .TriSea-technologies-Switch > label {
    cursor: pointer;
    height: 0px;
    position: relative; 
    width: 40px;  
  }

  .TriSea-technologies-Switch > label::before {
    background: rgb(0, 0, 0);
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    content: '';
    height: 16px;
    margin-top: -8px;
    position:absolute;
    opacity: 0.3;
    transition: all 0.4s ease-in-out;
    width: 40px;
  }
  .TriSea-technologies-Switch > label::after {
    background: rgb(255, 255, 255);
    border-radius: 16px;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
    content: '';
    height: 24px;
    left: -4px;
    margin-top: -8px;
    position: absolute;
    top: -4px;
    transition: all 0.3s ease-in-out;
    width: 24px;
  }
  .toggle.btn{
    min-width: 100px;
    min-height: 40px;
  }
  .TriSea-technologies-Switch > input[type="checkbox"]:checked + label::before {
    background: inherit;
    opacity: 0.5;
  }
  .TriSea-technologies-Switch > input[type="checkbox"]:checked + label::after {
    background: inherit;
    left: 20px;
}
</style>
</head>

<div id="customer_modal_add" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="display: none">&times;</button>
                <h4 class="modal-title" style="display: none">Ledger</h4>
            </div>
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ledger</h4>
                <?php echo $this->Form->create('Ledger', [ 'id' => 'Ledger_Form']); ?>
                   <?php $user_cost; ?>


                <div class="row" style="display: none">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <ul class="nav nav-tabs some">
                            <li class="active"><a data-toggle="tab" href="#home">Customer Details</a></li>
                            <li><a data-toggle="tab" href="#menu1">Discount Details</a></li>
                        </ul>
                    </div>
                </div>


                <hr>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">

                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Ledger Name</label>

                                                <?= $this->Form->input('name', array('class' => 'form-control', 'type' => 'text', 'required', 'id' => 'name_modal', 'label' => false,)); ?>

                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4 no-padding">
                                            <div class="col-md-10 col-sm-10 col-lg-10">
                                                <label class="col-sm-4 control-label">Sub Group</label>
                                                <?= $this->Form->input('sub_group_id', array('class' => 'form-control select2 sub_groups', 'type' => 'select', 'style' => 'width:100%', 'options' => $SubGroup_list, 'id' => 'sub_group_id', 'label' => false,'empty'=>'SELECT')); ?>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-lg-2">
                                                <i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#addsub_group" style="margin-top: 30px;"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                              <div class="col-md-10 col-sm-10 col-lg-10 no-padding">
                                                <label class="col-sm-4 control-label">Main Group</label>
                                                <?= $this->Form->input('main_group_id', array('class' => 'form-control main_groups', 'type' => 'text', 'style' => 'width:100%', 'id' => 'main_group_id', 'label' => false,'readonly')); ?>
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-lg-2" style="display: none">
                                            <i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#addmain_group"style="margin-top: 30px;"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4" style="display: none">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Code</label>
                                                <?= $this->Form->input('code', array('class' => 'form-control', 'type' => 'text', 'id' => 'code', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        
                                       <div class="col-md-4" style="display: none">
                                            <div class="form-group-btm-spc">
                                                <label>Division</label>
                                                <?= $this->Form->input('division_id', array('class' => 'form-control select2 division', 'type' => 'select', 'style' => 'width:100%','empty'=>'Select', 'options' => $Division_list, 'id' => 'division_id', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                        
                                        

                                    </div>
                                </div>
                                <br>
                                 <div class="row customer_feilds" style="display: none">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-4 col-sm-4 col-lg-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Customer Type</label>
                                                <?= $this->Form->input('customer_type_id', array('class' => 'form-control select2', 'type' => 'select', 'required', 'style' => 'width:100%', 'options' => $modalCustomerType_list, 'id' => 'customer_type_id', 'label' => false,'empty'=>'SELECT')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Route</label>
                                                <?= $this->Form->input('route_id', array('class' => 'form-control select2 route', 'type' => 'select', 'style' => 'width:100%', 'options' => $Route_list, 'id' => 'route_id', 'label' => false,'empty'=>'SELECT')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-lg-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Customer Group</label>
                                                <?= $this->Form->input('customer_group_id', array('class' => 'form-control select2 customer_groups', 'type' => 'select', 'options' => $CustomerGroup_list, 'style' => 'width:100%', 'id' => 'customer_group_id', 'label' => false,'empty'=>'SELECT')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Address</label>
                                                <?= $this->Form->input('address', array('class' => 'form-control', 'type' => 'textarea', 'step' => 'any', 'rows' => 2, 'id' => 'address', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Country</label>
                                                    <?= $this->Form->input('country', array('class' => 'form-control select2 country', 'type' => 'select', 'style' => 'width:100%', 'options' => $Country_list, 'id' => 'country', 'label' => false,'default'=>'1')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-lg-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">State</label>
                                                <?= $this->Form->input('state', array('class' => 'form-control select2 state', 'type' => 'select', 'style' => 'width:100%', 'options' => $State_list, 'id' => 'state','required', 'label' => false,'default'=>'19')); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-lg-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">District</label>
                                                <?= $this->Form->input('district', array('class' => 'form-control select2 district', 'type' => 'select', 'style' => 'width:100%', 'options' => $District_list, 'id' => 'district', 'label' => false,'empty'=>'SELECT')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Pin Code</label>
                                                <?= $this->Form->input('pin_code', array('class' => 'form-control', 'type' => 'text', 'id' => 'pin_code', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4">      
                                            <label class="col-sm-4 control-label">Telephone</label>
                                                <?= $this->Form->input('telephone', array('class' => 'form-control number', 'maxlength'=>12,'type' => 'text','id' => 'telephone', 'label' => false,)); ?>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                              <label class="col-sm-4 control-label">Email</label>
                                                <?= $this->Form->input('email', array('class' => 'form-control', 'type' => 'text', 'id' => 'email', 'label' => false,)); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-4 col-sm-4 col-lg-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Website</label>
                                                 <?= $this->Form->input('website', array('class' => 'form-control', 'type' => 'text', 'id' => 'website', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Contact Person</label>
                                                <?= $this->Form->input('contact_person', array('class' => 'form-control', 'type' => 'text', 'required', 'id' => 'contact_person', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-lg-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Contact No</label>
                                                
<?= $this->Form->input('contact_no', array('class' => 'form-control number', 'maxlength'=>12,'type' => 'text','id' => 'contact_no', 'label' => false,)); ?>
                                             
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="col-md-4 col-sm-4 col-lg-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">GSTIN/CR</label>
                                                 <?= $this->Form->input('gstin', array('class' => 'form-control', 'type' => 'text', 'id' => 'gstin', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-lg-4 col-sm-4">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Credit Limit</label>
                                                <?= $this->Form->input('credit_limit', array('class' => 'form-control credit_limit', 'type' => 'number', 'step' => 'any', 'required', 'id' => 'credit_limit', 'value' => 0, 'label' => false,)); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-lg-4">
<div class="form-group">
                                                <label class="col-sm-4 control-label">Credit Period</label>
                                                <?= $this->Form->input('credit_period', array('class' => 'form-control credit_period', 'type' => 'number', 'step' => 'any', 'required', 'id' => 'credit_period_modal', 'value' => 0, 'label' => false,)); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                   <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                             <div class="col-md-4 col-lg-4 col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Opening Balance</label>
                                                    <?= $this->Form->input('opening_balance', array('class' => 'form-control opening_balance', 'type' => 'number', 'step' => 'any','id' => 'opening_balance', 'label' => false, 'value' => 0, )); ?>
                                                </div>
                                            </div> 
                                        <div class="col-md-6 col-lg-6 col-sm-6">
                                            <div class="form-group">
                                                <label class="col-sm-4 control-label">Other Notes</label>
                                                <?= $this->Form->input('other_notes', array('class' => 'form-control', 'type' => 'textarea', 'step' => 'any', 'rows' => 2, 'id' => 'other_notes', 'label' => false,)); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                             <div class="col-md-4 col-lg-4 col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Status</label>
                                                    <!-- <?= $this->Form->input('status', array('class' => 'form-control', 'type' => 'checkbox', 'style' => 'width:100%', 'data-toggle'=>"toggle", 'label' => false,'data-on'=>'active','data-off'=>'Active')); ?> -->
                                                    <?= $this->Form->input('status', array('class' => 'form-control select2 status', 'type' => 'select', 'style' => 'width:100%', 'options' => $Status, 'id' => 'status', 'label' => false,)); ?>
                                                </div>
                                            </div> 
                                    <!-- </div> -->
                                    <!-- <div class="col-md-12 col-lg-12 col-sm-12"> -->
                           <?php if($user_cost=='Yes'){ ?>
                                             <div class="col-md-4 col-lg-4 col-sm-4">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label">Apply Cost Center</label>
                                                    <br>
                                           <?= $this->Form->input('cost_center', array('class' => 'form-control select2 cost_center', 'type' => 'select', 'style' => 'width:100%', 'options' => $cost_center, 'id' => 'cost_center', 'label' => false,)); ?>
                                                </div>
                                            </div>
                                            <?php } ?> 
                                    </div>
                                </div>

                                 <div class="row">
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                        <div class="pull-right">
                                            <button class="btn btn-success" style="border-radius: 3px !important; margin-top: 20px;" type="button" id="ledger_add_button" >Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <?= $this->Form->end(); ?>

            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
            </div>
        </div>

    </div>
</div>