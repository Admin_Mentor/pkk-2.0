 <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
       #AccountHead_table_filter .input-sm{
    height: 35px !important;
    font-size: 18px !important;
}
#AccountHead_table_filter{margin-top: 25px !important;}
#AccountHead_table_length{margin-top: 25px !important;}
    </style>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
    <h1> Ledger Report </h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary box_tp_brdr">
                <div class="row-wrapper">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-12">
                                    <div class="col-md-2 col-lg-2 col-sm-2" hidden>
                                        <div class="form-group" >
                                                 <?= $this->Form->input('district_id',array('class'=>'form-control select2 ','type'=>'select','empty'=>'All','options'=>$District_list,'id'=>'district_id','label'=>'District')); ?>
                                        </div>
                                    </div>
                                     <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group" >
                                                <?= $this->Form->input('main_group_ids',array('class'=>'form-control select2 main_group_id','type'=>'select','empty'=>'All','options'=>$MainGroup_list,'id'=>'main_group_ids','label'=>'Main Group')); ?>
                                        </div>
                                    </div>
                                     <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group" >
                                                <?= $this->Form->input('sub_group_ids',array('class'=>'form-control select2 sub_group_id','type'=>'select','empty'=>'All','options'=>$SubGroup_list,'id'=>'sub_group_ids','label'=>'Sub Group')); ?>
                                            </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2" style="display: none">
                                        <div class="form-group" >
                                                <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$from_date)); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-lg-2 col-sm-2">
                                        <div class="form-group" >
                                                <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$to_date)); ?>
                                            </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-6" style="margin-top:0%">
                                        <div class="form-group">
                                          <div class="col-md-4 col-lg-4 col-sm-4" id='cost_visibility' hidden>
                                          <input class='nav-toggle' type="checkbox" id='toggle_button_for_amount_visibility' data-width="130" data-toggle="toggle" data-off="Show Amount" data-on="Hide Amount">
                                          </div>
                                          <div class="col-md-8 col-lg-8 col-sm-8" >
                                          <h1 hidden class="total-amt amount-field">Total Amount : <span id='main_total_amount'>Loading .....</span></h1>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                             </div>   
                     <div class="row-wrapper">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-body" style="margin-left:20px;margin-right:50px;">
                                        <table class="table table-condensed table table boder" id="AccountHead_table" border="1" data-order='[[2,"asc"]]' >
                                            <thead>
                                                <tr class="blue-bg">
                                                    <th width="10%">Code</th>
                                                    <th>Name</th>
                                                    <th>Address</th>
                                                    <th>Gstin</th>
                                                    <th style="text-align: right;">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                             
                                        </tbody>
                                       <!-- <tfoot></tfoot> -->
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<?php require('general_journal_transaction_ledger_modal.php'); ?>
<script type="text/javascript">
    $('#sub_group_ids,#district_id,#main_group_ids,#to_date').change(function () {
        var sub_group_ids = $(this).val();
         table = $('#AccountHead_table').dataTable();
        table.fnDraw();
    });
    $('#AccountHead_table').DataTable( {
  "processing": true,
  "serverSide": true,
  "ajax": {
    "url": "<?= $this->webroot ?>Accounts/Ledger_report_ajax",
    "type": "POST",
    data:function( d ) {
        d.main_group_ids=$('#main_group_ids').val();
        d.sub_group_ids=$('#sub_group_ids').val();
        d.district_id=$('#district_id').val();
        d.to_date=$('#to_date').val();
      },
      "dataSrc": "records",
    },
      dom: 'Bfrtip',
      "lengthMenu": [[10,25,50,100], [10,25,50,100]],
        buttons: [
           {extend: 'print', title:"Ledger Report", exportOptions: { columns: ':visible' } },
        {extend: 'excel',title:"Ledger Report", exportOptions: { columns: ':visible' } },
        {extend: 'pageLength', },
        ],
    "columns": [
    { "data" : "AccountHead.code" },
    { "data" : "AccountHead.name",},
    { "data" : "AccountHead.address",},
     { "data" : "AccountHead.gstin",},
      { "data" : "AccountHead.balance",className:"text-right"},

    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;
      var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
        i : 0;
      };
      pageTotal = api.column( 1, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 1 ).footer() ).html(''+pageTotal+'');
    },
    "columnDefs": [
    ],
  });
$('#modal_from_date').val($('#from_date').val());
$('#modal_to_date').val($('#to_date').val());
$(document).on('change','#sub_group_ids',function(){
     $('#main_total_amount').html('Loading .....')
     var to_date=$('#to_date').val();
       var sub_group_id=$('#sub_group_ids').val();
    $.fn.amount_visiblility(sub_group_id,to_date);
  });

  $.fn.amount_visiblility= function(sub_group_id,to_date){
    if(sub_group_id!='')
    {
      var url_address= "<?= $this->webroot; ?>Reports/get_total_amount/"+sub_group_id+'/'+to_date;
      $.post( url_address, function( response ) {
        $('#main_total_amount').html(response);
      }, "json");
      $('.amount-field').show();
    }else{
      $('.amount-field').hide();
    }
  }
</script>
