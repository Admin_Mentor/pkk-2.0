<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1> Main Group 
<div class="col-sm-1 pull-right" style="margin-right: 5%">
              <!-- <a href="#"> <i class="fa fa-plus-circle ad-mar" id="Product_modal_add"></i></a> -->
            <button data-toggle="modal"  data-target="#addmain_group" class="btn btn-success">Create Main Group</button>
            </div>
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="row">
          <!-- <div class="col-md-12"> -->
           <!--  <div class="col-md-4"><?php echo $this->Form->input('Division',array('type'=>'select','empty' =>'ALL','options'=>$Division,'class'=>'form-control select_two_class','required'=>'required')) ?>
              
            </div> -->
             <!-- <div class="text-right" style="margin-top: 1.5%;margin-right: 2%"> -->
                   <!--  <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#adddivision"></i></a> -->
                   <!-- <button data-toggle="modal"  data-target="#addmain_group" class="btn btn-success">Create Main Group</button>    -->
                    
                  <!-- </div> -->
          </div>
          
        <!-- </div> -->
       
        <div class="box-body">
          <table class="table table-condensed tab_view_top_mar type_tab table-bordered" id='main_group_table'>
            <thead>
              <tr class="blue-bg">
                <th>Name</th>
                <th>Code</th>
                <th style="display:none"></th>
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php  foreach( $AccMainGroups as $value ){?>
                <tr class="blue-pd">
                  
                  <td class='main_group_name'><?= ucwords(strtolower($value['AccMainGroup']['name'])); ?></td>
                  <td class='main_group_code'><?= $value['AccMainGroup']['code']; ?></td>
                  <td style="display:none" class='main_group_id'><?= $value['AccMainGroup']['id'];?></td>
                  <td><a href="#"><i class="fa fa-edit fa-2x edit_new_btn ad-mar td_leted edit_dt"> </i></a></td>
                   <!-- <td><i data-id="<?= $value['Size']['id'];  ?>" class="fa fa-edit fa-2x edit_dt" aria-hidden="true" style="color: #3c8dbc;"></i></td> -->
                  
                </tr>
                <?php  }?>
              </tbody>
            </table>
          </div>
          <div id="main_group_edit_modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Main Group</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-3"><label for="inputEmail3" class="form-group">Name of Main Group</label></div>
                    <div class="col-md-9">
                      <input type="hidden" id="main_group_id">
                      <input type="text" class="form-control form-group" placeholder="" id="main_group_name">
                      <span id="prdct_type_error_edit" style="color:#db1802" class="help-inline"></span>
                    </div>
                    <div class="col-md-3"><label for="inputEmail3" class="form-group">Code</label></div>
                     <div class="col-md-9">
                      <!-- <input type="hidden" id="main_group_id"> -->
                      <input type="text" class="form-control form-group" placeholder="" id="main_group_code" readonly>
                      <span id="prdct_type_error_edit" style="color:#db1802" class="help-inline"></span>
                    </div>
                  </div>
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                  <button type="button" class="btn btn-success" data-dismiss="modal" id="main_group_edit">Update</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  <div id="addmain_group" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Add Main Group</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label"> Name of Main Group</label>
            <div class="col-sm-9">
              <input class="form-control location_disable toUpperCase " placeholder="" type="text" id="modal_main_group_name">
              <span id="main_group_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>
                  <div class="form-horizontal">
          <div class="form-group" style="display: none">
            <label for="inputEmail3" class="col-sm-3 control-label"> Code</label>
            <div class="col-sm-9">
              <input class="form-control"  type="text" id="modal_main_group_code" readonly>
              <span id="main_group_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>
          
        </div>
      
      <div class="modal-footer">
        <button  type='button' class="save btn btn-success" id="add_main_group">Save</button>
      </div>
      </div>
    </div>
  </div>
</div>
    <script>
     // $('#modal_main_group_name').keyup(function(){

//   var modal_main_group_name=$(this).val();
//    $('#add_main_group').attr('disabled',true);
//   var data={
//    main_group_name:modal_main_group_name
//  };
//   var url_address= '<?php //echo $this->webroot; ?>'+'Accounts/main_group_search';
//  $.ajax({
//   type: "post",
//   url:url_address,
//   data: data,
//   success: function(response) {
//     if(response=="Yes")
//     {
//       $('#main_group_error').html('This Main Group is already taken');
//       $('#add_main_group').attr('disabled',true);
//     }
//     else
//     {
//       $('main_group_error').html('');
//        $('#add_main_group').attr('disabled',false);
//     }
//   },
//   error:function (XMLHttpRequest, textStatus, errorThrown) {
//     alert(textStatus);
//   }
// });
//});
  $('#add_main_group').click(function(){
  var modal_main_group_name=$('#modal_main_group_name').val();
  //var modal_main_group_code=$('#modal_main_group_code').val();
  var data={
    name:modal_main_group_name,
   // code:modal_main_group_code,
  };
  var url_address= '<?php echo $this->webroot; ?>Accounts/add_main_group_ajax';
   $.ajax({
    method: "POST",
    url:url_address,
    data: data,
  }).done(function( result ) {
    location.reload(); 

  });
});
      $(document).on('click', '.edit_dt', function () {
        $('#main_group_table').find('td').removeClass('new_main_group_name');
        $('#main_group_table').find('td').removeClass('new_main_group_code');
        //var main_group_name=$(this).closest('tr').find('td.main_group_name').text();
        var main_group_code=$(this).closest('tr').find('td.main_group_code').text();
        var main_group_name=$(this).closest('tr').find('td.main_group_name').text();
        var main_group_id=$(this).closest('tr').find('td.main_group_id').text();
        $(this).closest('tr').find('td.main_group_name').addClass('new_main_group_name');
        $(this).closest('tr').find('td.main_group_code').addClass('new_main_group_code');
        $('#main_group_id').val(main_group_id);
        $('#main_group_name').val(main_group_name);
        $('#main_group_code').val(main_group_code);
        
        $('#main_group_edit_modal').modal('show');
      });

      $('#main_group_edit').click(function() {
        var main_group_id=$('#main_group_id').val();
        if(!main_group_id)
          alert('Empty Main Group Id');
        var main_group_name=$('#main_group_name').val().toUpperCase();
        var main_group_code=$('#main_group_code').val().toUpperCase();
        // if($.trim(main_group_name)=='')
        //   alert('Empty Main Group Name');
        
       
        var url_address= '<?php echo $this->webroot; ?>'+'Accounts/edit_main_group_ajax';
        var data=
        {
          id:main_group_id,
          name:main_group_name,
          code:main_group_code,
        };
        $.ajax({
          type: "post",  
          url:url_address,
          data: data, 
          dataType:'json',
          success: function(response) {
            if(response.result!='Success')
            {
              alert(response.result);
              return false;
            }
            $('.new_main_group_name').html(main_group_name);
            $('.new_main_group_code').html(main_group_code);
             $('#main_group_id').val('');
        $('#main_group_name').val('');
         $('#main_group_code').val('');
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      });

      // $(document).on('click','.customer_type_delete',function(){
      //   if(!confirm("Are you sure?"))
      //   {
      //     return false;
      //   }
      //   var id=$(this).closest('tr').find('td.customer_type_id').text();
      //   var rowindex = $(this).closest('tr').index();
      //   var url_address= '<?php echo $this->webroot; ?>'+'CustomerType/delete_by_ajax/'+$.trim(id);
      //   $.ajax({
      //     type: "post",  
      //     url:url_address,
      //     dataType:'json',
      //     success: function(response) {
      //       if(response.result=="Success")
      //       {
      //         $('#customer_type_table tbody tr:eq(' + rowindex + ')').remove();
      //         $("#customer_type option[value='"+id+"']").remove();
      //         $("#customer_type").select2('val','');
      //       }
      //       else
      //       {
      //         $('#customer_type_table tbody tr:eq(' + rowindex + ')').attr('class','blue-pd flash');
      //         $('#customer_type_table tbody tr:eq(' + rowindex + ')').attr('style','font-size:20px;color:red');
      //         alert(response.result);
      //         setTimeout(timer_for_table, 2000);
      //       }
      //     },
      //     error:function (XMLHttpRequest, textStatus, errorThrown) {
      //       alert(textStatus);
      //     }
      //   });
      // });
      function timer_for_table() {
        $('#customer_type_table tbody tr').each(function(){
          $(this).closest('tr').attr('class','blue-pd');
          $(this).closest('tr').attr('style','font-size:14px;color:');
        });
      }
//      $('.location_disable').keyup(function(){
//   var modal_location_name=$('#modal_location_name').val().trim();
//   if(modal_location_name!="")
//   {
//     $('#add_location').attr('disabled',false);
//   }
//   else{
//     $('#add_location').attr('disabled',true);
//   }
// });
//      $('#modal_location_name').keyup(function(){
//   var modal_location_name=$(this).val();
//   var data={
//    location_name:modal_location_name
//  };
//   var url_address= '<?php echo $this->webroot; ?>'+'Executive/route_search';
//  $.ajax({
//   type: "post",
//   url:url_address,
//   data: data,
//   success: function(response) {
//     if(response=="Yes")
//     {
//       $('#location_error').html('This Location is already taken');
//       $('#add_location').attr('disabled',true);
//     }
//     else
//     {
//       $('#location_error').html('');
//        $('#add_location').attr('disabled',false);
//     }
//   },
//   error:function (XMLHttpRequest, textStatus, errorThrown) {
//     alert(textStatus);
//   }
// });
// });
//      $('#modal_location_code').keyup(function(){
//   var modal_location_code=$(this).val();
//   var data={
//    code:modal_location_code
//  };
//   var url_address= '<?php echo $this->webroot; ?>'+'Route/route_search';
//  $.ajax({
//   type: "post",
//   url:url_address,
//   data: data,
//   success: function(response) {
//     if(response=="Yes")
//     {
//       $('#location_code_error').html('This Code is already taken');
//       $('#add_location').attr('disabled',true);
//     }
//     else
//     {
//       $('#location_code_error').html('');
//        $('#add_location').attr('disabled',false);
//     }
//   },
//   error:function (XMLHttpRequest, textStatus, errorThrown) {
//     alert(textStatus);
//   }
// });
// });
 $('#main_group_table').DataTable({
        dom: 'Bfrtip',
        buttons: [
        {
         extend: 'colvis',
         // columns: ':gt(2)'
         columns: ':eq(0),:eq(1),:eq(3)'
       },
       // {
       //   extend: 'csv',
       //    title:'Main Group',

       //         footer: true,
       //         customize: function (csv) {
       //           return "\tMain Group\n"+  csv ;
       //         },
       //  // footer: true,
       //   exportOptions: { columns: ':visible' }
       // },
       // {
       //   extend: 'excel',
       //   title:'Main Group',
       //   footer: true,
       //   exportOptions: { columns: ':visible' }
       // },
        {
         extend: 'print',
         title:'Main Group',
         footer: true,
         exportOptions: { columns: ':visible' }
       },
        {
         extend: 'pdf',
         title:'Main Group',
        // pageSize: 'A4',
        //  orientation: 'landscape',
         footer: true,
         exportOptions: { columns: ':visible' }
       },
       {
         extend: 'pageLength',
       },
       ],
       "columnDefs": []
     });
    </script>