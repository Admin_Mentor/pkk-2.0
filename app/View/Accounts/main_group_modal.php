<div id="addmain_group" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Add Main Group</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label"> Name of Main Group</label>
            <div class="col-sm-9">
              <input class="form-control location_disable toUpperCase " placeholder="" type="text" id="modal_main_group_name">
              <span id="main_group_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>
                  <div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label"> Code</label>
            <div class="col-sm-9">
              <input class="form-control location_disable toUpperCase " placeholder="" type="text" id="modal_main_group_code">
              <span id="main_group_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>
          
        </div>
      
      <div class="modal-footer">
        <button  type='button' class="save btn btn-success" id="add_main_group">Save</button>
      </div>
      </div>
    </div>
  </div>