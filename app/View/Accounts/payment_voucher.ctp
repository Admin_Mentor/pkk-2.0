<style type="text/css">
.algn_lft {
    text-align: center !important;
}

.pdng_tp {
    margin-top: 15px;
}

.clr_star {
    color: #a29da2;
}

.cart_clr {
    color: #a29da2;
}

.pgn_btm_rht {
    margin-bottom: 15px;
}

.color_of {
    color: red !important;
}

.size_of {
    font-size: 25px !important;
    padding-left: 55px !important;
}

.disable_field {
    pointer-events: none;
}

.simple_single_calculator,
.quantity {
    text-align: right;
}
</style>
<section class="content-header">
    <h1>Payment Voucher
        <div class="pull-right">
            <a href="<?php echo $this->webroot ?>Accounts/PaymentVoucherList"><button class='btn btn-success pull-right'>Payment Voucher List</button></a>&nbsp;
        </div>
    </h1>
</section>
<section class="content">
    <div class="row-wrapper">
        <div class="box box-primary">
            <?= $this->Form->create('JournalVoucher', array('url' => array('controller' => 'Accounts', 'action' => 'PaymentVoucher')));?>
            <?php //if(isset($PaymentVoucherItems)) $readonly='disabled'; else $readonly=''; ?>
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                    <div class="form-horizontal" style="margin-top: 15px;">
                        <div class="box-body">
                            <div class="form-group">
                                <div class="col-md-2" style="display: none">
                                    <?= $this->Form->input('branch_id',array('type'=>'select','id'=>'branch_id','class'=>'form-control select2','style'=>'width: 100%','options'=>array(''=>'Select',$BranchList),'label'=>'Branch')); ?>
                                </div>
                                <div class="col-md-2" style="display: none">
                                    <?= $this->Form->input('lctn_id',array('type'=>'select','id'=>'lctn_id','class'=>'form-control select2','style'=>'width: 100%','options'=>array(''=>'Select'),'label'=>'Location')); ?>
                                </div>
                                <div class="col-md-3"
                                    <?php if(strtolower($this->Session->read('UserRole.name') != 'admin')) { ?><?php } ?>>
                                    <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date'));  ?>
                                </div>
                                <div class="col-md-3" style="pointer-events:none;">
                                    <?= $this->Form->input('journal_no',array('class'=>'form-control','type'=>'text','id'=>'journal_no','label'=>'Document No')); ?>
                                </div>

                                <div class="col-md-3">
                                    <?= $this->Form->input('currency',array('class'=>'form-control','type'=>'text','required','id'=>'currency','label'=>'Currency','readonly')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <div class="col-md-3">
                                    <?php echo $this->Form->input('mode_category_ids',array('type'=>'select','class'=>'form-control select2','id'=>'mode_category_ids','style'=>'width: 100%;','options'=>$mode_category,'required','empty'=>'Select','label'=>'Mode Category')); ?>
                                </div>
                                <!-- <div class="col-md-2" style="display: none">
                                <label>Mode</label><br/>
                                <input class='nav-toggle' type="checkbox" name='data[JournalVoucher][settle_type]' id='toggle_button_for_type' data-width="130" data-toggle="toggle" data-off="Cash" data-on="Bank" data-onstyle="primary" data-offstyle="primary">
                           </div> -->
                                <div class="col-md-3 cash" style="display: none">
                                    <?php echo $this->Form->input('mode_cash',array('type'=>'select','class'=>'form-control select2 mode_cash','id'=>'mode','style'=>'width: 100%;','options'=>$Mode)); ?>
                                </div>
                                <div class="col-md-3 cheque" style="display: none">
                                    <?= $this->Form->input('mode_cheque',array('type'=>'select','id'=>'mode','class'=>'form-control select2 mode_bank','style'=>'width: 100%','options'=>array($banks),'label'=>'Bank')); ?>
                                </div>
                                <div class="col-md-3 cheque" style="display: none">
                                    <?= $this->Form->input('cheque_no',array('class'=>'form-control','type'=>'text','id'=>'cheque_no','label'=>'Cheque No')); ?>
                                </div>
                                <div class="col-md-3 cheque" style="display: none">
                                    <?php echo $this->Form->input('cheque_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'cheque_date','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Cheque Date','disabled'=>$readonly)); ?>
                                </div>
                                <div class="col-md-3 bank" style="display: none">
                                    <?= $this->Form->input('mode_bank',array('type'=>'select','id'=>'mode','class'=>'form-control select2','style'=>'width: 100%','options'=>array($banks),'label'=>'Bank')); ?>
                                </div>
                                <div class="col-md-3 bank" style="display: none">
                                    <?= $this->Form->input('reference_no',array('class'=>'form-control','type'=>'text','id'=>'reference_no','label'=>'Reference No')); ?>
                                </div>
                                <div class="col-md-3" style="display: none">
                                    <?php echo $this->Form->input('cheques_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'cheques_date','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date')); ?>
                                </div>

                                <div class="col-md-3 bankdiv" style="display:none">
                                    <?= $this->Form->input('bank',array('type'=>'select','id'=>'bank','class'=>'form-control select2','style'=>'width: 100%','options'=>array($banks),'label'=>'Bank')); ?>
                                </div>
                                 <div class="col-md-3" style="display: none">
                                    <?= $this->Form->input('branch_id',array('class'=>'form-control','type'=>'text','required','id'=>'branch_id','label'=>'Branch','readonly')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <div class="col-md-12">
                    <table class="table table-condensed table table-bordered" id="journal_table">
                        <thead>
                            <tr class="blue-bg">
                                <th>Account</th>
                                <th>Reference</th>
                                <th>Amount</th>
                                <th>Cost Center</th>
                                <th style="display: none"></th>
                                <th style="display: none"></th>
                                <?php //if(!isset($PaymentVoucherItems)) :?>
                                <th width="10%">Action</th>
                                <?php //endif; ?>
                            </tr>
                            <tr>
                            <?php //if(!isset($PaymentVoucherItems)) :?>
                                <td><?php echo $this->Form->input('account',['type'=>'select','style'=>'width:100%','id'=>'account','class'=>'form-control select2','empty'=>[''=>'Select'],'label'=>false]); ?>
                                </td>
                                <td><?= $this->Form->input('reference',array('class'=>'form-control','type'=>'text','id'=>'reference','label'=>false)); ?>
                                </td>
                                <td><?= $this->Form->input('amount',array('class'=>'form-control number amounts','type'=>'text','required','id'=>'amount','label'=>false)); ?>
                                </td>
                                <td style="width:5%;text-align:center;">
                                    <a href="#" onclick="return costcenteradd('',0)" class="add_cost_btn">
                                        <i style="cursor:pointer;"
                                            class="fa_purchase fa fa-plus-square-o create_icon fa-2x"></i></a>
                                </td>
                                <td style="display: none">
                                    <?= $this->Form->input('cost_center',['id'=>'cost_center','type'=>'hidden','class'=>'form-control','label'=>false]); ?>
                                </td>
                                <td style="display: none">
                                    <?= $this->Form->input('cost_id',['id'=>'cost_id','type'=>'hidden','class'=>'form-control cost_id','label'=>false]); ?>
                                </td>
                                <td><i style="cursor:pointer"
                                        class="fa fa-plus-circle create_icon fa-2x add_to_list"></i></td>
                            <?php //endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($PaymentVoucherItems)) :?>
                            <?php foreach ($PaymentVoucherItems as $key => $value): ?>
                                <tr>
                                    <td><input value='<?= $value['AccountHead']['name']; ?>'  class='form-control' id="account" readonly type='text'>
                                    </td>
                                    <td><input value='<?= $value['Journal']['remarks']; ?>'  class='form-control' readonly type='text'>
                                    </td>
                                    <td><input value='<?= number_format((float)$value['Journal']['amount'], 2, '.', ''); ?>' readonly  class='form-control amounts_total number amounts' type='text'>
                                    </td>
                                    <td style="width:5%;text-align:center;">
                                        <a href="#">
                                            <i style="cursor:pointer;" class="fa_purchase fa fa-plus-square-o create_icon fa-2x cost_center_btn" data-toggle="modal" data-target="#CostCenter_View_modal" data-id='<?= $value['Journal']['id']; ?>'></i></a>
                                    </td>
                                    <td style="display: none">
                                        <?= $this->Form->input('cost_center',['id'=>'cost_center','type'=>'hidden','class'=>'form-control','label'=>false]); ?>
                                    </td>
                                    <td style="display: none">
                                        <?= $this->Form->input('cost_id',['id'=>'cost_id','type'=>'hidden','class'=>'form-control cost_id','label'=>false]); ?>
                                    </td>
                                    <td><i style="cursor:pointer"
                                        class="fa fa-minus-square-o fa-2x ad-mar remove_journal" id="<?= $value['Journal']['id']; ?>" ></i></td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif; ?>
                        </tbody>
                        <tfoot>
                            <tr class="blue-pddng">
                                <td></td>
                                <td>
                                    <h5><label class="control-label">Total</label></h5>
                                </td>
                                <td class="settings_unit_price">
                                    <?= $this->Form->input('total',array('class'=>'form-control number main_calculator','readonly','type'=>'text','required','id'=>'total','label'=>false)); ?>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr class="blue-pddng">
                                <td></td>
                                <td>
                                    <h5><label class="control-label">Narration</label></h5>
                                </td>
                                <td class="settings_unit_price">
                                    <?= $this->Form->input('narration',array('class'=>'form-control','type'=>'text','id'=>'narration','label'=>false)); ?>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr class="blue-pddng" style="display: none">
                                <td class="settings_unit_price"></td>
                                <td class="settings_unit_price"></td>
                                <td class="settings_unit_price"></td>
                                <td class="settings_unit_price"></td>
                                <td class="settings_unit_price"></td>
                                <td class="settings_unit_price">
                                    <h5><label class="control-label" style="float: right;">Total Amount Of Debit</label>
                                    </h5>
                                </td>
                                <td class="settings_unit_price">
                                    <?= $this->Form->input('grand_debittotal',array('class'=>'form-control number','readonly','type'=>'text','step'=>'any','required','id'=>'grand_debittotal','label'=>false)); ?>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr class="blue-pddng" style="display: none">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <h5><label class="control-label" style="float: right;">Total Amount Of
                                            Credit</label></h5>
                                </td>
                                <td class="settings_unit_price">
                                    <?= $this->Form->input('grand_credittotal',array('class'=>'form-control number main_calculator','readonly','type'=>'text','step'=>'any','required','id'=>'grand_credittotal','label'=>false)); ?>
                                </td>
                                <td colspan="2"></td>
                            </tr>
                            <tr class="blue-pddng">
                                <td class="settings_unit_price"></td>
                                <td class="settings_unit_price"></td>
                                <td class="settings_unit_price"></td>
                                <!-- <td class="settings_unit_price"></td>
                       <td class="settings_unit_price"></td> -->
                            <?php //if(!isset($PaymentVoucherItems)) :?>
                                <td class="settings_unit_price pull-right"><button type='Submit' name='data[Sale][process]'
                                        value='delivery' id="delivery"
                                        class="btn btn-primary invoice-action create_icon btnclick">Save</button>
                                </td>
                            <?php //endif; ?>
                                <!-- <td colspan="2"></td> -->
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <br /> <?= $this->Form->end(); ?>
        </div>
        <?php require('cost_center_add_modal.php') ?>
        <?php require('cost_center_modal.php') ?>
        <div id="deletemodel" class="modal fade">
            <div class="modal-dialog modal-confirm">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: steelblue;color:white;"><b>NOTE</b> </div>
                    <div class="modal-body" style="text-align:center">
                        <div style="display:none;" id="confirmmsg">
                            <b>Are you sure to continue??<br />Please confirm invoice payments before proceeding..</b>
                        </div>
                        <div style="display:none;" id="advancemsg">
                            <b>Do you want to credit the balance amount as advance??</b>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="cancelmodal" class="btn btn-info deleteCancel">Cancel</button>
                        <button type="button" id="continuemodal" class="btn btn-success">Yes</button>
                    </div>
                </div>
            </div>
            <div id="confirmadvancemodel" class="modal fade">
                <div class="modal-dialog modal-confirm">
                    <div class="modal-content">
                        <div class="modal-header" style="background-color: steelblue;color:white;"><b>NOTE</b> </div>
                        <div class="modal-body" style="text-align:center">
                            <b>Do you want to credit the balance amount as advance??</b>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="canceladd" class="btn btn-info deleteCancel">Cancel</button>
                            <button type="button" id="advanceadd" class="btn btn-success">Yes</button>
                        </div>
                    </div>
                </div>

</section>
<script type="text/javascript">
  $(document).ready(function () {
 $("#account").select2({
    placeholder: "Search here...",
    width: '100%',
    ajax: {
      url: '<?= $this->webroot ?>Accounts/Searchcustomer',
      dataType: 'json',
      delay: 250,
      data: function (params) {
            return {
                      q: params.term, // search term
                      page: params.page
                   };
          },
          processResults: function (data, params) {
            params.page = params.page || 2;
            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: false
        },
      minimumInputLength: 2,
      templateResult: formatRepo,
      templateSelection: formatRepoSelection
    });

  function formatRepo (repo) 
  {
    if (repo.loading) return repo.text;
    return repo.text;
  }
  function formatRepoSelection (repo)
  {
    return repo.text || repo.text;
  }
  });
</script>
<script type="text/javascript">
var value=$('#mode_category_ids option:selected').val();
if (value == 2) {
$('.cheque').show();
} else if (value == 3) {
$('.bank').show();
} else if (value == 1) {
$('.cash').show();
}else{
    $('.bank').hide();
    $('.cheque').hide();
    $('.cash').hide();
}
$('.cost_center_btn').click(function(){
    var id=$(this).attr('data-id');
    var data={
      // executive_id:executive_id,
      id:id,
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Accounts/cost_center_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {
                console.log(response);
                for(var i=0; i<response.length; i++){
                    $('.cost_center_id').val(response[i].CostCenterDetail.cost_center_id).trigger('change');
            $('.cost_amount').val(response[i].CostCenterDetail.amount);
                }
                $('.ad-mar').hide();
                $('.cost_center_id').prop('disabled', true);
                $('.cost_amount').prop('disabled', true);
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
})
$('#mode_category_ids').change(function() {
    //alert('k');
    var mode_category_id = $(this).val();
    //alert(mode_category_id);
    // var data={sub_group_id,sub_group_id};
    //var url_address= "<?= $this->webroot; ?>Accounts/GetSystemParameter/"+mode_category_id;
    // $.post( url_address,data, function( response ) {
    //console.log(response) ;

    if (mode_category_id == 2) {

        $('.bank').hide();
        $('.cheque').show();
        $('.cash').hide();
          $('.mode_cash').attr('required',false);
        $('.mode_bank').attr('required',true);
        // $('#main_group_id_edit').val(response.option);
        // $('#main_group_id_edit').attr('readonly',true);
        //$('#main_group_id_edit').val('');
    } else if (mode_category_id == 3) {

        $('.bank').show();
        $('.cheque').hide();
        $('.cash').hide();
          $('.mode_cash').attr('required',false);
        $('.mode_bank').attr('required',true);
        //$('#main_group_id_edit').val(response.option);
        //$('#main_group_id_edit').attr('readonly',true);
        //$('#main_group_id_edit').val('');

    } else {
        $('.bank').hide();
        $('.cheque').hide();
        $('.cash').show();
         $('.mode_cash').attr('required',true);
        $('.mode_bank').attr('required',false);

    }

    // },'json');
    //}
});
var sum = 0;
  $(".amounts_total").each(function(){
    sum += parseFloat($(this).val());
    //alert(sum);
  });
  $('#total').val(sum.toFixed(2));

// $(".add_to_list").click(function()
//    {
//           $("#deletemodel").modal('show');
//           $("#advancemsg").css('display','none');
//           $("#confirmmsg").css('display','block');
//    });
$('.add_to_list').click(function(event) {
    // alert("k");
    event.preventDefault();
    stock_quantity = 3000;
    var ProductExist = 0;
    var check1 = 0;
    var tablecount = $("#journal_table tbody tr").length;
    var account_text = $('#account option:selected').text();
    //var barcode=$('#barcode').val();
    var account = $('#account').val();
    var reference = $('#reference').val();
    var amount = $('#amount').val();
    var cost_id = $('#cost_id').val();
    var cost_center = $('#cost_center').val();
     if (!account) {
        $('#account').select2('open');
        return false;
    }
    if (!reference) {
        $('#reference').focus();
        return false;
    }
    if (!amount) {
        $('#amount').focus();
        return false;
    }

    $('#journal_table tbody').append('<tr>\
<td colspan="1">\
<input class="productlist productsrow" hidden name="data[JournalVoucher][acount_head_id][]" value="' + account + '">\
<input class="form-control account_name" value="' + account_text + '" readonly>\
</td>\
<td><input name="data[JournalVoucher][reference][]" class="form-control reference" value="' + reference + '" readonly>\
<td><input name="data[JournalVoucher][amount][]" class="form-control amounts" value="' + amount + '">\
</td>\
<td  style="text-align:center;"><a href="#" onclick="return costcenteradd(' + account + ',' + (parseInt(tablecount) +
            1) + ')"><i style="cursor:pointer;" class="fa_purchase fa fa-plus-square-o create_icon fa-2x"></i></a></td>\
<td style="display:none">\
<input name="data[JournalVoucher][cost_center][]" class="form-control cost_center" type="hidden" value="' +
        cost_center + '" readonly>\
</td>\
<td style="display:none">\
<input name="data[JournalVoucher][cost_id][]" class="form-control cost_ids" value="' + cost_id + '" readonly>\
</td>\
<td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
</tr>');
    $('.main_calculator').val('0');
    $('#amount').val('0');
    $('#reference').val('');
    // $('#barcode').val('');
    // $('#retail_price').val('0');
    // $('#whole_sale_price').val('0');
    // $('#size_id').val('');
    //$('#quantity_mode').val(0).trigger('change.select2');
    $('#account').val('').trigger('change.select2');
    $.fn.main_calculator();
    //$.fn.button_disable();
    //$.fn.total_tax();
    // if(barcode) {
    // $('#barcode').focus();
    // } else {
    // $('#product').select2('open');
    // //$('#whole_sale_price').val('0');
    // }
});
$(document).on('keyup','.amounts',function(){
$.fn.main_calculator();
});
$(document).on('click', '.remove_tr', function() {
    if (confirm('Are you sure?')) {
        $(this).closest('tr').remove();
        $('#total').val(0);
        //$.fn.button_disable();
    }
});
$(document).on('click','.remove_journal',function(){
    var id=$(this).attr('id');
    var url_address= "<?= $this->webroot; ?>Accounts/payment_voucher_detail_delete_ajax";
    if(confirm("Are you sure want to delete?")){
        $.ajax({
            url: url_address,
            type: "POST",
            data: {id:id},
            success: function (respnse) {
            var response =JSON.parse(respnse);
                    if(response.result!='Success')
                    {
                        alert(response.message);
                        return false;
                    }
                    else
                    {
                        location.reload();
                        // var table = $('#journal_table').dataTable();
                        // table.fnDraw();
                    }
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    }else{
        return false;
    }
});
$.fn.main_calculator = function() {
    //var total_quantity=0;
    var each_total = 0;
    //var each_taxable_total=0;
    //var each_tax_amount_total=0;
    //var each_discount_val=0;
    $('#journal_table tbody tr').each(function() {
        var single_total = $(this).closest('tr').find('td input.amounts').val();
        each_total += parseFloat(single_total);
        // var single_taxable_value=$(this).closest('tr').find('td input.row_taxable_value').val();
        // each_taxable_total+=parseFloat(single_taxable_value);
        // var single_tax_amount=$(this).closest('tr').find('td input.row_tax_amount').val();
        // each_tax_amount_total+=parseFloat(single_tax_amount);
        // var quantity=$(this).closest('tr').find('td input.quantity').val();
        // var free_qty=$(this).closest('tr').find('td input.free_qty').val();
        // var discount_val=$(this).closest('tr').find('td input.total_net_value').val();
        // each_discount_val+=parseFloat(discount_val);
        // //var quantity=$('.quantity').val();
        // var quantity_mode=$(this).closest('tr').find('td input.quantity_mode').val();
        // // if(quantity_mode=='Cases')
        // // {
        // total_quantity=parseFloat(total_quantity)+parseFloat(quantity);
        // // }
        // //total_quantity=1;
    });
    // $('#total_quantity').val(total_quantity);
    var main_total = each_total;
    $('#total').val(main_total.toFixed(2));

}
// $('#cheque_no').val('');
// $('#reference_no').val('');

function costcenteradd(id, count) {
    $("#count").val('');
    $("#prod_id").val('');
    $('#productslno_table tbody').children('tr:not(:first)').remove();
    if (!id) {
        if (!$("#account").val()) {
            alert("Please Select Account");
            $("#account").select2('open');
            return false;
        }else if (!$("#amount").val()) {
            alert("Please Enter Amount");
            return false;
        } else {
            var id = $("#account").val();
            $("#prod_id").val(id);
            $("#CostCenter_Add_modal").modal('show');
            var cost_amount = $("#cost_center").val();
        }
    }
    if (id) {
        //$('#productslno_table tbody').children( 'tr:' ).remove();
        //alert(id);
        $("#prod_id").val(id);
        // $("#CostCenter_Add_modal").modal('show');
        // if(count ==0)
        // {
        //   var itemslno = $("#cost_center").val();
        // }
        // else{
        var each_cost_amount = $('#journal_table tbody tr:nth-child(' + count + ')').find('td:nth-child(6)').find(
            'input').val();
        //}
        //var each_cost_amount=$(this).closest('tr').find('td input.cost_center').val();
        //var each_cost_amount=$(this).closest('tr').find('td.cost_center').val();
        //console.log(each_cost_amount);
        //alert(typeof(each_size_qty));
        // alert(each_size_qty);
        // var values=each_size_qty.split('<input');
        if (each_cost_amount) {
            var values = new Array();
            values = each_cost_amount.split(",");
            //  alert(typeof(values));
            console.log(values);
            //  $('#product_details_table tbody tr').each(function(i,e){
            $('#productslno_view_table tbody').html('');
            // $('#productslno_table tfoot').html('');
            $.each(values, function(i, e) {
                // console.log(e);
                ///var product_td=$(this).closest('tr').find('.each_size_qty').html();
                // var product_td=$(this).closest('tr').find('.each_size_qty').val();
                //alert(product_td);
                var valuess = new Array();
                valuess = e.split("-");
                //console.log(valuess);

                var costcenter_ids = valuess[0];
                var two = valuess[1];

                // var size=$(this).closest('tr').find('td:eq(0) input').val();
                // var total_qty_append=$(this).closest('tr').find('td:eq(1) input').val();
                //var available_qty_append=$(this).closest('tr').find('td:eq(3)').html()

                //  html += '<tr class="blue-pddng">';
                //  html += '   <td style="display:none">'+product_name_append+'</td>';
                //  html += '   <td>'+product_name_append+'</td>';
                //  html += '   <td>'+two+'</td>';
                //  html += '<tr>';
                // grand_total_qty+=two;
                //   //tfoot+=

                $('#productslno_view_table tbody').append('<tr class="blue-pddng">\
    <td>' + costcenter_ids + '</td>\
    <td>' + two + '</td>\
    <td style="display:none">' + 'Saved' + '</td>\
    </tr>');
                // grand_total_qty+=parseFloat(two);

            });
            $("#CostCenter_View_modal").modal('show');
            // $('#productslno_table tfoot').append('<tr class="blue-pddng">\
            //     <td>Total</td>\
            //     <td>'+grand_total_qty+'</td>\
            //     </tr>');

            // });
        }
    }
    $("#count").val(count);
    // if(itemslno) {
    //   $("#slno").val(itemslno);
    //   var temp = new Array();
    //   temp = itemslno.split(",");
    //   for (var i = 0; i < temp.length; i++) {
    //     var tr = '<tr class="blue-pddng">';
    //     tr += '<td>'+(parseInt(i)+1)+'</td>';
    //     tr += '<td class="itemslno">' + temp[i] + '</td>';
    //     tr += '<td><i style="curosr:pointer;" class="fa fa-minus-circle delete_icon fa-2x remove_slno_tr" ></i></td>';
    //     tr += '</tr>';
    //     $('#productslno_table tbody').append(tr);
    //   }
    // }
    // else
    // {
    //   $("#slno").val('');
    // }
    return true;
}

$(".add_costcenter").click(function() {
    var itemslno = $("#cost_amount").val();
    var costcenter_id = $('#cost_center_id').val();
    var costcenter_text = $('#cost_center_id option:selected').text();
    if (!costcenter_id) {
    alert('Please Select cost center');
        return false;
    }
    if (!itemslno) {
        $('#cost_amount').focus();
        return false;
    }
    // if(itemslno) {
    //   var costcenter_id=$('#cost_center_id option:selected').text();
    //   var slnostring = $("#slno").val() + ',' + itemslno;
    //   $("#slno").val(slnostring.replace(/^,/, ''));
    //   var tr = '<tr class="blue-pddng">';
    //   tr += '<td>'+costcenter_id+'</td>';
    //   tr += '<td class="itemslno">' + itemslno + '</td>';
    //   tr += '<td><i style="cursor:pointer;" class="fa fa-minus-circle delete_icon fa-2x remove_slno_tr" ></i></td>';
    //   tr += '</tr>';
    //   $('#productslno_table tbody').append(tr);
    // }
    $('#productslno_table tbody').append('<tr class="blue-pddng">\
<td>\
<input class="productlist costcenter_id" hidden name="data[JournalVoucher][costcenter_id][]" value="' + costcenter_id + '">\
<input class="form-control cost_center_text" value="' + costcenter_text + '" readonly>\
</td>\
<td>\
</td>\
<td><input class="cost_amount total_cost" hidden name="data[JournalVoucher][cost_amount][]" value="' + itemslno + '">\
<input  class="form-control" value="' + itemslno + '" readonly>\
</td>\
<td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
</tr>');
    $("#cost_amount").val('');
    $('#cost_center_id').val('').trigger('change.select2');
    //$("#cost_amount").focus();

});

$("#CostCenter_Add_Btn").click(function() {
    var total_amt=$('#total_amt').val();
    var sum = 0;
    $(".total_cost").each(function(){
        sum += parseFloat($(this).val());
        
    });
    if(sum==total_amt){
        $("#CostCenter_Add_modal").modal('hide');
        var count = $("#count").val();
        var cost_center_id = $("#cost_center_id").val();
        var slno = $("#slno").val();
        //    var new_arry=[0,0,0,0,0,0,0,0,0,0,0,0,0];
        //   $('.costcenter_id').each(function(){
        //     //if($(this).closest('tr').find('td.costcenter_id input').val()){
        //       // if($(this).closest('tr').find('td.costcenter_id input').val() != 0){
        //         new_arry[$(this).val()-1]=$(this).val();
        //       // }
        //     //}
        //   });
        // console.log(new_arry);
        //  var new_amount_arry=[0,0,0,0,0,0,0,0,0,0,0,0,0];
        //   $('.cost_amount').each(function(){
        //     //if($(this).closest('tr').find('td.cost_amount input').val()){
        //       // if($(this).closest('tr').find('td.cost_amount input').val() != 0){
        //         new_amount_arry[$(this).val()-1]=$(this).val();
        //      // }
        //     //}
        //   });
        // console.log(new_amount_arry);
        var myarray = [];
        var myarray1 = [];
        $('#productslno_table tbody tr').each(function() {
            var eq_size = $(this).closest('tr').find('td:eq(0) input').val();
            var cost_center_text = $(this).closest('tr').find('td input.cost_center_text').val();
            //alert(cost_center_text);
            var table_item_case = $(this).closest('tr').find('td:eq(2) input').val();
            //  alert(table_item_case);
            if (table_item_case != '' && table_item_case != 0) {
                myarray1.push(eq_size + '-' + table_item_case);
                myarray.push(cost_center_text + '-' + table_item_case);
            }
        });
        //console.log(myarray);
        // console.log(myarray1);
        $("#cost_id").val(myarray);
        $("#cost_center").val(myarray1);
        //var s1111=$.fn.size_convertion(new_arry);  
        // if(parseInt(count)<=0)
        // {
        //   $("#cost_id").val(cost_center_id);
        //   $("#cost_center").val(slno);
        // }
        // else{
        //   $('#product_table tbody tr:nth-child('+count+')').find('td:nth-child(12)').find('input').val(slno);
        // }
    }else{
        alert('Amount mismatch!');
    }
});
$('#add_cost_button').click(function() {
    var name = $('#cost_center_name_modal').val();
    if (name == '') {
        $('#cost_center_name_modal').focus();
        return false;
    }
    // var code=$('#state_code_modal').val();
    // if(code=='')
    // {
    //   $('#state_code_modal').focus();
    //   return false;
    // }
    var website_url = '<?php echo $this->webroot; ?>Accounts/add_cost_center_ajax';
    var data = {
        name: name,
        // code:code,
    };
    $.ajax({
        method: "POST",
        url: website_url,
        data: data,
        dataType: 'json',
    }).done(function(data) {
        if (data.result != 'Success') {
            alert(data.result);
            return false;
        }
        $('#cost_center_add_modal').modal('toggle');
        $('#cost_center_name_modal').val('');
        //$('#state_code_modal').val('');
        $('#cost_center_id').append($("<option></option>").attr("value", data.key).text(data.value));
        $('#cost_center_id').val(data.key).trigger('change');
        //location.reload();


    });
});
$('.add_cost_btn').click(function(){
    var single_total = $(this).closest('tr').find('td input.amounts').val();
    $('#total_amt').val(single_total);
    
});
</script>