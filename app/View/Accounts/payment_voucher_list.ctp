
<section class="content-header">
	<h1>Payment Voucher List
        <div class="pull-right">
            <a href="<?php echo $this->webroot ?>Accounts/PaymentVoucher"><button class='btn btn-success pull-right'>New Payment Voucher</button></a>&nbsp;
        </div>
    </h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-header">
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control invoice-search date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control invoice-search date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
				</div>
			</div>
			<!-- <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?= $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','empty'=>'All','class'=>'form-control select_two_class','style'=>'width: 100%')); ?>
				</div>
			</div> -->
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12"><br>
				<button class='btn btn-success' id='get_button'>Get</button>
			</div>
		</div>
		<div class="box-body" >
			<table class="table table-condensed table table-bordered"  id="table_data" data-order='[[ 0, "desc" ]]' data-page-length='10'>
				<thead>
					<tr class="blue-bg">
						<th width="10%">Date</th>
						<th width="10%">Document No</th>
						<th width="30%">Particular</th>
						<th width="30%">Reference</th>
						<th>Total Amt</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<!-- <tfoot>
					<tr>
						<th colspan="4" style="font-size:20px; color:red;text-align:right">Total:</th>
						<th style="font-size:20px; color:red;"></th>
						<th style="font-size:20px; color:red;"></th>
						<th></th>
						<th></th>

					</tr>
				</tfoot> -->
			</table>
		</div>
	</div>
</div>
</section>
<div id="payment_edit_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit</h4>
			</div>
			<div class="modal-body">
				<?php echo $this->Form->create('PaymentVoucher', ['class'=>'form-horizontal','id'=>'PaymentVoucher_Form']); ?>
				<?= $this->Form->input('journal_id',array('class'=>'form-control','type'=>'hidden','id'=>'journal_id','label'=>false)); ?>
				<div class="form-group">
					<label class="col-sm-4 control-label">Account</label>
					<div class="col-sm-6">
						<?php echo $this->Form->input('account_name',['type'=>'text','id'=>'account_name','class'=>'form-control','label'=>false,'readonly']); ?>
						<?php echo $this->Form->input('account',['type'=>'hidden','id'=>'account','class'=>'form-control','label'=>false]); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Reference</label>
					<div class="col-sm-6">
						<?= $this->Form->input('reference',array('class'=>'form-control','type'=>'text','id'=>'reference','label'=>false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Amount</label>
					<div class="col-sm-6">
						<?= $this->Form->input('amount',array('class'=>'form-control number amounts','type'=>'text','required','id'=>'amount','label'=>false)); ?>
					</div>
				</div>
				<?= $this->Form->end(); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn_radious" id='edit_payment_button'>Edit</button>
				<button type="button" class="btn btn-primary btn_radious" id='delete_payment_button'>Delete</button>
			</div>
		</div>
	</div>
</div>
<script>
	
	$(document).on('click','.payment_edit',function(){
		var payment_id=$(this).closest('tr').find('td span.table_id').text();
		var url_address= "<?= $this->webroot; ?>Accounts/journal_get_ajax/"+payment_id;
		$.get( url_address, function( response ) {
			console.log(response);
			if(response.result!='Success')
			{
				alert(response.result);
				return false;
			}
			$('#PaymentVoucher_Form')[0].reset();
			$("#account").val(response.data.debit);
			$("#account_name").val(response.AccountHeadDebit);
			$("#reference").val(response.data.remarks);
			$("#amount").val(parseFloat(response.data.amount).toFixed(2));
			$("#journal_id").val(payment_id);
			$('#payment_edit_modal').modal('show');
		}, "json");
	});
	$(document).on('click','#edit_payment_button',function(){
		var data=$('#PaymentVoucher_Form').serialize();
		var url_address= "<?= $this->webroot; ?>Accounts/journal_edit_ajax";
		var formData = new FormData($('#PaymentVoucher_Form')[0]); /////formdata object

		$.ajax({
			url: url_address,
			type: "POST",
			data: formData,
			processData: false,
			contentType: false,
			success: function (respnse) {
			var response =JSON.parse(respnse);
					if(response.result!='Success')
					{
						alert(response.result);
						return false;
					}
					else
					{
						
						$('#payment_edit_modal').modal('toggle');
						$('#PaymentVoucher_Form')[0].reset();
						var table = $('#table_data').dataTable();
						table.fnDraw();
						}
			},
			error: function (msg) {
				console.log(msg);
			}
		});
	});
	$(document).on('click','#delete_payment_button',function(){
		var data=$('#PaymentVoucher_Form').serialize();
		var url_address= "<?= $this->webroot; ?>Accounts/journal_delete_ajax";
		var formData = new FormData($('#PaymentVoucher_Form')[0]); /////formdata object
		if(confirm("Are you sure want to delete?")){
			$.ajax({
				url: url_address,
				type: "POST",
				data: formData,
				processData: false,
				contentType: false,
				success: function (respnse) {
				var response =JSON.parse(respnse);
						if(response.result!='Success')
						{
							alert(response.result);
							return false;
						}
						else
						{
							
							$('#payment_edit_modal').modal('toggle');
							$('#PaymentVoucher_Form')[0].reset();
							var table = $('#table_data').dataTable();
							table.fnDraw();
							}
				},
				error: function (msg) {
					console.log(msg);
				}
			});
		}else{
            return false;
        }
	});
</script>
<!-- <script type="text/javascript">
  $('#table_data').dataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Accounts/payment_voucher_ajax",
      "type": "POST",
      "dataSrc": "records",
    },
    "columns": [
        { "data" : "Journal.date" },
		{ "data" : "Journal.id" },
		{ "data" : "Journal.narration" },
		{ "data" : "Journal.amount" },
		{ "data" : "Journal.action" },
    ],
    "columnDefs": [
    ],
  });
</script> -->
<script type="text/javascript">
	$('#table_data').DataTable( {
		"processing": true,
		"serverSide": true,
        "order": [[ 0, 'asc' ]],
		"ajax": {
			"url": "<?= $this->webroot ?>Accounts/payment_voucher_ajax",
			"type": "POST",
			data:function( d ) {
				d.from_date= $('#from_date').val();
				d.to_date= $('#to_date').val();
				// d.executive_id= $('#executive_id').val();
			},
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "Journal.date" },
		{ "data" : "Journal.id" },
		{ "data" : "Journal.account_head" },
		{ "data" : "Journal.reference" },
		{ "data" : "Journal.amount",className:"text-right" },
		{ "data" : "Journal.action" },
		],
	});
	$('#get_button').click(function(){
		table = $('#table_data').dataTable();
		table.fnDraw();
	});

	function openInvoicePrint(id)
	{
			var newWindow=window.open(<?php echo $this->webroot ?>+'Print/fpdf/'+id);
			newWindow.focus();
			newWindow.print();
			
	}
</script>