<style type="text/css">
.algn_lft{
  text-align: center !important;
}
.pdng_tp{
  margin-top: 15px;
}
.clr_star {
  color: #a29da2;
}
.cart_clr {
  color: #a29da2;
}
.pgn_btm_rht{
  margin-bottom: 15px;
}
.color_of{
  color :red !important;
}
.size_of
{
  font-size:25px !important;
  padding-left: 55px !important;
}
.disable_field
{
pointer-events:none;
}
.simple_single_calculator,.quantity { text-align:right; }
</style>
<section class="content-header">
  <h2>Receipt Voucher
    <div class="pull-right"></div></h2>
</section>
<section class="content">
  <div class="row-wrapper">
    <div class="box box-primary">
    <?= $this->Form->create('JournalVoucher', array('url' => array('controller' => 'Accounts', 'action' => 'ReceiptVoucher')));?>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
          <div class="form-group">
              <div class="col-md-2" style="display: none">
                 <?= $this->Form->input('branch_id',array('type'=>'select','id'=>'branch_id','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>array(''=>'Select',$BranchList),'label'=>'Branch')); ?>
              </div>
             <div class="col-md-2" style="display: none">
                      <?= $this->Form->input('lctn_id',array('type'=>'select','id'=>'lctn_id','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>array(''=>'Select'),'label'=>'Location')); ?>
             </div>
               <div class="col-md-2">
                    <?php echo $this->Form->input('mode_category_ids',array('type'=>'select','class'=>'form-control select2','id'=>'mode_category_ids','style'=>'width: 100%;','options'=>$mode_category,'required','empty'=>'Select','label'=>'Mode Category')); ?>
             </div>
                <!-- <div class="col-md-2" style="display: none">
                                <label>Mode</label><br/>
                                <input class='nav-toggle' type="checkbox" name='data[JournalVoucher][settle_type]' id='toggle_button_for_type' data-width="130" data-toggle="toggle" data-off="Cash" data-on="Bank" data-onstyle="primary" data-offstyle="primary">
                           </div> -->
                           <div class="col-md-2 cash" style="display: none">
            <?php echo $this->Form->input('mode',array('type'=>'select','class'=>'form-control select2','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,)); ?>
          </div>
          <div class="col-md-2 cheque" style="display: none">
                 <?= $this->Form->input('mode',array('type'=>'select','id'=>'mode','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>array($banks),'label'=>'Bank')); ?>
              </div>
              <div class="col-md-2 cheque" style="display: none">
                <?= $this->Form->input('cheque_no',array('class'=>'form-control','type'=>'text','id'=>'cheque_no','label'=>'Cheque No')); ?>
              </div>
              <div class="col-md-2 cheque" style="display: none">
                <?php echo $this->Form->input('cheque_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'cheque_date','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Cheque Date')); ?>
              </div>
               <div class="col-md-2 bank" style="display: none">
                <?= $this->Form->input('mode',array('type'=>'select','id'=>'mode','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>array($banks),'label'=>'Bank')); ?>
              </div>
              <div class="col-md-2 bank" style="display: none">
                <?= $this->Form->input('reference_no',array('class'=>'form-control','type'=>'text','id'=>'reference_no','label'=>'Reference No')); ?>
              </div>
              <div class="col-md-2" style="display: none">
                <?php echo $this->Form->input('cheques_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'cheques_date','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date')); ?>
              </div>

                           <div class="col-md-2 bankdiv" style="display:none">
                                 <?= $this->Form->input('bank',array('type'=>'select','id'=>'bank','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>array($banks),'label'=>'Bank')); ?>
                           </div>
              <div class="col-md-2" style="pointer-events:none;">
                <?= $this->Form->input('journal_no',array('class'=>'form-control','type'=>'text','id'=>'journal_no','label'=>'Document No')); ?>
              </div>
              <div class="col-md-2" <?php if(strtolower($this->Session->read('UserRole.name') != 'admin')) { ?><?php } ?>>
                <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date')); ?>
              </div>
               <div class="col-md-2">
                <?= $this->Form->input('currency',array('class'=>'form-control','type'=>'text','required','id'=>'currency','label'=>'Currency','readonly')); ?>
              </div>
             </div>
            </div>
        </div>
      </div>
    </div>
   <div class="box-body table-responsive no-padding">
         <div class="col-md-12">
           <table class="table table-condensed table table-bordered" id="journal_table">
             <thead>
               <tr class="blue-bg">
                 <th style="display: none">Account Type</th>
                 <th>Account</th>
                 <th style="display: none">Debit/Credit</th>
                 <th style="display: none">Div</th>
                 <th style="display: none">LCN</th>
                <th>Reference</th>
                 <th>Amount</th>
                 <th width="10%">Action</th>
               </tr>
               <tr>
                 <td width="10%" style="display: none"><?php echo $this->Form->input('account_type',['type'=>'select','style'=>'width:100%','id'=>'account_type','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'options'=>$accountypes,'label'=>false]); ?></td>
                 <td><?php echo $this->Form->input('account',['type'=>'select','style'=>'width:100%','id'=>'account','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'options'=>$AccountHead_list,'label'=>false]); ?></td>
                 <td width="13%" style="display: none"><?php echo $this->Form->input('debit_credit',['type'=>'select','style'=>'width:100%','id'=>'debit_credit','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'options'=>['Debit'=>'Debit','Credit'=>'Credit'],'label'=>false]); ?></td>
                 <td width="12%" style="display: none"><?php echo $this->Form->input('division_id',array('type'=>'select','id'=>'division_id','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>['' => 'Select',$Divisions],'empty'=>'Select','label'=>false)); ?></td>
                 <td width="12%" style="display: none"><?php echo $this->Form->input('location_id',array('type'=>'select','id'=>'location_id','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>[''=>'Select',$Locations],'label'=>false,'empty' => 'Select')); ?></td>
                 <td><?= $this->Form->input('reference',array('class'=>'form-control','type'=>'text','id'=>'reference','label'=>false)); ?></td>
                 <td><?= $this->Form->input('amount',array('class'=>'form-control number','type'=>'text','required','id'=>'amount','label'=>false)); ?></td>
                 <td><i style="cursor:pointer" class="fa fa-plus-circle create_icon fa-2x add_to_list"></i></td>
               </tr>
             </thead>
             <tbody>
             </tbody>
             <tfoot>
            <tr class = "blue-pddng">
             <td></td>
             <td><h5><label class="control-label">Total</label></h5></td>
             <td class="settings_unit_price"><?= $this->Form->input('total',array('class'=>'form-control number main_calculator','readonly','type'=>'text','required','id'=>'total','label'=>false)); ?></td>
             <td colspan="2"></td>
             </tr>
              <tr class = "blue-pddng">
             <td></td>
             <td><h5><label class="control-label">Narration</label></h5></td>
             <td class="settings_unit_price"><?= $this->Form->input('narration',array('class'=>'form-control','type'=>'text','id'=>'narration','label'=>false)); ?></td>
             <td colspan="2"></td>
             </tr>
             <tr class = "blue-pddng" style="display: none">
             <td class="settings_unit_price"></td>
             <td class="settings_unit_price"></td>
             <td class="settings_unit_price"></td>
             <td class="settings_unit_price"></td>
             <td class="settings_unit_price"></td>
             <td class="settings_unit_price"><h5><label class="control-label" style="float: right;">Total Amount Of Debit</label></h5></td>
             <td class="settings_unit_price"><?= $this->Form->input('grand_debittotal',array('class'=>'form-control number','readonly','type'=>'text','step'=>'any','required','id'=>'grand_debittotal','label'=>false)); ?></td>
             <td colspan="2"></td>
             </tr>
             <tr class = "blue-pddng" style="display: none">
             <td></td>
             <td></td>
             <td></td>
             <td></td>
             <td></td>
             <td><h5><label class="control-label" style="float: right;">Total Amount Of Credit</label></h5></td>
             <td class="settings_unit_price"><?= $this->Form->input('grand_credittotal',array('class'=>'form-control number main_calculator','readonly','type'=>'text','step'=>'any','required','id'=>'grand_credittotal','label'=>false)); ?></td>
             <td colspan="2"></td>
             </tr>
              <tr class = "blue-pddng">
                       <td class="settings_unit_price"></td>
                       <td class="settings_unit_price"></td>
                       <td class="settings_unit_price"></td>
                       <!-- <td class="settings_unit_price"></td>
                       <td class="settings_unit_price"></td> -->
                       <td class="settings_unit_price"><button type='Submit' name='data[Sale][process]' value='delivery' id="delivery" class="btn btn-primary invoice-action create_icon btnclick">Save</button></td>
                       <!-- <td colspan="2"></td> -->
              </tr>
             </tfoot>
           </table>
         </div>
       </div>
       	<div class="modal fade" id="invoice_check_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
       					<div class="modal-dialog">
       						<div class="modal-content modal_height" style="width:150%" >
       							<div class="modal-header">
       								<h4 class="modal-title" align='center'><span id='name_modal'></span>:  Invoice List</h4>
       							</div>
       							<div class="modal-body">
       								<div class="form-group">
       									<div class="col-sm-9">
       										<div class="col-sm-4">
       											<?= $this->Form->input('invoice_amount',array('class'=>'form-control number number_field money_keyup field_select','id'=>'invoice_amount','label'=>'Amount','value'=>'0','type'=>'text','step'=>1)) ?>
       										</div>
       										<div class="col-sm-4">
       											<?= $this->Form->input('balance_amount',array('class'=>'form-control number_field money_keyup field_select','id'=>'balance_amount','label'=>'Balance','readonly','value'=>'0')) ?>
       											<?= $this->Form->input('pending_balance',array('class'=>'form-control number_field money_keyup field_select','id'=>'pending_balance','label'=>'Balance','readonly','value'=>'0','type'=>'hidden')) ?>
       										</div>
       										<div class="col-sm-4">
       											<br>
       											<button type="button" class="btn-sm btn-primary btn" id='invoice_button'  data-dismiss="modal">Submit</button>
       										</div>
       									</div>
       								</div>
       								<br>
       								<br>
       								<br>
       								<div  id="customer_invoice_listdiv" style="display:none;" >
       								<table class="table table-condensed table-bordered" id="customer_invoice_list" data-page-length='100'>
       									<thead>
       										<tr class='blue-bg'>
       											<th width="2%" hidden></th>
       											<th width="2%" hidden></th>
       											<th width="15%">Date</th>
       											<th width="16%">Invoice</th>
       											<th  width="14%">Balance</th>
       											<th width="15%">Invoice Amount</th>
       											<th width="15%">Remaining</th>
       										</tr>
       									</thead>
       									<tbody>
       									</tbody>
       								</table>
       								</div>
       								<div  id="general_invoice_listdiv" style="display:none;" >
       								<table class="table table-condensed table-bordered"  id="general_invoice_list" data-page-length='100'>
                                           			<thead>
                                           			<tr class='blue-bg'>
                                           				<th width="2%" hidden></th>
                                           				<th width="2%" hidden></th>
                                           				<th width="40%">Account</th>
                                           				<th  width="20%">Balance</th>
                                           				<th width="20%">Invoice Amount</th>
                                           				<th width="20%" hidden>Remaining</th>
                                           			</tr>
                                           			</thead>
                                           			<tbody>
                                           			</tbody>
                                       </table>
                             			</div>
       							</div>
       						</div>
       					</div>
       				</div>
      <br/> <?= $this->Form->end(); ?>
     </div>
 <div id="deletemodel" class="modal fade">
   <div class="modal-dialog modal-confirm">
       <div class="modal-content">
           <div class="modal-header" style="background-color: steelblue;color:white;"><b>NOTE</b>  </div>
           <div class="modal-body" style="text-align:center">
           <div style="display:none;" id="confirmmsg">
               <b>Are you sure to continue??<br/>Please confirm invoice payments before proceeding..</b>
           </div>
             <div style="display:none;" id="advancemsg">
               <b>Do you want to credit the balance amount as  advance??</b>
           </div>
           </div>
           <div class="modal-footer">
               <button type="button" id="cancelmodal" class="btn btn-info deleteCancel">Cancel</button>
               <button type="button" id="continuemodal" class="btn btn-success">Yes</button>
           </div>
       </div>
   </div>
    <div id="confirmadvancemodel" class="modal fade">
          <div class="modal-dialog modal-confirm">
              <div class="modal-content">
                  <div class="modal-header" style="background-color: steelblue;color:white;"><b>NOTE</b>  </div>
                  <div class="modal-body" style="text-align:center">
                      <b>Do you want to credit the balance amount as  advance??</b>
                  </div>
                  <div class="modal-footer">
                      <button type="button" id="canceladd" class="btn btn-info deleteCancel">Cancel</button>
                      <button type="button" id="advanceadd" class="btn btn-success">Yes</button>
                  </div>
              </div>
          </div>
   </section>
   <script type="text/javascript">

$('#mode_category_ids').change(function(){
//alert('k');
  var mode_category_id=$(this).val();
//alert(mode_category_id);
 // var data={sub_group_id,sub_group_id};
 //var url_address= "<?= $this->webroot; ?>Accounts/GetSystemParameter/"+mode_category_id;
 // $.post( url_address,data, function( response ) {
     //console.log(response) ;

    if(mode_category_id==2)
    {

      $('.bank').hide();
      $('.cheque').show();
      $('.cash').hide();
     // $('#main_group_id_edit').val(response.option);
     // $('#main_group_id_edit').attr('readonly',true);
     //$('#main_group_id_edit').val('');
    }
    else if(mode_category_id==3)
    {
     
     $('.bank').show();
     $('.cheque').hide();
      $('.cash').hide();
     //$('#main_group_id_edit').val(response.option);
      //$('#main_group_id_edit').attr('readonly',true);
       //$('#main_group_id_edit').val('');

    }
    else{
      $('.bank').hide();
      $('.cheque').hide();
       $('.cash').show();

    }

 // },'json');
//}
});

// $(".add_to_list").click(function()
//    {
//           $("#deletemodel").modal('show');
//           $("#advancemsg").css('display','none');
//           $("#confirmmsg").css('display','block');
//    });
   $('.add_to_list').click(function(event){
   // alert("k");
event.preventDefault();
stock_quantity=3000;
var ProductExist = 0;
var check1=0;
var tablecount=$("#journal_table tbody tr").length;
var account_text=$('#account option:selected').text();
//var barcode=$('#barcode').val();
var account=$('#account').val();
var reference=$('#reference').val();
var amount=$('#amount').val();
if(!amount)
{
$('#amount').focus();
return false;
}

$('#journal_table tbody').prepend('<tr>\
<td colspan="1">\
<input class="productlist productsrow" hidden name="data[JournalVoucher][acount_head_id][]" value="'+account+'">\
<input class="form-control account_name" value="'+account_text+'" readonly>\
</td>\
<td><input name="data[JournalVoucher][reference][]" class="form-control reference" value="'+reference+'" readonly>\
<td><input name="data[JournalVoucher][amount][]" class="form-control amounts" value="'+amount+'" readonly>\
</td>\
<td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
</tr>');
$('.main_calculator').val('0');
$('#amount').val('0');
$('#reference').val('');
// $('#barcode').val('');
// $('#retail_price').val('0');
// $('#whole_sale_price').val('0');
// $('#size_id').val('');
//$('#quantity_mode').val(0).trigger('change.select2');
$('#account').val('').trigger('change.select2');
$.fn.main_calculator();
//$.fn.button_disable();
//$.fn.total_tax();
// if(barcode) {
// $('#barcode').focus();
// } else {
// $('#product').select2('open');
// //$('#whole_sale_price').val('0');
// }
});
$(document).on('click','.remove_tr',function(){
  if (confirm('Are you sure?')) {
$(this).closest('tr').remove();
$('#total').val(0);
//$.fn.button_disable();
}
});
$.fn.main_calculator=function(){
//var total_quantity=0;
var each_total=0;
//var each_taxable_total=0;
//var each_tax_amount_total=0;
//var each_discount_val=0;
$('#journal_table tbody tr').each(function(){
var single_total=$(this).closest('tr').find('td input.amounts').val();
each_total+=parseFloat(single_total);
// var single_taxable_value=$(this).closest('tr').find('td input.row_taxable_value').val();
// each_taxable_total+=parseFloat(single_taxable_value);
// var single_tax_amount=$(this).closest('tr').find('td input.row_tax_amount').val();
// each_tax_amount_total+=parseFloat(single_tax_amount);
// var quantity=$(this).closest('tr').find('td input.quantity').val();
// var free_qty=$(this).closest('tr').find('td input.free_qty').val();
// var discount_val=$(this).closest('tr').find('td input.total_net_value').val();
// each_discount_val+=parseFloat(discount_val);
// //var quantity=$('.quantity').val();
// var quantity_mode=$(this).closest('tr').find('td input.quantity_mode').val();
// // if(quantity_mode=='Cases')
// // {
// total_quantity=parseFloat(total_quantity)+parseFloat(quantity);
// // }
// //total_quantity=1;
 });
// $('#total_quantity').val(total_quantity);
var main_total=each_total;
$('#total').val(main_total.toFixed(2));

}
$('#cheque_no').val('');
$('#reference_no').val('');

   </script>
