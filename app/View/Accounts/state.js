$('#add_state_button').click(function(){
	var name=$('#state_name_modal').val();
	if(name=='')
	{
		$('#state_name_modal').focus();
		return false;
	}
	var code=$('#state_code_modal').val();
	if(code=='')
	{
		$('#state_code_modal').focus();
		return false;
	}
	var website_url='<?php echo $this->webroot; ?>Accountings/add_state_ajax';
	var data={
		name:name,
		code:code,
	};
	$.ajax({
		method: "POST",
		url: website_url,
		data: data,
		dataType:'json',
	}).done(function( data ) {
		if(data.result!='Success')
		{
			alert(data.result);
			return false;
		}
		$('#state_add_modal').modal('toggle');
		$('#state_name_modal').val('');
		$('#state_code_modal').val('');
		$('#state_id').append($("<option></option>").attr("value",data.key).text(data.value));
		$('#state_id').val(data.key).trigger('change');
		//location.reload();


	});
});
$('#state_id').change(function(){
	var id=$(this).val();
	$.get( "<?= $this->webroot ?>Accountings/get_state_code_ajax/"+id,function( data ) {
		$('#state_code').val(data.code);
	}, "json");
});
$('#state_id').trigger('change');