<section class="content-header">
  <h1>Sub Group</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
    </div>
      <div class="box-body">
      <div class="row">
             <?= $this->Form->create('SubGroup', array('id'=>'SubGroup_Form'));?>
      <?= $this->Form->hidden('id',['id'=>'SubGroup_id']); ?>
       <div class="col-md-12 col-sm-12">
        <div class="col-md-3 col-sm-3">
          <!-- <div class="form-group col-md-8"> -->
           <?php echo $this->Form->input('MainGroup',array('type'=>'select','empty' =>'ALL','options' => $AccMainGroup,'id'=>'main_group_id','class'=>'form-control select_two_class product_flitering search_class product_flitering','label'=>'Main Group')) ?>
          </div>
          <div class="col-md-3 col-sm-3">
             <?= $this->Form->input('name',array('type'=>'text','id'=>'name','class'=>'form-control','required'=>'required','label'=>'Name of Sub Group')); ?>
          </div>
        <!-- </div> -->

        <div class="col-md-3 col-sm-3">
          <!-- <div class="form-group col-md-12"> -->
           <?= $this->Form->input('code',array('type'=>'text','id'=>'code','class'=>'form-control','label'=>'Code','readonly')); ?>
          <!-- </div> -->
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
 <button type="button" class="btn btn-success" id='save_button' style="margin-top: 25px;">Save</button>
          </div>
        </div>
        
      </div>
    <!-- </div> -->
      <?= $this->Form->end(); ?>
    </div>
    <!-- </div> -->
    <div class="box-body">
      <table class="table table-hover boder table-bordered" id='table_data'>
        <thead>
          <tr class="blue-bg">
            <th>#</th>
           <th width="">Main Group</th>
            <th width="">Sub Group</th>
            <th width="">Code</th>
            <th width="10%">Action</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</section>
<script type="text/javascript">
  table_data=$('#table_data').dataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Accounts/SubGroup_Table_ajax",
      "type": "POST",
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "AccSubGroup.id" },
    { "data" : "AccMainGroup.name" },
    { "data" : "AccSubGroup.name" },
    { "data" : "AccSubGroup.code" },
    { "data" : "AccSubGroup.action" },
    ],
    "columnDefs": [
    ],
  });
</script>
<script type="text/javascript">
  $('input').click(function(){
    $(this).select();
  });

  $('#save_button').click(function(){
    var main_group_id=$('#main_group_id').val();
    if(!main_group_id)
    {
      $('#main_group_id').select2('open');
      return false;
    }
    var name=$('#name').val();
     if(!name)
    {
      $('#name').focus();
      return false;
    }
    var data=$('#SubGroup_Form').serialize();
    var url_address= "<?= $this->webroot; ?>Accounts/SubGroup_Save_ajax";
    $.post( url_address,data, function( response ) {
      if(response.result!='success') { alert(response.result); return false; }
      $('#SubGroup_Form')[0].reset();
      $('#SubGroup_Form input[name="data[AccSubGroup][id]"]').val(0);
      $('#main_group_id').val("").trigger('change');
      $("#main_group_id").prop("disabled", false);
      table_data.fnDraw();
    }, "json");
  });
  $(document).on('click','.delete_SubGroup',function(){
    if(!confirm('Are You Sure')) { return false; }
    id=$(this).attr('table_id');
    var url_address= "<?= $this->webroot; ?>Accounts/Delete_SubGroup_ajax/"+id;
    $.get( url_address, function( response ) {
      if(response.result!='Success') { alert(response.result); return false; }
      table_data.fnDraw();
    }, "json");
  });
  $(document).on('click','.edit_SubGroup',function(){
    id=$(this).attr('table_id');
    var url_address= "<?= $this->webroot; ?>Accounts/SubGroup_type_get_ajax/"+id;
    $.get( url_address, function( response ) {
 $('#SubGroup_id').val(response.AccSubGroup.id);
      $('#name').val(response.AccSubGroup.name);
       $('#code').val(response.AccSubGroup.code);
  $('#main_group_id').val(response.AccMainGroup.id).trigger('change');
        $("#main_group_id").prop("disabled", true);

    }, "json");
  });

  $('#main_group_id').change(function(){
  var id=$(this).val();
  $.get( "<?= $this->webroot ?>Accounts/get_sub_group_code_ajax/"+id,function( data ) {
    console.log(data);
    $('#code').val(data.code);
  }, "json");
});
</script>
