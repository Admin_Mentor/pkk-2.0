<div id="addsub_group" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Add Sub Group</h4>
      </div>
      <div class="modal-body">
      	<div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Main Group</label>
            <div class="col-sm-9">
               <?php echo $this->Form->input('MainGroup',array('type'=>'select','empty' =>'ALL','options' => $MainGroup_list,'id'=>'main_group_id_add','class'=>'form-control select_two_class','style'=>array('width:100%'),'label'=>false,)) ?>
              <span id="sub_group_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>

        <div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label"> Name of Sub Group</label>
            <div class="col-sm-9">
              <input class="form-control location_disable toUpperCase " placeholder="" type="text" id="modal_sub_group_name">
              <span id="sub_group_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>
                  <div class="form-horizontal">
          <div class="form-group" style="display: none">
            <label for="inputEmail3" class="col-sm-3 control-label"> Code</label>
            <div class="col-sm-9">
              <input class="form-control location_disable toUpperCase " placeholder="" type="text" id="modal_sub_group_code">
              <span id="sub_group_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>
          
        </div>
      
      <div class="modal-footer">
        <button  type='button' class="save btn btn-success" id="add_sub_group">Save</button>
      </div>
      </div>
    </div>
  </div>