<style>
@media (min-width: 992px)
.col-md-2{
    width: 16.66666667%;
}
</style>
<section class="content-header">
	<h1>Journal Voucher Transaction </h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
			<div class="col-md-12">
				<div class="form-group">
					<div class="col-md-2 col-sm-2">
						<?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date *')); ?>
					</div>
					<div class="col-md-2 col-sm-2">
                    	<?= $this->Form->input('document_no',array('class'=>'form-control','id'=>'document_no','readonly','label'=>'Document No *')) ?>
                    </div>
					<div class="col-md-2 col-sm-2">
						<?= $this->Form->input('to_type',array('type'=>'select','class'=>'form-control select_two_class','style'=>'width: 100%;','id'=>'to_type','empty'=>[''=>'Select'],'options'=>$accountypes,'required','label'=>'Debit Account Type *')); ?>
					</div>
					<div class="col-md-2 col-sm-2">
						<?= $this->Form->input('to_account',array('type'=>'select','class'=>'form-control account_head_class select_two_class','id'=>'type','style'=>'width: 100%;','id'=>'to_account','empty'=>[''=>'Select'],'options'=>[],'required','label'=>'Debit Account *',)); ?>
					</div>
									<div class="col-md-2 col-sm-2" style="display:none;">
                                                           <?= $this->Form->input('division',array('type'=>'select','class'=>'form-control select_two_class','style'=>'width: 100%','empty' => 'Select','default' =>'Select','options'=>$Divisions,'label'=>'Division','required'=>'required')); ?>
                					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="form-group">
								<div class="col-md-2 col-sm-2" style="display:none;">
                                                             <?= $this->Form->input('lcn',array('type'=>'select','id'=>'lcn','class'=>'form-control select_two_class','style'=>'width: 100%','empty' => 'Select','default' =>'Select','options'=>$Locations,'label'=>'LCN','required'=>'required')); ?>
                               	</div>
				      <div class="col-md-2 col-sm-2">
                         <?= $this->Form->input('by_type',array('type'=>'select','class'=>'form-control select_two_class','id'=>'branch','style'=>'width: 100%;','id'=>'by_type','empty'=>[''=>'Select'],'options'=>$accountypes,'required','label'=>'Credit Account  Type *',)); ?>
                      </div>
                	  <div class="col-md-2 col-sm-2">
                		<?= $this->Form->input('by_account',array('type'=>'select','class'=>'form-control account_head_class select_two_class','id'=>'by_account','style'=>'width: 100%;','empty'=>[''=>'Select'],'options'=>[],'required','label'=>'Credit Account *',)); ?>
                	</div>
					<div class="col-md-2 col-sm-2">
                		<?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher')) ?>
                	</div>
					<div class="col-md-2 col-sm-2" style="    width: 10.333333%;">
						<?= $this->Form->input('amount',array('class'=>'form-control number text-right','type'=>'text','required','id'=>'amount','label'=>'Amount *')); ?>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12" style="display:none">
						<?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
					</div>
					 <input type="hidden" id="pagetype" name="pagetype" value="<?=$page_type?>">
					<div class="col-md-2 col-lg-2 col-sm-3 col-xs-12" style="    margin-left: -1%;">
						<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','cols'=>'50','id'=>'remarks','label'=>'Remarks')) ?>
					</div>
					<div class="col-md-1" style="margin-left: 94%;margin-top: -4%;"> <br>
						<button type='submit' class="user_add_btn create_icon" style="width:74%">ADD</button>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body table-responsive no-padding">
			<table class="boder table table-condensed" id="table_data" style="width:100%">
				<thead>
					<tr class="blue-bg">
						<th class="padding_left">Acc Name</th>
						<th class="text-right">Total</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</section>
<?php require('voucher_journal_transaction.php') ?>
<script type="text/javascript">
	$('#table_data').DataTable( {
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Accountings/VoucherGeneral_table_ajax",
			"type": "POST",
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "AccountHead.name" },
		{ "data" : "AccountHead.total" },
		],
		"order": [[0, 'asc' ]],
		"columnDefs": [
		{ className: "name", "targets": [ 0 ] },
		{ className: "text-right", "targets": [ 1 ] },
		],
		"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
			$(nRow).addClass('view_transaction');
			return nRow;
		}
	});
	$('button[type="Submit"]').click(function(){
		var branch=$('#branch').val();
		if(!branch)
        {
        	$('#branch').select2('open');
        	return false;
        }
		var to_account=$('#to_account').val();
		if(!to_account)
		{
			$('#to_account').select2('open');
			return false;
		}
		var by_account=$('#by_account').val();
		if(!by_account)
		{
			$('#by_account').select2('open');
			return false;
		}
    	var amount=$('#amount').val();
		if(!amount)
		{
			$('#amount').select2('open');
			return false;
		}
	});
	$('.account_head_class').change(function(){
		var to_account=$('#to_account').val();
		var by_account=$('#by_account').val();
		if(to_account==by_account)
		{
			alert('Please select Diffrent Option');
			$(this).val('');
		}
	});

$("#branch").change(function()
{
    var id = $(this).val();
	if(id)
	{
		var value = {
			           'id': id,
			           'type': 'Journal Voucher',
	                }
		var url_address = "<?= $this->webroot; ?>Accountings/get_branch_document_no";
		$.post(url_address, value, function (response) {
			$("#document_no").val(response);
		}, "json");
	}
});


$("#to_type").change(function()
{
	var to_type = $(this).val();
	$("#to_account").html('');
	$("#to_account").html('<option value="">Select</option>');
	if(to_type)
	{
		var url_address = "<?= $this->webroot; ?>Accountings/accountheadbygroup/"+to_type
		$.get(url_address, function (response)
			{
				$("#to_account").html(response.option);
				$("#to_account").select2('open');
			},
			"json");
	}
});

$("#by_type").change(function()
{
	var by_type = $(this).val();
	$("#by_account").html('');
	$("#by_account").html('<option value="">Select</option>');
	if(by_type)
	{
		var url_address = "<?= $this->webroot; ?>Accountings/accountheadbygroup/"+by_type
		$.get(url_address, function (response)
			{
				$("#by_account").html(response.option);
				$("#by_account").select2('open');
			},
			"json");
	}
});
$("#branch").change(function()
{
       var val =$(this).val();
           var url = window.location.pathname;
           var count = url.split("/").length-1;
       if(val){
        var url_address = "<?= $this->webroot; ?>Stock/branch_lcn/"+val;
		$.post(url_address,  function (response) {
				$("#lcn").val(response).trigger('change');
		}, "json");
		}
});
</script>