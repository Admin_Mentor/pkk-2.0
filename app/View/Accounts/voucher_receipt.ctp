<style>
.acc_add_btn {
    padding-left: 0;
    width: 3%;
}
</style>
<section class="content-header">
    <h1> Receipt Voucher
    <div class="pull-right">
            <a href="<?php echo $this->webroot ?>Accounts/VoucherReceiptList"><button class='btn btn-success pull-right'>Receipt Voucher List</button></a>&nbsp;
        </div>
  </h1>
</section>
<section class="content">
    <div class="row-wrapper">
        <div class="box box-primary">
            <?php echo $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
             <?php if(isset($ReceiptVoucherList)) $readonly='disabled'; else $readonly=''; ?>
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
          <div class="form-horizontal" style="margin-top: 15px;">
            <div class="box-body">
              <div class="form-group">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                  <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','disabled'=>$readonly,)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3" style="pointer-events:none;">
                  <?= $this->Form->input('journal_no',array('class'=>'form-control','type'=>'text','id'=>'journal_no','label'=>'Document No',$readonly)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3" style="display: none">
                  <?= $this->Form->input('Journal_id',array('class'=>'form-control','type'=>'text','id'=>'journal_id','label'=>'Id',)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                  <?= $this->Form->input('currency',array('class'=>'form-control','type'=>'text','required','id'=>'currency','label'=>'Currency','readonly')); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                  <?php if(!isset($this->request->data['Journal']['id'])) :?>
                  <?php echo $this->Form->input('account_head',array('type'=>'select','class'=>'form-control select2','id'=>'type','style'=>'width: 100%;','id'=>'account_head','empty'=>'','required','label'=>'Acc/Name','disabled'=>$readonly,)); ?>
                <?php endif; ?>
                 <?php if(isset($this->request->data['Journal']['id'])) :?>
                 <?php echo $this->Form->input('account_head_name',array('type'=>'text','class'=>'form-control','id'=>'type','id'=>'account_head_name','empty'=>'All','required','label'=>'Acc/Name','disabled'=>$readonly,)); ?>
                 <?php endif; ?>
                </div>
                <?php if(!isset($ReceiptVoucherList)) :?>
                <div class="col-md-1 col-sm-1 col-lg-1 acc_add_btn">
                  <i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal"
                    data-target="#customer_modal_add" style="margin-top: 25px;"></i>
                </div>
                 <?php endif; ?>
              </div>
            </div>
            <div class="box-body">
              <div class="form-group">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                  <?= $this->Form->input('external_voucher',array('class'=>'form-control','id'=>'external_voucher','disabled'=>$readonly,)) ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                  <?= $this->Form->input('amount',array('class'=>'form-control number text-right','type'=>'text','required','id'=>'amount',$readonly)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" style="display:none">
                  <?= $this->Form->input('voucher_no',array('class'=>'form-control','id'=>'voucher_no')) ?>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                  <?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'3','required','id'=>'remarks','label'=>'Remarks','disabled'=>$readonly,)) ?>
                </div>
              </div>
            </div>
            <div class="box-body">
              <div class="form-group">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                  <?php echo $this->Form->input('mode_category_ids',array('type'=>'select','class'=>'form-control select2','id'=>'mode_category_ids','style'=>'width: 100%;','options'=>$mode_category,'empty'=>'Select','required','label'=>'Mode Category','disabled'=>$readonly,)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 cash" style="display: none">
                  <?php echo $this->Form->input('mode_cash',array('type'=>'select','class'=>'form-control select2','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'disabled'=>$readonly,)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 cheque" style="display: none">
                  <?= $this->Form->input('mode_cheque',array('type'=>'select','id'=>'mode','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>array($banks),'label'=>'Bank','disabled'=>$readonly,)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 cheque" style="display: none">
                  <?= $this->Form->input('cheque_no',array('class'=>'form-control','type'=>'text','id'=>'cheque_no','label'=>'Cheque No','disabled'=>$readonly,)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 cheque" style="display: none">
                  <?php echo $this->Form->input('cheque_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'cheque_date','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Cheque Date','disabled'=>$readonly,)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 bank" style="display: none">
                  <?= $this->Form->input('mode_bank',array('type'=>'select','id'=>'mode','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>array($banks),'label'=>'Bank','disabled'=>$readonly,)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12 bank" style="display: none">
                  <?= $this->Form->input('reference_no',array('class'=>'form-control','type'=>'text','id'=>'reference_no','label'=>'Reference No','disabled'=>$readonly,)); ?>
                </div>
              </div>
            </div>
             <?php if(!isset($ReceiptVoucherList)) :?>
            <div class="box-body pull-right" style="margin-right: 10%">
              <div class="form-group">
                <div class="col-md-5"></div>
                <!-- <div class="col-md-2"> <br> -->
                  <button class="user_add_btn">Save & Print</button>
                <!-- </div> -->
              </div>
            </div>
             <?php endif; ?>
             <?php if(isset($ReceiptVoucherList)) :?>
            <div class="box-body">
              <div class="form-group pull-right" style="margin-right: 10%">
                <div class="col-md-5"></div>
                <div class="col-md-2"> <br>
                  <!-- <button class="btn btn-success" id="print">Print</button> -->
                   <a target="_blank" href="<?= $this->webroot.'Print/receipt_voucher_fpdf/'.$this->request->data['Journal']['account_head'].'/'.$this->request->data['Journal']['voucher_no'].'/'.$this->request->data['Journal']['amount']; ?>"><button type='button' class="btn btn-success">Print</button></a>
                </div>
              </div>
            </div>
             <?php endif; ?>
          </div>
        </div>
            </div>
            <?= $this->Form->end(); ?>
        </div>

    </div>
</section>
<?php require('ledger_add_modal.php') ?>
<?php require('general_journal_transaction_modal.php') ?>
<script type="text/javascript">
  $(document).ready(function () {
 $("#account_head").select2({
    placeholder: "Search here...",
    width: '100%',
    ajax: {
      url: '<?= $this->webroot ?>Accounts/Searchcustomer',
      dataType: 'json',
      delay: 250,
      data: function (params) {
            return {
                      q: params.term, // search term
                      page: params.page
                   };
          },
          processResults: function (data, params) {
            params.page = params.page || 2;
            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: false
        },
      minimumInputLength: 2,
      templateResult: formatRepo,
      templateSelection: formatRepoSelection
    });

  function formatRepo (repo) 
  {
    if (repo.loading) return repo.text;
    return repo.text;
  }
  function formatRepoSelection (repo)
  {
    return repo.text || repo.text;
  }
  });
</script>
<script type="text/javascript">
$('#mode_category_ids').change(function() {
    //alert('k');
    var mode_category_id = $(this).val();
    //alert(mode_category_id);
    // var data={sub_group_id,sub_group_id};
    //var url_address= "<?= $this->webroot; ?>Accounts/GetSystemParameter/"+mode_category_id;
    // $.post( url_address,data, function( response ) {
    //console.log(response) ;

    if (mode_category_id == 2) {

        $('.bank').hide();
        $('.cheque').show();
        $('.cash').hide();
        // $('#main_group_id_edit').val(response.option);
        // $('#main_group_id_edit').attr('readonly',true);
        //$('#main_group_id_edit').val('');
    } else if (mode_category_id == 3) {

        $('.bank').show();
        $('.cheque').hide();
        $('.cash').hide();
        //$('#main_group_id_edit').val(response.option);
        //$('#main_group_id_edit').attr('readonly',true);
        //$('#main_group_id_edit').val('');

    } else {
        $('.bank').hide();
        $('.cheque').hide();
        $('.cash').show();

    }

    // },'json');
    //}
});
</script>
<script type="text/javascript">
var flash = '<?= $flash=$this->Session->flash(); ?>';
if (flash) {
    var current_url = window.location.href;
    var url = current_url.split("/");
    var length = url.length;
    var id = url[length - 3];
    var voucher_no = url[length - 2];
    var amount = url[length - 1];
    if (id) {
        //alert(id);
        var link = "<?= $this->webroot.'Print/receipt_voucher_fpdf/'?>" + id + '/' + voucher_no + '/' + amount;
        window.open(link, '_blank');
    }
}
//  $( document ).ready(function() {
//     $('#account_head').select2("open");
// });
</script>