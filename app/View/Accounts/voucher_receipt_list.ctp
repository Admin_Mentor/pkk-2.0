<style>
.acc_add_btn{padding-left: 0;width:3%;}
</style>
<section class="content-header">
    <h1> Voucher Receipt List 
		<div class="pull-right">
            <a href="<?php echo $this->webroot ?>Accounts/VoucherReceipt"><button class='btn btn-success pull-right'>New Receipt Voucher</button></a>&nbsp;
        </div>
	</h1>
</section>
<!-- <section class="content-header">
	<h1> Voucher Receipt List </h1>
</section> -->
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control invoice-search date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control invoice-search date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
				</div>
			</div>
			<!-- <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?= $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','empty'=>'All','class'=>'form-control select_two_class','style'=>'width: 100%')); ?>
				</div>
			</div> -->
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12"><br>
				<button class='btn btn-success' id='get_button'>Get</button>
			</div>
		</div>
		<div class="box-body">
			<table class="boder table table-condensed table table-bordered" id="myTable" data-order='[[ 0, "desc" ]]' data-page-length='10'>
				<thead>
					<tr class="blue-bg">
						<th>Date</th>
						<th>Document No</th>
						<th>Remarks</th>
						<th>Particular</th>
						<th style="text-align: right">Total Amt</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<!-- <tfoot>
					<tr>
						<th colspan="4" style="font-size:20px; color:red;text-align:right">Total:</th>
						<th style="font-size:20px; color:red;"></th>
						<th style="font-size:20px; color:red;"></th>
						<th></th>
						<th></th>

					</tr>
				</tfoot> -->
			</table>
		</div>
</div>
</section>
<div id="payment_edit_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit</h4>
			</div>
			<div class="modal-body">
				<?php echo $this->Form->create('PaymentVoucher', ['class'=>'form-horizontal','id'=>'PaymentVoucher_Form']); ?>
				<?= $this->Form->input('journal_id',array('class'=>'form-control','type'=>'hidden','id'=>'journal_id','label'=>false)); ?>
				<div class="form-group">
					<label class="col-sm-4 control-label">Account</label>
					<div class="col-sm-6">
						<?php echo $this->Form->input('account',['type'=>'hidden','id'=>'account','class'=>'form-control','label'=>false]); ?>
					<?php echo $this->Form->input('account_name',['type'=>'text','id'=>'account_name','class'=>'form-control','label'=>false,'readonly']); ?>

					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Reference</label>
					<div class="col-sm-6">
						<?= $this->Form->input('reference',array('class'=>'form-control','type'=>'text','id'=>'reference','label'=>false)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Amount</label>
					<div class="col-sm-6">
						<?= $this->Form->input('amount',array('class'=>'form-control number amounts','type'=>'text','required','id'=>'amount','label'=>false)); ?>
					</div>
				</div>
				<?= $this->Form->end(); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn_radious" id='edit_payment_button'>Edit</button>
				<button type="button" class="btn btn-primary btn_radious" id='delete_payment_button'>Delete</button>
			</div>
		</div>
	</div>
</div>
<?php require('ledger_add_modal.php') ?>
<?php require('general_journal_transaction_modal.php') ?>

<script type="text/javascript">
	$('#myTable').DataTable( {
		"processing": true,
		"serverSide": true,
        "order": [[ 0, 'asc' ]],
		"ajax": {
			"url": "<?= $this->webroot ?>Accounts/receipt_voucher_ajax",
			"type": "POST",
			data:function( d ) {
				d.from_date= $('#from_date').val();
				d.to_date= $('#to_date').val();
				// d.executive_id= $('#executive_id').val();
			},
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "Journal.date" },
		{ "data" : "Journal.id" },
		{ "data" : "Journal.remarks" },
		{ "data" : "Journal.particular" },
		{ "data" : "Journal.amount",className:"text-right" },
		{ "data" : "Journal.action" },
		],
	});
	$('#get_button').click(function(){
		table = $('#myTable').dataTable();
		table.fnDraw();
	});
	$('#account_head').change(function(){
		//alert("k");
		table = $('#myTable').dataTable();
		table.fnDraw();
	});
	$(document).on('click','.payment_edit',function(){
		var payment_id=$(this).closest('tr').find('td span.table_id').text();
		var url_address= "<?= $this->webroot; ?>Accounts/journal_get_ajax/"+payment_id;
		$.get( url_address, function( response ) {
			console.log(response);
			if(response.result!='Success')
			{
				alert(response.result);
				return false;
			}
			$('#PaymentVoucher_Form')[0].reset();
			$("#account").val(response.data.credit).trigger('change.select2');
			$("#account_name").val(response.AccountHeadCredit);
			$("#reference").val(response.data.remarks);
			$("#amount").val(parseFloat(response.data.amount).toFixed(2));
			$("#journal_id").val(payment_id);
			$('#payment_edit_modal').modal('show');
		}, "json");
	});
	$(document).on('click','#edit_payment_button',function(){
		var data=$('#PaymentVoucher_Form').serialize();
		var url_address= "<?= $this->webroot; ?>Accounts/journal_edit_ajax";
		var formData = new FormData($('#PaymentVoucher_Form')[0]); /////formdata object

		$.ajax({
			url: url_address,
			type: "POST",
			data: formData,
			processData: false,
			contentType: false,
			success: function (respnse) {
			var response =JSON.parse(respnse);
					if(response.result!='Success')
					{
						alert(response.result);
						return false;
					}
					else
					{
						
						$('#payment_edit_modal').modal('toggle');
						$('#PaymentVoucher_Form')[0].reset();
						var table = $('#myTable').dataTable();
						table.fnDraw();
						}
			},
			error: function (msg) {
				console.log(msg);
			}
		});
	});
	$(document).on('click','#delete_payment_button',function(){
		var data=$('#PaymentVoucher_Form').serialize();
		var url_address= "<?= $this->webroot; ?>Accounts/journal_delete_ajax";
		var formData = new FormData($('#PaymentVoucher_Form')[0]); /////formdata object
		if(confirm("Are you sure want to delete?")){
			$.ajax({
				url: url_address,
				type: "POST",
				data: formData,
				processData: false,
				contentType: false,
				success: function (respnse) {
				var response =JSON.parse(respnse);
						if(response.result!='Success')
						{
							alert(response.result);
							return false;
						}
						else
						{
							
							$('#payment_edit_modal').modal('toggle');
							$('#PaymentVoucher_Form')[0].reset();
							var table = $('#myTable').dataTable();
							table.fnDraw();
							}
				},
				error: function (msg) {
					console.log(msg);
				}
			});
		}else{
            return false;
        }
	});
</script>
<script type="text/javascript">
	var flash='<?= $flash=$this->Session->flash(); ?>';
	if(flash){
		var current_url=window.location.href;
		var url=current_url.split("/");
		var length=url.length;
		var id=url[length-3];
		var voucher_no=url[length-2];
		var amount=url[length-1];
		if(id){
			//alert(id);
			var link = "<?= $this->webroot.'Print/receipt_voucher_fpdf/'?>"+id+'/'+voucher_no+'/'+amount;
			window.open(link, '_blank');
		}
	}
// 	$( document ).ready(function() {
//     $('#account_head').select2("open");
// });
</script>