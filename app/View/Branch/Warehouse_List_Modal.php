 <div id="Warehouse_List_Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Warehouse Mappings</h4>
      </div>
     
      <div class="modal-body">
      <div class="box-body table-responsive no-padding">
      <table id="WarehouseTable" class="table  table-hover boder">
        <thead>
          <tr class="blue-bg">
          <th>#</th>
          <th>Warehouse</th>
           <th>Action</th>
          </tr>
        </thead>
        <tbody>
          
        </tbody>
      </table>
      </div>
      </div>
      <div class="modal-footer">
       <input type="hidden" value="" id=""/>
      </div>
    </div>
  </div>
</div>