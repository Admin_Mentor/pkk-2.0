<section class="content-header">
  <h1>Branch</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
    </div>
    <div class="box-body">
      <?= $this->Form->create('Branch', array('url' => array('controller' => 'Branch', 'action' => 'AddBranch'),'id'=>'Branch_Form'));?>
      <div class="row">
        <div id="staffDiv" class="col-md-4 col-sm-4">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('name',array('type'=>'text','id'=>'name','class'=>'form-control','required'=>'required'));
          ?>
          </div>
        </div>
        
        <div class="col-md-4 col-sm-4">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('mobile',array('type'=>'text','id'=>'mobile','label'=>'Contact No','maxlength'=>'13','class'=>'form-control number','required'=>'required')); ?>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-3" hidden="">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('username',array('type'=>'text','id'=>'username','class'=>'form-control')); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3" hidden="">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('password',array('type'=>'text','id'=>'password','class'=>'form-control')); ?>
          </div>
        </div>
        <div class="col-md-4 col-sm-4">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('warehouse_id',array('type'=>'select','empty'=>'select','id'=>'warehouse','options'=>$Warehouse,'class'=>'form-control select2','required'=>'required','label'=>'Main Warehouse')); ?>
          </div>
        </div>
      </div>
           <div class="row">
        <div class="col-md-4 col-sm-4">
          <div class="form-group  col-md-12">
            <input type='hidden' id='Branch_id' value="" name="id">
            <?php echo $this->Form->input('location_id',array('multiple' => true,'type'=>'select','id'=>'location_id','options'=>$Warehouse,'class'=>'form-control select2','required'=>'required','label'=>'Warehouse')); ?>
          </div>
          </div>
        <div class="col-md-2 col-sm-2" style="margin-top: 2%;" >
           <div class="form-group  col-md-12">
          <button class="save" id='save_button' >Save</button>
          <button style="display:none" id='edit_button' value='' class="save pull-right">Edit</button>
        </div>
        </div>
        <?= $this->Form->end(); ?>
      </div>
      <div class="row">
        <hr>
        <div class="col-md-12">
          <div class="box-body table">
            <table class="table datatable table-hover boder table-bordered">
              <thead>
                <tr class="blue-bg">
                  <th>Name</th>
                  <th>Contact No</th>
                  <th>Warehouse</th>
                  <th>Main Warehouse</th>
                  <th width="10%">Action</th>
<!--                   <th>Delete</th>
 -->                </tr>
              </thead>
             <?php foreach ($BranchAll as $key => $value): ?>
              
                <tr>
                  <td class='name'><?= $value['Branch']['name'];  ?></td>
                  <td ><?= $value['Branch']['mobile'];  ?></td>
                   <td><a data-id="<?= $value['Branch']['id'];  ?>" title="Show Warehouse" class="warehouse-show" href="#"><i class="fa fa-map-marker" aria-hidden="true"></i></a></td>
                  <td><?= $value['Warehouse']['name']; ?></td>
                  <td><i data-id="<?= $value['Branch']['id'];  ?>" class="fa fa-2x fa-edit edit_branch" aria-hidden="true" style="color: #3c8dbc;"></i></td>
                <?php endforeach ?>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <br>
    </div>
  </div>
</div>
</section>
<?php require 'Warehouse_List_Modal.php'; ?>

<script type="text/javascript">
$(document).on('change','#location_id',function(){
       var location_id=$(this).val();
       if(location_id){
       var branch_id=$('#Branch_id').val();
       if(!branch_id)
       {
        $.post( "<?= $this->webroot ?>Branch/get_branch_warehouse_ajax/"+location_id,function( data ) {
      if(data.status=="This Warehouse Already Used")
      {
        alert(data.status);
        $.each(data.BranchWarehouse, function( key, value ) {
        $("#location_id").find("option[value="+value+"]").prop("selected", "").trigger('change');
         });
      }
  }, "json");
       }
       else
       {
         $.post( "<?= $this->webroot ?>Branch/get_branch_Warehouse_ajax_edit/"+location_id+'/'+branch_id,function( data ) {
      if(data.status=="This Warehouse Already Used")
      {
        alert(data.status);
         $.each(data.BranchWarehouse, function( key, value ) {
        $("#location_id").find("option[value="+value+"]").prop("selected", "").trigger('change');
        });
      }
  }, "json");
       }
     }
});
$(document).on('click','.edit_branch',function(){
    var Branch_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Branch/get_branch_details_ajax/"+Branch_id,function( data ) {
       $("#location_id").val('').trigger('change');
       $('#name').val(data.Branch.name);
      $('#mobile').val(data.Branch.mobile);
      //$('#password').val(data.Branch.password);
      //$('#username').val(data.Branch.username);
      $('#warehouse').val(data.Branch.warehouse_id).trigger('change');
      // console.log(data.BranchRoute);
      $('#save_button').css('display','none');
      $('#edit_button').css('display','');
      $("#Branch_id").val(data.Branch.id);
        $("#mobile").append("<input type='text'  name='data[Branch][id]' value='"+Branch_id+"'>");
       $.each(data.BranchRoute, function( key, value ) {
        $("#location_id").find("option[value="+value+"]").prop("selected", "selected").trigger('change');
              });
      $('#Branch_Form').attr('action','<?= $this->webroot; ?>Branch/EditBranch');

  }, "json");
});
 $(document).on('click','.warehouse-show',function(){
    var Branch_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Branch/get_branch_warehouse/"+Branch_id,function( data ) {
        $('#Warehouse_List_Modal').modal('show');
        $('#Warehouse_List_Modal tbody').html(data.row);
        $('#branch_id').val(data.branch_id);
     
    }, "json");
  });
  $(document).on('click','.del-warehouse',function(){
    if(confirm('Are You Sure want to remove this warehouse'))
    {
      var id=$(this).data('id');
    var parentRow =$(this).parents('tr');
    $.get( "<?= $this->webroot ?>Branch/DeleteBranchwarehouse/"+id,function( data ) {
       parentRow.hide();
     
    }, "json");
    }
    
  });

</script>