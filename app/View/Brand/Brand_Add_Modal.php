<div id="Brand_Add_Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add  Brand</h4>
      </div>
      <?php echo $this->Form->create('Brand',array('id'=>'Brand_Add_Form')); ?>
      <div class="modal-body">
        <div class="form-horizontal">
         <!--  <div class="form-group">
            <div class="col-sm-10">
              <?php echo $this->Form->input('product_type_id',array('type'=>'select','options'=>$ProductType,'empty' =>'ALL','class'=>'form-control select_two_class search_class product_flitering','style'=>'width:100%','label'=>'Product Type','id'=>'product_type_id',)) ?>
            </div>
          </div> -->
          <div class="form-group">
            <div class="col-sm-10">
              <?php echo $this->Form->input('name',array('class'=>'form-control toUpperCase','label'=>'Brand Name')); ?>
              <?php echo $this->Form->input('product_type_id',array('type'=>'hidden')); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-10">
              <!-- <?php echo $this->Form->input('code',array('type'=>'text','class'=>'form-control','label'=>'Code')); ?> -->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id='add_Brand'>Add</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>

