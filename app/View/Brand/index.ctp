<style type="text/css">
  .type_one {
    padding-left: 31%;
    padding-top: 5%;
  }
  .type-modal {
    color: #337ab7;
  }
  .type_tab{
    margin-top: 3%;
  }
  @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
    .type_one {
      padding-left: 0% !important;
    }
  }
  @-webkit-keyframes flash {
    from, 50%, to {
      opacity: 1;
    }
    25%, 75% {
      opacity: 0;
    }
  }
  @keyframes flash {
    from, 50%, to {
      opacity: 1;
    }
    25%, 75% {
      opacity: 0;
    }
  }
  .flash {
    -webkit-animation-name: flash;
    animation-name: flash;
    /*animation-delay: 0.1s;*/
    animation-duration: 2s;
    background-color:red;
  }
  .tp_6px{
    margin-top: 1.5%;
  }
</style>
<section class="content-header">
  <h1> Brand 
<div class="col-sm-1 pull-right" style="margin-right: 20px">
              <a href="#"><button class='btn btn-success' data-toggle="modal"  data-target="#Brand_Add_Modal">Add Brand</button></a>
            </div>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="row">
          <div class="col-md-12">
           <!--  <div style="margin-left:5%;">
            <br>
              <a href="#"><button class='btn btn-success' data-toggle="modal"  data-target="#Brand_Add_Modal">Add Brand</button></a>
            </div> -->
          </div>
        </div>
        <div class="box-body">
          <table class="table table-condensed tab_view_top_mar datatable type_tab table-bordered" id='brand_table'>
            <thead>
              <tr class="blue-bg">
<!--                 <th>Product Type</th>
 -->                <th>Brand</th>
                <th style="display:none"></th>
                <th width="10%">Action</th>
<!--                 <th>Delete</th>
 -->              </tr>
            </thead>
            <tbody>
              <?php  foreach( $brands as $brand_list ){?>
                <tr class="blue-pd">
<!--                   <td class='product_type_name'><?= $brand_list['ProductType']['name']; ?></td>
 -->                  <td style="display:none" class='Brand_id'><?= $brand_list['Brand']['id'];?></td>
                  <td class='brand_name'><?= $brand_list['Brand']['name']; ?></td>
                  <td><a href="#"><i class="fa fa-2x fa-edit edit_new_btn ad-mar td_leted edit_dt"> </i></a></td>
<!--                   <td><a href="#"><i class="fa fa-2x fa-trash ad-mar brand_delete"></i></a></td>
 -->                </tr>
                <?php  }?>
              </tbody>
            </table>
          </div>
          <div id="brand_edit_modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Brand</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-3"><label for="inputEmail3" class="form-group">Brand</label></div>
                    <div class="col-md-9">
                      <input type="text" style="display:none" id="brand_id">
                      <input type="text" class="form-control form-group" placeholder="" id="brand_name">
                      <span id="prdct_type_error_edit" style="color:#db1802" class="help-inline"></span>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                  <button type="button" class="btn btn-success" data-dismiss="modal" id="brand_edit">Update</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php require 'Brand_Add_Modal.php'; ?>
    <script>
    $('#add_Brand').click(function(){
      var product_type_id=$('#product_type_id option:selected').val();
      if(!product_type_id)
      {
        //$('#product_type_id').select2('open');
        //return false;
      }
      $('#BrandProductTypeId').val(product_type_id);
      var data=$('#Brand_Add_Form').serialize();
      var url_address= "<?= $this->webroot; ?>Brand/brand_add_ajax";
      $.post( url_address,data, function( response ) {
        if(response.result!='Success')
        {
          // alert(response.result);
          // if(response.result=='Already Added')
          // {
          //   $('#brand_id').val(response.key).trigger('change');   
          //   $('#Brand_Add_Form')[0].reset();
          //   $('#Brand_Add_Modal').modal('toggle');
          // }
          // else
          // {
          //   return false;
          // }
        }
        else
        {
          $('#brand_id').append($("<option></option>").attr("value",response.key).text(response.value));
          $('#Brand_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
          $('#brand_id').val($('#brand_id option:last-child').val()).trigger('change');

          $('.brand_id').append($("<option></option>").attr("value",response.key).text(response.value));
          $('.brand_id').val($('#brand_id option:last-child').val()).trigger('change');

          $('#Brand_Add_Form')[0].reset();
          $('#Brand_Add_Modal').modal('toggle');
          location.reload();
        }
      }, "json");
    });
      $(document).on('click', '.edit_dt', function () {
        var brand_name=$(this).closest('tr').find('td.brand_name').text();
        var brand_id=$(this).closest('tr').find('td.Brand_id').text();
        $('.new_brand_name').each(function(){
          $(this).removeAttr('class');
        });
        $('.new_discount_percentage').each(function(){
          $(this).removeAttr('class');
        });
        $(this).closest('tr').find('td.brand_name').addClass('new_brand_name');
        $('#brand_id').val(brand_id);
        $('#brand_name').val(brand_name);
        $('#brand_edit_modal').modal('show');
      });
      $('#brand_edit').click(function() {
        var brand_id=$('#brand_id').val();
        if(!brand_id)
          alert('Empty Brand Id');
        var brand_name=$('#brand_name').val();
        if($.trim(brand_name)=='')
          alert('Empty Brand Name');
        var discount_percentage=$('#discount_percentage').val();
        var url_address= '<?php echo $this->webroot; ?>'+'Brand/brand_edit_ajax';
        var data=
        {
          brand_id:brand_id,
          brand_name:brand_name,
        };
        $.ajax({
          type: "post",  
          url:url_address,
          data: data, 
          dataType:'json',
          success: function(response) {
            if(response.result=='Error')
            {
              alert(response.message);
              return false;
            }
            $('.new_brand_name').html(brand_name);
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      });
      $('#brand_name').keyup(function(){
        $('#brand_name').val($(this).val().toUpperCase());
        var brand=$(this).val().trim();
        if(!brand)
        {
          $('#brand_edit').attr('disabled',true);
        }
        else
        {
          var data={
            brand:brand
          };
          var url_address= '<?php echo $this->webroot; ?>'+'Brand/check_brand_ajax';
          $.ajax({
            type: "post",  
            url:url_address,
            data: data,  
            success: function(response) {
              if(response=="Yes")
              {
                $('#prdct_type_error_edit').html('This Brand is already taken');
                $('#brand_edit').attr('disabled',true);
              }
              else
              {
                $('#prdct_type_error_edit').html('');
                $('#brand_edit').attr('disabled',false);
              }
            },
            error:function (XMLHttpRequest, textStatus, errorThrown) {
              alert(textStatus);
            }
          });
        }
      });
      $('#brand').change(function(){
        var brand_type=$(this).val();
        var data={
          brand_type:brand_type
        };
        var url_address= '<?php echo $this->webroot; ?>'+'Brand/brand_type_search';
        $.ajax({
          type: "post",  
          url:url_address,
          data: data,  
          dataType:'json',
          success: function(response) {
            $('#brand_table').DataTable().destroy();
            $('#brand_table tbody').html(response.row);
            $('#brand_table').DataTable();
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      });
      $('#BrandBrandName').keyup(function(){
        $('#BrandBrandName').val($(this).val().toUpperCase());
        var brand=$(this).val().trim();
        if(!brand)
        {
          $('#add_prdct_type').attr('disabled',true);
        }
        else
        {
          var data={
            brand:brand
          };
          var url_address= '<?php echo $this->webroot; ?>'+'Brand/check_brand_ajax';
          $.ajax({
            type: "post",  
            url:url_address,
            data: data,  
            success: function(response) {
              if(response=="Yes")
              {
                $('#brand_error').html('This Brand is already taken');
                $('#add_prdct_type').attr('disabled',true);
              } else {
                $('#brand_error').html('');
                $('#add_prdct_type').attr('disabled',false);
              }
            },
            error:function (XMLHttpRequest, textStatus, errorThrown) {
              alert(textStatus);
            }
          });
        }
      });
      $(document).on('click','.brand_delete',function(){
        if(!confirm("Are you sure?"))
        {
          return false;
        }
        var id=$(this).closest('tr').find('td.Brand_id').text();
        var rowindex = $(this).closest('tr').index();
        var url_address= '<?php echo $this->webroot; ?>'+'Brand/brand_delete_ajax/'+$.trim(id);
        $.ajax({
          type: "post",  
          url:url_address,
          dataType:'json',
          success: function(response) {
            if(response.result=="Success")
            {
              $('#brand_table tbody tr:eq(' + rowindex + ')').remove();
              $("#brand option[value='"+id+"']").remove();
              $("#brand").select2('val','');
              alert(response.result);
            }
            else
            {
              $('#brand_table tbody tr:eq(' + rowindex + ')').attr('class','blue-pd flash');
              $('#brand_table tbody tr:eq(' + rowindex + ')').attr('style','font-size:20px;color:red');
              alert(response.result);
              setTimeout(timer_for_table, 2000);
            }
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      });
      function timer_for_table() {
        $('#brand_table tbody tr').each(function(){
          $(this).closest('tr').attr('class','blue-pd');
          $(this).closest('tr').attr('style','font-size:14px;color:');
        });
      }
    </script>