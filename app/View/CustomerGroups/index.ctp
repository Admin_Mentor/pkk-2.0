<section class="content-header">
  <h1> Customer Group
   <div class="col-sm-1 pull-right" style="margin-right: 20px">
            <button data-toggle="modal"  data-target="#addcustomergroup" class="btn btn-success">Create</button>
            </div>
   </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <!-- <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"><?php echo $this->Form->input('CustomerGroup',array('type'=>'select','empty' =>'ALL','options'=>$CustomerGroup,'class'=>'form-control select2','required'=>'required')) ?></div>
          </div>
        </div> -->
        <!-- <div class="text-right" style="margin-top: 1.5%;margin-right: 2%"> -->
                    <!-- <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#addcustomergroup"></i></a> -->
                   
                    <!-- <button data-toggle="modal"  data-target="#addcustomergroup" class="btn btn-success">Create</button> -->
                  <!-- </div> -->
        <div class="box-body">
          <table class="table table-condensed tab_view_top_mar datatable type_tab table-bordered" id='customer_group_table'>
            <thead>
              <tr class="blue-bg">
                <th>Customer Group</th>
                <th style="display:none"></th>
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php  foreach( $CustomerGroups as $value ){?>
                <tr class="blue-pd">
                  <td style="display:none" class='customer_group_id'><?= $value['CustomerGroup']['id'];?></td>
                  <td class='customer_group_name'><?= $value['CustomerGroup']['name']; ?></td>
                  <td><a href="#"><i class="fa fa-2x fa-edit edit_new_btn ad-mar td_leted edit_dt"> </i></a></td>
                  
                </tr>
                <?php  }?>
              </tbody>
            </table>
          </div>
          <div id="customer_group_edit_modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Customer Group</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-3"><label for="inputEmail3" class="form-group">Customer Group</label></div>
                    <div class="col-md-9">
                      <input type="hidden" id="customer_group_id">
                      <input type="text" class="form-control form-group" placeholder="" id="customer_group_name">
                      <span id="prdct_type_error_edit" style="color:#db1802" class="help-inline"></span>
                    </div>
                  </div>
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                  <button type="button" class="btn btn-success" data-dismiss="modal" id="customer_group_edit">Update</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div id="addcustomergroup" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Add Customer Group</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label"> Name</label>
            <div class="col-sm-9">
              <input class="form-control location_disable toUpperCase " placeholder="" type="text" id="modal_customer_group_name">
              <span id="customer_group_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>
          
        </div>
      
      <div class="modal-footer">
        <button  type='button' class="save btn btn-success" id="add_customer_group">Save</button>
      </div>
      </div>
    </div>
  </div>
</div>
    <script>
      $('#modal_customer_group_name').keyup(function(){

  var modal_customer_group_name=$(this).val();
   $('#add_division').attr('disabled',true);
  var data={
   customer_group_name:modal_customer_group_name
 };
  var url_address= '<?php echo $this->webroot; ?>'+'CustomerGroups/customer_group_search';
 $.ajax({
  type: "post",
  url:url_address,
  data: data,
  success: function(response) {
    if(response=="Yes")
    {
      $('#customer_group_error').html('This Group is already taken');
      $('#add_customer_group').attr('disabled',true);
    }
    else
    {
      $('#customer_group_error').html('');
       $('#add_customer_group').attr('disabled',false);
    }
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
});
       $('#add_customer_group').click(function(){
  var modal_customer_group_name=$('#modal_customer_group_name').val();
  
  var data={
    name:modal_customer_group_name,
  };
  var url_address= '<?php echo $this->webroot; ?>CustomerGroups/add_customer_group_ajax';
 
  $.ajax({
    method: "POST",
    url:url_address,
    data: data,
  }).done(function( result ) {
    $.fn.show_alert('Success');
    location.reload(); 

  });
});
      $(document).on('click', '.edit_dt', function () {
        $('#customer_group_table').find('td').removeClass('new_customer_group_name');
        var customer_group_name=$(this).closest('tr').find('td.customer_group_name').text();
        var customer_group_id=$(this).closest('tr').find('td.customer_group_id').text();
        $(this).closest('tr').find('td.customer_group_name').addClass('new_customer_group_name');
        $('#customer_group_id').val(customer_group_id);
        $('#customer_group_name').val(customer_group_name);
        
        $('#customer_group_edit_modal').modal('show');
      });

      $('#customer_group_edit').click(function() {
        var customer_group_id=$('#customer_group_id').val();
        if(!customer_group_id)
          alert('Empty Division Id');
        var customer_group_name=$('#customer_group_name').val().toUpperCase();
        if($.trim(customer_group_name)=='')
          alert('Empty Division Name');
        
       
        var url_address= '<?php echo $this->webroot; ?>'+'CustomerGroups/edit_customer_group_ajax';
        var data=
        {
          id:customer_group_id,
          name:customer_group_name,
        };
        $.ajax({
          type: "post",  
          url:url_address,
          data: data, 
          dataType:'json',
          success: function(response) {
            if(response.result!='Success')
            {
              alert(response.result);
              return false;
            }
            //alert(response.result);
            $.fn.show_alert('Success');
            $('.new_customer_group_name').html(customer_group_name);
             $('#customer_group_id').val('');
        $('#customer_group_name').val('');
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      });

      $(document).on('click','.customer_type_delete',function(){
        if(!confirm("Are you sure?"))
        {
          return false;
        }
        var id=$(this).closest('tr').find('td.customer_type_id').text();
        var rowindex = $(this).closest('tr').index();
        var url_address= '<?php echo $this->webroot; ?>'+'CustomerType/delete_by_ajax/'+$.trim(id);
        $.ajax({
          type: "post",  
          url:url_address,
          dataType:'json',
          success: function(response) {
            if(response.result=="Success")
            {
              $('#customer_type_table tbody tr:eq(' + rowindex + ')').remove();
              $("#customer_type option[value='"+id+"']").remove();
              $("#customer_type").select2('val','');
            }
            else
            {
              $('#customer_type_table tbody tr:eq(' + rowindex + ')').attr('class','blue-pd flash');
              $('#customer_type_table tbody tr:eq(' + rowindex + ')').attr('style','font-size:20px;color:red');
              alert(response.result);
              setTimeout(timer_for_table, 2000);
            }
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      });
      function timer_for_table() {
        $('#customer_group_table tbody tr').each(function(){
          $(this).closest('tr').attr('class','blue-pd');
          $(this).closest('tr').attr('style','font-size:14px;color:');
        });
      }
     $('.customer_group_disable').keyup(function(){
  var modal_customer_group_name=$('#modal_customer_group_name').val().trim();
  if(modal_customer_group_name!="")
  {
    $('#add_customer_group').attr('disabled',false);
  }
  else{
    $('#add_customer_group').attr('disabled',true);
  }
});
    $.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
    </script>