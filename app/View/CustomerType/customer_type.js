$('#add_customer_type').click(function(){

	var name=$('#customer_type_name_modal').val();
	if(name=='')
	{
		$('#customer_type_name_modal').focus();
		return false;
	}

	var website_url='<?php echo $this->webroot; ?>CustomerType/add_customer_type_ajax';
	var data={
		name:name,
	};
	$.ajax({
		method: "POST",
		url: website_url,
		data: data,
		dataType:'json',
	}).done(function( data ) {
		if(data.result!='Success')
		{
			alert(data.result);
			return false;
		}
		//location.reload();
		$('#customer_type_modal').modal('toggle');
		$('#customer_type_name_modal').val('');
		$('#CustomerType').append($("<option></option>").attr("value",data.key).text(data.value));
		$('#modal_customer_type_id').append($("<option></option>").attr("value",data.key).text(data.value));
		$('#CustomerType').val(data.key).change();
		$('#modal_customer_type_id').val(data.key).change();
		table_data.fnDraw();
	});
});
// $(document).on('change','#customer_type_id,#modal_customer_type_id',function(){
// 	$('#modal_customer_type_id').val($(this).val()).trigger('change.select2');
// 	$('#customer_type_id').val($(this).val()).trigger('change.select2');
// 	var customer_type_id=$('#customer_type_id').val();
// 	if(customer_type_id==0 || customer_type_id==1)
// 		$('.general_customer_hidden_area').hide();
// 	else
// 		$('.general_customer_hidden_area').show();
// });

// $("#modal_customer_type_id option[value*='1']").prop('disabled',true);
