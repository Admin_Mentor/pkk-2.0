<div id="customer_type_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Customer Type</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-4 control-label">Customer Type</label>
					<div class="col-sm-8">
						<input class="form-control" id="customer_type_name_modal" type="text">
					</div>
					<br>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id='add_customer_type'>Save</button>
			</div>
		</div>
	</div>
</div>