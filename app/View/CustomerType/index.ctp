<section class="content-header">
  <h1> Customer Type 
    <div class="col-sm-1 pull-right" style="margin-right: 20px">
            <button data-toggle="modal"  data-target="#customer_type_modal" class="btn btn-success">Create</button>
            </div>
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="row">
          <div class="col-md-12">
            <!-- <div class="col-md-4"><?php echo $this->Form->input('CustomerType',array('type'=>'select','empty' =>'ALL','options'=>$CustomerType,'class'=>'form-control select2','required'=>'required')) ?></div> -->
          <!-- <div class="text-right" style="margin-top: 1.5%;margin-right: 2%"> -->
  <!-- <br> -->
  <!-- <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_type_modal"></i> -->
                      <!-- <button data-toggle="modal"  data-target="#customer_type_modal" class="btn btn-success">Create</button> -->
<!-- </div>  -->

          </div>
        </div>
        <div class="box-body">
          <table class="table table-hover boder table-bordered" id='customer_type_table'>
            <thead>
              <tr class="blue-bg">
                <th>#</th>
                <th>Customer Type</th>
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
        <div id="customer_type_edit_modal" class="modal fade" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit CustomerType</h4>
              </div>
              <div class="modal-body">
                <div class="row">
                  <div class="col-md-3"><label for="inputEmail3" class="form-group">Customer Type</label></div>
                  <div class="col-md-9">
                    <input type="hidden" id="customer_type_id">
                    <input type="text" class="form-control form-group" placeholder="" id="customer_type_name_edit">
                    <span id="prdct_type_error_edit" style="color:#db1802" class="help-inline"></span>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="customer_type_edit">Update</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php require('customer_type_modal.php') ?>

  <script type="text/javascript">
    <?php require('customer_type.js') ?>
    table_data=$('#customer_type_table').dataTable({
      "processing": true,
      "serverSide": true,
      "ajax": {
        "url": "<?= $this->webroot ?>CustomerType/CustomerType_Table_ajax",
        "type": "POST",
        "dataSrc": "records",
      },
      "columns": [
      { "data" : "CustomerType.id" },
      { "data" : "CustomerType.name" },
      { "data" : "CustomerType.action" },
      ],
      "columnDefs": [
      ],
    });
    $(document).on('click', '.edit_CustomerType', function () {
      var customer_type_name=$(this).closest('tr').find('td.customer_type_name').text();
      var customer_type_id=$(this).closest('tr').find('td.customer_type_id').text();
      $('.new_customer_type_name').each(function(){
        $(this).removeAttr('class');
      });
      $('.new_discount_percentage').each(function(){
        $(this).removeAttr('class');
      });
      $(this).closest('tr').find('td.customer_type_name').addClass('new_customer_type_name');
      $('#customer_type_id').val(customer_type_id);
      $('#customer_type_name').val(customer_type_name);
      $('#customer_type_edit_modal').modal('show');
    });

    $(document).on('click','.edit_CustomerType',function(){
      customer_type_id=$(this).attr('table_id');
      $('#customer_type_id').val(customer_type_id);
      customer_type_name=$(this).attr('customer_type_name');
      $('#customer_type_name_edit').val(customer_type_name);
      
    });
    $('#customer_type_edit').click(function() {
        var customer_type_id=$('#customer_type_id').val();
        if(!customer_type_id)
          alert('Empty customer_type Id');
        var customer_type_name=$('#customer_type_name_edit').val();
        if($.trim(customer_type_name)=='')
          alert('Empty customer_type Name');
        var url_address= '<?php echo $this->webroot; ?>'+'CustomerType/edit_ajax';
        var data=
        {
          id:customer_type_id,
          name:customer_type_name,
        };
        $.ajax({
          type: "post",  
          url:url_address,
          data: data, 
          dataType:'json',
          success: function(response) {
            if(response.result!='Success')
            {
              alert(response.result);
              return false;
            }
            table_data.fnDraw();
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      });
    $(document).on('click','.delete_CustomerType',function(){
      if(!confirm('Are You Sure')) { return false; }
      id=$(this).attr('table_id');
      var url_address= "<?= $this->webroot; ?>CustomerType/delete_by_ajax/"+id;
      $.get( url_address, function( response ) {
        if(response.result!='Success') { alert(response.result); return false; }
        table_data.fnDraw();
      }, "json");
    });
  </script>