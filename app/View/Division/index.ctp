<section class="content-header">
  <h1> Customer Category
    <div class="col-sm-1 pull-right" style="margin-right: 80px">
             <button data-toggle="modal"  data-target="#adddivision" class="btn btn-success">Create Customer Category</button>
            </div>
   </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="row">
          <div class="col-md-12">
            <!-- <div class="col-md-4"><?php echo $this->Form->input('Division',array('type'=>'select','empty' =>'ALL','options'=>$Division,'class'=>'form-control select2','required'=>'required')) ?></div> -->
          </div>
        </div>
        <!-- <div class="text-right" style="margin-top: 1.5%;margin-right: 2%"> -->
                    <!-- <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#adddivision"></i></a> -->
                    <!-- <button data-toggle="modal"  data-target="#adddivision" class="btn btn-success">Create Customer Category</button> -->
                   
                    
                  <!-- </div> -->
        <div class="box-body">
          <table class="table table-condensed tab_view_top_mar datatable type_tab table-bordered" id='division_table'>
            <thead>
              <tr class="blue-bg">
                <th>Category</th>
                <th style="display:none"></th>
              
                <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php  foreach( $Divisions as $value ){?>
                <tr class="blue-pd">
                  <td style="display:none" class='division_id'><?= $value['Division']['id'];?></td>
                  <td class='division_name'><?= $value['Division']['name']; ?></td>
                  <td><a href="#"><i class="fa fa-2x fa-edit edit_new_btn ad-mar td_leted edit_dt"> </i></a></td>
                  
                </tr>
                <?php  }?>
              </tbody>
            </table>
          </div>
          <div id="division_edit_modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Customer Category</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-3"><label for="inputEmail3" class="form-group">Name</label></div>
                    <div class="col-md-9">
                      <input type="hidden" id="division_id">
                      <input type="text" class="form-control form-group" placeholder="" id="division_name">
                      <span id="prdct_type_error_edit" style="color:#db1802" class="help-inline"></span>
                    </div>
                  </div>
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                  <button type="button" class="btn btn-success" data-dismiss="modal" id="division_edit">Update</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div id="adddivision" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Customer Category</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label"> Name</label>
            <div class="col-sm-9">
              <input class="form-control location_disable toUpperCase " placeholder="" type="text" id="modal_division_name">
              <span id="divison_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>
          
        </div>
      
      <div class="modal-footer">
        <button  type='button' class="save btn btn-success" id="add_division">Save</button>
      </div>
      </div>
    </div>
  </div>
</div>
    <script>
      $('#modal_division_name').keyup(function(){

  var modal_divison_name=$(this).val();
   $('#add_division').attr('disabled',true);
  var data={
   division_name:modal_divison_name
 };
  var url_address= '<?php echo $this->webroot; ?>'+'Division/division_search';
 $.ajax({
  type: "post",
  url:url_address,
  data: data,
  success: function(response) {
    if(response=="Yes")
    {
      $('#divison_error').html('This Division is already taken');
      $('#add_division').attr('disabled',true);
    }
    else
    {
      $('#divison_error').html('');
       $('#add_division').attr('disabled',false);
    }
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
});
       $('#add_division').click(function(){
  var modal_division_name=$('#modal_division_name').val();
  
  var data={
    name:modal_division_name,
  };
  var url_address= '<?php echo $this->webroot; ?>Division/add_division_ajax';
 
  $.ajax({
    method: "POST",
    url:url_address,
    data: data,
  }).done(function( result ) {
    //alert(data.result);
    $.fn.show_alert('Success');
    location.reload(); 

  });
});
      $(document).on('click', '.edit_dt', function () {
        $('#division_table').find('td').removeClass('new_division_name');
        var division_name=$(this).closest('tr').find('td.division_name').text();
        var division_id=$(this).closest('tr').find('td.division_id').text();
        $(this).closest('tr').find('td.division_name').addClass('new_division_name');
        $('#division_id').val(division_id);
        $('#division_name').val(division_name);
        
        $('#division_edit_modal').modal('show');
      });
      $.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
      $('#division_edit').click(function() {
        var division_id=$('#division_id').val();
        if(!division_id)
          alert('Empty Division Id');
        var division_name=$('#division_name').val().toUpperCase();
        if($.trim(division_name)=='')
          alert('Empty Division Name');
        
       
        var url_address= '<?php echo $this->webroot; ?>'+'Division/edit_division_ajax';
        var data=
        {
          id:division_id,
          name:division_name,
        };
        $.ajax({
          type: "post",  
          url:url_address,
          data: data, 
          dataType:'json',
          success: function(response) {
            if(response.result!='Success')
            {
              alert(response.result);
              return false;
            }
            //alert(response.result);
            $.fn.show_alert('Success');
            $('.new_division_name').html(division_name);
             $('#division_id').val('');
        $('#division_name').val('');
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      });

      $(document).on('click','.customer_type_delete',function(){
        if(!confirm("Are you sure?"))
        {
          return false;
        }
        var id=$(this).closest('tr').find('td.customer_type_id').text();
        var rowindex = $(this).closest('tr').index();
        var url_address= '<?php echo $this->webroot; ?>'+'CustomerType/delete_by_ajax/'+$.trim(id);
        $.ajax({
          type: "post",  
          url:url_address,
          dataType:'json',
          success: function(response) {
            if(response.result=="Success")
            {
              $('#customer_type_table tbody tr:eq(' + rowindex + ')').remove();
              $("#customer_type option[value='"+id+"']").remove();
              $("#customer_type").select2('val','');
            }
            else
            {
              $('#customer_type_table tbody tr:eq(' + rowindex + ')').attr('class','blue-pd flash');
              $('#customer_type_table tbody tr:eq(' + rowindex + ')').attr('style','font-size:20px;color:red');
              alert(response.result);
              setTimeout(timer_for_table, 2000);
            }
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      });
      function timer_for_table() {
        $('#customer_type_table tbody tr').each(function(){
          $(this).closest('tr').attr('class','blue-pd');
          $(this).closest('tr').attr('style','font-size:14px;color:');
        });
      }
     $('.location_disable').keyup(function(){
  var modal_location_name=$('#modal_location_name').val().trim();
  if(modal_location_name!="")
  {
    $('#add_location').attr('disabled',false);
  }
  else{
    $('#add_location').attr('disabled',true);
  }
});
     $('#modal_location_name').keyup(function(){
  var modal_location_name=$(this).val();
  var data={
   location_name:modal_location_name
 };
  var url_address= '<?php echo $this->webroot; ?>'+'Executive/route_search';
 $.ajax({
  type: "post",
  url:url_address,
  data: data,
  success: function(response) {
    if(response=="Yes")
    {
      $('#location_error').html('This Location is already taken');
      $('#add_location').attr('disabled',true);
    }
    else
    {
      $('#location_error').html('');
       $('#add_location').attr('disabled',false);
    }
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
});
     $('#modal_location_code').keyup(function(){
  var modal_location_code=$(this).val();
  var data={
   code:modal_location_code
 };
  var url_address= '<?php echo $this->webroot; ?>'+'Route/route_search';
 $.ajax({
  type: "post",
  url:url_address,
  data: data,
  success: function(response) {
    if(response=="Yes")
    {
      $('#location_code_error').html('This Code is already taken');
      $('#add_location').attr('disabled',true);
    }
    else
    {
      $('#location_code_error').html('');
       $('#add_location').attr('disabled',false);
    }
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
});
    </script>