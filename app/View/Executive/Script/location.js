$('#modal_location_name').keyup(function(){
  var modal_location_name=$(this).val();
  var data={
   location_name:modal_location_name
 };
 // var url_address= '<?php echo $this->webroot; ?>'+'Location/location_search';
  var url_address= '<?php echo $this->webroot; ?>'+'Executive/route_search';
 $.ajax({
  type: "post",
  url:url_address,
  data: data,
  success: function(response) {
    if(response=="Yes")
    {
      $('#location_error').html('This Location is already taken');
      $('#add_location').attr('disabled',true);
    }
    else
    {
      $('#location_error').html('');
       $('#add_location').attr('disabled',false);
    }
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
});
$('.location_disable').keyup(function(){
  var modal_location_name=$('#modal_location_name').val().trim();
  if(modal_warehouse_name!="")
  {
    $('#add_location').attr('disabled',false);
  }
  else{
    $('#add_location').attr('disabled',true);
  }
});
$.fn.location_append=function(result){
  console.log(result)
  // $('#location_id').append($("<option></option>").attr("value",result.key).text(result.value));
  $('#location_id').append(result);
  var latest_value = $("#location_id option:last").val();
  $('#location_id').val(latest_value).change();
  $('#addlocation').modal('hide');
  $('#location_id_modal').append(result);
  var latest_value = $("#location_id_modal option:last").val();
  $('#location_id_modal').val(latest_value).change();
}
$('#location_id').change(function(){
  var id=$(this).val();
  $('#location_id_modal').val(id).trigger('change.select2');;
});
$('#location_id_modal').change(function(){
  var id=$(this).val();
  $('#location_id').val(id);
});
$('#add_location').click(function(){
  var modal_location_name=$('#modal_location_name').val();
  var data={
    name:modal_location_name,
    // modal_location_name:modal_location_name, 
  };
  var url_address= '<?php echo $this->webroot; ?>Executive/add_route_ajax';
  // var url_address= '<?php echo $this->webroot; ?>Location/location_add_ajax';
  $.ajax({
    method: "POST",
    url:url_address,
    data: data,
  }).done(function( result ) {
    
    
    $.fn.location_append(result);
    $('#modal_location_name').val('');
  });
});
$('#Location_Edit_Btn').click(function(){
  var location_id=$('#location_id').val();
  
  if(!location_id)
  {
    $('#location_id').select2('open');
    return false; 
  }
  if (location_id.length > 1){
    alert('Choose one Locaction to change');
    
    $('#location_id').select2('open');
    return false; 

  }
    var url_address= "<?= $this->webroot; ?>Executive/route_get_ajax/"+location_id;
  
  // var url_address= "<?= $this->webroot; ?>Location/location_get_ajax/"+location_id;
  $.get( url_address, function( response ) {

    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $('#LocationEditName').val(response.data.name);
      $('#LocationEditId').val(response.data.id);
      $('#Location_Edit_Modal').modal('show');
    }
  }, "json");
});
$('#edit_Location').click(function(){
  var data=$('#Location_Edit_Form').serialize();
  var location_id=$('#location_id').val();
  var url_address= "<?= $this->webroot; ?>Executive/route_edit_ajax";
  // var url_address= "<?= $this->webroot; ?>Location/location_edit_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $("#location_id option[value='" + location_id+ "']").remove();
      $('#location_id').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#Location_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#location_id').val($('#location_id option:last-child').val()).trigger('change');
      $('#Location_Edit_Form')[0].reset();
      $('#Location_Edit_Modal').modal('toggle');
    }
  }, "json");
});
