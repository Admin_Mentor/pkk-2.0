<section class="content-header">
  <h1>Manage Executive</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
    </div>
    <div class="box-body">
       <?= $this->Form->create('Executive', array('url' => array('controller' => 'Executive', 'action' => 'AddSalesman'),'id'=>'Executive_Form'));?>
      <div class="row">
        <div id="staffDiv" class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
          <?php echo $this->Form->input('staff_id',array('type'=>'select','class'=>'form-control  select2','id'=>'staff_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'options'=>$Staff,'required','label'=>'Staff')); ?>
            <?php //echo $this->Form->input('name',array('type'=>'text','id'=>'name','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
         <div id="staffName" style="display: none;" class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
         <label>Staff</label><br>
          <span id="name"></span>
           
          </div>
        </div>
        <div class="col-md-3 col-sm-3" hidden="">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('cash',array('type'=>'text','id'=>'cashname','readonly','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('mobile',array('type'=>'text','id'=>'mobile','label'=>'Contact No','class'=>'form-control','readonly')); ?>
          </div>
        </div>
        
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('username',array('type'=>'text','id'=>'username','readonly','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('password',array('type'=>'text','id'=>'password','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('warehouse_id',array('type'=>'select','id'=>'warehouse','options'=>$Warehouse,'class'=>'form-control select2','required'=>'required')); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
        <div class="col-md-12">
          <div class="form-group ">
                        <input type='hidden' id='Executive_id' value="" name="id">
            <?php echo $this->Form->input('location_id',array('multiple' => true,'type'=>'select','id'=>'location_id','options'=>$Location,'class'=>'form-control select2','required'=>'required','label'=>'Route')); ?>
          </div>
          </div>
          <div class="col-md-2" style="display: none;">
                    <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#addlocation"></i></a>
                    <a href="#" id='Location_Edit_Btn'><i class="fa fa-pencil fa ad-mar"></i></a>
                  </div>
          </div>
          <div class="col-md-2 col-sm-2">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('daily_expense',array('type'=>'text','id'=>'daily_expense','class'=>'form-control number','required'=>'required')); ?>
          </div>
        </div>
         <div class="col-md-2 col-sm-2">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('executive_credit_limit',array('type'=>'text','id'=>'executive_credit_limit','class'=>'form-control number','required'=>'required','label'=>'Credit Limit')); ?>
          </div>
        </div>
        <div class="col-md-1 col-sm-1">
<div <?php  if($ExecutiveCount >= $this->Session->read('Executive.limit')){ ?> style="display:none" <?php }?>>
          <button style="margin-top: 27%;" class="save" id='save_button' >Save</button>
          </div>
          <button style="margin-top: 27%; display:none" id='edit_button' value='' class="save">Edit</button>
        </div>
                </div>
        <?= $this->Form->end(); ?>
      </div>
      <div class="row">
        <hr>
        <div class="col-md-12">
          <div class="box-body table">
            <table class="table  table-hover boder table-bordered" id="table_data">
              <thead>
                <tr class="blue-bg">
                  <th>Name</th>
                  <th>Contact No</th>
                  <th>Route</th>
                  <th>User Name</th>
                  <th>Password</th>
                  <th>Warehouse</th>
                   <th>Credit Limit</th>
                  <th>Edit</th>
<!--                   <th>Delete</th>
 -->                  <th>Block</th>
                </tr>
              </thead>
            </tbody>
          </table>
        </div>
      </div>
      <br>
    </div>
  </div>
</div>
</section>
<?php require 'Add_Modal/Location_Add_modal.php'; ?>
<?php require 'Edit_Modal/Location_Edit_Modal.php'; ?>
<?php require 'Edit_Modal/Route_List_Modal.php'; ?>

<script type="text/javascript">
 table_data=$('#table_data').dataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Executive/Executive_Table_ajax",
      "type": "POST",
      "dataSrc": "records",
      data:function(d){
        d.location_id     = $('#location_id').val();
      }
    },
    dom: 'Bfrtip',
    "columns": [
    { "data" : "Executive.name" },
    { "data" : "Executive.mobile" },
    { "data" : "Executive.route" },
    { "data" : "Executive.username" },
    { "data" : "Executive.password" },
    { "data" : "Warehouse.name" },
    { "data" : "Executive.executive_credit_limit" },
    { "data" : "Executive.edit"},
    { "data" : "Executive.block"},
    ],
    "columnDefs": [
    { "targets": [ 7 ],"orderable": false ,},
    { "targets": [ 8 ],"orderable": false ,},
    ],
  });
 $('#location_id').change(function(){
    table_data.fnDraw();
  });
$(document).on('change','#location_id',function(){
       var location_id=$(this).val();
        var Staff_id = $('#staff_id').val();
       if(Staff_id){
       var Executive_id=$('#Executive_id').val();
       if(!Executive_id)
       {
        $.post( "<?= $this->webroot ?>Executive/get_executive_route_ajax/"+location_id,function( data ) {
      if(data.status=="This Route Already Used")
      {
        alert(data.status);
        $.each(data.ExecutiveRoute, function( key, value ) {
       $("#location_id").find("option[value="+value+"]").prop("selected", "").trigger('change');
         });
      }
  }, "json");
       }
       else
       {
         $.post( "<?= $this->webroot ?>Executive/get_executive_route_ajax_edit/"+location_id+'/'+Executive_id,function( data ) {
      if(data.status=="This Route Already Used")
      {
        alert(data.status);
         $.each(data.ExecutiveRoute, function( key, value ) {
       // $("#location_id").find("option[value="+value+"]").prop("selected", "").trigger('change');
        });
      }
  }, "json");
       }
     }
});
//$(".select_two_class").select2();
$('#staff_id').on('change',function(){
  var Staff_id = $(this).val();
    // var Staff_name=$(this).closest('tr').find('td.name').text();
    $.post( "<?= $this->webroot ?>Executive/get_staff_details_ajax/"+Staff_id,function( data ) {
    
      $('#mobile').val(data.Staff.contact_no);
      $('#username').val(data.Staff.code);
      var cashname =  data.Staff.name+data.Staff.code+'_CASH';
      $('#cashname').val(cashname);
     
    }, "json");
});
  $(document).on('click','.edit_executive',function(){
    // var Executive_name=$(this).closest('tr').find('td.name').text();
    var Executive_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Executive/get_executive_details_ajax/"+Executive_id,function( data ) {
       
       $("#location_id").val('').trigger('change');
        //  console.log(data);
      // $('#name').val(data.Executive.name);
       // $("#location_id").select2({
       //      placeholder: "Add Route"
       //  });
       $('#staff_id').val(data.Executive.staff_id).trigger('change');
       $('#staffDiv').css('display','none');
       // $('#staff_id').val(Executive_id).trigger('change');
       var staffName = $('#staff_id option:selected').text();
       $('#staffName').css('display','block');
       $('#name').text(data.staffName)
       $('#cashname').val(data.Cash.name);
       
        $('#staff_id').attr('required',false);
      $('#mobile').val(data.Executive.mobile);
      $('#daily_expense').val(parseFloat(data.Executive.daily_expense));
       $('#executive_credit_limit').val(data.Executive.executive_credit_limit);
      $('#password').val(data.Executive.password);
      $('#username').val(data.Executive.username);
      $('#warehouse').val(data.Executive.warehouse_id).trigger('change');
            $("#Executive_id").val(data.Executive.id);
            //console.log(data.ExecutiveRoute);
      $.each(data.ExecutiveRoute, function( key, value ) {
        $("#location_id").find("option[value="+value+"]").prop("selected", "selected").trigger('change');
      });
      $('#save_button').css('display','none');
      $('#edit_button').css('display','');
      $("#mobile").append("<input type='text' id='Executive_id' name='data[Executive][id]' value='"+data.Executive.id+"'>");
      $('#Executive_Form').attr('action','<?= $this->webroot; ?>Executive/EditExecutive');
    }, "json");
  });
  $(document).on('click','.route-show',function(){
    var Executive_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Executive/get_executive_routes/"+Executive_id,function( data ) {
        $('#Route_List_Modal').modal('show');
        $('#RouteTable tbody').html(data.row);
        $('#executive_id').val(data.executive_id);
     
    }, "json");
  });
  $(document).on('click','.del-route',function(){
    if(confirm('Are You Sure want to remove this route'))
    {
      var id=$(this).data('id');
    var parentRow =$(this).parents('tr');
    $.get( "<?= $this->webroot ?>Executive/DeleteExecutiveRoute/"+id,function( data ) {
       parentRow.hide();
       location.reload();
     
    }, "json");
    }

    
  });
  <?php require 'Script/location.js' ?>
</script>
