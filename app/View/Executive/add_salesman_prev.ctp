<section class="content-header">
  <h1>Manage Sales Man</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
    </div>
    <div class="box-body">
      <div class="row">
        <?= $this->Form->create('Executive', array('url' => array('controller' => 'Executive', 'action' => 'AddSalesman'),'id'=>'Executive_Form'));?>
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('name',array('type'=>'text','id'=>'name','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('mobile',array('type'=>'text','id'=>'mobile','label'=>'Contact No','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-sm-12">
          <button style="margin-top: 5%;" class="save pull-right" id='save_button' >Save</button>
          <button style="margin-top: 5%; display:none" id='edit_button' value='' class="save pull-right">Edit</button>
        </div>
      </div>
      <?= $this->Form->end(); ?>
    </div>
    <div class="row">
      <hr>
      <div class="col-md-12">
        <div class="box-body table-responsive no-padding">
          <table class="table datatable table-hover boder">
            <thead>
              <tr class="blue-bg">
                <th>Name</th>
                <th>Contact No</th>
                <th>Edit</th>
                <th>Delete</th>
              </tr>
            </thead>
            <?php foreach ($Executive as $key => $value): ?>
              <tr>
                <td class='name'><?= $value['Executive']['name'];  ?></td>
                <td><?= $value['Executive']['mobile']; ?></td>
                <td><i class="fa fa-edit edit_executive" aria-hidden="true" style="color: #3c8dbc;"></i></td>
                <td><a onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Executive/DeleteExecutive/<?= $value['Executive']['id']; ?>"><i class="fa fa-trash blue-col blue-col"></i></a></td>
              <?php endforeach ?>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <br>
  </div>
</div>
</div>
</section>
<script type="text/javascript">
  $(document).on('click','.edit_executive',function(){
    var Executive_name=$(this).closest('tr').find('td.name').text();
    $.post( "<?= $this->webroot ?>Executive/get_executive_details_ajax/"+Executive_name,function( data ) {
      $('#name').val(data.Executive.name);
      $('#mobile').val(data.Executive.mobile);
      $('#password').val(data.Executive.password);
      $('#username').val(data.Executive.username);
      $('#warehouse').val(data.Executive.warehouse_id).trigger('change');
      $('#location').val(data.Executive.location_id).trigger('change');
      $('#save_button').css('display','none');
      $('#edit_button').css('display','');
      $("#name").append("<input type='text' id='Executive_id' name='data[Executive][id]' value='"+data.Executive.id+"'>");
      $('#Executive_Form').attr('action','<?= $this->webroot; ?>Executive/EditExecutive');
    }, "json");
  });
</script>