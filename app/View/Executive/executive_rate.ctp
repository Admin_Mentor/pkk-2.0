
<section class="content-header">
  <h1>Executive Rate</h1>
</section>
<section class="content">
     <?= $this->Form->create('ExecutiveRate', array('url' => array('controller' => 'Executive', 'action' => 'ExecutiveRate'),'id'=>'ExecutiveRate'));?>
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row">
         <div class="col-md-3">
           <?php echo $this->Form->input('executive_id',array('id'=>'executive_id','style'=>'width:100%','class'=>'form-control select_two_class')); ?>
         </div>
         <div class="col-md-6" class=" form-control" style="margin-top:2%;width:20%">
          <button class='btn btn-success' type='submit' value="commit" id='fetch_button'>Save</button>
        </div>
      </div>
    </div>
    <div class="box-body">
      <div class="col-md-4 col-xs-12">
        <h3 class="muted "></h3>
      </div>
  <table id='table_list' style="width:98% !important" class="table table-hover boder  table-bordered" > 
         <thead>
          <tr class="blue-bg">
            <th>Product</th>
            <th>Current Rate</th>
            <th>New Rate</th>
          </tr>
        </thead>
        <tbody>

        </tbody>
      </table>
    </div>
  </div>
</div>
        <?= $this->Form->end(); ?>
</section>
<script type="text/javascript"> 
 $('#table_list').DataTable( {
    "processing": true,
    "serverSide": true,
    "searching"  : false,
    "paging"     : false,
    "bInfo"      : false,
    "ordering"   : false,
    "ajax": {
      "url": "<?= $this->webroot ?>Executive/ExecutiveRate_Updation",
      "type": "POST",
      data:function( d ) {
        d.executive_id= $('#executive_id').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "Product.name" },
    { "data" : "Product.current_rate"},
    { "data" : "Product.new_rate"},
    ],
  });
 $(document).on('change', '#executive_id', function () {
    table = $('#table_list').dataTable();
    table.fnDraw();
  });
 
</script>