
<style type="text/css">
.pie_tp_mrgn{
  margin-top: 35px;
}

</style>

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/none.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<section class="dashboard_add_bg_color">

  <div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
      <div class="col-lg-4 col-xs-6">
        <div class="small-box bg-aqua Card_Box_Top">
          <div class="inner">
            <h3><?= round($net_amount,2); ?></h3>
            <p>Monthly Sale</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
<!--           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
 -->        </div>
      </div>
      <div class="col-lg-3 col-xs-6" hidden>
        <div class="small-box bg-green Card_Box_10">
          <div class="inner">
            <h3><?= number_format($collection); ?></h3>
            <p>Monthly Collection</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
<!--           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
 -->        </div>
      </div>
      <div class="col-lg-4 col-xs-6">
        <div class="small-box bg-yellow color_03">
          <div class="inner">
            <h3><?= number_format($stockcost); ?></h3>
            <p>Stock</p>
          </div>
          <div class="icon">
            <i class="ion ion-person-add"></i>
          </div>
<!--           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
 -->        </div>
      </div>
      <div class="col-lg-4 col-xs-6">
        <div class="small-box bg-red color_004">
          <div class="inner">
            <h3><?= number_format($expense); ?></h3>
            <p>Monthly Expense</p>
          </div>
          <div class="icon">
            <i class="ion ion-pie-graph"></i>
          </div>
<!--           <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
 -->        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12">
      <div class="col-md-12">
        <h4 class="Head_Graph">Sales Graph</h4>
        <div id="chartdiv_new" class="Bg_White_Graph"></div>
      </div>
    </div>
  </div>
</div>
<div class="row mrgn_pie_chart">
  <div class="col-md-12 col-lg-12 col-sm-12">
    <div class="col-md-6 col-lg-6 col-sm-6" hidden>
      <h4 class="Head_Graph pie_tp_mrgn">Route Monthly Sale</h4>
      <div id="Pie_Chart" class="Bg_White_Graph"></div>
      <!--   <div id="chartdiv01" class="Bg_White_Graph"></div> -->
    </div>
    <div class="col-md-12 col-lg-12 col-sm-12">
      <h4 class="Head_Graph pie_tp_mrgn">Purchase Graph</h4>
      <div id="Purchase_Graph" class="Bg_White_Graph"></div>
    </div>
  </div>
</div>
</section>

<section id="footer_btm">
  <p>© 2018 Mentor Performance Rating Private Limited.</p>
</section>

<script>

  <?php 

  ?>

</script>


<script>
 $(document).ready(function() {
  $('.progress .progress-bar').css("width",
    function() {
      return $(this).attr("aria-valuenow") + "%";
    }
    )
});
</script>

<script>
  $('.main-sidebar').ready(function(){
    $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
  });
</script>

<!--Recently Added-->

<script>
   <?php 
  $P_jan=$Sales_month[0]['jan'];
  $P_feb=$Sales_month[0]['feb'];
  $P_mar=$Sales_month[0]['mar'];
  $P_apr=$Sales_month[0]['apr'];
  $P_may=$Sales_month[0]['may'];
  $P_jun=$Sales_month[0]['jun'];
  $P_jul=$Sales_month[0]['jul'];
  $P_aug=$Sales_month[0]['aug'];
  $P_sep=$Sales_month[0]['sep'];
  $P_oct=$Sales_month[0]['oct'];
  $P_nov=$Sales_month[0]['nov'];
  $P_dec=$Sales_month[0]['dec'];
  $test = [$P_apr,$P_may,$P_jun,$P_jul,$P_aug,$P_sep,$P_oct,$P_nov,$P_dec,$P_jan,$P_feb,$P_mar];
  $sum_sale = 0;
  for ($i = 0; $i < count($test); $i++) {
    $sum_sale += $test[$i];
  }

  ?>
  var jan=<?php echo round($P_jan); ?>;
  var feb=<?php echo round($P_feb); ?>;
  var march=<?php echo round($P_mar); ?>;
  var april=<?php echo round($P_apr); ?>;
  var may=<?php echo round($P_may); ?>;
  var june=<?php echo round($P_jun); ?>;
  var july=<?php echo round($P_jul); ?>;
  var august=<?php echo round($P_aug); ?>;
  var sept=<?php echo round($P_sep); ?>;
  var oct=<?php echo round($P_oct); ?>;
  var nov=<?php echo round($P_nov); ?>;
  var dec=<?php echo round($P_dec);?>;
  var chart = AmCharts.makeChart( "chartdiv_new", {
    "type": "serial",
    "theme": "light",
    "dataProvider": [

    {
      "country": "Jan",
      "visits": jan,
      "color": "#FF0F00"
    }, {
      "country": "Feb",
      "visits": feb,
      "color": "#FF6600"
    }, {
      "country": "March",
      "visits": march,
      "color": "#FF9E01"
    }, {
      "country": "April",
      "visits": april,
      "color": "#FCD202"
    }, {
      "country": "May",
      "visits": may,
      "color": "#F8FF01"
    }, {
      "country": "June",
      "visits": june,
      "color": "#B0DE09"
    }, {
      "country": "July",
      "visits": july,
      "color": "#04D215"
    }, {
      "country": "Aug",
      "visits": august,
      "color": "#0D8ECF"
    }, {
      "country": "Sep",
      "visits": sept,
      "color": "#0D52D1"
    }, {
      "country": "Oct",
      "visits": oct,
      "color": "#2A0CD0"
    }, {
      "country": "Nov",
      "visits": nov,
      "color": "#8A0CCF"
    }, {
      "country": "Dec",
      "visits": dec,
      "color": "#CD0D74"
    },   ],
    "valueAxes": [ {
      "gridColor": "#FFFFFF",
      "gridAlpha": 0.2,
      "dashLength": 0
    } ],
    "gridAboveGraphs": true,
    "startDuration": 1,
    "graphs": [ {
      "balloonText": "[[category]]: <b>[[value]]</b>",
      "fillAlphas": 0.8,
      "lineAlpha": 0.2,
      "type": "column",
      "valueField": "visits"
    } ],
    "chartCursor": {
      "categoryBalloonEnabled": false,
      "cursorAlpha": 0,
      "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
      "gridPosition": "start",
      "gridAlpha": 0,
      "tickPosition": "start",
      "tickLength": 20
    },
    "export": {
      "enabled": true
    }

  } );
</script>
<script>
   <?php 
  $P_jan=$Purchase_month[0]['jan'];
  $P_feb=$Purchase_month[0]['feb'];
  $P_mar=$Purchase_month[0]['mar'];
  $P_apr=$Purchase_month[0]['apr'];
  $P_may=$Purchase_month[0]['may'];
  $P_jun=$Purchase_month[0]['jun'];
  $P_jul=$Purchase_month[0]['jul'];
  $P_aug=$Purchase_month[0]['aug'];
  $P_sep=$Purchase_month[0]['sep'];
  $P_oct=$Purchase_month[0]['oct'];
  $P_nov=$Purchase_month[0]['nov'];
  $P_dec=$Purchase_month[0]['dec'];
  $test = [$P_apr,$P_may,$P_jun,$P_jul,$P_aug,$P_sep,$P_oct,$P_nov,$P_dec,$P_jan,$P_feb,$P_mar];
  $sum_sale = 0;
  for ($i = 0; $i < count($test); $i++) {
    $sum_sale += $test[$i];
  }
  ?>
  var jan=<?php echo round($P_jan); ?>;
  var feb=<?php echo round($P_feb); ?>;
  var march=<?php echo round($P_mar); ?>;
  var april=<?php echo round($P_apr); ?>;
  var may=<?php echo round($P_may); ?>;
  var june=<?php echo round($P_jun); ?>;
  var july=<?php echo round($P_jul); ?>;
  var august=<?php echo round($P_aug); ?>;
  var sept=<?php echo round($P_sep); ?>;
  var octo=<?php echo round($P_oct); ?>;
  var nov=<?php echo round($P_nov); ?>;
  var dec=<?php echo round($P_dec);?>;
  var chart = AmCharts.makeChart( "Purchase_Graph", {
    "type": "serial",
    "theme": "light",
    "dataProvider": [
    {
      "country": "Jan",
      "visits": jan,
      "color": "#FF0F00"
    }, {
      "country": "Feb",
      "visits": feb,
      "color": "#FF6600"
    }, {
      "country": "March",
      "visits": march,
      "color": "#FF9E01"
    }, {
      "country": "April",
      "visits": april,
      "color": "#FCD202"
    }, {
      "country": "May",
      "visits": may,
      "color": "#F8FF01"
    }, {
      "country": "June",
      "visits": june,
      "color": "#B0DE09"
    }, {
      "country": "July",
      "visits": july,
      "color": "#04D215"
    }, {
      "country": "Aug",
      "visits": august,
      "color": "#0D8ECF"
    }, {
      "country": "Sep",
      "visits": sept,
      "color": "#0D52D1"
    }, {
      "country": "Oct",
      "visits": octo,
      "color": "#2A0CD0"
    }, {
      "country": "Nov",
      "visits": nov,
      "color": "#8A0CCF"
    }, {
      "country": "Dec",
      "visits": dec,
      "color": "#CD0D74"
    },],
    "valueAxes": [ {
      "gridColor": "#FFFFFF",
      "gridAlpha": 0.2,
      "dashLength": 0
    } ],
    "gridAboveGraphs": true,
    "startDuration": 1,
    "graphs": [ {
      "balloonText": "[[category]]: <b>[[value]]</b>",
      "fillAlphas": 0.8,
      "lineAlpha": 0.2,
      "type": "column",
      "valueField": "visits"
    } ],
    "chartCursor": {
      "categoryBalloonEnabled": false,
      "cursorAlpha": 0,
      "zoomable": false
    },
    "categoryField": "country",
    "categoryAxis": {
      "gridPosition": "start",
      "gridAlpha": 0,
      "tickPosition": "start",
      "tickLength": 20
    },
    "export": {
      "enabled": true
    }

  } );
</script>

