<style type="text/css">
    .blue-bg {
        background-color: #0e3d58;
        color: #FFF !important;
        white-space: nowrap;
    }
    .table_with_border_full {
        border: 1px solid #d2d2d2;
    }
    .tbl_top_hed {
        margin: 0px;
        padding-top: 10px;
        padding-bottom: 15px;
    }
    .view_color {
        color: #0e3d58;
    }

    .ADd_Btn {
        margin-top: 26px;
        border: none;
        padding-left: 20px;
        padding-right: 20px;
        letter-spacing: 0.3px;
        background-color: #3aa5a5;
    }

    .btn_sub_mit_2 {
        border: none;
        background-color: #a00000;
        margin-top: 24px;
        letter-spacing: 0.3px;
    }

    .btn_sub_mit_2:hover{
        background-color: #8a0e0e !important;
        transition: ease all 0.9s;
    }

    .btn_sub_mit_2:active{
        background-color: #8a0e0e !important;
    }

    .btn_sub_mit_2:focus{
        background-color: #8a0e0e !important;
    }



    .ADd_Btn:hover{
        background-color: #207f7f !important;
        transition: ease all 0.9s;
    }

    .ADd_Btn:active{
        background-color: #207f7f !important;
    }

    .ADd_Btn:focus{
        background-color: #207f7f !important;
    }

    .bdr_full_cover {
        border: 1px solid #d2d6de;
        margin-top: 15px;
        padding-top: 15px;
        padding-bottom: 15px;
    }

    .Wages_bdr {
        border: 1px solid #d2d6de;
        padding-left: 15px;
        padding-top: 11px;
        margin-bottom: 25px;
    }

    .pdng_10 {
        padding: 10px !important;
    }

</style>
<section class="content-header">
    <h1> Database Backup </h1>

</section>

<section class="content">
    <div class="box">
        <div class="row" style="margin-top: 2%;"> 
            <div class="col-md-12 col-lg-12 col-sm-12">
               
                <div class="col-md-3 col-lg-3 col-sm-3">
                    <div class="form-group"> 
                        <!-- <a href="<?= $this->webroot; ?>Homes/DbBackup"><button class="btn btn-success btn_sub_mit_2" id="Upload_document">Download Database</button></a> -->
                        <button class="btn btn-success btn_sub_mit_2" id="generate_db">Generate Database</button>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row" style="margin-top: 2%;">
            <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="box-body table-responsive pdng_10">
                    <table class="table datatable table-hover table-bordered">
                       <thead>
                            <tr class="blue-bg">
                                <!-- <th width="10%">Sl No.</th> -->
                                <th>File Name</th>
                                <th width="10%">Action</th>
                            </tr>
                       </thead>                         
                        <tbody>
                     <?php $i=1; rsort($files); foreach($files as $value): 
                     $ext = pathinfo($value, PATHINFO_EXTENSION);
                    //  print_r($ext);die;
                     if($ext=="sql"){ ?>
                            <tr>
                                <!-- <td><?= $i; ?></td> -->
                                <td><?= $value;?></td>
                                <td><a href="<?= $this->webroot; ?>app/webroot/sql/<?=$value?>" target="_blank" >Download</a>&nbsp;&nbsp;&nbsp;<a href="#" id="db_delete" data="<?= $value;?>">Delete</a></td>
                            </tr>
                        <?php $i++; } endforeach; ?> 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).on('click', '#generate_db', function(e){ 
       $.ajax({
                url: <?php echo $this->webroot ?> + 'Homes/DbBackup',
                type: 'GET',
                async:true,
                dataType: 'json',
                success: function(response){
                }
            });
            location.reload();
    });
    $(document).on('click', '#db_delete', function(e){ 
        var name = $(this).attr('data');
        if(confirm("Are you sure want to delete?")){
            $.ajax({
                url: <?php echo $this->webroot ?> + 'Homes/DbDelete',
                type: 'post',
                data: {name: name},
                dataType: 'json',
                success: function(response){
            
                    // Changing image source when remove
                    if(response == 1){
                        alert('File deleted successfully');
                        location.reload();
                    }
                    else alert('Filed to delete file!')
                }
            });
        }else{
            return false;
        }
        
    });
</script>

