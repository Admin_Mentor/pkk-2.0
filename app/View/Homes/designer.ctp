<table>
	<thead>
		<tr>
			<th>Type</th>
			<th>Group</th>
			<th>Sub Group</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($SubGroups as $key => $SubGroup): ?>
			<tr>
				<td><?= $value['Type']['name']; ?></td>
				<td><?= $value['Group']['name']; ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>