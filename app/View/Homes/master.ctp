  <section class="content">
    <div class="row">
     <section class="content"> 
      <div class="row mr-tp-50">
      <?php
      foreach ($master_dashboard as $key => $value) {
     
      ?>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <a href="<?= $this->webroot; ?><?= $value['href'];?>">
            <div href="#4a" class="dashboardCard third click_button mar_btm">
              <div class="info-box">
                <span class="info-box-icon <?= $value['icon_color'];?>"><i class="<?= $value['icon']; ?>"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text"><?= $value['name'];?></span>
                
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div></a>
          </div>
          <?php
        }
          ?>
         
              </div>
            </section> 
          </div>
        </section>
        
        <script type="text/javascript">
          $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
        </script>