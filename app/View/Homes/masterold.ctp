  <section class="content">
    <div class="row">
     <section class="content"> 
      <div class="row mr-tp-50">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <a href="<?= $this->webroot; ?>Accountings/Capital">
            <div href="#4a" class="dashboardCard third click_button mar_btm">
              <div class="info-box">
                <span class="info-box-icon bg_in_a"><i class="fa fa-credit-card"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Capital</span>
                  <!-- <span class="info-box-number"> 131,280<h3></h3></span>-->
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div></a>
          </div>
          <div class="col-md-3 col-sm-6 col-xs-12">
            <a href="<?= $this->webroot; ?>Accountings/CurrentLiabilityAdvanceIncome">
              <div href="#3a" class="dashboardCard first click_button mar_btm">
                <div class="info-box">
                  <span class="info-box-icon bg_in_b"><i class="fa fa-line-chart"></i></span>
                  <div class="info-box-content">
                    <span class="info-box-text">Liabilities</span>
                    <!--<span class="info-box-number">1,617,626<h3></h3></span>-->
                  </div>
                </div>
              </div></a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <a href="<?= $this->webroot; ?>Accountings/IncomeTransaction">
                <div href="#8a" class="dashboardCard six click_button mar_btm">
                  <div class="info-box">
                    <span class="info-box-icon bg_in_c"> <i class="fa fa-money"></i></span>
                    <div class="info-box-content">
                      <span class="info-box-text">Income</span>
                      <!--<span class="info-box-number">-5,840<h3></h3></span>-->
                    </div>
                  </div>
                </div></a>
              </div>
              <div class="col-md-3 col-sm-6 col-xs-12">
                <a href="<?= $this->webroot; ?>Accountings/AssetCurrentCashTransaction">
                  <div href="#7a" class="dashboardCard five click_button mar_btm">
                    <div class="info-box">
                      <span class="info-box-icon bg_in_d"> <i class="fa fa-briefcase"></i></span>
                      <div class="info-box-content">
                        <span class="info-box-text">Asset</span>
                        <!--<span class="info-box-number">0 <h3></h3></span>-->
                      </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
                  </div></a>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <a href="<?= $this->webroot; ?>Accountings/ExpenseTransaction">
                    <div href="#7a" class="dashboardCard five click_button mar_btm">
                      <div class="info-box">
                        <span class="info-box-icon bg_in_e"> <i class="fa fa-external-link"></i></span>
                        <div class="info-box-content">
                          <span class="info-box-text">Expense</span>
                          <!--<span class="info-box-number">0 <h3></h3></span>-->
                        </div><!-- /.info-box-content -->
                      </div><!-- /.info-box -->
                    </div>
                  </a>
                </div>
              </div>
            </section> 
          </div>
        </section>
        
        <script type="text/javascript">
          $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
        </script>