<section class="content-header">
  <h1>Manage Staffs</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
    </div>
    <div class="box-body">
      <div class="row">
        <?= $this->Form->create('Staff', array('url' => array('controller' => 'Hr', 'action' => 'AddStaff'),'id'=>'Staff_Form'));?>
        <div class="col-md-4 col-sm-4">
          <div class="form-group col-md-8">
            <?php echo $this->Form->input('name',array('type'=>'text','id'=>'name','class'=>'form-control','required'=>'required')); ?>
          </div>
          <div class="form-group col-md-4">
            <?php echo $this->Form->input('code',array('type'=>'text','readonly','id'=>'code','label'=>'Code','class'=>'form-control','value'=>'Auto-Gen')); ?>
          </div>
        </div>

        <div class="col-md-2 col-sm-2">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('contact_no',array('type'=>'text','id'=>'contact_no','label'=>'Contact No','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
            <?= $this->Form->input('date_of_joining',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Joining Date','value'=>date('d-m-Y'))); ?>
          </div>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
            <?php echo $this->Form->input('role_id',array('type'=>'select','class'=>'form-control  select_two_class','id'=>'role_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'required')); ?>
          </div>
        </div>
        <div class="col-md-2 col-sm-2">
          <div class="form-group col-md-12">
            <?= $this->Form->input('address',array('class'=>'form-control','type'=>'textarea','rows'=>2,'id'=>'address','label'=>'Address',)); ?>
          </div>
        </div>
        <div class="col-md-2 col-sm-2" style="display: none;">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('id_no',array('type'=>'text','id'=>'id_no','label'=>'ID No','class'=>'form-control')); ?>
          </div>
        </div>
        <div class="col-md-2 col-sm-2"  style="display: none;">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('passport_no',array('type'=>'text','id'=>'passport_no','label'=>'Passport No','class'=>'form-control')); ?>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-6 col-sm-12">
        <button  class="save pull-right" id='save_button' >Save</button>
        <button style="margin-top: 5%; display:none" id='edit_button' value='' class="save pull-right">Edit</button>
      </div>
      <?= $this->Form->end(); ?>
    </div>
    <div class="row">
      <hr>
      <div class="col-md-12">
        <div class="box-body table-responsive no-padding">
          <table class="table datatable table-hover boder">
            <thead>
              <tr class="blue-bg">
                <th>Name</th>
                <th>Contact No</th>
                <th>Code</th>
                <th>Role</th>
                <th>Date of Joining</th>
                <th>Edit</th>
                <th>Delete</th>
                <th  style="display: none;">Upload</th>
              </tr>
            </thead>
            <?php foreach ($Staff as $key => $value): ?>

              <tr>
                <td class='name'><?= $value['Staff']['name'];  ?></td>
                <td><?= $value['Staff']['contact_no']; ?></td>
                <td><?= $value['Staff']['code']; ?></td>
                <td><?= $value['Role']['name']; ?></td>
                <td><?= date('d-m-Y',strtotime($value['Staff']['date_of_joining'])); ?></td>
                <td><i class="fa fa-edit edit_staff" aria-hidden="true" data-id="<?= $value['Staff']['id']?>" style="color: #3c8dbc;"></i></td>
                <td><a onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Hr/DeleteStaff/<?= $value['Staff']['id']; ?>"><i class="fa fa-trash blue-col blue-col"></i></a></td>
                <td  style="display: none;">
                  <a href="#" class="Upload_Btn" data-id="<?= $value['Staff']['id']; ?>"><i class="fa fa-upload fa ad-mar"></i></a>
                </td>

              <?php endforeach ?>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    <br>
  </div>
</div>
</div>
</section>
<?php require 'Modal/Upload_file_Modal.php'; ?>


<script type="text/javascript">
  $(document).on('click','.edit_staff',function(){
    var Staff_id = $(this).data('id');
// var Staff_name=$(this).closest('tr').find('td.name').text();
$.post( "<?= $this->webroot ?>Hr/get_staff_details_ajax/"+Staff_id,function( data ) {
  $('#name').val(data.Staff.name);
  $('#contact_no').val(data.Staff.contact_no);
  $('#code').val(data.Staff.code);
  $('#address').val(data.Staff.address);
  $('#role_id').val(data.Staff.role_id).change();
  $('#date').val(data.Staff.date_of_joining);
  $('#save_button').css('display','none');
  $('#edit_button').css('display','');
  $("#name").append("<input type='text' id='Staff_id' name='data[Staff][id]' value='"+data.Staff.id+"'>");
  $('#Staff_Form').attr('action','<?= $this->webroot; ?>Hr/EditStaff');
}, "json");
});

  $(document).on('click','.Upload_Btn',function(){
    var id = $(this).data('id');
    $('#staff_id').val(id);
    $('#Upload_Modal').modal('show');
  });
  $('#Upolad_Form').on('submit', function(e){
    e.preventDefault();
    var formdatas = new FormData($('#Upolad_Form')[0]);
    $.ajax({
      url: '<?= $this->webroot ?>Hr/document_upload/',
      dataType: 'json',
      method: 'post',
      data:  formdatas,
      contentType: false,
      processData: false
    })
    .done(function(response) {
//show result
if (response.result == 'success') {


}  else {
//show default message
}
$("#Upolad_Form")[0].reset();
})
    .fail(function(jqXHR) {
      if (jqXHR.status == 403) {
        window.location = '/';
      } else {
        console.log(jqXHR);

      }
    });

  });
</script>
