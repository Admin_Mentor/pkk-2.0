<section class="content-header">
	<h1> Basic Pay Structure </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="row-wrapper">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<?php echo $this->Form->create('AllowDeduct', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'AllowDeduct_Form']); ?>
								<div class="box-body">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
													<?php echo $this->Form->input('name',array('type'=>'text','id'=>'name','class'=>'form-control','required'=>'required')); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('type',array('type'=>'select','class'=>'form-control  select2','id'=>'type','style'=>'width: 100%;','empty'=>[''=>'Select'],'options'=>array('Allowance'=>'Allowance','Deduction'=>'Deduction'),'value'=>'Deduction','required','label'=>'Type')); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('is_percentage',array('type'=>'select','class'=>'form-control  select2','id'=>'is_percentage','style'=>'width: 100%;','empty'=>[''=>'Select'],'options'=>array('1'=>'Yes','2'=>'No'),'value'=>'1','required','label'=>'Percentage')); ?>
													</div>
												</div>
											</div>
											
											
										</div>
									</div>
									<div class="col-md-12">
										<div class="row">
											
											<div class="col-md-6 col-lg-3 col-sm-3 col-xs-12">
												
											</div>
											
											<div class="col-md-6 col-sm-6 col-sm-12"> 
												<button style="margin-top: 5%;" class="save pull-right" id='save_button' >Save</button>
          <button style="margin-top: 5%; display:none" id='edit_button' value='' class="save pull-right">Edit</button>
											</div>
										</div>
									</div>
								</div>
								<?= $this->Form->end(); ?>
							</div>
							<div class="row-wrapper">
								<div class="row">
									<div class="col-md-12">
										<div class="box-body table-responsive no-padding">
											<table class="boder table table-condensed table datatable boder" id="myTable">
												<thead>
													<tr class="blue-bg">
														<th class="padding_left">Name</th>
														<th>Type</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<?php $total=0; 
													foreach ($AllowDeducts as $key => $value): ?>
													<tr class="blue-pddng view_transaction">
														<td class='name'><?= $value['AllowDeduct']['name']; ?></td>
														<td><?= $value['AllowDeduct']['type'] ?></td>
														<td>
														<a class="edit_allowdeduct" data-id="<?= $value['AllowDeduct']['id']?>" href="#"><i class="fa fa-edit " aria-hidden="true"  style="color: #3c8dbc;"></i></a>
														<a onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Hr/DeleteAllowDeduct/<?= $value['AllowDeduct']['id']; ?>"><i class="fa fa-trash blue-col blue-col"></i></a>
														</td>
													</tr>
												<?php endforeach ?>
											</tbody>
											
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
	</div>
</div>
</section>

<script type="text/javascript">
	$(document).on('click','.edit_allowdeduct',function(){
    var pay_id = $(this).data('id');
    $.post( "<?= $this->webroot ?>Hr/get_allowdeduct_details_ajax/"+pay_id,function( data ) {
    	
    	$('#is_percentage').val(data.AllowDeduct.is_percentage).trigger('change');
    	$('#type').val(data.AllowDeduct.type).trigger('change');
      $('#name').val(data.AllowDeduct.name);
      
      
      $('#save_button').css('display','none');
      $('#edit_button').css('display','');
      $("#name").append("<input type='text' id='pay_id' name='data[AllowDeduct][id]' value='"+data.AllowDeduct.id+"'>");
      $('#AllowDeduct_Form').attr('action','<?= $this->webroot; ?>Hr/EditAllowDeduct');
    }, "json");
  });
</script>
<script type="text/javascript">
	
</script>