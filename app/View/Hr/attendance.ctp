<style type="text/css">
input[type="checkbox"]{
	width: 20px;
	height: 20px;
}
th, td { white-space: nowrap; }
div.dataTables_wrapper {
	width: 100%;
	margin: 0 auto;
}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<section class="content-header">
	<h1> Attendance Management </h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?= $this->Form->create('BasicPay', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'BasicPay_Form']); ?>
			<div class="col-md-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
							<?= $this->Form->input('month',array('value'=>date('m-Y'),'type'=>'text','class'=>'form-control pull-right datepicker_month','id'=>'month','required','data-inputmask'=>"'alias': 'mm-yyyy'",'data-mask'=>'data-mask','label'=>'Month')); ?>
						</div>
						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
							<?= $this->Form->input('staff_id',array('type'=>'select','class'=>'form-control  select_two_class','id'=>'staff_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'required','label'=>'Staff')); ?>
						</div>
						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
							<h4 class="text-right"><b style="color:#00a65a">FDP:Full Day Present</b></h4>
							<h4 class="text-right"><b style="color:#00a65a">HDP:Half Day Present</b></h4>
							<h4 class="text-right"><b style="color:#dd4b39">Ab:Absent</b></h4>
							<h4 class="text-right"><b style="color:#005082">HD:Holiday</b></h4>
						</div>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body">
			<input type="hidden" id='table_edit_id' name="">
			<table class="boder table table-condensed table-bordered" id="table_data" width="100%">
				<thead>
					<tr class="blue-bg">
						<th rowspan="2">Name</th>
						<th rowspan="2">Month</th>
						<th rowspan="2">WrokingDays</th>
						<th rowspan="2">WorkedDays</th>
						<th rowspan="2">Action</th>
						<?php for ($i=1; $i <=31 ; $i++) : ?>
							<th class="text-center" colspan="2"><?= $i; ?></th>
						<?php endfor; ?>
					</tr>
					<tr class="blue-bg">
						<?php for ($i=1; $i <=31 ; $i++) : ?>
							<th>Full</th>
							<th>Half</th>
						<?php endfor; ?>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div> 
</section>
<script type="text/javascript">
	var table_data=$('#table_data').dataTable({
		"processing"    : true,
		"serverSide"    : true,
		"ordering"      : false,
		"paging"        : false,
		"bInfo"         : false,
		"searching"     : false,
		"scrollX"       : true,
		"scrollCollapse": true,
		"fixedColumns"  :{
			leftColumns: 5,
		},
		"ajax": {
			"url": "<?= $this->webroot ?>Hr/AttendanceTable",
			"type": "POST",
			"dataSrc": "records",
			data:function( d ) {
				d.staff_id= $('#staff_id').val();
				d.month    = '01-'+$('#month').val();
				d.table_id= $('#table_edit_id').val();
			},
		},
		"columns": [
		{ "data" : "Staff.name"},
		{ "data" : "Staff.month" },
		{ "data" : "Staff.no_of_working_days" },
		{ "data" : "Staff.worked_days" },
		{ "data" : "Staff.action" },
		<?php for ($i=1; $i <=31 ; $i++) : ?>
		{ "data" : "Staff.full_day_"+<?= $i ?> ,'className': "text-center" },
		{ "data" : "Staff.half_day_"+<?= $i ?> ,'className': "text-center" },
		<?php endfor; ?>
		],
		"columnDefs": [
		{ "targets": [ 0 ],'className': "text-center" },
		{ "targets": [ 1 ],'className': "text-center" },
		{ "targets": [ 2 ],'className': "text-center" },
		{ "targets": [ 3 ],'className': "text-center" },
		{ "targets": [ 4 ],'className': "text-center" },
		],
	});
	$('#staff_id,#month,#role_id').change(function(){
		table_data.fnDraw();
	});
</script>
<script type="text/javascript">
	$(document).on('click','.edit_attendance',function(){
		var table_id=$(this).attr('table_id');
		$('#table_edit_id').val(table_id);
		table_data.fnDraw();
	});
	$(document).on('click','.ok_attendance',function(){
		var data={
			staff_id  :$(this).attr('staff_id'),
			month     :$(this).attr('month'),
			full_day:$("#table_data input[name='full_day[]']:checkbox:checked").map(function(){
				return $(this).val();
			}).get(),
			half_day:$("#table_data input[name='half_day[]']:checkbox:checked").map(function(){
				return $(this).val();
			}).get(),
		};
		var url="<?=$this->webroot;?>Hr/Attendance_Update_Function";
		$.post(url,data,function(response){
			if(response.result!='success') { alert(response.result); return false; }
			$('#table_edit_id').val(0);
			table_data.fnDraw();
		},"json")
	});
	$('input').click(function(){
		$(this).select();
	});
	$('#month').inputmask('99-9999',{placeholder:"mm/yyyy"});
	$(".datepicker_month").datepicker( {
		format: "mm-yyyy",
		viewMode: "months", 
		minViewMode: "months"
	});
</script>