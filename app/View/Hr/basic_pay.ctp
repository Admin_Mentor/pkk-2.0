<section class="content-header">
	<h1> Basic Pay Structure </h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?= $this->Form->create('BasicPay', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'BasicPay_Form']); ?>
			<div class="col-md-12">
				<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
					<div class="form-group">
						<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
							<?= $this->Form->input('wef_date',array('type'=>'text','class'=>'form-control pull-right datepicker_month','id'=>'date','required','data-inputmask'=>"'alias': 'mm-yyyy'",'data-mask'=>'data-mask','label'=>'WEF Date')); ?>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
							<?= $this->Form->input('staff_id',array('type'=>'select','class'=>'form-control  select_two_class','id'=>'staff_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'required','label'=>'Staff')); ?>
						</div>
						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
							<?= $this->Form->input('amount',array('class'=>'form-control number','type'=>'text','required','id'=>'amount',)); ?>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
					<div class="form-group">
						<div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
							<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
						</div>
						<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2"><br>
							<button style="margin-top: 5%;" class="save pull-right create_icon" id='save_button' >Save</button>
							<button style="margin-top: 5%; display:none" id='edit_button' value='' class="save pull-right">Edit</button>
						</div>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body">
			<table class="boder table table-condensed" id="table_data" width="100%">
				<thead>
					<tr class="blue-bg">
						<th class="padding_left">Date</th>
						<th>Staff NAme</th>
						<th>Staff Code</th>
						<th>Remark</th>
						<th>Amount</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div> 
</section>
<script type="text/javascript">
	var table_data=$('#table_data').dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Hr/BasicPay_Table_ajax",
			"type": "POST",
			"dataSrc": "records",
			data:function( d ) {
				d.staff_id= $('#staff_id').val();
				d.date= '01-'+$('#date').val();
			},
		},
		"columns": [
		{ "data" : "BasicPay.wef_date" },
		{ "data" : "Staff.name" },
		{ "data" : "Staff.code" },
		{ "data" : "BasicPay.remarks" },
		{ "data" : "BasicPay.amount" },
		{ "data" : "BasicPay.action" },
		],
		"columnDefs": [
		],
	});
	$('#staff_id,#date').change(function(){
		table_data.fnDraw();
	});
</script>
<script type="text/javascript">
	$('input').click(function(){
		$(this).select();
	});
	$('#date').inputmask('99-9999',{placeholder:"mm/yyyy"});
	$(".datepicker_month").datepicker( {
		format: "mm-yyyy",
		viewMode: "months", 
		minViewMode: "months"
	});
</script>