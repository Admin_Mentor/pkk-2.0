<section class="content-header">
	<h1> Bonus Configuration </h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?= $this->Form->create('BasicPay', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'BasicPay_Form']); ?>
			<div class="col-md-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
							<?= $this->Form->input('date',array('value'=>date('m-Y'),'type'=>'text','class'=>'form-control pull-right datepicker_month','id'=>'date','required','data-inputmask'=>"'alias': 'mm-yyyy'",'data-mask'=>'data-mask','label'=>'Month')); ?>
						</div>
						<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
							<?= $this->Form->input('role_id',array('type'=>'select','class'=>'form-control  select_two_class','id'=>'role_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'required','label'=>'Role')); ?>
						</div>
						<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12" hidden>
							<?= $this->Form->input('staff_id',array('type'=>'select','class'=>'form-control  select_two_class','id'=>'staff_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'required','label'=>'Staff')); ?>
						</div>
						<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
							<?= $this->Form->input('role_wise_bonus',array('class'=>'form-control number','type'=>'text','required','id'=>'role_wise_bonus')); ?>
						</div>
						<div class="col-md-1 col-sm-1 col-lg-1"><br>
							<button id="role_wise_bonus_button" class="btn btn-success">ADD</button>
						</div>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body">
			<input type="hidden" id='table_edit_id' name="">
			<table class="boder table table-condensed" id="table_data" width="100%">
				<thead>
					<tr class="blue-bg">
						<th>Name</th>
						<th>Code</th>
						<th>Role</th>
						<th>Bonus</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div> 
</section>
<script type="text/javascript">
	var table_data=$('#table_data').dataTable({
		"processing": true,
		"serverSide": true,
		"ordering"  : false,
		"searching" : false,
		"ajax": {
			"url": "<?= $this->webroot ?>Hr/Bonus_Table_ajax",
			"type": "POST",
			"dataSrc": "records",
			data:function( d ) {
				d.role_id = $('#role_id').val();
				d.staff_id= $('#staff_id').val();
				d.date    = '01-'+$('#date').val();
				d.table_id= $('#table_edit_id').val();
			},
		},
		"columns": [
		{ "data" : "Staff.name" },
		{ "data" : "Staff.code" },
		{ "data" : "Role.name" },
		{ "data" : "StaffBonus.bonus" },
		{ "data" : "StaffBonus.action" },
		],
		"columnDefs": [
		{ "targets": [ 1 ],'className': "text-right" },
		{ "targets": [ 4 ],'className': "text-right" },
		],
	});
	$('#staff_id,#date,#role_id').change(function(){
		table_data.fnDraw();
	});
</script>
<script type="text/javascript">
	$(document).on('click','.edit_staff',function(){
		var table_id=$(this).attr('table_id');
		$('#table_edit_id').val(table_id);
		table_data.fnDraw();
	});
	$(document).on('click','.ok_staff',function(){
		var data={
			staff_id:$(this).attr('staff_id'),
			month:$(this).attr('month'),
			bonus:$('#staff_bonus').val()
		};
		var url="<?=$this->webroot;?>Hr/Bonus_Update_Function";
		$.post(url,data,function(response){
			if(response.result!='success') { alert(response.result); return false; }
			$('#table_edit_id').val(0);
			table_data.fnDraw();
		},"json")
	});
	$(document).on('click','#role_wise_bonus_button',function(){
		if(!$('#role_id').val()){ $('#role_id').select2('open'); return false; }
	    if(!$('#role_wise_bonus').val()){ $('#role_wise_bonus').focus(); return false; }
		var data={
			bonus  :$('#role_wise_bonus').val(),
			month  :'01-'+$('#date').val(),
			role_id:$('#role_id').val(),
		};
		var url="<?=$this->webroot;?>Hr/RoleWiseBonus_Update_Function";
		$.post(url,data,function(response){
			if(response.result!='success') { alert(response.result); return false; }
		$('#role_wise_bonus').val('');
			table_data.fnDraw();
		},"json")
	});
    $('#role_id').on("select2:select", function(e) {
		$('#role_wise_bonus').select();
    });
	$('input').click(function(){
		$(this).select();
	});
	$('#date').inputmask('99-9999',{placeholder:"mm/yyyy"});
	$(".datepicker_month").datepicker( {
		format: "mm-yyyy",
		viewMode: "months", 
		minViewMode: "months"
	});
</script>