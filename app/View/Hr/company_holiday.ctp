<style type="text/css">
input[type="checkbox"]{
	width: 20px;
	height: 20px;
}
th, td { white-space: nowrap; }
div.dataTables_wrapper {
	width: 100%;
	margin: 0 auto;
}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css">
<link rel="stylesheet" href="<?= $this->webroot; ?>css/switchery.css" />
<script src="<?= $this->webroot; ?>js/switchery.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<section class="content-header">
	<h1> Company Holiday Management </h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<div class="row">
				<div class="col-md-2">
					<?= $this->Form->input('holiday_date',array('value'=>date('m-Y'),'type'=>'text','class'=>'form-control pull-right datepicker_month','id'=>'holiday_date','required','data-inputmask'=>"'alias': 'mm-yyyy'",'data-mask'=>'data-mask','label'=>'Month')); ?>
				</div>
				<div class="col-md-2" style="margin-top: 4px"><br>
					<button type="button" id="holiday_get_button" class="btn btn-success">Get</button>
				</div>
				<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
					<h4 class="text-right"><b style="color:#005082">Hd:Holiday</b></h4>
				</div>
				<div class="col-md-2">
					<?= $this->Form->input('special_holiday',array('value'=>date('d-m-Y'),'type'=>'text','class'=>'form-control pull-right datepicker','id'=>'special_holiday','data-inputmask'=>"'alias': 'mm-yyyy'",'data-mask'=>'data-mask')); ?>
				</div>
				<div class="col-md-2" style="margin-top: 4px"><br>
					<button type="button" id="special_holiday_button" class="btn btn-success">Save</button>
				</div>
			</div>
		</div>
		<div class="box-body">
			<input type="hidden" id="holiday_table_edit_id">
			<table class="boder table table-condensed table-bordered" id="CompanyHoliday_table_data" width="100%">
				<thead>
					<tr class="blue-bg">
						<th>Name</th>
						<th>Month</th>
						<th>Holidays</th>
						<th>Action</th>
						<?php for ($i=1; $i <=31 ; $i++) : ?>
							<th class="text-center"><?= $i; ?></th>
						<?php endfor; ?>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</section>
<script type="text/javascript">
	var CompanyHoliday_table_data=$('#CompanyHoliday_table_data').dataTable({
		"processing"    : true,
		"serverSide"    : true,
		"ordering"      : false,
		"paging"        : false,
		"bInfo"         : false,
		"searching"     : false,
		"scrollX"       : true,
		"scrollCollapse": true,
		"fixedColumns"  : {
			leftColumns: 4,
		},
		"ajax": {
			"url": "<?= $this->webroot ?>Hr/CompanyHolidayTable",
			"type": "POST",
			"dataSrc": "records",
			data:function( d ) {
				d.month    = '01-'+$('#holiday_date').val();
				d.table_id= $('#holiday_table_edit_id').val();
			},
		},
		"columns": [
		{ "data" : "Staff.name"},
		{ "data" : "Staff.month" },
		{ "data" : "Staff.worked_days" },
		{ "data" : "Staff.action" },
		<?php for ($i=1; $i <=31 ; $i++) : ?>
		{ "data" : "Staff.holiday_"+<?= $i ?> },
		<?php endfor; ?>
		],
		"columnDefs": [
		{ "targets": [ 2 ],'className': "text-right" },
		],
	});
	$('#holiday_date').change(function(){
		CompanyHoliday_table_data.fnDraw();
	});
	$('#holiday_get_button').click(function(){
		CompanyHoliday_table_data.fnDraw();
	});
</script>
<script type="text/javascript">
	$(document).on('click','.edit_holiday',function(){
		var table_id=$(this).attr('table_id');
		$('#holiday_table_edit_id').val(table_id);
		CompanyHoliday_table_data.fnDraw();
	});
	$(document).on('click','.ok_holiday',function(){
		var data={
			staff_id  :$(this).attr('staff_id'),
			month     :$(this).attr('month'),
			holidays:$("#CompanyHoliday_table_data input:checkbox:checked").map(function(){
				return $(this).val();
			}).get(),
		};
		var url="<?=$this->webroot;?>Hr/CompanyHoliday_Update_Function";
		$.post(url,data,function(response){
			if(response.result!='success') { alert(response.result); return false; }
			$('#holiday_table_edit_id').val(0);
			CompanyHoliday_table_data.fnDraw();
		},"json")
	});
	$('#special_holiday_button').click(function(){
		var data={
			date:$('#special_holiday').val()
		}
		var url="<?=$this->webroot;?>Hr/SpecialHoliday_Store_Function";
		$.post(url,data,function(response){
			if(response.result!='success') { alert(response.result); return false; }
			CompanyHoliday_table_data.fnDraw();
		},"json")
	});
	$('#month').inputmask('99-9999',{placeholder:"mm/yyyy"});
	$(".datepicker_month").datepicker( {
		format: "mm-yyyy",
		viewMode: "months", 
		minViewMode: "months"
	});
</script>