<style type="text/css">
input[type="checkbox"]{
	width: 20px;
	height: 20px;
}
th, td { white-space: nowrap; }
div.dataTables_wrapper {
	width: 100%;
	margin: 0 auto;
}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css">
<link rel="stylesheet" href="<?= $this->webroot; ?>css/switchery.css" />
<script src="<?= $this->webroot; ?>js/switchery.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
<style type="text/css">
.switchery {
	width: 85px !important;
	border: 2px solid black;
}
.switchery:before {
	content: 'No';
	color: #dd4b39;
	position: absolute;
	left:40px;
	top: 50%;
	transform: translateY(-50%) translateX(-10%);
}
.js-switch:checked + .switchery:before {
	color: #00a65a;
	content: 'Yes';
	width:100px;
	left:40px;
}
</style>
<section class="content-header">
	<h1> Attendance Management </h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<div class="row">
				<div class="col-md-2">
					<?= $this->Form->input('day',array('value'=>date('d-m-Y'),'type'=>'text','class'=>'form-control pull-right datepicker','id'=>'day','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Day')); ?>
				</div>
				<div class="col-md-2" style="margin-top: 4px"><br>
					<button type="button" id="freeze_button" class="btn btn-success">Finish</button>
				</div>
			</div>
		</div>
		<div class="box-body">
			<table class="boder table table-condensed table-bordered" id="Attendance_table_data" width="100%">
				<thead>
					<tr class="blue-bg">
						<th>Staff</th>
						<th>Date</th>
						<th>Month</th>
						<th>Day</th>
						<th>Full Day</th>
						<th>Half Day</th>
						<th>HoliDay</th>
						<th>Freeze</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</section>
<script type="text/javascript">
	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
	elems.forEach(function(html) {
		var switchery = new Switchery(html);
	});
</script>
<script type="text/javascript">
	var Attendance_table_data=$('#Attendance_table_data').dataTable({
		"processing": true,
		"serverSide": true,
		"ordering"  : false,
		"paging"    : false,
		"bInfo"     : false,
		"searching" : false,
		"ajax": {
			"url": "<?= $this->webroot ?>Hr/DayAttendanceTable",
			"type": "POST",
			"dataSrc": "records",
			data:function( d ) {
				d.day= $('#day').val();
			},
		},
		"createdRow": function( row, data, dataIndex ) {
			if ( data['Staff']['freeze'] == "freezed" ) 
			{
				$('#freeze_button').hide();
			}
			else
			{
				$('#freeze_button').show();
			}
		},
		"fnDrawCallback": function(settings, json) {
			var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
			elems.forEach(function(html) {
				var switchery = new Switchery(html);
			});
		},
		"columns": [
		{ "data" : "Staff.name"},
		{ "data" : "Staff.day" },
		{ "data" : "Staff.month" },
		{ "data" : "Staff.day_name" },
		{ "data" : "Staff.full_day" },
		{ "data" : "Staff.half_day" },
		{ "data" : "Staff.holiday" },
		{ "data" : "Staff.freeze" },
		],
		"columnDefs": [
		{ "targets": [ 2 ],'className': "text-center" },
		],
	});
	$('#staff_id,#month,#role_id').change(function(){
		table_data.fnDraw();
	});
	$('#day').change(function(){
		Attendance_table_data.fnDraw();
	});
</script>
<script type="text/javascript">
	$('input').click(function(){
		$(this).select();
	});
	$('#month').inputmask('99-9999',{placeholder:"mm/yyyy"});
	$(".datepicker_month").datepicker( {
		format: "mm-yyyy",
		viewMode: "months", 
		minViewMode: "months"
	});
	$(document).on('change','.full_day_attendance_change',function(){
		var staff_id=$(this).attr('staff_id');
		var month=$(this).attr('month');
		var day=$(this).attr('day');
		var full_day=$(this).prop('checked');
		$.ajax({
			url: '<?= $this->webroot ?>/Hr/FullDayAttendance_Update_Function',
			type: 'post',
			data: {
				staff_id:staff_id,
				month:month,
				day:day,
				full_day:full_day,
			},
			dataType: 'JSON',
			success: function (data) { 
				if(data.result!='success') { alert(data.result); }
				Attendance_table_data.fnDraw();
				table_data.fnDraw();
			}
		}); 
	});
	$(document).on('change','.half_day_attendance_change',function(){
		var staff_id=$(this).attr('staff_id');
		var month=$(this).attr('month');
		var day=$(this).attr('day');
		var half_day=$(this).prop('checked');
		$.ajax({
			url: '<?= $this->webroot ?>/Hr/HalfDayAttendance_Update_Function',
			type: 'post',
			data: {
				staff_id:staff_id,
				month:month,
				day:day,
				half_day:half_day,
			},
			dataType: 'JSON',
			success: function (data) { 
				if(data.result!='success') { alert(data.result); }
				Attendance_table_data.fnDraw();
				table_data.fnDraw();
			}
		}); 
	});
	$('#freeze_button').click(function(){
		var date=$('#day').val();
		var data={
			date:date,
		};
		var url="<?=$this->webroot;?>Hr/Attendance_Freeze_Update_Function";
		$.post(url,data,function(response){
			if(response.result!='success') { alert(response.result); return false; }
			Attendance_table_data.fnDraw();
			table_data.fnDraw();
		},"json")
	});
</script>