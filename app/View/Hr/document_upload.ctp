<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>

<style type="text/css">
    .blue-bg {
        background-color: #0e3d58;
        color: #FFF !important;
        white-space: nowrap;
    }
    .table_with_border_full {
        border: 1px solid #d2d2d2;
    }
    .tbl_top_hed {
        margin: 0px;
        padding-top: 10px;
        padding-bottom: 15px;
    }
    .view_color {
        color: #0e3d58;
    }

    .ADd_Btn {
        margin-top: 26px;
        border: none;
        padding-left: 20px;
        padding-right: 20px;
        letter-spacing: 0.3px;
        background-color: #3aa5a5;
    }

    .btn_sub_mit_2 {
        border: none;
        background-color: #a00000;
        margin-top: 24px;
        letter-spacing: 0.3px;
    }

    .btn_sub_mit_2:hover{
        background-color: #8a0e0e !important;
        transition: ease all 0.9s;
    }

    .btn_sub_mit_2:active{
        background-color: #8a0e0e !important;
    }

    .btn_sub_mit_2:focus{
        background-color: #8a0e0e !important;
    }



    .ADd_Btn:hover{
        background-color: #207f7f !important;
        transition: ease all 0.9s;
    }

    .ADd_Btn:active{
        background-color: #207f7f !important;
    }

    .ADd_Btn:focus{
        background-color: #207f7f !important;
    }

    .bdr_full_cover {
        border: 1px solid #d2d6de;
        margin-top: 15px;
        padding-top: 15px;
        padding-bottom: 15px;
    }

    .Wages_bdr {
        border: 1px solid #d2d6de;
        padding-left: 15px;
        padding-top: 11px;
        margin-bottom: 25px;
    }

    .pdng_10 {
        padding: 10px !important;
    }

</style>
<section class="content-header">
    <h1> Document Upload </h1>

</section>

<section class="content">
<?php echo $this->Form->create('Document', ['type' => 'file','id' => 'Document_Form']); ?>
    <div class="box">
        <div class="row" style="margin-top: 2%;"> 
            <div class="col-md-12 col-lg-12 col-sm-12">
               
                 <div class="col-md-3 col-lg-3 col-sm-3">
                <div class="col-md-10 col-sm-10 col-lg-10">
                    <div class="form-group">
                        <label>Document Type</label>
                         <?= $this->Form->input('document_type', array('class' => 'form-control select_two_class', 'type' => 'select', 'required', 'style' => 'width:100%', 'id' => 'document_type', 'label' => false,'options'=>$DocumentTypeList)); ?>
                    </div>
                </div>
                    <div class="col-md-2 col-sm-2 col-lg-2" style="margin-top: 24px;">
                        <a href="#"> <i class="fa fa-plus-circle fa-2x plus-btn mdl_clr_blu" data-toggle="modal" data-target="#document_type_add"></i></a>
                      </div>
                </div>
                 <div class="col-md-2 col-lg-2 col-sm-2 common">
                    <div class="form-group">
                            <label id="Staff">Staff name</label><label id="Vehicle" style="display:none">Vehicle Number</label>
                         <?= $this->Form->input('id', array('class' => 'form-control', 'type' => 'hidden', 'required', 'style' => 'width:100%', 'id' => 'id', 'label' => false,)); ?>
                        <?= $this->Form->input('name', array('class' => 'form-control select_two_class', 'type' => 'select', 'required', 'style' => 'width:100%', 'id' => 'name','options'=>$staffList,'empty'=>'SELECT','label' => false,)); ?>
                    </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2">
                    <div class="form-group">
                        <label>Expiry Date</label>
                         <?= $this->Form->input('expiry_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'expiry_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask', 'label' => false,'required')); ?>       
                    </div>
                </div>
                 <div class="col-md-2 col-lg-2 col-sm-2">
                    <div class="form-group">
              <?php echo $this->Form->input('remarks',array('type'=>'textarea','id'=>'remarks','class'=>'form-control','label'=>'Remarks','rows'=>2)); ?>
              </div>
              </div>
                 <div class="col-md-2 col-lg-2 col-sm-2">
                    <div class="form-group">
                        <label>Document</label>
                        <?= $this->Form->input('document_file', array('class' => 'form-control', 'type' => 'file', 'style' => 'width:100%', 'id' => 'document_file', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-1">
                    <div class="form-group"> 
                        <button class="btn btn-success btn_sub_mit_2" id="Upload_document">Save</button>
                     <button  class="btn btn-success" style="margin-top: 20%; display:none" id='edit_button'>Update</button>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row" style="margin-top: 2%;">
            <div class="col-md-12 col-lg-12 col-sm-12">
                  <div class="box-body table-responsive ">
             <table class="table table-condensed table boder table-bordered" id="table_data">
                       <thead>
                            <tr class="blue-bg">
                                <th>Sl No.</th>
                                <th>Staff Name/Vehicle No.</th>
                                <th>Document Type</th>
                                <th>Expiry Date</th>
                                <th>Remaining Days</th>
                                <th>Documents</th>
                                <th>Action</th>
                            </tr>
                       </thead>                         
                        <tbody>
                    <!--  <?php $i=0; foreach($uploadList as $key=>$value): $i++;?>
                            <tr>
                                <td><?= $i; ?></td>
                                <td><?= $value['name'];?><span class="staff_id" style="display: none;"><?= $value['id'];?></span></td>
                                <td class="type_name"><?= $value['document_type_id'];?></td>
                                <td class=""><?=$value['expiry_date'];?></td>
                                <td class="file_name"><?= $value['uploaded_file'];?></td>
                                <td>
                                <a href="<?php echo Router::url('/', true).'profile/'?><?= $value['uploaded_file'];?>" download="<?= $value['uploaded_file'];?>"><i class="fa fa-2x fa-download" aria-hidden="true"></i><a/>
                                <a href='#'><i data-id="<?= $value['id'];  ?>" class="fa fa-2x fa-edit document_edit" aria-hidden="true"></i><a/></td>
                            </tr>
                        <?php endforeach; ?>  -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <?php $this->Form->end(); ?> 
</section>

<div id="document_type_add" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Document Type</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12 col-lg-12 col-sm-12">
          <div class="col-md-12 col-lg-12 col-sm-12">
              <div class="form-group">
                <label>Name</label>
                <?= $this->Form->input('type_name', array('class' => 'form-control', 'type' => 'text',  'style' => 'width:100%', 'options' => '', 'id' => 'type_name', 'label' => false,)); ?>
              </div>
            </div>
          </div>
           <div class="col-md-12 col-lg-12 col-sm-12">
          <div class="col-md-12 col-lg-12 col-sm-12">
              <div class="form-group">
                <label>Type</label>
               <?= $this->Form->input('type', array('class' => 'form-control select_two_class', 'type' => 'select', 'style' => 'width:100%', 'id' => 'type','options'=>[1=>"Staff",2=>"Vehicle",3=>"Common"],'empty'=>'SELECT','label' => false,)); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="btn_document_type">Save</button>
      </div>
    </div>
  </div>
</div>
<div id="view_image" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="document_name">My Document</h4>
            </div>
            <div class="modal-body">
                <?= $this->Form->create('Image',['id' => 'Image_Form']); ?>
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                        <div class="col-md-6 col-lg-6 col-sm-6">
                            <div class="form-group" id="image_modal">
                               <!-- <img id="plan_modal" src="#" alt="your image" /> -->
                           </div>
                       </div>
                   </div>
               </div>
               <?= $this->Form->end(); ?>
           </div>
       </div>
   </div>
</div>

<script type="text/javascript">
$('#table_data').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Hr/StaffDocument_table_ajax",
      "type": "POST",
      data:function( d ) {

      },
      "dataSrc": "records",
    },
    dom: 'Bfrtip',
      lengthMenu: [
    [10, 25, 50,-1],
    ['10 rows', '25 rows', '50 rows','show all']
    ],
    buttons: [
   // 'colvis',
    //{ extend: 'excel', title: 'Stock Report', exportOptions: { columns: ':visible' } }, 
    { extend: 'excel',   title: 'Document', exportOptions: { columns: [1,2,3,4,5] } },
    'pageLength',
    ],
    "columns": [
    { "data" : "StaffDocument.id" },
    { "data" : "StaffDocument.name" },
    { "data" : "StaffDocument.DocumentType" },
    { "data" : "StaffDocument.date" },
    {"data" : "StaffDocument.due_days" },
    {"data" : "StaffDocument.uploaded_file" },
    { "data" : "StaffDocument.action" },
    ],
    "columnDefs": [
    ],
    "createdRow": function( row, data, dataIndex){
        $(row).css('background-color',data.StaffDocument.colour);
        // $(row).css('background-color',data.StaffDocument.due_colour);
        //$(row).css('color','white');
      },
  });
    $('#btn_document_type').on('click',function(){
        var name=$('#type_name').val();
        var type=$('#type').val();
        if(!name)
        {
           $('#type_name').focus(); 
            return false;
        }
        if(!type)
        {
           $('#type').select2('open'); 
            return false;
        }
    var data={type:type,name:name};  
    $.post( "<?= $this->webroot ?>Hr/document_type_add_ajax",data ,function( data ) {
        if(data.result!='Success')
        {
            alert(data.result);
            return false;
        }
        $('#type_name').val("");
        $('#document_type').append($("<option></option>").attr("value",data.key).text(data.value));
        $('#document_type_add').modal('toggle');
        $('#document_type').val(data.key);
        $('#document_type').trigger('change');
    }, "json");
});
    $('.image_view').click(function(){
       var file_path=$(this).closest('tr').find('td.file_name').text();
       var document_name=$(this).closest('tr').find('td.type_name').text();
       $('#view_image').modal('toggle');
       $('#document_name').html(file_path);
       if(file_path!="")
       {
       var path="<?php echo Router::url('/', true).'profile/'?>"+file_path;
       $('#image_modal').html('<center><embed src="'+path+'" width="50" height="50"></img></center>');
      }
   });
    $(document).on('change','#document_type',function()
    {
        var document_type=$(this).val();
        var data={
            document_type:document_type,
        }

        var url_address="<?= $this->webroot;?>Hr/get_details_by_document_type";
        $.ajax({
            type:'POST',
            dataType:'Json',
            data:data,
            url:url_address,
            success:function(data)
            {
        if(data.document_type==1)
        {
        $('#Staff').css('display','');
        $('#Vehicle').css('display','none');
        $('.common').css('display','');
        $("#name").prop('required',true);
        }
        else if(data.document_type==2)
        {
        $('#Staff').css('display','none');
        $('#Vehicle').css('display','');
        $('.common').css('display','');
        $("#name").prop('required',true);
        }
        else
        {
        $('#Staff').css('display','none');
        $('#Vehicle').css('display','none');
        $('.common').css('display','none');
        $("#name").prop('required',false);
        }
           $('#name').html('');
           $('#name').append($("<option></option>").attr("value","").text("SELECT"))
            $.each(data.option,function(i,value)
            {
                $('#name').append($("<option></option>").attr("value",i).text(value));
            });
            }

        });
    });
    $(document).on('click','.document_edit',function(){
    var document_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Hr/get_document_details_ajax/"+document_id,function( data ) {
        $('#document_type').val(data.StaffDocument.document_type_id).trigger('change.select2');
       $('#name').val(data.StaffDocument.staff_id).trigger('change.select2');
        $('#remarks').val(data.StaffDocument.remarks);
        if(data.StaffDocument.document_type_id==1)
        {
        $('#Staff').css('display','');
        $('#Vehicle').css('display','none');
        $('.common').css('display','');
        $("#name").prop('required',true);
        }
         else if(data.StaffDocument.document_type_id==2)
        {
        $('#Staff').css('display','none');
        $('#Vehicle').css('display','');
        $('.common').css('display','');
        $("#name").prop('required',true);
        }
        else
        {
        $('#Staff').css('display','none');
        $('#Vehicle').css('display','none');
        $('.common').css('display','none');
        $("#name").prop('required',false);
        }
       $('#expiry_date').val(data.StaffDocument.expiry_date);
       $('#Upload_document').css('display','none');
      $('#edit_button').css('display','');
        $("#id").append("<input type='text' id='document_id' name='data[Document][id]' value='"+data.StaffDocument.id+"'>");
      $('#Document_Form').attr('action','<?= $this->webroot; ?>Hr/EditStaffDocument');
 }, "json");
});
</script>