<style type="text/css">
  <?php include "style.css" ?>
</style>

<section class="content-header">
  <h1> Executive Evaluation </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="Stockmanagement">
          <br>
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-4 col-lg-4 col-sm-4">
                <div class="box-body">
                  <div class="col-sm-8">
                    <div class="form-group">
                      <?php echo $this->Form->input('executive_id',array('type'=>'select','empty' =>'ALL','options'=>$Executive,'class'=>'form-control select2 evaluation-search','style'=>'width:100%','label'=>'Executive','id'=>'executive_id',)) ?>
                    </div>
                  </div>
                  <br>
                  <div class="col-md-4">
                    
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-4 col-lg-4">
                <div class="box-body">
                  <div class="col-sm-8">
                    <div class="form-group">
                    <?php 
                    $date = date('m.Y');
                    ?>
                      <!-- <label>Date</label><input type="text"  datepicker data-date-format="yyyy.MM"> -->
                      <?php echo $this->Form->input('month',array('type'=>'text','class'=>'form-control pull-right datepicker_month evaluation-search','id'=>'date','data-inputmask'=>"'alias': 'mm-yyyy'",'data-mask'=>'data-mask','value'=> $date)); ?>
                    </div>
                  </div>
                  
                </div>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                <div class="box-body">
                  <div class="col-sm-8">
                    <div class="form-group">
                       <?php echo $this->Form->input('performance_id',array('type'=>'select','empty' =>'ALL','options'=>array('1'=>'Below','2'=>'Above'),'class'=>'form-control select2 evaluation-search','style'=>'width:100%','label'=>'Performance','id'=>'performance_id')) ;?>
                    </div>
                  </div>
                  <br>
                  <div class="col-sm-4">
                    
                  </div>
                </div>
              </div>
              <div class="pd-tp-10">
                <div class="col-md-1">
                  <br>
                  <a href="#" id="showallbutton"> <i class="fa fa-refresh fa-2x ad-mar tp_6px"></i></a>
                </div>
              </div>
            </div>
          </div> 
          
        </div>
        <div class="row">
          <div class="col-md-offset-10 col-sm-12 col-xs-12">
            <h3 class="total-amt"> Sale : <span id='main_total_cost'><?php echo $totalSale; ?></span></h3>
          </div>
        </div>
        <hr/>
        <div class="inn_tabl_wrap">
          <div class="box-body">
            <table class="table table-condensed datatable text-center table-bordered" id='table_target_list' data-page-length='100'>
              <thead>
                <tr class="blue-bg">
                <th>Executive</th>
                  <th>Sale Target</th>
                  <th>Collection Target</th>
                  <th>Sale</th>
                   <th>Collection</th>
                      <th>Bonus Eligibility</th> 
                    <th>Calculated Bonus</th>
                  <th>Bonus</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($evaluationList as $key => $value) : ?>
                  <tr class="blue-pd flash">
                  <td class="executive_name"><?php echo $value['executive_name']; ?></td>
                    <td><?php echo $value['target']; ?></td>
                     <td><?php echo $value['collection_target']; ?></td>
                    
                    <td class="cost"><?php echo $value['sale_amount']; ?></td>
                    <td class="collection_cost"><?php echo $value['collection_total_amount']; ?></td>
                    <?php if($value['bonus_eligible']==1){
                     ?><td><span class="glyphicon glyphicon-ok"></span></td>
<?php
                    }
                    else
                      {
                        ?><td><span class="glyphicon glyphicon-remove"></span></td>
<?php
                      }?>
                    <td class="calculated_bonus" data-id="<?php echo $value['bonus_id']; ?>"><?php echo $value['calculated_bonus_amount']; ?></td>
                      <td class="bonus" data-id="<?php echo $value['bonus_id']; ?>"><?php echo $value['bonus_amount']; ?></td>
                   <td data-id="<?php echo $value['executive_id']; ?>" class='Edit'><a><i class="fa fa-edit"></i></a></td>
                    
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div id="Edit_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Bonus</h4>
            </div>
            <?php echo $this->Form->create('EditBonus'); ?>
            <div class="modal-body">
              <div class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Executive</label>
                  <div class="col-sm-5">
                    <input type='text' readonly=true value='' id='executive_name' class='form-control'>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-5">
                    <?php echo $this->Form->input('EditExecutiveId',array('type'=>'hidden','class'=>'form-control','label'=>false,'type'=>'hidden','id'=>'EditExecutiveId')) ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Bonus</label>
                  <div class="col-sm-5">
                    <input type='text' readonly=true value='' id="bonus_value" class='form-control'>
                    <?php echo $this->Form->input('id',array('type'=>'hidden','class'=>'form-control','label'=>false,'type'=>'hidden')) ?>
                  </div>
                </div>
                <br>
                <div class="form-group">
                  <label class="col-sm-4 control-label final_quant add_test_label">New Bonus</label>
                  <div class="col-sm-5">
                    <?php echo $this->Form->input('bonus_amount_new',array('type'=>'text','min'=>'0','class'=>'form-control Edit_stock1 final_quant add_test_label number_field','label'=>false,'id'=>'bonus_amount_new','value'=>'0')) ?>
                  </div>
                </div>
               
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" id="editstock" class="btn btn-primary" data-dismiss="modal" >Assign Bonus</button>
            </div>
            <?php echo $this->Form->end(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>



<script type="text/javascript">
  var product_configuration_type ='<?= $Profile['Profile']['product_configuration_type']; ?>';
  if(product_configuration_type=='Retail')
  {
    $('#whole_sales_price_field').css('display','none');
  }
  if(product_configuration_type=='WholeSale')
  {
    $('#retaile_price_field').css('display','none');
  }
</script>
<script type="text/javascript">
  $(document).on('change','.evaluation-search',function(){
     $.fn.table_search();
  });
$('#executive_id').on('change',function(){
  $.fn.table_search();
})
  $(document).on('click','.delete_stock',function(){
    if(!confirm("Are you sure?"))
    {
      return false;
    }
    var id=$(this).closest('tr').find('td.hidden_field span:eq(1)').text();
    var rowindex = $(this).closest('tr').index();
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/delete_stock_ajax/'+id;
    $.ajax({
      type: "post",
      url:url_address,
      dataType:'json',
      success: function(response) {
        if(response.result=='Success')
        {
          $('#table_stock_list tbody tr:eq(' + rowindex + ')').remove();
        }
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  });
  $('#table_target_list tbody').on( 'click', '.Edit', function () {
    var executive_id=$(this).data('id');
    var date = $('#date').val();
    // alert(date)
    var bonus_amount=$(this).closest('tr').find('td.bonus').text();
    var bonus_id=$(this).closest('tr').find('td.bonus').data('id');
    // var quantity=$(this).closest('tr').find('td.Quantity').text();
    var executive_name=$(this).closest('tr').find('td.executive_name').text();
    $('#EditExecutiveId').val(executive_id);
    $('#executive_name').val(executive_name);
    $('#bonus_value').val(bonus_amount);
    $('#EditBonusId').val(bonus_id);
    $('#Edit_modal').modal('show');
  });
  $('#editstock').click(function(){


     var executive_id = $('#EditExecutiveId').val();
    var bonus_amount_new = $('#bonus_amount_new').val();
   var table_id = $('#EditBonusId').val();
   var date = $('#date').val();

    if(bonus_amount_new=='')
    {
      $('#bonus_amount_new').focus().trigger('click');
      return false;
    }
    var data=$('#EditStockIndexForm').serialize();
    var url_address= '<?php echo $this->webroot; ?>'+'Hr/AssignBonus';
    $.ajax({
      type: "post",
      url:url_address,
      data: {
        'executive_id':executive_id,
        'bonus_amount_new': bonus_amount_new,
        'id':table_id,
        'month':date
      },
      dataType:'json',
      success: function(response) {
        if(response.result=='Success')
        {
          $('#table_target_list tbody tr').each(function(){
            var id=$(this).closest('tr').find('td.Edit').data('id');
            if(executive_id==id)
            {
              $(this).closest('tr').find('td.bonus').attr('data-id' , response.bonus_id);
              $(this).closest('tr').find('td.bonus').text(bonus_amount_new);
              // $(this).closest('tr').find('td.bonus').data('id');
              // var cost=$(this).closest('tr').find('td.cost').text();
              // $(this).closest('tr').find('td.total_cost').text(qty*cost);
              $(this).closest('tr').attr('style','font-size:20px;color:green');
              $(this).closest('tr').attr('class','blue-pd flash');
              setTimeout(timer, 2000);
            }
          });
          $('#EditBonusEvaluationForm')[0].reset();
        }
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  });
  function timer()
  {
    $('#table_target_list tbody tr').each(function(){
      $(this).closest('tr').attr('style','');
      $(this).closest('tr').attr('class','blue-pd');
    });
    // $('#product_type_id').change();
  }
  $.fn.table_search=function(){
    var executive_id=$('#executive_id option:selected').val();
    var performance_id=$('#performance_id option:selected').val();
   var month=$('#date ').val();
    var data={
      executive_id:executive_id,
      month : month,
      performance_id: performance_id
        };
    var url_address= '<?php echo $this->webroot; ?>'+'Hr/evaluation_search_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {
        $('#table_target_list').DataTable().destroy();
        $('#table_target_list tbody').html(response.row);
        $('#main_total_cost').text($.fn.indian_number_format(response.totalSale));
        if(response.result=='Success')
        {
          // $('#main_total_cost').text($.fn.indian_number_format(response.total_cost));
        }
        else
        {
          // $('#main_total_cost').text('0');
        }
        $('#table_target_list').DataTable();
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  }
  $('#showallbutton').click(function(){
    $('input[type="search"]').val('');
    $('select').val('').trigger('change');
    $.fn.table_search();
  });
  $('#Print').click(function(){
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/print_report';
    $(location).attr("href", url_address);
  });
 
   
</script>
<script type="text/javascript">
  $('#date').inputmask('99.9999',{placeholder:"mm/yyyy"});
  $(".datepicker_month").datepicker( {
    format: "mm-yyyy",
    viewMode: "months", 
    minViewMode: "months"
});


</script>