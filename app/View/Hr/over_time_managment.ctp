<section class="content-header">
	<h1>Over Time Management </h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?php echo $this->Form->create('OverTime', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'OverTime_Form']); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
						<div class="form-group">
							<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
								<?php echo $this->Form->input('date',array('value'=>date('d-m-Y'),'type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
							</div>
							<div class="col-md-8 col-lg-8 col-sm-8 col-xs-8">
								<?php echo $this->Form->input('staff_id',array('type'=>'select','class'=>'form-control select_two_class','style'=>'width: 100%;','id'=>'staff_id','required','label'=>'Staff',)); ?>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
								<?= $this->Form->input('amount',array('class'=>'form-control number','type'=>'text','required','id'=>'amount',)); ?>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
								<?= $this->Form->input('remarks',array('type'=>'text','class'=>'form-control','id'=>'remarks','label'=>'Remarks')) ?>
							</div>
						</div>
					</div>
					<div class="col-md-1 col-sm-1 col-lg-1"> 
						<div class="create-wrapper"><br>
							<button type="button" id="add_button" class="user_add_btn create_icon">ADD</button>
						</div>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<div class="box-body table-responsive no-padding">
						<table class="boder table table-condensed boder table-bordered" id="table_data">
							<thead>
								<tr class="blue-bg">
									<th>#</th>
									<th style="text-align: left">Date</th>
									<th style="text-align: left">Name</th>
									<th style="text-align: left">Remarks</th>
									<th style="text-align: right">Amount</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div> 
</section>
<?php require('general_journal_transaction_modal.php'); ?>
<script type="text/javascript">
	table_data=$('#table_data').dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Hr/OverTimeTable",
			"type": "POST",
			"dataSrc": "records",
			data:function( d ) {
				d.staff_id= $('#staff_id').val();
			},
		},
		dom: 'Bfrtip',
		buttons: [
		{ extend: 'csv'  ,text: 'CSV'},
		{ extend: 'excel',text: 'Excel' ,title:'Salary Summary',},
		{ extend: 'pdf'  ,text: 'Pdf'   ,title:'Salary Summary',},
		],
		"columns": [
		{ "data" : "OverTime.id"},
		{ "data" : "OverTime.date" },
		{ "data" : "Staff.name" },
		{ "data" : "OverTime.remarks" },
		{ "data" : "OverTime.amount",'className': "text-right"},
		{ "data" : "OverTime.action" },
		],
		"columnDefs": [
		{ "targets": [ 0 ],'className': "id" },
		{ "targets": [ 2 ],'width':'50%','className': "text-center" },
		{ "targets": [ 1 ],'className': "text-right" },
		{ "targets": [ 3 ],'className': "text-right" },
		],
	});
	$('#staff_id').change(function(){
		table_data.fnDraw();
	});
	$('#staff_id').on("select2:select", function(e) {
		$('#amount').select();
	});
	$('#add_button').click(function(){
		if(!$('#amount').val() || $('#amount').val()==0) { $('#amount').select(); return false; }
		var data=$('#OverTime_Form').serialize();
		var url="<?=$this->webroot;?>Hr/OverTime_add_Function";
		$.post(url,data,function(response){
			if(response.result!='success') { alert(response.result); return false; }
			$('#OverTime_Form input[name="data[OverTime][remarks]"]').val('');
			$('#OverTime_Form input[name="data[OverTime][amount]"]').val(0).select();
			table_data.fnDraw();
		},"json");
	});
	$(document).on('click', '.delete_overtime', function() {
		if (!confirm("Are you sure?")) {return false; }
		var table_id = $(this).attr('table_id');
		var url="<?=$this->webroot;?>Hr/OverTime_delete_Function/"+table_id;
		$.get(url,function(response){
			if(response.result!='success') { alert(response.result); return false; }
			table_data.fnDraw();
		},"json");
	});
</script>
