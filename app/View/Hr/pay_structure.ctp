<section class="content-header">
  <h1> Pay Structure </h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-header">
      <form id="pay_form" name="pay_form" method="post" action="<?= $this->webroot?>Hr/PayStructure">
        <div class="form-group">
          <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="col-md-1 col-lg-1 col-sm-1">
              <?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date_of_pay','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1">
              <?= $this->Form->input('month',array('type'=>'text','class'=>'form-control pull-right datepicker_month','id'=>'date','data-inputmask'=>"'alias': 'mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2">
              <?= $this->Form->input('staff_id',array('type'=>'select','empty' =>'Select','class'=>'form-control select_two_class','style'=>'width:100%','label'=>'Staff','id'=>'staff_id',)) ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2">
              <label>Basic Salary</label>
              <input id="basic_salary" name="basic_salary" readonly type="text" class="form-control number" style="width: 100%">
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1">
              <label>Bonus</label>
              <input id="bonus" name="bonus" type="text" class="form-control number calculation_field " style="width: 100%">
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1">
              <label>Allowance</label>
              <input id="allowance" name="allowance" value="0" type="text" class="form-control number calculation_field" style="width: 100%">
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1">
              <label>Deduction</label>
              <input id="deduction" name="deduction" value="0" type="text" class="form-control number calculation_field" style="width: 100%">
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2">
              <label>Net Salary</label>
              <input id="net_salary" readonly="" name="net_salary" type="text" class="form-control number">
            </div>
            <div class="col-md-1 col-lg-1 col-sm-1"><br>
              <button  id="save" type="submit" disabled="" class="btn btn-success create_icon pull-right">Save</button>
            </div>
          </div>
          <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="form-group">
              <div class="form-group">
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                  <?= $this->Form->input('actual_working_days',array('type'=>'text','class'=>'form-control number working_days_calculator','id'=>'actual_working_days','readonly','value'=>0)) ?>
                </div>
                <div class="col-md-1 col-lg-1 col-sm-1 col-xs-1">
                  <?= $this->Form->input('no_leave_days',array('type'=>'text','class'=>'form-control number working_days_calculator','id'=>'no_leave_days','label'=>'LeaveDays','value'=>0)) ?>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
                  <?= $this->Form->input('salary_deduction',array('type'=>'text','class'=>'form-control number working_days_calculator','id'=>'salary_deduction','label'=>'Salary Deduction','value'=>0)) ?>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                  <?= $this->Form->input('over_time',array('class'=>'form-control number','type'=>'text','required','id'=>'over_time','readonly')); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
    <div class="box-body">
      <table class="table table-condensed text-center table-bordered" id='table_data'>
        <thead>
          <tr class="blue-bg">
            <th>Staff</th>
            <th>Date</th>
            <th>Month Salary</th>
            <th>Basic</th>
            <th>Bonus</th>
            <th>Allowance</th>
            <th>Salary Deduction</th>
            <th>Deduction</th>
            <th>Net Salary</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</section>
<script type="text/javascript">
  var table_data=$('#table_data').dataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Hr/PayStructure_Table_ajax",
      "type": "POST",
      data:function(d){
        d.staff_id= $('#staff_id').val();
        d.month= $('#date').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "Staff.name" },
    { "data" : "PayStructure.date_of_pay" },
    { "data" : "PayStructure.month_year" },
    { "data" : "PayStructure.basic_salary" },
    { "data" : "PayStructure.bonus" },
    { "data" : "PayStructure.allowance" },
    { "data" : "PayStructure.salary_deduction" },
    { "data" : "PayStructure.deduction" },
    { "data" : "PayStructure.net_salary" },
    { "data" : "PayStructure.action" },
    ],
    "columnDefs": [
    { "targets": [ 3 ],'className': "text-right" },
    { "targets": [ 4 ],'className': "text-right" },
    { "targets": [ 5 ],'className': "text-right" },
    { "targets": [ 6 ],'className': "text-right" },
    { "targets": [ 7 ],'className': "text-right" },
    { "targets": [ 8 ],'className': "text-right" },
    ],
  });
  $('#staff_id,#date').change(function(){
    table_data.fnDraw();
  });
</script>
<script type="text/javascript">
  $('.calculation_field').on('keyup',function(){
    var allowance = $('#allowance').val() ? $('#allowance').val() :0;
    $('#allowance').val(allowance);
    var bonus = $('#bonus').val() ? $('#bonus').val() :0;
    $('#bonus').val(bonus);
    var salary_deduction = $('#salary_deduction').val() ? $('#salary_deduction').val() :0;
    $('#salary_deduction').val(salary_deduction);
    var deduction = $('#deduction').val() ? $('#deduction').val() :0;
    var over_time = $('#over_time').val() ? $('#over_time').val() :0;
    $('#deduction').val(deduction);
    var basic_salary = $('#basic_salary').val() ? $('#basic_salary').val() :0;
    var netsalary=parseFloat(basic_salary)+parseFloat(allowance)+parseFloat(bonus)-parseFloat(salary_deduction)-parseFloat(deduction)+parseFloat(over_time);
    $('#net_salary').val(parseFloat(netsalary.toFixed(2)));
  });
  $('#date').inputmask('99-9999',{placeholder:"mm/yyyy"});
  $(".datepicker_month").datepicker( {
    format: "mm-yyyy",
    viewMode: "months", 
    minViewMode: "months",
  });
  $('#date').change(function(){
    $('#staff_id').change();
  });
  $(document).on('change','#staff_id',function(){
    var Staff_id = $(this).val();
    var month = $('#date').val();
    $('#basic_salary').val(0);
    $('#bonus').val(0);
    $('#allowance').val(0);
    $('#deduction').val(0);
    $('#net_salary').val(0);
    if(!Staff_id){
      $.alert('choose Staff', {title:'Flash Message',type: 'error',position: ['top-right', [60, 0]],});
      $('#staff_id').select2('open'); return false;
    }
    if(!month){
      $.alert('choose Date', {title:'Flash Message',type: 'error',position: ['top-right', [60, 0]],});
      $('#date').focus(); return false;
    }
    $.ajax({
      url: '<?= $this->webroot ?>Hr/get_staff_pay_ajax',
      dataType: 'json',
      method: 'post',
      data:  {
        'Staff_id':Staff_id,
        'month':month
      },
    }).done(function(response) {
      $('.alert').fadeOut('fast');
      if(response.status != 'Success') {
        $.alert(response.status, {title:'Flash Message',type: 'error',position: ['top-right', [60, 0]],});
        $('#staff_id').select2('open'); 
        $('#save').attr('disabled',true);
        return false;
      }
      $('#basic_salary').val(response.basic_pay_amount);
      $('#over_time').val(response.over_time);
      $('#no_leave_days').val(response.leave_days);
      $('#bonus').val(response.bonus);
      $('#actual_working_days').val(response.no_of_working_days).keyup();
      $('.calculation_field').keyup();
      $('#save').attr('disabled',false);
    }).fail(function(jqXHR) {
      if (jqXHR.status == 403) {
        window.location = '/';
      } else {
        console.log(jqXHR);
      }
    });
  });
  $('.working_days_calculator').keyup(function(){
    var basic_salary=$('#basic_salary').val();
    var actual_working_days=$('#actual_working_days').val();
    var no_leave_days=$('#no_leave_days').val();
    var per_day_salary=parseFloat(basic_salary/actual_working_days).toFixed(2);
    var salary_deduction=no_leave_days*per_day_salary;
    if(salary_deduction>basic_salary)
    {
      salary_deduction=basic_salary;
    }
    salary_deduction=Math.round(salary_deduction,2);
    $('#salary_deduction').val(salary_deduction);
     $('.calculation_field').keyup();
  });
</script>
<script type="text/javascript">
  $('input').click(function(){
    $(this).select();
  });
</script>