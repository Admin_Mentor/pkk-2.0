<section class="content-header">
	<h1>Salary Outstanding Payment </h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
						<div class="form-group">
							<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
								<?= $this->Form->input('date',array('value'=>date('d-m-Y'),'type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
							</div>
							<div class="col-md-8 col-lg-8 col-sm-8 col-xs-8">
								<?= $this->Form->input('account_head_id',array('type'=>'select','class'=>'form-control select_two_class','style'=>'width: 100%;','id'=>'account_head_id','required','label'=>'Staff',)); ?>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
								<?= $this->Form->input('amount',array('class'=>'form-control','type'=>'text','required','id'=>'amount',)); ?>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
								<?= $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select_two_class','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id='mode_field'>
								<?= $this->Form->input('mode',array('type'=>'select','class'=>'form-control select_two_class','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
							</div>
						</div>
					</div>
					<div class="col-md-1 col-sm-1 col-lg-1"> 
						<div class="create-wrapper"><br>
							<button class="user_add_btn create_icon">PAY</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
								<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<div class="box-body table-responsive no-padding">
						<table class="boder table table-condensed boder" id="table_data">
							<thead>
								<tr class="blue-bg">
									<th style="text-align:center">Name</th>
									<th style="text-align:center">Total</th>
									<th style="text-align:center">Paid</th>
									<th style="text-align:center">Balance</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div> 
</section>
<?php require('general_journal_transaction_modal.php'); ?>
<script type="text/javascript">
	table_data=$('#table_data').dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Hr/Salary_Table_ajax",
			"type": "POST",
			"dataSrc": "records",
		},
		dom: 'Bfrtip',
		buttons: [
		{ extend: 'csv'  ,text: 'CSV'},
		{ extend: 'excel',text: 'Excel' ,title:'Salary Summary',},
		{ extend: 'pdf'  ,text: 'Pdf'   ,title:'Salary Summary',},
		],
		"columns": [
		{ "data" : "Staff.name" },
		{ "data" : "Staff.credit" },
		{ "data" : "Staff.debit" },
		{ "data" : "Staff.balance" },
		],
		"columnDefs": [
		{ "targets": [ 0 ],'className': "text-center"},
		{ "targets": [ 1 ],'className': "text-right" },
		{ "targets": [ 2 ],'className': "text-right" },
		{ "targets": [ 3 ],'className': "text-right" },
		],
	});
	$(document).on('keypress','#amount',function(e){
     if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57 || e.which==47)) {
       return false;
     }
   })
	$('button[type="Submit"]').click(function(){
  var amount=$('#amount').val();
  if(amount==0)
  {
        $('#amount').focus();
        return false;
  }
});
	$('#modal_from_date').val("<?=$from_date;?>");
$('#modal_to_date').val("<?=$to_date;?>");
</script>