<section class="content-header">
	<h1> Sale Target </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="row-wrapper">
					<div class="row">
						<div class="col-md-12">
							<div class="row">
								<?php echo $this->Form->create('SaleTarget', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'SaleTarget_Form']); ?>
								<div class="box-body">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'WEF Date')); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('executive_id',array('type'=>'select','class'=>'form-control  select2','id'=>'type','style'=>'width: 100%;','id'=>'executive_id','empty'=>[''=>'Select'],'options'=>$Executive,'required','label'=>'Executive')); ?>
													</div>
												</div>
											</div>
											<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?= $this->Form->input('sale_target',array('class'=>'form-control','type'=>'number','step'=>'any','required','id'=>'target',)); ?>
													</div>
												</div>
											</div>
											<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?= $this->Form->input('collection_target',array('class'=>'form-control','type'=>'number','step'=>'any','required','id'=>'collection_target',)); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
													</div>
												</div>
											</div>
											
										</div>
									</div>
									<div class="col-md-12">
										<div class="row">
											
											<div class="col-md-6 col-lg-3 col-sm-3 col-xs-12">
												
											</div>
											
											<div class="col-md-6 col-sm-6 col-sm-12"> 
												<button style="margin-top: 5%;" class="save pull-right" id='save_button' >Save</button>
          <button style="margin-top: 5%; display:none" id='edit_button' value='' class="save pull-right">Edit</button>


											</div>
										</div>
									</div>
								</div>
								<?= $this->Form->end(); ?>
							</div>
							<div class="row-wrapper">
								<div class="row">
									<div class="col-md-12">
										<div class="box-body table-responsive no-padding">
											<table class="boder table table-condensed table datatable boder" id="myTable">
												<thead>
													<tr class="blue-bg">
														<th class="padding_left">Date</th>
														<th>Executive</th>
														<th>Sale Target</th>
														<th>Collection Target</th>
														<th>Actions</th>
													</tr>
												</thead>
												<tbody>
													<?php $total=0; foreach ($Saletarget as $key => $value): ?>
													<tr class="blue-pddng view_transaction">
														<td><?= date('d-m-Y',strtotime($value['SaleTarget']['wef_date'])); ?></td>
														<td class='name'><?= $value['Executive']['name']; ?></td>
														<td><?= $value['SaleTarget']['target'] ?></td>
														<td><?= $value['SaleTarget']['collection_target'] ?></td>
														<td>
														<a class="edit_target" data-id="<?= $value['SaleTarget']['id']?>" href="#"><i class="fa fa-edit " aria-hidden="true"  style="color: #3c8dbc;"></i></a>
														<a onclick="return confirm('Are you sure?')" href="<?= $this->webroot; ?>Hr/DeleteSaleTarget/<?= $value['SaleTarget']['id']; ?>"><i class="fa fa-trash blue-col blue-col"></i></a>
														</td>
													</tr>
												<?php endforeach ?>
											</tbody>
											
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
	</div>
</div>
</section>

<script type="text/javascript">
	$(document).on('click','.edit_target',function(){
    var target_id = $(this).data('id');
    $.post( "<?= $this->webroot ?>Hr/get_target_details_ajax/"+target_id,function( data ) {
    	var dateAr = data.SaleTarget.wef_date.split('-');
    	var date = dateAr[2] + '-' + dateAr[1] + '-' + dateAr[0].slice(-2);
    	$('#executive_id').val(data.SaleTarget.executive_id).trigger('change');
      $('#target').val(data.SaleTarget.target);
        $('#collection_target').val(data.SaleTarget.collection_target);
      $('#remarks').val(data.SaleTarget.remarks);
      $('#date').val(date);
      
      $('#save_button').css('display','none');
      $('#edit_button').css('display','');
      $("#executive_id").append("<input type='text' id='target_id' name='data[SaleTarget][id]' value='"+data.SaleTarget.id+"'>");
      $('#SaleTarget_Form').attr('action','<?= $this->webroot; ?>Hr/EditSaleTarget');
    }, "json");
  });
</script>
<script type="text/javascript">
	
</script>