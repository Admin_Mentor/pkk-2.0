<section class="content-header">
  <h1>Manage Staff</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <?= $this->Form->create('Staff', array('id'=>'Staff_Form'));?>
      <?= $this->Form->hidden('id',['id'=>'staff_id']); ?>
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
          <div class="col-md-2 col-lg-2 col-sm-2">
            <div class="form-group">
              <?= $this->Form->input('name',array('type'=>'text','id'=>'name','class'=>'form-control','required'=>'required')); ?>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2">
            <div class="form-group">
              <?= $this->Form->input('contact_no',array('type'=>'text','id'=>'contact_no','label'=>'Contact No','class'=>'form-control','required'=>'required')); ?>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2">
              <div class="form-group">
                <?= $this->Form->input('date_of_joining',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>date('d-m-Y'))); ?>
              </div>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2">
              <div class="form-group">
                <?= $this->Form->input('no_of_working_days',array('type'=>'text','id'=>'no_of_working_days','class'=>'form-control number','label'=>'WorkingDays','required'=>'required','value'=>0)); ?>
              </div>
            </div>
          <div class="col-md-2 col-lg-2 col-sm-2">
            <div class="form-group">
              <?= $this->Form->input('role_id',array('type'=>'select','class'=>'form-control  select_two_class','id'=>'role_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'required')); ?>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2">
            <div class="form-group">
              <?= $this->Form->input('payment_mode',array('type'=>'select','class'=>'form-control  select_two_class','id'=>'payment_mode','style'=>'width: 100%;','options'=>['Salary'=>'Salary','Wages'=>'Wages'],'required')); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
          <div class="col-md-2 col-lg-2 col-sm-2" hidden>
            <div class="form-group">
              <?= $this->Form->input('experience',array('type'=>'text','id'=>'experience','class'=>'form-control number','required'=>'required')); ?>
            </div>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2">
            <div class="form-group">
              <?= $this->Form->input('grade',array('type'=>'select','class'=>'form-control  select_two_class','id'=>'grade','style'=>'width: 100%;','empty'=>[''=>'Select'],'options'=>['A'=>'A','B'=>'B','C'=>'C','D'=>'D'],)); ?>
            </div>
          </div>
          <div class="col-md-6 col-lg-6 col-sm-6">
            <div class="form-group">
              <?= $this->Form->input('address',array('class'=>'form-control','type'=>'textarea','rows'=>2,'id'=>'address','label'=>'Address',)); ?>
            </div>
          </div>
          <div class="col-md-1 col-lg-1 col-sm-1">
            <button type="button" class="btn btn-success" id='save_button' style="margin-top: 25px;">Save</button>
          </div>
        </div>
      </div>
      <?= $this->Form->end(); ?>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover boder table-bordered" id='table_data'>
              <thead>
                <tr class="blue-bg">
                  <th>#</th>
                  <th>Name</th>
                  <th>Code</th>
                  <th>Role</th>
                  <th>Contact No</th>
                  <th>WorkingDays</th>
                  <th>Grade</th>
                  <th>Payment Mode</th>
                  <th>Date of Joining</th>
                  <th>Address</th>
                  <th>Bonus</th>
                  <th>Paid</th>
                  <th>Outstanding</th>
                  <th>PrePaid</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require('general_journal_transaction_modal.php'); ?>
<script type="text/javascript">
  table_data=$('#table_data').dataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Hr/Staff_Table_ajax",
      "type": "POST",
      "dataSrc": "records",
      data:function(d){
        d.role_id     = $('#role_id').val();
         d.grade     = $('#grade').val();
        d.payment_mode= $('#payment_mode').val();
      }
    },
    dom: 'Bfrtip',
    buttons: [
    'colvis',
    { extend: 'csv'  ,text: 'CSV'},
    { extend: 'excel',text: 'Excel' ,title:'Salary Summary',},
    ],
    "columns": [
    { "data" : "Staff.id" ,'visible':false},
    { "data" : "Staff.name" },
    { "data" : "Staff.code" },
    { "data" : "Role.name" },
    { "data" : "Staff.contact_no" },
    { "data" : "Staff.no_of_working_days" },
     { "data" : "Staff.grade" },
    { "data" : "Staff.payment_mode" },
    { "data" : "Staff.date_of_joining",'visible':false },
    { "data" : "Staff.address" },
    { "data" : "Bonus.name" ,'visible':false },
    { "data" : "Paid.name" ,'visible':false },
    { "data" : "Outstanding.name" ,'visible':false },
    { "data" : "PrePaid.name" ,'visible':false },
    { "data" : "Staff.action"},
    ],
    "columnDefs": [
    //{ "targets": [ 5 ],'className': "text-right" },
    //{ "targets": [ 6 ],'className': "text-right" },
    ],
  });
  $('#role_id,#payment_mode,#grade').change(function(){
    table_data.fnDraw();
  });
</script>
<script type="text/javascript">
  $('input').click(function(){
    $(this).select();
  });
  $('#save_button').click(function(){
    var data=$('#Staff_Form').serialize();
    var url_address= "<?= $this->webroot; ?>Hr/staff_add_ajax";
    $.post( url_address,data, function( response ) {
      if(response.result!='success') { alert(response.result); return false; }
      $('#Staff_Form')[0].reset();
      $('#role_id').val('').change();
      $('#grade').val('').change();
         $('#role_id').prop('disabled',false);
      $('#date').val('<?= date('d-m-Y'); ?>');
      table_data.fnDraw();
    }, "json");
  });
  $(document).on('click','.delete_staff',function(){
    if(!confirm('Are You Sure')) { return false; }
    id=$(this).attr('table_id');
    var url_address= "<?= $this->webroot; ?>Hr/Delete_Staff_ajax/"+id;
    $.get( url_address, function( response ) {
      if(response.result!='Success') { alert(response.result); return false; }
      table_data.fnDraw();
    }, "json");
  });
  $(document).on('click','.edit_staff',function(){
    id=$(this).attr('table_id');
    var url_address= "<?= $this->webroot; ?>Hr/Get_Staff_ajax/"+id;
    $.get( url_address, function( response ) {
      $('#staff_id').val(response.Staff.id);
      $('#name').val(response.Staff.name);
      $('#grade').val(response.Staff.grade).change();
      $('#date').val(response.Staff.date_of_joining);
      $('#contact_no').val(response.Staff.contact_no);
      $('#no_of_working_days').val(response.Staff.no_of_working_days);
      $('#experience').val(response.Staff.experience);
      $('#role_id').val(response.Staff.role_id).change();
      $('#role_id').prop('disabled',true);
      $('#payment_mode').val(response.Staff.payment_mode).change();
      $('#address').val(response.Staff.address);
    }, "json");
  });
</script>
