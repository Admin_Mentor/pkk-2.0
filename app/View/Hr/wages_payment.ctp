<section class="content-header">
	<h1>Wages Payment </h1>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<?= $this->Form->create('Journal', ['class'=>'form-horizontal','style'=>'margin-top: 15px;','id'=>'Journal_Form']); ?>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-5 col-lg-5 col-sm-12 col-xs-12">
						<div class="form-group">
							<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
								<?= $this->Form->input('date',array('value'=>date('d-m-Y',strtotime('-1 month')),'type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
							</div>
							<div class="col-md-8 col-lg-8 col-sm-8 col-xs-8">
								<?= $this->Form->input('account_head_id',array('type'=>'select','class'=>'form-control select_two_class','style'=>'width: 100%;','empty'=>'Select Staff','id'=>'account_head_id','required','label'=>'Staff',)); ?>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
								<?= $this->Form->input('amount',array('class'=>'form-control','type'=>'text','required','id'=>'amount',)); ?>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
								<?= $this->Form->input('mode_catagory',array('type'=>'select','class'=>'form-control select_two_class','id'=>'mode_catagory','style'=>'width: 100%;','options'=>$mode_catagory,'required',)); ?>
							</div>
						</div>
					</div>
					<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
						<div class="form-group">
							<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id='mode_field'>
								<?= $this->Form->input('mode',array('type'=>'select','class'=>'form-control select_two_class','id'=>'mode','style'=>'width: 100%;','options'=>$Mode,'required',)); ?>
							</div>
						</div>
					</div>
					<div class="col-md-1 col-sm-1 col-lg-1"> 
						<div class="create-wrapper"><br>
							<button class="user_add_btn create_icon">PAY</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
						<div class="form-group">
							<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
								<?= $this->Form->input('wages',array('type'=>'text','class'=>'form-control number wages_calculator','id'=>'wages','readonly','value'=>0)) ?>
							</div>
							<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
								<?= $this->Form->input('no_of_working_days',array('type'=>'text','class'=>'form-control number wages_calculator','id'=>'no_of_working_days','label'=>'WorkingDays','value'=>1)) ?>
							</div>
							<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
								<?= $this->Form->input('total_wages',array('type'=>'text','class'=>'form-control number wages_calculator','id'=>'total_wages','label'=>'Total Wages')) ?>
							</div>
						</div>
					</div>
					<div class="col-md-8 col-lg-8 col-sm-8 col-xs-8">
						<div class="col-md-2 col-sm-2 col-lg-2"> 
							<div class="create-wrapper">
								<?= $this->Form->input('balance',array('type'=>'text','class'=>'form-control number','id'=>'balance','value'=>0,'label'=>'Balance','readonly')) ?>
							</div>
						</div>
						<div class="col-md-2 col-sm-2 col-lg-2"> 
							<div class="create-wrapper">
								<?= $this->Form->input('over_time',array('type'=>'text','class'=>'form-control number','id'=>'over_time','value'=>0,'label'=>'Over Time')) ?>
							</div>
						</div>
						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
							<div class="form-group">
								<?= $this->Form->input('remarks',array('type'=>'textarea','class'=>'form-control','rows'=>'1','id'=>'remarks','label'=>'Remarks')) ?>
							</div>
						</div>
						<div class="col-md-2 col-sm-2 col-lg-2"> 
							<div class="create-wrapper"><br>
								<button type="button" id="Multiple_Entry_Modal_button" class="btn btn-primary">Multiple Entry</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?= $this->Form->end(); ?>
			<div id="Multiple_Entry_Modal" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Multiple Entry Form</h4>
						</div>
						<div class="modal-body">
							<div class="row">
								<div class="col-md-12 col-xs-12">
									<?= $this->Form->create('JournalMultipleForm',['id'=>'JournalMultipleForm','url'=>'JournalMultipleForm']); ?>
									<div class="form-group">
										<?= $this->Form->hidden('account_head_id',array('id'=>'multiple_account_head_id')) ?>
										<?= $this->Form->hidden('mode',array('id'=>'multiple_mode')) ?>
										<?= $this->Form->hidden('remarks',array('id'=>'multiple_remarks')) ?>
										<table class="boder table table-condensed boder" width="100%" id="multiple_table_data">
											<thead>
												<tr class="blue-bg">
													<th>Date</th>
													<th>Amount</th>
													<th>Bonus</th>
													<th>Action</th>
												</tr>
												<tr>
													<th><?= $this->Form->input('dates',array('type'=>'text','value'=>date('d-m-Y'),'class'=>'form-control datepicker','id'=>'mutiple_date','data-inputmask'=>"'alias': 'dd-mm-yyyy'" ,'data-mask'=>"data-mask",'label'=>false)); ?></th>
													<th><?= $this->Form->input('multiple_amount',array('type'=>'text','class'=>'form-control number','id'=>'multiple_amount','style'=>'width:100%','label'=>false)); ?></th>
													<th><?= $this->Form->input('multiple_bonus',array('type'=>'text','class'=>'form-control number','id'=>'multiple_bonus','style'=>'width:100%','label'=>false)); ?></th>
													<th><i class="fa fa-plus-circle fa-2x plus-btn" id='multiple_data_add_button'></i></th>
												</tr>
											</thead>
											<tbody>
											</tbody>
											<tfoot>
												<tr>
													<th colspan="4"><button type="button" class="btn btn-success pull-right" id="save_multiple">Save Multiple</button></th>
												</tr>
											</tfoot>
										</table>
									</div>
									<?php $this->Form->end(); ?>
								</div>
							</div>
						</div>
						<div class="modal-footer">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<div class="box-body table-responsive no-padding">
						<table class="boder table table-condensed boder" width="100%" id="table_data">
							<thead>
								<tr class="blue-bg">
									<th>#</th>
									<th style="text-align: left">Name</th>
									<th>Code</th>
									<th>Role</th>
									<th style="text-align: left">Salary</th>
									<th style="text-align: left">Bonus</th>
									<th style="text-align: left">Total</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
							<tfoot>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div> 
</section>
<?php require('general_journal_transaction_modal.php'); ?>
<script type="text/javascript">
	table_data=$('#table_data').DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Hr/Wages_Table_ajax",
			"type": "POST",
			"dataSrc": "records",
		},
		dom: 'Bfrtip',
		buttons: [
		{ extend: 'csv'  ,text: 'CSV'},
		{ extend: 'excel',text: 'Excel' ,title:'Salary Summary',},
		{ extend: 'pdf'  ,text: 'Pdf'   ,title:'Salary Summary',},
		],
		"columns": [
		{ "data" : "Staff.paid_account_head_id" },
		{ "data" : "Staff.name" },
		{ "data" : "Staff.code" },
		{ "data" : "Role.name" },
		{ "data" : "Staff.salary" },
		{ "data" : "Staff.bonus" },
		{ "data" : "Staff.total" },
		],
		"columnDefs": [
		{ "targets": [ 0 ],'className': "id" },
		{ "targets": [ 1 ],'className': "name text-right",'width':'50%' },
		{ "targets": [ 4 ],'className': "text-right" },
		{ "targets": [ 5 ],'className': "text-right" },
		{ "targets": [ 6 ],'className': "text-right" },
		],
	});
	$('#table_data tbody').on('click','.table_click',function(){
		var id  =$(this).attr('table_id');
		var type=$(this).attr('table_type');
		var name=$(this).closest('tr').find('td.name').text();
		var from_date=$('#modal_from_date').val();
		var to_date=$('#modal_to_date').val();
		$('#account_holder_name').text(name+' : '+type);
		$('#account_holder_id').text(id);
		var data_array={
			name:id,from_date:from_date,to_date:to_date
		}
		$.fn.search_transaction_ajax(data_array);
		$('#view_transaction_modal').modal('show');
	});
	$('#account_head_id').change(function(){
		var account_head_id=$(this).val();
		if(!account_head_id) return false;
		var url_address= "<?= $this->webroot; ?>Hr/get_Wages_PayStructure/"+account_head_id;
		$.get( url_address, function( response ) {
			if(response.result!='success') { alert(response.result); return false; }
			$('#wages').val(response.amount).keyup();
			$('#balance').val(response.balance);
		}, "json");
	});
	$('.wages_calculator').keyup(function(){
		var wages=$('#wages').val();
		var no_of_working_days=$('#no_of_working_days').val();
		var total_wages=wages*no_of_working_days;
		$('#total_wages').val(total_wages);
		$('#amount').val(total_wages);
	});
	$('#Multiple_Entry_Modal_button').click(function(){
		var account_head_id=$('#account_head_id').val();
		if (!account_head_id) {
			$('#account_head_id').select2('open'); return false;
		}
		$('#Multiple_Entry_Modal').modal('show');
		$('#multiple_amount').select();
	});
	$('#mutiple_date').change(function(){
		$('#multiple_amount').select();
	});
	$('#multiple_amount,#multiple_bonus').keypress(function(e){
		var keyCode = e.keyCode || e.which;
		if (keyCode === 13) { 
			$('#multiple_data_add_button').click(); return false;
		}
	});
	multiple_table_data = $('#multiple_table_data').DataTable({
		'searching': false,
		'ordering' : false,
		'bInfo'    : false,
		'paging'   : false,
	});
	$('#multiple_data_add_button').on('click', function() {
		var mutiple_date    = $('#mutiple_date').val();
		var multiple_bonus = $('#multiple_bonus').val();
		var multiple_amount = $('#multiple_amount').val();
		if (!multiple_amount || multiple_amount<=0) {$('#multiple_amount').select(); return false; }
		if (!mutiple_date) {$('#mutiple_date').click(); return false; }
		multiple_table_data.row.add([
			'<input type="text" class="form-control"        readonly name="JournalMultipleForm[date][]"   value="' + mutiple_date + '"  style="width:100%">',
			'<input type="text" class="form-control number" readonly name="JournalMultipleForm[amount][]" value="' + multiple_amount + '" style="width:100%" >',
			'<input type="text" class="form-control number" readonly name="JournalMultipleForm[bonus][]"  value="' + multiple_bonus + '"  style="width:100%">',
			'<button type="button" class="btn btn-primary btn-sm multiple_minus_button"><i class="fa fa-minus"></i></button>',
			]).draw(false);
		$('#multiple_bonus').val('0');
		$('#mutiple_date').select();
	});
	$('#multiple_table_data').on('click', '.multiple_minus_button', function() {
		multiple_table_data.row($(this).parents('tr')).remove().draw();
	});
	$('#save_multiple').click(function(){
		var account_head_id=$('#account_head_id').val();
		var mode=$('#mode').val();
		var remarks=$('#remarks').val();
		if (!account_head_id) {
			$('#account_head_id').select2('open'); return false;
		}
		$('#multiple_account_head_id').val(account_head_id);
		$('#multiple_mode').val(mode);
		$('#multiple_remarks').val(remarks);
		var data=$('#JournalMultipleForm').serialize();
		var url_address= "<?= $this->webroot; ?>Hr/save_multiple_amount_ajax";
		$.post( url_address,data, function( response ) {
			if(response.result!='success') { alert(response.result); return false; }
			multiple_table_data.clear().draw();
			$('#multiple_amount').val(0);
			$('#Multiple_Entry_Modal').modal('hide');
			table_data.draw();
		}, "json");
	});
	$(document).on('keypress','#amount',function(e){
     if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57 || e.which==47)) {
       return false;
     }
   })
$(document).on('click','.user_add_btn',function(){
  var amount=$('#amount').val();
  if(amount==0)
  {
        $('#amount').focus();
        return false;
  }
});
</script>
