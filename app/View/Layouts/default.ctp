		<?php
		$cakeDescription = __d('cake_dev', 'Idrees');
		$cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
		?>
		<!DOCTYPE html>
		<html>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<head>
			<?php echo $this->Html->charset(); ?>
			<title>

			</title>
			<?php
			echo $this->Html->meta('icon');
			echo $this->Html->css(array(
				'bootstrap.min',
				'main',
				'new_sheet',
				'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css',
				'https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css',
				'https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css',
				'login',
				'iCresp-Dashboard',
				'AdminLTE.min',
				'datepicker3',
				'notifIt',
				'select2',
				'dataTables.bootstrap',
				'jquery.dataTables.min',
				'bootstrap',
				'new_custom',
				'new_style'
				));
			echo $this->Html->script(array(
				'https://code.jquery.com/jquery-2.2.4.js',
				'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js',
				'https://code.jquery.com/jquery-1.12.4.js',
				'https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js',
				'https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js',
				'https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js',
				'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js',
				'https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js',
				'https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js',
				'bootstrap.min',
				'app.min',
				'bootstrap-datepicker',
				'jquery.inputmask',
				'jquery.inputmask.date.extensions',
				'notifIt',
				'bootstrap-flash-alert',
				'select2.full.min',
				'jquery.dataTables',
				'dataTables.bootstrap.min',
				'shortcuts'
				));
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
			?>
			<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap2-toggle.min.css" rel="stylesheet">
			<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
			<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
			<style type="text/css">

			body{
				font-family: 'Roboto Condensed', sans-serif;
			}
			</style>
			<script type="text/javascript">
			$.fn.indian_number_format=function(number)
			{
				number=number.toString();
				number_ceil=Math.ceil(number);
				var flag_number=0;
				if(number_ceil != number)
				{
					flag_number=1;
					number_split = number.split(".");
					var integer_part=number_split[0];
					var fraction_part=number_split[1];
					number=integer_part;
				}
				var lastThree = number.substring(number.length-3);
				var otherNumbers = number.substring(0,number.length-3);
				if(otherNumbers != '')
					lastThree = ',' + lastThree;
				var result = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
				if(flag_number==1)
				{
					result=result+'.'+fraction_part.substring(0,2);
				}
				return result;
			};
			</script>
		</head>

		<body class="hold-transition skin-black  sidebar-mini">
			<div class="wrapper">
				<header class="main-header">

					<a href="<?= $this->webroot ?>Homes/Dashboard" class="logo">

						<span class="logo-mini">
							<i class="fa fa-home i_home_color" aria-hidden="true"></i>
						</span>

						<span class="logo-lg hidden-sm"><b>Icresp</b></span> <span class="logo-lg hidden-xs"></span> </a>
						<nav class="navbar navbar-static-top">

							<div class="hidden-xs cstm_header_nav_top">
								<?php if(isset($moduleaction['Inventory'])){ ?>
									<ul class="nav navbar-nav" >
										<li class="dropdown mega-dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">Inventory<span class="fa fa-chevron-down pull-right chev_rght_pdng"></span></a>
											<ul class="dropdown-menu mega-dropdown-menu row menu_for_inventory">
												<li class="col-sm-6 bdr_drop_right">
													<ul>
														<li class="dropdown-header Main_Head_Color">Stock</li>
														<?php foreach ($moduleaction['Inventory'] as $keyInventory => $valueInventory) : ?>
									<?php if('Stock/Index' == $valueInventory || 'Stock/StationaryIssue' == $valueInventory || 'Product/NegativeStock' == $valueInventory ):?>
										<li class='blue'><a href="<?= $this->webroot.$valueInventory;?>"><i class=""></i> <?=$keyInventory;?></a></li>
									<?php endif; ?>
											<?php endforeach ?>
													</li>
												</ul>
											</li>
											<li class="col-sm-6 bdr_drop_right">
												<ul>
													<li class="dropdown-header Main_Head_Color">Stock Transfer</li>
                                            <?php foreach ($moduleaction['Inventory'] as $keyInventory => $valueInventory) : ?>
									<?php if('Stock/Index' != $valueInventory && 'Stock/StationaryIssue' != $valueInventory && 'Product/NegativeStock' != $valueInventory ):?>
										<li class='blue'><a href="<?= $this->webroot.$valueInventory;?>"><i class=""></i> <?=$keyInventory;?></a></li>
									<?php endif; ?>
											<?php endforeach ?>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							</ul>
							<?php }?>
										 <?php if(isset($moduleaction['Sale'])){ ?>
									<ul class="nav navbar-nav" >
										<li class="dropdown mega-dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sale<span class="fa fa-chevron-down pull-right chev_rght_pdng"></span></a>
											<ul class="dropdown-menu mega-dropdown-menu row menu_for_sale">
												
												<li class="col-sm-8">
													<ul>
														<!-- <li class="dropdown-header Main_Head_Color">Domestic</li> -->
											<?php foreach ($moduleaction['Sale'] as $keySale => $valueSale) : ?>
											<li class='blue'><a href="<?= $this->webroot.$valueSale;?>"><i class=""></i> <?=$keySale;?></a></li>
											<?php endforeach ?>
													</li>
												</ul>
											</li>
											
									</ul>
								</li>
							</ul>
							<?php }?>
									<?php if(isset($moduleaction['Purchase'])){ ?>
									<ul class="nav navbar-nav">
										<li class="dropdown mega-dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">Purchase <span class="fa fa-chevron-down pull-right chev_rght_pdng"></span></a>
											<ul class="dropdown-menu mega-dropdown-menu row menu_for_purchase">
												<li class="col-sm-12">
													<ul>
											<?php foreach ($moduleaction['Purchase'] as $keyPurchase => $valuePurchase) : ?>
											<li class='blue'><a href="<?= $this->webroot.$valuePurchase;?>"><i class=""></i> <?=$keyPurchase;?></a></li>
											<?php endforeach ?>
													</ul>
												</li>
											</ul>
										</li>
									</ul>
									<?php }?>
									<?php if(isset($moduleaction['Production'])){ ?>
									<ul class="nav navbar-nav production_flag" >
										<li class="dropdown mega-dropdown">
											<a href="#" class="dropdown-toggle" data-toggle="dropdown">Production<span class="fa fa-chevron-down pull-right chev_rght_pdng"></span></a>
											<ul class="dropdown-menu mega-dropdown-menu row menu_for_production">
												<li class="col-sm-6 bdr_drop_right">
													<ul>
														<li class="dropdown-header Main_Head_Color">Production</li>
														<?php foreach ($moduleaction['Production'] as $keyProduction => $valueProduction) : ?>
									<?php if('ProductionPlan/SemiProductionList' != $valueProduction && 'ProductionPlan/SemiProduction' != $valueProduction && 'ProductionPlan/SemiProductionOrder' != $valueProduction ):?>
										<li class='blue'><a href="<?= $this->webroot.$valueProduction;?>"><i class=""></i> <?=$keyProduction;?></a></li>
									<?php endif; ?>
											<?php endforeach ?>
													</li>
												</ul>
											</li>
											<li class="col-sm-6 bdr_drop_right">
												<ul>
													<li class="dropdown-header Main_Head_Color">Semi Production</li>
                                             <?php foreach ($moduleaction['Production'] as $keyProduction => $valueProduction) : ?>
                                             <?php if('ProductionPlan/SemiProduction' == $valueProduction || 'ProductionPlan/SemiProductionOrder' == $valueProduction ):?>
										<li class='blue'><a href="<?= $this->webroot.$valueProduction;?>"><i class=""></i> <?=$keyProduction;?></a></li>
										<?php endif; ?>
											<?php endforeach ?>
												</li>
											</ul>
										</li>
									</ul>
								</li>
							</ul>
							<?php }?>
							<?php if(isset($moduleaction['Accounts'])){ ?>
							<ul class="nav navbar-nav">
								<li class="dropdown mega-dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Accounts <span class="fa fa-chevron-down pull-right chev_rght_pdng"></span></a>
									<ul class="dropdown-menu mega-dropdown-menu row menu_for_finance">

										<li class="col-sm-6 ">
											<ul> 
									    <?php foreach ($moduleaction['Accounts'] as $keyFinance => $valueFinance) : ?>
									       <?php if('Accounts/Ledger' != $valueFinance && 'Accounts/MainGroup' != $valueFinance && 'Accounts/SubGroup' != $valueFinance ):?>
								<li class='blue'><a href="<?= $this->webroot.$valueFinance;?>"><i class=""></i> <?=$keyFinance;?></a></li>
								<?php endif; ?>
									<?php endforeach ?>
											</ul>
										</li>
								</ul>
							</li>
						</ul>
						<?php }?>
						<?php if(isset($moduleaction['Reports'])){ ?>
						<ul class="nav navbar-nav">
							<li class="dropdown mega-dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Reports <span class="fa fa-chevron-down pull-right chev_rght_pdng"></span></a>
								<ul class="dropdown-menu mega-dropdown-menu row menu_for_Report rprt_bdr">
									<li class="custome_width_ Bdr_For_Menu_Items">
										<ul class="">
											<li class="dropdown-header Main_Head_Color acc_rprt_">Accounts Report</li>
											<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>ProfitLoss/ProfitLossReport"><i class=" "></i> Profit And Loss Account</a></li>
											<!-- <li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/ReportTrialBalance"><i class=" "></i> Trial Balance</a></li> -->
											<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/TrialBalanceNew"><i class=" "></i> Trial Balance</a></li>
											<!-- <li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/BalanceSheet"><i class=" "></i>Balance Sheet</a></li> -->
											<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/BalanceSheetNew"><i class=" "></i>Balance Sheet</a></li>
											<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/DayBook"><i class=" "></i> DayBook</a></li>
											<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Accounts/LedgerReport"><i class=" "></i> Ledger Report</a></li>
											<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Accounts/CustomerLedgerReport"><i class=" "></i>Customer Ledger Report</a></li>
											<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Accounts/CashBook"><i class=" "></i> CashBook</a></li>
											<!-- <li class='blue Sub_Item_Padng' ><a href="<?= $this->webroot; ?>Reports/RouteWiseProfitLossReport"><i class=" "></i> Route Wise Profit And Loss</a></li> -->
		<!-- <li class="dropdown-header Main_Head_Color">Expense Report</li>
			<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/ExpenseReport"><i class=" "></i> Monthly Expense Report</a></li> -->
		</ul>
		<ul class="">
			<li class="dropdown-header Main_Head_Color acc_rprt_">Other Report</li>
			<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/TraySummary"><i class=" "></i>Tray Summary</a></li>
			<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/DailyReport"><i class=" "></i>Daily Report</a></li>
			<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/SaleReturnReport"><i class=" "></i>Return Report</a></li>
			<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/ProductLog"><i class=""></i> Stock Movement Report</a></li>
			<li class='blue Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/BonusApprovalReport"><i class=""></i>Bonus Approval/Reject Report</a></li>
			<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/SaleCollectionReport"><i class=" "></i>Executive Wise Sale Report</a></li>
			<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/SalesReturnCollectionReport"><i class=" "></i>Executive Wise Sales Return Report</a></li>
		</ul>
		</li>
		<li class="custome_width_ Bdr_For_Menu_Items">
			<ul class="">
				<li class="dropdown-header Main_Head_Color acc_rprt_">Sales Report</li>
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/SaleCollectionReportProductwise"><i class=" "></i>Product Wise Sale Report</a></li>
				<li class="Sub_Item_Padng" hidden><a href="<?= $this->webroot; ?>Reports/SaleCollectionReportCustomerWise"><i class=" "></i>Customer Wise Sale Report</a></li>
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/CollectionReportExecutive"><i class=" "></i>Executive/Route Collection Report</a></li>
                
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/CollectionReport"><i class=" "></i>Collection Report</a></li> -->
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/SaleItemWise"><i class=" "></i> Sales Register Itemswise</a></li> -->
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/SaleSummary"><i class=" "></i> Sales Register Summary</a></li> -->
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/AllSaleReport"><i class=" "></i>Route Wise Sale Report</a></li> -->
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/RouteReport"><i class=" "></i> All Route Sale Report</a></li>
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/ProductWise"><i class=" "></i>Sale Report with ProductWise</a></li> -->
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/VATReport"><i class=" "></i> VAT Report</a></li> -->
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/CustomerPerformance"><i class=" "></i> Customer Profit</a></li> -->
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/InvoiceMargin"><i class=" "></i> Invoice Margin Report</a></li> -->
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/DueList"><i class=" "></i> Customer Due List Route Wise</a></li> -->
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/ExecutiveDueList"><i class=" "></i>Customer Due List Executive Wise</a></li> -->
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/SaleOutstanding"><i class=" "></i>Customer Outstanding</a></li>
				<li class="Sub_Item_Padng hidden"><a href="<?= $this->webroot; ?>Reports/ExpenseReport"><i class=" "></i> Monthly Expense Report</a></li>
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/CustomerAgeingReport"><i class=" "></i> Customer Ageing Report</a></li>
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/NosaleCustomerReport"><i class=" "></i> Non Buying Customer Report</a></li>
				<li class="Sub_Item_Padng" hidden><a href="<?= $this->webroot; ?>Reports/DailyPerformanceAnalysis"><i class=" "></i> Daily Performance Analysis</a></li>
			</ul>
		</li>

		<li class="custome_width_ Bdr_For_Menu_Items">
			<ul class="" style="display: none;">
				<li class="dropdown-header Main_Head_Color acc_rprt_">Graphical Report</li>
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/GraphicalReport"><i class=" "></i> GraphicalReport</a></li>
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/Summary"><i class=" "></i> Summary</a></li>

			</ul>
			<ul class="">
				<li class="dropdown-header Main_Head_Color acc_rprt_">Staff Report</li>
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/DayRegisterSummary"><i class=" "></i> Day Register Summary</a></li> -->
				<!-- 	<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/DayRegisterPrint"><i class=" "></i> Executive Day Close Register</a></li> -->
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/DayRegisterPrintRoute"><i class=" "></i> Route Day Close Register</a></li> -->
		<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/DayRegisterPrintOld"><i class=" "></i> Day Close Register Old</a></li>
		-->                          
		<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/BrandWiseReport"><i class=" "></i> Executive Brand Wise Sale Report</a></li> -->
		<li class='Sub_Item_Padng'><a href="<?= $this->webroot; ?>Sale/DayRegister"><i class=" "></i>Executive Day Register</a></li>
		<li class='Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/ExecutiveBonusReport"><i class=" "></i>Executive Bonus Report</a></li>
		<li class='Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/DateWiseBonusReport"><i class=" "></i>Date wise Bonus/Collection Report</a></li>
		<li class='Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/ExecutiveDateWiseBonusReport"><i class=" "></i>Executive wise Bonus/Collection Report</a></li>
		<li class='Sub_Item_Padng'><a href="<?= $this->webroot; ?>Reports/ExecutiveWiseBlockAmountReport"><i class=" "></i>Executive wise Block Amount Report</a></li>
		</ul>

		</li>

		<li class="custome_width_ Bdr_For_Menu_Items">
			<ul>
				<li class="dropdown-header Main_Head_Color acc_rprt_">Purchase Report</li>
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/PurchaseItemWise"><i class=" "></i> Purchase Register Itemswise</a></li>
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/PurchaseSummary"><i class=" "></i> Purchase  Register Summary</a></li>
				<!-- <li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/VATReport"><i class=" "></i> VAT Report</a></li> -->
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/PartyDueList"><i class=" "></i> Party Due List</a></li>
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/SupplierAgeingReport"><i class=" "></i> Supplier Ageing</a></li>
			</ul>
		</li>


		<li class="custome_width_">
			<ul>
				<li class="dropdown-header Main_Head_Color acc_rprt_">Tax Report</li>
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/TaxReportSale"><i class=" "></i> Tax Sale Wise Report</a></li> 
				<li class="Sub_Item_Padng"><a href="<?= $this->webroot; ?>Reports/TaxReport"><i class=" "></i> Tax Purchase Wise Report</a></li> 
				<li class="Sub_Item_Padng" hidden><a href="<?= $this->webroot; ?>Reports/TaxWiseVATReport"><i class=" "></i> Taxwise VAT Report</a></li>
				<li class="Sub_Item_Padng" hidden><a href="<?= $this->webroot; ?>Reports/SalesVATReport"><i class=" "></i> Sales VAT Report</a></li>
				<li class="Sub_Item_Padng" hidden><a href="<?= $this->webroot; ?>Reports/ItemwiseVATReport"><i class=" "></i> Itemwise VAT Report</a></li>

			</ul>
		</li>
		</ul>
		</li>
		</ul>
		<?php }?>
		<?php if(isset($moduleaction['HR'])){ ?>
			<ul class="nav navbar-nav">
				<li class="dropdown mega-dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">HR <span class="fa fa-chevron-down pull-right chev_rght_pdng"></span></a>
					<ul class="dropdown-menu mega-dropdown-menu row menu_for_staff">
						<li class="col-sm-6 ">
							<ul>
								 <?php foreach ($moduleaction['HR'] as $keyHR => $valueHR) : ?>
								<li class='blue'><a href="<?= $this->webroot.$valueHR;?>"><i class=""></i> <?=$keyHR;?></a></li>
								<?php endforeach ?>
		                    </ul>
		                </li>
		           </ul>
		        </li>
		</ul>
		<?php } ?>
		<?php if(isset($moduleaction['Admin'])){ ?>
			<ul class="nav navbar-nav">
				<li class="dropdown mega-dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin <span class="fa fa-chevron-down pull-right chev_rght_pdng"></span></a>
					<ul class="dropdown-menu mega-dropdown-menu row menu_for_admin">
						<li class="col-sm-6 ">
							<ul>
								 <?php foreach ($moduleaction['Admin'] as $keyAdmin => $valueAdmin) : ?>
								<li class='blue'><a href="<?= $this->webroot.$valueAdmin;?>"><i class=""></i> <?=$keyAdmin;?></a></li>
								<?php endforeach ?>
		                    </ul>
		                </li>
		           </ul>
		        </li>
		</ul>
		<?php } ?>
		</div>

		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
			<?php if(isset($moduleaction['Master'])){ ?>
				<li class="dropdown user user-menu hidden-xs"><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-plus-square fa-2x" aria-hidden="true"></i></a>
					<ul class="dropdown-menu mega-dropdown-menu row row_right_Style">
						<li><center><h2 class="Main_Drop_Head"> Master</h2></center></li>
						<li class="col-sm-2 col-md-2 col-lg-2  Bdr_For_Menu_Items">
							<ul class="">
								<li class="dropdown-header head_left_padg Main_Head_Color">Inventory</li>
								<?php foreach ($moduleaction['Master'] as $keyMaster => $valueMaster) : ?>
									<?php if('Product/index'==$valueMaster || 'ProductType/Index'==$valueMaster || 'Brand/Index'==$valueMaster || 'Unit/Index'==$valueMaster || 'Warehouse/index'==$valueMaster):?>
								<li class='blue'><a href="<?= $this->webroot.$valueMaster;?>"><i class=""></i> <?=$keyMaster;?></a></li>
								<?php endif; ?>
									<?php endforeach ?>
							</ul>
						</li>
							<li class="col-sm-2 col-md-2 col-lg-2 Bdr_For_Menu_Items">
								<ul class="">
									<li class="dropdown-header Main_Head_Color">Sale</li>
									<?php foreach ($moduleaction['Master'] as $keyMaster => $valueMaster) : ?>
									<?php if('Route/index'==$valueMaster || 'Division/index'==$valueMaster || 'Branch/AddBranch'==$valueMaster || 'CustomerGroups/index'==$valueMaster || 'CustomerType/index'==$valueMaster || 'Accountings/AssetCurrentAccountRecievable'==$valueMaster):?>
								<li class='blue'><a href="<?= $this->webroot.$valueMaster;?>"><i class=""></i> <?=$keyMaster;?></a></li>
								<?php endif; ?>
									<?php endforeach ?>
								</ul>

							</li>
							<li class="col-sm-2 col-md-2 col-lg-2 Bdr_For_Menu_Items">
								<ul class="">
									<li class="dropdown-header Main_Head_Color">Purchase</li>
									<?php foreach ($moduleaction['Master'] as $keyMaster => $valueMaster) : ?>
									<?php if('Accountings/CurrentLiabilityCrediterSupplier'==$valueMaster || 'Accountings/CurrentLiabilityVendor'==$valueMaster):?>
								<li class='blue'><a href="<?= $this->webroot.$valueMaster;?>"><i class=""></i> <?=$keyMaster;?></a></li>
								<?php endif; ?>
									<?php endforeach ?>
								</ul>
							</li>
							<li class="col-sm-2 col-md-2 col-lg-2 Bdr_For_Menu_Items">
								<ul class="">

									<li class="dropdown-header Main_Head_Color">HR</li>
                                    <?php foreach ($moduleaction['Master'] as $keyMaster => $valueMaster) : ?>
									<?php if('Executive/ExecutiveRate'==$valueMaster ||'Executive/AddSalesman'==$valueMaster || 'Hr/Staff'==$valueMaster || 'Hr/Role'==$valueMaster || 'Hr/DocumentUpload'==$valueMaster || 'Vehicle/AddVehicle'==$valueMaster):?>
								<li class='blue'><a href="<?= $this->webroot.$valueMaster;?>"><i class=""></i> <?=$keyMaster;?></a></li>
								<?php endif; ?>
									<?php endforeach ?>

								</ul>
							</li>
									<?php if(isset($moduleaction['Accounts'])){ ?>
							<li class="col-sm-2 col-md-2 col-lg-2">
								<ul class="">

									<li class="dropdown-header Main_Head_Color">Accounts</li>
                                     <?php foreach ($moduleaction['Accounts'] as $keyFinance => $valueFinance) : ?>
									       <?php if('Accounts/Ledger' == $valueFinance || 'Accounts/MainGroup' == $valueFinance || 'Accounts/SubGroup' == $valueFinance ):?>
								<li class='blue'><a href="<?= $this->webroot.$valueFinance;?>"><i class=""></i> <?=$keyFinance;?></a></li>
								<?php endif; ?>
									<?php endforeach ?>

								</ul>
							</li>
							<?php } ?>
						</ul>
					</li>
					<?php }?>
		<!-- Notifications -->
		<li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <!-- <img src="/icresp_pro/img/logo_new.jpg" class="user-image" alt="User Image"> --> 
			<span class="hidden-xs"><i class="fa fa-bell  fa-2x" aria-hidden="true" ></i><span class="badge"><?=count($document);?></span></span> </a>
			<ul class="dropdown-menu">
			 <li class="header" style="margin-left:13px">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu">
                                	<?php foreach ($document as $keynotifi => $valuenotifi) : ?>
                                    <li>
                                        <a href="<?= $this->webroot; ?>Hr/DocumentUpload">
                                            <div class="menu-info">
                                                <h4><?=$valuenotifi['notification'];?></h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> <?=$valuenotifi['time'];?>
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                    <?php endforeach ?>
                                </ul>
                            </li>
			</ul>
		</li>            
                    <!-- #END# Notifications -->
         <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <!-- <img src="/icresp_pro/img/logo_new.jpg" class="user-image" alt="User Image"> --> 
			<span class="hidden-xs"><i class="fa fa-user fa-2x" aria-hidden="true"></i></span> </a>
			<ul class="dropdown-menu">
				<li class="user-header"> <img src="<?= $this->webroot ?>profile/icresp_logo.png" class="img-circle" alt="User Image">
					<!-- <p>  <small></small> </p> -->
				</li>
				<li class="user-footer">
					<div class="pull-left"> <a href="#" class="btn btn-default btn-flat pass_change" id="change_password_side" data-toggle="modal" data-target="#change_pwd">Change Password</a> </div>
					<div class="pull-right"> <a href="<?= $this->webroot ?>User/logout" class="btn btn-default btn-flat pass_change">Sign out</a> </div>
				</li>
			</ul>
		</li>
		<!-- <li> <a href="#" data-toggle="control-sidebar"><i class="fa fa-bars"></i></a> </li>
		--></ul>
		</div>

		</nav>
		</header>

		<div class="content-wrapperss">
			<section class="content Content_Bg">
				<div align="center"><?php $flash=$this->Session->flash(); ?></div>
				<?php if($flash) : ?>
				<script type="text/javascript">
				var flash='<?= $flash ?>';
				$ .alert($(flash).text(), {title:'Flash Message',type: 'info',position: ['top-right', [60, 0]],});
				</script>
			<?php endif; ?>
			<div class="background_color_change">
				<?php echo $this->fetch('content'); ?>
			</div>
		</section>
		</div>
		<div id="wait" style="display:none;width:2000px;height:2000px;position:absolute;top:50%;left:50%;padding:2px;"><img src='<?php echo $this->webroot; ?>img/spinner_2.gif' width="100" height="100" /><br>Loading..</div>
		</div>
		</section>
		<!-- <aside class="control-sidebar control-sidebar-dark">
		<ul class="nav nav-tabs nav-justified control-sidebar-tabs">
		</ul>
		<div class="tab-content">
		<div class="right-nav">
		<ul>
		<li><a href="#">Link 1 Lorem ipsum dolor</a></li>
		<li><a href="#">Link 2 Lorem ipsum dolor</a></li>
		<li><a href="#">Link 3 Lorem ipsum dolor</a></li>
		<li><a href="#">Link 4 Lorem ipsum dolor</a></li>
		<li><a href="#">Link 5 Lorem ipsum dolor</a></li>
		<li><a href="#">Link 6 Lorem ipsum dolor</a></li>
		<li><a href="#">Link 7 Lorem ipsum dolor</a></li>
		<li><a href="#">Link 8 Lorem ipsum dolor</a></li>
		<li><a href="#">Link 9 Lorem ipsum dolor</a></li>
		<li><a href="#">Link 10 Lorem ipsum dolor</a></li>
		</ul>
		</div>
		</div>
		</aside> -->
		<div class="control-sidebar-bg"></div>
		</div>
		<div class="modal fade" id="change_pwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content modal_class">
					<div class="modal-header header_modal">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Change Password</h4>
					</div>
					<form role=form>
						<div class="modal-body">
							<div class="result">
							</div>
							<div class="body_modal">
								<div class="form-group">
									<div class="row">
										<label class="control-label col-sm-2" for="c_pwd"></label>
										<div class="col-sm-7">
											<input type="password" id="current_pass" placeholder="Current Password" class="form-control disable"  >
											<span id="current_pswd_error" style="color:#db1802" class="help-inline"></span>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<label class="control-label col-sm-2" for="c_pwd"></label>
										<div class="col-sm-7">
											<input type="password" id="new_pass" placeholder="New Password" class="form-control disable"  >
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<label class="control-label col-sm-2" for="c_pwd"></label>
										<div class="col-sm-7">
											<input type="password" id="r_new_pass"   placeholder="Confirm Password" class="form-control disable"  >
											<span id="confirm_pswd_error" style="color:#db1802" class="help-inline"></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pass_cancel" data-dismiss="modal">Cancel</button>
							<button type="button" class="btn btn-success change_pass">Save changes</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="edit_journal_amount_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content modal_class">
					<div class="modal-header header_modal">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Update Journal</h4>
					</div>
					<form role=form>
						<div class="modal-body">
							<div class="result">
							</div>
							<div class="body_modal">
								<div class="form-group">
									<div class="row">
										<label class="control-label col-sm-2">Old Amount</label>
										<div class="col-sm-7">
											<input type="number" id="old_journal_amount" readonly class="form-control">
											<input type="hidden" id="journal_id">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<label class="control-label col-sm-2">New Amount</label>
										<div class="col-sm-7">
											<input type="number" id="new_journal_amount" class="form-control">
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
							<button type="button" id='edit_journal_amount_button' class="btn btn-success">Save changes</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div style="display: none;width:2000px;height:2000px;position:absolute;top:50%;left:50%;padding:2px;" class="loading"><img src='<?php echo $this->webroot; ?>img/spinner_2.gif' width="100" height="100" /><br>Loading</div>
		<script type="text/javascript">
		$(document).ajaxStart(function(){
			$(".loading").css("display", "block");
		});
		$(document).ajaxComplete(function(){
			$(".loading").css("display", "none");
		});
		$(document).ready(function(){
			$('.change_pass').attr('disabled',true);
			$('.disable').on('change keyup blur',function(){
				var current_pass=$('#current_pass').val();
				var new_pass=$('#new_pass').val();
				var r_new_pass=$('#r_new_pass').val();
				if(current_pass!='' && new_pass!='' && r_new_pass!=''){
					$('.change_pass').attr('disabled',false);
				}
			});
			$('#current_pass').blur(function(){
				//alert("pass");
				var current_pass=$(this).val();
				var data={
					current_pass:current_pass
				};
				var url_address= '<?php echo $this->webroot; ?>'+'User/password_check_ajax';
				$.ajax({
					type: "post",
					url:url_address,
					data: data,
					success: function(response) {
						if(response=="No")
						{
							$('#current_pswd_error').html('Current password is not currect');
						}
						else
						{
							$('#current_pswd_error').html('');
						}
					},
					error:function (XMLHttpRequest, textStatus, errorThrown) {
						alert(textStatus);
					}
				});
			});
			$('.change_pass').click(function(){
				var flag=0;
				var current_pass=$('#current_pass').val();
				var new_pass=$('#new_pass').val();
				var r_new_pass=$('#r_new_pass').val();
				if($('#current_pswd_error').html() == '' && $('#current_pass').val() != "" )
				{
					flag = 1;
				}
				else
				{
					flag=0;
				}
				if(new_pass != "" && r_new_pass != "" )
				{
					if(new_pass == r_new_pass)
					{
						flag=1;
					}
					else
					{
						flag=0;
						$('#confirm_pswd_error').html('Password mismatch');
					}
				}
				else
				{
					flag = 0;
				}
				if(flag == 1)
				{
					var data={
						current_pass:current_pass,
						new_pass:new_pass,
						r_new_pass:r_new_pass
					};
					var url_address= '<?php echo $this->webroot; ?>'+'User/change_pswd';
					$.ajax({
						type: "post",
						url:url_address,
						data: data,
						success: function(response) {
							if(response=="Yes")
							{
								$('.result').html('Password changed successfuly');
								$('.modal-header').hide();
								$('.body_modal').hide();
								$('.modal-footer').hide();
							}
							else
							{
								$('.result').html('An error occured. please refresh page');
								$('.body_modal').hide();
								$('.modal-header ').hide();
								$('.modal-footer').hide();
							}
						},
						error:function (XMLHttpRequest, textStatus, errorThrown) {
							alert(textStatus);
						}
					});
				}
			});
		$('#change_password_side').click(function(){
			$('.result').html('');
			$('#current_pswd_error').html('');
			$('#confirm_pswd_error').html('');
			$('.body_modal').show();
			$('.body_modal input').val('');
			$('.modal-header ').show();
			$('.modal-footer').show();
		});
		});
		</script>
		<script type="text/javascript">
		$(".select2").select2();
		$('.datatable').DataTable({
			'aaSorting':[],
			'aLengthMenu': [ 10, 25, 50, 100,300,500 ],
		});
		$("[data-mask]").inputmask().val();
		$('.datepicker').datepicker({
			format: "dd-mm-yyyy",
			singleDatePicker: true,
			calender_style: "picker_4",
			autoclose: true
		});
		$(document).on('keyup','.number',function(){
			if(isNaN($(this).val()) || $(this).val()=='')
			{
				$(this).val('0').keyup();
			}
		});
		</script>
		<script type="text/javascript">
		var current_address=$(location).attr('href');
		var current_address_split=current_address.split('/');
		$('#dashboard_menu_list li').each(function(key,value){
			var href=$(this).children().attr('href');
			if(href!='#') {
				var href_split=href.split('/');
				var method = href_split[href_split.length-1];
				var current_method = current_address_split[current_address_split.length-1];
				var avoid ="#";
				if(!isNaN(current_method))
				{
					var current_method=current_address_split[current_address_split.length-2];
				}
				current_method = current_method.replace(avoid,'');
				if(method==current_method)
				{
					$(this).closest('li.main_li').css('display','');
					$(this).closest('li.main_li').attr('class','treeview '+method);
					$(this).closest('li.sub_li').attr('class','treeview '+method);
					$(this).closest('li.second_sub_li').attr('class','treeview '+method);
					$(this).closest('li.third_sub_li').attr('class','treeview '+method);
					$(this).closest('li.fourth_sub_li').attr('class','treeview '+method);
					$("."+method).attr('class',"treeview active "+method);
				}
			}
		});
		</script>
		<script type="text/javascript">
		$(".select_two_class").select2();
		$('#mode_catagory').change(function(){
			var mode_catagory=$(this).val();
			$.post( "<?= $this->webroot ?>Accountings/get_mode_ajax/"+mode_catagory,function( data ) {
				$('#mode').html('');
				if(data.result!='Success')
				{
					alert(data.message);
					$('#mode_catagory').val('1').change();
					return false;
				}
				$.each(data.options,function(key,value){
					$('#mode').append($("<option></option>").attr("value",key).text(value));
				})
				$('#mode_field_salary').css('display','');
				if(mode_catagory!=1)
				{
					$('#mode_field').css('display','');
				}
				else
				{
					$('#mode_field').css('display','none');
				}
			}, "json");
		});
		$(document).on('click','.delete',function(){
			if(!confirm("Are you sure?"))
			{
				return false;
			}
			var journal_id=$(this).closest('tr').find('td span.journal_id').text();
			var rowindex = $(this).closest('tr').index();
			$.post( "<?= $this->webroot ?>Accountings/Journal_delete/"+journal_id,function( data ) {
				if(data.result!='Success')
				{
					alert(data.message);
					return false;
				}
				$('#transaction_details_table tbody tr:eq(' + rowindex + ')').remove();
				$('#account_head').change();
			}, "json");
		});
		$(document).on('click','.cash_delete',function(){
			if(!confirm("Are you sure?"))
			{
				return false;
			}

		});

		$(document).on('click','.edit_journal_amount_modal',function(){
			var amount=$(this).closest('tr').find('td.amount').text();
			var journal_id=$(this).closest('tr').find('td span.journal_id').text();
			$('#journal_id').val(journal_id);
			$('#old_journal_amount').val(amount);
			$('#edit_journal_amount_modal').modal('show');
		});
		$(document).on('click','#edit_journal_amount_button',function(){
			var journal_id=$('#journal_id').val();
			var amount=$('#new_journal_amount').val();
			$.post( "<?= $this->webroot ?>Accountings/General_journal_amount_edit_function/",{journal_id:journal_id,amount:amount},function( data ) {
				if(data.result!='Success')
				{
					alert(data.result);
					return false;
				}
				$('#edit_journal_amount_modal').modal('toggle');
			}, "json");
		});
		var product_configuration_type='<?= $Profile['Profile']['product_configuration_type'];  ?>';
		$.fn.product_configuration_type=function()
		{
			if(product_configuration_type=='Retail' && product_configuration_type!='Retail_And_Wholesale')
			{
				$('.wholesale_type').css('display','none');
			}
			if(product_configuration_type=='WholeSale' && product_configuration_type!='Retail_And_Wholesale')
			{
				$('.retail_type').css('display','none');
			}
		}
		$.fn.product_configuration_type();
		var Country='<?= $Profile['State']['country_id'];  ?>';
		$.fn.Country_type=function()
		{
			if(Country!=1)
			{
				$('.tax_field').css('display','none');
			}
		}
		if(Country!=1)
		{
			$('#gst_visibility').css('display','none');
		}
		$('#toggle_button_for_gst_visibility').change(function(){
			$('.tax_field').toggle();
		});
		$('.tax_field').css('display','none');
		$('.instate').css('display','none');
		$('.interstate').css('display','none');
		$.fn.Country_type();
		//prevent default short cuts
		// 					$(window).keydown(function(event) {
		//   if(event.ctrlKey && event.keyCode == 84) {
		//     console.log("Hey! Ctrl+T event captured!");
		//     event.preventDefault();
		//   }
		//   if(event.ctrlKey && event.keyCode == 83) {
		//     console.log("Hey! Ctrl+S event captured!");
		//     event.preventDefault();
		//   }
		// });
		// Search in Settings
		$('#SettingsInput').keyup(function(){
			var searchText = $(this).val().toUpperCase();
			$('ul.settingsMenu > li').each(function(){
				var currentLiText = $(this).text().toUpperCase(),
				showCurrentLi = currentLiText.indexOf(searchText) !== -1;
				$(this).toggle(showCurrentLi);
				$('.settingsMenu').css('height','300px');
			});
		});
		var create_permission_flag='<?= $create_permission_flag ?>';
		var edit_permission_flag='<?= $edit_permission_flag ?>';
		var delete_permission_flag='<?= $delete_permission_flag ?>';
		$.fn.icon_permissions=function()
		{
			if(create_permission_flag==0)
			{
				$('.fa-plus-circle').hide();
				$('.create_icon').hide();
				$('.fa-cart-arrow-down').hide();
			}
			else
			{
				$('.fa-plus-circle').show();
				$('.create_icon').show();
				$('.fa-cart-arrow-down').show();
			}
			if(edit_permission_flag==0)
			{

				$('.fa-pencil').hide();
				$('.fa-edit').hide();
				$('.fa-pencil-square-o').hide();
				$('.edit_icon').hide();

			}
			else
			{

				$('.fa-pencil').show();
				$('.fa-edit').show();
				$('.fa-pencil-square-o').show();
				$('.edit_icon').show();

			}
			if(delete_permission_flag==0)
			{

				$('.fa-trash').hide();
				$('.fa-minus-circle').hide();
				$('.btn-danger').hide();
				$('.btn-warning').hide();
				$('.delete_icon').hide();
			}
			else
			{

				$('.fa-trash').show();
				$('.fa-minus-circle').show();
				$('.btn-danger').show();
				$('.btn-warning').show();
				$('.delete_icon').show();
			}
		}
		$.fn.icon_permissions();	

		// Search in Settings
		//prevent default short cuts
		//common shortcuts starts
		shortcut.add("alt+e", function() {
			$('#dashboard_collaps').click();
		});
		shortcut.add("ctrl+shift+s", function() {
			var link = "<?= $this->webroot.'Sale/sale'?>";
			window.location.href = link;
		// window.history.pushState('obj', 'Sale', link);
		// return false;
		// window.open(link, '_blank');
		});
		shortcut.add("ctrl+shift+p", function(e) {
			e.stopPropagation()
			var link = "<?= $this->webroot.'Purchase/Purchase'?>";
			window.location.href = link;
		// window.open(link, '_blank');
		});
		shortcut.add("ctrl+shift+c", function() {
			var link = "<?= $this->webroot.'Accountings/AssetCurrentAccountRecievable'?>";
			window.location.href = link;
		// window.open(link, '_blank');
		});
		shortcut.add("ctrl+shift+e", function() {
			var link = "<?= $this->webroot.'Executive/AddSalesman'?>";
			window.location.href = link;
		// window.open(link, '_blank');
		});
		shortcut.add("ctrl+shift+v", function() {
			var link = "<?= $this->webroot.'Accountings/CurrentLiabilityCrediterSupplier'?>";
			window.location.href = link;
			// window.open(link, '_blank');
		});
		shortcut.add("ctrl+shift+r", function() {
			var link = "<?= $this->webroot.'Route/index'?>";
			window.location.href = link;
		// window.open(link, '_blank');
		});
		shortcut.add("ctrl+shift+b", function() {
			var link = "<?= $this->webroot.'Branch/AddBranch'?>";
			window.location.href = link;
		// window.open(link, '_blank');
		});
		//common shortcuts ends
		</script>
		<script>
		$('.main-sidebar').ready(function(){
			$('body').attr('class','skin-black sidebar-mini sidebar-collapse');
		});
		</script>

		</body>
		</html>