<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Login | iCresp</title>
  <link href="<?php echo $this->webroot; ?>css/bootstrap.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/login.css">
  <link rel="stylesheet" type="text/css" href="<?php echo $this->webroot; ?>css/responsive.css">
  <style type="text/css">
  #forgot
  {
    color:#0CB7EA !important;
  }
  .logo-img {
    /*width: 67%;*/
  }
  .logo-div {
    left: 0%;
    position: absolute;
    top: 30%;
  }
  .fab_logo_style{
    margin-left: 0px;
    margin-top: 35px;
  }
</style>
</head>
<body class="login-body">
  <div class="logo-div fab_logo_style">
    <center>
      <img src="<?= $this->webroot.'profile/logo.png'; ?>" class="logo-img"> 
      <div class="full-form">
      </div>
    </center>
  </div>
  <div class="iCresp-form">
    <?php  echo $this->Form->create('Userlogin',array('class'=>'form-horizontal login-container')); ?>
    <div style="display:none;"><input type="hidden" name="_method" value="POST"/></div>
    <div >
      <center><h3>Login</h3></center>
    </div>
    <hr>
    <div align="center"><?= $flash=$this->Session->flash(); ?></div>
    <div class="form-group">
      <label for="username" class = "col-lg-3 col-md-3 col-sm-3 col-xs-3">Username</label>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
        <?php echo $this->Form->input('Username',array('type'=>'text','label'=>false,'class'=>'form-control alt-form-control','placeholder'=>'User Name','required'=>'required','autofocus'=>true)); ?> 
      </div>
    </div>
    <div class="form-group">
      <label for="username" class = "col-lg-3 col-md-3 col-sm-3 col-xs-3">Password</label>
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
        <?php echo $this->Form->input('Password',array('type'=>'password','label'=>false,'class'=>'form-control alt-form-control','placeholder'=>'Password','required'=>'required')); ?>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-4 pull-right">
        <button type="submit" class="btn btn-custom btn-block btn-flat">Login</button>
      </div>
      <div class="col-xs-8 pull-right" style="display: none;">
        <a href="#" class="pull-right forgotpw" data-toggle="modal" data-target="#forgot">Forgot Password?</a>
      </div>
    </div>
    <?php echo $this->Form->end(); ?>
  </div>
  <div class="modal fade" tabindex="-1" role="dialog" id="forgot">
    <div class="modal-dialog forget_modal">
      <div class="modal-content iCresp-Modal">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Let's find your account</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" role="form">
            <div class="forgot_text"></div>
            <div class="form-group email_form">
              <label class="control-label col-sm-2">EMail</label>
              <div class="col-sm-10">
                <input type="email" id="forget_mail" class="form-control" placeholder="Enter Email ID Associated with your account">
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" id="forget_close" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-info" id="forget_pass">Send</button>
        </div>
      </div> 
    </div>
  </div> 
</div>
<script src="<?php echo $this->webroot; ?>js/jquery.min.j"s></script>
<script src="<?php echo $this->webroot; ?>js/bootstrap.min.js"></script>
</body>
<footer class="i-footer">
  Powered by <a href="http://mentorperformance.com">Mentor Performance Rating Pvt. Ltd.</a>
</footer>
</html>