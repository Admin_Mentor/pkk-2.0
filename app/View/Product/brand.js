$('#add_Brand').click(function(){
  var product_type_id=$('#product_type_id_modal option:selected').val();
  if(!product_type_id)
  {
   // $('#product_type_id_modal').select2('open');
   // return false;
  }
  $('#BrandProductTypeId').val(product_type_id);
  var data=$('#Brand_Add_Form').serialize();
  var url_address= "<?= $this->webroot; ?>Brand/brand_add_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      if(response.result=='Already Added')
      {
        $('#brand_id').val(response.key).trigger('change');   
        $('#Brand_Add_Form')[0].reset();
        $('#Brand_Add_Modal').modal('toggle');
      }
      else
      {
        return false;
      }
    }
    else
    {
      // $('#brand_id').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#Brand_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#Brand_id_modal').val($('#Brand_id_modal option:last-child').val()).trigger('change');

      // $('.brand_id').append($("<option></option>").attr("value",response.key).text(response.value));
      // $('.brand_id').val($('#brand_id option:last-child').val()).trigger('change');

      $('#Brand_Add_Form')[0].reset();
      $('#Brand_Add_Modal').modal('toggle');
    }
  }, "json");
});