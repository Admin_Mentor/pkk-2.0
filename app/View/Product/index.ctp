<style type="text/css">
  <?php include "style.css" ?>
</style>
<section class="content-header">
  <h1> Product
  <div class="col-sm-1 pull-right">
              <!-- <a href="#"> <i class="fa fa-plus-circle ad-mar" id="Product_modal_add"></i></a> -->
              <a href="#"><button class='btn btn-success' id="Product_modal_add">Add Product</button></a>
            </div>
          </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <br>
        <div class="row" >
          <div class="col-md-12">
            <div class="col-md-4">
              <!-- <div class="col-md-10">  -->
                <?php echo $this->Form->input('product_type',array(
                'type'=>'select','empty' =>'Select','options'=>$ProductType,'class'=>'form-control select2 product_index_search','label'=>'Category','id'=>'product_type',
                )) ?>
              <!-- </div> -->
            </div>
             <div class="col-md-4">
              <!-- <div class="col-md-10"> -->
               <?php echo $this->Form->input('type_products',array(
                'type'=>'select','empty' =>'Select','options'=>$Type,'class'=>'form-control select2 product_index_search','label'=>'Type of Product','id'=>'type_products',
                )) ?>
              <!-- </div> -->
            </div>
            <div class="col-md-4">
              <!-- <div class="col-md-10">  -->
                <?php echo $this->Form->input('product_name',array('type'=>'select','empty' =>'Select','options'=>$product_name,'class'=>'form-control select2','label'=>'Product','id'=>'product_name')) ?>
                  
                <!-- </div> -->
              <!-- <br> -->
              <!-- <div class="col-sm-1">
              <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" id="Product_modal_add"></i></a>
            </div> -->
              
            </div>
          </div>
        </div>
        <div class="box-body" >
          <table class="table table-hover boder table-bordered" id='table_data' style="margin-left:-3%:margin-right:5%" data-order='[[ 9, "asc" ]]'>
            <thead>
              <tr class="blue-bg">
                <th>Category</th>
                 <th>Type</th>
                <th>Product</th>
                <th>Product Code</th>
<!--                 <th>Barcode</th>
 -->                <th>Unit</th>
                <th>No.of pieces</th>
                <th>Executive Price</th>
                <th>Retail Price</th>
                <th>Landing Cost</th>
                 <th>Priority</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>
        </div>
        <?php require('product_type_modal.php') ?>
        <?php require('Product_Add_modal.php') ?>
        <?php require('Product_Type_Add_Modal.php') ?>
        <?php require('Brand_Add_Modal.php') ?>
        <?php require('product_modal.php') ?>
        <?php require('Type_Add_Modal.php') ?>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  table_data=$('#table_data').dataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Product/Product_Table_ajax",
      "type": "POST",
      data:function( d ) {
        d.type_products= $('#type_products').val();
        d.product_type= $('#product_type').val();
        d.product= $('#product_name').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "ProductType.name" },
    { "data" : "TypeOfProduct.name" },
    { "data" : "Product.name" },
    { "data" : "Product.code" },
    // { "data" : "Product.barcode" },
    { "data" : "Unit.name" },
    { "data" : "Product.no_of_piece_per_unit" },
    { "data" : "Product.wholesale_price",className:"text-right" },
    { "data" : "Product.mrp",className:"text-right" },
    { "data" : "Product.cost",className:"text-right" },
    { "data" : "Product.priority" },
    { "data" : "Product.action" },
    ],
    "columnDefs": [
    ],
  });
   $(document).on('click','.edit_priority',function(){
        var priority=$(this).closest('tr').find('td input.priority_edit').val();
        var edit_id=$(this).attr('table_id');
        $.post("<?= $this->webroot ?>Product/update_product_priority/"+edit_id+'/'+priority,function(response)
        {
           if(response.status!="Success")
           {
            alert(response.status);
                return false;
           }

        },"json");
       table_data.fnDraw();
    });
 //product corresponds to product type
  $.fn.product_index_search=function(){
    var product_type_id=$('#product_type').val();
    var type_products= $('#type_products').val();
    var data={
      product_type_id:product_type_id,
      type_products:type_products,
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_type_select_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) { 
        $('#product_name').empty();     
        $('#product_name').append($("<option></option>").attr("value",'').text('SELECT'));
        $.each(response, function(i, value) {
          $('#product_name').append($("<option></option>").attr("value", i).text(value));
        });
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  }
$('.product_index_search').change(function(){
  $.fn.product_index_search();
      table_data.fnDraw();
});
  <?php require('script.js'); ?>
  <?php require('product.js'); ?>
  <?php require('product_type.js'); ?>  
  <?php require('brand.js'); ?>  
  <?php require('type.js'); ?>  
</script>