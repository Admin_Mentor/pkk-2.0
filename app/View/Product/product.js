  product_barcode='';
  $(document).on('click', '.edit_Product', function (event) {
    //var prdct_typ_id=$(this).closest('tr').find('td.Product_type_hidden_feilds span.product_type_id').text();
    // $('#prdct_type').val(prdct_typ_id).trigger('change');
    p_id=$(this).attr('table_id');
    //var p_id=$(this).closest('tr').find('td.Product_hidden_feilds span.Product_id').text();
    $.post( "<?= $this->webroot ?>Product/product_get_ajax/"+p_id, function( response ) {
     var prdct_name=response.data.Product.name;
     var prdct_id=response.data.Product.code;
     var threashold=response.data.Product.threashold;
     var cost=response.data.Product.cost;
     var wholesale_price=response.data.Product.wholesale_price;
     var mrp=response.data.Product.mrp;
     var party_id=response.data.Product.party_id;
     var unit_id=response.data.Product.unit_id;
     var brand_id=response.data.Product.brand_id;
     if(brand_id==0)
     {
      brand_id='G';
    }
    console.log(response.data.Product);
      $('#hsn_code_edit').val(response.data.Product.hsn_code);
      $('#ProductEditName').val(response.data.Product.name);
      $('#ProductEditTrayOccupation').val(response.data.Product.tray_occupation);
      $('#ProductEditProductionDays').val(response.data.Product.production_days);
      $('#ProductEditArabicName').val(response.data.Product.arabic_name);
      $('#ProductEditId').val(response.data.Product.id);
      $('#ProductEditCode').val(response.data.Product.code);
      $('#ProductEditUnitId').val(response.data.Product.unit_id).change();
      $('#ProductEditMrp').val(response.data.Product.mrp);
      $('#ProductEditCost').val(response.data.Product.cost);
      $('#ProductEditWholesalePrice').val(response.data.Product.wholesale_price);    
      $('#ProductEditThreshold').val(response.data.Product.threashold);
      $('#ProductEditVat').val(response.data.Product.vat);
      $('#ProductEditBonus').val(response.data.Product.bonus_percentage);
      $('#ProductEditProductTypeIdModal').val(response.data.Product.product_type_id).change();
      $('#ProductEditTypeOfProduct').val(response.data.Product.type).change();
      if(response.data.Product.party_id==0)
        $('#ProductEditPartyIdModal').val('empty').change();
      else
        $('#ProductEditPartyIdModal').val(response.data.Product.party_id).change();
      $('#ProductEditNoOfPiecePerUnit').val(response.data.Product.no_of_piece_per_unit);
      $('#ProductEditNoOfPiecePerUnit').attr('disabled', true);
      if(response.data.Product.barcode){
        $('#editbarcodeOn').attr('checked',true);
        $('.edit_custom_barcode').hide();
        $('#editcustom_barcode').hide();
        $('#editproduct_barcode').show();
        $("#ProductEditCustomBarcode").prop("readonly", true);
        
      }
      else
      {
        $('#editbarcodeOn').attr('checked',false);
        $('.edit_custom_barcode').show();
        $('#editcustom_barcode').show();
        $('#editproduct_barcode').hide();
        $("#ProductEditCustomBarcode").prop("readonly", false);
        
      }
      $('#editbarcodeOn').change();
      //$('#editcustombarcodeOn').attr('checked',false);
      $('#editcustombarcodeOn').change();
      product_barcode=response.data.Product.barcode;
      $('#ProductEditCustomBarcode').val(response.data.Product.barcode);
       if(response.data.Product.brand_id!=0)
      {
       
        $('#ProductEditBrandIdModal').val(response.data.Product.brand_id).change();  
      }
      else
      {
        $('#ProductEditBrandIdModal').val('G').change();
      }
     if(response.data.Product.unit_check==1)
     {
    $('#ProductEditUnitId').attr('disabled', true);
     $('#ProductEditNoOfPiecePerUnit').attr('disabled', true);
     }
     else
     {
          $('#ProductEditUnitId').attr('disabled', false);
          $('#ProductEditNoOfPiecePerUnit').attr('disabled', false);

     }
    $('#Brand_id').val(brand_id).trigger('change');
    $('#prdct_nm').text('');
    $('#product_id_error').text('');
    $('#cost_and_mrp_check').text('');
    $('#product_edit').attr('disabled',true);
    $('#p_id').val(p_id);
    $('#prdct_name').val(prdct_name);
    $('#prdct_id').val(prdct_id);
    $('#unit_name').val(unit_id).trigger('change');
    $('#threshold').val(threashold);
    $('#cost').val(cost);
    $('#wholesale_price').val(wholesale_price);
    $('#mrp').val(mrp);
    $('#party_id').val(party_id).trigger('change');
    
    $('#Product_Edit_Modal').modal('show');
  }, "json");
  });
  $('#prdct_name').keyup(function(){
    var product_name=$(this).val().trim();
    var data={
      product_name:product_name
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Product/check_product_ajax';
    $.ajax({
      type: "post",  
      url:url_address,
      data: data,  
      success: function(response) {
        if(response=="Yes")
        {
          $('#prdct_nm').html('This Product Type is already taken');
          $('#product_edit').attr('disabled',true);
        }
        else
        {
          $('#prdct_nm').html('');
          $('#product_edit').attr('disabled',false);
        }
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  });
  $('#prdct_id').keyup(function(){
    var product_id=$(this).val();
    var data={product_id:product_id};
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_id_search';
    $.ajax({
      type: "post",  
      url:url_address,
      data: data,  
      success: function(response) {
        if(response=="Yes")
        {
          $('#product_id_error').text('This Product Id is already taken');
        }
        else
        {
         $('#product_id_error').text('');
       }
     },
     error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  });
  $('#Product_Edit_Modal .modal-body input').keyup(function(){
    $('#product_edit').attr('disabled',false);
  });
  $('#Product_Edit_Modal .modal-body select').change(function(){
    $('#product_edit').attr('disabled',false);
  });
  $('#product_edit').click(function()   {
    var p_id=$('#p_id').val();
     console.log(p_id);
    var prdct_name=$.trim($('#prdct_name').val());
    var prdct_nm_error=$('#prdct_nm').text();
    if(prdct_name=='' || prdct_nm_error!='')
    {
      $('#prdct_name').focus(); 
      return false;       
    }
    var prdct_type=$('#prdct_type').val();
    var prdct_type_name=$('#prdct_type option:selected').text();
    var product_type=$('.product_type_id_modal option:selected').val();
    if(product_type=='' || product_type=='empty')
    {
      $('.product_type_id_modal').select2('open'); 
      return false;       
    }
    var Brand_id=$('#Brand_id option:selected').val();
       var party_id=$('#party_id option:selected').val();
    if(Brand_id=='' || Brand_id=='empty')
    {
      $('#Brand_id').select2('open'); 
      return false;       
    }
    var Brand_name=$('#Brand_id option:selected').text();
    var prdct_id=$('#prdct_id').val();
    var wholesale_price=$('#wholesale_price').val();
    var product_id_error=$('#product_id_error').text();
    if(prdct_id=='' || product_id_error!='')
    {
      $('#prdct_id').focus(); 
      return false;       
    }
    var make_id_modal= $('#make_id_modal').val();
    var make_id_modal_name= $('#make_id_modal option:selected').text();
    if(make_id_modal=='')
    {
      $('#make_id_modal').select2('open'); 
      return false;       
    }
    var unit_name=$('#unit_name').val();
    if(unit_name=='' || unit_name=='empty')
    {
      $('#unit_name').select2('open'); 
      return false;       
    }
    var threshold=$('#threshold').val();
    if(threshold=='')
    {
      $('#threshold').focus(); 
      return false;       
    }
    var cost=$('#cost').val();
    if(cost=='')
    {
      $('#cost').focus(); 
      return false;       
    }
    var mrp=$('#mrp').val();
    if(mrp=='')
    {
      $('#mrp').focus(); 
      return false;       
    }
    if(parseFloat(mrp)<parseFloat(cost))
    {
     // $('#cost_and_mrp_check').text('MRP Should Be Greater Than Cost');
     // $('#mrp').focus();
     // return false;
    }
    else
    {
      //$('#cost_and_mrp_check').text('');
    }
      var type_of_product=$('#ProductEditTypeOfProduct').val();
    if(type_of_product=='' || type_of_product=='empty' || type_of_product==null)
  {
    $('#ProductEditTypeOfProduct').select2('open');
    return false;
  }
    var data=$('#Product_Edit_Form').serialize();
    var url_address= '<?php echo $this->webroot;?>'+'Product/product_edit_ajax';
    $.ajax({
      type: "post",  
      url:url_address,
      data: data, 
      dataType:'json',
      success: function(response) {
        console.log(response);
        if(response.result=='Success')
        {
          $('#Product_Edit_Modal').modal('hide');
          // $('#check_table tbody tr').each(function(){
          //   var id=$(this).closest('tr').find('td.Product_hidden_feilds span.Product_id').text();
          //   if(id==p_id)
          //   {
          //     $(this).closest('tr').attr('class','blue-pd flash'); 
          //     $(this).closest('tr').attr('style','font-size:19px;color:green');
          //     $(this).closest('tr').find('td.Product_name').text(prdct_name);
          //     $(this).closest('tr').find('td.product_type_name').text(prdct_type_name);
          //   }
          // });
          table_data.fnDraw();
          $('#product_name option').each(function(){
            if($(this).val()==p_id)
            {
              $(this).text(prdct_name);
              if($('#product_name').val()==p_id)
              {
                $(this).change();
              }
            }
          });
           //setTimeout(timer_for_table, 2000);
        }
        else
        {
         alert(response.result)
       }
     },
     error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  });
  $(document).on('click','.delete_Product',function(){
    if(!confirm("Are you sure?"))
    {
      return false;
    }
    id=$(this).attr('table_id');
    //var id=$(this).closest('tr').find('td.Product_hidden_feilds span.Product_id').text();
    //var Product_name=$(this).closest('tr').find('td.Product_name').text();
    //var rowindex = $(this).closest('tr').index();
    var url_address= '<?php echo $this->webroot; ?>'+'Product/delete_Product_ajax/'+$.trim(id);
    $.ajax({
      type: "post",  
      url:url_address,
    // data: data, 
    dataType:'json',
    success: function(response) {
      if(response.result=="Success")
      {
        // $('#check_table tbody tr:eq(' + rowindex + ')').remove();
        // $("#product_name option[value='"+id+"']").remove();
        // $("#product_name").val('').trigger('change');
        table_data.fnDraw();
      }
      else
      {
        $('#check_table tbody tr:eq(' + rowindex + ')').attr('style','color:red');
        $('#check_table tbody tr:eq(' + rowindex + ')').attr('class','blue-pd flash');
        setTimeout(timer_for_table, 2000);
      }
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  });
  $('#prdct_type').change(function(){
    var product_type_id=$('#prdct_type option:selected').val();
    var url_address= '<?php echo $this->webroot; ?>Brand/get_brand_name_ajax/'+product_type_id;
    $.ajax({
      method: "POST",
      url:url_address,
    // data: data,  
    dataType:'json',
  }).done(function( result ) {
    $('#Brand_id').html(result.option);
    $('#Brand_id').val('');
  });
});
  $('#mrp').keyup(function(){
    var cost=$('#cost').val();
    var mrp=$('#mrp').val();
    if(parseFloat(mrp)<parseFloat(cost))
    {
      $('#cost_and_mrp_check').text('MRP Should Be Greater Than Cost');
    }
    else
    {
      $('#cost_and_mrp_check').text('');
    }
  });
  $('#Product_Add_Btn').click(function(){
  var brand_id=$('#Brand_id_modal').val();
  var product_type_id=$('#product_type_id_modal').val();
  var type_of_product=$('#type_of_product').val();
  var name=$('#ProductAddName').val();
    var party_id=$('#party_id').val();

  if($.trim(name)=='')
  {
    $('#ProductAddName').focus();
    return false;
  }
  var product_id=$('#ProductAddCode').val();
  if($.trim(product_id)=='')
  {
    $('#ProductAddCode').focus();
    return false;
  }
  if(product_type_id=='' || product_type_id=='empty')
  {
    $('#product_type_id_modal').select2('open');
    return false;
  }
  if(brand_id=='empty' || brand_id=='')
  {
    $('#Brand_id_modal').select2('open');
    return false;
  }
  if(type_of_product=='' || type_of_product=='empty')
  {
    $('#type_of_product').select2('open');
    return false;
  }
if(type_of_product!=2 && type_of_product!=3 && type_of_product!=8)
{
  if(party_id=='empty' || party_id=='')
  {
    $('#party_id').select2('open');
    return false;
  }
}
  var data=$('#Product_Add_Form').serialize();
  var url_address= "<?= $this->webroot; ?>Product/Product_add_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.message);
      return false;
    }
    else
    {
      $('#ProductAddCustomBarcode').attr('value',response.custom_barcode);
      $('#ProductEditCustomBarcode').attr('value',response.custom_barcode);

      $('#Product_Add_Form')[0].reset();
      $('#Product_Add_modal').modal('toggle');
      $('#product_name').append(response.Product);
      $('#product_name').val($('#product_name option:last-child').val()).trigger('change');
      $('#ProductAddUnitId').change();
      $('#Brand_id_modal').change();
      $('#type_of_product').change();
      $('#party_id').change();
      $('#product_type_id_modal').change();
      table_data.fnDraw();

    }
  }, "json");
});
$(document).on('keyup','.piece_price_calc',function(){
  var unit_level=$('#ProductAddUnitLevel').val();
  if(unit_level == 2 || unit_level == 3){
   var piece_per_box=$('#ProductAddNoOfPiecePerUnit').val();
   var retail_per_unit=$('#ProductAddUnitMrp').val();
   var wholsale_per_unit=$('#ProductAddUnitWholesalePrice').val();
   var cost_per_unit=$('#ProductAddUnitCost').val();
   if(unit_level == 2)
   {
     $('#ProductAddMrp').val(retail_per_unit/piece_per_box);
     $('#ProductAddWholesalePrice').val(wholsale_per_unit/piece_per_box);
     $('#ProductAddCost').val(cost_per_unit/piece_per_box);
   }else{
    var box_per_cartoon=$('#ProductAddNoOfBoxPerCarton').val();
    $('#ProductAddMrp').val((retail_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductAddWholesalePrice').val((wholsale_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductAddCost').val((cost_per_unit/box_per_cartoon)/piece_per_box);
  }
}
});
// edit Function
$(document).on('keyup','.piece_price_calc',function(){
  var unit_level=$('#ProductEditUnitLevel').val();
  if(unit_level == 2 || unit_level == 3){
   var piece_per_box=$('#ProductEditNoOfPiecePerUnit').val();
   var retail_per_unit=$('#ProductEditUnitMrp').val();
   var wholsale_per_unit=$('#ProductEditUnitWholesalePrice').val();
   var cost_per_unit=$('#ProductEditUnitCost').val();
   if(unit_level == 2)
   {
     $('#ProductEditMrp').val(retail_per_unit/piece_per_box);
     $('#ProductEditWholesalePrice').val(wholsale_per_unit/piece_per_box);
     $('#ProductEditCost').val(cost_per_unit/piece_per_box);
   }else{
    var box_per_cartoon=$('#ProductEditNoOfBoxPerCarton').val();
    $('#ProductEditMrp').val((retail_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductEditWholesalePrice').val((wholsale_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductEditCost').val((cost_per_unit/box_per_cartoon)/piece_per_box);
  }
}
});
// add Function
$(document).on('keyup','.piece_stock_calc',function(){
     var unit_level=$('#ProductAddUnitLevel').val();
     if(unit_level == 2 || unit_level == 3){
      var piece_per_box=$('#ProductAddNoOfPiecePerUnit').val();
      
      var box_stock=$('#ProductAddBoxCount').val();
      var piece_stock=$('#ProductAddPieceCount').val();
      if(unit_level == 2){
        $('#stock_quantity').val((parseFloat(box_stock)*parseFloat(piece_per_box))+parseFloat(piece_stock));
      }else{
        var cartoon_stock=$('#ProductAddCartoonCount').val();
        var box_per_cartoon=$('#ProductAddNoOfBoxPerCarton').val();
        var cartoon_stock=parseFloat(box_per_cartoon)*parseFloat(cartoon_stock)*parseFloat(piece_per_box);
        var box_stock=parseFloat(box_stock)*parseFloat(piece_per_box);
         $('#stock_quantity').val(parseFloat(cartoon_stock)+parseFloat(box_stock)+parseFloat(piece_stock));
      }
      
     }
});
$('.unit_id').change(function(){
  var unit_id=$(this).val();
  $('#stock_quantity').val(0);
  var data={unit_id,unit_id};
  var url_address= "<?= $this->webroot; ?>Product/GetProductUnitLevel";
  $.post( url_address,data, function( response ) { 
    $('#ProductAddUnitLevel').val(response.Unit.level);
    $('#ProductEditUnitLevel').val(response.Unit.level);
    if(response.Unit.level==2)
    {
      $('.cartoon_count_cls').hide();
      $('.box_count_cls').show();
      $('.piece_count_cls').show();
      $('.no_of_piece_per_unit_feild').show();
      $('.price_of_unit_per_unit_feild').show();
      $('.no_of_box_per_carton_feild').hide();
      $('.no_of_piece_per_unit_feild').show();
      $('.no_of_piece_per_unit').attr('readonly',false);
      $('#stock_quantity').attr('readonly',true);
      $('.price_of_piece_per_unit_feild input').each(function(){
       $(this).attr('readonly',true);
     });
    }
    else if(response.Unit.level==1)
    {
      $('.cartoon_count_cls').hide();
      $('.box_count_cls').hide();
      $('.piece_count_cls').hide();
      $('.price_of_unit_per_unit_feild').hide();
      $('.no_of_piece_per_unit_feild').hide();
      $('.no_of_box_per_carton_feild').hide();
      $('.no_of_piece_per_unit').val('1');
      $('.no_of_box_per_carton').attr('readonly',true);
      $('.no_of_piece_per_unit').attr('readonly',true);
      $('#stock_quantity').attr('readonly',false);
      $('.price_of_piece_per_unit_feild input').each(function(){
       $(this).attr('readonly',false);
     });
    }
    $('#ProductAddUnitMrp').parent().find('label').text('Retail Price/'+response.Unit.name);
    $('#ProductAddUnitWholesalePrice').parent().find('label').text('Wholesale Price/'+response.Unit.name);
    $('#ProductAddUnitCost').parent().find('label').text('Landing Cost/'+response.Unit.name);
  },'json');
});
$.fn.auto_generated_barcode=function(){
  $.post( "<?= $this->webroot ?>Product/get_auto_generated_barcode",function( data ) {
// alert(product_barcode);
    if(product_barcode=='' || product_barcode==null)
    {
      $('.auto_gen_barcode').val(data.barcode);
    }
    else
    {
      $('.auto_gen_barcode').val(product_barcode);
    }
  }, "json");
}
$(document).on('change','#barcodeOn',function(){
 var status = $(this).prop('checked');
 if(status==true){
  $.fn.auto_generated_barcode();
  $('.barcode-div').show();
  }else{
    $('.barcode-div').hide();
  }
});
$(document).on('change','#custombarcodeOn',function(){
 var status = $(this).prop('checked');
 if(status==true){
    $('#product_barcode').hide();
    $('#custom_barcode').show();
  }else{
    $('#custom_barcode').hide();
    $('#product_barcode').show();
  }
});

$(document).on('click','#Product_modal_add',function(){
  $('#barcodeOn').attr('checked',false);
  $('#barcodeOn').change();
  $('#custombarcodeOn').attr('checked',false);
  $('#custombarcodeOn').change();
  
  $('#Product_Add_modal').modal('show');
});

$(document).on('change','#editbarcodeOn',function(){
 var status = $(this).prop('checked');
 if(status==true){
  $('.barcode-div').show();
  $.fn.auto_generated_barcode();
  $('#product_edit').attr('disabled',false);
  }else{
    $('.barcode-div').hide();
  }
});
$(document).on('change','#editcustombarcodeOn',function(){
 var status = $(this).prop('checked');
 if(status==true){
    $('#editproduct_barcode').hide();
    $('#editcustom_barcode').show();
  }else{
    $('#editcustom_barcode').hide();
    $('#editproduct_barcode').show();
  }
});

$('#editbarcodeOn').attr('checked',false);
$('#editbarcodeOn').change();
$('#editcustombarcodeOn').attr('checked',false);
$('#editcustombarcodeOn').change();

