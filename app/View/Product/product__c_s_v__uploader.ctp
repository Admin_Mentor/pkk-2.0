<section class="content-header">
  <h1> Product details</h1>
</section>
<section class="content clearfix">
  <div class="col-md-12">
    <div class="box">
      <div class="Stockmanagement">
        <div class="box-body">
          <?php echo $this->Form->create('ProductDetails', array('type' => 'file')); ?>
          <div class="col-md-12">
            <div class="col-md-6"></div>
            <div class="col-md-6">
              <div class="col-md-4">
                <h5 class="up_text" style="white-space: nowrap;">Please Upload File :</h5>
              </div>
              <div class="col-md-4">
                <?php echo $this->Form->input('Upload',array( 'type' => 'File','class'=>'file_up','label'=>false)); ?>
              </div>
              <div class="col-md-4">
                <button class="btn btn-success up_btn upload_button" type='submit'>UPLOAD</button>
              </div>
            </div>
          </div>
        </div>
        <?php  echo $this->Form->end(); ?>
        <br>
      </div>
    </div>
  </div>
</section>