<style type="text/css">
.top_mdl_pop{
  margin-top: 29px;
}
.buton_click_advance {
  background-color: #005e6d;
  color: white;
  padding-top: 13px;
  padding-bottom: 13px;
  padding-left: 15px;
  padding-right: 15px;
  border-radius: 3px !important;
  cursor: pointer;

}
/*new lines */
.TriSea-technologies-Switch > input[type="checkbox"] {
  display: none;   
}

.add_btn_styl {
  padding-top: 11px;
  padding-bottom: 11px;
  padding-left: 15px;
  padding-right: 15px;
  letter-spacing: 0.6px;
  border-radius: 3px !important;
}

.TriSea-technologies-Switch > label {
  cursor: pointer;
  height: 0px;
  position: relative; 
  width: 40px;  
}

.form-group-1{
  margin-bottom: 15px !important;
}

.TriSea-technologies-Switch > label::before {
  background: rgb(0, 0, 0);
  box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
  border-radius: 8px;
  content: '';
  height: 16px;
  margin-top: -8px;
  position:absolute;
  opacity: 0.3;
  transition: all 0.4s ease-in-out;
  width: 40px;
}
.TriSea-technologies-Switch > label::after {
  background: rgb(255, 255, 255);
  border-radius: 16px;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
  content: '';
  height: 24px;
  left: -4px;
  margin-top: -8px;
  position: absolute;
  top: -4px;
  transition: all 0.3s ease-in-out;
  width: 24px;
}
.TriSea-technologies-Switch > input[type="checkbox"]:checked + label::before {
  background: inherit;
  opacity: 0.5;
}
.TriSea-technologies-Switch > input[type="checkbox"]:checked + label::after {
  background: inherit;
  left: 20px;
}
</style>
<div class="modal fade" id="Product_Edit_Modal" tabindex="" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width:60%">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Product</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <?php echo $this->Form->create('ProductEdit',array('id'=>'Product_Edit_Form')); ?>
          <div class="form-group">
            <div class="col-sm-8">
              <?php echo $this->Form->input('name',array('class'=>'form-control product_disable toUpperCase','label'=>'Product Name In Full *','placeholder'=>' Enter Product Name','id'=>'prdct_name')) ?>
              <?php echo $this->Form->input('id',array('type'=>'hidden','id'=>'p_id')) ?>
            </div>
             <div class="col-sm-9" hidden>
               <?php echo $this->Form->input('arabic_name',array('class'=>'form-control ','dir'=>'rtl','label'=>'Product Name Arabic','placeholder'=>'')) ?>
            </div>
            <div class="col-sm-2">
              <?php echo $this->Form->input('code',array('type'=>'text','disabled' => 'disabled','class'=>'form-control product_disable','label'=>'Product Code')) ?>
            </div>
             <div class="col-sm-2">
                <?php echo $this->Form->input('hsn_code',array('type'=>'text','class'=>'form-control','label'=>'HSN Code','id'=>'hsn_code_edit')) ?>
              </div>
          </div>
          <div class="form-group">
            <div class="col-sm-3">
              <?php echo $this->Form->input('product_type_id_modal',array('type'=>'select','disabled'=>'false','options'=>array('empty'=>'SELECT',$ProductType),'class'=>'form-control select2 product_type_id_modal','style'=>array('width:100%'),'label'=>'Category')); ?>
            </div>
            <div class="col-sm-3">
              <?php echo $this->Form->input('Brand_id_modal',array('type'=>'select','options'=>array('empty'=>'SELECT',$Brand),'class'=>'form-control select2 brand_class brand_id','style'=>array('width:100%'),'label'=>'Brand',)); ?>
            </div>
            <div class="col-sm-3" hidden>
              <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" data-toggle="modal"  data-target="#Brand_Add_Modal"></i></a>
            </div>
            <div class="col-sm-3">
              <?php echo $this->Form->input('Party_id_modal',array('type'=>'select','options'=>array('empty'=>'SELECT',$PartyList),'class'=>'form-control select2 ','style'=>array('width:100%'),'label'=>'Main Supplier')); ?>
            </div>
             <div class="col-sm-3">
              <?php echo $this->Form->input('vat',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'Tax %')); ?>
            </div>
          </div>
          <div class="row" style="margin-top: 1%;">
            <div class="col-md-12 col-lg-12 col-sm-12">
              <div class="col-md-4 col-lg-4 col-sm-4 no-padding">
                <div class="col-md-4 col-lg-4 col-sm-4">
                  <div class="TriSea-technologies-Switch " hidden>
                    <b>Barcode</b><br>
                    <input id="editbarcodeOn" name="data[ProductEdit][barcode_on]" checked type="checkbox"/>
                    <label for="editbarcodeOn" class="label-primary"></label>
                  </div>
                </div>
                
              </div>
              <div class="col-md-8 col-lg-8 col-sm-8 barcode-div">
                <div class="col-md-6 col-lg-6 col-sm-6">
                    <div class="TriSea-technologies-Switch edit_custom_barcode ">
                      <b>Custom Barcode</b><br>
                      <input id="editcustombarcodeOn" name="data[ProductEdit][custom_barcode_selection]" checked type="checkbox"/>
                      <label for="editcustombarcodeOn" class="label-primary"></label>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6">
                  <div class="form-group-1">
                    <div id="editcustom_barcode" style="display: none">
                      <?php echo $this->Form->input('custom_barcode',array('type'=>'text','class'=>'form-control')); ?>
                    </div>
                     <div id="editproduct_barcode">
                       <?php echo $this->Form->input('Product_barcode',array('type'=>'text','readonly','class'=>'form-control auto_gen_barcode',)); ?>
                     </div>
                  </div>
                </div>    
            </div>
          </div>
        </div>
          <div class="form-group">
            <div class="col-sm-4" hidden>
              <?php echo $this->Form->input('bonus',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'Bonus %')); ?>
            </div>
              <div class="col-sm-3">
                 <?php echo $this->Form->input('type_of_product',array('type'=>'select','empty'=>'select','options'=>$Type,'class'=>'form-control select_two_class','style'=>array('width:100%'),'label'=>"Type",'required')); ?>
              </div>
               <div class="col-sm-3">
              <?php echo $this->Form->input('unit_id',array('type'=>'select','options'=>$unit_name,'selected'=>1,'style'=>array('width:100%'),'class'=>'form-control select2 unit_id','label'=>'Unit')) ?>
               <?php echo $this->Form->input('unit_level',array('type'=>'hidden')) ?>
            </div>
             <div class="col-sm-3">
                <?php echo $this->Form->input('tray_occupation',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'Number of Occupation per tray','required')); ?>
              </div>
               <div class="col-sm-3">
                <?php echo $this->Form->input('production_days',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'Delivery Days',)); ?>
              </div>
          </div>

          
          <div class="form-group tax_field">
            <div class="col-sm-4">
              <?php echo $this->Form->input('cgst',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'CGST %')); ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('sgst',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'SGST %')); ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('igst',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'IGST %')); ?>
            </div>
          </div>
           <div class="form-group price_of_unit_per_unit_feild" style="display:none">
            <div class="col-sm-4 unit_retaile_price_field">
              <?php echo $this->Form->input('unit_mrp',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Retail Price/Unit')) ?>
            </div>
            <div class="col-sm-4 unit_whole_sales_price_field">
              <?php echo $this->Form->input('unit_wholesale_price',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Wholesale Price/Unit')) ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('unit_cost',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Landing Cost/Unit')) ?>
            </div>
          </div>
          <div class="form-group price_of_piece_per_unit_feild">
            <div class="col-sm-4 retaile_price_field">
              <?php echo $this->Form->input('mrp',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Retail Price/Piece',)) ?> 
            </div>
            <div class="col-sm-4 whole_sales_price_field">
              <?php echo $this->Form->input('wholesale_price',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Executive Price/Piece')) ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('cost',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Landing Cost/Piece')) ?>
            </div>
          </div>
          
          <div class="form-group" hidden>
            
            <div class="col-sm-6">
              <?php echo $this->Form->input('threshold',array('type'=>'text','class'=>'form-control product_disable number_field','label'=>'Threshold *','min'=>0)) ?>
            </div>
            <div class="col-sm-8" style="display: none;">
              <?php echo $this->Form->input('hsn_code',array('type'=>'text','class'=>'form-control','label'=>'HSN Code')) ?>
            </div>
            <div class="col-sm-3 no_of_piece_per_unit_feild" style="display:none">
              <?php echo $this->Form->input('no_of_piece_per_unit',array('type'=>'number','min'=>'0','class'=>'form-control product_disable no_of_piece_per_unit number_field piece_stock_calc piece_price_calc','value'=>1,'label'=>'No Of Pieces','readonly')) ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <?php echo $this->Form->input('quantity',array('class'=>'form-control number_field','label'=>'Stock *','value'=>'0','type'=>'hidden')) ?>
            </div>
            
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id='product_edit'>Edit Product</button>
        </div>
        <?php echo $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>