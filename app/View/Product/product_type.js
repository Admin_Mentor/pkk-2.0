$('#add_Product_Type').click(function(){
  var data=$('#ProductType_Add_Form').serialize();
  var url_address= "<?= $this->webroot; ?>ProductType/product_type_add_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $('#product_type').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#product_type').val($('#product_type option:last-child').val()).trigger('change');
      $('#product_type_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#product_type_id_modal').val($('#product_type_id_modal option:last-child').val()).trigger('change.select2');
      $('.product_type_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
      $('.product_type_id_modal').val($('#product_type_id_modal option:last-child').val()).trigger('change.select2');
      $('#ProductType_Add_Form')[0].reset();
      $('#Product_Type_Add_Modal').modal('toggle');
    }
  }, "json");
});
$(document).on('click', '.product_type_edit', function (event) {
  var type_id=$(this).closest('tr').find('td.Product_type_hidden_feilds span:eq(0)').text();
  var prdct_type_name=$(this).closest('tr').find('td.product_type_name').text();
  $('#prdct_type_error_edit').text('');
  $('#product_type_edit').attr('disabled',true);
  $('#prdct_type_id').val(type_id);
  $('#prdct_typ').val(prdct_type_name);
  $('#Product_Type_edit').modal('show');
});
$('#prdct_typ').keyup(function(){
  $('#prdct_typ').val($(this).val().toUpperCase());
  var prdct_type=$(this).val().trim();
  if(!prdct_type)
  {
    $('#product_type_edit').attr('disabled',true);
  }
  else
  {
    var data={
      prdct_type:prdct_type
    };
    var url_address= '<?php echo $this->webroot; ?>'+'ProductType/check_producttype_ajax';
    $.ajax({
      type: "post",  
      url:url_address,
      data: data,  
      success: function(response) {
        if(response=="Yes")
        {
          $('#prdct_type_error_edit').html('This Product Type is already taken');
          $('#product_type_edit').attr('disabled',true);
        }else{
          $('#prdct_type_error_edit').html('');
          $('#product_type_edit').attr('disabled',false);
        }
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  }
});
//  ajax call for : Edit Product Type
$('#product_type_edit').click(function(){
  var prdct_type_id=$('#prdct_type_id').val();
  var prdct_typ=$('#prdct_typ').val();
  if($.trim(prdct_typ)=='')
  {
    $('#prdct_typ').focus();
    return false;
  }
  var url_address= '<?php echo $this->webroot; ?>'+'ProductType/edit_ajax';
  var data={prdct_id:prdct_type_id,prdct_typ:prdct_typ};
  $.ajax({
    type: "post",  
    url:url_address,
    data: data, 
    dataType:'json',
    success: function(response) {
      if(response.result=='Success')
      {
        $('#check_table tbody tr').each(function(){
          var type_id=$(this).closest('tr').find('td.Product_type_hidden_feilds span:eq(0)').text();
          if(prdct_type_id==type_id)
          {
            $(this).closest('tr').find('td.product_type_name').text(prdct_typ);
            $(this).closest('tr').attr('class','blue-pd flash');
            $(this).closest('tr').attr('style','font-size:19px;color:green');
            $('#product_type option').each(function(){
              if($(this).val()==prdct_type_id)
              {
                $(this).text(prdct_typ);
                if($('#product_type').val()==prdct_type_id)
                {
                  $(this).change();
                }
              }
            });
            setTimeout(timer_for_table, 2000);
          }
        });
      }
      else
      {
        alert(response.result);
      }
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
});
$(document).on('click','.product_type_delete',function(){
  if(!confirm("Are you sure?"))
  {
    return false;
  }
  var id=$(this).closest('tr').find('td.Product_type_hidden_feilds span:eq(0)').text();
  var Product_name=$(this).closest('tr').find('td.Product_name').text();
  var rowindex = $(this).closest('tr').index();
  if($.trim(Product_name)=='')
  {
    var url_address= '<?php echo $this->webroot; ?>'+'ProductType/delete_ProductType_ajax/'+$.trim(id);
    $.ajax({
      type: "post",  
      url:url_address,
      // data: data, 
      dataType:'json',
      success: function(response) {
        if(response.result=="Success")
        {
          $('#check_table tbody tr:eq(' + rowindex + ')').remove();
          $("#product_type option[value='"+id+"']").remove();
          $("#product_type").select2('val','').trigger('change');
        }
        else
        {
          $('#check_table tbody tr:eq(' + rowindex + ')').attr('class','blue-pd flash');
          $('#check_table tbody tr:eq(' + rowindex + ')').attr('style','font-size:20px;color:red');
          setTimeout(timer_for_table, 2000);
        }
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  }
  else
  {
    $('#check_table tbody tr:eq(' + rowindex + ')').attr('style','font-size:20px;color:red');
    $('#check_table tbody tr:eq(' + rowindex + ')').attr('class','blue-pd flash');
    setTimeout(timer_for_table, 2000);
  }
});
function timer_for_table() {
  $('#check_table tbody tr').each(function(){
    $(this).closest('tr').attr('class','blue-pd');
    $(this).closest('tr').attr('style','font-size:14px;color:');
  });
}
$('.product_search_class').change(function(){
  $.fn.product_search();
});
 //product corresponds to product type
 $.fn.product_search = function() {
  var product_type=$('#product_type').val();
    var type_products=$('#type_products').val();
  var model=$('#model').val();
  var value={
    product_type:product_type,
    type_products:type_products,
    model:model,
  };
  var url_address= '<?php echo $this->webroot; ?>'+'Product/product_type_select_ajax';
  $.ajax({
    type: "post",  
    url:url_address,
    data: value,  
    dataType:'json',
    success: function(response) {
      $('#product_name').html(response.option);
      $('#product_name').select2('val','');
      $.fn.searchtype();
      table_data.fnDraw();
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
};