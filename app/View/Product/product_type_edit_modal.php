<div id="product_type_edit_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Product Type</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-3"><label class="form-group">Product Type</label></div>
          <div class="col-md-6">
            <input type="text" style="display:none" id="hidden_product_type_id">
            <input type="text" class="form-control form-group product_name" placeholder="" id="product_type_edit_name"></div>
          <div class="col-md-3">
            <i id='' class="check_product_type_status fa fa-times" aria-hidden="true"></i>
          </div>
        </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" data-dismiss="modal" id="product_type_edit" disabled=true>Update</button>
        </div>
      </div>
    </div>
  </div>
