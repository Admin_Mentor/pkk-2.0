$('button#Product_type_add').click(function(){
  var name=$.trim($('#Product_type_name').val());
  if(name=='')
  {
    $('#Product_type_name').focus();
    return false;
  }
  var data={name:name};
  var url_address= '<?php echo $this->webroot; ?>ProductType/product_type_add_ajax';
  $.ajax({
    method: "POST",
    url:url_address,
    data: data,  // post data
  }).done(function( result ) {
    $('#product_type').append(result);
    $('#product_type option:last-child').prop('selected',true);
    $('#ProductProductTypeId').append(result);
    $('#ProductProductTypeId option:last-child').prop('selected',true);
    $('#product_type').trigger('change');
    $('#Product_type_name').val('');

  });
});

$('.product_name').keyup(function(){
  var product_type=$(this).val().toUpperCase();
  if(product_type=='')
  {
    return false;
  }
  $(this).val(product_type);
  var website_url='<?php echo $this->webroot; ?>ProductType/check_producttype_ajax';
  var data={product_type:product_type};
  $.ajax({
    method: "POST",
    url: website_url,
    data: data,
  }).done(function( result ) {
    if(result=="Yes")
    {
      $('.check_product_type_status').attr('class','check_product_type_status fa fa-times');
      $('.product_name').trigger('keyup');
      $('.product_name').focus();
      $('#product_type_edit').attr('disabled',true);
      $('button#Product_type_add').attr('disabled',true);
    }
    else {
      $('.check_product_type_status').attr('class','check_product_type_status fa fa-check');
      $('#product_type_edit').attr('disabled',false);
      $('button#Product_type_add').attr('disabled',false);
    }
  });
});


//product corresponds to product type

$('#product_type').change(function(){
  var product_type=$(this).val();
$.fn.product_type_change(product_type);
});

$('#ProductProductTypeId').change(function(){
  var product_type=$(this).val();
$.fn.product_type_change(product_type);
});

$.fn.product_type_change=function(product_type_id){
  var data={product_type_id:product_type_id};


  var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_type_select_ajax';
  $.ajax({
      type: "post",  // Request method: post, get
      url:url_address,
      data: data,  // post data
      // dataType:'json',
      success: function(response) {
        $('#product_name').html(response);
        $("#product_name").val($("#product_name option:first").val()).attr('selected',true).trigger('change');
        $.fn.searchtype();
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  $('#product_type').select2('val',product_type_id);
  $('#ProductProductTypeId').select2('val',product_type_id);
  $.fn.search();
}


// edit script

$(document).on('click', '.product_type_edit_modal', function (event) {
  var id=$(this).closest('tr').find('span.product_type_hidden_id').text();
  $('#hidden_product_type_id').val(id);
  var url_address='<?php echo $this->webroot; ?>'+'ProductType/get_product_type_ajax/'+id;
  $.ajax({
    type: "POST",  // Request method: post, get
    url:url_address,
    //  data: data,  // post data
    dataType:'json',
    success: function(response) {
      $('#product_type_edit_name').val(response);
      $("#product_type_edit_modal").modal();
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
});

$(document).on('click', '.product_type_delete', function (event) {
  var id=$(this).closest('tr').find('span.product_type_hidden_id').text();
  var url_address='<?php echo $this->webroot; ?>'+'ProductType/delete_product_type_ajax/'+id;
  $.ajax({
    type: "POST",  // Request method: post, get
    url:url_address,
    //  data: data,  // post data
    dataType:'json',
    success: function(response) {
      if(response.Availablity=='Yes')
      {
        alert('This Product Type Cant be Deleted');
      }
      else 
      {
        $('#product_table').DataTable().destroy();
        var tableRow=$('#product_table tbody tr td:nth-child(1) span.product_type_hidden_id:contains("' + id + '")').closest('tr').index();
        $('table#product_table tr:nth-child(' + tableRow + ')').remove();
        $("#product_type option[value='"+id+"']").remove();
        $("#ProductProductTypeId option[value='"+id+"']").remove();
        $('#product_table').DataTable();
        $.fn.searchtype();

      }
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });

});




$('#product_type_edit').click(function()   {

  var id=$('#hidden_product_type_id').val();
  var name=$.trim($('#product_type_edit_name').val());
  if(name=='')
  {
    $('#product_type_edit_name').focus();

    return false;
  }
  var url_address= '<?php echo $this->webroot; ?>'+'ProductType/edit_ajax';
  var data={
    prdct_id:id,
    prdct_typ:name
  };
  $.ajax({
    type: "post",  // Request method: post, get
    url:url_address,
    data: data, // post data
    // dataType:'json',
    success: function(response) {
      $('#product_table').dataTable().fnDestroy();

      $('#product_table tbody tr td:nth-child(1) span.product_type_hidden_id:contains("' + id + '")').each(function(){
        var tableRow=$(this).closest('tr').index()+1;
        $('table#product_table tr:nth-child(' + tableRow + ') td:nth-child(1) span:nth-child(1)').text(name);
      });
      $("#product_type option[value='"+id+"']").text(name);
      $("#ProductProductTypeId option[value='"+id+"']").text(name);
      $('#product_table').dataTable();

      $.fn.searchtype();
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });

});
