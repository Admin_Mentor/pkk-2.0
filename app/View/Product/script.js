$.fn.searchtype = function() {
  var product_type=$('#product_type option:selected').val();
  var product_name=$('#product_name option:selected').val();
  var make=$('#make option:selected').val();
  var model=$('#model option:selected').val();
  var data={
    product_type:product_type,
    product_name:product_name,
    make:make,
    model:model,
  };
  var url_address= '<?php echo $this->webroot; ?>'+'Product/product_search_ajax';
  $.ajax({
    type: "post",
    url:url_address,
    data: data,
    dataType:'json',
    success: function(response) {
      // $('#check_table').DataTable().destroy();
      // $('#check_table tbody').html(response.row);
      // $('#check_table').DataTable();
      //$.fn.show_alert('Success');
      table_data.fnDraw();
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
}
$('#product_name').change(function(){
  $.fn.searchtype();
});
$('#showallbutton').click(function(){
  $('#product_type').select2().select2('val', $('option:eq(0)').val());
  $('#make').select2().select2('val', $('option:eq(0)').val());
  $('#model').select2().select2('val', $('option:eq(0)').val());
  $('#product_name').select2().select2('val', $('option:eq(0)').val());
  $.fn.searchtype();
});
$.fn.show_alert = function(flash)
     {
      //$.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
