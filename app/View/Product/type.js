$('#add_Type').click(function(){
  var data=$('#Type_Add_Form').serialize();
  var url_address= "<?= $this->webroot; ?>Product/Type_add_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      if(response.result=='Already Added')
      {
        $('#type_of_product').val(response.key).trigger('change');   
        $('#Type_Add_Form')[0].reset();
        $('#Type_Add_Modal').modal('toggle');
      }
      else
      {
        return false;
      }
    }
    else
    {
      $('#type_of_product').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#type_of_product').val($('#type_of_product option:last-child').val()).trigger('change');

      $('#Type_Add_Form')[0].reset();
      $('#Type_Add_Modal').modal('toggle');
    }
  }, "json");
});