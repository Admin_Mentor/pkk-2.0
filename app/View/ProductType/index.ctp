<section class="content-header">
  <h1>Category</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <?= $this->Form->create('ProductType', array('id'=>'ProductType_Form'));?>
      <?= $this->Form->hidden('id',['id'=>'ProductType_id']); ?>
        <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
          <div class="col-md-2"></div>  
          <div class="col-md-8 col-lg-8 col-sm-8">
            <div class="form-group">
              <?= $this->Form->input('name',array('type'=>'text','id'=>'name','class'=>'form-control','required'=>'required')); ?>
            </div>
          </div>
          <div class="col-md-1 col-lg-1 col-sm-1">
            <button type="button" class="btn btn-success" id='save_button' style="margin-top: 25px;">Save</button>
          </div>
        </div>
      </div>
      <?= $this->Form->end(); ?>
    </div>
    <div class="box-body">
      <table class="table table-hover boder table-bordered" id='table_data'>
        <thead>
          <tr class="blue-bg">
            <th>#</th>
            <th width="80%">Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</section>
<script type="text/javascript">
  table_data=$('#table_data').dataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>ProductType/ProductType_Table_ajax",
      "type": "POST",
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "ProductType.id" },
    { "data" : "ProductType.name" },
    { "data" : "ProductType.action" },
    ],
    "columnDefs": [
    ],
  });
</script>
<script type="text/javascript">
  $('input').click(function(){
    $(this).select();
  });

  $('#save_button').click(function(){
    var data=$('#ProductType_Form').serialize();
    var url_address= "<?= $this->webroot; ?>ProductType/ProductType_Save_ajax";
    $.post( url_address,data, function( response ) {
      if(response.result!='success') { alert(response.result); return false; }
      $('#ProductType_Form')[0].reset();
      $('#ProductType_Form input[name="data[ProductType][id]"]').val(0);
      table_data.fnDraw();
    }, "json");
  });
  $(document).on('click','.delete_ProductType',function(){
    if(!confirm('Are You Sure')) { return false; }
    id=$(this).attr('table_id');
    var url_address= "<?= $this->webroot; ?>ProductType/Delete_ProductType_ajax/"+id;
    $.get( url_address, function( response ) {
      if(response.result!='Success') { alert(response.result); return false; }
      table_data.fnDraw();
    }, "json");
  });
  $(document).on('click','.edit_ProductType',function(){
    id=$(this).attr('table_id');
    var url_address= "<?= $this->webroot; ?>ProductType/product_type_get_ajax/"+id;
    $.get( url_address, function( response ) {
      $('#ProductType_id').val(response.data.id);
      $('#name').val(response.data.name);
    }, "json");
  });
</script>
