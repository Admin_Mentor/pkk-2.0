<style type="text/css">
  .i_color_plus {
    color: #005082;
    margin-top: 3px;
  }
  .table_main_heading {
    margin: 0px;
    text-transform: capitalize;
    padding-left: 9px;
    padding-bottom: 10px;
  }
  .btn_save_ftr {
    background-color: #0e792a;
    color: white;
    letter-spacing: 0.6px;
    text-transform: capitalize;
    border-radius: 3px !important;
    margin-top: 23px;
    border: navajowhite;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 6px;
    padding-bottom: 6px;
  }

  .btn_save_ftr_2 {
    background-color: #194e6f;
    color: white;
    letter-spacing: 0.6px;
    text-transform: capitalize;
    border-radius: 3px !important;
    margin-top: 23px;
    border: navajowhite;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 6px;
    padding-bottom: 6px;
  }

  .boder_table {
    border: 1px solid #e2e2e2;
  }

  .sav_right_mrgn{
    margin-left: 15px;
  }
  .btn_modal_add {
    border: none;
    background-color: #860006;
    color: white;
    border-radius: 3px !important;
    letter-spacing: 0.6px;
  }
  .mdl_in_pls{
    margin-top: 27px;
  }

  .head_mdl {
    text-transform: capitalize;
    letter-spacing: 0.6px;
    font-size: 16px;
    padding-left: 15px;
    padding-bottom: 14px;
    color: #ab1d1d;
    cursor: pointer;
  }
  .right_item_mrgn{
    margin-top: 27px;
    margin-right: 27px;

  }

</style>

<section class="content-header">
  <h1>BOM</h1>
</section>
<?= $this->Form->create('Bom', array('url' => array('controller' => 'Production', 'action' => 'Bom')));?>
<section class="content">


  <div class="box box-primary">
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="pull-right right_item_mrgn">
        <a href="<?php echo $this->webroot ?>Production/BomList"><button type="button" style="margin-left:20px" type="button" class="btn btn-success save pull-right">BOM List</button></a>
        </div>
      </div>
    </div>
    <div class="row" style="margin-top: 2%;">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-6 col-lg-6 col-sm-6 no-padding">
          
          <div class="col-md-5 col-lg-5 col-sm-5">
            <div class="form-group">
              <label style="white-space: nowrap;">Product</label>
              <?php if(isset($BomItem)) : ?>
                <?= $this->Form->input('product_id',['type'=>'text','style'=>'width:100%','id'=>'product','class'=>'form-control','required','readonly','value'=>$this->request->data['Product']['name'],'label'=>false,]); ?></td>
              <?php else : ?>
                <?= $this->Form->input('product_id',['type'=>'select','style'=>'width:100%','id'=>'product','class'=>'form-control select_two_class','required','empty'=>[''=>'Select'],'options'=>$Product_list,'label'=>false,]); ?></td>
              <?php endif; ?>
            </div>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-3">
            <div class="form-group">
              <label style="white-space: nowrap;">Production Cost</label>
              <?= $this->Form->input('production_cost',['type'=>'text','style'=>'width:100%','id'=>'production_cost','class'=>'form-control','required','label'=>false,]); ?></td>
            </div>
          </div>
         
        </div>
        <div class="col-md-6 col-lg-6 col-sm-6">
          <button type='submit' name='data[Bom][process]' value='save' class=" btn btn_save_ftr_2 pull-right invoice-action" >Save</button>
        </div>
      </div>
    </div>
    <hr>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <h3 class="table_main_heading">input</h3>
        <div class="box-body table-responsive">
          <table class="table boder_table" style="margin-top: 0%;" id='bom_input_table'>
            <thead>
              <tr class="blue-bg">
                <th style="width: 45%;">Product</th>
                <th>Unit</th>
                <th>Quantity</th>
                <th>Action</th>

              </tr>
              <tr>
                <td>
                  <?= $this->Form->input('product_in',['type'=>'select','style'=>'width:100%','id'=>'product_in','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'options'=>$Product_list,'label'=>false,]); ?>
                </td>
                <td>
                  <?= $this->Form->input('unit_id',['type'=>'select','style'=>'width:100%','id'=>'unit_id','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'options'=>$Unit_list,'label'=>false,]); ?></td>
                </td>
                <td>
                  <?= $this->Form->input('quantity',['type'=>'text','style'=>'width:100%','id'=>'quantity','class'=>'form-control','label'=>false,]); ?></td>
                </td>
                 
                <td><i class="fa fa-plus-circle fa-2x i_color_plus" id='add_bom_input'></i></td> 
               
                  
                </tr>
              </thead>
              <tbody>

                <?php if(isset($BomItem)) : ?>
                  <?php foreach ($BomItem as $key => $value): ?>
                      <tr class="blue-pddng">
                        <td>
                          <input class="productlist productsrow" hidden name="data[BomItem][product_id][]" value="<?= $value['BomItem']['product_id']; ?>">
                          <input value='<?= $value['Product']['name']; ?>' class='form-control' readonly type='text'>
                          <input value='<?= $value['BomItem']['id']; ?>' name='data[BomItem][BomItem_id][]' type='hidden' class='table_id'>
                          <input value='input' name='data[BomItem][mode][]' type='hidden'>
                        </td>
                        <td><input class='form-control' value='<?= $value['Unit']['name']; ?>' readonly><input value='<?= $value['Unit']['id']; ?>' type='hidden'></td>
                        <td><input class='form-control product_cart_total_quantity' value='<?= $value['BomItem']['quantity']; ?>' name='data[BomItem][quantity][]'><input class='form-control production_item_total_quantity' type='hidden' value='<?= $value['BomItem']['quantity']; ?>'></td>
                        <td><i class="fa fa-minus-circle fa-2x remove_old_tr" ></i></td>
                       
                      </tr>
                  <?php endforeach ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    
    </div>
  </section>


  <?= $this->Form->end(); ?>  


  <script type="text/javascript">
    <?php require 'bom.js'; ?>
  </script>


