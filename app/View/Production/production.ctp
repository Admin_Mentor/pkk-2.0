<style type="text/css">
.i_color_plus {
  color: #005082;
  margin-top: 3px;
}
.table_main_heading {
  margin: 0px;
  text-transform: capitalize;
  padding-left: 9px;
  padding-bottom: 10px;
}
.btn_save_ftr {
  background-color: #0e792a;
  color: white;
  letter-spacing: 0.6px;
  text-transform: capitalize;
  border-radius: 3px !important;
  margin-top: 23px;
  border: navajowhite;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 6px;
  padding-bottom: 6px;
}

.btn_save_ftr_2 {
  background-color: #194e6f;
  color: white;
  letter-spacing: 0.6px;
  text-transform: capitalize;
  border-radius: 3px !important;
  margin-top: 23px;
  border: navajowhite;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 6px;
  padding-bottom: 6px;
}

.boder_table {
  border: 1px solid #e2e2e2;
}

.sav_right_mrgn{
  margin-left: 15px;
}
.btn_modal_add {
  border: none;
  background-color: #860006;
  color: white;
  border-radius: 3px !important;
  letter-spacing: 0.6px;
}
.mdl_in_pls{
  margin-top: 27px;
}

.head_mdl {
  text-transform: capitalize;
  letter-spacing: 0.6px;
  font-size: 16px;
  padding-left: 15px;
  padding-bottom: 14px;
  color: #ab1d1d;
  cursor: pointer;
}
.right_item_mrgn{
  margin-top: 27px;
  margin-right: 27px;

}
</style>
<section class="content-header">
  <h1>Production</h1>
</section>
<?= $this->Form->create('Production', array('url' => array('controller' => 'Production', 'action' => 'Production')));?>
<section class="content">
  <div class="box box-primary">
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="pull-right right_item_mrgn">
          <a href="<?php echo $this->webroot ?>Production/ProductionList"><button type="button" style="margin-left:20px" type="button" class="btn btn-success save pull-right">Production List</button></a>
        </div>
      </div>
    </div>
    <div class="row" style="margin-top: 1%;">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-2 col-lg-2 col-sm-2">
          <label style="white-space: nowrap;">Product</label>
          <?php if(isset($ProductionItem)) : ?>
            <?= $this->Form->input('product_id',['type'=>'text','style'=>'width:100%','id'=>'product','class'=>'form-control','required','readonly','value'=>$this->request->data['Product']['name'],'label'=>false,]); ?>
          <?php else : ?>
            <?= $this->Form->input('product_id',['type'=>'select','style'=>'width:100%','id'=>'product','class'=>'form-control select_two_class','required','empty'=>[''=>'Select'],'options'=>$Product_list,'label'=>false,]); ?>
          <?php endif; ?>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <!-- <div class="col-md-6 col-lg-6 col-sm-6" hidden="">
            <?= $this->Form->input('start_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'start_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
          </div>
          <div class="col-md-6 col-lg-6 col-sm-6" hidden="">
           <?= $this->Form->input('end_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'end_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
         </div> -->
<!--           <div class="col-md-6 col-lg-6 col-sm-6">
 -->           <?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
<!--          </div>
 -->       </div>
<!--         <div class="col-md-1 col-lg-1 col-sm-1">
          <?= $this->Form->input('start_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'start_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
        </div>
        <div class="col-md-1 col-lg-1 col-sm-1">
          <?= $this->Form->input('end_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'end_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
        </div> -->

        <div class="col-md-1 col-lg-1 col-sm-1">
          <label style="white-space: nowrap;">Production No</label>
          <?= $this->Form->input('production_no',['type'=>'text','style'=>'width:100%','id'=>'production_no','class'=>'form-control','readonly','required','label'=>false,]); ?>
        </div>

        <div class="col-md-1 col-lg-1 col-sm-1">
          <label style="white-space: nowrap;">Production Cost</label>
          <?= $this->Form->input('production_cost',['type'=>'text','style'=>'width:100%','id'=>'production_cost','class'=>'form-control production_calculation','readonly','required','label'=>false,]); ?>
        </div>
        <div class="col-md-1 col-lg-1 col-sm-1">
          <label style="white-space: nowrap;">Production Qty</label>
          <?= $this->Form->input('production_quantity',['type'=>'text','style'=>'width:100%','id'=>'production_quantity','class'=>'form-control production_calculation main_calculator','required','label'=>false,]); ?>
        </div>
        <div class="col-md-1 col-lg-1 col-sm-1">
          <label style="white-space: nowrap;">Total Cost</label>
          <?= $this->Form->input('total_production_cost',['type'=>'text','style'=>'width:100%','id'=>'total_production_cost','class'=>'form-control','readonly','required','label'=>false,]); ?>
        </div>
          <div class="col-md-1 col-lg-1 col-sm-1">
          <label style="white-space: nowrap;">Product Cost</label>
          <?= $this->Form->input('product_cost',['type'=>'text','style'=>'width:100%','id'=>'product_cost','class'=>'form-control','readonly','required','label'=>false,]); ?>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <div class="form-group">
           
            <?= $this->Form->input('staff',['type'=>'select','style'=>'width:100%','id'=>'staff','class'=>'form-control select_two_class','required','empty'=>[''=>'Select'],'label'=>'Tailor',]); ?>
            
          </div>
        </div>
        <div class="col-md-1 col-lg-1 col-sm-1">
          <?php if(isset($ProductionItem)) : ?>
            <button type='submit' name='data[Production][process]' value='update' class=" btn btn_save_ftr_2 pull-right invoice-action" >Update</button>
          <?php else : ?>
            <button type='submit' name='data[Production][process]' value='save' class=" btn btn_save_ftr_2 pull-right invoice-action" >Save</button>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-12 col-lg-12 col-sm-12">
          <h3 class="table_main_heading" style="margin-top: 2%;">input</h3>
          <div class="box-body table-responsive">
            <table class="table boder_table" style="margin-top: 0%;" id='production_input_table'>
              <thead>
                <tr class="blue-bg">
                  <th style="width: 45%;">Product</th>
                  <th>Unit</th>
                  <th>Quantity</th>
                  <th>Total Quantity</th>
                   <th>Product Cost</th>
                   <th>Total Cost</th>
                  <th>Action</th>

                </tr>
                <tr>
                </tr>
              </thead>
              <tbody>
                <?php if(isset($ProductionItem)) : ?>
                  <?php 
                  $row_total=0;
                  foreach ($ProductionItem as $key => $value): ?>
                    <tr class="blue-pddng">
                      <td>
                        <input class="productlist productsrow" hidden name="data[ProductionItem][product_id][]" value="<?= $value['ProductionItem']['product_id']; ?>">
                        <input value='<?= $value['Product']['name']; ?>' class='form-control' readonly type='text'>
                        <input value='<?= $value['ProductionItem']['id']; ?>' name='data[ProductionItem][ProductionItem_id][]' type='hidden' class='table_id'>

                      </td>
                      <td><input class='form-control' value='<?= $value['Unit']['name']; ?>' readonly><input value='<?= $value['Unit']['id']; ?>'  name='data[ProductionItem][unit_id][]' type='hidden'></td>
                      <td><input class='form-control quantity' readonly value='<?= $value['ProductionItem']['single_production_quantity']; ?>' name='data[ProductionItem][quantity][]'></td>
                      <td><input class='form-control total_quantity' value='<?= $value['ProductionItem']['total_quantity']; ?>' readonly name='data[ProductionItem][total_quantity][]'></td>
                      <td><input class='form-control single_product_cost' value='<?= $value['Product']['cost']; ?>' readonly name='data[ProductionItem][single_product_cost][]'></td>
                      <?php
                       $total=$value['ProductionItem']['total_quantity']*$value['Product']['cost'];
                       $row_total+=$total;
                       ?>
                      <td><input class='form-control total_cost main_calculator' value='<?= $total; ?>' readonly name='data[ProductionItem][total_cost][]'></td>
                      <td></td>
                    </tr>
                  <?php endforeach ?>
                <?php endif; ?>
              </tbody>
                <tfoot>
            <tr>            
             <?php if(isset($this->request->data['Production']['status'])==1){?>
              <th colspan="5" style="font-size:20px; color:red;text-align:right" class="row_total" hidden="">Total:</th>
              <td class="row_total"><input class='form-control' readonly id="total_cost_total" hidden=""></td>
              <?php }
              else
                {?>
              <th colspan="5" style="font-size:20px; color:red;text-align:right">Total:</th>
              <td><input class='form-control' value="<?= $row_total;?>" readonly id="total_cost_total"></td>
                  <?php }?>
                  <td></td>
            </tr>
            </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?= $this->Form->end(); ?>  
<script type="text/javascript">
  <?php require 'production.js'; ?>
</script>


