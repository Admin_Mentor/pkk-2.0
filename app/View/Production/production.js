$.fn.button_disable=function(){
	var length=$('#production_input_table tbody tr').length;
	if(length>0)
	{
		$('button[type="Submit"]').attr('disabled',false);
	}
	else
	{
		$('button[type="Submit"]').attr('disabled',true);
	}
};
$('#product').change(function(){

	var product=$('#product option:selected').val();
	$('#production_input_table tbody').empty();
	if(!product)
		return false;
	var url_address= '<?php echo $this->webroot; ?>'+'Production/get_bom_of_product/'+product;
		$.ajax({
		  type: "POST",
		  url:url_address,
		  dataType:'json',
		  success: function(data) {
		    if(data.result!='Success')
		    {
		      alert(data.result);
		      return false;
		    }
		   	$('#production_cost').val(data.Bom.production_cost);
		   	//$('#production_quantity').val(1);
		   	$('#total_production_cost').val(data.Bom.production_cost);
		   	$.each(data.BomItem,function(key,value){
		   		quantity=value.BomItem.quantity;
		   		total_quantity=quantity;
		   		product_cost=value.BomItem.product_cost;
		   		total_cost=total_quantity*product_cost;
		   		var remove_button='<i class="fa fa-minus-circle fa-2x fnt-awsm-btn remove_tr"></i>';
				var product_field='<input type="hidden" name="data[Production][product][]" value="'+value.BomItem.product_id+'" class="productsrow"><input type="text"  class="form-control" readonly value="'+value.BomItem.name+'" >';
				var quantity_field='<input type="text" name="data[Production][quantity][]" class="form-control number quantity" id="quantity" readonly  value="'+quantity+'">'+'';
				var total_quantity_field='<input type="text" name="data[Production][total_quantity][]" class="form-control total_quantity"  readonly value="'+total_quantity+'">'+'';
				var product_cost='<input type="text" name="data[Production][single_product_cost][]" class="form-control single_product_cost"  readonly value="'+product_cost+'">'+'';
				var total_cost='<input type="text" name="data[Production][total_cost][]" class="form-control total_cost main_calculator"  readonly value="'+total_cost+'">'+'';
				var unit_field='<input type="hidden" name="data[Production][unit_id][]" value="'+value.BomItem.unit_id+'"><input type="text" class="form-control" readonly value="'+value.BomItem.unit+'">'+'';
				var sl=key+1;
				$('#production_input_table tbody').append('<tr class="blue-pddng">\
				  <td>'+product_field+'</td>\
				  <td>'+unit_field+'</td>\
				  <td>'+quantity_field+'</td>\
				  <td>'+total_quantity_field+'</td>\
				  <td>'+product_cost+'</td>\
				   <td>'+total_cost+'</td>\
				  <td>'+remove_button+'</td>\
				  </tr>'); 
			$.fn.main_calculator();
		   	});
		   	$('.row_total').show();
		  $.fn.button_disable();
		  },
		  error:function (XMLHttpRequest, textStatus, errorThrown) {
		    alert(textStatus);
		  }
		});
})
$.fn.main_calculator=function(){
	var total=0;
	var total_quantity=0;
	$('#production_input_table tbody tr.blue-pddng').each(function(){
		var total_cost=$(this).closest('tr').find('td input.total_cost').val();
		total=parseFloat(total_cost)+parseFloat(total);
		var quantity=$(this).closest('tr').find('td input.total_quantity').val();
		total_quantity=parseFloat(total_quantity)+parseFloat(quantity);
	});
	$('#total_quantity_total').val(total_quantity);
	$('#total_cost_total').val(total.toFixed(4));
 var production_cost=$('#total_production_cost').val();
 if(!($('#production_quantity').val()))
 {
 	var production_quantity=0;
 	var average_product_cost=0;
 }
 else
 {
 	 var production_quantity=$('#production_quantity').val();
 	   var average_product_cost=(parseFloat(total)+parseFloat(production_cost))/production_quantity;
 }
   $('#product_cost').val(average_product_cost.toFixed(3));	
}
$('.production_calculation').keyup(function(){
  var production_cost=$('#production_cost').val();
  var production_quantity=$('#production_quantity').val();
  var total_production_cost=production_cost*production_quantity;
  $('#total_production_cost').val(total_production_cost.toFixed(3));
  $('#production_input_table tbody tr').each(function(){
    var quantity=$(this).closest('tr').find('td input.quantity').val();
     var cost=$(this).closest('tr').find('td input.single_product_cost').val();
    var total_quantity=production_quantity*quantity;
    var total_cost=total_quantity*cost;
    $(this).closest('tr').find('td input.total_quantity').val(total_quantity.toFixed(4));
     $(this).closest('tr').find('td input.total_cost').val(total_cost.toFixed(4));
  });
  	$.fn.main_calculator();

});
$(document).on('click','.remove_tr',function(){
  $(this).closest('tr').remove();
  $.fn.button_disable();
});
$.fn.button_disable();
$(document).on('click','.remove_old_tr',function(){
var row_index=$(this).parents('tr').index();
var id=$(this).closest('tr').find('td input.table_id').val();
var url_address= '<?php echo $this->webroot; ?>'+'Production/BomItem_delete/'+id;
$.ajax({
  type: "POST",
  url:url_address,
  dataType:'json',
  success: function(data) {
    if(data.result!='Success')
    {
      alert(data.result);
      return false;
    }
    $('#bom_input_table tbody tr:eq('+row_index+')').remove();
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
$.fn.button_disable();
});
$('form').submit(function(e) {
    // if the form is disabled don't allow submit
    if ($(this).hasClass('disabled')) {
        e.preventDefault();
        return;
    }
    $(this).addClass('disabled');
});