<style type="text/css">
  <?php include "style.css" ?>
  .ad-stk{
    color: #FFF !important;
    background-color: #13689e !important;
    padding: 5px 20px !important;
    border: 0px solid #FFF !important;
    border-radius: 3px !important;
    transition: all ease .5s;
    margin-top: 20%;
  }
  .ad-stk:hover {
    background-color: #023a5e !important;
  }
  .list-arrows {
    padding-top: 100px;
  }
  .list-arrows button {
    margin-bottom: 20px;
  }
  .mr-tp-30 {
    margin-top: 30px;
  }
</style>
<section class="content-header">
  <h1> Daily Stock Transfer  <a href="<?php echo $this->webroot ?>ProductionPlan/DailyTransferList"><button class='btn btn-success pull-right'>Daily Stock Transfer List</button></a></h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header"> 
    </div>
    <div class="box-body"> 
      <?= $this->Form->create('DailyStockTransfer', array('url' => array('controller' => 'ProductionPlan', 'action' => 'DailyStockTransfer')));?>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-md-12">
        <div class="col-md-2 col-sm-2 col-md-2">
              <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$date)); ?>
          </div>
          <div class="col-md-2 col-sm-2 col-md-2">
              <?php echo $this->Form->input('warehouse_from',array('type'=>'select','value'=>'','options'=>$Warehouse_from,'class'=>'form-control select2 search_class','label'=>'From','id'=>'warehouse_from','readonly')) ?>                 
            </div>
        <div class="col-md-2 col-sm-2 col-md-2">
              <?php echo $this->Form->input('warehouse_id',array('type'=>'select','empty'=>'Select To Warehouse','value'=>'','options'=>$Warehouse,'class'=>'form-control select2 search_class','label'=>'To','id'=>'warehouse_id')) ?>                
            </div>
        <div class="col-md-2 col-sm-2 col-md-2">
         <?= $this->Form->input('remarks', array('type' => 'textarea','class'=>"form-control",'rows'=>3)); ?>
        </div>
         <div class="col-md-2 col-sm-2 col-md-2" style="margin-top:2%">
  <button  type="button" class='order_form btn btn-success save pull-right' >Export Order Form</button>
           </div>
    <div class="col-md-2 col-sm-2 col-md-2" style="margin-top:2%">
  <button  type="button" class='order_form_excel btn btn-primary  pull-right' >Excel Order Form </button>
          </div>

                </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <div class="box-body table-responsive no-padding boder" style="width:90%;margin-left:35px" >
            <table class="table table-hover" id="stock_transfer_tbl" >                    
              <thead>
                <tr  class="blue-bg">
                  <th>Slno</th>
                  <th width="35%">Product</th>
                  <th>Stock Quantity</th>
                  <th>Order Quantity</th>
                  <th>Quantity To Move</th>
                  <th>Number of tray</th>
                  <th width="20px"></th>
                </tr>
                </thead>
                <tbody>
          </tbody>
                <tfoot>
                   <tr>
                  <td></td><td></td><td></td><td></td>
            <td>Total</td>
            <td><input class="form-control total_tray"  readonly type="text" value='0' ></td>
                    </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
        <br/>
        <div class="row">
          <div class="modal-footer" style="width:90%;margin-left:35px">
            <button type="submit" id='save_button' disabled class="btn btn-success">Transfer</button>
            <!-- <button type="button" class="btn btn-default print">Print</button> -->
          </div>
        </div>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </section>
  <script type="text/javascript">
    $.fn.button_disable=function(){
      var length=$('#stock_transfer_tbl tbody tr').length;
      exit=0;
      if(length>0)
      {
        $("#stock_transfer_tbl tbody tr").each(function () {
         var tranfer_status = $(this).closest('tr').find('td input.tranfer_status').prop('checked');
        var product_id = $(this).closest('tr').find('td input.productsrow').val();
        if(product_id!=1)
        {
       if(tranfer_status==false)
       {
        exit=1;
            return false;
        }
        }
      });
        if(exit==1)
        {
        $('button[type="Submit"]').attr('disabled',true);
        }
        else
        {
        $('button[type="Submit"]').attr('disabled',false); 
        }
      }
      else
      {
        $('button[type="Submit"]').attr('disabled',true);
      }
    };
    $(document).on('change','#warehouse_id,#date,#warehouse_from',function(){
            var warehouse_to=$('#warehouse_id').val();
             var warehouse_from=$('#warehouse_from').val();
            if(!warehouse_to)
            {
              $('#warehouse_id').select2("open");
              return false;
            }
            if($('#warehouse_from').val() == $('#warehouse_id').val())
            {
            $.fn.show_alert('Select Diffrent Warehouse');
             $('#stock_transfer_tbl tbody').empty();
            $('#warehouse_id').val('').trigger('change.select2');
            return false;
            }
              var date=$('#date').val();
     $.post("<?= $this->webroot ?>ProductionPlan/daily_transfer_ajax/" + warehouse_to+'/'+date+'/'+warehouse_from, function (responds) {
    if (responds.result != 'Success')
    {
      alert(responds.result);
      return false;
    }
    var move_quantity_tray=0;
    $('#stock_transfer_tbl tbody').empty();
    $.each(responds.orderitem,function(key,value){
      var sl=key+1;
           move_quantity_tray=parseFloat(move_quantity_tray)+parseFloat(value.tray_occupation); 

     if(parseInt(value.id)==1)
      {
             var readonly='readonly'; 
      }
      else
      {
              var readonly='';
      }
      var move_quantity=value.quantity;
      $('#stock_transfer_tbl tbody').append('<tr class="blue-pddng">\
       <td>'+sl+'</td>\
        <td>\
       <input class="productlist productsrow" hidden name="data[DailyStockTransfer][product_id][]" value="'+value.id+'">\
       <input value="'+value.name+'" class="form-control" readonly type="text">\
        <input value="'+value.order_id+'" name="data[DailyStockTransfer][order_id][]" type="hidden" class="table_id">\
       </td>\
        <td>\
        <input id="quantity_simple" class="form-control current_quantity"  readonly value="'+value.stock_quantity+'">\
        </td>\
        <td>\
        <input id="quantity_simple" class="form-control order_quantity"  name="data[DailyStockTransfer][order_quantity][]" readonly value="'+value.quantity+'">\
        </td>\
        <td>\
        <input id="quantity_simple" class="form-control move_quantity" name="data[DailyStockTransfer][quantity][]"  value="'+move_quantity+'">\
        </td>\
        <td>\
        <input type="hidden"  id="hidden_tray_occupation" class="form-control hidden_tray_occupation"  name="data[DailyStockTransfer][hidden_tray_occupation][]" value="'+value.hidden_tray_occupation+'">\
        <input type="text"  id="tray_occuption" class="form-control tray_occupation number"   name="data[DailyStockTransfer][tray_occupation][]" value="'+value.tray_occupation+'" '+readonly+'>\
        </td>\
        <td><input type="checkbox" class="tranfer_status" style="height:25px;width:25px"/></td>\
        </tr>'); 
    });
$('.total_tray').val(move_quantity_tray);
        $.fn.button_disable();
  }, "json");
    });
$(document).on('change','.tranfer_status',function(){
  $.fn.button_disable();
  });
$(document).on('keyup','.tray_occupation',function(){
  var total_tray=0;
   $("#stock_transfer_tbl tbody tr").each(function () {
  var tray_occupation = $(this).closest('tr').find('td input.tray_occupation').val()? parseInt( $(this).closest('tr').find('td input.tray_occupation').val()):0;
             total_tray=parseFloat(total_tray)+parseFloat(tray_occupation); 
    });
   $('.total_tray').val(total_tray);

  });
$(document).on('keyup','.move_quantity',function(){
      var hidden_tray_occupation=$(this).closest('tr').find('td input.hidden_tray_occupation').val(); 
  var current_quantity = $(this).closest('tr').find('td input.current_quantity').val()? parseInt( $(this).closest('tr').find('td input.current_quantity').val()):0;
  var move_quantity=$(this).val();
  if(parseFloat(current_quantity)  >=  parseFloat(move_quantity)){
          var actual_move_quantity=move_quantity;
          if(hidden_tray_occupation!=0)
          {
          var stock_quantity=actual_move_quantity/hidden_tray_occupation;
          var whole = parseFloat(stock_quantity).toFixed(2); 
          var fraction = actual_move_quantity%hidden_tray_occupation; 
          var pieces=Math.floor(fraction);
          var tray_piece=0;
      if(parseFloat(pieces) >=1)
      {
        var tray_piece=1;
      }
          var tray=whole+tray_piece;
          $(this).closest('tr').find('td input.tray_occupation').val(whole);
          }else{
          var actual_move_quantity=move_quantity;
          $(this).closest('tr').find('td input.tray_occupation').val(0);
          }
  }
  else{
    $(this).val('');
    $(this).closest('tr').find('td input.tray_occupation').val(0);
  }
  $('.tray_occupation').keyup();
});
$(document).on('click','#save_button',function(){
    var check_stock=0;
     $("#stock_transfer_tbl tbody tr").each(function () {
  var current_quantity = $(this).closest('tr').find('td input.current_quantity').val()? parseInt( $(this).closest('tr').find('td input.current_quantity').val()):0;
    var move_quantity = $(this).closest('tr').find('td input.move_quantity').val()? parseInt( $(this).closest('tr').find('td input.move_quantity').val()):0;
    var product_id = $(this).closest('tr').find('td input.productsrow').val();
        if(product_id!=1)
        {
          // if(parseFloat(current_quantity)!=0)
          // {
            if((parseFloat(current_quantity)<=0 && parseFloat(move_quantity)!=0) || parseFloat(current_quantity) < parseFloat(move_quantity))
             {
              $(this).closest('tr').find('td input.move_quantity').focus();
              check_stock=1;
                return false;
              }
              else
              {
                check_stock=0;
              }
          //   }
          // else
          // {
          //   $(this).closest('tr').find('td input.move_quantity').val(0);
          //      $(this).closest('tr').find('td input.tray_occupation').val(0);
          //   check_stock=0;
          // }
      }
      else if(product_id==1)
      {
         if(parseFloat(move_quantity)<0 || parseFloat(current_quantity)<parseFloat(move_quantity))
       {
        $(this).closest('tr').find('td input.move_quantity').focus();
        check_stock=1;
            return false;
        }
      }
      else
      {
        check_stock=0;
      }
      if(!move_quantity)
     {
        $(this).closest('tr').find('td input.move_quantity').val(0);
     }
    });
     if(check_stock==1)
     {
    $.fn.show_alert("Please Change Quantity");
    return false;   
     }
});
$.fn.show_alert = function(flash)
  {
   $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
 }
 $(document).on('click','.order_form',function(){
   // alert("kk");
    var date=$('#date').val();
    var warehouse_id=$('#warehouse_id').val();
    if(!warehouse_id)
    {
      $('#warehouse_id').select2('open');
      return false;
    }
    url='<?= $this->webroot."ProductionPlan/OrderForm_second/"; ?>'+date+'/'+warehouse_id;
    window.open(url);
});
 $(document).on('click','.order_form_excel',function(){
   // alert("kk");
    var date=$('#date').val();
    var warehouse_id=$('#warehouse_id').val();
    if(!warehouse_id)
    {
      $('#warehouse_id').select2('open');
      return false;
    }
    var url='<?= $this->webroot."ProductionPlan/order_form_excel/"; ?>'+date+'/'+warehouse_id;
     $(location).attr("href", url);
});
    $.fn.button_disable();
</script>
