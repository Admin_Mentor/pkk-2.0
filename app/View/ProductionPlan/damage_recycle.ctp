<style type="text/css">
.algn_lft{
  text-align: center !important;
}

</style>
<section class="content-header">
  <h2>New Damage Recycle <a href="<?php echo $this->webroot ?>ProductionPlan/DamageRecycleList"><button class='btn btn-success pull-right btn_bdr_radious'>Recycled List</button></a></h2>
</section>
<section class="content">
  <div class="row-wrapper">
  <div class="box box-primary">
    <?= $this->Form->create('DamageRecycle', array('url' => array('controller' => 'ProductionPlan', 'action' => 'DamageRecycle')));?>
    <div class="row">
      <div class="col-md-8">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-md-2 control-label algn_lft">Date</label>
              <div class="col-md-5">
                <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-md-2 control-label">Remarks</label>
              <div class="col-md-8">
                <?php echo $this->Form->input('remarks',array('type'=>'textarea','id'=>'remarks','class'=>'form-control','label'=>false,'rows'=>2)); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    <div class="box-body table-responsive no-padding">
      <div class="col-md-12">
        <table class="table table-condensed table text-center table-bordered" id="product_table">
          <thead>
            <tr class="blue-bg">
              <th>Nos</th>
              <?php if(!isset($this->request->data['DamageRecycle']['id'])){?>
              <th width="40%">Product</th>
               <th>Damage Quantity</th>
                <th>Quantity to be Recycle</th>
                 <th>Recycled Quantity</th>
                <th width="40%">Recycled Product</th>
                 <th>Action</th>
              <?php } ?>
              <?php if(isset($this->request->data['DamageRecycle']['id'])){?>
              <th width="40%">Recycled Product</th>
                 <th>Recycled Quantity</th>
              <?php } ?>
            </tr>
            <?php if(!isset($this->request->data['DamageRecycle']['id'])){?>
            <tr>
              <td>#</td>
              <td>
                <?php echo $this->Form->input('product_simple',['type'=>'select','style'=>'width:100%','id'=>'product_simple','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'label'=>false,'options'=>$Product]); ?>
              </td>
              <td><input type="text" id="damage_quantity" class=" form-control " value="0" readonly></td>
              <td><input type="text" id="recycle_quantity" class=" form-control " value=""></td>
               <td><input type="text" id="recycled_quantity" class=" form-control " value=""></td>
                <td><?php echo $this->Form->input('recycle_as',['type'=>'select','style'=>'width:100%','id'=>'recycle_as','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'label'=>false,'options'=>$Productsall]); ?>
              </td>
              <td><i class="fa fa-plus-circle fa-2x add_to_cart_simple"></i></td>
            </tr>
            <?php } ?>
          </thead>
          <tbody>
             <?php if(isset($DamageRecycleItem)) : foreach ($DamageRecycleItem as $key => $value) :?>
                    <tr>
                      <td><?= $key+1; ?></td>
                      <td>
                        <?= $this->Form->input('product_id',array('class'=>'form-control','label'=>false,'id'=>'','value'=>$value['Product']['name'],'type'=>'text','readonly')) ?> </td>
                        <td><input class="form-control " id="" type="text" readonly="readonly" value=<?= floatval($value['DamageRecycleItem']['recycled_quantity']);?>></td>
                      </tr>
                    <?php endforeach; endif; ?>

          </tbody>
          <tfoot>
            <?php if(!isset($this->request->data['DamageRecycle']['id'])){?>
            <tr class="blue-pddng">
                <td colspan="5"></td>
                <td><button type='submit' id="" name='data[DamageRecycle][process]' value='save' class="btn btn-success pull-right create_icon" disabled>SAVE</button></td>
                <td></td>
            </tr>
            <?php } ?>
          </tfoot>
        </table>
      </div>
    </div>
    <?= $this->Form->end(); ?>
  </div>
      </div>
</section>
<script type="text/javascript">
$(document).on('change','#product_simple',function(){
    var id=$('#product_simple').val();
    if(!id)
    {
        return false;
    }
    var data={product_id:id};
    var url_address= '<?php echo $this->webroot; ?>'+'ProductionPlan/get_damage_production_quantity/';
    $.ajax({
        type: "POST",
        url:url_address,
        data:data,
        dataType:'json',
        success: function(data) {
            if(data.result!='Success')
            {
                return false;
            }
            $('#damage_quantity').val(data.damage_quantity);
            $('#recycle_quantity').focus();
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
});
$.fn.button_disable=function(){
    var length=$('#product_table tbody tr').length;
    if(length>0)
    {
        $('button[type="Submit"]').attr('disabled',false);
    }
    else
    {
        $('button[type="Submit"]').attr('disabled',true);
    }
};
$(".add_to_cart_simple").click(function(){
    event.preventDefault();
    var ProductExist = 0;
    var product_id=$('#product_simple').val();
    if(!product_id){
        $('#product_simple').select2('open');
        return false;
    }
    var damage_quantity=$('#damage_quantity').val();
    var recycle_quantity=$('#recycle_quantity').val();
    var recycled_quantity=$('#recycled_quantity').val();
    if(!$.isNumeric(recycle_quantity) || recycle_quantity=='' || recycle_quantity<=0 )
    {
        $('#recycle_quantity').focus();
        return false;
    }
     if(!$.isNumeric(recycled_quantity) || recycled_quantity=='' || recycled_quantity<=0 )
    {
        $('#recycled_quantity').focus();
        return false;
    }
    if(parseFloat(recycle_quantity)>parseFloat(damage_quantity ))
    {
        $('#recycle_quantity').focus();
    $ .alert("Insuffcient Stock", {title:'Flash Message',type: 'info',position: ['top-right', [60, 0]],});
         return false;
    }
     var recycle_as=$('#recycle_as').val();
    if(!recycle_as){
        $('#recycle_as').select2('open');
        return false;
    }
    var i=0;
    $("#product_table tbody tr").each(function () {
        i=i+1;
        var productId = $(this).closest('tr').find('td input.productsrow').val();
        var table_product_unit = $(this).closest('tr').find('td:eq(1) input:eq(0)').val();
        if (product_id== productId){
            ProductExist = 1;
            return false;
        }
    });
    if(ProductExist)
    {
        alert("This product and warehouse already in cart");
        $('#product').select_two_class('open');
        return false;
    }
    var product_text=$('#product_simple option:selected').text();
     var recycle_as_text=$('#recycle_as option:selected').text();
      var length=$('#product_table tbody tr').length;
                length=length+1;
    $('#product_table tbody').append('<tr class="blue-pddng">\
        <td>'+length+'</td>\
        <td>\
        <input class="productlist productsrow" hidden name="data[DamageRecycle][product_id][]" value="'+product_id+'">\
        <input class="form-control" value="'+product_text+'" readonly>\
        </td>\
        <td colspan="2" class="qty"><input type="hidden" name="data[DamageRecycle][damage_quantity][]" value="'+damage_quantity+'" >\
        <input class="form-control" readonly name="data[DamageRecycle][recycle_quantity][]" value="'+recycle_quantity+'" ></td>\
         <td class="qty"><input class="form-control" readonly name="data[DamageRecycle][recycled_quantity][]" value="'+recycled_quantity+'" ></td>\
         <td>\
        <input class="productlist" hidden name="data[DamageRecycle][recycle_as][]" value="'+recycle_as+'">\
        <input class="form-control" value="'+recycle_as_text+'" readonly>\
        </td>\
        <td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
        </tr>');    
                    length=length+1;
    $("#recycle_quantity").val('');
    $("#recycled_quantity").val('');
    $('#product_simple').val('').trigger('change.select2');
    $('#recycle_as').val('').trigger('change.select2');
    $.fn.button_disable();
});
$(document).on('keypress','#recycle_quantity,#recycled_quantity',function(e){
     if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57 || e.which==47)) {
        return false;
    }
   });
$(document).on('click','.remove_tr',function(){
  $(this).closest('tr').remove();
$.fn.button_disable();
});
</script>
