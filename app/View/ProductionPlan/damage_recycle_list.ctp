<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js" type="text/javascript" language="javascript"></script>

<section class="content-header">
	<h2>Damage Production List
		<a href="<?php echo $this->webroot ?>ProductionPlan/DamageRecycle"><button class='btn btn-success pull-right'>New</button></a>
	</h2>
</section>
<section class="content">
	<div class="box">
		<div class="row">
			<div class="col-md-3 col-lg-2 col-sm-12 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$from,'label'=>'From Date')); ?>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-lg-2 col-sm-12 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$to,'label'=>'To Date')); ?>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="form-group"><br>
					<button class='btn btn-success' id='get_button'>Get</button>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box-body">
					<table class="boder table table-condensed table" id="table_data" data-order='[[ 1, "asc" ]]' data-page-length='10'>
						<thead>
							<tr class="blue-bg">
								<th>Date</th>
								<th>Remarks</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$('#table_data').DataTable({
		"processing": true,
		"serverSide": true,
		dom: 'Bfrtip',
		buttons: [
		{ extend: 'colvis', },
		{ extend: 'csv',   footer: false, exportOptions: { columns: ':visible' } },
		{ extend: 'excel', footer: false, exportOptions: { columns: ':visible' } },
		{ extend: 'pageLength', },
		],
		"lengthMenu": [[10,25,50,-1], [10,25,50,"All"]],
		"ajax": {
			"url": "<?= $this->webroot ?>ProductionPlan/get_recycle_list",
			"type": "POST",data:function( d ) {
				d.from_date= $('#from_date').val();
				d.to_date= $('#to_date').val();
			},
			"dataSrc": "records",
		},
		"columns": [
		// { "data" : "ProductionPlan.date" },
		// { "data" : "ProductionPlan.production_no" },
		{ "data" : "DamageRecycle.date" },
		{ "data" : "DamageRecycle.remarks" },
		{ "data" : "DamageRecycle.action" },
		],
		"columnDefs": [
		],
	});
	$('#get_button').click(function(){
		table = $('#table_data').dataTable();
		table.fnDraw();
	});
</script>