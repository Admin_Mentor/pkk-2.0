var unit_change_flag=0;
$.fn.button_disable=function(){
	var length=$('#ingredient_input_table tbody tr').length;
	if(length>0)
	{
		$('button[type="Submit"]').attr('disabled',false);
	}
	else
	{
		$('button[type="Submit"]').attr('disabled',true);
	}
};
$('#add_ingredient_input').click(function(){
	var product=$('#product option:selected').val();
	var product_text=$('#product option:selected').text();
	var brand_id=$('#brand_id option:selected').val();
	var brand_id_text=$('#brand_id option:selected').text();
	var product_in=$('#product_in option:selected').text();
	var product_in_id=$('#product_in option:selected').val();
	var unit_id=$('#unit_id option:selected').val();
	var unit_text=$('#unit_id option:selected').text();
	var quantity=$('#quantity').val();
	if(product_in_id==product)
	{
		alert("Select Product other than "+product_text);
		return false;
	}
	ProductExist=0;
	$("#ingredient_input_table tbody tr").each(function () {
		var productId = $(this).closest('tr').find('td input.productsrow').val();
		if (product_in_id== productId) {
			ProductExist=1;
		}
	});
	if(ProductExist==1)
	{
		alert("This product already in cart");
		$('#product_in').select2('open');
		return false;
	}
	if(!product_in_id){
		$('#product_in').select2('open');
		return false;
	}
	if(!unit_id){
		$('#unit_id').select2('open');
		return false;
	}
	if(!quantity){
		$('#quantity').focus();
		return false;
	}
	$('#ingredient_input_table tbody').prepend('<tr>\
		<td><input class="" hidden name="data[Ingredient][brand_id][]" value="'+brand_id+'"><input class="form-control" value="'+brand_id_text+'" readonly></td>\
		<td>\
		<input class="productlist productsrow" hidden name="data[Ingredient][product_in_id][]" value="'+product_in_id+'">\
		<input class="form-control product_name" value="'+product_in+'" readonly>\
		<td><input name="data[Ingredient][unit_id][]" hidden class="productlist productsrow" value='+unit_id+'>\
		<input class="form-control quantity_mode" value="'+unit_text+'" readonly>\
		</td>\
		<td><input class="form-control quantity" name="data[Ingredient][quantity][]" value='+quantity+' ></td>\
		<td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
		</tr>');
	$('#quantity').val('');
	$('#product_in').val('').trigger('change.select2');
	$('#unit_id').val('').trigger('change.select2');
	$('#product_in').select2('open');
	$.fn.button_disable();
});
$(document).on('click','.remove_tr',function(){
  $(this).closest('tr').remove();
  $.fn.button_disable();
});
$.fn.button_disable();
$(document).on('click','.remove_old_tr',function(){
var row_index=$(this).parents('tr').index();
var id=$(this).closest('tr').find('td input.table_id').val();
var url_address= '<?php echo $this->webroot; ?>'+'ProductionPlan/IngredientsItem_delete/'+id;
$.ajax({
  type: "POST",
  url:url_address,
  dataType:'json',
  success: function(data) {
    if(data.result!='Success')
    {
      alert(data.result);
      return false;
    }
    $('#ingredient_input_table tbody tr:eq('+row_index+')').remove();
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
$.fn.button_disable();
});

 $(document).on('click','#ingredients_print',function(){
  var product_id=$('#product_id').val();
  //alert(product_id);
  url='<?= $this->webroot."ProductionPlan/ingredients_print/"; ?>'+product_id;
  window.open(url);
});

$('#product_in').change(function(){
  unit_change_flag=0;
  $.fn.get_product_details(null);
});
$.fn.get_product_details=function(quantity_mode){
	var id=$('#product_in').val();
	var url_address= '<?php echo $this->webroot; ?>'+'Product/get_Product_ajax_sale/'+id;
	$.ajax({
		type: "POST",
		url:url_address,
		dataType:'json',
		success: function(data) {
			if(data.result!='Success')
			{
				alert(data.result);
				return false;
			}
			if(unit_change_flag==0)
	        {
	          $('#unit_id').val(data.unit_id).change();
	        }
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});  
}