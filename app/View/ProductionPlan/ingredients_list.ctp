<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js" type="text/javascript" language="javascript"></script>
<section class="content-header">
	<h1>Ingredients List<a  href="<?php echo $this->webroot ?>ProductionPlan/Ingredient"><input style="margin-left:20px"type="button" class="btn btn-success save pull-right" value="New Ingredients"></input></a></h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-body">
			<table class="boder table table-condensed table" id="table_data" data-order='[[ 0, "desc" ]]' data-page-length='25'>
				<thead>
					<tr class="blue-bg">
						<th>Ingredient Id</th>
						<th>Product</th>
						<th>Created At</th>
						<th>Updated At</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
		</div>
	</div>
</section>
<script type="text/javascript">
	$('#table_data').DataTable( {
		"processing": true,
		"serverSide": true,
		dom: 'Bfrtip',
		buttons: [
		{ extend: 'colvis', },
		{ extend: 'csv',   footer: false, exportOptions: { columns: ':visible' } },
		{ extend: 'excel', footer: false, exportOptions: { columns: ':visible' } },
		{ extend: 'pageLength', },
		],
		"lengthMenu": [[10,25,50,-1], [10,25,50,"All"]],
		"ajax": {
			"url": "<?= $this->webroot ?>ProductionPlan/IngredientList_ajax",
			"type": "POST",
			data:function( d ) {
				// d.from_date= $('#from_date').val();
				// d.to_date= $('#to_date').val();
			
			},
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "Ingredient.id" },
		{ "data" : "Product.name" },
		{ "data" : "Ingredient.created_at" },
		{ "data" : "Ingredient.updated_at" },
		{ "data" : "Ingredient.action" },
		],
		"footerCallback": function ( row, data, start, end, display ) {},
		"columnDefs": [
    	// { "targets": [ 7 ],"visible": false, },//modified_count
    	// { "targets": [ 8 ],"visible": false, },//created_at
    	// { "targets": [ 9],"visible": false, },//updated_at
    	],
    });
</script>