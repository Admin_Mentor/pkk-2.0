<style type="text/css">
.algn_lft{
  text-align: center !important;
}
.pdng_tp{
  margin-top: 15px;
}
.clr_star {
  color: #a29da2;
}
.cart_clr {
  color: #a29da2;
}
.pgn_btm_rht{
  margin-bottom: 15px;
}
.color_of{
  color :red !important;
}
.size_of
{
  font-size:25px !important;
  padding-left: 55px !important;
}


</style>
<section class="content-header">
  <h2>Labour Calculation</h2>
</section>
<section class="content">
  <div class="row-wrapper">
    <div class="box box-primary">
            <div class="box-header">
          <?= $this->Form->create('LabourCalculation', array('url' => array('controller' => 'ProductionPlan', 'action' => 'LabourCalculation')));?>
            <div class="form-group">
              <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12">
          <?= $this->Form->input('month',array('type'=>'text','class'=>'form-control datepicker_month','id'=>'date','required','data-inputmask'=>"'alias': 'mm-yyyy'",'data-mask'=>'data-mask')); ?>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
      <?php echo $this->Form->input('product_id',['type'=>'select','style'=>'width:100%','id'=>'product_id','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'options'=>$Product_list,'required']); ?>       
       </div>
        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
          <?= $this->Form->input('quantity',array('type'=>'text','class'=>'form-control number','id'=>'quantity','required')); ?>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
          <?= $this->Form->input('labour_production',array('type'=>'text','class'=>'form-control number','id'=>'labour_production','required')); ?>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
          <?= $this->Form->input('labour_packing',array('type'=>'text','class'=>'form-control number','id'=>'labour_packing','required')); ?>
        </div>
        <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12"><br>
          <button type="submit" class="btn btn-success create_icon">Save</button>
        </div>
              <?= $this->Form->end(); ?>
            </div>
          </div>
        <div class="box-body">
          <table class="table boder table-condensed" id="table_data">
            <thead>
              <tr class="blue-bg">
                <th>Month</th>
                <th>Product</th>
                <th>Quantity</th>
                <th>Labour Production</th>
                <th>Labour Packing</th>
                <th width="10%">Action</th>
                <!-- <th>Delete</th> -->
              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot> 
            </tfoot>
          </table>
     </div>
  </div>
  </div>
</section>
<script type="text/javascript">
  $('#date').inputmask('99-9999',{placeholder:"mm/yyyy"});
  $(".datepicker_month").datepicker( {
    format: "mm-yyyy",
    viewMode: "months", 
    minViewMode: "months"
  });
  $('.number').keyup();
  $('.number').click(function(){
    $(this).select();
  });
  function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
  }
</script>
<script type="text/javascript">
  $('#table_data').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>ProductionPlan/LabourCalculationTable",
      "type": "POST",
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "LabourCalculation.month" },
    { "data" : "Product.name" },
    { "data" : "LabourCalculation.quantity" },
    { "data" : "LabourCalculation.labour_production" },
    { "data" : "LabourCalculation.labour_packing" },
    { "data" : "LabourCalculation.action" },
    ],
    "footerCallback": function ( row, data, start, end, display ) {},
    "columnDefs": [
    { "width": "20%", "targets": 1 },
    ],
  });
</script>