<style type="text/css">
.i_color_plus {
  color: #005082;
  margin-top: 3px;
}
.table_main_heading {
  margin: 0px;
  text-transform: capitalize;
  padding-left: 9px;
  padding-bottom: 10px;
}

.btn_save_ftr_2 {
  background-color: #194e6f;
  color: white;
  letter-spacing: 0.6px;
  text-transform: capitalize;
  border-radius: 3px !important;
  margin-top: 23px;
  border: navajowhite;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 6px;
  padding-bottom: 6px;
}

.boder_table {
  border: 1px solid #e2e2e2;
}

.sav_right_mrgn{
  margin-left: 15px;
}
.btn_modal_add {
  border: none;
  background-color: #860006;
  color: white;
  border-radius: 3px !important;
  letter-spacing: 0.6px;
}
.mdl_in_pls{
  margin-top: 27px;
}

.head_mdl {
  text-transform: capitalize;
  letter-spacing: 0.6px;
  font-size: 16px;
  padding-left: 15px;
  padding-bottom: 14px;
  color: #ab1d1d;
  cursor: pointer;
}
.right_item_mrgn{
  margin-top: 27px;
  margin-right: 27px;

}
</style>
<section class="content-header">
  <h1>Material Request</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="pull-right right_item_mrgn">
          <a href="<?php echo $this->webroot ?>ProductionPlan/MaterialRequestList"><button type="button" style="margin-left:20px" type="button" class="btn btn-success save pull-right">Material Request List</button></a>
        </div>
      </div>
    </div>
          <?= $this->Form->create('MaterialRequest', array('url' => array('controller' => 'ProductionPlan', 'action' => 'MaterialRequest')));?>
     <div class="row">
      <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
            
          </div>
        </div>
      </div>

      <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
            <div class="form-group" hidden>
              <div class="col-md-5 col-lg-5 col-sm-5"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Production No:</label></div>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <?= $this->Form->input('production_no',array('class'=>'form-control','type'=>'text','required','readonly','id'=>'production_no','label'=>false,)); ?>
                <?= $this->Form->input('id',array('class'=>'form-control','type'=>'hidden','required','readonly')); ?>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-12 col-lg-12 col-sm-12">
          <div class="box-body table-responsive">
            <table class="table boder_table" style="margin-top: 0%;" id='production_input_table'>
              <thead>
                <tr class="blue-bg">
                  <th></th>
                  <th style="width: 45%;">Product</th>
                  <th>Order Quantity</th>
                  <th>Quantity to be Purchase</th>
                  <th>Unit</th>
                </tr>
              </thead>
              <tbody>
                 <?php if(isset($MaterialRequestItem)) : ?>
                  <?php 
                  foreach ($MaterialRequestItem as $key => $value1): ?>
                    <tr class="blue-pddng">
                      <td><?= $key+1;?></td>
                      <td><input class='form-control'  readonly value='<?= $value1['Product']['name']; ?>'></td>
                      <td><input class='form-control'  readonly value='<?= floatval($value1['MaterialRequestItem']['quantity']); ?>'></td>
                      <td><input class='form-control'  readonly value='<?= floatval($value1['MaterialRequestItem']['shortage']); ?>'></td>
                      <td><input class='form-control'  readonly value='<?= $value1['Unit']['name']; ?>'></td>
                    </tr>
                  <?php endforeach ?>
                <?php endif; ?>
              </tbody>
              <tfoot>
                <tr class="blue-pddng">
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><button type="button" id="material_request_print" class="btn btn-primary btn_radious" style="background-color: #4CAF50;">Print</button></td>
              </tr>
              </tfoot>
            </table>
          </div>
          <?= $this->Form->end(); ?>
        </div>
      </div>
    </div>

  </div>
</section>
<script type="text/javascript">
  $(document).on('click','#material_request_print',function(){
  var production_no=$('#production_no').val();
  url='<?= $this->webroot."ProductionPlan/material_request_print/"; ?>'+production_no;
  window.open(url);
});
</script>


