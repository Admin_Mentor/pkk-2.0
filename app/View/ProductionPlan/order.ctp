<style type="text/css">
.algn_lft{
  text-align: center !important;
}
.pdng_tp{
  margin-top: 15px;
}
.clr_star {
  color: #a29da2;
}
.cart_clr {
  color: #a29da2;
}
.pgn_btm_rht{
  margin-bottom: 15px;
}
.color_of{
  color :red !important;
}
.size_of
{
  font-size:25px !important;
  padding-left: 55px !important;
}


</style>
<section class="content-header">
  <h1>ORDER <a href="<?php echo $this->webroot ?>ProductionPlan/OrderIndex"><button class='btn btn-success pull-right btn_bdr_radious'>Order List</button></a></h1>
</section>
<section class="content">
  <div class="row-wrapper">
    <div class="box box-primary">
      <?php $status_flag=$this->request->data['Order']['status_flag'];?>
       <?php $delivery_days=$this->request->data['Order']['delivery_days'];?>
      <?php $status=$this->request->data['Order']['status']; ?>
      <?php $product_configuration_type=$Profile['Profile']['product_configuration_type']; ?>
      <?php $Country=$Profile['State']['country_id']; ?>
   
    <?= $this->Form->create('Order', array('url' => array('controller' => 'ProductionPlan', 'action' => 'Order')));?>
    <div class="row">
      <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
             <div class="form-group">
              <label for="inputEmail3" class="col-md-2 control-label algn_lft">Executive</label>
                    <?php if($status_flag==0){?>
              <div class="col-md-7">
                <?php echo $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','class'=>'form-control select2 customer_get_class','style'=>'width: 100%','options'=>['' => 'Select',$Executive_list],'label'=>false,'required'=>'required')); ?>
              </div>
            <?php }
            else 
            {
               ?>
               <div class="col-md-7">
                <?php echo $this->Form->input('executive_name',array('type'=>'text','class'=>'form-control customer_get_class','id'=>'executive_name','disabled','label'=>false,)); ?>
                <?php echo $this->Form->input('executive_id',array('type'=>'hidden','id'=>'executive_id')); ?>
              </div>
               <?php
            }
            ?>
            </div>
             <div class="form-group">
              <label for="inputEmail3" class="col-md-2 control-label algn_lft">Route</label>
                    <?php if($status_flag==0){?>
              <div class="col-md-7">
                <?php echo $this->Form->input('route_id',array('type'=>'select','id'=>'route_id','class'=>'form-control select2 customer_get_class ','style'=>'width: 100%','options'=>['' => 'Select',$Route_list],'label'=>false,'required'=>'required')); ?>
              </div>
            <?php }
            else 
            {
               ?>
               <div class="col-md-7">
                <?php echo $this->Form->input('route_name',array('type'=>'text','class'=>'form-control customer_get_class','id'=>'route_name','disabled','label'=>false,)); ?>
                <?php echo $this->Form->input('route_id',array('type'=>'hidden','id'=>'route_id')); ?>
              </div>
               <?php
            }
            ?>
            </div>
            <div class="form-group">
              <label for="inputEmail3" class="col-md-2 control-label algn_lft">Customer</label>
                    <?php if($status_flag==0){?>
              <div class="col-md-7">
                <?php echo $this->Form->input('account_head_id',array('type'=>'select','id'=>'customer_id','class'=>'form-control select2 customer_get_class','style'=>'width: 100%','options'=>['' => 'Select'],'label'=>false,'required'=>'required')); ?>
              </div>
            <?php }
            else 
            {
               ?>
               <div class="col-md-7">
               <?php echo $this->Form->input('account_head_name',array('type'=>'text','class'=>'form-control ','id'=>'account_head_name','disabled','label'=>false,)); ?>
                <div hidden><?php echo $this->Form->input('account_head_id',array('type'=>'hidden','id'=>'customer_id')); ?></div>
              </div>
               <?php
            }
            ?>
            </div>
           
            <div class="form-group">
              <label for="inputEmail3" class="col-md-2 control-label algn_lft">Address</label>
              <div class="col-md-7">
                <?= $this->Form->input('address',array('class'=>'form-control','type'=>'textarea','disabled','rows'=>2,'id'=>'address','label'=>false,)); ?>
              </div>
            </div> 
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Delivery Days</label></div>
              <div class="col-md-5 col-lg-5 col-sm-5">
                   <?php if($status_flag==0){?>
                <?php echo $this->Form->input('delivery_days',array('type'=>'select','id'=>'delivery_days','class'=>'form-control select2 customer_get_class ','style'=>'width: 100%','options'=>['' => 'Select',$delivery_days_list],'label'=>false,'required'=>'required')); ?>
                <?php echo $this->Form->input('delivery_days',array('type'=>'hidden','id'=>'delivery_days_hidden','class'=>'form-control','readonly','label'=>false)); ?>
                <?php } 
                else
                { ?>
                  <?php echo $this->Form->input('delivery_days',array('type'=>'text','id'=>'delivery_days_text','class'=>'form-control','readonly','label'=>false)); ?>
                  <div hidden>
                  <?php echo $this->Form->input('delivery_days',array('type'=>'select','id'=>'delivery_days','class'=>'form-control select2  ','style'=>'width: 100%','options'=>['' => 'Select',$delivery_days_list],'label'=>false,)); ?>
                </div>
              <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-5 col-lg-5 col-sm-5"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Order No:</label></div>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <?= $this->Form->input('order_no',array('class'=>'form-control','type'=>'text','required','readonly','id'=>'order_no','label'=>false,)); ?>
              </div>
            </div>
            <div class="form-group" style="display: none;">
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">customer Invoice No</label></div>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <?= $this->Form->input('invoice_no',array('class'=>'form-control','type'=>'text','id'=>'invoice_no','label'=>false,)); ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Date of order</label></div>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Date of Production</label></div>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <?php echo $this->Form->input('date_of_production',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date_of_production','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Date of Delivery</label></div>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <?php echo $this->Form->input('date_of_delivered',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date_of_delivered','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box-body table-responsive no-padding">
      <div class="col-md-12">
        <table class="table table-condensed table text-center table-bordered" id="product_table">
          <thead>
            <tr class="blue-bg">
              <th>Nos</th>
              <th width="60%">Product</th>
              <th hidden="">MRP</th>
              <th>Quantity</th>
                <th>Action</th>
            </tr>
            <?php if($status != 2)
            { ?>
            <tr>
              <td>#</td>
              <td>
                <?php echo $this->Form->input('product_simple',['type'=>'select','style'=>'width:100%','id'=>'product_simple','class'=>'form-control select2','empty'=>[''=>'Select'],'options'=>$Product_list,'label'=>false]); ?>
              </td>
              <td hidden=""><input type="text" id="mrp_simple" class="simple_single_calculator form-control button_click" value="0" readonly=""></td>
              <td><input type="text" id="quantity_simple" class="simple_single_calculator form-control button_click" value="0"></td>
              <td><i class=" remote fa fa-plus-circle create_icon fa-2x add_to_cart_simple"></i></td>
            </tr>
            <?php } ?>
          </thead>
          <tbody>
          <?php if(isset($OrderItems)) 
              { 
              foreach ($OrderItems as $key => $value){?>
                          <tr class="blue-pddng">
              <td><?=$key+1?></td>
              <td>
                  <input class="productlist productsrow" hidden name="data[Order][product_id][]" value="<?= $value['OrderItem']['product_id']; ?>">
                  <input value='<?= $value['Product']['name']; ?> <?= $value['Product']['code']; ?>' class='form-control' readonly type='text'>
                  <input value='<?= $value['OrderItem']['id']; ?>' name='data[Order][id][]' type='hidden' class='table_id'>
                </td>
              <td hidden>
              <input name="data[Order][mrp][]" value='<?= $value['OrderItem']['mrp']; ?>' class='form-control' type='text' style='width:100%;text-align:right' readonly></td>
              <td class="qty"><input type="text"  name="data[Order][quantity][]"  id="quantity_simple" class="simple_single_calculator form-control button_click cart_quantity" value='<?= floatval($value['OrderItem']['quantity']); ?>'></td>
              <?php if($status!=2) : ?>
              <td>
              <i class=" remote fa fa-minus-circle fa-2x remove_old_tr"></i>
              </td>
              <?php endif; ?>
            </tr>
            <?php } 
          }?>
          </tbody>
          <tfoot>
          <!--   <tr class = "blue-pddng">            
              <th colspan="2" style="font-size:20px; color:red;text-align:right">Total:</th>
              <th style="font-size:20px; color:red;" ></th>
              <th style="font-size:20px; color:red;"><span id='sum_total'></span></th>
              <th style="font-size:20px; color:red;"><span id="sum_qty"></span></th>
            </tr> -->
            <tr class="blue-pddng">
              <?php if(isset($OrderItems)) : ?>
                <?php $flag=$this->request->data['Order']['flag']; ?>
                <?php if($status==1) : ?>
                  <td><button type='submit' id="cancel_button" name='data[Order][process]' value='cancel' class="btn btn-warning">Cancel</button></td>
                  <td></td>
                  <td></td>
                  <td><button type='submit' id="order_delivery" name='data[Order][process]' value='update' class="btn btn-success edit_icon">Update</button></td>
                  <td></td>
                <?php elseif($status==3) : ?>
                  <td colspan="2"></td>
                  <td><button id="save_button" type='submit' name='data[Order][process]' value='after_altration' class="btn btn-success edit_icon">Save</button></td>
                <?php elseif($status==2) :  ?>
                  <td colspan="2"></td>
                  <td><button type="button" id="production_order_print" class="btn btn_radious btn_save_ftr_2 pull-right" style="background-color: #4CAF50;margin-left: 2%;">Print</button></td>
                <?php else : ?>
                  <td colspan="3"></td>
                  <td><button id="" type='submit' name='data[Order][process]' value='delete' class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button></td>
                    <td></td>
                <?php endif; ?>
              <?php else : ?>
                <td colspan="2"></td>
                <td><button type='submit' id="purchase_order" name='data[Order][process]' value='save'     class="btn btn-success pull-right create_icon" disabled>SAVE ORDER</button></td>
              <?php endif; ?>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
    <?= $this->Form->end(); ?>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function () {
 $("#customer_id").select2({
    placeholder: "Search Customer here...",
    width: '100%',
    ajax: {
      url: '<?= $this->webroot ?>ProductionPlan/Searchcustomer',
      dataType: 'json',
      delay: 250,
      data: function (params) {
            return {
                      q: params.term, // search term
                      page: params.page
                   };
          },
          processResults: function (data, params) {
            params.page = params.page || 2;
            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: false
        },
      minimumInputLength: 2,
      templateResult: formatRepo,
      templateSelection: formatRepoSelection
    });

  function formatRepo (repo) 
  {
    if (repo.loading) return repo.text;
    return repo.text;
  }
  function formatRepoSelection (repo)
  {
    return repo.text || repo.text;
  }
  });
</script>
<script type="text/javascript">
   <?php require('order.js'); ?>
   $(document).ready(function()
   {
      $('#delivery_days').val('<?= $delivery_days;?>').trigger('change');
   });

  var status='<?= $status; ?>';
   var status_flag='<?= $status_flag; ?>';
  if(status==2) $('.remote').hide();
</script>
<script type="text/javascript">
    $(document).on('click','.number',function(){
      $(this).select();
    });
</script>
