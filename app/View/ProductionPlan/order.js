$('#OrderOrderForm').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) { 
        e.preventDefault();
        return false;
    }
});

$('#customer_id').change(function(){
    var Customer_id=$(this).val();
    $.post( "<?= $this->webroot ?>Customer/get_customer_address_ajax/"+Customer_id ,function( data ) {
$('#address').val(data.address);
}, "json");
});
$('#executive_id').change(function(){
    var executive_id=$(this).val();
    $.post( "<?= $this->webroot ?>Executive/get_route_by_excutive/"+executive_id ,function( data ) {
$('#route_id').html('');
$('#route_id').html(data.option);
}, "json");
});
function roundToTwo(num) {
    return +(parseFloat(num).toFixed(3));
}
// $('.button_click').keyup(function(e){
//     var tr = $(this).parents('tr');
//     var quantity = $('#quantity_simple').val();
//     var mrp = $('#mrp_simple').val();
//     if (e.keyCode == 13) 
//     {
//         if(parseFloat(quantity)>0){
//             $(tr).find('.add_to_cart_simple').trigger('click');
//         }
//         else
//         {
//             $('#quantity_simple').focus();
//         }
//         return false;
//     }
// });
$(document).on('click','#order_delivery',function(){
    var exit=0;
     $("#product_table tbody tr").each(function () {
         var cart_quantity = $(this).closest('tr').find('td input.cart_quantity').val();
       if(parseFloat(cart_quantity)<0 || !cart_quantity)
       {
        $(this).closest('tr').find('td input.cart_quantity').focus();
        exit=1;
            return false;
        }
    });
     if(exit==1)
     {
    return false;   
     }
});

$(document).on('click','#production_order_print',function(){
   // alert("kk");
    var order_no=$('#order_no').val();
    url='<?= $this->webroot."ProductionPlan/production_order_print/"; ?>'+order_no;
    window.open(url);
});
$(document).on('click','.remove_old_tr',function(){
    var row_index=$(this).closest('tr').index();
    var id=$(this).closest('tr').find('td input.table_id').val();
    $('#product_table tbody tr:eq('+row_index+')').remove();
});
$('.main-sidebar').ready(function(){
    $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
});
$('button[type="Submit"]').click(function(){
    var total=$("#grand_total").val();
});
shortcut.add("alt+s", function() {
    $('#save_button').click();
});
$(document).on('change','#product_simple',function(){
    var id=$('#product_simple').val();
    var delivery_days=$('#delivery_days').val();
    if(!delivery_days)
    {
         $('#delivery_days').select2('open');
          $('#product_simple').val('').trigger('change.select2');
    }
    
});
$(document).on('change','#delivery_days',function(){
    var delivery_days=$('#delivery_days').val();
        if(!delivery_days)
    {
         delivery_days=3;
     }
        var newdate = new Date();
    newdate.setDate(newdate.getDate() + parseInt(delivery_days));
    var dd = newdate.getDate();
    var mm = newdate.getMonth()+1;
    var y = newdate.getFullYear();
    var someFormattedDate = dd + '-' + mm + '-' + y;
    if(status_flag==0)
    {
$('#date_of_delivered').val(someFormattedDate);
    }
    else
    {

    }
    var data={delivery_days:delivery_days};
    var url_address= '<?php echo $this->webroot; ?>'+'ProductionPlan/get_product_list/';
    $.ajax({
        type: "POST",
        url:url_address,
        data:data,
        dataType:'json',
        success: function(data) {
            $('#product_simple').html('');     
      $('#product_simple').append($("<option></option>").attr("value",'').text('Select'));
      $.each(data, function(i, value) {
        $('#product_simple').append($("<option></option>").attr("value", i).text(value));
      });
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
});
$(".add_to_cart_simple").click(function(){
    event.preventDefault();
    var ProductExist = 0;
    $("#customer_id").change();
    $("#delivery_days").attr('disabled',true);
     $("#delivery_days_hidden").val($("#delivery_days").val());
     var executive_id=$('#executive_id').val();
    if(!executive_id)
    {
        $('#executive_id').select2('open');
        return false;
    }
    var route_id=$('#route_id').val();
    if(!route_id)
    {
        $('#route_id').select2('open');
        return false;
    }
     var customer_id=$('#customer_id').val();
    if(!customer_id)
    {
        $('#customer_id').select2('open');
        return false;
    }
    var product_id=$('#product_simple').val();
    if(!product_id){
        $('#product_simple').select2('open');
        return false;
    }
    var product_quantity=$('#quantity_simple').val();
    if(!$.isNumeric(product_quantity) || product_quantity=='' || product_quantity<=0 )
    {
        $('#quantity_simple').focus();
        return false;
    }
    var product_mrp=$('#mrp_simple').val();
    if(!$.isNumeric(product_mrp) || product_mrp=='' || product_mrp<=0 )
    {
       // $('#mrp_simple').focus();
       // return false;
    }
    var i=0;
    $("#product_table tbody tr").each(function () {
        i=i+1;
        var productId = $(this).closest('tr').find('td input.productsrow').val();
        var table_product_unit = $(this).closest('tr').find('td:eq(1) input:eq(0)').val();
        if (product_id== productId) {
            ProductExist = 1;
            return false;
        }
    });
    if(ProductExist)
    {
        alert("This product already in cart");
        $('#product_simple').select2('open');
        return false;
    }
    var product_text=$('#product_simple option:selected').text();
    var search_barcode=$('#search_barcode').val();
        var length=$('#product_table tbody tr').length;
        var k=length+1;
    $('#product_table tbody').append('<tr class="blue-pddng">\
        <td>'+k+'</td>\
        <td>\
        <input class="productlist productsrow" hidden name="data[Order][product_id][]" value="'+product_id+'">\
        <input class="form-control" value="'+product_text+'" readonly>\
        </td>\
        <td class="qty" hidden><input class="form-control cart_quantity" readonly name="data[Order][mrp][]" value="'+product_mrp+'" ></td>\
        <td class="qty"><input class="form-control cart_quantity"  name="data[Order][quantity][]" value="'+product_quantity+'" ></td>\
        <td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
        </tr>');    
    $('#product_simple').select2('open');
    $('#quantity_simple').val('1');
    $('#search_barcode').val('');
    $("#quantity_simple").val('');
    $('#product_simple').val('').trigger('change.select2');
    $.fn.button_disable();
});
function roundToTwo(num) {
    return +(Math.round(num + "e+2")  + "e-2");
}
$.fn.button_disable=function(){
    var length=$('#product_table tbody tr').length;
    if(length>0)
    {
        $('button[type="Submit"]').attr('disabled',false);
    }
    else
    {
        $('button[type="Submit"]').attr('disabled',true);
        $('button.btn-danger').attr('disabled',false);
    }
};
$(document).on('click','.remove_tr',function(){
$(this).closest('tr').remove();
$.fn.button_disable();
});