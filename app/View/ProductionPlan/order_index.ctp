<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js" type="text/javascript" language="javascript"></script>
<section class="content-header">
	<h1>Order List
		<a href="<?php echo $this->webroot ?>ProductionPlan/Order"><button class='btn btn-success pull-right'>New Order</button></a>
	</h1>
</section>
<section class="content">
	<div class="box">
		<div class="row">
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$firstdate,'label'=>'Date of Order')); ?>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$todate,'label'=>'Date of Delivery')); ?>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('route_id',array('type'=>'select','id'=>'route_id','class'=>'form-control select_two_class customer_get_class ','style'=>'width: 100%','options'=>['' => 'All',$Route_list],'label'=>"Route")); ?>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','class'=>'form-control select_two_class customer_get_class','style'=>'width: 100%','options'=>['' => 'All',$Executive_list],'label'=>"Executive")); ?>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('account_head_id',array('type'=>'select','id'=>'customer_id','class'=>'form-control select2 customer_get_class','style'=>'width: 100%','options'=>['' => 'All'],'label'=>"Customer")); ?>
					</div>
				</div>
			</div>
			
			<!-- <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="form-group"><br>
					<button class='btn btn-success' id='get_button'>Get</button>
				
             </div>
			</div> -->
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box-body">
					<table class="boder table table-condensed table" id="table_data" data-order='[[ 2, "asc" ]]' data-page-length='10'>
						<thead>
							<tr class="blue-bg">
								<th>Order No:</th>
								<th>Date of Order</th>
								<th>Date of Delivery</th>
								<th>Route</th>
								<th>Executive</th>
								<th>Customer</th>
								<th>No Of Items</th>
								<th>Status</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
  $(document).ready(function () {
 $("#customer_id").select2({
    placeholder: "All",
    width: '100%',
    ajax: {
      url: '<?= $this->webroot ?>ProductionPlan/Searchcustomer',
      dataType: 'json',
      delay: 250,
      data: function (params) {
            return {
                      q: params.term, // search term
                      page: params.page
                   };
          },
          processResults: function (data, params) {
            params.page = params.page || 1;
            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: false
        },
      minimumInputLength: 1,
      templateResult: formatRepo,
      templateSelection: formatRepoSelection
    });

  function formatRepo (repo) 
  {
    if (repo.loading) return repo.text;
    return repo.text;
  }
  function formatRepoSelection (repo)
  {
    return repo.text || repo.text;
  }
  });
</script>
<script type="text/javascript">
	$('#table_data').DataTable({
		"processing": true,
		"serverSide": true,
		dom: 'Bfrtip',
		buttons: [
		{ extend: 'colvis', },
		{ extend: 'csv',   footer: false, exportOptions: { columns: ':visible' } },
		{ extend: 'excel', footer: false, exportOptions: { columns: ':visible' } },
		{ extend: 'pageLength', },
		],
		"lengthMenu": [[10,25,50,-1], [10,25,50,"All"]],
		"ajax": {
			"url": "<?= $this->webroot ?>ProductionPlan/order_search_ajax",
			"type": "POST",data:function( d ) {
				d.from_date= $('#from_date').val();
				d.type= 1;
				d.to_date= $('#to_date').val();
				d.route_id= $('#route_id').val();
				d.executive_id= $('#executive_id').val();
				d.customer_id= $('#customer_id').val();
			},
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "Order.order_no" },
		{ "data" : "Order.date" },
		{ "data" : "Order.date_of_delivered" },
		{ "data" : "Route.name" },
		{ "data" : "Executive.name" },
		{ "data" : "AccountHead.name" },
		{ "data" : "Order.no_of_items" },
		{ "data" : "Order.status" },
		{ "data" : "Order.action" },
		],
		"columnDefs": [
		],
	});
	$('#from_date,#to_date,#route_id,#executive_id,#customer_id').change(function(){
		table = $('#table_data').dataTable();
		table.fnDraw();
	});
</script>