<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>

<style type="text/css">
.i_color_plus {
  color: #005082;
  margin-top: 3px;
}
.table_main_heading {
  margin: 0px;
  text-transform: capitalize;
  padding-left: 9px;
  padding-bottom: 10px;
}

.btn_save_ftr_2 {
  background-color: #194e6f;
  color: white;
  letter-spacing: 0.6px;
  text-transform: capitalize;
  border-radius: 3px !important;
  margin-top: 23px;
  border: navajowhite;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 6px;
  padding-bottom: 6px;
}
.btn_save_ftr_1 {
  background-color:red;
  color: white;
  letter-spacing: 0.6px;
  text-transform: capitalize;
  border-radius: 3px !important;
  margin-top: 23px;
  border: navajowhite;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 6px;
  padding-bottom: 6px;
}
.boder_table {
  border: 1px solid #e2e2e2;
}

.sav_right_mrgn{
  margin-left: 15px;
}
.btn_modal_add {
  border: none;
  background-color: #860006;
  color: white;
  border-radius: 3px !important;
  letter-spacing: 0.6px;
}
.mdl_in_pls{
  margin-top: 27px;
}

.head_mdl {
  text-transform: capitalize;
  letter-spacing: 0.6px;
  font-size: 16px;
  padding-left: 15px;
  padding-bottom: 14px;
  color: #ab1d1d;
  cursor: pointer;
}
.right_item_mrgn{
  margin-top: 27px;
  margin-right: 27px;

}
</style>
<section class="content-header">
  <h1>Production <a href="<?php echo $this->webroot ?>ProductionPlan/ProductionPlanList"><button style="margin-left:20px" class="btn btn-success save pull-right">Production List</button></a>
     <?php if(!isset($this->request->data['ProductionPlan']['id'])) :?><a href=""<button type='button' style="margin-left:20px" class="btn btn-success save pull-right production_order_list" data-toggle="modal" data-target="#production_order_list" style="margin-right: -10%">Production Order List</button></a> <?php endif; ?> 
     <?php if($type_project==1)
     {?>
   <a href="<?php echo $this->webroot ?>ProductionPlan/OrderForm" target="_blank"><button  class='btn btn-success save pull-right' > Export Order Form</button></a>
   <?php } ?></h1>
</section>
<?= $this->Form->create('ProductionPlan', array('url' => array('controller' => 'ProductionPlan', 'action' => 'ProductionPlan')));?>
<section class="content">
  <div class="box box-primary">
    <?php $status=$this->request->data['ProductionPlan']['status']; ?>
    <?php $status_flag=$this->request->data['ProductionPlan']['status_flag']; ?>
    <div class="row" style="margin-top: 1%;">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-2 col-lg-2 col-sm-2" hidden>
           <?php if(isset($ProductionItem)) : ?>
          <?= $this->Form->input('date_of_order',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','readonly')); ?>
           <?php else : ?>
              <?= $this->Form->input('date_of_order',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-mask'=>'data-mask','placeholder'=>'Please select Order Date','readonly')); ?>
            <?php endif; ?>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <?= $this->Form->input('date_of_production',array('type'=>'text','class'=>'form-control pull-right','id'=>'date_of_production','readonly')); ?>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Date of Delivery</label>
          <?= $this->Form->input('date_of_delivered',array('type'=>'text','class'=>'form-control' ,'id'=>'date_of_delivered','label'=>false,'readonly')); ?>
        </div>
        <div class="col-md-1 col-lg-1 col-sm-1">
          <label style="white-space: nowrap;">Production No</label>
          <?= $this->Form->input('production_no',['type'=>'text','style'=>'width:100%','id'=>'production_no','class'=>'form-control','required','label'=>false,'readonly']); ?>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Product</label>
                     <?php if(isset($ProductionItem)) : ?>
             <?= $this->Form->input('product_name',['type'=>'text','style'=>'width:100%','id'=>'product_name','class'=>'form-control','label'=>false,'readonly']); ?>
          <?= $this->Form->input('product_id',['type'=>'hidden','style'=>'width:100%','id'=>'product_id','class'=>'form-control','required','label'=>false,'readonly']); ?>
             <?php else : ?>
              <?= $this->Form->input('product_id',['type'=>'select','style'=>'width:100%','id'=>'product_id','class'=>'form-control select_two_class','required','empty'=>[''=>'Select'],'options'=>$Product_array,'label'=>false,]); ?>
                      <?php endif; ?>

         </div>
          <div class="col-md-1 col-lg-1 col-sm-1">
          <label style="white-space: nowrap;">Production Labour</label>
          <?= $this->Form->input('production_labour',['type'=>'text','style'=>'width:100%','id'=>'production_labour','class'=>'form-control','required','label'=>false,'readonly']); ?>
        </div>
        <div class="col-md-1 col-lg-1 col-sm-1">
          <label style="white-space: nowrap;">Packing Labour</label>
          <?= $this->Form->input('packing_labour',['type'=>'text','style'=>'width:100%','id'=>'packing_labour','class'=>'form-control','required','label'=>false,'readonly']); ?>
        </div>
        <div class="col-md-2 col-lg-2 col-sm-2" hidden>
          <?php if(isset($ProductionItem)) : ?>
          <!--   <button type='submit' name='data[ProductionPlan][process]' value='update' class=" btn btn_save_ftr_2 pull-right invoice-action" >Update</button> -->
            <button type='button'  value='' class=" btn btn_save_ftr_2 pull-right bill_of_material" data-toggle="modal" data-target="#bill_of_material">Bill of Material</i></button>
          <?php else : ?>
            <button type='submit' name='data[ProductionPlan][process]' value='save' class=" btn btn_save_ftr_2 pull-right invoice-action" >Start Production</button>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-12 col-lg-12 col-sm-12">
          <h3 class="table_main_heading" style="margin-top: 2%;">input</h3>
          <div class="box-body table-responsive">
            <table class="table boder_table" style="margin-top: 0%;" id='production_input_table'>
              <thead>
                <tr class="blue-bg">
                  <th></th>
                  <th style="width: 45%;">Product</th>
                  <th>Quantity</th>
                  <th class="damage" hidden>Good Quantity</th>
                  <th class="damage" hidden>Damage Quantity</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                 <?php if(isset($ProductionItem)) : ?>
                  <?php 
                  foreach ($ProductionItem as $key => $value): ?>
                    <tr class="blue-pddng">
                      <td><?= $key+1;?></td>
                      <td>
                        <input value='<?= $value['Product']['name']; ?>' class='form-control' readonly type='text'>
                        <input value='<?= $value['Product']['id']; ?>' class='form-control product_id' type='hidden' name="data[ProductionPlanItem][product][]">
                        <input value='<?= $value['ProductionPlanItem']['id']; ?>' class='form-control' name="data[ProductionPlanItem][id][]" readonly type='hidden'>
                      </td>
                      <td><input class='form-control quantity' name="data[ProductionPlanItem][total_quantity][]" readonly value='<?= round($value['ProductionPlanItem']['total_quantity']); ?>'></td>
                      <?php if($this->request->data['ProductionPlan']['status']==1)
                        {?>
                      <td><input class='form-control quantity' name="data[ProductionPlanItem][good_quantity][]" value='' required></td>
                        <td><input class='form-control quantity' name="data[ProductionPlanItem][damage_quantity][]" value='0'></td>
                        <?php }
                     else{ ?>
                    <td><input class='form-control quantity' readonly value='<?= round($value['ProductionPlanItem']['good_quantity']); ?>'></td>
                    <td><input class='form-control quantity' readonly value='<?= round($value['ProductionPlanItem']['damage_quantity']); ?>'></td>
                    <?php  }?>
                      <td><a><i class="fa fa-2x fa-eye order_details" data-toggle="modal" data-target="#order_details"></i></a></td>
                    </tr>
                  <?php endforeach ?>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
        <div class="row">
          <div class="col-md-7 col-lg-7 col-sm-7">
            <?php if(isset($this->request->data['ProductionPlan']['id']))
              { ?>
            <div class="box-body table-responsive" style="margin-left: 18px;">
              <label>Additional Raw Material</label>
              <table class="table border_cover_table table-bordered" id="additional_raw_material">
             <thead>
            <tr class="blue-bg">
              <th>Nos</th>
              <th width="40%">Product</th>
              <th width="10%">Brand</th>
              <th>Quantity</th>
               <th>Unit</th>
                <th>Action</th>
            </tr>
                <?php if($this->request->data['ProductionPlan']['status']==1) {?>
            <tr>
              <td>#</td>
              <td>
                <?php echo $this->Form->input('product_additional_material',['type'=>'select','style'=>'width:100%','id'=>'product_additional_material','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'options'=>$ingredients_list,'label'=>false]); ?>
              </td>
              <td>
                <?php echo $this->Form->input('brand_additional_material',['type'=>'select','style'=>'width:100%','id'=>'brand_additional_material','class'=>'form-control select_two_class','options'=>$Brand_list,'label'=>false]); ?>
              </td>
              <td><input type="text" id="quantity_additional_material" class="form-control " value="0"></td>
              <td><input type="text" id="unit_additional_material" class="form-control " readonly="">
              <input type="hidden" id="unit_id_additional_material" class="form-control " readonly=""></td>
              <td><i class=" remote fa fa-plus-circle create_icon fa-2x add_to_additional_material"></i></td>
            </tr>
              <?php  } ?>
          </thead>
                <tbody>
                   <?php if(isset($additional_BOM)) : ?>
                  <?php 
                  foreach ($additional_BOM as $keyBOM => $valueBOM): ?>
                    <tr class="blue-pddng">
                      <td><?= $key+1;?></td>
                      <td><input value='<?= $valueBOM['product_name']; ?>' class='form-control' readonly type='text'></td>
                        <td><input value='<?= $valueBOM['brand']; ?>' class='form-control' readonly type='text'></td>
                      <td><input class='form-control ' readonly value='<?= $valueBOM['quantity']; ?>'></td>
                    <td><input value='<?= $valueBOM['unit']; ?>' class='form-control' readonly type='text'></td>
                    <td></td>
                    </tr>
                  <?php endforeach ?>
                <?php endif; ?>
                </tbody>
              </table>
            </div>
            <?php  } ?>
          </div>
          <div class="col-md-1 col-lg-1 col-sm-1"></div>
          <div class="col-md-4 col-lg-4 col-sm-4">
            <div class="box-body table-responsive" style="margin-top: 0px;">
              <table class="" id="production_footer">
                <?php if(!isset($this->request->data['ProductionPlan']['id']))
              {?>
              <tfoot>
                <tr class="blue-pddng">
               <td>
                <button type='button'  value='' class=" btn btn_save_ftr_2 pull-right bill_of_material" data-toggle="modal" data-target="#bill_of_material" style="margin-right: -10%">Bill of Material</button>
             <button type="button" id="start_production" class=" btn btn_save_ftr_1 pull-left" style="background-color: orange" data-toggle="modal" data-target="#bill_of_material">Start Production</button></td>
          </tr>
              </tfoot>
              <?php }
              else{ ?>
                <tfoot>
                <tr class="blue-pddng">
               <td><button type="button" id="production_print" class="btn btn_radious btn_save_ftr_2 pull-right" style="background-color: #4CAF50;margin-right:-2%;">Print</button>
                <button type='button'  value='' class=" btn btn_save_ftr_2 pull-right after_production_bill_of_material" data-toggle="modal" data-target="#after_production_bill_of_material" style="margin-right:2%">Bill of Material</button>
               <?php if($this->request->data['ProductionPlan']['status']==1) {?>
               <button type='submit' name='data[ProductionPlan][process]' onclick="return confirm('Are you sure?')" value='update' class=" btn btn_save_ftr_1 pull-left update-action" style="margin-left:-2%">End Production</button><?php }?></td>
          </tr>
              </tfoot>
              <?php }?>
              </table>
            </div>
          </div>
          </div>
  </div>
    <div id="order_details" class="modal fade" role="dialog" >
                <div class="modal-dialog modal-lg" style="width: 50%">
                  <div class="modal-content pull-middle">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <div class="box-body table-responsive no-padding xs_tp">
                        <table class="table table-bordered table-hover" id="order_details_table">
                          <thead>
                            <tr class="blue-bg">
                              <th></th>
                              <th>Route</th>
                              <th>Executive</th>
                              <th>Order No:</th>
                              <th>Quantity</th>
                              <th>Average Consumption</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
              <div id="production_order_list" class="modal fade" role="dialog" >
                <div class="modal-dialog modal-lg" style="width: 50%">
                  <div class="modal-content pull-middle">
                    <div class="modal-header">
                       <div class="col-md-3 col-sm-3 col-lg-3" hidden>
         <?php echo $this->Form->input('from_date',array(
          'type'=>'text',
          'id'=>'from_date',
          'value'=>$from,
          'class'=>'form-control cls_label_all date_field date_picker from_date
          datepicker',
          'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
          )); ?>
        </div>
        <div class="col-md-3 col-sm-3 col-lg-3">
          <?php echo $this->Form->input('to_date',array(
            'type'=>'text',
            'id'=>'to_date',
            'value'=>$to,
            'label'=>'Production Date',
            'class'=>'form-control cls_label_all date_field date_picker from_date
            datepicker',
            'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
            )); ?>
          </div>
<!--                       <button type="button" class="close" data-dismiss="modal">&times;</button>
 -->                    </div>
                    <div class="modal-body">
                      <div class="box-body table-responsive no-padding xs_tp">
                        <table class="table table-condensed  table boder no-footer" id="production_order_list_table">
                          <thead>
                            <tr class="blue-bg">
                              <th>#</th>
                              <th>Product</th>
                              <th>Quantity</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
              <div id="bill_of_material" class="modal fade" role="dialog" >
                <div class="modal-dialog modal-lg" style="width: 50%">
                  <div class="modal-content pull-middle">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                       <h4 class="modal-title">Bill of Material </h4>
                    </div>
                    <?php echo $this->Form->create('BOM', [ 'id' => 'BOM_Form']); ?>
                    <div class="modal-body">
                      <div class="box-body table-responsive no-padding xs_tp">
                        <table class="table table-bordered table-hover" id="bill_of_material_table">
                          <thead>
                            <tr class="blue-bg">
                              <th></th>
                              <th>Product</th>
                              <th class="stock_quantity">Stock Quantity</th>
                              <th>Quantity</th>
                              <th class="shortage">Quantity to be purchase</th>
                              <th>Unit</th>
                              <th class="brand">Brand</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                    <div class="modal-footer">
                       <button type="button"  id="BOM_Purchase" class="btn btn-primary btn_radious" style="background-color: #f44336;">Material Request</button>
                       <button type="button" id="BOM_print" class="btn btn-primary btn_radious" style="background-color: #4CAF50;">Print</button>
                       <button type="button" id="BOM_ok" class="btn btn-primary btn_radious" style="background-color: orange" >Ok</button>
                         <button type='submit' name='data[ProductionPlan][process]' onclick="return confirm('Are you sure?')" value='save' class=" btn btn-danger btn_radious invoice-action" id="production_start">Start </button>
                       <button hidden type="button"  id="BOM_Approved" disabled class="btn btn-primary btn_radious" style="background-color: orange;display: none">Approved</button>
                      <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  <?= $this->Form->end(); ?>  
                </div>
              </div> 
</section>
<?= $this->Form->end(); ?> 
<div id="after_production_bill_of_material" class="modal fade" role="dialog" >
                <div class="modal-dialog modal-lg" style="width: 50%">
                  <div class="modal-content pull-middle">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                       <h4 class="modal-title">Bill of Material </h4>
                    </div>
                    <div class="modal-body">
                      <div class="box-body table-responsive no-padding xs_tp">
                        <table class="table table-bordered table-hover" id="after_production_bill_of_material_table">
                          <thead>
                            <tr class="blue-bg">
                              <th>#</th>
                              <th>Product</th>
                              <th>Quantity</th>
                              <th>Unit</th>
                              <th>Brand</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                    <div class="modal-footer">
                       <button type="button" id="after_production_BOM_print" class="btn btn-primary btn_radious" style="background-color: #4CAF50;">Print</button>
                      <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div> 
 
<script type="text/javascript">
  <?php require 'production_plan.js'; ?>
  <?php if(!empty($this->request->data['ProductionPlan']['id'])) : ?>
$('.update-action').attr('disabled',false);
$('.damage').show();
    <?php endif; ?>
</script>


