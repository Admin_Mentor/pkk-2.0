$.fn.button_disable=function(){
	var length=$('#bill_of_material_table tbody tr').length;
	var length_production=$('#production_input_table tbody tr').length;
	if(length_production>0)
	{
		$('button[type="button"]').attr('disabled',false);
		$('button[type="Submit"]').attr('disabled',true);
	}
	else
	{
		$('button[type="button"]').attr('disabled',true);
		$('button[type="Submit"]').attr('disabled',true);
	}

};
$(document).on('keypress','.quantity',function(e){
 //$(".unit_price").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57 || e.which==47)) {
       // $(this).closest('tr').find('td input.unit_price').focus()
       return false;
     }
   });
$(document).on('click','.order_details',function(){
	var product_id=$(this).closest('tr').find('td input.product_id').val();
	var id="";
  <?php if(!empty($this->request->data['ProductionPlan']['id'])) : ?>
   	var id ="<?=$this->request->data['ProductionPlan']['id'] ?>";
    <?php endif; ?>
	var date=$('#date_of_production').val();
	  var type=1;
	$.post("<?= $this->webroot ?>ProductionPlan/individual_get_ajax/" + product_id+'/'+date+'/'+type+'/'+id, function (responds) {
		if (responds.result != 'Success')
		{
			alert(responds.result);
			return false;
		}
		$('#order_details_table tbody').empty();
		$.each(responds.orderitem,function(key,value){
			var sl=key+1;
			$('#order_details_table tbody').append('<tr class="blue-pddng">\
				<td>'+sl+'</td>\
				<td>'+value.order.Route+'</td>\
				<td>'+value.order.Executive+'</td>\
				<td>'+value.order.order_no+'</td>\
				<td>'+value.order.quantity+'</td>\
				<td>'+value.order.average_consumption+'</td>\
				</tr>'); 
		});
	}, "json");
});
$(document).on('click','.production_order_list',function(){
	var date=$('#date_of_production').val();
	  var type=1;
	$.post("<?= $this->webroot ?>ProductionPlan/get_production_order_list/" +date+'/'+type, function (responds) {
		$('#production_order_list_table').DataTable().destroy();
		$('#production_order_list_table tbody').empty();
		$.each(responds.orderitem,function(key,value){
			var sl=key+1;
			$('#production_order_list_table tbody').append('<tr class="blue-pddng">\
				<td>'+sl+'</td>\
				<td>'+value.Product.name+'</td>\
				<td>'+value.order.quantity+'</td>\
				</tr>'); 
		});
		$('#production_order_list_table').DataTable({
       "lengthMenu": [[25,10,50,-1], [25,10,50,"All"]],
        dom: 'Bfrtip',
        buttons: [
           {extend: 'print', title:"Order List", exportOptions: { columns: ':visible' } },
        {extend: 'excel',title:"Order List", exportOptions: { columns: ':visible' } },
        {extend: 'pageLength', },
        ],
    });
	}, "json");
});
$(document).on('change','.from_date',function(){
	var from_date= $('#from_date').val();
	var to_date= $('#to_date').val();
	  var type=1;
	  var data={
		from_date:from_date,
		to_date:to_date,
		type:1
	}
	$.post("<?= $this->webroot ?>ProductionPlan/get_production_order_list_fetch/",data,function (responds) {
		 $('#production_order_list_table').DataTable().destroy();
		 		$('#production_order_list_table tbody').empty();
		$.each(responds.orderitem,function(key,value){
			var sl=key+1;
			$('#production_order_list_table tbody').append('<tr class="blue-pddng">\
				<td>'+sl+'</td>\
				<td>'+value.Product.name+'</td>\
				<td>'+value.order.quantity+'</td>\
				</tr>'); 
		});
		$('#production_order_list_table').DataTable({
        dom: 'Bfrtip',
       "lengthMenu": [[25,10,50,-1], [25,10,50,"All"]],
        buttons: [
        {extend: 'print', title:"Order List", exportOptions: { columns: ':visible' } },
        {extend: 'excel',title:"Order List", exportOptions: { columns: ':visible' } },
        {extend: 'pageLength', },
        ],
    });
	}, "json");
	});
$(document).on('click','.bill_of_material',function(){
	var date=$('#date_of_production').val();
	var product_id=$('#product_id').val();
	var data={
		date:date,
		product_id:product_id,
		type_order:1
	}
	var count=0;
	$.post("<?= $this->webroot ?>ProductionPlan/bill_of_material_get_ajax/",data, function (responds) {
		if (responds.result != 'Success')
		{
			$('#BOM_Purchase').attr('disabled',true);
			$('#BOM_print').css('display','none');
			$('#BOM_ok').attr('disabled',true);
			if(responds.result=="Ingredients Empty")
			{
				            alert(responds.result);
			}
			return false;
		}
		$('#bill_of_material_table tbody').empty();
		$.each(responds.Ingredient,function(key,value){
			var sl=key+1;
			if(parseFloat(value.IngredientsItem.quantity) > parseFloat(value.IngredientsItem.stock_quantity))
			{
				var shortage=parseFloat(value.IngredientsItem.quantity)-parseFloat(value.IngredientsItem.stock_quantity);
				if(value.IngredientsItem.product_type_id!=8)
				{
				count=1;	
				}
			}
			else
			{
				var shortage=0;
			}
			$('#bill_of_material_table tbody').append('<tr class="blue-pddng">\
				<td>'+sl+'</td>\
				<td>\
				<input class="" hidden  name="data[BOM][product_id][]" value="'+value.IngredientsItem.product_id+'">\
				'+value.IngredientsItem.product_name+'</td>\
				<td class="stock_quantity">\
				<input class="" hidden  value="'+value.IngredientsItem.stock_quantity+'">\
				'+value.IngredientsItem.stock_quantity+'</td>\
				<td>\
				<input class="" hidden name="data[BOM][quantity][]"  value="'+parseFloat(value.IngredientsItem.quantity).toFixed(6)+'">\
				'+value.IngredientsItem.quantity+'</td>\
				<td class="shortage">\
				<input class="" hidden name="data[BOM][date]"  value="'+date+'">\
				<input class="" hidden name="data[BOM][order_product_id]"  value="'+product_id+'">\
				<input class="" hidden name="data[BOM][shortage][]"  value="'+parseFloat(shortage).toFixed(6)+'">\
				'+shortage+'</td>\
				<td>\
				<input class="" hidden  name="data[BOM][unit_id][]" value="'+value.IngredientsItem.unit_id+'">\
				'+value.IngredientsItem.unit+'</td>\
			   <td hidden><select id="product_brand_id'+key+'" class="form-control select_two_class brand_id"  required style="width: 100%" name="data[BOM][brand_id][]"></select></td>\
				</tr>'); 
				$.each(responds.Brand_list,function(index,value1)
				{
				$('#product_brand_id'+key+'').append($("<option></option>").attr("value",index).text(value1));
				});
		});
		if(responds.order_status==1)
		{
		if(count==0)
		{
			$('#BOM_Purchase').css('display','none');
			$('#BOM_ok').css('display','');
			$('#production_start').css('display','none');
			$('#BOM_ok').attr('disabled',false);
		    $('#BOM_Approved').css('display','none');
			$('.shortage').show();
			$('.brand').hide();
	    	$('.stock_quantity').show();
		}
		else
		{
			$('#BOM_Purchase').css('display','');
		   $('#BOM_ok').css('display','');
		   	$('#production_start').css('display','none');
		    $('#BOM_Approved').css('display','none');
			$('#BOM_ok').attr('disabled',true);
			$('.shortage').show();
		    $('.brand').hide();
	    	$('.stock_quantity').show();

		}
	    }
	    else
	    {    
	    	$('#BOM_Purchase').css('display','none');
	    	$('.shortage').hide();
	    	$('#production_start').css('display','none');
	    	$('#BOM_ok').css('display','none')
	        $('#BOM_Approved').css('display','');
	        $('#BOM_Approved').attr('disabled',true);
	        $('.brand').hide();
	    	$('.stock_quantity').hide();

	    }
	    $('#BOM_print').css('display','');
	}, "json");

});
$(document).on('click','.after_production_bill_of_material',function(){
	var production_no=$('#production_no').val();
	$.post("<?= $this->webroot ?>ProductionPlan/after_production_bill_of_material_get_ajax/"+production_no, function (responds) {
		if (responds.result != 'Success')
		{
			alert(responds.result);
		}
		$('#after_production_bill_of_material_table tbody').empty();
		$.each(responds.BOM,function(key,value){
			var sl=key+1;
			$('#after_production_bill_of_material_table tbody').append('<tr class="blue-pddng">\
				<td>'+sl+'</td>\
				<td>'+value.product_name+'</td>\
				<td>'+value.quantity+'</td>\
				<td>'+value.unit+'</td>\
				<td>'+value.brand+'</td>\
				</tr>'); 
		});
	}, "json");

});
$(document).on('click','#BOM_print',function(){
	var product_id=$('#product_id').val();
	var product_name=$('#product_id option:selected').text();
	var date=$('#date_of_delivered').val();
	var date_of_production=$('#date_of_production').val();
		// var data={
		// product_name:product_name,
		// date:date,
		// date_of_production:date_of_production,
		// ProductId:product_id,
		// type_order:1,
		// }
	url='<?= $this->webroot."ProductionPlan/BOM_print/"; ?>'+date_of_production+'/'+product_name+'/'+date+'/'+product_id+'/'+1;
	window.open(url);
});
$(document).on('click','#after_production_BOM_print',function(){
	var production_no=$('#production_no').val();
	url='<?= $this->webroot."ProductionPlan/after_production_BOM_print/"; ?>'+production_no;
	window.open(url);
});
$(document).on('click','#production_print',function(){
	var production_no=$('#production_no').val();
	url='<?= $this->webroot."ProductionPlan/production_print/"; ?>'+production_no;
	window.open(url);
});
$(document).on('click','#BOM_ok',function(){
	var product_id=$('#product_id').val();
		var date=$('#date_of_production').val();
		var data={
		date:date,
		ProductId:product_id,
		type_order:1,
		}
	if(!confirm("Are you sure?"))
  		{
  		 return false;
  		}
	$.post("<?= $this->webroot ?>ProductionPlan/bill_of_material_ok_ajax",data, function (responds) {
		if (responds.result != 'Success')
		{
			alert(responds.result);
			return false;
		}
		$('button[type="Submit"]').attr('disabled',false);
		$('#start_production').attr('disabled',false);
		$('#BOM_ok').css('display','none');
		$('#BOM_Approved').css('display','');
	}, "json");
	$('#bill_of_material').modal('toggle');
});
$(document).on('click','#start_production',function(){
	var date=$('#date_of_production').val();
	var product_id=$('#product_id').val();
	var data={
		date:date,
		product_id:product_id,
		type_order:1
	}
	var count=0;
	$.post("<?= $this->webroot ?>ProductionPlan/bill_of_material_get_ajax/",data, function (responds) {
		if (responds.result != 'Success')
		{
			$('#BOM_Purchase').attr('disabled',true);
			$('#BOM_print').css('display','none');
			$('#BOM_ok').attr('disabled',true);
			if(responds.result=="Ingredients Empty")
			{
			alert(responds.result);
			}
			return false;
		}
		$('#bill_of_material_table tbody').empty();
		$.each(responds.Ingredient,function(key,value){
			var sl=key+1;
			if(parseFloat(value.IngredientsItem.quantity) > parseFloat(value.IngredientsItem.stock_quantity))
			{
				var shortage=parseFloat(value.IngredientsItem.quantity)-parseFloat(value.IngredientsItem.stock_quantity);
				if(value.IngredientsItem.product_type_id!=8)
				{
				count=1;	
				}
			}
			else
			{
				var shortage=0;
			}
			$('#bill_of_material_table tbody').append('<tr class="blue-pddng">\
				<td>'+sl+'</td>\
				<td>\
				<input class="" hidden  name="data[BOM][product_id][]" value="'+value.IngredientsItem.product_id+'">\
				'+value.IngredientsItem.product_name+'</td>\
				<td class="stock_quantity">\
				<input class="" hidden  value="'+value.IngredientsItem.stock_quantity+'">\
				'+value.IngredientsItem.stock_quantity+'</td>\
				<td>\
				<input class="" hidden name="data[BOM][quantity][]"  value="'+parseFloat(value.IngredientsItem.quantity).toFixed(6)+'">\
				'+parseFloat(value.IngredientsItem.quantity).toFixed(6)+'</td>\
				<td class="shortage">\
				<input class="" hidden name="data[BOM][date]"  value="'+date+'">\
				<input class="" hidden name="data[BOM][order_product_id]"  value="'+product_id+'">\
				<input class="" hidden name="data[BOM][shortage][]"  value="'+parseFloat(shortage).toFixed(6)+'">\
				'+shortage+'</td>\
				<td>\
				<input class="" hidden  name="data[BOM][unit_id][]" value="'+value.IngredientsItem.unit_id+'">\
				'+value.IngredientsItem.unit+'</td>\
			   <td><select id="product_brand_id'+key+'" class="form-control select_two_class brand_id"  required style="width: 100%" name="data[BOM][brand_id][]"></select></td>\
				</tr>'); 
				$.each(responds.Brand_list,function(index,value1)
				{
				$('#product_brand_id'+key+'').append($("<option></option>").attr("value",index).text(value1));
				});
		});
			$('#production_start').css('display','');
			$('#BOM_Purchase').css('display','none');
			$('#BOM_ok').css('display','none')
			$('#BOM_print').css('display','none');
		    $('#BOM_Approved').css('display','none');
			$('.shortage').hide();
	    	$('.stock_quantity').hide();
	    	$('.brand').show();
	}, "json");

});
$(document).on('click','#BOM_Purchase',function(){
	var bom='bom';
	var id=null;
	var data=$('#ProductionPlanProductionPlanForm').serialize();
	$.post("<?= $this->webroot ?>ProductionPlan/material_request_add_ajax",data, function (responds) {
		// if (responds.result != 'Success')
		// {
		// 	alert(responds.result);
		// 	return false;
		// }
	}, "json");
	$('#bill_of_material').modal('toggle');
//url= '<?php echo $this->webroot; ?>'+'Purchase/Purchase/'+id+'/'+bom;
url= '<?php echo $this->webroot; ?>'+'ProductionPlan/MaterialRequestList';
	window.open(url);
});
 $(document).on('change','#product_id',function(){
		var product_id=$(this).val();
		var date=$('#date_of_production').val();
		var data={
		date:date,
		product_id:product_id,
		type_order:1,
		}
     $.post("<?= $this->webroot ?>ProductionPlan/single_production_ajax/" ,data, function (responds) {
    if (responds.result != 'Success')
    {
      alert(responds.result);
      return false;
    }
    $('#production_input_table tbody').empty();
        $.fn.button_disable();
        if(product_id)
        {
      $('#production_input_table tbody').append('<tr class="blue-pddng">\
       <td>#</td>\
        <td>\
       <input class=" form-control product_id" type="hidden" name="data[ProductionPlan][product][]"" value="'+responds.orderitem.Product_id+'">\
       <input value="'+responds.orderitem.Product_name+'" class="form-control" readonly type="text">\
       </td>\
        <td>\
        <input id="quantity" readonly class="form-control quantity" name="data[ProductionPlan][quantity][]"  value="'+responds.orderitem.Product_quantity+'">\
        </td>\
        <td>\
        <a><i class="fa fa-2x fa-eye order_details" data-toggle="modal" data-target="#order_details"></i></a>\
        </td>\
        </tr>'); 
      if(responds.order_status==2)
      {
      $('button[type="button"]').attr('disabled',false);
		$('button[type="Submit"]').attr('disabled',false);
      }
      else
      {
      	$('button[type="button"]').attr('disabled',false);
      	 $('.bill_of_material').attr('disabled',false);
		$('#start_production').attr('disabled',true);
      }
            $('#date_of_delivered').val(responds.orderitem.date_of_delivered);
      $('#production_labour').val(responds.labour_packing);
      $('#packing_labour').val(responds.labour_production);
               }
  }, "json");
$.post("<?= $this->webroot ?>ProductionPlan/bill_of_material_get_ajax/",data, function (responds) {
		if (responds.result != 'Success')
		{
			return false;
		}
		$('#bill_of_material_table tbody').empty();
		$.each(responds.Ingredient,function(key,value){
			var sl=key+1;
			if(parseFloat(value.IngredientsItem.quantity) > parseFloat(value.IngredientsItem.stock_quantity))
			{
				var shortage=parseFloat(value.IngredientsItem.quantity)-parseFloat(value.IngredientsItem.stock_quantity);
			}
			else
			{
				var shortage=0;
			}
			$('#bill_of_material_table tbody').append('<tr class="blue-pddng">\
				<td>'+sl+'</td>\
				<td>\
				<input class="" hidden  name="data[BOM][product_id][]" value="'+value.IngredientsItem.product_id+'">\
				'+value.IngredientsItem.product_name+'</td>\
				<td class="stock_quantity">\
				<input class="" hidden  value="'+value.IngredientsItem.stock_quantity+'">\
				'+value.IngredientsItem.stock_quantity+'</td>\
				<td>\
				<input class="" hidden name="data[BOM][quantity][]"  value="'+parseFloat(value.IngredientsItem.quantity).toFixed(6)+'">\
				'+parseFloat(value.IngredientsItem.quantity).toFixed(6)+'</td>\
				<td class="shortage">\
				<input class="" hidden name="data[BOM][shortage][]"  value="'+parseFloat(shortage).toFixed(6)+'">\
				'+shortage+'</td>\
				<td>\
				<input class="" hidden  name="data[BOM][unit_id][]" value="'+value.IngredientsItem.unit_id+'">\
				'+value.IngredientsItem.unit+'</td>\
				<td hidden><select id="product_brand_id'+key+'" class="form-control select_two_class brand_id"  required style="width: 100%" name="data[BOM][brand_id][]"></select></td>\
				</tr>'); 
			$.each(data.Brand_list,function(index,value1)
				{
				$('#product_brand_id'+key+'').append($("<option></option>").attr("value",index).text(value1));
				});
		});
	}, "json");
    });
$(".add_to_additional_material").click(function(){
    event.preventDefault();
    var ProductExist = 0;
    var product_id=$('#product_additional_material').val();
   var product_text=$('#product_additional_material option:selected').text();
    var brand_text=$('#brand_additional_material option:selected').text();
    var product_unit=$('#unit_additional_material').val();
    var brand_id=$('#brand_additional_material').val();
    var product_unit_id=$('#unit_id_additional_material').val();
    if(!product_id){
        $('#product_additional_material').select2('open');
        return false;
    }
    var product_quantity=$('#quantity_additional_material').val();
    if(!$.isNumeric(product_quantity) || product_quantity=='' || product_quantity<=0 )
    {
        $('#quantity_additional_material').focus();
        return false;
    }
    var i=0;
    $("#product_table tbody tr").each(function () {
        i=i+1;
        var productId = $(this).closest('tr').find('td input.productsrow_additional').val();
        var table_product_unit = $(this).closest('tr').find('td:eq(1) input:eq(0)').val();
        if (product_id== productId) {
            ProductExist = 1;
            return false;
        }
    });
    if(ProductExist)
    {
        alert("This product already in cart");
        $('#product_additional_material').select2('open');
        return false;
    }
    var production_no=$('#production_no').val();
    var data={
		production_no:production_no,
		product_id:product_id,
		unit_id:product_unit_id,
		brand_id:brand_id,
		quantity:product_quantity,
		}
	$.post("<?= $this->webroot ?>ProductionPlan/additional_material_add",data, function (responds) {
		if (responds.result != 'Success')
		{
			alert(responds.result);
		}
		var length=$('#additional_raw_material tbody tr').length;
        var k=length+1;
    $('#additional_raw_material tbody').append('<tr class="blue-pddng">\
        <td>'+k+'</td>\
        <td><input class="form-control" value="'+product_text+'" readonly></td>\
        <td><input class="form-control cart_quantity" readonly value="'+brand_text+'" ></td>\
        <td><input class="form-control cart_quantity" readonly value="'+product_quantity+'" ></td>\
         <td><input class="form-control cart_quantity" readonly value="'+product_unit+'" ></td>\
        <td></td>\
        </tr>');  
	}, "json");
   $('#product_additional_material').select2('open');
    $('#quantity_additional_material').val('0');
     $('#unit_additional_material').val('');
    $('#product_additional_material').val('').trigger('change.select2');
});
$('#product_additional_material').change(function(){
	var id=$('#product_additional_material').val();
	var url_address= '<?php echo $this->webroot; ?>'+'Product/get_product_unit/'+id;
	$.ajax({
		type: "POST",
		url:url_address,
		dataType:'json',
		success: function(data) {
			if(data.result!='Success')
			{
				alert(data.result);
				return false;
			}
	          $('#unit_additional_material').val(data.unit_name);
	           $('#unit_id_additional_material').val(data.unit_id);
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	}); 
	});  
$.fn.button_disable();
