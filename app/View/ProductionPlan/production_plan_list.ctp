<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js" type="text/javascript" language="javascript"></script>
<section class="content-header">
	<h1>Production List	<a  href="<?php echo $this->webroot ?>ProductionPlan/ProductionPlan"><input style="margin-left:20px"type="button" class="btn btn-success save pull-right" value="New Production"></input></a></h1>
</section>
<section class="content">
	<div class="box">
			<div class="row">
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('date_of_production',array('type'=>'text','class'=>'form-control filter_class date_picker datepicker','id'=>'date_of_production','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$firstdate,'label'=>'Date of Production')); ?>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('date_of_delivered',array('type'=>'text','class'=>'form-control filter_class date_picker datepicker','id'=>'date_of_delivered','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$todate,'label'=>'Date of Delivery')); ?>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('product_id',array('type'=>'select','id'=>'product_id','class'=>'form-control select_two_class filter_class','style'=>'width: 100%','options'=>['' => 'All',$products],'label'=>"Product")); ?>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('status_id',array('type'=>'select','id'=>'status_id','class'=>'form-control select_two_class filter_class','style'=>'width: 100%','options'=>['' => 'All','1'=>"PRODUCTION STARTED",'2'=>'PRODUCTION COMPLETED'],'label'=>"Status")); ?>
					</div>
				</div>
			</div>
			
			<!-- <div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="form-group"><br>
					<button class='btn btn-success' id='get_button'>Get</button>
				
             </div>
			</div> -->
		</div>
		<div class="box-body">
			<table class="boder table table-condensed table" id="table_data" data-order='[[ 2, "asc" ]]' data-page-length='25'>
				<thead>
					<tr class="blue-bg">
						<th>Production No</th>
       					<th>Date of Production</th>
                         <th>Date of Delivery</th>
                         <th>Product</th>
                         <th>Quantity</th>
                         <th>Good Quantity</th>
                         <th>Damage Quantity</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
				</tfoot>
			</table>
		</div>
	</div>
</section>
<script type="text/javascript">
	$('#table_data').DataTable( {
		"processing": true,
		"serverSide": true,
		dom: 'Bfrtip',
		buttons: [
		{ extend: 'colvis', },
		{ extend: 'csv',   footer: false, exportOptions: { columns: ':visible' } },
		{ extend: 'excel', footer: false, exportOptions: { columns: ':visible' } },
		{ extend: 'pageLength', },
		],
		"lengthMenu": [[10,25,50,-1], [10,25,50,"All"]],
		"ajax": {
			"url": "<?= $this->webroot ?>ProductionPlan/ProductionPlanList_ajax",
			"type": "POST",
			data:function( d ) {
			   d.type= 1;
				d.date_of_production= $('#date_of_production').val();
				d.date_of_delivered= $('#date_of_delivered').val();
				d.product_id= $('#product_id').val();
				d.status_id= $('#status_id').val();
			},
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "ProductionPlan.production_no" },
		{ "data" : "ProductionPlan.date" },
		{ "data" : "ProductionPlan.date_of_delivered" },
	    { "data" : "Product.name" },
		{ "data" : "ProductionPlanItem.total_quantity" },
		{ "data" : "ProductionPlanItem.good_quantity" },
		{ "data" : "ProductionPlanItem.damage_quantity" },
		{ "data" : "ProductionPlan.status" },
		{ "data" : "ProductionPlan.action" },
		],
		"columnDefs": [
    	],
    });
$('.filter_class').change(function(){
		table = $('#table_data').dataTable();
		table.fnDraw();
	});
</script>