<style type="text/css">
.i_color_plus {
  color: #005082;
  margin-top: 3px;
}
.table_main_heading {
  margin: 0px;
  text-transform: capitalize;
  padding-left: 9px;
  padding-bottom: 10px;
}

.btn_save_ftr_2 {
  background-color: #194e6f;
  color: white;
  letter-spacing: 0.6px;
  text-transform: capitalize;
  border-radius: 3px !important;
  margin-top: 23px;
  border: navajowhite;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 6px;
  padding-bottom: 6px;
}
.btn_save_ftr_1 {
  background-color:red;
  color: white;
  letter-spacing: 0.6px;
  text-transform: capitalize;
  border-radius: 3px !important;
  margin-top: 23px;
  border: navajowhite;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 6px;
  padding-bottom: 6px;
}

.boder_table {
  border: 1px solid #e2e2e2;
}

.sav_right_mrgn{
  margin-left: 15px;
}
.btn_modal_add {
  border: none;
  background-color: #860006;
  color: white;
  border-radius: 3px !important;
  letter-spacing: 0.6px;
}
.mdl_in_pls{
  margin-top: 27px;
}

.head_mdl {
  text-transform: capitalize;
  letter-spacing: 0.6px;
  font-size: 16px;
  padding-left: 15px;
  padding-bottom: 14px;
  color: #ab1d1d;
  cursor: pointer;
}
.right_item_mrgn{
  margin-top: 27px;
  margin-right: 27px;

}
</style>
<section class="content-header">
  <h1>Semi Production <a href="<?php echo $this->webroot ?>ProductionPlan/SemiProductionList"><button style="margin-left:20px" class="btn btn-success save pull-right">Semi Production List</button></a></h1>
</section>
<?= $this->Form->create('ProductionPlan', array('url' => array('controller' => 'ProductionPlan', 'action' => 'SemiProduction')));?>
<section class="content">
  <div class="box box-primary">
    <?php $status=$this->request->data['ProductionPlan']['status']; ?>
    <?php $status_flag=$this->request->data['ProductionPlan']['status_flag']; ?>

    <div class="row" style="margin-top: 1%;">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-2 col-lg-2 col-sm-2">
          <?= $this->Form->input('date_of_production',array('type'=>'text','class'=>'form-control','id'=>'date_of_production','required','readonly')); ?>
        </div>
        <div class="col-md-1 col-lg-1 col-sm-1">
          <label style="white-space: nowrap;">Production No</label>
          <?= $this->Form->input('production_no',['type'=>'text','style'=>'width:100%','id'=>'production_no','class'=>'form-control','required','label'=>false,'readonly']); ?>
        </div>
        <?php if(!isset($ProductionItem)) : ?>
        <div class="col-md-2 col-lg-2 col-sm-2">
          <label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Product</label>
             <?= $this->Form->input('product_id',['type'=>'select','style'=>'width:100%','id'=>'product_id','class'=>'form-control select_two_class','required','empty'=>[''=>'Select'],'options'=>$Product_array,'label'=>false,]); ?>
         </div>
                     <?php endif; ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-lg-12 col-sm-12">
        <div class="col-md-12 col-lg-12 col-sm-12">
          <h3 class="table_main_heading" style="margin-top: 2%;">input</h3>
          <div class="box-body table-responsive">
            <table class="table boder_table" style="margin-top: 0%;" id='production_input_table'>
              <thead>
                <tr class="blue-bg">
                  <th></th>
                  <th style="width: 45%;">Product</th>
                  <th>Quantity</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                 <?php if(isset($ProductionItem)) : ?>
                  <?php 
                  foreach ($ProductionItem as $key => $value): ?>
                    <tr class="blue-pddng">
                      <td><?= $key+1;?></td>
                      <td>
                        <input value='<?= $value['Product']['name']; ?>' class='form-control' readonly type='text'>
                        <input value='<?= $value['Product']['id']; ?>' class='form-control product_id' readonly type='hidden'>
                      </td>
                      <td><input class='form-control quantity' readonly value='<?= round($value['ProductionPlanItem']['total_quantity']); ?>'></td>
                      <td><a><i class="fa fa-2x fa-eye semi_order_details" data-toggle="modal" data-target="#semi_order_details"></i></a></td>
                    </tr>
                  <?php endforeach ?>
                <?php endif; ?>
              </tbody>
              <?php if(!isset($this->request->data['ProductionPlan']['id']))
              {?>
              <tfoot>
                <tr class="blue-pddng">
                  <td></td>
                  <td></td>
               <td><button type='button'  value='' class=" btn btn_save_ftr_2 pull-right bill_of_material" data-toggle="modal" data-target="#bill_of_material" style="margin-left: 2%">Bill of Material</button>
            <button type='submit' name='data[ProductionPlan][process]' onclick="return confirm('Are you sure?')" value='save' class=" btn btn_save_ftr_1 pull-right invoice-action" >Start Production</button></td>
            <td></td>
          </tr>
              </tfoot>
               <?php }
              else{ ?>
                <tfoot>
                <tr class="blue-pddng">
                  <td></td>
                  <td></td>
               <td><button type="button" id="semi_production_print" class="btn btn_radious btn_save_ftr_2 pull-right" style="background-color: #4CAF50;margin-left: 2%;">Print</button>
                <button type='button'  value='' class=" btn btn_save_ftr_2 pull-right after_production_bill_of_material" data-toggle="modal" data-target="#after_production_bill_of_material" style="margin-left: 2%">Bill of Material</button>
            <td></td>
          </tr>
              </tfoot>
              <?php }?>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="after_production_bill_of_material" class="modal fade" role="dialog" >
                <div class="modal-dialog modal-lg" style="width: 50%">
                  <div class="modal-content pull-middle">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                       <h4 class="modal-title">Bill of Material </h4>
                    </div>
                    <div class="modal-body">
                      <div class="box-body table-responsive no-padding xs_tp">
                        <table class="table table-bordered table-hover" id="after_production_bill_of_material_table">
                          <thead>
                            <tr class="blue-bg">
                              <th>#</th>
                              <th>Product</th>
                              <th>Order Quantity</th>
                              <th>Unit</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                    <div class="modal-footer">
                       <button type="button" id="after_production_BOM_print" class="btn btn-primary btn_radious" style="background-color: #4CAF50;">Print</button>           
                    <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div> 
    <div id="semi_order_details" class="modal fade" role="dialog" >
                <div class="modal-dialog modal-lg" style="width: 50%">
                  <div class="modal-content pull-middle">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                      <div class="box-body table-responsive no-padding xs_tp">
                        <table class="table table-bordered table-hover" id="semi_order_details_table">
                          <thead>
                            <tr class="blue-bg">
                              <th>#</th>
                              <th>Order No:</th>
                              <th>Quantity</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div>
               <div id="bill_of_material" class="modal fade" role="dialog" >
                <div class="modal-dialog modal-lg" style="width: 50%">
                  <div class="modal-content pull-middle">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                       <h4 class="modal-title">Bill of Material </h4>
                    </div>
                    <?php echo $this->Form->create('BOM', [ 'id' => 'BOM_Form']); ?>
                    <div class="modal-body">
                      <div class="box-body table-responsive no-padding xs_tp">
                        <table class="table table-bordered table-hover" id="bill_of_material_table">
                          <thead>
                            <tr class="blue-bg">
                              <th></th>
                              <th>Product</th>
                              <th class="stock_quantity">Stock Quantity</th>
                              <th>Order Quantity</th>
                              <th class="shortage">Quantity to be purchase</th>
                              <th>Unit</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                    <div class="modal-footer">
                       <button type="button"  id="BOM_Purchase" class="btn btn-primary btn_radious" style="background-color: #f44336;">Material Request</button>
                       <button type="button" id="BOM_print" class="btn btn-primary btn_radious" style="background-color: #4CAF50;">Print</button>
                       <button type="button" id="BOM_ok" class="btn btn-primary btn_radious" style="background-color: orange" >Ok</button>
                       <button type="button"  id="BOM_Approved" class="btn btn-primary btn_radious" style="background-color: orange;display: none">Approved</button>
                      <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                  <?= $this->Form->end(); ?>  
                </div>
              </div> 
</section>
<?= $this->Form->end(); ?> 
<script type="text/javascript">
  <?php require 'semi_production_plan.js'; ?>
</script>


