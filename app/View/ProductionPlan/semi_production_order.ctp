<style type="text/css">
.algn_lft{
  text-align: center !important;
}
.pdng_tp{
  margin-top: 15px;
}
.clr_star {
  color: #a29da2;
}
.cart_clr {
  color: #a29da2;
}
.pgn_btm_rht{
  margin-bottom: 15px;
}
.color_of{
  color :red !important;
}
.size_of
{
  font-size:25px !important;
  padding-left: 55px !important;
}


</style>
<section class="content-header">
  <h2>ORDER <a href="<?php echo $this->webroot ?>ProductionPlan/SemiProductionOrderIndex"><button class='btn btn-success pull-right btn_bdr_radious'>Order List</button></a></h2>
</section>
<section class="content">
  <div class="row-wrapper">
    <div class="box box-primary">
      <?php $status_flag=$this->request->data['SemiProductionOrder']['status_flag'];?>
      <?php $status=$this->request->data['SemiProductionOrder']['status']; ?>
      <?php $product_configuration_type=$Profile['Profile']['product_configuration_type']; ?>
      <?php $Country=$Profile['State']['country_id']; ?>
   
    <?= $this->Form->create('SemiProductionOrder', array('url' => array('controller' => 'ProductionPlan', 'action' => 'SemiProductionOrder'),'id'=>'SemiProductionOrderform'));?>
    <div class="row">
      <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
            
          </div>
        </div>
      </div>
      <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
            <div class="form-group">
              <div class="col-md-5 col-lg-5 col-sm-5"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Order No:</label></div>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <?= $this->Form->input('order_no',array('class'=>'form-control','type'=>'text','required','readonly','id'=>'order_no','label'=>false,)); ?>
                <?= $this->Form->input('id',array('class'=>'form-control','type'=>'hidden','required','readonly')); ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Date</label></div>
              <div class="col-md-5 col-lg-5 col-sm-5">
                <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box-body table-responsive no-padding">
      <div class="col-md-12">
        <table class="table table-condensed table text-center table-bordered" id="product_table">
          <thead>
            <tr class="blue-bg">
              <th>Nos</th>
              <th width="60%">Product</th>
              <th>Quantity</th>
                <th>Action</th>
            </tr>
            <?php if($status != 2)
            { ?>
            <tr>
              <td>#</td>
              <td>
                <?php echo $this->Form->input('product_simple',['type'=>'select','style'=>'width:100%','id'=>'product_simple','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'options'=>$Product_list,'label'=>false]); ?>
              </td>
              <td><input type="text" id="quantity_simple" class="simple_single_calculator form-control button_click" value="0"></td>
              <td><i class=" remote fa fa-plus-circle create_icon fa-2x add_to_cart_simple"></i></td>
            </tr>
            <?php } ?>
          </thead>
          <tbody>
          <?php if(isset($OrderItems)) 
              { 
              foreach ($OrderItems as $key => $value){?>
                          <tr class="blue-pddng">
              <td><?=$key+1?></td>
              <td>
                  <input class="productlist productsrow" hidden name="data[SemiProductionOrder][product_id][]" value="<?= $value['OrderItem']['product_id']; ?>">
                  <input value='<?= $value['Product']['name']; ?> <?= $value['Product']['code']; ?>' class='form-control' readonly type='text'>
                  <input value='<?= $value['OrderItem']['id']; ?>' name='data[SemiProductionOrder][order_id][]' type='hidden' class='table_id'>
                </td>
              <td hidden>
              <td class="qty"><input type="text"  name="data[SemiProductionOrder][quantity][]"  id="quantity_simple" class="simple_single_calculator form-control button_click cart_quantity" value='<?= floatval($value['OrderItem']['quantity']); ?>'></td>
              <?php if($status!=2) : ?>
              <td>
              <i class=" remote fa fa-minus-circle fa-2x remove_old_tr"></i>
              </td>
              <?php endif; ?>
            </tr>
            <?php } 
          }?>
          </tbody>
          <tfoot>
          <!--   <tr class = "blue-pddng">            
              <th colspan="2" style="font-size:20px; color:red;text-align:right">Total:</th>
              <th style="font-size:20px; color:red;" ></th>
              <th style="font-size:20px; color:red;"><span id='sum_total'></span></th>
              <th style="font-size:20px; color:red;"><span id="sum_qty"></span></th>
            </tr> -->
            <tr class="blue-pddng">
              <?php if(isset($OrderItems)) : ?>
                <?php $flag=$this->request->data['SemiProductionOrder']['flag']; ?>
                <?php if($status==1) : ?>
                  <td><button type='submit' id="cancel_button" name='data[SemiProductionOrder][process]' value='cancel' class="btn btn-warning">Cancel</button></td>
                  <td></td>
                  <td></td>
                  <td><button type='submit' id="order_delivery" name='data[SemiProductionOrder][process]' value='update' class="btn btn-success edit_icon">Update</button></td>
                  <td></td>
                <?php elseif($status==3) : ?>
                  <td colspan="2"></td>
                  <td><button id="save_button" type='submit' name='data[SemiProductionOrder][process]' value='after_altration' class="btn btn-success edit_icon">Save</button></td>
                <?php elseif($status==2) :  ?>
                  <td colspan="2"></td>
                                 <td><button type="button" id="semi_production_order_print" class="btn btn_radious btn_save_ftr_2 pull-right" style="background-color: #4CAF50;margin-left: 2%;">Print</button></td>
                <?php else : ?>
                  <td colspan="3"></td>
                  <td><button id="" type='submit' name='data[SemiProductionOrder][process]' value='delete' class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button></td>
                    <td></td>
                <?php endif; ?>
              <?php else : ?>
                <td colspan="2"></td>
                <td><button type='submit' id="purchase_order" name='data[SemiProductionOrder][process]' value='save'     class="btn btn-success pull-right create_icon" disabled>SAVE ORDER</button></td>
              <?php endif; ?>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
    <?= $this->Form->end(); ?>
  </div>
</section>
<script type="text/javascript">
   $(document).ready(function()
   {
      main_calculator();
   });

  var status='<?= $status; ?>';
  if(status==2) $('.remote').hide();
</script>
<script type="text/javascript">
    $(document).on('click','.number',function(){
      $(this).select();
    });
    $('#SemiProductionOrderform').on('keyup keypress', function(e) {
    var keyCode = e.keyCode || e.which;
    if (keyCode === 13) { 
        e.preventDefault();
        return false;
    }
});
$(document).on('click','#order_delivery',function(){
    var exit=0;
     $("#product_table tbody tr").each(function () {
         var cart_quantity = $(this).closest('tr').find('td input.cart_quantity').val();
       if(parseFloat(cart_quantity)<=0)
       {
        $(this).closest('tr').find('td input.cart_quantity').focus();
        exit=1;
            return false;
        }
    });
     if(exit==1)
     {
    return false;   
     }
});

$(document).on('click','.remove_old_tr',function(){
    var row_index=$(this).closest('tr').index();
    var id=$(this).closest('tr').find('td input.table_id').val();
    $('#product_table tbody tr:eq('+row_index+')').remove();
});
$('.main-sidebar').ready(function(){
    $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
});
$('button[type="Submit"]').click(function(){
    var total=$("#grand_total").val();
});
shortcut.add("alt+s", function() {
    $('#save_button').click();
});
$(".add_to_cart_simple").click(function(){
    event.preventDefault();
    var ProductExist = 0;
    var product_id=$('#product_simple').val();
    if(!product_id){
        $('#product_simple').select2('open');
        return false;
    }
    var product_quantity=$('#quantity_simple').val();
    if(!$.isNumeric(product_quantity) || product_quantity=='' || product_quantity<=0 )
    {
        $('#quantity_simple').focus();
        return false;
    }
    var i=0;
    $("#product_table tbody tr").each(function () {
        i=i+1;
        var productId = $(this).closest('tr').find('td input.productsrow').val();
        var table_product_unit = $(this).closest('tr').find('td:eq(1) input:eq(0)').val();
        if (product_id== productId) {
            ProductExist = 1;
            return false;
        }
    });
    if(ProductExist)
    {
        alert("This product already in cart");
        $('#product').select2('open');
        return false;
    }
    var product_text=$('#product_simple option:selected').text();
    var search_barcode=$('#search_barcode').val();
        var length=$('#product_table tbody tr').length;
        var k=length+1;
    $('#product_table tbody').append('<tr class="blue-pddng">\
        <td>'+k+'</td>\
        <td>\
        <input class="productlist productsrow" hidden name="data[SemiProductionOrder][product_id][]" value="'+product_id+'">\
        <input class="form-control" value="'+product_text+'" readonly>\
        </td>\
        <td class="qty"><input class="form-control cart_quantity" readonly name="data[SemiProductionOrder][quantity][]" value="'+product_quantity+'" ></td>\
        <td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
        </tr>');    
    $('#product_simple').select2('open');
    $('#quantity_simple').val('1');
    $("#quantity_simple").val('');
    $('#product_simple').val('').trigger('change.select2');
    $.fn.button_disable();
});
$(document).on('click','#semi_production_order_print',function(){
   // alert("kk");
    var order_no=$('#order_no').val();
    url='<?= $this->webroot."ProductionPlan/semi_production_order_print/"; ?>'+order_no;
    window.open(url);
});
function roundToTwo(num) {
    return +(Math.round(num + "e+2")  + "e-2");
}

$.fn.button_disable=function(){
    var length=$('#product_table tbody tr').length;
    if(length>0)
    {
        $('button[type="Submit"]').attr('disabled',false);
    }
    else
    {
        $('button[type="Submit"]').attr('disabled',true);
        $('button.btn-danger').attr('disabled',false);
    }
};
$(document).on('click','.remove_tr',function(){
$(this).closest('tr').remove();
$.fn.button_disable();
});
</script>
