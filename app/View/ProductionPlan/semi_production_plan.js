$.fn.button_disable=function(){
	var length=$('#bill_of_material_table tbody tr').length;
	var length_production=$('#production_input_table tbody tr').length;
	if(length_production>0)
	{
		$('button[type="button"]').attr('disabled',false);
		$('button[type="Submit"]').attr('disabled',true);
	}
	else
	{
		$('button[type="button"]').attr('disabled',true);
		$('button[type="Submit"]').attr('disabled',true);
	}

};
$(document).on('click','.after_production_bill_of_material',function(){
	var production_no=$('#production_no').val();
	$.post("<?= $this->webroot ?>ProductionPlan/after_production_bill_of_material_get_ajax/"+production_no, function (responds) {
		if (responds.result != 'Success')
		{
			alert(responds.result);
		}
		$('#after_production_bill_of_material_table tbody').empty();
		$.each(responds.BOM,function(key,value){
			var sl=key+1;
			$('#after_production_bill_of_material_table tbody').append('<tr class="blue-pddng">\
				<td>'+sl+'</td>\
				<td>'+value.product_name+'</td>\
				<td>'+value.quantity+'</td>\
				<td>'+value.unit+'</td>\
				</tr>'); 
		});
	}, "json");

});
$(document).on('click','.semi_order_details',function(){
  var product_id=$(this).closest('tr').find('td input.product_id').val();
  var id="";
  <?php if(!empty($this->request->data['ProductionPlan']['id'])) : ?>
    var id ="<?=$this->request->data['ProductionPlan']['id'] ?>";
    <?php endif; ?>
  var date=$('#date_of_production').val();
  var type=2;
  $.post("<?= $this->webroot ?>ProductionPlan/individual_get_ajax/" + product_id+'/'+date+'/'+type+'/'+id, function (responds) {
    if (responds.result != 'Success')
    {
      alert(responds.result);
      return false;
    }
    $('#semi_order_details_table tbody').empty();
    $.each(responds.orderitem,function(key,value){
      var sl=key+1;
      $('#semi_order_details_table tbody').append('<tr class="blue-pddng">\
        <td>'+sl+'</td>\
        <td>'+value.order.order_no+'</td>\
        <td>'+value.order.quantity+'</td>\
        </tr>'); 
    });
  }, "json");
});
$(document).on('click','.bill_of_material',function(){
	var count=0;
	var date=$('#date_of_production').val();
	var product_id=$('#product_id').val();
	var data={
		date:date,
		product_id:product_id,
		type_order:2
	}
	$.post("<?= $this->webroot ?>ProductionPlan/bill_of_material_get_ajax/",data, function (responds) {
		if (responds.result != 'Success')
		{
			$('#BOM_Purchase').attr('disabled',true);
			$('#BOM_print').css('display','none');
			$('#BOM_ok').attr('disabled',true);
			if(responds.result=="Ingredients Empty")
			{
				            alert(responds.result);
			}
			return false;
		}
		$('#bill_of_material_table tbody').empty();
		$.each(responds.Ingredient,function(key,value){
			var sl=key+1;
			if(value.IngredientsItem.quantity>value.IngredientsItem.stock_quantity)
			{
				var shortage=value.IngredientsItem.quantity-value.IngredientsItem.stock_quantity;
				count=1;	
			}
			else
			{
				var shortage=0;
			}
			$('#bill_of_material_table tbody').append('<tr class="blue-pddng">\
				<td>'+sl+'</td>\
				<td>\
				<input class="" hidden  name="data[BOM][product_id][]" value="'+value.IngredientsItem.product_id+'">\
				'+value.IngredientsItem.product_name+'</td>\
				<td class="stock_quantity">\
				<input class="" hidden  value="'+value.IngredientsItem.stock_quantity+'">\
				'+value.IngredientsItem.stock_quantity+'</td>\
				<td>\
				<input class="" hidden name="data[BOM][quantity][]"  value="'+parseFloat(value.IngredientsItem.quantity).toFixed(6)+'">\
				'+value.IngredientsItem.quantity+'</td>\
				<td class="shortage">\
				<input class="" hidden name="data[BOM][date]"  value="'+date+'">\
				<input class="" hidden name="data[BOM][order_product_id]"  value="'+product_id+'">\
				<input class="" hidden name="data[BOM][shortage][]"  value="'+parseFloat(shortage).toFixed(6)+'">\
				'+shortage+'</td>\
				<td>\
				<input class="" hidden  name="data[BOM][unit_id][]" value="'+value.IngredientsItem.unit_id+'">\
				'+value.IngredientsItem.unit+'</td>\
				</tr>'); 
		});
		if(responds.order_status==1)
		{
		if(count==0)
		{
			$('#BOM_Purchase').css('display','none');
			$('#BOM_ok').css('display','')
			$('#BOM_ok').attr('disabled',false);
		    $('#BOM_Approved').css('display','none');
			$('.shortage').show();
	    	$('.stock_quantity').show();
		}
		else
		{
			$('#BOM_Purchase').css('display','');
		   $('#BOM_ok').css('display','')
		    $('#BOM_Approved').css('display','none');
			$('#BOM_ok').attr('disabled',true);
			$('.shortage').show();
	    	$('.stock_quantity').show();

		}
	    }
	    else
	    {    
	    	$('#BOM_Purchase').css('display','none');
	    	$('.shortage').hide();
	    	$('#BOM_ok').css('display','none')
	        $('#BOM_Approved').css('display','');
	        $('#BOM_Approved').attr('disabled',true);
	    	$('.stock_quantity').hide();

	    }
	}, "json");
});
$(document).on('click','#BOM_print',function(){
	var product_id=$('#product_id').val();
	var product_name=$('#product_id option:selected').text();
	var date=$('#date_of_production').val();
	var date_of_production=$('#date_of_production').val();
		// var data={
		// product_name:product_name,
		// date:date,
		// date_of_production:date_of_production,
		// ProductId:product_id,
		// type_order:1,
		// }
	url='<?= $this->webroot."ProductionPlan/BOM_print/"; ?>'+date_of_production+'/'+product_name+'/'+date+'/'+product_id+'/'+2;
	window.open(url);
});
$(document).on('click','#after_production_BOM_print',function(){
   var production_no=$('#production_no').val();
	url='<?= $this->webroot."ProductionPlan/after_production_BOM_print/"; ?>'+production_no;
	window.open(url);
});
$(document).on('click','#semi_production_print',function(){
   // alert("kk");
    var production_no=$('#production_no').val();
    url='<?= $this->webroot."ProductionPlan/semi_production_print/"; ?>'+production_no;
    window.open(url);
});
$(document).on('click','#BOM_ok',function(){
	var product_id=$('#product_id').val();
		var date=$('#date_of_production').val();
		var data={
		date:date,
		ProductId:product_id,
		type_order:2,
		}
	if(!confirm("Are you sure?"))
  		{
  		 return false;
  		}
	$.post("<?= $this->webroot ?>ProductionPlan/bill_of_material_ok_ajax",data, function (responds) {
		if (responds.result != 'Success')
		{
			alert(responds.result);
			return false;
		}
		$('button[type="Submit"]').attr('disabled',false);
		$('#BOM_ok').css('display','none');
		$('#BOM_Approved').css('display','');
	}, "json");
	$('#bill_of_material').modal('toggle');
});
$(document).on('click','#BOM_Purchase',function(){
	var bom='bom';
	var id=null;
	var data=$("#ProductionPlanSemiProductionForm").serialize();
	$.post("<?= $this->webroot ?>ProductionPlan/material_request_add_ajax",data, function (responds) {
		// if (responds.result != 'Success')
		// {
		// 	alert(responds.result);
		// 	return false;
		// }
	}, "json");
	$('#bill_of_material').modal('toggle');
//url= '<?php echo $this->webroot; ?>'+'Purchase/Purchase/'+id+'/'+bom;
url= '<?php echo $this->webroot; ?>'+'ProductionPlan/MaterialRequestList';
	window.open(url);
});
 $(document).on('change','#product_id',function(){
		var product_id=$(this).val();
		var date=$('#date_of_production').val();
		var data={
		date:date,
		product_id:product_id,
		type_order:2,
		}
     $.post("<?= $this->webroot ?>ProductionPlan/single_production_ajax/" ,data, function (responds) {
    if (responds.result != 'Success')
    {
      alert(responds.result);
      return false;
    }
    $('#production_input_table tbody').empty();
        $.fn.button_disable();
         if(product_id)
        {
      $('#production_input_table tbody').append('<tr class="blue-pddng">\
       <td>#</td>\
        <td>\
       <input class=" form-control product_id" type="hidden" name="data[ProductionPlan][product][]"" value="'+responds.orderitem.Product_id+'">\
       <input value="'+responds.orderitem.Product_name+'" class="form-control" readonly type="text">\
       </td>\
        <td>\
        <input id="quantity" readonly class="form-control quantity" name="data[ProductionPlan][quantity][]"  value="'+responds.orderitem.Product_quantity+'">\
        </td>\
        <td>\
        <a><i class="fa fa-2x fa-eye semi_order_details" data-toggle="modal" data-target="#semi_order_details"></i></a>\
        </td>\
        </tr>'); 
      if(responds.order_status==2)
      {
      $('button[type="button"]').attr('disabled',false);
		$('button[type="Submit"]').attr('disabled',false);
      }
      else
      {
      	 $('button[type="button"]').attr('disabled',false);
		$('button[type="Submit"]').attr('disabled',true);
      }
  }
  }, "json");
$.post("<?= $this->webroot ?>ProductionPlan/bill_of_material_get_ajax/",data, function (responds) {
		if (responds.result != 'Success')
		{
			return false;
		}
		$('#bill_of_material_table tbody').empty();
		$.each(responds.Ingredient,function(key,value){
			var sl=key+1;
			if(parseFloat(value.IngredientsItem.quantity) > parseFloat(value.IngredientsItem.stock_quantity))
			{
				var shortage=parseFloat(value.IngredientsItem.quantity)-parseFloat(value.IngredientsItem.stock_quantity);
			}
			else
			{
				var shortage=0;
			}
			$('#bill_of_material_table tbody').append('<tr class="blue-pddng">\
				<td>'+sl+'</td>\
				<td>\
				<input class="" hidden  name="data[BOM][product_id][]" value="'+value.IngredientsItem.product_id+'">\
				'+value.IngredientsItem.product_name+'</td>\
				<td class="stock_quantity">\
				<input class="" hidden  value="'+value.IngredientsItem.stock_quantity+'">\
				'+value.IngredientsItem.stock_quantity+'</td>\
				<td>\
				<input class="" hidden name="data[BOM][quantity][]"  value="'+parseFloat(value.IngredientsItem.quantity).toFixed(6)+'">\
				'+value.IngredientsItem.quantity+'</td>\
				<td class="shortage">\
				<input class="" hidden name="data[BOM][shortage][]"  value="'+parseFloat(shortage).toFixed(6)+'">\
				'+shortage+'</td>\
				<td>\
				<input class="" hidden  name="data[BOM][unit_id][]" value="'+value.IngredientsItem.unit_id+'">\
				'+value.IngredientsItem.unit+'</td>\
				</tr>'); 
		});
	}, "json");
    });

$.fn.button_disable();
