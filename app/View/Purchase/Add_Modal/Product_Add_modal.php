<style type="text/css">
  .top_mdl_pop{
    margin-top: 29px;
  }
  .buton_click_advance {
    background-color: #005e6d;
    color: white;
    padding-top: 13px;
    padding-bottom: 13px;
    padding-left: 15px;
    padding-right: 15px;
    border-radius: 3px !important;
    cursor: pointer;

  }
  /*new lines */
  .TriSea-technologies-Switch > input[type="checkbox"] {
    display: none;   
  }

  .add_btn_styl {
    padding-top: 11px;
    padding-bottom: 11px;
    padding-left: 15px;
    padding-right: 15px;
    letter-spacing: 0.6px;
    border-radius: 3px !important;
  }

  .TriSea-technologies-Switch > label {
    cursor: pointer;
    height: 0px;
    position: relative; 
    width: 40px;  
  }

  .form-group-1{
    margin-bottom: 15px !important;
  }

  .TriSea-technologies-Switch > label::before {
    background: rgb(0, 0, 0);
    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
    border-radius: 8px;
    content: '';
    height: 16px;
    margin-top: -8px;
    position:absolute;
    opacity: 0.3;
    transition: all 0.4s ease-in-out;
    width: 40px;
  }
  .TriSea-technologies-Switch > label::after {
    background: rgb(255, 255, 255);
    border-radius: 16px;
    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
    content: '';
    height: 24px;
    left: -4px;
    margin-top: -8px;
    position: absolute;
    top: -4px;
    transition: all 0.3s ease-in-out;
    width: 24px;
  }
  .TriSea-technologies-Switch > input[type="checkbox"]:checked + label::before {
    background: inherit;
    opacity: 0.5;
  }
  .TriSea-technologies-Switch > input[type="checkbox"]:checked + label::after {
    background: inherit;
    left: 20px;
  </style>
  <div class="modal fade" id="Product_Add_modal" tabindex="" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Add Product</h4>
        </div>
        <div class="modal-body">
          <div class="form-horizontal">
            <?php echo $this->Form->create('ProductAdd',array('id'=>'Product_Add_Form')); ?>
            <div class="form-group">
              <div class="col-sm-9">
                <?php echo $this->Form->input('name',array('class'=>'form-control product_disable toUpperCase','label'=>'Product Name In Full *','placeholder'=>' Enter Product Name')) ?>
              </div>
              <div class="col-sm-9">
                <?php echo $this->Form->input('arabic_name',array('class'=>'form-control ','label'=>'Product Name Arabic','placeholder'=>' Enter Product Name in Arabic')) ?>
              </div>

              <div class="col-sm-3" style="display: none;">
                <?php echo $this->Form->input('code',array('type'=>'text','disabled' => 'disabled','class'=>'form-control product_disable','value'=>'Auto-Gen','label'=>'Product Code')) ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Product Type *</label>
              <div class="col-sm-6">
                <?php echo $this->Form->input('product_type_id_modal',array('type'=>'select','options'=>array(''=>'SELECT',$ProductType),'class'=>'form-control select2','style'=>array('width:100%'),'label'=>false,'id'=>'product_type_id_modal')); ?>
              </div>
              <div class="col-sm-3" style="display: none">
                <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" data-toggle="modal"  data-target="#Product_Type_Add_Modal"></i></a>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-3 control-label">Brand *</label>
              <div class="col-sm-6">
                <?php echo $this->Form->input('Brand_id_modal',array('type'=>'select','options'=>array('empty'=>'SELECT',$Brand),'class'=>'form-control select2 brand_class','style'=>array('width:100%'),'label'=>false,'id'=>'Brand_id_modal')); ?>
              </div>
              <div class="col-sm-3" style="display: none">
                <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" data-toggle="modal"  data-target="#Brand_Add_Modal"></i></a>
              </div>
            </div>
            <!-- // -->
            <div class="form-group">
              <label class="col-sm-3 control-label">Main Supplier *</label>
              <div class="col-sm-6">
                <?php echo $this->Form->input('party_id',array('type'=>'select','options'=>array('empty'=>'SELECT',$PartyList),'class'=>'form-control select2 ','style'=>array('width:100%'),'label'=>false,'id'=>'party_id')); ?>
              </div>

            </div>
            <div class="row" style="margin-top: 1%;">
              <div class="col-md-12 col-lg-12 col-sm-12">
                <div class="col-md-4 col-lg-4 col-sm-4 no-padding">
                  <div class="col-md-4 col-lg-4 col-sm-4">
                    <div class="TriSea-technologies-Switch ">
                      <b>Barcode</b><br>
                      <input id="barcodeOn" name="data[ProductAdd][barcode_on]" checked type="checkbox"/>
                      <label for="barcodeOn" class="label-primary"></label>
                    </div>
                  </div>

                </div>

                <div class="col-md-8 col-lg-8 col-sm-8 barcode-div">
                  <div class="col-md-6 col-lg-6 col-sm-6 no-padding">
                    <div class="TriSea-technologies-Switch ">
                      <b>Custom Barcode</b><br>
                      <input id="custombarcodeOn" name="data[ProductAdd][custom_barcode_selection]" checked type="checkbox"/>
                      <label for="custombarcodeOn" class="label-primary"></label>
                    </div>

                  </div>
                  <div class="col-md-6 col-lg-6 col-sm-6 no-padding">
                    <div class="form-group-1">
                      <div id="custom_barcode" style="display: none">
                        <?php echo $this->Form->input('custom_barcode',array('type'=>'text','class'=>'form-control')); ?>
                      </div>
                      <div id="product_barcode">
                        <?php echo $this->Form->input('Product_barcode',array('type'=>'text','readonly','class'=>'form-control auto_gen_barcode',)); ?>
                      </div>

                    </div>
                  </div>    
                </div>
              </div>
            </div>
            <div class="form-group ">
              <div class="col-sm-4">
                <?php echo $this->Form->input('vat',array('type'=>'number','value'=>'5','class'=>'form-control','label'=>'VAT %')); ?>
              </div>
              <div class="col-sm-4">
              <?php echo $this->Form->input('bonus',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'Bonus %')); ?>
              </div>

            </div>
            <!--  -->

            <div class="form-group">
              <div class="col-sm-4" style="display:none;">
                <?php echo $this->Form->input('stock_date',array('type'=>'text','class'=>'form-control datepicker','label'=>'Date','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
              </div>

              <div class="col-sm-4" style="display: none;">
                <?php echo $this->Form->input('hsn_code',array('type'=>'text','class'=>'form-control','label'=>'HSN Code')) ?>
              </div>
            </div>
            <div class="form-group tax_field">
              <div class="col-sm-4">
                <?php echo $this->Form->input('cgst',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'CGST %')); ?>
              </div>
              <div class="col-sm-4">
                <?php echo $this->Form->input('sgst',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'SGST %')); ?>
              </div>
              <div class="col-sm-4">
                <?php echo $this->Form->input('igst',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'IGST %')); ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-4">
                <?php echo $this->Form->input('unit_id',array('type'=>'select','options'=>$unit_name,'selected'=>1,'style'=>array('width:100%'),'class'=>'form-control select2 unit_modal_id','label'=>'Unit')) ?>
                <?php echo $this->Form->input('unit_level',array('type'=>'hidden')) ?>
              </div>
              <div class="col-sm-3 no_of_piece_per_unit_feild" style="display:none">
                <?php echo $this->Form->input('no_of_piece_per_unit',array('type'=>'number','min'=>'0','class'=>'form-control product_disable no_of_piece_per_unit number_field piece_stock_calc piece_price_calc','value'=>1,'label'=>'No Of Pieces','readonly')) ?>
              </div>
              <div class="col-sm-4">
                <?php echo $this->Form->input('threshold',array('type'=>'text','class'=>'form-control product_disable number_field','label'=>'Threshold ','min'=>0)) ?>
              </div>
              <div class="col-sm-4">
                <?php echo $this->Form->input('quantity',array('type'=>'number','class'=>'form-control','id'=>'stock_quantity','label'=>'Total Stock (Pieces)','value'=>'0')) ?>
              </div>
            </div>
            <div class="form-group price_of_unit_per_unit_feild" style="display:none">
              <div class="col-sm-4 unit_retaile_price_field">
                <?php echo $this->Form->input('unit_mrp',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Retail Price/Unit *')) ?>
              </div>
              <div class="col-sm-4 unit_whole_sales_price_field">
                <?php echo $this->Form->input('unit_wholesale_price',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Wholesale Price/Unit *')) ?>
              </div>
              <div class="col-sm-4">
                <?php echo $this->Form->input('unit_cost',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Landing Cost/Unit *')) ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-4" id='retaile_price_field'>
                <?php echo $this->Form->input('mrp',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'MRP/Retail Price/pieces *')) ?>
              </div>
              <div class="col-sm-4" id='whole_sales_price_field'>
                <?php echo $this->Form->input('wholesale_price',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Wholesale Price/pieces *')) ?>
              </div>
              <div class="col-sm-4">
                <?php echo $this->Form->input('cost',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Landing Cost/pieces *')) ?>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-4 box_count_cls" style="display: none;">
                <?php echo $this->Form->input('box_count',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_stock_calc','value'=>0,'label'=>'Case(Stock)')) ?>
              </div>
              <div class="col-sm-4 piece_count_cls" style="display: none;">
                <?php echo $this->Form->input('piece_count',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_stock_calc','value'=>0,'label'=>'Pieces (Stock)')) ?>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id='Product_Add_Btn'>Add To Stock</button>
          </div>
          <?php echo $this->Form->end(); ?>
        </div>
      </div>
    </div>
  </div>