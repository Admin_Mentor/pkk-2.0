<div class="modal fade" id="Product_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Product</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <?php echo $this->Form->create('Product'); ?>
          <div class="form-group">
            <div class="col-sm-9">
              <?php echo $this->Form->input('product_name',array('class'=>'form-control product_disable toUpperCase','label'=>'Product Name In Full *','placeholder'=>' Enter Product Name')) ?>
            </div>
            <div class="col-sm-3">
              <?php echo $this->Form->input('product_id',array('type'=>'text','class'=>'form-control product_disable','label'=>'Product Id')) ?>
            </div>
            <span style="color:red;"  id="product_name_error"></span><span style="color:red;"  id="product_id_error"></span>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Product Type</label>
            <div class="col-sm-6">
              <?php echo $this->Form->input('product_type_id_modal',array('type'=>'select','options'=>array('empty'=>'SELECT',$Product_type),'class'=>'form-control select2','style'=>array('width:100%'),'label'=>false,'id'=>'product_type_id_modal')); ?>
            </div>
            <div class="col-sm-3">
              <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" data-toggle="modal"  data-target="#Product_Type_Add_Modal"></i></a>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Brand Name</label>
            <div class="col-sm-6">
              <?php echo $this->Form->input('Brand_id_modal',array('type'=>'select','options'=>array('empty'=>'SELECT',$Brand),'class'=>'form-control select2 brand_class','style'=>array('width:100%'),'label'=>false,'id'=>'Brand_id_modal')); ?>
            </div>
            <div class="col-sm-3">
              <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" data-toggle="modal"  data-target="#Brand_Add_Modal"></i></a>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-4">
              <?php echo $this->Form->input('stock_date',array('type'=>'text','class'=>'form-control datepicker','label'=>'Date','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('unit_id',array('type'=>'select','options'=>$Unit,'selected'=>1,'style'=>array('width:100%'),'class'=>'form-control select2','label'=>'Unit')) ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('hsn_code',array('type'=>'text','class'=>'form-control','label'=>'HSN Code')) ?>
            </div>
          </div>
          <div class="form-group tax_field">
            <div class="col-sm-4">
              <?php echo $this->Form->input('cgst',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'CGST %')); ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('sgst',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'SGST %')); ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('igst',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'IGST %')); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-4" id='retaile_price_field'>
              <?php echo $this->Form->input('mrp',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'MRP/Retaile Price')) ?>
            </div>
            <div class="col-sm-4" id='whole_sales_price_field'>
              <?php echo $this->Form->input('wholesale_price',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Wholesale Price')) ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('cost',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Landing Cost')) ?>
            </div>
            <div class="row">
              <div class="col-md-9 col-md-offset-3">
                <div id="product_error" style="color:red; text-align:left;"></div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-6">
              <?php echo $this->Form->input('quantity',array('class'=>'form-control number_field','label'=>'Stock *','value'=>'0')) ?>
            </div>
            <div class="col-sm-6">
              <?php echo $this->Form->input('threshold',array('type'=>'text','class'=>'form-control product_disable number_field','label'=>'Threshold *','min'=>0)) ?>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary AddProductAndStockButton" id='AddProductAndStockButton'>Add To Stock</button>
        </div>
        <?php echo $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>