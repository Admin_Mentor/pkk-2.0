<style type="text/css">
  .algn_lft{
    text-align: center !important;
  }
  .pdng_tp{
    margin-top: 15px;
  }
  .clr_star {
    color: #a29da2;
  }
  .cart_clr {
    color: #a29da2;
  }
  .pgn_btm_rht{
    margin-bottom: 15px;
  }
  .color_of{
    color :red !important;
  }
  .size_of
  {
    font-size:25px !important;
    padding-left: 55px !important;
  }
</style>
<section class="content-header">
  <h1> Asset Purchase<a href="<?php echo $this->webroot ?>Purchase/AssetPurchaseIndex"><button class='btn btn-success pull-right'>Asset Purchase List</button></a></h1>
</section>
<section class="content">
  <div class="row-wrapper">
    <div class="box box-primary">
      <?php $status=$this->request->data['AssetPurchase']['status']; ?>
      <?php if(isset($this->request->data['AssetPurchase']['date_of_purchase'])) : ?>
        <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row size_of"><span class='color_of'>ORDERED DATE</span>: <?php echo date('d-M-Y', strtotime($this->request->data['AssetPurchase']['date_of_purchase'])); ?></div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row size_of"><span class='color_of'>
              <?php if($this->request->data['AssetPurchase']['status']==2) {
                $process="DELIVERED DATE"; 
              } 
              if($this->request->data['AssetPurchase']['flag']==0) {
                $process="Cancelled Data";              
              } 
              if(isset($process)) :  echo $process; ?>
            </span>: <?php echo date('d-M-Y', strtotime($this->request->data['AssetPurchase']['date_of_delivered'])); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>
  <?= $this->Form->create('AssetPurchase', array('url' => array('controller' => 'Purchase', 'action' => 'AssetPurchase')));?>
  <div class="row">
    <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
      <div class="form-horizontal" style="margin-top: 15px;">
        <div class="box-body">
          <div class="form-group">
            <label for="inputEmail3" class="col-md-2 control-label algn_lft">Vendor</label>
            <div class="col-md-7">
              <?php echo $this->Form->input('account_head_id',array('type'=>'select','id'=>'vendor_id','class'=>'form-control select2','style'=>'width: 100%','options'=>$Vendor_list,'label'=>false,'required'=>'required')); ?>
            </div>
            <div class="col-md-2"><i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#Account_modal"></i></div>
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-md-2 control-label algn_lft">Address</label>
            <div class="col-md-7">
              <?= $this->Form->input('address',array('class'=>'form-control','type'=>'textarea','disabled','rows'=>2,'id'=>'address','label'=>false,)); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3 col-md-offset-6"><button style='display: none' type='button' id="hide_exp" class="btn btn-success pull-right
              ">Expense Detailes</button>
            </div>
          </div>
        </div>
        <div class="row" id="exp_div" style="display:none">
          <br>
          <table class="table table-condensed table_bordered_new" id='Table_expence'>
            <thead>
              <tr class="blue">
                <th>Expense Type</th>
                <th></th>
                <th>Amount</th>
                <th></th>
              </tr>
              <tr class="blue-pd">
                <td>
                  <?php echo $this->Form->input('purchase_expence',array('type'=>'select','id'=>'purchase_expence_id_fk','class'=>'select2 sel_last_boxed','style'=>'width: 100%','options'=>[],'label'=>false)); ?>
                </td>
                <td  >
                 <i class="fa fa-plus-circle fa-2x pull-left mdlz mdl_finel_aded" data-toggle="modal" data-target="#Purchase_Expence_add"></i>
               </td>
               <td>
                <?php echo $this->Form->input('expence_amount',array('class'=>'form-control tab_sel_wid','label'=>false)); ?>
              </td>
              <td style="display: none">
                <?php echo $this->Form->input('expence_type',array('type'=>'select','class'=>'ht acc_sel field_wid_two','style'=>'width: 100%','options'=>array( 2 => 'Select Account Type','0'=>'Unpaid','1'=>'Paid'),'selected'=>1,'label'=>false)); ?>
              </td>
              <td style="display: none">
                <div class="cash_select_boxx" id="bank_cash" style="display:">
                  <?php echo $this->Form->input('mode',array('type'=>'select','class'=>'ht cash_list_style','style'=>'width: 250% !important','options'=>array('0'=>'Cash','1'=>'Bank'),'label'=>false)); ?>
                </div>
              </td>
              <td><i class="fa fa-plus-circle fa-2x pull-left mdlz pls_btn final_purchase_btn" id='add_expence_button'></i></td>
            </tr>
          </thead>
          <tbody>
            <tr></tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
    <div class="form-horizontal" style="margin-top: 15px;">
     <div class="box-body">
      <div class="form-group">
        <div class="col-md-5 col-lg-5 col-sm-5"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Order Id</label></div>
        <div class="col-md-5 col-lg-5 col-sm-5">
          <?= $this->Form->input('order_no',array('class'=>'form-control','type'=>'text','required','readonly','id'=>'order_no','label'=>false,)); ?>
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;"> Invoice No</label></div>
        <div class="col-md-5 col-lg-5 col-sm-5">
          <?= $this->Form->input('invoice_no',array('class'=>'form-control','type'=>'text','required','id'=>'invoice_no','label'=>false,)); ?>
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Date</label></div>
        <div class="col-md-5 col-lg-5 col-sm-5">
          <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
        </div>
      </div>
      <div class="form-group">
        <div class="col-md-5 col-lg-5 col-sm-5" id='gst_visibility'>
          <input class='nav-toggle' type="checkbox" id='toggle_button_for_gst_visibility' data-width="130" data-toggle="toggle" data-off="Show Gst" data-on="Hide Gst">
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php if($status==1) : ?>
  <div class="row">
    <div class="col-md-12">
     <div class="box-body table-responsive no-padding">
      <table class="boder table table-condensed table datatable" id="table_stock">
        <thead>
          <tr class="blue-bg">
            <th>Fixed Asset</th>
            <th>Stock Quantity</th>
            <th>Purchase Qty</th>
            <th>Add To Cart</th>
          </tr>
        </thead>
        <tbody>
          <tr class="blue-pddng">
            <td class='Product'>
              <?php echo $this->Form->input('asset_account_head_id',array('type'=>'select','id'=>'asset_account_head_id','class'=>'form-control select2','style'=>'width: 100%','empty'=>'Select','options'=>$Fixed_Asset_list,'label'=>false,)); ?>
            </td>
            <td><input class="form-control" value='0' readonly id="stock_qty"></td>
            <td><input class="form-control product_quantity" id='product_quantity' step="any" type="number" value='0'></td>
            <td><i class="fa fa-cart-arrow-down fa-2x  cart_clr add_to_cart" aria-hidden="true"></i></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
<?php endif; ?>
<div class="box-body table-responsive no-padding">
  <table class="boder table table-condensed table text-center table-bordered" id="product_table">
    <thead>
      <tr class="blue-bg">
        <th rowspan='2'>Product</th>
        <th rowspan='2'>Price</th> 
        <th rowspan='2'>Quantity</th>
        <th rowspan='2'>Net Amount</th>
        <th rowspan='2'>Discount</th>
        <th rowspan='2' class='tax_field'>Taxable Value</th>
        <th colspan='2' class='tax_field'>CGST</th>
        <th colspan='2' class='tax_field'>SGST</th>
        <th colspan='2' class='tax_field'>IGST</th>
        <th rowspan='2'>Total</th>
        <?php if($status==1) : ?>
          <th rowspan='2'>Action</th>
        <?php endif; ?>
      </tr>
      <tr class="blue-bg">
        <th class='tax_field'>Rate</th>
        <th class='tax_field'>Amount</th>
        <th class='tax_field'>Rate</th>
        <th class='tax_field'>Amount</th>
        <th class='tax_field'>Rate</th>
        <th class='tax_field'>Amount</th>
      </tr>
    </thead>
    <tbody>
      <?php if(isset($AssetPurchasedItem)) : ?>
        <?php foreach ($AssetPurchasedItem as $key => $value): ?>
          <tr class="blue-pddng">
            <td>
              <input value='<?= $value['AccountHead']['name']; ?>' class='form-control' readonly type='text'>
              <input class='table_id' value='<?= $value['AssetPurchasedItem']['id']; ?>' name='data[AssetPurchasedItem][AssetPurchasedItem_id][]' type='hidden'>
            </td>
            <td><input value='<?= $value['AssetPurchasedItem']['price']; ?>' class='form-control number single_calculator price'  name='data[AssetPurchasedItem][price][]' type='text'></td>
            <td><input value='<?= $value['AssetPurchasedItem']['quantity']; ?>' class='form-control number single_calculator quantity'  name='data[AssetPurchasedItem][quantity][]' type='text'></td>
            <td><input value='<?= $value['AssetPurchasedItem']['net_value']; ?>' class='form-control number single_calculator net_value'  name='data[AssetPurchasedItem][net_value][]' type='text'></td>
            <td><input value='<?= $value['AssetPurchasedItem']['discount']; ?>' class='form-control number single_calculator discount'  name='data[AssetPurchasedItem][discount][]' type='text'></td>
            <td class='tax_field'><input value='<?= $value['AssetPurchasedItem']['taxable_value']; ?>' class='form-control number single_calculator taxable_value'  name='data[AssetPurchasedItem][taxable_value][]' type='text'></td>
            <td class='tax_field'><input value='<?= $value['AssetPurchasedItem']['cgst']; ?>' class='form-control number single_calculator cgst'  name='data[AssetPurchasedItem][cgst][]' type='text'></td>
            <td class='tax_field'><input value='<?= $value['AssetPurchasedItem']['cgst_amount']; ?>' class='form-control number single_calculator cgst_amount'  name='data[AssetPurchasedItem][cgst_amount][]' type='text'></td>
            <td class='tax_field'><input value='<?= $value['AssetPurchasedItem']['sgst']; ?>' class='form-control number single_calculator sgst'  name='data[AssetPurchasedItem][sgst][]' type='text'></td>
            <td class='tax_field'><input value='<?= $value['AssetPurchasedItem']['sgst_amount']; ?>' class='form-control number single_calculator sgst_amount'  name='data[AssetPurchasedItem][sgst_amount][]' type='text'></td>
            <td class='tax_field'><input value='<?= $value['AssetPurchasedItem']['igst']; ?>' class='form-control number single_calculator igst'  name='data[AssetPurchasedItem][igst][]' type='text'></td>
            <td class='tax_field'><input value='<?= $value['AssetPurchasedItem']['igst_amount']; ?>' class='form-control number single_calculator igst_amount'  name='data[AssetPurchasedItem][igst_amount][]' type='text'></td>
            <td><input value='<?= $value['AssetPurchasedItem']['total']; ?>' class='form-control number single_calculator row_total'  name='data[AssetPurchasedItem][row_total][]' type='text'></td>
            <?php if($status==1) : ?>
             <td><i class="fa fa-minus-circle fa-2x remove_old_tr" ></i></td>
           <?php endif; ?>
         </tr>
       <?php endforeach ?>
     <?php endif; ?>
   </tbody>
   <tfoot>
    <tr>
      <td colspan='4'></td>
      <td colspan='7' class='tax_field'></td>
      <td><label for="inputEmail3" class="col-md-2 control-label">Total</label></td>
      <td><?= $this->Form->input('total',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'total','label'=>false,)); ?></td>
      <?php if($status==1) : ?>
       <td></td>
     <?php endif; ?>
   </tr>
   <tr>
    <td colspan='4'></td>
    <td colspan='7' class='tax_field'></td>
    <td><?= $this->Form->input('other_name',array('class'=>'form-control','type'=>'text','required','id'=>'other_name','value'=>'Round Off','label'=>false,)); ?></td>
    <td><?= $this->Form->input('other_value',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'other_value','label'=>false,)); ?></td>
    <?php if($status==1) : ?>
     <td></td>
   <?php endif; ?>
 </tr>
 <tr>
  <td colspan='4'></td>
  <td colspan='7' class='tax_field'></td>
  <td><label for="inputEmail3" class="col-md-2 control-label" style="white-space: nowrap;">Grand Total</label></td>
  <td><?= $this->Form->input('grand_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'grand_total','label'=>false,)); ?></td>
  <?php if($status==1) : ?>
   <td></td>
 <?php endif; ?>
</tr>
</tfoot>
</table>
</div>
<div class="row">
  <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 col-md-offset-9">
    <br>
    <?php if(isset($AssetPurchasedItem)) : ?>
      <?php $flag=$this->request->data['AssetPurchase']['flag']; ?>
      <?php if($status!=2 && $flag!=0) : ?>
        <button type='submit' name='data[AssetPurchase][process]' value='cancel' class="btn btn-success">Cancel</button>
        <button type='submit' name='data[AssetPurchase][process]' value='update' class="btn btn-success">Update</button>
        <button type='submit' name='data[AssetPurchase][process]' value='delivery' class="btn btn-success">Order Delivery</button>
      <?php endif; ?>
    <?php else : ?>
      <button type='submit' value='save' class="btn btn-success" disabled>Purchase</button>
    <?php endif; ?>
    <br>
    <br>
  </div>
</div>
<?= $this->Form->end(); ?>
</div>
</section>
<?php require('vendor_modal.php') ?>
<script type="text/javascript">
  <?php require('asset_purchase.js'); ?>
  var status='<?= $status; ?>';
  if(status==2)
  {
    $('.fa').hide();
  }
</script>