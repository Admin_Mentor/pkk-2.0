$('#vendor_id').change(function(){
	var id=$(this).val();
	var url_address= '<?php echo $this->webroot; ?>'+'Purchase/get_vendor_address/'+id;
	$.ajax({
		type: "get",  
		url:url_address,
		dataType:'json',
		success: function(response) {
			$('#address').val(response.address);
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
});
$('#asset_account_head_id').change(function(){
	var id=$(this).val();
	var url_address= '<?php echo $this->webroot; ?>'+'Purchase/get_fixed_stock_quantity/'+id;
	$.ajax({
		type: "get",  
		url:url_address,
		dataType:'json',
		success: function(response) {
			$('#stock_qty').val(response.quantity);
			$('#product_quantity').focus();
			$('#product_quantity').select();
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
});
$('#hide_exp').click(function(){
	$("#exp_div").toggle();
});
$(document).on('click','.add_to_cart',function(){
});
function roundToTwo(num) {
	return +(Math.round(num + "e+3")  + "e-3");
}
$(document).on('keyup','.single_calculator',function(){
	$(this).closest('tr').find('td input.single_calculator').each(function(){
		var price=$(this).closest('tr').find('td input.price').val();
		var cgst=$(this).closest('tr').find('td input.cgst').val();
		var sgst=$(this).closest('tr').find('td input.sgst').val();
		var igst=$(this).closest('tr').find('td input.igst').val();
		var quantity=$(this).closest('tr').find('td input.quantity').val();
		var discount=$(this).closest('tr').find('td input.discount').val();
		var net_value_amount=price*quantity;
		var taxable_value_amount=net_value_amount-discount;
		var cgst_amount=taxable_value_amount*cgst/100;
		$(this).closest('tr').find('td input.cgst_amount').val(cgst_amount);
		var sgst_amount=taxable_value_amount*sgst/100;
		$(this).closest('tr').find('td input.sgst_amount').val(sgst_amount);
		var igst_amount=taxable_value_amount*igst/100;
		$(this).closest('tr').find('td input.igst_amount').val(igst_amount);
		var tax_amount=parseFloat(cgst_amount)+parseFloat(sgst_amount)+parseFloat(igst_amount);
		var total=parseFloat(tax_amount)+parseFloat(taxable_value_amount);
		$(this).closest('tr').find('td input.net_value').val(net_value_amount);
		$(this).closest('tr').find('td input.taxable_value').val(taxable_value_amount);
		$(this).closest('tr').find('td input.tax_amount').val(roundToTwo(tax_amount));
		$(this).closest('tr').find('td input.row_total').val(roundToTwo(total));
	});
	$.fn.main_calculator();
});
$.fn.main_calculator=function(){
	var total=0;
	$('#product_table tbody tr').each(function(){
		single_total=$(this).closest('tr').find('td input.row_total').val();
		total+=parseFloat(single_total);
	});
	$('#total').val(Math.round(total));
	var other_value=$('#other_value').val();
	if(other_value)
	{
		var grand_total=parseFloat(total)+parseFloat(other_value);	
	}
	else
	{
		var grand_total=parseFloat(total);
	}
	$('#grand_total').val(Math.round(grand_total));
}
$('.main_calculator').keyup(function(){
	$.fn.main_calculator();
});
$.fn.button_disable=function(){
	var length=$('#product_table tbody tr').length;
	if(length>0)
	{
		$('button[type="Submit"]').attr('disabled',false);	
	}
	else
	{
		$('button[type="Submit"]').attr('disabled',true);
	}
};
$('.add_to_cart').click(function(){
	var vendor_id=$('#vendor_id').val();
	var invoice_no=$('#invoice_no').val();
	if(!vendor_id)
	{
		$('#vendor_id').select2('open');
		return false;
	}
	if(!invoice_no)
	{
		$('#invoice_no').focus();
		return false;
	}
	var product_quantity=$(this).closest('tr').find('td input.product_quantity').val();
	if(!$.isNumeric(product_quantity) || product_quantity=='' || product_quantity<=0 )
	{
		$(this).closest('tr').find('td input.product_quantity').focus();
		return false;
	}
	var Asset_name=$('#asset_account_head_id option:selected').text();
	var Asset_id=$('#asset_account_head_id').val();
	if(!Asset_id)
	{
		$('#asset_account_head_id').select2('open');
		return false;
	}
	var Asset_name_field="<input type='text' class='form-control' readonly value='"+Asset_name+"'><input type='hidden' value='"+Asset_id+"' name='data[AssetPurchase][Asset_id][]'>";
	var price_field="<input type='text' class='form-control number single_calculator price'  value='0' name='data[AssetPurchase][price][]'>";
	var Assetpurchase_qty_field="<input type='text' class='form-control number single_calculator quantity'  value='"+product_quantity+"' name='data[AssetPurchase][quantity][]'>";
	var net_value_field="<input type='text' class='form-control number single_calculator net_value'  value='0' name='data[AssetPurchase][net_value][]'>";
	var discount_field="<input type='text' class='form-control number single_calculator discount'  value='0' name='data[AssetPurchase][discount][]'>";
	var taxable_value_field="<input type='text' class='form-control number single_calculator taxable_value'  value='0' name='data[AssetPurchase][taxable_value][]'>";
	var cgst_field="<input type='text' class='form-control number single_calculator cgst'  value='0' name='data[AssetPurchase][cgst][]'>";
	var cgst_amount_field="<input type='text' class='form-control number single_calculator cgst_amount'  value='0' name='data[AssetPurchase][cgst_amount][]'>";
	var sgst_field="<input type='text' class='form-control number single_calculator sgst'  value='0' name='data[AssetPurchase][sgst][]'>";
	var sgst_amount_field="<input type='text' class='form-control number single_calculator sgst_amount'  value='0' name='data[AssetPurchase][sgst_amount][]'>";
	var igst_field="<input type='text' class='form-control number single_calculator igst'  value='0' name='data[AssetPurchase][igst][]'>";
	var igst_amount_field="<input type='text' class='form-control number single_calculator igst_amount'  value='0' name='data[AssetPurchase][igst_amount][]'>";
	var total_field="<input type='text' class='form-control number single_calculator row_total'  value='0' name='data[AssetPurchase][row_total][]'>";
	flag="true";
	$('#product_table tbody tr').each(function(){
		var table_product=($(this).find('td:nth-child(1)').find('input').val());
		console.log(table_product);
		if(table_product == Asset_name)
		{
			$(this).find('td').find('input.quantity').select().focus();
			flag="false";
		}
	});
	if(flag=="false")
	{
		alert("Product alredy added");
		return false;
	}
	$('#product_table tbody').append('<tr class="blue-pddng">\
		<td>'+Asset_name_field+'</td>\
		<td>'+price_field+'</td>\
		<td>'+Assetpurchase_qty_field+'</td>\
		<td>'+net_value_field+'</td>\
		<td>'+discount_field+'</td>\
		<td class="tax_field">'+taxable_value_field+'</td>\
		<td class="tax_field">'+cgst_field+'</td>\
		<td class="tax_field">'+cgst_amount_field+'</td>\
		<td class="tax_field">'+sgst_field+'</td>\
		<td class="tax_field">'+sgst_amount_field+'</td>\
		<td class="tax_field">'+igst_field+'</td>\
		<td class="tax_field">'+igst_amount_field+'</td>\
		<td>'+total_field+'</td>\
		<td><i class="fa fa-minus-circle fa-2x remove_tr" ></i></td>\
		</tr>');
	var toggle_button_for_gst_visibility=$('#toggle_button_for_gst_visibility').prop("checked");
	if(toggle_button_for_gst_visibility==false)
	{
		$('.tax_field').css('display','none');
	}
	$.fn.Country_type();
	$('#asset_account_head_id').val('').trigger('change.select2');
	$.fn.main_calculator();
	$(this).closest('tr').find('td input.product_quantity').val('0');
	$.fn.button_disable();
	$('.price').focus();
	$('.price').select();
});
$(document).on('click','.remove_tr',function(){
	$(this).closest('tr').remove();
	$.fn.main_calculator();
	$.fn.button_disable();
});
$(document).on('click','.remove_old_tr',function(){
	var row_index=$(this).closest('tr').index();
	var id=$(this).closest('tr').find('td input.table_id').val();
	var url_address= '<?php echo $this->webroot; ?>'+'Purchase/PurchasedItem_delete/'+id;
	$.ajax({
		type: "POST",
		url:url_address,
		dataType:'json',
		success: function(data) {
			if(data.result!='Success')
			{
				alert(data.result);
				return false;
			}
			$('#product_table tbody tr:eq('+row_index+')').remove();
			$.fn.main_calculator();
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
	$(this).closest('tr').remove();
	$.fn.button_disable();
});
$.fn.button_disable();
$('#add_Vendor').click(function(){
	var data=$('#Vendor_Form').serialize();
	var url_address= '<?php echo $this->webroot; ?>'+'Purchase/Vendor_Account_head_ajax';
	$.ajax({
		type: "POST",  
		url:url_address,
		data:data,
		dataType:'json',
		success: function(response) {
			if(response.result!='Success')
			{
				return false;
			}
			$('#vendor_id').append($("<option></option>").attr("value",response.key).text(response.value));
			$('#vendor_id').val(response.key).trigger('change');
			$('#vendor_Form')[0].reset();
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
});
$('#add_state_button').click(function(){
	var name=$('#state_name_modal').val();
	if(name=='')
	{
		$('#state_name_modal').focus();
		return false;
	}
	var code=$('#state_code_modal').val();
	if(code=='')
	{
		$('#state_code_modal').focus();
		return false;
	}
	var website_url='<?php echo $this->webroot; ?>Customer/add_state_ajax';
	var data={
		name:name,
		code:code,
	};
	$.ajax({
		method: "POST",
		url: website_url,
		data: data,
		dataType:'json',
	}).done(function( data ) {
		if(data.result!='Success')
		{
			alert(data.result);
			return false;
		}
		$('#state_add_modal').modal('toggle');
		$('#state_name_modal').val('');
		$('#state_code_modal').val('');
		$('#state_id').append($("<option></option>").attr("value",data.key).text(data.value));
		$('#state_id').val(data.key).trigger('change');
	});
});
$('#state_id').change(function(){
	var id=$(this).val();
	$.get( "<?= $this->webroot ?>Customer/get_state_code_ajax/"+id,function( data ) {
		$('#state_code').val(data.code);
	}, "json");
});
$('#state_id').trigger('change');
$('.main-sidebar').ready(function(){
	$('body').attr('class','skin-black sidebar-mini sidebar-collapse');
});