<?php 
function get_status_name($status)
{
	if($status==0)
	{
		$name='Cancelled';
	}
	if($status==1)
	{
		$name='Order Placed';
	}
	if($status==2)
	{
		$name='Order Delivered';	
	}
	return $name;
}
?>
<section class="content">
	<div class="row">
		<div class="col-md-12">
		</br>
			<h2>AssetPurchase List</h2>
			<a href="<?php echo $this->webroot ?>Purchase/AssetPurchase"><input type="button" class="btn btn-success save pull-right" value="New Asset Purchase"></input></a>
		</div>
		<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control invoice-search pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$firstdate,'label'=>'From Date')); ?>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
												<div class="form-group">
													<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
														<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control invoice-search pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$todate,'label'=>'To Date')); ?>
													</div>
												</div>
											</div>
		<div class="col-md-12">
			<div class="box-body">
				<table class="boder table table-condensed table datatable" id="purchase_table" data-order='[[ 1, "asc" ]]' data-page-length='25'>
					<thead>
						<tr class="blue-bg">
							<th>Date</th>
							<th>Party</th>
							<th>Order ID</th>
							<th>Invoice No</th>
							<th>Total Amt</th>
							<th width="10%">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($AssetPurchase as $key=>$value ) {?>
							<tr class="blue-pddng">
								<td>
									<span><?= date('d-m-Y', strtotime($value['AssetPurchase']['date_of_purchase'])); ?></span>
									<span class='purchase_id' style='display:none;'><?= $value['AssetPurchase']['id']; ?></span>
								</td>
								<td><?= $value['AccountHead']['name']; ?></td>
								<td><?= $value['AssetPurchase']['order_no']; ?></td>
								<td><?= $value['AssetPurchase']['invoice_no']; ?></td>
								<td class="text-right"><?= number_format($value['AssetPurchase']['grand_total']); ?></td>
								<td><?= get_status_name($value['AssetPurchase']['status']) ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		$.fn.table_search=function(){
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    var data={
      to_date:to_date,
      from_date : from_date,
        };
    var url_address= '<?php echo $this->webroot; ?>'+'Purchase/asset_purchase_search_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {
        $('#purchase_table').DataTable().destroy();
        $('#purchase_table tbody').html(response.row);
        
        if(response.result=='Success')
        {
          // $('#main_total_cost').text($.fn.indian_number_format(response.total_cost));
        }
        else
        {
          // $('#main_total_cost').text('0');
        }
        $('#purchase_table').DataTable();
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  }
		$(document).on('click','table tbody tr',function(){
			var purchase_id=$(this).closest('tr').find('.purchase_id').html();
			var url = "<?= $this->webroot; ?>Purchase/AssetPurchase/"+purchase_id;
			$(location).attr("href", url);
		});
		$(document).on('change','.invoice-search',function(){
     $.fn.table_search();
  });
	</script>