<style type="text/css">
.algn_lft{
  text-align: center !important;
}
.pdng_tp{
  margin-top: 15px;
}
.clr_star {
  color: #a29da2;
}
.cart_clr {
  color: #a29da2;
}
.pgn_btm_rht{
  margin-bottom: 15px;
}
.color_of{
  color :red !important;
}
.size_of
{
  font-size:25px !important;
  padding-left: 55px !important;
}
.first-half {
  float: left;
  width: 50%;
}
.second-half {
  float: right;
  width: 50%;
}
</style>
<section class="content-header">
  <h1>Purchase 
    <a href="<?php echo $this->webroot ?>Purchase/PurchaseIndex"><button class='btn btn-success pull-right'>Purchase List</button></a>
  </h1>
</section>
<section class="content">
  <div class="row-wrapper">
    <div class="box box-primary">
      <?php $status=$this->request->data['Purchase']['status']; ?>
      <?php $purchase_type=$this->request->data['Purchase']['purchase_type']; ?>
      <?php $product_configuration_type=$Profile['Profile']['product_configuration_type']; ?>
      <?php $Country=$Profile['State']['country_id']; ?>
      <?php if(isset($this->request->data['Purchase']['date_of_purchase'])) : ?>
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <br>
          <button type='button' class='btnbtn btn-success form-control'>ORDERED DATE : <?php echo date('d-M-Y', strtotime($this->request->data['Purchase']['date_of_purchase'])); ?></button>
        </div>
        <br>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <?php if($this->request->data['Purchase']['status']==2) { $process="DELIVERED DATE";  } 
          if($this->request->data['Purchase']['flag']==0) { $process="Cancelled Data"; }
          if(isset($process)) : ?>
          <button type='button' class='btnbtn btn-primary form-control'><?= $process; ?> : <?php echo date('d-M-Y', strtotime($this->request->data['Purchase']['date_of_delivered'])); ?></button>
        <?php endif; ?>
      </div>
    </div>
  <?php endif; ?>
  <?= $this->Form->create('Purchase', array('url' => array('controller' => 'Purchase', 'action' => 'Purchase')));?>
  <div class="row">
    <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
      <div class="form-horizontal" style="margin-top: 15px;">
        <div class="box-body">
          <div class="form-group">
            <label for="inputEmail3" class="col-md-2 control-label algn_lft">Vendor</label>
            <div class="col-md-7">
              <?php echo $this->Form->input('account_head_id',array('type'=>'select','id'=>'party_id','class'=>'form-control select2','style'=>'width: 100%','options'=>$Party_list,'label'=>false,'required'=>'required')); ?>
            </div>
            <!-- <div class="col-md-2"><i class="add_button fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#Account_modal"></i></div> -->
          </div>
          <div class="form-group">
            <label for="inputEmail3" class="col-md-2 control-label algn_lft">Address</label>
            <div class="col-md-7">
              <?= $this->Form->input('address',array('class'=>'form-control','type'=>'textarea','disabled','rows'=>2,'id'=>'address','label'=>false,)); ?>
              <?= $this->Form->input('state_id',array('class'=>'form-control','type'=>'hidden','id'=>'state_id','label'=>false,)); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3 col-md-offset-6"><button style='display: none' type='button' id="hide_exp" class="btn btn-success pull-right
              ">Expense Detailes</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
      <div class="form-horizontal" style="margin-top: 15px;">
        <div class="box-body">
          <div class="form-group">
            <div class="col-md-5 col-lg-5 col-sm-5"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Order Id</label></div>
            <div class="col-md-5 col-lg-5 col-sm-5">
              <?= $this->Form->input('order_no',array('class'=>'form-control','type'=>'text','required','readonly','id'=>'order_no','label'=>false,)); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">vendor Invoice No</label></div>
            <div class="col-md-5 col-lg-5 col-sm-5">
              <?= $this->Form->input('invoice_no',array('class'=>'form-control','type'=>'text','required','id'=>'invoice_no','label'=>false,)); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Date</label></div>
            <div class="col-md-5 col-lg-5 col-sm-5">
              <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
          </div>
         <div class="form-group">
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"></div>
        <div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
      <?php $options1 = array(
      'CashPurchase' => 'CashPurchase &nbsp;&nbsp;&nbsp;&nbsp;',
      'CreditPurchase' => 'CreditPurchase'
      );
      $attributes1 = array(
      'legend' => false,
      'value' => 0,
      'class'=>'toggle_button_for_purchase_type',
      );
      echo $this->Form->radio('purchase_type', $options1, $attributes1); ?>
        </div>
        </div>
         </div>
        <?php if($status != 2) {?>
        <div class="form-group" hidden>
          <?= $this->Form->input('tax_type',array('class'=>'nav-toggle','type'=>'checkbox','id'=>'toggle_button_for_tax_visibility','data-toggle'=>'toggle', 'data-off'=>'Local','data-on'=>'Import','label'=>false,'data-width'=>'130')); ?>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>

  <?php if($status==1 || $status==3) : ?>
  <div>
    <span hidden id="PurchaseAdvanceBtn" class="padding_left" style="cursor: pointer;color: #3c8dbc;">Show Advanced Purchase Options</span>
    <span  data-toggle="modal" id="last_purchase" data-target="#lastpurchase" class="padding_left" style="cursor: pointer;color: #008000;">Show Last Five Purchase List</span>
    <span>
      <!-- <a href="#"><button type='button' class='btn btn-primary' data-toggle="modal" style="margin-left: 1%"  data-target="#Product_Add_modal">Add Product</button></a> -->
    </span>
  </div>
  <div class="row" id="PurchaseAdvance" hidden>
    <div class="col-md-12">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs padding_left">
          <li style="display:none"><a href="#tab_party_vice_purchase_cart" data-toggle="tab" aria-expanded="false">Vendor Vice Purchase Cart</a></li>
          <li class="active"><a href="#tab_stock" data-toggle="tab" aria-expanded="false">Stock</a></li>
          <li class=""><a href="#tab_wish_list" data-toggle="tab" aria-expanded="true">Wish List</a></li>
        </ul>
        <div class="tab-content">
          <div class="tab-pane" id="tab_party_vice_purchase_cart" hidden>
            <div class="box-body no-padding">
            </div>
            <table class="table table-condensed table datatable" id="party_vice_purchase_cart_table">
              <thead>
                <tr class="blue-bg">
                  <th class="padding_left">Product Type</th>
                  <th>Brand</th>
                  <th>Product</th>
                  <th>Type</th>
                  <th>Unit Price</th>
                  <th>Qty</th>
                  <th style="width: 11%;">Purchase Qty</th>
                  <th style="width: 11%;">Add To Cart</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
          <div class="tab-pane active" id="tab_stock">
            <div class="box-body no-padding">
              <table class="table table-condensed datatable" id="table_stock">
                <thead>
                  <tr class="blue-bg">
                    <th class="padding_left">Product Type</th>
                    <th>Brand</th>
                    <th>Product</th>
                    <th>Unit</th>
                    <th>Quantity</th>
                    <th>Status</th> 
                    <th style="width: 11%;">Purchase Qty</th>
                    <th style="width: 11%;">Add To Cart</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($Stock_List as $key => $value): ?>
                  <tr class="blue-pddng">
                    <td class='ProductType_name'><?= $value['ProductType']['name']; ?></td>
                    <td class='Brand_name'><?php if($value['Brand']['name']) : echo $value['Brand']['name']; else : echo "GENERAL"; endif;  ?></td>                        
                    <td class='Product'>
                      <span class='Product_name'><?= $value['Product']['name']; ?></span>
                      <span style='display: none' class='cost'><?= $value['Product']['cost']; ?></span>
                      <span style='display: none' class='mrp'><?= $value['Product']['mrp']; ?></span>
                      <span style='display: none' class='wholesale_price'><?= $value['Product']['wholesale_price']; ?></span>
                      <span style='display: none' class='Tax'><?= $value['Product']['tax']; ?></span>
                      <span style='display: none' class='product_id'><?= $value['Product']['id']; ?></span>
                      <span style='display: none' class='unit_id'><?= $value['Product']['unit_id']; ?></span>
                      <span style='display: none' class='product_unit_level'><?= $value['Unit']['level']; ?></span>
                      <span style='display: none' class='purchase_unit_level'><?= $value['Unit']['level']; ?></span>
                      <span style='display: none' class='no_of_piece_per_unit'><?= $value['Product']['no_of_piece_per_unit']; ?></span>
                    </td>
                    <td><?= $this->Form->input('quantity_mode',['type'=>'select','style'=>'width:100%','id'=>'no_id'.$key,'class'=>'form-control quantity_mode select2','options'=>$quantity_mode,'label'=>false,'value'=>$value['Product']['unit_id']]); ?></td>
                    <td><?= $value['Stock']['quantity']; ?></td>
                    <td>
                      <?php if($value['Stock']['quantity']==$value['Product']['threashold']) { ?><i aria-hidden="true" class="fa fa-star-half-o fa-2x clr_star"></i><?php } ?>
                      <?php if($value['Stock']['quantity']<$value['Product']['threashold'])  { ?><i aria-hidden="true" class="fa fa-star-o fa-2x clr_star"></i><?php } ?>
                      <?php if($value['Stock']['quantity']>$value['Product']['threashold'])  { ?><i aria-hidden="true" class="fa fa-star fa-2x clr_star"></i><?php } ?>
                    </td>
                    <td><input class="form-control product_quantity" step="any" type="number" value='0'></td>
                    <td><i class="fa fa-cart-arrow-down fa-2x  cart_clr add_to_cart" aria-hidden="true"></i></td>
                  </tr>
                <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="tab-pane" id="tab_wish_list">
          <div class="box-body no-padding">
            <table class="table table-condensed table datatable" id="table_wish_list">
              <thead>
                <tr class="blue-bg">
                  <th class="padding_left">Product Type</th>
                  <th>Brand</th>
                  <th>Product</th>
                  <th>Unit</th>
                  <th>Quantity</th>
                  <th>Threshold</th>
                  <th style="width: 11%;">Purchase Qty</th>
                  <th style="width: 11%;">Add To Cart</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($Stock_List as $key => $value): ?>
                <?php if($value['Product']['threashold'] >= $value['Stock']['quantity'] && !in_array( $value['Product']['id'] , $UnwantedList,true)  ) : ?>
                <tr class="blue-pddng">
                  <td class='ProductType_name'><?= $value['ProductType']['name']; ?></td>
                  <td class='Brand_name'><?php if($value['Brand']['name']) : echo $value['Brand']['name']; else : echo "GENERAL"; endif;  ?></td>
                  <td class='Product'>
                    <span class='Product_name'><?= $value['Product']['name']; ?></span>
                    <span style='display: none' class='cost'><?= $value['Product']['cost']; ?></span>
                    <span style='display: none' class='mrp'><?= $value['Product']['mrp']; ?></span>
                    <span style='display: none' class='wholesale_price'><?= $value['Product']['wholesale_price']; ?></span>
                    <span style='display: none' class='Tax'><?= $value['Product']['tax']; ?></span>
                    <span style='display: none' class='product_id'><?= $value['Product']['id']; ?></span>
                    <span style='display: none' class='unit_id'><?= $value['Product']['unit_id']; ?></span>
                    <span style='display: none' class='product_unit_level'><?= $value['Unit']['level']; ?></span>
                    <span style='display: none' class='purchase_unit_level'>1</span>
                    <span style='display: none' class='no_of_piece_per_unit'><?= $value['Product']['no_of_piece_per_unit']; ?></span>
                  </td>
                  <td><?= $this->Form->input('quantity_mode',['type'=>'select','id'=>'no_id_2'.$key,'style'=>'width:100%','class'=>'form-control quantity_mode','options'=>$quantity_mode,'label'=>false,'value'=>$value['Product']['unit_id']]); ?></td>
                  <td><?= $value['Stock']['quantity']; ?></td>
                  <td><?= $value['Product']['threashold']; ?></td>
                  <td><input class="form-control product_quantity" step="any" type="number" value=<?= $value['Product']['threashold']-$value['Stock']['quantity']; ?>></td>
                  <td><i class="fa fa-cart-arrow-down fa-2x  cart_clr add_to_cart" aria-hidden="true"></i></td>
                  <td><a class="btn btn-danger fa fa-trash-o delete_wish_list" ref="#"></a></td>
                </tr>
              <?php endif ?>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</div>

<?php endif; ?>
<div class="row">
  <div class="col-md-12 col-lg-12 col-sm-12">
    <div class="box-body table-responsive" style="margin-top: 50px;">
      <table class="table border_cover_" id="product_table">
        <thead>
          <tr class="blue-bg">
            <th hidden>Type</th>
            <th class="">Brand</th>
            <th style="width:20%;">Product</th>
            <th style="width:10%">Unit Cost</th>
<!--             <th style="width:5%">Unit Price</th>
 -->            <th style="width:1%">Unit</th>
            <th style="width:7%">Quantity</th>
            <th style="width:8%">Net Amount</th>
            <th style="width:7%">Tax Rate</th>
            <th style="width:8%">Tax Amount</th>
            <th style="width:10%">Total</th>
            <th style="width:1%">Action</th>
          </tr>
          <tr>
            <td hidden></td>
           <td>
              <?php echo $this->Form->input('Brand_id',['type'=>'select','style'=>'width:100%','id'=>'Brand_id','class'=>'form-control select2','options'=>$Brand,'label'=>false]); ?>
            </td>
            <td>
              <?php echo $this->Form->input('product_simple',['type'=>'select','style'=>'width:100%','id'=>'product_simple','class'=>'form-control select2','empty'=>[''=>'Select'],'options'=>$Product_list,'label'=>false]); ?>
            </td>
            <td colspan="1">
              <input type="text" id="unit_price_simple" class="simple_single_calculator form-control" value="0"></td>
              <td class="wholesale_type" hidden=""><input type="text" id="wholesale_simple" class="simple_single_calculator form-control" value="0">
              </td>
              <td class="retail_type" hidden=""><input type="text" id="mrp_simple" class="simple_single_calculator form-control" value="0"></td> 
              <td style="width:5%">
                <?= $this->Form->input('quantity_mode_simple',['type'=>'select','style'=>'width:100%','class'=>'form-control select2','id'=>'quantity_mode_simple','options'=>$quantity_mode,'empty'=>'Select','readonly','label'=>false]); ?>
                <input type="hidden" value="1" id="unit_id_simple">
                <input type="hidden" value="1" id="product_unit_level_simple">
                <input type="hidden" value="1" id="purchase_unit_level_simple">
                <input type="hidden" value="1" id="no_of_piece_per_unit_simple">
                <input type="hidden" value="1" id="no_of_box_per_carton_simple">
              </td>
              <td>
                <input type="text" id="quantity_simple" class="simple_single_calculator form-control number" value="0">
              </td>
              <td><input type="text" id="net_value_simple" class="simple_single_calculator number form-control" value="0" readonly></td>
              <td class=""><input type="text" id="tax_simple" class="simple_single_calculator number form-control" value="0" readonly></td>
              <td class=""><input type="text" id="tax_amount_simple" class="simple_single_calculator number form-control" value="0" readonly></td>
              <td>
                <input type="text" id="total_simple" class="simple_single_calculator form-control" value="0" readonly>
              </td>
              <td>
                <i class="fa fa-plus-circle fa-2x add_to_cart_simple"></i>
              </td>
            </tr>
          </thead>
          <tbody>
            <?php if(isset($PurchasedItem)) :?>
            <?php foreach ($PurchasedItem as $key => $value): ?>
            <tr class="blue-pddng">
              <td hidden><input value='<?= $value['ProductType']['name']; ?>' class='form-control' readonly type='text'></td>
              <td style="width:10%"><input value='<?php if($value['Brand']['name']) : echo $value['Brand']['name']; else : echo "GENERAL"; endif; ?>' class='form-control' readonly type='text'></td>
              <td style="width:28%">
                <input  value='<?= $value['Product']['name']; ?> <?= $value['Product']['code']; ?>' class='form-control' type='text' readonly>
                <input  value='<?= $value['PurchasedItem']['id']; ?>'          class='table_id ' name='data[PurchasedItem][PurchasedItem_id][]' type='hidden'>
                <input class="product_id"  value='<?= $value['PurchasedItem']['product_id']; ?>'                                                            name="data[PurchasedItem][product_id][]" type="hidden">
              </td>
              <td style="width:8%" hidden> <input value='<?= floatval($value['PurchasedItem']['unit_cost']); ?>'       class='form-control number  unit_cost'   name='data[PurchasedItem][unit_cost][]'       type='text' readonly></td>
              <td style="width:8%">   <input value='<?= floatval($value['PurchasedItem']['unit_price']); ?>'      class='form-control number single_calculator unit_price'       name='data[PurchasedItem][unit_price][]'      type='text'></td>
              <td class='wholesale_type' hidden=""><input value='<?= $value['PurchasedItem']['wholesale_price']; ?>' class='form-control number single_calculator wholesale_price'  name='data[PurchasedItem][wholesale_price][]' type='text'></td> 
              <td class='retail_type' hidden="">   <input value='<?= $value['PurchasedItem']['mrp']; ?>'             class='form-control number single_calculator mrp'              name='data[PurchasedItem][mrp][]'             type='text'></td> 
              <td style="width:8%">
                <input   value='<?= $value['PurchasedItem']['unit_name']; ?>'  class="form-control quantity_mode" readonly>
                <input   value='<?= $value['PurchasedItem']['quantity_mode']; ?>'  name="data[PurchasedItem][quantity_mode][]" type="hidden">
                <input   value='<?= $value['PurchasedItem']['quantity_mode']; ?>'                                                        name="data[PurchasedItem][unit_id][]" class="form-control unit_id_purchase_tbl" type="hidden">
              </td>
              <td><input value='<?= floatval($value['PurchasedItem']['quantity']); ?>'   class='form-control number single_calculator quantity total_quantity'    name='data[PurchasedItem][quantity][]'   type='text'></td>
              <td><input value='<?= floatval($value['PurchasedItem']['net_value']); ?>'  class='form-control number single_calculator taxable_value'                                     readonly       type='text'></td>
              <td><input value='<?= floatval($value['PurchasedItem']['tax']); ?>'  readonly      class='form-control number single_calculator tax'         name='data[PurchasedItem][tax][]'    readonly    type='text'></td>
              <td><input value='<?= floatval($value['PurchasedItem']['tax_amount']); ?>' class='form-control number single_calculator tax_amount'                                      readonly     type='text'></td>
              <td><input value='<?= floatval($value['PurchasedItem']['total']); ?>'    readonly  class='form-control number single_calculator row_total'   name='data[PurchasedItem][row_total][]'  type='text'></td>
              <td><i class="fa fa-minus-circle fa-2x remove_old_tr" ></i></td>
          </tr>
        <?php endforeach ?>
      <?php endif; ?>
    </tbody>
  </table>
</div>
</div>
</div>
<div class="row">
  <div class="col-md-6 col-lg-6 col-sm-6" >
    <div class="box-body table-responsive" style="margin-top: 0px;" hidden>
      <table class="table border_cover_table table-bordered" id="expense_table" >
        <tbody>
                  <?php if(empty($this->request->data['Purchase']['id'])) :?>
          <tr class="expense-row">    
            <td style="width: 10%;">
              <div class="pull-left"><h4>Expense</h4></div>
              <div class="pull-right"> <i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#Expense_Add"></i></div>
            </td>
            <td>
              <?php echo $this->Form->input('expense',array('type'=>'select','id'=>'expense','class'=>'form-control select2','style'=>'width:100%','options'=>$Expense_list,'label'=>false)); ?>
            </td>
            <td style="width: 20%;">
              <div class="pull-left">
                <?= $this->Form->input('expense_amount_add',array('class'=>'form-control ','type'=>'number','step'=>'any','id'=>'expense_amount','label'=>false,)); ?>
              </div>
              <?php if($status==1 || $status==3) : ?>
              <div class="pull-right"><i class="fa fa-plus-circle fa-2x ad-mar blue-col" id='add_to_expense'></i></div>
            <?php endif; ?>
          </td>
        </tr>
                <?php endif; ?>
        <?php if(isset($PurchaseExpense)) :?>
        <?php foreach ($PurchaseExpense as $key => $value): ?>
        <tr class="blue-pddng expense-rows">
          <td><input class="tbl-expense-id" type="hidden" value="<?php echo $value['id']; ?>"><input class="expense-id" type="hidden" value="<?php echo $value['expense_id']; ?>"></td>

          <td colspan="1">
            <?php echo $value['name']; ?>
          </td>
          <td colspan="1"><?= $this->Form->input('expenseamount',array('class'=>'form-control expense_amount','type'=>'number','step'=>'any','readonly','required','id'=>'expenseamount','value'=>$value['amount'],'label'=>false,)); ?></td>
          <?php if($status==1 || $status==3) : ?>
          <td>
            <div class="first-half"> <i class="fa fa-minus-circle fa-2x remove_old_expense_tr" ></i></div>
            <div class="second-half"></div>
          </td>
        <?php endif; ?>
      </tr>
    <?php endforeach; ?>
  <?php endif; ?>
</tbody>
<tfoot>
 <tr >    
  <td style="width: 20%;">
  </td>
  <td>
    <h4>Expense Total</h4>
  </td>
  <td style="width: 37%;">
    <div class="pull-left">
      <?= $this->Form->input('expense_total',array('class'=>'form-control ','type'=>'number','step'=>'any','id'=>'expense_total','label'=>false,)); ?>
    </div>
    <div></div>
  </td>
</tr>
</tfoot>
</table>
</div>
</div>
<div class="col-md-6 col-lg-6 col-sm-6">
  <div class="box-body table-responsive" style="margin-top: 0px;">
    <table class="table border_cover_table ">
      <tbody>
        <tr>
          <td colspan="" style="width:19%;"><h4>Total</h4></td>
         <!-- <td style="margin-left:2%" hidden>
            <?= $this->Form->input('total_quantity',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'totalquantity','label'=>false,)); ?>
          </td> -->
          <td style="width:22%;margin-left:-60px" ><?= $this->Form->input('net_total',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'net_total','label'=>false,)); ?></td>
          <td style="width:10%;"></td>
          <td style="width:20%;margin-right:15%"><?= $this->Form->input('row_total_tax',array('class'=>'form-control','type'=>'number','step'=>'any','readonly','id'=>'row_total_tax','label'=>false,)); ?></td>
          <td style="width:22%;margin-right:8%">
            <?= $this->Form->input('total',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'total','label'=>false,)); ?>
          </td>
          <?php if($status==1 || $status==3) : ?>
          <td></td>
        <?php endif; ?>
        <td></td>
        <td></td>
      </tr>
         <tr>
      <td colspan="2"></td>
      <td><label>SGST</label></td>
      <td>
      <?= $this->Form->input('sgst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'sgst_amt_total','readonly','label'=>false,)); ?>
      </td><td></td>
      <?php if($status==1 || $status==3) : ?>
      <td></td>
      <?php endif; ?>
      <td></td>
      <td></td>
      </tr>
       <tr>
      <td colspan="2"></td>
      <td><label>CGST</label>
      <td>
      <?= $this->Form->input('cgst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'cgst_amt_total','readonly','label'=>false,)); ?>
      </td>
      <td></td>
      <?php if($status==1 || $status==3) : ?>
      <td></td>
      <?php endif; ?>
      <td></td>
      <td></td>
      </tr>
      <tr>
      <td colspan="2"></td>
      <td><label>IGST</label>
      <td>
      <?= $this->Form->input('igst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'igst_amt_total','readonly','label'=>false,)); ?>
      </td>
      <td></td>
      <?php if($status==1 || $status==3) : ?>
      <td></td>
      <?php endif; ?>
      <td></td>
      <td></td>
      </tr>
       <td colspan=""></td>
          <td colspan="2"><div class="form-group" style="margin-top: 25px;">
          <select class="form-control select2" id="select_discount_type">
            <option value="1">INR</option>
            <option value="2">%</option>
          </select>
        </div></td>     
       <td>
          <label>Discount</label>
          <?= $this->Form->input('discount',array('class'=>'form-control','type'=>'text','readonly','max'=>'100','id'=>'discount','label'=>false)); ?>
        </td>
        <td>
          <label>Discount Amount</label>
          <?= $this->Form->input('discount_amount',array('class'=>'form-control main_calculator','type'=>'text','id'=>'discount_amount','label'=>false)); ?>
        </td>
      <?php if($status==1 || $status==3) : ?>
      <td></td>
      <?php endif; ?>
      <td></td>
      <td></td>
      </tr>
      <tr hidden>
      <td colspan="3"></td>
      <td></td>
      <td>
        <label>Tax</label>
        <div class="form-group">
          <?= $this->Form->input('total_tax',array('class'=>'form-control main_calculator','type'=>'text','max'=>'100','id'=>'total_tax','label'=>false)); ?>
          <?= $this->Form->input('total_tax_import',array('class'=>'form-control main_calculator','type'=>'text','max'=>'100','id'=>'total_tax_import','label'=>false)); ?>
        </div>
      </td>
      <td><label>Tax Amount</label>
        <div class="form-group">
          <?= $this->Form->input('total_tax_amount',array('class'=>'form-control main_calculator','readonly','type'=>'text','id'=>'total_tax_amount','label'=>false)); ?>
        </div>
      </td>
      <?php if($status==1 || $status==3) : ?>
      <td></td>
    <?php endif; ?>
    <td></td>
    <td></td>
  </tr>
  <tr class="blue-pddng" style="display: none;">
    <td></td>
    <td><?= $this->Form->input('other_name',array('class'=>'form-control','type'=>'text','required','id'=>'other_name','value'=>'Round Off','label'=>false,)); ?></td>
    <td><?= $this->Form->input('other_value',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'other_value','label'=>false,)); ?></td>
    <?php if($status==1 || $status==3) : ?>
    <td></td>
  <?php endif; ?>
</tr>
       <tr>
      <td colspan="3"></td>
       <td ><h4>Grand Total</h4></td>
  <td><div class="form-group">
    <?= $this->Form->input('grand_total',array('class'=>'form-control main_calculator number','type'=>'text','step'=>'any','required','id'=>'grand_total','label'=>false,)); ?> 
  </div></td>
      <?php if($status==1 || $status==3) : ?>
      <td></td>
      <?php endif; ?>
      <td></td>
      <td></td>
      </tr>
      </tbody>
<tfoot>
  <tr class="blue-pddng">
    <?php if(isset($PurchasedItem)) : ?>
    <?php $flag=$this->request->data['Purchase']['flag']; ?>
    <?php if($status==1) : ?>
    <td></td>
    <td hidden ><button type='submit' id="cancel_button" name='data[Purchase][process]' value='cancel' class="btn btn-success">Cancel</button></td>
    <td><button id="delete_purchase" type='submit' name='data[Purchase][process]' value='delete' class="btn btn-danger class_delete">Delete</button></td>
    <td><button type='submit' id="update_button" name='data[Purchase][process]' value='update' class="btn btn-success">Update</button></td>
    <td><button type='submit' id="order_delivery" name='data[Purchase][process]' value='delivery' class="btn btn-success">Order Delivery</button></td>
    <td><a target="_blank" href="<?= $this->webroot.'Print/fpdf_purchase/'.$this->request->data['Purchase']['id']; ?>"><button type='button' class="btn btn-success">Print</button></a></td>
  <?php elseif($status==3) : ?>
  <td colspan="4"></td>
  <td><button id="save_button" type='submit' name='data[Purchase][process]' value='after_altration' class="btn btn-success">Save</button></td>
<?php elseif($status==2) : ?>
  <td></td>
  <td></td>
  <td></td>
  <td><button id="after_delete_purchase" type='submit' name='data[Purchase][process]' value='after_delete' class="btn btn-danger class_delete">Delete</button></td>
   <td><button type='submit' id="update_button_alter" name='data[Purchase][process]' value='altration' class="btn btn-success">Update</button></td>
   <td> <a target="_blank" href="<?= $this->webroot.'Print/fpdf_purchase/'.$this->request->data['Purchase']['id']; ?>"><button type='button' class="btn btn-success">Print</button></a></td>
  <?php else : ?>
  <td colspan="4"></td>
  <td><button id="delete_purchase" type='submit' name='data[Purchase][process]' value='delete' class="btn btn-danger class_delete">Delete</button></td>
<?php endif; ?>
<?php else : ?>
  <td></td><td></td><td></td>
  <td><button type='submit' id="purchase_order" name='data[Purchase][process]' value='save'     class="btn btn-success pull-right" disabled>Purchase Order</button></td>
  <td><button type='submit' id="order_delivery" name='data[Purchase][process]' value='delivery' class="btn btn-success" disabled>Order Delivery</button></td>
<?php endif; ?>
<?php if($status==1 || $status==3) : ?>
  <td></td>
<?php endif; ?>
<td></td>
<td></td>
</tr>
</tfoot>
</table>
</div>
</div>
</div>
<?= $this->Form->end(); ?>
</div>
</div>
<div id="lastpurchase" class="modal fade" role="dialog" >
  <div class="modal-dialog modal-lg" style="width: 50%">
    <div class="modal-content pull-middle">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="box-body table-responsive no-padding xs_tp">
          <table class="table table-bordered table-hover" id="last_purchase_details_table">
            <thead>
              <tr class="blue-bg">
                <th>Party</th>
                <th>Invoice</th>
                <th>Date</th>
                <th>Rate</th>
                <th>Unit</th>
                <th>Quantity</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
            </tfoot>
          </table>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</section>
<?php require('party_modal.php') ?>
<?php require('Add_Modal/Product_Add_modal.php') ?>
<?php require('Add_Modal/Expense_Add_modal.php') ?>
<script type="text/javascript">
<?php require('purchase.js'); ?>
var status='<?= $status; ?>';
if(status==2)
{
  $('.add_button').hide();
}
$("#party_id").change();
$.fn.button_disable();
</script>




