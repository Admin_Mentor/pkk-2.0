var purchase_type='<?= $purchase_type; ?>';
if(purchase_type=='CreditPurchase')
{
$('.toggle_button_for_purchase_type')[1].checked = true;
$('.toggle_button_for_purchase_type').change();
}
else
{
	$('.toggle_button_for_purchase_type')[0].checked = true;
$('.toggle_button_for_purchase_type').change();
}
var unit_change_flag=0;
$('#PurchasePurchaseForm').on('keyup keypress', function(e) {
	var keyCode = e.keyCode || e.which;
	if (keyCode === 13) { 
		e.preventDefault();
		return false;
	}
});
$('#party_id').change(function(){
	// $.fn.party_vice_purchase_cart_table_function();
	var id=$(this).val();
	var url_address= '<?php echo $this->webroot; ?>'+'Purchase/get_party_address/'+id;
	$.ajax({
		type: "get",  
		url:url_address,
		dataType:'json',
		success: function(response) {
			$('#address').val(response.address);
			$('#state_id').val(response.state_id);
				$.fn.main_calculator();

		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
});
$(document).on('keypress','#unit_price_simple',function(e){
     if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57 || e.which==47)) {
       return false;
     }
   });
$('#hide_exp').click(function(){
	$("#exp_div").toggle();
});
function roundToTwo(num) {
	// return +(Math.round(num + "e+3")  + "e-3");
	return +(parseFloat(num).toFixed(2));
}
$.fn.expense_calculator=function(){
    var total=0;
    $('#expense_table tbody .expense-rows').each(function(){
        single_total=$(this).closest('tr').find('td input.expense_amount').val();
        total+=parseFloat(single_total);
    });
    total=roundToTwo(total);
    
        var grand_total=parseFloat(total);
    $('#expense_total').val(Math.round(grand_total));
}
$.fn.expense_distribution=function(){
    var cost_total=0;
    var expense_total = $('#expense_total').val() ? $('#expense_total').val():0;
    
    $('#product_table tbody tr').each(function(){
        single_total=$(this).closest('tr').find('td input.unit_price').val()?$(this).closest('tr').find('td input.unit_price').val():0;
        single_quantity=$(this).closest('tr').find('td input.quantity').val()?$(this).closest('tr').find('td input.quantity').val():0;
        sigle_cost_total = parseFloat(single_total)*parseFloat(single_quantity);
        cost_total+=parseFloat(sigle_cost_total);
    });
    $('#product_table tbody tr').each(function(){
        single_cost=$(this).closest('tr').find('td input.unit_price').val();
        single_quantity=$(this).closest('tr').find('td input.quantity').val()?$(this).closest('tr').find('td input.quantity').val():0;
        single_cost_total = parseFloat(single_total)*parseFloat(single_quantity);
        prop_cost = parseFloat(single_cost) +(parseFloat(expense_total)*parseFloat(single_cost/cost_total));
         $(this).closest('tr').find('td input.unit_cost').val(parseFloat(prop_cost).toFixed(2));
        if(isNaN(prop_cost))
        {
        $(this).closest('tr').find('td input.unit_cost').val(0);
        }
    });
    
    
    // total=roundToTwo(total);
    
    
}
$(document).on('keyup','.single_calculator',function(){
	$(this).closest('tr').find('td input.single_calculator').each(function(){
		var unit_price=$(this).closest('tr').find('td input.unit_price').val();
		var tax=$(this).closest('tr').find('td input.tax').val();
		//var tax=0;
		var quantity=$(this).closest('tr').find('td input.quantity').val();
		//var discount=$(this).closest('tr').find('td input.discount').val();
		var discount=0;
		var net_value_amount=unit_price*quantity;
		var taxable_value_amount=net_value_amount-discount;
	    var tax_amount=net_value_amount*tax/100;
		var tax_amount=parseFloat(tax_amount);
		var total=parseFloat(tax_amount)+parseFloat(taxable_value_amount);
		 $(this).closest('tr').find('td input.net_value').val(parseFloat(net_value_amount).toFixed(2));
		 $(this).closest('tr').find('td input.tax_amount').val(parseFloat(tax_amount).toFixed(2));
		 $(this).closest('tr').find('td input.taxable_value').val(roundToTwo(taxable_value_amount));
		$(this).closest('tr').find('td input.row_total').val(parseFloat(total).toFixed(2));
	});
	$.fn.main_calculator();
	$.fn.expense_distribution();
});
$.fn.main_calculator=function(){
	var total=0;
	var net_total =0;
		var total_tax=0;
	//var quantity=0;
	var total_quantity=0;
	$('#product_table tbody tr').each(function(){
		single_total=$(this).closest('tr').find('td input.row_total').val();
		var tax_amount=$(this).closest('tr').find('td input.tax_amount').val();
		total_tax=parseFloat(total_tax)+parseFloat(tax_amount);
		net_value=$(this).closest('tr').find('td input.taxable_value').val();
		//net_value=$(this).closest('tr').find('td input.row_total').val();
		if(typeof single_total != 'undefined')
			total+=parseFloat(single_total);
		if(typeof net_value != 'undefined')
			net_total+=parseFloat(net_value);
	
    
     var quantity=$(this).closest('tr').find('td input.total_quantity').val();
     var quantity_mode=$(this).closest('tr').find('td input.quantity_mode').val();
  		total_quantity=parseFloat(total_quantity)+parseFloat(quantity);
	});
	var state_id=$('#state_id').val();
	if(state_id==19)
	{
		var tax_amount_split=parseFloat(total_tax)/2;
		tax_amount_split=tax_amount_split.toFixed(3);
		$('#cgst_amt_total').val(tax_amount_split);
		$('#sgst_amt_total').val(tax_amount_split);
		$('#igst_amt_total').val(0.00);	
	}
	else
	{
	var tax_amount_split=parseFloat(total_tax);
	tax_amount_split=tax_amount_split.toFixed(2);
	$('#cgst_amt_total').val(0);
	$('#sgst_amt_total').val(0);
	$('#igst_amt_total').val(tax_amount_split);
	}


	$('#totalquantity').val(total_quantity);
//$('#total').val(total);
	var roundoff=0;
	total=parseFloat(total).toFixed(2);
	net_total = parseFloat(net_total).toFixed(2);
		total_tax = parseFloat(total_tax).toFixed(2);
	$('#total').val(total);
		$('#net_total').val(net_total);
	$('#row_total_tax').val(total_tax);

	$('#other_value').val(roundoff.toFixed(2));
	var insurance_amount=$('#insurance_amount').val()?$('#insurance_amount').val():0;
	var insurance_tax=$('#insurance_tax').val()?$('#insurance_tax').val():0;
	insurance_tax_amount=insurance_amount*insurance_tax/100;
	var insurance_total=parseFloat(insurance_amount)+parseFloat(insurance_tax_amount);
	$('#insurance_total').val(insurance_total);

	var miscellanious_expense_amount=$('#miscellanious_expense_amount').val()?$('#miscellanious_expense_amount').val():0;
	var miscellanious_expense_tax=$('#miscellanious_expense_tax').val()?$('#miscellanious_expense_tax').val():0;
	miscellanious_expense_tax_amount=miscellanious_expense_amount*miscellanious_expense_tax/100;
	var miscellanious_expense_total=parseFloat(miscellanious_expense_amount)+parseFloat(miscellanious_expense_tax_amount);
	$('#miscellanious_expense_total').val(miscellanious_expense_total);

	var other_value=$('#other_value').val();
	if(other_value)
	{
		var grand_total=parseFloat(total)+parseFloat(other_value)+parseFloat(insurance_total)+parseFloat(miscellanious_expense_total);	
	}
	else
	{
		var grand_total=parseFloat(total)+parseFloat(insurance_total)+parseFloat(miscellanious_expense_total);
	}
	var main_total=grand_total;
	var discount_amount=0;
	if($('#discount_amount').val())
	{
		discount_amount=$('#discount_amount').val();
	}
	var discounted_amount=parseFloat(main_total)-parseFloat(discount_amount);
	// var total_tax=0;
	// if($('#toggle_button_for_tax_visibility').is(":checked")){
	// 	var tax=$('#total_tax_import').val();
	// 	$('#total_tax_import').show();
	// 	$('#total_tax').hide();
	// 		}
	// 		else
	// 		{
	// 		var tax=$('#total_tax').val();
	// 		$('#total_tax').show();
	// 		$('#total_tax_import').hide();
	// 		}
	// // if($('#total_tax').val())
	// // {
	// 	total_tax=tax;
	// //}	
	// var total_tax_amount=parseFloat(total_tax)*parseFloat(discounted_amount)/100;
	// var total_tax_amount=total_tax_amount.toFixed(2);
	// $('#total_tax_amount').val(total_tax_amount);
	
	grand_total=parseFloat(discounted_amount);
	$('#grand_total').val(parseFloat(grand_total).toFixed(2));
}
$('.main_calculator').keyup(function(){
	$.fn.main_calculator();
});

$.fn.main_calculator();
$.fn.button_disable=function(){
	var length=$('#product_table tbody tr').length;
	if(length>0)
	{
		$('button[type="Submit"]').attr('disabled',false);	
	}
	else
	{
		$('button[type="Submit"]').attr('disabled',true);
	}
};
$('.product_quantity').keyup(function(e){
	var tr = $(this).parents('tr');
	var quantity = $(this).val();
	if (e.keyCode == 13) 
	{
		if(parseFloat(quantity)>0){
			$(tr).find('.add_to_cart').trigger('click');
		}
		else{
			alert('Check quantity');
			$(this).focus();
		}
		return false;
	}
})
$(document).on('click','.add_to_cart',function(){
	//$("#party_id").change();
	var party_id=$('#party_id').val();
	var invoice_no=$('#invoice_no').val();
	if(!party_id)
	{
		$('#party_id').select2('open');
		return false;
	}
	if(!invoice_no)
	{
		$('#invoice_no').focus();
		return false;
	}
	var product_quantity=$(this).closest('tr').find('td input.product_quantity').val();
	if(!$.isNumeric(product_quantity) || product_quantity=='' || product_quantity<=0 )
	{
		$(this).closest('tr').find('td input.product_quantity').focus();
		return false;
	}
	var ProductType_name=$(this).closest('tr').find('td.ProductType_name').text();
	var quantity_mode=$(this).closest("tr").find('select option:selected').attr("selected", "selected").val();
	var quantity_mode_text=$(this).closest("tr").find('select option:selected').attr("selected", "selected").text();
	var unit_id=$(this).closest('tr').find('td.Product span.unit_id').text();
	var product_unit_level=$(this).closest('tr').find('td.Product span.product_unit_level').text();
	var purchase_unit_level=$(this).closest('tr').find('td.Product span.purchase_unit_level').text();
	var no_of_piece_per_unit=$(this).closest('tr').find('td.Product span.no_of_piece_per_unit').text();
	var Brand_name=$(this).closest('tr').find('td.Brand_name').text();
	var Product_name=$(this).closest('tr').find('td.Product span.Product_name').text();
	var product_id=$(this).closest('tr').find('td.Product span.product_id').text();
	var unit_price=$(this).closest('tr').find('td.Product span.cost').text();
	var mrp=$(this).closest('tr').find('td.Product span.mrp').text();
	var excluded_unit_array = ["Carton", "Container", "Box"];
	var wholesale_price=$(this).closest('tr').find('td.Product span.wholesale_price').text();
	console.log(purchase_unit_level);
	if(purchase_unit_level == 2){
		unit_price=(unit_price*no_of_piece_per_unit).toFixed(2);
		mrp=(mrp*no_of_piece_per_unit).toFixed(2);
		wholesale_price=(wholesale_price*no_of_piece_per_unit).toFixed(2);
	}
	var net_value_amount=unit_price*product_quantity;
	// if($('#toggle_button_for_tax_visibility').is(":checked")){
     var tax=0;
      // }
      // else
      // {
      //   	var tax=$(this).closest('tr').find('td.Product span.Tax').text();

      // }
  	var tax=$(this).closest('tr').find('td.Product span.Tax').text();
	var tax_amount = roundToTwo(parseFloat(net_value_amount,10)*(parseFloat(tax,10)/100));
	var tax_total=parseFloat(tax_amount);
	var total=roundToTwo(net_value_amount+tax_total);
	var Product_type_field="<input type='text' class='form-control' readonly value='"+ProductType_name+"'>";
	var Brand_name_field="<input type='text' class='form-control' readonly value='"+Brand_name+"'>";
	var Product_name_field="<input type='text' class='form-control' readonly value='"+Product_name+"'><input class='product_id' type='hidden' value='"+product_id+"' name='data[Purchase][product_id][]'>";
	//cost entry
	var unit_cost_field="<input type='text' class='form-control number  single_calculator unit_cost' value='"+unit_price+"' name='data[Purchase][unit_cost][]'>";	
	//cost entry ends
	var unit_price_field="<input type='text' class='form-control number single_calculator unit_price'  value='"+unit_price+"' name='data[Purchase][unit_price][]'>";
	var MRP_field="<input type='text' class='form-control number single_calculator mrp' name='data[Purchase][mrp][]' value='"+mrp+"'>";
	var quantity_mode_field="<input type='text'  readonly class='form-control quantity_mode'  value='"+quantity_mode_text+"'><input type='hidden'  readonly class='form-control '  value='"+purchase_unit_level+"'><input type='hidden'  readonly class='form-control unit_id_purchase_tbl'  value='"+quantity_mode+"' name='data[Purchase][unit_id][]'>";
	var wholesale_price_field="<input type='text' class='form-control number single_calculator wholesale_price' name='data[Purchase][wholesale_price][]'  value='"+wholesale_price+"' >";
	var purchase_qty_field="<input type='text' class='form-control number single_calculator quantity total_quantity'  value='"+product_quantity+"' name='data[Purchase][quantity][]'>";
	var net_value_field="<input type='text' class='form-control number single_calculator net_value'  value='"+net_value_amount+"' >";
	var discount_field="<input type='text' class='form-control number single_calculator discount'  value='0' name='data[Purchase][discount][]'>";
	var taxable_value_field="<input type='text' class='form-control number single_calculator taxable_value'  value='"+net_value_amount+"'>";
	var tax_field="<input type='text' class='form-control number single_calculator tax'  value='"+tax+"' name='data[Purchase][tax][]'>";
	var tax_amount_field="<input type='text' class='form-control number single_calculator tax_amount'  value='"+tax_amount+"' name='data[Purchase][tax_amount][]'>";
	var total_field="<input type='text' class='form-control number single_calculator row_total'  value='"+total+"' name='data[Purchase][row_total][]'>";
	flag="true";
	$('#product_table tbody tr').each(function(){
		var table_product=($(this).find('td:nth-child(3)').find('input.product_id').val());
		var table_unit=($(this).find('td input.unit_id_purchase_tbl').val());
		if(table_product == product_id)
		{	
			if(table_unit == quantity_mode){
				$(this).find('td').find('input.quantity').select().focus();
				flag="false";
			}
		}
	});
	if(flag=="false")
	{
		alert("Product already added");
		return false;
	}
	$('#product_table tbody').prepend('<tr class="blue-pddng">\
		<td hidden>'+Product_type_field+'</td>\
		<td hidden>'+Brand_name_field+'</td>\
		<td>'+Product_name_field+'</td>\
		<td hidden style="">'+unit_cost_field+'</td>\
		<td >'+unit_price_field+'</td>\
		<td class="wholesale_type" hidden>'+wholesale_price_field+'</td>\
		<td class="retail_type" hidden>'+MRP_field+'</td>\
		<td class="retail_type">'+quantity_mode_field+'</td>\
		<td>'+purchase_qty_field+'</td>\
		<td>'+taxable_value_field+'</td>\
		<td>'+tax_field+'</td>\
		<td>'+tax_amount_field+'</td>\
		<td>'+total_field+'</td>\
		<td><i class="fa fa-minus-circle fa-2x remove_tr" ></i></td>\
		</tr>');
	$.fn.Country_type();
	$.fn.product_configuration_type();
	// $('.single_calculator').keyup();
	$.fn.main_calculator();
	$(this).closest('tr').find('td .quantity_mode').val(1).change();
	$(this).closest('tr').find('td input.product_quantity').val('0');
	$.fn.button_disable();
	$.fn.expense_distribution();
});
$(document).on('click','.delete_wish_list',function(){
	if(!confirm('Are You Sure'))
	{
		return false;
	}
	var index=$(this).closest('tr').index();
	var product_id=$(this).closest('tr').find('td.Product span.product_id').text();
	var id=$(this).val();
	var url_address= '<?php echo $this->webroot; ?>'+'Purchase/delete_wish_list/'+product_id;
	$.ajax({
		type: "get",  
		url:url_address,
		dataType:'json',
		success: function(response) {
			if(response.result!='Success')
			{
				return false;
			}
			$('#table_wish_list').DataTable().destroy();
			$('#table_wish_list tbody tr:eq('+index+')').remove();
			$('#table_wish_list').DataTable();
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
});
$('#add_expense').click(function(){
  var modal_expense_name=$('#modal_expense_name').val();
  
  var data={
    name:modal_expense_name,
  };
  var url_address= '<?php echo $this->webroot; ?>Purchase/expense_add_ajax';
 
$.ajax({
        type: "POST",  
        url:url_address,
        data:data,
        dataType:'json',
        success: function(response) {
            if(response.result!='Success')
            {
                return false;
            }
            $('#expense').append($("<option></option>").attr("value",response.key).text(response.value));
            $('#expense').val(response.key).trigger('change');
            $('#modal_expense_name').val('');
            $('#Expense_Add').modal('hide');
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });

  
});

 $('#add_to_expense').click(function(event){
	event.preventDefault();
	stock_quantity=3000;
	var ExpenseExist = 0;
	var tablecount=$("#product_table tfoot tr .expense-rows").length;
	var expense_text=$('#expense option:selected').text();
	var expense_amount=$('#expense_amount').val();
	var expense_id=$('#expense option:selected').val();
	$("#product_table tfoot .expense-rows").each(function () {
		var expenseId = $(this).closest('tr').find('td input.expense-id').val();
		if (expense_id== expenseId) {
			ExpenseExist = 1;
			return false;
		}
	});
	if(ExpenseExist)
	{
		alert("This Expense already added");
		$('#expense').select2('open');
		return false;
	}

	var quantity=parseFloat(quantity,10);
	if(expense_amount<=0)
	{
		$('#expense_amount').focus();
		return false;
	}
	if(!expense_text){
		$('#expense').select2('open');
		return false;
	}
	$('#expense_table tbody .expense-row').after('<tr class="expense-rows">\
		<td colspan="1"></td>\
		<td colspan="1">\
		<input class="productlist expense-id" hidden name="data[Purchase][expense_id][]" value="'+expense_id+'">\
		'+expense_text+'\
		</td>\
		<td colspan="1"><div class="pull-left"><div class="input number"><input name="data[Purchase][expense_amount][]" class="form-control expense_amount" step="any" value='+expense_amount+' type="number" readonly></div></div>\
		<div class="pull-right"><i class="fa fa-minus-circle fa-2x ad-mar remove_expense_tr"></i></div></td>\
		</tr>');
	$('#expense_amount').val('0');
	$.fn.expense_calculator();
	$.fn.expense_distribution();
});
$(document).on('click','.remove_tr',function(){
	$(this).closest('tr').remove();
	$.fn.main_calculator();
	$.fn.button_disable();
	$.fn.expense_distribution();
});
$(document).on('click','.remove_expense_tr',function(){
    $(this).closest('tr').remove();
    $.fn.expense_calculator();
    $.fn.expense_distribution();
    
});
$(document).on('click','.remove_old_expense_tr',function(){
	var row_index=$(this).closest('tr').index();
	var tr= $(this).closest('tr');
	var id=$(this).closest('tr').find('td input.tbl-expense-id').val();
	var url_address= '<?php echo $this->webroot; ?>'+'Purchase/PurchaseExpense_delete/'+id;
	$.ajax({
		type: "POST",
		url:url_address,
		dataType:'json',
		success: function(data) {
			if(data.result!='Success')
			{
				alert(data.result);
				return false;
			}
			tr.remove();
			// $('#product_table tfoot tr:eq('+row_index+')').remove();
			$.fn.main_calculator();
			$.fn.button_disable();
			 $.fn.expense_calculator();
			$.fn.expense_distribution();
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
});
$(document).on('click','.remove_old_tr',function(){
	if(!confirm('Are You Sure'))
	{
		return false;
	}
	var row_index=$(this).closest('tr').index();
	var id=$(this).closest('tr').find('td input.table_id').val();
	var url_address= '<?php echo $this->webroot; ?>'+'Purchase/PurchasedItem_delete/'+id;
	$.ajax({
		type: "POST",
		url:url_address,
		dataType:'json',
		success: function(data) {
			if(data.result!='Success')
			{
				alert(data.result);
				return false;
			}
			$('#product_table tbody tr:eq('+row_index+')').remove();
			$.fn.main_calculator();
			$.fn.button_disable();
			$.fn.expense_distribution();
			$('button[type="Submit"]').attr('disabled',false);	
			$('#update_button_alter').click();
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});
});
$('#add_party').click(function()
{
	var email = $("#email").val();
	var opening_balance = $("#opening_balance_modal").val();
	if(email)
	{
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if(!regex.test(email))
		{
			alert('Invalid Email Format!');
			$("#email").focus();
			return false;
		}
	}
	if(opening_balance != '' && parseInt(opening_balance) <0)
	{
		alert('Invalid Opening Balance!');
		$("#opening_balance_modal").focus();
		return false;
	}
	if(true) {
		var data = $('#Party_Form').serialize();
		var url_address = '<?php echo $this->webroot; ?>' + 'Purchase/Party_Account_head_ajax/';
		$.ajax({
			type: "POST",
			url: url_address,
			data: data,
			dataType: 'json',
			success: function (response) {
				if (response.result != 'Success') {
					return false;
				}
				$('#party_id').append($("<option></option>").attr("value", response.key).text(response.value));
				$('#party_id').val(response.key).trigger('change');
				$('#Party_Form')[0].reset();
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}
});
$.fn.party_vice_purchase_cart_table_function=function(){
	var id=$('#party_id').val();
	$.post( "<?= $this->webroot ?>Purchase/get_party_purchased_list/"+id ,function( data ) {
		$('#party_vice_purchase_cart_table').DataTable().destroy();
		$('#party_vice_purchase_cart_table tbody').empty();
		$.each(data.PurchasedItems,function(key,value){
			var party_vice_purchase_cart_table="<tr class='blue-pddng'>";
			party_vice_purchase_cart_table+="<td class='ProductType_name'>"+value.ProductType+"</td>";
			party_vice_purchase_cart_table+="<td class='Brand_name'>"+value.Brand+"</td>";
			party_vice_purchase_cart_table+="<td class='Product'>";
			party_vice_purchase_cart_table+="<span class='Product_name'>"+value.product_name+"</span>";
			party_vice_purchase_cart_table+="<span style='display: none' class='cost'>"+value.cost+"</span>";
			party_vice_purchase_cart_table+="<span style='display: none' class='wholesale_price'>"+value.wholesale_price+"</span>";
			party_vice_purchase_cart_table+="<span style='display: none' class='purchase_unit_level'>"+value.wholesale_price+"</span>";
			party_vice_purchase_cart_table+="<span style='display: none' class='tax'>"+value.tax+"</span>";
			party_vice_purchase_cart_table+="<span style='display: none' class='product_id'>"+value.id+"</span></td>";
			party_vice_purchase_cart_table+="<td><select name='data[Purchase][quantity_mode]' style='width:100%' class='form-control select2 quantity_mode'>";
			// console.log(jQuery('.quantity_mode').attr('class').split('opti')[0]);
			$(".quantity_mode:first option").each(function(){
				party_vice_purchase_cart_table+="<option value='"+$(this).val()+"'>"+$(this).text()+"</option>";
			});
			party_vice_purchase_cart_table+="<td ><span  class='cost'>"+value.unit_price+"</span></td >";
			party_vice_purchase_cart_table+="<td ><span  class='cost'>"+value.quantity+"</span></td >";
			party_vice_purchase_cart_table+="<td><input style='width:120px !important'  class='form-control product_quantity' step='any' type='number' value='0'></td>";
			party_vice_purchase_cart_table+="<td><i class='fa fa-cart-arrow-down fa-2x  cart_clr add_to_cart' aria-hidden='true'></i></td>";
			party_vice_purchase_cart_table+="</tr>";
			$('#party_vice_purchase_cart_table tbody').append(party_vice_purchase_cart_table);
		});
		$('#party_vice_purchase_cart_table').DataTable();
	}, "json");
};
// $.fn.party_vice_purchase_cart_table_function();
$('#add_state_button').click(function(){
	var name=$('#state_name_modal').val();
	if(name=='')
	{
		$('#state_name_modal').focus();
		return false;
	}
	var code=$('#state_code_modal').val();
	if(code=='')
	{
		$('#state_code_modal').focus();
		return false;
	}
	var website_url='<?php echo $this->webroot; ?>Customer/add_state_ajax';
	var data={
		name:name,
		code:code,
	};
	$.ajax({
		method: "POST",
		url: website_url,
		data: data,
		dataType:'json',
	}).done(function( data ) {
		if(data.result!='Success')
		{
			alert(data.result);
			return false;
		}
		$('#state_add_modal').modal('toggle');
		$('#state_name_modal').val('');
		$('#state_code_modal').val('');
		$('#state_id').append($("<option></option>").attr("value",data.key).text(data.value));
		$('#state_id').val(data.key).trigger('change');
	});
});

$('.main-sidebar').ready(function(){
	$('body').attr('class','skin-black sidebar-mini sidebar-collapse');
});
$('button[type="Submit"]').click(function(){
	var total=$("#grand_total").val();
});
$('.class_delete').click(function(){
	if(!confirm('Are You Sure'))
	{
		return false;
	}
});
$('form').submit(function(){
$(this).find('button[type=submit]').hide();
});
$.fn.gst_state_check=function()	
{
	var state=$('#state_id').val();
	var toggle_button_for_gst_visibility=$('#toggle_button_for_gst_visibility').prop("checked");
}
$('#toggle_button_for_gst_visibility').change(function(){
  // $.fn.gst_state_check();
});
shortcut.add("alt+p", function() {
	$('#purchase_order').click();
});
shortcut.add("alt+o", function() {
	$('#order_delivery').click();
});
shortcut.add("alt+u", function() {
	$('#update_button').click();
});
shortcut.add("alt+c", function() {
	$('#cancel_button').click();
});
shortcut.add("alt+a", function() {
	$('#alter_button').click();
});
shortcut.add("alt+s", function() {
	$('#save_button').click();
});
// $.fn.qnt_calculator=function(){
// 	$('#product_table tbody tr').each(function(){
// 		$(this).childre
// 		var unit_id=$(this).closest('td').find('.quantity_mode').val();
// 		alert(unit_id);
// 	var tr=$(this).closest('tr');
// 	var url='<?= $this->webroot."Purchase/GetUnitLevel/"; ?>'+unit_id;
// 	$.post(url, function(result){
// 		// console.log(tr.closest('tr').find('td.Product span.purchase_unit_level').text());
// 		tr.closest('td').find('.Product span.purchase_unit_level').text(result.Unit.level);
// 		// console.log(tr.closest('tr').find('td.Product span.purchase_unit_level').text());
// 	},'json');
// 	});
// }
// $('.quantity_mode').val(2).trigger('change');
//$.fn.qnt_calculator();
$(document).on('change','.quantity_mode',function(){
	var unit_id=$(this).val();
	var tr=$(this).closest('tr');
	var url='<?= $this->webroot."Purchase/GetUnitLevel/"; ?>'+unit_id;
	$.post(url, function(result){
		// console.log(tr.closest('tr').find('td.Product span.purchase_unit_level').text());
		tr.closest('tr').find('td.Product span.purchase_unit_level').text(result.Unit.level);
		// console.log(tr.closest('tr').find('td.Product span.purchase_unit_level').text());
	},'json');
});
// code for simple purchase
$('#quantity_mode_simple').change(function(){  
  var quantity_mode=$('#quantity_mode_simple').val();
  unit_change_flag=1;
  $.fn.get_product_details(quantity_mode);
});
$('#product_simple').change(function(){
  unit_change_flag=0;
  $.fn.get_product_details(null);

});
$.fn.get_product_details=function(quantity_mode){
	var id=$('#product_simple').val();
	var party_id=$('#party_id').val();
	if(!id)
	{
		return false;
	}
	
	//var quantity_mode=$('#unit_price_simple').val();
	var url_address= '<?php echo $this->webroot; ?>'+'Product/get_Product_ajax/'+id+'/'+party_id+'/'+quantity_mode;
	$.ajax({
		type: "POST",
		url:url_address,
		dataType:'json',
		success: function(data) {
			if(data.result!='Success')
			{
				alert(data.result);
				return false;
			}
			//$('#unit_price_simple').val(data.Product.cost);
			$('#unit_price_simple').val(data.lastunitcost);
			//$('#unit_price_simple').val(data.Product.cost);
			$('#mrp_simple').val(data.Product.mrp);
			$('#wholesale_simple').val(data.Product.wholesale_price);
			$('#unit_id_simple').val(data.Product.unit_id);
            //$('#quantity_mode_simple').val(data.Product.unit_id);
			$('#product_unit_level_simple').val(data.Product.product_unit_level);
			$('#purchase_unit_level_simple').val(data.Product.purchase_unit_level);
			$('#no_of_piece_per_unit_simple').val(data.Product.no_of_piece_per_unit);
			$('#no_of_box_per_carton_simple').val(data.Product.no_of_box_per_carton);
			var tax=data.Product.tax;
			 if($('#toggle_button_for_tax_visibility').is(":checked")){
			$('#tax_simple').val(0);
			}
			else
			{
				$('#tax_simple').val(tax);
			}

			$('#quantity_simple').val('');
			if(unit_change_flag==0)
	        {
	          $('#quantity_mode_simple').val(data.unit_id).change();
	        }
			$('.simple_single_calculator').keyup();
			$('#unit_price_simple').select();
		},
		error:function (XMLHttpRequest, textStatus, errorThrown) {
			alert(textStatus);
		}
	});  
}
$(document).on('change','#toggle_button_for_tax_visibility',function(){
	$.fn.main_calculator();
	
});

$(document).on('keydown','.simple_single_calculator',function(e){
	if (e.keyCode == 13)
	{
		$('.add_to_cart_simple').trigger('click');
		return false;
	}
});
$(document).on('keyup','.simple_single_calculator',function(e){
	var unit_price=$('#unit_price_simple').val();
	var tax=$('#tax_simple').val();
	var quantity=$('#quantity_simple').val();
	var discount=0;
	var net_value_amount=unit_price*quantity;
	var taxable_value_amount=net_value_amount-discount;
	var tax_amount=net_value_amount*tax/100;
	$('#tax_amount_simple').val(roundToTwo(tax_amount));
	var tax_amount=parseFloat(tax_amount);
	var total=parseFloat(tax_amount)+parseFloat(taxable_value_amount);
	$('#net_value_simple').val(net_value_amount.toFixed(2));
	//$('#taxable_value_simple').val(taxable_value_amount.toFixed(2));
	$('#total_simple').val(total.toFixed(2));
	// $.fn.simple_main_calculator();
});
// $.fn.simple_main_calculator=function(){
// 	var total=0;
// 	$('#product_table tbody tr').each(function(){
// 		single_total=$(this).closest('tr').find('td input.row_total').val();
// 		if(typeof single_total != 'undefined')
// 			total+=parseFloat(single_total);
// 	});
// 	total=total.toFixed(2);
// 	$('#total').val(total);
// 	var grand_total=parseFloat(total);
// 	$('#grand_total').val(grand_total.toFixed(2));
// }
$(document).on('click','.add_to_cart_simple',function(){
	event.preventDefault();
	$("#party_id").change();
	var party_id=$('#party_id').val();
	var invoice_no=$('#invoice_no').val();
	if(!party_id)
	{
		$('#party_id').select2('open');
		return false;
	}
	if(!invoice_no)
	{
		$('#invoice_no').focus();
		return false;
	}
	var product_quantity=$('#quantity_simple').val();
	if(!$.isNumeric(product_quantity) || product_quantity=='' || product_quantity<=0 )
	{
		$('#quantity_simple').focus();
		return false;
	}
	var ProductType_name='';
	var quantity_mode=$('#quantity_mode_simple').val();
	var quantity_mode_text=$('#quantity_mode_simple option:selected').text();
	var unit_id=$('#unit_id_simple').val();
	var product_unit_level=$('#product_unit_level_simple').val();
	var purchase_unit_level=$('#purchase_unit_level_simple').val();
	var no_of_piece_per_unit=$('#no_of_piece_per_unit_simple').val();
	var Brand_name=$('#Brand_id option:selected').text();
	var Product_name=$('#product_simple option:selected').text();
	var product_id=$('#product_simple').val();
	var Brand_id=$('#Brand_id').val();
	var unit_price=$('#unit_price_simple').val();
	var mrp=$('#mrp_simple').val();
	var wholesale_price=$('#wholesale_simple').val();
	// if(purchase_unit_level == 2){
	// 	unit_price=unit_price*no_of_piece_per_unit;
	// 	mrp=mrp*no_of_piece_per_unit;
	// 	wholesale_price=wholesale_price*no_of_piece_per_unit;
	// }
	var net_value_amount=$('#net_value_simple').val();
	//var discount_simple=$('#discount_simple').val();
	//var tax=$(this).closest('tr').find('td.Product span.tax').text();
	//var tax_amount = roundToTwo(parseFloat(net_value_amount,10)*(parseFloat(tax,10)/100));
	var tax=$('#tax_simple').val();
	var tax_amount = $('#tax_amount_simple').val();
	var tax_total=parseFloat(tax_amount);
	//var total=roundToTwo(parseFloat(net_value_amount)+parseFloat(tax_total)-parseFloat(discount_simple));
	var total=$('#total_simple').val();
	var Product_type_field="<input type='text' class='form-control' readonly value='"+ProductType_name+"'>";
	var Brand_name_field="<input type='text' class='form-control' readonly value='"+Brand_name+"'><input type='hidden' value='"+Brand_id+"' name='data[Purchase][Brand_id][]'>";
	var Product_name_field="<input type='text' class='form-control' readonly value='"+Product_name+"'><input type='hidden' value='"+product_id+"' name='data[Purchase][product_id][]'>";

	//cost entry
	var unit_cost_field="<input type='text' class='form-control number  single_calculator unit_cost' value='"+unit_price+"' name='data[Purchase][unit_cost][]'>";	
	//cost entry ends
	var unit_price_field="<input type='text' class='form-control number single_calculator unit_price'  value='"+unit_price+"' name='data[Purchase][unit_price][]'>";
	var MRP_field="<input type='text' class='form-control number single_calculator mrp' name='data[Purchase][mrp][]' value='"+mrp+"' >";
	var quantity_mode_field="<input type='text'  readonly class='form-control quantity_mode'  value='"+quantity_mode_text+"'><input type='hidden'  readonly class='form-control '  value='"+product_unit_level+"'>\
	<input type='hidden'  readonly class='form-control unit_id_purchase_tbl'  value='"+quantity_mode+"' name='data[Purchase][unit_id][]'>";
	var wholesale_price_field="<input type='text' class='form-control number single_calculator wholesale_price' name='data[Purchase][wholesale_price][]'  value='"+wholesale_price+"' >";
	var purchase_qty_field="<input type='text' class='form-control number single_calculator quantity total_quantity'  value='"+product_quantity+"' name='data[Purchase][quantity][]'>";
	// var net_value_field="<input type='text' class='form-control number single_calculator net_value'  value='"+net_value_amount+"' >";
	// var discount_field="<input type='text' class='form-control number single_calculator discount'  value='"+discount_simple+"' name='data[Purchase][discount][]'>";
	var taxable_value_field="<input readonly type='text' class='form-control number single_calculator taxable_value'  value='"+net_value_amount+"' name='data[Purchase][net_value][]'>";
	var tax_field="<input type='text'readonly  class='form-control number single_calculator tax'  value='"+tax+"' name='data[Purchase][tax][]'>";
	var tax_amount_field="<input type='text'  readonly class='form-control number single_calculator tax_amount'  value='"+tax_amount+"' name='data[Purchase][tax_amount][]'>";
	var total_field="<input type='text' readonly class='form-control number single_calculator row_total'  value='"+total+"' name='data[Purchase][row_total][]'>";
	flag="true";
	$('#product_table tbody tr').each(function(){
		var table_product=($(this).find('td:nth-child(3)').find('input').val());
		var table_unit=($(this).find('td input.unit_id_purchase_tbl').val());
		if(table_product == Product_name)
		{	
			if(table_unit == quantity_mode){
				$(this).find('td').find('input.quantity').select().focus();
				flag="false";
			}
		}
	});
	if(flag=="false")
	{
		alert("Product already added");
		$('#product_simple').select2('open');
		return false;
	}
	$('#product_table tbody').prepend('<tr class="blue-pddng">\
		<td hidden>'+Product_type_field+'</td>\
		<td>'+Brand_name_field+'</td>\
		<td>'+Product_name_field+'</td>\
		<td hidden>'+unit_cost_field+'</td>\
		<td >'+unit_price_field+'</td>\
		<td class="wholesale_type" hidden>'+wholesale_price_field+'</td>\
		<td class="retail_type" hidden>'+MRP_field+'</td>\
		<td class="retail_type">'+quantity_mode_field+'</td>\
		<td>'+purchase_qty_field+'</td>\
	    <td>'+taxable_value_field+'</td>\
		<td>'+tax_field+'</td>\
		<td>'+tax_amount_field+'</td>\
		<td>'+total_field+'</td>\
		<td><i class="fa fa-minus-circle fa-2x remove_tr" ></i></td>\
		</tr>');
	$.fn.Country_type();
	$.fn.product_configuration_type();
	// $('.single_calculator').keyup();
	$.fn.main_calculator();
	$(this).closest('tr').find('td input.quantity_simple').val('0');
	$.fn.button_disable();
	$('#product_simple').val('').change();
		$('#brand_id').val('').change();
	$('#product_simple').select2('open');
	$('#unit_price_simple').val('0');
	$('#wholesale_simple').val('0');
	$('#mrp_simple').val('0');
	$('#quantity_simple').val('0');
	$('#net_value_simple').val('0');
	$('#discount_simple').val('0');
	$('#tax_simple').val('0');
	$('#tax_amount_simple').val('0');
	$('#total_simple').val('0');
	$.fn.expense_distribution();
});

$(document).on('click','#PurchaseAdvanceBtn',function(){
	if($('#PurchaseAdvance').is(":visible")){
		$('#PurchaseAdvance').hide();
		$(this).text('Show Advanced Purchase Options')
	}else{
		$('#PurchaseAdvance').show();
		$(this).text('Hide Advanced Purchase Options')
	} 
});
$(document).on('click','#last_purchase',function(){
  var party_id=$('#party_id').val();
  var product=$('#product_simple').val();
  var data_array={
    party_id:party_id,product:product
  }
  $.fn.search_transaction_ajax(data_array);
  $('#lastpurchase').modal('show');
});
$.fn.search_transaction_ajax=function(data_array){
  $.post( "<?= $this->webroot ?>Purchase/get_last_purchase_ajax",data_array ,function( data ) {
    $('#last_purchase_details_table').DataTable().destroy();
   $('#last_purchase_details_table tbody').empty();
    $('#last_purchase_details_table tbody').html(data.row);
    $('#last_purchase_details_table').DataTable({
    	"bSort" : false
    });
  }, "json");
};
$(document).on('change','#select_discount_type',function(){
  var select_discount_type=$(this).val();
  $('#discount').keyup();
  if(select_discount_type==2)
  {
    $('#discount').attr('readonly',false);
    $('#discount_amount').attr('readonly',true);
  }
  else
  {
    $('#discount').val('0');
    $('#discount').attr('readonly',true);
    $('#discount_amount').attr('readonly',false);
  }
});
$(document).on('change keyup','#discount',function(){
  var discount=0;
  if($('#discount').val())
  {
  	discount=$('#discount').val();
  }
  var main_total=$('#total').val();
  var discount_amount=(main_total*discount)/100;
  $('#discount_amount').val(discount_amount);
  var total_tax=0;
  if($('#total_tax').val())
  {
  	total_tax=$('#total_tax').val();
  }
  var discounted_total=parseFloat(main_total)-parseFloat(discount_amount);
  var total_tax_amount=discounted_total*total_tax/100;
  
  if($('#total_tax').val())
  {
  	$('#total_tax_amount').val(total_tax_amount);
  }
  //var grand_total=parseFloat(total_tax_amount)+parseFloat(discounted_total);
  var grand_total=parseFloat(discounted_total);
  $('#grand_total').val(grand_total);
});
$('.unit_modal_id').change(function(){
  var unit_id=$(this).val();
  $('#stock_quantity').val(0);
  var data={unit_id,unit_id};
  var url_address= "<?= $this->webroot; ?>Product/GetProductUnitLevel";
  $.post( url_address,data, function( response ) { 
    $('#ProductAddUnitLevel').val(response.Unit.level);
    $('#ProductEditUnitLevel').val(response.Unit.level);
    if(response.Unit.level==2)
    {
      $('.cartoon_count_cls').hide();
      $('.box_count_cls').show();
      $('.piece_count_cls').show();
      $('.no_of_piece_per_unit_feild').show();
      $('.price_of_unit_per_unit_feild').show();
      $('.no_of_box_per_carton_feild').hide();
      $('.no_of_piece_per_unit_feild').show();
      $('.no_of_piece_per_unit').attr('readonly',false);
      $('#stock_quantity').attr('readonly',true);
      $('.price_of_piece_per_unit_feild input').each(function(){
       $(this).attr('readonly',true);
     });
    }
    else if(response.Unit.level==1)
    {
      $('.cartoon_count_cls').hide();
      $('.box_count_cls').hide();
      $('.piece_count_cls').hide();
      $('.price_of_unit_per_unit_feild').hide();
      $('.no_of_piece_per_unit_feild').hide();
      $('.no_of_box_per_carton_feild').hide();
      $('.no_of_piece_per_unit').val('1');
      $('.no_of_box_per_carton').attr('readonly',true);
      $('.no_of_piece_per_unit').attr('readonly',true);
      $('#stock_quantity').attr('readonly',false);
      $('.price_of_piece_per_unit_feild input').each(function(){
       $(this).attr('readonly',false);
     });
    }
    $('#ProductAddUnitMrp').parent().find('label').text('Retail Price/'+response.Unit.name);
    $('#ProductAddUnitWholesalePrice').parent().find('label').text('Wholesale Price/'+response.Unit.name);
    $('#ProductAddUnitCost').parent().find('label').text('Landing Cost/'+response.Unit.name);
  },'json');
});
product_barcode='';
$.fn.auto_generated_barcode=function(){
  $.post( "<?= $this->webroot ?>Product/get_auto_generated_barcode",function( data ) {
    if(product_barcode=='' || product_barcode==null)
    {
      $('.auto_gen_barcode').val(data.barcode);
    }
    else
    {
      $('.auto_gen_barcode').val(product_barcode);
    }
  }, "json");
}
$(document).on('change','#barcodeOn',function(){
 var status = $(this).prop('checked');
 if(status==true){
  $.fn.auto_generated_barcode();
  $('.barcode-div').show();
  }else{
    $('.barcode-div').hide();
  }
});
$(document).on('change','#custombarcodeOn',function(){
 var status = $(this).prop('checked');
 if(status==true){
    $('#product_barcode').hide();
    $('#custom_barcode').show();
  }else{
    $('#custom_barcode').hide();
    $('#product_barcode').show();
  }
});
$(document).on('click','#Product_modal_add',function(){
  $('#barcodeOn').attr('checked',false);
  $('#barcodeOn').change();
  $('#custombarcodeOn').attr('checked',false);
  $('#custombarcodeOn').change();
  
  $('#Product_Add_modal').modal('show');
});
// Product add Function
$(document).on('keyup','.piece_stock_calc',function(){
     var unit_level=$('#ProductAddUnitLevel').val();
     if(unit_level == 2 || unit_level == 3){
      var piece_per_box=$('#ProductAddNoOfPiecePerUnit').val();
      
      var box_stock=$('#ProductAddBoxCount').val();
      var piece_stock=$('#ProductAddPieceCount').val();
      if(unit_level == 2){
        $('#stock_quantity').val((parseFloat(box_stock)*parseFloat(piece_per_box))+parseFloat(piece_stock));
      }else{
        var cartoon_stock=$('#ProductAddCartoonCount').val();
        var box_per_cartoon=$('#ProductAddNoOfBoxPerCarton').val();
        var cartoon_stock=parseFloat(box_per_cartoon)*parseFloat(cartoon_stock)*parseFloat(piece_per_box);
        var box_stock=parseFloat(box_stock)*parseFloat(piece_per_box);
         $('#stock_quantity').val(parseFloat(cartoon_stock)+parseFloat(box_stock)+parseFloat(piece_stock));
      }
      
     }
});
$('#barcodeOn').attr('checked',false);
$('#barcodeOn').change();
$('#custombarcodeOn').attr('checked',false);
$('#custombarcodeOn').change();
$(document).on('keyup','.piece_price_calc',function(){
  var unit_level=$('#ProductAddUnitLevel').val();
  if(unit_level == 2 || unit_level == 3){
   var piece_per_box=$('#ProductAddNoOfPiecePerUnit').val();
   var retail_per_unit=$('#ProductAddUnitMrp').val();
   var wholsale_per_unit=$('#ProductAddUnitWholesalePrice').val();
   var cost_per_unit=$('#ProductAddUnitCost').val();
   if(unit_level == 2)
   {
     $('#ProductAddMrp').val(retail_per_unit/piece_per_box);
     $('#ProductAddWholesalePrice').val(wholsale_per_unit/piece_per_box);
     $('#ProductAddCost').val(cost_per_unit/piece_per_box);
   }else{
    var box_per_cartoon=$('#ProductAddNoOfBoxPerCarton').val();
    $('#ProductAddMrp').val((retail_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductAddWholesalePrice').val((wholsale_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductAddCost').val((cost_per_unit/box_per_cartoon)/piece_per_box);
  }
}
});
$('#Product_Add_Btn').click(function(){
  var brand_id=$('#Brand_id_modal').val();
  var product_type_id=$('#product_type_id_modal').val();
  var name=$('#ProductAddName').val();
  if($.trim(name)=='')
  {
    $('#ProductAddName').focus();
    return false;
  }
  var product_id=$('#ProductAddCode').val();
  if($.trim(product_id)=='')
  {
    $('#ProductAddCode').focus();
    return false;
  }
  if(product_type_id=='' || product_type_id=='empty')
  {
    $('#product_type_id_modal').select2('open');
    return false;
  }
  if(brand_id=='empty' || brand_id=='')
  {
    $('#Brand_id_modal').select2('open');
    return false;
  }
  var data=$('#Product_Add_Form').serialize();
  var url_address= "<?= $this->webroot; ?>Product/Product_add_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.message);
      return false;
    }
    else
    {
      $('#ProductAddCustomBarcode').attr('value',response.custom_barcode);
      $('#ProductEditCustomBarcode').attr('value',response.custom_barcode);

      $('#Product_Add_Form')[0].reset();
      $('#Product_Add_modal').modal('toggle');
      $('#product_simple').append(response.Product);
      $('#product_simple').val($('#product_simple option:last-child').val()).trigger('change');
      $('#ProductAddUnitId').change();
      $('#Brand_id_modal').change();
      $('#product_type_id_modal').change();
      $('#unit_price_simple').focus();

    }
  }, "json");
});