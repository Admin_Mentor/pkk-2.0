<?php 
function get_status_name($status)
{
	if($status==0)
	{
		$name='Cancelled';
	}
	if($status==1)
	{
		$name='Order Placed';
	}
	if($status==2)
	{
		$name='Order Delivered';	
	}
	if($status==3)
	{
		$name='Altration';	
	}
	return $name;
}
function get_status($status)
{
	if($status==0)
	{
		$name='<i title="Cancelled" style="color:red;" class="fa fa-close fa-3x" aria-hidden="true"></i>';
	}
	if($status==1)
	{
		$name='<i title="Order Placed" style="color:red;" class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>';
	}
	if($status==2)
	{
		$name='<i title="Order Delivered" style="color:green;" class="fa fa-truck fa-2x" aria-hidden="true"></i>';
	}
	if($status==3)
	{
		$name='<i title="Altration" style="color:red;" class="fa fa-cart-plus fa-2x" aria-hidden="true"></i>';
	}
	return $name;
}
?>
<section class="content-header">
	<h1>Purchase Order List
		<a href="<?php echo $this->webroot ?>Purchase/Purchase"><button class='btn btn-success pull-right'>New Purchase</button></a>
	</h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-header">
	<!-- <div class="row"> -->
		<div class="col-md-3 col-lg-2 col-sm-12 col-xs-12">
			<div class="form-group">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control invoice-search pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$firstdate,'label'=>'From Date')); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-lg-2 col-sm-12 col-xs-12">
			<div class="form-group">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control invoice-search pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$todate,'label'=>'To Date')); ?>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="box-body">
			<table class="boder table table-condensed table datatable table-bordered" id="purchase_table" data-order='[[ 0, "desc" ]]' data-page-length='10'>
					<thead>
						<tr class="blue-bg">
							<th width="10%">Date</th>
							<th width="30%">Party</th>
							<th width="10%">Order ID</th>
							<th>Invoice No</th>
							<th class="text-right">Total Amt</th>
							<th width="10%">Status</th>
							<!-- <th style="display: none;">Action</th> -->
						</tr>
					</thead>
					<tbody>
						<?php foreach($Purchase as $key=>$value ) : ?>
							<tr class="blue-pddng">
								<td>
									<span><?= date('d-m-Y', strtotime($value['Purchase']['date_of_purchase'])); ?></span>
									<span class='purchase_id' style='display:none;'><?= $value['Purchase']['id']; ?></span>
								</td>
								<td><?= $value['AccountHead']['name']; ?></td>
								<td><?= $value['Purchase']['order_no']; ?></td>
								<td><?= $value['Purchase']['invoice_no']; ?></td>
								<td class="text-right"><?= number_format($value['Purchase']['grand_total'],2,'.',''); ?></td>
								<td><?= get_status($value['Purchase']['status']) ?>
									&nbsp;&nbsp;&nbsp;<?php if($value['Purchase']['status']==2){?>
									<span style="width: 20px"><a target="_blank" href="<?= $this->webroot; ?>Print/fpdf_purchase/<?= $value['Purchase']['id']  ?>"><i class="fa fa-print fa-2x" aria-hidden="true"></i></a><a></a></span>
									<?php }
									else
									{
										?>
										<span style="width: 20px"><a target="_blank" href="<?= $this->webroot; ?>Print/fpdf_purchase/<?= $value['Purchase']['id']  ?>"><i class="fa fa-print fa-2x" aria-hidden="true"></i></a><a></a></span>
										<?php

									}?> &nbsp;&nbsp;&nbsp;
								</td>
<!-- <td style="display: none;">
<span style=""><a target="_blank" href="<?= $this->webroot; ?>Purchase/fpdf/<?= $value['Purchase']['id']  ?>"><i class="fa fa-print" aria-hidden="true"></i></a><a></a></span><a> &nbsp;&nbsp;&nbsp;
</td> -->
</tr>
<?php endforeach; ?>
</tbody>
</table>
</div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
	$.fn.table_search=function(){
		var from_date=$('#from_date').val();
		var to_date=$('#to_date').val();
		var data={
			to_date:to_date,
			from_date : from_date,
		};
		var url_address= '<?php echo $this->webroot; ?>'+'Purchase/purchase_order_search_ajax';
		$.ajax({
			type: "post",
			url:url_address,
			data: data,
			dataType:'json',
			success: function(response) {
				$('#purchase_table').DataTable().destroy();
				$('#purchase_table tbody').html(response.row);
				$('#purchase_table').DataTable();
				$.fn.show_alert('Success');
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}
	$.fn.show_alert = function(flash)
	{
		$.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
	}

	$(document).on('click','table tbody tr',function(){
		var purchase_id=$(this).closest('tr').find('.purchase_id').html();
		var url = "<?= $this->webroot; ?>Purchase/Purchase/"+purchase_id;
		$(location).attr("href", url);
	});
	$(document).on('change','.invoice-search',function(){
		$.fn.table_search();
	});
</script>