<section class="content-header">
  <h1>
  Purchase Return
  <a href="<?php echo $this->webroot ?>Purchase/PurchaseReturnIndex"><button class='btn btn-success pull-right'>Purchase Return List</button></a>
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <?php $status=$this->request->data['PurchaseReturn']['status']; ?>
        <div class="row-wrapper">
          <div class="row">
            <?= $this->Form->create('PurchaseReturn', array('url' => array('controller' => 'Purchase', 'action' => 'PurchaseReturn')));?>
            <div class="col-md-12">
              <div class="row">
                <div class="form-horizontal" style="margin-top: 15px;">
                  <div class="box-body">
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                      <div class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                          <?= $this->Form->input('account_head_id',array('type'=>'select','id'=>'account_head_id','class'=>'form-control select2','style'=>'width: 100%','empty'=>'select','options'=>$Party_list,'label'=>'Party','required'=>'required')); ?>
                           <?= $this->Form->input('state_id',array('class'=>'form-control','type'=>'hidden','id'=>'state_id','label'=>false,)); ?>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 col-md-offset-4">
                      <div class="col-md-12">
                        <div class="col-md-6">
                          <div class="form-group">
                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                              <?= $this->Form->input('invoice_no',array('class'=>'form-control','type'=>'text','required','id'=>'invoice_no',)); ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row-wrapper">
                <div class="col-md-12">
                  <div class="box-body table-responsive no-padding">
                    <table class="table boder table-condensed table text-center table-bordered" id="product_table">
                      <thead>
                        <tr class="blue-bg">
                          <th width='11%' class="padding_left">Invoice No</th>
                          <th width='20%'>Product</th>
                          <th>Unit</th>
                          <th>Invoice Price</th>
                          <th>Act Qty</th>
                          <th>Qty</th>
                          <th>Amount</th>
                          <th>Net Amount</th>
                          <th>Tax Rate</th>
                          <th>Tax Amount</th>
                          <th width='10%'>Total Amount</th>
                          <?php if($status!=2) : ?>
                            <th></th>
                          <?php endif; ?>
                        </tr>
                        <?php if($status!=2) : ?>
                          <tr class="blue-pddng">
                            <td><?= $this->Form->input('invoice_no_list',['type'=>'select','id'=>'invoice_no_list','class'=>'form-control select2','options'=>$invoice_no_list,'label'=>false]); ?></td>
                            <td><?= $this->Form->input('product',['type'=>'select','id'=>'product','class'=>'form-control select2','empty'=>[''=>'Select'],'options'=>$Product_list,'label'=>false]); ?></td>
                              <td>
                              <!-- <?= $this->Form->input('unit_id_text',['type'=>'text','id'=>'quantity_mode_text','class'=>'form-control ','label'=>false,'readonly']); ?>-->
                              <?= $this->Form->input('unit_id',['type'=>'hidden','id'=>'quantity_mode','class'=>'form-control ','label'=>false]); ?> 
                              <?=$this->Form->input('unit_id_text',['type'=>'select','id'=>'quantity_mode_text','style'=>'width:100%','class'=>'form-control select2 single_calculator','options'=>$Unit,'label'=>false]); ?></td> 
                            <td><input id='actual_invoice_price' class="form-control single_calculator" type="text" readonly></td>
                            <td><input id='actual_quantity' class="form-control single_calculator" type="text" readonly></td>
                            <td><input id='quantity' class="form-control single_calculator" type="text"></td>
                            <td><input id='unit_price' class="form-control single_calculator" type="text"></td>
                            <td><input id='net_value' class="form-control single_calculator" type="text" readonly></td>
                            <td ><input id='tax' class="form-control single_calculator" type="text" readonly></td>
                            <td ><input id='tax_amount' class="form-control single_calculator" type="text" readonly></td> 
                            <td><input id='row_total' class="form-control single_calculator" type="text" readonly></td>
                            <td><i class="fa fa-plus-circle fa-2x ad-mar blue-col" id='add_to_cart'></i></td>
                          </tr>
                        <?php endif; ?>
                      </thead>
                      <tbody>
                        <?php if(isset($PurchaseReturnItem)) :  foreach ($PurchaseReturnItem as $key => $value): ?>
                          <tr class="blue-pddng">
                            <td colspan="1">
                              <?php 
                              $invoice_no=$value['PurchaseReturnItem']['invoice_no'];
                              if($value['PurchaseReturnItem']['invoice_no']==0)
                              {
                                $invoice_no="General Invoice";
                              }?>
                              <input value='<?= $invoice_no; ?>' class='form-control' readonly type='text'>
                              <input value='<?= $value['PurchaseReturnItem']['id']; ?>' name='data[PurchaseReturnItem][PurchaseReturnItem_id][]' type='hidden' class='table_id'>
                            </td>
                            <td colspan="1"><input value='<?= $value['Product']['name']; ?>' class='form-control' readonly type='text'></td>
                            <td colspan="1"><input value='<?= $value['Unit']['name']; ?>' class='form-control' readonly type='text'></td>
                            <td><input readonly value='<?= $value['PurchaseReturnItem']['invoice_price']; ?>' class='form-control'  name='data[PurchaseReturnItem][invoice_price][]' type='text'></td>
                            <td colspan="2"><input readonly value='<?= $value['PurchaseReturnItem']['quantity']; ?>' class='form-control'  name='data[PurchaseReturnItem][quantity][]' type='text'></td>
                            <td><input readonly value='<?= $value['PurchaseReturnItem']['unit_price']; ?>' class='form-control'  name='data[PurchaseReturnItem][unit_price][]' type='text'></td>
                            <td><input readonly value='<?= $value['PurchaseReturnItem']['net_value']; ?>' class='form-control taxable_value'  name='data[PurchaseReturnItem][net_value][]' type='text'></td>
                            <td ><input readonly value='<?= $value['PurchaseReturnItem']['tax']; ?>' class='form-control'  name='data[PurchaseReturnItem][tax][]' type='text'></td>
                            <td><input readonly value='<?= $value['PurchaseReturnItem']['tax_amount']; ?>' class='form-control tax_amount'  name='data[PurchaseReturnItem][tax_amount][]' type='text'></td>
                            <td><input readonly value='<?= $value['PurchaseReturnItem']['total']; ?>' class='form-control row_total'  name='data[PurchaseReturnItem][row_total][]' type='text'></td>
                            <?php if($status!=2) : ?><td><i class="fa fa-minus-circle fa-2x remove_old_tr" ></i></td><?php endif; ?>
                          </tr>
                        <?php endforeach; endif; ?>
                      </tbody>
                      <tfoot>
                        <tr class="blue-pddng">
                          <td colspan='9'></td>
                          <td colspan='1'><label>Total</label></td>
                          <td colspan='1'><?= $this->Form->input('grand_total',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'grand_total','label'=>false,)); ?></td>
                          <?php if($status!=2) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr>
                         <tr class="blue-pddng">
                        <td colspan="9"></td>
                         <td colspan='1'><label>SGST</label></td>
                        <td colspan='1'><?= $this->Form->input('sgst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'sgst_amt_total','readonly','label'=>false,)); ?>
                        </td>
                        <?php if($status==1 || $status==3) : ?>
                        <td></td>
                        <?php endif; ?>
                        </tr>
                         <tr class="blue-pddng">
                        <td colspan="9"></td>
                        <td colspan='1'><label>CGST</label>
                        <td colspan='1'><?= $this->Form->input('cgst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'cgst_amt_total','readonly','label'=>false,)); ?>
                        </td>
                        <?php if($status==1 || $status==3) : ?>
                        <td></td>
                        <?php endif; ?>
                        </tr>
                          <tr class="blue-pddng">
                        <td colspan="9"></td>
                        <td colspan='1'><label>IGST</label>
                        <td colspan='1'><?= $this->Form->input('igst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'igst_amt_total','readonly','label'=>false,)); ?>
                        </td>
                        <?php if($status==1 || $status==3) : ?>
                        <td></td>
                        <?php endif; ?>
                        </tr>
                        <tr class="blue-pddng">
                          <td colspan='9'></td>
                          <td colspan='1'><label>Discount</label></td>
                          <td colspan='1'><?= $this->Form->input('discount',array('class'=>'form-control number','type'=>'text','id'=>'discount','label'=>false,)); ?></td>
                          <?php if($status!=2) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr>
                       <!--  <tr class="blue-pddng" hidden="">
                          <td colspan='9'></td>
                          <td colspan='1'><label class="col-md-5 col-lg-5 col-sm-5 col-xs-12 control-label" style="white-space: nowrap;">Tax</label></td>
                         <td colspan='1'><?= $this->Form->input('total_tax',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'total_tax','label'=>false,)); ?></td>
                          <?php if($status!=2) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr> -->
                        <!--  <tr class="blue-pddng">
                          <td colspan='9'></td>
                          <td colspan='1'><label class="col-md-5 col-lg-5 col-sm-5 col-xs-12 control-label" style="white-space: nowrap;">Tax Amount</label></td>
                         <td colspan='1'><?= $this->Form->input('total_tax_amount',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'total_tax_amount','label'=>false,)); ?></td>
                          <?php if($status!=2) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr> -->
                        <tr class="blue-pddng">
                          <td colspan='9'></td>
                          <td colspan='1'><label >Grand Total</label></td>
                          <td colspan='1'><?= $this->Form->input('net_amount',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'net_amount','label'=>false,)); ?></td>
                          <?php if($status!=2) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr>
                        <tr class="blue-pddng">
                          <td colspan='6'></td>
                          <?php if(isset($PurchaseReturnItem)) : ?>
                            <?php $flag=$this->request->data['PurchaseReturn']['flag']; ?>
                            <?php if($status!=2 && $flag!=0) : ?>
                              <td><button type='submit'   name='data[PurchaseReturn][process]' value='delete' class="btn btn-danger pull-right" disabled>Delete</button></td>
                              <td><button type='submit'   name='data[PurchaseReturn][process]' value='cancel' class="btn btn-warning pull-left">Cancel</button></td>
                              <td><button type='submit'   name='data[PurchaseReturn][process]' value='update' class="btn btn-success">Update</button></td>
                              <td><button type='submit'   name='data[PurchaseReturn][process]' value='delivery' class="btn btn-primary pull-right">Purchase Return</button></td>
                            <?php else : ?>
                              <td colspan='3'></td>
                              <td hidden><button type='submit'   name='data[PurchaseReturn][process]' value='delete' class="btn btn-danger pull-right" disabled>Delete</button></td>
                            <?php endif; ?>
                          <?php else : ?>
                            <td colspan='2'></td>
                            <td hidden><button type='submit'     name='data[PurchaseReturn][process]' value='save' class="btn btn-success pull-right" disabled>Save</button></td>
                            <td><button type='submit'     name='data[PurchaseReturn][process]' value='delivery' class="btn btn-success pull-right">Purchase Return</button></td>
                          <?php endif; ?>
                          <?php if($status!=2) : ?>
                            <td></td>
                          <?php endif; ?>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
          <!--       <div class="col-md-4 col-lg-4 col-sm-4 col-md-offset-9">
            <div class="box-body table-responsive" style="margin-top: 0px;">
              <table class="table border_cover_table ">
                <tbody>
                       
                        </tbody>
                    </table>
                  </div>
                </div> -->
              </div>
            </div>
            <?= $this->Form->end(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script type="text/javascript">
    <?php require('purchase_return.js'); ?>
    <?php if(isset($PurchaseReturnItem)) : ?>
    <?php if($status!=1) : ?>
    $('#remove_old_tr').hide();
        $('#add_to_cart').hide();
  <?php endif; ?>
<?php endif; ?>
</script>