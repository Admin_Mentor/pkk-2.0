var status='<?= $this->request->data["PurchaseReturn"]["status"]; ?>';
if(status==1){
}
$(document).on('change','#account_head_id',function(){
  var account_head_id=$(this).val();
  $.post( "<?= $this->webroot ?>Purchase/get_invoice_list_ajax/"+account_head_id ,function( data ) {
    $('#invoice_no_list').html('');
    $.each(data.options,function(key,value){
      $('#invoice_no_list').append($("<option></option>").attr("value",key).text(value));
    });
    $('#invoice_no_list').trigger('change');
  }, "json");
});
$(document).on('change','#invoice_no_list',function(){
  var invoice_no=$(this).val();
  $.post( "<?= $this->webroot ?>Purchase/get_product_list_ajax/"+invoice_no ,function( data ) {
    // console.log(data);
    $('#product').html('');
    var sortedbyValueJSONArray=sortByValue(data);
    console.log(sortedbyValueJSONArray);
    $.each(data.products,function(key,value){
      $('#product').append($("<option></option>").attr("value",key).text(value));
    });
     $('#product').val($("#product").val()).trigger('change');
  }, "json");
});
function sortByValue(data){
  var sortedArray=[];
  for(var i in data){
    sortedArray.push([data[i],i])
  }
  return sortedArray.sort();
}
$(document).on('change','#product,#quantity_mode_text',function(){
  var product_id=$('#product').val();
  var unit_id=$('#quantity_mode_text').val();
  var purchase_id=$('#invoice_no_list option:selected').text();
  var data=
  {
    product_id:product_id,
    unit_id:unit_id,
    purchase_id:purchase_id,
  }
  $.post( "<?= $this->webroot ?>Purchase/get_product_purchased_item_detials_ajax/",data ,function( data ) {
  console.log(JSON.stringify(data));
    var no_of_pieces_per_unit=data.Product.no_of_piece_per_unit;
    var quantity =0;
    var no_of_pieces_per_unit=data.Product.no_of_piece_per_unit;
    var quantity =data.Product.quantity;
    // $('#quantity_mode').val(data.Product.quantity_mode);
    // $('#quantity_mode_text').val(data.Product.quantity_mode).change();
    $('#actual_invoice_price').val(data.Product.mrp);
    $('#unit_price').val(roundToTwo(data.Product.mrp));
    $('#actual_quantity').val(data.Product.quantity);
    $('#quantity').val(data.Product.quantity);
    $('#tax').val(roundToTwo(data.Product.tax));
    $('.single_calculator').keyup();
  }, "json");
});
$(document).on('change','#quantity_mode_text',function(){

});
$("#add_to_cart").click(function(event){
  event.preventDefault();
  var product_text=$('#product option:selected').text();
  product_text = product_text.replace(/"/g, '');
  var product=$('#product').val();
  var invoice_no_list_text=$('#invoice_no_list option:selected').text();
  var invoice_no_list=$('#invoice_no_list').val();
 var account_head_id=$('#account_head_id').val();
  var quantity=$('#quantity').val();
  if(!account_head_id)
  {
    $('#account_head_id').select2('open');
    return false;
  }
  if(!quantity)
  {
    $('#quantity').focus();
    return false;
  }
  var quantity=parseFloat(quantity,10);
  if(invoice_no_list)
  {
    actual_quantity=$('#actual_quantity').val();
  }
  else
  {
    actual_quantity=quantity+1;
  }
  if(quantity<=0 || parseFloat(actual_quantity,10)<quantity)
  {
    $('#quantity').focus();
    return false;
  }
  if(!product){
    $('#product').select2('open');
    return false;
  }
  var checkexist = 0;
  $("#product_table tbody tr").each(function () {
    var table_product_text = $(this).closest('tr').find('td input:eq(2)').val();
    if (product== table_product_text) {
      checkexist = 1;
    }
  });
  if(checkexist)
  {
    alert("This product already in return cart");
    return false;
  }
  var quantity_mode=$('#quantity_mode_text option:selected').val();
  var quantity_mode_text=$('#quantity_mode_text option:selected').text();
// if(quantity_mode==2)
// {
// var quantity_mode_text="Cases";

// }
// else
// {
//  var quantity_mode_text="Pieces";
 
// }
  var actual_invoice_price=$('#actual_invoice_price').val();
  var unit_price=$('#unit_price').val();
  var net_value=$('#net_value').val();
  var tax=$('#tax').val();
  $('#total_tax').val(tax);
  var tax_amount=$('#tax_amount').val();
  var row_total=$('#row_total').val();
  $('#product_table tbody').append('<tr>\
    <td colspan="1">\
    <input hidden name="data[PurchaseReturn][invoice_no_list][]" value="'+invoice_no_list+'">\
    <input class="form-control" value="'+invoice_no_list_text+'" readonly>\
    </td>\
    <td colspan="1">\
    <input hidden name="data[PurchaseReturn][product_id][]" value="'+product+'">\
    <input class="form-control" value="'+product_text+'" readonly>\
    </td>\
     <td colspan="1">\
         <input type="hidden" class="form-control" readonly  name="data[PurchaseReturn][quantity_mode][]" value="'+quantity_mode+'">\
    <input class="form-control" readonly  value="'+quantity_mode_text+'">\
    </td>\
    <td><input class="form-control" name="data[PurchaseReturn][invoice_price][]" value='+actual_invoice_price+' readonly ></td>\
    <td colspan="2"><input class="form-control" name="data[PurchaseReturn][quantity][]" value='+quantity+' readonly ></td>\
    <td><input class="form-control" name="data[PurchaseReturn][unit_price][]" value='+unit_price+' readonly></td>\
    <td><input class="form-control taxable_value" name="data[PurchaseReturn][net_value][]" value='+net_value+' readonly></td>\
    <td><input class="form-control" name="data[PurchaseReturn][tax][]" value='+tax+' readonly></td>\
    <td><input class="form-control tax_amount" name="data[PurchaseReturn][tax_amount][]" value='+tax_amount+' readonly></td>\
    <td><input class="form-control row_total" name="data[PurchaseReturn][row_total][]" value='+row_total+' id="total" readonly></td>\
    <td hidden><input class="form-control"  value='+tax+' id="tax" readonly></td>\
    <td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
    </tr>');
  var toggle_button_for_gst_visibility=$('#toggle_button_for_gst_visibility').prop("checked");
  if(toggle_button_for_gst_visibility==false)
  {
    $('.tax_field').css('display','none');
  }
  $.fn.Country_type();
  $('#product').val('').change();
  $.fn.main_calculater();
  $.fn.gst_state_check();
  $.fn.button_disable();
});
$(document).on('click','.remove_tr',function(){
  $(this).closest('tr').remove();
  $.fn.button_disable();
});
$(document).on('click','.remove_old_tr',function(){
  var row_index=$(this).closest('tr').index();
  var id=$(this).closest('tr').find('td input.table_id').val();
  var url_address= '<?php echo $this->webroot; ?>'+'Purchase/PurchaseReturnItem_delete/'+id;
  $.ajax({
    type: "POST",
    url:url_address,
    dataType:'json',
    success: function(data) {
      if(data.result!='Success')
      {
        alert(data.result);
        return false;
      }
      $('#product_table tbody tr:eq('+row_index+')').remove();
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  $(this).closest('tr').remove();
  $.fn.button_disable();
});
$.fn.button_disable=function(){
  $.fn.main_calculater();
  var length=$('#product_table tbody tr').length;
  if(length>0)
  {
    $('button[type="Submit"]').attr('disabled',false);  
  }
  else
  {
    $('button[type="Submit"]').attr('disabled',true);
  }
};
$.fn.main_calculater=function(){
  var each_total=0;  var each_taxable_value=0; var each_tax_amount=0;
  $('#product_table tbody tr').each(function(){
    var row_total=$(this).closest('tr').find('td input.row_total').val();
    var tax_amount=$(this).closest('tr').find('td input.tax_amount').val();
    var taxable_value=$(this).closest('tr').find('td input.taxable_value').val();
    each_total+=parseFloat(row_total);
   each_tax_amount+=parseFloat(tax_amount);
  each_taxable_value+=parseFloat(taxable_value);
  });
  var grand_total=each_total;
  $('#grand_total').val(roundToTwo(each_taxable_value));
  var discount=$('#discount').val();
  if(isNaN(discount) || discount=="")
  {
    var discount=$('#discount').val('0');
  }
  var tax=$('#total_tax').val();
    //$('#tax').val(tax);

  var net_amount=parseFloat(grand_total)-parseFloat(discount);
  net_amount=roundToTwo(net_amount);
  //var grand_total=net_amount+(net_amount*tax)/100;
  var total_tax_amount=(net_amount*tax)/100;
   $('#total_tax_amount').val(total_tax_amount);
var state_id=$('#state_id').val();
  if(state_id==19)
  {
    var tax_amount_split=parseFloat(each_tax_amount)/2;
    tax_amount_split=tax_amount_split.toFixed(3);
    $('#cgst_amt_total').val(tax_amount_split);
    $('#sgst_amt_total').val(tax_amount_split);
    $('#igst_amt_total').val(0.000);  
  }
  else
  {
  var tax_amount_split=parseFloat(each_tax_amount);
  tax_amount_split=tax_amount_split.toFixed(3);
  $('#cgst_amt_total').val(0.000);
  $('#sgst_amt_total').val(0.000);
  $('#igst_amt_total').val(tax_amount_split);
  }
  $('#net_amount').val(net_amount);
  //$('#tax').val(tax);

}
$(document).on('change keyup','#discount',function(){
  $.fn.main_calculater();
});
$.fn.button_disable();
$('.single_calculator').keyup(function(){
  var unit_price=parseFloat($('#unit_price').val());
  var tax=parseFloat($('#tax').val());
  var quantity=parseFloat($('#quantity').val());
  if(!unit_price)
  {
    unit_price=0;
  }
  if(!quantity)
  {
    quantity=0;
  }
  var net_value=unit_price*quantity;
  $('#net_value').val(roundToTwo(net_value));
  var tax_amount=net_value*tax/100;
  $('#tax_amount').val(roundToTwo(tax_amount));
  $(this).closest('tr').find('td total_taxinput.tax_amount').val(tax_amount);
  row_total=parseFloat(net_value)+parseFloat(tax_amount);
  $('#row_total').val(roundToTwo(row_total));
  //$('#total_tax_amount').val(roundToTwo(tax_amount));
});
$('.main-sidebar').ready(function(){
  $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
});
function roundToTwo(num) {
  return +(Math.round(num + "e+3")  + "e-3");
}
$.fn.gst_state_check=function() 
{
  var state=$('#state_id').val();
  var toggle_button_for_gst_visibility=$('#toggle_button_for_gst_visibility').prop("checked");
  if(toggle_button_for_gst_visibility==false)
  {
    $('.tax_field').css('display','none');
    $('.interstate').css('display','none');
    $('.instate').css('display','none');
  }
  else
  {
    if(state!=19)
    {
      $('.interstate').show();
      $('.instate').hide();
    }
    else
    {
      $('.instate').show();
      $('.interstate').hide();
    }
  }
};
$('#toggle_button_for_gst_visibility').change(function(){
  $.fn.gst_state_check();
});
$('#account_head_id').change(function(){
  var id=$(this).val();
  var url_address= '<?php echo $this->webroot; ?>'+'Purchase/get_party_address/'+id;
  $.ajax({
    type: "get",  
    url:url_address,
    dataType:'json',
    success: function(response) {
      $('#address').val(response.address);
      $('#state_id').val(response.state_id);
      $.fn.main_calculater();

      $.fn.gst_state_check();
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
});