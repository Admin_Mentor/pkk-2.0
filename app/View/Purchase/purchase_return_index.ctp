<?php 
function get_status_name($status)
{
	if($status==0)
	{
		$name='<i title="Cancelled" style="color:red;" class="fa fa-close fa-3x" aria-hidden="true"></i>';
	}
	if($status==1)
	{
		$name='<i title="Order Placed" style="color:red;" class="fa fa-shopping-cart fa-2x" aria-hidden="true"></i>';
	}
	if($status==2)
	{
		$name='<i title="Order Delivered" style="color:green;" class="fa fa-truck fa-2x" aria-hidden="true"></i>';
	}
	return $name;
}
?>
<section class="content-header">
	<h1>Purchase Return List
		<a href="<?php echo $this->webroot ?>Purchase/PurchaseReturn"><button class='btn btn-success pull-right'>New Purchase Return</button></a>
	</h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-header">
	<!-- <div class="row"> -->
		<div class="col-md-3 col-lg-2 col-sm-12 col-xs-12">
			<div class="form-group">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control invoice-search pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$firstdate,'label'=>'From Date')); ?>
				</div>
			</div>
		</div>
		<div class="col-md-3 col-lg-2 col-sm-12 col-xs-12">
			<div class="form-group">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control invoice-search pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$todate,'label'=>'To Date')); ?>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="box-body">
				<table class="boder table table-condensed table datatable table-bordered" id="purchase_table" data-order='[[ 1, "asc" ]]' data-page-length='25'>
					<thead>
						<tr class="blue-bg">
							<th>Date</th>
							<th width="30%">Party</th>
							<th>Invoice No</th>
							<th class="text-right">Total Amt</th>
							<th width="10%">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($PurchaseReturn as $key=>$value ) : ?>
							<tr class="blue-pddng">
								<td>
									<span><?= date('d-m-Y',strtotime($value['PurchaseReturn']['date'])); ?></span>
									<span class='PurchaseReturn_id' style='display:none;'><?= $value['PurchaseReturn']['id']; ?></span>
								</td>
								<td><?= $value['AccountHead']['name']; ?></td>
								<td><?= $value['PurchaseReturn']['invoice_no']; ?></td>
								<td class="text-right"><?= number_format($value['PurchaseReturn']['grand_total'],3,'.',''); ?></td>
								<td><?= get_status_name($value['PurchaseReturn']['status']) ?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</section>
<script type="text/javascript">
	$.fn.table_search=function(){
		var from_date=$('#from_date').val();
		var to_date=$('#to_date').val();
		var data={
			to_date:to_date,
			from_date : from_date,
		};
		var url_address= '<?php echo $this->webroot; ?>'+'Purchase/purchase_return_search_ajax';
		$.ajax({
			type: "post",
			url:url_address,
			data: data,
			dataType:'json',
			success: function(response) {
				$('#purchase_table').DataTable().destroy();
				$('#purchase_table tbody').html(response.row);
				$('#purchase_table').DataTable();
				$.fn.show_alert('Success');
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}
	$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }

	$(document).on('click','table tbody tr',function(){
		var PurchaseReturn_id=$(this).closest('tr').find('.PurchaseReturn_id').html();
		var url = "<?= $this->webroot; ?>Purchase/PurchaseReturn/"+PurchaseReturn_id;
		$(location).attr("href", url);
	});
	$(document).on('change','.invoice-search',function(){
		$.fn.table_search();
	});
</script>