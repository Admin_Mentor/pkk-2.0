<div id="Account_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Vendor's Details</h4>
			</div>
			<div class="modal-body">
				<?php echo $this->Form->create('Vendor', ['class'=>'form-horizontal','id'=>'Vendor_Form']); ?>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-4 control-label">Name</label>
					<div class="col-sm-6">
						<?= $this->Form->input('name',array('class'=>'form-control name','type'=>'text','required','id'=>'name_modal','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-4 control-label">Description</label>
					<div class="col-sm-6">
						<?= $this->Form->input('description',array('class'=>'form-control','type'=>'textarea','step'=>'any','rows'=>2,'id'=>'description','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-4 control-label">Opening Balance</label>
					<div class="col-sm-6">
						<?= $this->Form->input('opening_balance',array('class'=>'form-control opening_balance','type'=>'number','step'=>'any','required','id'=>'opening_balance_modal','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-4 control-label">Place</label>
					<div class="col-sm-6">
						<?= $this->Form->input('place',array('class'=>'form-control','type'=>'text','id'=>'place','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-4 control-label">Vendor Code</label>
					<div class="col-sm-6">
						<?= $this->Form->input('code',array('class'=>'form-control','type'=>'text','id'=>'code','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group"  style="display:none;">
					<label class="col-sm-4 control-label">State</label>
					<div class="col-sm-5">
						<?= $this->Form->input('state_id',array('class'=>'form-control select2','type'=>'select','style'=>'width:100%','options'=>$State_list,'id'=>'state_id','label'=>false,)); ?>
					</div>
					<div class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#state_add_modal"></i></div>
				</div>
				<div class="form-group"  style="display:none;">
					<label class="col-sm-4 control-label">State Code</label>
					<div class="col-sm-6">
						<?= $this->Form->input('state_code',array('class'=>'form-control','type'=>'text','readonly','id'=>'state_code','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-4 control-label">Email</label>
					<div class="col-sm-6">
						<?= $this->Form->input('email',array('class'=>'form-control','type'=>'text','id'=>'email','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-4 control-label">Mobile No</label>
					<div class="col-sm-6">
						<?= $this->Form->input('mobile',array('class'=>'form-control','type'=>'text','id'=>'mobile','label'=>false,)); ?>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">GSTin No</label>
					<div class="col-sm-6">
						<?= $this->Form->input('gstin',array('class'=>'form-control','type'=>'text','id'=>'gstin','label'=>false,)); ?>
					</div>
				</div>
				<?= $this->Form->end(); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn_radious" id='add_Vendor' data-dismiss="modal">Save</button>
			</div>
		</div>
	</div>
</div>
<?php require('state_modal.php') ?>