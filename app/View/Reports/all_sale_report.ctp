<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>

<section class="content-header">
  <h1>Route Wise Sales Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row">
         <div class="col-md-3" hidden="hidden">
          <?= $this->Form->input('executive_id',array('id'=>'executive_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'ALL')); ?>
        </div>
        <div class="col-md-2">
          <?= $this->Form->input('route_id',array('id'=>'route_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'ALL')); ?>
        </div>
        <div class="col-md-2">
          <?= $this->Form->input('customer_id',array('id'=>'customer_id','style'=>'width:100%','class'=>'form-control select_two_class','empty'=>'ALL')); ?>
        </div>
        <div class="col-md-2">
          <div class="col-md-12">
            <?= $this->Form->input('from_date',array('type'=>'text', 'id'=>'from_date', 'class'=>'form-control search_field date_picker datepicker', 'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask', )); ?>
          </div>
        </div>
        <div class="col-md-2">
          <div class="col-md-12">
            <?= $this->Form->input('to_date',array('type'=>'text', 'id'=>'to_date', 'class'=>'form-control  search_field date_picker datepicker', 'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask', )); ?>
          </div>
        </div>
        <div class="col-md-1"><br>
          <button class='btn' type='button' id='fetch_button'>Fetch</button>
        </div>
      </div>
    </div>
    <div class="box-body">
      <div class="col-md-4 col-xs-12">
        <h3 class="muted "></h3>
      </div>
      <table class="table table-condensed datatable table boder no-footer" id='table_collection_list'>
        <thead>
          <tr class="blue-bg">
            <th>Executive</th>
            <th>Route</th>
            <th>Customer</th>
            <th>Net Amount</th>
            <th>Discount Amount</th>
             <th>Taxable Value</th>
            <th>Tax Amount</th>
<!--       <th>Round Off</th>
 -->       
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
        <tfoot>
        </tfoot>
      </table>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  $('.customer_get_class').change(function(){
    var executive_id=$('#executive_id').val();
    var circle_id=$('#customercircle_id').val();
    var route_id=$('#route_id').val();
    var data={
      executive_id:executive_id,
      circle_id:circle_id,
      route_id:route_id
    };
    var url_address= "<?= $this->webroot; ?>Reports/GetCustomerForSaleCollectionReportAll";
    $.post( url_address,data, function( response ) {
      $('#customer_id').empty();
      $('#customer_id').append($("<option></option>").attr("value",'').text('ALL'));
      $.each(response,function(key,value){
        $('#customer_id').append($("<option></option>").attr("value",key).text(value));
      });
    }, "json");
  });
  $('#executive_id').change();
  $('#fetch_button').click(function(){
   var data={
    executive_id:$('#executive_id').val(),
    customer_id :$('#customer_id').val(),
    from_date   :$('#from_date').val(),
    to_date     :$('#to_date').val(),
    route_id    :$('#route_id').val(),
  };
  var url_address= "<?= $this->webroot; ?>Reports/SaleCollectionReportAjaxAll";
  $.post( url_address,data, function( response ) {
      // $('#table_collection_list').DataTable().destroy();
      // $('#table_collection_list tbody').empty();
      // var net_value=0;
      // var dicount_amount=0;
      // var other_value=0;
      // var tax_amount=0;
      // var total=0;
      // $.each(response,function(key,value){
      //   var tr='<tr>';
      //   tr+='<td>'+value.executive+'</td>';
      //   tr+='<td>'+value.circle+'</td>';
      //   tr+='<td>'+value.name+'</td>';
      //   net_value=parseFloat(net_value)+parseFloat(value.net_value);
      //   tr+='<td>'+value.net_value+'</td>';


      //   tax_amount=parseFloat(tax_amount)+parseFloat(value.tax_amount);
      //   tr+='<td>'+value.tax_amount+'</td>';
      //   other_value=parseFloat(other_value)+parseFloat(value.other_value);
      //   tr+='<td>'+value.other_value+'</td>';
      //   dicount_amount=parseFloat(dicount_amount)+parseFloat(value.dicount_amount);
      //   tr+='<td>'+value.dicount_amount+'</td>';
      //   total=parseFloat(total)+parseFloat(value.total);
      //   tr+='<td>'+value.total+'</td>';
      //   tr+='</tr>';
      //   $('#table_collection_list tbody').append(tr);
      // });
      // var tr='<tr>';
      // tr+='<td></td>';
      // tr+='<td></td>';
      // tr+='<td></td>';
      // tr+='<td>'+parseFloat(net_value).toFixed(2)+'</td>';

      // tr+='<td>'+parseFloat(tax_amount).toFixed(2)+'</td>';
      //  tr+='<td>'+parseFloat(other_value).toFixed(2)+'</td>';
      //  tr+='<td>'+parseFloat(dicount_amount).toFixed(2)+'</td>';

      // tr+='<td>'+parseFloat(total).toFixed(2)+'</td>';
      // tr+='</tr>';
      // $('#table_collection_list tfoot').html(tr);
      $('#table_collection_list').DataTable().destroy();
      $('#table_collection_list tbody').empty();
      $('#table_collection_list tfoot').empty();
      var taxable_amount     =0;
        var net_total_amount     =0;
      var tax_total_amount     =0;
      var other_total_amount   =0;
      var discount_total_amount=0;
      var grand_total_amount   =0;
      $.each(response,function(key,value){
        var tr='<tr>';
        tr+='<td>'+value.executive+'</td>';
        tr+='<td>'+value.route+'</td>';
        tr+='<td>'+value.name+'</td>';
        tr+='<td>'+value.net_value+'</td>';
      tr+='<td>'+value.dicount_amount+'</td>';
         tr+='<td>'+value.taxable_amount+'</td>';
        tr+='<td>'+value.tax_amount+'</td>';
        // tr+='<td>'+value.other_value+'</td>';
        tr+='<td>'+value.grandtotal+'</td>';
        tr+='</tr>';
        taxable_amount+=value.taxable_amount;
         net_total_amount+=value.net_value;
        tax_total_amount+=value.tax_amount;
        other_total_amount+=value.other_value;
        discount_total_amount+=value.dicount_amount;
        grand_total_amount+=value.grandtotal;
        $('#table_collection_list tbody').append(tr);
      });
      var tr_foot='<tr>';
      tr_foot+='<td colspan="2"></td>';
      tr_foot+='<td colspan><h4><b>Total</h4></b></td>';
      tr_foot+='<td><h4><b>'+Math.round(net_total_amount,2)+'</h4></b></td>';
      tr_foot+='<td><h4><b>'+Math.round(discount_total_amount)+'</h4></b></td>';
      tr_foot+='<td><h4><b>'+Math.round(taxable_amount,2)+'</h4></b></td>';
      tr_foot+='<td><h4><b>'+Math.round(tax_total_amount,2)+'</h4></b></td>';
      // tr_foot+='<td><h4><b>'+Math.round(other_total_amount,2)+'</h4></b></td>';
      tr_foot+='<td><h4><b>'+Math.round(grand_total_amount,2)+'</h4></b></td>';
      tr_foot+='</tr>';
      $('#table_collection_list tfoot').append(tr_foot);
      $('#table_collection_list').DataTable({
        dom: 'Bfrtip',
        buttons: [
        {extend: 'colvis', },
        {extend: 'print', footer: true, exportOptions: { columns: ':visible' } },
        {extend: 'csv', footer: true, exportOptions: { columns: ':visible' } },
        {extend: 'excel', footer: true, exportOptions: { columns: ':visible' } },
        {extend: 'pageLength', },
        ],
      // "columnDefs": [{"targets": [ 0 ], "visible": false,"searchable": false}]
    });
    }, "json");
});
</script>
