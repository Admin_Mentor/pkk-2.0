<section class="content">
    <div class="row">
      <a href="<?= $this->webroot.'Sale/SaleIndex'; ?>">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php  echo $Pending_Saleorders; ?></h3>
              <p>Pending Sale Order(s)</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>        
          </div>
        </div>
      </a>
      <a href="<?= $this->webroot.'Purchase/PurchaseIndex'; ?>">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $Pending_Purchaseorders; ?></h3>
              <p>Pending Purchase Order(s)</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
      </a>
</div>
<div class="row">
  <section class="col-lg-6 connectedSortable">
    <div class="box  box-solid bg-teal-gradient">
      <div class="box-header">
        <i class="fa fa-th"></i>
        <h3 class="box-title">Sales Graph</h3>
        <div class="box-tools pull-right">
          <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body border-radius-none">
        <div class="chart" id="Sales"></div>
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['jan'];?>" id="jan" />
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['feb'];?>" id="feb" />
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['mar'];?>" id="mar" />
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['apr'];?>" id="apr" />
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['may'];?>" id="may" />
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['jun'];?>" id="jun" />
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['jul'];?>" id="jul" />
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['aug'];?>" id="aug" />
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['sep'];?>" id="sep" />
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['oct'];?>" id="oct" />
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['nov'];?>" id="nov" />
        <input type="text" style="display:none" value="<?php echo $Sales_month[0]['dec'];?>" id="dec" />
      </div>
    </div>
  </section>
  <section class="col-lg-6 connectedSortable ui-sortable">
    <div class="box  box-solid bg-teal-gradient">
      <div class="box-header">
        <i class="fa fa-th"></i>
        <h3 class="box-title">Purchase Graph</h3>
        <div class="box-tools pull-right">
          <button class="btn bg-teal btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <button class="btn bg-teal btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body border-radius-none">
        <div class="chart" id="Purchase"></div>
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['jan'];?>" id="P_jan" />
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['feb'];?>" id="P_feb" />
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['mar'];?>" id="P_mar" />
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['apr'];?>" id="P_apr" />
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['may'];?>" id="P_may" />
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['jun'];?>" id="P_jun" />
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['jul'];?>" id="P_jul" />
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['aug'];?>" id="P_aug" />
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['sep'];?>" id="P_sep" />
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['oct'];?>" id="P_oct" />
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['nov'];?>" id="P_nov" />
        <input type="text" style="display:none" value="<?php echo $Purchase_month[0]['dec'];?>" id="P_dec" />
      </div>
    </div>
  </section>
  <section class="col-lg-6 connectedSortable">
</section>
</div>
</section>
</div>
<script type="text/javascript">
  $(function () {
    "use strict";
$(".connectedSortable").sortable({
  placeholder: "sort-highlight",
  connectWith: ".connectedSortable",
  handle: ".box-header, .nav-tabs",
  forcePlaceholderSize: true,
  zIndex: 999999
});
$(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");
$(".todo-list").sortable({
  placeholder: "sort-highlight",
  handle: ".handle",
  forcePlaceholderSize: true,
  zIndex: 999999
});
$(".textarea").wysihtml5();
$('.daterange').daterangepicker({
  ranges: {
    'Today': [moment(), moment()],
    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
    'This Month': [moment().startOf('month'), moment().endOf('month')],
    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
  },
  startDate: moment().subtract(29, 'days'),
  endDate: moment()
}, function (start, end) {
  window.alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
});
$(".knob").knob();
var visitorsData = {
"US": 398, //USA
"SA": 400, //Saudi Arabia
"CA": 1000, //Canada
"DE": 500, //Germany
"FR": 760, //France
"CN": 300, //China
"AU": 700, //Australia
"BR": 600, //Brazil
"IN": 800, //India
"GB": 320, //Great Britain
"RU": 3000 //Russia
};
//World map by jvectormap
$('#world-map').vectorMap({
  map: 'world_mill_en',
  backgroundColor: "transparent",
  regionStyle: {
    initial: {
      fill: '#e4e4e4',
      "fill-opacity": 1,
      stroke: 'none',
      "stroke-width": 0,
      "stroke-opacity": 1
    }
  },
  series: {
    regions: [{
      values: visitorsData,
      scale: ["#92c1dc", "#ebf4f9"],
      normalizeFunction: 'polynomial'
    }]
  },
  onRegionLabelShow: function (e, el, code) {
    if (typeof visitorsData[code] != "undefined")
      el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
  }
});
//Sparkline charts
var myvalues = [1000, 1200, 920, 927, 931, 1027, 819, 930, 1021];
$('#sparkline-1').sparkline(myvalues, {
  type: 'line',
  lineColor: '#92c1dc',
  fillColor: "#ebf4f9",
  height: '50',
  width: '80'
});
myvalues = [515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921];
$('#sparkline-2').sparkline(myvalues, {
  type: 'line',
  lineColor: '#92c1dc',
  fillColor: "#ebf4f9",
  height: '50',
  width: '80'
});
myvalues = [15, 19, 20, 22, 33, 27, 31, 27, 19, 30, 21];
$('#sparkline-3').sparkline(myvalues, {
  type: 'line',
  lineColor: '#92c1dc',
  fillColor: "#ebf4f9",
  height: '50',
  width: '80'
});
$("#calendar").datepicker();
$('#chat-box').slimScroll({
  height: '250px'
});
var data=[<?php foreach ($Sales as $key => $value) : ?>
{ label: "<?php echo $value['name']; ?>",value : <?php echo $value['amount']; ?>},
<?php endforeach; ?>
];
var colors=[<?php foreach ($Sales as $key => $value) : ?>
"<?php echo $value['color']; ?>",
<?php endforeach; ?>
];
//Donut Chart
var donut = new Morris.Donut({
  element: 'sales-chart',
  resize: true,
  colors: colors,
  data: data,
  hideHover: 'auto',
  parseTime: false
});
$('.box ul.nav a').on('shown.bs.tab', function () {
  area.redraw();
  donut.redraw();
  line.redraw();
});
$(".todo-list").todolist({
  onCheck: function (ele) {
    window.console.log("The element has been checked");
    return ele;
  },
  onUncheck: function (ele) {
    window.console.log("The element has been unchecked");
    return ele;
  }
});
var opt = {
  avgActive: true,
  avgColorIndicator: true,
  lbDecimals: 2,
  horTitle: 'bi-title-id',
  milestones: false
};
});
</script>
<script type="text/javascript">
  <?php 
  $P_jan=$Sales_month[0]['jan'];
  $P_feb=$Sales_month[0]['feb'];
  $P_mar=$Sales_month[0]['mar'];
  $P_apr=$Sales_month[0]['apr'];
  $P_may=$Sales_month[0]['may'];
  $P_jun=$Sales_month[0]['jun'];
  $P_jul=$Sales_month[0]['jul'];
  $P_aug=$Sales_month[0]['aug'];
  $P_sep=$Sales_month[0]['sep'];
  $P_oct=$Sales_month[0]['oct'];
  $P_nov=$Sales_month[0]['nov'];
  $P_dec=$Sales_month[0]['dec'];
  $test = [$P_apr,$P_may,$P_jun,$P_jul,$P_aug,$P_sep,$P_oct,$P_nov,$P_dec,$P_jan,$P_feb,$P_mar];
  $sum_sale = 0;
  for ($i = 0; $i < count($test); $i++) {
    $sum_sale += $test[$i];
  }
  $perc_jan=(($P_jan/ $sum_sale) * 100);
  $perc_feb=(($P_feb/ $sum_sale) * 100);
  $perc_mar=(($P_mar/ $sum_sale) * 100);
  $perc_apr=(($P_apr/ $sum_sale) * 100);
  $perc_may=(($P_may/ $sum_sale) * 100);
  $perc_jun=(($P_jun/ $sum_sale) * 100);
  $perc_jul=(($P_jul/ $sum_sale) * 100);
  $perc_aug=(($P_aug/ $sum_sale) * 100);
  $perc_sep=(($P_sep/ $sum_sale) * 100);
  $perc_oct=(($P_oct/ $sum_sale) * 100);
  $perc_nov=(($P_nov/ $sum_sale) * 100);
  $perc_dec=(($P_dec/ $sum_sale) * 100);
  $perc_jan=number_format($perc_jan, 2, '.', '');
  $perc_feb=number_format($perc_feb, 2, '.', '');
  $perc_mar=number_format($perc_mar, 2, '.', '');
  $perc_apr=number_format($perc_apr, 2, '.', '');
  $perc_may=number_format($perc_may, 2, '.', '');
  $perc_jun=number_format($perc_jun, 2, '.', '');
  $perc_jul=number_format($perc_jul, 2, '.', '');
  $perc_aug=number_format($perc_aug, 2, '.', '');
  $perc_sep=number_format($perc_sep, 2, '.', '');
  $perc_oct=number_format($perc_oct, 2, '.', '');
  $perc_nov=number_format($perc_nov, 2, '.', '');
  $perc_dec=number_format($perc_dec, 2, '.', '');
  ?>
  var perc_jan= <?php echo $perc_jan; ?>;
  var perc_feb= <?php echo $perc_feb; ?>;
  var perc_mar= <?php echo $perc_mar; ?>;
  var perc_apr= <?php echo $perc_apr; ?>;
  var perc_may= <?php echo $perc_may; ?>;
  var perc_jun= <?php echo $perc_jun; ?>;
  var perc_jul= <?php echo $perc_jul; ?>;
  var perc_aug= <?php echo $perc_aug; ?>;
  var perc_sep= <?php echo $perc_sep; ?>;
  var perc_oct= <?php echo $perc_oct; ?>;
  var perc_nov= <?php echo $perc_nov; ?>;
  var perc_dec= <?php echo $perc_dec; ?>;
  zingchart.THEME="classic";
var initState = null; // Used later to store the chart state before changing the data
var store = { // Data store
  ie:[["v11.0",24.1],["v8.0",17.2],["v9.0",8.1],["v10.0",5.3],["v6.0",1.1],["v7.0",0.5]],
  chrome:[["v40.0",5.0],["v41.0",4.3],["v42.0",3.7],["v39.0",3.0],["v36.0",2.5],["v43.0",1.4],["v31.0",1.2],["v35.0",0.8],["v38.0",0.6],["v32.0",0.6],["v37.0",0.4],["v33.0",0.2],["v34.0",0.1],["v30.0",0.1]],
  firefox:[["v35",2.8],["v36",2.3],["v37",2.3],["v34",1.3],["v38",1.0],["v31",0.3],["v33",0.2],["v32",0.1]],
  safari:[["v8.0",2.6],["v7.1",0.8],["v5.1",0.4],["v5.0",0.3],["v6.1",0.3],["v7.0",0.3],["v6.2",0.2]],
  opera:[["v12.x",0.3],["v28",0.2],["v27",0.2],["v29",0.2]]
};
var bgColors = ["#7CB5EC","#434348","#90ED7D","#F7A35C","#8085E9","#F9B6C6"]; // ie, chrome, ff, safari, opera, unknown
var myConfig = {
  "globals": {
    "font-family": "Helvetica"
  },
  "type": "bar",
  "background-color": "white",
  "title": {
    "color": "#606060",
    "background-color": "white",
  },
  "subtitle": {
    "color": "#606060",
  },
  "scale-y": {
    "line-color": "none",
    "tick": {
      "line-color": "none"
    },
    "guide": {
      "line-style": "solid"
    },
    "item": {
      "color": "#606060"
    }
  },
  "scale-x": {
    "values": [
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
    "Jan",
    "Feb",
    "Mar",
    ],
    "line-color": "#C0D0E0",
    "line-width": 1,
    "tick": {
      "line-width": 1,
      "line-color": "#C0D0E0"
    },
    "guide": {
      "visible": false
    },
    "item": {
      "color": "#606060"
    }
  },
  "crosshair-x": {
    "marker": {
      "visible": false
    },
    "line-color": "none",
    "line-width": "0px",
    "scale-label": {
      "visible": false
    },
    "plot-label": {
      "text": "%data-browser: %v% of total",
      "multiple": true,
      "font-size": "10px",
      "color": "#606060",
      "background-color": "white",
      "border-width": 1,
      "alpha": 0.8,
      "callout": true,
      "callout-position": "bottom",
      "shadow": 0,
      "placement": "node-top",
      "border-radius": 4,
      "padding": 6,
      "rules": [
      {
        "rule": "%i==0",
        "border-color": "#7CB5EC"
      },
      {
        "rule": "%i==1",
        "border-color": "#434348"
      },
      {
        "rule": "%i==2",
        "border-color": "#90ED7D"
      },
      {
        "rule": "%i==3",
        "border-color": "#F7A35C"
      },
      {
        "rule": "%i==4",
        "border-color": "#8085E9"
      },
      {
        "rule": "%i==5",
        "border-color": "#F9B6C6"
      }
      ]
    }
  },
  "plot": {
    "data-browser": [
    ],
    "cursor": "hand",
    "value-box": {
      "text": "%v%",
      "text-decoration": "underline",
      "color": "#606060"
    },
    "tooltip": {
      "visible": false
    },
    "animation": {
      "effect": "7"
    },
    "rules": [
    {
      "rule": "%i==0",
      "background-color": "#7CB5EC"
    },
    {
      "rule": "%i==1",
      "background-color": "#434348"
    },
    {
      "rule": "%i==2",
      "background-color": "#90ED7D"
    },
    {
      "rule": "%i==3",
      "background-color": "#F7A35C"
    },
    {
      "rule": "%i==4",
      "background-color": "#8085E9"
    },
    {
      "rule": "%i==5",
      "background-color": "#F9B6C6"
    }
    ]
  },
  "series": [
  {
    "values": [
    perc_apr,
    perc_may,
    perc_jun,   
    perc_jul,
    perc_aug,
    perc_sep,
    perc_oct,
    perc_nov,
    perc_dec,
    perc_jan,
    perc_feb,
    perc_mar
    ]
  }
  ]
};
zingchart.render({
  id : 'Sales', 
  data : myConfig, 
  height: 500, 
  width: 725
});
zingchart.bind('Sales','node_click',updateChart);
zingchart.shape_click = function(p){ // Listen for back button click
  zingchart.unbind(p.id,'animation_end');
  if (p.shapeid == "back_btn"){
zingchart.exec(p.id,'setdata',{ // Set the data back to the state it was in when the node was clicked
  data:initState
});
zingchart.bind(p.id,'node_click',updateChart);
}
}
</script>
<script type="text/javascript">
  <?php 
  $P_jan=$Purchase_month[0]['jan'];
  $P_feb=$Purchase_month[0]['feb'];
  $P_mar=$Purchase_month[0]['mar'];
  $P_apr=$Purchase_month[0]['apr'];
  $P_may=$Purchase_month[0]['may'];
  $P_jun=$Purchase_month[0]['jun'];
  $P_jul=$Purchase_month[0]['jul'];
  $P_aug=$Purchase_month[0]['aug'];
  $P_sep=$Purchase_month[0]['sep'];
  $P_oct=$Purchase_month[0]['oct'];
  $P_nov=$Purchase_month[0]['nov'];
  $P_dec=$Purchase_month[0]['dec'];
  $test = [$P_apr,$P_may,$P_jun,$P_jul,$P_aug,$P_sep,$P_oct,$P_nov,$P_dec,$P_jan,$P_feb,$P_mar];
  $sum_sale = 0;
  for ($i = 0; $i < count($test); $i++) {
    $sum_sale += $test[$i];
  }
  $perc_jan=(($P_jan/ $sum_sale) * 100);
  $perc_feb=(($P_feb/ $sum_sale) * 100);
  $perc_mar=(($P_mar/ $sum_sale) * 100);
  $perc_apr=(($P_apr/ $sum_sale) * 100);
  $perc_may=(($P_may/ $sum_sale) * 100);
  $perc_jun=(($P_jun/ $sum_sale) * 100);
  $perc_jul=(($P_jul/ $sum_sale) * 100);
  $perc_aug=(($P_aug/ $sum_sale) * 100);
  $perc_sep=(($P_sep/ $sum_sale) * 100);
  $perc_oct=(($P_oct/ $sum_sale) * 100);
  $perc_nov=(($P_nov/ $sum_sale) * 100);
  $perc_dec=(($P_dec/ $sum_sale) * 100);
  $perc_jan=number_format($perc_jan, 2, '.', '');
  $perc_feb=number_format($perc_feb, 2, '.', '');
  $perc_mar=number_format($perc_mar, 2, '.', '');
  $perc_apr=number_format($perc_apr, 2, '.', '');
  $perc_may=number_format($perc_may, 2, '.', '');
  $perc_jun=number_format($perc_jun, 2, '.', '');
  $perc_jul=number_format($perc_jul, 2, '.', '');
  $perc_aug=number_format($perc_aug, 2, '.', '');
  $perc_sep=number_format($perc_sep, 2, '.', '');
  $perc_oct=number_format($perc_oct, 2, '.', '');
  $perc_nov=number_format($perc_nov, 2, '.', '');
  $perc_dec=number_format($perc_dec, 2, '.', '');
  ?>
  var perc_jan= <?php echo $perc_jan; ?>;
  var perc_feb= <?php echo $perc_feb; ?>;
  var perc_mar= <?php echo $perc_mar; ?>;
  var perc_apr= <?php echo $perc_apr; ?>;
  var perc_may= <?php echo $perc_may; ?>;
  var perc_jun= <?php echo $perc_jun; ?>;
  var perc_jul= <?php echo $perc_jul; ?>;
  var perc_aug= <?php echo $perc_aug; ?>;
  var perc_sep= <?php echo $perc_sep; ?>;
  var perc_oct= <?php echo $perc_oct; ?>;
  var perc_nov= <?php echo $perc_nov; ?>;
  var perc_dec= <?php echo $perc_dec; ?>;
  zingchart.THEME="classic";
var initState = null; // Used later to store the chart state before changing the data
var bgColors = ["#7CB5EC","#434348","#90ED7D","#F7A35C","#8085E9","#F9B6C6"]; // ie, chrome, ff, safari, opera, unknown
var myConfig = {
  "globals": {
    "font-family": "Helvetica"
  },
  "type": "bar",
  "background-color": "white",
  "title": {
    "color": "#606060",
    "background-color": "white",
//"text": "Browser market shares. January, 2015 to May, 2015"
},
"subtitle": {
  "color": "#606060",
// "text": "Click the columns to view versions. Source: netmarketshare.com."
},
"scale-y": {
  "line-color": "none",
  "tick": {
    "line-color": "none"
  },
  "guide": {
    "line-style": "solid"
  },
  "item": {
    "color": "#606060"
  }
},
"scale-x": {
  "values": [
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
  "Jan",
  "Feb",
  "Mar",
  ],
  "line-color": "#C0D0E0",
  "line-width": 1,
  "tick": {
    "line-width": 1,
    "line-color": "#C0D0E0"
  },
  "guide": {
    "visible": false
  },
  "item": {
    "color": "#606060"
  }
},
"crosshair-x": {
  "marker": {
    "visible": false
  },
  "line-color": "none",
  "line-width": "0px",
  "scale-label": {
    "visible": false
  },
  "plot-label": {
    "text": "%data-browser: %v% of total",
    "multiple": true,
    "font-size": "10px",
    "color": "#606060",
    "background-color": "white",
    "border-width": 1,
    "alpha": 0.8,
    "callout": true,
    "callout-position": "bottom",
    "shadow": 0,
    "placement": "node-top",
    "border-radius": 4,
    "padding": 6,
    "rules": [
    {
      "rule": "%i==0",
      "border-color": "#7CB5EC"
    },
    {
      "rule": "%i==1",
      "border-color": "#434348"
    },
    {
      "rule": "%i==2",
      "border-color": "#90ED7D"
    },
    {
      "rule": "%i==3",
      "border-color": "#F7A35C"
    },
    {
      "rule": "%i==4",
      "border-color": "#8085E9"
    },
    {
      "rule": "%i==5",
      "border-color": "#F9B6C6"
    }
    ]
  }
},
"plot": {
  "data-browser": [
  ],
  "cursor": "hand",
  "value-box": {
    "text": "%v%",
    "text-decoration": "underline",
    "color": "#606060"
  },
  "tooltip": {
    "visible": false
  },
  "animation": {
    "effect": "7"
  },
  "rules": [
  {
    "rule": "%i==0",
    "background-color": "#7CB5EC"
  },
  {
    "rule": "%i==1",
    "background-color": "#434348"
  },
  {
    "rule": "%i==2",
    "background-color": "#90ED7D"
  },
  {
    "rule": "%i==3",
    "background-color": "#F7A35C"
  },
  {
    "rule": "%i==4",
    "background-color": "#8085E9"
  },
  {
    "rule": "%i==5",
    "background-color": "#F9B6C6"
  }
  ]
},
"series": [
{
  "values": [
  perc_apr,
  perc_may,
  perc_jun,   
  perc_jul,
  perc_aug,
  perc_sep,
  perc_oct,
  perc_nov,
  perc_dec,
  perc_jan,
  perc_feb,
  perc_mar
  ]
}
]
};
zingchart.render({
  id : 'Purchase', 
  data : myConfig, 
  height: 500, 
  width: 725
});
zingchart.bind('Purchase','node_click',updateChart);
zingchart.shape_click = function(p){ // Listen for back button click
  zingchart.unbind(p.id,'animation_end');
  if (p.shapeid == "back_btn"){
zingchart.exec(p.id,'setdata',{ // Set the data back to the state it was in when the node was clicked
  data:initState
});
zingchart.bind(p.id,'node_click',updateChart);
}
}
</script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['Year', <?php  echo $commaList = implode(',', $TOTAL_USERS_); ?>],
//apr
<?php   $sum_apr = 0;
for ($i = 0; $i < count($apr_total); $i++) {
  $sum_apr += $apr_total[$i];
}
if($sum_apr!=0)
  {?>
    ['Apr',  <?php  echo $apr_total = implode(',', $apr_total); ?>],
    <?php } ?>   
//may
<?php   $sum_may = 0;
for ($i = 0; $i < count($may_total); $i++) {
  $sum_may += $may_total[$i];
}
if($sum_may!=0)
  {?>
    ['May',  <?php  echo $may_total = implode(',', $may_total); ?>],
    <?php } ?>   
//jun
<?php   $sum_jun = 0;
for ($i = 0; $i < count($jun_total); $i++) {
  $sum_jun += $jun_total[$i];
}
if($sum_jun!=0)
  {?>
    ['Jun',  <?php  echo $jun_total = implode(',', $jun_total); ?>],
    <?php } ?>
//jul
<?php   $sum_jul = 0;
for ($i = 0; $i < count($jul_total); $i++) {
  $sum_jul += $jul_total[$i];
}
if($sum_jul!=0)
  {?>
    ['Jul',  <?php  echo $jul_total = implode(',', $jul_total); ?>],
    <?php } ?>
//aug
<?php   $sum_aug = 0;
for ($i = 0; $i < count($aug_total); $i++) {
  $sum_aug += $aug_total[$i];
}
if($sum_aug!=0)
  {?>
    ['Aug',  <?php  echo $aug_total = implode(',', $aug_total); ?>],
    <?php } ?>
//sep
<?php   $sum_sep = 0;
for ($i = 0; $i < count($sep_total); $i++) {
  $sum_sep += $sep_total[$i];
}
if($sum_sep!=0)
  {?>
    ['Sep',  <?php  echo $sep_total = implode(',', $sep_total); ?>],
    <?php } ?>
//oct
<?php   $sum_oct = 0;
for ($i = 0; $i < count($oct_total); $i++) {
  $sum_oct += $oct_total[$i];
}
if($sum_oct!=0)
  {?>
    ['Oct',  <?php  echo $oct_total = implode(',', $oct_total); ?>],
    <?php } ?>
//nov
<?php   $sum_nov = 0;
for ($i = 0; $i < count($nov_total); $i++) {
  $sum_nov += $nov_total[$i];
}
if($sum_nov!=0)
  {?>
    ['Nov',  <?php  echo $nov_total = implode(',', $nov_total); ?>],
    <?php } ?>
//dec
<?php   $sum_dec = 0;
for ($i = 0; $i < count($dec_total); $i++) {
  $sum_dec += $dec_total[$i];
}
if($sum_dec!=0)
  {?>
    ['Dec',  <?php  echo $dec_total = implode(',', $dec_total); ?>],
    <?php } ?>
//jan
<?php   $sum_jan = 0;
for ($i = 0; $i < count($jan_total); $i++) {
  $sum_jan += $jan_total[$i];
}
if($sum_jan!=0)
  {?>
    ['Jan',  <?php  echo $jan_total = implode(',', $jan_total); ?>],
    <?php } ?>
//feb
<?php   $sum_feb = 0;
for ($i = 0; $i < count($feb_total); $i++) {
  $sum_feb += $feb_total[$i];
}
if($sum_feb!=0)
  {?>
    ['Feb',  <?php  echo $feb_total = implode(',', $feb_total); ?>],
    <?php } ?>
//mar
<?php   $sum_mar = 0;
for ($i = 0; $i < count($mar_total); $i++) {
  $sum_mar += $mar_total[$i];
}
if($sum_mar!=0)
  {?>
    ['Mar',  <?php  echo $mar_total1 = implode(',', $mar_total); ?>],
    <?php } ?>
    ]);
    var options = {
// title: 'Executive Sales Graph',
curveType: 'function',
legend: { position: 'bottom' }
};
var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));
chart.draw(data, options);
}
</script>
<script>
  $(document).ready(function(){
    $('.side_tog').hide();
// $('.side_tog active').show();
});
</script>
<script>
  $('.main-sidebar').ready(function(){
    $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
  });
</script>