<section class="content-header">
  <div class="box box-primary">
    <div class="box-header">
      <div class="row">
        <div class="form-group">
          <div class="col-md-6">
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
              <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
              <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><br>
              <button type='button' class='btn btn-primary' id='get_button'>GET</button>
            </div>
          </div>
          <div class="col-md-6"><h1>Balance Sheet</h1></div>
        </div>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-6">
            <div class="panel panel-default">
              <div class="panel-heading">
                <b>Particulars</b>
              </div>
              <div class="panel-body" id='Left-Particulars'>
                <div class="panel-group" id="Particulars_Left">
                  <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #ffffff">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#Particulars_Left" href="#collapse2" class="collapsed" aria-expanded="false">
                          <b>Liabilities</b><b id='type_id_2' class='pull-right type_amount-left'>0.00</b>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                        <div class="panel-group" id="Liabilities">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #ffffff">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#Particulars_Left" href="#collapse4" class="collapsed" aria-expanded="false">
                          <b>Capital</b><b id='type_id_4' class='pull-right type_amount-left'>0.00</b>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                        <div class="panel-group" id="Capital">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="panel panel-default">
              <div class="panel-heading">
                <b>Particulars</b>
              </div>
              <div class="panel-body" id='Right-Particulars'>
                <div class="panel-group" id="Particulars_Right">
                  <div class="panel panel-default">
                    <div class="panel-heading" style="background-color: #ffffff">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#Particulars_Right" href="#collapse1" class="collapsed" aria-expanded="false">
                          <b>Asset</b><b id='type_id_1' class='pull-right type_amount-right'>0.00</b>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                      <div class="panel-body">
                        <div class="panel-group" id="Asset">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box-footer">
      <div class="col-md-12">
        <div class="col-md-6" id='diffrence_in_opening-left' style="display: none">
          <div class="panel panel-default">
            <div class="panel-heading diffrence_opening">
              <h4 style="color: red"><b>Difference In Opening Balance : <span class='pull-right' id='diffrence-left'>0</span></b></h4>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-md-offset-6" id='diffrence_in_opening-right' style="display: none">
          <div class="panel panel-default">
            <div class="panel-heading diffrence_opening" >
              <h4 style="color: red"><b>Difference In Opening Balance : <span class='pull-right' id='diffrence-right'>0</span></b></h4>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: blue"><b>Total : <span class='pull-right' id='grand_total-left'>0</span><span style="display: none" class='pull-right' id='net_left'>0</span></b></h4>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: blue"><b>Total : <span class='pull-right' id='grand_total-right'>0</span><span class='pull-right' style="display: none" id='net_right'>0</span></b></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require('general_journal_transaction_ledger_modal.php'); ?>
<section class="content"></section>
<script type="text/javascript">
  $('#get_button').click(function(){
    //alert('k');
    $('#wait').show();
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    //  $(".sub_name").each(function () {
    //    // console.log($(this).text());
    //    // console.log($(this).attr('id'));
    //   type[$(this).attr("id")] = $(this).text();
    //   //alert(type);
    // });

    type={
      1:'Asset',
      2:'Capital',
      3:'Expense',
      4:'Income',
      5:'Liabilities',
    };
    var activeAjaxConnections=0;
    $.each(type,function(type_id,type_name){
      var data={
        from_date:from_date,
        to_date:to_date,
        type_id:type_id,
      };
      $.ajax({
        method: "POST",
        url: "<?= $this->webroot; ?>Reports/Get_Master_Group_Ajax",
        beforeSend: function(xhr) {
          activeAjaxConnections++;
        },
        data: data,
        dataType: "json",
      }).done(function(data) {
        activeAjaxConnections--;
        $("#"+type_name+"").empty();
        $.each(data.SubGroup,function(sub_group_id,sub_group_name){
          $("#"+type_name+"").append(
            '<div class="panel panel-default">\
            <div class="panel-heading" style="background-color: #ffffff">\
            <h4 class="panel-title">\
            <a data-toggle="collapse" data-parent="#'+type_name+'" href="#SubGroup_'+sub_group_id+'" aria-expanded="false" class="collapsed"><b>'+sub_group_name+'</b><b class="pull-right master_group_amount" data-parent=type_id_'+type_id+' id="master_group_id_'+sub_group_id+'">0.00</b></a>\
            </h4>\
            </div>\
            <div id="SubGroup_'+sub_group_id+'" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">\
            <div class="panel-body">\
            </div>\
            </div>\
            </div>'
            );  
          var data={
            from_date:from_date,
            to_date:to_date,
            sub_group_id:sub_group_id,
             type_id:type_id,
          };
              $.ajax({
                    method: "POST",
                    url: "<?= $this->webroot; ?>Reports/Get_AccountHead_Ajax_balance_sheet",
                    beforeSend: function(xhr) {
                      activeAjaxConnections++;
                    },
                    data: data,
                    dataType: "json",
                  }).done(function(data) {
                    activeAjaxConnections--;
                    if(activeAjaxConnections==0)
                    {
                      $('#wait').hide();
                    }
                    // console.log(activeAjaxConnections);
                    $('#SubGroup_'+sub_group_id+' > .panel-body').empty();

                    $.each(data.AccountHead,function(key,account){
                      console.log(account.net_right);
                      if(sub_group_id==account.profit_sub_group_id){
                            if(account.id==account.profit_loss_account_head_id){
                          $('#SubGroup_'+sub_group_id+' > .panel-body').append(
                          '<h4>\
                          <u><b class="name">'+account.name+'</b>&nbsp<b class="pull-right journal_amount" data-parent="master_group_id_'+sub_group_id+'">'+parseFloat(account.net_right).toFixed(2)+'</b></u>\
                          </h4>'
                          );
                        }else{
                           $('#SubGroup_'+sub_group_id+' > .panel-body').append(
                          '<h4>\
                          <u><b class="name">'+account.name+'</b>&nbsp<b class="pull-right journal_amount" data-parent="master_group_id_'+sub_group_id+'">'+parseFloat(account.net_left).toFixed(2)+'</b></u>\
                          </h4>'
                          );
                        }
                      }else{
                      // var net_right=account.net_right;
                      // var net_left=account.net_left;
                      // $('#net_left').text((account.net_left));
                      // $('#net_right').text((account.net_right));
                      // if(account.amount)
                      // {
                         // if(account.name!="STOCK")
              // {   
                        $('#SubGroup_'+sub_group_id+' > .panel-body').append(
                          '<h4>\
                          <u><b class="name">'+account.name+'</b>&nbsp<b class="pull-right journal_amount" data-parent="master_group_id_'+sub_group_id+'">'+parseFloat(account.amount).toFixed(2)+'</b></u>\
                          </h4>'
                          );
                      }
                    });
                  }).fail(function(){ activeAjaxConnections-- });
                //});
             // }).fail(function(){ activeAjaxConnections-- });
            //});
         // }).fail(function(){ activeAjaxConnections-- });
        });
}).fail(function() { activeAjaxConnections-- });
});
// $(document).ajaxComplete(function(){
 // $(document).ajaxComplete(function(){});
 $(document).ajaxStop(function(){
  $(".journal_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text(0);
  });
  $(".journal_amount").each(function(){
    var amount=$(this).closest('b').text();
   // console.log(amount);
    var parent=$(this).data('parent');
    $('#'+parent).text((parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
  });
  $(".sub_group_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text(0);
  });
  $(".sub_group_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text((parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
  });
  $(".group_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text(0);
  });
  $(".group_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text((parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
  });
  $(".master_group_amount").each(function(){
    var amount=$(this).closest('b').text();
    var parent=$(this).data('parent');
    $('#'+parent).text(0);
  });
  $(".master_group_amount").each(function(){
    var amount=$(this).closest('b').text();
    // console.log(amount);
    var parent=$(this).data('parent');
     parnt=$('#'+parent).text();
  if(parnt.includes('Cr'))
  {
    var amt_3 = parseFloat((parnt.toString()).replace(' Cr',''));
    parnt = pos_to_neg(amt_3);
  }
  var amt_2 = parseFloat(parnt)+parseFloat(amount);

   if(amt_2.toFixed(2)>0){
    $('#'+parent).text(Math.abs(amt_2).toFixed(2)+' Dr');
  }
        else if(amt_2.toFixed(2)<0) {
          var amt_3 = parseFloat((amt_2.toString()).substring(1));
          // amt_2 = parseFloat(amt_2);
          // var str = "Hello world!";
          // var res = str.substring(1);
           // var amt=parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2);
          $('#'+parent).text(amt_3.toFixed(2)+' Cr');
        }
        else {
          $('#'+parent).text(Math.abs(amt_2).toFixed(2));
        }
   // $('#'+parent).text((parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
  });
  $('#grand_total-left').text(0);
  $(".type_amount-left").each(function(){
    var amount=$(this).closest('b').text();
    $('#grand_total-left').text((parseFloat($('#grand_total-left').text())+parseFloat(amount)).toFixed(2));
  });
  $('#grand_total-right').text(0);
  $(".type_amount-right").each(function(){
    var amount=$(this).closest('b').text();
    $('#grand_total-right').text((parseFloat($('#grand_total-right').text())+parseFloat(amount)).toFixed(2));
  });
  // net_right=$('#net_right').text();
  // net_left=$('#net_left').text();
  // alert(net_right);
  // if(net_right!=0){
  // var grand_total_left=$('#grand_total-left').text()-parseFloat(net_right);
  // }else{
  //   var grand_total_left=$('#grand_total-left').text()+parseFloat(net_left);
  // }
  var grand_total_left=$('#grand_total-left').text();
  var grand_total_right=$('#grand_total-right').text();
  $('#diffrence-left').text(0);
  $('#diffrence-right').text(0);
  if(grand_total_left>grand_total_right)
  {
    var diffrence=grand_total_left-grand_total_right;
    $('#diffrence-right').text(diffrence.toFixed(2));
    if(diffrence)
    {
      $('#diffrence_in_opening-left').hide();
      $('#diffrence_in_opening-right').show();
    }
    $('#grand_total-right').text((parseFloat(grand_total_right)+parseFloat(diffrence)).toFixed(2));
  }

  else
  { 
    var diffrence=grand_total_right-grand_total_left;
    $('#diffrence-left').text(diffrence.toFixed(2));
    if(diffrence!=0)
    {
      $('#diffrence_in_opening-left').show();
      $('#diffrence_in_opening-right').hide();
      $('#grand_total-left').text((parseFloat(grand_total_left)+parseFloat(diffrence)).toFixed(2));
    }
    else
    {
      $('#diffrence_in_opening-right').show();
    }
  }
});
$('#modal_from_date').val(from_date);
$('#modal_to_date').val(to_date);
});
function pos_to_neg(num)
{
return -Math.abs(num);
}
</script>
<script type="text/javascript">
  <?php require('general_journal_transaction_ledger_ajax.js'); ?>
  <?php //require('general_journal_transaction_ajax.js'); ?>
</script>