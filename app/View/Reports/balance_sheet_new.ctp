<?php ini_set('max_execution_time', '300'); ?>
<section class="content-header">
  <h1>Balance Sheet</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <div class="row">
        <div class="form-group">
          <div class="col-md-8">
            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
              <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
              <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" hidden>
            <?= $this->Form->input('branch_id',array('type'=>'select','id'=>'branch_id','class'=>'form-control select_two_class','style'=>'width: 100%','empty' => 'All','options'=>$BranchList,'default' =>'Select','label'=>'Branch')); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><br>
              <button type='button' class='btn btn-success' id='get_button'>GET</button>
            </div>
          </div>
          <div class="col-md-4">
            <div class="text-right" style="margin-top: 0.2%;margin-right: 2%;"><br>
              <input type="button"  class="save" name="button" id="exportExcel" value="Export"></a>
              <input type="button"  class="save" name="button" id="exportpdf" value="Print"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-6">
                <table class=" table-bordered table-hover table-striped table-condensed" id='dataTable' width="98%" style="margin-left:10px">
                    <thead>
                    <tr  class="blue-bg">
                    <th width="40%"><b>Particulars</b></th>
                    <th class="text-right" style="display:none;"><b>Opening</b></th>
                    <th class="text-right" style="display:none;"><b>Debit</b></th>
                    <th class="text-right" style="display:none;"><b>Credit</b></th>
                    <th class="text-right"><b>Closing</b></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
             </div>
            <div class="col-lg-6">
                <table class=" table-bordered table-hover table-striped table-condensed" id='dataTable1' width="98%" style="margin-left:10px">
                    <thead>
                    <tr  class="blue-bg">
                    <th width="40%"><b>Particulars</b></th>
                    <th class="text-right" style="display:none;"><b>Opening</b></th>
                    <th class="text-right" style="display:none;"><b>Debit</b></th>
                    <th class="text-right" style="display:none;"><b>Credit</b></th>
                    <th class="text-right"><b>Closing</b></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
    </div>
    <div class="box-footer">
      <div class="col-md-12">
        <div class="col-md-6" id='diffrence_in_opening-left' style="display: none">
          <div class="panel panel-default">
            <div class="panel-heading diffrence_opening">
              <h4 style="color: red"><b>Difference : <span class='pull-right' id='diffrence-left'>0</span></b></h4>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-md-offset-6" id='diffrence_in_opening-right' style="display: none">
          <div class="panel panel-default">
            <div class="panel-heading diffrence_opening" >
              <h4 style="color: red"><b>Difference : <span class='pull-right' id='diffrence-right'>0</span></b></h4>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-12">
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: blue"><b>Total : <span class='pull-right' id='grand_total-left'>0</span><span style="display: none" class='pull-right' id='net_left'>0</span></b></h4>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: blue"><b>Total : <span class='pull-right' id='grand_total-right'>0</span><span class='pull-right' style="display: none" id='net_right'>0</span></b></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="Accounthead_modal" class="modal fade" role="dialog" >
    <div class="modal-dialog modal-lg" style="width: 75%">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span id='subgroup_name'></span></h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <table class="boder table table-condensed table" id="Accounthead_table" data-order='[[ 1, "asc" ]]' data-page-length='25'>
              <thead>
                <tr  class="blue-bg">
                <th width="40%"><b>Particulars</b></th>
                <th class=""><b>Opening</b></th>
                <th class=""><b>Debit</b></th>
                <th class=""><b>Credit</b></th>
                <th class=""><b>Closing</b></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
                <tr>
                  <th style="font-size:20px; color:red;text-align:right">Total:</th>
                  <th style="font-size:20px; color:red;"></th>
                  <th style="font-size:20px; color:red;"></th>
                  <th style="font-size:20px; color:red;"></th>
                  <th style="font-size:20px; color:red;"></th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require('general_journal_transaction_balance_modal.php'); ?>
<section class="content"></section>
<style>
div.dataTables_wrapper div.dataTables_processing {
    left: -10px;
    padding: 10px !important;
    color: blue !important;
    opacity: 1 !important;
    text-decoration: none;
}
.hide{display:none;}
.type_id{cursor: pointer;}
.sub_group_id{cursor: pointer;}
.account_name{cursor: pointer;}
.subgroup_name{padding: 15px;}
</style>
<script type="text/javascript">
$(document).ready(function() {
    var net_left=$('#net_left').text();
    var net_right=$('#net_right').text();
    if(net_left>net_right) {
        var balance=net_left-net_right;
        net_left=parseFloat(net_left)+parseFloat(balance);
        $('#diffrence-left').text(parseFloat(balance).toFixed(2));
    }
    else {
        var balance=net_right-net_left;
        net_right=parseFloat(net_right)+parseFloat(balance);
        $('#diffrence-right').text(parseFloat(balance).toFixed(2));
    }
    $('#grand_total-left').text(parseFloat(net_left).toFixed(2));
    $('#grand_total-right').text(parseFloat(net_right).toFixed(2));
}); 
$('#dataTable').DataTable( {
    "processing" : true,
    "serverSide" : true,
    "ordering"   : true,
    "searching"  : false,
    "paging"     : false,
    "bInfo"      : false,
    "fixedHeader": true,
    "ajax": {
    "url": "<?= $this->webroot ?>Reports/balance_sheet_left_ajax",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
       // d.branch_id= $('#branch_id').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "AccMainGroup.name",width:"50%" },
    { "data" : "AccMainGroup.opening_balance",className:"text-right hide" },
    { "data" : "AccMainGroup.debit",className:"text-right hide" },
    { "data" : "AccMainGroup.credit",className:"text-right hide" },
    { "data" : "AccMainGroup.amount",className:"text-right" },
    ],
    "columnDefs": [
    {"targets": [ 0 ],"className": 'type_id'},
    ],
    "createdRow": function( row, data, dataIndex){
        $(row).css('font-weight','bold');

        $(row).css('font-size','120%');
    },
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;
      var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0;};
      pageTotal_opening=api.column(1,{page:'current'}).data().reduce(function(a,b){return intVal(a) + intVal(b);
      },0);
      pageTotal_debit=api.column(2,{page:'current'}).data().reduce(function(a,b){return intVal(a) + intVal(b);
      },0);
      pageTotal_credit=api.column(3,{page:'current'}).data().reduce(function(a,b){return intVal(a) + intVal(b);
      },0);
      var pageTotal_balance=pageTotal_opening+pageTotal_debit-pageTotal_credit;
      $('#net_left').text(parseFloat(pageTotal_balance).toFixed(2));
      datatable_second(Math.abs(pageTotal_balance));
      
    },
});
function datatable_second(net_left){
$('#dataTable1').DataTable( {
    "processing" : true,
    "serverSide" : true,
    "ordering"   : true,
    "searching"  : false,
    "paging"     : false,
    "bInfo"      : false,
    "fixedHeader": true,
    "ajax": {
    "url": "<?= $this->webroot ?>Reports/balance_sheet_right_ajax",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
        //d.branch_id= $('#branch_id').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "AccMainGroup.name",width:"50%" },
    { "data" : "AccMainGroup.opening_balance",className:"text-right hide" },
    { "data" : "AccMainGroup.debit",className:"text-right hide" },
    { "data" : "AccMainGroup.credit",className:"text-right hide" },
    { "data" : "AccMainGroup.amount",className:"text-right" },
    ],
    "columnDefs": [
    {"targets": [ 0 ],"className": 'type_id'},
    ],
    "createdRow": function( row, data, dataIndex){
              $(row).css('font-weight','bold');

               $(row).css('font-size','120%');
      },
      "footerCallback": function ( row, data, start, end, display ) {
       var api = this.api(), data;
      var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0;};
      pageTotal_opening=api.column(1,{page:'current'}).data().reduce(function(a,b){return intVal(a) + intVal(b);
      },0);
      pageTotal_debit=api.column(2,{page:'current'}).data().reduce(function(a,b){return intVal(a) + intVal(b);
      },0);
      pageTotal_credit=api.column(3,{page:'current'}).data().reduce(function(a,b){return intVal(a) + intVal(b);
      },0);
      var pageTotal_balance=pageTotal_opening+pageTotal_debit-pageTotal_credit;
      $('#net_right').text(parseFloat(pageTotal_balance).toFixed(2));
      var net_right=pageTotal_balance;
      // console.log(pageTotal_opening);
      $('#diffrence_in_opening-right').hide();
      $('#diffrence_in_opening-left').hide();
      if(net_left>net_right) {
        var balance=parseFloat(net_left)-parseFloat(net_right);
        net_right=parseFloat(net_right)+parseFloat(balance);
        $('#diffrence_in_opening-right').show();
        $('#diffrence-right').text(parseFloat(balance).toFixed(2));
      }
      else if(net_left<net_right){
        var balance=parseFloat(net_right)-parseFloat(net_left);
        net_left=parseFloat(net_left)+parseFloat(balance);
        $('#diffrence_in_opening-left').show();
        $('#diffrence-left').text(parseFloat(balance).toFixed(2));
      }
      else{
        $('#diffrence_in_opening-right').hide();
        $('#diffrence_in_opening-left').hide();
      }
        $('#grand_total-left').text(parseFloat(net_left).toFixed(2));
        $('#grand_total-right').text(parseFloat(net_right).toFixed(2));
    },
});
}

var previous_index='';
$(document).on('click','td.type_id',function(){
  var type_id=$(this).closest('tr').find('span.type_id').text();
  var type_name=$(this).closest('tr').find('span.type_name').text();
  var from_date= $('#from_date').val();
  var to_date= $('#to_date').val();
 // var branch_id= $('#branch_id').val();
  var new_data={
     from_date:from_date,
     to_date:to_date,
     type_id:type_id,
   //  branch_id:branch_id,
  };
  var row_index=$(this).parents('tr').index();
   if ($('.all_data'+type_id)[0])
    {
    $('.all_data'+type_id).remove();
    } 
    else
    {
  if(parseFloat(previous_index) != parseFloat(row_index))
  {
    $.post( "<?= $this->webroot ?>Reports/balance_sheet_subgroupwise",new_data ,function( data ) {
        $.each(data.Subgroup,function(key,value){
            if(type_id==1){
                $('#dataTable1 tbody tr:eq('+row_index+')').after('<tr class="all_data'+type_id+'">\
                <td class="sub_group_id"><span hidden class="hidden_id">'+value.sub_group_id+'</span><span hidden class="hidden_type_id">'+type_id+'</span><span class="subgroup_name">'+value.name+'</span></td>\
                <td class="text-right" style="display:none;">'+value.opening_balance+'</td>\
                <td class="text-right" style="display:none;">'+value.debit+'</td>\
                <td class="text-right" style="display:none;">'+value.credit+'</td>\
                <td class="text-right">'+value.amount+'</td>\
                </tr>');
            } 
            else{
                if(value.sub_group_id==value.profit_sub_group_id){
                    $('#dataTable tbody tr:eq('+row_index+')').after('<tr class="all_data'+type_id+'">\
                    <td class="sub_group_id"><span hidden class="hidden_id">'+value.sub_group_id+'</span><span hidden class="hidden_type_id">'+type_id+'</span><span class="subgroup_name">'+value.name+'</span></td>\
                    <td class="text-right" style="display:none;">'+value.opening_balance+'</td>\
                    <td class="text-right" style="display:none;">'+value.net_right+'</td>\
                    <td class="text-right" style="display:none;">'+value.net_left+'</td>\
                    <td class="text-right">'+value.profit+'</td>\
                    </tr>');
                }else{
                    $('#dataTable tbody tr:eq('+row_index+')').after('<tr class="all_data'+type_id+'">\
                    <td class="sub_group_id"><span hidden class="hidden_id">'+value.sub_group_id+'</span><span hidden class="hidden_type_id">'+type_id+'</span><span class="subgroup_name">'+value.name+'</span></td>\
                    <td class="text-right" style="display:none;">'+value.opening_balance+'</td>\
                    <td class="text-right" style="display:none;">'+value.debit+'</td>\
                    <td class="text-right" style="display:none;">'+value.credit+'</td>\
                    <td class="text-right">'+value.amount+'</td>\
                    </tr>');
                }
            }
        });
    }, "json");
    previous_index=row_index;
  }
  else
  {
    previous_index='';
  }
 }
});
$(document).on('click','td.sub_group_id',function(){
  $('#Accounthead_table').DataTable().destroy();
  $('#Accounthead_table tbody').empty();
  $('#Accounthead_modal').modal('show');
  var subgroup_name=$(this).closest('tr').find('span.subgroup_name').text();
  var sub_group_id=$(this).closest('tr').find('span.hidden_id').text();
  var hidden_type_id=$(this).closest('tr').find('span.hidden_type_id').text();
  $('#subgroup_name').text(subgroup_name);
  var table=$('#Accounthead_table').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/balance_sheet_accounthead",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
      //  d.branch_id= $('#branch_id').val();
        d.type_id= hidden_type_id;
        d.sub_group_id= sub_group_id;
      },
      "dataSrc": "records",
    },
    dom: 'Bfrtip',
    lengthMenu: [
    [25, 50,100,500],
    ['25 rows', '50 rows','100 rows','500 rows' ]
    ],
    buttons: [],
   "columns": [
    { "data" : "AccountHead.name" },
    { "data" : "AccountHead.opening_balance",className:"text-right" },
    { "data" : "AccountHead.debit",className:"text-right" },
    { "data" : "AccountHead.credit",className:"text-right" },
    { "data" : "AccountHead.amount",className:"text-right" },
    ],
   "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;
      var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
        i : 0;
      };
      pageTotal = api.column( 1, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 1 ).footer() ).html(''+pageTotal+'');
      pageTotal = api.column( 2, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 2 ).footer() ).html(''+pageTotal+'');
      pageTotal = api.column( 3, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 3 ).footer() ).html(''+pageTotal+'');
      pageTotal = api.column( 4, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 4 ).footer() ).html(''+pageTotal+'');
    },
   "columnDefs": [
    {"targets": [ 0 ],"className": 'account_name' ,},
    ],
  });

});
$('#get_button').click(function(){
  $('#dataTable1').DataTable().destroy();
    table = $('#dataTable').dataTable();
    table.fnDraw();
});
</script>
<script type="text/javascript">
  $(document).ready(function(){

    $('#exportExcel').click(function()   { 
      var date_from= $('#from_date').val();
      var date_to=$('#to_date').val();
     // var branch_id=$('#branch_id').val();
      if(!date_from)
      {
        alert("Take From Date");
        return false;
      }
      if(!date_to)
      {
        alert("Take To Date");
        return false;
      }
      var website_url3='<?php echo $this->webroot; ?>Reports/print_balance_sheet_report/'+date_from+'/'+date_to;
      $(location).attr("href", website_url3);
    });
    $('#exportpdf').click(function()   { 
      var date_from= $('#from_date').val();
      var date_to=$('#to_date').val();
      //var branch_id=$('#branch_id').val();
      if(!date_from)
      {
        alert("Take From Date");
        return false;
      }
      if(!date_to)
      {
        alert("Take To Date");
        return false;
      }
      var website_url3='<?php echo $this->webroot; ?>Reports/BalanceSheet_Print/'+date_from+'/'+date_to;
      // $(location).attr("href", website_url3);
      window.open(website_url3, '_blank');
    });
  });
</script>
<script type="text/javascript">
  <?php require('general_journal_transaction_balance_ajax.js'); ?>
  <?php //require('general_journal_transaction_ajax.js'); ?>
</script>