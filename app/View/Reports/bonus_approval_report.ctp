
<style type="text/css">
.cls_label_all {
  padding-top: 5%;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Bonus Approval/Reject Report</h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-header">
            <div class="col-md-12">
        <div class="row-wrapper">
          <div class="row">
            <div class="col-md-12">
            <div class="col-md-3 col-lg-3 col-sm-3">
                <div class="box-body">
                  <div class="form-group">
                    <?php echo $this->Form->input('producttype_id',array('type'=>'select','empty' =>'ALL','class'=>'form-control select_two_class product_flitering search_class product_flitering','style'=>'width:100%','label'=>'Product Type','id'=>'product_type_id',)) ?>
                    <?= $this->Form->input('hidden_id',array('type'=>'hidden','class'=>'form-control','label'=>false,'id'=>'hidden_product_search_id','value'=>1)) ?>
                  </div>
                </div>
              </div>
             <div class="col-md-3 col-lg-3 col-sm-3">
                <div class="box-body">
                  <div class="form-group">
                    <?php echo $this->Form->input('brand',array('type'=>'select','empty' =>'ALL','style'=>'width:100%','id'=>'brand_id','class'=>'form-control select_two_class product_flitering search_class product_flitering','label'=>'Brand')) ?>
                  </div>
                </div>
              </div>
              <div class="col-md-3 col-lg-3 col-sm-3">
              <div class="box-body">
                <div class="form-group">
                  <?php echo $this->Form->input('product_id',array('type'=>'select','empty' =>'ALL','class'=>'form-control select_two_class search_class ','style'=>'width:100%','label'=>'Product','id'=>'product_id',)) ?>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3">
              <div class="box-body">
                <div class="form-group">
                  <?php echo $this->Form->input('customertype_id',array('type'=>'select','empty'=>'ALL','class'=>'form-control select_two_class  search_class','label'=>'Customer Type','id'=>'customer_type_id')) ?>
                </div>
              </div>
            </div> 
            </div>
        </div> 
        <div class="row">
          <div class="col-md-12">            
            <div class="col-md-3 col-lg-3 col-sm-3">
              <div class="box-body">
                <div class="form-group">
      <?php echo $this->Form->input('customer_id',array('type'=>'select','empty'=>'ALL','class'=>'form-control select_two_class search_class','label'=>'Customer','id'=>'customer_id')) ?>
                </div>
              </div>
            </div> 
            <div class="col-md-3 col-lg-3 col-sm-3">
              <div class="box-body">
                <div class="form-group">
                  <?php echo $this->Form->input('executive_id',array('type'=>'select','empty'=>'ALL','class'=>'form-control select_two_class search_class ','label'=>'Executive','id'=>'executive_id')) ?>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3">
              <div class="box-body">
                <div class="form-group">
                  <?php echo $this->Form->input('route_id',array('type'=>'select','empty' =>'ALL','class'=>'form-control select_two_class search_class ','style'=>'width:100%','label'=>'Route','id'=>'route_id',)) ?>
                </div>
              </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3">
              <div class="box-body">
                <div class="form-group">
                  <div class="col-md-6 col-lg-6 col-sm-6">
                <?php echo $this->Form->input('from_date',array(
                'type'=>'text',
                'id'=>'from_date',
                'value'=>$from,
                'class'=>'search_class form-control cls_label_all date_field date_picker
                datepicker',
                'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                )); ?>
              </div>
              <div class="col-md-6 col-lg-6 col-sm-6">
                 <?php echo $this->Form->input('to_date',array(
                'type'=>'text',
                'id'=>'to_date',
                'value'=>$to,
                'class'=>'search_class form-control cls_label_all date_field date_picker
                datepicker',
                'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                )); ?>
                 </div>
                </div>
              </div>
            </div> 
          </div>
        </div>  
        </div> 
    </div>
      <div class="box-body">
        <div class="col-md-4 col-xs-12">
          <h3 class="muted "></h3>
        </div>
        <table class="table table-condensed table boder table-bordered" id='table_bonus_approval' data-page-length="10">
          <thead>
            <tr class="blue-bg">
               <th width="20%">Customer</th>
               <th>Invoice No:</th>
              <th width="25%">Product</th>
              <th align="right">Sale Qty</th>
               <th align="right">Executive Rate</th>
               <th align="right">Sale Rate</th>
               <th align="right">Bonus</th>
              <th align="right">Status</th>
             </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</section>

<script type="text/javascript">
$('#table_bonus_approval').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/Bonus_approval_report_ajax",
      "type": "POST",
      data:function( d ) {
        d.product_type_id= $('#product_type_id').val();
        d.brand_id= $('#brand_id').val();
        d.product_id= $('#product_id').val();
        d.executive_id= $('#executive_id').val();
        d.customer_type_id= $('#customer_type_id').val();
        d.customer_id= $('#customer_id').val();
        d.route_id= $('#route_id').val();
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
      },
      "dataSrc": "records",
    },
   dom: 'Bfrtip',
   lengthMenu: [
    [10,25, 50,100,-1],
    ['10 rows','25 rows', '50 rows','100 rows','Show all' ]
    ],
    buttons: [
        { extend: 'excel', title: 'Return Report', exportOptions: { columns: ':visible' } }, 
        { extend: 'csv', title: 'Return Report', exportOptions: { columns: ':visible' } }, 
    'pageLength',
    ],
    "columns": [
     { "data" : "AccountHead.name" },
      { "data" :"Sale.invoice_no" },
    { "data" : "Product.name" },
    { "data" : "SaleItem.quantity" ,className:"text-right"},
    { "data" : "SaleItem.actual_price" ,className:"text-right"},
     { "data" : "SaleItem.unit_price" ,className:"text-right"},
      { "data" : "SaleItem.bonus" ,className:"text-right"},
        { "data" : "SaleItem.status" },
    ],
    "columnDefs": [],
  });
  $('.search_class').change(function(){
    table = $('#table_bonus_approval').dataTable();
    table.fnDraw();
  });

  $.fn.product_flitering=function(){
    var product_type_id=$('#product_type_id').val();
    var brand_id=$('#brand_id').val();
    var hidden_product_search_id=$('#hidden_product_search_id').val();
    var data={
      product_type_id:product_type_id,
      brand_id:brand_id,
      hidden_id:hidden_product_search_id,
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_type_select_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) { 
        $('#product_id').html('');     
        $('#product_id').append($("<option></option>").attr("value",'').text('SELECT'));
        $.each(response, function(i, value) {
          $('#product_id').append($("<option></option>").attr("value", i).text(value));
        });
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  }
  
$(document).on('change','#customer_type_id',function(){
  var customer_type_id=$(this).val();
  $.post( "<?= $this->webroot ?>Customer/get_customer_by_customer_type_ajax/"+customer_type_id ,function( data ) {
    $('#customer_id').empty();
      $('#customer_id').append($("<option></option>").attr("value",'').text('SELECT'));
    $.each(data.options,function(key,value){
     $('#customer_id').append($("<option></option>").attr("value",key).text(value));
  });
  }, "json");
   });
  $('.product_flitering').change(function(){
    $.fn.product_flitering();
  });
</script>
<script type="text/javascript">
 $('.date_range_picker').daterangepicker({
    opens: 'left',
    locale: { format: 'DD.MM.YYYY'}
  });
  $('.date_range_picker').on('blur', function(ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
  });
</script>