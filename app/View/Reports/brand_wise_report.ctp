<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>

<section class="content-header">
  <h1>Executive Brand wise sale Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row">
         <div class="col-md-3">
            <?php echo $this->Form->input('executive_id',array(
                      'type'=>'select',
                      'id'=>'executive_id',
                      'style'=>'width:100%',
                      'class'=>'form-control select_two_class search_field',
                      'empty'=>'ALL',
                      )); ?>
         </div>
          <div class="col-md-2">
                      <div class="col-md-12">
                       <?php echo $this->Form->input('from_date',array(
                        'type'=>'text',
                        'id'=>'from_date',
                        'class'=>'form-control search_field date_picker datepicker',
                        'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                        )); ?>
                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="col-md-12">
                        <?php echo $this->Form->input('to_date',array(
                          'type'=>'text',
                          'id'=>'to_date',
                          'class'=>'form-control  search_field date_picker datepicker',
                          'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                          )); ?>
                        </div>
                      </div>
         <div class="col-md-1"><br>
           <button class='btn' type='button' id='collection_report_button'>Fetch</button>
        </div>
          <div class="col-md-4 pd-lt-0"> <br>
                <label>Export</label> <span class="print-span"><a id="exportExcel" target="_blank"><i class="fa fa-download fa fa-2x" aria-hidden="true" style="padding-left: 10px;"></i></a></span>
              </div>
      </div>
    </div>
    <div class="box-body">
      <div class="col-md-4 col-xs-12">
        <h3 class="muted "></h3>
      </div>
      <table class="table table-condensed table " id='table_collection_list' data-page-length="-1" border="1">
            <thead>
              <tr class="blue-bg">
                <th>Executive Name</th>
                <!-- <th>Customer Name</th> -->
                <th>Brand Name</th>
                <th>Product Name</th>
                <!-- <th>Sub Brand Name</th> -->
                <th>Quantity</th>
                  <th>Amount</th>
               
           </tr>
            </thead>
            <tbody>
           
            </tbody>
            <tfoot></tfoot>
          </table>
    </div>
  </div>
</div>

</section>
<script type="text/javascript">
$(document).ready(function(){
$('#table_collection_list').DataTable();
$('#collection_report_button').on('click',function(){
  $.fn.table_search();
});
$.fn.table_search=function(){  
//alert(); 
 var from_date=$('#from_date').val();
 var to_date=$('#to_date').val();
 var executive_id=$('#executive_id').val();
  var data={
     from_date:from_date,
          to_date:to_date,
    executive_id:executive_id
  };
  // console.log(data);
      var url_address= '<?php echo $this->webroot; ?>'+'Reports/brand_wise_report_ajax';

  //var url_address= '<?php echo $this->webroot; ?>'+'Reports/aging_report_ajax';
  $.ajax({
    type: "post",
    url:url_address,
    data: data,
    dataType:'json',
    success: function(response) {
      $('#table_collection_list').DataTable().destroy();
      $('#table_collection_list tbody').html(response.row);
      $('#table_collection_list tfoot').html(response.tfoot);
       $('#table_collection_list').find('.toggle_rows').hide();
      $('#table_collection_list').DataTable(
        {

        "ordering": false,
        dom: 'Bfrtip',
        buttons: [
         {
               extend: 'colvis',
             },
        {
          extend: 'print',
          // text: 'Print' ,
          // title: 'Executive Brand Wise Sale Report',
           footer: true,
       exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                    // '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
                    // '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
                    // '<h5 align="center">Thiruvangoor</h5',
                    // '<h5 align="center">Calicut-673 304</h5>',
                    // '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
            '<h3 align="center">Executive Brand Wise Sale Report</h3>',
            '<h5>Executive :'+response.executives+'</h5>',
                    '<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+'</h5>'
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
                // .prepend(
                //   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
                //   )
              }
            },
            {
              //extend: 'csv',
              // text: 'CSV' ,
              extend: 'csvHtml5',
            title:'Brand Wise Sale Report of '+response.executives+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',

               footer: true,
               customize: function (csv) {
                 return "\tBrand Wise Sale Report of  "+response.executives+"\n"+"\t(Period : From "+$("#from_date").val()+"    To : "+$("#to_date").val()+")\n"+  csv ;
               },
       exportOptions: { columns: ':visible'},
            },
            {
              extend: 'excel',
              // text: 'Excel' ,
              // title:'Executive Brand Wise Sale Report',
               title:'Brand Wise Sale Report of'+response.executives +'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',

               footer: true,
       exportOptions: { columns: ':visible'},
            },
            {
              extend: 'pdf',
             // text: 'Pdf' ,
              //title:'Executive Brand Wise Sale Report',
              title:'Brand Wise Sale Report of '+response.executives+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',

               footer: true,
       exportOptions: { columns: ':visible'},
            },
            {
               extend: 'pageLength',
             },
            ]
            
          });
        $.fn.show_alert('Success');
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
}

$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }

 $('#table_collection_list').find('.toggle_rows').hide();
  $('#table_collection_list').on("click","td", function(){
    var idOfParent = $(this).parents('tr').attr('id');
    //alert(idOfParent);
    $('tr.child-'+idOfParent).toggle('fast');
  });
  });
$('#exportExcel').on('click',function(e)   { 
    e.preventDefault();
     var from_date=$('#from_date').val();
     var to_date=$('#to_date').val();
     var executive_id=$('#executive_id').val();


    // var website_url3='<?= $this->webroot; ?>Stock/FilterExcel/'+brand_id+'/'+product_type_id +'/'+'/'+warehouse_id+'/' +product_id;
    var website_url3='<?= $this->webroot; ?>Reports/BrandExport/'+from_date+'/'+to_date;
    $(location).attr("href", website_url3);
  });
// $.fn.table_search();
</script>