<style type="text/css">

.cls_label_all {
  padding-top: 5%;
}
.row_top_row{

  margin-top: 5%;
}
.deaf_btn_btn {
  margin-left: 4%;
}

.row_new_add{
  margin-top: 2%;
}
#radio_butto_add {
  margin-top: 7px;
  margin-left: -51%;
}
.check_bx_rght{
  white-space: nowrap;
  padding-top: 20px;
}
.chkbx_icn_clk {
    position: absolute;
    margin-left: -16px !important;
}
</style>

<section class="content-header">
  <h1>Collection Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="Stockmanagement">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                <div class="row" style="margin-top: 2%;">
                  <div class="col-md-3" hidden="hidden">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('executive_id',array(
                      'type'=>'select',
                      'id'=>'executive_id',
                      'style'=>'width:100%',
                      'options'=>$executive_list,
                      'class'=>'form-control select_two_class search_field',
                      'empty'=>'ALL',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('from_date',array(
                      'type'=>'text',
                      'id'=>'from_date',
                      'class'=>'form-control search_field date_picker datepicker',
                      'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12">
                      <?php echo $this->Form->input('to_date',array(
                        'type'=>'text',
                        'id'=>'to_date',
                        'class'=>'form-control  search_field date_picker datepicker',
                        'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                        )); ?>
                      </div>
                    </div>
                    
                    <div class="col-md-2">
                      <div class="col-md-12">
                        <br>
                        <button class='btn' type='button' id='collection_report_button'>Fetch</button>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="col-md-12">
                        <div class="checkbox">
                          <label class="label_chk_bx check_bx_rght"><input type="checkbox" name="data[Report]['check']" id="checkbox" value="" class="chkbx_icn_clk">Show Zero Balance Customers</label>
                        </div>
                       <!--  <button class='btn' type='button' id='collection_report_button'>Fetch</button> -->
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="box-body">
                    <div class="col-md-4 col-xs-12">
                      <h3 class="muted "></h3>
                    </div>
                    <table class="table table-condensed table boder no-footer" id='table_collection_list'>
                      <thead>
                        <tr class="blue-bg">
                          <!-- <th>Executive</th> -->
                          <!-- <th>Circle</th> -->
                          <th>Date</th>
                          <th>Customer</th>
                          <th>Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                      <tfoot>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <script type="text/javascript">
      $('#table_collection_list').DataTable();
      $('#collection_report_button').on('click',function(){
        $.fn.table_search();
      });
      $('#checkbox').click(function(){
        $.fn.table_search();
      });
      $.fn.table_search=function(){   
        var from_date=$('#from_date').val();
        var to_date=$('#to_date').val();
        var executive_id=$('#executive_id').val();
        if($('#checkbox').is(':checked')){
          var check_id=0;
        }

        else

        {
          var check_id=1;
        }
        var data={
          from_date:from_date,
          to_date:to_date,
          executive_id:executive_id,
          check_id:check_id,
        };

        var url_address= '<?php echo $this->webroot; ?>'+'Reports/collection_report_ajax';
        $.ajax({
          type: "post",
          url:url_address,
          data: data,
          dataType:'json',
          success: function(response) {
            $('#table_collection_list').DataTable().destroy();
            $('#table_collection_list tbody').html(response.row);
            $('#table_collection_list tfoot').html(response.tfoot);
            $('#table_collection_list').DataTable();
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      }
// $.fn.table_search();
</script>
