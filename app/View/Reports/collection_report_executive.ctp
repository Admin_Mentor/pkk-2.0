<style type="text/css">

.cls_label_all {
  padding-top: 5%;
}
.row_top_row{

  margin-top: 5%;
}
.deaf_btn_btn {
  margin-left: 4%;
}

.row_new_add{
  margin-top: 2%;
}
#radio_butto_add {
  margin-top: 7px;
  margin-left: -51%;
}
.check_bx_rght{
  white-space: nowrap;
  padding-top: 20px;
}
.chkbx_icn_clk {
    position: absolute;
    margin-left: -16px !important;
}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Executive/Route Collection Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="Stockmanagement">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                <div class="row" style="margin-top: 2%;">
                  <div class="col-md-3">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('branch_id',array(
                      'type'=>'select',
                      'id'=>'branch_id',
                      'style'=>'width:100%',
                      'options'=>$branch_list,
                      'class'=>'form-control select_two_class search_field',
                      'empty'=>'MAIN',
                      )); ?>
                    </div>
                  </div>
                   <div class="col-md-3">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('route_id',array(
                      'type'=>'select',
                      'id'=>'route_id',
                      'style'=>'width:100%',
                      'options'=>$route_list,
                      'class'=>'form-control select_two_class search_field',
                      'empty'=>'ALL',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('executive_id',array(
                      'type'=>'select',
                      'id'=>'executive_id',
                      'style'=>'width:100%',
                      'options'=>$executive_list,
                      'class'=>'form-control select_two_class search_field',
                      'empty'=>'ALL',
                      )); ?>
                    </div>
                  </div>
                 
                  <div class="col-md-2">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('from_date',array(
                      'type'=>'text',
                      'id'=>'from_date',
                      'class'=>'form-control search_field date_picker datepicker',
                      'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12">
                      <br>
                      <?php echo $this->Form->input('to_date',array(
                        'type'=>'text',
                        'id'=>'to_date',
                        'class'=>'form-control  search_field date_picker datepicker',
                        'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                        )); ?>
                      </div>
                    </div>
                    
                    <div class="col-md-2" style="margin-top: 25px">
                      <!-- <div class="col-md-12"> -->
                        <br>
                        <button class='btn btn-success' type='button' id='collection_report_button'>Get</button>
                      <!-- </div> -->
                    </div>
                    <div class="col-md-3">
                      <div class="col-md-12">
                        <br>
                        <div class="checkbox">
                          <label class="label_chk_bx check_bx_rght"><input type="checkbox" name="data[Report]['check']" id="checkbox" value="" class="chkbx_icn_clk">Show Balance Customers</label>
                        </div>
                       <!--  <button class='btn' type='button' id='collection_report_button'>Fetch</button> -->
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="box-body">
                    <div class="col-md-4 col-xs-12">
                      <h3 class="muted "></h3>
                    </div>
                <table id='table_collection_list' style="width: 100% !important" class="table table-hover boder  table-bordered"  data-order='[[ 2, "desc" ]]'>
                <thead>
                <tr class="blue-bg">
                <th>Date</th>
                <th>Customer</th>
                <th>Amount</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                <tr>
                <th colspan="1" style="font-size:20px; color:red;text-align:right">Total:</th>
                <th style="font-size:20px; color:red;text-align:right"></th>
                </tr>
                </tfoot>
                </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
      <script type="text/javascript">
$('#table_collection_list').DataTable( {
  "processing": true,
  "serverSide": true,
  "ajax": {
    "url": "<?= $this->webroot ?>Reports/collection_report_executive_ajax",
    "type": "POST",
    data:function( d ) {
      d.from_date= $('#from_date').val();
      d.to_date= $('#to_date').val();
       d.executive_id= $('#executive_id').val();
       d.route_id=$('#route_id').val();
        if($('#checkbox').is(':checked')){
         d.check_id=1;
        }

        else

        {
         d.check_id=0;
        }
      },
      "dataSrc": "records",
    },
      dom: 'Bfrtip',
    lengthMenu: [
   [10, 50,100,200],
    ['10 rows', '50 rows','100 rows','200 rows' ]
    ],
    buttons: [
    { extend: 'colvis', },
    { extend: 'excel', title: 'Executive/Route Collection Report', exportOptions: { columns: ':visible' } }, 
   // { extend: 'csv',   title: 'Bill Wise Report', exportOptions: { columns: ':visible' } },
    'pageLength',
    ],
    "columns": [
    { "data" : "AccountHead.date" },
    { "data" : "AccountHead.name"},
    { "data" : "AccountHead.amount" ,className:"text-right"},
    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;
      var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
        i : 0;
      };
       pageTotal = api.column( 2, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 2 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
    },
    "columnDefs": [],
  });
$('#collection_report_button').click(function(){
  table = $('#table_collection_list').dataTable();
  table.fnDraw();
});
$('#checkbox').click(function(){
  table = $('#table_collection_list').dataTable();
  table.fnDraw();
      });
      </script>
