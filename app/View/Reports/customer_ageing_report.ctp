<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>

<section class="content-header">
  <h1>Customer Ageing Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row">
          <br>
         <div class="col-md-3">
          <?php echo $this->Form->input('customer_id',array(
            'type'=>'select',
            'id'=>'customer_id',
            'style'=>'width:100%',
            'options'=>$Customer_list,
            'class'=>'form-control select_two_class search_field',
            )); ?>
          </div>
          <div class="col-md-1"><br>
           <button class='btn btn-success' type='button' id='collection_report_button'>Get</button>
         </div>
       </div>
     </div>
     <div class="box-body">
      <div class="col-md-4 col-xs-12">
        <h3 class="muted "></h3>
      </div>
      <table class="table table-condensed table " id='table_collection_list' data-page-length="-1" border="1">
        <thead>
          <tr class="blue-bg">
            <th>Customer Name</th>
            <th class="text-right">30 Days</th>
            <th class="text-right">60 Days</th>
            <th class="text-right">90 Days</th>
            <th class="text-right">Above 90 Days</th>
            <th class="text-right">Amount</th>
          </tr>
        </thead>
        <tbody>
         
        </tbody>
        <tfoot></tfoot>
      </table>
    </div>
  </div>
</div>

</section>
<script type="text/javascript">

  $('#table_collection_list').DataTable();
  $('#collection_report_button').on('click',function(){
    $.fn.table_search();
  });

  $.fn.table_search=function(){  
//alert(); 
var customer_id=$('#customer_id').val();
//var from_date=$('#from_date').val();
var data={
  customer_id:customer_id,
  //from_date:from_date
};
  // console.log(data);
  var url_address= '<?php echo $this->webroot; ?>'+'Reports/customer_ageing_report_ajax';

  //var url_address= '<?php echo $this->webroot; ?>'+'Reports/aging_report_ajax';
  $.ajax({
    type: "post",
    url:url_address,
    data: data,
    dataType:'json',
    success: function(response) {
      $('#table_collection_list').DataTable().destroy();
      $('#table_collection_list tbody').html(response.row);
      $('#table_collection_list tfoot').html(response.tfoot);

      $('#table_collection_list').DataTable({

        "ordering": false,
        dom: 'Bfrtip',
        buttons: [
        {
               extend: 'colvis',
             },
        {
          extend: 'print',
          //text: 'Print' ,
          //title: 'Executive Wise Ageing Report',
           footer: true,
       exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                    // '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
                    // '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
                    // '<h5 align="center">Thiruvangoor</h5',
                    // '<h5 align="center">Calicut-673 304</h5>',
                    // '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
                    '<h3 align="center">Customer Ageing Report</h3>',
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
                // .prepend(
                //   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
                //   )
              }
            },
            {
              extend: 'csv',
             // text: 'CSV' ,
             title:'Customer Ageing Report',
              footer: true,
       exportOptions: { columns: ':visible'},
       customize: function (csv) {
                 return "\t   Customer Ageing Report\n"+  csv ;
               }
            },
            {
              extend: 'excel',
             // text: 'Excel' ,
              title:'Customer Ageing Report',
               footer: true,
       exportOptions: { columns: ':visible'},
            },
            {
              extend: 'pdf',
             // text: 'Pdf' ,
             title:'Customer Ageing Report',
              footer: true,
       exportOptions: { columns: ':visible'},
            },
            {
               extend: 'pageLength',
             },
            ]
            
          });
      $.fn.show_alert('Success');

    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
}
// $.fn.table_search();
$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
</script>