<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<section class="content-header">
  <h1>Daily Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row">
           <div class="col-md-3">
              <?php echo $this->Form->input('select_executive_id',array('type'=>'select','id'=>'select_executive_id','style'=>'width:100%','class'=>'form-control select_two_class search_field','options'=>$executives)); ?>
            </div>
          <div class="col-md-2">
            <div class="col-md-12">
              <?php echo $this->Form->input('from_date',array(
                'type'=>'text',
                'id'=>'from_date',
                'class'=>'form-control search_field date_picker datepicker',
                'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                'label'=>'Date',
              )); ?>
            </div>
          </div>
          <div class="col-md-1"><br>
            <button class='btn btn-success' type='button' id='fetch_button'>Get</button>
          </div>
        </div>
      </div>
      <div class="box-body">
        <div class="col-md-4 col-xs-12">
          <h3 class="muted "></h3>
        </div>
        <table class="table table-condensed table boder no-footer table-bordered" id='table_daily_report' data-page-length="5" width="98%">
          <thead>
            <tr class="blue-bg">
              <th style="width:15%">Executive</th>
              <th style="width:12%">Route</th>
              <th style="width:12%">Group</th>
              <th style="width:10%">Day Regstr Time</th>
              <th style="width:10%">Day Close Time</th>
              <th style="width:10%">Starting Km</th>
              <th style="width:10%">Ending Km</th>
              <th style="width:10%">Expense</th>
              <th style="width:10%">CreditSale</th>
              <th style="width:10%">Cash Sale</th>
              <th style="width:10%">Sale Total</th>
               <th style="width:10%">Sales Return</th>
              <th style="width:10%">Receipt</th>
              <th style="width:8%">Status</th>
              <th style="width:8%">Export</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <!-- <tfoot>
          </tfoot> -->
        </table>
      </div>
    </div>
  </div>
  <div id="Inhandcash_modal" class="modal fade" role="dialog" >
                <div class="modal-dialog modal-lg">
                  <div class="modal-content pull-middle">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" align='center'><span  hidden id='day_register_id'></span><span hidden id='route_id_hidden_id'></span><span hidden id='executive_id'></span><span id='route_name'></span> &nbsp&nbsp&nbsp Inhand Cash Transfer</h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-sm-12 inhand_class"> <div class="col-sm-4">
                          <?= $this->Form->input('collection_amount',array('class'=>'form-control number ','id'=>'collection_amount','label'=>'Collection Total','value'=>'','type'=>'text','readonly')) ?>
                        </div></div></div>
                      <div class="row">
                        <div class="col-sm-12 inhand_class">
                            <div class="col-sm-4">
                              <?= $this->Form->input('inhand_amount',array('class'=>'form-control number ','id'=>'inhand_amount','label'=>'Collected Amt','value'=>'','type'=>'text',)) ?>
                            </div>
                            <div class="col-sm-4">
                              <?= $this->Form->input('bonus_payment',array('class'=>'form-control number ','id'=>'bonus_payment','label'=>'Bonus Payment','value'=>0,'type'=>'text',)) ?>
                               <?= $this->Form->input('bonus_payment_hidden',array('class'=>'form-control number ','id'=>'bonus_payment_hidden','label'=>'Bonus Payment hidden','value'=>0,'type'=>'hidden','readonly')) ?>
                            </div>
                            <div class="col-sm-4" style="margin-top:3%">
                              <button type="button" class="btn-sm btn-success btn" id='inhandcash_button' >Transfer</button>
                            </div>
                        </div>
                        </div>
                        <br>
                        <div class="row">
                        <div class="col-sm-9">
                            <div class="col-sm-4">
                            <label>First Sale Time : </label><span id="first_sale_time"></span>
                            </div>
                            <div class="col-sm-4">
                            <label>Last Sale Time : </label><span id="last_sale_time"></span>
                            </div>
                        </div>
                        </div>
                         <div class="modal-header">
                  <h5 class="modal-title" align='center'>Denominations</h5>
                    </div>
                      <div class="box-body table-responsive no-padding xs_tp">
                        <table class="table table-bordered table-hover" id="Denominations_table">
                          <thead>
                            <tr class="blue-bg">
                              <th>Denominations</th>
                               <th>Count</th>
                               <th>Total Amount</th>
                               <th>Status</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                      <div class="modal-header">
                  <h5 class="modal-title" align='center'>Sale Below Executive Rate</h5>
                    </div>
                      <div class="box-body table-responsive no-padding xs_tp">
                        <table class="table table-bordered table-hover" id="sale_table">
                          <thead>
                            <tr class="blue-bg">
                              <th>Customer</th>
                              <th>Invoice No</th>
                              <th>Product</th>
                               <th>Quantity</th>
                               <th>Executive Rate</th>
                               <th>Sale Rate</th>
                               <th>Bonus Return</th>
                               <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                          </tbody>
                          <tfoot>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div> 
</section>
<script type="text/javascript">
  $('#table_daily_report').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/DailyReportAjax",
      "type": "POST",
      data:function( d ) {
        d.date= $('#from_date').val();
        d.executive_id= $('#select_executive_id').val();
      },
      "dataSrc": "records",
    },
  dom: 'Bfrtip',
    lengthMenu: [
    [10,25, 50,100,-1],
    ['10 rows, 25 rows', '50 rows','100 rows','Show all' ]
    ],
     buttons: [
    { extend: 'colvis', },
    {extend: 'excel',text: 'excel',title: "Daily Report",filename: 'Daily Report',}, 
    {extend: 'print',text: 'print',title: "Daily Report",filename: 'Daily Report',}, 
    'pageLength',
    ],
    "columns": [
    { "data" : "Executive.name" },
    { "data" : "Executive.route" },
    { "data" : "Executive.customer_group" },
    { "data" : "Executive.day_register_time" },
    { "data" : "Executive.day_close_time" },
    { "data" : "Executive.starting_km" ,className:"text-right"},
    { "data" : "Executive.ending_km" ,className:"text-right"},
     { "data" : "Executive.expense",className:"text-right"},
    { "data" : "Executive.credit_sale" ,className:"text-right"},
     { "data" : "Executive.cash_sale" ,className:"text-right"},
    { "data" : "Executive.total_sale" ,className:"text-right"},
     { "data" : "Executive.total_sales_return" ,className:"text-right"},
    { "data" : "Executive.receipt_amount" ,className:"text-right"},
    { "data" : "Executive.status" },
     { "data" : "Executive.action" },
    ],
    "columnDefs": [
    {"targets": [ 0 ],"className": 'executive_name'},
     {"targets": [ 2 ] ,visible: false},
      {"targets": [ 3 ] ,visible: false},
       {"targets": [ 4 ] ,visible: false},
       {"targets": [ 13 ] ,orderable: false,},
        {"targets": [ 14 ] ,orderable: false,},
    ],
    "createdRow": function( row, data, dataIndex){
        //$(row).css('background-color','#'+data.Complaint.priority);
        $(row).css('background-color',data.Executive.difference);
      },
  });
  $('#fetch_button').click(function(){
    table = $('#table_daily_report').dataTable();
    table.fnDraw();
  });
 $(document).on('click','td.executive_name',function(){
  var executive_id=$(this).closest('tr').find('td span.executive_id').text();
  var is_transfer=$(this).closest('tr').find('td span.is_transfer').text();
    var day_register_id=$(this).closest('tr').find('td span.day_register_id').text();
  var route_id=$(this).closest('tr').find('td span.route_id').text();
  var route_name=$(this).closest('tr').find('td span.route_name').text();
  var executive_inhand_cash=$(this).closest('tr').find('td span.executive_inhand_cash').text();
    var executive_collected=$(this).closest('tr').find('td span.executive_collected').text();
   var bonus_payment=$(this).closest('tr').find('td span.bonus_payment').text();
    var bonus_hidden_payment=$(this).closest('tr').find('td span.bonus_hidden_payment').text();
  var executive_name=$(this).closest('tr').find('td.executive_name').text();
  var date= $('#from_date').val();
  var new_data={
     date:date,
     route_id:route_id,
     executive_id:executive_id,
     day_register_id:day_register_id
  };
   $('#route_name').text(route_name);
   $('#executive_id').text(executive_id);
    $('#day_register_id').text(day_register_id);
    $('#route_id_hidden_id').text(route_id);
    $('#inhand_amount').val(executive_collected);
    $('#collection_amount').val(executive_inhand_cash);
    $('#bonus_payment').val(bonus_payment);
    $('#bonus_payment_hidden').val(bonus_hidden_payment);
    $('#inhandcash_button').prop('disabled',false);
    $('.inhand_class').show();
$('#inhandcash_button').show();
    // if(is_transfer==1)
    // {
    //   $('.inhand_class').hide();
    // }
  $.post( "<?= $this->webroot ?>Reports/get_executive_denomination",new_data ,function( data ) {
    $('#Denominations_table').DataTable().destroy();
    $('#Denominations_table tbody').empty();
    $('#Denominations_table tbody').html(data.row.tbody);
    $('#Denominations_table tfoot').empty();
    $('#Denominations_table tfoot').html(data.row.tfoot);
    $('#sale_table').DataTable().destroy();
    $('#sale_table tbody').empty();
    $('#sale_table tbody').html(data.row.tbody_sale);
    $('#first_sale_time').text(data.sale_first);
    $('#last_sale_time').text(data.sale_last);
  $('#Inhandcash_modal').modal('show');
}, "json");

});
$(document).on('change','.denomination_confirm',function(){
  var id=$(this).closest('tr').find('td span.denomination_id').text();
   // if(!confirm("Are you sure?"))
   //      {
   //          return false;
   //      }
        var data={
            id:id,
        };
        var website_url = '<?php echo $this->webroot; ?>Reports/denomination_confirm';
        $.ajax({
            method: "POST",
            url: website_url,
            data: data,
        }).done(function (data) {
        });
           $(this).closest('tr').find('td input.denomination_confirm').prop('disabled',true);

  });
 $(document).on('click','#inhandcash_button',function(){
  var route_id=$('#route_id_hidden_id').text();
  var executive_id=$('#executive_id').text();
  var inhand_amount=$('#inhand_amount').val();
   var collection_amount=$('#collection_amount').val();
    var bonus_payment_hidden=$('#bonus_payment_hidden').val();
  var bonus_payment=$('#bonus_payment').val();
  var day_register_id=$('#day_register_id').text();
   // if(inhand_amount==0)
   //  {
   //    $('#inhand_amount').focus();
   //    return false;
   //  }
   $('#inhandcash_button').hide();
  var date= $('#from_date').val();
  var new_data={
     date:date,
     route_id:route_id,
     amount:inhand_amount,
     day_register_id:day_register_id,
     executive_id:executive_id,
     bonus_payment:bonus_payment,
     bonus_payment_hidden:bonus_payment_hidden,
     collection_amount:collection_amount
  };
  $.post( "<?= $this->webroot ?>Reports/inhandcash_transfer",new_data ,function( data ) {
    $('#Inhandcash_modal').modal('toggle');
      table = $('#table_daily_report').dataTable();
    table.fnDraw();
}, "json");

});
// $('#Inhandcash_modal').on('hidden.bs.modal', function (e) {
//      table = $('#table_daily_report').dataTable();
//     table.fnDraw();
// });
    $(document).on('click', '.aprv_bttn', function () {
        var value=$(this).val();
            if(!confirm("Are you sure?"))
        {
            return false;
        }
        var id=$(this).closest('tr').find('td span.saleitem_id').text();
        var data={
            id:id,
            value:value
        };
        var website_url = '<?php echo $this->webroot; ?>Reports/bonus_approval';
        $.ajax({
            method: "POST",
            url: website_url,
            data: data,
        }).done(function (data) {
                    // $('#Inhandcash_modal').modal('toggle');
      table = $('#table_daily_report').dataTable();
    table.fnDraw();
        });
      $(this).closest('tr').remove();
    });
</script>
