<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Date wise Bonus/Collection Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="box-header">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-3">
              <?php echo $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','style'=>'width:100%','class'=>'form-control select_two_class search_field')); ?>
            </div>
            <div class="col-md-2">
              <div class="col-md-12">
                <?php echo $this->Form->input('from_date',array('type'=>'text','id'=>'from_date','class'=>'form-control search_field date_picker datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
            <div class="col-md-2">
              <div class="col-md-12">
                <?php echo $this->Form->input('to_date',array('type'=>'text','id'=>'to_date','class'=>'form-control  search_field date_picker datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
            <div class="col-md-1"><br>
              <button class='btn btn-success' type='button' id='fetch_button'>Get</button>
            </div>
          </div>
        </div>
      </div>
      <div class="box-body" style="overflow:auto">
        <table class="table table-condensed" id='table_bonus' data-page-length="-1" border="1">
          <thead>
            <tr class="blue-bg">
              <th>Date</th>
              <th>Total Bonus</th>
               <th>Total Bonus Paid</th>
              <th>Total Collection Amt</th>
              <th>Total Collected Amount</th>
              <th>Balance</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <tr>
              <th style="font-size:20px; color:red;text-align:right">Total:</th>
              <th style="font-size:20px; color:red;">0</th>
               <th style="font-size:20px; color:red;">0</th>
              <th style="font-size:20px; color:red;">0</th>
              <th style="font-size:20px; color:red;">0</th>
              <th style="font-size:20px; color:red;">0</th>
              <th></th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
    <div id="bonus_modal" class="modal fade" role="dialog" >
                <div class="modal-dialog modal-lg" style="width: 75%">
                  <div class="modal-content pull-middle">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h5 class="modal-title" align='center'>Bonus Details</h5>
                    </div>
                    <div class="modal-body">
                      <div class="box-body table-responsive no-padding xs_tp">
                    <table class="table table-condensed" id='bonus_details_table' data-page-length="10" border="1">
                    <thead>
                    <tr class="blue-bg">
                    <th>Product</th>
                    <th>Sale Invoice No</th>
                    <th>Credit Note No</th>
                    <th>V2B Transfer No</th>
                    <th>Bonus Amount</th>
                    <th>Bonus Return Amount</th>
                    <th>Total Bonus</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                    </table>
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
                    </div>
                  </div>
                </div>
              </div> 
</section>
<script type="text/javascript">
  $('#table_bonus').dataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/DateWiseBonusReport_ajax",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
        d.executive_id= $('#executive_id').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "ExecutiveBonusCalculation.date" },
    { "data" : "ExecutiveBonusCalculation.total_bonus" ,className:"text-right"},
    { "data" : "ExecutiveBonusCalculation.bonus_paid" ,className:"text-right"},
    { "data" : "ExecutiveBonusCalculation.total_collection_amt" ,className:"text-right"},
    { "data" : "ExecutiveBonusCalculation.total_collected" ,className:"text-right"},
    { "data" : "ExecutiveBonusCalculation.balance",className:"text-right" },
     { "data" : "ExecutiveBonusCalculation.action" },
    ],
    "dom": 'Bfrtip',
    'bSort':false,
    "buttons": [
    { extend: 'colvis' },
    {
      extend: 'print',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    {
      extend: 'excel',
      title:'Executive Bonus Report of '+$('#executive_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    {
      extend: 'pdf',
      title:'Executive Bonus Report of '+$('#executive_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
  //  {extend: 'pageLength'},
    ],
    "columnDefs": [
    {"targets": [ 0 ],"className": 'bonus_date' ,},
    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data; var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
      var pagecount=1;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
      pagecount++;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
      pagecount++;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
       pagecount++;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
       pagecount++;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
    },
  });
  $('#fetch_button').on('click',function(){
    table = $('#table_bonus').dataTable();
    table.fnDraw();
  });
   $(document).on('click','td.bonus_date',function(){
  var executive_id=$('#executive_id').val();
  var day_register_id=$(this).closest('tr').find('td span.day_register_id').text();
  var new_data={
     day_register_id:day_register_id,
     executive_id:executive_id,
  };
  $.post( "<?= $this->webroot ?>Reports/get_ProductBonus_details",new_data ,function( data ) {
    $('#bonus_details_table').DataTable().destroy();
    $('#bonus_details_table tbody').empty();
    $('#bonus_details_table tbody').html(data.row.tbody);
    $('#bonus_details_table tfoot').empty();
    $('#bonus_details_table tfoot').html(data.row.tfoot);
    $('#bonus_details_table').DataTable( {
       "paging": false,
                dom: 'Bfrtip',
                buttons: [
               {
              extend: 'excel',
             title:'Product Wise Bonus',
               },
                ]
            } );
  $('#bonus_modal').modal('show');
}, "json");

});
   $(document).on('click','.button_inhandcash',function(){
      if(!confirm("Are you sure?"))
        {
            return false;
        }
  var date=$(this).closest('tr').find('td.bonus_date').text();
   var day_register_id=$(this).closest('tr').find('td span.day_register_id').text();
   var type_balance=$(this).closest('tr').find('td span.type_balance').text();
  var balance=$(this).closest('tr').find('td span.balance').text();
  var new_data={
     date:date,
     day_register_id:day_register_id,
     balance:balance,
     type_balance:type_balance,
  };
  $.post( "<?= $this->webroot ?>Reports/inhandcash_transfer_by_bonus_report",new_data ,function( data ) {
      table = $('#table_bonus').dataTable();
    table.fnDraw();
}, "json");

});
</script>