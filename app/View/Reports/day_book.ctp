<section class="content-header">
  <h1> DayBook </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary box_tp_brdr">
        <div class="row-wrapper">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <?= $this->Form->create('Reports');?>
                <div class="box-body">
                  <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <div class="form-group">
                      <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
                    </div>
                  </div>
                  <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <div class="form-group">
                      <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
                    </div>
                  </div>
                  <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                    <div class="form-group"><br>
                      <button type="button" class='btn btn-success' id='fetch_button'>Get</button>
                    </div>
                  </div>
                </div>
                <?= $this->Form->end(); ?>
              </div>
              <br>
              <div class="row-wrapper">
                <div class="row">
                  <div class="col-md-12">
                    <div class="box-body table-responsive no-padding">
                      <table class="table table-bordered table-condensed boder" data-order='[[ 1, "desc" ]]' id="table_data">
                        <thead>
                          <tr class="blue-bg">
                            <th width="9%">Date</th>
                            <th width="10%">Voucher No</th>
                            <th width="30%">Particulars</th>
                            <th>Voucher Type</th>
                            <th width="15%">Debit</th>
                             <th width="15%">Credit</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#table_data').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/DayBook_ajax",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
      },
      "dataSrc": "records",
    },
     "columns": [
    { "data" : "Journal.date" },
    { "data" : "Journal.voucher_no",className:"text-center" },
    { "data" : "AccountHeadCredit.name" },
    { "data" : "Journal.work_flow" },
    { "data" : "Journal.debit",className:"text-right" },
    { "data" : "Journal.credit",className:"text-right" },
    ],
  });
  $(document).on('click','#fetch_button',function(){
    table = $('#table_data').dataTable();
    table.fnDraw();
  });
  $(document).on('click','.daybook_delete',function(){
    if(!confirm("Are you sure?"))
    {
      return false;
    }
    var journal_id=$(this).closest('tr').find('td span.journal_id').text();
    $.post( "<?= $this->webroot ?>Accountings/Journal_delete/"+journal_id,function( data ) {
      if(data.result!='Success')
      {
        alert(data.message);
        return false;
      }
    table = $('#table_data').dataTable();
    table.fnDraw();
    }, "json");
    
  });
</script>