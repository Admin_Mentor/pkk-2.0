<section class="content-header">
  <h1> Day Close Register</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary box_tp_brdr">
        <div class="row-wrapper">
          <div class="row">
            <?= $this->Form->create('Reports');?>
            <div class="box-body">
              <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                  <?php echo $this->Form->input('from',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Register Date')); ?>
              </div>
              <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
               <?php echo $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','class'=>'form-control select2 ','style'=>'width: 100%','empty'=>'Select','style'=>array('width:100%'))); ?>
             </div>
             <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12"><br>
              <div class="col-md-3 col-lg-6 col-sm-6 col-xs-12">
                <span class="print-span"><a id="get_print" target="_blank"><i class="fa fa-print fa-2x" aria-hidden="true"></i></a></span>
              </div>
            </div>
          </div>
          <?= $this->Form->end(); ?>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  $('#get_print').on('click',function(e)   { 
    e.preventDefault();
    var register=$('#from').val();
    var executive=$('#executive_id').val(); 
    if(!executive)
    {
      $('#executive_id').select2('open');
      return false;
    }
    if(register!='' && executive!='0' ){
     var website_url='<?php echo $this->webroot; ?>Reports/dcrpdf_old/'+executive+'/'+register;
     window.open(website_url);
   }
 });
</script>
</script>