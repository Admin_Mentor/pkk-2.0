<section class="content-header">
  <h1> Day Close Register</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary box_tp_brdr">
        <div class="row-wrapper">
          <div class="row">
            <?= $this->Form->create('Reports');?>
            <div class="box-body">
              <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
                <?php echo $this->Form->input('from',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Date')); ?>
              </div>
              <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12" hidden="">
                <?php echo $this->Form->input('to',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'Closing Date')); ?>
              </div>
              <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
               <?php echo $this->Form->input('route_id',array('type'=>'select','id'=>'route_id','class'=>'form-control select2 ','style'=>'width: 100%','empty'=>'Select','style'=>array('width:100%'))); ?>
             </div>
             <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
               <?php echo $this->Form->input('day_register_id',array('type'=>'select','id'=>'day_register_id','class'=>'form-control select2 ','style'=>'width: 100%','style'=>array('width:100%'))); ?>
             </div>
             <div class="col-md-1 col-lg-1 col-sm-1 col-xs-12"><br>
              <div class="col-md-3 col-lg-6 col-sm-6 col-xs-12">
                <span class="print-span"><a id="get_print" target="_blank"><i class="fa fa-print fa-2x" aria-hidden="true"></i></a></span>
              </div>
            </div>
          </div>
          <?= $this->Form->end(); ?>
        </div>
        <br>
      </div>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">

  $('#route_id,#from').change(function(e){
    e.preventDefault();
    var route_id=$('#route_id').val();
    if(!route_id)
    {
      $('#route_id').select2('open');
      return false;
    }
    var register=$('#from').val();
    if(!register)
    {
      $('#from').focus();
      return false;
    }
    $.get( '<?= $this->webroot ?>Reports/CheckDayRegister1/'+route_id+'/'+register, function( responds ) {

      if(responds.result!='Success')
      {
        alert('Empty Day Register');
        return false;
      }
      $('#day_register_id').empty();
      $.each(responds.list,function(key,value){
        $('#day_register_id').append($("<option></option>").attr("value",key).text(value));
      });
    }, "json");
  });
  $('#get_print').on('click',function(e){ 
    e.preventDefault();
    var register=$('#from').val();
    if(!register)
    {
      $('#from').focus();
      return false;
    }
    var close=$('#to').val();
    // if(!close)
    // {
    //   $('#to').focus();
    //   return false;
    // }
    var day_register_id=$('#day_register_id').val();
    if(!day_register_id)
    {
      $('#day_register_id').select2('open');
      return false;
    }
    var website_url='<?php echo $this->webroot; ?>Reports/dcrpdf1/'+day_register_id+'/'+register+'/'+close;
    window.open(website_url);

  });
</script>
</script>