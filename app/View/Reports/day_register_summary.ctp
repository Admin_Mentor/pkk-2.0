<section class="content-header">
  <h1> Day Register Summary</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary box_tp_brdr">
        <div class="row-wrapper">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <?= $this->Form->create('Reports');?>
                <div class="box-body">
                  <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-md-4 col-lg-4 col-sm-4 col-xs-12 control-label"> Date</label>
                      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                        <?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'main_from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>false,)); ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-md-4 col-lg-4 col-sm-4 col-xs-12 control-label">Executive</label>
                      <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                       <?php echo $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','class'=>'form-control select2 ','style'=>'width: 100%','empty'=>array(0 => 'Select'),'options'=>$Executive_list,'label'=>false,'required'=>'required')); ?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                      
                      <div class="col-md-3 col-lg-6 col-sm-6 col-xs-12">
                        <span class="print-transfer"></span>
                      </div>
                      <div class="col-md-3 col-lg-6 col-sm-6 col-xs-12">
                      <span class="print-span"></span>
                      </div>

                    </div>
                  </div>
                   <!-- <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                     
                      <div class="col-md-3 col-lg-6 col-sm-6 col-xs-12">
                      <span class="print-span"></span>
                      </div>
                    </div>
                  </div >-->
                </div>
                <?= $this->Form->end(); ?>
              </div>
              <br>
              <div class="row-wrapper">
                <div class="row">
                  <div class="col-md-12">
                    <div class="box-body table-responsive no-padding">
                      <table class="table table-condensed table table boder boder" id="journal_table">
                        <thead>
                          <tr class="blue-bg">
                            <th>Customer</th>
                            <th>Action</th>
                            <th>Remark</th>
                             <th></th>
                          </tr>
                        </thead>
                        <tbody>
                      </tbody>
                      <tfoot>
                        
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div id="view_transaction_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span id='account_holder_name'></span></h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="control-label">From Date</label></div>
              <div class="col-md-7">
                <?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>false,)); ?>
              </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6">
              <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="control-label">To Date</label></div>
              <div class="col-md-7">
                <?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>false,)); ?>
              </div>
            </div>
          </div>
          <div class="box-body table-responsive no-padding xs_tp">
            <table class="table table-bordered table-hover" id="transaction_details_table">
              <thead>
                <tr class="blue-bg">
                  <th>Date</th>
                  <th>Cask/Bank</th>
                  <th>Remarks</th>
                  <th>Debit</th>
                  <th>Credit</th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Save</button>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $.fn.search_transaction_ajax=function(data_array){
    $.post( "<?= $this->webroot ?>Reports/DayRegisterReport",data_array ,function( data ) {
      $('#journal_table').DataTable().destroy();
      $('#journal_table tbody').empty();
      $('#journal_table tfoot').empty();
      console.log(data);
      $('#journal_table tbody').html(data.row.tbody);
      $('.print-transfer').html(data.transfer_print);
      $('.print-span').html('<a target="_blank" href="<?= $this->webroot; ?>Reports/DayRegisterDetailFunction/'+data.executive+'/'+data.date+'"><i class="fa fa-print fa-2x" aria-hidden="true"></i></a><a></a></span><a>');
   
      
      // $('#journal_table tfoot').html(data.row.tfoot);
      $('#journal_table').DataTable({
        "bSort" : false,
         "columnDefs": [ {
          "targets"  : 'no-sort',
            "orderable": false,
          }],
                dom: 'Blfrtip',
                buttons: [
                //'csv'
                  {
              extend: 'csv',
             // text: 'CSV' ,
             title:'Day Register Summary',
              footer: true,
       exportOptions: { columns: ':visible'},
       customize: function (csv) {
                 return "\t   Day Register Summary\n"+"\t(Period : From "+$("#main_from_date").val()+"    Executive : "+$("#executive_id option:selected").text()+")\n" +  csv ;
               }
            },
                ]

      });
      $.fn.show_alert('Success');
    }, "json");
  }
  $(document).ready(function(){
    $('#journal_table').DataTable( {
          "columnDefs": [ {
          "targets"  : 'no-sort',
            "orderable": false,
          }],
                dom: 'Blfrtip',
                buttons: [
               // 'csv'
               {
              extend: 'csv',

             // text: 'CSV' ,
             title:'Day Register Summary'+'(Period : From '+$('#main_from_date').val()+')',
              footer: true,
       exportOptions: { columns: ':visible'},
       customize: function (csv) {
                 return "\t   Day Register Summary\n"+"\t(Period : From "+$("#main_from_date").val()+"    Executive : "+$("#executive_id option:selected").text()+")\n" +  csv ;
               }
            },
                ]
            } );
  });
  $(document).on('change keyup','#main_from_date,#executive_id',function(){
    var from_date=$('#main_from_date').val();
    var executive=$('#executive_id').val();
    if(from_date!='' && executive!='0' ){
      var data_array={
      date:from_date,
      executive:executive
    }
    $.fn.search_transaction_ajax(data_array);
    }
    
  });
  $.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
</script>