
<section class="content-header">
<h1>Customer Due List Route Wise Report</h1>
</section>
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-primary box_tp_brdr">
<div class="row-wrapper">
<div class="row">
<?php echo $this->Form->create('Report',array(
'type'=>'file',
'url' => array('controller' => 'Reports', 'action' => 'DueListPrint')
)) ?>	
<div class="box-body">
<div class="row">
<div class="col-md-12 col-sm-12 col-lg-12">

<!-- <div class="col-md-3 col-lg-3 col-sm-3">
<div class="form-group">
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
<?= $this->Form->input('customer_type',array('class'=>'form-control select2','type'=>'select','empty'=>'select','options'=>$CustomerType_list,'id'=>'customer_type',)); ?>
</div>

</div>
</div> -->
<div class="col-md-2 col-lg-2 col-sm-2">
<div class="form-group">
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
<?= $this->Form->input('route_id',array('class'=>'form-control select2 name','type'=>'select','options'=>$route_list,'id'=>'route_id','label'=>'Route')); ?>
</div>


</div>
</div>
<div class="col-md-2 col-lg-2 col-sm-2">
<div class="form-group">
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
<?= $this->Form->input('customer_group',array('class'=>'form-control select2 name','type'=>'select','empty'=>'select','options'=>$customer_group,'id'=>'customer_group','label'=>'Customer Group')); ?>
</div>


</div>
</div>

<div class="col-md-2 col-lg-2 col-sm-2">
<div class="form-group">
<div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
<?= $this->Form->input('name',array('class'=>'form-control select2 name','type'=>'select','empty'=>'select','options'=>$Customer_list,'id'=>'name','label'=>'Customer')); ?>
</div>


</div>
</div>
 <div class="col-md-3">
                      <div class="col-md-12">
                        <div class="checkbox">
                          <label class="label_chk_bx check_bx_rght"><input type="checkbox" name="data[Report]['check']" id="checkbox" value="" class="chkbx_icn_clk">Show Zero Balance Customers</label>
                        </div>
                       <!--  <button class='btn' type='button' id='collection_report_button'>Fetch</button> -->
                        </div>
                      </div>
<div class="col-md-2">
<div class="col-md-12">
<br>
<button class='btn' type='button' id='route_report_button'>Fetch</button>
</div>
</div>		

</div>
<br/>
</div>
<br/>

<div class="row">
<div class="col-md-6 col-sm-6 col-xs-6 ">
</div>
<div class="col-md-3 col-sm-4 col-xs-4 ">

<!-- 	<span>
<button type='submit' class="print">Due List Print </button></span> -->
<?php echo $this->Form->end(); ?>
</div>
<div class="col-md-3 col-sm-4 col-xs-4">
<!-- <button type="button" id="report_print" class="print"  >Invoice Print</button> -->
</div>
</div>
<br>
<div class="row-wrapper">
<div class="row">
<div class="col-md-12">
<div class="box-body table-responsive no-padding" style="margin-left:10px;margin-right:10px">
<table class="table table-condensed table table boder boder" id="AccountHead_table">
<thead>
<tr class="blue-bg">
<th>Code</th>
<th>Customer</th>
<th>Route</th>	
<th>Group</th>
<th>30 Days</th>
<th>60 Days</th>
<th>90 days</th>
<th>90+ days</th>
<th>PDC</th>
<th>Outstanding Amount</th>
</tr>
</thead>
<tbody>
</tbody>
<tfoot>
</tfoot>
</table>
</div>
</div>
</div>
</div>
<div id="customer_type_modal" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h4 class="modal-title" id="myModalLabel">Customer Type</h4>
</div>
<div class="modal-body">
<div class="form-group">
<label for="inputEmail3" class="col-sm-4 control-label">Customer Type</label>
<div class="col-sm-8">
<input class="form-control" id="customer_type_name" type="text">
</div>
<br>
</div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<button type="button" class="btn btn-primary" id='add_customer_type'>Save</button>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
<script type="text/javascript">
// 
$('#report_print').click(function(){
var customer=$('#name').val();
var customer_type=$('#customer_type').val();

if(customer == ''){
customer=0;
}
if(customer_type == ''){
customer_type=0;
}
url='<?= $this->webroot."Reports/account_recievable_print/"; ?>'+customer_type+'/'+customer;
window.open(url);

});

$('#customer_type').change(function(){
var customer_type=$(this).val();
$.fn.customer_type_change(customer_type);
//$.fn.table_search();
});

$.fn.customer_type_change=function(customer_type){
var data={customer_type:customer_type};
var url_address= '<?php echo $this->webroot; ?>'+'Accountings/customer_type_select_ajax';
$.ajax({
type: "post",  
url:url_address,
data: data,  
success: function(response) {
$('#name').html(response);
},
error:function (XMLHttpRequest, textStatus, errorThrown) {
alert(textStatus);
}
});
}
$.fn.route_type_change=function(route_id){
var data={route_id:route_id};
var url_address= '<?php echo $this->webroot; ?>'+'Accountings/route_customer_select_ajax';
$.ajax({
type: "post",  
url:url_address,
data: data,  
success: function(response) {
$('#name').html(response);
},
error:function (XMLHttpRequest, textStatus, errorThrown) {
alert(textStatus);
}
});
}
$.fn.customer_group_type_change=function(customer_group){
var route_id=$('#route_id').val();
var data={route_id:route_id,customer_group:customer_group};
var url_address= '<?php echo $this->webroot; ?>'+'Accountings/customer_group_select_ajax';
$.ajax({
type: "post",  
url:url_address,
data: data,  
success: function(response) {
$('#name').html(response);
},
error:function (XMLHttpRequest, textStatus, errorThrown) {
alert(textStatus);
}
});
}
$('#route_id').change(function(){
var route_id=$('#route_id').val();
$.fn.route_type_change(route_id);
//$.fn.table_search();
});
$('#name').change(function(){
//$.fn.table_search();
});
$('#customer_group').change(function(){
var customer_group=$('#customer_group').val();
$.fn.customer_group_type_change(customer_group);
//$.fn.table_search();
});
$(document).ready(function(){
$('#AccountHead_table').DataTable( {
"columnDefs": [ {
"targets"  : 'no-sort',
"orderable": false,
}],
dom: 'Blfrtip',
buttons: [
//'print',
{
extend: 'print',
footer: true,
//exportOptions: { columns: ':visible' },
customize: function ( win ) {
$(win.document.body)
.css( 'font-size', '8pt' )
.prepend(
// '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
// '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
// '<h5 align="center">Thiruvangoor</h5',
// '<h5 align="center">Calicut-673 304</h5>',
// '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
'<h3 align="center">Customer Due List Route Wise Report</h3>',
//'<h5>CustomerType :'+$('#customer_type_id').val()+'</h5>',
//'<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+'</h5>'
);
$(win.document.body).find( 'table' )
.addClass( 'compact' )
.css( 'font-size', 'inherit' )
// .prepend(
//   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
//   )
}
},
{
//extend: 'csv',
// title:"Customer Due List Route Wise Report",
//footer: true,
extend: 'csvHtml5',
title:"Customer Due List Route Wise Report",
footer: true,
customize: function (csv) {
return "\t   Customer Due List Route Wise Report\n"+  csv ;
}
// exportOptions: { columns: ':visible' },
//  title:'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
},
]
} );
});
$('#route_report_button').on('click',function(){
$.fn.table_search();
});
$.fn.table_search=function(){
var customer_type=$('#customer_type option:selected').val();
var route_id=$('#route_id').val();
var group_id=$('#customer_group').val();
var name=$('#name option:selected').text();
if($('#checkbox').is(':checked')){
var check_id=0;
}

else

{
var check_id=1;
}
var data={
customer_type:customer_type,
name:name,
route_id:route_id,
group_id:group_id,
check_id:check_id
};
var url_address= '<?php echo $this->webroot; ?>'+'Reports/due_report_ajax';
$.ajax({
type: "post",
url:url_address,
data: data,
dataType:'json',
success: function(response) {
console.log(response);
$('#AccountHead_table').DataTable().destroy();
$('#AccountHead_table tbody').html(response.row);
$('#AccountHead_table tfoot').html(response.tfoot);
$('#AccountHead_table').DataTable({
"columnDefs": [ {
"targets"  : 'no-sort',
"orderable": false,
}],
dom: 'Blfrtip',
buttons: [
//'print',
//'csv'
{
extend: 'print',
footer: true,
//exportOptions: { columns: ':visible' },
customize: function ( win ) {
$(win.document.body)
.css( 'font-size', '10pt' )
.prepend(
// '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
// '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
// '<h5 align="center">Thiruvangoor</h5',
// '<h5 align="center">Calicut-673 304</h5>',
// '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
'<h3 align="center">Customer Due List Route Wise Report</h3>',
//'<h5>CustomerType :'+$('#customer_type_id').val()+'</h5>',
//'<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+'</h5>'
);
$(win.document.body).find( 'table' )
.addClass( 'compact' )
.css( 'font-size', 'inherit' )
// .prepend(
//   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
//   )
}
},
{
// extend: 'csv',
// title:"Customer Due List Route Wise Report",
// footer: true,
// exportOptions: { columns: ':visible' },
//  title:'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
extend: 'csvHtml5',
title:"Customer Due List Route Wise Report",
footer: true,
customize: function (csv) {
return "\t   Customer Due List Route Wise Report\n"+  csv ;
}
},
]
});
$.fn.show_alert('Success');
},
error:function (XMLHttpRequest, textStatus, errorThrown) {
alert(textStatus);
}
});
}
$.fn.show_alert = function(flash)
{
$.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
}
</script>
<script type="text/javascript">
$('#checkbox').click(function(){
$.fn.table_search();
});
</script>