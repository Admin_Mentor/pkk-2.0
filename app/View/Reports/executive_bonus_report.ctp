<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Executive Bonus Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="box-header">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-3">
              <?php echo $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','style'=>'width:100%','class'=>'form-control select_two_class search_field','empty'=>'ALL',)); ?>
            </div>
            <div class="col-md-2" hidden>
              <div class="col-md-12">
                <?php echo $this->Form->input('from_date',array('type'=>'text','id'=>'from_date','class'=>'form-control search_field date_picker datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
            <div class="col-md-2" hidden>
              <div class="col-md-12">
                <?php echo $this->Form->input('to_date',array('type'=>'text','id'=>'to_date','class'=>'form-control  search_field date_picker datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
            <div class="col-md-1"><br>
              <button class='btn btn-success' type='button' id='fetch_button'>Get</button>
            </div>
            <div class="col-md-4 pd-lt-0" hidden> <br>
              <label>Export</label> <span class="print-span"><a id="exportExcel" target="_blank"><i class="fa fa-download fa fa-2x" aria-hidden="true" style="padding-left: 10px;"></i></a></span>
            </div>
          </div>
        </div>
      </div>
      <div class="box-body" style="overflow:auto">
        <table class="table table-condensed" id='table_collection_list' data-page-length="10" border="1">
          <thead>
            <tr class="blue-bg">
              <th>Executive Name</th>
              <th>Bonus Amount</th>
              <th>Bonus Return Amount</th>
              <th>Total Bonus</th>
               <th>Bonus Paid</th>
               <th>Bonus Balance</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <tr>
              <th style="font-size:20px; color:red;text-align:right">Total:</th>
              <th style="font-size:20px; color:red;">0</th>
              <th style="font-size:20px; color:red;">0</th>
              <th style="font-size:20px; color:red;">0</th>
              <th style="font-size:20px; color:red;">0</th>
               <th style="font-size:20px; color:red;">0</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#table_collection_list').dataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/executive_bonus_report_ajax",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
        d.executive_id= $('#executive_id').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "Executive.name" },
    { "data" : "ExecutiveBonusCalculation.bonus_amount" },
    { "data" : "ExecutiveBonusCalculation.bonus_return_amount" },
   { "data" : "ExecutiveBonusCalculation.total_bonus" },
   { "data" : "ExecutiveBonusCalculation.bonus_paid" },
   { "data" : "ExecutiveBonusCalculation.bonus_balance" },
    ],
    "dom": 'Bfrtip',
    "buttons": [
    { extend: 'colvis' },
    {
      extend: 'print',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    // {
    //   extend: 'csv',
    //   title:'Executive Bonus Report of '+$('#executive_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
    //   footer: true,
    //   exportOptions: { columns: ':visible'},
    // },
    {
      extend: 'excel',
      title:'Executive Bonus Report of '+$('#executive_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    {
      extend: 'pdf',
      title:'Executive Bonus Report of '+$('#executive_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    {extend: 'pageLength'},
    ],
    "columnDefs": [
    { className: "text-right", "targets": [ 1 ] },
    { className: "text-right", "targets": [ 2 ] },
    { className: "text-right", "targets": [ 3 ] },
    { className: "text-right", "targets": [ 4 ] },
    { className: "text-right", "targets": [ 5 ] },
    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data; var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
      var pagecount=1;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
      pagecount++;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
      pagecount++;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
       pagecount++;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
       pagecount++;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
    },
  });
  $('#fetch_button').on('click',function(){
    table = $('#table_collection_list').dataTable();
    table.fnDraw();
  });
  $(document).on('click','.ExecutiveBonusDetails_delete',function(){
    if(!confirm("Are you sure?")) { return false; }
    var table_id=$(this).closest('tr').find('td span.ExecutiveBonusDetails_id').text();
    $.post( "<?= $this->webroot ?>Hr/ExecutiveBonusDetails_delete/"+table_id,function( data ) {
      if(data.result!='success') { alert(data.result); return false; }
      table = $('#table_collection_list').dataTable();
      table.fnDraw();
    }, "json");
  });
  $('#exportExcel').on('click',function(e)   { 
    e.preventDefault();
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    var executive_id=$('#executive_id').val();
    var website_url3='<?= $this->webroot; ?>Reports/ExecutiveBonusExport/'+from_date+'/'+to_date;
    $(location).attr("href", website_url3);
  });
</script>