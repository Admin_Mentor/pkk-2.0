
<style type="text/css">
.label_chk_bx {
	font-size: 18px;
	padding-right: 6px;
	padding-top: 26px;
	font-weight: 300;
	letter-spacing: 0.1px;
}
.table_data_padng{
	padding: 25px;
}
</style>

<section class="content-header">
	<h1>Customer Due List Executive Wise Report</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary box_tp_brdr">
				<div class="row-wrapper">
					<div class="row">
						<?php echo $this->Form->create('Report',array(
							'type'=>'file',
							'url' => array('controller' => 'Reports', 'action' => 'ExecutiveDueListPrint')
							)) ?>   
							<div class="box-body">
								<div class="row">
									<div class="col-md-12 col-sm-12 col-lg-12">

										<div class="col-md-3">
											<div class="form-group">
												<div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
													<!-- <?= $this->Form->input('executive_id',array('class'=>'form-control select2','type'=>'select','empty'=>'select','options'=>$Executive_list,'id'=>'executive_id','required'=>'required')); ?> -->
													<?php echo $this->Form->input('executive_id',array('type'=>'select','class'=>'form-control  select_two_class','id'=>'executive_id','style'=>'width: 100%;','required','label'=>'Executive','options'=>$Executive_list)); ?>
												</div>

											</div>

										</div>
										<div class="col-md-2">
											<div class="form-group">
												<div class="col-md-12">
													<?php echo $this->Form->input('from_date',array(
														'type'=>'text',
														'id'=>'from_date',
														'value'=>$final,
														'class'=>'form-control search_field date_picker datepicker',
														'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
														)); ?>
													</div>

												</div>
											</div>
											<!-- <div class="col-md-2">
												<div class="col-md-12">
													<?php echo $this->Form->input('to_date',array(
														'type'=>'text',
														'id'=>'to_date',
														'value'=>$date1,
														'class'=>'form-control  search_field date_picker datepicker',
														'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
														)); ?>
													</div>
												</div> -->
              <div class="col-md-2">
                      <div class="col-md-12">
                        <br>
                        <button class='btn' type='button' id='collection_report_button'>Fetch</button>
                      </div>
                    </div>
												<div class="col-md-3 col-lg-3 col-sm-3" hidden="">

													<div class="checkbox">
														<label class="label_chk_bx"><input type="checkbox" name="data[Report]['check']" id="checkbox" value="">Show Zero Balance Customers</label>
													</div>

												</div> 
											</div>

										</div>
										<br/>
									</div>
									<br/>       
									<div class="row">
										<div class="col-md-6 col-sm-6 col-xs-6 ">
										</div>
										<div class="col-md-3 col-sm-4 col-xs-4 ">

											<!--  <span> <button type='submit' class="print">Due List Print </button></span> -->
											<?php echo $this->Form->end(); ?>
										</div>
										<div class="col-md-3 col-sm-4 col-xs-4">
											<!-- <button type="button" id="report_print" class="print"  >Invoice Print</button> -->
										</div>
									</div>
									<br>
									<div class="test">
										<div class="row">
											<div class="col-md-12">
												<div class="box-body table-responsive table_data_padng">
													<table class="table table-condensed table table boder boder" id="AccountHead_table">
														<thead>
															<tr class="blue-bg">
																<th>Executive</th>  
																<th>Customer</th> 
																<th>Invoice Number</th>
																<th>Invoice Date</th>
																<th>Outstanding Amount</th>
															</tr>
														</thead>
														<tbody>
														</tbody>
														<tfoot>
														</tfoot>
													</table>
												</div>
											</div>
										</div>
									</div>
									<div id="customer_type_modal" class="modal fade" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													<h4 class="modal-title" id="myModalLabel">Customer Type</h4>
												</div>
												<div class="modal-body">
													<div class="form-group">
														<label for="inputEmail3" class="col-sm-4 control-label">Customer Type</label>
														<div class="col-sm-8">
															<input class="form-control" id="customer_type_name" type="text">
														</div>
														<br>
													</div>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
													<button type="button" class="btn btn-primary" id='add_customer_type'>Save</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<script type="text/javascript">

// $('.name').on('keyup change',function(){
//  $(this).val($(this).val().trim());
//  $('#name').val($(this).val());
//  $('#name_modal').val($(this).val());
// });
$('.opening_balance').on('keyup change',function(){
	$('#opening_balance').val($(this).val());
	$('#opening_balance_modal').val($(this).val());
});
$(document).on('change','#customer_type,#modal_customer_type',function(){
	$('#modal_customer_type').val($(this).val()).trigger('change.select2');
	$('#customer_type').val($(this).val()).trigger('change.select2');
});
</script>

<!-- For delete -->

<script type="text/javascript">
	$('#report_print').click(function(){


		var customer=$('#name').val();
		var customer_type=$('#customer_type').val();

		if(customer == ''){
			customer=0;
		}
		if(customer_type == ''){
			customer_type=0;
		}
		url='<?= $this->webroot."Reports/account_recievable_print/"; ?>'+customer_type+'/'+customer;
		window.open(url);

	});



	// $('#executive_id').change(function(){
	// 	$.fn.table_search();
	// });

	$('#checkbox').click(function(){
		$.fn.table_search();
	});

	$.fn.customer_type_change=function(customer_type){
		var data={customer_type:customer_type};
		var url_address= '<?php echo $this->webroot; ?>'+'Accountings/customer_type_select_ajax';
		$.ajax({
			type: "post",  
			url:url_address,
			data: data,  
			success: function(response) {
				$('#name').html(response);
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}
	$.fn.route_type_change=function(route_id){
		var data={route_id:route_id};
		var url_address= '<?php echo $this->webroot; ?>'+'Accountings/route_customer_select_ajax';
		$.ajax({
			type: "post",  
			url:url_address,
			data: data,  
			success: function(response) {
				$('#name').html(response);
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}
	$.fn.customer_group_type_change=function(customer_group){
		var route_id=$('#route_id').val();
		var data={route_id:route_id,customer_group:customer_group};
		var url_address= '<?php echo $this->webroot; ?>'+'Accountings/customer_group_select_ajax';
		$.ajax({
			type: "post",  
			url:url_address,
			data: data,  
			success: function(response) {
				$('#name').html(response);
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}
	$(document).ready(function(){
		$('#AccountHead_table').DataTable( {
			"columnDefs": [ {
				"targets"  : 'no-sort',
				"orderable": false,
			}],
			dom: 'Blfrtip',
			// buttons: [
			// 'print','csv'
			// ]
			 buttons: [
        
        {
          extend: 'print',
          footer: true,
       exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                    '<h3 align="center">Customer Due List Executive Wise Report</h3>',
                    '<h5>Period : From '+$('#from_date').val()+'</h5>'
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
              }
            },
            {
              extend: 'csv',
              footer: true,
       exportOptions: { columns: ':visible'},
       title:'Customer Due List Executive Wise Report'+'(Period : From '+$('#from_date').val()+')',
        customize: function (csv) {
                 return "\tCustomer Due List Executive Wise Report\n"+"\t(Period : From "+$("#from_date").val()+")\n"+  csv ;
               },
            },
            {
              extend: 'excel',
              footer: true,
       exportOptions: { columns: ':visible'},
       title:'Customer Due List Executive Wise Report'+'(Period : From '+$('#from_date').val()+')',
            },
       //      {
       //        extend: 'pdf',
       //        footer: true,
       // exportOptions: { columns: ':visible'},
       // title:'Customer Due List Executive Wise Report'+'(Period : From '+$('#from_date').val()+')',
       //      },
            {
               extend: 'pageLength',
             },
            ]
		} );
	});
	$('#route_id').change(function(){
		var route_id=$('#route_id').val();
		$.fn.route_type_change(route_id);
		$.fn.table_search();
	});
	$('#name').change(function(){
		$.fn.table_search();
	});
	$('#customer_group').change(function(){
		var customer_group=$('#customer_group').val();
		$.fn.customer_group_type_change(customer_group);
		$.fn.table_search();
	});
  $('#collection_report_button').on('click',function(){
        $.fn.table_search();
      });
	$.fn.table_search=function(){
		var executive_id=$('#executive_id').val();
		var from_date=$('#from_date').val();
		var to_date=$('#to_date').val();
		if($('#checkbox').is(':checked')){
			var check_id=0;
		}
		else{
			var check_id=1;
		}

        //var check_id=$('#toggle_button_for_gst_visibility').val();
        var data={
        	executive_id:executive_id,check_id:check_id,from_date:from_date,
        	to_date:to_date,
        };
       // console.log(data);
       // if(executive_id!=""){
       var url_address= '<?php echo $this->webroot; ?>'+'Reports/executive_due_report_ajax';
       $.ajax({
       	type: "post",
       	url:url_address,
       	data: data,
       	dataType:'json',
       	success: function(response) {
       		$('#AccountHead_table').DataTable().destroy();
       		$('#AccountHead_table tbody').empty();
       		$('#AccountHead_table tfoot').empty();
       		 var total_invoice=0;
       var total_balance=0;
      $.each(response.data,function(key,value){
        for (var i = 0; i<value.balance.length; i++) {
        var tr="<tr>";
        tr+="<td>"+value.Executive_name+"</td>";
        tr+="<td>"+value.party_name+"</td>";
        tr+="<td>"+value.invoice_no[i]+"</td>";
        tr+="<td>"+value.delivered_date[i]+"</td>";
        tr+="<td>"+parseFloat(value.balance[i]).toFixed(2)+"</td>";
          total_balance+=parseFloat(value.balance[i]);
        tr+="</tr>";
        $('#AccountHead_table tbody').append(tr);
        }
      });
        var tm="<tr>";
        tm+="<td></td>";
        tm+="<td></td>";
        tm+="<td></td>";
        tm+="<td><h4><b>Total</b></h4></td>";
        tm+="<td><h4><b>"+Math.round(total_balance)+"</h4></b</td>";
        tm+="</tr>";
         $('#AccountHead_table tfoot').append(tm);
       		$('#AccountHead_table').DataTable( {
       			

        "ordering": false,
        dom: 'Bfrtip',
        buttons: [
        
        {
          extend: 'print',
          footer: true,
       exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                    '<h3 align="center">Customer Due List Executive Wise Report</h3>',
                    '<h5>Period : From '+$('#from_date').val()+'</h5>'
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
              }
            },
            {
              extend: 'csv',
              footer: true,
       exportOptions: { columns: ':visible'},
       title:'Customer Due List Executive Wise Report'+'(Period : From '+$('#from_date').val()+')',
        customize: function (csv) {
                 return "\tCustomer Due List Executive Wise Report\n"+"\t(Period : From "+$("#from_date").val()+")\n"+  csv ;
               },
            },
            {
              extend: 'excel',
              footer: true,
       exportOptions: { columns: ':visible'},
       title:'Customer Due List Executive Wise Report'+'(Period : From '+$('#from_date').val()+')',
            },
       //      {
       //        extend: 'pdf',
       //        footer: true,
       // exportOptions: { columns: ':visible'},
       // title:'Customer Due List Executive Wise Report'+'(Period : From '+$('#from_date').val()+')',
       //      },
            {
               extend: 'pageLength',
             },
            ]
            
          
       		} );
       		$.fn.show_alert('Success');
       	},
       	error:function (XMLHttpRequest, textStatus, errorThrown) {
       		alert(textStatus);
       	}
       });
   // }
      }

     </script>
     <script type="text/javascript">

      $.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
     </script>