<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Executive Wise Block Amount Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="box-header">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-3">
              <?php echo $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','style'=>'width:100%','empty'=>'All','class'=>'form-control select_two_class search_field')); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="box-body" style="overflow:auto">
        <table class="table table-condensed" id='table_block_amount' data-page-length="10" border="1">
          <thead>
            <tr class="blue-bg">
              <th>Executive</th>
              <th>Is Block</th>
              <th>Block Amount</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <tr>
              <th></th>
              <th style="font-size:20px; color:red;text-align:right">Total:</th>
              <th style="font-size:20px; color:red;">0</th>
               <th style="font-size:20px; color:red;">0</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#table_block_amount').dataTable( {
    "processing": true,
    "serverSide": true,
    "paging":false,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/ExecutiveWiseBlockAmountReport_ajax",
      "type": "POST",
      data:function( d ) {
        d.executive_id= $('#executive_id').val();
      },
      "dataSrc": "records",
      lengthMenu: [
    [10,25, 50,100],
    ['10 rows, 25 rows', '50 rows','100 rows']
    ],
    },
    "columns": [
    { "data" : "Executive.name" },
    { "data" : "Executive.is_block" },
    { "data" : "Executive.block_amount",className:"text-right" },
    { "data" : "Executive.amount",className:"text-right" },
    ],
    "createdRow": function( row, data, dataIndex){
        //$(row).css('background-color','#'+data.Complaint.priority);
        $(row).css('background-color',data.Executive.colour);
      },
    "dom": 'Bfrtip',
    "buttons": [
    {
      extend: 'print',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    {
      extend: 'excel',
      title:'Executive wise Block Amount Report of '+$('#executive_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
   {extend: 'pageLength'},
    ],
    "columnDefs": [
    {"targets": [ 0 ],"className": 'executive_id' ,},
    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data; var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
      var pagecount=2;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
       var pagecount=3;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
    },
  });
  $('.search_field').on('change',function(){
    table = $('#table_block_amount').dataTable();
    table.fnDraw();
  });
</script>