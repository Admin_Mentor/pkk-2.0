<section class="content-header">
  <h1>Monthly Expense Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-2">
                  <?= $this->Form->input('from_date',array('type'=>'text','id'=>'from_date','class'=>'form-control date_field datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
                </div>
                <div class="col-md-2">
                  <?= $this->Form->input('to_date',array('type'=>'text','id'=>'to_date','class'=>'form-control date_field datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
                </div>
               <div class="col-md-2">
                          <br>
                          <label class="label_chk_bx check_bx_rght"><input type="checkbox" name="data[Report]['check']" id="checkbox" value="" class="chkbx_icn_clk" >Zero Expense</label>
                        </div>
                        
                <div class="col-md-2"><br>
                  <button class="btn btn-success" id='get_button'>Fetch</button>
                </div>
              </div>
            </div><br><br><br><br>
            <div class="box-body">
              <table class="table table-condensed table boder" id='table_data'>
                <thead>
                  <tr class="blue-bg">
                    <th>SubCategory</th>
                    <th>AccountHead</th>
                    <th>Total Expense</th>
                    <th>Total Paid</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                  <tr>
                    <th colspan="2" style="font-size:20px; color:red;text-align:right">Total:</th>
                    <th style="font-size:20px; color:red;"></th>
                    <th style="font-size:20px; color:red;"></th>
                  </tr>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
 
  $('#table_data').DataTable( {
    "processing": false,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/ExpenseReport_ajax",
      "type": "POST",data:function( d ) {
         if($('#checkbox').is(':checked')){
          var check_id=0;
        }

        else

        {
          var check_id=1;
        }
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
        d.check_id=check_id;
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "SubGroup.name" },
    { "data" : "AccountHead.name" },
    { "data" : "AccountHead.exepense" },
    { "data" : "AccountHead.paid" },
    ],

    rowCallback: function (row, data) {
      $.fn.show_alert('Success');
    },
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;
      var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
        i : 0;
      };
      pageTotal = api.column( 2, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 2 ).footer() ).html(''+pageTotal+'');
      pageTotal = api.column( 3, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 3 ).footer() ).html(''+pageTotal+'');
    },
    "columnDefs": [
    ],
     "dom": 'Bfrtip',
        lengthMenu: [
            [-1, 10, 25, 50],
            ['Show all' ,'10 rows', '25 rows', '50 rows' ]
        ],
        buttons: [
            'pageLength',
            {
      extend: 'csv',
      text: 'CSV' ,
      title:'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
       customize: function (csv) {
                 return "\tMonthly Expense Report\n"+"\t(Period : From "+$("#from_date").val()+"    To : "+$("#to_date").val()+")\n"+  csv ;
               },
    },
    {
      extend: 'print',
      text: 'Print' ,
      //title: 'Monthly Expense Report',
      customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                    '<h3 align="center">Monthly Expense Report</h3>',
                    '<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+'</h5>'
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
              }
    },
        ]


  });
  $('#get_button').click(function(){
    table = $('#table_data').dataTable();

    table.fnDraw();

  });
  $.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
</script>