<section class="content-header">
  <h1>GST Report</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header"> 
    </div>
    <div class="box-body"> 
      <?= $this->Form->create('GSTReport'); ?>
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="col-md-6">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-sm-12 col-md-12">
                  <?php echo $this->Form->input('type_id',array('type'=>'select','empty' =>'All','options'=>array('1'=>'Sale','2'=>'Purchase',),'style'=>'width: 100%;','class'=>'form-control select2 select2-hidden-accessible','label'=>'Type')) ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-sm-12 col-md-12">
                  <?php echo $this->Form->input('type_id',array('type'=>'select','empty' =>'All','options'=>$GST_RATE,'style'=>'width: 100%;','class'=>'form-control select2 select2-hidden-accessible','label'=>'GST')) ?>
                </div>
              </div>
            </div>
          </div>
        </div> 
        <div class="col-md-2 col-sm-2">
          <div class="form-horizontal">
            <div class="form-group">
              <div class="col-sm-12 col-md-12">
                <?php echo $this->Form->input('product_type_id',array('id'=>'product_type_id','type'=>'select','empty' =>'All','options'=>$ProductType,'style'=>'width: 100%;','class'=>'form-control select2 Search select2-hidden-accessible','label'=>'Product Type')) ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="form-horizontal">
            <div class="form-group">
              <div class="col-sm-12 col-md-12">
                <?php echo $this->Form->input('product_id',array('id'=>'product_id','type'=>'select','empty' =>'All','options'=>$Product,'style'=>'width: 100%;','class'=>'form-control select2 Search select2-hidden-accessible','label'=>'Product')) ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="col-md-6">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-sm-12 col-md-12">
                  <?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker Search datepicker','id'=>'from_date','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-sm-12 col-md-12">
                  <?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker Search datepicker','id'=>'to_date','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-1 col-sm-1 pull-right mar-tp-5">
          <br>
          <!-- <button class="print" data-toggle="modal" data-target="#print">Print</button> -->
        </div>
      </div>
    </div>
    <?= $this->Form->end(); ?>
    <div class="row">
      <div class="col-md-12">
        <div class="box-body table-responsive no-padding">
          <table class="table datatable table-hover boder text-center table-bordered" id='GSTReport_table' >
            <thead>
              <tr class="blue-bg">
                <th rowspan='2'>Date</th>
                <th rowspan='2'>Type</th>
                <th rowspan='2'>Product Name</th>
                <th rowspan='2'>HSN</th>
                <th rowspan='2'>Inv.No</th>
                <th rowspan='2'>Taxable Value</th>
                <th colspan='3'>CGST</th>
                <th colspan='3'>SGST</th>
                <th colspan='3'>IGST</th>
                <th colspan='2'>GST Total</th>
                <th rowspan='2'>BALANCE</th>
              </tr>
              <tr class="blue-bg">
                <th>Rate %</th>
                <th>In</th>
                <th>Out</th>
                <th>Rate %</th>
                <th>In</th>
                <th>Out</th>
                <th>Rate %</th>
                <th>In</th>
                <th>Out</th>
                <th>In</th>
                <th>Out</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($GSTReport['list'] as $key => $value) :?>
                <tr>
                  <td style='border: "1"'><?= $value['date']; ?></td>
                  <td><?= $value['type']; ?></td>
                  <td><?= $value['product']; ?></td>
                  <td><?= $value['hsn']; ?></td>
                  <td><?= $value['invoice_no']; ?></td>
                  <td><?= $value['taxable_value']; ?></td>
                  <td><?= $value['cgst']['rate_percentage']; ?></td>
                  <td><?= $value['cgst']['in']; ?></td>
                  <td><?= $value['cgst']['out']; ?></td>
                  <td><?= $value['sgst']['rate_percentage']; ?></td>
                  <td><?= $value['sgst']['in']; ?></td>
                  <td><?= $value['sgst']['out']; ?></td>
                  <td><?= $value['igst']['rate_percentage']; ?></td>
                  <td><?= $value['igst']['in']; ?></td>
                  <td><?= $value['igst']['out']; ?></td>
                  <td><?= $value['total']['in']; ?></td>
                  <td><?= $value['total']['out']; ?></td>
                  <td></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
            <tfoot>
              <td colspan="5"></td>
              <td><h4><b>Total</b></h4></td>
              <td></td>
              <td><h4><b><?= $GSTReport['total']['cgst']['in']; ?></b></h4></td>
              <td><h4><b><?= $GSTReport['total']['cgst']['out']; ?></b></h4></td>
              <td></td>
              <td><h4><b><?= $GSTReport['total']['sgst']['in']; ?></b></h4></td>
              <td><h4><b><?= $GSTReport['total']['sgst']['out']; ?></b></h4></td>
              <td></td>
              <td><h4><b><?= $GSTReport['total']['igst']['in']; ?></b></h4></td>
              <td><h4><b><?= $GSTReport['total']['igst']['out']; ?></b></h4></td>
              <td><h4><b><?= $GSTReport['total']['total']['in']; ?></b></h4></td>
              <td><h4><b><?= $GSTReport['total']['total']['out']; ?></b></h4></td>
              <td><h4><b><?= $GSTReport['total']['total']['out']-$GSTReport['total']['total']['in']; ?></b></h4></td>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#product_type_id').change(function(){
    var product_type_id=$('#product_type_id').val();
    var data={
      product_type:product_type_id,
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Product/product_type_select_ajax';
    $.ajax({
      type: "post",  
      url:url_address,
      data: data,  
      dataType:'json',
      success: function(response) {
        $('#product_id').html(response.option);
        $('#product_id').val('').change();
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  });
  $('.Search').change(function(){
    $.fn.Search();
  });
  $.fn.Search=function(){
    var product_type_id=$('#product_type_id').val();
    var product_id=$('#product_id').val();
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    var data={
      product_type_id:product_type_id,
      product_id:product_id,
      from_date:from_date,
      to_date:to_date,
    };
    $.post( "<?= $this->webroot ?>Reports/GSTReport_ajax",data ,function( data ) {
    }, "json");
  }
  $('.main-sidebar').ready(function(){
    $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
  });
</script>