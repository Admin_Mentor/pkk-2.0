$.fn.search_transaction_ajax=function(data_array){
  $.post( "<?= $this->webroot ?>Accountings/general_journal_transaction_ajax",data_array ,function( data ) {
    $('#transaction_details_table').DataTable().destroy();
    $('#transaction_details_table tbody').empty();
    $('#transaction_details_table tfoot').empty();
    $('#transaction_details_table tbody').html(data.row.tbody);
    $('#transaction_details_table tfoot').html(data.row.tfoot);
    $('#transaction_details_table').DataTable({
      "bSort": false,
      "paging": false,
      dom: 'Bfrtip',
      buttons: [
      {
       extend: 'print',
       footer: true,
       exportOptions: { columns: [0,1,2,3,4,5,6,7] },
       customize: function ( win ) {
        $(win.document.body)
        .css( 'font-size', '10pt' )
        .prepend( '<tr><td colspan="8"><h4>'+$('#account_holder_name').text()+'</h4></td></tr>' )
        .prepend( '<h5>Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+'</h5>' );
        $(win.document.body).find( 'table' )
        .addClass( 'compact' )
        .css( 'font-size', 'inherit' )
      }
    },
    {
     extend: 'csv',
     footer: true,
     exportOptions: {
      columns: [0,1,2,3,4,5,6,7]
    },
  },
  {
   extend: 'excel',
   footer: true,
   exportOptions: { columns: [0,1,2,3,4,5,6,7]
   },
   customize: function (xlsx) {
    var sheet = xlsx.xl.worksheets['sheet1.xml'];
    var numrows = 5;
    var clR = $('row', sheet);
    clR.each(function () {
      var attr = $(this).attr('r');
      var ind = parseInt(attr);
      ind = ind + numrows;
      $(this).attr("r",ind);
    });
    $('row c ', sheet).each(function () {
      var attr = $(this).attr('r');
      var pre = attr.substring(0, 1);
      var ind = parseInt(attr.substring(1, attr.length));
      ind = ind + numrows;
      $(this).attr("r", pre + ind);
    });
    function Addrow(index,data) {
      msg='<row r="'+index+'">'
      for(i=0;i<data.length;i++){
        var key=data[i].key;
        var value=data[i].value;
        msg += '<c t="inlineStr" r="' + key + index + '">';
        msg += '<is>';
        msg +=  '<t>'+value+'</t>';
        msg+=  '</is>';
        msg+='</c>';
      }
      msg += '</row>';
      return msg;
    }
    var r1 = Addrow(1, [{ key: 'A', value: '' }]);
    var r2 = Addrow(2, [{ key: 'A', value: 'Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+'' }]);
    var r3 = Addrow(3, [{ key: 'A', value: '' }]);
    var r4 = Addrow(4, [{ key: 'A', value: ''+$('#account_holder_name').text()+'' }]);
    var r5 = Addrow(5, [{ key: 'A', value: '' }]);
    sheet.childNodes[0].childNodes[1].innerHTML = r1 + r2+ r3+ r4+ r5+ sheet.childNodes[0].childNodes[1].innerHTML;
  }
},
],
"columnDefs": [
{"width": "20%",   "targets": 0},
{"width": "30%",   "targets": 1},
{"width": "30%",   "targets": 4}
],
});
  }, "json");
};
$(document).on('click','.view_transaction',function(){
	var name=$(this).closest('tr').find('td.name').text();
	var from_date=$('#from_date').val();
	var to_date=$('#to_date').val();
	$('#account_holder_name').text(name);
	var data_array={
		name:name,from_date:from_date,to_date:to_date
	}
	$.fn.search_transaction_ajax(data_array);
	$('#view_transaction_modal').modal('show');
});
$(document).on('click','#fetch_button_journal',function(){
  var from_date=$('#modal_from_date').val();
  var to_date=$('#modal_to_date').val();
  var name=$('#account_holder_name').text();
  var data_array={
    name:name,from_date:from_date,to_date:to_date
  }
  $.fn.search_transaction_ajax(data_array);
});
$(document).on('click','.name',function(){
	var name=$(this).text();
	var from_date=$('#modal_from_date').val();
	var to_date=$('#modal_to_date').val();
	$('#account_holder_name').text(name);
	var data_array={
		name:name,from_date:from_date,to_date:to_date
	}
	$.fn.search_transaction_ajax(data_array);
	$('#view_transaction_modal').modal('show');
});
$(document).on('click','td.voucher_no',function(){
  var voucher_no=$(this).closest('tr').find('td.voucher_no').text();
  var name=$('#account_holder_name').text();
  $.get( "<?= $this->webroot ?>Accountings/general_journal_transaction_by_voucher_no_ajax/"+voucher_no+'/'+name ,function( data ) {
    $('#transaction_details_voucher_table').DataTable().destroy();
    $('#transaction_details_voucher_table tbody').empty();
    $('#transaction_details_voucher_table tfoot').empty();
    $('#transaction_details_voucher_table tbody').html(data.row.tbody);
    $('#transaction_details_voucher_table tfoot').html(data.row.tfoot);
    $('#transaction_details_voucher_table').DataTable({
      "columnDefs": [
      { "width": "50px",  "targets": 0 },
      { "width": "100px", "targets": 1 },
      { "width": "100px", "targets": 2 },
      { "width": "40px",  "targets": 3 },
      { "width": "40px",  "targets": 4 },
      { "width": "150px", "targets": 5 },
      { "width": "100px", "targets": 6 }
      ],
    });
    $('#view_transaction_by_voucher_no_modal').modal('show');
  }, "json");
});
