<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1> Invoice Wise Margin Report </h1>
  <!-- ///***///--> 
</section>
<!-- Main content -->
<section class="content">
  <?= $this->Form->create('InvoiceMargin', array('url' => array('controller' => 'Reports', 'action' => 'InvoiceMargin')));?>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="Stockmanagement">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-2">
                <div class="col-md-12">
                  <?= $this->Form->input('from_date',array('type'=>'text', 'id'=>'from_date', 'value'=>$final, 'class'=>'form-control cls_label_all date_field search_field date_picker datepicker', 'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask', )); ?>
                </div> 
              </div>
              <div class="col-md-2">
                <div class="col-md-12">
                  <?= $this->Form->input('to_date',array('type'=>'text', 'id'=>'to_date', 'value'=>$date1, 'class'=>'form-control cls_label_all date_field search_field date_picker datepicker', 'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask', )); ?>
                </div>
              </div>
              <div class="col-md-2">
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <br>
                  <input type="submit"  class="print" id="getdata" value="Get"></a>
                </div> 
              </div>
              <div class="col-md-2">
                <div class="col-md-12 col-lg-12 col-sm-12">
                  <br>
                  <input type="button"  class="save" name="button" id="exportExcel" value="Export"></a>
                </div> 
              </div>
            </div>
          </div>
          <br/><br/>
          <div class="box-body">
            <div class="col-md-4 col-xs-12">
              <h3 class="muted "></h3>
            </div>
            <table class="table table-hover boder text-center table-bordered" id='table_sales_margin_list' data-order='[[ 1, "asc" ]]'>
              <thead>
                <tr class="blue-bg">
                  <th>Invoice Date</th>
                  <th>Invoice No</th>
                  <th>Sales Price</th>
                  <th>Sales Cost</th>
                  <th>Margin</th> 
                </tr>
              </thead>
              <tbody>
                <?php $amount=0;$cost=0;$margin=0;
                foreach ($list_array as $key => $value) : ?>
                <?php foreach ($value['single_invoice'] as $key => $value_single) : ?>
                  <tr>
                    <td><?= $value_single['invoice_date']; ?></td>
                    <td><?= $value_single['invoice_no']; ?></td>
                    <td><?= $value_single['amount']; ?></td> 
                    <td><?= $value_single['cost']; ?></td> 
                    <td><?= $value_single['margin'];
                      $margin+=$value_single['margin'];
                      $amount+=$value_single['amount'];
                      $cost+=$value_single['cost']; ?></td> 
                    </tr>
                  <?php endforeach; ?>  
                <?php endforeach; ?>  
              </tbody>
              <tfoot>
                <tr>
                  <td></td>
                  <td>Total</td>
                  <td><?= $amount; ?></td> 
                  <td><?= $cost; ?></td> 
                  <td><?= $margin; ?></td> 
                </tr>
              </tfoot>
            </table>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<?= $this->Form->end(); ?>
</section>
<script type="text/javascript">
  $(document).ready(function(){
   // $('#table_sales_margin_list').DataTable();
   $('#table_sales_margin_list').find('.toggle_rows').hide();
   $('#table_sales_margin_list').DataTable({
    dom: 'Bfrtip',
    lengthMenu: [
    [ 10, 25, 50, -1 ], [ '10 rows', '25 rows', '50 rows', 'Show all' ]
    ],
    buttons: [
      'pageLength',
    {
      extend: 'colvis',
    },
    {
     extend: 'print',
     footer: true,
     exportOptions: { columns: ':visible' },
     customize: function ( win ) {
      $(win.document.body)
      .css( 'font-size', '10pt' )
      .prepend(
                    // '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
                    // '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
                    // '<h5 align="center">Thiruvangoor</h5',
                    // '<h5 align="center">Calicut-673 304</h5>',
                    // '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
                    '<h3 align="center">Invoice Wise Margin Report</h3>',
            //'<h5>CustomerType :'+$('#customer_type_id').val()+'</h5>',
            '<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+'</h5>'
            );
      $(win.document.body).find( 'table' )
      .addClass( 'compact' )
      .css( 'font-size', 'inherit' )
                // .prepend(
                //   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
                //   )
              }
            },
            {
             extend: 'csv',
             footer: true,
             exportOptions: { columns: ':visible' },
             title:'Invoice Wise Margin Report'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
             customize: function (csv) {
               return "\tInvoice Wise Margin Report\n"+"\t(Period : From "+$("#from_date").val()+"    To : "+$("#to_date").val()+")\n"+  csv ;
             },
           },
           {
             extend: 'excel',
             footer: true,
             exportOptions: { columns: ':visible' },
             title:'Invoice Wise Margin Report'+'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
           },
           {
             extend: 'pageLength',
           },
           ],
       // "columnDefs": [{"targets": [ 0 ], "visible": false,"searchable": false}]
     });
   $('#exportExcel').click(function()   { 
    var date_from= $('#from_date').val();
    var date_to=$('#to_date').val();
    if(!date_from)
    {
      alert("Take From Date");
      return false;
    }
    if(!date_to)
    {
      alert("Take To Date");
      return false;
    }
    var website_url3='<?php echo $this->webroot; ?>Reports/print_invoice_margin_report/'+date_from+'/'+date_to;
    $(location).attr("href", website_url3);
  });
 });

</script>