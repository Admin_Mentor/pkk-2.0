<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<style type="text/css">

.cls_label_all {
  padding-top: 5%;
}
.row_top_row{

  margin-top: 5%;
}
.deaf_btn_btn {
  margin-left: 4%;
}

.row_new_add{
  margin-top: 2%;
}
#radio_butto_add {
  margin-top: 7px;
  margin-left: -51%;
}
.check_bx_rght{
  white-space: nowrap;
  padding-top: 20px;
}
.chkbx_icn_clk {
    position: absolute;
    margin-left: -16px !important;
}
</style>

<section class="content-header">
  <h1>Non Buying Customer Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="Stockmanagement">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                <div class="row" style="margin-top: 2%;">
                  
                  <div class="col-md-2">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('route_id',array(
                      'type'=>'select',
                      'id'=>'route_id',
                      'style'=>'width:100%',
                      'options'=>$route_list,
                      'class'=>'form-control select_two_class search_field',
                      'empty'=>'ALL',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('from_date',array(
                      'type'=>'text',
                      'id'=>'from_date',
                      'class'=>'form-control search_field date_picker datepicker',
                      'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12">
                      <?php echo $this->Form->input('to_date',array(
                        'type'=>'text',
                        'id'=>'to_date',
                        'class'=>'form-control  search_field date_picker datepicker',
                        'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                        )); ?>
                      </div>
                    </div>
                    <div class="col-md-2">
                    <div class="col-md-12">
                     <?= $this->Form->input('Amount',array('class'=>'form-control','type'=>'text','required','id'=>'Amount','required','value'=>1000)); ?>

                      </div>
                    </div>
                    <div class="col-md-2">
                      <div class="col-md-12">
                        <br>
                        <button class='btn btn-success' type='button' id='nosale_report_button'>Get</button>
                      </div>
                    </div>
                     
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="col-md-4 col-xs-12">
                      <h3 class="muted "></h3>
                    </div>
                    <table style="width: 100% !important" class="table table-hover boder table-bordered"  data-order='[[ 2, "desc" ]]' id='table_nosale_list'>
                      <thead>
                        <tr class="blue-bg">
                          <!-- <th>Executive</th> -->
                           <th>Code</th>
                          <th>Customer</th>
                          <th>Sale Amount</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <script type="text/javascript">
      $('#table_nosale_list').DataTable( {
  "processing": true,
  "serverSide": true,
  "ajax": {
    "url": "<?= $this->webroot ?>Reports/nosale_report_ajax",
    "type": "POST",
    data:function( d ) {
      d.from_date= $('#from_date').val();
      d.to_date= $('#to_date').val();
       d.route_id=$('#route_id').val();
        d.Amount=$('#Amount').val();
      },
      "dataSrc": "records",
    },
      dom: 'Bfrtip',
    lengthMenu: [
   [10, 50,100,200],
    ['10 rows', '50 rows','100 rows','200 rows' ]
    ],
    buttons: [
    { extend: 'colvis', },
    { extend: 'excel', title: 'Non Buying Customer Report', exportOptions: { columns: ':visible' } }, 
   // { extend: 'csv',   title: 'Bill Wise Report', exportOptions: { columns: ':visible' } },
    'pageLength',
    ],
    "columns": [
     { "data" : "Customer.code" },
    { "data" : "AccountHead.name" },
    { "data" : "Sale.amount" ,className:"text-right"},
    ],
   "footerCallback": function ( row, data, start, end, display ) {},
    "columnDefs": [],
  });
$('#nosale_report_button').click(function(){
  table = $('#table_nosale_list').dataTable();
  table.fnDraw();
});
</script>
