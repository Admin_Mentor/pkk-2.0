<section class="content-header">
	<h1>Party Due List Report</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary box_tp_brdr">
				<div class="row-wrapper">
					<div class="row">
						<?php echo $this->Form->create('Report',array(
							'type'=>'file',
							'url' => array('controller' => 'Reports', 'action' => 'PartyDueListPrint')
							)) ?>	
							<div class="box-body">
								<div class="row">
								<div class="col-md-3 col-lg-3 col-sm-3">
											<div class="form-group">
												<div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
													<?= $this->Form->input('name',array('class'=>'form-control select2','type'=>'select','empty'=>'select','options'=>$Party_list,'id'=>'name','label'=>'Party')); ?>
												</div>

											</div>
										</div>
									
									<br/>
								</div>
								<br/>		
								<div class="row">
									<div class="col-md-6 col-sm-6 col-xs-6 ">
									</div>
									<div class="col-md-3 col-sm-4 col-xs-4 ">

										<!-- <span> <button type='submit' class="print">Due List Print </button></span> -->
										<?php echo $this->Form->end(); ?>
									</div>
									<div class="col-md-3 col-sm-4 col-xs-4">
										<!-- <button type="button" id="report_print" class="print"  >Invoice Print</button> -->
									</div>
								</div>
								<br>
								<div class="row-wrapper">
									<div class="row" style="margin-left: 5px;margin-right: 5px">
										<div class="col-md-12">
											<div class="box-body table-responsive no-padding">
												<table class="table table-condensed table table table-bordered" id="AccountHead_table">
													<thead>
														<tr class="blue-bg">
															
															<th>Party</th>
															<th>Address</th>
															<th class="text-right">Outstanding Amount</th>
														</tr>
													</thead>
													<tbody>
														<?php foreach ($new_Party_array as $key => $value): ?>
															<tr class="blue-pddng">
																
																<td><span style="display:none" class='AccountHead_id'><?= $value['Party']['AccountHead']['id']; ?></span><span><?= $value['Party']['AccountHead']['name']; ?></span></td>
																<td><?= substr($value['Party']['Party']['place'],0,80); ?></td>
																<td style="text-align:right"><?= $value['Balance_Total']; ?></td>
															</tr>
														<?php endforeach ?>
													</tbody>
												</table>
											</div>
										</div>
									</div>
								</div>
							
							</div>
						</div>
					</div>
				</div>
			</section>
	<script type="text/javascript">


$('.opening_balance').on('keyup change',function(){
	$('#opening_balance').val($(this).val());
	$('#opening_balance_modal').val($(this).val());
});
$(document).on('change','#customer_type,#modal_customer_type',function(){
	$('#modal_customer_type').val($(this).val()).trigger('change.select2');
	$('#customer_type').val($(this).val()).trigger('change.select2');
});
</script>



<script type="text/javascript">

	$('#report_print').click(function(){
		var customer=$('#name').val();
		if(customer == ''){
			customer=0;
		}

		url='<?= $this->webroot."Reports/purchase_invoice_wise_print/"; ?>'+customer;
		window.open(url);

	});
	
	$(document).ready(function(){
		$('#AccountHead_table').DataTable( {
                "columnDefs": [ {
     			"targets"  : 'no-sort',
      			"orderable": false,
    			}],
                dom: 'Blfrtip',
                buttons: [
                //'csv'
                {
               //extend: 'csv',
              // title:"Customer Due List Route Wise Report",
               //footer: true,
                extend: 'csvHtml5',
                title:"Party Due List Report",
                footer: true,
              customize: function (csv) {
                 return "\t   Party Due List Report\n"+  csv ;
               }
              // exportOptions: { columns: ':visible' },
              //  title:'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
             },
                ]
            } );
		//$.fn.show_alert('Success');
	});
	
	$('#name').change(function(){
		$.fn.table_search();
	});
	$.fn.table_search=function(){
		
		var name=$('#name option:selected').text();
		var data={
			name:name,
		};
		var url_address= '<?php echo $this->webroot; ?>'+'Reports/party_due_report_ajax';
		$.ajax({
			type: "post",
			url:url_address,
			data: data,
			dataType:'json',
			success: function(response) {
				console.log(response);
				$('#AccountHead_table').DataTable().destroy();
				$('#AccountHead_table tbody').html(response.row);
				$('#AccountHead_table').DataTable({
					 "columnDefs": [ {
     			"targets"  : 'no-sort',
      			"orderable": false,
    			}],
                dom: 'Blfrtip',
                buttons: [

                //'print',
                //'csv'
                {
               extend: 'print',
               footer: true,
               //exportOptions: { columns: ':visible' },
                customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                    // '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
                    // '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
                    // '<h5 align="center">Thiruvangoor</h5',
                    // '<h5 align="center">Calicut-673 304</h5>',
                    // '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
           '<h3 align="center">Party Due List Report</h3>',
            //'<h5>CustomerType :'+$('#customer_type_id').val()+'</h5>',
                    //'<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+'</h5>'
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
                // .prepend(
                //   '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
                //   )
              }
             },
{
               //extend: 'csv',
              // title:"Customer Due List Route Wise Report",
               //footer: true,
                extend: 'csvHtml5',
                title:"Party Due List Report",
                footer: true,
              customize: function (csv) {
                 return "\t   Party Due List Report\n"+  csv ;
               }
              // exportOptions: { columns: ':visible' },
              //  title:'(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
             },
                ]
				});
				$.fn.show_alert('Success');
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	}
	$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
</script>
