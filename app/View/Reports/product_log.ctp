<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<section class="content-header">
  <h1>Stock Movement Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row" style="margin-top: 2%;">
         <div class="col-md-3">
           <?php echo $this->Form->input('product_id',array('id'=>'product_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'ALL')); ?>
         </div>
         <div class="col-md-3">
           <?php echo $this->Form->input('warehouse_id',array('id'=>'warehouse_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'ALL')); ?>
         </div>
         <div class="col-md-2">
              <div class="col-md-12">
                <?php echo $this->Form->input('from_date',array('type'=>'text','id'=>'from_date','class'=>'form-control search_field date_picker datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
            <div class="col-md-2">
              <div class="col-md-12">
                <?php echo $this->Form->input('to_date',array('type'=>'text','id'=>'to_date','class'=>'form-control  search_field date_picker datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
         <div class="col-md-1"><br>
          <button class='btn btn-success' type='button' id='fetch_button'>Get</button>
        </div>
      </div>
    </div>
    <div class="box-body">
      <table class="table table-condensed table boder no-footer table-bordered" id='table_data' data-page-length="10" data-order='[[ 0, "asc" ]]' width="98%">
        <thead>
          <tr class="blue-bg">
            <th style="width:5%">LOG ID</th>
            <th>date</th>
            <th>Remark</th>
             <th style="width:10%">Customer</th>
            <th style="width:20%">Product</th>
            <th >Warehouse</th>
            <th>Executive Rate</th>
            <th>Quantity_In</th>
            <th>Quantity_Out</th>
            <th>Balance</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  $('#table_data').DataTable({
    "processing": true,
    "serverSide": true,
    "ordering": false,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/GetProductLogAjax",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
         d.to_date= $('#to_date').val();
        d.warehouse_id= $('#warehouse_id').val();
        d.product_id= $('#product_id').val();
      },
      "dataSrc": "records",
    },
    "lengthMenu": [[10,25,50,100], [10,25,50,100]],
    "columns": [
      { "data" : "StockLog.id" },
      { "data" : "StockLog.updated_at" },
      { "data" : "StockLog.remark" },
      { "data" : "StockLog.customer_name" },
      { "data" : "Product.name" },
      { "data" : "Warehouse.name" ,className:"text-center"},
      { "data" : "StockLog.wholesale_price" ,className:"text-right" },
      { "data" : "StockLog.quantity_in" ,className:"text-right" },
      { "data" : "StockLog.quantity_out" ,className:"text-right" },
      { "data" : "StockLog.quantity" ,className:"text-right" },

    ],
    "footerCallback": function ( row, data, start, end, display ) {},
    "columnDefs": [
    ],
  });
  $('#fetch_button').click(function(){
    table = $('#table_data').dataTable();
    table.fnDraw();
  });
  $('.date_range_picker').daterangepicker({
    opens: 'left',
    locale: { format: 'DD.MM.YYYY'}
  });
  $('.date_range_picker').on('blur', function(ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
  });
</script>
