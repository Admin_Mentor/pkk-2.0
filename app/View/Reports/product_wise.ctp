
<style type="text/css">
.cls_label_all {
  padding-top: 5%;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Product Wise Report</h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-header">
      <div class="col-md-12">
          <div class="col-md-3">
           <?= $this->Form->input('route_id',array('class'=>'form-control select2 name','type'=>'select','options'=>$route_list,'id'=>'route_id','label'=>'Route','empty'=>'ALL')); ?>
           </div>
        <div class="col-md-3">
        <?= $this->Form->input('customer_group',array('class'=>'form-control select2 name','type'=>'select','empty'=>'select','options'=>$customer_group,'id'=>'customer_group','label'=>'Customer Group')); ?>
             </div>

        <div class="col-md-3">
          <?php echo $this->Form->input('executives',array('class'=>'form-control select_two_class','id'=>'executive_id')) ?>
        </div>
        <div class="col-md-3" >
           <?php echo $this->Form->input('product_type_id',array('type'=>'select','empty' =>'ALL','options'=>$Product_type,'class'=>'form-control select_two_class search_class product_flitering','style'=>'width:100%','label'=>'Product Type','id'=>'product_type_id',)) ?>
         </div>
        
      </div>
      <div class="col-md-12"><br>
        <div class="col-md-3" >
            <?php echo $this->Form->input('brand',array('type'=>'select','empty' =>'ALL','options'=>$Brand,'style'=>'width:100%','id'=>'brand_id','class'=>'form-control select_two_class search_class product_flitering','label'=>'Brand')) ?>
         </div>
          <div class="col-md-3" >
           <?php echo $this->Form->input('product_id',array('type'=>'select', 'empty' =>'ALL','options'=>$Product,'style'=>'width:100%','class'=>'form-control select_two_class search_class','label'=>'PRODUCT','id'=>'product_id')) ?>
         </div>
        <div class="col-md-2">
         <?php echo $this->Form->input('from_date',array(
          'type'=>'text',
          'id'=>'from_date',
          'value'=>$from,
          'class'=>'form-control cls_label_all date_field date_picker
          datepicker',
          'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
          )); ?>
        </div>
        <div class="col-md-2">
          <?php echo $this->Form->input('to_date',array(
            'type'=>'text',
            'id'=>'to_date',
            'value'=>$to,
            'class'=>'form-control cls_label_all date_field date_picker
            datepicker',
            'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
            )); ?>
          </div>
          <div class="col-md-2"><br>
            <button class='btn' type='button' id='fetch_button'>Fetch</button>
          </div>
        </div>
      </div>
      <div class="box-body">
        <div class="col-md-4 col-xs-12">
          <h3 class="muted "></h3>
        </div>
        <table class="table table-condensed table boder" id='table_product_wise_list' data-page-length="-1">
          <thead>
            <tr class="blue-bg">
              <th width="20%">Executive Name</th>
              <th width="20%">Customer Name</th>
              <th width="40%">Brand</th>
              <th width="40%">Category</th>
              <th width="40%">Product</th>
              <th>Sale No</th>
              <th>Date</th>
              <th align="right">Rate</th>
              <th align="right">Qty</th>
              <!-- <th align="right">Returned Qty</th> -->
              <th align="right">Amount</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  $('#table_product_wise_list').DataTable( );
  $.fn.brand_change=function(brand_id){
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/get_product_by_brand_list/'+brand_id;
    $.ajax({
      type: "GET",
      url:url_address,
      dataType:'json',
      success: function(response) {
       $('#category_id').empty();
       $('#category_id').append($("<option></option>").attr("value", ' ').text('All'));
       $.each(response, function(key, value) {
        $('#category_id').append($("<option></option>").attr("value", key).text(value));
      });
     },
     error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  }
  $.fn.executive_change=function(executive_id){
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/get_executive_by_brand_list/'+executive_id;
    $.ajax({
      type: "GET",
      url:url_address,
      dataType:'json',
      success: function(response) {
       $('#brand_id').empty();
       $('#brand_id').append($("<option></option>").attr("value", ' ').text('All'));
       $.each(response, function(key, value) {
        $('#brand_id').append($("<option></option>").attr("value", key).text(value));
      });
     },
     error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  }
  $.fn.category_change=function(category_id){
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/get_category_by_product/'+category_id;
    $.ajax({
      type: "GET",
      url:url_address,
      dataType:'json',
      success: function(response) {
       $('#product_id').empty();
       $('#product_id').append($("<option></option>").attr("value", ' ').text('All'));
       $.each(response, function(key, value) {
        $('#product_id').append($("<option></option>").attr("value", key).text(value));
      });
     },
     error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  }
  $('#brand_id').change(function(){
    var brand_id=$(this).val();
    $.fn.brand_change(brand_id);
  });
  $('#executive_id').change(function(){
    var executive_id=$(this).val();
    $.fn.executive_change(executive_id);
  });
  $('#category_id').change(function(){
    var category_id=$(this).val();
    $.fn.category_change(category_id);
  });
  $.fn.search = function() {
    var brand_id=$('#brand_id').val();
    var product_id=$('#product_id').val();
    var executive_id=$('#executive_id').val();
    var category_id=$('#category_id').val();
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    var data={
      brand_id:brand_id,
      product_id:product_id,
      executive_id:executive_id,
      category_id:category_id,
      to_date:to_date,
      from_date:from_date,
    };
    console.log(data);
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/SaleItemWise_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {
        $('#table_product_wise_list').DataTable().destroy();
        $('#table_product_wise_list tbody').html(response.row);
        $('#total_amount').html(response.total);
        $('#table_product_wise_list').DataTable( {
          "ordering": false,
          dom: 'Bfrtip',
          buttons: [
          {
           extend: 'colvis',
         },
         {
          extend: 'print',
             // text: 'Print' ,
             footer: true,
              //title: 'Brand Wise',
              exportOptions: { columns: ':visible' },
              customize: function ( win ) {
                $(win.document.body)
                .css( 'font-size', '10pt' )
                .prepend(
                    // '<h3 align="center">Nigella\'s Herbal Healthcare</h3>',
                    // '<h5 align="center">#7/777,<b>Industrial Compound</b></h5>',
                    // '<h5 align="center">Thiruvangoor</h5',
                    // '<h5 align="center">Calicut-673 304</h5>',
                    // '<h5 align="center">Ph: 0496 2633270,9745005600</h5>',
                    '<h3 align="center">Brand Wise Report</h3>',
                    '<h5>Executive :'+response.executives+'</h5>',
                    '<h5>Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+'</h5>'
                    );
                $(win.document.body).find( 'table' )
                .addClass( 'compact' )
                .css( 'font-size', 'inherit' )
                .prepend(
                  '<tr><td colspan="8"><h4>Item Name : '+$('#product_id option:selected').text()+'</h4></td></tr>'
                  )
              }
            },
            {
              extend: 'csv',
             // text: 'CSV' ,
             footer: true,
              title:'Brand Wise Report of '+response.executives+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
              exportOptions: { columns: ':visible' },
            },
            {
              extend: 'excel',
             // text: 'Excel' ,
             footer: true,
              title:'Brand Wise Report of '+response.executives+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
              exportOptions: { columns: ':visible' },
            },
            {
              extend: 'pdf',
              //text: 'Pdf' ,
              footer: true,
              title:'Brand Wise Report of '+response.executives+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
             exportOptions: { columns: ':visible' },
           },
           {
             extend: 'pageLength',
           },
           ]
         } );
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  };
  $('#fetch_button').click(function(){
    $.fn.search();
  });
   $('#route_id').change(function(){
    var route_id=$(this).val();
    $.fn.route_change(route_id);
  });
  $.fn.route_change=function(route_id){
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/get_executive_by_route_list/'+route_id;
    $.ajax({
      type: "GET",
      url:url_address,
      dataType:'json',
      success: function(response) {
       $('#executive_id').empty();
       $('#executive_id').append($("<option></option>").attr("value", ' ').text('Select'));
       $.each(response, function(key, value) {
        $('#executive_id').append($("<option></option>").attr("value", key).text(value));
      });
     },
     error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  }
  $.fn.executive_change=function(executive_id){
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/get_executive_by_product_type/'+executive_id;
    $.ajax({
      type: "GET",
      url:url_address,
      dataType:'json',
      success: function(response) {
       $('#product_type_id').empty();
       $('#product_type_id').append($("<option></option>").attr("value", ' ').text('Select'));
       $.each(response, function(key, value) {
        $('#product_type_id').append($("<option></option>").attr("value", key).text(value));
      });
     },
     error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  }
  $('#brand_id').change(function(){
    var brand_id=$(this).val();
    $.fn.brand_change(brand_id);
  });
  $('#executive_id').change(function(){
    var executive_id=$(this).val();
    $.fn.executive_change(executive_id);
  });
  $('#product_type_id').change(function(){
    var product_type_id=$(this).val();
    $.fn.product_type_change(product_type_id);
  });
  $.fn.product_type_change=function(product_type_id){
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/get_product_type_by_brand/'+product_type_id;

    $.ajax({
      type: "GET",
      url:url_address,
      dataType:'json',
      success: function(response) {
       $('#brand_id').empty();
       $('#brand_id').append($("<option></option>").attr("value", ' ').text('select'));
       $.each(response, function(key, value) {
        $('#brand_id').append($("<option></option>").attr("value", key).text(value));
      });
     },
     error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  }
  $('#brand_id').change(function(){
    var brand_id=$(this).val();
    $.fn.brand_change(brand_id);
  });
   $.fn.brand_change=function(brand_id){
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/get_product_by_brand_list/'+brand_id;
    $.ajax({
      type: "GET",
      url:url_address,
      dataType:'json',
      success: function(response) {
       $('#product_id').empty();
       $('#product_id').append($("<option></option>").attr("value", ' ').text('select'));
       $.each(response, function(key, value) {
        $('#product_id').append($("<option></option>").attr("value", key).text(value));
      });
     },
     error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  }
</script>