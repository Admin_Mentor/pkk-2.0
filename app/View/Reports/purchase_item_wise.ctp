<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<style type="text/css">

.cls_label_all {
  padding-top: 5%;
}

.row_top_row{

  margin-top: 5%;
}
.deaf_btn_btn {
  margin-left: 4%;
}

.row_new_add{

  margin-top: 2%;
}

#radio_butto_add {
  margin-top: 7px;
  margin-left: -51%;
}
</style>
<section class="content-header">
  <h1>Purchase Register Itemswise</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="box-header">
        <div class="col-md-12">
          <div class="col-md-2">
            <?= $this->Form->input('product_type_id',array('type'=>'select','empty' =>'All','options'=>$Product_type,'class'=>'form-control select_two_class rec_select_box search_field','label'=>'Product Type')) ?>
          </div>
          <div class="col-md-3">
            <?= $this->Form->input('product_id',array('type'=>'select', 'empty' =>'All','options'=>$Product,'class'=>'form-control select_two_class rec_select_box search_field','label'=>'Product')) ?>
          </div>
          <div class="col-md-2">
            <div class="col-md-12">
              <?= $this->Form->input('from_date',array('type'=>'text', 'id'=>'from_date', 'value'=>$final, 'class'=>'form-control cls_label_all date_field search_field date_picker datepicker', 'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask', )); ?>
            </div>
          </div>
          <div class="col-md-2">
            <div class="col-md-12">
              <?= $this->Form->input('to_date',array('type'=>'text', 'id'=>'to_date', 'value'=>$date1, 'class'=>'form-control cls_label_all date_field search_field date_picker datepicker', 'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask', )); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-condensed table boder table-bordered" id='table_product_wise_list'>
          <thead>
            <tr class="blue-bg">
              <th>ProductType</th>
              <th>Product</th>
              <th>Purchase No</th>
              <th>Date</th>
              <th>Rate</th>
              <th>Qty</th>
              <th>Returned Qty</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" style="font-size:20px; color:red;text-align:right">Total:</th>
              <th style="font-size:20px; color:red;"></th>
              <th style="font-size:20px; color:red;"></th>
              <th style="font-size:20px; color:red;"></th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function(){
    $("[data-mask]").inputmask();
    $('select').select2();
    var table_product_wise_list=$('#table_product_wise_list').DataTable( {
      "processing": true,
      "ordering"  : false,
      "serverSide": true,
      "lengthMenu": [[50, 100, 200, 1000], [50, 100, 200, 1000, ] ],
      "ajax": {
        "url": "<?= $this->webroot ?>Reports/purchase_item_wise_ajax",
        "type": "POST",
        data:function( d ) {
          d.product_id     = $('#product_id').val();
          d.product_type_id= $('#product_type_id').val();
          d.from_date      = $('#from_date').val();
          d.to_date        = $('#to_date').val();
        },
        "dataSrc": "records",
      },
      "dom": 'Bfrtip',
      "buttons": [
      { extend: 'colvis' },
      { extend: 'print', footer: true, exportOptions: { columns: ':visible'}, },
      { extend: 'csv'  , footer: true, exportOptions: { columns: ':visible'}, },
      { extend: 'excel', footer: true, exportOptions: { columns: ':visible'}, },
      { extend: 'pageLength'},
      ],
      footerCallback: function ( row, data, start, end, display ) {
        var api = this.api(), data;
        var intVal = function ( i ) {return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
        var pagecolumn=5;
        pageTotal = api.column( pagecolumn, { page: 'current'} ).data().reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 ); $( api.column( pagecolumn ).footer() ).html(''+parseFloat(pageTotal)+'');
        pagecolumn++; pageTotal = api.column( pagecolumn, { page: 'current'} ).data().reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 ); $( api.column( pagecolumn ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
        pagecolumn++; pageTotal = api.column( pagecolumn, { page: 'current'} ).data().reduce( function (a, b) {return intVal(a) + intVal(b); }, 0 ); $( api.column( pagecolumn ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      },
      "columns": [
      { "data" : "ProductType.name" ,'className':'text-center'},
      { "data" : "Product.name" ,'className':'text-center'},
      { "data" : "Purchase.invoice_no" ,'className':'text-right'},
      { "data" : "Purchase.date_of_delivered" },
      { "data" : "PurchasedItem.unit_price" ,'className':'text-right'},
      { "data" : "PurchasedItem.quantity" ,'className':'text-right'},
      { "data" : "PurchasedItem.return_qty" ,'className':'text-right'},
      { "data" : "PurchasedItem.total" ,'className':'text-right'},
      ],
    });
    $.fn.product_type_change=function(product_type_id){
      var data={product_type_id:product_type_id};
      var url_address= '<?= $this->webroot; ?>'+'Reports/product_type_select_ajax_purchaseitem';
      $.ajax({
        type: "post",
        url:url_address,
        data: data,
        success: function(response) {
          $('#product_id').html(response);
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    }
    $('#product_type_id').change(function(){
      var product_type_id=$(this).val();
      $('#product_id').select2().select2('val', $('option:eq(0)').val());
      $.fn.product_type_change(product_type_id);
    });
    $('#product_id,#product_type_id,#from_date,#to_date').on('change',function(){
      table_product_wise_list.draw();
    });
  });
</script>