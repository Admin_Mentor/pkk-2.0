<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<style type="text/css">
.number_format_alignclass="text-right"  {
text-align:right;
}
</style>
<section class="content-header">
<h1>Purchase Register Summary</h1>
</section>
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box">
<div class="Stockmanagement">
<div class="row">
<div class="col-md-12">
<div class="col-md-12">
<div class="row">
	<br>
<div class="col-md-2">
<div class="col-md-12">
<?php echo $this->Form->input('from_date',array(
'type'=>'text',
'id'=>'from_date',
'value'=>$final,
'class'=>'form-control cls_label_all date_field search_field date_picker datepicker',
// 'label'=>false,
'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
)); ?>

</div>
</div>
<div class="col-md-2">
<div class="col-md-12">
<?php echo $this->Form->input('to_date',array(
'type'=>'text',
'id'=>'to_date',
'value'=>$date1,
'class'=>'form-control cls_label_all date_field search_field date_picker datepicker',
// 'label'=>false,
'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
)); ?>
</div>
</div>
<div class="col-md-1"><br>
<button class='btn btn-success' type='button' id='fetch_button'>Get</button>
</div>
</div>
</div>
<div class="box-body">
<div class="col-md-4 col-xs-12">
<h3 class="muted "></h3>
</div>
<table class="table table-condensed table boder table-bordered" style="width: 100% !important" id='table_product_wise_list' data-page-length="-1">
<thead>
<tr class="blue-bg">
<th>No</th>
<th>Date</th>
<th style="text-align:right">Party Name</th>
<th style="text-align:right">Net Amount</th>
<th style="text-align:right">Discount</th>
<th style="text-align:right">Taxable Amount</th>
<th style="text-align:right">Vat</th>
<!-- <th>R.off</th>
 -->
 <th style="text-align:right">Total Amount</th>
</tr>
</thead>
<tbody>                 
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<script type="text/javascript">
$('#fetch_button').click(function(){
var from_date=$('#from_date').val();
var to_date=$('#to_date').val();
var data={
to_date:to_date,
from_date:from_date
};
var url_address= '<?php echo $this->webroot; ?>'+'Reports/purchase_summary_ajax';
$.ajax({
type: "post",  // Request method: post, get
url:url_address,
data: data,  // post data
dataType:'json',
success: function(response) {
$('#table_product_wise_list').DataTable().destroy();
$('#table_product_wise_list tbody').html(response.row);

$('#table_product_wise_list').DataTable( {
"ordering": false,
dom: 'Bfrtip',
buttons: [
{
extend: 'colvis',
},
{
extend: 'print',
footer: true,
exportOptions: { columns: ':visible'},
customize: function ( win ) {
$(win.document.body)
.css( 'font-size', '10pt' )
.prepend(
                    '<h3 align="center">Purchase Register Report</h3>',
                    '<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+'</h5>'

                    );

$(win.document.body).find( 'table' )
.addClass( 'compact' )
.css( 'font-size', 'inherit' )
.prepend(
'<tr><td colspan="8"></td></tr>'
);
}
},
{
extend: 'csv',
footer: true,
 title:'Purchase Register Report  '+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
 customize: function (csv) {
                 return "\tPurchase Register Report \n"+"\t(Period : From "+$("#from_date").val()+"    To : "+$("#to_date").val()+")\n"+  csv ;
               },

exportOptions: { columns: ':visible'},
},
{
extend: 'excel',
footer: true,
title:'Purchase Register Report  '+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
exportOptions: { columns: ':visible'},
},
{
extend: 'pdf',
footer: true,
title:'Purchase Register Report  '+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
exportOptions: { columns: ':visible'},
},
{
extend: 'pageLength',
},
]
} );
$.fn.show_alert('Success');
},
error:function (XMLHttpRequest, textStatus, errorThrown) {
alert(textStatus);
}
});
});
$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
</script>
