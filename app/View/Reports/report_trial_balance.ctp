<style>
.main_head b{color:red;font-size:14px;}
</style>
<section class="content-header">
  <div class="box box-primary">
    <div class="box-header">
      <div class="row">
        <div class="form-group">
          <div class="col-md-6">
            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
              <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
              <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12" style="display: none">
            <?= $this->Form->input('branch_id',array('type'=>'select','id'=>'branch_id','class'=>'form-control select_two_class','style'=>'width: 100%','empty' => 'All','options'=>$BranchList,'default' =>'Select','label'=>'Branch')); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><br>
              <button type='button' class='btn btn-primary' id='get_button'>GET</button>
            </div>
            <div class="col-md-2">
              <div class="col-md-6 col-lg-6 col-sm-6" style="margin-top: 5%;margin-left: 71%;display: none">
                <input type="button"  class="save" name="button" id="exportExcel" value="Export"></a>
              </div> 
            </div>
          </div>
          <div class="col-md-6" style="margin-left: 57%;margin-top: -5%;"><h1>Trial Balance</h1></div>
        </div>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
              <div class="panel-heading main_head">
                 <b>Particulars</b>
                <b style="margin-left: 38%;">Opening</b>
                <b style="margin-left: 24%;">Transactions</b>
                <b style="margin-left: 18%;">Closing</b>
                <b style="margin-left: 61%;">Debit</b>
                <b style="margin-left: 15%;">Credit</b>
              </div>
              <div class="panel-body" id='Left-Particulars'>
                <div class="panel-group" id="Particulars_Left">
                  <?php foreach ($MainGroup as $key => $value) { ?>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#Particulars_Left" href="#collapse<?=$value['AccMainGroup']['id']?>" class="collapsed" aria-expanded="false">
                          <div class="row">
                            <div class="sub_name col-md-4" id="<?=$value['AccMainGroup']['id']?>"><b><?=$value['AccMainGroup']['name']?></b></div>
                            <div class="col-md-2"><b id='type_opening_id_<?=$value['AccMainGroup']['id']?>' class='pull-right type_amount-opening'></b></div>
                            <div class="col-md-2"><b id='type_debit_id_<?=$value['AccMainGroup']['id']?>' class='pull-right type_amount-debit'></b></div>
                            <div class="col-md-2"><b id='type_credit_id_<?=$value['AccMainGroup']['id']?>' class='pull-right type_amount-credit'></b></div>
                            <div class="col-md-62"><b id='type_id_<?=$value['AccMainGroup']['id']?>' class='pull-right type_amount-closing'>0</b></div>
                            </div>
                          </a>
                        </h4>
                      </div>
                      <div id="collapse<?=$value['AccMainGroup']['id']?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                          <div class="panel-group" id="<?=$value['AccMainGroup']['name']?>">
                          </div>
                        </div>
                      </div>
                    </div>
                  <?php } ?>
                </div>
              </div>
            </div>
          
        </div>
      </div>
    </div>
    <div class="box-footer">
      <!-- <div class="col-md-12">
        <div class="col-md-6" id='diffrence_in_opening-left' style="display: none">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: red"><b>Difference In Opening Balance : <span class='pull-right' id='diffrence-left'>0</span></b></h4>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-md-offset-6" id='diffrence_in_opening-right' style="display: none">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: red"><b>Difference In Opening Balance : <span class='pull-right' id='diffrence-right'>0</span></b></h4>
            </div>
          </div>
        </div>
      </div> -->
      <div class="col-md-12" style="display: none">
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: blue"><b>Total Debit : <span class='pull-right' id='grand_total-left'>0</span></b></h4>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: blue"><b>Total Credit : <span class='pull-right' id='grand_total-right'>0</span></b></h4>
            </div>
          </div>
        </div>
      </div>
    </div>
     <div class="box-footer">
      <!-- <div class="col-md-12">
        <div class="col-md-6" id='diffrence_in_opening-left' style="display: none">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: red"><b>Difference In Opening Balance : <span class='pull-right' id='diffrence-left'>0</span></b></h4>
            </div>
          </div>
        </div>
        <div class="col-md-6 col-md-offset-6" id='diffrence_in_opening-right' style="display: none">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: red"><b>Difference In Opening Balance : <span class='pull-right' id='diffrence-right'>0</span></b></h4>
            </div>
          </div>
        </div>
      </div> -->
      <div class="col-md-12" id='diffrence_in_opening'>
          <div class="panel panel-default">
          
            <div class="panel-heading">
              <div class="row">
                <div class="col-md-6">
                  <h4 style="color: red"><b>Difference In Opening Balance :</b></h4>
                </div>
                <div class="col-md-2">
                  <h4 style="color: red"><b><span class='pull-right' id='diffrence-left'>0</span></b></h4>
                </div>
                <div class="col-md-2">
                  <h4 style="color: red"><b><span class='pull-right' id='diffrence-right'>0</span></b></h4>
                </div>
                <div class="col-md-2">
                </div>
              </div>
            </div>
            
          </div>
      </div>
      <div class="col-md-12">
          <div class="panel panel-default">
          
            <div class="panel-heading">
              <div class="row">
                <div class="col-md-6">
                  <h4 style="color: blue"><b>Grand Total :</b></h4>
                </div>
                <div class="col-md-2">
                  <h4 style="color: blue"><b><span class='pull-right' id='grand_total-debit'>0</span></b></h4>
                </div>
                <div class="col-md-2">
                  <h4 style="color: blue"><b><span class='pull-right' id='grand_total-credit'>0</span></b></h4>
                </div>
                <div class="col-md-2">
                  <h4 style="color: blue"><b><span class='pull-right' id='grand_total-closing'>0</span></b></h4>
                </div>
              </div>
            </div>
            
          </div>
        <!-- <div class="col-md-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 style="color: blue"><b>Total Credit : <span class='pull-right' id='grand_total-right'>0</span></b></h4>
            </div>
          </div>
        </div> -->
      </div>
    </div>
  </div>
</section>
<?php require('general_journal_transaction_ledger_modal.php'); ?>
<section class="content"></section>
<script type="text/javascript">
$('.sub_name').click(function(){
  var sub_id=$(this).attr('id');
 // alert(sub_id);
});

$.fn.trialbalance=function(){
    $('#wait').show();
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    var branch = $("#branch_id").val();
   var decimalpoint = '2';
    var type={};
    $(".sub_name").each(function () {
       // console.log($(this).text());
       // console.log($(this).attr('id'));
      type[$(this).attr("id")] = $(this).text();
      //alert(type);
    });
    // type={
    //   1:'Asset',
    //   2:'Capital',
    //   3:'Expense',
    //   4:'Income',
    //   5:'Liabilities',
    // };
    var activeAjaxConnections=0;
    $.each(type,function(type_id,type_name){
      var data={
        from_date:from_date,
        to_date:to_date,
        type_id:type_id,
      };
      $.ajax({
        method: "POST",
        url: "<?= $this->webroot; ?>Reports/Get_Master_Group_Ajax",
        beforeSend: function(xhr) {
          activeAjaxConnections++;
        },
        data: data,
        dataType: "json",
      }).done(function(data) {
        activeAjaxConnections--;
        $("#"+type_name+"").empty();
        $.each(data.SubGroup,function(sub_group_id,sub_group_name){
          $("#"+type_name+"").append(
            '<div class="panel panel-default">\
            <div class="panel-heading">\
            <h4 class="panel-title">\
            <a data-toggle="collapse" data-parent="#'+type_name+'" href="#SubGroup_'+sub_group_id+'" aria-expanded="false" class="collapsed"><div class="row"><div class="col-md-4"><b>'+sub_group_name+'</b></div><div class="col-md-2"><b class="opening_amount pull-right" data-parent=type_opening_id_'+type_id+' id="master_group_opening_id_'+sub_group_id+'">0</b></div><div class="col-md-2"><b class="debit_amount pull-right" data-parent=type_debit_id_'+type_id+' id="master_group_debit_id_'+sub_group_id+'">0</b></div><div class="col-md-2"><b class="credit_amount pull-right" data-parent=type_credit_id_'+type_id+' id="master_group_creit_id_'+sub_group_id+'">0</b></div><div class="col-md-2"><b class="pull-right master_group_amount" data-parent=type_id_'+type_id+' id="master_group_id_'+sub_group_id+'">0</b></div></div></a>\
            </h4>\
            </div>\
            <div id="SubGroup_'+sub_group_id+'" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">\
            <div class="panel-body">\
            </div>\
            </div>\
            </div>'
            );  
          var data={
            from_date:from_date,
            to_date:to_date,
            sub_group_id:sub_group_id,
            type_id:type_id,
            branch:branch,
          };
          $.ajax({
            method: "POST",
            url: "<?= $this->webroot; ?>Reports/Get_AccountHead_Ajax",
            beforeSend: function(xhr) {
              activeAjaxConnections++;
            },
            data: data,
            dataType: "json",
          }).done(function(data) {
            activeAjaxConnections--;
            if(activeAjaxConnections==0)
            {
              $('#wait').hide();
            }
            $('#SubGroup_'+sub_group_id+' > .panel-body').empty();
            $('#SubGroup_'+sub_group_id+' > .panel-body').append(
              '<table class="table table-condensed cell-border compact stripe Table_SubGroup_'+sub_group_id+'">\
              <thead>\
              </thead>\
              <tbody></tbody>\
              </table>'
              );
              $.each(data.AccountHead,function(key,account){
              //if(account.amount)
              // console.log(account);
                 // if(account.debit>0 || account.credit>0)
              // if(account.amount)
              if(account.name!="STOCK")
              {
                $('.Table_SubGroup_'+sub_group_id+' tbody').append(
                  '<tr>\
                  <td class="name"><b>'+account.name+'</b></td>\
                  <td class="text-right"><b data-parent="master_group_opening_id_'+sub_group_id+'" class="journal_opening_amount">'+parseFloat(account.opening_balance).toFixed(2)+'</b></td>\
                  <td class="text-right"><b data-parent="master_group_debit_id_'+sub_group_id+'" class="journal_debit_amount">'+parseFloat(account.debit).toFixed(2)+'</b></td>\
                  <td class="text-right"><b data-parent="master_group_creit_id_'+sub_group_id+'" class="journal_credit_amount">'+parseFloat(account.credit).toFixed(2)+'</b></td>\
                  <td class="text-right"><b data-parent="master_group_id_'+sub_group_id+'" class="journal_amount">'+parseFloat(account.amount).toFixed(2)+'</b></td>\
                  </tr>'
                  );
              }
            });
          }).fail(function(){ activeAjaxConnections-- });
      });
    }).fail(function() { activeAjaxConnections-- });
  });
  $(document).ajaxComplete(function(){
    var decimalpoint = '2';
    $(".journal_amount").each(function(){
      var amount=$(this).text();
      var parent=$(this).data('parent');
      $('#'+parent).text(0);
    });
    $(".journal_amount").each(function(){
      var amount=$(this).text();
      var parent=$(this).data('parent');
      $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
    });
     $(".journal_opening_amount").each(function(){
      var amount=$(this).text();
      var parent=$(this).data('parent');
      $('#'+parent).text(0);
    });
    $(".journal_opening_amount").each(function(){
      var amount=$(this).text();
      var parent=$(this).data('parent');
      $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
    });
    $(".journal_debit_amount").each(function(){
      var amount=$(this).text();
      var parent=$(this).data('parent');
      $('#'+parent).text(0);
    });
    $(".journal_debit_amount").each(function(){
      var amount=$(this).text();
      var parent=$(this).data('parent');
      $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
    });
    $(".journal_credit_amount").each(function(){
      var amount=$(this).text();
      var parent=$(this).data('parent');
      $('#'+parent).text(0);
    });
    $(".journal_credit_amount").each(function(){
      var amount=$(this).text();
      var parent=$(this).data('parent');
      $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
    });
    
    $(".master_group_amount").each(function(){
      var amount=$(this).closest('b').text();
      var parent=$(this).data('parent');
      $('#'+parent).text(0);
    });
    $(".master_group_amount").each(function(){
      var amount=$(this).closest('b').text();
     // console.log(amount);
      // alert(amount);
      var parent=$(this).data('parent');
       var type_id=$(this).attr('data-parent');
  //      if(type_id=='type_id_3'){
  //        $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2)+' Dr');
  //      }else{
  //       if(amount>0){
  //     $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2)+' Dr');
  //   }else{
  //     $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2)+' Cr');
  //   }
  // }
  // console.log(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
  parnt=$('#'+parent).text();
  if(parnt.includes('Cr'))
  {
    var amt_3 = parseFloat((parnt.toString()).replace(' Cr',''));
    parnt = pos_to_neg(amt_3);
  }
  var amt_2 = parseFloat(parnt)+parseFloat(amount);

   if(amt_2.toFixed(2)>0){
    $('#'+parent).text(Math.abs(amt_2).toFixed(2)+' Dr');
  }
        else if(amt_2.toFixed(2)<0) {
          var amt_3 = parseFloat((amt_2.toString()).substring(1));
          // amt_2 = parseFloat(amt_2);
          // var str = "Hello world!";
          // var res = str.substring(1);
           // var amt=parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2);
          $('#'+parent).text(amt_3.toFixed(2)+' Cr');
        }
        else {
          $('#'+parent).text(Math.abs(amt_2).toFixed(2));
        }
    });
     $(".opening_amount").each(function(){
      var amount=$(this).closest('b').text();
      var parent=$(this).data('parent');
      $('#'+parent).text(0);
    });
    $(".opening_amount").each(function(){
      var amount=$(this).closest('b').text();
      var parent=$(this).data('parent');
      var type_id=$(this).attr('data-parent');
  //      if(type_id=='type_opening_id_3'){
  //       $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2)+' Dr');
  //      }else{
  //    if(amount>0){
  //     $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2)+' Dr');
  //   }else {$('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2)+' Cr');}
  // }
  // if(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2)>0){
  //   $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2)+' Dr');
  // }
  //       else if(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2)<0) {
  //         var amt=parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2);
  //         $('#'+parent).text((amt)+' Cr');
  //       }
  //       else {
  //         $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
  //       }
   parnt=$('#'+parent).text();
  if(parnt.includes('Cr'))
  {
    var amt_3 = parseFloat((parnt.toString()).replace(' Cr',''));
    parnt = pos_to_neg(amt_3);
  }
  var amt_2 = parseFloat(parnt)+parseFloat(amount);

   if(amt_2.toFixed(2)>0){
    $('#'+parent).text(Math.abs(amt_2).toFixed(2)+' Dr');
  }
        else if(amt_2.toFixed(2)<0) {
          var amt_3 = parseFloat((amt_2.toString()).substring(1));
          // amt_2 = parseFloat(amt_2);
          // var str = "Hello world!";
          // var res = str.substring(1);
           // var amt=parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2);
          $('#'+parent).text(amt_3.toFixed(2)+' Cr');
        }
        else {
          $('#'+parent).text(Math.abs(amt_2).toFixed(2));
        }
      // $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
    });
    $(".debit_amount").each(function(){
      var amount=$(this).closest('b').text();
      var parent=$(this).data('parent');
      $('#'+parent).text(0);
    });
    $(".debit_amount").each(function(){
      var amount=$(this).closest('b').text();
      var parent=$(this).data('parent');
      $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
    });
    $(".credit_amount").each(function(){
      var amount=$(this).closest('b').text();
      var parent=$(this).data('parent');
      $('#'+parent).text(0);
    });
    $(".credit_amount").each(function(){
      var amount=$(this).closest('b').text();
      var parent=$(this).data('parent');
      $('#'+parent).text(parseFloat(parseFloat($('#'+parent).text())+parseFloat(amount)).toFixed(2));
    });
    // $('#grand_total-left').text(0);
    // $(".type_amount-left").each(function(){
    //   var amount=$(this).closest('b').text();
    //   $('#grand_total-left').text(parseFloat(parseFloat($('#grand_total-left').text())+parseFloat(amount)).toFixed(2));
    // });
    // $('#grand_total-right').text(0);
    // $(".type_amount-right").each(function(){
    //   var amount=$(this).closest('b').text();
    //   $('#grand_total-right').text(parseFloat((parseFloat($('#grand_total-right').text())+parseFloat(amount))).toFixed(2));
    // });
    // var grand_total_left=$('#grand_total-left').text();
    // var grand_total_right=$('#grand_total-right').text();
    // $('#diffrence-left').text(0);
    // $('#diffrence-right').text(0);
    // if(grand_total_left>grand_total_right)
    // {
    //   var diffrence=grand_total_left-grand_total_right;
    //   $('#diffrence-right').text(parseFloat(diffrence).toFixed(2));
    //   if(diffrence)
    //   {
    //     $('#diffrence_in_opening-left').hide();
    //     $('#diffrence_in_opening-right').show();
    //   }
    //   $('#grand_total-right').text(parseFloat(parseFloat(grand_total_right)+parseFloat(diffrence)).toFixed(2));
    // }
    // else
    // {
    //   var diffrence=grand_total_right-grand_total_left;
    //   $('#diffrence-left').text(parseFloat(diffrence).toFixed(2));
    //   if(diffrence)
    //   {
    //     $('#diffrence_in_opening-left').show();
    //     $('#diffrence_in_opening-right').hide();
    //     var ttl = parseFloat(grand_total_left)+parseFloat(diffrence);
    //     $('#grand_total-left').text(parseFloat(ttl).toFixed(2));
    //   }
    // }
     $('#grand_total-debit').text(0);
    $(".type_amount-debit").each(function(){
      var amount=$(this).closest('b').text();
      $('#grand_total-debit').text(parseFloat($('#grand_total-debit').text()+parseFloat(amount)).toFixed(2));
    });
    //  $('#grand_total-closing').text(0);
    // $(".type_amount-closing").each(function(){
    //   var amount=$(this).closest('b').text();
    //   $('#grand_total-closing').text(parseFloat(parseFloat($('#grand_total-closing').text())+parseFloat(amount)).toFixed(2));
    // });
    $('#grand_total-credit').text(0);
    $(".type_amount-credit").each(function(){
      var amount=$(this).closest('b').text();
      $('#grand_total-credit').text(parseFloat($('#grand_total-credit').text()+parseFloat(amount)).toFixed(2));
    });
    var grand_total_left=$('#grand_total-debit').text();
    var grand_total_right=$('#grand_total-credit').text();
    $('#diffrence-left').text((0).toFixed(2));
    $('#diffrence-right').text((0).toFixed(2));
    if((grand_total_left>grand_total_right))
    {
      var diffrence=grand_total_left-grand_total_right;
     
      if(diffrence)
      {
         if(diffrence<1){
          $('#diffrence-right').text((0).toFixed(2));
      //$('#diffrence-right').text(parseFloat(diffrence).toFixed(2));
      }
      else{
          $('#diffrence-right').text(parseFloat(diffrence).toFixed(2));
      //}
        $('#diffrence_in_opening-left').hide();
        $('#diffrence_in_opening-right').show();
      }
    }
      $('#grand_total-credit').text(parseFloat(parseFloat(grand_total_right)+parseFloat(diffrence)).toFixed(2));
    }
    else
    {
      var diffrence=grand_total_right-grand_total_left;
    //    if(diffrence>0){
    //   $('#diffrence-left').text(parseFloat(diffrence).toFixed(2));
    // }
    //  else{
    //      $('#diffrence-left').text(0);
    //   }
      if(diffrence)
      {
       if(diffrence<1){
          $('#diffrence-left').text((0).toFixed(2));
      //$('#diffrence-right').text(parseFloat(diffrence).toFixed(2));
      }
      else{
        $('#diffrence-left').text(parseFloat(diffrence).toFixed(2));
        $('#diffrence_in_opening').show();
        // $('#diffrence_in_opening-left').show();
        $('#diffrence_in_opening-right').hide();
        // var ttl = parseFloat(grand_total_left);
        var ttl = parseFloat(grand_total_left)+parseFloat(diffrence);
        $('#grand_total-debit').text(parseFloat(ttl).toFixed(2));
      }
    }
    }
  });
}
$('#get_button').click(function(){
  $.fn.trialbalance();
});
$.fn.trialbalance();

function pos_to_neg(num)
{
return -Math.abs(num);
}
</script>
<script type="text/javascript">
  <?php require('general_journal_transaction_ledger_ajax.js'); ?>
</script>
<script type="text/javascript">
  $(document).ready(function(){

    $('#exportExcel').click(function()   { 
      var date_from= $('#from_date').val();
      var date_to=$('#to_date').val();
      if(!date_from)
      {
        alert("Take From Date");
        return false;
      }
      if(!date_to)
      {
        alert("Take To Date");
        return false;
      }
      var website_url3='<?php echo $this->webroot; ?>Reports/print_trial_balance_report/'+date_from+'/'+date_to;
      $(location).attr("href", website_url3);
    });
  });
</script>