<style type="text/css">
.second_line_padding_left
{
  padding-left: 45px !important;
  color: blue;
}
.single_account_name
{
  margin-left: 45px;
  color: blue;
}
td
{
  border-left: 1px solid #cdd0d4;
}
#sub_table tfoot td { color: red; }
</style>
<section class="content-header">
<!--   <h1> Route Wise Profit And Loss <span hidden>of company_name </span> for the year ended<span hidden> (date)</span></h1>
 -->    <h1> Route Wise Profit And Loss</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <div class="col-md-12">
        <?= $this->Form->create('Reports');?>
        <div class="form-group">
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('route_id',array('type'=>'select','class'=>'form-control select_two_class','style'=>'width: 100%;','id'=>'route_id','required','label'=>'Route',)); ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12">
            <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2 col-xs-12"><br>
            <button type='button' class='btn btn-primary' id='get_button'>GET</button>
          </div>
        </div>
        <?= $this->Form->end(); ?>
      </div>
    </div>
    <div class="box-body">
      <div class="col-md-6">
        <table class="table table-condensed table boder" id="myTable">
          <thead>
            <tr class="blue-bg">
              <th class="padding_left">Particulars</th>
              <th>Amount (AED)</th>
            </tr>
          </thead>
          <tbody>
            <tr class="blue-pddng toggle_class">
              <td class="padding_left">Cost of Sales</td>
              <td class='type_name text-right' id="costValue"><?= floatval($ProfitLoss['CostofSale']['costValue']); ?></td>
            </tr>
            <tr class="blue-pddng toggle_class">
              <td class="padding_left">Direct Expense</td>
              <td class='type_name text-right' id='DirectExpense'><?= floatval($ProfitLoss['Expense']['DirectExpense']['amount']); ?></td>
            </tr>
            <tr class='single_DirectExpense' style='display:none;'>
              <td>
                <table class="table table-condensed table boder second_line_padding_left" id="DirectExpense_sub_table">
                  <thead>
                    <tr>
                      <td>Name</td>
                      <td>Amount</td>
                      <td>Outstanding</td>
                      <td>PrePaid</td>
                      <td>Total</td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $Outstanding=0; $paid=0; $PrePaid=0; $total=0; foreach ($ProfitLoss['Expense']['DirectExpense']['single'] as $key => $value) : if($value['total'] || $value['Outstanding'] || $value['PrePaid']) : ?>
                    <tr>
                      <td class="sub_group_name" type_name='Expense'><?= $key; ?></td>
                      <td><span class='pull-right'><?= $value['paid']; $paid+=$value['paid']; ?></span></td>
                      <td><span class='pull-right'><?= $value['Outstanding']; $Outstanding+=$value['Outstanding']; ?></span></td>
                      <td><span class='pull-right'><?= $value['PrePaid']; $PrePaid+=$value['PrePaid']; ?></span></td>
                      <td><span class='pull-right'><?= $value['total']; $total+=$value['total']; ?></span></td>
                    </tr>
                  <?php endif; endforeach; ?>
                </tbody>
                <tfoot>
                  <td><span class='pull-right'>Total</span></td>
                  <td><span class='pull-right'><?= $paid; ?></span></td>
                  <td><span class='pull-right'><?= $Outstanding; ?></span></td>
                  <td><span class='pull-right'><?= $PrePaid; ?></span></td>
                  <td><span class='pull-right'><?= $total; ?></span></td>
                </tfoot>
              </table>
            </td>
            <td></td>
          </tr>
          <tr class="blue-pddng Gross_Profit">
            <td class="padding_left">Gross Profit c/d</td>
            <td id='FirstLeft' class='text-right'><?= floatval($ProfitLoss['First']['Left']); ?></td>
          </tr>
          <tr class="blue-pddng">
            <td class="total_amount padding_left">Total</td>
            <td id='FirstTotalLeft' class="total_amount text-right"><?= floatval($ProfitLoss['FirstTotal']['Left']); ?></td>
          </tr>
          <tr class="blue-pddng"  style="display: <?php if(!$ProfitLoss['Gross']['Left']) { echo 'none'; } ?>">
            <td class="padding_left">Gross Loss b/d</td>
            <td id='GrossLeft' class="text-right"><?= floatval($ProfitLoss['Gross']['Left']); ?></td>
          </tr>
          <tr class="blue-pddng toggle_class">
            <td class="padding_left">Indirect Expense</td>
            <td class='type_name text-right' id='IndirectExpense'><?= floatval($ProfitLoss['Expense']['IndirectExpense']['amount']); ?></td>
          </tr>
          <tr class='single_IndirectExpense' style='display:none;'>
            <td>
              <table class="table table-condensed table boder second_line_padding_left" id="IndirectExpense_sub_table">
                <thead>
                  <tr>
                    <td>Name</td>
                    <td>Amount</td>
                    <td>Outstanding</td>
                    <td>PrePaid</td>
                    <td>Total</td>
                  </tr>
                </thead>
                <tbody>
                  <?php  $Outstanding=0; $paid=0; $PrePaid=0; $total=0; foreach ($ProfitLoss['Expense']['IndirectExpense']['single'] as $key => $value) : ?>
                  <?php if($value['total'] || $value['Outstanding'] || $value['PrePaid']) : ?>
                    <tr>
                      <td class="sub_group_name" type_name='Expense'><?= $key; ?></td>
                      <td><span class='pull-right'><?= $value['paid']; $paid+=$value['paid']; ?></span></td>
                      <td><span class='pull-right'><?= $value['Outstanding']; $Outstanding+=$value['Outstanding']; ?></span></td>
                      <td><span class='pull-right'><?= $value['PrePaid']; $PrePaid+=$value['PrePaid']; ?></span></td>
                      <td><span class='pull-right'><?= $value['total']; $total+=$value['total']; ?></span></td>
                    </tr>
                  <?php endif; endforeach; ?>
                </tbody>
                <tfoot>
                  <td><span class='pull-right'>Total</span></td>
                  <td><span class='pull-right'><?= $paid; ?></span></td>
                  <td><span class='pull-right'><?= $Outstanding; ?></span></td>
                  <td><span class='pull-right'><?= $PrePaid; ?></span></td>
                  <td><span class='pull-right'><?= $total; ?></span></td>
                </tfoot>
              </table>
            </td>
            <td></td>
          </tr>
          <tr class="blue-pddng Net_Profit" style="display: <?php if(!$ProfitLoss['Net']['Left']) { echo 'none'; } ?>">
            <td class="padding_left">Net Profit c/d</td>
            <td id='NetLeft' class='text-right'><?= floatval($ProfitLoss['Net']['Left']); ?></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-6">
      <table class="table table-condensed table boder" id="myTable">
        <thead>
          <tr class="blue-bg">
            <th>Particulars</th>
            <th>Amount (AED)</th>
          </tr>
        </thead>
        <tbody>
          <tr class="blue-pddng toggle_class">
            <td>Net Sales</td>
            <td class='type_name text-right' id="NetSale"><?= floatval($ProfitLoss['Sale']['SaleValue']-$ProfitLoss['Sale']['SalesReturn']); ?></td>
          </tr>
          <tr class="blue-pddng single_NetSale" style='display:none;'>
            <td class="padding_left second_line_padding_left">
              <span>Sales</span>
              <span id='SaleValue' class='pull-right'><?= floatval($ProfitLoss['Sale']['SaleValue']); ?></span>
            </td>
            <td></td>
          </tr>
          <tr class="blue-pddng single_NetSale" style='display:none;'>
            <td class="padding_left second_line_padding_left">
              <span>(-)Sales Return</span>
              <span id='SalesReturn' class='pull-right'><?= floatval($ProfitLoss['Sale']['SalesReturn']); ?></span>
            </td>
            <td></td>
          </tr>
         <!--  <tr class="blue-pddng toggle_class">
            <td>Direct Income</td>
            <td class='type_name text-right' id='DirectIncome'><?= floatval($ProfitLoss['Income']['DirectIncome']['amount']); ?></td>
          </tr> -->
          <tr class='single_DirectIncome' style='display:none;'>
            <td>
              <table class="table table-condensed table boder second_line_padding_left" id="DirectIncome_sub_table">
                <thead>
                  <tr>
                    <td>Name</td>
                    <td>Amount</td>
                    <td>Accrued</td>
                    <td>Advance</td>
                    <td>Total</td>
                  </tr>
                </thead>
                <tbody>
                  <?php $Received=0; $Accrued=0; $Advance=0; $total=0; if(isset($ProfitLoss['Income']['DirectIncome']['single'])): foreach ($ProfitLoss['Income']['DirectIncome']['single'] as $key => $value) :  ?>
                  <?php if($value['Total'] || $value['Accrued'] || $value['Advance']) : ?>
                    <tr>
                      <td class="sub_group_name" type_name='Income'><?= $key; ?></td>
                      <td><span class='pull-right'><?= $value['Received']; $Received+=$value['Received']; ?></span></td>
                      <td><span class='pull-right'><?= $value['Accrued']; $Accrued+=$value['Accrued']; ?></span></td>
                      <td><span class='pull-right'><?= $value['Advance']; $Advance+=$value['Advance']; ?></span></td>
                      <td><span class='pull-right'><?= $value['Total']; $total+=$value['Total']; ?></span></td>
                    </tr>
                  <?php endif; endforeach; endif;  ?>
                </tbody>
                <tfoot>
                  <td><span class='pull-right'>Total</span></td>
                  <td><span class='pull-right'><?= $Received; ?></span></td>
                  <td><span class='pull-right'><?= $Accrued; ?></span></td>
                  <td><span class='pull-right'><?= $Advance; ?></span></td>
                  <td><span class='pull-right'><?= $total; ?></span></td>
                </tfoot>
              </table>
            </td>
            <td></td>
          </tr>
          <tr class="blue-pddng" style="display: <?php if(!$ProfitLoss['First']['Right']) { echo 'none'; } ?>">
            <td>Gross Loss c/d</td>
            <td id='FirstRight' class="text-right"><?= floatval($ProfitLoss['First']['Right']); ?></td>
          </tr>
          <tr class="blue-pddng">
            <td class="total_amount">Total</td>
            <td id='FirstTotalRight' class="total_amount text-right"><?= floatval($ProfitLoss['FirstTotal']['Right']); ?></td>
          </tr>
          <tr class="blue-pddng" style="display: <?php if(!$ProfitLoss['Gross']['Right']) { echo 'none'; } ?>">
            <td>Gross Profit b/d</td>
            <td id='GrossRight' class='text-right'><?= floatval($ProfitLoss['Gross']['Right']); ?></td>
          </tr>
         <!--  <tr class="blue-pddng toggle_class">
            <td>Indirect Income </td>
            <td class='type_name text-right' id='IndirectIncome'><?= floatval($ProfitLoss['Income']['IndirectIncome']['amount']); ?></td>
          </tr> -->
          <tr class='single_IndirectIncome' style='display:none;'>
            <td>
              <table class="table table-condensed table boder second_line_padding_left" id="IndirectIncome_sub_table">
                <thead>
                  <tr>
                    <td>Name</td>
                    <td>Amount</td>
                    <td>Accrued</td>
                    <td>Advance</td>
                    <td>Total</td>
                  </tr>
                </thead>
                <tbody>
                  <?php $Received=0; $Accrued=0; $Advance=0; $total=0; foreach ($ProfitLoss['Income']['IndirectIncome']['single'] as $key => $value) : ?>
                  <?php if($value['Total'] || $value['Accrued'] || $value['Advance']) : ?>
                    <tr>
                      <td class="sub_group_name" type_name='Income'><?= $key; ?></td>
                      <td><span class='pull-right'><?= $value['Received']; $Received+=$value['Received']; ?></span></td>
                      <td><span class='pull-right'><?= $value['Accrued']; $Accrued+=$value['Accrued']; ?></span></td>
                      <td><span class='pull-right'><?= $value['Advance']; $Advance+=$value['Advance']; ?></span></td>
                      <td><span class='pull-right'><?= $value['Total']; $total+=$value['Total']; ?></span></td>
                    </tr>
                  <?php endif; endforeach; ?>
                </tbody>
                <tfoot>
                  <td><span class='pull-right'>Total</span></td>
                  <td><span class='pull-right'><?= $Received; ?></span></td>
                  <td><span class='pull-right'><?= $Accrued; ?></span></td>
                  <td><span class='pull-right'><?= $Advance; ?></span></td>
                  <td><span class='pull-right'><?= $total; ?></span></td>
                </tfoot>
              </table>
            </td>
            <td></td>
          </tr>
          <tr class="blue-pddng Net_Loss" style="display: <?php if(!$ProfitLoss['Net']['Right']) { echo 'none'; } ?>">
            <td>Net Loss c/d</td>
            <td id='NetRight' class='text-right'><?= floatval($ProfitLoss['Net']['Right']); ?></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="box-body table-responsive no-padding">
      <div class="col-md-6">
        <table class="table table-condensed table boder second_line_padding_left">
          <tfoot>
            <tr class="blue-pddng">
              <td class="total_amount padding_left">Total</td>
              <td id='SecondTotalLeft' class="total_amount text-right"><?= floatval($ProfitLoss['SecondTotal']['Left']); ?></td>
            </tr>
          </tfoot>
        </table>
      </div>
      <div class="col-md-6">
        <table class="table table-condensed table boder second_line_padding_left">
          <tfoot>
            <tr class="blue-pddng">
              <td class="total_amount">Total</td>
              <td id='SecondTotalRight' class="total_amount text-right"><?= floatval($ProfitLoss['SecondTotal']['Right']); ?></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
</section>
<div id="sub_group_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><span id='sub_group_name'>SALARY PAID</span> :  Sub Group List</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <table class="table table-condensed boder" id="sub_group_modal_table">
            <thead>
              <tr class="blue-bg">
                <th>Account Head</th>
                <th>Amount</th>
                <th>Outstanding</th>
                <th>PrePaid</th>
                <th>Total</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
              <tr>
                <th style="font-size:20px; color:red;text-align:right">Total:</th>
                <td class="dt-body-right" align="right" style="font-size:20px; color:red;"></td>
                <td class="dt-body-right" align="right" style="font-size:20px; color:red;"></td>
                <td class="dt-body-right" align="right" style="font-size:20px; color:red;"></td>
                <td class="dt-body-right" align="right" style="font-size:20px; color:red;"></td>
              </tr>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<?php require('general_journal_transaction_modal.php'); ?>
<script type="text/javascript">
  <?php require('general_journal_transaction_ajax.js'); ?>
</script>
<script type="text/javascript">
  $('.toggle_class').click(function(){
    var id_name=$(this).closest('tr').find('td.type_name').attr('id');
    $('.single_'+id_name).slideToggle();
  });
  $(document).on('click','#get_button',function(){
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    var route_id=$('#route_id').val();
    if(route_id=="")
    {
      $('#route_id').select2("open");
      return false;
    }
    var data={
      from_date:from_date,
      to_date:to_date,
      route_id:route_id,
    }
    $.post( "<?= $this->webroot ?>Reports/ProfitLossReport_ajax",data,function( data ) {
      $('#costValue').text(data.CostofSale.costValue);
      $('#SaleValue').text(data.Sale.SaleValue);
      var NetSale=parseFloat(data.Sale.SaleValue)-parseFloat(data.Sale.SalesReturn);
      $('#NetSale').text(parseFloat(NetSale));
      $('#SalesReturn').text(data.Sale.SalesReturn);
      var SaleValue_SalesReturn=data.Sale.SaleValue-data.Sale.SalesReturn;
      $('#SaleValue_SalesReturn').text(SaleValue_SalesReturn);
      $('#DirectExpense').text(data.Expense.DirectExpense.amount);
      $('#DirectIncome').text(data.Income.DirectIncome.amount);
      $('#IndirectExpense').text(data.Expense.IndirectExpense.amount);
      $('#IndirectIncome').text(data.Income.IndirectIncome.amount);
      $('#FirstTotalLeft').text(data.FirstTotal.Left);
      $('#FirstTotalRight').text(data.FirstTotal.Right);
      $('#GrossRight').text(data.Gross.Right);
      if(!data.Gross.Right)
      {
        $('#GrossRight').closest('tr').hide();
      }
      else
      {
        $('#GrossRight').closest('tr').show();
      }
      $('#FirstLeft').text(data.First.Left);
      if(!data.First.Left)
      {
        $('#FirstLeft').closest('tr').hide();
      }
      else
      {
        $('#FirstLeft').closest('tr').show();
      }
      $('#FirstRight').text(data.First.Right);
      if(!data.First.Right)
      {
        $('#FirstRight').closest('tr').hide();
      }
      else
      {
        $('#FirstRight').closest('tr').show();
      }
      $('#GrossLeft').text(data.Gross.Left);
      if(!data.Gross.Left)
      {
        $('#GrossLeft').closest('tr').hide();
      }
      else
      {
        $('#GrossLeft').closest('tr').show();
      }
      $('#NetLeft').text(data.Net.Left);
      $('#NetRight').text(data.Net.Right);
      if(!data.Net.Right)
      {
        $('#NetRight').closest('tr').hide();
      } 
      else
      {
        $('#NetRight').closest('tr').show();
      }
      if(!data.Net.Left)
      {
        $('#NetLeft').closest('tr').hide();
      } 
      else
      {
        $('#NetLeft').closest('tr').show();
      }
      $('#SecondTotalLeft').text(data.SecondTotal.Left);
      $('#SecondTotalRight').text(data.SecondTotal.Right);
      var Accrued=0;
      var Received=0;
      var Advance=0;
      var total=0;
      $('#IndirectIncome_sub_table tbody').empty();
      $('#IndirectIncome_sub_table tfoot').empty();
      $.each(data.Income.IndirectIncome.single,function(key,value){
        if(value.Total || value.Advance || value.Accrued) 
        {
          var IndirectIncome_sub_table='<tr>';
          IndirectIncome_sub_table+="<td class='sub_group_name' type_name='Income'>"+key+"</td>";
          Received=parseFloat(Received)+parseFloat(value.Received);
          Advance=parseFloat(Advance)+parseFloat(value.Advance);
          Accrued=parseFloat(Accrued)+parseFloat(value.Accrued);
          total=parseFloat(total)+parseFloat(value.Total);
          IndirectIncome_sub_table+="<td><span class='pull-right'>"+value.Received+"</span></td>";
          IndirectIncome_sub_table+="<td><span class='pull-right'>"+value.Accrued+"</span></td>";
          IndirectIncome_sub_table+="<td><span class='pull-right'>"+value.Advance+"</span></td>";
          IndirectIncome_sub_table+="<td><span class='pull-right'>"+value.Total+"</span></td>";
          IndirectIncome_sub_table+="</tr>";
          $('#IndirectIncome_sub_table tbody').append(IndirectIncome_sub_table);
        }
      });
      var IndirectIncome_sub_table='<tr>';
      IndirectIncome_sub_table+="<td><span class='pull-right'>Total</span></td>";
      IndirectIncome_sub_table+="<td><span class='pull-right'>"+Received+"</span></td>";
      IndirectIncome_sub_table+="<td><span class='pull-right'>"+Accrued+"</span></td>";
      IndirectIncome_sub_table+="<td><span class='pull-right'>"+Advance+"</span></td>";
      IndirectIncome_sub_table+="<td><span class='pull-right'>"+total+"</span></td>";
      IndirectIncome_sub_table+="</tr>";
      $('#IndirectIncome_sub_table tfoot').append(IndirectIncome_sub_table);
      var Accrued=0;
      var Advance=0;
      var Received=0;
      var total=0;
      $('#DirectIncome_sub_table tbody').empty();
      $('#DirectIncome_sub_table tfoot').empty();
      $.each(data.Income.DirectIncome.single,function(key,value){
        if(value.Total || value.Advance || value.Accrued) 
        {
          var DirectIncome_sub_table='<tr>';
          DirectIncome_sub_table+="<td class='sub_group_name' type_name='Income'>"+key+"</td>";
          Received=parseFloat(Received)+parseFloat(value.Received);
          Advance=parseFloat(Advance)+parseFloat(value.Advance);
          Accrued=parseFloat(Accrued)+parseFloat(value.Accrued);
          total=parseFloat(total)+parseFloat(value.Total);
          DirectIncome_sub_table+="<td><span class='pull-right'>"+value.Received+"</span></td>";
          DirectIncome_sub_table+="<td><span class='pull-right'>"+value.Accrued+"</span></td>";
          DirectIncome_sub_table+="<td><span class='pull-right'>"+value.Advance+"</span></td>";
          DirectIncome_sub_table+="<td><span class='pull-right'>"+value.Total+"</span></td>";
          DirectIncome_sub_table+="</tr>";
          $('#DirectIncome_sub_table tbody').append(DirectIncome_sub_table);
        }
      });
      var DirectIncome_sub_table='<tr>';
      DirectIncome_sub_table+="<td><span class='pull-right'>Total</span></td>";
      DirectIncome_sub_table+="<td><span class='pull-right'>"+Received+"</span></td>";
      DirectIncome_sub_table+="<td><span class='pull-right'>"+Accrued+"</span></td>";
      DirectIncome_sub_table+="<td><span class='pull-right'>"+Advance+"</span></td>";
      DirectIncome_sub_table+="<td><span class='pull-right'>"+total+"</span></td>";
      DirectIncome_sub_table+="</tr>";
      $('#DirectIncome_sub_table tfoot').append(DirectIncome_sub_table);
      var PrePaid=0;
      var Outstanding=0;
      var paid=0;
      var total=0;
      $('#DirectExpense_sub_table tbody').empty();
      $('#DirectExpense_sub_table tfoot').empty();
      $.each(data.Expense.DirectExpense.single,function(key,value){
        if(value.total || value.Outstanding || value.PrePaid) 
        {
          var DirectExpense_sub_table='<tr>';
          DirectExpense_sub_table+="<td class='sub_group_name' type_name='Expense'>"+key+"</td>";
          paid=parseFloat(paid)+parseFloat(value.paid);
          Outstanding=parseFloat(Outstanding)+parseFloat(value.Outstanding);
          PrePaid=parseFloat(PrePaid)+parseFloat(value.PrePaid);
          total=parseFloat(total)+parseFloat(value.total);
          DirectExpense_sub_table+="<td><span class='pull-right'>"+value.paid+"</span></td>";
          DirectExpense_sub_table+="<td><span class='pull-right'>"+value.Outstanding+"</span></td>";
          DirectExpense_sub_table+="<td><span class='pull-right'>"+value.PrePaid+"</span></td>";
          DirectExpense_sub_table+="<td><span class='pull-right'>"+value.total+"</span></td>";
          DirectExpense_sub_table+="</tr>";
          $('#DirectExpense_sub_table tbody').append(DirectExpense_sub_table);
        }
      });
      var DirectExpense_sub_table='<tr>';
      DirectExpense_sub_table+="<td><span class='pull-right'>Total</span></td>";
      DirectExpense_sub_table+="<td><span class='pull-right'>"+paid+"</span></td>";
      DirectExpense_sub_table+="<td><span class='pull-right'>"+Outstanding+"</span></td>";
      DirectExpense_sub_table+="<td><span class='pull-right'>"+PrePaid+"</span></td>";
      DirectExpense_sub_table+="<td><span class='pull-right'>"+total+"</span></td>";
      DirectExpense_sub_table+="</tr>";
      $('#DirectExpense_sub_table tfoot').append(DirectExpense_sub_table);
      var PrePaid=0;
      var paid=0;
      var Outstanding=0;
      var total=0;
      $('#IndirectExpense_sub_table tbody').empty();
      $('#IndirectExpense_sub_table tfoot').empty();
      $.each(data.Expense.IndirectExpense.single,function(key,value){
        if(value.total || value.Outstanding || value.PrePaid) 
        {
          var IndirectExpense_sub_table='<tr>';
          IndirectExpense_sub_table+="<td class='sub_group_name' type_name='Expense'>"+key+"</td>";
          paid=parseFloat(paid)+parseFloat(value.paid);
          Outstanding=parseFloat(Outstanding)+parseFloat(value.Outstanding);
          PrePaid=parseFloat(PrePaid)+parseFloat(value.PrePaid);
          total=parseFloat(total)+parseFloat(value.total);
          IndirectExpense_sub_table+="<td><span class='pull-right'>"+value.paid+"</span></td>";
          IndirectExpense_sub_table+="<td><span class='pull-right'>"+value.Outstanding+"</span></td>";
          IndirectExpense_sub_table+="<td><span class='pull-right'>"+value.PrePaid+"</span></td>";
          IndirectExpense_sub_table+="<td><span class='pull-right'>"+value.total+"</span></td>";
          IndirectExpense_sub_table+="</tr>";
          $('#IndirectExpense_sub_table tbody').append(IndirectExpense_sub_table);
        }
      });
      var IndirectExpense_sub_table='<tr>';
      IndirectExpense_sub_table+="<td><span class='pull-right'>Total</span></td>";
      IndirectExpense_sub_table+="<td><span class='pull-right'>"+paid+"</span></td>";
      IndirectExpense_sub_table+="<td><span class='pull-right'>"+Outstanding+"</span></td>";
      IndirectExpense_sub_table+="<td><span class='pull-right'>"+PrePaid+"</span></td>";
      IndirectExpense_sub_table+="<td><span class='pull-right'>"+total+"</span></td>";
      IndirectExpense_sub_table+="</tr>";
      $('#IndirectExpense_sub_table tfoot').append(IndirectExpense_sub_table);
    }, "json");
});

$(document).on('click','.sub_group_name',function(){
  var name=$(this).closest('tr').find('td.sub_group_name').text();
  var type_name=$(this).closest('tr').find('td').attr('type_name');
  if(type_name=='Expense')
  {
    var thead='<tr class="blue-bg">';
    thead="<th>Account Head</th>";
    thead+="<th>Amount</th>";
    thead+="<th>Outstanding</th>";
    thead+="<th>PrePaid</th>";
    thead+="<th>Total</th>";
    thead+="</tr>";
  }
  else
  {
    var thead='<tr class="blue-bg">';
    thead="<th>Account Head</th>";
    thead+="<th>Amount</th>";
    thead+="<th>Accrued</th>";
    thead+="<th>Advance</th>";
    thead+="<th>Total</th>";
    thead+="</tr>";
  }
  $('#sub_group_name').text(name);
  $('#sub_group_modal_table thead tr').html(thead);
  $('#sub_group_modal').modal('toggle');
  table = $('#sub_group_modal_table').dataTable();
  table.fnDraw();
});
$('#sub_group_modal_table').DataTable( {
  "processing": true,
  "serverSide": true,
  "paging": false,
  "info": false,
  "searching": false,
  "ajax": {
    "url": "<?= $this->webroot ?>Reports/get_accounthead_by_sub_group_ajax",
    "type": "POST",
    data:function( d ) {
      d.sub_group_name= $('#sub_group_name').text();
      d.from_date= $('#from_date').val();
      d.to_date= $('#to_date').val();
    },
    "dataSrc": "records",
  },
  "columns": [
  { "data" : "name" },
  { "data" : "amount" },
  { "data" : "first" },
  { "data" : "second" },
  { "data" : "total" },
  ],
  "columnDefs": [
  {"targets":[0 ],className:"name"         },
  {"targets":[1 ],className:"dt-body-right"},
  {"targets":[2 ],className:"dt-body-right"},
  {"targets":[3 ],className:"dt-body-right"},
  {"targets":[4 ],className:"dt-body-right"},
  ],
  "footerCallback": function ( row, data, start, end, display ) {
    var api = this.api(), data;
    var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0;};
    pageTotal=api.column(1,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
    $(api.column(1).footer()).html(''+Math.round(pageTotal)+'');
    pageTotal=api.column(2,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
    $(api.column(2).footer()).html(''+Math.round(pageTotal)+'');
    pageTotal=api.column(3,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
    $(api.column(3).footer()).html(''+Math.round(pageTotal)+'');
    pageTotal=api.column(4,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
    $(api.column(4).footer()).html(''+Math.round(pageTotal)+'');
  },
});
</script>