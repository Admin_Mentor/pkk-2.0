
<style type="text/css">
.cls_label_all {
  padding-top: 5%;
}
.row_top_row{
  margin-top: 5%;
}
.deaf_btn_btn {
  margin-left: 4%;
}
.row_new_add{
  margin-top: 2%;
}
#radio_butto_add {
  margin-top: 7px;
  margin-left: -51%;
}
</style>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Sales Report(Product Wise)</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="Stockmanagement">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                <br>
                <div class="row">
                  <div class="col-md-3">
                   <?php echo $this->Form->input('branch_id',array('type'=>'select', 'empty' =>'MAIN','options'=>$Branch,'class'=>'form-control select2 rec_select_box search_field','label'=>'Branch')) ?>
                 </div>
                 <div class="col-md-3">
                   <?php echo $this->Form->input('route_id',array('id'=>'route_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'ALL')); ?>
                 </div>
                 <div class="col-md-3">
                   <?php echo $this->Form->input('executive_id',array('id'=>'executive_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','empty'=>'All')); ?>
                 </div>
                 <div class="col-md-3">
                  <?php echo $this->Form->input('product_type_id',array('type'=>'select','empty' =>'All','options'=>$Product_type,'class'=>'form-control select2','label'=>'Product Type')) ?>
                </div>
                <div class="col-md-3">
                  <br>
                  <?php echo $this->Form->input('brand_id',array('type'=>'select', 'empty' =>' All','options'=>$Brand,'class'=>'form-control select2 rec_select_box search_field','label'=>'Brand')) ?>
                </div>
                <div class="col-md-3">
                  <br>
                  <?php echo $this->Form->input('product_id',array('type'=>'select', 'empty' =>' All','options'=>$Product,'class'=>'form-control select2 rec_select_box search_field','label'=>'Product')) ?>
                </div>
                <div class="col-md-3">
                <div class="col-md-6">
                  <br>
                 <?php echo $this->Form->input('from_date',array(
                  'type'=>'text',
                  'id'=>'from_date',
                  'value'=>$from,
                  'class'=>'form-control cls_label_all date_field date_picker
                  datepicker',
                  'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                  )); ?>
                </div>
                <div class="col-md-6">
                  <br>
                  <?php echo $this->Form->input('to_date',array(
                    'type'=>'text',
                    'id'=>'to_date',
                    'value'=>$to,
                    'class'=>'form-control cls_label_all date_field date_picker
                    datepicker',
                    'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                    )); ?>
                  </div>
                  </div>
                  <div class="col-md-1" style="margin-top: 25px"><br>
                    <button class='btn btn-success' type='button' id='fetch_button'>Get</button>
                  </div>
                  <div class="col-md-2" style="margin-top:47px">
  <button  type="button" class='excel_export btn btn-primary  pull-right' >Excel Export </button>
          </div>

                </div>
              </div>
              <div class="box-body">
                <div class="col-md-4 col-xs-12">
                  <h3 class="muted "></h3>
                </div>
                <table class="table table-condensed table boder table-bordered" id='table_product_wise_list' data-order='[[ 0, "asc" ]]'>
                  <thead>
                    <tr class="blue-bg">
                     <th width="5%">Date</th>
                      <th width="15%">Executive</th>
                       <th width="10%">Customer Name</th>
                      <th width="8%">Route</th>
                      <th width="15%">Product</th>
                       <th class="" width="5%">Quantity</th>
                      <th class="" width="5%">Net Amount</th>
                      <th class="" width="5%">Tax Amount</th>
                      <th class="" width="5%">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td  class="text-right"  colspan="6" style="font-size:20px; font-weight bold; color:#dd4b39;">Total</td>
                      <td class="text-right" style="font-size:20px; font-weight bold; color:#dd4b39;"></td>
                      <!--  <td class="text-right" style="font-size:20px; font-weight bold; color:#dd4b39;"></td> -->
                      <td class="text-right" style="font-size:20px; font-weight bold; color:#dd4b39;"></td>
                      <td class="text-right" style="font-size:20px; font-weight bold; color:#dd4b39;"></td>

                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  //$('#table_product_wise_list').DataTable( );
  $.fn.product_type_change=function(product_type_id){
    var url_address= '<?php echo $this->webroot; ?>'+'Reports/product_type_select_ajax/'+product_type_id;
    $.ajax({
      type: "GET",
      url:url_address,
      dataType:'json',
      success: function(response) {
       $('#product_id').empty();
       $('#product_id').append($("<option></option>").attr("value", '').text('All'));
       $.each(response, function(key, value) {
        $('#product_id').append($("<option></option>").attr("value", key).text(value));
      });
     },
     error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  }
  $('#product_type_id').change(function(){
   // alert();
   var product_type_id=$(this).val();
   $.fn.product_type_change(product_type_id);
    table = $('#table_product_wise_list').dataTable();
    table.fnDraw();
 });
  $('#table_product_wise_list').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/SaleCollectionReportProductwiseAjax",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.product_type_id= $('#product_type_id').val();
        d.to_date= $('#to_date').val();
        d.product_id= $('#product_id').val();
        d.branch_id=$('#branch_id').val();
        d.route_id=$('#route_id').val();
        d.executive_id=$('#executive_id').val();
        d.brand_id=$('#brand_id').val();
      },
      "dataSrc": "records",
    },
    dom: 'Bfrtip',

    lengthMenu: [
    [10, 50,100],
    ['10 rows', '50 rows','100 rows']
    ],
    buttons: [
    { extend: 'colvis', },
    { 
      extend: 'excel',
     exportOptions: { columns: ':visible' } 
   },

    { extend: 'pdf'  ,
      exportOptions: { columns: ':visible' } }, 
    'pageLength',
    ],
    "columns": [
    { "data" : "Sale.date_of_delivered" },
    { "data" : "Executive.name" },
    { "data" : "Customer.name" },
    { "data" : "Route.name" },
    { "data" : "Product.name" },
    { "data" : "SaleItem.quantity" ,className:"text-right"},
    { "data" : "SaleItem.unit_price" ,className:"text-right"},

    { "data" : "SaleItem.tax_amount" ,className:"text-right"},
    { "data" : "SaleItem.total" ,className:"text-right"},
    ],

    "rowCallback": function( row, data ) {
    $.fn.show_alert('Success');

  },
    "footerCallback":function(row,data,start,end,display){
      var api = this.api(), data; var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
      pageTotal=api.column(6,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(6).footer()).html(''+pageTotal+'');
      pageTotal=api.column(7,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(7).footer()).html(''+pageTotal+'');
      pageTotal=api.column(8,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      $(api.column(8).footer()).html(''+pageTotal+'');
      //pageTotal=api.column(6,{page:'current'}).data().reduce(function(a,b){ return intVal(a) + intVal(b); },0);
      //$(api.column(6).footer()).html(''+Math.round(pageTotal)+'');
    },
    "columnDefs": [
    // { className: "dt-body-right", "targets": [ 3 ] },
    // { className: "dt-body-right", "targets": [ 4 ] },
    // { className: "dt-body-right", "targets": [ 5 ] },
    // { className: "dt-body-right", "targets": [ 6 ] },
    //{ "visible": false, "targets": [ 8 ] },
    ],
  });
  $(document).on('click','#fetch_button',function(){
    table = $('#table_product_wise_list').dataTable();
    table.fnDraw();
  });
  $.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
      $(document).on('click','.excel_export',function(){
        var from_date= $('#from_date').val();
        var to_date= $('#to_date').val();
        var product_type_id=null;
        if($('#product_type_id').val())
        {
          var product_type_id= $('#product_type_id').val();
        }
        var product_id=null;
        if($('#product_id').val())
        {
        var product_id= $('#product_id').val();
        }
        var route_id=null;
        if($('#route_id').val())
        {
        var route_id=$('#route_id').val();
        }
        var branch_id=null;
        if($('#branch_id').val())
        {
          var branch_id= $('#branch_id').val();
        }
        var brand_id=null;
        if($('#brand_id').val())
        {
          var brand_id= $('#brand_id').val();
        }
        var executive_id=null;
        if($('#executive_id').val())
        {
          var executive_id= $('#executive_id').val();
        }
        var url='<?= $this->webroot."Reports/sale_report_excel/"; ?>'+from_date+'/'+to_date+'/'+executive_id+'/'+route_id+'/'+product_id+'/'+product_type_id+'/'+brand_id+'/'+branch_id;
     $(location).attr("href", url);
});
</script>
