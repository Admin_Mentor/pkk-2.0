<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Customer Due List Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary box_tp_brdr">
        <div class="row-wrapper">
          <div class="row">
            <?php echo $this->Form->create('Report',array('type'=>'file','url' => array('controller' => 'Reports', 'action' => 'DueListPrint'))) ?> 
            <div class="box-body">
              <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12">
                 <!--  <div class="col-md-3 col-lg-3 col-sm-3">
                    <?= $this->Form->input('executive_id',array('class'=>'form-control select_two_class','type'=>'select','empty'=>'select','id'=>'executive_id')); ?>
                  </div> -->
                  <div class="col-md-3 col-lg-3 col-sm-3">
                    <?= $this->Form->input('customer_id',array('class'=>'form-control select_two_class name','type'=>'select','empty'=>'select','options'=>$Customer_list,'id'=>'customer_id','label'=>'Customer')); ?>
                  </div>
                  <div class="col-md-3 col-lg-3 col-sm-3"><br>
                    <button type="button" class='btn btn-success' id='Fetch_button'>Get</button>
                  </div>
                </div>
                <br/>
              </div>
              <br/>   
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 ">
                </div>
                <!-- <div class="col-md-3 col-sm-4 col-xs-4 ">
                  <span> <button type='submit' class="print">Due List Print </button></span>
                  <?php echo $this->Form->end(); ?>
                </div> -->
                <!-- <div class="col-md-3 col-sm-4 col-xs-4">
                  <button type="button" id="report_print" class="print"  >Invoice Print</button>
                </div> -->
                 <!-- <div class="col-md-3 col-sm-4 col-xs-4">
                  <button type="button" id="sale_print" class="print"  >Due Print</button>
                </div> -->
              </div>
              <br>
              <div class="row-wrapper">
                <div class="row">
                  <div class="col-md-12">
                    <div class="box-body">
                    <table class="table table-condensed table-bordered" id='table_customer_list' data-page-length="10" border="1">
                        <thead>
                          <tr class="blue-bg">
                            <!-- <th>Executive</th> -->
                            <th>Customer Type</th>
                            <th>Customer</th>
                            <th>Outstanding Amount</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th colspan="2" style="font-size:20px; color:red;text-align:right">Total:</th>
                            <th style="font-size:20px; color:red;text-align:right">0</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#report_print').click(function(){
    var customer=$('#customer_id').val();
    var customer_type=$('#customer_type').val();
    if(customer == ''){
      customer=0;
    }
    if(customer_type == ''){
      customer_type=0;
    }
    url='<?= $this->webroot."Reports/account_recievable_print/"; ?>'+customer_type+'/'+customer;
    window.open(url);
  });
  $('#table_customer_list').dataTable( {
    "processing": true,
    "serverSide": true,
    "searching": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/customer_due_list_report_ajax",
      "type": "POST",
      data:function( d ) {
        d.customer_id= $('#customer_id').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "CustomerType.name" },
    { "data" : "AccountHead.name" },
    { "data" : "Customer.Balance_Total",className:"text-right"},
    ],
    "dom": 'Brtip',
    lengthMenu: [
    [10, 25, 50],['10 rows', '25 rows', '50 rows']
    ],
    "buttons": [
    // { extend: 'colvis' },
    // {extend: 'print', footer: true, exportOptions: { columns: ':visible'}, },
    // {
    //   extend: 'csv',
    //   title:'Customer Outstanding Report of '+$('#executive_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
    //   footer: true,
    //   exportOptions: { columns: ':visible'},
    // },
    {
      extend: 'excel',
      title:'Customer Outstanding Report of '+$('#executive_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    {
      extend: 'pdf',
      title:'Customer Outstanding Report of '+$('#executive_id option:selected').text()+ '(Period : From '+$('#from_date').val()+'    To : '+$('#to_date').val()+')',
      footer: true,
      exportOptions: { columns: ':visible'},
    },
    {extend: 'pageLength'},
    ],
    "columnDefs": [
    { className: "text-right", "targets": [ 2 ] },
    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data; var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
      var pagecount=2;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(2))+'');
    },
  });
  $('#Fetch_button').on('click',function(){
    table = $('#table_customer_list').dataTable();
    table.fnDraw();
  });
//   $('#Fetch_button').click(function(){
//     $.fn.table_search();
//   });
//   $.fn.table_search=function(){
//    // var executive_id=$('#executive_id').val();
//     var customer_id=$('#customer_id').val();
//     var data={
//      // executive_id:executive_id,
//       customer_id:customer_id,
//     };
//     var url_address= '<?php echo $this->webroot; ?>'+'Reports/customer_due_report_ajax';
//     $.ajax({
//       type: "post",
//       url:url_address,
//       data: data,
//       dataType:'json',
//       success: function(response) {
//         $('#table__customer_list tbody').empty();
//         //$('#table__customer_list').DataTable().destroy();
//         $('#table__customer_list tbody').html(response.row);
//         $('#table__customer_list tfoot').html(response.tfoot);
//         // if(response.result=='Success')
//         // {
//         //   $.each(response.data,function(key,value){
//         //     var tr='<tr class="blue-pddng">';
//         //    // tr+='<td>'+value.Executive.name+'</td>';
//         //     tr+='<td>'+value.CustomerType.name+'</td>';
//         //     tr+='<td>'+value.AccountHead.name+'</td>';
//         //     tr+='<td>'+value.Customer.Balance_Total+'</td>';
//         //     tr+='</tr>';
//         //     $('#table__customer_list tbody').append(tr);
//         //   });
//         //   $('#table__customer_list').DataTable();
//         // }
//  //$('#table__customer_list').DataTable();
// // $('#table__customer_list').DataTable( {
// //   "ordering": false,
// //   dom: 'Bfrtip',
// //   buttons: [
// //   {
// //     extend: 'colvis',
// //   },
// //   {

// //     extend: 'print',
// // //text: 'Print' ,
// // footer: true,
// // // title: 'Customer Due List',
// // exportOptions: { columns: ':visible' },
// // customize: function ( win ) {
// //   $(win.document.body)
// //   .css( 'font-size', '10pt' )
// //   .prepend(
// // '<h3 align="center">Customer Due List</h3>',
// // );
// //   $(win.document.body).find( 'table' )
// //   .addClass( 'compact' )
// //   .css( 'font-size', 'inherit' )
// // }
// // },
// // {
// //   extend: 'csv',
// // footer: true,
// //  title:'Customer Due List',
// //  customize: function (csv) {
// //                  return "\t   Customer Due List\n"+  csv ;
// //                },

// // exportOptions: { columns: ':visible' },
// // },
// // {
// //   extend: 'excel',
// // footer: true,
// //  title:'Customer Due List',
// // exportOptions: { columns: ':visible' },
// // },
// // {
// //   extend: 'pdf',
// // //text: 'Pdf' ,
// // footer: true,
// //  title:'Customer Due List',
// // exportOptions: { columns: ':visible' },
// // },
// // {
// //   extend: 'pageLength',
// // },
// // ]
// // } );
// $.fn.show_alert('Success');
// },
// error:function (XMLHttpRequest, textStatus, errorThrown) {
//   alert(textStatus);
// }
// });
//   }

//   $.fn.show_alert = function(flash)
//      {
//      // $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
//       }
</script>