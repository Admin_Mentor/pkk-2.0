<style type="text/css">
  .sav_btn {
    margin-top: 27px;
  }
  .sav_btn a {
    color: white;
    background-color: #008d4c;
    padding-top: 6px;
    padding-bottom: 6px;
    padding-left: 13px;
    display: inline-block;
    padding-right: 13px;
    border-radius: 3px !important;
    letter-spacing: 0.6px;
    text-transform: uppercase;
    white-space: nowrap;
  }
  .sav_btn a:hover {
    background-color: #117c4b !important;
    transition: all ease .9s;
    cursor: pointer;
  }

  .bg_print a{
    background-color: #13689e !important;
  }
  #chk_box_div{
    margin-top: 20px;
  }
</style>

<section class="content-header">
  <h1>Sales Outstanding</h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="Stockmanagement">
          <div class="row">
            <div class="col-md-12">
             <div class="col-md-12">
               <br>
               <div class="row">
                <div class="col-md-3">
                  <div class="col-md-12">
                    <?php echo $this->Form->input('customers',array('empty'=>'All','type'=>'select','value'=>$customer_id,'options'=>$Customers,'class'=>'form-control select2','id'=>'customers')); ?>
                  </div>
                </div>
                <div class="col-md-2 col-lg-2 col-sm-2">
                  <div class="form-group m_t_10px">
                    <?php echo $this->Form->input('to_date',array('class'=>'form-control datepicker')); ?>
                  </div>
                </div>
                <?php if($customer_id){ ?>
                <div class="col-md-2 col-lg-2 col-sm-2">
                 <div class="checkbox" id="chk_box_div">
                 <label><input id="chk_days" type="checkbox" value="">Required Pending Days?</label>
                </div>
              </div>
              <div class="col-md-2 col-lg-2 col-sm-2">
               <div class="sav_btn bg_print" id="print_button">
                <a>Print</a>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>

        <div class="box-body">
          <div class="col-md-4 col-xs-12">
            <h3 class="muted "></h3>
          </div>
          <?php if($customer_id){ ?>
          <table class="table table-condensed table " id='SaleOutstandingTbl' data-page-length="-1">
            <thead>
              <tr class="blue-bg">
                <th>Sl No</th>
                <th>Invoice Date</th>
                <th>Invoice Number</th>
                <th>Invoice Amount</th>
                <th>Paid</th>
                <th>Balance</th>
                <th>Days</th>
              </tr>
            </thead>
            <tbody>
              <?php  $i=1; foreach ($Outstanding as $key => $value) { 
                $balance=$value['total']-$value['paid'];
                echo "<tr><td>".$value['sl_no']."</td>";
                echo "<td>".$value['date']."</td>";
                echo "<td>".$value['invoice_no']."</td>";
                echo "<td>".$value['total']."</td>";
                echo "<td>".$value['paid']."</td>";
                echo "<td>".$balance."</td>";
                echo "<td>".$value['days']."</td></tr>";
                $i++; 
              } ?>
            </tbody>
          </table>
          
          <?php } ?>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
</div>
</div>
</div>
</section>

<script type="text/javascript">

  $(document).on('change','#customers,#to_date',function(){
    var id=$('#customers').val();
    var to_date=$('#to_date').val();
    window.location.href='<?php echo $this->webroot ?>Reports/SaleOutstanding1/'+id+'/'+to_date;
  });
   $(document).on('click','#print_button',function(){
    var id=$('#customers').val();
    var to_date=$('#to_date').val();
   
if ($('#chk_days').is(':checked')) {
      var url='<?php echo $this->webroot ?>Reports/OutstandingPrint/'+id+'/'+to_date+'/1';
    }else{
      var url='<?php echo $this->webroot ?>Reports/OutstandingPrint/'+id+'/'+to_date+'/0';
    }
    window.location.href=url;
  });
  $('#SaleOutstandingTbl').DataTable({
   "ordering": false,
    "columnDefs": [ {
          "targets"  : 'no-sort',
            "orderable": false,
          }],
                dom: 'Blfrtip',
                buttons: [
                'print','csv'
                ]
 });
   $('#ChequeOutstandingTbl').DataTable({
   "ordering": false,
    "columnDefs": [ {
          "targets"  : 'no-sort',
            "orderable": false,
          }],
                dom: 'Blfrtip',
                buttons: [
                'print','csv'
                ]
 });
  $('#customers').select2();
</script>


