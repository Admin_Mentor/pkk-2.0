<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>

<section class="content-header">
  <h1>Executive Wise Sales Return Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <br>
        <div class="row">
         <div class="col-md-4">
           <?php echo $this->Form->input('executive_id',array('id'=>'executive_id','style'=>'width:100%','class'=>'form-control select_two_class customer_get_class','required')); ?>
         </div>
         <div class="col-md-3">
           <?php echo $this->Form->input('customer_id',array('id'=>'customer_id','style'=>'width:100%','class'=>'form-control select_two_class','empty'=>'ALL')); ?>
         </div>
         <div class="col-md-2">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('from_date',array(
                      'type'=>'text',
                      'id'=>'from_date',
                      'class'=>'form-control search_field date_picker datepicker',
                      'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12">
                      <?php echo $this->Form->input('to_date',array(
                        'type'=>'text',
                        'id'=>'to_date',
                        'class'=>'form-control  search_field date_picker datepicker',
                        'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                        )); ?>
                      </div>
                    </div>
         <div class="col-md-1"><br>
          <button class='btn btn-success' type='button' id='fetch_button'>Get</button>
        </div>
      </div>
    </div>
    <div class="box-body">
      <div class="col-md-4 col-xs-12">
        <h3 class="muted "></h3>
      </div>
        <table id='table_salesreturn_collection_list' style="width: 100% !important" class="table table-hover boder  table-bordered"  data-order='[[ 2, "desc" ]]'>
        <thead>
          <tr class="blue-bg">
            <th>Route</th>
            <th>Customer</th>
            <th>Net Amount</th>
            <th>Taxable Value</th>
            <th>Tax Amount</th>
            <th>Total</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
         <tfoot>
          <tr>
            <th colspan="2" style="font-size:20px; color:red;text-align:right">Total:</th>
             <th style="font-size:20px; color:red;text-align:right"></th>
              <th style="font-size:20px; color:red;text-align:right"></th>
            <th style="font-size:20px; color:red;text-align:right"></th>
             <th style="font-size:20px; color:red;text-align:right"></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
 $('#table_salesreturn_collection_list').DataTable( {
  "processing": true,
  "serverSide": true,
  "ajax": {
    "url": "<?= $this->webroot ?>Reports/SalesReturnCollectionReport_Ajax",
    "type": "POST",
    data:function( d ) {
      d.from_date= $('#from_date').val();
      d.to_date= $('#to_date').val();
       d.executive_id= $('#executive_id').val();
        d.customer_id= $('#customer_id').val();
      },
      "dataSrc": "records",
    },
      dom: 'Bfrtip',
    lengthMenu: [
   [10, 50,100,200],
    ['10 rows', '50 rows','100 rows','200 rows' ]
    ],
    buttons: [
    { extend: 'colvis', },
    { extend: 'excel', title: 'Executive Sales Return Report', exportOptions: { columns: ':visible' } }, 
   // { extend: 'csv',   title: 'Bill Wise Report', exportOptions: { columns: ':visible' } },
    'pageLength',
    ],
    "columns": [
    { "data" : "Route.name" },
    { "data" : "AccountHead.name"},
    { "data" : "SalesReturn.net_value" ,className:"text-right"},
    { "data" : "SalesReturn.taxable_total" ,className:"text-right"},
    { "data" : "SalesReturn.tax_amount" ,className:"text-right"},
    { "data" : "SalesReturn.grand_total" ,className:"text-right"},
    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;
      var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
        i : 0;
      };
       pageTotal = api.column( 2, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 2 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 3, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 3 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 4, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 4 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 5, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 5 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
    },
    "columnDefs": [],
  });
$('#fetch_button').click(function(){
  table = $('#table_salesreturn_collection_list').dataTable();
  table.fnDraw();
});
</script>
