<style type="text/css">
	.cls_label_all {
		
		white-space: nowrap;
	}
	.row_top_row{
		margin-top: 5%;
	}
	.deaf_btn_btn {
		margin-left: -6%;
	}
	.row_new_add{
		margin-top: 2%;
	}
	#radio_butto_add {
		margin-top: 7px;
		margin-left: -51%;
	}
	.t_date
	{
		margin-left: -29px !important;
	}
	.label_typ_pro
	{
		padding-top: 5px;
	}
</style>
<section class="content-header">
	<h1> Stock Transaction Report </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="Stockmanagement">
					<div class="row">
						<div class="col-md-12">
							<div class="row row_new_add">
								<div class="col-md-12">
									<div class="col-md-8 col-md-offset-2" >
										<div class="col-md-2"><label class="control-label cls_label_all radio_button_keyup">Product Wise</label></div>
										<div class="col-md-2"><input type="radio" checked='checked' value='product' name="sorting"> </div>
										<div class="col-md-2"><label class="control-label cls_label_all radio_button_keyup">All Product</label></div>
										<div class="col-md-2"><input type="radio"  name="sorting" value='date'> </div>
										<div class="col-md-2"><label class="control-label cls_label_all radio_button_keyup">Opening Stock</label></div>
										<div class="col-md-2"><input type="radio"  name="sorting" value='opening_stock'> </div>
									</div>
								</div>
							</div>
							<br>
							<div class="col-md-4">
								<div class="col-md-5"><label class="control-label cls_label_all label_typ_pro">Product Type</label></div>
								<div class="col-md-7">
									<?php echo $this->Form->input('product_type_id',array('type'=>'select','empty' =>'Select','options'=>$Product_type,'class'=>'form-control select2','label'=>false)) ?>
								</div>
							</div>
							<div class="col-md-5">
								<div class="col-md-3"><label class="control-label cls_label_all label_typ_pro">Product</label></div>
								<div class="col-md-9">
									<?php echo $this->Form->input('product_id',array('type'=>'select', 'empty' =>' Select','options'=>$Product,'class'=>'form-control select2 rec_select_box','label'=>false)) ?>
								</div>
							</div>
							<div class="col-md-3">
								<div class="col-md-4"><label class="control-label cls_label_all label_typ_pro">Batch No</label></div>
								<div class="col-md-8">
									<select id='batch_no' class='form-control select2 rec_select_box'>
										<option value=''>Select</option>
									</select>
								</div>
							</div>
							<div class="row row_top_row">
								<div class="col-md-12">
									<div class="col-md-4">
										<div class="col-md-5"><label class="control-label cls_label_all label_typ_pro">From Date</label></div>  
										<div class="col-md-7"><input type="text" class="form-control  cls_label_all date_field" id="from_date" value=<?php echo $date1 ?>></div>
									</div>
									<div class="col-md-4">
										<div class="col-md-5"><label class="control-label cls_label_all label_typ_pro">To Date</label></div>  
										<div class="col-md-7"><input type="text" class="form-control  cls_label_all date_field t_date" id="to_date" value=<?php echo $date1 ?>></div>
									</div>
									<div class="col-md-4">
									</div>
								</div>
							</div>
							<div class="box-body">
								<div class="col-md-4 col-xs-12">
									<h3 class="muted "></h3>
								</div>
								<table class="table table-condensed" id='table_stock_list' data-page-length='100'>
									<thead>
										<tr class="blue">
											<th>Products ID</th>
											<th>Products</th>
											<th>Batch No</th>
											<th>Product Type</th>
											<th>Quantity</th>
											<th>Unit</th>
											<th>Threshold</th>
											<th>Remark</th>
											<th>Date</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($StockLog as $key => $value) { ?>
											<tr class="blue-pd">
												<td><?php echo $value['Product']['product_id']; ?></td>
												<td><?php echo $value['Product']['product_name'] ?></td>
												<td><?php echo $value['StockLog']['batch_no']; ?></td>
												<td><?php echo $value['ProductType']['product_type']; ?></td>
												<td><?php echo $value['StockLog']['product_qty']; ?></td>
												<td><?php echo $value['Unit']['unit_name']; ?></td>
												<td><?php echo $value['Product']['threashold']; ?></td>
												<td><?php echo $value['StockLog']['remark']; ?></td>
												<td><?php echo date('d-m-Y',strtotime($value['StockLog']['created'])); ?></td>
											</tr>
											<?php } ?>	
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#table_stock_list').DataTable({
				"order" :[]
			});
			$('.date_field').daterangepicker({
				format: "DD-MM-YYYY",
				singleDatePicker: true,
				calender_style: "picker_4"
			});
			$('#product_type_id').select2();
			$('#product_id').select2();
			$('#batch_no').select2();
			$.fn.product_type_change=function(product_type_id){
				var data={product_type_id:product_type_id};
				var url_address= '<?php echo $this->webroot; ?>'+'Report/product_type_select_ajax';
				$.ajax({
				type: "post",
				url:url_address,
				data: data,
				// dataType:'json',
				success: function(response) {
					$('#product_id').html(response);
				},
				error:function (XMLHttpRequest, textStatus, errorThrown) {
					alert(textStatus);
				}
			});
				$.fn.search();
			}
			$.fn.product_change=function(product_id){
				var data={product_id:product_id};
				var url_address= '<?php echo $this->webroot; ?>'+'Report/batch_select_ajax';
				$.ajax({
				type: "post",
				url:url_address,
				data: data,
				// dataType:'json',
				success: function(response) {
					$('#batch_no').html(response);
				},
				error:function (XMLHttpRequest, textStatus, errorThrown) {
					alert(textStatus);
				}
			});
				$.fn.search();
			}
			$('#product_type_id').change(function(){
				var product_type_id=$(this).val();
				$('#product_id').select2().select2('val', $('option:eq(0)').val());
				$.fn.product_type_change(product_type_id);
			});
			$('#showallbutton').click(function(){
				$('input[type="search"]').val('');
				$('#product_type_id').select2().select2('val', $('option:eq(0)').val());
				$('#product_id').select2().select2('val', $('option:eq(0)').val());
				$('#batch_no').val('');
				var url_address= '<?php echo $this->webroot; ?>'+'Report/stock_report_check_ajax_all';
				$.ajax({
					type: "post",
					url:url_address,
					// data: data,
					// dataType:'json',
					success: function(response) {
						$('#table_stock_list').DataTable().destroy();
						$('#table_stock_list tbody').html(response);
						$('#table_stock_list').DataTable();
					},
					error:function (XMLHttpRequest, textStatus, errorThrown) {
						alert(textStatus);
					}
				});
			});
			$.fn.search = function() {
				var product_type_id=$('#product_type_id option:selected').val();
				var product_id=$('#product_id option:selected').val();
				var batch_no=$('#batch_no option:selected').val();
				var from_date=$('#from_date').val();
				var to_date=$('#to_date').val();	
				var sorting=$('input[name="sorting"]:checked').val();				
				var data={sorting:sorting,product_type_id:product_type_id,product_id:product_id,batch_no:batch_no,to_date:to_date,from_date:from_date};
				var url_address= '<?php echo $this->webroot; ?>'+'Report/stock_report_serach_ajax';
				$.ajax({
			type: "post",
			url:url_address,
			data: data,
			success: function(response) {
				$('#table_stock_list').DataTable().destroy();
				$('#table_stock_list tbody').html(response);
				$('#table_stock_list').DataTable();
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
			};
			$('#product_id').change(function(){
				var product_id=$(this).val();
				$.fn.product_change(product_id);
				$('#batch_no').select2().select2('val', $('option:eq(0)').val());
				$.fn.search();
			});
			$( "#batch_no" ).change(function(){
				$.fn.search();
			});
			$('#from_date').change(function(){
				$.fn.search();
			});
			$('#to_date').change(function(){
				$.fn.search();
			});
			$('input[name="sorting"]').change(function(){	
				var sorting=$(this).val();
				if(sorting=='date')
				{
					$('#product_type_id').attr('disabled',true);
					$('#product_type_id').select2().select2('val', $('option:eq(0)').val());
					$('#product_id').attr('disabled',true);
					$('#product_id').select2().select2('val', $('option:eq(0)').val());
					$('#batch_no').attr('disabled',true);
					$('#batch_no').select2().select2('val', $('option:eq(0)').val());
					$('#to_date').attr('disabled',false);
				}
				if(sorting=='product')
				{
					$('#product_type_id').attr('disabled',false);
					$('#product_id').attr('disabled',false);
					$('#batch_no').attr('disabled',false);
					$('#to_date').attr('disabled',false);
				}
				if(sorting=='opening_stock')
				{
					$('#product_type_id').attr('disabled',true);
					$('#product_type_id').select2().select2('val', $('option:eq(0)').val());
					$('#product_id').attr('disabled',true);
					$('#product_id').select2().select2('val', $('option:eq(0)').val());
					$('#batch_no').attr('disabled',true);
					$('#batch_no').select2().select2('val', $('option:eq(0)').val());
					$('#to_date').attr('disabled',true);
				}
				$.fn.search();
			});
		});
	</script>