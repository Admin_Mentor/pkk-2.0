<section class="content-header">
  <h1> Stock vs Purchase Report </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header"> 
    </div>
    <div class="box-body"> 
      <?= $this->Form->create('StockPurchaseReport'); ?>
      <div class="row">
        <div class="col-md-2 col-sm-2">
          <div class="form-horizontal">
            <div class="form-group">
              <div class="col-sm-12 col-md-12">
                <?php echo $this->Form->input('product_type_id',
                  array('type'=>'select',
                   'empty' =>'All',
                   'options'=>$ProductType,
                   'style'=>'width: 100%;',
                   'class'=>'form-control select2 select2-hidden-accessible',
                   'label'=>'Product Type')) ?>
                 </div>
               </div>
             </div>
           </div>
           <div class="col-md-3 col-sm-3">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-sm-12 col-md-12">
                  <?php echo $this->Form->input('product_id',
                    array('type'=>'select',
                     'empty' =>'All',
                     'options'=>$Product,
                     'style'=>'width: 100%;',
                     'class'=>'form-control select2 select2-hidden-accessible',
                     'label'=>'Product')) ?>
                   </div>
                 </div>
               </div>
             </div>
             <div class="col-md-3 col-sm-3">
              <div class="form-horizontal">
                <div class="form-group">
                  <div class="col-sm-12 col-md-12">
                    <?php echo $this->Form->input('product_id',
                      array('type'=>'select',
                       'empty' =>'All',
                       'options'=>$Product,
                       'style'=>'width: 100%;',
                       'class'=>'form-control select2 select2-hidden-accessible',
                       'label'=>'Product')) ?>
                     </div>
                   </div>
                 </div>
               </div>
               <div class="col-md-2 col-sm-2">
                <div class="form-horizontal">
                  <div class="form-group">
                    <div class="col-sm-12 col-md-12">
                      <?php echo $this->Form->input('from_date',
                        array('type'=>'text',
                          'class'=>'form-control pull-right date_picker datepicker',
                          'id'=>'from_date',
                          'data-inputmask'=>"'alias': 'dd-mm-yyyy'",
                          'data-mask'=>'data-mask',)); ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-2">
                    <div class="form-horizontal">
                      <div class="form-group">
                        <div class="col-sm-12 col-md-12">
                          <?php echo $this->Form->input('to_date',
                            array('type'=>'text',
                              'class'=>'form-control pull-right date_picker datepicker',
                              'id'=>'to_date',
                              'data-inputmask'=>"'alias': 'dd-mm-yyyy'",
                              'data-mask'=>'data-mask',)); ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3 col-sm-3">
                      <div class="form-horizontal">
                        <div class="form-group">
                          <label class="col-sm-5 col-md-5 control-label">Select Product</label>
                          <div class="col-sm-7 col-md-7">
                            <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true">
                              <option selected="selected">Product 1</option>
                              <option>Product 2</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-2 pull-right mar-tp-5">
                      <button class="print" data-toggle="modal" data-target="#print">Print</button>
                    </div>
                  </div>
                  <?= $this->Form->end(); ?>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="box-body table-responsive no-padding">
                        <table class="table table-hover boder">
                          <tbody>
                            <tr class="blue-bg">
                              <th>PO No</th>
                              <th>PO Date</th>
                              <th>Party Name</th>
                              <th>Items</th>
                              <th>Qty</th>
                              <th>Rate/Unit</th>
                              <th>Total</th>
                              <th>View Invoice</th>
                              <th>Old Stock</th>
                              <th>New Stock</th>
                              <th>Payment Status</th>
                              <th>Balance</th>
                              <th>Details</th>
                            </tr>
                            <tr>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                              <td></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>