<style type="text/css">
	@import url(http://fonts.googleapis.com/css?family=Roboto:400,300);
	button:focus {
		outline: none;
	}
	button:hover {
		opacity: .8;
	}
	.fa {
		font-size: 20px;
	}
	.fa-info {
		color: white;
	}
	#container_card {
		width: 330px;
		height: 350px;
		max-width: 330px;
		background: white;
		position: relative;
		top: 50%;
		left: 50%;
		overflow: hidden;
		box-shadow: 0 5px 15px 0 rgba(0,0,0,0.25);
		transform: translate3d(-50%, -00%, 0);
	}
	h2 {
		padding: 20px;
		color: white;
		background: #3E4FB2;
		font-weight: 300;
		text-align: center;
		font-size: 18px;
		font-family: 'Roboto', sans-serif;
	}
	.detail {
		color: #777;
		padding: 20px;
		line-height: 1.5;
		font-family: 'Roboto', sans-serif;
	}
	.img-wrapper {
		padding: 0;
		position: relative;
	}
	.img-wrapper:after {
		content: "";
		position: absolute;
		left: 0;
		top: 0;
		right: 0;
		bottom: 0;
		background: rgba(62, 79, 178, .25);
		width: 100%;
	}
	.img-wrapper img {
		width: 100%;
		height: 200px;
		-o-object-fit: cover;
		object-fit: cover;
		margin: 0;
		display: block;
		position: relative;
	}
	.button-wrapper {
		width: 50px;
		height: 100%;
		position: absolute;
		bottom: 0;
		right: 0;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		-webkit-transform-origin: 50% 50%;
		-ms-transform-origin: 50% 50%;
		transform-origin: 50% 50%;
		right: 20px;
		bottom: 20px;
	}
	button {
		width: 50px;
		height: 50px;
		border: none;
		border-radius: 50%;
		cursor: pointer;
		box-shadow: 0 1px 3px 0 rgba(0,0,0,0.4);
		z-index: 9;
		position: relative;
	}
	.main-button {
		background: #ff2670;
		-webkit-align-self: flex-end;
		-ms-flex-item-align: end;
		align-self: flex-end;
	}
	.ripple {
		position: absolute;
		left: 0;
		right: 0;
		bottom: 0;
		top: 0;
		border-radius: 50%;
		z-index: -9;
		background: transparent;
		border: 1px solid #ff2670;
		-webkit-transform: scale(.5);
		-ms-transform: scale(.5);
		transform: scale(.5);
		-webkit-transition: .3s all ease;
		transition: .3s all ease;
		opacity: 1;
	}
	.rippling {
		-webkit-animation: .3s rippling 1;
		animation: .3s rippling 1;
		-moz-animation: .3s rippling 1;
	}
	@-webkit-keyframes rippling {
		25% {
			-webkit-transform: scale(1.5);
			transform: scale(1.5);
			opacity: 1;
		}
		100% {
			-webkit-transform: scale(2);
			transform: scale(2);
			opacity: 0;
		}
	}
	@-moz-keyframes rippling {
		25% {
			-moz-transform: scale(1.5);
			transform: scale(1.5);
			opacity: 1;
		}
		100% {
			-moz-transform: scale(2);
			transform: scale(2);
			opacity: 0;
		}
	}
	@keyframes rippling {
		25% {
			transform: scale(1.5);
			opacity: 1;
		}
		100% {
			transform: scale(2);
			opacity: 0;
		}
	}
	.layer {
		position: absolute;
		left: 0;
		right: 0;
		bottom: 0;
		width: 50px;
		height: 50px;
		background: #ff2670;
		border-radius: 50%;
		z-index: -99;
		pointer-events: none;
	}
	.button-wrapper.clicked {
		-webkit-transform: rotate(90deg) translateY(-96px);
		-ms-transform: rotate(90deg) translateY(-96px);
		transform: rotate(90deg) translateY(-96px);
		right: 0;
		bottom: 0;
		-webkit-transition: .3s all ease .6s;
		transition: .3s all ease .6s;
	}
	.button-wrapper.clicked .main-button {
		opacity: 0;
		-webkit-transition: .3s all ease .3s;
		transition: .3s all ease .3s;
	}
	.button-wrapper.clicked .layer {
		-webkit-transform: scale(100);
		-ms-transform: scale(100);
		transform: scale(100);
		-webkit-transition: 2.25s all ease .9s;
		transition: 2.25s all ease .9s;
	}
	.layered-content {
		position: absolute;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		display: -webkit-box;
		display: -webkit-flex;
		display: -ms-flexbox;
		display: flex;
		-webkit-box-align: center;
		-webkit-align-items: center;
		-ms-flex-align: center;
		align-items: center;
		-webkit-box-pack: center;
		-webkit-justify-content: center;
		-ms-flex-pack: center;
		justify-content: center;
		opacity: 0;
	}
	.layered-content.active {
		opacity: 1;
	}
	.close-button {
		background: white;
		position: absolute;
		right: 20px;
		top: 20px;
		color: #ff2670;
	}
	.layered-content.active .close-button {
		-webkit-animation: .5s bounceIn;
		animation: .5s bounceIn;
	}
	.layered-content .content img {
		width: 80px;
		-webkit-shape-outside: 80px;
		shape-outside: 80px;
		border-radius: 50%;
		display: block;
		margin: 0 auto 15px;
		padding: 10px;
		box-sizing: border-box;
		background: white;
		box-shadow: 0 1px 3px 0 rgba(0,0,0,0.4);
		-webkit-transition: .3s all ease;
		transition: .3s all ease;
	}
	.content p {
		color: white;
		font-weight: 300;
		text-align: center;
		line-height: 1.5;
		font-family: 'Roboto', sans-serif;
	}
	.content p a {
		font-size: 12px;
		background: white;
		padding: 2.5px 5px;
		color: #ff2670;;
		text-decoration: none;
		border-radius: 50px;
		display: inline-block;
		margin-left: 1.5px;
	}
	.content img,
	.content p {
		opacity: 0;
		position: relative;
		top: -7.5px;
	}
	.layered-content.active .content img {
		opacity: 1;
		top: 0;
		-webkit-transition: .5s all ease .5s;
		transition: .5s all ease .5s;
	}
	.layered-content.active .content p {
		opacity: 1;
		top: 0;
		-webkit-transition: .5s all ease 1s;
		transition: .5s all ease 1s;
	}
</style>
<section class="content-header">
	<h1> Summary </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="Stockmanagement">
					<div class="row">
						<div class="col-md-12">
							<div class="row row_top_row">
								<div class="col-md-12">
									<div class="col-md-4">
										<div class="col-md-5"><label for="inputEmail3" class="control-label cls_label_all">From Date</label></div>  
										<div class="col-md-4">
											<?php echo $this->Form->input('from_date',array('class'=>'form-control  cls_label_all date_field date_picker','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','id'=>'from_date')); ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="col-md-5"><label for="inputEmail3" class="control-label cls_label_all">To Date</label></div>  
										<div class="col-md-4">
											<?php echo $this->Form->input('to_date',array('class'=>'form-control  cls_label_all date_field date_picker','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','id'=>'to_date')); ?>
										</div>
									</div>
									<div class="col-md-4">
										<!-- <button id='SummaryPrint'>Print</button> -->
									</div>
								</div>
							</div>
							<div class="box-body">
							</div>
							<section class="col-lg-4 connectedSortable">
								<div class="box box-solid">
									<main id="container_card" class="dashboardCard">
										<h2>Sale</h2>
										<div class="img-wrapper">
										</div>
										<div class="button-wrapper" id='button-wrapper-Sale'>
											<div class="layer"></div>
											<button class="main-button fa fa-info" id='Sale'>
												<div class="ripple" id='ripple-Sale'></div>
											</button>
										</div>
										<div class="layered-content" id="layered-content-Sale">
											<button class="close-button fa fa-times" id='Sale'></button>
											<div class="content">
												<h3 align="center">Sale</h3>
												<p>Sale Total :  <span class='indian_number_format' id='SaleTotal'><?= $SaleTotal['SaleCompletedTotal']; ?></span></p>
												<p>Sale Landing Cost : <span class='indian_number_format' id='SaleProductCost'><?= $SaleSparePartTotal['SaleProductCost']; ?></span></p>
												<p>Return : <span class='indian_number_format' id='SaleReturnTotal'><?= $SaleReturnTotal; ?></span></p>
												<p>Return Landing Cost : <span class='indian_number_format' id='SaleReturnProductCost'><?= $SaleReturnSparePartTotal['SaleReturnProductCost']; ?></span></p>
												<p><b>Sale Profit : <span class='indian_number_format' id='SaleProfit'><?= $SaleTotal['SaleCompletedTotal']-$SaleSparePartTotal['SaleProductCost']; ?></span></b></p>
												<!--<p>Cash Recieved : <span class='indian_number_format' id='SalesPaid'><?= $SaleTotal['SalesPaid']; 
												?></span></p>
												<p>Balance Amount : <span class='indian_number_format' id='SalesTopay'><?= $SaleTotal['SalesTopay']; ?></span></p>-->
											</div>
										</div>
									</main>
								</div>
								<div class="box box-solid">
									<main id="container_card" class="dashboardCard">
										<h2>Income</h2>
										<div class="img-wrapper">
										</div>
										<div class="button-wrapper" id="button-wrapper-Income">
											<div class="layer"></div>
											<button class="main-button fa fa-info" id='Income'>
												<div class="ripple" id="ripple-Income"></div>
											</button>
										</div>
										<div class="layered-content" id="layered-content-Income">
											<button class="close-button fa fa-times" id='Income'></button>
											<div class="content">
												<h3 align="center">Income</h3>
												<p>Income Total: <span class='indian_number_format' id='IncomeTotal'>0</span></p>
												<!-- <p>Income Total: <span class='indian_number_format' id='IncomeTotal'><?= $IncomeTotal; ?></span></p> -->
											</div>
										</div>
									</main>
								</div>
								<div class="box  box-solid">
								</div>
							</section>
							<section class="col-lg-4 connectedSortable">
								<div class="box box-solid">
									<main id="container_card" class="dashboardCard">
										<h2>Purchase</h2>
										<div class="img-wrapper">
										</div>
										<div class="button-wrapper" id='button-wrapper-Purchase'>
											<div class="layer"></div>
											<button class="main-button fa fa-info" id='Purchase'>
												<div class="ripple" id='ripple-Purchase'></div>
											</button>
										</div>
										<div class="layered-content" id="layered-content-Purchase">
											<button class="close-button fa fa-times" id='Purchase'></button>
											<div class="content">
												<h3 align="center">Purchase</h3>
												<p>Purchase Total : <span class='indian_number_format' id='PurchaseTotal'><?= $PurchaseTotal; ?></span></p>
												<p>Purchase Return Total : <span class='indian_number_format' id='PurchaseReturnTotal'><?= $PurchaseReturnTotal; ?></span></p>
												<!-- <p>Amount Paid : <span class='indian_number_format' id='AccountPayable'><?= $AccountPayable; ?></span></p> -->
											</div>
										</div>
									</main>
								</div>
								<div class="box  box-solid">
									<main id="container_card" class="dashboardCard">
										<h2>Expense</h2>
										<div class="img-wrapper">
										</div>
										<div class="button-wrapper" id="button-wrapper-Expance">
											<div class="layer"></div>
											<button class="main-button fa fa-info" id='Expance'>
												<div class="ripple" id="ripple-Expance"></div>
											</button>
										</div>
										<div class="layered-content" id="layered-content-Expance">
											<button class="close-button fa fa-times" id='Expance'></button>
											<div class="content">
												<h3 align="center">Expense</h3>
												<p>Expense Total: <span class='indian_number_format' id='ExpanceTotal'>0</span></p>
												<p>Purchase Expense : <span class='indian_number_format' id='ExpancePurchaseTotal'>0</span></p>
												<!-- <p>Expense Total: <span class='indian_number_format' id='ExpanceTotal'><?= $ExpanceTotal['Total']; ?></span></p>
												<p>Purchase Expense : <span class='indian_number_format' id='ExpancePurchaseTotal'><?= $ExpanceTotal['PurchaseTotal']; ?></span></p> -->
											</div>
										</div>
									</main>
								</div>
								<div class="box  box-solid">
								</div>
							</section>
							<section class="col-lg-4 connectedSortable">
									<!-- <div class="box  box-solid">
									<main id="container_card" class="dashboardCard">
										<h2>Service</h2>
										<div class="img-wrapper">
										</div>
										<div class="button-wrapper" id="button-wrapper-Service">
											<div class="layer"></div>
											<button class="main-button fa fa-info"  id='Service'>
												<div class="ripple" id="ripple-Service"></div>
											</button>
										</div>
										<div class="layered-content" id='layered-content-Service'>
											<button class="close-button fa fa-times" id='Service'></button>
											<div class="content">
												<h3 align="center">Service</h3>
												<p>Spare Parts : <span class='indian_number_format' id='ServiceSparePartsAmount'><?= $ServiceSparePartTotal['SparePartsListAmount']; ?></span></p>
												<p>Spare Parts Landing Cost : <span class='indian_number_format' id='ServiceSparePartsLanding'><?= $ServiceSparePartTotal['ProductCost']; ?></span></p>
												<!-- ********************************updated by Jasim************************************* -->
												<!--  <p><b>Spare Parts Profit : <span class='indian_number_format' id='ServiceSparePartsProfit'><?= $ServiceSparePartTotal['SparePartsListAmount']-$ServiceSparePartTotal['ProductCost']; ?></span><b></p>
												<p>Labour Charge Total : <span class='indian_number_format' id='LabourCharge'><?= $LabourChargeTotal; ?></span></p>
												<p>Total Discount : <span class='indian_number_format' id='ServiceDiscountTotal'><?= $ServiceAmountTotal['ServiceDiscountTotal']; ?></span></p>
												<!-- ********************************updated by Jasim************************************* -->
												<!-- <p>Service Total : <span class='indian_number_format' id='ServiceGrandTotal'><?= $ServiceAmountTotal['ServiceGrandTotal']; ?></span></p>
												<p><b>Service Profit : <span class='indian_number_format' id='ServiceTotalProfit'><?= $ServiceTotalProfit; ?></span><b></p>
												<p>Amount Recieved : <span class='indian_number_format' id='ServiceRecievedTotal'><?= $ServiceAmountTotal['ServiceRecievedTotal']; ?></span></p>
												<p>Balance Amount : <span class='indian_number_format' id='ServiceBalanceTotal'><?= $ServiceAmountTotal['ServiceBalanceTotal']; ?></span></p>
											</div>
										</div>
									</main>
								</div> -->  
								<div class="box  box-solid">
									<main id="container_card" class="dashboardCard">
										<h2>Cash</h2>
										<div class="img-wrapper">
										</div>
										<div class="button-wrapper" id="button-wrapper-Profit">
											<div class="layer"></div>
											<button class="main-button fa fa-info" id='Profit'>
												<div class="ripple" id="ripple-Expance"></div>
											</button>
										</div>
										<div class="layered-content" id="layered-content-Profit">
											<button class="close-button fa fa-times" id='Profit'></button>
											<div class="content">
												<h3 align="center">Cash</h3>
												<?php //$TotalSaleProfit=$SaleTotal['SaleCompletedTotal']-$SaleSparePartTotal['SaleProductCost']; 
												$IncomeTotal=0;
												$ExpanceTotal['Total']=0;
												$ServiceReceiptCash=0;
												?>
												<p>Opening Balance : <span class='indian_number_format' id='opening_balance'><?= $opening_balance; ?></span></p>
												<p>Sale : <span class='indian_number_format' id='TotalSaleProfit'><?= $ReceiptCash; ?></span></p>
												<!-- <p>Service: <span class='indian_number_format' id='ServiceReceiptCash'><?= $ServiceReceiptCash; ?></span></p> -->
												<p>Income : <span class='indian_number_format' id='TotalIncome'><?= $IncomeTotal; ?></span></p>
												<p>Expense : <span class='indian_number_format' id='TotalExpance'><?= $ExpanceTotal['Total']; ?></span></p>
												<p>Purchase: <span class='indian_number_format' id='TotalPurchase'><?= $PaymentCash; ?></span></p>
												<?php $TotalProfit=$ReceiptCash+$ServiceReceiptCash+$IncomeTotal-$PaymentCash-$ExpanceTotal['Total'];  ?>
												<p><b>Total Collection : <span class='indian_number_format' id='TotalCollection'><?= $TotalProfit; ?></span></b></p>
												<p>Closing Balance : <span class='indian_number_format' id='closing_balance'><?= $opening_balance+$closing_balance; ?></span></p>
											</div>
										</div>
									</main>
								</div>
							</section>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	<?php // require('SummaryPrint.js'); ?>
	$(document).ready(function(){
		$('#table_stock_list').DataTable();
		var d = new Date();
		var month = d.getMonth()+1;
		var day = d.getDate();
		var output = (day<10 ? '0' : '') + day +'-'+
		(month<10 ? '0' : '') + month +'-'+
		d.getFullYear() ;
		$("[data-mask]").inputmask().val(output);
		$('#showallbutton').click(function(){
		});
		$.fn.search = function() {
			var from=$('#from_date').val();
			var to=$('#to_date').val();
			var data=
			{
				to:to,
				from:from,
			};
			var url_address= '<?php echo $this->webroot; ?>'+'Reports/SummaryAjax';
			$.ajax({
			type: "post",  // Request method: post, get
			url:url_address,
			data: data,  // post data
			dataType:'json',
			success: function(response) {
				// ********************************updated by Jasim  Code for Service Tab*************************************
				$('#ServiceSparePartsAmount').text(response.ServiceSparePartTotal.SparePartsListAmount);
				$('#ServiceSparePartsLanding').text(response.ServiceSparePartTotal.ProductCost);
				var ServiceSparePartsProfit=response.ServiceSparePartTotal.SparePartsListAmount-response.ServiceSparePartTotal.ProductCost;
				$('#ServiceSparePartsProfit').text(ServiceSparePartsProfit);
				$('#LabourCharge').text(response.LabourChargeTotal);
				$('#ServiceDiscountTotal').text(response.ServiceAmountTotal.ServiceDiscountTotal);
				var ServiceTotalProfit=ServiceSparePartsProfit+response.LabourChargeTotal-response.ServiceAmountTotal.ServiceDiscountTotal;
				$('#ServiceGrandTotal').text(response.ServiceAmountTotal.ServiceGrandTotal);
				$('#ServiceTotalProfit').text(ServiceTotalProfit);
				$('#ServiceRecievedTotal').text(response.ServiceAmountTotal.ServiceRecievedTotal);
				$('#ServiceBalanceTotal').text(response.ServiceAmountTotal.ServiceBalanceTotal);
				// ********************************updated by Jasim  Code for Service Tab*************************************
				// ********************************Code for Sale Tab*************************************
				$('#SaleTotal').text(response.SaleCompletedTotal['SaleCompletedTotal']);
				$('#SalesPaid').text(response.SaleCompletedTotal['SalesPaid']);
				$('#SalesTopay').text(response.SaleCompletedTotal['SalesTopay']);
				$('#SaleProductCost').text(response.SaleSparePartTotal.SaleProductCost);
				$('#SaleReturnTotal').text(response.SaleReturnTotal);
				$('#SaleReturnProductCost').text(response.SaleReturnSparePartTotal.SaleReturnProductCost);
				var SaleProfit=response.SaleCompletedTotal['SaleCompletedTotal']-response.SaleSparePartTotal.SaleProductCost;
				$('#SaleProfit').text(SaleProfit);
				// ********************************Code for Sale Tab*************************************
				// ********************************Code for Expence Tab*************************************
				$('#ExpanceTotal').text(response.ExpanceTotal.Total);
				$('#ExpancePurchaseTotal').text(response.ExpanceTotal.PurchaseTotal);
				// ********************************Code for Expence Tab*************************************
				// ********************************Code for Income Tab*************************************
				$('#IncomeTotal').text(response.IncomeTotal);
				// ********************************Code for Income Tab*************************************
				// ********************************Code for Purchase Tab*************************************
				$('#PurchaseTotal').text(response.PurchaseTotal);
				$('#PurchaseReturnTotal').text(response.PurchaseReturnTotal);
				$('#AccountPayable').text(response.AccountPayable);
				// ********************************Code for Purchase Tab*************************************
				// ********************************Code for cash Tab*************************************
				$('#TotalSaleProfit').text(response.ReceiptCash);
				$('#ServiceReceiptCash').text(response.ServiceReceiptCash);
				$('#TotalIncome').text(response.IncomeTotal);
				$('#TotalExpance').text(response.ExpanceTotal.Total);
				$('#TotalPurchase').text(response.PaymentCash);
				var TotalCollection=parseFloat(response.ReceiptCash)+parseFloat(response.ServiceReceiptCash)+parseFloat(response.IncomeTotal)-parseFloat(response.ExpanceTotal.Total)-parseFloat(response.PaymentCash);
				$('#TotalCollection').text(TotalCollection);
				$('#opening_balance').text(response.opening_balance);
				$('#closing_balance').text(parseFloat(TotalCollection)+parseFloat(response.opening_balance));
				// ********************************Code for cash Tab*************************************
				$('.indian_number_format').each(function(){
					$(this).text($.fn.indian_number_format($(this).text()));
				});
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
};
$('#from_date').on('change keyup',function(){
	$.fn.search();
});
$('#to_date').on('change keyup',function(){
	$.fn.search();
});
$('#report_li').show();
});
</script>
<script type="text/javascript">
	var $mainButton = $(".main-button"),
	$closeButton = $(".close-button"),
	$buttonWrapper = $(".button-wrapper"),
	$ripple = $(".ripple"),
	$layer = $(".layered-content");
	$mainButton.on("click", function(){
		var section=$(this).closest('div').find('.main-button').attr('id');
		$("#ripple-"+section).addClass("rippling");
		$("#button-wrapper-"+section).addClass("clicked").delay(150).queue(function(){
		// $("#layered-content-"+section).addClass("active");
	});
		setTimeout(function(){ $("#layered-content-"+section).addClass("active"); }, 30);
	});
	$closeButton.on("click", function(){
		var section=$(this).closest('div').find('.close-button').attr('id');
		$("#button-wrapper-"+section).removeClass("clicked");
		$("#ripple-"+section).removeClass("rippling");
		$("#layered-content-"+section).removeClass("active");
	});
// copy
// balapaCop("Material Overlay Animation", "#777");
</script>