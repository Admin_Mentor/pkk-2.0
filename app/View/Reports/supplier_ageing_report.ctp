<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>

<section class="content-header">
  <h1>Supplier Ageing Report</h1>
</section>
<section class="content">
  <div class="row">
    <div class="box">
      <div class="col-md-12">
        <div class="row">
          <br>
         <div class="col-md-3">
         <?= $this->Form->input('name',array('class'=>'form-control select_two_class','type'=>'select','empty'=>'select','options'=>$Party_list,'id'=>'party_id','label'=>'Party')); ?>
          </div>
           <div class="col-md-2">
                    <div class="col-md-12">
                     <?php echo $this->Form->input('from_date',array(
                      'type'=>'text',
                      'id'=>'from_date',
                      'class'=>'form-control search_field date_picker datepicker',
                      'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                      )); ?>
                    </div>
                  </div>
          <div class="col-md-1"><br>
           <button class='btn btn-success' type='button' id='supplier_report_button'>Get</button>
         </div>
       </div>
     </div>
     <div class="box-body">
      <div class="col-md-4 col-xs-12">
        <h3 class="muted "></h3>
      </div>
      <table class="table table-condensed table " id='table_supplier_list' data-page-length="-1" border="1">
        <thead>
          <tr class="blue-bg">
            <th>Invoice Number</th>
            <th>Invoice Date</th>
            <th>Date</th>
            <th class="text-right">Amount</th>
            <th class="text-right">Balance</th>
            <th class="text-right">Due</th>
          </tr>
        </thead>
        <tbody>
         
        </tbody>
        <tfoot></tfoot>
      </table>
    </div>
  </div>
</div>

</section>
<script type="text/javascript">

  $('#table_supplier_list').DataTable();
  $('#supplier_report_button').on('click',function(){
    $.fn.table_search();
  });

  $.fn.table_search=function(){  
var party_id=$('#party_id').val();
var from_date=$('#from_date').val();
var data={
  party_id:party_id,
  from_date:from_date,
};
  var url_address= '<?php echo $this->webroot; ?>'+'Reports/supplier_aging_report_ajax';
  $.ajax({
    type: "post",
    url:url_address,
    data: data,
    dataType:'json',
    success: function(response) {
      $('#table_supplier_list').DataTable().destroy();
      $('#table_supplier_list tbody').empty();
      $('#table_supplier_list tfoot').empty();
       var total_invoice=0;
       var total_balance=0;
       var cumulative_balance=0;
      $.each(response.data,function(key,value){
        for (var i = 0; i<value.balance.length; i++) {
        var tr="<tr>";
       cumulative_balance+=value.balance[i];
        tr+="<td>"+value.invoice_no[i]+"</td>";
        tr+="<td>"+value.delivered_date[i]+"</td>";
        tr+="<td>"+value.invoice_date[i]+"</td>";
        tr+="<td style='text-align:right'>"+parseFloat(value.invoice_amount[i]).toFixed(2)+"</td>";
        tr+="<td style='text-align:right'>"+parseFloat(value.balance[i]).toFixed(2)+"</td>";
        tr+="<td style='text-align:right'>"+parseFloat(value.due[i]).toFixed(2)+"</td>";
         total_invoice+=parseFloat(value.invoice_amount[i]);
          total_balance+=parseFloat(value.balance[i]);
        tr+="</tr>";
        $('#table_supplier_list tbody').append(tr);
        }
      });
        var tm="<tr>";
        tm+="<td></td>";
        tm+="<td></td>";
        tm+="<td><h4><b>Total</b></h4></td>";
        tm+="<td style='text-align:right'><h4><b>"+parseFloat(total_invoice).toFixed(2)+"</b></h4></td>";
        tm+="<td style='text-align:right'><h4><b>"+parseFloat(total_balance).toFixed(2)+"</h4></b</td>";
        tm+="<td></td>";
        tm+="</tr>";
         $('#table_supplier_list tfoot').append(tm);
         $('#table_supplier_list').DataTable({

        "ordering": false,
        dom: 'Bfrtip',
        buttons: [
        
        {
          extend: 'print',
           footer: true,
       exportOptions: { columns: ':visible'},
          customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                    '<h3 align="center">Supplier Ageing Report</h3>',
                    '<h5>Period : From '+$('#from_date').val()+'</h5>'

                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
              }
            },
       //      {
       //        extend: 'csv',
       //        footer: true,
       // exportOptions: { columns: ':visible'},
       // title:'Supplier Ageing Report'+'(Period : From '+$('#from_date').val()+')',

       //      },
       {
              extend: 'csv',
              footer: true,
       exportOptions: { columns: ':visible'},
       title:'Supplier Ageing Report'+'(Period : From '+$('#from_date').val()+')',
        customize: function (csv) {
                 return "\tSupplier Ageing Report\n"+"\t(Period : From "+$("#from_date").val()+")\n"+  csv ;
               },
            },
            {
              extend: 'excel',
              footer: true,
       exportOptions: { columns: ':visible'},
       title:'Supplier Ageing Report'+'(Period : From '+$('#from_date').val()+')',
            },
       //      {
       //        extend: 'pdf',
       //        footer: true,
       // exportOptions: { columns: ':visible'},
       // title:'Supplier Ageing Report'+'(Period : From '+$('#from_date').val()+')',
       //      },
            {
               extend: 'pageLength',
             },
            ]
            
          });
         $.fn.show_alert('Success');
    },

    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
}
$.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
</script>