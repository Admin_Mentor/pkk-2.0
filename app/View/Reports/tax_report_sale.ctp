<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<style type="text/css">
.number_format_alignclass="text-right"  {
  text-align:right;
}
</style>

<section class="content-header">
  <h1>Tax Sale Wise Report</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-body"> 
      <div class="col-md-12">
        <div class="row">
          <ul class="nav nav-pills tabs" role="tablist" id="aaa">
<li class="nav-item active" ><a class="active nav-link" data-id="1" id="Sale-tab" data-toggle="tab" href="#Sale" role="tab" aria-controls="Sale" aria-selected="false">SALE</a></li>
<li class="nav-item" ><a data-id="2" id="SaleReturn-tab" data-toggle="tab" href="#SaleReturn" role="tab" aria-controls="SaleReturn" aria-selected="false" >SALE RETURN</a></li>
</ul>
        </div>
        <br>
        <div class="tab-content"><br>
        <div class="tab-pane active" id="Sale" role="tabpanel" aria-labelledby="Sale-tab">
           <div class="row">
            <div class="col-md-2 col-lg-2 col-sm-2">
                <?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$from_date)); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2">
                <?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$to_date)); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2"><br>
              <button class="btn btn-success" id='fetch_button_sale'>Get</button>
            </div>
          </div>
        <div class="row">
          <div class="col-md-12">
              <div class="box-body table-responsive no-padding"><br>
                <table id='table_list-Sale' style="width: 100% !important" class="table table-hover boder table-bordered"  data-order='[[ 1, "desc" ]]'>
                  <thead>
                  <tr class="blue-bg">
                    <th>INV.No</th>
                    <th width="15%">SALE DATE</th>
                   <th >CUSTOMER TYPE</th>
                    <th width="15%">CUSTOMER</th>
                    <th>PLACE </th>
                    <th>GST NO:</th>
                    <th>NET AMOUNT</th>
                    <th>TAXABLE AMOUNT</th>
                    <th>CGST</th>
                     <th>SGST</th>
                      <th>IGST</th>
                      <th>DISCOUNT</th>
                    <th>TOTAL</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
          <tr>
            <th colspan="6" style="font-size:20px; color:red;text-align:right">Total:</th>
            <th style="font-size:20px; color:red;text-align:right"></th>
            <th style="font-size:20px; color:red;text-align:right"></th>
            <th style="font-size:20px; color:red;text-align:right"></th>
             <th style="font-size:20px; color:red;text-align:right"></th>
              <th style="font-size:20px; color:red;text-align:right"></th>
            <th style="font-size:20px; color:red;text-align:right"></th>
             <th style="font-size:20px; color:red;text-align:right"></th>
          </tr>
        </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane " id="SaleReturn" role="tabpanel" aria-labelledby="SaleReturn-tab">
         <div class="row">
            <div class="col-md-2 col-lg-2 col-sm-2">
                <?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'from_date_return','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$from_date)); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2">
                <?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'to_date_return','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$to_date)); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-2"><br>
              <button class="btn btn-success" id='fetch_button_return'>Get</button>
            </div>
          </div>
        <div class="row">
          <div class="col-md-12">
              <div class="box-body table-responsive no-padding"><br>
                <table id='table_list-SaleReturn' style="width: 100% !important" data-order='[[ 1, "desc" ]]' class="table table-hover boder table-bordered"  >
                  <thead>
                  <tr class="blue-bg">
                    <th>INV.No</th>
                    <th width="15%">DATE</th>
                      <th>CUSTOMER TYPE</th>
                    <th width="15%">CUSTOMER</th>
                    <th>PLACE </th>
                    <th>GST NO:</th>
                    <th>NET AMOUNT</th>
                    <th>TAXABLE AMOUNT</th>
                    <th>CGST</th>
                     <th>SGST</th>
                      <th>IGST</th>
                    <th>TOTAL</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
          <tr>
            <th colspan="6" style="font-size:20px; color:red;text-align:right">Total:</th>
            <th style="font-size:20px; color:red;text-align:right"></th>
            <th style="font-size:20px; color:red;text-align:right"></th>
             <th style="font-size:20px; color:red;text-align:right"></th>
              <th style="font-size:20px; color:red;text-align:right"></th>
            <th style="font-size:20px; color:red;text-align:right"></th>
             <th style="font-size:20px; color:red;text-align:right"></th>
          </tr>
        </tfoot>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
$('#table_list-Sale').DataTable( {
  "processing": true,
  "serverSide": true,
  "ajax": {
    "url": "<?= $this->webroot ?>Reports/TaxReport_sale",
    "type": "POST",
    data:function( d ) {
      d.from_date= $('#from_date').val();
      d.to_date= $('#to_date').val();
      },
      "dataSrc": "records",
    },
      dom: 'Bfrtip',
    lengthMenu: [
   [10,25, 50,100,200],
    ['10 rows','25 rows', '50 rows','100 rows','200 rows' ]
    ],
    buttons: [
    { extend: 'colvis', },
    { extend: 'excel', title: 'Tax Report', exportOptions: { columns: ':visible' } }, 
   // { extend: 'csv',   title: 'Bill Wise Report', exportOptions: { columns: ':visible' } },
    'pageLength',
    ],
    "columns": [
    { "data" : "Sale.invoice_no" },
    { "data" : "Sale.date_of_delivered" },
    { "data" : "Sale.customer_type" },
    { "data" : "AccountHead.name",},
    { "data" : "Sale.place" },
    { "data" : "Sale.vat_no" },
    { "data" : "Sale.net_value" ,className:"text-right"},
    { "data" : "Sale.taxable_total" ,className:"text-right"},
    { "data" : "Sale.cgst",className:"text-right" },
    { "data" : "Sale.sgst",className:"text-right" },
    { "data" : "Sale.igst",className:"text-right" },
    { "data" : "Sale.discount_amount",className:"text-right" },
    { "data" : "Sale.grand_total" ,className:"text-right"},
    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;
      var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
        i : 0;
      };
      pageTotal = api.column( 6, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 6 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 7, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 7 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 8, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 8 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 9, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 9 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 10, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 10 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 11, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 11 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 12, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 12 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
    },
    "columnDefs": [],
  });
$('#fetch_button_sale,#Sale-tab').click(function(){
  table = $('#table_list-Sale').dataTable();
  table.fnDraw();
});
$('#fetch_button_return,#SaleReturn-tab').click(function(){
$('#table_list-SaleReturn').DataTable().destroy();
$('#table_list-Sale').DataTable().destroy();
$('#table_list-SaleReturn').DataTable( {
  "processing": true,
  "serverSide": true,
  "ajax": {
    "url": "<?= $this->webroot ?>Reports/TaxReport_saleReturn",
    "type": "POST",
    data:function( d ) {
      d.from_date= $('#from_date_return').val();
      d.to_date= $('#to_date_return').val();

      },
      "dataSrc": "records",
    },
      dom: 'Bfrtip',
    lengthMenu: [
    [10,25, 50,100,200],
    ['10 rows','25 rows', '50 rows','100 rows','200 rows' ]
    ],
    buttons: [
    { extend: 'colvis', },
    { extend: 'excel', title: 'Tax Report', exportOptions: { columns: ':visible' } }, 
   // { extend: 'csv',   title: 'Bill Wise Report', exportOptions: { columns: ':visible' } },
    'pageLength',
    ],
    "columns": [
    { "data" : "SalesReturn.invoice_no" },
    { "data" : "SalesReturn.date_of_delivered" },
    { "data" : "SalesReturn.customer_type" },
    { "data" : "AccountHead.name",},
    { "data" : "SalesReturn.place" },
    { "data" : "SalesReturn.vat_no" },
    { "data" : "SalesReturn.net_value" ,className:"text-right"},
    { "data" : "SalesReturn.taxable_total" ,className:"text-right"},
   { "data" : "SalesReturn.cgst" ,className:"text-right"},
    { "data" : "SalesReturn.sgst" ,className:"text-right"},
    { "data" : "SalesReturn.igst" ,className:"text-right"},
    { "data" : "SalesReturn.grand_total",className:"text-right" },
    ],
    "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;
      var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
        i : 0;
      };
      pageTotal = api.column( 6, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 6 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 7, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 7 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 8, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 8 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 9, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 9 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 10, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 10 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
      pageTotal = api.column( 11, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 11 ).footer() ).html(''+parseFloat(pageTotal).toFixed(2)+'');
    },
    "columnDefs": [],
  });
});
</script>