<section class="content-header">
  <h1> Taxwise VAT Report </h1>
  <!-- ///***///--> 
</section>
<!-- Main content -->
<section class="content">
  <?= $this->Form->create('TaxWiseVATReport', array('url' => array('controller' => 'Reports', 'action' => 'TaxWiseVATReport')));?>
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="Stockmanagement">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-2">
                <div class="col-md-12">
                  <?= $this->Form->input('from_date',array(
                    'type'=>'text',
                    'id'=>'from_date',
                    'value'=>$final,
                    'class'=>'form-control cls_label_all date_field search_field date_picker datepicker',
                    'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                    )); ?>
                  </div> 
                </div>
                <div class="col-md-2">
                  <div class="col-md-12">
                    <?= $this->Form->input('to_date',array(
                      'type'=>'text',
                      'id'=>'to_date',
                      'value'=>$date1,
                      'class'=>'form-control cls_label_all date_field search_field date_picker datepicker',
                      'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
                      )); ?>
                    </div>
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <br>
                      <input type="submit"  class="print" id="getdata" value="Get"></a>
                    </div> 
                  </div>
                  <div class="col-md-2">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                      <br>
                      <input type="button"  class="save" name="button" id="exportExcel" value="Export"></a>
                    </div> 
                  </div>
                </div>
              </div>
              <br/><br/>
              <div class="box-body table-responsive no-padding">
                <div class="col-md-4 col-xs-12">
                  <h3 class="muted "></h3>
                </div>
                <table class="table table-hover boder text-center table-bordered" id='table_sales_margin_list' data-order='[[ 0, "desc" ]]'>
                  <thead>
                    <tr class="blue-bg">
                      <th>Invoice Date</th>
                      <th>Customer Name</th>
                      <th>Invoice No</th>
                      <th>Address1</th> 
                     <!--  <th>GSTIN</th>  -->
                      <th>VAT%</th>
                      <th>Taxable Amt</th>
                      <th>VAT Amt</th>
                      <th>Quantity</th>
                      <th>Total Amt</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($VATReport['list'] as $invoice) : ?>
                    <?php foreach ($invoice as $key => $value_single) : ?>
                      <tr>
                        <td><?= $value_single['invoice_date']; ?></td>
                        <td><?= $value_single['customer_name']; ?></td> 
                        <td><?= $value_single['invoice_no']; ?></td> 
                        <td><?= $value_single['place']; ?></td> 
             <!--            <td><?= $value_single['gststin']; ?></td>  -->
                        <td><?= floatval($value_single['vat']); ?></td> 
                        <td><?= floatval($value_single['taxable_value']); ?></td> 
                        <td><?= floatval($value_single['vat_amount']); ?></td> 
                        <td><?= floatval($value_single['quantity']); ?></td> 
                        <td><?= floatval($value_single['total']); ?></td> 
                        
                      </tr>
                    <?php endforeach; ?>  
                    <?php endforeach; ?>  
                  </tbody>
                  <tfoot>
                    <td colspan="3"></td>
                  <td></td>
                    <td><h4><b>Total</b></h4></td>
                    <td><h4><b><?= floatval($VATReport['total']['taxable_value']); ?></b></h4></td>
                    <td><h4><b><?= floatval($VATReport['total']['vat_amount']); ?></b></h4></td>
                    <td><h4><b><?= floatval($VATReport['total']['quantity']); ?></b></h4></td>
                    <td><h4><b><?= floatval($VATReport['total']['total_amount']); ?></b></h4></td>
                   
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?= $this->Form->end(); ?>
  </section>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#table_sales_margin_list').DataTable({
        "columnDefs": [ {
          "targets"  : 'no-sort',
            "orderable": false,
          }],
                dom: 'Blfrtip',
                buttons: [
                {
               extend:'print',
               text:'Print',
               customize: function ( win ) {
            $(win.document.body)
            .css( 'font-size', '10pt' )
            .prepend(
                    '<h3 align="center">Tax Wise VAT Report</h3>',
                    '<h5>Period : From '+$('#from_date').val()+'  To : '+$('#to_date').val()+'</h5>'
                    );
            $(win.document.body).find( 'table' )
            .addClass( 'compact' )
            .css( 'font-size', 'inherit' )
              }
            },
                ]
      });
      $('#table_sales_margin_list').find('.toggle_rows').hide();
      $('#exportExcel').click(function()   { 
        var date_from= $('#from_date').val();
        var date_to=$('#to_date').val();
        if(!date_from)
        {
          alert("Take From Date");
          return false;
        }
        if(!date_to)
        {
          alert("Take To Date");
          return false;
        }
        var website_url3='<?php echo $this->webroot; ?>Reports/print_tax_wise_vat_report/'+date_from+'/'+date_to;
        $(location).attr("href", website_url3);
      });
    });
  </script>