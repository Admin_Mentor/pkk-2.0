<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<style type="text/css">
.color_of{
  color :red !important;
}
</style>
<section class="content-header">
	<h1>Tray Summary</h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-header">
			<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?= $this->Form->input('warehouse_id',array('class'=>'form-control select_two_class name','type'=>'select','empty'=>'All','options'=>$warehouses,'id'=>'warehouse_id','label'=>'Warehouse')); ?>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12"><br>
				<button class='btn btn-success' id='get_button'>Get</button>
			</div>
		</div>
		<div class="box-body">
			<table class="boder table table-condensed table table-bordered" id="table_data" data-order='[[ 1, "asc" ]]' data-page-length='25'>
				<thead>
					<tr class="blue-bg">
						<th>Warehouse</th>
						<th>Quantity</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
				<tr>
				<th style="font-size:20px; color:red;text-align:right">Total:</th>
				<th style="font-size:20px; color:red;">0</th>
				</tr>
          </tfoot>
			</table>
		</div>
	</div>
</div>
<div id="details_modal" class="modal fade" role="dialog" >
	<div class="modal-dialog modal-lg" style="width: 50%">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Warehouse Name: <span id='Warehouse_name'></span></h4>
				<h4 class="modal-title color_of" >Quantity: <span  class="color_of" id='quantity'></span></h4>
			</div>
			<div class="modal-body">
				<div class="box-body table-responsive no-padding xs_tp">
					<table class="table table-bordered table-hover" id="details_table">
						<thead>
							<tr class="blue-bg">
								<th>Description</th>
								<th>Quantity</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
						<tfoot>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
</section>
<script type="text/javascript">
$('#table_data').DataTable( {
	"processing": true,
	"serverSide": true,
	"ajax": {
		"url": "<?= $this->webroot ?>Reports/TraySummary_ajax",
		"type": "POST",
		data:function( d ) {
				d.warehouse_id= $('#warehouse_id').val();
			},
			"dataSrc": "records",
		},
		  dom: 'Bfrtip',
		"columns": [
		{ "data" : "Warehouse.name" },
		{ "data" : "Stock.quantity",},
		],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};
			pageTotal = api.column( 1, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 1 ).footer() ).html(''+pageTotal+'');
		},
		"columnDefs": [
		{"targets": [ 0 ],"className": 'warehouses' ,},
        {"targets": [ 1 ],"className": 'quantity',className:"text-right"},
		],
	});
$('#get_button').click(function(){
	table = $('#table_data').dataTable();
	table.fnDraw();
});
$(document).on('click','td.warehouses',function(){
     var warehouse_id=$(this).closest('tr').find('span.warehouse_id').text();
	var quantity=$(this).closest('tr').find('td.quantity').text();
    var Warehouse_name=$(this).closest('tr').find('span.warehouse_name').text();
	 $('#Warehouse_name').text(Warehouse_name);	  
	  $('#quantity').text(quantity);
	$.get( "<?= $this->webroot ?>Reports/individual_tray_summary_ajax/"+warehouse_id ,function( data ) {
    $('#details_table').DataTable().destroy();
    $('#details_table tbody').empty();
    $('#details_table tfoot').empty();
    $('#details_table tbody').html(data.row.tbody);
    $('#details_table tfoot').html(data.row.tfoot);
    $('#details_table').DataTable({
"columnDefs": [
	{"targets": [ 1 ],"orderable": false ,},
	{"targets": [ 0 ],"orderable": false ,},

		],    });
	$('#details_modal').modal('show');
}, "json");

});
</script>