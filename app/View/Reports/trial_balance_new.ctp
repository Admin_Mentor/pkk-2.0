<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<style>
.main_head b{color:red;font-size:14px;}
</style>
<section class="content-header">
  <h1>Trial Balance</h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-header">
      <div class="row">
        <div class="form-group">
          <div class="col-md-8">
            <div class="col-md-2 col-lg-2 col-sm-3 col-xs-12">
              <?= $this->Form->input('from_date',array('type'=>'text','class'=>'form-control datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-2 col-lg-2 col-sm-3 col-xs-12">
              <?= $this->Form->input('to_date',array('type'=>'text','class'=>'form-control datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
            </div>
            <div class="col-md-1 col-lg-1 col-sm-2 col-xs-12"><br>
              <button type='button' class='btn btn-success' id='get_button'>GET</button>
            </div>
           <!--  <div class="col-md-1 col-lg-1 col-sm-2 col-xs-12 pull-right">
              <div class="col-md-6 col-lg-6 col-sm-6" style="margin-top: 5%;margin-left: 71%;">
                <br>
                <input type="button"  class="save" name="button" id="exportExcel" value="Export"></a>
              </div> 
            </div> -->
          </div>
       <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
            <div class="text-right" style="margin-top: 0.5%;margin-right: 2%"><br>
                <input type="button"  class="save" name="button" id="exportExcel" value="Export"></a>
                <input type="button"  class="save" name="button" id="exportmainpdf" value="Print"></a>
                <input type="button"  class="save" name="button" id="exportpdf" value="Detail Print"></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="box-body">
       <div class="row">
            <div class="col-md-12">
                <table class=" table-bordered table-hover table-striped table-condensed" id='dataTable' width="98%" style="margin-left:10px">
                        <thead>
                    <tr  class="blue-bg">
                    <th width="40%"><b>Particulars</b></th>
                    <th class="text-right"><b>Opening</b></th>
                    <th class="text-right"><b>Debit</b></th>
                    <th class="text-right"><b>Credit</b></th>
                    <th class="text-right"><b>Closing</b></th>
                    <th class="text-right" style="display: none"><b>Amount</b></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
       <div class="panel-heading">
              <div class="row">
                  <table class=" table-bordered table-hover table-striped table-condensed" id='difference_dataTable' width="100%">
                    <tbody>
                      <tr>
                        <td  colspan="2" width="40%"><h4 style="color: red"><b>Difference In Opening Balance :</b></h4></td>
                         <td width="14.5%" style='text-align:right'><h4 style="color: red"><b><span id='diffrence-open'>0</span></b></h4></td>
                         <td  width="14.5%" style='text-align:right'><h4 style="color: red"><b><span id='diffrence-left'>0</span></b></h4></td>
                        <td width="14.5%" style='text-align:right'><h4 style="color: red"><b><span id='diffrence-right'>0</span></b></h4></td>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
              </div>
                </div>
  <div class="panel-heading">
              <div class="row">
                <table class=" table-bordered table-hover table-striped table-condensed" id='grand_total_dataTable' width="100%">
                    <tbody>
                      <tr>
                        <td  colspan="2" width="54.5%"><h4 style="color: blue"><b>Grand Total :</b></h4></td>
                         <td  width="14.5%" style='text-align:right'><h4 style="color: blue"><b><span class='text-right' id='grand_total-debit'>0</span></b></h4></td>
                        <td width="14.5%" style='text-align:right'><h4 style="color: blue"><b><span class='text-right' id='grand_total-credit'>0</span></b></h4></td>
                        <td style='text-align:right'><h4 style="color: blue"><b><span class='text-right' id='grand_total-closing'>0</span></b></h4></td>
                      </tr>
                    </tbody>
                  </table>
            
          </div>
            </div>
    </div>
  <div id="Accounthead_modal" class="modal fade" role="dialog" >
    <div class="modal-dialog modal-lg" style="width: 75%">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span id='subgroup_name'></span></h4>
        </div>
        <div class="modal-body">
          <div class="box-body">
            <table class="boder table table-condensed table" id="Accounthead_table" data-order='[[ 0, "asc" ]]' data-page-length='25'>
              <thead>
                <tr  class="blue-bg">
                <th width="40%"><b>Particulars</b></th>
                <th class=""><b>Opening</b></th>
                <th class=""><b>Debit</b></th>
                <th class=""><b>Credit</b></th>
                <th class=""><b>Closing</b></th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
                <tr>
                  <th style="font-size:20px; color:red;text-align:right">Total:</th>
                  <th style="font-size:20px; color:red;"></th>
                  <th style="font-size:20px; color:red;"></th>
                  <th style="font-size:20px; color:red;"></th>
                  <th style="font-size:20px; color:red;"></th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary btn_radious" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</section>
<style>
table.dataTable tfoot th {
  padding: 5px 5px !important;
}
div.dataTables_wrapper div.dataTables_processing {
    left: -10px;
    padding: 10px !important;
    color: blue !important;
    opacity: 1 !important;
    text-decoration: none;
}
.footer-box{
    padding-left: 25px;
    padding-right: 30px;
}
.type_id{cursor: pointer;}
.sub_group_id{cursor: pointer;}
.account_name{cursor: pointer;}
.subgroup_name{padding: 15px;}
</style>
<?php require('general_journal_transaction_ledger_modal.php'); ?>
<script type="text/javascript">
$('#dataTable').DataTable( {
    "processing" : true,
    "serverSide" : true,
    "ordering"   : true,
    "searching"  : false,
    "paging"     : false,
    "bInfo"      : false,
    "fixedHeader": true,
    "ajax": {
    "url": "<?= $this->webroot ?>Reports/trial_balance_ajax",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "AccMainGroup.name" },
    { "data" : "AccMainGroup.opening_balance",className:"text-right" },
    { "data" : "AccMainGroup.debit",className:"text-right" },
    { "data" : "AccMainGroup.credit",className:"text-right" },
    { "data" : "AccMainGroup.amount",className:"text-right" },
     { "data" : "AccMainGroup.opening_balances" ,className: 'text-right hide'},
    ],
    "columnDefs": [
    {"targets": [ 0 ],"className": 'type_id'},
    ],
    "createdRow": function( row, data, dataIndex){
              $(row).css('font-weight','bold');

               $(row).css('font-size','120%');
      },
      "footerCallback": function ( row, data, start, end, display ) {
       var api = this.api(), data;
      var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0;};
      pageTotal_debit=api.column(2,{page:'current'}).data().reduce(function(a,b){return intVal(a) + intVal(b);
      },0);
      pageTotal_credit=api.column(3,{page:'current'}).data().reduce(function(a,b){return intVal(a) + intVal(b);
      },0);
       pageTotal_amount=api.column(5,{page:'current'}).data().reduce(function(a,b){return intVal(a) + intVal(b);
      },0);

        $('#diffrence-open').text(parseFloat(pageTotal_amount).toFixed(2));
      if(pageTotal_debit>pageTotal_credit)
      {
        var diffrence=parseFloat(pageTotal_debit-pageTotal_credit).toFixed(2);
        if(diffrence>0) $('#diffrence_in_opening').show();
        $('#diffrence-right').text(parseFloat(diffrence).toFixed(2));
        pageTotal_credit = parseFloat(pageTotal_credit)+parseFloat(diffrence);
      }
      else
      {
        var diffrence=parseFloat(pageTotal_credit-pageTotal_debit).toFixed(2);
        if(diffrence>0) $('#diffrence_in_opening').show();
        $('#diffrence-left').text(parseFloat(diffrence).toFixed(2));
        // var ttl = parseFloat(pageTotal_debit)+parseFloat(diffrence);
        pageTotal_debit = parseFloat(pageTotal_debit)+parseFloat(diffrence);
        
      }
      $('#grand_total-credit').text(parseFloat(pageTotal_debit).toFixed(2));
      $('#grand_total-debit').text(parseFloat(pageTotal_debit).toFixed(2));
    },
});
var previous_index='';
$(document).on('click','td.type_id',function(){
  var type_id=$(this).closest('tr').find('span.type_id').text();
  var type_name=$(this).closest('tr').find('span.type_name').text();
  var from_date= $('#from_date').val();
  var to_date= $('#to_date').val();
  var new_data={
     from_date:from_date,
     to_date:to_date,
     type_id:type_id,
  };
  var row_index=$(this).parents('tr').index();
    if ($('.all_data'+row_index)[0])
    {
    $('.all_data'+row_index).remove();
    } 
    else
    {
  if(parseFloat(previous_index) != parseFloat(row_index))
  {
    $.post( "<?= $this->webroot ?>Reports/Trial_balance_subgroupwise",new_data ,function( data ) {
      $.each(data.Subgroup,function(key,value){
           
        $('#dataTable tbody tr:eq('+row_index+')').after('<tr class="all_data'+row_index+'"+>\
        <td class="sub_group_id"><span hidden class="hidden_id">'+value.sub_group_id+'</span><span hidden class="hidden_type_id">'+type_id+'</span><span class="subgroup_name">'+value.name+'</span></td>\
        <td class="text-right">'+value.opening_balance+'</td>\
        <td class="text-right">'+value.debit+'</td>\
        <td class="text-right">'+value.credit+'</td>\
        <td class="text-right">'+value.amount+'</td>\
        </tr>');
      });
    }, "json");
    previous_index=row_index;
  }
  else
  {
    previous_index='';
  }
  }

});
$(document).on('click','td.sub_group_id',function(){
  $('#Accounthead_table').DataTable().destroy();
  $('#Accounthead_table tbody').empty();
  $('#Accounthead_modal').modal('show');
  var subgroup_name=$(this).closest('tr').find('span.subgroup_name').text();
  var sub_group_id=$(this).closest('tr').find('span.hidden_id').text();
  var hidden_type_id=$(this).closest('tr').find('span.hidden_type_id').text();
  var from_date= $('#from_date').val();
  var to_date= $('#to_date').val();
  var opening=0;
  var debit=0;
  var credit=0;
  var total=0;
  var url_address= '<?php echo $this->webroot; ?>Reports/get_total_amount_trial_balance/'+sub_group_id+'/'+from_date+'/'+to_date;
  $.ajax({
    'async': false,
    method: "POST",
    url:url_address,
    dataType:'json',
    'success': function (data) {
      opening = data.opening;
      debit = data.debit;
      credit = data.credit;
      total = data.total;
    }
  });
  // return total;
  // console.log(debit);
  $('#subgroup_name').text(subgroup_name);
  var table=$('#Accounthead_table').DataTable( {
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Reports/Trial_balance_accounthead",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
        d.type_id= hidden_type_id;
        d.sub_group_id= sub_group_id;
      },
      "dataSrc": "records",
    },
    dom: 'lrtip',
    "aLengthMenu": [
    [25, 50,100,-1],
    ['25 rows', '50 rows','100 rows','Show all' ]
    ],
    "iDisplayLength": 25,
    buttons: ['pageLength'],
   "columns": [
    { "data" : "AccountHead.name" },
    { "data" : "AccountHead.opening_balance",className:"text-right" },
    { "data" : "AccountHead.debit",className:"text-right" },
    { "data" : "AccountHead.credit",className:"text-right" },
    { "data" : "AccountHead.amount",className:"text-right" },
    ],
   "footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data;
      var intVal = function ( i ) {
        return typeof i === 'string' ?
        i.replace(/[\$,]/g, '')*1 :
        typeof i === 'number' ?
        i : 0;
      };
      pageTotal = api.column( 1, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 1 ).footer() ).html(''+opening+'');
      pageTotal = api.column( 2, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 2 ).footer() ).html(''+debit+'');
      pageTotal = api.column( 3, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 3 ).footer() ).html(''+credit+'');
      pageTotal = api.column( 4, { page: 'current'} ).data().reduce( function (a, b) {
        return intVal(a) + intVal(b);
      }, 0 );
      $( api.column( 4 ).footer() ).html(''+total+'');
    },
   "columnDefs": [
    {"targets": [ 0 ],"className": 'account_name' ,},
    ],
  });

});
$('#get_button').click(function(){
    table = $('#dataTable').dataTable();
    table.fnDraw();
  });
</script>
<script type="text/javascript">
  $(document).ready(function(){

    $('#exportExcel').click(function()   { 
      var date_from= $('#from_date').val();
      var date_to=$('#to_date').val();
      if(!date_from)
      {
        alert("Take From Date");
        return false;
      }
      if(!date_to)
      {
        alert("Take To Date");
        return false;
      }
      var website_url3='<?php echo $this->webroot; ?>Reports/print_trial_balance_report/'+date_from+'/'+date_to;
      $(location).attr("href", website_url3);
    });
     $('#exportpdf').click(function()   { 
      // alert("k");
      var date_from= $('#from_date').val();
      var date_to=$('#to_date').val();
      if(!date_from)
      {
        alert("Take From Date");
        return false;
      }
      if(!date_to)
      {
        alert("Take To Date");
        return false;
      }
      var website_url3='<?php echo $this->webroot; ?>Reports/trial_balance_Print/'+date_from+'/'+date_to;
      // $(location).attr("href", website_url3);
      window.open(website_url3, '_blank');
    });
    $('#exportmainpdf').click(function()   { 
      // alert("k");
      var date_from= $('#from_date').val();
      var date_to=$('#to_date').val();
      if(!date_from)
      {
        alert("Take From Date");
        return false;
      }
      if(!date_to)
      {
        alert("Take To Date");
        return false;
      }
      var website_url3='<?php echo $this->webroot; ?>Reports/trial_balance_main_Print/'+date_from+'/'+date_to;
      // $(location).attr("href", website_url3);
      window.open(website_url3, '_blank');
    });
  });
</script>
<script type="text/javascript">
  <?php require('general_journal_transaction_ledger_ajax.js'); ?>
</script>