<section class="content-header">
  <h1>VAT Report</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header"> 
    </div>
    <div class="box-body"> 
      <?= $this->Form->create('GSTReport'); ?>
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="col-md-6">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-sm-12 col-md-12">
                <?php echo $this->Form->input('type_id',array('type'=>'select','empty' =>'All','options'=>array('Sale'=>'Sale','Purchase'=>'Purchase',),'style'=>'width: 100%;','class'=>'form-control select2 select2-hidden-accessible Search','label'=>'Type','id'=>'type_id')) ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-sm-12 col-md-12">
                  <?php echo $this->Form->input('gst_id',array('type'=>'select','empty' =>'All','options'=>$GST_RATE,'style'=>'width: 100%;','class'=>'form-control select2 select2-hidden-accessible Search','label'=>'VAT','id'=>'gst_id')) ?>
                </div>
              </div>
            </div>
          </div>
        </div> 
        <div class="col-md-2 col-sm-2">
          <div class="form-horizontal">
            <div class="form-group">
              <div class="col-sm-12 col-md-12">
                <?php echo $this->Form->input('product_type_id',array('id'=>'product_type_id','type'=>'select','empty' =>'All','options'=>$ProductType,'style'=>'width: 100%;','class'=>'form-control select2 Search select2-hidden-accessible','label'=>'Product Type')) ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="form-horizontal">
            <div class="form-group">
              <div class="col-sm-12 col-md-12">
                <?php echo $this->Form->input('product_id',array('id'=>'product_id','type'=>'select','empty' =>'All','options'=>$Product,'style'=>'width: 100%;','class'=>'form-control select2 Search select2-hidden-accessible','label'=>'Product')) ?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="col-md-6">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-sm-12 col-md-12">
                  <?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control pull-right date_picker Search datepicker','id'=>'from_date','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-horizontal">
              <div class="form-group">
                <div class="col-sm-12 col-md-12">
                  <?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control pull-right date_picker Search datepicker','id'=>'to_date','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-1 col-sm-1 pull-right mar-tp-5">
          <br>
         <!--  <button type="button" class="btn btn-primary" id="get_button">Get</button> -->
        </div>
        <div class="col-md-1 col-sm-1 pull-right mar-tp-5">
          <br>
          <!-- <button class="print" data-toggle="modal" data-target="#print">Print</button> -->
        </div>
      </div>
    </div>
    <?= $this->Form->end(); ?>
    <div class="row">
      <div class="col-md-12">
        <div class="box-body table-responsive no-padding">
          <table style="width: 100% !important" class="table datatable table-hover boder text-center table-bordered" id='GSTReport_table' >
            <thead>
              <tr class="blue-bg">
                <th rowspan='2'>Date</th>
                <th rowspan='2'>Type</th>
                <th rowspan='2'>Product Name</th>
                <th rowspan='2'>HSN</th>
                <th rowspan='2'>Inv.No</th>
                <th rowspan='2'>Taxable Value</th>
                <th colspan='3'>VAT</th>
                <th rowspan='2'>Outstanding</th>
              </tr>
              <tr class="blue-bg">
                <th>Rate %</th>
                <th>In</th>
                <th>Out</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($GSTReport['list'] as $key => $value) :?>
                <tr>
                  <td style='border: "1"'><?= $value['date']; ?></td>
                  <td><?= $value['type']; ?></td>
                  <td><?= $value['product']; ?></td>
                  <td><?= $value['hsn']; ?></td>
                  <td><?= $value['invoice_no']; ?></td>
                  <td><?= $value['taxable_value']; ?></td>
                  <td><?= $value['igst']['rate_percentage']; ?></td>
                  <td><?= $value['total']['in']; ?></td>
                  <td><?= $value['total']['out']; ?></td>
                  <td></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
           <tfoot>
              <td colspan="4"></td>
              <td><h4><b>Total</b></h4></td>
              <td><h4><b><?= $GSTReport['total']['taxable_value']; ?></td>
              <td></td>
              <td><h4><b><?= $GSTReport['total']['total']['in']; ?></b></h4></td>
              <td><h4><b><?= $GSTReport['total']['total']['out']; ?></b></h4></td>
              <td><h4><b><?= $GSTReport['total']['total']['out']-$GSTReport['total']['total']['in']; ?></b></h4></td>
            </tfoot>
          </table>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#product_type_id').change(function(){
    var product_type_id=$('#product_type_id').val();
    var data={
      product_type:product_type_id,
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Product/product_type_select_ajax';
    $.ajax({
      type: "post",  
      url:url_address,
      data: data,  
      dataType:'json',
      success: function(response) {
        $('#product_id').html(response.option);
        $('#product_id').val('').change();
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  });
  $('.Search').change(function(){
    $.fn.Search();
  });
  // $('#get_button').click(function(){
  //   $.fn.Search();
  // });
  $.fn.Search=function(){
    var product_type_id=$('#product_type_id').val();
    var product_id=$('#product_id').val();
    var from_date=$('#from_date').val();
    var to_date=$('#to_date').val();
    var type_id=$('#type_id').val();
    var gst_id=$('#gst_id').val();

    var data={
      product_type_id:product_type_id,
      product_id:product_id,
      from_date:from_date,
      to_date:to_date,
      type:type_id,
      gst:gst_id,
    };
    $.post( "<?= $this->webroot ?>Reports/VATReport_ajax",data ,function( data ) {
      $('#GSTReport_table').DataTable().destroy();
      $('#GSTReport_table tbody').empty();
      $('#GSTReport_table tfoot').empty();
      var total_cgst_in=0;
      var total_cgst_out=0;
      var total_sgst_in=0;
      var total_sgst_out=0;
      var total_igst_in=0;
      var total_igst_out=0;
      var total_in=0;
      var total_out=0;
      var taxable_value=0;
      $.each(data.list,function(key,value){
        var GSTReport_table_tbody_append='<tr>';
        GSTReport_table_tbody_append+='<td>'+value.date+'</td>';
        GSTReport_table_tbody_append+='<td>'+value.type+'</td>';
        GSTReport_table_tbody_append+='<td>'+value.product+'</td>';
        GSTReport_table_tbody_append+='<td>'+value.hsn+'</td>';
        GSTReport_table_tbody_append+='<td>'+value.invoice_no+'</td>';
        GSTReport_table_tbody_append+='<td>'+value.taxable_value+'</td>';
        taxable_value=parseFloat(taxable_value)+parseFloat(value.taxable_value);

        GSTReport_table_tbody_append+='<td>'+value.igst.rate_percentage+'</td>';
        GSTReport_table_tbody_append+='<td>'+value.total.in+'</td>';
        total_in=parseFloat(total_in)+parseFloat(value.total.in);
        GSTReport_table_tbody_append+='<td>'+value.total.out+'</td>';
        total_out=parseFloat(total_out)+parseFloat(value.total.out);
        GSTReport_table_tbody_append+='<td></td>';
        GSTReport_table_tbody_append+='</tr>';
        $('#GSTReport_table tbody').append(GSTReport_table_tbody_append);
      });
      var GSTReport_table_tfoot_append='<tr>';
      GSTReport_table_tfoot_append+='<td colspan="4"></td>';
      GSTReport_table_tfoot_append+='<td colspan><h4><b>Total</h4></b></td>';
      GSTReport_table_tfoot_append+='<td colspan><h4><b>'+taxable_value+'</h4></b></td>';
      GSTReport_table_tfoot_append+='<td></td>';

      GSTReport_table_tfoot_append+='<td><h4><b>'+total_in+'</h4></b></td>';
      GSTReport_table_tfoot_append+='<td><h4><b>'+total_out+'</h4></b></td>';
      var outstanding=total_out-total_in;
      GSTReport_table_tfoot_append+='<td><h4><b>'+outstanding+'</h4></b></td>';
      GSTReport_table_tfoot_append+='</tr>';
      $('#GSTReport_table tfoot').append(GSTReport_table_tfoot_append);
      $('#GSTReport_table').DataTable();
    }, "json");
  }
  $('.main-sidebar').ready(function(){
    $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
  });
</script>