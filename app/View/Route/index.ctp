<section class="content-header">
  <h1> Route 
    <div class="col-sm-1 pull-right" style="margin-right: 20px">
             <button data-toggle="modal"  data-target="#addlocation" class="btn btn-success">Create</button>
            </div>
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
<!--         <div class="row">
          <div class="col-md-12">
            <div class="col-md-4"><?php echo $this->Form->input('Route',array('type'=>'select','empty' =>'ALL','options'=>$Route,'class'=>'form-control select2','required'=>'required')) ?></div>
          </div>
        </div> -->
        <!-- <div class="text-right" style="margin-top: 1.5%;margin-right: 2%"> -->
                    <!-- <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#addlocation"></i></a> -->
                   <!-- <button data-toggle="modal"  data-target="#addlocation" class="btn btn-success">Create</button> -->
                    
                  <!-- </div> -->
        <div class="box-body">
          <table class="table table-condensed tab_view_top_mar datatable type_tab table-bordered" id='route_table'>
            <thead>
              <tr class="blue-bg">
                <th>Route</th>
                <th style="display:none"></th>
                <th>Code</th>
                <th>CashName</th>
                 <th>Mobile</th>
           <th width="10%">Action</th>
              </tr>
            </thead>
            <tbody>
              <?php  foreach( $Routes as $value ){?>
              <?php 
              if($value['Route']['account_head_id'])
                {
              $cash_name=$value['Route']['name'].'_CASH';
               }
               else
                {
                   $cash_name="";
                }?>
                <tr class="blue-pd">
                  <td style="display:none" class='route_id'><?= $value['Route']['id'];?></td>
                  <td class='route_name'><?= $value['Route']['name']; ?></td>
                  <td class='route_code'><?= $value['Route']['code']; ?></td>
                  
                  <td ><?= $cash_name; ?></td>
                  <td class='route_mobile'><?= $value['Route']['mobile']; ?></td>
                  <td><a href="#"><i class="fa fa-2x fa-edit edit_new_btn ad-mar td_leted edit_dt"> </i></a>
                    &nbsp; &nbsp; &nbsp;
                       <!--  <span><a href="#"><i class="fa fa-trash ad-mar route-delete"></i></a></span> -->
                  </td>
                  
                </tr>
                <?php  }?>
              </tbody>
            </table>
          </div>
          <div id="route_edit_modal" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Route Code</h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-md-3"><label for="inputEmail3" class="form-group">Route</label></div>
                    <div class="col-md-9">
                      <input type="hidden" id="route_id">
                      <input type="text" class="form-control form-group" placeholder="" id="route_name" readonly="">
                      <span id="prdct_type_error_edit" style="color:#db1802" class="help-inline"></span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3"><label for="inputEmail3" style="text-transform:uppercase" class="form-group">Code</label></div>
                    <div class="col-md-9">
                      <input type="text" class="form-control form-group" placeholder="" id="route_code" required="" readonly="">
                      <span id="prdct_type_error_edit" style="color:#db1802" class="help-inline"></span>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-3"><label for="inputEmail3" style="text-transform:uppercase" class="form-group">Mobile</label></div>
                    <div class="col-md-9">
                      <input type="text" class="form-control form-group" placeholder="" id="route_mobile" required="">
                      <span id="prdct_type_error_edit" style="color:#db1802" class="help-inline"></span>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" >Cancel</button>
                  <button type="button" class="btn btn-success" data-dismiss="modal" id="route_edit">Update</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div id="addlocation" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Add Route</h4>
      </div>
      <div class="modal-body">
         <?php echo $this->Form->create('Route', ['class' => 'form-horizontal', 'id' => 'Route_Form']); ?>
        <div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Name *</label>
            <div class="col-sm-9">
              <input class="form-control location_disable toUpperCase " placeholder="" type="text" id="modal_location_name" name="modal_location_name">
              <span id="location_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>
          <div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Code *</label>
            <div class="col-sm-9">
              <input class="form-control location_disable toUpperCase " placeholder="" type="text" id="modal_location_code" name="modal_location_codes">
              <span id="location_code_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>
           <div class="form-horizontal">
          <div class="form-group">
            <label for="inputEmail3" class="col-sm-3 control-label">Mobile *</label>
            <div class="col-sm-9">
              <input class="form-control location_disable toUpperCase " placeholder="" type="text" id="modal_location_mobile" name="modal_location_mobile">
              <span id="location_code_error" style="color:#db1802" class="help-inline"></span>
            </div>
          </div>
          
          </div>
        </div>
                 <?= $this->Form->end(); ?>

      <div class="modal-footer">
        <button  type='button' class="save btn btn-success" id="add_location">Save</button>
      </div>
      </div>
    </div>
  </div>
</div>
    <script>
//        $('#add_location').click(function(){
//   var modal_location_name=$('#modal_location_name').val();
//   var modal_location_code=$('#modal_location_code').val();
//   var data={
//     name:modal_location_name,
//     code:modal_location_code,
//   };
//   var url_address= '<?php echo $this->webroot; ?>Route/add_route_ajax';
 
//   $.ajax({
//     method: "POST",
//     url:url_address,
//     data: data,
//   }).done(function( result ) {
//     location.reload(); 

//   });
// });
       $('#add_location').click(function(){
  var data=$('#Route_Form').serialize();
  console.log(data);
  $.post( "<?= $this->webroot ?>Route/add_route_ajax",data ,function( data ) {
    if(data.result!='Success')
     {
       alert(data.result);
       return false;
     }
    // $('#Route_Form')[0].reset();
     //alert(data.result);
     $.fn.show_alert('Success');
     location.reload();
    // $('#add_location').modal('toggle');
   }, "json");
});
      $(document).on('click', '.edit_dt', function () {
        $('#route_table').find('td').removeClass('new_route_name');
        $('#route_table').find('td').removeClass('new_route_code');
        $('#route_table').find('td').removeClass('new_route_mobile');
        var route_name=$(this).closest('tr').find('td.route_name').text();
        var route_id=$(this).closest('tr').find('td.route_id').text();
          var route_code=$(this).closest('tr').find('td.route_code').text();
          var route_mobile=$(this).closest('tr').find('td.route_mobile').text();
        $(this).closest('tr').find('td.route_name').addClass('new_route_name');
        $(this).closest('tr').find('td.route_code').addClass('new_route_code');
         $(this).closest('tr').find('td.route_mobile').addClass('new_route_mobile');
        $('#route_id').val(route_id);
        $('#route_name').val(route_name);
        $('#route_code').val(route_code);
         $('#route_mobile').val(route_mobile);
        $('#route_edit_modal').modal('show');
      });

      $('#route_edit').click(function() {
        var route_id=$('#route_id').val();
        if(!route_id)
          alert('Empty Route Id');
        var route_name=$('#route_name').val().toUpperCase();
        if($.trim(route_name)=='')
          alert('Empty Route Name');
         var route_code=$('#route_code').val().toUpperCase();
          var route_mobile=$('#route_mobile').val();
        if($.trim(route_code)=='')
          alert('Empty Route Code');
       if(route_mobile=='')
          alert('Empty Route Mobile');
       
        var url_address= '<?php echo $this->webroot; ?>'+'Route/edit_ajax';
        var data=
        {
          id:route_id,
          name:route_name,
          code:route_code,
          mobile:route_mobile,
        };
        $.ajax({
          type: "post",  
          url:url_address,
          data: data, 
          dataType:'json',
          success: function(response) {
            if(response.result!='Success')
            {
              alert(response.result);
              return false;
            }
            //alert(response.result);
            $.fn.show_alert('Success');
            $('.new_route_name').html(route_name);
            $('.new_route_code').html(route_code);
              $('.new_route_mobile').html(route_mobile);
             $('#route_id').val('');
        $('#route_name').val('');
        $('#route_name').val('');
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      });

      $(document).on('click','.route-delete',function(){
        if(!confirm("Are you sure?"))
        {
          return false;
        }
        var id=$(this).closest('tr').find('td.route_id').text();
        var rowindex = $(this).closest('tr').index();
        var url_address= '<?php echo $this->webroot; ?>'+'Route/delete_by_ajax/'+$.trim(id);
        $.ajax({
          type: "post",  
          url:url_address,
          dataType:'json',
          success: function(response) {
            if(response.result=="Success")
            {
              $('#route_table tbody tr:eq(' + rowindex + ')').remove();
              $("#route_table option[value='"+id+"']").remove();
              $("#Route").select2('val','');
            }
            else
            {
              $('#route_table tbody tr:eq(' + rowindex + ')').attr('class','blue-pd flash');
              $('#route_table tbody tr:eq(' + rowindex + ')').attr('style','font-size:20px;color:red');
              alert(response.result);
              setTimeout(timer_for_table, 2000);
            }
          },
          error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
          }
        });
      });
      function timer_for_table() {
        $('#customer_type_table tbody tr').each(function(){
          $(this).closest('tr').attr('class','blue-pd');
          $(this).closest('tr').attr('style','font-size:14px;color:');
        });
      }
     $('.location_disable').keyup(function(){
  var modal_location_name=$('#modal_location_name').val().trim();
  if(modal_location_name!="")
  {
    $('#add_location').attr('disabled',false);
  }
  else{
    $('#add_location').attr('disabled',true);
  }
});
     $('#modal_location_name').keyup(function(){
  var modal_location_name=$(this).val();
  var data={
   location_name:modal_location_name
 };
  var url_address= '<?php echo $this->webroot; ?>'+'Executive/route_search';
 $.ajax({
  type: "post",
  url:url_address,
  data: data,
  success: function(response) {
    if(response=="Yes")
    {
      $('#location_error').html('This Location is already taken');
      $('#add_location').attr('disabled',true);
    }
    else
    {
      $('#location_error').html('');
       $('#add_location').attr('disabled',false);
    }
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
});
     $('#modal_location_code').keyup(function(){
  var modal_location_code=$(this).val();
  var data={
   code:modal_location_code
 };
  var url_address= '<?php echo $this->webroot; ?>'+'Route/route_search';
 $.ajax({
  type: "post",
  url:url_address,
  data: data,
  success: function(response) {
    if(response=="Yes")
    {
      $('#location_code_error').html('This Code is already taken');
      $('#add_location').attr('disabled',true);
    }
    else
    {
      $('#location_code_error').html('');
       $('#add_location').attr('disabled',false);
    }
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
});
     $.fn.show_alert = function(flash)
     {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
      }
      $(document).on('keypress','#modal_location_code',function(e){
     if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57 || e.which==47)) {
       return false;
     }
   });
    </script>