
<style type="text/css">
.btn{
	margin-bottom:21px;
}
</style>
<section class="content-header">
	<h1 style="padding-left: 12%;"> Credit Note </h1>
</section>
<section class="content">
	<div class="box">
		<?= $this->Form->create('CreditNote', array('url' => array('controller' => 'Sale', 'action' => 'CreditNote')));?>
		<div class="row" style="margin-top: 1%;">
			<div class="col-md-8">
				<div class="col-md-4 col-lg-4 col-sm-4">
					<div class="form-group">
						<label>Customer type</label>
						<?= $this->Form->input('customer_type_id',array('type'=>'select','id'=>'customer_type_id','class'=>'form-control select2 ','style'=>'width: 100%','options'=>$CustomerType,'label'=>false,'required'=>'required','empty'=>array('0'=>'ALL'))); ?>
					</div>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-4">
					<div class="form-group">
						<label>Customer</label>
						<?= $this->Form->input('customer_id',array('type'=>'select','id'=>'customer_id','class'=>'form-control select2 ','style'=>'width: 100%','options'=>$Customer_list,'label'=>false,'required'=>'required')); ?>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Credit No</label>
							<td> <?= $this->Form->input('credit_no',array('class'=>'form-control','type'=>'text','required','id'=>'credit_no','label'=>false,'readonly')); ?></td>

						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<label>Date</label>
							<?= $this->Form->input('date',array('type'=>'text','id'=>'date','class'=>'form-control date_picker datepicker','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>false)); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-lg-12 col-sm-12">
				<div class="box-body table-responsive">
					<table class="table table_with_border_full" id="credit_note">
						<thead>
							<tr class="blue-bg">
								<th>Description</th>
								<th >Amount</th>
								<?php if(!$this->request->data['CreditNote']['id'])
								{?>
									<th>Action</th>
								<?php } ?>
							</tr>
							<tr>
								<?php if(!$this->request->data['CreditNote']['id'])
								{?>
									<td><?= $this->Form->input('description',array('class'=>'form-control','type'=>'textarea','rows'=>2,'id'=>'description','label'=>false)); ?></td>
									<td> <?= $this->Form->input('amount',array('class'=>'form-control','type'=>'number','id'=>'amount','label'=>false)); ?></td>

									<td><a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px"  id='add_credit'></i></a></td>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php if(isset($CreditDetail))
							{
								foreach ($CreditDetail as $key => $value)
								{
									?>
									<tr class="blue-pddng">
										<td colspan="">
											<textarea class="form-control credit_list" readonly rows=2 name="data[CreditNote][description][]"><?= $value['CreditDetail']['description'];?></textarea></td>
											<td><input class=" form-control credit_list row_total" name="data[CreditNote][amount][]" value='<?= $value['CreditDetail']['amount']; ?>' readonly></td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
							<tfoot>
								<tr>
									<td style="float: right;"><h4>Total</h4></td>
									<td><?= $this->Form->input('total',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'total','label'=>false,)); ?></td>
									<td></td>

								</tr></tfoot>
							</table>

						</div>
					</div>
				</div>
				<?php if(!$this->request->data['CreditNote']['id'])
				{?>
					<div class="row">
						<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 col-md-offset-10">
							<br>
							<button type='Submit' name='data[CreditNote][process]' id="save_buttton" value='delivery' class="btn btn-danger">Save & Print</button>
						</div>
					</div>
				<?php } ?>
				<?= $this->Form->end(); ?>

			</div>

		</section>

		<script type="text/javascript">
			$('#add_credit').click(function(event){
				event.preventDefault();
				var customer_id=$('#customer_id').val();
				var description=$('#description').val();
				var amount=$('#amount').val();
				var date=$('#date').val();
				var credit_no=$('#credit_no').val();
				if(!customer_id)
				{
					$('#customer_id').select2('open');
					return false;
				}
				if(!description)
				{
					$('#description').focus();
					return false;
				}
				if(!amount)
				{
					$('#amount').focus();
					return false;
				}

				$('#credit_note tbody').append('<tr>\
					<td>\
					<textarea class="form-control credit_list" readonly rows=2 name="data[CreditNote][description][]">'+description+'</textarea></td>\
					<td><input class=" form-control credit_list row_total" name="data[CreditNote][amount][]" value="'+amount+'" readonly>\
					</td>\
					<td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
					</tr>');
				$('#amount').val('');
				$('#description').val('');
				$.fn.main_calculater();
			});
			$(document).on('click','.remove_tr',function(){
				$(this).closest('tr').remove();
				$.fn.main_calculater();
			});
			$.fn.main_calculater=function(){
				var each_total=0;
				$('#credit_note tbody tr').each(function(){
					var single_total=$(this).closest('tr').find('td input.row_total').val();
					each_total+=parseFloat(single_total);
				});
				var main_total=each_total;
				$('#total').val(main_total);
			}
			$(document).on('change','#customer_type_id',function(){
				var customer_type_id=$(this).val();
				$.post( "<?= $this->webroot ?>Customer/get_customer_by_customer_type_ajax/"+customer_type_id ,function( data ) {
					var id = $('#customer_id').val();
					<?php if(!empty($this->request->data['CreditNote']['account_head_id'])){?>
						var id = <?=$this->request->data['CreditNote']['account_head_id']?>;

						<?php 
					}?>
					$('#customer_id').html('');
					$.each(data.options,function(key,value){
						$('#customer_id').append($("<option></option>").attr("value",key).text(value));
						if(id==key){
							$('#customer_id').val(id).trigger('change');
						}
					});
					$('#customer_id').select2("open");

					$('#customer_id').trigger('change');
				}, "json");
			});

		</script>
		<script type="text/javascript">
			var flash='<?= $flash=$this->Session->flash(); ?>';
			<?php if(!empty($this->request->data['CreditNote']['id'])) :?>
			if(flash)
			{
				var id ="<?=$this->request->data['CreditNote']['id']?>";
				var link = "<?= $this->webroot.'Print/creditnote/'?>"+id;
				window.open(link, '_blank');
				var current_url_split=current_url.split("/");
				window.location.replace(current_url_split[0]+"/"+current_url_split[1]+"/"+current_url_split[2]+"/"+current_url_split[3]);
			} 
			<?php endif; ?>
		</script>