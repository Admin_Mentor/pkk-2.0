<section class="content-header">
	<h2>Credit Note List
		<a href="<?php echo $this->webroot ?>Sale/CreditNote"><button class='btn btn-success pull-right'>New Credit Note</button></a>
	</h2>
</section>
<section class="content">
	<div class="box">
		<!-- <div class="row">
			<div class="col-md-3 col-lg-2 col-sm-12 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$firstdate,'label'=>'From Date')); ?>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-lg-2 col-sm-12 col-xs-12">
				<div class="form-group">
					<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
						<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','value'=>$todate,'label'=>'To Date')); ?>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="form-group"><br>
					<button class='btn btn-success' id='get_button'>Get</button>
				</div>
			</div>
		</div> -->
		<div class="row">
			<div class="col-md-12">
				<div class="box-body">
					<table class="boder table table-condensed table" id="table_data" data-order='[[ 1, "asc" ]]' data-page-length='25'>
						<thead>
							<tr class="blue-bg">
								<th>Date</th>
								<th>Customer</th>
								<th>Credit NO</th>
								<th>Amount</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	$('#table_data').DataTable({
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Sale/credit_note_index",
			"type": "POST",data:function( d ) {
				
			},
			"dataSrc": "records",
		},
		"columns": [
		{ "data" : "CreditNote.date" },
		{ "data" : "AccountHead.name" },
		{ "data" : "CreditNote.credit_no" },
		{ "data" : "CreditNote.total" },
		{ "data" : "CreditNote.action" },
		],
		"columnDefs": [
		{ className: "Product", "targets": [ 1 ],"width": "30%", },
		],
	});
	$('#get_button').click(function(){
		table = $('#table_data').dataTable();
		table.fnDraw();
	});
</script>