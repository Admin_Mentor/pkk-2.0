<div id="customer_modal_add" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Customer Details</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->create('Customer', ['class' => 'form-horizontal', 'id' => 'Customer_Form']); ?>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Customer Type</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('customer_type_id', array('class' => 'form-control select2', 'type' => 'select', 'required', 'style' => 'width:100%', 'options' => $CustomerType_model, 'id' => 'modal_customer_type_id', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Customer Name</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('name', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'name_modal', 'label' => false,)); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Customer Name In Arabic</label>
                    <div class="col-sm-6">
                        <?= $this->Form->input('arabic_name', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'arabic_name_modal', 'label' => false,'dir'=>'rtl')); ?>
                    </div>
                </div>
                <hr>

                <div class="Advanced_Stng">
                    <a href="#" id="SaleAdvanceBtn">Advanced Settings <i class="fa fa-chevron-down"></i></a>
                </div>


                <div id="SaleAdvance" hidden>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group-btm-spc">
                                <label>Opening Balance</label>
                                <?= $this->Form->input('opening_balance', array('value'=>0,'class' => 'form-control opening_balance', 'type' => 'number', 'step' => 'any', 'required', 'id' => 'opening_balance_modal', 'label' => false,)); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group-btm-spc">
                                <label>Division</label>
                                <?= $this->Form->input('division_id', array('class' => 'form-control select2 division', 'type' => 'select', 'style' => 'width:100%','empty'=>'Select', 'options' => $Division_list, 'id' => 'division_id', 'label' => false,)); ?>
                            </div>
                        </div>
                        <div class="col-md-4 no-padding">
                            <div class="col-md-9">
                                <div class="form-group-btm-spc">
                                    <label>Route</label>
                                    <?= $this->Form->input('route_id', array('class' => 'form-control select2 routes', 'type' => 'select', 'style' => 'width:100%','empty'=>'Select', 'options' => $Route_list, 'id' => 'route_id', 'label' => false,)); ?>
                                </div>
                            </div>
                            <div class="col-md-2">

                                <i class="fa fa-plus-circle fa-2x ad-mar blue-col col_blue_pdng" data-toggle="modal" data-target="#route_add_modal"></i>

                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 2%;">
                        <div class="col-md-4 no-padding">
                            <div class="form-group-btm-spc">
                                <div class="col-md-9">
                                    <label>Group</label>
                                    <?= $this->Form->input('customer_group_id', array('class' => 'form-control select2 customergroup', 'type' => 'select', 'options' => $CustomerGroup_list, 'style' => 'width:100%', 'id' => 'customer_group_id', 'label' => false,)); ?>
                                </div>
                                <div class="col-md-2">

                                    <i class="fa fa-plus-circle fa-2x ad-mar blue-col col_blue_pdng" data-toggle="modal" data-target="#group_add_modal"></i> 

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group-btm-spc">
                                <label>Description</label>
                                <?= $this->Form->input('description', array('class' => 'form-control', 'type' => 'textarea', 'step' => 'any', 'rows' => 2, 'id' => 'description', 'label' => false,)); ?>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group-btm-spc">
                                <label>Credit Limit</label>
                                <?= $this->Form->input('credit_limit', array('class' => 'form-control credit_limit', 'type' => 'number', 'step' => 'any', 'required', 'id' => 'credit_limit_modal', 'value' => 0, 'label' => false,)); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group-btm-spc">
                                <label>Credit Period</label>
                                <?= $this->Form->input('credit_period', array('class' => 'form-control credit_period', 'type' => 'number', 'step' => 'any', 'required', 'id' => 'credit_period_modal', 'value' => 0, 'label' => false,)); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group-btm-spc">
                                <label>Address</label>
                                <?= $this->Form->input('place', array('class' => 'form-control', 'type' => 'text', 'id' => 'place', 'label' => false,)); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group-btm-spc">
                                <label>Email</label>
                                <?= $this->Form->input('email', array('class' => 'form-control', 'type' => 'text', 'id' => 'email', 'label' => false,)); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group-btm-spc">
                                <label>State Code</label>
                                <?= $this->Form->input('state_code', array('class' => 'form-control', 'type' => 'text', 'readonly', 'id' => 'state_code', 'label' => false,)); ?>
                            </div>
                        </div>
                        <div class="col-md-4" style="display: none;">
                            <label>Customer Code</label>
                            <?= $this->Form->input('code', array('class' => 'form-control', 'type' => 'text', 'id' => 'code', 'label' => false,)); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 no-padding" style="display: none;">
                            <div class="form-group-btm-spc">
                                <div class="col-md-9">
                                    <label>State</label>
                                    <?= $this->Form->input('state_id', array('class' => 'form-control select2', 'type' => 'select', 'style' => 'width:100%',  'id' => 'state_id', 'label' => false,)); ?>
                                </div>
                                <div class="col-md-2">
                                    <i class="fa fa-plus-circle fa-2x ad-mar blue-col col_blue_pdng" data-toggle="modal" data-target="#state_add_modal"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group-btm-spc">
                                <label>Mobile No</label>
                                <?= $this->Form->input('mobile', array('class' => 'form-control', 'type' => 'text', 'id' => 'mobile', 'label' => false,)); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group-btm-spc">
                                <label>Contact Person</label>
                                <?= $this->Form->input('contact_person', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'contact_person_modal', 'label' => false,)); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group-btm-spc">
                                <label>
                                    VAT No
                                </label>
                                <?= $this->Form->input('vat_no', array('class' => 'form-control', 'type' => 'text', 'id' => 'vat_no', 'label' => false,)); ?>
                            </div>
                        </div>
                    </div>
                </div>


<!--  <div id="SaleAdvance" hidden>
<div class="form-group">
<div class="col-sm-6">
<label class="col-sm-4 control-label">Opening Balance</label>
<div class="col-sm-6">
<?= $this->Form->input('opening_balance', array('value'=>0,'class' => 'form-control opening_balance', 'type' => 'number', 'step' => 'any', 'required', 'id' => 'opening_balance_modal', 'label' => false,)); ?>
</div>
</div>
<div class="col-sm-6">
<label class="col-sm-4 control-label">Division</label>
<div class="col-sm-6">
<?= $this->Form->input('division_id', array('class' => 'form-control select2 division', 'type' => 'select', 'style' => 'width:100%', 'options' => $Division_list, 'id' => 'division_id', 'label' => false,)); ?>
</div>
<div class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#adddivision"></i></div>
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<label class="col-sm-4 control-label">Route</label>
<div class="col-sm-6">
<?= $this->Form->input('route_id', array('class' => 'form-control select2 routes', 'type' => 'select', 'style' => 'width:100%', 'options' => $Route_list, 'id' => 'route_id', 'label' => false,)); ?>
</div>
<div class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#route_add_modal"></i></div>
</div>
<div class="col-sm-6">
<label class="col-sm-4 control-label">Group</label>
<div class="col-sm-6">
<?= $this->Form->input('customer_group_id', array('class' => 'form-control select2 customergroup', 'type' => 'select', 'options' => $CustomerGroup_list, 'style' => 'width:100%', 'id' => 'customer_group_id', 'label' => false,)); ?>
</div>
<div class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#group_add_modal"></i></div>
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<label class="col-sm-4 control-label">Description</label>
<div class="col-sm-6">
<?= $this->Form->input('description', array('class' => 'form-control', 'type' => 'textarea', 'step' => 'any', 'rows' => 2, 'id' => 'description', 'label' => false,)); ?>
</div>
</div>
<div class="col-sm-6">
<label class="col-sm-4 control-label">Credit Limit</label>
<div class="col-sm-6">
<?= $this->Form->input('credit_limit', array('class' => 'form-control credit_limit', 'type' => 'number', 'step' => 'any', 'required', 'id' => 'credit_limit_modal', 'value' => 0, 'label' => false,)); ?>
</div>
</div>
</div>
<div class="form-group">
<div class="col-sm-6">
<label class="col-sm-4 control-label">Address</label>
<div class="col-sm-6">
<?= $this->Form->input('place', array('class' => 'form-control', 'type' => 'text', 'id' => 'place', 'label' => false,)); ?>
</div>
</div>
<div class="col-sm-6">
<label class="col-sm-4 control-label">Email</label>
<div class="col-sm-6">
<?= $this->Form->input('email', array('class' => 'form-control', 'type' => 'text', 'id' => 'email', 'label' => false,)); ?>
</div>
</div>
</div>
<div class="form-group" style="display:none;">
<label class="col-sm-4 control-label">Customer Code</label>
<div class="col-sm-6">
<?= $this->Form->input('code', array('class' => 'form-control', 'type' => 'text', 'id' => 'code', 'label' => false,)); ?>
</div>
</div>
<div class="form-group" style="display: none;">
<label class="col-sm-4 control-label">State</label>
<div class="col-sm-5">
<?= $this->Form->input('state_id', array('class' => 'form-control select2', 'type' => 'select', 'style' => 'width:100%',  'id' => 'state_id', 'label' => false,)); ?>
</div>
<div class='col-md-1'><i class="fa fa-plus-circle fa-2x ad-mar blue-col" data-toggle="modal" data-target="#state_add_modal"></i></div>
</div>
<div class="form-group" style="display:none;">
<label class="col-sm-4 control-label">State Code</label>
<div class="col-sm-6">
<?= $this->Form->input('state_code', array('class' => 'form-control', 'type' => 'text', 'readonly', 'id' => 'state_code', 'label' => false,)); ?>
</div>
</div>

<div class="form-group">
<div class="col-sm-6">
<label class="col-sm-4 control-label">Mobile No</label>
<div class="col-sm-6">
<?= $this->Form->input('mobile', array('class' => 'form-control', 'type' => 'text', 'id' => 'mobile', 'label' => false,)); ?>
</div>
</div>
<div class="col-sm-6">
<label class="col-sm-4 control-label">Contact Person</label>
<div class="col-sm-6">
<?= $this->Form->input('contact_person', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'contact_person_modal', 'label' => false,)); ?>
</div>
</div>
</div>
<div class="form-group" style="">
<div class="col-sm-6">
<label class="col-sm-4 control-label">VAT No</label>
<div class="col-sm-6">
<?= $this->Form->input('vat_no', array('class' => 'form-control', 'type' => 'text', 'id' => 'vat_no', 'label' => false,)); ?>
</div>
</div>
</div>
</div> -->







<?= $this->Form->end(); ?>

</div>
<div class="modal-footer">
    <button type="button" class="btn btn-primary btn_radious" id='customer_add_button'>Save</button>
</div>
</div>
</div>
</div>

<script type="text/javascript">
    $(document).on('click','#SaleAdvanceBtn',function(){
        if($('#SaleAdvance').is(":visible")){
            $('#SaleAdvance').hide();
        }else{
            $('#SaleAdvance').show();
        } 
    });
</script>