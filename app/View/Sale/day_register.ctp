<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Day Register</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
      <?= $this->Form->create('DayRegister', array('url' => array('controller' => 'Sale', 'action' => 'DayRegister'),'id'=>'DayRegister_Form'));?>
      <div class="form-group">
        <div class="col-md-2 col-sm-2">
          <?= $this->Form->input('executive_id',array('type'=>'select','class'=>'form-control search-field  select_two_class','id'=>'executive_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'required','label'=>'Executive')); ?>
        </div>
        <div class="col-md-2 col-sm-2">
          <?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control search_class pull-right date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'From Date','value'=>$from_date)); ?>
        </div>
        <div class="col-md-2 col-sm-2">
          <?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control search_class pull-right date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'To Date','value'=>$to_date)); ?>
        </div>
        <div class="col-md-2 col-sm-2">
           <?= $this->Form->input('route_id',array('type'=>'select','class'=>'form-control search-field  select_two_class','id'=>'route_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'required','label'=>'Route')); ?>
         </div>
         <div id="enddiv" class="col-md-2 col-sm-2">
           <?= $this->Form->input('customer_group_id',array('type'=>'select','class'=>'form-control search-field select_two_class','id'=>'customer_group_id','style'=>'width: 100%;','empty'=>[''=>'Select'],'options'=>$CustomerGroup_list,'required','label'=>'Group')); ?>
         </div>
       <div class="col-md-1 col-sm-1">
           <?= $this->Form->input('present',array('type'=>'select','class'=>'form-control search-field select_two_class','id'=>'present','style'=>'width: 100%;','empty'=>[''=>'All'],'options'=>['0'=>'Leave','1'=>'Present'],'required','label'=>'Present')); ?>
         </div>
        <!--  <div class="col-md-4 col-sm-4">
           <?= $this->Form->input('km',array('type'=>'text','class'=>'form-control number','id'=>'km','required')); ?>
         </div> -->
       <div class="col-md-1 col-sm-1"><br>
<!--         <button hidden id='edit_button' value='' class="save pull-right edit_icon">Edit</button>
 -->        <button type='button' id='get_button' value='' class="btn btn-success">Get</button>
<!--         <button type='Submit' id='add_button' value='' class="btn btn-success create_icon">Create</button>
 -->      </div>
    </div>
  </div>
    <div class="box-body" style="margin-right: 5px">
          <table class="table table-condensed table table-bordered" id="table_data" data-page-length="25">
      <thead>
        <tr class="blue-bg">
          <th style="width:12%">Executive</th>
          <th>Date</th>
          <th style="width:12%">Route</th>
          <th>Group</th>
          <th>Present</th>
          <th>From Km</th>
          <th>To Km</th>
          <th>Regstr:time</th>
          <th>Close:Time</th>
          <th>Driver Name</th>
          <th>Vehicle No</th>
          <th>Vehicle<br>Condition</th>
          <th style="width:5%">Physical<br>Condition</th>
           <th>Document Verifed <br>
            and Certified</th>
             <th style="width:5%">DayClose</th>
          <!-- <th>Edit</th>
          <th>DayReg</th>
          <th>DayClose</th> -->
        </tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
</div>
</section>
<script type="text/javascript">
  $('#table_data').DataTable( {
    "processing": false,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Sale/day_register_table",
      "type": "POST",
      data:function( d ) {
        d.from_date= $('#from_date').val();
         d.to_date= $('#to_date').val();
        d.customer_group_id= $('#customer_group_id').val();
        d.route_id= $('#route_id').val();
        d.present= $('#present').val();
        d.executive_id= $('#executive_id').val();
      },
      "dataSrc": "records",
    },

   dom: 'Bfrtip',
     buttons: [
    { extend: 'colvis', },
    ],
    "columns": [
    { "data" : "Executive.name" },
    { "data" : "DayRegister.date" },
    { "data" : "Route.name" },
    { "data" : "CustomerGroup.name" ,visible: false},
    { "data" : "DayRegister.present" ,visible: false},
    { "data" : "DayRegister.km" },
    { "data" : "DayRegister.to_km"},
    { "data" : "DayRegister.time" },
    { "data" : "DayRegister.closetime" },
    { "data" : "DayRegister.drivername" },
    { "data" : "DayRegister.VehicleNo" },
    { "data" : "DayRegister.vehicle_condition" },
    { "data" : "DayRegister.physical_condition" },
    { "data" : "DayRegister.document_status" ,visible: false},
    { "data" : "DayRegister.day_close" },
    //{ "data" : "DayRegister.action" },
    //{ "data" : "DayRegister.day_register_delete" },
   // { "data" : "DayRegister.day_close_delete" },
    ],
   "columnDefs": [
 {"targets": [ 14 ],"orderable": false ,},
    ],   
  });
  $('#get_button').click(function(){
    table = $('#table_data').dataTable();
    table.fnDraw();
  });
</script>
<script type="text/javascript">
  $(document).on('click','.edit_dayRegister',function(){
    var Dayregister_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Sale/get_dayregister_details_ajax/"+Dayregister_id,function( data ) {
     $('#executive_id').val(data.DayRegister.executive_id).trigger('change');
     $('#route_id').val(data.DayRegister.route_id).trigger('change');
     $('#customer_group_id').val(data.DayRegister.customer_group_id).trigger('change');
     $('#present').val(data.DayRegister.present).trigger('change');
     $('#km').val(data.DayRegister.km);
     $('#edit_button').show();
     $("#enddiv").append("<input type='hidden' id='DayRegister_id' name='data[DayRegister][id]' value='"+data.DayRegister.id+"'>");
     $('#DayRegister_Form').attr('action','<?= $this->webroot; ?>Sale/EditDayRegister');
   }, "json");
  });
  $(document).on('change','.erp_dayclose',function(){
  var id=$(this).closest('tr').find('td span.day_register_id').text();
   if(!confirm("Are you sure?"))
        {
            return false;
        }
        var data={
            id:id,
        };
        var website_url = '<?php echo $this->webroot; ?>Sale/erp_dayclose';
        $.ajax({
            method: "POST",
            url: website_url,
            data: data,
        }).done(function (data) {
        });
         table = $('#table_data').dataTable();
         table.fnDraw();
  });
  $(".datepicker_month").datepicker( {
    format: "mm-yyyy",
    viewMode: "months", 
    minViewMode: "months"
  });
  $(document).on('click','.delete_action',function(){
    if(!confirm("Are you sure?")) { return false; }
  });
</script>
  <script>
    $('.date_range_picker').daterangepicker({
      opens: 'left',
      locale: { format: 'DD.MM.YYYY'}
    });
    $('.date_range_picker').on('blur', function(ev, picker) {
      $(this).val(picker.startDate.format('DD-MM-YYYY') + ' / ' + picker.endDate.format('DD-MM-YYYY'));
    });
  </script>