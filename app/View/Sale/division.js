$('#modal_division_name').keyup(function(){

  var modal_divison_name=$(this).val();
  var data={
   division_name:modal_divison_name
 };
  var url_address= '<?php echo $this->webroot; ?>'+'Division/division_search';
 $.ajax({
  type: "post",
  url:url_address,
  data: data,
  success: function(response) {
    if(response=="Yes")
    {
      $('#divison_error').html('This Division is already taken');
      $('#add_division').attr('disabled',true);
    }
    else
    {
      $('#divison_error').html('');
       $('#add_division').attr('disabled',false);
    }
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
});
$('.division_disable').keyup(function(){
  var modal_division_name=$('#modal_division_name').val().trim();
  if(modal_division_name!="")
  {
    $('#add_division').attr('disabled',false);
  }
  else{
    $('#add_division').attr('disabled',true);
  }
});
$.fn.division_append=function(result){
  console.log(result)
  // $('#location_id').append($("<option></option>").attr("value",result.key).text(result.value));
  $('.division').append(result);
  var latest_value = $("#division_id option:last").val();
  $('#division_id').val(latest_value).change();
  $('#adddivision').modal('hide');
  $('#division_id_modal').append(result);
  var latest_value = $("#division_modal option:last").val();
  $('#division_id_modal').val(latest_value).change();
}
$('#division_id').change(function(){
  var id=$(this).val();
  $('#division_id_modal').val(id).trigger('change.select2');;
});
$('#division_id_modal').change(function(){
  var id=$(this).val();
  $('#division_id').val(id);
});
$('#add_division').click(function(){
  var modal_division_name=$('#modal_division_name').val();
  var data={
    modal_divion_name:modal_division_name, 
  };
  var url_address= '<?php echo $this->webroot; ?>Division/division_add_ajax';
  $.ajax({
    method: "POST",
    url:url_address,
    data: data,
  }).done(function( result ) {
    
    
    $.fn.division_append(result);
    $('#modal_division_name').val('');
  });
});
$('#Division_Edit_Btn').click(function(){
  var division_id=$('#location_id').val();
  if(!division_id)
  {
    $('#division_id').select2('open');
    return false; 
  }

  var url_address= "<?= $this->webroot; ?>Division/division_get_ajax/"+division_id;
  $.get( url_address, function( response ) {

    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $('#DivisionEditName').val(response.data.name);
      $('#DivisionEditId').val(response.data.id);
      $('#Division_Edit_Modal').modal('show');
    }
  }, "json");
});
$('#edit_Division').click(function(){
  var data=$('#Division_Edit_Form').serialize();
  var division_id=$('#division_id').val();
  var url_address= "<?= $this->webroot; ?>Division/division_edit_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $(".division_id option[value='" + division_id+ "']").remove();
      $('.division').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#division_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#division_id').val($('#division_id option:last-child').val()).trigger('change');
      $('#Division_Edit_Form')[0].reset();
      $('#Division_Edit_Modal').modal('toggle');
    }
  }, "json");
});
