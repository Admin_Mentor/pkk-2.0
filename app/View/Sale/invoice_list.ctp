<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
	<h1>Invoice List</h1>
</section>
<section class="content">
	<div class="box">
		<div class="box-header">
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control invoice-search date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control invoice-search date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
				</div>
			</div>
			<div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
				<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
					<?= $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','empty'=>array(0 => 'No Executive'),'class'=>'form-control select_two_class','style'=>'width: 100%')); ?>
				</div>
			</div>
			<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12"><br>
				<button class='btn btn-success' id='get_button'>Get</button>
			</div>
		</div>
		<div class="box-body">
			<table class="boder table table-condensed table table-bordered" id="table_data" data-order='[[ 0, "desc" ]]' data-page-length='10'>
				<thead>
					<tr class="blue-bg">
						<th>Date</th>
						<th>Executive</th>
						<th>Customer</th>
						<th>Route</th>
						<th>Invoice No</th>
						<th>Total Quantity</th>
						<th class="text-right">Total Amt</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="5" style="font-size:20px; color:red;"></th>
						<th style="font-size:20px; color:red;">Total:</th>
						<th style="font-size:20px; color:red;"></th>
						<th></th>
						<th></th>

					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
</section>
<script type="text/javascript">
	$('#table_data').DataTable( {
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Sale/invoice_list_ajax",
			"type": "POST",
			data:function( d ) {
				d.from_date= $('#from_date').val();
				d.to_date= $('#to_date').val();
				d.executive_id= $('#executive_id').val();
			},
			"dataSrc": "records",
		},
		 dom: 'Bfrtip',
      lengthMenu: [
    [10, 25, 50,-1],
    ['10 rows', '25 rows', '50 rows','show all']
    ],
    buttons: [
   // 'colvis',
    //{ extend: 'excel', title: 'Stock Report', exportOptions: { columns: ':visible' } }, 
    { extend: 'excel',   title: 'Invoice List', exportOptions: { columns: [0,1,2,3,4,5,6,7] } },
    'pageLength',
    ],
		"columns": [
		{ "data" : "Sale.date_of_delivered" },
		{ "data" : "Executive.name" },
		{ "data" : "AccountHead.name" },
		{ "data" : "Route.name" },
		{ "data" : "Sale.invoice_no" },
		{ "data" : "Sale.quantity" },
		{ "data" : "Sale.grand_total",className:"text-right"},
		{ "data" : "Sale.status" },
		{ "data" : "Sale.action" },
		],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};
			pageTotal = api.column( 6, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 6 ).footer() ).html(''+pageTotal.toFixed(2)+'');
		},
		 "columnDefs": [
		{"targets": [ 8 ],"orderable": false ,                          },
		],
	});
	$('#get_button').click(function(){
		table = $('#table_data').dataTable();
		table.fnDraw();
	});
</script>