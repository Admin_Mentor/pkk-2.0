<style type="text/css">
.Table_Border {
border: 1px solid #bbb;
}
.tab_tltp a{
color: white;
}
.btn_for_aprove {
border: none;
border-radius: 3px !important;
background-color: #0c4467 !important;
}
.btn_for_aproved{
border: none;
border-radius: 3px !important;
background-color: #047541 !important;
}
.a_none{
color: #7b0101 !important;
cursor: pointer;
font-weight: 600;
}
.btn_vew {
margin-top: 25px;
border-radius: 3px !important;
border: none;
color: #1e466f;
}

.btn_mrgn_25x{ 
margin-top: 25px;
}

</style>
<section class="content-header">
<h1> Product Wise Offer List<a href="<?php echo $this->webroot ?>Sale/ProductWiseOffers"><button class='btn btn-success pull-right btn_bdr_radious'>New</button></a></h1>
</section>
<section class="content">
<div class="row">
<div class="col-md-12">
<div class="box box-primary box_tp_brdr">
	<div class="row">
    <br>
	<div class="col-md-12">
        <div class="col-md-2">
          <div class="col-md-12">
          <?php echo $this->Form->input('from_date',array(
          'type'=>'text',
          'id'=>'from_date',
          'value'=>$from,
          'class'=>'form-control search_field date_picker datepicker',
          'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
          )); ?>
          </div>
          </div>
          <div class="col-md-2">
          <div class="col-md-12">
          <?php echo $this->Form->input('to_date',array(
          'type'=>'text',
          'id'=>'to_date',
          'value'=>$to,
          'class'=>'form-control  search_field date_picker datepicker',
          'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
           )); ?>
          </div>
           </div>  
        <div id="" class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
          <?php echo $this->Form->input('executive_id',array('type'=>'select','class'=>'form-control  select_two_class','id'=>'executive_id','style'=>'width: 100%;','empty'=>[''=>'All',$Executive],'required','label'=>'Executive')); ?>
          </div>
        </div> 
       <div class="col-md-1 col-sm-1 col-lg-1" style="margin-top: 6px;"><br>
            <button class='btn btn-success' id='get_button'>Get</button>
          </div>
      </div>
  </div>
<div class="row">
<div class="col-md-12">
<div class="box-body table-responsive ">
<table class="table table-condensed table boder boder table-bordered" id="table_data">
<thead>
<tr class="blue-bg">
<th>No </th>
<th>Date</th>
<th>Warehouse</th>
<th>Remarks</th>
<th>Action</th>
</tr>
</thead>
<tbody>
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript">
</script>
<script type="text/javascript">
$('#table_data').DataTable( {
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Sale/product_wise_table_ajax",
        "type": "POST",
			 data:function( d ) {
         d.from_date=$('#from_date').val();
        d.to_date=$('#to_date').val();
        d.executive_id= $('#executive_id').val();
      },
      "dataSrc": "records",
    },
    dom: 'Bfrtip',
      lengthMenu: [
    [10, 25, 50,-1],
    ['10 rows', '25 rows', '50 rows','show all']
    ],
    buttons: [
   // 'colvis',
    //{ extend: 'excel', title: 'Stock Report', exportOptions: { columns: ':visible' } }, 
    { extend: 'excel',   title: 'ProductWiseOffer', exportOptions: { columns: [0,1,2,3] } },
    'pageLength',
    ],
		"columns": [
		{ "data" : "ProductWiseOffer.transfer_no" },
		{ "data" : "ProductWiseOffer.date" },
    { "data" : "Executive.name" },
    { "data" : "ProductWiseOffer.remarks" },
		{ "data" : "ProductWiseOffer.action" },
		],
		"columnDefs": [
		],
});
$('#get_button').click(function(){
    table = $('#table_data').dataTable();
    table.fnDraw();
  });
</script>
