<div id="route_add_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Route</h4>
			</div>
			<div class="modal-body">
				<?php echo $this->Form->create('Route', ['class'=>'form-horizontal','id'=>'Route_Form']); ?>
				<div class="form-group">
					<label class="col-sm-4 control-label">Name</label>
					<div class="col-sm-6">
						<?= $this->Form->input('name',array('class'=>'form-control','type'=>'text','required','id'=>'route_name_modal','label'=>false,)); ?>
						<span id="location_error" style="color:#db1802" class="help-inline"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label">Code</label>
					<div class="col-sm-6">
						<?= $this->Form->input('code',array('class'=>'form-control','type'=>'text','required','id'=>'route_code_modal','label'=>false,)); ?>
						<span id="location_code_error" style="color:#db1802" class="help-inline"></span>
					</div>
				</div>
				
				<?= $this->Form->end(); ?>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn_radious" id='add_route_button'>Save</button>
			</div>
		</div>
	</div>
</div>