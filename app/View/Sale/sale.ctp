<style type="text/css">
.algn_lft{
  text-align: center !important;
}
.pdng_tp{
  margin-top: 15px;
}
.clr_star {
  color: #a29da2;
}
.cart_clr {
  color: #a29da2;
}
.pgn_btm_rht{
  margin-bottom: 15px;
}
.color_of{
  color :red !important;
}
.size_of
{
  font-size:25px !important;
  padding-left: 55px !important;
}
.gst_head_alignment
{
  position: relative;
  align-items: center;
}

#wht_spc_non{white-space: nowrap;}
</style>
<section class="content-header">
  <h1> Sale 
   <div class="pull-right">
<!--     <span class="fa fa-gear" data-toggle="modal" data-target="#exampleModal"></span>
 -->  </div>
  <div class="pull-right">
  <!--   <a href="<?php echo $this->webroot ?>Sale/QuotationList"><button class='btn btn-success pull-right'>QuotationList List</button></a>&nbsp; -->
  </div>
  <div class="pull-right">
    <a href="<?php echo $this->webroot ?>Sale/InvoiceList"><button class='btn btn-success pull-right'>Invoice List</button></a>
  </div>
</h2>
</section>
<section class="content">
  <div class="row-wrapper">
    <div class="box box-primary">
     <?php
     $settings_unit_price='hidden'; if($CustomerSettingList['unit_price'] == 1) $settings_unit_price='';
     $settings_net_value='hidden'; if($CustomerSettingList['net_value'] == 1) $settings_net_value='';
     $settings_tax_amount='hidden'; $settings_tax_amount='';
     $settings_executive='hidden'; if($CustomerSettingList['executive'] == 1) $settings_executive='';
     $settings_warehouse='hidden'; if($CustomerSettingList['warehouse'] == 1) $settings_warehouse='';
     ?>
     <?php $status=$this->request->data['Sale']['status'];
     $sale_type=$this->request->data['Sale']['sale_type'];
     $sale_type1=$this->request->data['Sale']['sale_type1']; ?>
  <!--    <?php $sale_type=$Profile['Profile']['product_configuration_type']; ?> -->
     <?php $sale_type_in_table=$this->request->data['Sale']['sale_type']; ?>
     <?php $sale_type_1_in_table=$this->request->data['Sale']['sale_type1']; ?>
     <?php $Country=$Profile['State']['country_id']; ?>
     <?php if(isset($this->request->data['Sale']['date_of_order'])) : ?>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row size_of"><span class='color_of'>ORDERED DATE </span>: <?= date('d-M-Y', strtotime($this->request->data['Sale']['date_of_order'])); ?></div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="row size_of">
              <span class='color_of'>
                <?php if($this->request->data['Sale']['status']==2) { $process="DELIVERED DATE"; } 
                if($this->request->data['Sale']['flag']==0) { $process="Cancelled Date"; } 
                if(isset($process)) :  echo $process; ?>
              </span>: <?= date('d-M-Y', strtotime($this->request->data['Sale']['date_of_delivered'])); ?>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
  <?php endif; ?>
  <?= $this->Form->create('Sale', array('url' => array('controller' => 'Sale', 'action' => 'Sale')));?>
  <div class="row">
    <div class="col-md-7 col-lg-7 col-sm-7 col-xs-12">
      <div class="form-horizontal" style="margin-top: 15px;">
        <div class="box-body">
          <div class="form-group">
            <div class="col-md-6 settings_executive" <?= $settings_executive ?>>
              <?= $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','class'=>'form-control select2 ','style'=>'width: 100%','empty'=>array(0 => 'No Executive'),'options'=>$Executive_list,'label'=>'Executive','required'=>'required')); ?>
            </div>
            <div class="col-md-3 settings_warehouse" <?= $settings_warehouse ?>>
              <?= $this->Form->input('warehouse_id',array('type'=>'select','id'=>'warehouse_id','class'=>'form-control select2 ','style'=>'width: 100%','empty'=>array(0 => 'Select'),'options'=>$Warehouse_list,'label'=>'Warehouse','required'=>'required')); ?>
            </div>
          </div>
          <!-- <div class="form-group">
            <div class="col-md-6 Toggle_button_for_customer_visibility">
              <input style='' type="checkbox" id='Toggle_button_for_customer_visibility' data-width="130" data-toggle="toggle" data-off="Show Customer" data-on="Hide Customer">
            </div>
          </div> -->
          <div class="form-group general_hidden_area" >
            <div class="settings_customer_type">
              <div class="col-md-5"><?= $this->Form->input('customer_type_id',array('type'=>'select','id'=>'customer_type_id','class'=>'form-control select2','style'=>'width: 100%','options'=>$CustomerType_list,'label'=>'Customer Type','required'=>'required')); ?></div>
              <div class="col-md-1"><br><i class="fa fa-plus-circle fa-2x plus-btn" data-toggle="modal" data-target="#customer_type_modal"></i></div>
            </div>
            <!-- <div class="general_customer_hide" style="display: none;"> -->
              <div class="general_customer_hidden_area general_customer_hide" style="display: none;">
                <div class="col-md-5"><?= $this->Form->input('account_head_id',array('type'=>'select','class'=>'form-control select2','style'=>'width: 100%','options'=>$Customer_list,'label'=>'Customer','required'=>'required')); ?></div>
              <!--   <div class="col-md-1"><br><i class="fa fa-plus-circle fa-2x plus-btn modal_round_mrgn" data-toggle="modal" data-target="#customer_modal_add"></i></div> -->
              </div>
            <!-- </div> -->
          </div>
          <div class="form-group" id='general_customer_hidden_area' style="display: none;" >
            <div class="col-md-6">
              <?= $this->Form->input('customer_name',array('type'=>'text','id'=>'customer_name','class'=>'form-control','label'=>'Customer Name','autofocus'=>'')); ?>
            </div>
            <div class="col-md-6">
              <?= $this->Form->input('customer_mobile',array('type'=>'text','id'=>'customer_mobile','class'=>'form-control','label'=>'Customer Mobile')); ?>
            </div>
          </div>
          <div id="address-area" class="form-group general_hidden_area" style="display: none;">
            <div class="col-md-10">
              <input type="hidden" id="credit_limit" value="0">
              <input type="hidden" id="prevbalance" value="0">
              <?= $this->Form->input('address',array('class'=>'form-control','type'=>'textarea','disabled','rows'=>2,'id'=>'address','label'=>'Address',)); ?>
              <?= $this->Form->input('customer_type',array('type'=>'hidden','id'=>'customer_type')); ?>
            </div>
            <div class="col-md-2">
              <?= $this->Form->input('division',array('type'=>'text','id'=>'division','class'=>'form-control','readonly','label'=>'Category')); ?>
              <?= $this->Form->input('state_id',array('class'=>'form-control','type'=>'hidden','id'=>'state_id','label'=>false,)); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-5 col-lg-5 col-sm-5 col-xs-12">
      <div class="form-horizontal" style="margin-top: 15px;">
        <div class="box-body">
          <div class="col-md-12">
            <div class="col-md-6">
              <?= $this->Form->input('invoice_no',array('class'=>'form-control','type'=>'text','required','id'=>'invoice_no','readonly')); ?>
            </div>
            <div class="col-md-6">
              <?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="box-body">
        <div class="col-md-12">
          <div class="col-md-6 pd-lt-0" id='product_configuration_type'>
           <?php $options = array(
              //'Retail' => 'Retail
            'Retail' => 'Retail
            &nbsp;&nbsp;&nbsp;&nbsp;',
            //'WholeSale' => 'WholeSale'
            'WholeSale' => 'WholeSale'
           
          );
           $attributes = array(
            'legend' => false,
            'value' => 0,
            'class'=>'toggle_button_for_sale_type',
          );
          echo $this->Form->radio('type', $options, $attributes); ?>
        </div>

        <?php
        if($show_cost ==1) : ?>
        <div class="col-md-6 pd-lt-0 settings_show_margin" id='margin_visibility' >
          <input class='nav-toggle' type="checkbox" id='toggle_button_for_margin_visibility' data-width="130" data-toggle="toggle" data-off="Show Margin" data-on="Hide Margin">
        </div>
        <div class="col-md-6 pd-lt-0" id='cost_visibility' style="margin-top: 25px;" hidden>
          <input class='nav-toggle' type="checkbox" id='toggle_button_for_cost_visibility' data-width="130" data-toggle="toggle" data-off="Show Cost" data-on="Hide Cost">
        </div>
      <?php endif; ?>
      <div class="col-md-6 pd-lt-0" id='gst_visibility' style="display: none;">
        <input class='nav-toggle' type="checkbox" id='toggle_button_for_gst_visibility' data-width="130" data-toggle="toggle" data-off="Show Gst" data-on="Hide Gst">
      </div>
    </div>
  </div>
  <div class="box-body">
    <div class="col-md-12 col-lg-12 col-sm-12">
      <div class="col-md-6 col-lg-6 col-sm-6 pd-lt-0" id="wht_spc_non">
       <?php $options1 = array(
        'CashSale' => 'CashSale &nbsp;&nbsp;&nbsp;&nbsp;',
        'CreditSale' => 'CreditSale'
      );
       $attributes1 = array(
        'legend' => false,
        'value' => 0,
        'class'=>'toggle_button_for_sale_type_1',
      );
      echo $this->Form->radio('type1', $options1, $attributes1); ?>
    </div>
  </div>
</div>
  <div class="box-body" hidden>
    <div class="col-md-12 col-lg-12 col-sm-12">
      <div class="col-md-6 col-lg-6 col-sm-6 pd-lt-0" id="wht_spc_non">
       <?php $options2 = array(
        'Quotation' => 'Quotation &nbsp;&nbsp;&nbsp;&nbsp;',
        'Sale' => 'Sale'
      );
       $attributes2 = array(
        'legend' => false,
        'value' => 1,
        'class'=>'toggle_button_for_sale_type_2',
      );
      echo $this->Form->radio('type2', $options2, $attributes2); ?>
    </div>
  </div>
</div>
</div>
</div>
<div class="col-md-12">
  <div class="box-body table-responsive no-padding">
    <table class="boder table table-condensed table" id="product_table">
      <thead>
        <tr class="blue-bg">
          <th width="10%" hidden> Barcode</th>
          <th width="25%"> Product</th>
          <th width="10%">Unit</th> 
          <th class='sale_type' >Rate</th>
          <th class='sale_type' style='display:none'>Rate</th>
     <!--      <th class="settings_unit_price">Unit Price</th> -->
          <th style="display:none;" class="cost-field">Cost</th>
          <th style='display:none' class="margin-field">Margin</th>
          <th>Quantity</th>
          <th>Free Qty</th>
          <th>Amount</th>
          <th  class=''>Tax</th>
          <th class="settings_net_value">Taxable Value</th>
          <th  class='settings_tax_amount'>Tax Amount</th>
          
          <?php if($status!=2) : ?>
            <th>Action</th>
          <?php endif; ?>
        </tr>
        <?php
        $text_type ='readonly';
        if($this->Session->read('User.id')==1)
        {
          $text_type="";
        }
        ?>
        <?php if($status!=2) : ?>
          <tr class="blue-pddng">
            <td hidden><?= $this->Form->input('barcode',   ['type'=>'text'  ,'id'=>'barcode','class'=>'form-control','label'=>false,'style'=>'width:100%',"tabindex"=>"7"]); ?></td>
            <td><?= $this->Form->input('product',['type'=>'select','id'=>'product','class'=>'form-control select2','empty'=>[''=>'Select'],'options'=>$Product_list,'label'=>false]); ?></td>
            <td><?= $this->Form->input('unit_id',['type'=>'select','id'=>'quantity_mode','class'=>'form-control select2','options'=>$Unit,'label'=>false]); ?>
              <?= $this->Form->input('unit_level',['type'=>'hidden','id'=>'unit_level','value'=>1,'class'=>'form-control','label'=>false]); ?>
              <?= $this->Form->input('selling_rate',['type'=>'hidden','id'=>'selling_rate','class'=>'form-control','label'=>false]); ?>
              <?= $this->Form->input('unit_price',['type'=>'hidden','id'=>'unit_price',$text_type,'class'=>'form-control number single_calculator','label'=>false]); ?>
         
            </td> 
            <td class='sale_type' style='display:none' ><?= $this->Form->input('mrp',['type'=>'text','id'=>'retail_price','class'=>'form-control mrp single_calculator','label'=>false]); ?></td>
            <td class='sale_type' ><?= $this->Form->input('whole_sale_price',['type'=>'text','id'=>'whole_sale_price','class'=>'form-control mrp single_calculator','label'=>false]); ?>
              <?= $this->Form->input('hidden_mrp',['type'=>'hidden','id'=>'hidden_mrp','class'=>'form-control hidden_mrp','label'=>false]); ?></td>
       <!--      <td class="settings_unit_price" ></td> -->
            <td style="display:none;" class="cost-field"><?= $this->Form->input('cost',['type'=>'text','id'=>'cost',$text_type,'class'=>'form-control number single_calculator','label'=>false]); ?></td>
            <td style='display:none' class="margin-field"><?= $this->Form->input('profit',['type'=>'text','id'=>'profit','class'=>'form-control number single_calculator ','label'=>false,'readonly']); ?></td>
            <td><?= $this->Form->input('quantity',['value'=>'1','type'=>'text','id'=>'quantity','class'=>'form-control number single_calculator','label'=>false]); ?></td>
            <td><?= $this->Form->input('free_qty',['value'=>'1','type'=>'text','id'=>'free_qty','class'=>'form-control number single_calculator','label'=>false]); ?></td>
            <td><?= $this->Form->input('total',['type'=>'text','id'=>'single_total','class'=>'form-control number single_calculator','value'=>'0','label'=>false]); ?></td>
            <td class=''><?= $this->Form->input('tax',['type'=>'text','value'=>'0','id'=>'tax','class'=>'form-control number single_calculator','label'=>false]); ?></td>
            <td class="settings_net_value"><?= $this->Form->input('net_value',['type'=>'text','id'=>'net_value','class'=>'form-control number single_calculator','label'=>false]); ?></td>
            <td class='settings_tax_amount'><?= $this->Form->input('tax_amount',['type'=>'text','value'=>'0','id'=>'tax_amount','class'=>'form-control number single_calculator','label'=>false]); ?></td>
            <td><i class="fa fa-plus-circle fa-2x ad-mar blue-col" id='add_to_cart'></i></td>
          </tr>
        <?php endif; ?>
      </thead>
      <tbody>
        <?php if(isset($SaleItem)) : 
        // pr($SaleItem);
        // exit;
        ?>
        <?php $sl_no=1; foreach ($SaleItem as $key => $value): ?>
          <tr class="blue-pddng">
           <td hidden><input value='<?= $value['Product']['barcode']; ?>' class='form-control product_barcode' readonly type='text' ></td>
           <td colspan="">
            <input class="productlist productsrow" hidden name="data[SaleItem][product_id][]" value="<?= $value['SaleItem']['product_id']; ?>">
            <input value='<?= $value['Product']['name']; ?> <?= $value['Product']['code']; ?>' class='form-control product_name' readonly type='text'>
            <input value='<?= $value['SaleItem']['id']; ?>' name='data[SaleItem][SaleItem_id][]' type='hidden' class='table_id'>
             <input value='<?= $value['Stock']['quantity']; ?>' type='hidden' class="stock_quantity">
              <input value='<?= $value['Product']['no_of_piece_per_unit']; ?>' type='hidden' class="no_of_piece_per_unit">
              <input value='<?= $value['SaleItem']['quantity_mode']; ?>' class='form-control quantity_mode'  type='hidden'>
              <input name='data[SaleItem][warehouse_id][]' value='<?= $value['SaleItem']['warehouse_id']; ?>' type='hidden'>
          </td>
          <td colspan=""><input value='<?= $value['SaleItem']['quantity_mode']; ?>' class='form-control quantity_mode' readonly type='text'>
          <input name='data[SaleItem][unit_id][]' value='<?= $value['SaleItem']['unit_id']; ?>' type='hidden'>
          </td>
       <!--    <td><input readonly value='<?= $value['SaleItem']['unit_price']; ?>' class='form-control'  name='data[SaleItem][mrp][]' type='text'></td> -->
          <td class="settings_unit_price" ><input readonly value='<?= $value['SaleItem']['unit_price']; ?>' class='form-control'  name='data[SaleItem][unit_price][]' type='text'>
            <input name="data[SaleItem][actual_price][]" class="form-control" value='<?= $value['SaleItem']['actual_price']; ?>' type="hidden">
</td>
          <?php 
          $margin=($value['SaleItem']['unit_price']-$value['Product']['cost']);
          ?> 
          <td style="display:none;" class="cost-field" colspan="1"><input readonly value='<?= $value['Product']['cost']; ?>' class='form-control'  name='data[SaleItem][cost][]' type='text'></td>
          <td style="display: none;" class="margin-field" colspan="1"><input readonly value='<?= $margin; ?>' class='form-control'  name='data[SaleItem][margin][]' type='text'></td>
          <td ><input readonly value='<?= $value['SaleItem']['quantity'] ; ?>' class='form-control quantity'  name='data[SaleItem][quantity][]' type='text'></td>
           <td ><input readonly value='<?= $value['SaleItem']['free_qty'] ; ?>' class='form-control free_qty'  name='data[SaleItem][free_qty][]' type='text'></td>
           <td><input readonly value='<?= $value['SaleItem']['unit_price']*$value['SaleItem']['quantity']; ?>' class='form-control row_total' 
            type='text'></td>
          <td><input readonly value='<?= round($value['SaleItem']['tax'],2); ?>' class='form-control'  name='data[SaleItem][tax][]' type='text'></td>
          <td class="settings_net_value"><input readonly value='<?= round($value['SaleItem']['net_value'],2); ?>' class='form-control row_taxable_value'  name='data[SaleItem][net_value][]' type='text'></td>
          <td class="settings_tax_amount"><input readonly value='<?= round($value['SaleItem']['tax_amount'],2); ?>' class='form-control tax-amount row_tax_amount'  name='data[SaleItem][tax_amount][]' type='text'></td>
          
          <?php if($status!=2) : ?>
            <td><i class="fa fa-minus-circle fa-2x remove_old_tr" ></i></td>
          <?php endif; ?>
        </tr>
      <?php endforeach ?>
    <?php endif; ?>
  </tbody>
  <tfoot>
    <tr >
      <!-- <td colspan="3"></td> -->
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td style="display:none;" class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <!-- <td colspan=""></td> -->
     <!--  <td></td> -->
      <td><h4>Total</h4></td>
      <td><?= $this->Form->input('total_quantity',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'total_quantity','label'=>false,)); ?></td>
      <td></td>
      <td><?= $this->Form->input('main_total',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'main_total','label'=>false,)); ?></td>
      <td></td>
      <td><?= $this->Form->input('taxable_value_total',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'taxable_value_total','label'=>false,)); ?></td>
      <td><?= $this->Form->input('tax_amount_total',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'tax_amount_total','label'=>false,)); ?></td>
      

      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
          <tr>
      <td colspan="3"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td></td>
      <td style="display:none;" class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td><label class="control-label" style="float: right;">CGST Amount</label></td>
      <td><?= $this->Form->input('cgst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'cgst_amt_total','readonly','label'=>false,)); ?></td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
          <tr>
      <td colspan="3"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td></td>
      <td style="display:none;" class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td><label class="control-label" style="float: right;">SGST Amount</label></td>
      <td><?= $this->Form->input('sgst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'sgst_amt_total','readonly','label'=>false,)); ?></td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
     <tr>
      <td colspan="3"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td></td>
      <td style="display:none;" class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td><label class="control-label" style="float: right;">IGST Amount</label></td>
      <td><?= $this->Form->input('igst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'igst_amt_total','readonly','label'=>false,)); ?></td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
    <tr class="settings_discount">
      <td colspan="3"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td></td>
      <td style="display:none;"  class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td><label class="control-label" style="float: right;">Discount</label></td>
      <td><label class="control-label" style="float: right;">Discount Amount</label></td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
    <tr  class="settings_discount">
      <td colspan="3"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td style="display:none;"  class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td>
        <select class="form-control select2" id="select_discount_type">
          <option value="1">INR</option>
          <option value="2">%</option>
        </select>
      </td>
      <td><?= $this->Form->input('discount',array('class'=>'form-control','type'=>'text','readonly','max'=>'100','id'=>'discount','label'=>false)); ?></td>
      <td><?= $this->Form->input('discount_amount',array('class'=>'form-control','type'=>'text','id'=>'discount_amount','label'=>false)); ?></td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
    
    <tr hidden>
      <td colspan="3"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td></td>
      <td style="display:none;" class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td><label class="control-label" style="float: right;">Taxable Amount</label></td>
      <td><?= $this->Form->input('taxable_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'taxable_total','readonly','label'=>false,)); ?></td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
    <tr hidden>
      <td colspan="2"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td></td>
      <td style="display:none;" class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td><label class="control-label" style="float: right;">VAT %</label></td>
      <td><?= $this->Form->input('total_tax',array('class'=>'form-control main_calculator','type'=>'text','step'=>'any','required','id'=>'total_tax','value'=>0,'readonly','label'=>false,)); ?></td>
      <td><label class="control-label" style="float: right;">VAT Amount</label></td>
      <td><?= $this->Form->input('tax_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'tax_amt_total','readonly','label'=>false,)); ?></td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
    <tr>
      <td colspan="3"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td></td>
      <td style="display:none;" class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td><label class="control-label" style="float: right;">Total</label></td>
      <td><?= $this->Form->input('total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'sale_total','readonly','label'=>false,)); ?></td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
    <tr hidden>
      <td colspan="3"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td></td>
      <td style="display:none;" class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td>
        <label class="control-label" style="float: right;">Round Off</label>
        <!-- <?= $this->Form->input('other_name',array('class'=>'form-control','type'=>'text','required','id'=>'other_name','value'=>'Round Off','label'=>false,)); ?> -->
      </td>
      <td><?= $this->Form->input('other_value',array('class'=>'form-control main_calculator','type'=>'text','step'=>'any','','id'=>'other_value','label'=>false,)); ?></td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
    
    <tr>
      <td colspan="3"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td></td>
      <td style="display:none;" class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td><label class="control-label" style="float: right;">Grand Total</label></td>
      <td><?= $this->Form->input('grand_total',array('class'=>'form-control main_calculator','type'=>'text','step'=>'any','required','id'=>'grand_total','label'=>false,)); ?></td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
    <tr >
      <td colspan="3"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td></td>
      <td style="display:none;" class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td><label class="control-label" style="float: right;">Paid</label></td>
      <td><?= $this->Form->input('paid',array('class'=>'form-control payment_calculator','type'=>'number','step'=>'any','required','id'=>'paid','label'=>false,'readonly')); ?></td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
    <tr class="balance">
      <td colspan="3"></td>
      <td class="settings_unit_price"></td>
      <td class="settings_net_value"></td>
      <td class="settings_tax_amount"></td>
      <td></td>
      <td style="display:none;" class="cost-field"></td>
      <td style='display:none' class="margin-field"></td>
      <td>
       <?php if($status!=2) : ?>
        <label class="control-label" style="float: right;">Balances</label></td>
        <td><?= $this->Form->input('balance',array('class'=>'form-control payment_calculator','type'=>'number','step'=>'any','required','id'=>'balance','label'=>false,'readonly')); ?>
        <?php endif; ?>
        <label style="color:red" id="messagelbl"></label>
      </td>
      <?php if($status!=2) : ?>
        <td></td>
      <?php endif; ?>
    </tr>
  </tfoot>
</table>
</div>
</div>
<?= $this->Form->input('total_tax_amount',array('type'=>'hidden','step'=>'any','id'=>'total_tax_amount')); ?>
<div class="row">
  <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 col-md-offset-5">
    <br>
    <?php if(isset($SaleItem)) : ?>
      <?php  $flag=$this->request->data['Sale']['flag']; ?>
      <?php  if($flag==1) : ?>
        <?php if($status==1) : ?>
          <button type='Submit' name='data[Sale][process]' value='delete' class="btn btn-danger">Delete</button>
          <button type='Submit' name='data[Sale][process]' value='cancel' class="btn btn-warning">Cancel</button>
          <button type='Submit' name='data[Sale][process]' value='update' class="btn btn-success">Update</button>
          <button type='Submit' id="save_buttton" name='data[Sale][process]' value='delivery' class="btn btn-primary invoice-action">Save & Print Invoice</button>
          <a class="sale-print" target="_blank" href="<?= $this->webroot; ?>Print/fpdf/<?= $this->request->data['Sale']['id'] ?>"></a>
        <?php elseif($status==2) : ?>
          <button type='Submit' name='data[Sale][process]' value='delete' class="btn btn-danger">Delete</button>
          <a target="_blank" href="<?= $this->webroot.'Print/fpdf/'.$this->request->data['Sale']['id']; ?>"><button type='button' class="btn btn-success">Print</button></a>
          <!--<button type='submit' name='data[Sale][process]' value='altration' class="btn btn-primary">Altration</button>-->
        <?php elseif($status==3) : ?>
          <button type='Submit' name='data[Sale][process]' value='delete' class="btn btn-danger">Delete</button>
          <button type='Submit' name='data[Sale][process]' value='after_altration' class="btn btn-success">Update</button>
        <?php else :  ?> 
        <?php endif; ?>
      <?php else :  ?> 
        <button type='Submit' name='data[Sale][process]' value='delete' class="btn btn-danger">Delete</button>
      <?php endif; ?>
    <?php else :  ?>
<!--       <button type='Submit' name='data[Sale][process]'  id="quotation" value='save' class="btn btn-success invoice-action">Save & Print Quotation</button>
 -->      <button type='Submit' name='data[Sale][process]' id="save_buttton" value='delivery' class="btn btn-primary invoice-action">Save & Print Invoice</button>  
    <?php endif; ?>
  </div>
</div>
<?= $this->Form->end(); ?>
<br/>
</div>
<?php require('sales_settings.php') ?>
<?php require('customer_modal.php') ?>
<?php require('customer_type_modal.php') ?>
<?php require('route_modal.php') ?>
<?php require('customer_group_modal.php') ?>
<?php require('Division_Add_modal.php') ?>
</section>
<script type="text/javascript">
  <?php require('sale.js') ?>
  <?php require('customer_type.js') ?>
  <?php require('customer.js') ?>
  <?php require('division.js') ?>
  shortcut.add("alt+q", function() {
    $('#print_quotation').click();
  });
  shortcut.add("alt+i", function() {
    $('#print_invoice').click();
  });
  $("#Customer_id").change();
  var status='<?= $status; ?>';
  var sale_type_in_table='<?= $sale_type_in_table; ?>';
  if(sale_type_in_table=='Retail')
  {
    $('.toggle_button_for_sale_type')[0].checked = true;
  }
  var sale_type_1_in_table='<?= $sale_type_in_table; ?>';
  if(sale_type_1_in_table=='CreditSale')
  {
    //$('.toggle_button_for_sale_type_1')[0].checked = true;
  }
  <?php if(!empty($this->request->data['Sale']['id'])) : ?>
  if(status==2)
  {
   var id ="<?=$this->request->data['Sale']['id']?>";
   var link = "<?= $this->webroot.'Print/fpdf/'?>"+id;
   window.open(link, '_blank');
   var current_url="<?= $this->here ?>";
   var current_url_split=current_url.split("/");
   // window.location.replace(current_url_split[0]+"/"+current_url_split[1]+"/"+current_url_split[2]+"/"+current_url_split[3]);
 } 
 else if(status == 2) 
 {
  if(confirm('Print ?')) {
    var id ="<?=$this->request->data['Sale']['id']?>";
    var link = "<?= $this->webroot.'Print/fpdf/'?>"+id;
    window.open(link, '_blank');
  }
  //var current_url="<?= $this->here ?>";
 // current_url.location;
} 
else 
{
  $('.toggle_button_for_sale_type_2')[0].checked = true;
  //var current_url="<?= $this->here ?>";
  current_url.location;
  if(confirm('Print ?')) {
   var id ="<?=$this->request->data['Sale']['id']?>";
   var link = "<?= $this->webroot.'Print/qutofpdf/'?>"+id;
   window.open(link, '_blank');
 }
 current_url.location;
}


<?php endif; ?>
// $(document).on('click','.toggle_button_for_sale_type_2',function(){
//     <?php if(empty($this->request->data['Sale']['id'])) : ?>

//   $("#product_table tbody tr").each(function () {
//    $(this).closest('tr').remove();
//  $.fn.button_disable();
 
//     });
//   <?php endif; ?>
// });
</script>