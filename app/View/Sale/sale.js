
var unit_change_flag=0;
$.fn.total_tax=function(){
var each_total=0;
$('#product_table tbody tr').each(function(){
var single_total=$(this).closest('tr').find('td input.tax-amount').val();
each_total+=parseFloat(single_total);
});
var main_total=each_total;
$('#total_tax_amount').val(main_total);
}
$.fn.warehouseproducts=function(){
var id=$('#warehouse_id option:selected').val();
$.post( "<?= $this->webroot ?>Product/get_product_quantity_by_warehouse/"+id,function( data ) {
$('#product').html(data.option);
if(data.result=='Success')
{
$('#product').change();
}
}, "json");
}
$.fn.warehouseload=function(){
// var id=$('#executive_id option:selected').val();
// if(!id)
//   return false;
// $.post( "<?= $this->webroot ?>Executive/get_warehouse_by_executive/"+id,function( data ) {

//   if(data.result=='Success')
//   {
//     $('#warehouse_id').html(data.option);
//     $('#warehouse_id').change();

//   }
// }, "json");
}
$.fn.warehouseload();
$('#single_total').on('blur',function(){
$('#discount_amount').focus();
})
$('#add_to_cart').click(function(event){
event.preventDefault();
stock_quantity=3000;
var ProductExist = 0;
var check1=0;
var tablecount=$("#product_table tbody tr").length;
var product_text=$('#product option:selected').text();
var barcode=$('#barcode').val();
var product=$('#product').val();
var quantity=$('#quantity').val();
var free_qty=$('#free_qty').val();
var unit=$('#quantity_mode').val();
var quantity_mode=$('#quantity_mode').val();
var warehouse_id=$('#warehouse_id').val();
var unit_text=$('#quantity_mode option:selected').text();
var unit_level=$('#unit_level').val();
if(!unit){
$('#quantity_mode').select2("open");
return false;
}
var sl_no=1;
$("#product_table tbody tr").each(function () {
sl_no++;
var productId = $(this).closest('tr').find('td input.productsrow').val();
var quantiry = $(this).closest('tr').find('td:eq(6) input').val();
var free_quantiry = $(this).closest('tr').find('td:eq(7) input').val();
var table_product_unit = $(this).closest('tr').find('td:eq(2) input:eq(0)').val();
if (product== productId) {
if(table_product_unit==unit){
ProductExist = 1;
return false;
}
}
});
if(ProductExist)
{
alert("This product already in cart");
$('#product').select2('open');
return false;
}
if(!quantity)
{
$('#quantity').focus();
return false;
}

var quantity=parseFloat(quantity,10);
if(quantity<=0)
{
$('#quantity').focus();
return false;
}
if(!free_qty)
{
$('#free_qty').focus();
return false;
}
if(free_qty<0)
{
$('#free_qty').focus();
return false;
}
sale_qty=parseFloat(free_qty)+parseFloat(quantity);
if(parseFloat(available_quantity)<parseFloat(sale_qty))   
{
alert('Insufficient quantity');
$('#quantity').focus();
return false;
}
if(!product){
$('#product').select2('open');
return false;
}
var warehouse=$('#warehouse_id').val();
var warehouse_text=$('#warehouse_id option:selected').text();
if(!warehouse){
$('#warehouse_id').select2('open');
return false;
}
var hidden_mrp=$('#hidden_mrp').val();
var unit_price=$('#unit_price').val();
var cost=$('#cost').val();
var margin=$('#profit').val();
var net_value=$('#net_value').val();
var tax=$('#tax').val();
var tax_amount=$('#tax_amount').val();
var single_total=$('#single_total').val();
var mrp=$('#unit_price').val();
var toggle_button_for_sale_type=$('.toggle_button_for_sale_type').prop('checked');
if(toggle_button_for_sale_type==true)
{
mrp=$('#retail_price').val();
}
else
{
mrp=$('#whole_sale_price').val();
}
var toggle_button_for_cost = 'display:none';
if($('#toggle_button_for_cost_visibility').is(":checked")){
toggle_button_for_cost ='';
}
var toggle_button_for_margin = 'display:none';
if($('#toggle_button_for_margin_visibility').is(":checked")){
toggle_button_for_margin ='';
}
var settings_unit_price='hidden';
if($('.settings_unit_price').is(":visible")){
var settings_unit_price='';
}
var settings_net_value='hidden';
if($('.settings_net_value').is(":visible")){
var settings_net_value='';
}
var settings_tax_amount='hidden';
if($('.settings_tax_amount').is(":visible")){
var settings_tax_amount='';
}
if(quantity_mode==2)
{
unit_mode="Cases";
}
else
{
unit_mode="Pieces";
}
$('#product_table tbody').prepend('<tr>\
<td hidden><input class="form-control product_barcode" value="'+barcode+'" readonly></td>\
<td colspan="1">\
<input class="productlist productsrow" hidden name="data[Sale][product_id][]" value="'+product+'">\
<input class="form-control product_name" value="'+product_text+'" readonly>\
<input class="form-control stock_quantity" value="'+available_quantity+'" type="hidden">\
<input class="form-control no_of_piece_per_unit " value="'+no_of_piece_per_unit+'" type="hidden">\
<input class="form-control quantity_mode " value="'+unit_mode+'" type="hidden">\
</td>\
<td><input name="data[Sale][unit_id][]" hidden class="productlist productsrow" value='+unit+'>\
<input class="form-control quantity_mode" value="'+unit_text+'" readonly>\
<input hidden value="'+unit_level+'" >\
</td>\
<td><input name="data[Sale][unit_price][]" class="form-control" value='+mrp+' readonly><input name="data[Sale][actual_price][]" class="form-control" value='+hidden_mrp+' type="hidden"></td>\
<td class="cost-field" style="'+toggle_button_for_cost+'" ><input name="data[Sale][cost][]" class="form-control" value='+cost+' readonly></td>\
<td style="'+toggle_button_for_margin+'" class="margin-field"><input class="form-control" value="'+margin+'" readonly></td>\
<td><input class="form-control quantity" name="data[Sale][quantity][]" value='+quantity+' readonly ></td>\
<td><input class="form-control quantity" name="data[Sale][free_qty][]" value='+free_qty+' readonly ></td>\
<td><input class="form-control row_total" value='+single_total+' id="total" readonly></td>\
<td><input class="form-control" name="data[Sale][tax][]" value='+tax+' readonly></td>\
<td class="settings_net_value" '+settings_net_value+'><input class="form-control row_taxable_value" name="data[Sale][net_value][]" value='+net_value+' readonly></td>\
<td class="settings_tax_amount" '+settings_tax_amount+'><input class="form-control tax-amount row_tax_amount" name="data[Sale][tax_amount][]" value='+tax_amount+' readonly></td>\
<td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
</tr>');
 $('#product').val('');
 $('#product').select2('open');
$('.single_calculator').val('0');
$('#quantity').val('1');
$('#free_qty').val('');
$('#barcode').val('');
$('#retail_price').val('0');
$('#whole_sale_price').val('0');
$('#quantity_mode').val(0).change();
$('#product').val('').trigger('change');
$.fn.main_calculater();
$.fn.button_disable();
//$.fn.total_tax();
if(barcode) {
$('#barcode').focus();
} else {
$('#product').select2('open');
}
});
function roundToTwo(num) {
return +(Math.round(num + "e+2")  + "e-2");
}

$('.mrp').keyup(function(){
$('#unit_price').val($(this).val());
$('#unit_price').keyup();
});
$('#quantity_mode').change(function(){  
var quantity_mode=$('#quantity_mode').val();
unit_change_flag=1;
$.fn.get_product_details(quantity_mode);
});
$('#product').change(function(){
$('.single_calculator').val('0');
$('#quantity').val('0');
$('#free_qty').val('');
$('#retail_price').val('0');
$('#whole_sale_price').val('0');
unit_change_flag=0;
$.fn.get_product_details(null);

});
$.fn.get_product_details=function(quantity_mode){
var party=$('#SaleAccountHeadId').val();
var id=$('#product').val();
var warehouse_id=$('#warehouse_id').val();

if(id!="")
{

var url_address= '<?php echo $this->webroot; ?>'+'Product/get_Product_ajax_sale/'+id+'/'+party+'/'+quantity_mode+'/'+warehouse_id;
$.ajax({
type: "POST",
url:url_address,
dataType:'json',
success: function(data) {
if(data.result!='Success')
{
//alert(data.result);
return false;
}
$('#retail_price').val(data.Product.mrp);
no_of_piece_per_unit=data.Product.no_of_piece_per_unit;
available_quantity=data.available_quantity;
$('#cost').val(data.Product.cost);
$('#unit_level').val(data.Product.level);
$('#whole_sale_price').val(data.Product.lastunitcost);
$('#hidden_mrp').val(data.Product.wholesale_price);
$('#selling_rate').val(data.Product.selling_rate);
if(data.Product.selling_rate>0)
{
$('#retail_price').val(data.Product.selling_rate);
$('#whole_sale_price').val(data.Product.selling_rate);
}
$('#tax').val(data.Product.tax);
var toggle_button_for_sale_type=$('.toggle_button_for_sale_type').prop("checked");
if(toggle_button_for_sale_type==false)
{
$('#whole_sale_price').keyup();
}
else
{
$('#retail_price').keyup();
}
$('.single_calculator').keyup();
if(unit_change_flag==0)
{
$('#quantity_mode').val(data.unit_id).change();
$('#quantity').focus();
}
//$('#retail_price').select();

},
error:function (XMLHttpRequest, textStatus, errorThrown) {
alert(textStatus);
}
});
}
}

$('#SaleSaleForm').on('keyup keypress', function(e) {
var keyCode = e.keyCode || e.which;
if (keyCode === 13) { 
e.preventDefault();
return false;
}
});

$('.single_calculator').keyup(function(e){
var unit_price=$('#unit_price').val()? $('#unit_price').val():0;

var toggle_button_for_sale_type=$('.toggle_button_for_sale_type').prop("checked");
if(toggle_button_for_sale_type==false)
{
var mrp=$('#whole_sale_price').val()? $('#whole_sale_price').val():0;
}
else
{
var mrp=$('#retail_price').val()? $('#retail_price').val():0;
}
if (e.keyCode == 13) 
{
var product_quantity=$('#quantity').val();
if(!$.isNumeric(product_quantity) || product_quantity=='' || product_quantity<=0 ) {
$('#quantity').val('');
$('#quantity').focus();
return false;
}
$('#free_qty').focus();
var product_free_quantity=$('#free_qty').val();
if(!$.isNumeric(product_free_quantity) || product_free_quantity=='' || product_free_quantity<0 ) {
$('#free_qty').val('');
$('#free_qty').focus();
return false;
}
$('#add_to_cart').trigger('click');
$('#barcode').focus();
return false;
}
// if($('#selling_rate').val() != 0){
// mrp = $('#selling_rate').val();
// }
if(!unit_price)
{
unit_price=0;
}
var quantity=parseFloat($('#quantity').val());
if(!quantity)
{
quantity=0;
}
var free_qty=parseFloat($('#free_qty').val());
if(!free_qty)
{
free_qty=0;
}
var tax=parseFloat($('#tax').val());
if(!tax)
{
tax=0;
}
var incl_tax=(parseFloat(unit_price,10)*(parseFloat(tax,10)/100));
var tax_rate=(parseFloat(incl_tax,10)/(parseFloat(unit_price,10)+parseFloat(incl_tax,10)))*100;
if(!tax_rate)
{
tax_rate=0;
}
var basic_rate=parseFloat(mrp,10)-(parseFloat(mrp,10)*(parseFloat(tax_rate,10)/100));
if(!basic_rate)
{
basic_rate=0;
}
unit_price=basic_rate.toFixed(2);//inclusive
//unit_price=mrp;//execlusive
var net_amount = unit_price*quantity;
$('#net_value').val(net_amount.toFixed(2));
$('#taxable_value').val(net_amount.toFixed(2));
var TaxAmount = parseFloat(net_amount,10)*(parseFloat(tax,10)/100);
$('#tax_amount').val(TaxAmount.toFixed(2));
//var total_amount = parseFloat(net_amount,10)+parseFloat(TaxAmount,10);
var total_amount = mrp*quantity;
$('#single_total').val(total_amount.toFixed(2));
});
$.fn.button_disable=function(){
$.fn.main_calculater();
var length=$('#product_table tbody tr').length;
$('#messagelbl').html('');
if(length>0)
{
$('button[type="Submit"]').attr('disabled',false);
var balance = $('#balance').val()? $('#balance').val():0;
var prevbalance =  $('#prevbalance').val();
var credit_limit = $('#credit_limit').val();
//alert(credit_limit);
var totalBalance = parseFloat(balance)+parseFloat(prevbalance);
var radio_value=$("input[type='radio'][name='data[Sale][type1]']:checked").val();
if(radio_value=="CreditSale")
{
if(parseFloat(totalBalance)>parseFloat(credit_limit))
{
$('#messagelbl').html('Insufficient credit limit');
$('.invoice-action').attr('disabled',true);
}
else
{
$('#messagelbl').html('');
$('.invoice-action').attr('disabled',false);
}
}
else
{
$('#messagelbl').html('');
//$('.balance').hide();
}
}
else
{
$('button[type="Submit"]').attr('disabled',true);
}
};
$(document).on('click','.remove_tr',function(){
$(this).closest('tr').remove();
$.fn.button_disable();
});
$(document).on('click','.remove_old_tr',function(){
// var row_index=$(this).closest('tr').index();
var row_index=$(this).parents('tr').index();
var id=$(this).closest('tr').find('td input.table_id').val();
var url_address= '<?php echo $this->webroot; ?>'+'Sale/SaleItem_delete/'+id;
$.ajax({
type: "POST",
url:url_address,
dataType:'json',
success: function(data) {
if(data.result!='Success')
{
alert(data.result);
return false;
}
$('#product_table tbody tr:eq('+row_index+')').remove();
$.fn.main_calculater();
},
error:function (XMLHttpRequest, textStatus, errorThrown) {
alert(textStatus);
}
});
// $(this).closest('tr').remove();
$.fn.button_disable();
});
$.fn.main_calculater=function(){
var total_quantity=0;
var each_total=0;
var each_taxable_total=0;
var each_tax_amount_total=0;
$('#product_table tbody tr').each(function(){
var single_total=$(this).closest('tr').find('td input.row_total').val();
each_total+=parseFloat(single_total);
var single_taxable_value=$(this).closest('tr').find('td input.row_taxable_value').val();
each_taxable_total+=parseFloat(single_taxable_value);
var single_tax_amount=$(this).closest('tr').find('td input.row_tax_amount').val();
each_tax_amount_total+=parseFloat(single_tax_amount);
var quantity=$(this).closest('tr').find('td input.quantity').val();
var free_qty=$(this).closest('tr').find('td input.free_qty').val();
//var quantity=$('.quantity').val();
var quantity_mode=$(this).closest('tr').find('td input.quantity_mode').val();
// if(quantity_mode=='Cases')
// {
total_quantity=parseFloat(total_quantity)+parseFloat(quantity);
// }
//total_quantity=1;
});
$('#total_quantity').val(Math.round(total_quantity));
var main_total=each_total;
$('#main_total').val(main_total.toFixed(2));
$('#taxable_value_total').val(each_taxable_total.toFixed(2));
$('#tax_amount_total').val(each_tax_amount_total.toFixed(2));
var other_value=0;
// if(!other_value)
// {
//   var other_value=$('#other_value').val('0');
// }
if($('#other_value').val())
{
var other_value=$('#other_value').val();
}
if(!$('#discount_amount').val())
{
$('#discount_amount').val(0);
}
var inclusive_total=parseFloat(each_taxable_total.toFixed(2))+parseFloat(each_tax_amount_total.toFixed(2))
var discount_amount=$('#discount_amount').val();
var taxable_total=parseFloat(main_total)-parseFloat(discount_amount);
//$('#taxable_total').val(taxable_total);
//var tax=5;
var tax=$('#total_tax').val();
var tax_amount_total=parseFloat(taxable_total)*parseFloat(tax)/100;
tax_amount_total=tax_amount_total.toFixed(2);
//$('#tax_amt_total').val(tax_amount_total);
//var total=parseFloat(taxable_total)+parseFloat(tax_amount_total);
var state_id=$('#state_id').val();
	if(state_id==19)
	{
		var tax_amount_split=parseFloat(each_tax_amount_total)/2;
		tax_amount_split=tax_amount_split.toFixed(3);
		$('#cgst_amt_total').val(tax_amount_split);
		$('#sgst_amt_total').val(tax_amount_split);
		$('#igst_amt_total').val(0.000);	
	}
	else
	{
	var tax_amount_split=parseFloat(each_tax_amount_total);
	tax_amount_split=tax_amount_split.toFixed(3);
	$('#cgst_amt_total').val(0.000);
	$('#sgst_amt_total').val(0.000);
	$('#igst_amt_total').val(tax_amount_split);
	}


var total=parseFloat(inclusive_total)-parseFloat(discount_amount);

total=total.toFixed(2);
$('#sale_total').val(total);
var grand_total=parseFloat(total)+parseFloat(other_value);
// var grand_total=roundToTwo(total);
// other_value=parseFloat(grand_total)-parseFloat(total);
grand_total=grand_total.toFixed(2);
$('#grand_total').val(grand_total);
// other_value=other_value.toFixed(3);
//$('#other_value').val(other_value);
var radio_value=$("input[type='radio'][name='data[Sale][type1]']:checked").val();
if(radio_value=="CashSale")
{

$('#paid').val(grand_total);
}
else
{
$('#paid').val(0);
}
var balance=$('#balance').val();
var paid = $('#paid').val();
if(paid=='')
{
$('#paid').val('0');
paid = 0;
}
$('#balance').val(parseFloat(grand_total)-parseFloat(paid));
$.fn.single_tax_calculater(main_total,tax_amount_total,taxable_total);
}
$('#grand_total').keyup(function(){
var grand_total=$('#grand_total').val()? $('#grand_total').val():0;
var total=$('#sale_total').val();
var other_value=parseFloat(grand_total)-parseFloat(total);
other_value=other_value.toFixed(2);
$('#other_value').val(other_value);
$('#paid').val(grand_total);

});
$.fn.single_tax_calculater=function(main_total,tax_amount_total,taxable_total){}
// $.fn.single_tax_calculater=function(main_total,tax_amount_total,taxable_total){
// var each_taxable_total=0;
// var each_tax_amount_total=0;
// $('#product_table tbody tr').each(function(){
// var item_amount=$(this).closest('tr').find('td input.row_total').val();
// var item_amount_per=parseFloat(item_amount)/parseFloat(main_total)*100;
// var item_tax_amount=parseFloat(item_amount_per)*parseFloat(tax_amount_total)/100;
// $(this).closest('tr').find('td input.row_tax_amount').val(item_tax_amount.toFixed(3));
// //var item_taxable_amount=parseFloat(item_amount)-parseFloat(item_tax_amount);
// var item_taxable_amount=parseFloat(item_amount_per)*parseFloat(taxable_total)/100;
// $(this).closest('tr').find('td input.row_taxable_value').val(item_taxable_amount.toFixed(3));
// each_taxable_total+=parseFloat(item_taxable_amount);
// each_tax_amount_total+=parseFloat(item_tax_amount);
// });
// $('#taxable_value_total').val(each_taxable_total.toFixed(3));
// $('#tax_amount_total').val(each_tax_amount_total.toFixed(3));
// }
$.fn.total_tax();
$('#balance').keyup(function(){
var grand_total=$('#grand_total').val();
var balance=$('#balance').val();
$('#paid').val(grand_total-balance);
$.fn.button_disable();
});
$('#paid').keyup(function(){
var grand_total=$('#grand_total').val();
var paid=$('#paid').val();
var balance = parseFloat(grand_total)-parseFloat(paid);
$('#balance').val(balance);
$.fn.button_disable();
});
$('.payment_calculator').keyup(function(){
if($(this).val()=='')
$(this).val('0');
});
$(document).on('change keyup','#discount ,#discount_amount,#other_value',function(){
$.fn.main_calculater();
});
$(document).on('change','#select_discount_type',function(){
var select_discount_type=$(this).val();
$('#discount').keyup();
if(select_discount_type==2)
{
$('#discount').attr('readonly',false);
$('#discount_amount').attr('readonly',true);
}
else
{
$('#discount').val('0');
$('#discount').attr('readonly',true);
$('#discount_amount').attr('readonly',false);
}
});
$('#discount').keyup(function(){
var discount=$('#discount').val();
var main_total=$('#main_total').val();
var discount_amount=(main_total*discount)/100;
$('#discount_amount').val(discount_amount.toFixed(2));
});
$(document).on('change','#customer_type_id',function(){
var customer_type_id=$(this).val();
if(customer_type_id==1)
{
$('.general_customer_hidden_area').hide();
$('#general_customer_hidden_area').show();
$('#address-area').hide();
}
else{
$('.general_customer_hidden_area').show();
$('#general_customer_hidden_area').hide();
$('#address-area').show();
}
$.post( "<?= $this->webroot ?>Customer/get_customer_by_customer_type_ajax/"+customer_type_id ,function( data ) {
var id = $('#SaleAccountHeadId').val();
<?php if(!empty($this->request->data['Sale']['account_head_id'])){?>
var id = <?=$this->request->data['Sale']['account_head_id']?>;

<?php 
}?>
$('#SaleAccountHeadId').html('');
$.each(data.options,function(key,value){
$('#SaleAccountHeadId').append($("<option></option>").attr("value",key).text(value));
if(id==key){
$('#SaleAccountHeadId').val(id).trigger('change');
}
});
if(customer_type_id!=1)
{
//$('#SaleAccountHeadId').select2("open");
}

$('#SaleAccountHeadId').trigger('change');
}, "json");
});

$(document).on('change','#SaleAccountHeadId',function(){
	
//$('.toggle_button_for_sale_type_1')[1].checked = true;
var Customer_id=$(this).val();
//alert(Customer_id);
<?php if(!empty($this->request->data['Sale']['id'])){?>
var type1 = "<?=$this->request->data['Sale']['type1']?>";
if(type1=="CashSale")
{
// alert(type1);
$('.toggle_button_for_sale_type_1')[0].checked = true;
}
else
{
  $('.toggle_button_for_sale_type_1')[1].checked = true;

}
<?php 
}
else{

?>

if(Customer_id==3)
{
//$('.toggle_button_for_sale_type_1')[1].checked = true;
$('#general_customer_hidden_area').show();
$('#address-area').hide();
}
else
{
//$('.toggle_button_for_sale_type_1')[0].checked = true;
$('.general_customer_hidden_area').show();
$('#general_customer_hidden_area').hide();
$('#address-area').show();
}
<?php }?>
$.post( "<?= $this->webroot ?>Customer/get_customer_address_ajax/"+Customer_id ,function( data ) {
$('#address').val(data.address);
$('#division').val(data.division);
$('#prevbalance').val(data.balance);
$('#credit_limit').val(data.credit_limit);
$('.toggle_button_for_sale_type_1').change();
$('#state_id').val(data.state_id);
$.fn.main_calculater();
//$('#barcode').focus();
}, "json");

});

$('#SaleAccountHeadId').trigger('change');
$('#customer_type_id').trigger('change');

var customer_type_id=$('#customer_type_id').val();
if(customer_type_id!=1)
{
}
$('.toggle_button_for_sale_type').change(function(){
$('.sale_type').toggle();
var toggle_button_for_sale_type=$('.toggle_button_for_sale_type').prop('checked');
// if(toggle_button_for_sale_type==true)
// {
//   $('.wholesale_type').css('display','none');
// }
// if(product_configuration_type=='WholeSale' && product_configuration_type!='Retail_And_Wholesale')
// {
//   $('.retail_type').css('display','none');
// }
});

$('#credit_limit_modal_button').click(function(){
var credit_limit=$('#credit_limit').val();
$('#old_limit').val(credit_limit);
$('#credit_limit_modal').modal('show');
});
$('#send_credit_request_button').click(function(){
var new_limit=$('#new_limit').val();
var Customer_id=$('#Customer_id').val();
if(isNaN(new_limit))
{
$('#new_limit').focus();
return false;
}
var data ={
customer_id:Customer_id,
credit_limit:new_limit
};
$.post( "<?= $this->webroot ?>Sale/credit_limit_request_by_ajax",data,function( data ) {
if(data.result!='Success')
{
alert(data.result);
return false;
}
$('#credit_limit_modal').modal('hide');
}, "json");
});
$('button[type="Submit"]').click(function(){
var toggle_button_for_sale_type=$('.toggle_button_for_sale_type').prop("checked");
$.fn.main_calculater();
if(toggle_button_for_sale_type==false)
{
var credit_balance=$('#credit_balance').val();
var balance=$('#balance').val();
if(balance!=0)
{
if(credit_balance<balance)
{
$('#balance').focus();
alert('Your Credit Limit Exceeded');
return false;
}
}
}
});

$('form').submit(function(){
$(this).find('button[type=submit]').hide();
});
$('#check_product_availability_button').click(function(){
var product_id=$('#product_search_id').val();
var shop_id=$('#shop_search_id').val();
var data={
product_id:product_id,
shop_id:shop_id,
};
$.post( "<?= $this->webroot ?>Sale/check_product_availability",data ,function( data ) {
$('#check_product_availability_table tbody').empty();
$.each(data,function(key,value){
$('#check_product_availability_table tbody').append('<tr>\
<td>'+value.Shop+'</td>\
<td>'+value.Product+'</td>\
<td>'+value.quantity+'</td>\
</tr>');
});
}, "json");
});
$('#last_invoice_button').click(function(){
var Customer_id=$('#SaleAccountHeadId').val();
var products = [];
$('#product_table tbody tr').each(function(){
var product_id=$(this).closest('tr').find('td:eq(0) input:eq(0)').val();
products.push(product_id);
});
var data={
customer_id:Customer_id,
products:products,
};
$.post( "<?= $this->webroot ?>Sale/check_product_last_invoice_detials",data,function( data ) {
$('#check_product_last_invoice_detials_table tbody').empty();
if(data.result!='Success')
{
alert(data.result);
return false;
}
$('#product_last_invoice_detial_modal').modal('show');
$.each(data.Product_detial,function(key,value){
$('#check_product_last_invoice_detials_table tbody').append('<tr>\
<td>'+value.name+'</td>\
<td>'+value.invoice+'</td>\
<td>'+value.date+'</td>\
<td>'+value.unit_price+'</td>\
<td><a target="_blank" href="<?= $this->webroot; ?>Sale/Sale/'+value.invoice+'"><button class="btn">View</button></a></td>\
</tr>');
});
}, "json");
});
$('#warehouse_id').change(function(){
var id=$(this).val();
if(!id)
return false;
$('.single_calculator').val('0');
$('#quantity').val('1');
$('#free_qty').val('');
$('#retail_price').val('0');
$('#whole_sale_price').val('0');
$('#product').change();
$.fn.warehouseproducts();
});
$('#warehouse_id').val(1).change();

$('#unit_price').keyup(function(){
var product=$('#product').val();
var unit_price=$('#unit_price').val();
var quantity_mode=$('#quantity_mode').val();
var warehouse_id=$('#warehouse_id').val();
if(product) {
// $.post( "<?= $this->webroot ?>Product/get_Product_ajax_sale/"+product+'/'+quantity_mode+'/'+warehouse_id,function( data ) {

//var cost=data.Product.cost;
var cost=$('#cost').val();
profit=unit_price-cost;
$('#profit').val(profit);
// }, "json");
}
});
$('#executive_id').change(function(){
var id=$(this).val();
// if(id!=0){
//   $.post( "<?= $this->webroot ?>Executive/get_warehouse_by_executive/"+id,function( data ) {
//     $('#warehouse_id').html(data.option);
//     $('#warehouse_id').change();
//     if(data.result=='Success')
//     {
//      // $('#warehouse_id').change();
//     }
//   }, "json");
// }
// else{
//   $('#warehouse_id').html($('<option>', {
//     value: 1,
//     text: 'Main'
//   }));
// }
$.fn.warehouseproducts();
});
$.fn.main_calculater();
$.fn.button_disable();
$('#margin_visibility').change(function(){
$('.margin-field').toggle();
var toggle_button_for_margin=$('#margin_visibility').prop('checked');
if(toggle_button_for_margin==true)
{
$('#profit').keyup();
$('.margin-text').attr('colspan',2);
}
else{
$('.margin-text').attr('colspan',1);
}
});
$(document).on('change','#cost_visibility',function(){
$('.cost-field').toggle();
var toggle_button_for_cost=$('#cost_visibility').prop('checked');
});
$('.toggle_button_for_sale_type_1').change(function(){

var grand_total=$('#grand_total').val();
var radio_value=$("input[type='radio'][name='data[Sale][type1]']:checked").val();
if(radio_value=="CashSale")
{

$('#paid').val(grand_total);
}
else
{
$('#paid').val(0);
}
var balance=$('#balance').val();
var paid = $('#paid').val();
if(paid=='')
{
$('#paid').val('0');
paid = 0;
}
$('#balance').val(parseFloat(grand_total)-parseFloat(paid));
$.fn.button_disable();
});
// $('#save_buttton').click(function(){
// var check2=0;
// //var product_name='';
// $("#product_table tbody tr").each(function () {
// var productId = $(this).closest('tr').find('td input.productsrow').val();
// var product_name = $(this).closest('tr').find('td input.product_name').val();
// var available_quantity_new = $(this).closest('tr').find('td input.stock_quantity').val();
// var no_of_piece_per_unit = $(this).closest('tr').find('td input.no_of_piece_per_unit').val();
// var quantity = $(this).closest('tr').find('td:eq(6) input').val();
// var free_quantity = $(this).closest('tr').find('td:eq(7) input').val();
// var quantity_mode = $(this).closest('tr').find('td input.quantity_mode').val();
// var unit_level = $(this).closest('tr').find('td:eq(2) input:eq(2)').val();
// //var table_product_unit = $(this).closest('tr').find('td:eq(1) input:eq(0)').val();


// if(unit_level==2){var nq1=quantity*no_of_piece_per_unit;
// var nq2=free_quantity*no_of_piece_per_unit;}
// else{var nq1=quantity;
// var nq2=free_quantity;}
// sale_qty=parseFloat(nq1)+parseFloat(nq2);
// if(parseFloat(available_quantity_new)-parseFloat(sale_qty)<0)   
// {
// check2=1;
// alert(product_name+' quantity is insufficient');

// }
// });
// if(check2)
// {

// return false;
// }
// else
// {

// }
// });
$(document).on('keyup','#barcode',function(){
var barcode=$('#barcode').val();
$.post( "<?= $this->webroot ?>Product/get_product_by_barcode/"+barcode ,function( data ) {
if(data.result=='Success')
{
$('#product').val(data.Product.id).change();
}
else
{
$('#product').val('').change();
return false;
}
}, "json");
});
// var sale_type='<?= $sale_type; ?>';
// if(sale_type=='Retail')
// {
//   $('.toggle_button_for_sale_type').click();
// }
var sale_type1='<?= $sale_type1; ?>';
if(sale_type1=='CreditSale')
{

$('.toggle_button_for_sale_type_1')[0].checked = true;
$('.toggle_button_for_sale_type_1').change();
}
$('.toggle_button_for_sale_type').click();
$('.toggle_button_for_sale_type_2').click();
$('.toggle_button_for_sale_type_1').click();
