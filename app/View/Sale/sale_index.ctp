<?php 
function get_status_name($status)
{
	if($status==0)
	{
		$name='Cancelled';
	}
	if($status==1)
	{
		$name='Order Placed';
	}
	if($status==2)
	{
		$name='Order Delivered';	
	}
	if($status==3)
	{
		$name='Altration';	
	}
	return $name;
}
?>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<h2>Sale List</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="box-body">
				<table class="table table-condensed table datatable" id="sale_table" data-order='[[ 1, "asc" ]]' data-page-length='25'>
					<thead>
						<tr class="blue-bg">
							<th>Date</th>
							<th>Customer</th>
							<th>Invoice No</th>
							<th>Total Amt</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($Sale as $key=>$value ) {?>
							<tr class="blue-pddng">
								<td>
									<span><?= date('d-m-Y', strtotime($value['Sale']['date_of_order'])); ?></span>
									<span class='Sale_id' style='display:none;'><?= $value['Sale']['id']; ?></span>
								</td>
								<td><?= $value['AccountHead']['name']; ?></td>
								<td><?= $value['Sale']['invoice_no']; ?></td>
								<td><?= number_format($value['Sale']['grand_total']); ?></td>
								<td><?= get_status_name($value['Sale']['status']) ?></td>
								<td>
									<span><a target='_blank' href='<?=$this->webroot.'Sale/fpdf/'.$value['Sale']['id'];?>'><i class="fa fa-print" aria-hidden="true"></i></i><a/></span> &nbsp;&nbsp;&nbsp;
									<span><a href='<?=$this->webroot.'Sale/Sale/'.$value['Sale']['id'];?>'><i class="fa fa-eye" aria-hidden="true"></i></i><a/></span>
								</td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>