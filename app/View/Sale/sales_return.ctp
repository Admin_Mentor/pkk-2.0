<?php 
  $type=$this->request->data['SalesReturn']['type'];
?>
<section class="content-header">
  <h1>Sales Return
    <div class="pull-right">
      <a href="<?php echo $this->webroot ?>Sale/SalesReturnIndex"><button class='btn btn-success pull-right'>SalesReturn List</button></a>
    </div>
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <?php $status=$this->request->data['SalesReturn']['status']; ?>
        <div class="row-wrapper">
          <div class="row">
            <?= $this->Form->create('SalesReturn', array('url' => array('controller' => 'Sale', 'action' => 'SalesReturn')));?>
            <div class="col-md-12">
              <div class="row">
                <div class="form-horizontal" style="margin-top: 15px;">
                  <div class="box-body">
                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12" style="display: none;">
                      <div class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12"  >
                          <?php echo $this->Form->input('customer_type_id',array('type'=>'select','id'=>'customer_type_id','class'=>'form-control select2','style'=>'width: 100%','empty'=>'ALL','label'=>'Customer Type')); ?>
                        </div>
                      </div>
                    </div>
                   <!--  <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                        <div class="0" id='' style="white-space: nowrap; margin-top: 15px;">
                            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                          <?php $options1 = array(
                            'Refund' => 'Refund &nbsp;&nbsp;&nbsp;&nbsp;',
                            'Replace' => 'Replace &nbsp;&nbsp;&nbsp;&nbsp;'
                          );
                          $attributes1 = array(
                            'legend' => false,
                            'value' => 1,
                            'class'=>'toggle_button_for_sale_return_type_1',
                            'required'=>'required',
                             'value' => $type,
                          );
                          echo $this->Form->radio('payment_type', $options1, $attributes1); ?>
                        </div>
                      </div>
                        </div> -->
                   
                     <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                          <?= $this->Form->input('warehouse_id',array('type'=>'select','empty'=>'Select','class'=>'form-control select_two_class search_class','label'=>'Warehouse','id'=>'warehouse_id','options'=>$warehouse,'required')) ?>       
                        <?= $this->Form->input('warehouse_id_hidden',array('type'=>'hidden','class'=>'form-control ','id'=>'warehouse_id_hidden')) ?>  <?= $this->Form->input('damage_warehouse_id_hidden',array('type'=>'hidden','class'=>'form-control ','id'=>'damage_warehouse_id_hidden')) ?> 
                        </div>
                      </div>
                    </div>
                     <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                          <?php if($status!=2) { ?>
                          <?php echo $this->Form->input('account_head_id',array('type'=>'select','id'=>'Customer_id','class'=>'form-control select2','style'=>'width: 100%','label'=>'Customer','required'=>'required')); ?>
                          <?php }?>
                          <?php if($status==2)  { ?>
                          <?php echo $this->Form->input('account_head_id',array('type'=>'text','id'=>'','class'=>'form-control','label'=>'Customer','required'=>'required','readonly')); ?>
                          <?php } ?>
                          <?= $this->Form->input('state_id',array('type'=>'hidden','id'=>'state_id')); ?>
                          <?= $this->Form->input('customer_type',array('type'=>'hidden','id'=>'customer_type')); ?>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-12 col-md-offset-2">
                      <div class="col-md-12">
                        <div class="col-md-6">
                          <div class="form-group">
                            <div class="col-md-10 col-lg-10 col-sm-10 col-xs-12">
                              <?= $this->Form->input('invoice_no',array('class'=>'form-control','type'=>'text','required','id'=>'invoice_no','label'=>'Credit Note No','readonly')); ?>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6">
                          <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row-wrapper">
                <div class="col-md-12">
                  <div class="box-body table-responsive no-padding">
                    <table class="table boder table-condensed table text-center table-bordered" id="product_table">
                      <thead>
                        <tr class="blue-bg">
                          <th  width='11%' class="padding_left">Invoice No</th>
                          <th  width='20%'>Product</th>
                          <th>Unit</th>
                          <th  >Invoice Price</th>
                          <th  >Act Qty</th>
                          <th  >Qty</th>
                          <th>Type</th>
                          <th  >Amount</th>
                          <!-- tax section head starts -->
                          <th class=''>Tax</th>
                           <th >Taxable Value</th>
                          <th class='settings_tax_amount'  width='10%'> Tax Amount</th>
                          <!-- tax section head ends -->
                          <th  hidden>Total Amount</th>
                          <?php if($status!=2) : ?>
                            <th></th>
                          <?php endif; ?>
                        </tr>
                        <?php if($status!=2) : ?>
                          <tr class="blue-pddng">
                            <td><?= $this->Form->input('invoice_no_list',['type'=>'select','style'=>'width:100%','id'=>'invoice_no_list','class'=>'form-control select2','options'=>$invoice_no_list,'label'=>false]); ?></td>
                            <td><?= $this->Form->input('product',['type'=>'select','style'=>'width:100%','id'=>'product','class'=>'form-control select2','empty'=>[''=>'Select'],'options'=>$Product_list,'label'=>false]); ?></td>
                            <td>
                              <!-- <?= $this->Form->input('unit_id_text',['type'=>'text','id'=>'quantity_mode_text','class'=>'form-control ','label'=>false,'readonly']); ?>-->
                              <?= $this->Form->input('unit_id',['type'=>'hidden','id'=>'quantity_mode','class'=>'form-control ','label'=>false]); ?> 
                              <?=$this->Form->input('unit_id_text',['type'=>'select','id'=>'quantity_mode_text','style'=>'width:100%','class'=>'form-control select2 single_calculator','options'=>$Unit,'label'=>false]); ?></td> 
                              <td>
                                <?= $this->Form->input('single_item_bonus_amount',['type'=>'hidden','id'=>'single_item_bonus_amount','label'=>false]); ?> 
                                <input id='actual_invoice_price' class="form-control single_calculator" type="text" ></td>
                              <td><input id='actual_quantity' autocomplete="off" autocorrect="off" class="form-control single_calculator" type="text" readonly >

                              </td>
                              <td><input id='quantity' class="form-control single_calculator" type="text"></td>
                              <td><?php echo $this->Form->input('type_id',array('type'=>'select','options'=>array('Damage'=>'Damage','Good'=>'Good'),'class'=>'form-control select_two_class','id'=>'type_id','label'=>false,'style'=>array('width:100%'))); ?></td>

                              <td><input id='net_value' class="form-control single_calculator" type="text"></td>
                               <td class=''><?= $this->Form->input('tax',['type'=>'text','value'=>'0','id'=>'tax','class'=>'form-control number single_calculator','label'=>false]); ?></td>
                              <td><input id='taxable_value' class="form-control single_calculator" type="text"></td>
                              <td class='settings_tax_amount'><?= $this->Form->input('tax_amount',['type'=>'text','value'=>'0','id'=>'tax_amount','class'=>'form-control number single_calculator','label'=>false]); ?></td>
                              <td hidden><input id='row_total' class="form-control single_calculator" type="text"></td>
                              <td><i class="fa fa-plus-circle fa-2x ad-mar blue-col" id='add_to_cart'></i></td>
                            </tr>
                          <?php endif; ?>
                        </thead>
                        <tbody>
                          <?php if(isset($SalesReturnItem)) : ?>
                            <?php foreach ($SalesReturnItem as $key => $value): ?>
                              <tr class="blue-pddng">
                                <td colspan="1">
                                  <?php if($value['SalesReturnItem']['invoice_no']==0){
                                    $invoice_no="General Invoice";
                                  }else{
                                    $invoice_no=$value['SalesReturnItem']['invoice_no'];
                                  }?>
                                  <input value='<?= $invoice_no; ?>' class='form-control inv-no' readonly type='text'>
                                  <input value='<?= $value['SalesReturnItem']['id']; ?>' name='data[SalesReturnItem][SalesReturnItem_id][]' type='hidden' class='table_id'>
                                </td>
                                <td colspan="1"><input value='<?= $value['Product']['name']; ?>' class='form-control' readonly type='text'></td>
                                <?php if($value['SalesReturnItem']['quantity_mode']==2)
                                {
                                  $quantity=$value['SalesReturnItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
                                  //
                                  $unit="Cases";
                                  $unit_price=$value['SalesReturnItem']['unit_price']*$value['Product']['no_of_piece_per_unit'];
                                }
                                else
                                {
                                  $quantity=$value['SalesReturnItem']['quantity'];
                                  //
                                  $unit="Pieces";
                                  $unit_price=$value['SalesReturnItem']['unit_price'];
                                }
                                ?> 
                                <td colspan="1"><input value='<?php echo $value['Unit']['name'];?>' class='form-control' readonly type='text'></td>
                                <td><input readonly value='<?= $value['SalesReturnItem']['invoice_price']; ?>' class='form-control'  name='data[SalesReturnItem][invoice_price][]' type='text'></td>
                                <td colspan="2"><input readonly value='<?= $quantity; ?>' class='form-control'  name='data[SalesReturnItem][quantity][]' type='text'></td>
                                 <td><input  readonly value='<?= $value['SalesReturnItem']['type']; ?>' class='form-control '  name='data[SalesReturnItem][type_id][]' type='text'></td>
                                <!-- <td><input readonly value='<?= $unit_price; ?>' class='form-control'  name='data[SalesReturnItem][unit_price][]' type='text'></td> -->
                                <td><input readonly value='<?= $value['SalesReturnItem']['total']; ?>' class='form-control'  name='data[SalesReturnItem][total][]' type='text'></td>
                                <td><input readonly value='<?= $value['SalesReturnItem']['tax']; ?>' class='form-control'  name='data[SalesReturnItem][tax][]' type='text'></td>
                                <td><input readonly value='<?= $value['SalesReturnItem']['net_value']; ?>' class='form-control taxable_value'  name='data[SalesReturnItem][net_value][]' type='text'></td>
                                <td class="settings_tax_amount"><input readonly value='<?= $value['SalesReturnItem']['tax_amount']; ?>' class='form-control tax-amount tax_amount'  name='data[SalesReturnItem][tax_amount][]' type='text'></td>
                                <td hidden><input readonly value='<?= $value['SalesReturnItem']['total']; ?>' class='form-control row_total'  name='data[SalesReturnItem][row_total][]' type='text'></td>
                                <?php if($status!=2) : ?>
                                  <td><i class="fa fa-minus-circle fa-2x remove_old_tr" ></i></td>
                                <?php endif; ?>
                              </tr>
                            <?php endforeach ?>
                          <?php endif; ?>
                        </tbody>
                        <tfoot>
                           <tr>
                            <td colspan='9'></td>
                            <td><label class="col-md-5 col-lg-5 col-sm-5 col-xs-12 control-label" style="white-space: nowrap;">SGST</label></td>
                            <td>
                            <?= $this->Form->input('sgst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'sgst_amt_total','readonly','label'=>false,)); ?>
                            </td>
                            <?php if($status!=2) : ?>
                              <td></td>
                            <?php endif; ?>
                          </tr>
                           <tr>
                            <td colspan='9'></td>
                            <td><label class="col-md-5 col-lg-5 col-sm-5 col-xs-12 control-label" style="white-space: nowrap;">CGST</label></td>
                            <td colspan='1'><?= $this->Form->input('cgst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'cgst_amt_total','readonly','label'=>false,)); ?></td>
                            <?php if($status!=2) : ?>
                              <td></td>
                            <?php endif; ?>
                          </tr>
                           <tr>
                            <td colspan='9'></td>
                            <td><label class="col-md-5 col-lg-5 col-sm-5 col-xs-12 control-label" style="white-space: nowrap;">IGST</label></td>
                            <td colspan='1'><?= $this->Form->input('igst_amt_total',array('class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'igst_amt_total','readonly','label'=>false,)); ?></td>
                            <?php if($status!=2) : ?>
                              <td></td>
                            <?php endif; ?>
                          </tr>
                          <tr>
                            <td colspan='8'></td>
                            <td class="settings_tax_amount"></td>
                            <td><label class="col-md-5 col-lg-5 col-sm-5 col-xs-12 control-label" style="white-space: nowrap;">Grand Total</label></td>
                            <td colspan='1'><?= $this->Form->input('grand_total',array('class'=>'form-control','type'=>'number','step'=>'any','required','readonly','id'=>'grand_total','label'=>false,)); ?></td>
                            <?php if($status!=2) : ?>
                              <td></td>
                            <?php endif; ?>
                          </tr>
                          <tr style="display: none">
                            <td colspan='8'></td>
                            <td class="settings_tax_amount"></td>
                            <td><label class="col-md-5 col-lg-5 col-sm-5 col-xs-12 control-label" style="white-space: nowrap;">Refund</label></td>
                            <td colspan='1'><?= $this->Form->input('refund',array('class'=>'form-control','type'=>'text','id'=>'refund','label'=>false)); ?></td>
                            <?php if($status!=2) : ?>
                              <td></td>
                            <?php endif; ?>
                          </tr>
                          <tr style="display: none">
                            <td colspan='8'></td>
                            <td class="settings_tax_amount"></td>
                            <td><label class="col-md-5 col-lg-5 col-sm-5 col-xs-12 control-label" style="white-space: nowrap;">Discount</label></td>
                            <td colspan='1'><?= $this->Form->input('discount',array('class'=>'form-control','type'=>'number','id'=>'discount','label'=>false)); ?></td>
                            <?php if($status!=2) : ?>
                              <td></td>
                            <?php endif; ?>
                          </tr>
                          <tr style="display: none;">
                            <td colspan='8'></td>
                            <td class="settings_tax_amount"></td>
                            <td><label class="col-md-5 col-lg-5 col-sm-5 col-xs-12 control-label" style="white-space: nowrap;">Net Amount</label></td>
                            <td colspan='1'><?= $this->Form->input('net_amount',array('readonly','class'=>'form-control main_calculator','type'=>'number','step'=>'any','required','id'=>'net_amount','label'=>false,)); ?></td>
                            <?php if($status!=2) : ?>
                              <td></td>
                            <?php endif; ?>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="tbl_ftr_lst">
                    <div class="col-md-6 col-lg-6 col-xs-12 col-sm-6 col-md-offset-6">
                      <div class="form-group">
                        <?php if(isset($SalesReturnItem)) : ?>
                          <?php $flag=$this->request->data['SalesReturn']['flag']; ?>
                          <?php if($status!=2 && $flag!=0) : ?>
                            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3" hidden>
                              <button type='submit' name='data[SalesReturn][process]' value='delete' class="btn btn-danger">Delete</button> 
                            </div>
                            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
                              <button type='submit' name='data[SalesReturn][process]' value='cancel' class="btn btn-warning">Cancel</button> 
                            </div>
                            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
                              <button type='submit' name='data[SalesReturn][process]' value='update' class="btn btn-success">Update</button>
                            </div>
                            <div class="col-md-3 col-lg-3 col-xs-12 col-sm-3">
                              <button type='submit' name='data[SalesReturn][process]' value='delivery' class="btn btn-primary">Order Delivery</button>
                            </div>
                          <?php else : ?>
                            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-3 col-md-offset-8">
                              <a class='print_button' target='_blank' href='<?=$this->webroot.'Print/sales_return_print/'.$this->request->data['SalesReturn']['id'];?>'><i  class="btn btn-success ">Print</i></a>
<!--                               <button type='submit' name='data[SalesReturn][process]' value='delete' class="btn btn-danger">Delete</button> 
 -->                            </div>
                          <?php endif; ?>
                        <?php else : ?>
                          <div class="col-md-12 col-lg-12 col-xs-12 col-sm-3">
                            <br>
                            <button type='submit' style="display: none;" name='data[SalesReturn][process]' value='save' class="btn btn-success">Save</button>
                            <button type='submit' name='data[SalesReturn][process]' value='delivery' class="btn btn-primary">Order Delivery</button>
                            <br>
                            <br>
                          <?php endif; ?>
                        </div>
                      </div>
                      <br/>
                    </div>
                  </div>
                  <br/>
                </div>
              </div>
              <?= $this->Form->end(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <script type="text/javascript">
    <?php require('sales_return.js'); ?>
    $("#Customer_id").change();
  </script>

  <script type="text/javascript">
  $(document).ready(function () {
 $("#Customer_id").select2({
    placeholder: "Search Here",
    width: '100%',
    ajax: {
      url: '<?= $this->webroot ?>ProductionPlan/Searchcustomer',
      dataType: 'json',
      delay: 250,
      data: function (params) {
            return {
                      q: params.term, // search term
                      page: params.page
                   };
          },
          processResults: function (data, params) {
            params.page = params.page || 1;
            return {
              results: data.items,
              pagination: {
                more: (params.page * 30) < data.total_count
              }
            };
          },
          cache: false
        },
      minimumInputLength: 1,
      templateResult: formatRepo,
      templateSelection: formatRepoSelection
    });

  function formatRepo (repo) 
  {
    if (repo.loading) return repo.text;
    return repo.text;
  }
  function formatRepoSelection (repo)
  {
    return repo.text || repo.text;
  }
  });
</script>