$('#SalesReturnSalesReturnForm').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});
$(document).on('change','#customer_type_id',function(){
  var customer_type_id=$(this).val();
  $.post( "<?= $this->webroot ?>Customer/get_customer_by_customer_type_ajax/"+customer_type_id ,function( data ) {
    $('#Customer_id').html('');
    $.each(data.options,function(key,value){
      $('#Customer_id').append($("<option></option>").attr("value",key).text(value));
    });
    $('#Customer_id').trigger('change');
  }, "json");
});
$(document).on('change','#Customer_id',function(){
  var Customer_id=$(this).val();
  $.post( "<?= $this->webroot ?>Customer/get_customer_address_ajax/"+Customer_id ,function( data ) {
    $('#address').val(data.address);
    $('#state_id').val(data.state_id);
    $('#customer_type').val(data.customertype);
    $('.single_calculator').keyup();
  }, "json");
  $.post( "<?= $this->webroot ?>Sale/get_invoice_list_ajax/"+Customer_id ,function( data ) {
    $('#invoice_no_list').html('');
    $.each(data.options,function(key,value){
      $('#invoice_no_list').append($("<option></option>").attr("value",key).text(value));
    });
    $('#invoice_no_list').trigger('change');
    $('.single_calculator').keyup();
      $.fn.main_calculater();

  }, "json");
});
$(document).on('change','#invoice_no_list',function(){
  var invoice_no=$(this).val();
  $.post( "<?= $this->webroot ?>Sale/get_product_list_ajax/"+invoice_no ,function( data ) {
    $('#product').html('');
    $.each(data.options,function(key,value){
      $('#product').append($("<option></option>").attr("value",key).text(value));
    });
    $('#product').val($("#product option:eq(1)").val()).trigger('change');
  }, "json");
});
$(document).on('change','#product',function(){
 $.fn.gst_state_check();
 var product_id=$(this).val();
 var sale_id=$('#invoice_no_list option:selected').val();
 var warehouse_id=$('#warehouse_id').val();
 if(!warehouse_id)
 {
  if(product_id){
  $('#warehouse_id').select2('open');
        $("#invoice_no_list").change();

  return false;
}
}
 var data=
 {
  product_id:product_id,
  sale_id:sale_id,
}
$.post( "<?= $this->webroot ?>Sale/get_product_sale_item_detials_ajax/",data ,function( data ) {
  var no_of_pieces_per_unit=data.Product.no_of_piece_per_unit;
  var quantity =0;
      if(data.Product.level != 1){
        var quantity=data.Product.quantity/no_of_pieces_per_unit;
        var quantity_mode_text ='Cases';
      }else{
        var quantity=data.Product.quantity;
        var quantity_mode_text ='Pieces';
      }
  $('#actual_invoice_price').val(data.Product.mrp);
  $('#unit_price').val(data.Product.mrp);
  $('#quantity_mode').val(data.Product.level);
  //$('#quantity_mode_text').val(quantity_mode_text);
   $('#quantity_mode_text').val(data.Product.level).change();
  // $('#actual_quantity').val(data.Product.quantity);
  $('#actual_quantity').val(quantity);
  $('#quantity').val(quantity);
  var net_value = parseFloat((data.Product.quantity),10)*parseFloat(data.Product.mrp);
  $('#net_value').val(parseFloat(net_value).toFixed(2));
  var tax=parseFloat((data.Product.tax),10);
  var tax_amount = tax * parseFloat((data.Product.quantity),10);
  $('#tax').val(tax);
  $('#tax_amount').val(parseFloat(tax_amount).toFixed(2));
  $('.single_calculator').keyup();
}, "json");
});
$(document).on('change','#quantity_mode_text',function(){
 $.fn.gst_state_check();
 var unit_id_text=$(this).val();
 var sale_id=$('#invoice_no_list option:selected').val();
 var product_id=$('#product option:selected').val();
 var data=
 {
  product_id:product_id,
  sale_id:sale_id,
}
$.post( "<?= $this->webroot ?>Sale/get_product_sale_item_detials_ajax_unit_change/",data ,function( data ) {
  var no_of_pieces_per_unit=data.Product.no_of_piece_per_unit;
  var quantity =0;
      if(data.Product.level != 1){
        var quantity=data.Product.quantity/no_of_pieces_per_unit;
        var quantity_mode_text ='Cases';
      }else{
        var quantity=data.Product.quantity;
        var quantity_mode_text ='Pieces';
      }
     
      if(unit_id_text==2)
      {
        var new_invoice_price=data.Product.mrp*no_of_pieces_per_unit;
        var new_quantity=data.Product.quantity/no_of_pieces_per_unit;
      }
      else
      {
        var new_invoice_price=data.Product.mrp;
        var new_quantity=parseFloat(data.Product.quantity);
      }
  $('#single_item_bonus_amount').val(data.Product.single_item_bonus_amount);
  $('#actual_invoice_price').val(new_invoice_price);
  $('#unit_price').val(new_invoice_price);
  $('#quantity_mode').val(unit_id_text);
  //$('#quantity_mode_text').val(quantity_mode_text);
  // $('#quantity_mode_text').val(data.Product.level).change();
  // $('#actual_quantity').val(data.Product.quantity);
  $('#actual_quantity').val(new_quantity);
  $('#quantity').val(new_quantity);
  var net_value = parseFloat((new_quantity),10)*parseFloat(new_invoice_price);
  $('#net_value').val(parseFloat(net_value).toFixed(2));
  var tax=parseFloat((data.Product.tax),10);
  var tax_amount = tax * parseFloat((new_quantity),10);
  $('#tax').val(tax);
  $('#tax_amount').val(parseFloat(tax_amount).toFixed(2));
  $('.single_calculator').keyup();
}, "json");
});
$("#add_to_cart").click(function(event){
  event.preventDefault();
  var product_text=$('#product option:selected').text();
  product_text = product_text.replace(/"/g, '');
  var product=$('#product').val();
  var invoice_no_list_text=$('#invoice_no_list option:selected').text();
  var invoice_no_list=$('#invoice_no_list').val();
  var quantity=$('#quantity').val();
  if(isNaN(quantity))
  {
   $('#quantity').focus();
   return false;
 }
 if(!quantity)
 {
  $('#quantity').focus();
  return false;
}
if(invoice_no_list)
{
  actual_quantity=$('#actual_quantity').val();
}
else
{
  actual_quantity=quantity+1;
}
var quantity=parseFloat(quantity,10);
if(invoice_no_list !=0){
  if(quantity<=0 || parseFloat(actual_quantity,10)<quantity)
  {
    $('#quantity').focus();
    return false;
  }
}

if(!product){
  $('#product').select2('open');
  return false;
}
var actual_invoice_price=$('#actual_invoice_price').val();
var single_item_bonus_amount=$('#single_item_bonus_amount').val()? $('#single_item_bonus_amount').val():0;
var unit_price=$('#unit_price').val();
// var quantity_mode=$('#quantity_mode option:selected').val();
// var quantity_mode_text=$('#quantity_mode option:selected').text();
var quantity_mode=$('#quantity_mode').val();
if(quantity_mode==2)
{
var quantity_mode_text="Cases";

}
else
{
 var quantity_mode_text="Pieces";
 
}
var type_id=$('#type_id').val();
if(type_id=="Damage")
{
 var damage_warehouse_id_hidden=$('#damage_warehouse_id_hidden').val();
 if(!damage_warehouse_id_hidden)
 {
  alert('Damage warehouse not created');
    return false;

 }
}
var net_value=$('#net_value').val();
var tax=$('#tax').val();
var tax_amount=$('#tax_amount').val();
var row_total=$('#row_total').val();
var taxable_value=$('#taxable_value').val();
var checkexist = 0;
$("#product_table tbody tr.tocheckproductid_return_new").each(function () {
                        var pr_id_load = $(this).closest('tr').find('.togetproductid_return_new').val();// row 2
                        if (product_text== pr_id_load) {
                          alert("This product already in return cart first remove that");
                          checkexist = 1;
                        }
                         var invoice = $(this).closest('tr').find('.inv-no').val();// row 2
                        if (invoice_no_list_text!= invoice) {
                          alert("Use Same invoice NO");
                          checkexist = 1;
                        }
                      });
  //if(quantity<=actual_quantity)
  if(checkexist==0)
  {
   $('#product_table tbody').append('<tr class="blue-pd tocheckproductid_return_new">\
    <td colspan="1">\
    <input hidden name="data[SalesReturn][invoice_no_list][]" value="'+invoice_no_list+'">\
    <input class="form-control inv-no" value="'+invoice_no_list_text+'" readonly>\
    </td>\
    <td colspan="1">\
    <input hidden name="data[SalesReturn][product_id][]" value="'+product+'">\
    <input hidden name="data[SalesReturn][sale_unit_level][]" value="'+quantity_mode+'">\
    <input class="form-control togetproductid_return_new"  value="'+product_text+'" readonly>\
    </td>\
    <td colspan="1">\
    <input hidden name="data[SalesReturn][unit_id][]" value="'+quantity_mode+'">\
    <input class="form-control" readonly  name="data[SalesReturn][quantity_mode][]" value="'+quantity_mode_text+'">\
    </td>\
    <td>\
    <input hidden name="data[SalesReturn][single_item_bonus_amount][]" value="'+single_item_bonus_amount+'">\
    <input class="form-control" name="data[SalesReturn][invoice_price][]" value='+actual_invoice_price+' readonly >\
    </td>\
    <td colspan="2"><input class="form-control" name="data[SalesReturn][quantity][]" value='+quantity+' readonly ></td>\
    <td><input class="form-control" name="data[SalesReturn][type_id][]" value='+type_id+' readonly></td>\
    <td><input class="form-control" name="data[SalesReturn][total][]" value='+net_value+' readonly></td>\
    <td class=" "><input class="form-control " name="data[SalesReturn][tax][]" value='+tax+' readonly></td>\
    <td><input class="form-control taxable_value" name="data[SalesReturn][net_value][]" value='+taxable_value+' readonly></td>\
    <td class=" "><input class="form-control tax_amount" name="data[SalesReturn][tax_amount][]" value='+tax_amount+' readonly></td>\
    <td hidden><input class="form-control row_total" name="data[SalesReturn][row_total][]" value='+row_total+' id="total" readonly></td>\
    <td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
    </tr>');
     //actual_quantity=actual_quantity-quantity;
     //alert(actual_quantity);
   }
   var toggle_button_for_gst_visibility=$('#toggle_button_for_gst_visibility').prop("checked");
   if(toggle_button_for_gst_visibility==false)
   {
    $('.tax_field').css('display','none');
  }
  $.fn.Country_type();
  $('#type_id').val('Good').change();
  $('#product').val('').change();
  $.fn.main_calculater();
  $.fn.button_disable();
  $.fn.gst_state_check();
});
$(document).on('click','.remove_tr',function(){
  $(this).closest('tr').remove();
  $.fn.button_disable();
});
$(document).on('click','.remove_old_tr',function(){
  var row_index=$(this).closest('tr').index();
  var id=$(this).closest('tr').find('td input.table_id').val();
  var url_address= '<?php echo $this->webroot; ?>'+'Sale/SalesReturnItem_delete/'+id;
  $.ajax({
    type: "POST",
    url:url_address,
    dataType:'json',
    success: function(data) {
      if(data.result!='Success')
      {
        alert(data.result);
        return false;
      }
      $('#product_table tbody tr:eq('+row_index+')').remove();
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
  $(this).closest('tr').remove();
  $.fn.button_disable();
});
$(document).on('change','#warehouse_id',function(){
  var warehouse_id=$(this).val();
  $.post( "<?= $this->webroot ?>Warehouse/get_damage_warehouse/"+warehouse_id ,function( data ) {
    $('#damage_warehouse_id_hidden').val(data.Warehouse);
  }, "json");
});
$.fn.button_disable=function(){
  $.fn.main_calculater();
  var length=$('#product_table tbody tr').length;
 var length=$('#product_table tbody tr').length;
  if(length>0)
  {
    $('button[type="Submit"]').attr('disabled',false);  
     $('#warehouse_id').prop('disabled',true);
    $('#warehouse_id_hidden').val($('#warehouse_id').val());
     
  }
  else
  {
    $('button[type="Submit"]').attr('disabled',true);
     $('#warehouse_id').prop('disabled',false);
  }
};
$.fn.main_calculater=function(){
  var each_total=0; var each_taxable_value=0; var each_tax_amount=0;
  $('#product_table tbody tr').each(function(){
    var row_total=$(this).closest('tr').find('td input.row_total').val();
        var tax_amount=$(this).closest('tr').find('td input.tax_amount').val();
        var taxable_value=$(this).closest('tr').find('td input.taxable_value').val();

    each_total+=parseFloat(row_total);
    each_taxable_value+=parseFloat(taxable_value);
    each_tax_amount+=parseFloat(tax_amount);

  });
  var grand_total=parseFloat(each_taxable_value)+parseFloat(each_tax_amount);
  grand_total=roundToTwo(grand_total);
var state_id=$('#state_id').val();
  if(state_id==19)
  {
    var tax_amount_split=parseFloat(each_tax_amount)/2;
    tax_amount_split=tax_amount_split.toFixed(3);
    $('#cgst_amt_total').val(tax_amount_split);
    $('#sgst_amt_total').val(tax_amount_split);
    $('#igst_amt_total').val(0.000);  
  }
  else
  {
  var tax_amount_split=parseFloat(each_tax_amount);
  tax_amount_split=tax_amount_split.toFixed(3);
  $('#cgst_amt_total').val(0.000);
  $('#sgst_amt_total').val(0.000);
  $('#igst_amt_total').val(tax_amount_split);
  }
  $('#grand_total').val(grand_total);
  var discount=$('#discount').val();
  if(!discount)
  {
    var discount=$('#discount').val('0');
  }
  var net_amount=parseFloat(grand_total)-parseFloat(discount);
  net_amount=roundToTwo(net_amount);
  $('#net_amount').val(net_amount);
}
$('#discount').blur(function(){
  if(!$(this).val())
  {
    $(this).val('0').keyup();
  }
  $(this).keyup();
});
$(document).on('change keyup','#discount',function(){
  $.fn.main_calculater();
});
$.fn.button_disable();
$('.single_calculator').keyup(function(e){
  //var unit_price=parseFloat($('#unit_price').val());
    var unit_price=parseFloat($('#actual_invoice_price').val());
  if(!unit_price)
  {
    unit_price=0;
  }
  var quantity=parseFloat($('#quantity').val());
  if(!quantity)
  {
    quantity=0;
  }
  var net_value=unit_price*quantity;
  net_value=roundToTwo(net_value);
  $('#net_value').val(net_value);
  var tax=parseFloat($('#tax').val());
  var taxable_value=parseFloat((net_value*100)/(tax+100));
  var tax_amount=taxable_value*tax/100;
  tax_amount=roundToTwo(tax_amount);
  $('#taxable_value').val(taxable_value.toFixed(2));
  $('#tax_amount').val(tax_amount.toFixed(2));
  row_total=parseFloat(taxable_value.toFixed(2))+parseFloat(tax_amount.toFixed(2));
  row_total=roundToTwo(row_total);
  $('#row_total').val(row_total);
  if (e.keyCode == 13) 
{
  $('#add_to_cart').trigger('click');
  $('#product').focus();
  return false;
}
});
$('.main-sidebar').ready(function(){
  $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
});
function roundToTwo(num) {
  return +(Math.round(num + "e+3")  + "e-3");
}
$.fn.gst_state_check=function(){
  var state=$('#state_id').val();
  var customertype=$('#customer_type').val();
  var toggle_button_for_gst_visibility=$('#toggle_button_for_gst_visibility').prop("checked");
  if(toggle_button_for_gst_visibility==false)
  {
    $('.tax_field').css('display','none');
    $('.interstate').css('display','none');
    $('.instate').css('display','none');
  }
  else
  {
    if(state!=19 && customertype!=1)
    {
      $('.interstate').show();
      $('.instate').hide();
    }
    else
    {
      $('.instate').show();
      $('.interstate').hide();
    }
  }
};
$('#toggle_button_for_gst_visibility').change(function(){
  $.fn.gst_state_check();
});