<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js" type="text/javascript" language="javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js" type="text/javascript" language="javascript"></script>
<section class="content-header">
	<h1> Sales Return List
		<div class="pull-right">
			<a href="<?php echo $this->webroot ?>Sale/SalesReturn"><button class='btn btn-success pull-right'>New SalesReturn</button></a>
		</div>
	</h2>
</section>
<section class="content">
	<div class="box box-primary">
		<div class="box-header">
			<div class="col-md-12" style="margin-left:-3%">
				<div class="col-md-3 col-lg-2 col-sm-12 col-xs-12">
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<?php echo $this->Form->input('from_date',array('type'=>'text','class'=>'form-control date_picker datepicker','id'=>'from_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'From Date')); ?>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-lg-2 col-sm-12 col-xs-12">
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<?php echo $this->Form->input('to_date',array('type'=>'text','class'=>'form-control date_picker datepicker','id'=>'to_date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask','label'=>'To Date')); ?>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
					<div class="form-group">
						<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
							<?= $this->Form->input('executive_id',array('type'=>'select','id'=>'executive_id','empty'=>array(0 => 'No Executive'),'class'=>'form-control select_two_class','style'=>'width: 100%')); ?>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-lg-2 col-sm-12 col-xs-12">
					<div class="form-group"><br>
						<button class='btn btn-success' id='get_button'>Get</button>
					</div>
				</div>
			</div>
		</div>
		<div class="box-body">
			<table class="boder table table-condensed" id="table_data" data-order='[[ 0, "desc" ]]' data-page-length='25' style="width: 100%">
				<thead>
					<tr class="blue-bg">
						<th>Date</th>
						<th>Executive</th>
						<th>Customer</th>
						<th>Credit Note No</th>
						<th style="text-align: left">Total Amt</th>
						<th>Status</th>
						<th data-orderable="false">Action</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="4" style="font-size:20px; color:red;text-align:right">Total:</th>
						<th style="font-size:20px; color:red;text-align:right"></th>
						<th></th>
						<th></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</section>
<script type="text/javascript">
	$('#table_data').DataTable( {
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url": "<?= $this->webroot ?>Sale/SalesReturnIndex_ajax",
			"type": "POST",data:function( d ) {
				d.from_date= $('#from_date').val();
				d.to_date= $('#to_date').val();
				d.executive_id= $('#executive_id').val();
			},
			"dataSrc": "records",
		},
		dom: 'Bfrtip',
		buttons: [
		{ extend: 'colvis', },
		{ extend: 'csv',   footer: false, exportOptions: { columns: ':visible' } },
		{ extend: 'excel', footer: false, exportOptions: { columns: ':visible' } },
		{ extend: 'pageLength', },
		],
		"columns": [
		{ "data" : "SalesReturn.date" },
		{ "data" : "Executive.name" },
		{ "data" : "AccountHead.name" },
		{ "data" : "SalesReturn.invoice_no" },
		{ "data" : "SalesReturn.grand_total",'class':"text-right" },
		{ "data" : "SalesReturn.status" },
		{ "data" : "SalesReturn.action" },
		],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};
			pageTotal = api.column( 4, { page: 'current'} ).data().reduce( function (a, b) {
				return intVal(a) + intVal(b);
			}, 0 );
			$( api.column( 4 ).footer() ).html(''+Math.round(pageTotal).toFixed(2)+'');
		},
		"columnDefs": [
		// { className: "Product", "targets": [ 0 ],"width": "10%", },
		// { className: "Product", "targets": [ 1 ],"width": "20%", },
		 { className: "Product", "targets": [ 2 ],"width": "40%", },
		// { className: "Product", "targets": [ 3 ],"width": "10%", },
		// { className: "Product", "targets": [ 4 ],"width": "10%", },
		// { className: "Product", "targets": [ 5 ],"width": "10%", },
		],
	});
	$('#get_button').click(function(){
		table = $('#table_data').dataTable();
		table.fnDraw();
	});
</script>