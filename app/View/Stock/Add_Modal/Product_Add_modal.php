<div class="modal fade" id="Product_Add_modal" tabindex="" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add Product</h4>
      </div>
      <div class="modal-body">
        <div class="form-horizontal">
          <?php echo $this->Form->create('ProductAdd',array('id'=>'Product_Add_Form')); ?>
          <div class="form-group">
            <div class="col-sm-9">
              <?php echo $this->Form->input('name',array('class'=>'form-control product_disable toUpperCase','label'=>'Product Name In Full *','placeholder'=>' Enter Product Name')) ?>
            </div>
             </div>
             <div class="form-group">
            <div class="col-sm-9">
             <?php echo $this->Form->input('arabic_name',array('class'=>'form-control ','label'=>'Product Name Arabic','placeholder'=>' Enter Product Name in Arabic')) ?>
            </div>
             </div>
            <div class="col-sm-3" style="display: none;">
              <?php echo $this->Form->input('code',array('type'=>'text','disabled' => 'disabled','class'=>'form-control product_disable','value'=>'Auto-Gen','label'=>'Product Code')) ?>
            </div>
          </div>
         <!--  <div class="form-group" style="display: none;">
           <div class="col-sm-9">
            <?php echo $this->Form->input('arabic_name',array('class'=>'form-control ','label'=>'Product Name Arabic','placeholder'=>' Enter Product Name in Arabic')) ?>
          </div>
        </div> -->
        <div class="form-group">
          <label class="col-sm-3 control-label">Product Type</label>
          <div class="col-sm-6">
            <?php echo $this->Form->input('product_type_id_modal',array('type'=>'select','options'=>array(''=>'SELECT',$Product_type),'class'=>'form-control select2','style'=>array('width:100%'),'label'=>false,'id'=>'product_type_id_modal')); ?>
          </div>

          <div class="col-sm-3">
            <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" data-toggle="modal"  data-target="#Product_Type_Add_Modal"></i></a>
          </div>
          </div><br><br>
        <div class="form-group">
          <label class="col-sm-3 control-label">Brand </label>
          <div class="col-sm-6">
            <?php echo $this->Form->input('Brand_id_modal',array('type'=>'select','options'=>array('empty'=>'SELECT',$Brand),'class'=>'form-control select2 brand_class','style'=>array('width:100%'),'label'=>false,'id'=>'Brand_id_modal')); ?>
          </div>
          <div class="col-sm-3">
            <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" data-toggle="modal"  data-target="#Brand_Add_Modal"></i></a>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-4" style="display:none;">
            <?php echo $this->Form->input('stock_date',array('type'=>'text','class'=>'form-control datepicker','label'=>'Date','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask')); ?>
          </div>
          <div class="col-sm-4" style="display: none;">
            <?php echo $this->Form->input('hsn_code',array('type'=>'text','class'=>'form-control','label'=>'HSN Code')) ?>
          </div>
        </div>
        <!--  -->
        <span id="StockAdvanceBtn" style="cursor: pointer;color: #3c8dbc;"><i>Advanced...</i></span>
        <div id="StockAdvance" hidden>
          <div class="form-group ">
            <div class="col-sm-4">
            <?php echo $this->Form->input('tax',array('type'=>'number','value'=>'5','class'=>'form-control','label'=>'Tax %')); ?>
            </div>
            <label class="col-sm-3 control-label">Main Supplier</label>
            <div class="col-sm-6">
              <?php echo $this->Form->input('party_id',array('type'=>'select','options'=>array(''=>'SELECT',$PartyList),'class'=>'form-control select2 ','style'=>array('width:100%'),'label'=>false,'id'=>'party_id')); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-4">
              <?php echo $this->Form->input('unit_id',array('type'=>'select','options'=>$Unit,'selected'=>1,'style'=>array('width:100%'),'class'=>'form-control select2 unit_id','label'=>'Unit')) ?>
              <?php echo $this->Form->input('unit_level',array('type'=>'hidden')) ?>
            </div>
            <div class="col-sm-3 no_of_piece_per_unit_feild" style="display:none">
              <?php echo $this->Form->input('no_of_piece_per_unit',array('type'=>'number','min'=>'0','class'=>'form-control product_disable no_of_piece_per_unit number_field piece_stock_calc piece_price_calc','value'=>1,'label'=>'No Of Pieces','readonly')) ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('threshold',array('type'=>'text','class'=>'form-control product_disable number_field','label'=>'Threshold *','min'=>0,'value'=>'0')) ?>
            </div>
            <div class="col-sm-4">
             <?php echo $this->Form->input('quantity',array('type'=>'number','class'=>'form-control','id'=>'stock_quantity','label'=>'Total Stock (Pieces)','value'=>'0')) ?>
           </div>
         </div>
         <div class="form-group price_of_unit_per_unit_feild" style="display:none">
          <div class="col-sm-4 unit_retaile_price_field">
            <?php echo $this->Form->input('unit_mrp',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Retail Price/Unit')) ?>
          </div>
          <div class="col-sm-4 unit_whole_sales_price_field">
            <?php echo $this->Form->input('unit_wholesale_price',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Wholesale Price/Unit')) ?>
          </div>
          <div class="col-sm-4">
            <?php echo $this->Form->input('unit_cost',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Landing Cost/Unit')) ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-4" id='retaile_price_field'>
            <?php echo $this->Form->input('mrp',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'MRP/Retail Price/pieces')) ?>
          </div>
          <div class="col-sm-4" id='whole_sales_price_field'>
            <?php echo $this->Form->input('wholesale_price',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Wholesale Price/pieces')) ?>
          </div>
          <div class="col-sm-4">
            <?php echo $this->Form->input('cost',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Landing Cost/pieces')) ?>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-4 box_count_cls" style="display: none;">
            <?php echo $this->Form->input('box_count',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_stock_calc','value'=>0,'label'=>'Case(Stock)')) ?>
          </div>
          <div class="col-sm-4 piece_count_cls" style="display: none;">
            <?php echo $this->Form->input('piece_count',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_stock_calc','value'=>0,'label'=>'Pieces (Stock)')) ?>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" id='Product_Add_Btn'>Add To Stock</button>
    </div>
    <?php echo $this->Form->end(); ?>
  </div>
</div>
</div>
</div>
<script type="text/javascript">
  $(document).on('click','#StockAdvanceBtn',function(){
    if($('#StockAdvance').is(":visible")){
      $('#StockAdvance').hide();
    }else{
      $('#StockAdvance').show();
    } 
  });
</script>