 <div id="Product_Type_Add_Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add Product Type</h4>
      </div>
      <?php echo $this->Form->create('ProductTypeAdd',array('id'=>'ProductType_Add_Form')); ?>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <div class="col-sm-10">
              <?php echo $this->Form->input('name',array('class'=>'form-control toUpperCase','label'=>'Name')); ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-10">
              <!-- <?php echo $this->Form->input('code',array('type'=>'text','class'=>'form-control','label'=>'Code')); ?> -->
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id='add_Product_Type'>Add</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>