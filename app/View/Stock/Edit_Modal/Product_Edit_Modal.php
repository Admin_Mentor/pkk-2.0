<div class="modal fade" id="Product_Edit_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit Product</h4>
      </div>
      <div class="modal-body">



        <div class="row">
          <?php echo $this->Form->create('ProductEdit',array('id'=>'Product_Edit_Form')); ?>
          <div class="col-md-8 col-lg-8 col-sm-8">
           <div class="form-group">
            <?php echo $this->Form->input('name',array('class'=>'form-control product_disable toUpperCase','label'=>'Product Name In Full *','placeholder'=>' Enter Product Name')) ?>
            <?php echo $this->Form->input('id',array('type'=>'hidden')) ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-8">
          <div class="form-group">
            <?php echo $this->Form->input('arabic_name',array('class'=>'form-control ','dir'=>'rtl','label'=>'Product Name Arabic','placeholder'=>'')) ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-3 col-lg-3 col-sm-3">
          <div class="form-group">
            <?php echo $this->Form->input('code',array('type'=>'text','disabled' => 'disabled','class'=>'form-control product_disable','label'=>'Product Code')) ?>
          </div>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-4">
          <div class="form-group">
            <label>Product Type</label>
            <?php echo $this->Form->input('product_type_id_modal',array('type'=>'select','disabled' => 'disabled','options'=>array('empty'=>'SELECT',$Product_type),'class'=>'form-control select2 product_type_id_modal','style'=>array('width:100%'),'label'=>false)); ?>
          </div>
        </div>
        <div class="col-md-5 col-lg-5 col-sm-5">
          <div class="col-md-9 col-lg-9 col-sm-9">
            <div class="form-group">
              <label>Brand</label>
              <?php echo $this->Form->input('Brand_id_modal',array('type'=>'select','options'=>array('empty'=>'SELECT',$Brand),'class'=>'form-control select2 brand_class brand_id','style'=>array('width:100%'),'label'=>false,)); ?>
            </div>
          </div>
          <div class="col-md-3 col-lg-3 col-sm-3 no-padding">
           <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" data-toggle="modal"  data-target="#Brand_Add_Modal" style="margin-top: 27px;"></i></a>
         </div>
       </div>
     </div>
     <div class="row">
      <div class="col-md-4 col-lg-4 col-sm-4">
       <div class="form-group">
         <?php echo $this->Form->input('unit_cost',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Landing Cost/Unit')) ?>
       </div>
     </div>
      <div class="col-md-4 col-lg-4 col-sm-4">
        <div class="form-group">
         <?php echo $this->Form->input('unit_mrp',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Retail Price/Unit')) ?>
       </div>
     </div>
     <div class="col-md-4 col-lg-4 col-sm-4">
       <div class="form-group">
         <?php echo $this->Form->input('unit_wholesale_price',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Wholesale Price/Unit')) ?>
       </div>
     </div>
   </div>
   <div class="row">
<div class="col-md-4 col-lg-4 col-sm-4">
     <div class="form-group">
      <?php echo $this->Form->input('cost',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Landing Cost/Piece')) ?>
    </div>
  </div>
     <div class="col-md-4 col-lg-4 col-sm-4">
       <div class="form-group">
        <?php echo $this->Form->input('mrp',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Retail Price/Piece')) ?> 
      </div>
    </div>
    <div class="col-md-4 col-lg-4 col-sm-4">
      <div class="form-group">
        <?php echo $this->Form->input('wholesale_price',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Wholesale Price/Piece')) ?>
      </div>
    </div>
  </div>
  <div class="row">
   
  <div class="col-md-4 col-lg-4 col-sm-4">
   <div class="form-group">
    <?php echo $this->Form->input('unit_id',array('type'=>'select','options'=>$Unit,'selected'=>1,'style'=>array('width:100%'),'disabled'=>'disabled','class'=>'form-control select2 unit_id','label'=>'Unit')) ?>
    <?php echo $this->Form->input('unit_level',array('type'=>'hidden')) ?>
  </div>
</div>
<div class="col-md-4 col-lg-4 col-sm-4">
 <div class="form-group">
   <?php echo $this->Form->input('threshold',array('type'=>'text','class'=>'form-control product_disable number_field','label'=>'Threshold *','min'=>0)) ?>
 </div>
</div>
<div class="col-md-4 col-lg-4 col-sm-4">
 <div class="form-group">
   <?php echo $this->Form->input('no_of_piece_per_unit',array('type'=>'number','min'=>'0','class'=>'form-control product_disable no_of_piece_per_unit number_field piece_stock_calc piece_price_calc','value'=>1,'label'=>'No Of Pieces','readonly')) ?>
 </div>
</div>
  <div class="col-md-4 col-lg-4 col-sm-4">
        <div class="form-group">
          <?php echo $this->Form->input('vat',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'VAT %')); ?>
        </div>
      </div> 
<div class="col-md-4 col-lg-4 col-sm-4">
 <div class="form-group">
   <button type="button" class="btn btn-primary" id="Edit_Product" style="margin-top: 25px;">Edit Product</button>
 </div>
</div>
<?php echo $this->Form->end(); ?>
</div>









        <!-- <div class="form-horizontal">
          <?php echo $this->Form->create('ProductEdit',array('id'=>'Product_Edit_Form')); ?>
          <div class="form-group">
            <div class="col-sm-9">
              <?php echo $this->Form->input('name',array('class'=>'form-control product_disable toUpperCase','label'=>'Product Name In Full *','placeholder'=>' Enter Product Name')) ?>
              <?php echo $this->Form->input('id',array('type'=>'hidden')) ?>
            </div>
          </div>
          <div class="form-group">
           <div class="col-sm-9">
            <?php echo $this->Form->input('arabic_name',array('class'=>'form-control ','dir'=>'rtl','label'=>'Product Name Arabic','placeholder'=>'')) ?>
          </div>
        </div>
            <div class="col-sm-3">
              <?php echo $this->Form->input('code',array('type'=>'text','disabled' => 'disabled','class'=>'form-control product_disable','label'=>'Product Code')) ?>
            </div>
          </div> -->
         <!--  <div class="form-group">
            <label class="col-sm-3 control-label">Product Type</label>
            <div class="col-sm-6">
              <?php echo $this->Form->input('product_type_id_modal',array('type'=>'select','disabled' => 'disabled','options'=>array('empty'=>'SELECT',$Product_type),'class'=>'form-control select2 product_type_id_modal','style'=>array('width:100%'),'label'=>false)); ?>
            </div>
          </div> -->


        <!--   <div class="form-group">
            <label class="col-sm-2 control-label">Brand</label>
            <div class="col-sm-6">
              <?php echo $this->Form->input('Brand_id_modal',array('type'=>'select','options'=>array('empty'=>'SELECT',$Brand),'class'=>'form-control select2 brand_class brand_id','style'=>array('width:100%'),'label'=>false,)); ?>
            </div>
            <div class="col-sm-2">
              <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" data-toggle="modal"  data-target="#Brand_Add_Modal"></i></a>
            </div>
          </div>


          <div class="form-group">
            <label class="col-sm-3 control-label">Main Supplier</label>
            <div class="col-sm-6">
              <?php echo $this->Form->input('Party_id_modal',array('type'=>'select','options'=>array('0'=>'SELECT',$PartyList),'class'=>'form-control select2 ','style'=>array('width:100%'),'label'=>false)); ?>
            </div>
          </div>
          <div class="form-group ">
            <div class="col-sm-4">
              <?php echo $this->Form->input('vat',array('type'=>'number','value'=>'0','class'=>'form-control','label'=>'VAT %')); ?>
            </div>
          </div>
           <div class="form-group price_of_unit_per_unit_feild" style="display:none">
            <div class="col-sm-4 unit_retaile_price_field">
              <?php echo $this->Form->input('unit_mrp',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Retail Price/Unit')) ?>
            </div>
            <div class="col-sm-4 unit_whole_sales_price_field">
              <?php echo $this->Form->input('unit_wholesale_price',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Wholesale Price/Unit')) ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('unit_cost',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field piece_price_calc','value'=>0,'label'=>'Landing Cost/Unit')) ?>
            </div>
          </div> -->
          <!-- <div class="form-group price_of_piece_per_unit_feild">
            <div class="col-sm-4 retaile_price_field">
              <?php echo $this->Form->input('mrp',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Retail Price/Piece')) ?> 
            </div>
            <div class="col-sm-4 whole_sales_price_field">
              <?php echo $this->Form->input('wholesale_price',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Wholesale Price/Piece')) ?>
            </div>
            <div class="col-sm-4">
              <?php echo $this->Form->input('cost',array('type'=>'text','min'=>'0','step'=>'0.01','class'=>'form-control product_disable number_field','value'=>0,'label'=>'Landing Cost/Piece')) ?>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-6">
              <?php echo $this->Form->input('unit_id',array('type'=>'select','options'=>$Unit,'selected'=>1,'style'=>array('width:100%'),'disabled'=>'disabled','class'=>'form-control select2 unit_id','label'=>'Unit')) ?>
               <?php echo $this->Form->input('unit_level',array('type'=>'hidden')) ?>
            </div>
            <div class="col-sm-6">
              <?php echo $this->Form->input('threshold',array('type'=>'text','class'=>'form-control product_disable number_field','label'=>'Threshold *','min'=>0)) ?>
            </div>
            <div class="col-sm-8" style="display: none;">
              <?php echo $this->Form->input('hsn_code',array('type'=>'text','class'=>'form-control','label'=>'HSN Code')) ?>
            </div>
            <div class="col-sm-3 no_of_piece_per_unit_feild" style="display:none">
              <?php echo $this->Form->input('no_of_piece_per_unit',array('type'=>'number','min'=>'0','class'=>'form-control product_disable no_of_piece_per_unit number_field piece_stock_calc piece_price_calc','value'=>1,'label'=>'No Of Pieces','readonly')) ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-2">
              <?php echo $this->Form->input('quantity',array('class'=>'form-control number_field','label'=>'Stock *','value'=>'0','type'=>'hidden')) ?>
            </div>
            
          </div> -->
        </div>

       <!--  <div class="modal-footer">
          <button type="button" class="btn btn-primary" id='Edit_Product'>Edit Product</button>
        </div> -->
        <?php echo $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>