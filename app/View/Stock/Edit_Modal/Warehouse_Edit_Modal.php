 <div id="Warehouse_Edit_Modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Warehouse</h4>
      </div>
      <?php echo $this->Form->create('WarehouseEdit',array('id'=>'Warehouse_Edit_Form')); ?>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <div class="col-sm-10">
              <?php echo $this->Form->input('name',array('class'=>'form-control toUpperCase','label'=>'Name')); ?>
              <?php echo $this->Form->input('id',array('type'=>'hidden')); ?>
            </div>
          </div>
         <div class="form-group">
            <div class="col-sm-10">
              <?php echo $this->Form->input('edit_description',array('class'=>'form-control toUpperCase','label'=>'Description')); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id='edit_Warehouse'>Edit</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>