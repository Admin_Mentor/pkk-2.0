$('#Product_Add_Btn').click(function(){
  var brand_id=$('#Brand_id_modal').val();
  var product_type_id=$('#product_type_id_modal').val();
  var name=$('#ProductAddName').val();
  if($.trim(name)=='')
  {
    $('#ProductAddName').focus();
    return false;
  }
  var product_id=$('#ProductAddCode').val();
  if($.trim(product_id)=='')
  {
    $('#ProductAddCode').focus();
    return false;
  }
  if(product_type_id=='' || product_type_id=='empty')
  {
    $('#product_type_id_modal').select2('open');
    return false;
  }
  if(brand_id=='empty' || brand_id=='')
  {
    $('#Brand_id_modal').select2('open');
    return false;
  }
  var data=$('#Product_Add_Form').serialize();
  var url_address= "<?= $this->webroot; ?>Product/Product_stock_add_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.message);
      return false;
    }
    else
    {
      $('#product_id').append(response.Product);
      $('#product_id').val($('#product_id option:last-child').val()).trigger('change');
      $('#Product_Add_Form')[0].reset();
      $('#Brand_id_modal').change();
      $('#product_type_id_modal').change();
    }
  }, "json");
});
$('#ProductCost,#ProductSalePrice,#ProductQuantity,#ProductTax').click(function(){
  if($(this).val() == 0)
  {
    $(this).val('');
  }
});
$.fn.product_flitering=function(){
  var product_type_id=$('#product_type_id option:selected').val();
  var brand_id=$('#brand_id option:selected').val();
  if(product_type_id == 'All' && brand_id == 'All'){
      $('#product_id').html(''); 
    return false;
  }
  var data={
    product_type_id:product_type_id,
    brand_id:brand_id,
  };
  var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_type_select_ajax';
  $.ajax({
    type: "post",
    url:url_address,
    data: data,
    dataType:'json',
     success: function(response) { 
      $('#product_id').html('');     
      $('#product_id').append($("<option></option>").attr("value",'').text('SELECT'));
      $.each(response, function(i, value) {
        $('#product_id').append($("<option></option>").attr("value", i).text(value));
      });
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
}
$('.product_flitering').change(function(){
  $.fn.product_flitering();
});
$('#product_id').change(function(){
  // $.fn.table_search();
});
$('#Product_Delete_Btn').click(function(){
  var product_id=$('#product_id').val();
  if(!product_id)
  {
    $('#product_id').select2('open');
    return false; 
  }
  if(!confirm("Are you sure?"))
  {
    return false;
  }
  var url_address= "<?= $this->webroot; ?>Product/product_delete_ajax/"+product_id;
  $.get( url_address, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $("#product_id option[value='" + product_id+ "']").remove();
      $("#product_id").val('').change();
    }
  }, "json");
});
$('#Product_Edit_Btn').click(function(){
  var product_id=$('#product_id').val();
  if(!product_id)
  {
    $('#product_id').select2('open');
    return false; 
  }
  var url_address= "<?= $this->webroot; ?>Product/product_get_ajax/"+product_id;
  $.get( url_address, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      console.log(response);
      $('#ProductEditName').val(response.data.Product.name);
      $('#ProductEditId').val(response.data.Product.id);
      $('#ProductEditCode').val(response.data.Product.code);
      $('#ProductEditMrp').val(response.data.Product.mrp);
      $('#ProductEditUnitId').val(response.data.Product.unit_id).change();
      $('#ProductEditCost').val(response.data.Product.cost);
      $('#ProductEditWholesalePrice').val(response.data.Product.wholesale_price);    
      $('#ProductEditThreshold').val(response.data.Product.threashold);
      $('#ProductEditVat').val(response.data.Product.tax);
      $('#ProductEditProductTypeIdModal').val(response.data.Product.product_type_id).change();
      $('#ProductEditPartyIdModal').val(response.data.Product.party_id).change();
      $('#ProductEditNoOfPiecePerUnit').val(response.data.Product.no_of_piece_per_unit);
      $('#ProductEditNoOfPiecePerUnit').attr('disabled', true);
      
      if(response.data.Product.brand_id!=0)
      {
       
        $('#ProductEditBrandIdModal').val(response.data.Product.brand_id).change();  
      }
      else
      {
        $('#ProductEditBrandIdModal').val('G').change();
      }
      $('#Product_Edit_Modal').modal('show');
    }
  }, "json");
});
$('#Edit_Product').click(function(){
  var data=$('#Product_Edit_Form').serialize();
  var product_id=$('#product_id').val();
  var url_address= "<?= $this->webroot; ?>Product/product_edit_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $("#product_id option[value='" + product_id+ "']").remove();
      $('#product_id').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#Product_Edit_Modal').modal('toggle');
      $('#Product_Edit_Form')[0].reset();
      $("#product_id").val(product_id).trigger('change');
    }
  }, "json");
});
$(document).on('keyup','.piece_stock_calc',function(){
     var unit_level=$('#ProductAddUnitLevel').val();
     if(unit_level == 2 || unit_level == 3){
      var piece_per_box=$('#ProductAddNoOfPiecePerUnit').val();
      
      var box_stock=$('#ProductAddBoxCount').val();
      var piece_stock=$('#ProductAddPieceCount').val();
      if(unit_level == 2){
        $('#stock_quantity').val((parseFloat(box_stock)*parseFloat(piece_per_box))+parseFloat(piece_stock));
      }else{
        var cartoon_stock=$('#ProductAddCartoonCount').val();
        var box_per_cartoon=$('#ProductAddNoOfBoxPerCarton').val();
        var cartoon_stock=parseFloat(box_per_cartoon)*parseFloat(cartoon_stock)*parseFloat(piece_per_box);
        var box_stock=parseFloat(box_stock)*parseFloat(piece_per_box);
         $('#stock_quantity').val(parseFloat(cartoon_stock)+parseFloat(box_stock)+parseFloat(piece_stock));
      }
      
     }
});
// edit FUnction
$(document).on('keyup','.piece_price_calc',function(){
  var unit_level=$('#ProductEditUnitLevel').val();
  if(unit_level == 2 || unit_level == 3){
   var piece_per_box=$('#ProductEditNoOfPiecePerUnit').val();
   var retail_per_unit=$('#ProductEditUnitMrp').val();
   var wholsale_per_unit=$('#ProductEditUnitWholesalePrice').val();
   var cost_per_unit=$('#ProductEditUnitCost').val();
   if(unit_level == 2)
   {
     $('#ProductEditMrp').val(retail_per_unit/piece_per_box);
     $('#ProductEditWholesalePrice').val(wholsale_per_unit/piece_per_box);
     $('#ProductEditCost').val(cost_per_unit/piece_per_box);
   }else{
    var box_per_cartoon=$('#ProductEditNoOfBoxPerCarton').val();
    $('#ProductEditMrp').val((retail_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductEditWholesalePrice').val((wholsale_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductEditCost').val((cost_per_unit/box_per_cartoon)/piece_per_box);
  }
}
});
$(document).on('keyup','.piece_price_calc',function(){
  var unit_level=$('#ProductAddUnitLevel').val();
  if(unit_level == 2 || unit_level == 3){
   var piece_per_box=$('#ProductAddNoOfPiecePerUnit').val();
   var retail_per_unit=$('#ProductAddUnitMrp').val();
   var wholsale_per_unit=$('#ProductAddUnitWholesalePrice').val();
   var cost_per_unit=$('#ProductAddUnitCost').val();
   if(unit_level == 2)
   {
     $('#ProductAddMrp').val(retail_per_unit/piece_per_box);
     $('#ProductAddWholesalePrice').val(wholsale_per_unit/piece_per_box);
     $('#ProductAddCost').val(cost_per_unit/piece_per_box);
   }else{
    var box_per_cartoon=$('#ProductAddNoOfBoxPerCarton').val();
    $('#ProductAddMrp').val((retail_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductAddWholesalePrice').val((wholsale_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductAddCost').val((cost_per_unit/box_per_cartoon)/piece_per_box);
  }
}
});
// edit FUnction
$(document).on('keyup','.piece_price_calc',function(){
  var unit_level=$('#ProductEditUnitLevel').val();
  if(unit_level == 2 || unit_level == 3){
   var piece_per_box=$('#ProductEditNoOfPiecePerUnit').val();
   var retail_per_unit=$('#ProductEditUnitMrp').val();
   var wholsale_per_unit=$('#ProductEditUnitWholesalePrice').val();
   var cost_per_unit=$('#ProductEditUnitCost').val();
   if(unit_level == 2)
   {
     $('#ProductEditMrp').val(retail_per_unit/piece_per_box);
     $('#ProductEditWholesalePrice').val(wholsale_per_unit/piece_per_box);
     $('#ProductEditCost').val(cost_per_unit/piece_per_box);
   }else{
    var box_per_cartoon=$('#ProductEditNoOfBoxPerCarton').val();
    $('#ProductEditMrp').val((retail_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductEditWholesalePrice').val((wholsale_per_unit/box_per_cartoon)/piece_per_box);
    $('#ProductEditCost').val((cost_per_unit/box_per_cartoon)/piece_per_box);
  }
}
});
$('.unit_id').change(function(){
  var unit_id=$(this).val();
  $('#stock_quantity').val(0);
  var data={unit_id,unit_id};
  var url_address= "<?= $this->webroot; ?>Product/GetProductUnitLevel";
  $.post( url_address,data, function( response ) { 
    $('#ProductAddUnitLevel').val(response.Unit.level);
    $('#ProductEditUnitLevel').val(response.Unit.level);
    if(response.Unit.level==2)
    {
      $('.cartoon_count_cls').hide();
      $('.box_count_cls').show();
      $('.piece_count_cls').show();
      $('.no_of_piece_per_unit_feild').show();
      $('.price_of_unit_per_unit_feild').show();
      $('.no_of_box_per_carton_feild').hide();
      $('.no_of_piece_per_unit_feild').show();
      $('.no_of_piece_per_unit').attr('readonly',false);
      $('#stock_quantity').attr('readonly',true);
      $('.price_of_piece_per_unit_feild input').each(function(){
       $(this).attr('readonly',true);
     });
    }
    else if(response.Unit.level==1)
    {
      $('.cartoon_count_cls').hide();
      $('.box_count_cls').hide();
      $('.piece_count_cls').hide();
      $('.price_of_unit_per_unit_feild').hide();
      $('.no_of_piece_per_unit_feild').hide();
      $('.no_of_box_per_carton_feild').hide();
      $('.no_of_piece_per_unit').val('1');
      $('.no_of_box_per_carton').attr('readonly',true);
      $('.no_of_piece_per_unit').attr('readonly',true);
      $('#stock_quantity').attr('readonly',false);
      $('.price_of_piece_per_unit_feild input').each(function(){
       $(this).attr('readonly',false);
     });
    }
    $('#ProductAddUnitMrp').parent().find('label').text('Retail Price/'+response.Unit.name);
    $('#ProductAddUnitWholesalePrice').parent().find('label').text('Wholesale Price/'+response.Unit.name);
    $('#ProductAddUnitCost').parent().find('label').text('Landing Cost/'+response.Unit.name);
  },'json');
});
