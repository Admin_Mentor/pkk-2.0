$(document).ready(function(){
	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	var output = (day<10 ? '0' : '') + day +
	(month<10 ? '0' : '') + month +
	d.getFullYear() ;
	$("[data-mask]").inputmask().val(output);
	$('.datepicker').daterangepicker({
		format: "DD-MM-YYYY",
		singleDatePicker: true,
		calender_style: "picker_4"
	});
	$('#product_type_id').change(function(){
		var product_type_id=$(this).val();
		$.fn.product_type_change(product_type_id);
		$('#product_id').val($('option:eq(0)').val());
	});
	$('#showallbutton').click(function(){
		$('input[type="search"]').val('');
		$('#product_type_id').val($('option:eq(0)').val());
		$('#product_id').val($('option:eq(0)').val());
		$('#batch_no').val('').trigger('keyup');
		$('#product_type_id').trigger('change');
	});
	$.fn.stockcheck = function( options ) {
		var StockProductId=$('#product_id option:selected').val();
		var StockBatchNo=$('#batch_no').val();
		if(StockProductId!='' && StockBatchNo!=''){
			var data={StockProductId:StockProductId,StockBatchNo:StockBatchNo};
			var url_address= '<?php echo $this->webroot; ?>'+'Stock/stock_check_ajax';
			$.ajax({
				type: "post",
				url:url_address,
				data: data,  // post data
				// dataType:'json',
				success: function(response) {
					if(response=="No")
					{
						$('#addstockbutton').attr('disabled',false);
						$('#table_stock_list').DataTable().destroy();
						$('#table_stock_list tbody').html('');
						$('#table_stock_list').DataTable();
					}
					else
					{
						$('#addstockbutton').attr('disabled',true);
						$.fn.search();
					}
				},
				error:function (XMLHttpRequest, textStatus, errorThrown) {
					alert(textStatus);
				}
			});
		}
		else
		{
			$('#addstockbutton').attr('disabled',true);
		}
	};
	$.fn.search = function() {
		var product_type_id=$('#product_type_id').val();
		var product_id=$('#product_id').val();
		var warehouse_id=$('#warehouse_id').val();
		var batch_no=$.trim($('#batch_no').val());
		var data={
			product_type_id:product_type_id,
			product_id:product_id,
			warehouse_id:warehouse_id,
			batch_no:batch_no
		};
		var url_address= '<?php echo $this->webroot; ?>'+'Stock/stock_search_ajax';
		$.ajax({
				type: "post",  // Request method: post, get
				url:url_address,
				data: data,  // post data
				success: function(response) {
					$('#table_stock_list').DataTable().destroy();
					$('#table_stock_list tbody').html(response);
					$('#table_stock_list').DataTable();
					// location.reload();
				},
				error:function (XMLHttpRequest, textStatus, errorThrown) {
					alert(textStatus);
				}
			});
	};
	$('#product_id').change(function(){
		var product_id=$(this).val();
		$('#product_id_new').val(product_id).prop('selected', true);
		$.fn.search();
		var data={product_id:product_id};
		var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_id_select_ajax';
		$.ajax({
			type: "post",  // Request method: post, get
			url:url_address,
			data: data,  // post data
			success: function(response) {
				$( "#batch_no" ).keyup();
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
	});
	$('#batch_no').keyup(function(){
		$('#batch_no_new').val($.trim($(this).val()));
		$.fn.stockcheck();
		$.fn.search();
	});
	$('#addstock').click(function(){
		// $('#table_stock_list tbody').on( 'click', '#addstock', function () {
			var product_type_id=$('#product_type_id_new option:selected').val();
			var product_id=$('#product_id_new option:selected').val();
			var batch_no=$.trim($('#batch_no_new').val());
			if(batch_no=='')
			{
				$('#batch_no_new').focus()
				return false;
			}
			var quantity=$.trim($('#quantity').val());
			if(quantity=='')
			{
				$('#quantity').focus();
				return false;
			}
			var stock_date=$('#stock_date').val();
			var exp_date=$('#exp_date').val();
			var data={
				product_type_id:product_type_id,
				product_id:product_id,
				batch_no:batch_no,
				quantity:quantity,
				stock_date:stock_date,
				exp_date:exp_date,
			};
			var url_address= '<?php echo $this->webroot; ?>'+'Stock/Stockadd';
			$.ajax({
			type: "post",  // Request method: post, get
			url:url_address,
			data: data,  // post data
			dataType:'json',
			success: function(response) {
				$('.flash_message').html(response.message);
				$.fn.search();
				$('#quantity').val('');
				$('#addstockbutton').attr('disabled',true);
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
		});
	$('.product_type_disable').keyup(function(){
		var product_type_id=$('#prdct_type').val().trim();
		// var product_code=$('#ProductTypeProductTypeCode').val();
		if(product_type_id!="")// && product_code!="")
		{
			$('.addproductTypebutton').attr('disabled',false);
		}
		else{
			$('.addproductTypebutton').attr('disabled',true);
		}
	});
	$('#EditStockStockQuantityNew').keyup(function(){
		var edit_quantity=$('#EditStockStockQuantityNew').val();
		if(edit_quantity!="")
		{
			$('#editstock').attr('disabled',false);
		}
		else{
			$('#editstock').attr('disabled',true);
		}
	});
	$('#quantity').keyup(function(){
		var quantity=$('#quantity').val();
		if(quantity!="")
		{
			$('#addstock').attr('disabled',false);
		}
		else{
			$('#addstock').attr('disabled',true);
		}
	});
	
	$.fn.product_type_append=function(result){
		$('#product_type_id').append(result);
		$('#product_type_id_new').append(result);
		$('#ProductProductTypeIdNew1').append(result);
		$('#ProductProductTypeId').append(result);
	}
	$('#Print').click(function(){
		var url_address= '<?php echo $this->webroot; ?>'+'Stock/print_report';
		$(location).attr("href", url_address);
	});
	$('.input_number').on('click',function(){
		$(this).select();
	});
	$('.input_number').on('keyup',function(){
		if(isNaN($(this).val())){
			$(this).val('0');
		}
	});
	
});
$('#addstockbutton').click(function(){
	var product_type_id=$('#product_type_id option:selected').val();
	var product_id=$('#product_id option:selected').val();
	var batch_no=$.trim($('#batch_no').val());
	if(product_type_id=='')
	{
		$('#product_type_id').select2('open');
		return false;
	}
	if(product_id=='')
	{
		$('#product_type_id').select2('open');
		return false;
	}
	if(batch_no=='')
	{
		$('#batch_no').focus();
		return false;
	}
	$('#product_type_id_new').val(product_type_id).trigger('change');
	$('#product_id_new').val(product_id).trigger('change');
	$('#batch_no_new').val(batch_no);
	$('#addtostock').modal('show');
});
$(document).on('click','.Edit',function(){
	var id=$(this).closest('tr').find('td.id span.Stock_id').text();
	var quantity=$(this).closest('tr').find('td.Quantity').text();
	var exp_date=$(this).closest('tr').find('td.exp_date').text();
	var split_date =exp_date.split('-');
	var product_id=$(this).closest('tr').find('td.product_name').text();
	$('#EditStockId').val(id);
	$('#EditStockExpDate').inputmask().val(split_date[0]+split_date[1]+split_date[2]);
	$('#product_name').val(product_id);
	$('#quanaity_value').val(quantity);
	$('#Edit_modal').modal('show');
});
$('#editstock').click(function(){
	var id=$('#EditStockId').val();
	var exp_date=$('#EditStockExpDate').val();
	var quantity=$.trim($('#EditStockStockQuantityNew').val());
	if(quantity=='')
	{
		$('#EditStockStockQuantityNew').focus();
		return false;
	}
	var remark=$('#EditStockStockRemark').val();
	var data=$('#EditStockIndexForm').serialize();
	var url_address= '<?php echo $this->webroot; ?>'+'Stock/EditStock_ajax';
	$.ajax({
			type: "post",  // Request method: post, get
			url:url_address,
			data: data,  // post data
			dataType:'json',
			success: function(response) {
				$('.flash_message').html(response.result);
				$('#table_stock_list tbody tr').each(function(){
					var table_id=$(this).closest('tr').find('td.id span.Stock_id').text();
					if(table_id==id)
					{
						$(this).closest('tr').find('td.exp_date').text(exp_date);
						$(this).closest('tr').find('td.Quantity').text(quantity);
						$(this).closest('tr').attr('style','font-size:20px;color:green');
						$(this).closest('tr').attr('class','blue-pd flash');
						setTimeout(timer, 2000);
					}
				});
				$('#EditStockStockQuantityNew').val('');
				$('#addstockbutton').attr('disabled',true);
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
});
$(document).on('click','.Delete',function(){
	if(!confirm("Are you sure?"))
	{
		return false;
	}
	var id=$(this).closest('tr').find('td.id span.Stock_id').text();
	var rowindex = $(this).closest('tr').index();
	var url_address= '<?php echo $this->webroot; ?>'+'Stock/delete_ajax/'+id;
	$.ajax({
			type: "post",  // Request method: post, get
			url:url_address,
			// data: data,  // post data
			dataType:'json',
			success: function(response) {
				if(response.result!='Success')
				{
					var table_id=$(this).closest('tr').find('td.id span.Stock_id').text();
					$('#table_stock_list tbody tr:eq(' + rowindex + ')').attr('style','color:red;font-size:20px;');
					$('#table_stock_list tbody tr:eq(' + rowindex + ')').attr('class','blue-pd flash');
					setTimeout(timer, 2000);
				}
				else
				{
					$('#table_stock_list tbody tr:eq(' + rowindex + ')').remove();
				}
				$('.flash_message').html(response.result);
				
			},
			error:function (XMLHttpRequest, textStatus, errorThrown) {
				alert(textStatus);
			}
		});
});
function timer()
{
	$('#table_stock_list tbody tr').each(function(){
		$(this).closest('tr').attr('style','');
		$(this).closest('tr').attr('class','blue-pd');
	});
}