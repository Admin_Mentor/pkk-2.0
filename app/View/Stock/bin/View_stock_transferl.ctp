<style type="text/css">
  <?php include "style.css" ?>
  .ad-stk{
    color: #FFF !important;
    background-color: #13689e !important;
    padding: 5px 20px !important;
    border: 0px solid #FFF !important;
    border-radius: 3px !important;
    transition: all ease .5s;
    margin-top: 20%;
  }
  .ad-stk:hover {
    background-color: #023a5e !important;
  }
  .list-arrows {
    padding-top: 100px;
  }
  .list-arrows button {
    margin-bottom: 20px;
  }
  .mr-tp-30 {
    margin-top: 30px;
  }
</style>
<aside class="main-sidebar">    
  <section class="sidebar" style="height: auto;">         
    <ul class="sidebar-menu">       
      <li class="active treeview">
        <a href="#">
          <i class="fa fa-share"></i> <span>Stock</span>
          <i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">
          <!-- <li><a href="<?php echo $this->webroot; ?>Stock/Index"><i class="fa fa-circle-o"></i> Stock Management </a></li> -->
          <li><a href="<?php echo $this->webroot; ?>Stock/StockTransferList"><i class="fa fa-circle-o"></i> Stock Transfer </a></li>
          <!-- <li><a href="<?php echo $this->webroot; ?>Stock/DamagedStock"><i class="fa fa-circle-o"></i> Damaged Stock </a></li> -->
        </ul>
      </li>
    </ul>
  </section>        
</aside>
<section class="content-header">
  <h1> Stock Transfer </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header"> 
    </div>
    <div class="box-body"> 
      <?= $this->Form->create('StockTransfer', array('url' => array('controller' => 'Stock', 'action' => 'StockTransfer')));?>
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="row">
            <div class="form-group col-md-10">
            <?php pr($StockTransfer);exit; ?>
              <?php echo $StockTransfer['StockTransfer']['from_warehouse'] ?>                   
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-10">
              <?php echo $this->Form->input('warehouse_to',array('type'=>'select','empty'=>'Select Warehouse','options'=>$Warehouse,'class'=>'form-control select2 search_class','label'=>'To','id'=>'warehouse_to')) ?>                
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="form-group col-md-12">
            <?= $this->Form->input('remarks', array('type' => 'textarea','class'=>"form-control",'rows'=>4)); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-md-offset-3">
          <div class="row">
            <div class="form-group col-md-10">
              <?= $this->Form->input('transfer_no',array('class'=>'form-control','label'=>'Transfer ID','type'=>'text','required','id'=>'transfer_no',)); ?>                 
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-10">
              <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <div class="box-body table-responsive no-padding boder">
            <table class="table table-hover" id="stock_transfer_tbl" >                    
              <thead>
                <tr  class="blue-bg">
                  <th>Slno</th>
                  <th>Product</th>
                  <th>Current Quantity</th>
                  <th>Quantity To Move</th>
                  <th>Balance Quantity</th>
                  <th></th>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <?php echo $this->Form->input('product_id',array('type'=>'select', 'empty' =>'Select Product','class'=>'form-control select2 search_class','label'=>false,'id'=>'product_id','style'=>'width:100%')) ?> 
                  </td>
                  <td><input class="form-control" id="current_quantity" type="text" readonly="readonly"></td>
                  <td><input class="form-control" id="move_quantity" type="text"></td>
                  <td><input class="form-control" id="balance_quantity" type="text" readonly="readonly"></td>
                  <td><i id="add_transfer_product" class="fa fa-plus-circle fa-2x fnt-awsm-btn"></i></td>                         
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <br/>
      <div class="row">
        <div class="modal-footer">
          <button type="submit" id='save_button' disabled class="btn btn-success">Save</button>
          <!-- <button type="button" class="btn btn-default print">Print</button> -->
        </div>
      </div>
      <?= $this->Form->end(); ?>
    </div>
  </div>
</section>
<script type="text/javascript">
  $.fn.product_flitering=function(){
    $('#brand_in_display').val('');
    var warehouse_id=$('#warehouse_from').val();
    var product_type_id='';
    var brand_name_id='';
    var modal='';
    var data={
      warehouse_id:warehouse_id,
      product_type_id:product_type_id,
      brand_name_id:brand_name_id,
      modal:modal,
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_list_get_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {              
        $('#product_id').html(response.option);
        $('#product_id').val('');
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  }
  $(document).on('change','#warehouse_from',function(){
    $.fn.product_flitering();
  })
  $(document).on('change','#warehouse_from,#warehouse_to',function(){
    if($('#warehouse_from').val() == $('#warehouse_to').val())
    {
      $.fn.show_alert('Select Diffrent Warehouse');
      $('#warehouse_to').val('').trigger('change.select2');
      return false;
    }
  });
  $(document).on('change','#product_id',function(){
    if(!$('#warehouse_from').val()){ $('#warehouse_from').select2('open'); $.fn.show_alert("Select 'From' warehouse"); $('#current_quantity').val(''); return false; }
    if(!$('#warehouse_to').val()){ $('#warehouse_to').select2('open'); $.fn.show_alert("Select 'To' warehouse"); $('#current_quantity').val(''); return false; }
    if(!$('#product_id').val()){ $('#product_id').select2('open'); $.fn.show_alert('Select Product'); $('#current_quantity').val(''); return false; }
    var product_id=$(this).val();
    var warehouse_id=$('#warehouse_from').val();
    var data={
      warehouse_id:warehouse_id,
      product_id:product_id,
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/get_product_quantity';
    $.post( url_address,data, function( response ) {
      $('#current_quantity').val(parseInt(response.quantity));
    }, "json");
  });
  $.fn.show_balance_quantity =function()
  {
    $('#balance_quantity').val(parseInt( $('#current_quantity').val()) - parseInt( $('#move_quantity').val()) );
  }
  $(document).on('keyup','#move_quantity',function(){
    $.fn.show_balance_quantity();
  });
  $(document).on('click','#add_transfer_product',function(){
    var tablecount=$("#stock_transfer_tbl tbody tr").length;
    var product_text=$('#product_id option:selected').text();
    var move_quantity = $('#move_quantity').val();
    var product_id = $('#product_id').val();
    if(!product_id){
      $('#product_id').select2('open');
      $.fn.show_alert("Select Product");
      return false;
    }
    if(!move_quantity){
      $('#move_quantity').focus();
      $.fn.show_alert("Enter Quantity");
      return false;
    }
    var current_quantity=$('#current_quantity').val();
    var balance_quantity=$('#balance_quantity').val();
    var slno=parseInt(tablecount)+parseInt(1);
    var remove_button='<i class="fa fa-minus-circle fa-2x fnt-awsm-btn remove_button"></i>';
    var product_field='<input type="hidden" name="data[StockTransfer][product][]" value="'+product_id+'"><input type="text" class="form-control" readonly value="'+product_text+'">'+'';
    var move_quantity_field='<input type="text" class="form-control" readonly name="data[StockTransfer][quantity][]" value="'+move_quantity+'">'+'';
    var current_quantity_field='<input type="text" class="form-control" readonly value="'+current_quantity+'">'+'';
    var balance_quantity_field='<input type="text" class="form-control" readonly value="'+balance_quantity+'">'+'';
    $('#stock_transfer_tbl tbody').append('<tr class="product_'+product_id+'">\
      <td>'+slno+'</td>\
      <td>'+product_field+'</td>\
      <td>'+current_quantity_field+'</td>\
      <td>'+move_quantity_field+'</td>\
      <td>'+balance_quantity_field+'</td>\
      <td>'+remove_button+'</td>\
    </tr>');
    $('#save_button').removeAttr('disabled');
  });
  $(document).on('click','.remove_button',function(){
    $(this).closest('tr').remove();
    $('#save_button').attr('disabled',true);
  });
</script>
