<section class="content-header">
  <h1> Damaged Stock </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
    </div>
    <div class="box-body">
      <?= $this->Form->create('DamagedStock'); ?>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
            <div class="form-horizontal">
              <?= $this->Form->input('warehouse_id',array('options'=>$Warehouse,'empty'=>'ALL','class'=>'form-control select2 product_flitering','id'=>'warehouse_id')); ?>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
            <div class="form-horizontal">
              <div class="form-group">
                <?= $this->Form->input('product_type',array('options'=>$ProductType,'empty'=>'ALL','class'=>'form-control select2 product_flitering','id'=>'product_type_id')); ?>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
            <div class="form-horizontal">
              <?= $this->Form->input('brand',array('options'=>$Brand,'empty'=>'ALL','class'=>'form-control select2 product_flitering','id'=>'brand_id')); ?>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
            <div class="form-horizontal">
              <?= $this->Form->input('product_id',array('options'=>$Product,'class'=>'form-control select2','id'=>'product_id')); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 col-sm-12">
          <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
            <div class="form-horizontal">
              <?= $this->Form->input('actual_qty',array('class'=>'form-control','id'=>'actual_qty_id','readonly')); ?>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
            <div class="form-horizontal">
             <div class="form-group">
              <?= $this->Form->input('quantity',array('class'=>'form-control','id'=>'damaged_qty_id')); ?>
            </div></div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
            <div class="form-horizontal">
              <?= $this->Form->input('type',array('options'=>['Damaged'=>'Damaged','Expired'=>'Expired'],'class'=>'form-control select2','id'=>'type_id')); ?>
            </div>
          </div>
          <div class="col-md-3 col-sm-3 col-lg-3 col-xs-12">
            <div class="form-horizontal">
              <?= $this->Form->input('remark',array('class'=>'form-control','id'=>'remark_id')); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <?= $this->Form->submit('Save',array('class'=>'save pull-right','id'=>'save_button')); ?>
        </div>
      </div>
      <?= $this->Form->end(); ?>
      <div class="row">
        <hr>
        <div class="col-md-12">
          <div class="box-body table-responsive no-padding">
            <table class="table table-hover boder">
              <thead>
                <tr class="blue-bg">
                  <th>Product Type</th>
                  <th>Brand</th>
                  <th>Product</th>
                  <th>Type</th>
                  <th>Cost</th>
                  <th>Mrp</th>
                  <th>Quantity</th>
                  <th>Date</th>
                  <!-- <td>Action</td> -->
                </tr>
              </thead>
              <tbody>
              <?php foreach ($DamagedStock as $key => $value):?>
                <tr>
                <td><?= $value['ProductType']['name']; ?></td>
                <td><?= $value['Brand']['name']; ?></td>
                <td><?= $value['Product']['name']; ?></td>
                 <td><?= $value['DamagedStock']['type']; ?></td>
                <td><?= $value['Product']['cost']; ?></td>
                <td><?= $value['Product']['mrp']; ?></td>
                <td><?= $value['DamagedStock']['quantity']; ?></td>
                <td><?= date('d-m-Y',strtotime($value['DamagedStock']['updated_at'])); ?></td>
                <!-- <td></td> -->
                </tr>
              <?php endforeach ?>
                
              </tbody>
            </table>
          </div>
        </div>
        <br>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
  $('#product_type_id').change(function(){
    var product_type_id=$(this).find('option:selected').val();
    if(product_type_id)
    {
      var url_address= '<?php echo $this->webroot; ?>Brand/get_brand_name_ajax/'+product_type_id;
      $.ajax({
        method: "POST",
        url:url_address,
        dataType:'json',
      }).done(function( result ) {
        $('#brand_id').html(result.option);
        $('#brand_id').val('');
      });
    }
  });
  $('.product_flitering').change(function(){
    $.fn.product_flitering();
  });
  $.fn.product_flitering=function(){
    var warehouse_id=$('#warehouse_id').val();
    var product_type_id=$('#product_type_id').val();
    var brand_id=$('#brand_id').val();
    var data={
      warehouse_id:warehouse_id,
      product_type_id:product_type_id,
      brand_name_id:brand_id,
    };
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_list_get_ajax';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {
        $('#product_id').html(response.option);
        $('#actual_qty_id').val('0');
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  }
  $('#product_id').change(function(){
    var product_id=$(this).val();
    var warehouse_id=$('#warehouse_id').val();
    if(!warehouse_id)
    {
      $('#warehouse_id').select2('open');
      return false;
    }
    var data={
      warehouse_id:warehouse_id,
      product_id:product_id
    }
    $.post( "<?= $this->webroot ?>Stock/get_product_quantity",data ,function( data ) {
      if(data.quantity)
      {
        $('#actual_qty_id').val(data.quantity);  
      }

    }, "json");
  });
</script>