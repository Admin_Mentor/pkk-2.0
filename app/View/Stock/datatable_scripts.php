
<script type="text/javascript">
  $('#table_stock_list').DataTable( {
    "processing": true,
    "serverSide": true,
    "searching": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Stock/stock_data_datatable",
      "type": "POST",
      "dataSrc": "records",
    },
    dom: 'Bfrtip',
    lengthMenu: [
    [10, 25, 50,-1],
    ['10 rows', '25 rows', '50 rows','Show all']
    ],
    buttons: [
    { extend: 'colvis' },
    'pageLength',
    {
      extend: 'print',
      footer:true,
      customize: function ( win ) {
        $(win.document.body).find( 'table' )
        .css( 'font-size', '8pt' );
      },
      title: 'Stock Management - '+$('#product_type_id option:selected').text(),
      exportOptions: { columns: ':visible'},
},
{
  extend: 'excel',
  footer:true,
  title: 'Stock Management',
  exportOptions: { columns: ':visible'},
},
],
"columns": [
{ "data" : "Product.code" },
{ "data" : "Product.name" },
{ "data" : "ProductType.name" },
{ "data" : "TypeOfProduct.name" },
{ "data" : "Brand.name" },
{ "data" : "Unit.name" },
{ "data" : "Product.cost" },
{ "data" : "Warehouse.name" },
{ "data" : "Stock.quantity" },
{ "data" : "Product.mrp",className:"text-right" },
{ "data" : "Stock.line_total",className:"text-right" },
{ "data" : "Product.action" },
],
"columnDefs": [
{ className: "cost-field text-right", "targets": [ 6 ], },
{ "targets": [ 10 ]},//edit
{ "targets": [ 11 ],"visible": false,"orderable": false },//edit
],
"rowCallback": function( row, data ) {
    $.fn.show_alert('Success');

  },
// "footerCallback": function ( row, data, start, end, display ) {
//   var api = this.api();
//   var intVal = function ( i ) {
//     return typeof i === 'string' ?
//     i.replace(/[\$,]/g, '')*1 :
//     typeof i === 'number' ?
//     i : 0;
//   };
//   var total_mrp = api
//   .column( 9)
//   .data()
//   .reduce( function (a, b) {
//     return intVal(a) + intVal(b);
//   } ,0);
//   var total_mrp=total_mrp.toFixed(2);
//   $( api.column( 9 ).footer() ).html(
//     total_mrp
//     );
// },
"footerCallback": function ( row, data, start, end, display ) {
      var api = this.api(), data; var intVal = function ( i ) { return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ? i : 0; };
      var pagecount=10;
      pageTotal = api.column( pagecount, { page: 'current'} ).data().reduce( function (a, b) { return intVal(a) + intVal(b); },0); $( api.column( pagecount ).footer() ).html(''+(pageTotal.toFixed(3))+'');
    
    },
"fnDrawCallback": function( oSettings ) {
  $.fn.cost_visiblility();
}
});

  $.fn.show_alert = function(flash)
     {
      //$.alert(flash, {title:' ',type: 'info',position: ['top-right', [45, 25]],});
      }
      
  $('#table_stock_list tbody').on( 'click', '.edit_stock_btn', function () {
    var batch_id=$(this).attr('id');
    var quantity=$(this).closest('tr').find('td span.stock_quantity').text();
    var product_name=$(this).closest('tr').find('td span.product_name').text();
    $('#EditStockId').val(batch_id);
    $('#product_id_name').val(product_name);
    $('#quanaity_value').val(quantity);
    $('#Edit_modal').modal('show');
  });

  $(document).on('change','#type_product,#product_type_id,#brand_id,#product_visibility,#warehouse_id,#size_id,#product_id,#Out_Of_Stock_button', function () {
    var product_type_id=$('#product_type_id option:selected').val();
    var type_product=$('#type_product option:selected').val();
    var product_id=$('#product_id option:selected').val();
    var brand_id=$('#brand_id option:selected').val();
    var warehouse_id=$('#warehouse_id option:selected').val();
    var size_id=$('#size_id option:selected').val();
    var batch_id=$('#batch_id option:selected').val();
    if($('#Out_Of_Stock_button').is(':checked')){
      var check_id=0;
    }
    else{
      var check_id=1;
    }
    var data={
      type_product:type_product,
      product_type_id:product_type_id,
      product_id:product_id,
      warehouse_id:warehouse_id,
      check_id:check_id,
      brand_in_display:brand_id
    };

    var url_address= '<?php echo $this->webroot; ?>'+'Stock/StockFilter';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {
        $.fn.product_filter_function_datatable();
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  });
  $.fn.product_filter_function_datatable = function(){
    table = $('#table_stock_list').dataTable();
    table.fnDraw();
  }
</script>
