<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>

<style type="text/css">
<?php include "style.css" ?>

.out_stock_btn {
  margin-left: 11px;
  margin-top: 35px;
}

</style>
<section class="content-header">
  <h1> Stock Management </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="Stockmanagement">
          <br>
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-3 col-lg-3 col-sm-3">
                <!-- <div class="box-body"> -->
                  <!-- <div class="col-sm-12"> -->
                    <div class="form-group">
                      <?php echo $this->Form->input('product_type_id',array('type'=>'select','empty' =>'ALL','options'=>$Product_type,'class'=>'form-control select_two_class search_class product_flitering','style'=>'width:100%','label'=>'PRODUCT TYPE','id'=>'product_type_id',)) ?>
                    </div>
                  <!-- </div> -->
<!--                   <br>
 -->                 <!--  <div class="col-md-4" hidden>
                    <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#Product_Type_Add_Modal"></i></a>
                    <a href="#" id="Product_Type_Edit_Btn"><i class="fa fa-pencil fa ad-mar"></i></a>
                    <a href="#" id='Product_Type_Delete_Btn'><i class="fa fa-trash fa ad-mar"></i></a>
                  </div> -->
                <!-- </div> -->
              </div>
              <div class="col-md-3 col-lg-3 col-sm-3">
                <!-- <div class="box-body"> -->
                  <!-- <div class="col-sm-12"> -->
                    <div class="form-group">
                     <?php echo $this->Form->input('type_product',array(
                'type'=>'select','empty' =>'ALL','options'=>$Type,'class'=>'form-control select_two_class search_class product_flitering','label'=>'Type of Product','id'=>'type_product',
                )) ?>
                    </div>
                  <!-- </div> -->
                <!-- </div> -->
              </div>
              <div class="col-md-3 col-lg-3 col-sm-3">
                <!-- <div class="box-body"> -->
                  <!-- <div class="col-sm-12"> -->
                    <div class="form-group">
                      <?php echo $this->Form->input('brand',array('type'=>'select','empty' =>'ALL','options'=>$Brand,'style'=>'width:100%','id'=>'brand_id','class'=>'form-control select_two_class search_class product_flitering','label'=>'Brand')) ?>
                    </div>
                  <!-- </div> -->
                  <!-- <div class="col-sm-4 col-md-4 col-lg-4" hidden>
                    <br>
                    <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#Brand_Add_Modal"></i></a>
                    <a href="#" id='Brand_Edit_Btn'><i class="fa fa-pencil fa ad-mar"></i></a>
                    <a href="#" id='Brand_Delete_Btn'><i class="fa fa-trash fa ad-mar"></i></a>
                  </div> -->
                <!-- </div> -->
              </div>
              <div class="col-md-3 col-lg-3 col-sm-3">
                <!-- <div class="box-body"> -->
                  <!-- <div class="col-sm-12"> -->
                    <div class="form-group">
                      <?php echo $this->Form->input('product_id',array('type'=>'select', 'empty' =>'ALL','options'=>$Product,'style'=>'width:100%','class'=>'form-control select_two_class search_class','label'=>'PRODUCT','id'=>'product_id')) ?>
                    </div>
                  <!-- </div> -->
                <!-- </div> -->
              </div>
            </div>
          </div> 
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-3 col-lg-3 col-sm-3">
                  <!-- <div class="col-sm-9"> -->
                      <?php echo $this->Form->input('warehouse_id',array('type'=>'select','empty'=>'ALL','options'=>$Warehouse,'class'=>'form-control select_two_class search_class','label'=>'WAREHOUSE','id'=>'warehouse_id')) ?>
                    <!-- </div> -->
               <!--    <div class="col-sm-3" style="margin-top:7%;display: none">
                    <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" data-toggle="modal" data-target="#addwarehouse"></i></a>
                    <a href="#" id="Warehouse_Edit_Btn"><i class="fa fa-pencil fa ad-mar"></i></a>
                </div> -->
              </div>
             <!--  <div class="col-md-1 col-lg-1 col-sm-1" style="margin-top:0%;display: none">
                  <a href="#" id="showallbutton"> <i class="fa fa-refresh fa-2x ad-mar tp_6px"></i></a>
                </div> -->

                <div class="col-md-2 col-lg-2 col-sm-2 out_stock_btn product_visibility" style="margin-top:2%;" >
                  <!-- <div class="col-sm-12 product_visibility"> -->
                    <input class='nav-toggle' type="checkbox" id='Out_Of_Stock_button' data-width="130" data-toggle="toggle" data-off="Show All Products" data-on="Hide Out Of Stock">
                    <!-- </div> -->
              </div>
              <div class="col-md-2 col-lg-2 col-sm-2" id='cost_visibility' style="margin-top:2%;" >
                  <!-- <div class="col-sm-12"> -->
                    <input class='nav-toggle' type="checkbox" id='toggle_button_for_cost_visibility' data-width="130" data-toggle="toggle" data-off="Show Cost" data-on="Hide Cost">
                    <!-- </div> -->
              </div>
                              <?php if($show_cost ==1) : ?>
               <div class="col-md-3 col-lg-3 col-sm-3 out_stock_btn" style="margin-top: 1%;">
                  <!-- <div class="col-sm-12"> -->
                      <h3 hidden class="total-amt cost-field">Total Cost : <span id='main_total_cost'><?php echo round($total_cost,2); ?></span></h3>
                    <!-- </div> -->
              </div>
              <?php endif; ?>
              </div>
              </div>
        <hr/>
        <div class="inn_tabl_wrap">
          <div class="box-body">
            <table class="table table-condensed text-center table-bordered" id='table_stock_list' width="100%" data-order='[[ 1, "asc" ]]'>
              <thead>
                <tr class="blue-bg">
                  <th>Product Code</th>
                  <th>Product</th>
                  <th>Category</th>
                  <th>Type</th>
                  <th>Brand</th>
                  <th>Unit</th>
                  <th class="cost-field" style="display: none;">Cost</th>
                  <th>Warehouse</th>
                  <th>Quantity</th>
                  <th>MRP</th>
                  <th>Line Total</th>
                  <th>Edit</th>
                </tr>
              </thead>

              <tbody>
              </tbody>
              <tfoot>
                <tr>
                  <th style="font-size:20px; color:red;text-align:right">Total:</th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th style="font-size:20px; color:red;"></th>
                  <th></th>

                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <div id="Edit_modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Edit Stock</h4>
            </div>
            <?php echo $this->Form->create('EditStock'); ?>
            <div class="modal-body">
              <div class="form-horizontal">
                <div class="form-group">
                  <label class="col-sm-4 control-label">Product</label>
                  <div class="col-sm-5">
                    <input type='text' readonly=true value='' id='product_id_name' class='form-control'>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-5">
                    <?php echo $this->Form->input('id',array('type'=>'hidden','class'=>'form-control','label'=>false,'type'=>'hidden')) ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Old Quantity</label>
                  <div class="col-sm-5">
                    <input type='text' readonly=true value='' id="quanaity_value" class='form-control'>
                  </div>
                </div>
                <br>
                <div class="form-group">
                  <label class="col-sm-4 control-label final_quant add_test_label">Quantity (Pieces)</label>
                  <div class="col-sm-5">
                    <?php echo $this->Form->input('stock_Quantity_new',array('type'=>'text','min'=>'0','class'=>'form-control Edit_stock1 final_quant add_test_label number_field','label'=>false,'id'=>'QuantityNew','value'=>'0')) ?>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-4 control-label">Remark</label>
                  <div class="col-sm-5">
                    <?php echo $this->Form->input('stock_Remark',array('type'=>'text','class'=>'form-control Edit_stock1','label'=>false,'readonly',
                    'value'=>'Stock Updation')) ?>
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" id="editstock" class="btn btn-primary" data-dismiss="modal" >Edit Stock</button>
            </div>
            <?php echo $this->Form->end(); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require 'Add_Modal/Product_Add_modal.php'; ?>
<?php require 'Edit_Modal/Product_Edit_Modal.php'; ?>
<?php require 'Edit_Modal/Product_Type_Edit_Modal.php'; ?>
<?php require 'Add_Modal/Product_type_Add_modal.php'; ?>
<?php require 'Add_Modal/Brand_Add_modal.php'; ?>
<?php require 'Edit_Modal/brand_Edit_modal.php'; ?>
<?php require 'Add_Modal/Warehouse_Add_modal.php'; ?>
<?php require 'Edit_Modal/Warehouse_Edit_Modal.php'; ?>
<script type="text/javascript">
  var product_configuration_type ='<?= $Profile['Profile']['product_configuration_type']; ?>';
  if(product_configuration_type=='Retail')
  {
    $('#whole_sales_price_field').css('display','none');
  }
  if(product_configuration_type=='WholeSale')
  {
    $('#retaile_price_field').css('display','none');
  }
</script>
<script type="text/javascript">
  $(document).on('click','.delete_stock',function(){
    if(!confirm("Are you sure?"))
    {
      return false;
    }
    var id=$(this).closest('tr').find('td.stock-info span:eq(1)').text();
    var rowindex = $(this).closest('tr').index();
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/delete_stock_ajax/'+id;
    $.ajax({
      type: "post",
      url:url_address,
      dataType:'json',
      success: function(response) {
        if(response.result=='Success')
        {
          $('#table_stock_list tbody tr:eq(' + rowindex + ')').remove();
        }
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  });

  $('#table_stock_list tbody').on( 'click', '.Edit', function () {
    var id=$(this).closest('tr').find('td.stock-info span:eq(1)').text();
    var quantity=$(this).closest('tr').find('td.Quantity').text();
    var product_id=$(this).closest('tr').find('td.product_name').text();
    $('#EditStockId').val(id);
    $('#product_id_name').val(product_id);
    $('#quanaity_value').val(quantity);
    $('#Edit_modal').modal('show');
  });
  $('#editstock').click(function(){
    var table_id=$('#EditStockId').val();
    var qty=$.trim($('#QuantityNew').val());
    if(qty=='')
    {
      $('#QuantityNew').focus().trigger('click');
      return false;
    }

    var data=$('#EditStockIndexForm').serialize();
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/EditStock';
    $.ajax({
      type: "post",
      url:url_address,
      data: data,
      dataType:'json',
      success: function(response) {

        if(response.result=='Success')
        {
          $.fn.product_filter_function_datatable();
          $('#table_stock_list tbody tr').each(function(){
            var id=$(this).closest('tr').find('td span.stock_id').text();
            if(table_id==id)
            {
              $(this).closest('tr').attr('style','font-size:20px;color:green');
              $(this).closest('tr').attr('class','blue-pd flash');
              setTimeout(timer, 2000);
            }
          });
          $('#EditStockIndexForm')[0].reset();
        }
      },
      error:function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus);
      }
    });
  });
  function timer()
  {
    $('#table_stock_list tbody tr').each(function(){
      $(this).closest('tr').attr('style','');
      $(this).closest('tr').attr('class','blue-pd');

    });
    $('#product_type_id').change();
  }

  $('#showallbutton').click(function(){
    $('input[type="search"]').val('');
    $('select').val('empty').trigger('change');
    $('#warehouse_id').val('').trigger('change');
    $('#product_type_id').val('').trigger('change');
});
  $('#Print').click(function(){
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/print_report';
    $(location).attr("href", url_address);
  });
  <?php require 'Script/product_type.js' ?>
  <?php require 'Script/brand.js' ?>            
  <?php require 'Script/product.js' ?>
  <?php require 'Script/warehouse.js' ?>
  var main_total_cost=$('#main_total_cost').text();
  $('#main_total_cost').text($.fn.indian_number_format(main_total_cost));
  $('.main-sidebar').ready(function(){
    $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
  });
  var country='<?= $Profile['State']['country_id']; ?>';
  if(country==1)
  {
    $('.tax_field').attr('class','form-group');
  }
  $(document).on('change','#cost_visibility',function(){
    $.fn.cost_visiblility();
  });


  $.fn.cost_visiblility= function(){
    if($('#toggle_button_for_cost_visibility').is(":checked")){
      var url_address= "<?= $this->webroot; ?>Stock/get_total_cost";
      $.post( url_address, function( response ) {
        $('#main_total_cost').html(Math.round(response.Total));
      }, "json");
      $('.cost-field').show();
    }else{
      $('.cost-field').hide();
    }
  }

  $('#exportExcel').on('click',function(e)   { 
    e.preventDefault();
    var product_type_id=$('#product_type_id option:selected').val()?$('#product_type_id option:selected').val():'All';
    var product_id=$('#product_id option:selected').val()?$('#product_id option:selected').val():'All';
    var brand_id=$('#brand_id option:selected').val()?$('#brand_id option:selected').val():'All';
    var warehouse_id=$('#warehouse_id option:selected').val()?$('#warehouse_id option:selected').val():'All';
    if(product_type_id =='All' && product_id =='All' && brand_id =='All' && warehouse_id =='All'){
      alert('Select any filter');
      return false;
    }

    var website_url3='<?php echo $this->webroot; ?>Stock/FilterExcel/'+brand_id+'/'+product_type_id +'/'+'/'+warehouse_id+'/' +product_id;
    $(location).attr("href", website_url3);
  });

  $('#exportExcel1').on('click',function(e)   { 
    e.preventDefault();
    var product_type_id=$('#product_type_id option:selected').val()?$('#product_type_id option:selected').val():'All';
    var product_id=$('#product_id option:selected').val()?$('#product_id option:selected').val():'All';
    var brand_id=$('#brand_id option:selected').val()?$('#brand_id option:selected').val():'All';
    var warehouse_id=$('#warehouse_id option:selected').val()?$('#warehouse_id option:selected').val():'All';
    if(product_type_id =='All' && product_id =='All' && brand_id =='All' && warehouse_id =='All'){
      alert('Select any filter');
      return false;
    }

    var website_url3='<?php echo $this->webroot; ?>Stock/FilterExcelNew/'+brand_id+'/'+product_type_id +'/'+'/'+warehouse_id+'/' +product_id;
    $(location).attr("href", website_url3);
  });
</script>
<?php require 'datatable_scripts.php'; ?>
