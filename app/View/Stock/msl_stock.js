$('#MslStockOrderMslStockOrderForm').on('keyup keypress', function(e) {
  var keyCode = e.keyCode || e.which;
  if (keyCode === 13) { 
    e.preventDefault();
    return false;
  }
});

function roundToTwo(num) {
  return +(parseFloat(num).toFixed(3));
}
$('.button_click').keyup(function(e){
  var tr = $(this).parents('tr');
  var quantity = $('#quantity_simple').val();
  if (e.keyCode == 13) 
  {
    if(parseFloat(quantity)>0){
      $(tr).find('.add_to_cart_simple').trigger('click');
    }
    else
    {
      $('#quantity_simple').focus();
    }
    return false;
  }
});

$('.button_clicks').keyup(function(e){
  var tr = $(this).parents('tr');
  var avg = $('#quantity_avg').val();
  if (e.keyCode == 13) 
  {
    if(parseFloat(avg)>=0){
      $(tr).find('.add_to_cart_simple').trigger('click');
    }
    else
    {
      $('#quantity_avg').focus();
    }
    return false;
  }
});
$('.main-sidebar').ready(function(){
  $('body').attr('class','skin-black sidebar-mini sidebar-collapse');
});
$('button[type="Submit"]').click(function(){
  var total=$("#grand_total").val();
});
shortcut.add("alt+s", function() {
  $('#save_button').click();
});
$(document).on('click','.add_to_cart_simple',function(){
  event.preventDefault();
  $("#warehouse_id").change();
  var warehouse_id=$('#warehouse_id').val();
  if(!warehouse_id)
  {
    $('#warehouse_id').select2('open');
    return false;
  }
  var product_quantity=$('#quantity_simple').val();
  if(!$.isNumeric(product_quantity) || product_quantity=='' || product_quantity<=0 )
  {
    $('#quantity_simple').focus();
    return false;
  }
  var unit_id=$('#unit_simple').val();
  var product_id=$('#product_simple').val();
  if(!product_id)
  {
    $('#product_simple').select2('open');
    return false;
  }

  var quantity_avg=$('#quantity_avg').val();
  if(!$.isNumeric(quantity_avg) || quantity_avg=='' || quantity_avg<0 )
  {
    $('#quantity_avg').focus();
    return false;
  }
  var data={
    date:$('#date').val(),
    order_no:$('#order_no').val(),
    product_id:product_id,
    warehouse_id:warehouse_id,
    unit_id:unit_id,
    quantity:product_quantity,
    avg:quantity_avg,
  }
  $.post( "<?= $this->webroot ?>Stock/add_msl_order_item_ajax",data ,function( response ) { 
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    history.pushState(null, null,response.website);
    body_length=$('#product_table tbody tr').length;
    if(!body_length)
    {
      window.location.href = response.website;
    }
    table = $('#product_table').dataTable();
    table.fnDraw();
    $('#product_simple').val('').change();
    $('#product_simple').select2('open');
    $('#quantity_simple').val('0');
    $('#quantity_avg').val();
// $('#mrp_simple').val('0');
}, "json");
});
$(document).on('click','#PurchaseAdvanceBtn',function(){
  if($('#PurchaseAdvance').is(":visible")){
    $('#PurchaseAdvance').hide();
    $(this).text('Show Advanced Purchase Options')
  }else{
    $('#PurchaseAdvance').show();
    $(this).text('Hide Advanced Purchase Options')
  } 
});
$(document).on('keyup','.cart_single_item',function(){
  var quantity=$(this).val();
  var item_id=$(this).closest('tr').find('td:eq(1) input:eq(2)').val();
  var warehouse_id = $('#warehouse_id').val();
  var quantity_old = $(this).closest('tr').find('td:eq(3) input:eq(1)').val();
  var product_id = $(this).closest('tr').find('td:eq(1) input:eq(1)').val();
  var unit_id = $(this).closest('tr').find('td:eq(2) input:eq(1)').val();
  var data={
    item_id:item_id,
    product_id:product_id,
    warehouse_id:warehouse_id,
    unit_id:unit_id,
    quantity:quantity,
    quantity_old:quantity_old,
  }
  $.post( "<?= $this->webroot ?>Stock/add_msl_order_item_update",data ,function( response ) { 
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
// history.pushState(null, null,response.website);
// body_length=$('#product_table tbody tr').length;
// if(!body_length)
// {
//     window.location.href = response.website;
// }
// table = $('#product_table').dataTable();
// table.fnDraw();
// $('#product_simple').val('').change();
// $('#product_simple').select2('open');
// $('#quantity_simple').val('0');
// // $('#mrp_simple').val('0');
}, "json");

});


$('#product_simple').change(function(){
  var id=$('#product_simple').val();
  if(id!="")
  {
    var url_address= '<?php echo $this->webroot; ?>'+'Product/get_Product_msl_unit_ajax_sale/'+id;
    $.ajax({

      type: "POST",
      url:url_address,
      dataType:'json',
      success: function(data) {
//  console.log(data);
if(data.result!='Success')
{
  alert(data.result);
  return false;
}
else{
  console.log(data);
  $('#unit_simple').val(data.unit).trigger('change.select2');
  $('#quantity_avg').val(data.avg);
}

},
error:function (XMLHttpRequest, textStatus, errorThrown) {
  alert(textStatus);
}


});
  }
});

$('#unit_simple').change(function(){
  var id=$('#product_simple').val();
  if(id!="")
  {
    var quantity_mode=$('#unit_simple').val();
    var url_address= '<?php echo $this->webroot; ?>'+'Product/get_Product_msl_ajax_sale/'+id+'/'+quantity_mode;
    $.ajax({

      type: "POST",
      url:url_address,
      dataType:'json',
      success: function(data) {
//  console.log(data);
if(data.result!='Success')
{
  alert(data.result);
  return false;
}
else{
  console.log(data.avg);
  $('#quantity_avg').val(data.avg);
}

},
error:function (XMLHttpRequest, textStatus, errorThrown) {
  alert(textStatus);
}


});
  }
});