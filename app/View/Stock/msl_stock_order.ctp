<style type="text/css">
.algn_lft{
	text-align: center !important;
}
.pdng_tp{
	margin-top: 15px;
}
.clr_star {
	color: #a29da2;
}
.cart_clr {
	color: #a29da2;
}
.pgn_btm_rht{
	margin-bottom: 15px;
}
.color_of{
	color :red !important;
}
.size_of
{
	font-size:25px !important;
	padding-left: 55px !important;
}
</style>
<section class="content-header">
	<h2>MSL Configuartion</h2>
</section>
<section class="content">
	<div class="row-wrapper">
		<div class="box box-primary">
			<?php $status_flag=$this->request->data['MslStockOrder']['status_flag'];?>
			<?php $status=$this->request->data['MslStockOrder']['status']; ?>
			<?php $product_configuration_type=$Profile['Profile']['product_configuration_type']; ?>
			<?= $this->Form->create('MslStockOrder', array('url' => array('controller' => 'Stock', 'action' => 'MslStockOrder')));?>
			<div class="row">
				<div class="col-md-8 col-lg-8 col-sm-8 col-xs-12">
					<div class="form-horizontal" style="margin-top: 15px;">
						<div class="box-body">
							<div class="form-group">
								<label for="inputEmail3" class="col-md-2 control-label algn_lft">Warehouse</label>
								<div class="col-md-7">

									<?php 
									if($this->request->data['MslStockOrder']['id']){
										echo $this->Form->input('warehouse_id',array('type'=>'select','id'=>'warehouse_id','class'=>'form-control select_two_class','options'=>$Warehouse,'style'=>'width: 100%','label'=>false,'disabled'));
									}else
									{
										echo $this->Form->input('warehouse_id',array('type'=>'select','id'=>'warehouse_id','class'=>'form-control select_two_class','options'=>$Warehouse,'style'=>'width: 100%','label'=>false,'required'=>'required'));
									}
									?>

								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="col-md-4 col-lg-4 col-sm-4 col-xs-12">
					<div class="form-horizontal" style="margin-top: 15px;">
						<div class="box-body">
							<div class="form-group">
								<div class="col-md-5 col-lg-5 col-sm-5"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Order Id</label></div>
								<div class="col-md-5 col-lg-5 col-sm-5">
									<?= $this->Form->input('order_no',array('class'=>'form-control','type'=>'text','required','readonly','id'=>'order_no','label'=>false,)); ?>
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-5 col-lg-5 col-sm-5 col-xs-12"><label for="inputEmail3" class="col-md-2 control-label algn_lft" style="white-space: nowrap;">Date</label></div>
								<div class="col-md-5 col-lg-5 col-sm-5">
									<?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box-body table-responsive no-padding">
				<div class="col-md-12">
					<table class="table table-condensed table text-center table-bordered" id="product_table">
						<thead>
							<tr class="blue-bg">
								<th>Nos</th>
								<th width="50%">Product</th>
								<th>Unit</th>
								<th>Last Month AVG Qty</th>
								<th>Quantity</th>
								<th>Action</th>
							</tr>
							<?php if($status != 2) { ?>
							<tr>
								<td>0</td>
								<td>
									<?php echo $this->Form->input('product_simple',array('type'=>'select','id'=>'product_simple','class'=>'form-control select_two_class','options'=>$Product_list,'style'=>'width: 100%','empty'=>[''=>'Select'],'label'=>false)); ?>
<!--                 <?php echo $this->Form->input('product_simple',['type'=>'select','style'=>'width:100%','id'=>'product_simple','class'=>'form-control','options'=>$Product_list,'empty'=>[''=>'Select'],'label'=>false]); ?>
-->              </td>
<td>
	<?php echo $this->Form->input('unit_simple',array('type'=>'select','id'=>'unit_simple','class'=>'form-control select_two_class','options'=>$Unit,'empty'=>'Select','style'=>'width: 100%','label'=>false)); ?>
</td>
<td><input type="text" id="quantity_avg" class="simple_single_calculator form-control button_clicks" readonly="readonly" value=""></td>
<td><input type="text" id="quantity_simple" class="simple_single_calculator form-control button_click" value="0"></td>
<td><i class="fa fa-plus-circle create_icon fa-2x add_to_cart_simple"></i></td>
</tr>
<?php } ?>
</thead>
<tbody>
</tbody>
<tfoot>
	<tr>            
		<th colspan="3" style="font-size:20px; color:red;text-align:right">Total:</th>
		<th style="font-size:20px; color:red;"></th>
		<th style="font-size:20px; color:red;"></th>
	</tr>
	<tr class="blue-pddng">
		<?php if(isset($MslStockOrderItem)) : ?>
			<?php $flag=$this->request->data['MslStockOrder']['flag']; ?>
			<?php if($status==1) : ?>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<!-- <td><button id="delete_purchase" type='submit' name='data[SparePartsOrder][process]' value='delete' class="btn btn-danger" onclick="return confirm('Are you sure?')">Delete</button></td> -->
				<td></td>
				<td></td>
			<?php endif; ?>
		<?php else : ?>
			<td colspan="3"></td>
			<td><button type='submit' id="msl_order" name='data[MslStockOrder][process]' value='save' class="btn btn-success pull-right create_icon" disabled>SAVE</button></td>
		<?php endif; ?>


	</tr>
</tfoot>
</table>
</div>
</div>
<?= $this->Form->end(); ?>
</div>
</section>
<script type="text/javascript">
	<?php require('msl_stock.js'); ?>
	var status='<?= $status; ?>';
	if(status==2) $('.fa').hide();

</script>
<script type="text/javascript">
	var order_id ='<?= $this->request->data['MslStockOrder']['id'] ?>';
	if(order_id)
	{
		$('#product_table').DataTable( {
			"processing": false,
			"serverSide": true,
			"ajax": {
				"url": "<?= $this->webroot ?>Stock/get_mslstockorder_item_ajax/"+order_id,
				"type": "POST",
// data:function( d ) {
//   d.grn_order_id= grn_order_id;
// },
"dataSrc": "records",
},
"columns": [
{ "data" : "MslStockOrderItem.key" },
{ "data" : "Product.name" },
{ "data" : "MslStockOrderItem.unit" },
{ "data" : "MslStockOrderItem.avg" },
{ "data" : "MslStockOrderItem.quantity" },
{ "data" : "MslStockOrderItem.status" },
],
// "rowCallback": function( row, data ) {
//     alert('Success');
//   },
"footerCallback": function ( row, data, start, end, display ) {
	var api = this.api(), data;
	var intVal = function ( i ) {
		return typeof i === 'string' ?
		i.replace(/[\$,]/g, '')*1 :
		typeof i === 'number' ?
		i : 0;
	};
	pageTotal = api.column( 4, { page: 'current'} ).data().reduce( function (a, b) {
		value_array=b.split(' ');
		value=value_array[1].split('=');
		var b_value = value[1].split("'")[1].split("'")[0];
		return intVal(a) + intVal(b_value);
	}, 0 );

	$( api.column(4 ).footer() ).html(''+pageTotal+'');
},
"columnDefs": [
{ className: "Product", "targets": [ 1 ],"width": "70%", },
],
});
	}
	$(document).on('click','.number',function(){
		$(this).select();
	})

</script>

