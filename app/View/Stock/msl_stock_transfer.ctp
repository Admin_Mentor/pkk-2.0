<style type="text/css">
<?php include "style.css" ?>
.ad-stk{
  color: #FFF !important;
  background-color: #13689e !important;
  padding: 5px 20px !important;
  border: 0px solid #FFF !important;
  border-radius: 3px !important;
  transition: all ease .5s;
  margin-top: 20%;
}
.ad-stk:hover {
  background-color: #023a5e !important;
}
.list-arrows {
  padding-top: 100px;
}
.list-arrows button {
  margin-bottom: 20px;
}
.mr-tp-30 {
  margin-top: 30px;
}
</style>
<!-- <aside class="main-sidebar">    
<section class="sidebar" style="height: auto;">         
<ul class="sidebar-menu">       
<li class="active treeview">
<a href="#">
<i class="fa fa-share"></i> <span>Stock</span>
<i class="fa fa-angle-left pull-right"></i>
</a>
<ul class="treeview-menu">
<li><a href="<?php echo $this->webroot; ?>Stock/Index"><i class="fa fa-circle-o"></i> Stock Management </a></li>
<li><a href="<?php echo $this->webroot; ?>Stock/StockTransferList"><i class="fa fa-circle-o"></i> Stock Transfer </a></li>
<li><a href="<?php echo $this->webroot; ?>Stock/DamagedStock"><i class="fa fa-circle-o"></i> Damaged Stock </a></li>
</ul>
</li>
</ul>
</section>        
</aside> -->
<section class="content-header">
  <h1> MSL Stock Transfer </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header"> 
    </div>
    <div class="box-body"> 
      <?= $this->Form->create('MslStockTransfer', array('url' => array('controller' => 'Stock', 'action' => 'MslStockTransfer')));?>
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="row">
            <div class="form-group col-md-10">
              <?php echo $this->Form->input('warehouse_from',array('type'=>'select','empty'=>'Select Warehouse','value'=>'','options'=>$Warehouse,'class'=>'form-control select2 search_class','label'=>'From','id'=>'warehouse_from')) ?>                   
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-10">
              <?php echo $this->Form->input('warehouse_to',array('type'=>'select','empty'=>'Select Warehouse','value'=>'','options'=>$Warehouse,'class'=>'form-control select2 search_class','label'=>'To','id'=>'warehouse_to')) ?>                
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="form-group col-md-12">
            <?= $this->Form->input('remarks', array('type' => 'textarea','class'=>"form-control",'rows'=>4)); ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-md-offset-3">
          <div class="row">
            <div class="form-group col-md-10">
              <?= $this->Form->input('transfer_no',array('class'=>'form-control','label'=>'Transfer ID','type'=>'text','required','id'=>'transfer_no',)); ?>                 
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-10">
              <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <div class="box-body table-responsive no-padding boder">
            <table class="table table-hover" id="stock_transfer_tbl" >                    
              <thead>
                <tr  class="blue-bg">
                  <th>Slno</th>
                  <th style="width:20%">Product</th>
                  <th>Unit</th>
                  <th>From Quantity</th>
                  <th>To Quantity</th>
                  <th>MSL Quantity</th>
                  <th>Quantity To Move</th>
                  <th>Balance Quantity</th>
                  <th></th>
                </tr>
                <tr style="display:none;">
                  <td></td>
                  <td>
                    <?php echo $this->Form->input('product_id',array('type'=>'select', 'empty' =>'Select Product','class'=>'form-control select2 search_class','label'=>false,'id'=>'product_id','style'=>'width:100%')) ?> 
                  </td>
                  <td>
                    <?php echo $this->Form->input('product_unit',array('type'=>'select', 'empty' =>'Select Unit','class'=>'form-control select2','label'=>false,'id'=>'product_unit','options'=>$Unit)) ?> 
                  </td>
                  <td><input class="form-control transfer-field" id="current_quantity" type="text" readonly="readonly">
                    <td><input class="form-control transfer-field" id="to_quantity" type="text" readonly="readonly">
                      <span id="hidden_unit_level" style="display:none;"></span>
                      <span id="hidden_quantity" style="display:none;"></span>
                      <span id="hidden_no_of_pieces_per_unit" style="display:none;"></span>
                    </td>
                    <td><input class="form-control transfer-field" id="msl_quantity" type="text" readonly="readonly"></td>
                    <td><input class="form-control transfer-field" id="move_quantity" type="text"><span id="hidden_move_quantity" style="display: none;"></span></td>
                    <td><input class="form-control transfer-field" id="balance_quantity" type="hidden" readonly="readonly" ><span id="hidden_balance_quantity"></span></td>
                    <td><i id="add_transfer_product" class="fa fa-plus-circle fa-2x fnt-awsm-btn"></i></td>                         
                  </tr>
                </thead>
                <tbody>
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
          </div>
        </div>
        <br/>
        <div class="row">
          <div class="modal-footer">
            <button type="submit" id='save_button' disabled class="btn btn-success">Save</button>
            <!-- <button type="button" class="btn btn-default print">Print</button> -->
          </div>
        </div>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </section>
  <script type="text/javascript">
    $.fn.button_disable=function(){
      var length=$('#stock_transfer_tbl tbody tr').length;
      if(length>0)
      {
        $('button[type="Submit"]').attr('disabled',false);
      }
      else
      {
        $('button[type="Submit"]').attr('disabled',true);
      }


    };
    $.fn.show_alert = function(flash)
    {
      $.alert(flash, {title:' ',type: 'info',position: ['top-right', [60, 600]],});
    }
    shortcut.add("alt+s", function() {
      $.fn.button_disable();
      $('#save_button').click();
    });
    $('#MslStockTransferMslStockTransferForm').on('keyup keypress', function(e) {
      var keyCode = e.keyCode || e.which;
      if (keyCode === 13) { 
        e.preventDefault();
        return false;
      }
    });
    $('.transfer-field').keyup(function(e){

      if (e.keyCode == 13) 
      {
        $('#add_transfer_product').trigger('click');
        $('#product').focus();
        return false;
      }});
    $.fn.product_flitering=function(){
      $('#brand_in_display').val('');
      var warehouse_id=$('#warehouse_from').val();
      var product_type_id='';
      var brand_name_id='';
      var modal='';
      var data={
        warehouse_id:warehouse_id,
        product_type_id:product_type_id,
        brand_name_id:brand_name_id,
        modal:modal,
      };
      var url_address= '<?php echo $this->webroot; ?>'+'Stock/product_list_get_ajax_search';
      $.ajax({
        type: "post",
        url:url_address,
        data: data,
        dataType:'json',
        success: function(response) {              
          $('#product_id').html(response.option);
          $('#product_id').val('');
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
          alert(textStatus);
        }
      });
    }
    $(document).on('change','#warehouse_from',function(){
      $.fn.product_flitering();
    })

    $(document).on('change','#warehouse_from,#warehouse_to',function(){
      if($('#warehouse_from').val() == $('#warehouse_to').val())
      {
        $.fn.show_alert('Select Diffrent Warehouse');
        $('#warehouse_to').val('').trigger('change.select2');
        return false;
      }
      $('#stock_transfer_tbl tbody tr').closest('tr.product_tr').html('');
    });
    $(document).on('change','#warehouse_to',function(){
      var warehouse_from=$('#warehouse_from').val();
      var warehouse_to=$('#warehouse_to').val();
      var data={
        warehouse_from:warehouse_from,
        warehouse_to:warehouse_to,
      };
      var url_address= '<?php echo $this->webroot; ?>'+'Stock/get_msl_product';
      $.post( url_address,data, function( response ) {
        $('#stock_transfer_tbl tbody tr').closest('tr.product_tr').html('');
        var check=0;
        $.each(response.MslStockOrderItem,function(key,value){
// $('#stock_transfer_tbl tbody').append('<tr class="product_tr product_'+product_id+'">\
var stock_quantity=value.Stock.quantity;

var to_quantity_first=value.to_stock.quantity;
var msl_quantity_first=value.MslStockOrderItem.quantity;
var move_quantity_first=msl_quantity_first-to_quantity_first;
if(value.MslStockOrderItem.unit == 1){
  var total_quantity=Math.floor(stock_quantity);
  var msl_quantity=Math.floor(msl_quantity_first);
  var to_quantity=Math.floor(to_quantity_first);
  var move_quantity=msl_quantity-to_quantity;
  var greater_move_quantity=Math.floor(stock_quantity);
  var unit_id="Pieces";
      var piece_move_quantity=move_quantity;

}else{
  if(value.MslStockOrderItem.unit ==2){
    var unit_id="Cases";
    var quantity=stock_quantity/(value.Product.no_of_piece_per_unit);
    var quantity_to=to_quantity_first/(value.Product.no_of_piece_per_unit);
    var quantity_msl=msl_quantity_first/(value.Product.no_of_piece_per_unit);
    var quantity_move=move_quantity_first/(value.Product.no_of_piece_per_unit);
    if(quantity < 1){
      var fraction=stock_quantity;
      var fraction_msl=msl_quantity_first;
      var fraction_to=to_quantity_first;
      var fraction_move=move_quantity_first;
      var whole=0;var whole_to=0;var whole_msl=0;var whole_move=0;
    }else{
      var whole = Math.floor(quantity); 
      var whole_msl = Math.floor(quantity_msl); 
      var whole_to = Math.floor(quantity_to); 
      var whole_move = Math.floor(quantity_move); 

      var fraction = stock_quantity%(value.Product.no_of_piece_per_unit); 
      var fraction_msl = msl_quantity_first%(value.Product.no_of_piece_per_unit); 
      var fraction_to = to_quantity_first%(value.Product.no_of_piece_per_unit); 
      var fraction_move = move_quantity_first%(value.Product.no_of_piece_per_unit); 

    }
    var total_quantity=whole+'/'+Math.floor(fraction);
    var msl_quantity=whole_msl+'/'+Math.floor(fraction_msl);
    var to_quantity=whole_to+'/'+Math.floor(fraction_to);
    var move_quantity=whole_msl-whole_to;
     var greater_move_quantity=whole;
         var piece_move_quantity=move_quantity*value.Product.no_of_piece_per_unit;

//var move_quantity=move_quantity_first/(value.Product.no_of_piece_per_unit);


}
}

var old_quantity=total_quantity;
var current_quantity =  total_quantity? parseInt(total_quantity):0;
var colour="";
if(parseInt(current_quantity)  >=  parseInt(move_quantity)){
  if(value.MslStockOrderItem.unit == 2){
    var actual_move_quantity=move_quantity*value.Product.no_of_piece_per_unit;
    var str=old_quantity.split("/");
// var str1=total_quantity.split("/");
var balance_quantity=parseInt(current_quantity- move_quantity)+'/'+(str[1]);
}else{
  var actual_move_quantity=move_quantity;
  var balance_quantity=(parseInt(current_quantity- move_quantity));
}
var colour="";
}
else{

  var move_quantity=greater_move_quantity;

  var colour="background-color:#FF4747";
  var balance_quantity=0;
}
if(parseInt(to_quantity)  >  parseInt(msl_quantity))
{
   //var colour="background-color:#FF4747";
  var move_quantity=0;var balance_quantity=0;
}
else
{
  //var colour="";
}

var remove_button='<i class="fa fa-minus-circle fa-2x fnt-awsm-btn remove_button"></i>';
var product_field='<input type="hidden" name="data[MslStockTransfer][product][]" value="'+value.Product.id+'" class="productsrow"><input type="text"  class="form-control" readonly value="'+value.Product.name+'" ><input type="hidden" class="form-control" readonly value="'+value.Product.no_of_piece_per_unit+'">'+'';
var move_quantity_field='<input type="hidden" class="form-control" name="data[MslStockTransfer][quantity][]" value="'+piece_move_quantity+'"><input type="text" class="form-control number move_quantity_class" id="move_quantity"  value="'+move_quantity+'">'+'';
var current_quantity_field='<input type="text" class="form-control" readonly value="'+total_quantity+'">'+'';
var to_quantity='<input type="text" class="form-control" readonly value="'+to_quantity+'">'+'';
var msl_quantity='<input type="text" class="form-control" readonly value="'+msl_quantity+'">'+'';
var balance_quantity_field='<input type="text" class="form-control" id="balance_quantity" readonly value="'+balance_quantity+'">'+'';
var unit='<input type="hidden" name="data[MslStockTransfer][unit][]" value="'+value.MslStockOrderItem.unit+'"><input type="text" class="form-control" readonly value="'+unit_id+'">'+'';
var sl=key+1;
$('#stock_transfer_tbl tbody').append('<tr class="product_tr" style="'+colour+'">\
  <td>'+sl+'</td>\
  <td>'+product_field+'</td>\
  <td>'+unit+'</td>\
  <td>'+current_quantity_field+'</td>\
  <td>'+to_quantity+'</td>\
  <td>'+msl_quantity+'</td>\
  <td>'+move_quantity_field+'</td>\
  <td>'+balance_quantity_field+'</td>\
  <td>'+remove_button+'</td>\
  </tr>');     
}); 

}, "json");

})

$(document).on('keyup','#move_quantity',function(){
  var move_quantity=$(this).val();
  var total_quantity=$(this).closest('tr').find('td:eq(3) input:eq(0)').val();
  var old_quantity=$(this).closest('tr').find('td:eq(3) input:eq(0)').val();
  var current_quantity =  total_quantity? parseInt(total_quantity):0;
  var unit=$(this).closest('tr').find('td:eq(2) input:eq(0)').val();
  var unit=$(this).closest('tr').find('td:eq(2) input:eq(0)').val();
  var no_of_piece_per_unit=$(this).closest('tr').find('td:eq(1) input:eq(2)').val();
  if(parseInt(current_quantity)  >=  parseInt(move_quantity)){
    if(unit== 2){
      var piece_quantity=move_quantity*no_of_piece_per_unit;
      var actual_move_quantity=move_quantity*no_of_piece_per_unit;
      var str=old_quantity.split("/");
      var actual_move_quantity=parseInt(current_quantity- move_quantity)+'/'+(str[1]);
      $(this).closest('tr').find('td:eq(7) input:eq(0)').val(actual_move_quantity);
       $(this).closest('tr').find('td:eq(6) input:eq(0)').val(piece_quantity);
    }else{
      var actual_move_quantity=parseInt(current_quantity- move_quantity);
      $(this).closest('tr').find('td:eq(7) input:eq(0)').val(actual_move_quantity);
      $(this).closest('tr').find('td:eq(6) input:eq(0)').val(move_quantity);

    }
  }
  else{

    $(this).closest('tr').find('td:eq(6) input:eq(1)').val(0);
    $(this).closest('tr').find('td:eq(7) input:eq(0)').val(0);

  }
});

$(document).on('click','.remove_button',function(){
  $(this).closest('tr').remove();
  $('#save_button').attr('disabled',true);
});

$(document).on('click','body',function(){

  $.fn.button_disable();
})
$('#save_button').click(function(){
  var check1=0;
  var product_name='';
  $("#stock_transfer_tbl tbody tr").each(function () {
var quantity= $(this).closest('tr').find('td:eq(6) input:eq(1)').val();
  if(quantity<=0)
  {
    check1=1;
  }
   });
  if(check1)
  {
  //    $("form").submit(function(e){
  //     alert('Zero or negative quantity not allowed1');
  //     e.preventDefault(e);
  // });
  alert('Zero or negative quantity not allowed');
 
    return false;
  }
  else
  {
   // $( "#save_buttton" ).submit();
  }
});
</script>
