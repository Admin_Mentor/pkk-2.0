<style type="text/css">
.algn_lft{
  text-align: center !important;
}

</style>
<section class="content-header">
  <h1>New Issue <a href="<?php echo $this->webroot ?>Stock/StationaryIssueList"><button class='btn btn-success pull-right btn_bdr_radious'>Issue List</button></a></h1>
</section>
<section class="content">
  <div class="row-wrapper">
  <div class="box box-primary">
    <?= $this->Form->create('StationaryIssue', array('url' => array('controller' => 'Stock', 'action' => 'StationaryIssue')));?>
    <div class="row">
      <div class="col-md-8">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-md-2 control-label">Warehouse</label>
              <div class="col-md-5">
                <?php echo $this->Form->input('warehouse_id',array('type'=>'select','id'=>'warehouse_id','class'=>'form-control select_two_class','style'=>'width: 100%','options'=>$Warehouse,'label'=>false,'required'=>'required')); ?>
              </div>
            </div>
            <br>
            <div class="form-group">
              <label for="inputEmail3" class="col-md-2 control-label">Date</label>
              <div class="col-md-5">
                <?php echo $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','label'=>false,'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-4">
        <div class="form-horizontal" style="margin-top: 15px;">
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-md-2 control-label">Remarks</label>
              <div class="col-md-8">
                <?php echo $this->Form->input('remarks',array('type'=>'textarea','id'=>'remarks','class'=>'form-control','label'=>false,'rows'=>2)); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    <div class="box-body table-responsive no-padding">
      <div class="col-md-12">
        <table class="table table-condensed table text-center table-bordered" id="product_table">
          <thead>
            <tr class="blue-bg">
              <th>Nos</th>
              <th width="40%">Product</th>
              <?php if(!isset($this->request->data['StationaryIssue']['id'])){?>
               <th>Stock Quantity</th>
               <?php }?>
              <th>Quantity</th>
              <?php if(!isset($this->request->data['StationaryIssue']['id'])){?>
              <th>Action</th>
              <?php } ?>
            </tr>
            <?php if(!isset($this->request->data['StationaryIssue']['id'])){?>
            <tr>
              <td>#</td>
              <td>
                <?php echo $this->Form->input('product_simple',['type'=>'select','style'=>'width:100%','id'=>'product_simple','class'=>'form-control select_two_class','empty'=>[''=>'Select'],'label'=>false,'options'=>$Product]); ?>
              </td>
              <td><input type="text" id="stock_quantity" class=" form-control " value="0" readonly></td>
              <td><input type="text" id="quantity_simple" class=" form-control " value="">
              <td><i class="fa fa-plus-circle fa-2x add_to_cart_simple"></i></td>
            </tr>
            <?php } ?>
          </thead>
          <tbody>
             <?php if(isset($StationaryIssueItem)) : foreach ($StationaryIssueItem as $key => $value) :?>
                    <tr>
                      <td><?= $key+1; ?></td>
                      <td>
                        <?= $this->Form->input('product_id',array('class'=>'form-control','label'=>false,'id'=>'','value'=>$value['Product']['name'],'type'=>'text','readonly')) ?> </td>
                        <td><input class="form-control " id="" type="text" readonly="readonly" value=<?= floatval($value['StationaryIssueItem']['quantity']);?>></td>
                      </tr>
                    <?php endforeach; endif; ?>

          </tbody>
          <tfoot>
            <?php if(!isset($this->request->data['StationaryIssue']['id'])){?>
            <tr class="blue-pddng">
                <td colspan="2"></td>
                <td><button type='submit' id="" name='data[StationaryIssue][process]' value='save' class="btn btn-success pull-right create_icon" disabled>SAVE</button></td>
                <td></td>
            </tr>
            <?php } ?>
          </tfoot>
        </table>
      </div>
    </div>
    <?= $this->Form->end(); ?>
  </div>
      </div>
</section>
<script type="text/javascript">
$(document).on('change','#product_simple,#warehouse_id',function(){
  var warehouse_id=$('#warehouse_id').val();
    if(!warehouse_id)
    {
      $('#warehouse_id').select2('open');
      $('#product_simple').val('').trigger('change.select2');
        return false;
    }
    var id=$('#product_simple').val();
    if(!id)
    {
        return false;
    }
    var data={product_id:id,warehouse_id:warehouse_id};
    var url_address= '<?php echo $this->webroot; ?>'+'Stock/get_warehousewise_Product_ajax/';
    $.ajax({
        type: "POST",
        url:url_address,
        data:data,
        dataType:'json',
        success: function(data) {
            if(data.result!='Success')
            {
                return false;
            }
            $('#stock_quantity').val(data.quantity);
            $('#quantity_simple').focus();
        },
        error:function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus);
        }
    });
});
$.fn.button_disable=function(){
    var length=$('#product_table tbody tr').length;
    if(length>0)
    {
        $('button[type="Submit"]').attr('disabled',false);
    }
    else
    {
        $('button[type="Submit"]').attr('disabled',true);
    }
};
$(".add_to_cart_simple").click(function(){
    event.preventDefault();
    var ProductExist = 0;
    var product_id=$('#product_simple').val();
    if(!product_id){
        $('#product_simple').select2('open');
        return false;
    }
    var product_quantity=$('#quantity_simple').val();
    var stock_quantity=$('#stock_quantity').val();
    if(!$.isNumeric(product_quantity) || product_quantity=='' || product_quantity<=0 )
    {
        $('#quantity_simple').focus();
        return false;
    }
    if(parseFloat(product_quantity)>parseFloat(stock_quantity ))
    {
        $('#quantity_simple').focus();
    $ .alert("Insuffcient Stock", {title:'Flash Message',type: 'info',position: ['top-right', [60, 0]],});
         return false;
    }
    var i=0;
    $("#product_table tbody tr").each(function () {
        i=i+1;
        var productId = $(this).closest('tr').find('td input.productsrow').val();
        var table_product_unit = $(this).closest('tr').find('td:eq(1) input:eq(0)').val();
        if (product_id== productId){
            ProductExist = 1;
            return false;
        }
    });
    if(ProductExist)
    {
        alert("This product and warehouse already in cart");
        $('#product').select_two_class('open');
        return false;
    }
    var product_text=$('#product_simple option:selected').text();
      var length=$('#product_table tbody tr').length;
                length=length+1;
    $('#product_table tbody').append('<tr class="blue-pddng">\
        <td>'+length+'</td>\
        <td>\
        <input class="productlist productsrow" hidden name="data[StationaryIssue][product_id][]" value="'+product_id+'">\
        <input class="form-control" value="'+product_text+'" readonly>\
        </td>\
        <td colspan="2" class="qty"><input class="form-control cart_quantity" readonly name="data[StationaryIssue][quantity][]" value="'+product_quantity+'" ></td>\
        <td><i class="fa fa-minus-circle fa-2x ad-mar remove_tr"></i></td>\
        </tr>');    
                    length=length+1;
    $("#quantity_simple").val('');
    $('#product_simple').val('').trigger('change.select2');
    $.fn.button_disable();
});
$(document).on('keypress','#quantity_simple',function(e){
     if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57 || e.which==47)) {
        return false;
    }
   });
$(document).on('click','.remove_tr',function(){
  $(this).closest('tr').remove();
$.fn.button_disable();
});
</script>
