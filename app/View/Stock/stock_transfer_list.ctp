<section class="content-header">
  <h1> Stock Transfer List
   <a  href="<?php echo $this->webroot ?>Stock/StockTransfer"><input style="margin-left:20px"type="button" class="btn btn-success save pull-right" value="New Transfer"></input></a></h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header"> 
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="box-body table-responsive no-padding">
            <!-- <a  href="<?php echo $this->webroot ?>Stock/StockTransfer"><input style="margin-left:20px"type="button" class="btn btn-success save pull-right" value="New Transfer"></input></a> -->
            <!--  <a  href="<?php echo $this->webroot ?>Stock/MslStockTransfer"><input type="button" class="btn btn-success save pull-right" value="New MSL Transfer"></input></a> -->
          <!--   <br>
            <br> -->
            <table class="table table-hover datatable boder table-bordered">
              <thead>
                <tr class="blue-bg">
                  <th>Transfer ID</th>
                  <th>Date</th>
                   <th>Time</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Description</th>
                  <th>Actions</th>
                  <!-- <th></th> -->
                </tr>
              </thead>
              <tbody>
              <?php foreach ($StockTransfer as $key => $value): ?>
                <tr>
                  <td>
                    <span style='display: none' class='id'><?= $value['id']; ?></span>
                    <span><?= $value['transfer_no']; ?></span>
                  </td>
                  <td><?= date('d-m-Y',strtotime($value['date'])); ?></td>
                  <td><?= date('h:i a',strtotime($value['created_at'])); ?></td>
                  <td><?= $value['WarehouseFrom']; ?></td>
                  <td><?= $value['WarehouseTo']; ?></td>
                  <td><?= $value['remarks']; ?></td>
                  <td><span></span><a target="_blank" href="<?= $this->webroot; ?>Stock/ViewStockTransfer/<?= $value['id']  ?>"><i class="fa fa-2x fa-eye" aria-hidden="true"></i></a>
                   <span></span><a target="_blank" href="<?= $this->webroot; ?>Print/stockfpdf/<?= $value['id']  ?>"><i class="fa fa-2x fa-print" aria-hidden="true"></i></a></td>
                </tr>
              <?php endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
  <script type="text/javascript">
    $(document).on('click','table tbody tr',function(){
      var id=$(this).closest('tr').find('span.id').html();
      var url = "<?= $this->webroot; ?>Stock/StockTransfer/"+id;
      // $(location).attr("href", url);
    });
  </script>