<section class="content-header">
  <h1> V2B Stock Transfer List
    <!-- <a href="<?php echo $this->webroot ?>Stock/VanBranchStockTransfer"><button class='btn btn-success pull-right'>New V2B Stock Transfer</button></a> -->
  </h1>
</section>
<section class="content">
  <div class="box box-primary">
   <div class="box-header">
     <div class="col-sm-12">
      <div class="form-group">
        <div class="col-md-2 col-sm-2 col-lg-2">
            <?= $this->Form->input('status',array('type'=>'select','options'=>[1=>'Pending',2=>'Approved'],'class'=>'form-control select_two_class','style'=>'width:100%','id'=>'status',)) ?>
          </div>
        <div class="col-md-2 col-sm-2 col-lg-2">
         <?php echo $this->Form->input('from_date',array(
          'type'=>'text',
          'id'=>'from_date',
          'value'=>$from,
          'class'=>'form-control cls_label_all date_field date_picker
          datepicker',
          'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
          )); ?>
        </div>
        <div class="col-md-2 col-sm-2 col-lg-2">
          <?php echo $this->Form->input('to_date',array(
            'type'=>'text',
            'id'=>'to_date',
            'value'=>$to,
            'class'=>'form-control cls_label_all date_field date_picker
            datepicker',
            'data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',
            )); ?>
          </div>
          <div class="col-md-1 col-sm-1 col-lg-1" style="margin-top: 4px;"><br>
            <button class='btn btn-success' id='get_button'>Get</button>
          </div>
        </div>
      </div>
    </div>
    <div class="box-body">
      <div class="row">
        <div class="col-md-12">
          <div class="box-body table">
            <table class="boder table table-condensed table boder table-bordered" id="table_data" data-order='[[ 1, "desc" ]]'>
              <thead>
                <tr class="blue-bg">
                  <th>Transfer ID</th>
                  <th>Date</th>
                  <th>From</th>
                  <th>To</th>
                  <th>Description</th>
                  <th>Status</th>
                  <th width="10%">Actions</th>
                  <!--              <th></th>   -->
                </tr>
              </thead>
              <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<script type="text/javascript">
  $('#table_data').DataTable( {
    
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Stock/get_V2B_ajax",
      "type": "POST",
      data:function( d ) {
         d.status=$('#status').val();
        d.from_date= $('#from_date').val();
        d.to_date= $('#to_date').val();
      },
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "StockTransfer.transfer_no" },
    { "data" : "StockTransfer.date" },
    { "data" : "WarehouseFrom.name" },
    { "data" : "WarehouseTo.name" },
    { "data" : "StockTransfer.remarks" },
    { "data" : "StockTransfer.status" },
    { "data" : "StockTransfer.action",'class':"text-center" },
    ],
    "createdRow": function( row, data, dataIndex){
    },    
    "columnDefs": [
        {"targets": [ 5 ],"orderable": false ,},
        {"targets": [ 6 ],"orderable": false ,},
    ],
    "rowCallback": function (row, data, index) {},  
  });
 
  $('#get_button').click(function(){
    table = $('#table_data').dataTable();
    table.fnDraw();
  });
 
</script>