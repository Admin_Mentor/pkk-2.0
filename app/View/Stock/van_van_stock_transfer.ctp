<style type="text/css">
  <?php include "style.css" ?>
  .ad-stk{
    color: #FFF !important;
    background-color: #13689e !important;
    padding: 5px 20px !important;
    border: 0px solid #FFF !important;
    border-radius: 3px !important;
    transition: all ease .5s;
    margin-top: 20%;
  }
  .ad-stk:hover {
    background-color: #023a5e !important;
  }
  .list-arrows {
    padding-top: 100px;
  }
  .list-arrows button {
    margin-bottom: 20px;
  }
  .mr-tp-30 {
    margin-top: 30px;
  }
  .disableddiv {
    pointer-events: none;
    opacity: 0.9;
  }
</style>
<section class="content-header">
  <h1> New V2V Stock Transfer
    <a href="<?= $this->webroot ?>Stock/VanVanTransferList"><button class='btn btn-primary pull-right'>V2V Stock Transfer List</button></a>
  </h1>
  <section class="content">
    <div class="box box-primary">
      <div class="box-header"> 
      </div>
      <div class="box-body"> 
        <?= $this->Form->create('StockTransfer', array('id'=>'StockTransfer','url' => array('controller' => 'Stock', 'action' => 'VanBranchStockTransfer')));?>
        <div class="row">
          <div class="col-md-3 col-sm-3">
            <div class="row">
              <div class="form-group col-md-10" id="fromwarehouse">
                 <?= $this->Form->input('warehouse_from_name',array('type'=>'text','class'=>'form-control','readonly','label'=>'From','id'=>'warehouse_from_name')) ?>  <?= $this->Form->input('warehouse_from',array('type'=>'hidden','class'=>'form-control','readonly','label'=>false,'id'=>'warehouse_from')) ?>                   
                  
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-10">
                <?= $this->Form->input('warehouse_to_name',array('type'=>'text','class'=>'form-control','readonly','label'=>'To','id'=>'warehouse_to_name')) ?> <?= $this->Form->input('warehouse_to',array('type'=>'hidden','class'=>'form-control','readonly','label'=>false,'id'=>'warehouse_to')) ?>                   
            
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-3">
            <div class="row">
              <div class="form-group col-md-10">
              <?= $this->Form->input('transfer_no',array('class'=>'form-control','label'=>'Transfer ID','type'=>'text','required','id'=>'transfer_no','readonly')); ?>                 
              </div>
            </div>
            <div class="row">
              <div class="form-group col-md-10">
              <?= $this->Form->input('date',array('type'=>'text','class'=>'form-control pull-right date_picker datepicker','id'=>'date','required','data-inputmask'=>"'alias': 'dd-mm-yyyy'",'data-mask'=>'data-mask',)); ?>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-sm-6">
            <div class="form-group col-md-12">
              <?= $this->Form->input('remarks', array('type' => 'textarea','class'=>"form-control",'rows'=>4,'style'=>"resize:none;margin: 0px -9.91667px 0px 0px; width: 372px; height: 111px;",'required'=>false)); ?>
            </div>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-md-12">
            <div class="box-body table-responsive no-padding boder">
              <table class="table table-hover" id="stock_transfer_tbl" >                    
                <thead>
                  <tr  class="blue-bg">
                    <th>Slno</th>
                    <th>Product</th>
                    <th>Quantity</th>
                  </tr>
                </thead>
                <tbody>
                  <?php if(isset($StockTransferItem)) : foreach ($StockTransferItem as $key => $value) :?>
                    <tr>
                    <td><?= $key+1; ?></td>
                    <td><input class="productsrow" type="hidden" name='data[StockTransfer][product][]' value="<?=$value['StockTransferItem']['product_id']?>" >
                      <input  type="hidden" name='data[StockTransfer][item_id][]' value="<?=$value['StockTransferItem']['id']?>" >
                      <input class="form-control" readonly type="text" name='data[StockTransfer][product_name][]' value="<?=$value['StockTransferItem']['product_name']?>" ></td>
                   </tr>
                <?php endforeach; endif; ?>
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <br/>
      <?= $this->Form->end(); ?>
    </div>
  </div>
</section>
<script type="text/javascript">
</script>