<style type="text/css">
  <?php include "style.css" ?>
  .ad-stk{
    color: #FFF !important;
    background-color: #13689e !important;
    padding: 5px 20px !important;
    border: 0px solid #FFF !important;
    border-radius: 3px !important;
    transition: all ease .5s;
    margin-top: 20%;
  }
  .ad-stk:hover {
    background-color: #023a5e !important;
  }
  .list-arrows {
    padding-top: 100px;
  }
  .list-arrows button {
    margin-bottom: 20px;
  }
  .mr-tp-30 {
    margin-top: 30px;
  }
</style>

<section class="content-header">
  <h1> Stock Transfer </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header"> 
    </div>
    <div class="box-body"> 
      <?= $this->Form->create('StockTransfer', array('url' => array('controller' => 'Stock', 'action' => 'StockTransfer')));?>
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <div class="row">
            <div class="form-group col-md-10">
            <label>From</label><br>
             <?php //pr($StockTransfer['StockTransfer']['from_warehouse']);exit; ?>
              <?php echo $StockTransfer['StockTransfer']['from_warehouse']; ?>                   
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-10">
            <label>To</label><br>
              <?php echo $StockTransfer['StockTransfer']['to_warehouse']; ?>            
            </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-6">
          <div class="form-group col-md-12">
          <label>Remarks</label><br>
              <?php echo $StockTransfer['StockTransfer']['remarks']; ?> 
           
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-md-offset-3">
          <div class="row">
            <div class="form-group col-md-10">
             <label>Transfer ID</label><br>
              <?php echo $StockTransfer['StockTransfer']['transfer_no']; ?> 
                              
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-10">
            
             <label>Date</label><br>
              <?php echo date('d-m-Y',strtotime($StockTransfer['StockTransfer']['date'])); ?> 
             
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-md-12">
          <div class="box-body table-responsive no-padding boder">
            <table class="table table-hover" id="stock_transfer_tbl" >                    
              <thead>
                <tr  class="blue-bg">
                  
                  <th>Product</th>
                  <th>Unit</th>
                  <th>Quantity</th>
                 
                  <th></th>
                </tr>
                
                
              </thead>
              <tbody>
              <?php foreach ($StockTransferitems as $key => $value) {
                  if($value['Unit']['level'] == 2){
                    $quantity=$value['StockTransferItem']['quantity']/$value['Product']['no_of_piece_per_unit'];
                    $whole = floor($quantity);      
                    $fraction = $value['StockTransferItem']['quantity']%$value['Product']['no_of_piece_per_unit']; 
                    $pieces=number_format($fraction);
                    $cases=$whole;
                    $total_quantity=$cases.'/'.$pieces;
                  }else{
                    $quantity=$value['StockTransferItem']['quantity'];
                    $total_quantity=number_format($quantity);
                  }
                 ?>
                 <tr>
                 <td><?= $value['Product']['name'];?></td>
                 <td><?= $value['Unit']['name'];?></td>
                 <td><?= $total_quantity;?></td>
                 </tr>
                 <?php
                }
                ?>
              </tbody>
              <tfoot>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <br/>
      <div class="row" style="display: none;">
        <div class="modal-footer">
          <button type="submit" id='save_button' disabled class="btn btn-success">Save</button>
          <!-- <button type="button" class="btn btn-default print">Print</button> -->
        </div>
      </div>
      <?= $this->Form->end(); ?>
    </div>
  </div>
</section>
