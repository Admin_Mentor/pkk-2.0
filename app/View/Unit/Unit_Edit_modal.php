<div id="Unit_Edit_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Unit</h4>
      </div>
      <?php echo $this->Form->create('UnitEdit',array('id'=>'Unit_Edit_Form')); ?>
      <div class="modal-body">
        <div class="form-horizontal">
          <div class="form-group">
            <div class="col-sm-10">
              <?php echo $this->Form->input('name',array('class'=>'form-control toUpperCase','label'=>'Name')); ?>
              <?php echo $this->Form->input('id',array('type'=>'hidden')) ?>
             
            </div>
          </div>
         
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id='edit_Unit'>Edit</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>