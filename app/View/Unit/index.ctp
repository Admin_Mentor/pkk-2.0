
<style type="text/css"> 
  .blue-bg{
    color: white;
    background-color: #0f4361;
  }
  .border_cover_{
    border: 1px solid #c7c7c7;
    margin: 1px;
  }
  .btn_save_success {
    border: none;
    color: white;
    padding-top: 7px !important;
    padding-bottom: 7px !important;
    background-color: #065b73;
    letter-spacing: 0.6px;
    padding-left: 21px;
    padding-right: 21px;
  }
  .btn_save_success:hover{
    color: #ffffff;
    transition: ease all 0.9s;
    background-color: #077290;
  }
  .btn_save_success:active{
    color: #ffffff;
  }
  .btn_save_success:focus{
    color: #ffffff;
  }
  .icn_lft__{
    padding-left: 10px;
  }
  .form_2{
    width: 65% !important;
  }
</style>
<section class="content-header">
  <h1> Unit
<div class="col-sm-1 pull-right" style="margin-right: 20px">
             <a href="#"><button class='btn btn-success' data-toggle="modal"  data-target="#Unit_Add_modal">Add Unit</button></a>
            </div>
  </h1>
</section>
<section class="row"> 
  <section class="content"> 
    <div class="box">
      <div class="row">
      <div class="box-body table-responsive">
        <!-- <div style="margin-left:5%;">
          <a href="#"><button class='btn btn-success' data-toggle="modal"  data-target="#Unit_Add_modal">Add Unit</button></a>
        </div> -->
        <div class="col-md-8 col-lg-8 col-sm-8" hidden>
          
            <table class="table border_cover_ table-bordered" id="unit_conversion_table">
              <thead>
                <tr class="blue-bg">
                  <th style="width: 16%;">Unit</th>
                  <th style="width: 16%">Sub Unit</th>
                  <th style="width: 16%;">Conversion</th>
                  <th style="width: 16%;"></th>
                </tr>
                <tr>
                  <td>
                    <div class="form-group">
                      <div class="col-md-2" hidden>
                         <!-- <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar tp_6px" data-toggle="modal"  data-target="#Unit_Add_modal"></i></a> -->

                      </div>
                      <div class="col-md-12">
                        <?= $this->Form->input('unit',array('type'=>'select','empty' =>'Select','options'=>$Unit_list,'class'=>'form-control select_two_class','label'=>'','id'=>'unit',)) ?>
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="form-group">
                      <?= $this->Form->input('sub_unit',array('type'=>'select','empty' =>'Select','options'=>$Unit_list,'class'=>'form-control select_two_class','label'=>'','id'=>'sub_unit',)) ?>
                    </div>
                  </td>
                  <td>
                    <div class="form-group">
                      <input type="text" class="form-control " id="conversion">
                    </div>
                  </td>
                  <td><i class="fa fa-plus-circle fa-2x plus-btn" id='add_conversion_to_cart'></i></td>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($Unit_Conversion_list as $key => $value) : ?>
                  <tr>
                    <td><label><input type='hidden' class='unit_cart_id' value='<?= $value['Unit']['id']; ?>'><label><?= $value['Unit']['name']; ?></label></td>
                    <td><label><input type='hidden' class='sub_unit_cart_id' value='<?= $value['Unit']['id']; ?>'><label><?= $value['SubUnit']['name']; ?></label></td>
                    <td><input type='text' disabled="disabled" class='form-control conversion_cart' value='<?= $value['Unit']['conversion']; ?>'></td>
                    <td><i class="fa fa-minus-circle fa-2x plus-btn remove_conversion_old"></i></td>
                  </tr>
                <?php endforeach ?>
              </tbody>
              <tfoot>
                <tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                <!--   <td><button type="submit" name='data[Unit][process]' value="save" class="btn btn_save_success bdr_radious">Save</button></td> -->
          
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <div class="col-md-12 col-lg-12 col-sm-12">
          <div class="box-body table-responsive">
          <table class="table  border_cover_ table-bordered" id="tbl_unit">
              <thead>
                <tr class="blue-bg">
                  <th>Unit</th>
                  <th width="10%">Action</th>
                  <!-- <th style="width: 0%;">Delete</th> -->
                </tr>
              </thead>
              <tbody>
                <?php foreach ($Unit as $key_list => $value_list) : ?>
                  
                  <tr>
                    <td><input type='hidden' class='unit_list_id' value='<?= $value_list['Unit']['id']; ?>'><label><?= $value_list['Unit']['name']; ?></label></td>
                    <td><a> <i class="fa fa-2x fa-edit unit_edit"></i></a>
                    <a><i class="fa fa-2x fa-trash unit_delete"></i></a></td>
                  </tr>
                <?php endforeach ?>
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</section>
<?php require 'Unit_Add_modal.php'; ?>
<?php require('Unit_Edit_modal.php') ?>
<script>
  <?php require('unit.js'); ?>  
</script>