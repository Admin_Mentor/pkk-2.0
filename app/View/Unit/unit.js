$('#add_conversion_to_cart').click(function(){
var unit_id=$('#unit').val();
var unit_text=$('#unit option:selected').text();
var sub_unit_id=$('#sub_unit').val();
var sub_unit_text=$('#sub_unit option:selected').text();
var conversion=$('#conversion').val();
if(!unit_id)
{
  $('#unit').select2("open");
  return false;
}
if(!sub_unit_id)
{
  $('#sub_unit').select2("open");
  return false;
}
if(!conversion)
{
  $('#conversion').focus();
  return false;
}
// $('#unit_conversion_table tbody').append('<tr>\
//   <td>\
//   <input class=" " hidden name="data[Unit][unit_id][]" value="'+unit_id+'">\
//   <label>'+unit_text+'</label>\
//   </td>\
//   <td><input hidden class=" " value='+sub_unit_id+'>\
//   <label>'+sub_unit_text+'</label>\
//   </td>\
//   <td><input class="form-control" disabled="disabled" value='+conversion+' ></td>\
//   <td><i class="fa fa-minus-circle fa-2x plus-btn remove_conversion"></i></td>\
//   </tr>');
var data={
    unit_id:unit_id,
    sub_unit_id:sub_unit_id,
    conversion:conversion,
  };
  $.post("<?= $this->webroot ?>Unit/unit_conversion_add_ajax",data,function(response){
    if(response.result!='Success')
    {
     alert(response.result);
     $('#unit').select();
     return false;
   }
   history.pushState(null, null,response.website);
   window.location.href = response.website;
  

 },"json");

});
$('#add_Unit').click(function(){
  var data=$('#Unit_Add_Form').serialize();
  var url_address= "<?= $this->webroot; ?>Unit/unit_add_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      if(response.result=='Already Added')
      {
        $('#Unit_Add_Form')[0].reset();
        $('#Unit_Add_Modal').modal('toggle');
      }
      else
      {
        return false;
      }
    }
    else
    {
      location.reload();
      // $('#unit').append($("<option></option>").attr("value",response.key).text(response.value));
      // $('#unit').val($('#unit option:last-child').val()).trigger('change');
      // $('#sub_unit').append($("<option></option>").attr("value",response.key).text(response.value));
      // $('#Unit_Add_Form')[0].reset();
      // $('#Unit_Add_modal').modal('toggle');
      
    }
  }, "json");
});
$(document).on('click','.remove_conversion',function(){
  $(this).closest('tr').remove();
  $.fn.button_disable();
});
$(document).on('click','.remove_conversion_old',function(){
  if(!confirm("Are you sure?"))
  {
    return false;
  }
  var row_index=$(this).closest('tr').index();

  var id=$(this).closest('tr').find('td input.unit_cart_id').val();
  var url_address= '<?php echo $this->webroot; ?>'+'Unit/unit_conversion_delete/'+id;
  $.ajax({
    type: "POST",
    url:url_address,
    dataType:'json',
    success: function(data) {
      if(data.result!='Success')
      {
        alert(data.result);
        return false;
      }
      $('#unit_conversion_table tbody tr:eq('+row_index+')').remove();
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
});
$(document).on('click','.unit_delete',function(){
    if(!confirm("Are you sure?"))
    {
      return false;
    }
    var id=$(this).closest('tr').find('td input.unit_list_id').val();
    var rowindex = $(this).closest('tr').index();
    var url_address= '<?php echo $this->webroot; ?>'+'Unit/unit_delete_ajax/'+id;
    $.ajax({
      type: "post",  
      url:url_address,
    // data: data, 
    dataType:'json',
    success: function(response) {
      if(response.result!='Success')
      {
        alert(response.result);
        return false;
      }
      //location.reload();
      $('#tbl_unit tbody tr:eq('+rowindex+')').remove();
      $(this).closest('tr').remove();
    },
    error:function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
  });
});
$(document).on('click','.unit_edit',function(){
  var id=$(this).closest('tr').find('td input.unit_list_id').val();
  $.post( "<?= $this->webroot ?>Unit/get_Unit_ajax/"+id, function( data ) {
    if(data.result=='Success')
    {
      $('#UnitEditName').val(data.Unit.name);
      $('#UnitEditId').val(data.Unit.id);
      $('#Unit_Edit_modal').modal('show');
    }
    else
    {
      alert(data.result);
    }

  }, "json");
});
$('#edit_Unit').click(function(){
  var data=$('#Unit_Edit_Form').serialize();
  var unit_id=$('#UnitId').val();
  var url_address= "<?= $this->webroot; ?>Unit/unit_edit_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      location.reload();
      // $("#product_id option[value='" + product_id+ "']").remove();
      // $('#product_id').append($("<option></option>").attr("value",response.key).text(response.value));
      // $('#Product_Edit_Modal').modal('toggle');
      // $('#Product_Edit_Form')[0].reset();
      // $("#product_id").val(product_id).trigger('change');
     
    }
  }, "json");
});
$('#tbl_unit').DataTable({
"columnDefs": [
  {"targets": [ 1 ],"orderable": false ,},
  {"targets": [ 2 ],"orderable": false ,},

    ],    });