



<style type="text/css">

  .nav_down{
    display: grid;
    margin-top: 25px;
  }
  .nav_bdr_right {
    border: 1px solid #ccc;
    margin-bottom: 20px;
  }
  .nav_bdr_right li {
    padding-top: 10px;
    padding-left: 10px;
    padding-right: 10px;
  }
  .nav_bdr_right>li.active>a, .nav_bdr_right>li.active>a:focus, .nav_bdr_right>li.active>a:hover {
    color: #fff !important;
    cursor: default;
    background-color: #0a6d82 !important;
    border: 1px solid #ddd;
    border-bottom-color: transparent;
  }
  .nav_bdr_right a {
    color: #0a0a0a !important;
  }
  .rght_hnd{
    padding-right: 8px;
  }
  .tb_cntnt_bdr{
    border: 1px solid #ccc;
    margin-top: 25px;
    width:65%;
    margin-bottom: 25px;
  }
  .tb_cnt_lst ul li {
    padding-top: 5px;
  }
  .tb_cnt_lst ul {
    list-style-type: none;
    padding-top: 10px;
    padding-left: 9px;
  }
  .spn_lbl_check {
    font-family: 'Roboto Condensed', sans-serif;
    letter-spacing: 0.4px;
  }
  .Main_h4_head {
    font-family: 'Roboto Condensed', sans-serif;
    padding-left: 11px;
    margin-bottom: 0;
  }
  .ast_fxd {
    padding-right: 7px;
    color: #0a6d82;
  }
  .asset_wrapper_01 {
    margin-top: 25px;
  }
  .align_sub_area {
    padding-left: 55px;
  }
  .csh_bld {
    font-weight: 600;
  }

  .menu_selected{
    background-color: #d0d0d0;
  }

</style>
<section class="content-header">                              
  <h1> Role </h1>
</section>
<section class="content">
  <div class="row">
    <?php echo $this->Form->create('Role', ['type' => 'file','class'=>'form-horizontal','id'=>'Role_Form']); ?>
    <div class="box">
      <div class="row">
        <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
          <div class="form-group" style="margin-left: 5%;margin-top: 5%;">
            <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">
              <?= $this->Form->input('role_name',array('class'=>'form-control select_two_class','type'=>'select','id'=>'role_name','options'=>$UserRole_list)); ?>
              <?= $this->Form->input('menus',array('class'=>'form-control','type'=>'hidden','required','id'=>'menus',)); ?>
            </div>

            <br>
            <div class="col-sm-2"  style="margin-top: 1%;">
              <a href="#"> <i class="fa fa-plus-circle create_icon fa-2x ad-mar" data-toggle="modal"  data-target="#UserRole_Add_Modal"></i></a>
            </div>
          </div>

        </div>

        <div class="col-md-12 col-lg-12 col-sm-12">
          <div class="col-md-4 col-lg-4 col-sm-4">
            <ul class="nav nav-tabs nav_down nav_bdr_right">
              <table class="table top_menu_tbl">
                <thead >

                  <?php foreach ($Modules_list as $key => $value): ?>
                    <tr>
                      <th class="top_menu_thead">

                        <li><a data-toggle="tab" class="top_menu" href="#mytab2"><i class="fa fa-hand-o-right rght_hnd" aria-hidden="true"></i><input type="hidden" class="menulist"  value="<?= $key; ?>"><?= $value; ?></a></li>

                      </th>
                    </tr>
                  <?php endforeach ?>


                </thead>
              </table>
            </ul>
          </div>
          <div class="col-md-8 col-lg-8 col-sm-8 tb_cntnt_bdr" style="display:none;">
            <div class="tab-content  ">
              <input type="hidden" id="menus_full_permission">
              <input type="hidden" id="menus_view">
              <input type="hidden" id="menus_edit">
              <input type="hidden" id="menus_create">
              <input type="hidden" id="menus_delete">

              <div id="mytab2" class="tab-pane fade in">
                <table class="table side_menu_tbl">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col" class="spn_lbl_check">Name</th>
                      <th scope="col" class="spn_lbl_check">View</th>
                      <th scope="col" class="spn_lbl_check">Create</th>
                      <th scope="col" class="spn_lbl_check">Edit</th>
                      <th scope="col" class="spn_lbl_check">Delete</th>
                    </tr>
                  </thead>
                  <tbody>


                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <?= $this->Form->end(); ?>
  </div>
</section>
<?php require('UserRole_Add_Modal.php') ?>
<script type="text/javascript">
  $('#datepickerss').datepicker({
    autoclose: true
  });
  $('#datepickerss2').datepicker({
    autoclose: true
  });
  $('#datepickerss3').datepicker({
    autoclose: true
  });
  var user_role_id='<?= $user_role_id; ?>';
  $('#role_name').val(user_role_id);




  $(document).on('click','.top_menu',function(){
    var user_role_id=$('#role_name').val();
    $(".top_menu_tbl th").removeClass("menu_selected");
    $(this).closest('tr').find('th').addClass("menu_selected");
    var top_menu_id=$(this).closest('tr').find('th input.menulist').val();
    var top_menu=$.trim($(this).closest('tr').find('th.top_menu_thead').text());
    var data={
      top_menu_id:top_menu_id,
      top_menu:top_menu,
      user_role_id:user_role_id,
    };

    $.post( "<?= $this->webroot ?>User/NewRole_ajax",data,function( data ) {
      $('.side_menu_tbl tbody').empty();
//console.log(data);
$.each(data,function(key,value){
  var Checked_full='';
  var Checked_view='';
  var Checked_edit='';
  var Checked_create='';
  var Checked_delete='';
  if(value.full_permission==1)
    Checked_full='Checked';
  if(value.view_permission==1)
    Checked_view='Checked';
  if(value.edit_permission==1)
    Checked_edit='Checked';
  if(value.create_permission==1)
    Checked_create='Checked';
  if(value.delete_permission==1)
    Checked_delete='Checked';
  var create_flag=value.create_flag;
  var edit_flag=value.edit_flag;
  var delete_flag=value.delete_flag;
  var tr='<tr>';
  tr+='<td scope="row"><label class="checkbox-inline ">';
  tr+='<input type="hidden" value="'+value.menu_id+'" class="menu_id"><input data-id="'+value.menu_id+'" '+Checked_full+' class="menucheck_full menucheck" type="checkbox"><span class="spn_lbl_check">'+value.menu_name+'</span></label></td>';
  tr+='<td><label class="checkbox-inline"><input data-id="'+value.menu_id+'"  '+Checked_view+' class="menucheck_view menucheck check_menu" type="checkbox"></label></td>';
  if(create_flag==1)
  {
    tr+='<td><label class="checkbox-inline"><input data-id="'+value.menu_id+'"  '+Checked_create+' class="menucheck_create menucheck check_menu" type="checkbox"></label></td>';
  }
  else
  {
    tr+='<td></td>';
  }
  if(edit_flag==1)
  {
    tr+='<td><label class="checkbox-inline"><input data-id="'+value.menu_id+'"  '+Checked_edit+' class="menucheck_edit menucheck check_menu" type="checkbox"></label></td>';
  }
  else
  {
    tr+='<td></td>';
  }
  if(delete_flag==1)
  {
    tr+='<td><label class="checkbox-inline"><input data-id="'+value.menu_id+'"  '+Checked_delete+' class="menucheck_delete menucheck check_menu" type="checkbox"></label></td>';
  }
  else
  {
    tr+='<td></td>';
  }
  tr+='<tr>';
  $('.side_menu_tbl tbody').append(tr);
  $('.tb_cntnt_bdr').show();

});
}, "json");
  });
  $(document).on('change','#role_name',function(){
    $('.tb_cntnt_bdr').hide();
    $('.side_menu_tbl tbody').empty();
    $(".top_menu_tbl th").removeClass("menu_selected");
  });
  $('#add_UserRole').click(function(){
    var name=$('#Role_name_modal').val();
    if(name=='')
    {
      $('#Role_name_modal').focus();
      return false;
    }
    var website_url='<?php echo $this->webroot; ?>User/add_user_role_ajax';
    var data={
      name:name,
    };
    $.ajax({
      method: "POST",
      url: website_url,
      data: data,
      dataType:'json',
    }).done(function( data ) {
      if(data.result!='Success')
      {
        alert(data.result);
        return false;
      }

      $('#Role_name_modal').val('');
      $('#UserRole_Add_Modal').modal('toggle');
      $('#role_name').append($("<option></option>").attr("value",data.key).text(data.value));
      $('#role_name').val(data.key).trigger('change');
    });
  });
  $(document).on('change', '.menucheck_full', function() {
    if($(this).closest('tr').find('td input.menucheck_full').is(':checked')) {
      $(this).closest('tr').find('td input.menucheck').prop('checked',true);
    }
    else
    {
      $(this).closest('tr').find('td input.menucheck').prop('checked',false);
    }

  });
  $(document).on('change', '.check_menu', function() {
    if($(this).closest('tr').find('td input.check_menu').length==$(this).closest('tr').find('td input.check_menu:checked').length)
    {
      $(this).closest('tr').find('td input.menucheck_full').prop('checked',true);
    }
    {
      $(this).closest('tr').find('td input.menucheck_full').prop('checked',false);
    }

  });
  $(document).on('change', '.menucheck', function() {
    var user_role_id=$('#role_name').val();
    var menu_id=$(this).closest('tr').find('td input.menu_id').val();
    var full_permission_flag=0;
    var view_flag=0;
    var create_flag=0;
    var edit_flag=0;
    var delete_flag=0;


    if($(this).closest('tr').find('td input.menucheck_full').is(':checked')) {
      full_permission_flag=1;
    }
    if($(this).closest('tr').find('td input.menucheck_view').is(':checked')) {
      view_flag=1;
    }
    if($(this).closest('tr').find('td input.menucheck_create').is(':checked')) {
      create_flag=1;
    }
    if($(this).closest('tr').find('td input.menucheck_edit').is(':checked')) {
      edit_flag=1;
    }
    if($(this).closest('tr').find('td input.menucheck_delete').is(':checked')) {
      delete_flag=1;
    }

    var data={
      user_role_id:user_role_id,
      menu_id:menu_id,
      full_permission_flag:full_permission_flag,
      view_flag:view_flag,
      create_flag:create_flag,
      edit_flag:edit_flag,
      delete_flag:delete_flag,
    }
    var url_address = '<?php echo $this->webroot; ?>' + 'User/permission_update_ajax';
    $.ajax({
    type: "post", // Request method: post, get
    url: url_address,
    data: data, // post data
    success: function (response) {
    //$('#warehouse_id').html(response);
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
      alert(textStatus);
    }
    });

  });

</script>

