<link rel="stylesheet" href="<?= $this->webroot ?>css/user/css/my_style_4.css">
      <link rel="stylesheet" href="<?= $this->webroot ?>css/user/css/demo.css">
      <link rel="stylesheet" href="<?= $this->webroot ?>css/user/css/normalize.css">
      <link rel="stylesheet" href="<?= $this->webroot ?>css/user/css/tabs.css">
      <link rel="stylesheet" href="<?= $this->webroot ?>css/user/css/tabstyles.css">
<section class="content-header">
  <h2>Role</h2>
</section>
<?php
// pr($MenuList);
?>
<section class="content">
              <div class="box box-primary">
                <div class="row" style="margin-top: 3%;">
                  <div class="col-md-6 col-lg-6 col-sm-6">
                    <div class="col-md-2 col-lg-2 col-sm-2">
                      <label style="white-space: nowrap;">User Role</label>
                    </div>
                    <div class="col-md-5 col-lg-5 col-sm-5">
                      <select class="form-control">
                        <option>option 1</option>
                        <option>option 2</option>
                      </select>
                    </div>
                    <div class="col-md-1">
                      <a href="#"> <i class="fa fa-plus-circle fa-2x ad-mar" style="margin-top: 5px;" data-toggle="modal" data-target="#myModal"></i></a>
                    </div>
                    <div class="col-md-1">
                      <i class="fa fa-cogs edt_icn" aria-hidden="true"></i>
                    </div>
                    <div class="col-md-1">
                      <i class="fa fa-trash trsh_icn" aria-hidden="true"></i>
                    </div>
                  </div>
                </div>
                <hr>
                <div class="row" style="margin-top: 2%;">
                  <div class="col-md-12 col-lg-12 col-sm-12">
                    <section>
                      <div class="tabs tabs-style-bar">
                        <nav>
                          <ul>
                          <?php 
                          foreach ($top_modules as $key => $module) {
                          ?>
                            <span>
                              <div class="checkbox chk_top_styl">
                                <label><input type="checkbox" value=""></label>
                              </div>
                            </span>
                            <li><a href="#section-bar-<?= $module['id']; ?>"><span class="spn_lft"><?= $module['name']; ?>hh<?= $module['id']; ?></span>
                            </a></li>
                            <?php
                        }
                          ?>
                          </ul>
                         
                        </nav>
                        <div class="content-wrap">
                        <?php
                        foreach ($MenuList as $key => $value) 
                        {
// pr($value);
                        ?>

<!-- hghhgg -->
<section id="section-bar-<?= $key; ?>">


 <p>
                        <div class="stock_elements">
                          <h3 class="tab_head_01"><?= $key; ?></h3>
                          <div class="tab_btm_line"></div><br>
                          <ul>
                            <?php foreach ($value['main_ul'] as $keyli => $valuemli): ?>
                               <?php foreach ($valuemli['main_li'] as $keyli => $valueli): ?>
                            <li>
                          
                              <div class="row row_add_color_bg">
                                <div class="col-md-4">
                                  <div class="col-md-2"><a href="#"><span class="flt_lft"><div class="checkbox">
                                    <label><input type="checkbox" value=""></label>
                                  </div></span><span class="flt_rght font_weight_add"><?= $valueli['name'] ?></span></a></div>
                                  
                                </div>
                              </div>
                              <div class="row">
                                <div class="main_head_bg">
                                  <div class="col-md-4 mrg_lft_36px">
                                    <div class="col-md-4"><a href="#"><span class="flt_lft"><div class="checkbox">
                                      <label><input type="checkbox" value=""></label>
                                    </div></span><span class="flt_rght font_weight_add color_2">1.current asset</span></a></div>
                                    <div class="col-md-6 lft_of_mrg_2">
                                      <div class="col-md-3"><i class="fa fa-pencil-square-o edt_icn tp_algn_5px" aria-hidden="true"></i></div>
                                      <div class="col-md-3"><i class="fa fa-trash trsh_icn tp_algn_5px" aria-hidden="true"></i></div>
                                      <div class="col-md-3"><i class="fa fa-eye ey_icn icn_2_ey tp_algn_5px" aria-hidden="true"></i></div>
                                    </div>  
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="align_row_2">
                                  <div class="col-md-1 lft_28px"><a href="#"><span class="flt_lft"><div class="checkbox">
                                    <label><input type="checkbox" value=""></label>
                                  </div></span><span class="flt_rght">cash</span></a></div>
                                  <div class="col-md-3">
                                    <div class="col-md-2 no_padding"><i class="fa fa-pencil-square-o edt_icn tp_algn_5px add_new_stl" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-trash trsh_icn tp_algn_5px add_new_stl" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-eye ey_icn tp_algn_5px" aria-hidden="true"></i></div>
                                  </div>  
                                </div>
                              </div>
                              <div class="row">
                                <div class="align_row_2 add_algn_2">
                                  <div class="col-md-2"><a href="#"><span class="flt_lft"><div class="checkbox">
                                    <label><input type="checkbox" value=""></label>
                                  </div></span><span class="flt_rght">All</span></a></div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="align_row_2 add_algn_2">
                                  <div class="col-md-2"><a href="#"><span class="flt_lft"><div class="checkbox">
                                    <label><input type="checkbox" value=""></label>
                                  </div></span><span class="flt_rght">1. create</span></a></div>
                                  <div class="col-md-6 col-md-offset-1">
                                    <div class="col-md-2 no_padding"><i class="fa fa-pencil-square-o edt_icn" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-trash trsh_icn" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-eye ey_icn" aria-hidden="true"></i></div>
                                  </div>  
                                </div>
                                <div class="align_row_2 add_algn_2">
                                  <div class="col-md-2"><a href="#"><span class="flt_lft"><div class="checkbox">
                                    <label><input type="checkbox" value=""></label>
                                  </div></span><span class="flt_rght">2.transaction</span></a></div>
                                  <div class="col-md-6 col-md-offset-1">
                                    <div class="col-md-2 no_padding"><i class="fa fa-pencil-square-o edt_icn" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-trash trsh_icn" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-eye ey_icn" aria-hidden="true"></i></div>
                                  </div>  
                                </div>
                              </div>
                              
                              <hr>
                            </li>
                             <?php endforeach ?>
                               <?php endforeach ?>
                          </ul>
                        </div>
                      </p>
                    </section>
<!-- jkjkhjh -->


<?php
                        }
                        ?>





































                      <section id="section-bar-4"><p>
                        <div class="stock_elements">
                          <h3 class="tab_head_01">master</h3>
                          <div class="tab_btm_line"></div><br>
                          <ul>
                            <li>
                              <div class="row row_add_color_bg">
                                <div class="col-md-4">
                                  <div class="col-md-2"><a href="#"><span class="flt_lft"><div class="checkbox">
                                    <label><input type="checkbox" value=""></label>
                                  </div></span><span class="flt_rght font_weight_add">asset</span></a></div>
                                  <div class="col-md-5 lft_of_mrg">
                                    <div class="col-md-2"><i class="fa fa-pencil-square-o edt_icn tp_algn_5px" aria-hidden="true"></i></div>
                                    <div class="col-md-2"><i class="fa fa-trash trsh_icn tp_algn_5px" aria-hidden="true"></i></div>
                                    <div class="col-md-2"><i class="fa fa-eye ey_icn icn_2_ey tp_algn_5px" aria-hidden="true"></i></div>
                                  </div>  
                                </div>
                              </div>
                              <div class="row">
                                <div class="main_head_bg">
                                  <div class="col-md-4 mrg_lft_36px">
                                    <div class="col-md-4"><a href="#"><span class="flt_lft"><div class="checkbox">
                                      <label><input type="checkbox" value=""></label>
                                    </div></span><span class="flt_rght font_weight_add color_2">1.current asset</span></a></div>
                                    <div class="col-md-6 lft_of_mrg_2">
                                      <div class="col-md-3"><i class="fa fa-pencil-square-o edt_icn tp_algn_5px" aria-hidden="true"></i></div>
                                      <div class="col-md-3"><i class="fa fa-trash trsh_icn tp_algn_5px" aria-hidden="true"></i></div>
                                      <div class="col-md-3"><i class="fa fa-eye ey_icn icn_2_ey tp_algn_5px" aria-hidden="true"></i></div>
                                    </div>  
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="align_row_2">
                                  <div class="col-md-1 lft_28px"><a href="#"><span class="flt_lft"><div class="checkbox">
                                    <label><input type="checkbox" value=""></label>
                                  </div></span><span class="flt_rght">cash</span></a></div>
                                  <div class="col-md-3">
                                    <div class="col-md-2 no_padding"><i class="fa fa-pencil-square-o edt_icn tp_algn_5px add_new_stl" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-trash trsh_icn tp_algn_5px add_new_stl" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-eye ey_icn tp_algn_5px" aria-hidden="true"></i></div>
                                  </div>  
                                </div>
                              </div>
                              <div class="row">
                                <div class="align_row_2 add_algn_2">
                                  <div class="col-md-2"><a href="#"><span class="flt_lft"><div class="checkbox">
                                    <label><input type="checkbox" value=""></label>
                                  </div></span><span class="flt_rght">All</span></a></div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="align_row_2 add_algn_2">
                                  <div class="col-md-2"><a href="#"><span class="flt_lft"><div class="checkbox">
                                    <label><input type="checkbox" value=""></label>
                                  </div></span><span class="flt_rght">1. create</span></a></div>
                                  <div class="col-md-6 col-md-offset-1">
                                    <div class="col-md-2 no_padding"><i class="fa fa-pencil-square-o edt_icn" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-trash trsh_icn" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-eye ey_icn" aria-hidden="true"></i></div>
                                  </div>  
                                </div>
                                <div class="align_row_2 add_algn_2">
                                  <div class="col-md-2"><a href="#"><span class="flt_lft"><div class="checkbox">
                                    <label><input type="checkbox" value=""></label>
                                  </div></span><span class="flt_rght">2.transaction</span></a></div>
                                  <div class="col-md-6 col-md-offset-1">
                                    <div class="col-md-2 no_padding"><i class="fa fa-pencil-square-o edt_icn" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-trash trsh_icn" aria-hidden="true"></i></div>
                                    <div class="col-md-2 no_padding"><i class="fa fa-eye ey_icn" aria-hidden="true"></i></div>
                                  </div>  
                                </div>
                              </div>
                              
                              <hr>
                            </li>
                            
                          </ul>
                        </div>
                      </p>
                    </section>


















                    <section id="section-bar-5"><p>
                      test
                                <!-- <div class="stock_elements">
                                  <h3 class="tab_head_01">voucher</h3>
                                  <div class="tab_btm_line"></div><br>
                                  <ul>
                                    <li><a href="#"><i class="fa fa-hand-o-right hnd_rghts" aria-hidden="true"></i>voucher</a></li>
                                    <li><a href="#"><i class="fa fa-hand-o-right hnd_rghts" aria-hidden="true"></i>payment</a></li>
                                    <li><a href="#"><i class="fa fa-hand-o-right hnd_rghts" aria-hidden="true"></i>general voucher</a></li>
                                    <li><a href="#"><i class="fa fa-hand-o-right hnd_rghts" aria-hidden="true"></i>contra</a></li>
                                  </ul>
                                </div> -->
                              </p></section>


                              <section id="section-bar-6"><p>
                                test
                                <!-- <div class="stock_elements">
                                  <h3 class="tab_head_01">report</h3>
                                  <div class="tab_btm_line"></div><br>
                                  <ul>
                                    <li><a href="#"><i class="fa fa-chevron-down hnd_rghts" aria-hidden="true"></i><span class="txt_span_bld">accounting report</span></a></li>
                                    <div class="drop_sub_items">
                                      <ul>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>1.profit & loss</a></li>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>2.trial balance</a></li>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>3.balance sheet</a></li>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>4.daybook</a></li>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>5.ledger</a></li>
                                      </ul>
                                    </div>
                                    <li><a href="#"><i class="fa fa-chevron-down hnd_rghts" aria-hidden="true"></i><span class="txt_span_bld">sales report</span></a></li>
                                    <div class="drop_sub_items">
                                      <ul>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>1.sales register itemwise</a></li>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>2.sales register summary</a></li>

                                      </ul>
                                    </div>
                                    <li><a href="#"><i class="fa fa-chevron-down hnd_rghts" aria-hidden="true"></i><span class="txt_span_bld">purchase report</span></a></li>
                                    <div class="drop_sub_items">
                                      <ul>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>1.purchase register itemwise</a></li> 
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>2.purchase register summary</a></li>                  
                                      </ul>
                                    </div>
                                    <li><a href="#"><i class="fa fa-chevron-down hnd_rghts" aria-hidden="true"></i><span class="txt_span_bld">graphical report</span></a></li>
                                    <div class="drop_sub_items">
                                      <ul>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>1.graphics report</a></li>                    
                                      </ul>
                                    </div>
                                    <li><a href="#"><i class="fa fa-chevron-down hnd_rghts" aria-hidden="true"></i><span class="txt_span_bld">summary report</span></a></li>
                                    <div class="drop_sub_items">
                                      <ul>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>1.summary report</a></li>                   
                                      </ul>
                                    </div>
                                  </ul>
                                </div> -->
                              </p></section>
                              <section id="section-bar-7"><p>
                                test
                                <!-- <div class="stock_elements">
                                  <h3 class="tab_head_01">user</h3>
                                  <div class="tab_btm_line"></div><br>
                                  <ul>
                                    <li><a href="#"><i class="fa fa-hand-o-right hnd_rghts" aria-hidden="true"></i>user list</a></li>
                                    <li><a href="#"><i class="fa fa-chevron-down hnd_rghts" aria-hidden="true"></i><span class="txt_span_bld">roles</span></a></li>
                                    <div class="drop_sub_items">
                                      <ul>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>1.role list</a></li>
                                      </ul>
                                    </div>
                                  </ul>
                                </div> -->
                              </p></section>
                              <section id="section-bar-8"><p>
                                test
                                <!-- <div class="stock_elements">
                                  <h3 class="tab_head_01">hr</h3>
                                  <div class="tab_btm_line"></div><br>
                                  <ul>
                                    <li><a href="#"><i class="fa fa-chevron-down hnd_rghts" aria-hidden="true"></i><span class="txt_span_bld">manage staffs</span></a></li>
                                    <div class="drop_sub_items">
                                      <ul>
                                        <li><a href="#"><i class="fa fa-chevron-right hnd_rghts" aria-hidden="true"></i>1.manage staffs</a></li>
                                      </ul>
                                    </div>
                                  </ul>
                                </div> -->
                              </p>
                            </section>
                          </div><!-- /content -->
                        </div><!-- /tabs -->
                      </section>
                    </div>
                  </div>
                  <hr>
                </div>
              </section>
              <script src="<?= $this->webroot ?>js/user/js/cbpFWTabs.js"></script>
    <script src="<?= $this->webroot ?>js/user/js/modernizr.custom.js"></script>
<script type="text/javascript">
 
      (function() {
        [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
          new CBPFWTabs( el );
        });
      })();
    </script>