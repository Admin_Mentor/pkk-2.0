<style type="text/css">
  .add_btm_spc__{
    padding-bottom: 25px;
  }
</style>
<section class="content-header">
  <h1> Customer Profile </h1>
</section>
<section class="content">
  <div class="row">
    <?php echo $this->Form->create('Profile', ['type' => 'file','class'=>'form-horizontal','id'=>'Profile_Form']); ?>
    <div class="col-md-12">
      <div class="box box-primary add_btm_spc__">


        <div class="row" style="margin-top: 2%;">
          <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
              <?= $this->Form->input('company_name',array('class'=>'form-control','type'=>'text','required','id'=>'company_name','label'=>'Company Name',)); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3" hidden>
              <?= $this->Form->input('company_name_arabic', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'name_modal', 'label' => 'Company Name In Arabic','dir'=>'rtl')); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12" >
              <?= $this->Form->input('vat_code',array('class'=>'form-control','type'=>'text','id'=>'vat_code','label'=>'GST No:',)); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
              <?= $this->Form->input('country_id',array('class'=>'form-control select2','type'=>'select','options'=>$Country_list,'style'=>"width: 100%;",'id'=>'country_id','label'=>'Country',)); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
              <?= $this->Form->input('state_id',array('class'=>'form-control select2','type'=>'select','options'=>$State_list,'style'=>"width: 100%;",'id'=>'state_id','label'=>'State',)); ?>
            </div>
          </div>
        </div>
        <div class="row" style="margin-top: 1%;">
          <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
              <?= $this->Form->input('address_line_1',array('class'=>'form-control','type'=>'text','id'=>'address_line_1','label'=>'Address Line 1',)); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3" hidden>
              <?= $this->Form->input('address_line_1_arabic', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'name_modal', 'label' => 'Address Line 1 In Arabic','dir'=>'rtl')); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
              <?= $this->Form->input('address_line_2',array('class'=>'form-control','type'=>'text','id'=>'address_line_2','label'=>'Address Line 2',)); ?>
            </div>
             <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
              <?= $this->Form->input('mail_address',array('class'=>'form-control','type'=>'text','id'=>'mail_address','label'=>'Mail Address',)); ?>
            </div>
             <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
              <?= $this->Form->input('web_site_adddress',array('class'=>'form-control','type'=>'text','id'=>'web_site_adddress','label'=>'Web Site Address',)); ?>
            </div>
          </div>
        </div>
        <div class="row" style="margin-top: 1%;">
          <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="col-md-3 col-lg-3 col-sm-3" hidden>
               <?= $this->Form->input('address_line_2_arabic', array('class' => 'form-control name', 'type' => 'text', 'required', 'id' => 'name_modal', 'label' => 'Address Line 2 In Arabic','dir'=>'rtl')); ?>
            </div>
                  
            <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
              <?= $this->Form->input('mobile',array('class'=>'form-control number','type'=>'text','id'=>'mobile','label'=>'Mobile',)); ?>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
              <?= $this->Form->input('extra_mobile',array('class'=>'form-control number','type'=>'text','id'=>'extra_mobile','label'=>'Customer Care No:')); ?>
            </div>
              <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('logo',array('class'=>'form-control','type'=>'file','id'=>'logo','label'=>'Logo',)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('company_image',array('class'=>'form-control','type'=>'file','id'=>'company_image','label'=>'Company Image',)); ?>
                </div>
            <!-- <div class="row" style="margin-top: 6%;">
              <div class="col-md-12 col-lg-12 col-sm-12">
                
              
              </div>
            </div> -->


          </div>
        </div>





          <!-- <div class="row">
            <div class="col-md-12">
              <div class="test" style="margin-top: 2%;">
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('company_name',array('class'=>'form-control','type'=>'text','required','id'=>'company_name','label'=>'Company Name',)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('vat_code',array('class'=>'form-control','type'=>'text','id'=>'vat_code','label'=>'VAT Code',)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('country_id',array('class'=>'form-control select2','type'=>'select','options'=>$Country_list,'style'=>"width: 100%;",'id'=>'country_id','label'=>'Country',)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('state_id',array('class'=>'form-control select2','type'=>'select','options'=>$State_list,'style'=>"width: 100%;",'id'=>'state_id','label'=>'State',)); ?>
                </div>
              </div>
              <div class="test" style="margin-top: 10%;">
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('address_line_1',array('class'=>'form-control','type'=>'text','id'=>'address_line_1','label'=>'Address Line 1',)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('address_line_2',array('class'=>'form-control','type'=>'text','id'=>'address_line_2','label'=>'Address Line 2',)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('mail_address',array('class'=>'form-control','type'=>'text','id'=>'mail_address','label'=>'Mail Address',)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('web_site_adddress',array('class'=>'form-control','type'=>'text','id'=>'web_site_adddress','label'=>'Web Site Address',)); ?>
                </div>
              </div>
              <div class="test" style="margin-top: 18%;">
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('mobile',array('class'=>'form-control','type'=>'text','id'=>'mobile','label'=>'Mobile',)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('logo',array('class'=>'form-control','type'=>'file','id'=>'logo','label'=>'Logo',)); ?>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-4 col-xs-12">
                  <?= $this->Form->input('company_image',array('class'=>'form-control','type'=>'file','id'=>'company_image','label'=>'Company Image',)); ?>
                </div>
              </div>
              <div class="clearfix"></div>
              <div class="top_mrgn">
                <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                  <h3 class="product_label">Product  Configuration</h3>
                  <?php $attributes = array( 'legend' => false, ); echo $this->Form->radio('product_configuration_type', $Product_Configuration_list, $attributes); ?>
                </div>
              </div>
            </div>
          </div> -->

        </div>
      </div>
      <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <button class="btn btn-success btn_cstm_style pull-right">Save</button>
      </div>
      <?= $this->Form->end(); ?>
    </div>
  </section>