<section class="content-header">
  <h1> <span>Manage Roles</span> 
  <a href="<?php echo $this->webroot ?>User/NewRole"><input type="button" class="btn btn-success save pull-right create_icon" value="Create Role"></input></a>
  </h1>
  <!-- <br> -->
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary box_tp_brdr">
        <div class="row-wrapper">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
              </div>
              <div class="row-wrapper">
                <div class="row">
                  <div class="col-md-12">
                    <div class="box-body table">
                      <table class="table table-condensed table table boder boder datatable table-bordered" id="myTable">
                        <thead>
                          <tr class="blue-bg">
                            <th>Name</th>
                           <th width="10%">Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($RoleList as $key => $value): ?>
                            <tr>
                              <td>
                                <span><?= $value['UserRole']['role_name']; ?></span>
                                <span style='display: none' class='role_id'><?= $value['UserRole']['id']; ?></span>
                              </td>
                             <td>
                               <a href="<?= $this->webroot; ?>User/NewRole/<?= $value['UserRole']['id']  ?>" class="edit_icon"><i class="fa fa-2x fa-eye" aria-hidden="true"></i></a><a>
                             </td>
                            </tr>
                          <?php endforeach ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<div class="modal fade" id="User_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add User</h4>
      </div>
      <?= $this->Form->create('User', array('url' => array('controller' => 'User', 'action' => 'Add')));?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6 col-md-offset-3">
              <div class="col-md-12">
                <?php echo $this->Form->input('name',array('type'=>'text','class'=>'form-control','label'=>'User Name')); ?>
              </div>
               <div class="col-md-12">
               <?= $this->Form->input('user_role_id',array('class'=>'form-control select_two_class','type'=>'select','options'=>$RoleList,'style'=>"width: 100%;",'id'=>'user_role_id','label'=>'Role',)); ?>
               
              </div>
              <div class="col-md-12">
                <?php echo $this->Form->input('mobile',array('type'=>'text','class'=>'form-control')); ?>
              </div>
              <div class="col-md-12">
                <?php echo $this->Form->input('password',array('type'=>'password','class'=>'form-control','label'=>'Password')); ?>
              </div>
              <div class="col-md-12">
                <?php echo $this->Form->input('email',array('type'=>'text','class'=>'form-control')); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id='Add_user_btn'>Add User</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).on('click','.status_updation',function(){
    var user_id=$(this).closest('tr').find('td span.user_id').text();
    $.post( "<?= $this->webroot ?>User/status_updation_ajax/"+user_id,function( data ) {
      if(data.result!='Success')
      {
        alert(data.result);
        return false;
      }
    }, "json");
  });
</script>