<section class="content-header">
  <h1> <span>Manage Users</span> <span class='pull-right create_icon' ><button class='btn btn-success' data-toggle="modal" data-target="#User_modal"><i class="fa fa-plus-circle"></i> Add User</button></span></h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary box_tp_brdr">
        <div class="row-wrapper">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
              </div>
              <div class="row-wrapper">
                <div class="row">
                  <div class="col-md-12">
                    <div class="box-body table">
                      <table class="table table-condensed table table boder boder datatable table-bordered" id="myTable">
                        <thead>
                          <tr class="blue-bg">
                            <th>Staff Name</th>
                            <th>Role</th>
                            <th>Branch</th>
                            <th>Email</th>
                             <th>Username</th>
                              <th>Password</th>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($UserList as $key => $value): ?>
                            <tr>
                              <td>
                                <span><?= $value['Staff']['name']; ?></span>
                                <span style='display: none' class='user_id'><?= $value['User']['id']; ?></span>
                              </td>
                              <td><?= $value['UserRole']['role_name']; ?></td>
                              <td><?= $value['Branch']['name']; ?></td>
                              <td><?= $value['User']['email']; ?></td>
                              <td><?= $value['User']['name']; ?></td>
                              <td><?= $value['User']['password']; ?></td>
                              <td><?= date('d-m-Y',strtotime($value['User']['created_at'])); ?></td>
                              <td><?php $flag=$value['User']['flag']; if($flag==1) {$button_value="Active"; } else { $button_value="Deactive"; } ?> <a href="<?php echo $this->webroot.'User/status_updation_ajax/'.$value['User']['id']; ?>"><button class='btn'><?= $button_value; ?></button></a></td>
                              <td><i class="fa fa-2x fa-pencil-square-o blue-col edit_head" data-toggle="modal" data-target="#edit_User_modal"></i></td>
                            </tr>
                          <?php endforeach ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
<div class="modal fade" id="User_modal"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Add User</h4>
      </div>
      <?= $this->Form->create('User', array('url' => array('controller' => 'User', 'action' => 'Add')));?>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6 col-md-offset-3">
            <div class="col-md-12 staff_id">
               <?= $this->Form->input('staff_id',array('class'=>'form-control select_two_class','type'=>'select','options'=>$staff,'style'=>"width: 100%;",'id'=>'staff_id','label'=>'Staff','empty'=>[''=>'select'])); ?>
              </div>
               <div class="col-md-12">
               <?= $this->Form->input('user_role_id',array('class'=>'form-control select_two_class','type'=>'select','options'=>$RoleList,'style'=>"width: 100%;",'id'=>'user_role_id','label'=>'Role',)); ?>
             </div>
             <div class="col-md-12 branchid" hidden="">
               <?= $this->Form->input('branch_id',array('class'=>'form-control select_two_class','type'=>'select','options'=>$BranchList,'style'=>"width: 100%;",'id'=>'branch_id','label'=>'Branch','empty'=>[''=>'select'])); ?>
              </div>
              <div class="col-md-12">
                <?php echo $this->Form->input('name',array('type'=>'text','class'=>'form-control','label'=>'User Name','autocomplete' => 'off','readonly','id'=>'name')); ?>
              </div>
              <div class="col-md-12">
                <?php echo $this->Form->input('password',array('type'=>'password','class'=>'form-control','label'=>'Password','autocomplete' => 'off','id'=>'password')); ?>
              </div>
              <div class="col-md-12">
                <?php echo $this->Form->input('mobile',array('type'=>'text','class'=>'form-control','autocomplete' => 'off','onkeypress'=>'return isNumberKey(event);','required'=>'off','id' => 'mobile')); ?>
              </div>
              
              <div class="col-md-12">
                <?php echo $this->Form->input('email',array('type'=>'email','id' => 'email','class'=>'form-control','required'=>'off')); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id='Add_user_btn'>Add User</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>
<div class="modal fade" id="edit_User_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Edit User</h4>
      </div>
     <?php echo $this->Form->create('EditUser', ['class' => 'form-horizontal', 'id' => 'EditUser_Form']); ?>

      <!-- <?= $this->Form->create('EditUser', array('id'=>'EditUser'));?> -->
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-6 col-md-offset-3">
            <div class="col-md-12 editstaff" hidden="">
               <?= $this->Form->input('edit_staff_id',array('class'=>'form-control select2','type'=>'select','options'=>$staff,'style'=>"width: 100%;",'id'=>'edit_staff_id','label'=>'Staff','disabled')); ?>    
              </div>
              <div class="col-md-12">
               <?= $this->Form->input('edit_user_role_id',array('class'=>'form-control select2','type'=>'select','options'=>$RoleList,'style'=>"width: 100%;",'id'=>'edit_user_role_id','label'=>'Role','disabled')); ?>
              </div>
              <div class="col-md-12 editbranch" hidden="">
               <?= $this->Form->input('edit_branch_id',array('class'=>'form-control select2','type'=>'select','options'=>$BranchList,'style'=>"width: 100%;",'id'=>'edit_branch_id','label'=>'Branch','disabled')); ?>
              </div>
              <div class="col-md-12">
               
                <?php echo $this->Form->input('edit_name',array('type'=>'text','class'=>'form-control','label'=>'User Name','autocomplete' => 'off' ,'id'=>'name_edit')); ?>
                <?php echo $this->Form->input('user_edit_id',array('type'=>'hidden','class'=>'form-control','label'=>'User Name','autocomplete' => 'off' ,'id'=>'user_edit_id')); ?>
              </div>
              <div class="col-md-12">
                <?php echo $this->Form->input('edit_password',array('type'=>'text','class'=>'form-control','label'=>'Password','autocomplete' => 'off','id'=>'edit_password')); ?>
              </div>
             
              <div class="col-md-12">
                <?php echo $this->Form->input('edit_mobile',array('type'=>'text','class'=>'form-control','autocomplete' => 'off','onkeypress'=>'return isNumberKey(event);','required'=>'off','id'=>'edit_mobile','label'=>'mobile')); ?>
              </div>
              
              <div class="col-md-12">
                <?php echo $this->Form->input('edit_email',array('type'=>'email','class'=>'form-control','required'=>'off','id'=>'edit_email','label'=>'Email')); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary"  id='edit_user_btn'>Edit User</button>
      </div>
      <?php echo $this->Form->end(); ?>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).on('click','.status_updation',function(){
    var user_id=$(this).closest('tr').find('td span.user_id').text();
    $.post( "<?= $this->webroot ?>User/status_updation_ajax/"+user_id,function( data ) {
      if(data.result!='Success')
      {
        alert(data.result);
        return false;
      }
    }, "json");
  });
  
  
  $(document).on('change','#user_role_id',function() {
    var user_role_id=$('#user_role_id').val();
    var role = $('#user_role_id option:selected').text();
    if(role.toLowerCase() == 'branchadmin') {
      $('.branchid').show();
    }
    else
    {
      $('.branchid').hide();
    }
  });


  $(document).on('change','#staff_id',function(){
    var staff_id=$('#staff_id').val();
     $('#password').val("");
    $.post( "<?= $this->webroot ?>User/staff_ajax/"+staff_id,function( data ) {
     $('#name').val(data.code);
     $("#mobile").val(data.contact_no);
    }, "json");
 
  });
  
  
  $(document).on('click', '.edit_head', function () {
        var id = $(this).closest('tr').find('td span.user_id').text();
        $.post("<?= $this->webroot ?>User/User_get_ajax/" + id, function (responds) {  
           $(".editbranch").hide();
            if (responds.result != 'Success')
            {
                alert(responds.message);
                return false;
            }
           var rolename = responds.data.UserRole.role_name;
           if(rolename == '') {
           if(rolename.toLowerCase() == 'branchadmin')
           {
                 $(".editbranch").show();
           }
           else
           {
                 $(".editbranch").hide();
           }
           }
           
           var staffid = responds.data.User.staff_id;
           if(staffid == '' ||staffid == null) {
            $(".editstaff").show();
             $("#edit_staff_id").prop('disabled',false)
           }
           else
           {
            $(".editstaff").show();
             $("#edit_staff_id").prop('disabled',true)
           }
            $("#edit_staff_id").val(staffid).trigger('change');
            $("#edit_branch_id").val(responds.data.User.branch_id).trigger('change');
            $('#name_edit').val(responds.data.User.name);
            $('#user_edit_id').val(responds.data.User.id);
            $('#edit_password').val(responds.data.User.password);
            $('#edit_mobile').val(responds.data.User.mobile);
            $('#edit_email').val(responds.data.User.email);
            $('#edit_user_role_id').val(responds.data.User.user_role_id).trigger('change');
        }, "json");
  });
 
 
  $(document).on('click', '#edit_user_btn', function () {
  var data=$('#EditUser_Form').serialize();
  var edit_email = $("#edit_email");
  if(edit_email.val() != '') {
  if(!validateEmail(edit_email.val())) 
  {
    alert('Invalid Email Format');
    edit_email.focus();
    return false;  
  }
  }
  $.post( "<?= $this->webroot ?>User/User_edit",data ,function( data ) {
     if(data.result!='Success')
     {
       alert(data.result);
     }
   }, "json");
  location.reload();
  // location.href = <?= $this->webroot ?>+"User/UserList";
   //$('#edit_User_modal').modal('toggle');
  });

  $("#Add_user_btn").click(function()
  {
    var staff_id=$('#staff_id').val();
    if(!staff_id)
    {
      $('#staff_id').select2('open');
      return false;
    }
    var role = $('#user_role_id option:selected').text();
    if(role.toLowerCase() == 'branchadmin') {
           var branch = $("#branch_id");
           if(branch.val() == '') 
           {
             alert('Select  Branch');
             branch.focus();
             return false;
           } 
    }
    var email = $("#email");
    if(email.val() != '')
    {
    if(!validateEmail(email.val())) 
    {
    alert('Invalid Email Format');
    email.focus();
    return false;
    }
    }
    return true;
  });

function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;

          return true;
}

function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if( !emailReg.test( email ) ) {
        return false;
    } else {
        return true;
    }
}
</script>