
<style type="text/css">
.ul_items ul{
  list-style-type: none;
}
input[type=checkbox], input[type=radio] {
  margin: 13px 4px 6px 0px !important;
  margin-top: 1px\9 !important;
  line-height: normal !important;
}
input[type=checkbox] {
  zoom: 1.4 !important;
}

.ul_items ul li a{
  font-size: 22px;
}
.sub_li_2 {
  background-color: #efefef !important;
  padding-bottom: 9px;
  padding-left: 16px;
}
.primary-li {
  background-color: #dcdcdc;
}
.spn_label {
  font-weight: 500;
  font-size: 19px;
  font-style: normal;
  font-family: monospace;
}
.fa_share_stl {
  padding-left: 12px;
  padding-top: 22px;
  padding-bottom: 22px;
}
.stl_aftr_li{

  margin-top: 16px !important;
  margin-bottom: 16px !important;
}
</style>

<section class="content-header">
  <h2>Role</h2>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">

        <div class="row-wrapper">
          <div class="row">
            <div class="col-md-12" style="display: none;">
              <a href="<?= $this->webroot; ?>User/RoleList/">
                <!-- <i class="fa fa-hand-o-left pull-right" aria-hidden="true"></i> -->
                <button  value='' class="btn btn-success pull-right" >Back</button>
              </a>
            </div>
          </div>
          <div class="row">

            <?= $this->Form->create('UpdateRole', array('url' => array('controller' => 'User', 'action' => 'ViewRole')));?>
            <div class="col-md-12">










              <div class="form-horizontal" style="margin-top: 15px;">
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-12">
                      <div class="form-group">
                        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                          <?php
                          $user_role_menus = $user_role['UserRole']['menus'];
                          $permitted_menu_list = explode(',', $user_role_menus);
                          ?>
                          <?= $this->Form->input('role_name',array('class'=>'form-control','type'=>'text','required','id'=>'role_name','value'=> $user_role['UserRole']['role_name'])); ?>
                          <?= $this->Form->input('menus',array('class'=>'form-control','type'=>'hidden','required','id'=>'menus','value'=> $user_role['UserRole']['menus'] )); ?>
                          <?= $this->Form->input('id',array('class'=>'form-control','type'=>'hidden','required','id'=>'id','value'=> $user_role['UserRole']['id'] )); ?>
                        </div>
                      </div>
                      <div class="col-md-5 col-lg-5 col-sm-5">
                        <div class="form_chk_bx">
                          <span>
                            <input type="checkbox" value="1" <?php if($user_role['UserRole']['show_cost']==1){ ?> checked <?php } ?> name="data[UpdateRole][show_cost]" class="Chk_Bx_Mrgn">
                          </span>
                          <span><label style="white-space: nowrap;">Show Cost</label></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr>

                  <br>
                  <div class="col-md-12" > 
                    <div class="ul_items">
                      <ul class="" id='dashboard_menu_listl'>
                        <?php foreach ($MenuList as $key => $value): ?>
                          <li class="treeview main_li primary-li" >
                            <i class="fa fa-share fa_share_stl"></i> <span class='name spn_label'><?= $value['main_ul']['name'][0]; ?></span>
                            <!-- <i class="fa fa-angle-left pull-right"></i> -->
                            <ul class="treeview-menu sub_ul">
                              <?php foreach ($value['main_ul']['main_li'] as $keyli => $valueli): ?>
                                <?php if(empty($valueli['sub_li'])) : ?>
                                  <li  class='sub_li_2'>
                                    <input data-id="<?= $valueli['action_id'] ?>" <?= in_array($valueli['action_id'], $permitted_menu_list)? 'Checked' : '' ?> class="menucheck" type="checkbox"> <?= $valueli['name'] ?>
                                  </li>
                                <?php else : ?>
                                  <li class="treeview sub_li_2 second_sub_li">
                                   <i class="fa fa-pie-chart"></i> <span><?= $valueli['name']; ?></span> 
                                   <div class="stl_aftr_li"></div>
                                   <!-- <i class="fa fa-angle-left pull-right"></i>  -->
                                   <ul class="treeview-menu second_sub_ul">
                                    <?php foreach ($valueli['sub_li'] as $keysub_li => $valuesub_li): ?>
                                      <?php if(empty($valuesub_li['third_sub_li'])) : ?>
                                        <li class='sub_li sub_li_2'>
                                          <!-- <i class="fa fa-circle-o"></i> -->
                                          <input data-id="<?= $valuesub_li['action_id'] ?>" <?= in_array($valuesub_li['action_id'], $permitted_menu_list)? 'Checked' : '' ?> class="menucheck" type="checkbox"> 

                                          <?= $valuesub_li['name']; ?>
                                        </li>
                                      <?php else : ?>
                                        <li class="treeview third_sub_li">
                                          <i class="fa fa-pie-chart"></i> <span><?= $valuesub_li['name']; ?></span>
                                          <!-- <i class="fa fa-angle-left pull-right"></i> -->
                                          <ul class="treeview-menu third_sub_ul">
                                            <?php foreach ($valuesub_li['third_sub_li'] as $keythird_sub_li => $valuethird_sub_li): ?>
                                              <li class='fourth_sub_li sub_li_2'>
                                                <!-- <i class="fa fa-circle-o"></i> -->
                                                <input data-id="<?= $valuethird_sub_li['action_id'] ?>" <?= in_array($valuethird_sub_li['action_id'], $permitted_menu_list)? 'Checked' : '' ?> class="menucheck" type="checkbox"> 

                                                <?= $valuethird_sub_li['name']; ?>
                                              </li>
                                            <?php endforeach ?>
                                          </ul>
                                        </li>
                                      <?php endif; ?>
                                    <?php endforeach; ?>
                                  </ul>
                                </li>
                              <?php endif; ?>
                            <?php endforeach; ?>
                          </ul>
                        </li>
                      <?php endforeach; ?>
                    </ul>
                  </div>


                </div>
                <br>
              </div>
            </div>










          </div>
          <div class="row-wrapper">
            <div class="col-md-12">
              <div class="box-body table-responsive no-padding">

              </div>
            </div>
          </div>
          <div class="col-md-12 col-lg-12 col-xs-12 col-sm-3">
            <br>
            <button type='submit' value='save' class="btn btn-success pull-right" >Update</button>
            <br>
            <br>

          </div>
        </div>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>
</div>
</section>
<script type="text/javascript">
  $('.menucheck').on('click',function(){
    var list="";
    $('.menucheck').each(function () { 
      if($(this).is(":checked")){
        var id=$(this).data('id');
        list = list +id+',';

      }
    });

    $('#menus').val(list);
  });

</script>
