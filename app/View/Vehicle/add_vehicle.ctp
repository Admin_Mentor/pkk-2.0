<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<section class="content-header">
  <h1>Vehicle</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <!-- <div class="box-header">
       <a href="<?php echo $this->webroot ?>Vehicle/VehicleLog"><input type="button" class="btn btn-success save pull-right" value="Vehicle Log" style="margin-top: 2px;"></input></a>
    </div> -->
    <div class="box-body">
      <div class="row">
      <?= $this->Form->create('Vehicle', array('url' => array('controller' => 'Vehicle', 'action' => 'AddVehicle'),'id'=>'Vehicle_Form','type' => 'file'));?>
      <div class="col-md-12 col-sm-12">
        <div id="vehicleDiv" class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('registration_number',array('type'=>'text','id'=>'registration_number','class'=>'form-control','required'=>'required'));
          ?>
          </div>
        </div>
        <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('make',array('type'=>'text','id'=>'make','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
         <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
            <?php echo $this->Form->input('model',array('type'=>'text','id'=>'model','class'=>'form-control','required'=>'required')); ?>
          </div>
        </div>
          <div class="col-md-3 col-sm-3">
          <div class="form-group col-md-12">
             <button style="margin-top: 8%;" class="save " id='save_button' >Save</button>
          <button style="margin-top: 8%; display:none" id='edit_button' value='' class="save">Edit</button>
          </div>
        </div>
      </div>
     </div>
              <?= $this->Form->end(); ?>
  </div>
  <div class="row">
        <hr>
        <div class="col-md-12">
          <div class="box-body table">
            <table class="table table-hover boder table-bordered" id='table_data_vehicle'>
              <thead>
                <tr class="blue-bg">
                  <th>Sl.No:</th>
                  <th>Number</th>
                  <th>Make</th>
                  <th>Model</th>
                  <th width="10%">Action</th>
                  <!-- <th>Delete</th> -->
              </tr>
              </thead>
           <tbody>
            </tbody>
          </table>
        </div>
      </div>
      <br>
    </div>
</div>
</section>
<script type="text/javascript">
   $('#table_data_vehicle').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Vehicle/get_vehicle_ajax",
      "type": "POST",
      "dataSrc": "records",
    },
    dom: 'Bfrtip',
      lengthMenu: [
    [10, 25, 50,-1],
    ['10 rows', '25 rows', '50 rows','show all']
    ],
    buttons: [
   // 'colvis',
    //{ extend: 'excel', title: 'Stock Report', exportOptions: { columns: ':visible' } }, 
    { extend: 'excel',   title: 'Vehicle', exportOptions: { columns: [1,2,3] } },
    'pageLength',
    ],
    "columns": [
    { "data" : "VehicleNo.id" },
    { "data" : "VehicleNo.number" },
    { "data" : "VehicleNo.make" },
    { "data" : "VehicleNo.model" },
    { "data" : "VehicleNo.action" },
    //{ "data" : "VehicleNo.delete" },
    ],
    "columnDefs": [
    ],
     
  });
  $(document).on('click','.edit_VehicleNo',function(){
    var vehicle_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Vehicle/get_vehiclel_details_ajax/"+vehicle_id,function( data ) {
       $('#registration_number').val(data.VehicleNo.number);
      $('#make').val(data.VehicleNo.make);
       $('#model').val(data.VehicleNo.model);
      $('#save_button').css('display','none');
      $('#edit_button').css('display','');
      $("#registration_number").append("<input type='text' id='VehicleNo_id' name='data[Vehicle][id]' value='"+data.VehicleNo.id+"'>");
      $('#Vehicle_Form').attr('action','<?= $this->webroot; ?>Vehicle/EditVehicle');

  }, "json");
});
 $(document).on('click','.delete_vehicle_no',function(){
    if(!confirm("Are you sure?"))
        {
          return false;
        }
    var vehicle_id=$(this).data('id');
    $.post( "<?= $this->webroot ?>Vehicle/DeleteVehicleNo/"+vehicle_id,function( data ) {
      if(data.result!="Success")
           {
            alert(data.result);
           }
      table = $('#table_data_vehicle').dataTable();
      table.fnDraw(); 
  }, "json");
});
</script>
  </script>