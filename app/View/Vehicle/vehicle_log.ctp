<style type="text/css">
.pls_btn_add_stl_tp{
  padding-top: 28px;
}
</style>
<section class="content-header">
  <h1>Vehicle log</h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-header">
    </div>
    <div class="box-body">
      <table class="table table-hover boder" id='table_data'>
        <thead>
          <tr class="blue-bg">
            <th>Staff</th>
            <th>Vehicle Number</th>
            <th>Make</th>
            <th>Model</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</section>

<script type="text/javascript">
  $('#table_data').DataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Vehicle/get_vehiclelog_ajax",
      "type": "POST",
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "Staff.name" },
    { "data" : "VehicleNo.number" },
    { "data" : "VehicleNo.make" },
    { "data" : "VehicleNo.model" },
    ],
    "columnDefs": [
    ],
     
  });
</script>
<script type="text/javascript">
  $('input').click(function(){
    $(this).select();
  });
</script>
<script type="text/javascript">
  $('#add_Designation').click(function(){
    var data=$('#Desgination_Add_Form').serialize();
    var url_address= "<?= $this->webroot; ?>Hr/designation_add_ajax";
    $.post( url_address,data, function( response ) {
      if(response.result!='Success')
      {

        $('#designation').val(response.key).trigger('change');
        $('#Desgination_Add_Form')[0].reset();
        $('#Designation_Add_Modal').modal('toggle');

      }
      else
      {
        $('#designation').append($("<option></option>").attr("value",response.key).text(response.value));
        $('#Desgination_Add_Form')[0].reset();
        $('#Designation_Add_Modal').modal('toggle');
      }
    }, "json");
  });
  $('#add_Role').click(function(){
    var data=$('#Role_Add_Form').serialize();
    var url_address= "<?= $this->webroot; ?>Hr/role_add_ajax";
    $.post( url_address,data, function( response ) {
      if(response.result!='Success')
      {

        $('#role').val(response.key).trigger('change');
        $('#Role_Add_Form')[0].reset();
        $('#Role_Add_Modal').modal('toggle');

      }
      else
      {
        $('#role').append($("<option></option>").attr("value",response.key).text(response.value));
        $('#Role_Add_Form')[0].reset();
        $('#Role_Add_Modal').modal('toggle');
      }
    }, "json");
  });
  $('#Designation_Edit_Btn').click(function(){
    var designation=$('#designation').val();
    if(!designation)
    {
      $('#designation').select2('open');
      return false; 
    }
    var url_address= "<?= $this->webroot; ?>Hr/get_designation/"+designation;
    $.get( url_address, function( response ) {
      if(response.result!='Success')
      {
        alert(response.result);
        return false;
      }
      else
      {
        $('#DesignationEditName').val(response.data.name);
        $('#DesignationEditId').val(response.data.id);
        $('#Designation_Edit_Modal').modal('show');
      }
    }, "json");
  });
  $('#edit_Designation').click(function(){
    var data=$('#Designation_Edit_Form').serialize();
    var designation=$('#designation').val();
    var url_address= "<?= $this->webroot; ?>Hr/designation_edit_ajax";
    $.post( url_address,data, function( response ) {
      if(response.result!='Success')
      {
        alert(response.result);
        return false;
      }
      else
      {
        $("#designation option[value='" + designation+ "']").remove();
        $('#designation').append($("<option></option>").attr("value",response.key).text(response.value));
        $('#designation').val($('#designation option:last-child').val()).trigger('change');
        $('#Designation_Edit_Form')[0].reset();
        $('#Designation_Edit_Modal').modal('toggle');
      }
    }, "json");
  });
  $('#Designation_Delete_Btn').click(function(){
    var designation=$('#designation').val();
    if(!designation)
    {
      $('#designation').select2('open');
      return false; 
    }
    if(!confirm("Are you sure?"))
    {
      return false;
    }
    var url_address= "<?= $this->webroot; ?>Hr/designation_delete_ajax/"+designation;
    $.get( url_address, function( response ) {
      if(response.result!='Success')
      {
        alert(response.result);
        return false;
      }
      else
      {
        $("#designation option[value='" + designation+ "']").remove();
      }
    }, "json");
  });
  $('#Role_Edit_Btn').click(function(){
    var role=$('#role').val();
    if(!role)
    {
      $('#role').select2('open');
      return false; 
    }
    var url_address= "<?= $this->webroot; ?>Hr/get_role/"+role;
    $.get( url_address, function( response ) {
      if(response.result!='Success')
      {
        alert(response.result);
        return false;
      }
      else
      {
        $('#RoleEditName').val(response.data.name);
        $('#RoleEditId').val(response.data.id);
        $('#Role_Edit_Modal').modal('show');
      }
    }, "json");
  });
  $('#edit_Role').click(function(){
    var data=$('#Role_Edit_Form').serialize();
    var role=$('#role').val();
    var url_address= "<?= $this->webroot; ?>Hr/role_edit_ajax";
    $.post( url_address,data, function( response ) {
      if(response.result!='Success')
      {
        alert(response.result);
        return false;
      }
      else
      {
        $("#role option[value='" + role+ "']").remove();
        $('#role').append($("<option></option>").attr("value",response.key).text(response.value));
        $('#role').val(response.key).trigger('change');
        $('#Role_Edit_Form')[0].reset();
        $('#Role_Edit_Modal').modal('toggle');
      }
    }, "json");
  });
  $('#Role_Delete_Btn').click(function(){
    var role=$('#role').val();
    if(!role)
    {
      $('#role').select2('open');
      return false; 
    }
    if(!confirm("Are you sure?"))
    {
      return false;
    }
    var url_address= "<?= $this->webroot; ?>Hr/role_delete_ajax/"+role;
    $.get( url_address, function( response ) {
      if(response.result!='Success')
      {
        alert(response.result);
        return false;
      }
      else
      {
        $("#role option[value='" + role+ "']").remove();
      }
    }, "json");
  });
</script>