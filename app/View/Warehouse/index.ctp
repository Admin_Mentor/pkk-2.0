<style type="text/css">
  .type_one {
    padding-left: 31%;
    padding-top: 5%;
  }
  .type-modal {
    color: #337ab7;
  }
  .type_tab{
    margin-top: 3%;
  }
  @media only screen and (min-device-width : 320px) and (max-device-width : 480px) {
    .type_one {
      padding-left: 0% !important;
    }
  }
  @-webkit-keyframes flash {
    from, 50%, to {
      opacity: 1;
    }
    25%, 75% {
      opacity: 0;
    }
  }
  @keyframes flash {
    from, 50%, to {
      opacity: 1;
    }
    25%, 75% {
      opacity: 0;
    }
  }
  .flash {
    -webkit-animation-name: flash;
    animation-name: flash;
    /*animation-delay: 0.1s;*/
    animation-duration: 2s;
    background-color:red;
  }
  .tp_6px{
    margin-top: 1.5%;
  }
</style>
<section class="content-header">
  <h1> Warehouse
  <div class="col-sm-1 pull-right" style="margin-right: 20px">
              <a href="#"><button class='btn btn-success' data-toggle="modal"  data-target="#addwarehouse">Add Warehouse</button></a>
            </div>
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="row">
          <div class="col-md-12">
          <!--   <div class="text-right" style="margin-right: 2%;">
            <br>
              <a href="#"><button class='btn btn-success' data-toggle="modal"  data-target="#addwarehouse">Add Warehouse</button></a>
            </div> -->
          </div>
        </div>
            <div class="box-body">
      <table class="table table-hover boder table-bordered" id='table_data'>
        <thead>
          <tr class="blue-bg">
            <th>#</th>
            <th>Name</th>
            <th>Description</th>
            <th width="20%">Damage Warehouse Of</th>
            <th>Is Damage</th>
            <th width="10%">Action</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
     <?php require('Warehouse_Add_modal.php') ?>
    <?php require('Warehouse_Edit_Modal.php') ?>
        </div>
      </div>
    </section>


    <script type="text/javascript">
  table_data=$('#table_data').dataTable({
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?= $this->webroot ?>Warehouse/Warehouse_Table_ajax",
      "type": "POST",
      "dataSrc": "records",
    },
    "columns": [
    { "data" : "Warehouse.id" },
    { "data" : "Warehouse.name" },
    { "data" : "Warehouse.description" },
    { "data" : "Warehouse.warehouse_id" },
    { "data" : "Warehouse.is_damage" },
    { "data" : "Warehouse.action" },
    ],
    "columnDefs": [
    ],
  });
  <?php require('warehouse.js'); ?>

</script>


    