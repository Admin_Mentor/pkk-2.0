$('#modal_warehouse_name').keyup(function(){
  var modal_warehouse_name=$(this).val();
  var data={
   warehouse_name:modal_warehouse_name
 };
 var url_address= '<?php echo $this->webroot; ?>'+'Stock/warehouse_search';
 $.ajax({
  type: "post",
  url:url_address,
  data: data,
  success: function(response) {
    if(response=="Yes")
    {
      $('#warehouse_error').html('This Warehouse is already taken');
      $('#add_warehouse').attr('disabled',true);
    }
    else
    {
      $('#warehouse_error').html('');
    }
  },
  error:function (XMLHttpRequest, textStatus, errorThrown) {
    alert(textStatus);
  }
});
});
$('.warehouse_disable').keyup(function(){
  var modal_warehouse_name=$('#modal_warehouse_name').val().trim();
  if(modal_warehouse_name!="")
  {
    $('#add_warehouse').attr('disabled',false);
  }
  else{
    $('#add_warehouse').attr('disabled',true);
  }
});
$.fn.warehouse_append=function(result){
  $('#warehouse_id').append(result);
  var latest_value = $("#warehouse_id option:last").val();
  $('#warehouse_id').val(latest_value).change();
  $('#addwarehouse').modal('hide');
  $('#warehouse_id_modal').append(result);
  var latest_value = $("#warehouse_id_modal option:last").val();
  $('#warehouse_id_modal').val(latest_value).change();
}
$('#warehouse_id').change(function(){
  var id=$(this).val();
  $('#warehouse_id_modal').val(id).trigger('change.select2');;
});
$('#warehouse_id_modal').change(function(){
  var id=$(this).val();
  $('#warehouse_id').val(id);
});
// $('#add_warehouse').click(function(){
//   var modal_warehouse_name=$('#modal_warehouse_name').val();
//   if(!modal_warehouse_name)
//   {
//     $('#modal_warehouse_name').focus();
//       return false;
//   }
//   var modal_warehouse_desc=$('#modal_warehouse_desc').val();
//   var toggle_button_for_is_damage=$('.toggle_button_for_is_damage').prop("checked");
//   if(toggle_button_for_is_damage==true)
//   {
//      var damage_warehouse_id=$('#damage_warehouse_id').val();
//      if(!damage_warehouse_id)
//      {
//       $('#damage_warehouse_id').select2('open');
//       return false;
//      }
//      var is_damage=1;
//   }
//   else
//   {
//    var damage_warehouse_id="";
//     var is_damage="";
//   }
//   var data={
//     modal_warehouse_name:modal_warehouse_name, modal_warehouse_desc:modal_warehouse_desc,
//     is_damage:is_damage,
//     damage_warehouse_id:damage_warehouse_id,
//   };
//   var url_address= '<?php echo $this->webroot; ?>Warehouse/warehouse_add_ajax';
//   $.ajax({
//     method: "POST",
//     url:url_address,
//     data: data,
//   }).done(function( result ) {
//     $.fn.warehouse_append(result);
//     $('#modal_warehouse_name').val('');
//     $('#modal_warehouse_desc').val('');
//      $("#warehousediv").hide();
//     $("#damage_warehouse_id").prop("disabled", true);  
//      $('.toggle_button_for_is_damage')[1].checked = true;
//   });
// });
$('#add_warehouse').click(function(){
  var modal_warehouse_name=$('#modal_warehouse_name').val();
  if(!modal_warehouse_name)
  {
    $('#modal_warehouse_name').focus();
      return false;
  }
  var modal_warehouse_desc=$('#modal_warehouse_desc').val();
  var toggle_button_for_is_damage=$('.toggle_button_for_is_damage').prop("checked");
  if(toggle_button_for_is_damage==true)
  {
     var damage_warehouse_id=$('#damage_warehouse_id').val();
     if(!damage_warehouse_id)
     {
      $('#damage_warehouse_id').select2('open');
      return false;
     }
     var is_damage=1;
  }
  else
  {
   var damage_warehouse_id="";
    var is_damage="";
  }
  var data={
    modal_warehouse_name:modal_warehouse_name, modal_warehouse_desc:modal_warehouse_desc,
    is_damage:is_damage,
    damage_warehouse_id:damage_warehouse_id,
  };
    $.post( "<?= $this->webroot ?>Warehouse/new_warehouse_add_ajax",data ,function( data ) {
        if(data.result!='Success')
        {
            alert(data.result);
            return false;
        }
        $('#modal_warehouse_name').val('');
        $('#modal_warehouse_desc').val('');
        $("#warehousediv").hide();
        $("#damage_warehouse_id").prop("disabled", true);  
        $('.toggle_button_for_is_damage')[1].checked = true;
        $('#warehouse_id').append($("<option></option>").attr("value",data.key).text(data.value));
        $('#warehouse_id').val(data.key);
        $('#warehouse_id').trigger('change');
        table = $('#table_data').dataTable();
    table.fnDraw();
    }, "json");
    $('#addwarehouse').modal('toggle');
});
$('#Warehouse_Edit_Btn').click(function(){
  var warehouse_id=$('#warehouse_id').val();
  if(!warehouse_id)
  {
    $('#warehouse_id').select2('open');
    return false; 
  }
  var url_address= "<?= $this->webroot; ?>Warehouse/warehouse_get_ajax/"+warehouse_id;
  $.get( url_address, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $('#WarehouseEditName').val(response.data.name);
      $('#WarehouseEditId').val(response.data.id);
      // $('#BrandEditCode').val(response.data.code);
      $('#Warehouse_Edit_Modal').modal('show');
    }
  }, "json");
});
$('#edit_Warehouse').click(function(){
  var data=$('#Warehouse_Edit_Form').serialize();
  var warehouse_id=$('#warehouse_id').val();
  var url_address= "<?= $this->webroot; ?>Warehouse/warehouse_edit_ajax";
  $.post( url_address,data, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $("#warehouse_id option[value='" + warehouse_id+ "']").remove();
      $("#Warehouse_id_modal option[value='" + warehouse_id+ "']").remove();
      $('#warehouse_id').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#Warehouse_id_modal').append($("<option></option>").attr("value",response.key).text(response.value));
      $('#warehouse_id').val($('#warehouse_id option:last-child').val()).trigger('change');
      $('#Warehouse_Edit_Form')[0].reset();
      $('#Warehouse_Edit_Modal').modal('toggle');
    }
     table = $('#table_data').dataTable();
    table.fnDraw();
  }, "json");
});
 $(document).on('click', '.edit_Warehouses', function () {
    //alert("k");
  //var warehouse_id=$('#warehouse_id').val();
 var warehouse_id=$(this).attr('table_id');
 //alert(warehouse_id);
  // if(!warehouse_id)
  // {
  //   $('#warehouse_id').select2('open');
  //   return false; 
  // }
  var url_address= "<?= $this->webroot; ?>Warehouse/warehouse_get_ajax/"+warehouse_id;
  $.get( url_address, function( response ) {
    if(response.result!='Success')
    {
      alert(response.result);
      return false;
    }
    else
    {
      $('#WarehouseEditName').val(response.data.name);
      $('#WarehouseEditEditDescription').val(response.data.description);
      $('#WarehouseEditId').val(response.data.id);
      // $('#BrandEditCode').val(response.data.code);
      $('#Warehouse_Edit_Modal').modal('show');
    }
  }, "json");
});
  $(document).on('change','.toggle_button_for_is_damage',function(){
   var is_damage=$(this).val();
   if(is_damage=="Yes")
   {
      $("#warehousediv").show(); 
      $("#damage_warehouse_id").removeAttr("disabled");
    //  $('#damage_warehouse_id').select2('open');
   }
   else
   {
   $("#warehousediv").hide();  
    $("#damage_warehouse_id").prop("disabled", true);
   }
   var url_address= '<?php echo $this->webroot; ?>Warehouse/get_warehouse_ajax';
    $.ajax({
      method: "POST",
      url:url_address,
    // data: data,  
    dataType:'json',
  }).done(function( result ) {
    $("#damage_warehouse_id").html('');
    $('#damage_warehouse_id').html(result.option);
    $('#damage_warehouse_id').val('');
  });
  });