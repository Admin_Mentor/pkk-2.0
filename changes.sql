ALTER TABLE `purchases` ADD `grn_order_id` INT NULL DEFAULT NULL AFTER `purchase_order_id`;
ALTER TABLE `purchases` ADD `current_date` DATETIME NULL DEFAULT CURRENT_TIMESTAMP AFTER `date_of_delivered`;

<!-- in new purchase, -->
<!-- select grn first. -->
<!-- qty should be 0. zero qty product shud be removed while save. -->
mrp(should update in product), last purchase rate, rate, tax, free product,
total in all columns
insurance, insurance tax
total product tax+insurance tax.
product amount+insurance amount
<!-- after save.. status of grn should be updated to 'Purchased' -->

10000
1500
1000
100
12600
direct expense -> insurance Paid 1000
direct expense -> tax on insurance paid  100 


ALTER TABLE `purchases` ADD `miscellanious_expense_amount` FLOAT(8,2) NULL DEFAULT '0' AFTER `insurance_total`, ADD `miscellanious_expense_tax` FLOAT(8,2) NULL DEFAULT '0' AFTER `miscellanious_expense_amount`, ADD `miscellanious_expense_total` FLOAT(8,2) NULL DEFAULT '0' AFTER `miscellanious_expense_tax`;

ALTER TABLE `grn_order_items` ADD `mrp` FLOAT(16,2) NOT NULL AFTER `quantity`;

GRN:
<!-- need total quantity in grn -->
<!-- correct si no -->
<!-- change save to update after sale -->
<!-- show mrp in grn -->
<!-- enter key control -->
Purchase:
<!-- need si no in purchase also -->
<!-- purchase shud be of same order as in grn -->
ALTER TABLE `journals` ADD `external_voucher` VARCHAR(200) NULL DEFAULT NULL AFTER `voucher_no`;

ALTER TABLE `purchased_order_items` ADD `delivered_quantity` FLOAT(8,2) NOT NULL DEFAULT '0' AFTER `quantity`;

ALTER TABLE `complaints` CHANGE `date` `complaint_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE `complaints` ADD `account_head_id` INT NOT NULL AFTER `product_id`;



ALTER TABLE `complaints` ADD `status` VARCHAR(250) NULL AFTER `complaint`;
ALTER TABLE `complaints` ADD `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `status`, ADD `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `created_at`;
ALTER TABLE `complaints` ADD `type` VARCHAR(250) NULL AFTER `status`;
INSERT INTO `warehouses` (`id`, `name`, `description`) VALUES (NULL, 'ERPT Temp Damaged', 'Temporary Damaged');
INSERT INTO `warehouses` (`id`, `name`, `description`) VALUES (NULL, 'Main Temp Damaged', 'Temporary Damaged');
ALTER TABLE `complaints` ADD `flag` INT NOT NULL DEFAULT '1' AFTER `type`;



ALTER TABLE `complaints` ADD `remarks` VARCHAR(250) NULL AFTER `complaint`;
ALTER TABLE `complaints` ADD `technician_id` INT NULL AFTER `status`;
ALTER TABLE `complaints` CHANGE `status` `status` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'Rejected';
ALTER TABLE `complaints` ADD `second_status` VARCHAR(10) NULL DEFAULT 'Rejected' AFTER `status`;
ALTER TABLE `complaints` ADD `technician_remarks` VARCHAR(250) NULL AFTER `technician_id`;
ALTER TABLE `complaints` CHANGE `status` `status` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '', CHANGE `second_status` `second_status` VARCHAR(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '';
ALTER TABLE `complaints` ADD `customer_feedback` VARCHAR(250) NULL AFTER `technician_remarks`;
ALTER TABLE `complaints` CHANGE `second_status` `second_status` VARCHAR(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '';

INSERT INTO `warehouses` (`id`, `name`, `description`) VALUES (NULL, 'Permanent Stock', 'All Permanent Stock');

-----------5-6-2018-----------
spare  parts page
product type
brand
product

manage techinition page

damaged stock page
ERPT Temp Damaged
Main Temp Damaged


replacement
service

replacement page

replacememt
create delivery note -> print move to temp warehouse

after moving temp
move to permanent warehouse
or to stock


UPDATE `account_heads` SET `name` = 'ABDULGAFOOR OUTSTANDING' WHERE `account_heads`.`id` = 27;

ALTER TABLE `parties` ADD `bank_name` VARCHAR(50) NOT NULL AFTER `gstin`, ADD `branch` VARCHAR(50) NOT NULL AFTER `bank_name`, ADD `account_number` VARCHAR(50) NOT NULL AFTER `branch`;

ALTER TABLE `products` ADD `barcode_image` VARCHAR(200) NULL DEFAULT NULL AFTER `barcode_name`;
CREATE TABLE `colours` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `name` VARCHAR(100) NOT NULL , `fill_r` INT(10) NOT NULL , `fill_g` INT(10) NOT NULL , `fill_b` INT(10) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

ALTER TABLE `products` ADD `colour_id` INT(15) NOT NULL DEFAULT '1' AFTER `barcode_image`;
INSERT INTO `colours` (`id`, `name`, `fill_r`, `fill_g`, `fill_b`) VALUES (NULL, 'White', '255', '255', '255'), (NULL, 'Red', '255', '0', '0'), (NULL, 'Blue', '0', '0', '255') ,(NULL, 'Green', '0', '255', '0');

ALTER TABLE `executive_journals` ADD `external_voucher` VARCHAR(200) NOT NULL AFTER `voucher_no`;



ALTER TABLE `complaints` ADD `priority` VARCHAR(250) NOT NULL DEFAULT 'yellow' AFTER `remarks`;
ALTER TABLE `sale_targets` ADD `incentive` FLOAT(8,2) NULL DEFAULT '0' AFTER `target`;
ALTER TABLE `journals` CHANGE `voucher_no` `voucher_no` BIGINT(191) NULL DEFAULT NULL;

--barcode print size   2.55906 * 1.5748 inch


ALTER TABLE `staffs` ADD `date_of_joining` DATE NULL AFTER `name`;
ALTER TABLE `staffs` ADD `designation` VARCHAR(250) NULL AFTER `date_of_joining`;

ALTER TABLE `sale_items` ADD `add_disc` VARCHAR(200) NOT NULL DEFAULT '0' AFTER `mrp`;

ALTER TABLE `quotation_items` ADD `add_disc` VARCHAR(200) NOT NULL DEFAULT '0' AFTER `mrp`;

ALTER TABLE `staffs` ADD `month_leave` FLOAT(8,2) NOT NULL DEFAULT '0' AFTER `contact_no`;

ALTER TABLE `basic_pays` ADD `allowance` FLOAT(8,2) NOT NULL DEFAULT '0' AFTER `amount`;

ALTER TABLE `leave_managements` ADD `carry_leave` FLOAT(8,2) NULL DEFAULT '0' AFTER `staff_leave`;

ALTER TABLE `leave_managements` CHANGE `total_working_days` `total_working_days` FLOAT(8,2) NOT NULL, CHANGE `leave_taken` `leave_taken` FLOAT(8,2) NOT NULL DEFAULT '0', CHANGE `lop` `lop` FLOAT(8,2) NOT NULL DEFAULT '0', CHANGE `staff_leave` `staff_leave` FLOAT(8,2) NOT NULL DEFAULT '0', CHANGE `working_days` `working_days` FLOAT(8,2) NOT NULL;

ALTER TABLE `staffs` ADD `carry_leave` FLOAT(8,2) NOT NULL DEFAULT '0' AFTER `month_leave`;
ALTER TABLE `executive_journals` DROP `external_voucher`;

ALTER TABLE `executive_journals` CHANGE `receipt_no` `external_voucher` VARCHAR(190) 

ALTER TABLE `leave_managements` ADD `leave_encashment` FLOAT(8,2) NOT NULL DEFAULT '0' AFTER `carry_leave`;

ALTER TABLE `complaints` ADD `warehouse_id` INT(11) NOT NULL DEFAULT '0' AFTER `customer_feedback`;

ALTER TABLE `day_registers` ADD `km` FLOAT(8,2) NOT NULL DEFAULT '0' AFTER `date`;

ALTER TABLE `day_registers` ADD `present` INT(11) NOT NULL DEFAULT '1' AFTER `customer_group_id`;

ALTER TABLE `closing_days` ADD `km` FLOAT(8,2) NOT NULL DEFAULT '0' AFTER 
`status`;

ALTER TABLE `pay_structures` ADD `leave_encashment` FLOAT(8,2) NOT NULL DEFAULT '0' AFTER `bonus`;


ALTER TABLE `complaints` ADD `executive_id` INT(111) NOT NULL AFTER `product_id`;

ALTER TABLE `technician_day_registers` ADD `km` FLOAT(8,2) NOT NULL DEFAULT '0' AFTER `date`;

ALTER TABLE `cheques` CHANGE `cheque_amount` `cheque_amount` DOUBLE(13,2) NOT NULL;

ALTER TABLE `closing_days` ADD `day_register_id` INT NOT NULL DEFAULT '0' AFTER `km`;

ALTER TABLE `parties` ADD `ifsc_number` VARCHAR(200) NOT NULL AFTER `account_number`;
ALTER TABLE `parties` ADD `contact_person` VARCHAR(200) NOT NULL AFTER `state_id`;

ALTER TABLE `day_registers` ADD `status` INT(1) NOT NULL DEFAULT '0' AFTER `present`;

ALTER TABLE `pay_structures` ADD `daily_allowance` FLOAT(8,2) NOT NULL DEFAULT '0' AFTER `allowance`;

ALTER TABLE `technician_day_closes` ADD `technician_day_register_id` INT NOT NULL DEFAULT '0' AFTER `status`;

ALTER TABLE `quotations` ADD `print` INT(11) NOT NULL DEFAULT '0'  AFTER `flag`;

ALTER TABLE `staffs` ADD `role` INT(11) NULL DEFAULT NULL AFTER `designation`;
ALTER TABLE `executive_journals` ADD `day_register_id` INT(11) NOT NULL DEFAULT '0' AFTER `flag`;
ALTER TABLE `remarks` ADD `created_at` TIMESTAMP NULL DEFAULT NULL AFTER `remark`;
ALTER TABLE `no_sales` ADD `created_at` TIMESTAMP NULL DEFAULT NULL AFTER `description`;


CREATE TABLE `mozaik_live`.`executive_bonus_details` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `receipt_no` VARCHAR(100) NOT NULL , `executive_id` INT(10) NOT NULL , `invoice_no` VARCHAR(100) NOT NULL , `amount` DOUBLE(10,4) NOT NULL , `sale_date` DATE NOT NULL , `receipt_date` DATE NOT NULL , `eligibility` VARCHAR(20) NOT NULL , `bonus_amount` DOUBLE(10,4) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `executive_bonus_details` CHANGE `receipt_no` `receipt_no` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

--18-10-2018
CREATE TABLE `mozaik_live`.`user_menu_permissions` ( `id` INT(10) NOT NULL AUTO_INCREMENT , `user_role_id` INT(10) NOT NULL , `menu_id` INT(10) NOT NULL , `full_permission` INT(10) NOT NULL , `view_permission` INT(10) NOT NULL , `edit_permission` INT(10) NOT NULL , `create_permission` INT(10) NOT NULL , `delete_permission` INT(10) NOT NULL , `created_at` TIMESTAMP NULL DEFAULT NULL , `updated_at` TIMESTAMP NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

INSERT INTO `menus` (`id`, `module_id`, `name`, `action`, `menu_id`, `active`) VALUES (NULL, '1', 'Stock Transfer', 'Stock/StockTransfer', '0', '1'), (NULL, '1', 'Sample Stock Transfer', 'Stock/SampleStockTransfer', '0', '1');
UPDATE `menus` SET `name` = 'Stock Transfer List' WHERE `menus`.`id` = 2;
UPDATE `menus` SET `name` = 'Sample Stock Transfer List' WHERE `menus`.`id` = 13;

ALTER TABLE `menus` ADD `create_flag` INT(10) NOT NULL DEFAULT '1' AFTER `action`, ADD `edit_flag` INT(10) NOT NULL DEFAULT '1' AFTER `create_flag`, ADD `delete_flag` INT(10) NOT NULL DEFAULT '1' AFTER `edit_flag`;
INSERT INTO `menus` (`id`, `module_id`, `name`, `action`, `create_flag`, `edit_flag`, `delete_flag`, `menu_id`, `active`) VALUES (NULL, '7', 'Role', 'User/NewRole', '1', '1', '1', '0', '1');
update menus set action='Reports/Ledger' where id=42;
INSERT INTO `menus` (`id`, `module_id`, `name`, `action`, `create_flag`, `edit_flag`, `delete_flag`, `menu_id`, `active`) VALUES (NULL, '4', 'Complaint', 'Executive/ComplaintManagement', '1', '1', '1', '0', '1');
INSERT INTO `menus` (`id`, `module_id`, `name`, `action`, `create_flag`, `edit_flag`, `delete_flag`, `menu_id`, `active`) VALUES (NULL, '8', 'Sale Order', 'Sale/ApproveQuotation', '1', '1', '1', '0', '1');
INSERT INTO `menus` (`id`, `module_id`, `name`, `action`, `create_flag`, `edit_flag`, `delete_flag`, `menu_id`, `active`) VALUES (NULL, '3', 'GRR', 'Purchase/GrnOrder', '1', '1', '1', '0', '1');
INSERT INTO `menus` (`id`, `module_id`, `name`, `action`, `create_flag`, `edit_flag`, `delete_flag`, `menu_id`, `active`) VALUES (NULL, '3', 'Purchase Order', 'Purchase/PurchaseOrder', '1', '1', '1', '0', '1');
INSERT INTO `menus` (`id`, `module_id`, `name`, `action`, `create_flag`, `edit_flag`, `delete_flag`, `menu_id`, `active`) VALUES (NULL, '2', 'Sales Return', 'Sale/SalesReturn', '1', '1', '1', '0', '1'), (NULL, '2', 'Sale', 'Sale/Sale', '1', '1', '1', '0', '1');


ALTER TABLE `mozaik_live`.`users` 
DROP INDEX `users_email_unique` ;

ALTER TABLE `user_roles` CHANGE `show_cost` `show_cost` TINYINT(4) NOT NULL DEFAULT '1';

ALTER TABLE `customers` ADD `priority` INT(11) NULL DEFAULT NULL AFTER `code`;
ALTER TABLE `bank_details` ADD `shwn_in_vouchr` INT(11) NOT NULL DEFAULT '0' AFTER `account_no`;

UPDATE `pkk_test`.`menus` SET `module_id` = '1', `action` = 'Warehouse/index' WHERE (`id` = '118');
INSERT INTO `user_menu_permissions` (`id`, `user_role_id`, `menu_id`, `full_permission`, `view_permission`, `edit_permission`, `create_permission`, `delete_permission`, `created_at`, `updated_at`) VALUES (NULL, '1', '118', '1', '1', '1', '1', '1', '2020-08-27 16:52:01', '2020-08-27 16:52:01');