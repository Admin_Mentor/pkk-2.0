ALTER TABLE `journals` CHANGE `date` `date` DATE NULL DEFAULT NULL;
DELETE FROM `journals` WHERE `journals`.`credit` = 0;
CREATE TABLE journals_history AS SELECT * FROM journals;
TRUNCATE TABLE `journals`;
DELETE FROM `executive_journals` WHERE `executive_journals`.`credit` = 0;
CREATE TABLE executive_journals_history AS SELECT * FROM executive_journals;
TRUNCATE TABLE `executive_journals`;

CREATE TABLE purchases_history AS SELECT * FROM purchases;
TRUNCATE TABLE `purchases`;
CREATE TABLE purchased_items_history AS SELECT * FROM purchased_items;
TRUNCATE TABLE `purchased_items`;
CREATE TABLE purchase_expenses_history AS SELECT * FROM purchase_expenses;
TRUNCATE TABLE `purchase_expenses`;

CREATE TABLE 	purchase_orders_history AS SELECT * FROM purchase_orders;
TRUNCATE TABLE `purchase_orders`;
CREATE TABLE 	purchased_order_items_history AS SELECT * FROM purchased_order_items;
TRUNCATE TABLE `purchased_order_items`;
CREATE TABLE 	purchase_returns_history AS SELECT * FROM purchase_returns;
TRUNCATE TABLE `purchase_returns`;
CREATE TABLE 	purchase_return_items_history AS SELECT * FROM purchase_return_items;
TRUNCATE TABLE `purchase_return_items`;
SET SQL_SAFE_UPDATES=0;
CREATE TABLE sales_history AS SELECT * FROM sales;
TRUNCATE TABLE `sales`;
CREATE TABLE sale_items_history AS SELECT * FROM sale_items;
TRUNCATE TABLE `sale_items`;

DELETE FROM `quotations` WHERE `quotations`.`account_head_id` = 0;
DELETE FROM `quotations` WHERE `quotations`.`id` = 4058;
CREATE TABLE quotations_history AS SELECT * FROM quotations;
TRUNCATE TABLE `quotations`;
CREATE TABLE quotation_items_history AS SELECT * FROM quotation_items;
TRUNCATE TABLE `quotation_items`;

CREATE TABLE 	no_sales_history AS SELECT * FROM no_sales;
TRUNCATE TABLE `no_sales`;

CREATE TABLE 	sales_returns_history AS SELECT * FROM sales_returns;
TRUNCATE TABLE `sales_returns`;
CREATE TABLE 	sales_return_items_history AS SELECT * FROM sales_return_items;
TRUNCATE TABLE `sales_return_items`;
SET SQL_SAFE_UPDATES=1;